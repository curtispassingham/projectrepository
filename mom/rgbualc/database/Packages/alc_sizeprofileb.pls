CREATE OR REPLACE PACKAGE BODY ALC_SIZE_PROFILE_SQL IS
-----------------------------------------------------------------------------------------------------------------------
-- Name    : GET_LOCATIONS
-- Purpose : This function gets the list of stores based on the location search criteria
--           i.e., Store Grade Group, Store Grade, Location List, Location Trait, Allocation Group
--                 Single Store, All Stores
--           This function is a common function uses in all the computations of size profile whereever stores are needed
--           Returns a table of numbers (stores)
-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_LOCATIONS(I_store_grade_group   IN NUMBER,
                       I_store_grade         IN NUMBER,
                       I_location_list       IN NUMBER,
                       I_location_trait      IN NUMBER,
                       I_allocation_group    IN NUMBER,
                       I_all_stores          IN NUMBER,
                       I_single_store        IN NUMBER,
                       I_all_whs             IN NUMBER,
                       I_wh                  IN NUMBER)
RETURN OBJ_NUMERIC_ID_TABLE IS

   L_program          VARCHAR2(61) := 'ALC_SIZE_PROFILE_SQL.GET_LOCATIONS';
   L_start_time       TIMESTAMP    := SYSTIMESTAMP;

   C_LOCS_REFCURSOR   SYS_REFCURSOR;
   L_locs_table       OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();
   L_loc              NUMBER(30);
   L_loc_query        VARCHAR2(2048);
   L_error_message    VARCHAR2(255)  := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   L_loc_query := '';

   if I_all_stores = 1 then
      L_loc_query := 'select store from store';
   elsif I_single_store is NOT NULL then
      L_loc_query := 'select store from store where store = ' || I_single_store;
   end if;

   if I_all_whs = 1 then
      L_loc_query := 'select wh from wh';
   elsif I_wh is NOT NULL then
      L_loc_query := 'select wh from wh where wh = ' || I_wh;
   end if;

   if LENGTH(L_loc_query) = 0 or L_loc_query is NULL then
      if I_store_grade_group is NOT NULL then
         L_loc_query := 'select store from store_grade_store where store_grade_group_id = ' || I_store_grade_group  || ' and store_grade =  ' || I_store_grade;
      end if;

      if I_location_list is NOT NULL then
         if LENGTH(L_loc_query) > 0 then
            L_loc_query := L_loc_query || ' INTERSECT ';
         end if;

         L_loc_query := L_loc_query || ' select location store from loc_list_detail where loc_list = ' || I_location_list;
      end if;

      if I_location_trait is NOT NULL then
         if LENGTH(L_loc_query) > 0 then
            L_loc_query := L_loc_query || ' INTERSECT ';
         end if;

         L_loc_query := L_loc_query || ' select store from loc_traits_matrix where loc_trait = ' || I_location_trait;
      end if;
   end if;

   open C_LOCS_REFCURSOR for L_loc_query;
   loop
      fetch C_LOCS_REFCURSOR into L_loc;
      EXIT when C_LOCS_REFCURSOR%NOTFOUND;
      L_locs_table.EXTEND;
      L_locs_table(L_locs_table.last) := L_loc;
   end loop;
   close C_LOCS_REFCURSOR;

   LOGGER.LOG_TIME(L_program, L_start_time);

   return L_locs_table;

EXCEPTION
   when OTHERS then
      L_error_message := SQLERRM || ' from ALC_SIZE_PROFILE_SQL.GET_LOCATIONS';
      return NULL;
END GET_LOCATIONS;
-----------------------------------------------------------------------------------------------------------------------
-- Name    : GET_GID_PROFILES
-- Purpose : This function gets the list of GID Profiles (GID Headers)
--           Returns a cursor for the resultset
-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_GID_PROFILES(O_error_message        IN OUT VARCHAR2,
                          I_session_id           IN     VARCHAR2,
                          I_result_type          IN     NUMBER,
                          I_id                   IN     NUMBER,
                          I_size_profile_level   IN     NUMBER,
                          I_dept                 IN     VARCHAR2,
                          I_class                IN     VARCHAR2,
                          I_subclass             IN     VARCHAR2,
                          I_style                IN     VARCHAR2,
                          I_store_grade_group    IN     NUMBER,
                          I_store_grade          IN     NUMBER,
                          I_location_list        IN     NUMBER,
                          I_location_trait       IN     NUMBER,
                          I_allocation_group     IN     NUMBER,
                          I_all_stores           IN     NUMBER,
                          I_single_store         IN     NUMBER,
                          I_all_whs              IN     NUMBER,
                          I_wh                   IN     NUMBER,
                          I_diff_group_id1       IN     VARCHAR2,
                          I_diff_group_id2       IN     VARCHAR2,
                          I_diff_group_id3       IN     VARCHAR2,
                          I_diff_group_id4       IN     VARCHAR2)
RETURN NUMBER IS

   L_program                     VARCHAR2(61) := 'ALC_SIZE_PROFILE_SQL.GET_GID_PROFILES';
   L_start_time                  TIMESTAMP    := SYSTIMESTAMP;

   C_GID_HEADERS                 SYS_REFCURSOR;
   L_mainquery                   VARCHAR2(4096);
   L_sizegroupquery              VARCHAR2(1024);
   L_merchierquery               VARCHAR2(4096);
   L_stylequery                  VARCHAR2(4096) := '';
   L_gid_id                      NUMBER(15);
   L_gid                         VARCHAR2(20);
   L_gid_desc                    VARCHAR2(100);
   L_gid_profile_id              NUMBER(5);
   L_size_profile_desc           VARCHAR2(1024);
   L_size_profile_level          NUMBER(1);
   L_prev_size_profile_desc      VARCHAR2(1024) := ' ';
   L_prev_gid                    VARCHAR2(40);
   L_prev_gid_desc               VARCHAR2(120);
   L_prev_size_profile_level     NUMBER(1);

   L_store_grade_group           VARCHAR2(16);
   L_store_grade                 VARCHAR2(16);
   L_location_list               VARCHAR2(16);
   L_location_trait              VARCHAR2(16);
   L_allocation_group            VARCHAR2(16);
   L_all_stores                  VARCHAR2(16);
   L_single_store                VARCHAR2(16);
   L_all_whs                     VARCHAR2(16);
   L_wh                          VARCHAR2(16);

   L_gid_profile_ids_table       OBJ_NUMERIC_ID_TABLE          := OBJ_NUMERIC_ID_TABLE();
   L_gid_profile_headers_table   OBJ_GID_PROFILE_HEADERS_TABLE := OBJ_GID_PROFILE_HEADERS_TABLE();

   L_i                           NUMBER(5);
   L_seq_id                      NUMBER(4);
   L_sizeprofiles_rowcount       NUMBER(5);
   L_found_non_gid_data          NUMBER(1)     := 0;
   L_error_message               VARCHAR2(255) := NULL;
   L_gid_profile_ids             OBJ_NUMERIC_ID_TABLE;

   L_gid_profile_ids_parsed      VARCHAR2(4000);
   L_gid_profile_ids_seq         NUMBER(15);

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   O_error_message  := '';
   L_sizegroupquery := '';
   L_merchierquery  := '';
   L_seq_id         := 1;

   delete
     from alc_session_size_profile_ratio
    where session_id  = I_session_id
      and result_type = I_result_type;

   delete
     from alc_session_size_profile
    where session_id  = I_session_id
      and result_type = I_result_type;

   delete
     from alc_session_gid_profile_list
    where gid_profile_list_id IN (select gid_profile_list_id
                                    from alc_session_gid_profile
                                   where session_id  = I_session_id
                                     and result_type = I_result_type);

   delete
     from alc_session_gid_profile
    where session_id  = I_session_id
      and result_type = I_result_type;

   if I_store_grade_group is NULL then
      L_store_grade_group := 'NULL';
   else
      L_store_grade_group := TO_CHAR(I_store_grade_group);
   end if;

   if I_store_grade is NULL then
      L_store_grade := 'NULL';
   else
      L_store_grade := TO_CHAR(I_store_grade);
   end if;

   if I_location_list is NULL then
      L_location_list := 'NULL';
   else
      L_location_list := TO_CHAR(I_location_list);
   end if;

   if I_location_trait is NULL then
      L_location_trait := 'NULL';
   else
      L_location_trait := TO_CHAR(I_location_trait);
   end if;

   if I_allocation_group is NULL then
      L_allocation_group := 'NULL';
   else
      L_allocation_group := TO_CHAR(I_allocation_group);
   end if;

   if I_all_stores is NULL then
      L_all_stores := 'NULL';
   else
      L_all_stores := TO_CHAR(I_all_stores);
   end if;

   if I_single_store is NULL then
      L_single_store := 'NULL';
   else
      L_single_store := TO_CHAR(I_single_store);
   end if;

   if I_dept is NOT NULL then
      L_merchierquery := ' dept = TO_CHAR(' || I_dept || ')';

      if I_class is NOT NULL then
         L_merchierquery := L_merchierquery || ' and class = TO_CHAR(' || I_class || ')';

         if I_subclass is NOT NULL then
            L_merchierquery := L_merchierquery || ' and subclass = TO_CHAR(' || I_subclass || ')';
         else
            L_merchierquery := L_merchierquery || ' and subclass is NULL ';
         end if;
      else
         L_merchierquery := L_merchierquery || ' and class is NULL and subclass is NULL';
      end if;
   end if;

   if I_diff_group_id1 is NOT NULL then
      L_sizegroupquery  := ' (size1 in (select diff_id from diff_group_detail where diff_group_id = ''' ||   I_diff_group_id1 || ''')';
   else
      L_sizegroupquery  := ' (size1 is NULL ';
   end if;

   if I_diff_group_id2 is NOT NULL then
      L_sizegroupquery  := L_sizegroupquery || ' and size2 in (select diff_id from diff_group_detail where diff_group_id = ''' ||   I_diff_group_id2 || ''')';
   else
      L_sizegroupquery  := L_sizegroupquery || ' and size2 is NULL ';
   end if;

   if I_diff_group_id3 is NOT NULL then
      L_sizegroupquery  := L_sizegroupquery || ' and size3 in (select diff_id from diff_group_detail where diff_group_id = ''' ||   I_diff_group_id3 || ''')';
   else
      L_sizegroupquery  := L_sizegroupquery || ' and size3 is NULL ';
   end if;

   if I_diff_group_id4 is NOT NULL then
      L_sizegroupquery  := L_sizegroupquery || ' and size4 in (select diff_id from diff_group_detail where diff_group_id = ''' ||   I_diff_group_id4 || '''))';
   else
      L_sizegroupquery  := L_sizegroupquery || ' and size4 is NULL) ';
   end if;

   if I_style is NULL then
      if I_diff_group_id1 is NOT NULL then
         L_stylequery := L_stylequery || ' (diff_1_aggregate_ind = ''N'' and diff_1 = ''' || I_diff_group_id1 || ''') ';
      end if;

      if I_diff_group_id2 is NOT NULL then
         if LENGTH(L_stylequery) > 0 then
            L_stylequery := L_stylequery || ' or ';
         end if;
         L_stylequery := L_stylequery || ' (diff_2_aggregate_ind = ''N'' and diff_2 = ''' || I_diff_group_id2 || ''') ';
      end if;

      if I_diff_group_id3 is NOT NULL then
         if LENGTH(L_stylequery) > 0 then
            L_stylequery := L_stylequery || ' or ';
         end if;
         L_stylequery := L_stylequery || ' (diff_3_aggregate_ind = ''N'' and diff_3 = ''' || I_diff_group_id3 || ''') ';
      end if;

      if I_diff_group_id4 is NOT NULL then
         if LENGTH(L_stylequery) > 0 then
            L_stylequery := L_stylequery || ' or ';
         end if;
         L_stylequery := L_stylequery || ' (diff_4_aggregate_ind = ''N'' and diff_4 = ''' || I_diff_group_id4 || ''') ';
      end if;
   end if;

   if I_all_whs is NULL or I_all_whs = 0 then
      L_all_whs := 'NULL';
   else
      L_all_whs := TO_CHAR(I_all_whs);
   end if;

   if I_wh is NULL then
      L_wh := 'NULL';
   else
      L_wh := TO_CHAR(I_wh);
   end if;

   -- For Warehouse search, there are no GID Profiles
   if (I_all_whs is NULL or I_all_whs = 0) and I_wh is NULL then
      L_mainquery := '
         select NVL(header.id, -1),
                header.gid,
                header.gid_desc,
                profile.gid_profile_id,
                level_info.size_profile_desc,
                level_info.size_profile_level
           from alc_gid_header header
           join alc_gid_profile profile
             on header.id = profile.gid_id
          right join (select inner2.gid_profile_id, (DECODE(inner2.dept, NULL, '''', ''Dept:'' || inner2.dept) ||
                             DECODE(inner2.class, NULL, '''', ''/Class:'' || inner2.class) ||
                             DECODE(inner2.subclass, NULL, '''', ''/Subclass:'' || inner2.subclass) ||
                             DECODE(inner2.style, NULL, '''', ''Style:'' || inner2.style || '' '' )) as size_profile_desc, inner2.size_profile_level as size_profile_level
                        from (select dept,
                                     class,
                                     subclass,
                                     style,
                                     gid_profile_id,
                                     size_profile_level
                                from (select dept,
                                             class,
                                             subclass,
                                             style,
                                             gid_profile_id,
                                             size_profile_level
                                        from alc_size_profile
                                       where EXISTS (select column_value
                                                       from table(cast(ALC_SIZE_PROFILE_SQL.GET_LOCATIONS(' || L_store_grade_group || ',' || L_store_grade || ',' || L_location_list || ',' || L_location_trait || ',' || L_allocation_group || ',' || L_all_stores || ',' || L_single_store || ',' || L_all_whs || ',' || L_wh || ')'
                                                            ||  ' as OBJ_NUMERIC_ID_TABLE)) storelist where alc_size_profile.loc = storelist.column_value)  ';

         if I_dept is NULL and
            I_style is NULL then
               L_mainquery := L_mainquery || ' and ';
               L_mainquery := L_mainquery || ' (';
               L_mainquery := L_mainquery || L_sizegroupquery || ' or ';
               L_mainquery := L_mainquery || ' ( style in (select item from item_master where ' || L_stylequery || '))';
               L_mainquery := L_mainquery || ' )';
         end if;

         if I_dept is NOT NULL and
            I_style is NOT NULL then
               L_mainquery :=  L_mainquery || ' and style = TO_CHAR(''' || I_style || ''')';
         end if;

         if I_dept is NULL and
            I_style is NOT NULL then
               L_mainquery :=  L_mainquery || ' and style = TO_CHAR(''' || I_style || ''')';
         end if;

         if I_dept is NOT NULL and
            I_style is null then
               L_mainquery := L_mainquery || ' and ' || L_merchierquery || ' and ' || L_sizegroupquery;
         end if;

         if I_size_profile_level is NOT NULL then
               L_mainquery := L_mainquery || ' and size_profile_level = ' ||  I_size_profile_level;
         end if;

         L_mainquery := L_mainquery ||') inner1 group by inner1.dept, inner1.class, inner1.subclass, inner1.style, inner1.gid_profile_id, inner1.size_profile_level
                                       ) inner2
                                       ) level_info
                                      on level_info.gid_profile_id = profile.gid_profile_id';

         if I_id is NOT NULL then
            L_mainquery :=  L_mainquery || ' where header.id = ' || I_id;
         end if;

         L_mainquery := L_mainquery || ' order by size_profile_level, size_profile_desc, gid_desc ';

         -- dbms_output.put_line(L_mainquery);
         open C_GID_HEADERS for L_mainquery;

         L_i := 0;
         loop
            fetch C_GID_HEADERS into L_gid_id,
                                     L_gid,
                                     L_gid_desc,
                                     L_gid_profile_id,
                                     L_size_profile_desc,
                                     L_size_profile_level;
            EXIT when C_GID_HEADERS%NOTFOUND;

            L_i := L_i + 1;
            if L_gid_id = -1 then
               L_found_non_gid_data := 1;
            end if;

            if L_i = 1 then
               L_prev_size_profile_desc  := L_size_profile_desc;
               L_prev_gid                := L_gid;
               L_prev_gid_desc           := L_gid_desc;
               L_prev_size_profile_level := L_size_profile_level;

               if L_gid_profile_id is NOT NULL then
                  L_gid_profile_ids_table.EXTEND;
                  L_gid_profile_ids_table(L_gid_profile_ids_table.last) := L_gid_profile_id;
               end if;

               continue;
            end if;

            if L_prev_gid = L_gid and L_prev_size_profile_desc = L_size_profile_desc and L_prev_size_profile_level = L_size_profile_level then
               if L_gid_profile_id is NOT NULL then
                  L_gid_profile_ids_table.EXTEND;
                  L_gid_profile_ids_table(L_gid_profile_ids_table.last) := L_gid_profile_id;
               end if;
            else
                L_gid_profile_headers_table.EXTEND;
                L_gid_profile_headers_table(L_gid_profile_headers_table.last) := OBJ_GID_PROFILE_HEADERS(L_seq_id,
                                                                                                         L_prev_gid,
                                                                                                         L_prev_gid_desc,
                                                                                                         L_gid_profile_ids_table,
                                                                                                         L_prev_size_profile_desc,
                                                                                                         L_prev_size_profile_level);
                L_gid_profile_ids_table := OBJ_NUMERIC_ID_TABLE();
                L_seq_id                := L_seq_id + 1;

                if L_gid_profile_id is NOT NULL then
                  L_gid_profile_ids_table.EXTEND;
                  L_gid_profile_ids_table(L_gid_profile_ids_table.last) := L_gid_profile_id;
                end if;
            end if;

            L_prev_gid                  := L_gid;
            L_prev_gid_desc             := L_gid_desc;
            L_prev_size_profile_desc    := L_size_profile_desc;
            L_prev_size_profile_level   := L_size_profile_level;
         end loop;

         if L_i <> 0 then
            L_gid_profile_headers_table.EXTEND;
            L_gid_profile_headers_table(L_gid_profile_headers_table.last) := OBJ_GID_PROFILE_HEADERS(L_seq_id,
                                                                                                     L_prev_gid,
                                                                                                     L_prev_gid_desc,
                                                                                                     L_gid_profile_ids_table,
                                                                                                     L_prev_size_profile_desc,
                                                                                                     L_prev_size_profile_level);
            L_gid_profile_ids_table := OBJ_NUMERIC_ID_TABLE();
            L_seq_id := L_seq_id + 1;
         end if;
         close C_GID_HEADERS;
   end if; -- End of if I_all_whs is NULL and I_wh is NULL

   if L_found_non_gid_data = 0 and I_id is NULL then
      L_sizeprofiles_rowcount := ALC_SIZE_PROFILE_SQL.GET_ROWCOUNT_FOR_SIZEPROFILES(O_error_message,
                                                                                    I_session_id,
                                                                                    I_result_type,
                                                                                    L_seq_id,
                                                                                    I_size_profile_level,
                                                                                    I_dept,
                                                                                    I_class,
                                                                                    I_subclass,
                                                                                    I_style,
                                                                                    I_store_grade_group,
                                                                                    I_store_grade,
                                                                                    I_location_list,
                                                                                    I_location_trait,
                                                                                    I_allocation_group,
                                                                                    I_all_stores,
                                                                                    I_single_store,
                                                                                    I_all_whs,
                                                                                    I_wh,
                                                                                    I_diff_group_id1,
                                                                                    I_diff_group_id2,
                                                                                    I_diff_group_id3,
                                                                                    I_diff_group_id4);
      if L_sizeprofiles_rowcount <> 0 then
         if I_style is NOT NULL then
            L_size_profile_desc := 'Style:' || I_style;
         else
            if I_dept is NOT NULL then
               L_size_profile_desc := 'Dept:' || I_dept;
               if I_class is NOT NULL then
                  L_size_profile_desc := L_size_profile_desc || '/' || 'Class:' || I_class;
                  if I_subclass is NOT NULL then
                     L_size_profile_desc := L_size_profile_desc || '/' || 'Subclass:' || I_subclass;
                  end if;
               end if;
            end if;
         end if;

         L_gid_profile_headers_table.EXTEND;
         L_gid_profile_ids_table := OBJ_NUMERIC_ID_TABLE();
         L_gid_profile_headers_table(L_gid_profile_headers_table.last) := OBJ_GID_PROFILE_HEADERS(L_seq_id,
                                                                                                  NULL,
                                                                                                  NULL,
                                                                                                  L_gid_profile_ids_table,
                                                                                                  L_size_profile_desc,
                                                                                                  I_size_profile_level);
      end if;
   end if;

   open C_GID_HEADERS for select seq_id,
                                 gid,
                                 gid_desc,
                                 gid_profile_ids,
                                 size_profile_desc,
                                 size_profile_level
                            from table(cast(L_gid_profile_headers_table as OBJ_GID_PROFILE_HEADERS_TABLE));

   loop
      fetch C_GID_HEADERS into L_seq_id,
                               L_gid,
                               L_gid_desc,
                               L_gid_profile_ids,
                               L_size_profile_desc,
                               L_size_profile_level;
      EXIT when C_GID_HEADERS%NOTFOUND;

      L_gid_profile_ids_seq := NULL;
      if (L_gid_profile_ids is NOT NULL and L_gid_profile_ids.COUNT > 0) then
         select alc_session_gidprofileids_seq.NEXTVAL
           into L_gid_profile_ids_seq
           from dual;
      end if;

      insert into alc_session_gid_profile(session_id,
                                          result_type,
                                          seq_id,
                                          gid,
                                          gid_desc,
                                          gid_profile_list_id,
                                          size_profile_desc,
                                          size_profile_level)
                                  values (I_session_id,
                                          I_result_type,
                                          L_seq_id,
                                          L_gid,
                                          L_gid_desc,
                                          L_gid_profile_ids_seq,
                                          L_size_profile_desc,
                                          L_size_profile_level);

      if L_gid_profile_ids_seq is NOT NULL then
         insert into alc_session_gid_profile_list(session_id,
                                                  gid_profile_list_id,
                                                  gid_profile_id)
                                           select I_session_id,
                                                  L_gid_profile_ids_seq,
                                                  column_value
                                             from table(cast(L_gid_profile_ids as OBJ_NUMERIC_ID_TABLE) );
      end if;
   end loop;

   close C_GID_HEADERS;

   LOGGER.LOG_TIME(L_program, L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM || ' from ALC_SIZE_PROFILE_SQL.GET_GID_PROFILES';
      return 0;
END GET_GID_PROFILES;
-----------------------------------------------------------------------------------------------------------------------
-- Name    : GET_SIZE_PROFILE_QUERY
-- Purpose : This function gets the size profile query based on the search criteria provided by the user
--           This query is used to fetch the size profile results.
--           Returns a string which is the dynamically constructed query
-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_SIZE_PROFILE_QUERY(O_error_message         IN OUT VARCHAR2,
                                O_size_profile_query       OUT VARCHAR2,
                                I_gid_profile_list_id   IN     NUMBER,
                                I_size_profile_level    IN     NUMBER,
                                I_dept                  IN     VARCHAR2,
                                I_class                 IN     VARCHAR2,
                                I_subclass              IN     VARCHAR2,
                                I_style                 IN     VARCHAR2,
                                I_store_grade_group     IN     NUMBER,
                                I_store_grade           IN     NUMBER,
                                I_location_list         IN     NUMBER,
                                I_location_trait        IN     NUMBER,
                                I_allocation_group      IN     NUMBER,
                                I_all_stores            IN     NUMBER,
                                I_single_store          IN     NUMBER,
                                I_all_whs               IN     NUMBER,
                                I_wh                    IN     NUMBER,
                                I_diff_group_id1        IN     VARCHAR2,
                                I_diff_group_id2        IN     VARCHAR2,
                                I_diff_group_id3        IN     VARCHAR2,
                                I_diff_group_id4        IN     VARCHAR2)
RETURN NUMBER IS

   L_program                VARCHAR2(61) := 'ALC_SIZE_PROFILE_SQL.GET_SIZE_PROFILE_QUERY';
   L_start_time             TIMESTAMP    := SYSTIMESTAMP;

   L_mainquery              VARCHAR2(32767) := '';
   L_gid_profile_id         NUMBER;
   L_gid_profile_id_query   VARCHAR2(32767) := '';
   L_error_message          VARCHAR2(255)   := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   L_mainquery := 'select OBJ_SIZEPROFILE(NVL(size_profile_id, alc_size_profile_seq.NEXTVAL),
                                          NVL(size_profile_id, -1),
                                          gid_profile_id,
                                          loc,
                                          dept,
                                          class,
                                          subclass,
                                          style,
                                          styledesc,
                                          child_diff1,
                                          diff_group_id1,
                                          child_diff2,
                                          diff_group_id2,
                                          child_diff3,
                                          diff_group_id3,
                                          child_diff4,
                                          diff_group_id4,
                                          display_seq,
                                          qty,
                                          size_profile)
                     from (';

   if I_size_profile_level in (1, 2, 3) then
      L_mainquery := L_mainquery ||'select sp.size_profile_id,
                                           sp.gid_profile_id,
                                           sp.loc,
                                           diffs.dept,
                                           diffs.class,
                                           diffs.subclass,
                                           NULL as style,
                                           NULL as styledesc,
                                           diffs.child_diff1,
                                           diffs.diff_group_id1,
                                           diffs.child_diff2,
                                           diffs.diff_group_id2,
                                           diffs.child_diff3,
                                           diffs.diff_group_id3,
                                           diffs.child_diff4,
                                           diffs.diff_group_id4,
                                           dgd.display_seq,
                                           NVL(sp.qty, 0) as qty,
                                           diffids1.diff_desc || '':'' ||  diffids2.diff_desc || '':'' ||  diffids3.diff_desc || '':'' || diffids4.diff_desc as size_profile
                                      from alc_size_profile sp,
                                           diff_group_detail dgd,
                                           diff_ids diffids1,
                                           diff_ids diffids2,
                                           diff_ids diffids3,
                                           diff_ids diffids4,
                                           (select DISTINCT parent.dept,';

      if I_class is NOT NULL then
         L_mainquery := L_mainquery || ' parent.class as class,';
      else
         L_mainquery := L_mainquery || ' NULL as class,';
      end if;

      if I_subclass is NOT NULL then
         L_mainquery := L_mainquery || ' parent.subclass as subclass,';
      else
         L_mainquery := L_mainquery || ' NULL as subclass,';
      end if;

      L_mainquery    := L_mainquery || 'DECODE (parent.diff_1_aggregate_ind, ''N'', child.diff_1, NULL)  as child_diff1,
                                        DECODE (parent.diff_2_aggregate_ind, ''N'', child.diff_2, NULL)  as child_diff2,
                                        DECODE (parent.diff_3_aggregate_ind, ''N'', child.diff_3, NULL)  as child_diff3,
                                        DECODE (parent.diff_4_aggregate_ind, ''N'', child.diff_4, NULL)  as child_diff4,
                                        DECODE (parent.diff_1_aggregate_ind, ''N'', parent.diff_1, NULL) as diff_group_id1,
                                        DECODE (parent.diff_2_aggregate_ind, ''N'', parent.diff_2, NULL) as diff_group_id2,
                                        DECODE (parent.diff_3_aggregate_ind, ''N'', parent.diff_3, NULL) as diff_group_id3,
                                        DECODE (parent.diff_4_aggregate_ind, ''N'', parent.diff_4, NULL) as diff_group_id4
                                   from item_master child,
                                        (select dept,';
      if I_class is NOT NULL then
         L_mainquery  := L_mainquery || ' class as class,';
      else
         L_mainquery  := L_mainquery || ' NULL as class,';
      end if;
      if I_subclass is NOT NULL then
         L_mainquery  := L_mainquery || ' subclass as subclass,';
      else
         L_mainquery  := L_mainquery || ' NULL as subclass,';
      end if;
      L_mainquery     := L_mainquery || ' diff_1_aggregate_ind,
                                          diff_2_aggregate_ind,
                                          diff_3_aggregate_ind,
                                          diff_4_aggregate_ind,
                                          diff_1,
                                          diff_2,
                                          diff_3,
                                          diff_4,
                                          item
                                     from item_master im
                                    where item_level       = 1
                                      and FN_AGGR_Y_DEPT_RET(item_aggregate_ind, dept) = TO_NUMBER(' || I_dept || ')';
      if I_class is NOT NULL then
         L_mainquery  := L_mainquery || ' and class = ' || I_class;
      end if;
      if I_subclass is NOT NULL then
         L_mainquery     := L_mainquery || ' and subclass = ' || I_subclass;
      end if;

      L_mainquery    := L_mainquery || ') parent
                                 where (   child.item_level = 2  and child.item_parent   = parent.item)
                                        or (child.item_level    = 3  and child.item_grandparent = parent.item)) diffs
                                       where sp.loc(+)                    IN (:store)
                                         and NVL(diffs.child_diff1, ''x'') = NVL(sp.size1(+), ''x'')
                                         and NVL(diffs.child_diff2, ''x'') = NVL(sp.size2(+), ''x'')
                                         and NVL(diffs.child_diff3, ''x'') = NVL(sp.size3(+), ''x'')
                                         and NVL(diffs.child_diff4, ''x'') = NVL(sp.size4(+), ''x'')
                                         and sp.dept(+)                    = TO_CHAR(' || I_dept || ')';
      if I_class is NOT NULL then
         L_mainquery  := L_mainquery || ' and sp.class(+) = TO_CHAR(' || I_class || ')';
      else
         L_mainquery  := L_mainquery || ' and sp.class(+) IS NULL';
      end if;

      if I_subclass is NOT NULL then
         L_mainquery     := L_mainquery || ' and sp.subclass(+) = TO_CHAR(' || I_subclass || ')';
      else
         L_mainquery := L_mainquery || ' and sp.subclass(+) IS NULL';
      end if;

      L_mainquery := L_mainquery ||' and NVL(diffs.diff_group_id1, ''x'') = NVL( ''' || I_diff_group_id1 || ''', ''x'')';
      L_mainquery := L_mainquery ||' and NVL(diffs.diff_group_id2, ''x'') = NVL( ''' || I_diff_group_id2 || ''', ''x'')';
      L_mainquery := L_mainquery ||' and NVL(diffs.diff_group_id3, ''x'') = NVL( ''' || I_diff_group_id3 || ''', ''x'')';
      L_mainquery := L_mainquery ||' and NVL(diffs.diff_group_id4, ''x'') = NVL( ''' || I_diff_group_id4 || ''', ''x'')';
      L_mainquery := L_mainquery ||' and ((    diffs.child_diff1  is NOT NULL
                                           and diffs.child_diff2    is NULL
                                           and diffs.child_diff3    is NULL
                                           and diffs.child_diff4    is NULL
                                           and diffs.diff_group_id1 = dgd.diff_group_id
                                           and diffs.child_diff1    = dgd.diff_id )
                                      or (    diffs.child_diff2     is NOT NULL
                                          and diffs.child_diff3     is NULL
                                          and diffs.child_diff4     is NULL
                                          and diffs.diff_group_id2  = dgd.diff_group_id
                                          and diffs.child_diff2     = dgd.diff_id )
                                      or (    diffs.child_diff3     is NOT NULL
                                          and diffs.child_diff4     is NULL
                                          and diffs.diff_group_id3  = dgd.diff_group_id
                                          and diffs.child_diff3     = dgd.diff_id )
                                      or (    diffs.child_diff4     is NOT NULL
                                          and diffs.diff_group_id4  = dgd.diff_group_id
                                          and diffs.child_diff4     = dgd.diff_id )) ';
      L_mainquery := L_mainquery || ' and diffIds1.diff_id(+) = diffs.child_diff1
                                      and diffIds2.diff_id(+) = diffs.child_diff2
                                      and diffIds3.diff_id(+) = diffs.child_diff3
                                      and diffIds4.diff_id(+) = diffs.child_diff4';
   end if;  -- END OF CONDITION ( SIZE_PROFILE_LEVEL = 1 ) - for Dept/Class/Subclass Finder

   if I_size_profile_level = 5 then
      L_mainquery := L_mainquery ||'select spf.size_profile_id,
                                           spf.gid_profile_id,
                                           spf.loc,
                                           NULL as dept,
                                           NULL as class,
                                           NULL as subclass,
                                           parent.item as style,
                                           parent.item_desc as styleDesc,
                                           child.diff_1 as child_diff1,
                                           NULL as diff_group_id1,
                                           child.diff_2 as child_diff2,
                                           NULL as diff_group_id2,
                                           child.diff_3 as child_diff3,
                                           NULL as diff_group_id3,
                                           child.diff_4 as child_diff4,
                                           NULL as diff_group_id4,
                                           dgd.display_seq,
                                           NVL(spf.qty, 0) as qty,
                                           diffIds1.diff_desc || '':'' ||  diffIds2.diff_desc || '':'' ||  diffIds3.diff_desc || '':'' || diffIds4.diff_desc as size_profile
                                      from item_master parent,
                                           item_master child,
                                           alc_size_profile spf,
                                           diff_group_detail dgd,
                                           diff_ids diffIds1,
                                           diff_ids diffIds2,
                                           diff_ids diffIds3,
                                           diff_ids diffIds4
                                     where NVL(child.diff_1, ''x'') = NVL(spf.size1(+), ''x'')
                                       and NVL(child.diff_2, ''x'') = NVL(spf.size2(+), ''x'')
                                       and NVL(child.diff_3, ''x'') = NVL(spf.size3(+), ''x'')
                                       and NVL(child.diff_4, ''x'') = NVL(spf.size4(+), ''x'')
                                       and DECODE(child.item_level,3,child.item_grandparent,child.item_parent) = spf.style(+)
                                       and spf.loc(+) = :store
                                       and ((    child.item_level  = 2
                                             and child.item_parent = parent.item )
                                              or (    child.item_level       = 3
                                                  and child.item_grandparent = parent.item))
                                       and child.item_level = child.tran_level
                                       and parent.status    = ''A''
                                       and child.status     = ''A''
                                       and parent.item_aggregate_ind = ''Y''
                                       and parent.item_level = 1
                                       and parent.item = ''' || I_style || '''
                                       and
                                          (   (parent.diff_1 is NOT NULL and parent.diff_2 is NULL and parent.diff_3 is NULL and parent.diff_4 is NULL and parent.diff_1_aggregate_ind = ''N'' and parent.diff_1 = dgd.diff_group_id  and child.diff_1 = dgd.diff_id )
                                           or (parent.diff_2 is NOT NULL and parent.diff_3 is NULL and parent.diff_4 is NULL and parent.diff_2 = dgd.diff_group_id  and child.diff_2 = dgd.diff_id )
                                           or (parent.diff_3 is NOT NULL and parent.diff_4 is NULL and parent.diff_3 = dgd.diff_group_id  and child.diff_3 = dgd.diff_id )
                                           or (parent.diff_4 is NOT NULL and parent.diff_4 = dgd.diff_group_id  and child.diff_4 = dgd.diff_id ))
                                       and diffids1.diff_id(+) = child.diff_1
                                       and diffids2.diff_id(+) = child.diff_2
                                       and diffids3.diff_id(+) = child.diff_3
                                       and diffids4.diff_id(+) = child.diff_4';
   end if; -- END OF CONDITION ( SIZE_PROFILE_LEVEL = 2 ) - for StyleFinder

   if I_size_profile_level = 4 then
      L_mainquery := L_mainquery ||'select sp.size_profile_id,
                                           sp.gid_profile_id,
                                           sp.loc,
                                           NULL as dept,
                                           NULL as class,
                                           NULL as subclass,
                                           diffs.item as style,
                                           NULL styledesc,
                                           diffs.child_diff1,
                                           diffs.diff_group_id1,
                                           diffs.child_diff2,
                                           diffs.diff_group_id2,
                                           diffs.child_diff3,
                                           diffs.diff_group_id3,
                                           diffs.child_diff4,
                                           diffs.diff_group_id4,
                                           dgd.display_seq,
                                           NVL(sp.qty, 0) as qty,
                                           diffids1.diff_desc || '':'' || diffids2.diff_desc || '':'' || diffids3.diff_desc || '':'' || diffids4.diff_desc as size_profile
                                      from alc_size_profile sp,
                                           diff_group_detail dgd,
                                           diff_ids diffids1,
                                           diff_ids diffids2,
                                           diff_ids diffids3,
                                           diff_ids diffids4,
                                           (select DISTINCT parent.dept,
                                                            NULL as class,
                                                            NULL as subclass,
                                                            parent.item as item,
                                                            DECODE (parent.diff_1_aggregate_ind, ''N'', child.diff_1, NULL) as child_diff1,
                                                            DECODE (parent.diff_2_aggregate_ind, ''N'', child.diff_2, NULL) as child_diff2,
                                                            DECODE (parent.diff_3_aggregate_ind, ''N'', child.diff_3, NULL) as child_diff3,
                                                            DECODE (parent.diff_4_aggregate_ind, ''N'', child.diff_4, NULL) as child_diff4,
                                                            DECODE (parent.diff_1_aggregate_ind, ''N'', parent.diff_1, NULL) as diff_group_id1,
                                                            DECODE (parent.diff_2_aggregate_ind, ''N'', parent.diff_2, NULL) as diff_group_id2,
                                                            DECODE (parent.diff_3_aggregate_ind, ''N'', parent.diff_3, NULL) as diff_group_id3,
                                                            DECODE (parent.diff_4_aggregate_ind, ''N'', parent.diff_4, NULL) as diff_group_id4
                                              from item_master child,
                                                   (select dept,
                                                           NULL as class,
                                                           NULL as subclass,
                                                           diff_1_aggregate_ind,
                                                           diff_2_aggregate_ind,
                                                           diff_3_aggregate_ind,
                                                           diff_4_aggregate_ind,
                                                           diff_1,
                                                           diff_2,
                                                           diff_3,
                                                           diff_4,
                                                           item
                                                      from item_master im
                                                     where item_level       = 1
                                                       and item_aggregate_ind = ''Y''
                                                       and item = ''' || I_style || ''') parent
                                             where (child.item_level = 2 and child.item_parent = parent.item)
                                                or (child.item_level = 3 and child.item_grandparent = parent.item)) diffs
                                     where sp.loc(+) in (:store)
                                       and NVL(diffs.child_diff1, ''x'') = NVL(sp.size1(+), ''x'')
                                       and NVL(diffs.child_diff2, ''x'') = NVL(sp.size2(+), ''x'')
                                       and NVL(diffs.child_diff3, ''x'') = NVL(sp.size3(+), ''x'')
                                       and NVL(diffs.child_diff4, ''x'') = NVL(sp.size4(+), ''x'')
                                       and (  (diffs.child_diff1 is NOT NULL and diffs.child_diff2 is NULL and diffs.child_diff3 is NULL and diffs.child_diff4 is NULL and diffs.diff_group_id1 = dgd.diff_group_id and diffs.child_diff1 = dgd.diff_id )
                                           or (diffs.child_diff2 is NOT NULL and diffs.child_diff3 is NULL and diffs.child_diff4 is NULL and diffs.diff_group_id2 = dgd.diff_group_id and diffs.child_diff2 = dgd.diff_id )
                                           or (diffs.child_diff3 is NOT NULL and diffs.child_diff4 is NULL and diffs.diff_group_id3 = dgd.diff_group_id and diffs.child_diff3 = dgd.diff_id )
                                           or (diffs.child_diff4 is NOT NULL and diffs.diff_group_id4 = dgd.diff_group_id and diffs.child_diff4 = dgd.diff_id ))
                                       and diffids1.diff_id(+) = diffs.child_diff1
                                       and diffids2.diff_id(+) = diffs.child_diff2
                                       and diffids3.diff_id(+) = diffs.child_diff3
                                       and diffids4.diff_id(+) = diffs.child_diff4
                                       and size_profile_level(+) = 4
                                       and sp.style(+) = ''' || I_style || '''';
   end if; -- END OF CONDITION ( SIZE_PROFILE_LEVEL = 2 ) - for StyleFinder

   if I_gid_profile_list_id is NOT NULL then
      L_mainquery := L_mainquery || ' and gid_profile_id in (select gid_profile_id from alc_session_gid_profile_list where gid_profile_list_id = ' || I_gid_profile_list_id || ')';
   else
      L_mainquery := L_mainquery || ' and gid_profile_id(+) is null';
   end if;

   L_mainquery := L_mainquery || ' order by display_seq, size_profile_id ';
   L_mainquery := L_mainquery || ' ) ';

   O_size_profile_query := L_mainquery;

   LOGGER.LOG_TIME(L_program, L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM || ' from ALC_SIZE_PROFILE_SQL.GET_SIZE_PROFILE_QUERY';
      return 0;
END GET_SIZE_PROFILE_QUERY;
-----------------------------------------------------------------------------------------------------------------------
-- Name    : GET_SIZE_PROFILES
-- Purpose : This function gets the size profiles based on the search criteria provided by the user
--           Returns a cursor for the resultset
-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_SIZE_PROFILES(O_error_message         IN OUT VARCHAR2,
                           I_session_id            IN     VARCHAR2,
                           I_result_type           IN     NUMBER,
                           I_seq_id                IN     NUMBER,
                           I_gid_profile_list_id   IN     NUMBER,
                           I_size_profile_level    IN     NUMBER,
                           I_dept                  IN     VARCHAR2,
                           I_class                 IN     VARCHAR2,
                           I_subclass              IN     VARCHAR2,
                           I_style                 IN     VARCHAR2,
                           I_store_grade_group     IN     NUMBER,
                           I_store_grade           IN     NUMBER,
                           I_location_list         IN     NUMBER,
                           I_location_trait        IN     NUMBER,
                           I_allocation_group      IN     NUMBER,
                           I_all_stores            IN     NUMBER,
                           I_single_store          IN     NUMBER,
                           I_all_whs               IN     NUMBER,
                           I_wh                    IN     NUMBER,
                           I_diff_group_id1        IN     VARCHAR2,
                           I_diff_group_id2        IN     VARCHAR2,
                           I_diff_group_id3        IN     VARCHAR2,
                           I_diff_group_id4        IN     VARCHAR2)
RETURN NUMBER IS

   L_program                   VARCHAR2(61) := 'ALC_SIZE_PROFILE_SQL.GET_SIZE_PROFILES';
   L_start_time                TIMESTAMP    := SYSTIMESTAMP;

   C_SIZE_PROFILES             SYS_REFCURSOR;
   L_mainquery                 VARCHAR2(32767);
   O_size_profile_query        VARCHAR2(32767);
   L_size_profile_table        OBJ_SIZEPROFILE_TABLE := OBJ_SIZEPROFILE_TABLE();
   L_sizeprofile_record        OBJ_SIZEPROFILE;
   L_store                     VARCHAR2(40);
   L_error_message             VARCHAR2(255)  := NULL;
   L_size_profile_qry_result   NUMBER(1);

   O_size_profiles             SYS_REFCURSOR;
   L_size_profile_id           NUMBER(20);
   L_new_size_profile_id       NUMBER(20);
   L_gid_profile_id            NUMBER(15);
   L_dept                      VARCHAR2(40);
   L_class                     VARCHAR2(40);
   L_subclass                  VARCHAR2(40);
   L_style                     VARCHAR2(25);
   L_styleDesc                 VARCHAR2(250);
   L_child_diff1               VARCHAR2(40);
   L_diff_group_id1            VARCHAR2(40);
   L_child_diff2               VARCHAR2(40);
   L_diff_group_id2            VARCHAR2(40);
   L_child_diff3               VARCHAR2(40);
   L_diff_group_id3            VARCHAR2(40);
   L_child_diff4               VARCHAR2(40);
   L_diff_group_id4            VARCHAR2(40);
   L_display_seq               NUMBER(4);
   L_qty                       NUMBER(12,4);
   L_size_profile              VARCHAR2(512);

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   delete
     from alc_session_size_profile_ratio
    where session_id  = I_session_id
      and result_type = I_result_type
      and seq_id      = I_seq_id;

   delete
     from alc_session_size_profile
    where session_id  = I_session_id
      and result_type = I_result_type
      and seq_id      = I_seq_id;

   for v_locs in
      (select value(input) loc
         from table(cast(ALC_SIZE_PROFILE_SQL.GET_LOCATIONS(I_store_grade_group,
                                                            I_store_grade,
                                                            I_location_list,
                                                            I_location_trait,
                                                            I_allocation_group,
                                                            I_all_stores,
                                                            I_single_store,
                                                            I_all_whs,
                                                            I_wh) as OBJ_NUMERIC_ID_TABLE)) input)
   loop
      L_size_profile_qry_result := ALC_SIZE_PROFILE_SQL.GET_SIZE_PROFILE_QUERY(O_error_message,
                                                                               O_size_profile_query,
                                                                               I_gid_profile_list_id,
                                                                               I_size_profile_level,
                                                                               I_dept,
                                                                               I_class,
                                                                               I_subclass,
                                                                               I_style,
                                                                               I_store_grade_group,
                                                                               I_store_grade,
                                                                               I_location_list,
                                                                               I_location_trait,
                                                                               I_allocation_group,
                                                                               I_all_stores,
                                                                               I_single_store,
                                                                               I_all_whs,
                                                                               I_wh,
                                                                               I_diff_group_id1,
                                                                               I_diff_group_id2,
                                                                               I_diff_group_id3,
                                                                               I_diff_group_id4);


      L_store := TO_CHAR(v_locs.loc);

      open C_SIZE_PROFILES for O_size_profile_query using L_store;

      loop
         fetch C_SIZE_PROFILES into L_sizeprofile_record;
         EXIT when C_SIZE_PROFILES%NOTFOUND;

         L_size_profile_table.EXTEND;
         L_size_profile_table(L_size_profile_table.Last) := OBJ_SIZEPROFILE(L_sizeprofile_record.size_profile_id,
                                                                            L_sizeprofile_record.new_size_profile_id,
                                                                            L_sizeprofile_record.gid_profile_id,
                                                                            v_locs.loc,
                                                                            L_sizeprofile_record.dept,
                                                                            L_sizeprofile_record.class,
                                                                            L_sizeprofile_record.subclass,
                                                                            L_sizeprofile_record.style,
                                                                            L_sizeprofile_record.styleDesc,
                                                                            L_sizeprofile_record.child_diff1,
                                                                            L_sizeprofile_record.diff_group_id1,
                                                                            L_sizeprofile_record.child_diff2,
                                                                            L_sizeprofile_record.diff_group_id2,
                                                                            L_sizeprofile_record.child_diff3,
                                                                            L_sizeprofile_record.diff_group_id3,
                                                                            L_sizeprofile_record.child_diff4,
                                                                            L_sizeprofile_record.diff_group_id4,
                                                                            L_sizeprofile_record.display_seq,
                                                                            L_sizeprofile_record.qty,
                                                                            L_sizeprofile_record.size_profile);
      end loop;
      close C_SIZE_PROFILES;
   end loop;

   insert into alc_session_size_profile(session_id,
                                        result_type,
                                        seq_id,
                                        size_profile_id,
                                        new_size_profile_id,
                                        gid_profile_id,
                                        store,
                                        dept,
                                        class,
                                        subclass,
                                        style,
                                        styledesc,
                                        child_diff1,
                                        diff_group_id1,
                                        child_diff2,
                                        diff_group_id2,
                                        child_diff3,
                                        diff_group_id3,
                                        child_diff4,
                                        diff_group_id4,
                                        display_seq,
                                        qty,size_profile)
                                 select I_session_id,
                                        I_result_type,
                                        I_seq_id,
                                        size_profile_id,
                                        new_size_profile_id,
                                        gid_profile_id,
                                        store,
                                        dept,
                                        class,
                                        subclass,
                                        style,
                                        styledesc,
                                        child_diff1,
                                        diff_group_id1,
                                        child_diff2,
                                        diff_group_id2,
                                        child_diff3,
                                        diff_group_id3,
                                        child_diff4,
                                        diff_group_id4,
                                        display_seq,
                                        qty,
                                        size_profile
                                   from table (cast(L_size_profile_table as OBJ_SIZEPROFILE_TABLE));

   LOGGER.LOG_TIME(L_program, L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM || ' from ALC_SIZE_PROFILE_SQL.GET_SIZE_PROFILES';
      return 0;
END GET_SIZE_PROFILES;
-----------------------------------------------------------------------------------------------------------------------
-- Name    : DELETE_SIZE_PROFILES
-- Purpose : This function deletes the size profiles based on the search criteria provided by the user
--           Returns number, which holds 0 or 1 for false and true.
-----------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_SIZE_PROFILES(O_error_message                  IN OUT VARCHAR2,
                              I_session_id                     IN     VARCHAR2,
                              I_seq_id                         IN     NUMBER,
                              I_list_of_gid_profile_list_ids   IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_SIZE_PROFILE_SQL.DELETE_SIZE_PROFILES';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- seq_id is passed for non-gid case
   if I_seq_id is NOT NULL then

      delete
        from alc_size_profile sp
       where exists (select 1
                       from alc_session_size_profile ssp
                      where ssp.session_id     = I_session_id
                        and ssp.seq_id         = I_seq_id
                        and sp.size_profile_id = ssp.size_profile_id);

      delete
        from alc_session_size_profile_ratio
       where session_id = I_session_id
         and seq_id     = I_seq_id;

      delete
        from alc_session_size_profile
       where session_id = I_session_id
         and seq_id     = I_seq_id;

   end if;

   if I_list_of_gid_profile_list_ids is NOT NULL then
      delete
        from alc_size_profile sp
       where gid_profile_id IN (select gid_profile_id
                                  from alc_session_gid_profile_list
                                 where gid_profile_list_id IN (select column_value
                                                                 from table(cast(I_list_of_gid_profile_list_ids as OBJ_NUMERIC_ID_TABLE)))) ;


      delete
        from alc_gid_profile gp
       where gid_profile_id in (select gid_profile_id
                                  from alc_session_gid_profile_list
                                 where gid_profile_list_id IN (select column_value
                                                                 from table(cast(I_list_of_gid_profile_list_ids as OBJ_NUMERIC_ID_TABLE)))) ;

     delete alc_session_size_profile_ratio
      where (session_id, result_type, seq_id) IN (select session_id,
                                                         result_type,
                                                         seq_id
                                                    from alc_session_size_profile
                                                   where gid_profile_id IN (select gid_profile_id
                                                                              from alc_session_gid_profile_list
                                                                             where gid_profile_list_id IN (select column_value
                                                                                                             from table(cast(I_list_of_gid_profile_list_ids as OBJ_NUMERIC_ID_TABLE))))) ;

      delete from alc_session_gid_profile_list
       where gid_profile_list_id IN (select column_value
                                       from table(cast(I_list_of_gid_profile_list_ids as OBJ_NUMERIC_ID_TABLE))) ;

   end if;

   LOGGER.LOG_TIME(L_program, L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM || ' from ALC_SIZE_PROFILE_SQL.DELETE_SIZE_PROFILES';
      return 0;
END DELETE_SIZE_PROFILES;
-----------------------------------------------------------------------------------------------------------------------
-- Name    : GET_ROWCOUNT_FOR_SIZE_PROFILES
-- Purpose : This function returns the row count of size profiles based on the search criteria
--           This is used in the function GET_GID_PROFILES, in a case where the size profiles,
--           should be listed for non-existent rows in the table ALC_SIZE_PROFILE and upon getting a rows
--           for size profiles, need to show as NON GID row in the UI (for new record insertions) - Only for NON GID
--           Returns number, which is the row count of size profiles
-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ROWCOUNT_FOR_SIZEPROFILES(O_error_message        IN OUT VARCHAR2,
                                       I_session_id           IN     VARCHAR2,
                                       I_result_type          IN     NUMBER,
                                       I_seq_id               IN     NUMBER,
                                       I_size_profile_level   IN     NUMBER,
                                       I_dept                 IN     VARCHAR2,
                                       I_class                IN     VARCHAR2,
                                       I_subclass             IN     VARCHAR2,
                                       I_style                IN     VARCHAR2,
                                       I_store_grade_group    IN     NUMBER,
                                       I_store_grade          IN     NUMBER,
                                       I_location_list        IN     NUMBER,
                                       I_location_trait       IN     NUMBER,
                                       I_allocation_group     IN     NUMBER,
                                       I_all_stores           IN     NUMBER,
                                       I_single_store         IN     NUMBER,
                                       I_all_whs              IN     NUMBER,
                                       I_wh                   IN     NUMBER,
                                       I_diff_group_id1       IN     VARCHAR2,
                                       I_diff_group_id2       IN     VARCHAR2,
                                       I_diff_group_id3       IN     VARCHAR2,
                                       I_diff_group_id4       IN     VARCHAR2)
RETURN NUMBER IS

   L_program                VARCHAR2(61) := 'ALC_SIZE_PROFILE_SQL.GET_ROWCOUNT_FOR_SIZEPROFILES';
   L_start_time             TIMESTAMP    := SYSTIMESTAMP;

   L_size_profile_level     NUMBER(1);
   L_size_profiles_result   NUMBER(1);
   L_rowcount               NUMBER(6);

BEGIN

   L_rowcount             := 0;
   L_size_profile_level   := 1;

   if I_style is NOT NULL then
      L_size_profile_level := 2;
   end if;

   L_size_profiles_result := ALC_SIZE_PROFILE_SQL.GET_SIZE_PROFILES(O_error_message,
                                                                    I_session_id,
                                                                    I_result_type,
                                                                    I_seq_id,
                                                                    NULL,
                                                                    I_size_profile_level,
                                                                    I_dept,
                                                                    I_class,
                                                                    I_subclass,
                                                                    I_style,
                                                                    I_store_grade_group,
                                                                    I_store_grade,
                                                                    I_location_list,
                                                                    I_location_trait,
                                                                    I_allocation_group,
                                                                    I_all_stores,
                                                                    I_single_store,
                                                                    I_all_whs,
                                                                    I_wh,
                                                                    I_diff_group_id1,
                                                                    I_diff_group_id2,
                                                                    I_diff_group_id3,
                                                                    I_diff_group_id4);

   select count(1)
     into L_rowcount
     from alc_session_size_profile
    where session_id  = I_session_id
      and result_type = I_result_type
      and seq_id      = I_seq_id;

   return L_rowcount;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM || ' from ALC_SIZE_PROFILE_SQL.GET_ROWCOUNT_FOR_SIZEPROFILES';
      return NULL;
END GET_ROWCOUNT_FOR_SIZEPROFILES;
-----------------------------------------------------------------------------------------------------------------------
FUNCTION COPY_ENTIRE_PARENT(O_error_message        IN OUT VARCHAR2,
                            I_session_id           IN   VARCHAR2,
                            I_to_seq_id            IN   NUMBER,
                            I_from_seq_id          IN   NUMBER,
                            I_multiple_stores      IN   NUMBER)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_SIZE_PROFILE_SQL.COPY_ENTIRE_PARENT';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

   L_from_style   VARCHAR2(40);
   L_to_style     VARCHAR2(40);

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   select style
     into L_from_style
     from alc_session_size_profile
    where session_id  = I_session_id
      and result_type = 2
      and seq_id      = I_from_seq_id
      and rownum      = 1;

   select style
     into L_to_style
     from alc_session_size_profile
    where session_id  = I_session_id
      and result_type = 1
      and seq_id      = I_to_seq_id
      and rownum      = 1;

   if I_multiple_stores = 0 then
      merge into alc_session_size_profile dest
      using alc_session_size_profile src
      on (    dest.session_id = src.session_id
          and dest.session_id = I_session_id
          and NVL(src.child_diff1, 0) = NVL(dest.child_diff1, 0)
          and NVL(src.child_diff2, 0) = NVL(dest.child_diff2, 0)
          and NVL(src.child_diff3, 0) = NVL(dest.child_diff3, 0)
          and NVL(src.child_diff4, 0) = NVL(dest.child_diff4, 0)
          and src.result_type         = 2
          and src.style               = L_from_style
          and dest.style              = L_to_style
          and dest.result_type        = 1
          and src.seq_id              = I_from_seq_id
          and dest.seq_id             = I_to_seq_id)
      when MATCHED then
         update
            set dest.qty = src.qty;
   else
      merge into alc_session_size_profile dest
      using alc_session_size_profile src
      on (    dest.session_id = src.session_id
          and dest.session_id = I_session_id
          and NVL(src.child_diff1, 0) = NVL(dest.child_diff1, 0)
          and NVL(src.child_diff2, 0) = NVL(dest.child_diff2, 0)
          and NVL(src.child_diff3, 0) = NVL(dest.child_diff3, 0)
          and NVL(src.child_diff4, 0) = NVL(dest.child_diff4, 0)
          and src.result_type         = 2
          and src.style               = L_from_style
          and dest.style              = L_to_style
          and dest.result_type        = 1
          and dest.store              = src.store
          and src.seq_id              = I_from_seq_id
          and dest.seq_id             = I_to_seq_id)
      when MATCHED then
         update
            set dest.qty = src.qty;
   end if;

   LOGGER.LOG_TIME(L_program, L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM || ' from ALC_SIZE_PROFILE_SQL.COPY_STYLE';
      return 0;
END COPY_ENTIRE_PARENT;
-----------------------------------------------------------------------------------------------------------------------
FUNCTION COPY_SINGLE_DIFF(O_error_message          IN OUT VARCHAR2,
                          I_session_id             IN     VARCHAR2,
                          I_to_seq_id              IN     NUMBER,
                          I_from_seq_id            IN     NUMBER,
                          I_from_aggregated_diff   IN     VARCHAR2,
                          I_to_aggregated_diff            OBJ_VARCHAR_ID_TABLE,
                          I_multiple_stores        IN     NUMBER)
RETURN NUMBER IS

   L_program                VARCHAR2(61) := 'ALC_SIZE_PROFILE_SQL.COPY_SINGLE_DIFF';
   L_start_time             TIMESTAMP    := SYSTIMESTAMP;

   L_from_style             VARCHAR2(40);
   L_to_style               VARCHAR2(40);
   L_diff_1_aggregate_ind   VARCHAR2(1);
   L_diff_2_aggregate_ind   VARCHAR2(1);
   L_diff_3_aggregate_ind   VARCHAR2(1);
   L_diff_4_aggregate_ind   VARCHAR2(1);
   L_diff_1                 VARCHAR2(10);
   L_diff_2                 VARCHAR2(10);
   L_diff_3                 VARCHAR2(10);
   L_diff_4                 VARCHAR2(10);
   L_agg_query              VARCHAR2(8192);
   L_execute_query          VARCHAR2(8192);
   L_agg_query1             VARCHAR2(8192);

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   select style
     into L_to_style
     from alc_session_size_profile
    where session_id  = I_session_id
      and result_type = 1
      and seq_id      = I_to_seq_id
      and rownum      = 1;

   select style
     into L_from_style
     from alc_session_size_profile
    where session_id  = I_session_id
      and result_type = 2
      and seq_id      = I_from_seq_id
      and rownum      = 1;

   select diff_1_aggregate_ind,
          diff_2_aggregate_ind,
          diff_3_aggregate_ind,
          diff_4_aggregate_ind,
          diff_1,
          diff_2,
          diff_3,
          diff_4
     into L_diff_1_aggregate_ind,
          L_diff_2_aggregate_ind,
          L_diff_3_aggregate_ind,
          L_diff_4_aggregate_ind,
          L_diff_1,
          L_diff_2,
          L_diff_3,
          L_diff_4
     from item_master
    where item_aggregate_ind = 'Y'
      and item               = L_to_style;

   L_agg_query := '';

   if L_diff_1_aggregate_ind = 'Y' then
       L_agg_query := 'src.child_diff1';
   end if;

   if L_diff_2_aggregate_ind = 'Y' then
       if LENGTH(L_agg_query) > 0 then
          L_agg_query := L_agg_query || ' || '':'' || ' || 'src.child_diff2';
       else
          L_agg_query := 'src.child_diff2';
       end if;
   end if;

   if L_diff_3_aggregate_ind = 'Y' then
       if LENGTH(L_agg_query) > 0 then
          L_agg_query := L_agg_query || ' || '':'' || ' || 'src.child_diff3';
       else
          L_agg_query := 'src.child_diff3';
       end if;
   end if;

   if L_diff_4_aggregate_ind = 'Y' then
       if LENGTH(L_agg_query) > 0 then
          L_agg_query := L_agg_query || ' || '':'' || ' || 'src.child_diff4';
       else
          L_agg_query := 'src.child_diff4';
       end if;
   end if;

   L_agg_query1  := '';
   if L_diff_1_aggregate_ind = 'N' then
        L_agg_query1 := ' NVL(src.child_diff1, 0) = NVL(dest.child_diff1, 0) ';
   end if;

   if L_diff_2_aggregate_ind = 'N' then
      if LENGTH(L_agg_query1) > 0 then
        L_agg_query1 := L_agg_query1 || ' and NVL(src.child_diff2, 0) = NVL(dest.child_diff2, 0) ';
      else
        L_agg_query1 := ' NVL(src.child_diff2, 0) = NVL(dest.child_diff2, 0) ';
      end if;
   end if;

   if L_diff_3_aggregate_ind = 'N' then
      if LENGTH(L_agg_query1) > 0 then
        L_agg_query1 := L_agg_query1 || ' and NVL(src.child_diff3, 0) = NVL(dest.child_diff3, 0) ';
      else
        L_agg_query1 := ' NVL(src.child_diff3, 0) = NVL(dest.child_diff3, 0) ';
      end if;
   end if;

   if L_diff_4_aggregate_ind = 'N' then
      if LENGTH(L_agg_query1) > 0 then
        L_agg_query1 := L_agg_query1 || ' and NVL(src.child_diff4, 0) = NVL(dest.child_diff4, 0) ';
      else
        L_agg_query1 := ' NVL(src.child_diff4, 0) = NVL(dest.child_diff4, 0) ';
      end if;
   end if;

   L_execute_query :=
      'merge into alc_session_size_profile dest
       using alc_session_size_profile src
       on (    src.session_id  = dest.session_id
           and dest.session_id = :session_id
           and ' ||   L_agg_query1 ||
           ' and src.result_type = 2 and src.style = :from_style and dest.style = :to_style and dest.result_type = 1 ';

   if I_multiple_stores = 1 then
        L_execute_query := L_execute_query || ' and dest.store = src.store ';
   end if;

   L_execute_query := L_execute_query ||
                      ' and ' || L_agg_query || ' =  :fromdiff'  || ' and ' || replace(L_agg_query, 'src', 'dest')  || ' =  :todiff'  ||
                      ' and src.seq_id = :fromSeqId and dest.seq_id = :toSeqId)
                      when MATCHED then
                         update
                            set dest.qty = src.qty';

   for i in I_to_aggregated_diff.FIRST .. I_to_aggregated_diff.LAST
   loop
      EXECUTE IMMEDIATE L_execute_query using I_session_id,
                                              L_from_style,
                                              L_to_style,
                                              I_from_aggregated_diff,
                                              I_to_aggregated_diff(i),
                                              I_from_seq_id, I_to_seq_id;
   end loop;

   LOGGER.LOG_TIME(L_program, L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM || ' from ALC_SIZE_PROFILE_SQL.COPY_STYLE';
      return 0;
END COPY_SINGLE_DIFF;
-----------------------------------------------------------------------------------------------------------------------
FUNCTION FLUSH_SESSION_TABLES(O_error_message   IN OUT VARCHAR2,
                              I_session_id      IN     VARCHAR2)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_SIZE_PROFILE_SQL.FLUSH_SESSION_TABLES';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   delete
     from alc_session_gid_profile_list
    where gid_profile_list_id IN (select gid_profile_list_id
                                    from alc_session_gid_profile
                                   where session_id = I_session_id);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_GID_PROFILE_LIST - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_session_size_profile_ratio
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_SIZE_PROFILE_RATIO - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_session_size_profile
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_SIZE_PROFILE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_session_gid_profile
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_GID_PROFILE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program, L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM || ' from ALC_SIZE_PROFILE_SQL.FLUSH_SESSION_TABLES';
      return 0;
END;
-----------------------------------------------------------------------------------------------------------------------
END ALC_SIZE_PROFILE_SQL;
/