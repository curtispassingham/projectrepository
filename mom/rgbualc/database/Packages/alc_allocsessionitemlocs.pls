CREATE OR REPLACE PACKAGE ALC_ALLOC_SESSION_ITEM_LOC_SQL IS
-------------------------------------------------------------------------------
FUNCTION ADD_ITEM_LOC(O_error_message          IN OUT VARCHAR2,
                      I_alloc_id               IN     NUMBER,
                      I_facet_session_id       IN     VARCHAR2,
                      I_work_header_id         IN     NUMBER,
                      I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                      I_alc_location_attr      IN     ALC_LOCATION_ATTR_TBL,
                      I_alloc_type             IN     VARCHAR2 DEFAULT NULL,
                      I_what_if_ind            IN     NUMBER DEFAULT 0,
                      I_nfrc_supp_chain_ind    IN     NUMBER DEFAULT 0,
                      I_size_profile           IN     ALC_RULE.SIZE_PROFILE%TYPE DEFAULT NULL,
                      I_size_profile_type      IN     ALC_RULE.SIZE_PROFILE_TYPE%TYPE DEFAULT NULL,
                      I_pack_threshold         IN     ALC_RULE.PACK_THRESHOLD%TYPE DEFAULT NULL,
                      I_rule_mode              IN     ALC_RULE.RULE_MODE%TYPE DEFAULT NULL,
                      I_spwhavailpct_ind       IN     ALC_RULE.SIZE_PROFILE_WH_AVAIL_PCT_IND%TYPE DEFAULT NULL,
                      I_spdflthier_ind         IN     ALC_RULE.SIZE_PROFILE_DEFAULT_HIER_IND%TYPE DEFAULT NULL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION SAVE_ALC_ITEM_LOC(O_error_message      IN OUT VARCHAR2,
                           I_alloc_id           IN     NUMBER,
                           I_facet_session_id   IN     VARCHAR2,
                           I_alloc_type         IN     VARCHAR2,
                           I_flush_ind          IN     NUMBER)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION LOAD_ALC_ITEM_LOC(O_error_message      IN OUT VARCHAR2,
                           I_alloc_id           IN     NUMBER,
                           I_facet_session_id   IN     VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION SPREAD_ALL_LOC(O_error_message         IN OUT VARCHAR2,
                        I_spread_method         IN     NUMBER,
                        I_facet_session_id      IN     VARCHAR2,
                        I_alc_spread_attr_tbl   IN     ALC_SPREAD_ATTR_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION SPREAD_SINGLE_LOC(O_error_message             IN OUT VARCHAR2,
                           I_facet_session_id          IN     VARCHAR2,
                           I_alc_spread_loc_attr_tbl   IN     ALC_SPREAD_LOC_ATTR_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_AVAIL_QTY(O_error_message          IN OUT VARCHAR2,
                                 O_valid_ind_tbl          IN OUT ALC_VALID_IND_TBL,
                                 I_rule_need_type         IN     NUMBER,
                                 I_facet_session_id       IN     VARCHAR2,
                                 I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_ALLOCATED_QTY(O_error_message      IN OUT VARCHAR2,
                                     O_failure_key           OUT VARCHAR2,
                                     I_freeze_ind         IN     VARCHAR2,
                                     I_facet_session_id   IN     VARCHAR2,
                                     I_alc_spread_attr    IN     ALC_SPREAD_ATTR_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION UPDATE_FREEZE_IND(O_error_message          IN OUT VARCHAR2,
                           I_facet_session_id       IN     VARCHAR2,
                           I_freeze_ind             IN     VARCHAR2,
                           I_item_source_dest_tbl   IN     ALC_ITEM_SOURCE_DEST_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION RESET_FILTERED_IND(O_error_message        IN OUT VARCHAR2,
                            I_facet_session_id     IN     VARCHAR2,
                            I_filtered_ind_value   IN     VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION UPDATE_FILTERED_IND(O_error_message             IN OUT VARCHAR2,
                             I_facet_session_id          IN     VARCHAR2,
                             I_alc_spread_loc_attr_tbl   IN     ALC_SPREAD_LOC_ATTR_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------
-- Name:    UPDATE_SESSION_RELEASE_DATE
-- Purpose: This function will update the release date from the
--          ALC_SESSION_ITEM_LOC table
-------------------------------------------------------------------------------
FUNCTION UPDATE_SESSION_RELEASE_DATE(O_error_message          IN OUT VARCHAR2,
                                     I_facet_session_id       IN     VARCHAR2,
                                     I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------
--alter session set plsql_ccflags = 'UTPLSQL:TRUE'
-- compile private functions as public for unit testing
$if $$UTPLSQL=TRUE $then
-------------------------------------------------------------------------------
FUNCTION POPULATE_SESSION_ITEM_LOC(O_error_message          IN OUT VARCHAR2,
                                   I_alloc_id               IN     NUMBER,
                                   I_facet_session_id       IN     VARCHAR2,
                                   I_work_header_id         IN     NUMBER,
                                   I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                                   I_alc_location_attr      IN     ALC_LOCATION_ATTR_TBL,
                                   I_alloc_type             IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION POP_SESSION_ITEM_LOC_NOT_FPG(O_error_message          IN OUT VARCHAR2,
                                      I_alloc_id               IN     NUMBER,
                                      I_facet_session_id       IN     VARCHAR2,
                                      I_work_header_id         IN     NUMBER,
                                      I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                                      I_alc_location_attr      IN     ALC_LOCATION_ATTR_TBL,
                                      I_alloc_type             IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION POP_SESSION_ITEM_LOC_FPG(O_error_message          IN OUT VARCHAR2,
                                  I_alloc_id               IN     NUMBER,
                                  I_facet_session_id       IN     VARCHAR2,
                                  I_work_header_id         IN     NUMBER,
                                  I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                                  I_alc_location_attr      IN     ALC_LOCATION_ATTR_TBL,
                                  I_alloc_type             IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION POPULATE_SESSION_QTY_LIMITS(O_error_message      IN OUT VARCHAR2,
                                     I_alloc_id           IN     NUMBER,
                                     I_facet_session_id   IN     VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION SET_PACK_QTY(O_error_message      IN OUT VARCHAR2,
                      I_facet_session_id   IN     VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION POP_EXCLUSIONS(O_error_message          IN OUT VARCHAR2,
                        I_facet_session_id       IN     VARCHAR2,
                        I_work_header_id         IN     NUMBER,
                        I_alloc_type             IN     ALC_ALLOC.TYPE%TYPE,
                        I_what_if_ind            IN     NUMBER DEFAULT 0,
                        I_nfrc_supp_chain_ind    IN     NUMBER DEFAULT 0,
                        I_size_profile           IN     ALC_RULE.SIZE_PROFILE%TYPE DEFAULT NULL,
                        I_size_profile_type      IN     ALC_RULE.SIZE_PROFILE_TYPE%TYPE DEFAULT NULL,
                        I_pack_threshold         IN     ALC_RULE.PACK_THRESHOLD%TYPE DEFAULT NULL,
                        I_rule_mode              IN     ALC_RULE.RULE_MODE%TYPE DEFAULT NULL,
                        I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                        I_spwhavailpct_ind       IN     ALC_RULE.SIZE_PROFILE_WH_AVAIL_PCT_IND%TYPE DEFAULT NULL,
                        I_spdflthier_ind         IN     ALC_RULE.SIZE_PROFILE_DEFAULT_HIER_IND%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION FIND_SIZE_PROFILE(O_error_message      IN OUT VARCHAR2,
                           I_facet_session_id   IN     VARCHAR2,
                           I_alloc_type         IN     ALC_ALLOC.TYPE%TYPE,
                           I_size_profile       IN     ALC_RULE.SIZE_PROFILE%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
$end
-------------------------------------------------------------------------------
END ALC_ALLOC_SESSION_ITEM_LOC_SQL;
/

