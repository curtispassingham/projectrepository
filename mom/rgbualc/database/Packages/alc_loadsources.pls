CREATE OR REPLACE PACKAGE ALC_LOAD_ITEM_SOURCE AS
--------------------------------------------------------------------
FUNCTION LOAD(O_error_message IN OUT VARCHAR2,
              I_alloc_ids     IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION PURGE_LOAD_TABLES(O_error_message IN OUT VARCHAR2,
                           I_alloc_ids     IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------
END ALC_LOAD_ITEM_SOURCE;
/
