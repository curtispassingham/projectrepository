CREATE OR REPLACE PACKAGE ALC_CONSTANTS_SQL AS

   -----------------------------------------------------------------------------------------------------------------------------
   -- This package specification holds static constant values that the ALC PL/SQL code can utilize.
   -----------------------------------------------------------------------------------------------------------------------------

   SUCCESS                               CONSTANT NUMBER       := 1; -- Successful run
   FAILURE                               CONSTANT NUMBER       := 0; -- Unclassified (fatal) Error

   -----------------------------------------------------------------------------------------------------------------------------
   -- ALC_HUB_FACET_CFG_SQL.VALIDATE_FPG_ITEMS
   -----------------------------------------------------------------------------------------------------------------------------
   MSG_FPG_VALIDATION_PASS            CONSTANT VARCHAR2(55) := 'MSG_FPG_VALIDATION_PASS';
   ERRMSG_FPG_STYLE_FAIL              CONSTANT VARCHAR2(55) := 'ERRMSG_FPG_STYLE_FAIL';
   ERRMSG_FPG_MULTI_SOURCE_FAIL       CONSTANT VARCHAR2(55) := 'ERRMSG_FPG_MULTI_SOURCE_FAIL';
   ERRMSG_MIN_QL_AVAILABLE_QTY        CONSTANT VARCHAR2(55) := 'ERRMSG_MIN_QL_AVAILABLE_QTY';
   ERRMSG_MAX_QL_AVAILABLE_QTY        CONSTANT VARCHAR2(55) := 'ERRMSG_MAX_QL_AVAILABLE_QTY';
   ERRMSG_ALLOC_QTY_GT_RST_QTY        CONSTANT VARCHAR2(55) := 'ERRMSG_ALLOC_QTY_GT_RST_QTY';
   ERRMSG_SINGLE_WH_MULTI_SRC         CONSTANT VARCHAR2(55) := 'ERRMSG_SINGLE_WH_MULTI_SRC';
   ERRMSG_DEFAULT_MSG                 CONSTANT VARCHAR2(55) := 'ERRMSG_DEFAULT_MSG';

   ERRMSG_ONE_ITEM_CRITERIA_REQ       CONSTANT VARCHAR2(55) := 'ERRMSG_ONE_ITEM_CRITERIA_REQ';
   ERRMSG_SNAPSHOT_HIER               CONSTANT VARCHAR2(55) := 'ERRMSG_SNAPSHOT_HIER';

   ERRMSG_FPG_SINGLEPARENT_VALID      CONSTANT VARCHAR2(55) := 'ERRMSG_FPG_SINGLEPARENT_VALID';
   ERRMSG_FPG_STAPLE_VALID            CONSTANT VARCHAR2(55) := 'ERRMSG_FPG_STAPLE_VALID';

   ERRMSG_ASN_DISTRIB                 CONSTANT VARCHAR2(55) := 'ERRMSG_ASN_DISTRIB';
   ERRMSG_NO_WKST_ITEM                CONSTANT VARCHAR2(55) := 'ERRMSG_NO_WKST_ITEM';

   ERRMSG_CREDIT_CHECK                CONSTANT VARCHAR2(55) := 'ERRMSG_CREDIT_CHECK';
   ERRMSG_QUICK_CREATE_NOT_ITEM       CONSTANT VARCHAR2(55) := 'ERRMSG_QUICK_CREATE_NOT_ITEM';
   ERRMSG_QUICK_CREATE_STYLE          CONSTANT VARCHAR2(55) := 'ERRMSG_QUICK_CREATE_STYLE';
   ERRMSG_QUICK_CREATE_INV_TXN        CONSTANT VARCHAR2(55) := 'ERRMSG_QUICK_CREATE_INV_TXN';
   ERRMSG_QUICK_CREATE_FOUND_NDA      CONSTANT VARCHAR2(55) := 'ERRMSG_QUICK_CREATE_FOUND_NDA';   --(NDA NO DATA AVALILABLE)

   -----------------------------------------------------------------------------------------------------------------------------
   -- ALC_ALLOC_SESSION_ITEM_LOC_SQL.ADD_ITEM_LOC
   -----------------------------------------------------------------------------------------------------------------------------
   NUM_SOURCE_TYPE_PO                 CONSTANT NUMBER(1)    := 1;
   NUM_SOURCE_TYPE_ASN                CONSTANT NUMBER(1)    := 2;
   NUM_SOURCE_TYPE_OH                 CONSTANT NUMBER(1)    := 3;
   NUM_SOURCE_TYPE_WHATIF             CONSTANT NUMBER(1)    := 4;
   NUM_SOURCE_TYPE_BOL                CONSTANT NUMBER(1)    := 5;
   NUM_SOURCE_TYPE_TSF                CONSTANT NUMBER(1)    := 6;
   NUM_SOURCE_TYPE_ALLOC              CONSTANT NUMBER(1)    := 7;
   --
   CHAR_SOURCE_TYPE_PO                CONSTANT VARCHAR2(10) := 'PO';
   CHAR_SOURCE_TYPE_ASN               CONSTANT VARCHAR2(10) := 'ASN';
   CHAR_SOURCE_TYPE_OH                CONSTANT VARCHAR2(10) := 'OH';
   CHAR_SOURCE_TYPE_WHAT_IF           CONSTANT VARCHAR2(10) := 'WHIF';
   CHAR_SOURCE_TYPE_BOL               CONSTANT VARCHAR2(10) := 'BOL';
   CHAR_SOURCE_TYPE_TSF               CONSTANT VARCHAR2(10) := 'TSF';
   CHAR_SOURCE_TYPE_ALLOC             CONSTANT VARCHAR2(10) := 'ALLOC';

   -----------------------------------------------------------------------------------------------------------------------------
   --Allocation Types
   -----------------------------------------------------------------------------------------------------------------------------
   FASHION_ALLOCATION                 CONSTANT VARCHAR2(10) := 'FA';
   FASHION_PACK_ALLOCATION            CONSTANT VARCHAR2(25) := 'FPA';
   STAPLE_ALLOCATION                  CONSTANT VARCHAR2(25) := 'SA';
   FASHION_PACK_GROUP_ALLOCATION      CONSTANT VARCHAR2(25) := 'FPG';

   -----------------------------------------------------------------------------------------------------------------------------
   --Rule Levels
   -----------------------------------------------------------------------------------------------------------------------------
   RULE_LEVEL_DEPT                    CONSTANT NUMBER(2) := 1;
   RULE_LEVEL_CLASS                   CONSTANT NUMBER(2) := 2;
   RULE_LEVEL_SUBCLASS                CONSTANT NUMBER(2) := 3;
   RULE_LEVEL_STYLE                   CONSTANT NUMBER(2) := 4; --style-diff
   RULE_LEVEL_SKU                     CONSTANT NUMBER(2) := 5;
   RULE_LEVEL_STYLELEVEL              CONSTANT NUMBER(2) := 6; --style
   --
   USER_RULE_LEVEL_UDA                CONSTANT NUMBER(2) := 8;
   USER_RULE_LEVEL_UDA_VALUE          CONSTANT NUMBER(2) := 9;
   USER_RULE_LEVEL_ITEMLIST           CONSTANT NUMBER(2) := 10;

   -----------------------------------------------------------------------------------------------------------------------------
   --Rule Types
   -----------------------------------------------------------------------------------------------------------------------------
   RULE_TYPE_HISTORY                  CONSTANT NUMBER(2) := 1;
   RULE_TYPE_FORECAST                 CONSTANT NUMBER(2) := 2;
   RULE_TYPE_PLAN                     CONSTANT NUMBER(2) := 3;
   RULE_TYPE_HISTORY_AND_PLAN         CONSTANT NUMBER(2) := 4;
   RULE_TYPE_PLAN_REPROJECT           CONSTANT NUMBER(2) := 5;
   RULE_TYPE_CORPORATE_RULES          CONSTANT NUMBER(2) := 6;
   RULE_TYPE_MANUAL                   CONSTANT NUMBER(2) := 7;
   RULE_TYPE_RECIEPT_AND_PLAN         CONSTANT NUMBER(2) := 8;
   EXCLUDE_CROSS_LEGAL_ENTITIES       CONSTANT NUMBER(5) := 9;

   -----------------------------------------------------------------------------------------------------------------------------
   --Exclusion Reason Codes
   -----------------------------------------------------------------------------------------------------------------------------
   EXCLUDE_MLD_DELETION               CONSTANT NUMBER(5) := 1;
   EXCLUDE_ITEM_LOC_STATUS            CONSTANT NUMBER(5) := 2;
   EXCLUDE_ALLOCATION_SPLIT           CONSTANT NUMBER(5) := 3;
   EXCLUDE_MLD_PATH                   CONSTANT NUMBER(5) := 4;
   EXCLUDE_STOP_SHIP                  CONSTANT NUMBER(5) := 5;
   EXCLUDE_STORE_CLOSED               CONSTANT NUMBER(5) := 6;
   EXCLUDE_SIZE_PROFILE               CONSTANT NUMBER(5) := 7;
   EXCLUDE_WH_CYCLE                   CONSTANT NUMBER(5) := 8;
   EXCLUDE_SUPPLY_CHAIN_PRIORITY      CONSTANT NUMBER(5) := 9;

   -----------------------------------------------------------------------------------------------------------------------------
   --Source Item Types
   -----------------------------------------------------------------------------------------------------------------------------
   STYLE                              CONSTANT VARCHAR2(10) := 'STYLE';
   FASHIONITEM                        CONSTANT VARCHAR2(10) := 'FA';
   --
   STAPLEITEM                         CONSTANT VARCHAR2(10) := 'ST';
   FASHIONSKU                         CONSTANT VARCHAR2(10) := 'FASHIONSKU';
   PACKCOMP                           CONSTANT VARCHAR2(10) := 'PACKCOMP';
   --
   NONSELLFASHIONSIMPLEPACK           CONSTANT VARCHAR2(10) := 'NSFSP';
   NONSELLSTAPLESIMPLEPACK            CONSTANT VARCHAR2(10) := 'NSSSP';
   NONSELLSTAPLECOMPLEXPACK           CONSTANT VARCHAR2(10) := 'NSSCP';
   NONSELLFASHIONMULTICOLORPACK       CONSTANT VARCHAR2(10) := 'NSFMCP';
   NONSELLFASHIONSINGLECOLORPACK      CONSTANT VARCHAR2(10) := 'NSFSCP';
   SELLABLEPACK                       CONSTANT VARCHAR2(10) := 'SELLPACK';

   -----------------------------------------------------------------------------------------------------------------------------
   -- Worksheet Types
   -----------------------------------------------------------------------------------------------------------------------------
   WORKSHEET_TYPE_WORK                CONSTANT VARCHAR2(2) := 'WK';  --Standard Worksheet
   WORKSHEET_TYPE_ALLOC               CONSTANT VARCHAR2(2) := 'WD';  --Tied to Allocation

   -----------------------------------------------------------------------------------------------------------------------------
   -- Allocation Status
   -----------------------------------------------------------------------------------------------------------------------------
   STATUS_WORKSHEET                   CONSTANT VARCHAR2(2) := '0';
   STATUS_SUBMITTED                   CONSTANT VARCHAR2(2) := '1';
   STATUS_APPROVED                    CONSTANT VARCHAR2(2) := '2';
   STATUS_PROCESSED                   CONSTANT VARCHAR2(2) := '3';
   STATUS_CLOSED                      CONSTANT VARCHAR2(2) := '4';
   STATUS_CANCELLED                   CONSTANT VARCHAR2(2) := '5';
   STATUS_RESERVED                    CONSTANT VARCHAR2(2) := '6';
   STATUS_DELETED                     CONSTANT VARCHAR2(2) := '7';
   STATUS_APPROVED_IN_PROCESS         CONSTANT VARCHAR2(2) := '8';
   STATUS_RESERVED_IN_PROCESS         CONSTANT VARCHAR2(2) := '9';
   STATUS_PO_CREATED                  CONSTANT VARCHAR2(2) := '10';
   STATUS_SCHEDULED                   CONSTANT VARCHAR2(2) := '11';

   -----------------------------------------------------------------------------------------------------------------------------
   -- Filter Types
   -----------------------------------------------------------------------------------------------------------------------------
   FILTER_TYPE_RESULT                 CONSTANT VARCHAR2(2) := 'R';
   FILTER_TYPE_QTY_LIMITS             CONSTANT VARCHAR2(2) := 'Q';

   -----------------------------------------------------------------------------------------------------------------------------
   -- Size Profile Levels
   -----------------------------------------------------------------------------------------------------------------------------
   SIZE_PROFILE_LEVEL_DEPARTMENT      CONSTANT NUMBER(2) := 1;
   SIZE_PROFILE_LEVEL_CLASS           CONSTANT NUMBER(2) := 2;
   SIZE_PROFILE_LEVEL_SUBCLASS        CONSTANT NUMBER(2) := 3;
   SIZE_PROFILE_LEVEL_STYLE           CONSTANT NUMBER(2) := 4;
   SIZE_PROFILE_LEVEL_STYLE_COLOR     CONSTANT NUMBER(2) := 5;

   -----------------------------------------------------------------------------------------------------------------------------
   -- Rule Modes
   -----------------------------------------------------------------------------------------------------------------------------
   RULE_MODE_SIMPLE                   CONSTANT VARCHAR2(2) := '1';
   RULE_MODE_CASCADE                  CONSTANT VARCHAR2(2) := '2';
   RULE_MODE_PACK_DISTRIBUTION        CONSTANT VARCHAR2(2) := '3';
   RULE_MODE_SPREAD_DEMAND            CONSTANT VARCHAR2(2) := '4';

   -----------------------------------------------------------------------------------------------------------------------------
   -- Spread Method
   -----------------------------------------------------------------------------------------------------------------------------
   SPREAD_METHOD_PROPORTIONAL         CONSTANT NUMBER(2)   := 1;
   SPREAD_METHOD_EVENLY               CONSTANT NUMBER(2)   := 2;
   SPREAD_METHOD_PER_STORE            CONSTANT NUMBER(2)   := 3;

   -----------------------------------------------------------------------------------------------------------------------------
   -- Intercompany Transfer Basis
   -----------------------------------------------------------------------------------------------------------------------------
   BASED_ON_TRANSFER                  CONSTANT VARCHAR2(1) := 'T';
   BASED_ON_SET_OF_BOOKS              CONSTANT VARCHAR2(1) := 'B';
   
   -----------------------------------------------------------------------------------------------------------------------------
   -- Flexible Supply Chain Rule Priority
   -----------------------------------------------------------------------------------------------------------------------------
   PRIORITY_ITEM_LOC                  CONSTANT NUMBER(2)   := 1;
   PRIORITY_DEFAULT_WH                CONSTANT NUMBER(2)   := 2;
   --
   PRIORITY_SOSC_NOTPRTCT_PRIMARY     CONSTANT NUMBER(2)   := 3;   -- Same Org, Same Channel, Not Protected and the Primary Virtual WH
   PRIORITY_SOSC_NOTPRTCT_NOTPRI      CONSTANT NUMBER(2)   := 4;   -- Same Org, Same Channel, Not Protected and Not the Primary Virtual WH
   PRIORITY_SOSC_PROTECT_PRIMARY      CONSTANT NUMBER(2)   := 5;   -- Same Org, Same Channel, Protected and Primary VWH
   PRIORITY_SOSC_PROTECT_NOTPRI       CONSTANT NUMBER(2)   := 6;   -- Same Org, Same Channel, Protected and Not the Primary VWH
   --
   PRIORITY_SODC_NOTPRTCT_PRIMARY     CONSTANT NUMBER(2)   := 7;   -- Same Org, Different Channel, Not Protected and the Primary Virtual WH
   PRIORITY_SODC_NOTPRTCT_NOTPRI      CONSTANT NUMBER(2)   := 8;   -- Same Org, Different Channel, Not Protected and Not the Primary Virtual WH
   PRIORITY_SODC_PROTECT_PRIMARY      CONSTANT NUMBER(2)   := 9;   -- Same Org, Different Channel, Protected and Primary VWH
   PRIORITY_SODC_PROTECT_NOTPRI       CONSTANT NUMBER(2)   := 10;  -- Same Org, Different Channel, Protected and Not the Primary VWH
   --
   PRIORITY_DOSC_NOTPRTCT_PRIMARY     CONSTANT NUMBER(2)   := 11;  -- Different Org, Same Channel, Not Protected and the Primary Virtual WH
   PRIORITY_DOSC_NOTPRTCT_NOTPRI      CONSTANT NUMBER(2)   := 12;  -- Different Org, Same Channel, Not Protected and Not the Primary Virtual WH
   PRIORITY_DOSC_PROTECT_PRIMARY      CONSTANT NUMBER(2)   := 13;  -- Different Org, Same Channel, Protected and Primary VWH
   PRIORITY_DOSC_PROTECT_NOTPRI       CONSTANT NUMBER(2)   := 14;  -- Different Org, Same Channel, Protected and Not the Primary VWH
   --
   PRIORITY_DODC_NOTPRTCT_PRIMARY     CONSTANT NUMBER(2)   := 15;  -- Different Org, Different Channel, Not Protected and the Primary Virtual WH
   PRIORITY_DODC_NOTPRTCT_NOTPRI      CONSTANT NUMBER(2)   := 16;  -- Different Org, Different Channel, Not Protected and Not the Primary Virtual WH
   PRIORITY_DODC_PROTECT_PRIMARY      CONSTANT NUMBER(2)   := 17;  -- Different Org, Different Channel, Protected and Primary VWH
   PRIORITY_DODC_PROTECT_NOTPRI       CONSTANT NUMBER(2)   := 18;  -- Different Org, Different Channel, Protected and Not the Primary VWH

   ALLOC_APP_CODE                       CONSTANT VARCHAR2(10) := 'ALC';

END ALC_CONSTANTS_SQL;
/
