CREATE OR REPLACE 
PACKAGE ALC_REP_UTIL_SQL 
AS 

FUNCTION        ALC_REP_GET_CURRENT_AVAIL(
      I_item      IN item_loc.item%TYPE
    )
    RETURN item_loc_soh.stock_on_hand%TYPE;
    
FUNCTION        ALC_REP_GET_CURRENT_AVAIL(
      I_item      IN item_loc.item%TYPE,
      I_loc       IN item_loc.loc%TYPE
    )
    RETURN item_loc_soh.stock_on_hand%TYPE;  
    
FUNCTION GET_LOC_CURRENT_AVAIL
        (O_error_message  IN OUT VARCHAR2,
         O_available      IN OUT item_loc_soh.stock_on_hand%TYPE,
         I_item           IN     item_loc.item%TYPE,
         I_loc            IN     item_loc.loc%TYPE,
         I_loc_type       IN     item_loc.loc_type%TYPE)
RETURN BOOLEAN;

END ALC_REP_UTIL_SQL;
/