CREATE OR REPLACE PACKAGE BODY ALC_LOAD_ITEM_SOURCE AS
-------------------------------------------------------------------------------------------------------------
TYPE ASN_DIST_REC_TYPE IS RECORD
(
   alloc_id           NUMBER,
   item               VARCHAR2(60),
   wh                 NUMBER,
   source_type        NUMBER,
   doc_no             VARCHAR2(40),
   avail              NUMBER,
   po_not_after_date  DATE
);
TYPE ASN_DIST_TABLE_TYPE IS TABLE OF ASN_DIST_REC_TYPE INDEX BY BINARY_INTEGER;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_FA(O_error_message   IN OUT VARCHAR2,
                I_alloc_ids       IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_NOT_FA(O_error_message   IN OUT VARCHAR2,
                    I_alloc_ids       IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_INVENTORY(O_error_message   IN OUT VARCHAR2,
                       I_alloc_ids       IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_ASN_INV(O_error_message   IN OUT VARCHAR2,
                     I_alloc_ids       IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_IMAGE(O_error_message   IN OUT VARCHAR2,
                   I_alloc_ids       IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION LOAD(O_error_message   IN OUT VARCHAR2,
              I_alloc_ids       IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_LOAD_ITEM_SOURCE.LOAD';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if PURGE_LOAD_TABLES(O_error_message,
                        I_alloc_ids) = 0 then
      return 0;
   end if;

   if GET_FA(O_error_message,
             I_alloc_ids) = FALSE then
      return 0;
   end if;
   --
   if GET_NOT_FA(O_error_message,
                 I_alloc_ids) = FALSE then
      return 0;
   end if;
   --
   if POP_INVENTORY(O_error_message,
                    I_alloc_ids) = FALSE then
      return 0;
   end if;
   --
   if POP_IMAGE(O_error_message,
                I_alloc_ids) = FALSE then
      return 0;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END LOAD;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_FA(O_error_message   IN OUT VARCHAR2,
                I_alloc_ids       IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(61)    := 'ALC_LOAD_ITEM_SOURCE.GET_FA';
   L_pack_insert   VARCHAR2(25000) := NULL;

BEGIN

   --fashionsku under the style color
   --FASHIONSKU
   insert into alc_load_temp (alloc_id,
                              item_source_id,
                              item,
                              item_type,
                              item_desc,
                              wh,
                              source_type,
                              doc_no,
                              dept,
                              class,
                              subclass,
                              dept_name,
                              class_name,
                              subclass_name,
                              item_aggregate_ind,
                              diff_1_aggregate_ind,
                              diff_2_aggregate_ind,
                              diff_3_aggregate_ind,
                              diff_4_aggregate_ind,
                              diff_1,
                              diff_2,
                              diff_3,
                              diff_4,
                              diff_1_desc,
                              diff_2_desc,
                              diff_3_desc,
                              diff_4_desc,
                              item_parent,
                              item_parent_desc,
                              item_grandparent,
                              pack_ind,
                              sellable_ind,
                              tran_level,
                              item_level,
                              inner_pack_size,
                              supp_pack_size,
                              ti,
                              hi,
                              avail_qty,
                              backorder_qty,
                              break_pack_ind,
                              default_level,
                              release_date,
                              hold_back_pct_flag,
                              hold_back_value,
                              min_avail_qty,
                              threshold_percent,
                              calc_multiple,
                              on_hand_qty,
                              future_on_hand_qty,
                              tsr_ind,
                              pack_round,
                              po_not_after_date,
                              qty_ordered,
                              proportion)
                       select DISTINCT
                              inner.alloc_id,
                              inner.item_source_id,
                              im.item,
                              ALC_CONSTANTS_SQL.FASHIONSKU,
                              im.item_desc,
                              inner.wh_id,
                              inner.source_type,
                              inner.order_no,
                              im.dept,
                              im.class,
                              im.subclass,
                              mh.dept_name,
                              mh.class_name,
                              mh.subclass_name,
                              'Y' item_aggregate_ind,
                              DECODE(NVL(inner.source_diff1_id,'-1'),
                                     '-1', 'N',
                                     'Y') diff_1_aggregate_ind,
                              DECODE(NVL(inner.source_diff2_id,'-1'),
                                     '-1', 'N',
                                     'Y') diff_2_aggregate_ind,
                              DECODE(NVL(inner.source_diff3_id,'-1'),
                                     '-1', 'N',
                                     'Y') diff_3_aggregate_ind,
                              DECODE(NVL(inner.source_diff4_id,'-1'),
                                     '-1', 'N',
                                     'Y') diff_4_aggregate_ind,
                              im.diff_1,
                              im.diff_2,
                              im.diff_3,
                              im.diff_4,
                              d1.diff_desc,
                              d2.diff_desc,
                              d3.diff_desc,
                              d4.diff_desc,
                              im.item_parent,
                              imp.item_desc item_parent_desc,
                              im.item_grandparent,
                              im.pack_ind,
                              im.sellable_ind,
                              im.tran_level,
                              im.item_level,
                              isc.inner_pack_size,
                              isc.supp_pack_size,
                              isc.ti,
                              isc.hi,
                              inner.avail_qty,
                              NULL backorder_qty,
                              w.break_pack_ind,
                              inner.default_level,
                              inner.release_date,
                              inner.hold_back_pct_flag,
                              inner.hold_back_value,
                              inner.min_avail_qty,
                              inner.threshold_percent,
                              inner.calc_multiple,
                              inner.on_hand_qty,
                              inner.future_on_hand_qty,
                              inner.tsr_ind,
                              inner.pack_round,
                              NULL po_not_after_date,
                              NULL qty_ordered,
                              NULL proportion
                         from item_master im,
                              item_master imp,
                              wh w,
                              diff_ids d1,
                              diff_ids d2,
                              diff_ids d3,
                              diff_ids d4,
                              item_supp_country isc,
                              (select d.dept,
                                      d.dept_name,
                                      c.class,
                                      c.class_name,
                                      s.subclass,
                                      s.sub_name subclass_name
                                 from deps d,
                                      class c,
                                      subclass s
                                where d.dept  = c.dept
                                  and c.dept  = s.dept
                                  and c.class = s.class) mh,
                              (select i2.alloc_id,
                                      i2.release_date,
                                      i2.default_level,
                                      i2.hold_back_pct_flag,
                                      i2.hold_back_value,
                                      i2.min_avail_qty,
                                      i2.threshold_percent,
                                      i2.calc_multiple,
                                      i2.tsr_ind,
                                      i2.on_hand_qty,
                                      i2.future_on_hand_qty,
                                      i2.avail_qty,
                                      i2.pack_round,
                                      i2.item_source_id,
                                      i2.wh_id,
                                      i2.source_type,
                                      i2.order_no,
                                      i2.item_type,
                                      i2.item_id source_item_id,
                                      --
                                      case
                                         when i2.one_pos = 1 then
                                            i2.one_value
                                         when i2.two_pos = 1 then
                                            i2.two_value
                                         when i2.three_pos = 1 then
                                            i2.three_value
                                         else
                                            NULL
                                      end source_diff1_id,
                                      --
                                      case
                                         when i2.one_pos = 2 then
                                            i2.one_value
                                         when i2.two_pos = 2 then
                                            i2.two_value
                                         when i2.three_pos = 2 then
                                            i2.three_value
                                         else
                                            NULL
                                      end source_diff2_id,
                                      --
                                      case
                                         when i2.one_pos = 3 then
                                            i2.one_value
                                         when i2.two_pos = 3 then
                                            i2.two_value
                                         when i2.three_pos = 3 then
                                            i2.three_value
                                         else
                                            NULL
                                      end source_diff3_id,
                                      --
                                      case
                                         when i2.one_pos = 4 then
                                            i2.one_value
                                         when i2.two_pos = 4 then
                                            i2.two_value
                                         when i2.three_pos = 4 then
                                            i2.three_value
                                         else
                                            NULL
                                      end source_diff4_id
                                 from (select i1.alloc_id,
                                              i1.item_source_id,
                                              i1.wh_id,
                                              i1.source_type,
                                              i1.order_no,
                                              i1.release_date,
                                              i1.default_level,
                                              i1.hold_back_pct_flag,
                                              i1.hold_back_value,
                                              i1.min_avail_qty,
                                              i1.threshold_percent,
                                              i1.calc_multiple,
                                              i1.tsr_ind,
                                              i1.on_hand_qty,
                                              i1.future_on_hand_qty,
                                              i1.avail_qty,
                                              i1.pack_round,
                                              i1.item_type,
                                              i1.item_id,
                                              i1.input,
                                              SUBSTR(i1.one,1,1) one_pos,
                                              SUBSTR(i1.one,3) one_value,
                                              SUBSTR(i1.two,1,1) two_pos,
                                              SUBSTR(i1.two,3) two_value,
                                              SUBSTR(i1.three,1,1) three_pos,
                                              SUBSTR(i1.three,3) three_value
                                         from (select ais.alloc_id,
                                                      ais.item_source_id,
                                                      ais.wh_id,
                                                      ais.source_type,
                                                      ais.order_no,
                                                      ais.release_date,
                                                      ais.default_level,
                                                      ais.hold_back_pct_flag,
                                                      ais.hold_back_value,
                                                      ais.min_avail_qty,
                                                      ais.threshold_percent,
                                                      ais.calc_multiple,
                                                      ais.tsr_ind,
                                                      ais.on_hand_qty,
                                                      ais.future_on_hand_qty,
                                                      ais.avail_qty,
                                                      ais.pack_round,
                                                      ais.item_type,
                                                      ais.item_id,
                                                      ais.diff1_id||':' input,
                                                      SUBSTR(ais.diff1_id||':', 1, INSTR(ais.diff1_id||':',':')-1) one,
                                                      SUBSTR(ais.diff1_id||'::', INSTR(ais.diff1_id||'::', ':') +1,
                                                              INSTR(ais.diff1_id||'::', ':', 1, 2 )-INSTR(ais.diff1_id||'::',':')-1) two,
                                                      RTRIM(SUBSTR(ais.diff1_id||'::', INSTR(ais.diff1_id||'::',':',1,2)+1),':') three
                                                 from alc_item_source ais,
                                                      table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input
                                                where alloc_id      = value(input)
                                                  and ais.item_type = ALC_CONSTANTS_SQL.FASHIONITEM) i1) i2
                              ) inner
                        where inner.source_item_id                                  = im.item_parent(+)
                          and NVL(inner.source_diff1_id, NVL(im.diff_1(+),'-999'))  = NVL(im.diff_1(+),'-999')
                          and NVL(inner.source_diff2_id, NVL(im.diff_2(+),'-999'))  = NVL(im.diff_2(+),'-999')
                          and NVL(inner.source_diff3_id, NVL(im.diff_3(+),'-999'))  = NVL(im.diff_3(+),'-999')
                          and NVL(inner.source_diff4_id, NVL(im.diff_4(+),'-999'))  = NVL(im.diff_4(+),'-999')
                          --
                          and inner.wh_id                                           = w.wh(+)
                          --
                          and im.item                                               = isc.item(+)
                          and isc.primary_supp_ind(+)                               = 'Y'
                          and isc.primary_country_ind(+)                            = 'Y'
                          --
                          and im.dept                                               = mh.dept
                          and im.class                                              = mh.class
                          and im.subclass                                           = mh.subclass
                          and im.diff_1                                             = d1.diff_id(+)
                          and im.diff_2                                             = d2.diff_id(+)
                          and im.diff_3                                             = d3.diff_id(+)
                          and im.diff_4                                             = d4.diff_id(+)
                          and im.item_parent                                        = imp.item;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_LOAD_TEMP - FASHIONSKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --non-sellable packs under the style/color
   --NSFSP, NSFMCP, NSFSCP
   --have to use dynamic sql due to database bug with stragg
   L_pack_insert := q'[
                       insert into alc_load_temp (alloc_id,
                                                  item_source_id,
                                                  item,
                                                  item_type,
                                                  item_desc,
                                                  wh,
                                                  source_type,
                                                  doc_no,
                                                  dept,
                                                  class,
                                                  subclass,
                                                  dept_name,
                                                  class_name,
                                                  subclass_name,
                                                  item_aggregate_ind,
                                                  diff_1_aggregate_ind,
                                                  diff_2_aggregate_ind,
                                                  diff_3_aggregate_ind,
                                                  diff_4_aggregate_ind,
                                                  diff_1,
                                                  diff_2,
                                                  diff_3,
                                                  diff_4,
                                                  diff_1_desc,
                                                  diff_2_desc,
                                                  diff_3_desc,
                                                  diff_4_desc,
                                                  item_parent,
                                                  item_parent_desc,
                                                  item_grandparent,
                                                  pack_ind,
                                                  sellable_ind,
                                                  tran_level,
                                                  item_level,
                                                  inner_pack_size,
                                                  supp_pack_size,
                                                  ti,
                                                  hi,
                                                  avail_qty,
                                                  backorder_qty,
                                                  break_pack_ind,
                                                  default_level,
                                                  release_date,
                                                  hold_back_pct_flag,
                                                  hold_back_value,
                                                  min_avail_qty,
                                                  threshold_percent,
                                                  calc_multiple,
                                                  on_hand_qty,
                                                  future_on_hand_qty,
                                                  tsr_ind,
                                                  pack_round,
                                                  po_not_after_date,
                                                  qty_ordered,
                                                  proportion)
                                           select DISTINCT
                                                  i.alloc_id,
                                                  i.item_source_id,
                                                  i.pack_no,
                                                  impack.alc_item_type,
                                                  impack.item_desc,
                                                  i.wh,
                                                  i.source_type,
                                                  i.doc_no,
                                                  impack.dept,
                                                  impack.class,
                                                  impack.subclass,
                                                  mh.dept_name,
                                                  mh.class_name,
                                                  mh.subclass_name,
                                                  i.agg_ind item_aggregate_ind,
                                                  i.agg_1 diff_1_aggregate_ind,
                                                  i.agg_2 diff_2_aggregate_ind,
                                                  i.agg_3 diff_3_aggregate_ind,
                                                  i.agg_4 diff_4_aggregate_ind,
                                                  STRAGG(DISTINCT imsku.diff_1) diff_1,
                                                  STRAGG(DISTINCT imsku.diff_2) diff_2,
                                                  STRAGG(DISTINCT imsku.diff_3) diff_3,
                                                  STRAGG(DISTINCT imsku.diff_4) diff_4,
                                                  STRAGG(DISTINCT d1.diff_desc) diff_1,
                                                  STRAGG(DISTINCT d2.diff_desc) diff_2,
                                                  STRAGG(DISTINCT d3.diff_desc) diff_3,
                                                  STRAGG(DISTINCT d4.diff_desc) diff_4,
                                                  i.item_parent,
                                                  imparent.item_desc item_parent_desc,
                                                  i.item_grandparent,
                                                  impack.pack_ind,
                                                  impack.sellable_ind,
                                                  impack.item_level,
                                                  impack.tran_level,
                                                  isc.inner_pack_size,
                                                  isc.supp_pack_size,
                                                  isc.ti,
                                                  isc.hi,
                                                  i.avail_qty,
                                                  NULL backorder_qty,
                                                  w.break_pack_ind,
                                                  --
                                                  i.default_level,
                                                  i.release_date,
                                                  i.hold_back_pct_flag,
                                                  i.hold_back_value,
                                                  i.min_avail_qty,
                                                  i.threshold_percent,
                                                  i.calc_multiple,
                                                  i.on_hand_qty,
                                                  i.future_on_hand_qty,
                                                  i.tsr_ind,
                                                  i.pack_round,
                                                  i.po_not_after_date,
                                                  i.qty_ordered,
                                                  i.proportion
                                             from item_master impack,
                                                  packitem_breakout pb,
                                                  item_master imsku,
                                                  diff_ids d1,
                                                  diff_ids d2,
                                                  diff_ids d3,
                                                  diff_ids d4,
                                                  item_master imparent,
                                                  wh w,
                                                  item_supp_country isc,
                                                  (select d.dept,
                                                          d.dept_name,
                                                          c.class,
                                                          c.class_name,
                                                          s.subclass,
                                                          s.sub_name subclass_name
                                                     from deps d,
                                                          class c,
                                                          subclass s
                                                    where d.dept  = c.dept
                                                      and c.dept  = s.dept
                                                      and c.class = s.class) mh,
                                                  (select DISTINCT
                                                          lt.alloc_id,
                                                          lt.item_source_id,
                                                          pb2.pack_no,
                                                          lt.wh,
                                                          lt.source_type,
                                                          lt.doc_no,
                                                          --
                                                          lt.release_date,
                                                          lt.default_level,
                                                          lt.hold_back_pct_flag,
                                                          lt.hold_back_value,
                                                          lt.min_avail_qty,
                                                          lt.threshold_percent,
                                                          lt.calc_multiple,
                                                          lt.tsr_ind,
                                                          lt.pack_round,
                                                          lt.on_hand_qty,
                                                          lt.future_on_hand_qty,
                                                          NULL avail_qty,
                                                          lt.po_not_after_date,
                                                          lt.qty_ordered,
                                                          lt.proportion,
                                                          --
                                                          MIN(im.item_parent) OVER (PARTITION BY pb2.pack_no,
                                                                                                 lt.wh,
                                                                                                 lt.source_type,
                                                                                                 lt.doc_no) item_parent,
                                                          MIN(im.item_grandparent) OVER (PARTITION BY pb2.pack_no,
                                                                                                      lt.wh,
                                                                                                      lt.source_type,
                                                                                                      lt.doc_no) item_grandparent,
                                                          MIN(imp.item_aggregate_ind) OVER (PARTITION BY pb2.pack_no,
                                                                                                         lt.wh,
                                                                                                         lt.source_type,
                                                                                                         lt.doc_no) agg_ind,
                                                          COUNT(DISTINCT im.item_parent) OVER (PARTITION BY pb2.pack_no,
                                                                                                            lt.wh,
                                                                                                            lt.source_type,
                                                                                                            lt.doc_no) parent_cnt,
                                                          --need to check for single color packs
                                                          MIN(imp.diff_1_aggregate_ind) OVER (PARTITION BY pb2.pack_no,
                                                                                                           lt.wh,
                                                                                                           lt.source_type,
                                                                                                           lt.doc_no) agg_1,
                                                          MIN(imp.diff_2_aggregate_ind) OVER (PARTITION BY pb2.pack_no,
                                                                                                           lt.wh,
                                                                                                           lt.source_type,
                                                                                                           lt.doc_no) agg_2,
                                                          MIN(imp.diff_3_aggregate_ind) OVER (PARTITION BY pb2.pack_no,
                                                                                                           lt.wh,
                                                                                                           lt.source_type,
                                                                                                           lt.doc_no) agg_3,
                                                          MIN(imp.diff_4_aggregate_ind) OVER (PARTITION BY pb2.pack_no,
                                                                                                           lt.wh,
                                                                                                           lt.source_type,
                                                                                                           lt.doc_no) agg_4,
                                                          --diff counts
                                                          COUNT(DISTINCT im.diff_1) OVER (PARTITION BY pb2.pack_no,
                                                                                                       lt.wh,
                                                                                                       lt.source_type,
                                                                                                       lt.doc_no) diff_1_cnt,
                                                          COUNT(DISTINCT im.diff_2) OVER (PARTITION BY pb2.pack_no,
                                                                                                       lt.wh,
                                                                                                       lt.source_type,
                                                                                                       lt.doc_no) diff_2_cnt,
                                                          COUNT(DISTINCT im.diff_3) OVER (PARTITION BY pb2.pack_no,
                                                                                                       lt.wh,
                                                                                                       lt.source_type,
                                                                                                       lt.doc_no) diff_3_cnt,
                                                          COUNT(DISTINCT im.diff_4) OVER (PARTITION BY pb2.pack_no,
                                                                                                       lt.wh,
                                                                                                       lt.source_type,
                                                                                                       lt.doc_no) diff_4_cnt
                                                     from table(cast(:1 as OBJ_NUMERIC_ID_TABLE)) input,
                                                          alc_load_temp lt,
                                                          packitem_breakout pb,
                                                          packitem_breakout pb2,
                                                          item_master im,
                                                          item_master imp
                                                    where lt.alloc_id     = value(input)
                                                      and lt.source_type != 4 --ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF
                                                      and pb.item         = lt.item
                                                      and pb.pack_no      = pb2.pack_no
                                                      and pb2.item        = im.item
                                                      and im.item_parent  = imp.item(+)) i
                                            where i.parent_cnt        = 1
                                              and i.agg_ind           = 'Y'
                                              --
                                              and i.pack_no           = impack.item
                                              and impack.status       = 'A'
                                              and impack.sellable_ind = 'N'
                                              and ((i.agg_1 = 'Y' and i.diff_1_cnt = 1) or i.agg_1 = 'N')
                                              and ((i.agg_2 = 'Y' and i.diff_2_cnt = 1) or i.agg_2 = 'N')
                                              and ((i.agg_3 = 'Y' and i.diff_3_cnt = 1) or i.agg_3 = 'N')
                                              and ((i.agg_4 = 'Y' and i.diff_4_cnt = 1) or i.agg_4 = 'N')
                                              --
                                              and impack.dept         = mh.dept
                                              and impack.class        = mh.class
                                              and impack.subclass     = mh.subclass
                                              --
                                              and impack.item                = isc.item(+)
                                              and isc.primary_supp_ind(+)    = 'Y'
                                              and isc.primary_country_ind(+) = 'Y'
                                              --
                                              and i.wh                       = w.wh(+)
                                              --
                                              and impack.item                = pb.pack_no
                                              and pb.item                    = imsku.item
                                              and imsku.diff_1               = d1.diff_id(+)
                                              and imsku.diff_2               = d2.diff_id(+)
                                              and imsku.diff_3               = d3.diff_id(+)
                                              and imsku.diff_4               = d4.diff_id(+)
                                              and imparent.item(+)           = imsku.item_parent
                                            group by i.alloc_id,
                                                     i.item_source_id,
                                                     i.pack_no,
                                                     impack.alc_item_type,
                                                     impack.item_desc,
                                                     i.wh,
                                                     i.source_type,
                                                     i.doc_no,
                                                     i.release_date,
                                                     i.default_level,
                                                     i.hold_back_pct_flag,
                                                     i.hold_back_value,
                                                     i.min_avail_qty,
                                                     i.threshold_percent,
                                                     i.calc_multiple,
                                                     i.tsr_ind,
                                                     i.pack_round,
                                                     i.on_hand_qty,
                                                     i.future_on_hand_qty,
                                                     i.avail_qty,
                                                     i.po_not_after_date,
                                                     i.qty_ordered,
                                                     i.proportion,
                                                     impack.dept,
                                                     impack.class,
                                                     impack.subclass,
                                                     mh.dept_name,
                                                     mh.class_name,
                                                     mh.subclass_name,
                                                     i.agg_ind,
                                                     i.agg_1,
                                                     i.agg_2,
                                                     i.agg_3,
                                                     i.agg_4,
                                                     i.item_parent,
                                                     imparent.item_desc,
                                                     i.item_grandparent,
                                                     impack.pack_ind,
                                                     impack.sellable_ind,
                                                     impack.item_level,
                                                     impack.tran_level,
                                                     isc.inner_pack_size,
                                                     isc.supp_pack_size,
                                                     isc.ti,
                                                     isc.hi,
                                                     w.break_pack_ind
   ]';

   EXECUTE IMMEDIATE L_pack_insert
      using I_alloc_ids;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_FA;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_NOT_FA(O_error_message   IN OUT VARCHAR2,
                    I_alloc_ids       IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(61)    := 'ALC_LOAD_ITEM_SOURCE.GET_NOT_FA';
   L_pack_insert   VARCHAR2(25000) := NULL;

BEGIN

   --NSFSP, NSFMCP, NSFSCP
   --have to use dynamic sql due to database bug with stragg
   L_pack_insert := q'[
                       insert into alc_load_temp (alloc_id,
                                                  item_source_id,
                                                  item,
                                                  item_type,
                                                  item_desc,
                                                  wh,
                                                  source_type,
                                                  doc_no,
                                                  dept,
                                                  class,
                                                  subclass,
                                                  dept_name,
                                                  class_name,
                                                  subclass_name,
                                                  item_aggregate_ind,
                                                  diff_1_aggregate_ind,
                                                  diff_2_aggregate_ind,
                                                  diff_3_aggregate_ind,
                                                  diff_4_aggregate_ind,
                                                  diff_1,
                                                  diff_2,
                                                  diff_3,
                                                  diff_4,
                                                  diff_1_desc,
                                                  diff_2_desc,
                                                  diff_3_desc,
                                                  diff_4_desc,
                                                  item_parent,
                                                  item_parent_desc,
                                                  item_grandparent,
                                                  pack_ind,
                                                  sellable_ind,
                                                  tran_level,
                                                  item_level,
                                                  inner_pack_size,
                                                  supp_pack_size,
                                                  ti,
                                                  hi,
                                                  avail_qty,
                                                  backorder_qty,
                                                  break_pack_ind,
                                                  default_level,
                                                  release_date,
                                                  hold_back_pct_flag,
                                                  hold_back_value,
                                                  min_avail_qty,
                                                  threshold_percent,
                                                  calc_multiple,
                                                  on_hand_qty,
                                                  future_on_hand_qty,
                                                  tsr_ind,
                                                  pack_round,
                                                  po_not_after_date,
                                                  qty_ordered,
                                                  proportion)
                                           select ws.alloc_id,
                                                  ws.item_source_id,
                                                  ws.item_id,
                                                  ws.item_type,
                                                  imp.item_desc,
                                                  ws.wh_id,
                                                  ws.source_type,
                                                  ws.order_no,
                                                  imp.dept,
                                                  imp.class,
                                                  imp.subclass,
                                                  mh.dept_name,
                                                  mh.class_name,
                                                  mh.subclass_name,
                                                  imparent.item_aggregate_ind,
                                                  imparent.diff_1_aggregate_ind,
                                                  imparent.diff_2_aggregate_ind,
                                                  imparent.diff_3_aggregate_ind,
                                                  imparent.diff_4_aggregate_ind,
                                                  STRAGG(DISTINCT imc.diff_1) diff_1,
                                                  STRAGG(DISTINCT imc.diff_2) diff_2,
                                                  STRAGG(DISTINCT imc.diff_3) diff_3,
                                                  STRAGG(DISTINCT imc.diff_4) diff_4,
                                                  STRAGG(DISTINCT d1.diff_desc) diff_1_desc,
                                                  STRAGG(DISTINCT d2.diff_desc) diff_2_desc,
                                                  STRAGG(DISTINCT d3.diff_desc) diff_3_desc,
                                                  STRAGG(DISTINCT d4.diff_desc) diff_4_desc,
                                                  MIN(imc.item_parent) item_parent,
                                                  imparent.item_desc item_parent_desc,
                                                  MIN(imc.item_grandparent) item_grandparent,
                                                  imp.pack_ind,
                                                  imp.sellable_ind,
                                                  imp.tran_level,
                                                  imp.item_level,
                                                  isc.inner_pack_size,
                                                  isc.supp_pack_size,
                                                  isc.ti,
                                                  isc.hi,
                                                  ws.avail_qty,
                                                  NULL back_order_qty,
                                                  w.break_pack_ind,
                                                  ws.default_level,
                                                  ws.release_date,
                                                  ws.hold_back_pct_flag,
                                                  ws.hold_back_value,
                                                  ws.min_avail_qty,
                                                  ws.threshold_percent,
                                                  ws.calc_multiple,
                                                  ws.on_hand_qty,
                                                  ws.future_on_hand_qty,
                                                  ws.tsr_ind,
                                                  ws.pack_round,
                                                  NULL po_not_after_date,
                                                  NULL qty_ordered,
                                                  NULL proportion
                                             from alc_item_source ws,
                                                  item_master imp,
                                                  (select d.dept,
                                                          d.dept_name,
                                                          c.class,
                                                          c.class_name,
                                                          s.subclass,
                                                          s.sub_name subclass_name
                                                     from deps d,
                                                          class c,
                                                          subclass s
                                                    where d.dept  = c.dept
                                                      and c.dept  = s.dept
                                                      and c.class = s.class) mh,
                                                  packitem_breakout pb,
                                                  item_master imc,
                                                  item_master imparent,
                                                  diff_ids d1,
                                                  diff_ids d2,
                                                  diff_ids d3,
                                                  diff_ids d4,
                                                  wh w,
                                                  item_supp_country isc,
                                                  table(cast(:1 as OBJ_NUMERIC_ID_TABLE)) input
                                            where ws.alloc_id                = value(input)
                                              and ws.item_type               IN ('NSFSP',
                                                                                 'NSFMCP',
                                                                                 'NSFSCP')
                                              and ws.item_id                 = imp.item
                                              --
                                              and imp.dept                   = mh.dept
                                              and imp.class                  = mh.class
                                              and imp.subclass               = mh.subclass
                                              --
                                              and ws.item_id                 = pb.pack_no
                                              and pb.item                    = imc.item
                                              and imc.item_parent            = imparent.item
                                              and imc.diff_1                 = d1.diff_id(+)
                                              and imc.diff_2                 = d2.diff_id(+)
                                              and imc.diff_3                 = d3.diff_id(+)
                                              and imc.diff_4                 = d4.diff_id(+)
                                              --
                                              and ws.wh_id                   = w.wh(+)
                                              --
                                              and ws.item_id                 = isc.item(+)
                                              and isc.primary_supp_ind(+)    = 'Y'
                                              and isc.primary_country_ind(+) = 'Y'
                                            group by ws.alloc_id,
                                                     ws.item_source_id,
                                                     ws.item_id,
                                                     ws.item_type,
                                                     imp.item_desc,
                                                     ws.wh_id,
                                                     ws.source_type,
                                                     ws.order_no,
                                                     imp.dept,
                                                     imp.class,
                                                     imp.subclass,
                                                     mh.dept_name,
                                                     mh.class_name,
                                                     mh.subclass_name,
                                                     imparent.item_aggregate_ind,
                                                     imparent.diff_1_aggregate_ind,
                                                     imparent.diff_2_aggregate_ind,
                                                     imparent.diff_3_aggregate_ind,
                                                     imparent.diff_4_aggregate_ind,
                                                     imparent.item_desc,
                                                     imp.pack_ind,
                                                     imp.sellable_ind,
                                                     imp.item_level,
                                                     imp.tran_level,
                                                     imp.pack_ind,
                                                     isc.inner_pack_size,
                                                     isc.supp_pack_size,
                                                     isc.ti,
                                                     isc.hi,
                                                     ws.release_date,
                                                     ws.default_level,
                                                     ws.hold_back_pct_flag,
                                                     ws.hold_back_value,
                                                     ws.min_avail_qty,
                                                     ws.threshold_percent,
                                                     ws.calc_multiple,
                                                     ws.on_hand_qty,
                                                     ws.future_on_hand_qty,
                                                     ws.avail_qty,
                                                     ws.tsr_ind,
                                                     ws.pack_round,
                                                     w.break_pack_ind
   ]';

   EXECUTE IMMEDIATE L_pack_insert
      using I_alloc_ids;

   --NSSSP, NSSCP, ST, FASHIONSKU, SELLPACK
   insert into alc_load_temp (alloc_id,
                              item_source_id,
                              item,
                              item_type,
                              item_desc,
                              wh,
                              source_type,
                              doc_no,
                              dept,
                              class,
                              subclass,
                              dept_name,
                              class_name,
                              subclass_name,
                              item_aggregate_ind,
                              diff_1_aggregate_ind,
                              diff_2_aggregate_ind,
                              diff_3_aggregate_ind,
                              diff_4_aggregate_ind,
                              diff_1,
                              diff_2,
                              diff_3,
                              diff_4,
                              diff_1_desc,
                              diff_2_desc,
                              diff_3_desc,
                              diff_4_desc,
                              item_parent,
                              item_parent_desc,
                              item_grandparent,
                              pack_ind,
                              sellable_ind,
                              tran_level,
                              item_level,
                              inner_pack_size,
                              supp_pack_size,
                              ti,
                              hi,
                              avail_qty,
                              backorder_qty,
                              break_pack_ind,
                              default_level,
                              release_date,
                              hold_back_pct_flag,
                              hold_back_value,
                              min_avail_qty,
                              threshold_percent,
                              calc_multiple,
                              on_hand_qty,
                              future_on_hand_qty,
                              tsr_ind,
                              pack_round,
                              po_not_after_date,
                              qty_ordered,
                              proportion)
                       select DISTINCT
                              ws.alloc_id,
                              ws.item_source_id,
                              ws.item_id,
                              ws.item_type,
                              im.item_desc,
                              ws.wh_id,
                              ws.source_type,
                              ws.order_no,
                              im.dept,
                              im.class,
                              im.subclass,
                              mh.dept_name,
                              mh.class_name,
                              mh.subclass_name,
                              NULL item_aggregate_ind,
                              NULL diff_1_aggregate_ind,
                              NULL diff_2_aggregate_ind,
                              NULL diff_3_aggregate_ind,
                              NULL diff_4_aggregate_ind,
                              im.diff_1,
                              im.diff_2,
                              im.diff_3,
                              im.diff_4,
                              d1.diff_desc,
                              d2.diff_desc,
                              d3.diff_desc,
                              d4.diff_desc,
                              im.item_parent,
                              imparent.item_desc    item_parent_desc,
                              im.item_grandparent,
                              im.pack_ind,
                              im.sellable_ind,
                              im.tran_level,
                              im.item_level,
                              isc.inner_pack_size,
                              isc.supp_pack_size,
                              isc.ti,
                              isc.hi,
                              ws.avail_qty,
                              NULL backorder_qty,
                              w.break_pack_ind,
                              ws.default_level,
                              ws.release_date,
                              ws.hold_back_pct_flag,
                              ws.hold_back_value,
                              ws.min_avail_qty,
                              ws.threshold_percent,
                              ws.calc_multiple,
                              ws.on_hand_qty,
                              ws.future_on_hand_qty,
                              ws.tsr_ind,
                              ws.pack_round,
                              NULL po_not_after_date,
                              NULL qty_ordered,
                              NULL proportion
                         from alc_item_source ws,
                              item_master im,
                              item_master imparent,
                              (select d.dept,
                                      d.dept_name,
                                      c.class,
                                      c.class_name,
                                      s.subclass,
                                      s.sub_name subclass_name
                                 from deps d,
                                      class c,
                                      subclass s
                                where d.dept  = c.dept
                                  and c.dept  = s.dept
                                  and c.class = s.class) mh,
                              wh w,
                              diff_ids d1,
                              diff_ids d2,
                              diff_ids d3,
                              diff_ids d4,
                              item_supp_country isc,
                              table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input
                        where ws.alloc_id                = value(input)
                          and ws.item_type               IN (ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                             ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                             ALC_CONSTANTS_SQL.STAPLEITEM,
                                                             ALC_CONSTANTS_SQL.FASHIONSKU,
                                                             ALC_CONSTANTS_SQL.SELLABLEPACK)
                          and ws.item_id                 = im.item
                          --
                          and im.dept                    = mh.dept
                          and im.class                   = mh.class
                          and im.subclass                = mh.subclass
                          and im.diff_1                  = d1.diff_id(+)
                          and im.diff_2                  = d2.diff_id(+)
                          and im.diff_3                  = d3.diff_id(+)
                          and im.diff_4                  = d4.diff_id(+)
                          --
                          and im.item_parent             = imparent.item(+)
                          --
                          and ws.wh_id                   = w.wh(+)
                          --
                          and ws.item_id                 = isc.item(+)
                          and isc.primary_supp_ind(+)    = 'Y'
                          and isc.primary_country_ind(+) = 'Y';

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_LOAD_TEMP - NSSSP, NSSCP, ST, FASHIONSKU, SELLPACK - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into alc_load_temp target
   using (select i.alloc_id,
                 i.item,
                 i.wh,
                 i.source_type,
                 i.doc_no,
                 DECODE(imp.item_aggregate_ind,
                        'Y','Y',
                        'N') sellpack_fashion_ind
            from (select lt.alloc_id,
                         lt.item,
                         lt.wh,
                         lt.source_type,
                         lt.doc_no,
                         COUNT(DISTINCT im.item_parent) parent_cnt,
                         MIN(DISTINCT im.item_parent) item_parent
                    from (select i.alloc_id,
                                 i.item,
                                 i.wh,
                                 i.source_type,
                                 i.doc_no
                            from alc_load_temp i,
                                 table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input
                           where i.alloc_id  = value(input)
                             and i.item_type = ALC_CONSTANTS_SQL.SELLABLEPACK
                             and rownum      > 0) lt,
                         packitem_breakout pb2,
                         item_master im
                   where lt.item           = pb2.pack_no
                     and pb2.item          = im.item
                   group by lt.alloc_id,
                            lt.item,
                            lt.wh,
                            lt.source_type,
                            lt.doc_no) i,
                 item_master imp
           where i.item_parent = imp.item(+)) use_this
   on(    target.alloc_id           = use_this.alloc_id
      and target.item               = use_this.item
      and target.wh                 = use_this.wh
      and target.source_type        = use_this.source_type
      and NVL(target.doc_no,'-999') = NVL(use_this.doc_no,'-999'))
   when MATCHED then
      update
         set target.sellpack_fashion_ind      = use_this.sellpack_fashion_ind;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_LOAD_TEMP - SELLPACK - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_NOT_FA;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_INVENTORY(O_error_message   IN OUT VARCHAR2,
                       I_alloc_ids       IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(61) := 'ALC_LOAD_ITEM_SOURCE.POP_INVENTORY';

BEGIN

   merge into alc_load_temp target
   using (
          --OH
          select DISTINCT
                 al.alloc_id,
                 al.item,
                 al.wh,
                 al.source_type,
                 al.doc_no,
                 ((GREATEST((ils.stock_on_hand),0)) -
                  (GREATEST(ils.tsf_reserved_qty,0) +
                   GREATEST(ils.rtv_qty,0) +
                   GREATEST(ils.non_sellable_qty, 0) +
                   GREATEST(ils.customer_resv,0)))
                 +
                 --add back it the reserved qty subtracted out due to the alloc we are loading
                 NVL((select SUM(ald.qty_allocated)
                        from alloc_header alh,
                             alloc_detail ald,
                             alc_xref alx
                       where alh.alloc_no      = alx.xref_alloc_no
                         and ald.alloc_no      = alh.alloc_no
                         and alh.item          = al.item
                         and alh.wh            = al.wh
                         and alx.alloc_id      = al.alloc_id
                         and al.source_type    = ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_OH
                         and alh.order_no      is NULL
                       group by alh.item,
                             alh.doc),0) avail,
                 ils.customer_backorder backorder_qty,
                 al.qty_ordered,
                 al.proportion
            from table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input,
                 alc_load_temp al,
                 item_loc_soh ils
           where al.alloc_id     = value(input)
             and al.item         = ils.item (+)
             and al.wh           = ils.loc (+)
             and al.source_type !=  ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF) use_this
       on(    target.alloc_id        = use_this.alloc_id
      and target.item                = use_this.item
      and target.wh                  = use_this.wh
      and target.source_type         = use_this.source_type
      and NVL(target.doc_no,'-999')  = NVL(use_this.doc_no,'-999'))
   when MATCHED then
      update
         set target.avail_qty      = DECODE(use_this.source_type,
                                            ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_OH, use_this.avail,
                                            NULL),
             target.backorder_qty  = use_this.backorder_qty;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_LOAD_TEMP OH - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --non-sellable packs, backorder qty is sum of component items
   merge into alc_load_temp target
   using (
          select DISTINCT
                 al.alloc_id,
                 al.item,
                 al.wh,
                 al.source_type,
                 al.doc_no,
                 sum(ils.customer_backorder) backorder_qty
            from table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input,
                 alc_load_temp al,
                 packitem_breakout pb,
                 item_loc_soh ils
           where al.alloc_id    = value(input)
             and al.item_type   IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                    ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                    ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                    ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                    ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK)
             and al.item        = pb.pack_no
             and pb.item        = ils.item
             and al.wh          = ils.loc
           group by al.alloc_id,
                    al.item,
                    al.wh,
                    al.source_type,
                    al.doc_no) use_this
   on(    target.alloc_id            = use_this.alloc_id
      and target.item                = use_this.item
      and target.wh                  = use_this.wh
      and target.source_type         = use_this.source_type
      and NVL(target.doc_no,'-999')  = NVL(use_this.doc_no,'-999'))
   when MATCHED then
      update
         set target.backorder_qty  = target.backorder_qty + use_this.backorder_qty;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_LOAD_TEMP BACKORDER NON-SELLABLE PACKS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into alc_load_temp target
   using (
          -- PO
          select al.alloc_id,
                 al.item,
                 al.wh,
                 al.source_type,
                 al.doc_no,
                 inner.avail,
                 NULL qty_ordered,
                 oh.not_after_date po_not_after_date,
                 NULL proportion
            from table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input,
                 alc_load_temp al,
                 ordhead oh,
                 (select ol.order_no,
                         ol.item,
                         ol.location,
                         ol.qty_ordered - NVL(ol.qty_received,0) - NVL((select SUM(ald.qty_allocated)
                                                                          from alloc_header alh,
                                                                               alloc_detail ald
																		 where (alh.origin_ind!='ALC' or
																			  (alh.origin_ind='ALC' and alh.alloc_no in (select distinct xref_alloc_no from alc_xref where alloc_id != a.alloc_id)))
                                                                           and ald.alloc_no    = alh.alloc_no
                                                                           and alh.item        = a.item
                                                                           and alh.order_no    = a.doc_no
                                                                           and alh.wh          = a.wh
                                                                         group by alh.item,
                                                                                  alh.order_no),0) avail
                    from table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) v,
                         alc_load_temp a,
                         ordloc ol
                   where a.alloc_id  = value(v)
                     and a.doc_no    = TO_CHAR(ol.order_no)
                     and a.item      = ol.item
                     and a.wh        = ol.location) inner
            where al.alloc_id    = value(input)
              and oh.status      = 'A'
              and al.doc_no      = oh.order_no (+)
              and al.doc_no      = inner.order_no (+)
              and al.item        = inner.item (+)
              and al.wh          = inner.location (+)
              and al.source_type = ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_PO
          union all
          --TSF
          select al.alloc_id,
                 al.item,
                 al.wh,
                 al.source_type,
                 al.doc_no,
                 td.tsf_qty - NVL(td.ship_qty,0) - NVL((select SUM(ald.qty_allocated)
                                                          from alloc_header alh,
                                                               alloc_detail ald,
                                                               alc_xref alx
                                                         where alh.alloc_no    = alx.xref_alloc_no
                                                           and ald.alloc_no    = alh.alloc_no
                                                           and alh.item        = al.item
                                                           and alh.order_no    = th.tsf_no
                                                           and alh.wh          = al.wh
                                                           and alx.alloc_id   != al.alloc_id
                                                         group by alh.item,
                                                                  alh.order_no),0) avail,
                 NULL qty_ordered,
                 th.not_after_date po_not_after_date,
                 NULL proportion
            from table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input,
                 alc_load_temp al,
                 tsfhead th,
                 tsfdetail td
           where al.alloc_id    = value(input)
             and al.doc_no      = th.tsf_no
             and al.wh          = th.to_loc
             and th.tsf_no      = td.tsf_no
             and al.item        = td.item
             and al.source_type = ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_TSF
          union all
          --BOL->TSF
          select al.alloc_id,
                 al.item,
                 al.wh,
                 al.source_type,
                 al.doc_no,
                 ss.qty_expected - NVL(ss.qty_received,0) - NVL((select SUM(ald.qty_allocated)
                                                                   from alloc_header alh,
                                                                        alloc_detail ald,
                                                                        alc_xref alx
                                                                  where alh.alloc_no    = alx.xref_alloc_no
                                                                    and ald.alloc_no    = alh.alloc_no
                                                                    and alh.item        = al.item
                                                                    and alh.doc_type    = 'BOL'
                                                                    and alh.doc         = sh.bol_no
                                                                    and alh.wh          = al.wh
                                                                    and alx.alloc_id   != al.alloc_id
                                                                  group by alh.item,
                                                                           alh.doc),0) avail,
                 td.tsf_qty qty_ordered,
                 th.not_after_date po_not_after_date,
                 NULL proportion
            from table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input,
                 alc_load_temp al,
                 shipment sh,
                 shipsku ss,
                 wh w,
                 tsfdetail td,
                 tsfhead th
           where al.alloc_id    = value(input)
             and al.source_type = ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_BOL
             and sh.bol_no      = al.doc_no
             and al.wh          = w.wh
             and w.physical_wh  = sh.to_loc
             --
             and sh.shipment    = ss.shipment
             and ss.item        = al.item
             --
             and ss.distro_type = 'T'
             and ss.distro_no   = td.tsf_no
             and ss.item        = td.item
             and td.tsf_no      = th.tsf_no
             and al.wh          = th.to_loc
          union all
          --BOL->ALLOC
          select i.alloc_id,
                 i.item,
                 i.wh,
                 i.source_type,
                 i.doc_no,
                 i.avail*i.proportion,
                 i.qty_ordered,
                 i.po_not_after_date,
                 NULL proportion
            from (select al.alloc_id,
                         al.item,
                         al.wh,
                         al.source_type,
                         al.doc_no,
                         ad.to_loc,
                         SUM(ss.qty_expected - NVL(ss.qty_received,0) - NVL((select SUM(ald.qty_allocated)
                                                                               from alloc_header alh,
                                                                                    alloc_detail ald,
                                                                                    alc_xref alx
                                                                              where alh.alloc_no    = alx.xref_alloc_no
                                                                                and ald.alloc_no    = alh.alloc_no
                                                                                and alh.item        = al.item
                                                                                and alh.doc_type    = 'BOL'
                                                                                and alh.doc         = sh.bol_no
                                                                                and alh.wh          = al.wh
                                                                                and alx.alloc_id   != al.alloc_id
                                                                              group by alh.item,
                                                                                       alh.doc),0)) OVER (PARTITION BY al.alloc_id,
                                                                                                                       al.item, al.wh,
                                                                                                                       al.source_type,
                                                                                                                       al.doc_no,
                                                                                                                       ad.to_loc,
                                                                                                                       ad.qty_allocated,
                                                                                                                       ah.release_date) avail,
                         ad.qty_allocated qty_ordered,
                         ah.release_date po_not_after_date,
                         ad.qty_allocated / (SUM(ad.qty_allocated) OVER (PARTITION BY al.alloc_id,
                                                                                      al.wh, al.item,
                                                                                      al.doc_no,
                                                                                      w.physical_wh)) proportion
                    from table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input,
                         alc_load_temp al,
                         shipment sh,
                         shipsku ss,
                         wh w,
                         wh allocwh,
                         alloc_header ah,
                         alloc_detail ad
                   where al.alloc_id    = value(input)
                     and al.source_type = ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_BOL
                     and sh.bol_no      = al.doc_no
                     and al.wh          = w.wh
                     and w.physical_wh  = sh.to_loc
                     --
                     and sh.shipment    = ss.shipment
                     and ss.item        = al.item
                     --
                     and ss.distro_type = 'A'
                     and ss.distro_no   = ah.alloc_no
                     and ss.item        = ah.item
                     and ah.alloc_no    = ad.alloc_no
                     and ad.to_loc      = allocwh.wh
                     and w.physical_wh  = allocwh.physical_wh) i
           where i.to_loc = i.wh
          union all
          --ALLOC
          select al.alloc_id,
                 al.item,
                 al.wh,
                 al.source_type,
                 al.doc_no,
                 SUM(ad.qty_allocated - NVL(ad.qty_transferred,0) - NVL((select SUM(ald.qty_allocated)
                                                                           from alloc_header alh,
                                                                                alloc_detail ald,
                                                                                alc_xref alx
                                                                          where alh.alloc_no    = alx.xref_alloc_no
                                                                            and ald.alloc_no    = alh.alloc_no
                                                                            and alh.item        = al.item
                                                                            and alh.order_no    = ah.alloc_no
                                                                            and alh.wh          = al.wh
                                                                            and alx.alloc_id   != al.alloc_id
                                                                          group by alh.item,
                                                                                   alh.order_no),0)) avail,
                 NULL qty_ordered,
                 ah.release_date po_not_after_date,
                 NULL proportion
            from table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input,
                 alc_load_temp al,
                 alc_xref x,
                 alloc_header ah,
                 alloc_detail ad
           where al.alloc_id     = value(input)
             and al.doc_no       = x.alloc_id
             and x.xref_alloc_no = ah.alloc_no
             and ah.item         = al.item
             and ah.alloc_no     = ad.alloc_no
             and al.wh           = ad.to_loc
             and al.source_type  = ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ALLOC
           group by al.alloc_id,
                    al.item,
                    al.wh,
                    al.source_type,
                    al.doc_no,
                    ah.release_date
   ) use_this
   on(    target.alloc_id            = use_this.alloc_id
      and target.item                = use_this.item
      and target.wh                  = use_this.wh
      and target.source_type         = use_this.source_type
      and NVL(target.doc_no,'-999')  = NVL(use_this.doc_no,'-999'))
   when MATCHED then
      update
         set target.avail_qty         = use_this.avail,
             target.po_not_after_date = use_this.po_not_after_date,
             target.qty_ordered       = use_this.qty_ordered,
             target.proportion        = use_this.proportion;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_LOAD_TEMP PO-TSF-BOL-ALLOC - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if GET_ASN_INV(O_error_message,
                  I_alloc_ids) = FALSE then
      return FALSE;
   end if;

   --delete any pack item that does not exist on the PO/ASN/TSF/BOL source, or not ranged to the WH for OH source
   delete from alc_load_temp
    where rowid in(select t.rowid
                     from alc_load_temp t,
                          alc_alloc aa
                    where t.alloc_id IN (select value(input)
                                           from table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input)
                      and aa.alloc_id = t.alloc_id
                      and aa.type     = ALC_CONSTANTS_SQL.FASHION_ALLOCATION
                      and t.source_type IN (ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_PO,
                                            ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ASN,
                                            ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_BOL,
                                            ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_TSF,
                                            ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_OH,
                                            ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ALLOC)
                      and t.item_type IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                          ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                          ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                          ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                          ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK,
                                          ALC_CONSTANTS_SQL.SELLABLEPACK)
                      and t.avail_qty is NULL);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_LOAD_TEMP PACKITEM- SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_INVENTORY;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_ASN_INV(O_error_message   IN OUT VARCHAR2,
                     I_alloc_ids       IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'ALC_LOAD_ITEM_SOURCE.GET_ASN_INV';

   L_dist_tab       DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   L_ctr            NUMBER(10) := 0;
   L_asn_dist_table ASN_DIST_TABLE_TYPE;

   cursor C_SHIPSKU is
      select al.alloc_id,
             al.source_type,
             al.doc_no,
             ss.item,
            sum((ss.qty_expected         -
              NVL(ss.qty_received,0)) -
              NVL((select SUM(ald.qty_allocated)
                     from alloc_header alh,
                          alloc_detail ald,
                          alc_xref alx
                    where alh.alloc_no    = alx.xref_alloc_no
                      and ald.alloc_no    = alh.alloc_no
                      and alh.item        = al.item
                      and alh.doc_type    = 'ASN'
                      and alh.doc         = sh.asn
                      and alh.wh          = al.wh
                      and alx.alloc_id   != al.alloc_id
                    group by alh.item,
                          alh.doc),0) -
              NVL((select SUM(ald.qty_allocated)
                     from alloc_header alh,
                          alloc_detail ald,
                          alc_xref alx
                    where alh.alloc_no    = alx.xref_alloc_no
                      and ald.alloc_no    = alh.alloc_no
                      and alh.item        = al.item
                      and alh.doc_type    = 'PO'
                      and alh.order_no    = oh.order_no
                      and alh.wh          = al.wh
                      and alx.alloc_id   != al.alloc_id
                    group by alh.item,
                             alh.order_no),0)) qty,
             sh.order_no,
             sh.to_loc,
             oh.not_after_date
        from table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input,
             alc_load_temp al,
             shipment sh,
             shipsku_loc ss,
             ordhead oh,
		  wh w
       where al.alloc_id    = value(input)
         and al.source_type = ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ASN
         and sh.asn         = al.doc_no
         and sh.shipment    = ss.shipment
	    and w.wh = ss.to_loc
         and w.physical_wh     = sh.to_loc
         and sh.order_no    = oh.order_no
         and al.item        = ss.item
         group by al.alloc_id,
             al.source_type,
             al.doc_no,
             ss.item,
             sh.order_no,
             sh.to_loc,
             oh.not_after_date;

BEGIN

   for rec in C_SHIPSKU loop

      if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                     O_DIST_TAB           => L_dist_tab,
                                     I_ITEM               => rec.item,
                                     I_LOC                => rec.to_loc,
                                     I_QTY                => rec.qty,
                                     I_CMI                => 'ORDRCV',
                                     I_INV_STATUS         => NULL,
                                     I_TO_LOC_TYPE        => NULL,
                                     I_TO_LOC             => NULL,
                                     I_ORDER_NO           => rec.order_no,
                                     I_SHIPMENT           => NULL,
                                     I_SEQ_NO             => NULL,
                                     I_CYCLE_COUNT        => NULL,
                                     I_RTV_ORDER_NO       => NULL,
                                     I_RTV_SEQ_NO         => NULL,
                                     I_TSF_NO             => NULL,
                                     I_TSF_CREATE_IND     => NULL,
                                     I_CUST_ORDER_LOC_IND => NULL,
                                     I_ALLOC_NO           => NULL) = FALSE then
         return FALSE;
      end if;

      if L_dist_tab is null or L_dist_tab.COUNT = 0 then

         O_error_message := ALC_CONSTANTS_SQL.ERRMSG_ASN_DISTRIB;
         return FALSE;

      else

         for i in 1 .. L_dist_tab.COUNT LOOP

            L_ctr := L_ctr + 1;
            L_asn_dist_table(L_ctr).alloc_id           := rec.alloc_id;
            L_asn_dist_table(L_ctr).item               := rec.item;
            L_asn_dist_table(L_ctr).wh                 := L_dist_tab(i).wh;
            L_asn_dist_table(L_ctr).source_type        := rec.source_type;
            L_asn_dist_table(L_ctr).doc_no             := rec.doc_no;
            L_asn_dist_table(L_ctr).avail              := L_dist_tab(i).dist_qty;
            L_asn_dist_table(L_ctr).po_not_after_date  := rec.not_after_date;

         end loop;

      end if;

   end loop;

   FORALL i IN L_asn_dist_table.FIRST .. L_asn_dist_table.LAST
      merge into alc_load_temp target
      using (select al.alloc_id,
                    al.item,
                    al.wh,
                    al.source_type,
                    al.doc_no
               from alc_load_temp al
              where al.alloc_id    = L_asn_dist_table(i).alloc_id
                and al.item        = L_asn_dist_table(i).item
                and al.wh          = L_asn_dist_table(i).wh
                and al.source_type = L_asn_dist_table(i).source_type
                and al.doc_no      = L_asn_dist_table(i).doc_no) use_this
      on(    target.alloc_id    = use_this.alloc_id
         and target.item        = use_this.item
         and target.wh          = use_this.wh
         and target.source_type = use_this.source_type
         and target.doc_no      = use_this.doc_no)
      when MATCHED then
         update
            set target.avail_qty         = NVL(L_asn_dist_table(i).avail,0),
                target.po_not_after_date = L_asn_dist_table(L_ctr).po_not_after_date;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_LOAD_TEMP ASN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ASN_INV;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_IMAGE(O_error_message   IN OUT VARCHAR2,
                   I_alloc_ids   IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(61) := 'ALC_LOAD_ITEM_SOURCE.POP_IMAGE';

BEGIN

   merge into alc_load_temp target
   using (select i.alloc_id,
                 i.item,
                 i.wh,
                 i.source_type,
                 i.doc_no,
                 i.image_name,
                 i.image_addr
            from (select al.alloc_id,
                         al.item,
                         al.wh,
                         al.source_type,
                         al.doc_no,
                         im.image_name,
                         im.image_addr,
                         row_number() OVER (PARTITION BY al.alloc_id,
                                                         al.item,
                                                         al.wh,
                                                         al.source_type,
                                                         al.doc_no
                                                ORDER BY al.alloc_id,
                                                         al.item,
                                                         al.wh,
                                                         al.source_type,
                                                         al.doc_no) num
                    from table(cast(I_alloc_ids as OBJ_NUMERIC_ID_TABLE)) input,
                         alc_load_temp al,
                         item_image im
                   where al.alloc_id    = value(input)
                     and al.item        = im.item(+)) i
           where num = 1) use_this
   on(    target.alloc_id            = use_this.alloc_id
      and target.item                = use_this.item
      and target.wh                  = use_this.wh
      and target.source_type         = use_this.source_type
      and NVL(target.doc_no,'-999')  = NVL(use_this.doc_no,'-999'))
   when MATCHED then
      update
         set target.image_name = use_this.image_name,
             target.image_addr = use_this.image_addr;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_LOAD_TEMP IMAGE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_IMAGE;
-------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_LOAD_TABLES(O_error_message   IN OUT VARCHAR2,
                           I_alloc_ids       IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_LOAD_ITEM_SOURCE.PURGE_LOAD_TABLES';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   delete from alc_load_temp
    where alloc_id IN (select value(input)
                         from table(cast(I_alloc_ids  as OBJ_NUMERIC_ID_TABLE)) input);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_LOAD_TEMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END PURGE_LOAD_TABLES;
-------------------------------------------------------------------------------------------------------------
END ALC_LOAD_ITEM_SOURCE;
/
