drop type alc_item_search_criteria_rec force;

create or replace type alc_item_search_criteria_rec as object (
   po_source_type_ind       NUMBER(1),
   asn_source_type_ind      NUMBER(1),
   tsf_source_type_ind      NUMBER(1),
   bol_source_type_ind      NUMBER(1),
   alloc_source_type_ind    NUMBER(1),
   wh_source_type_ind       NUMBER(1),
   --
   dept                     NUMBER(4),
   class                    NUMBER(4),
   subclass                 NUMBER(4),
   season                   NUMBER(3),
   phase                    NUMBER(3),
   --
   items                    OBJ_VARCHAR_ID_TABLE,
   item_lists               OBJ_NUMERIC_ID_TABLE,
   udas                     OBJ_NUMERIC_ID_TABLE,
   uda_values               OBJ_NUMERIC_ID_TABLE,
   sups                     OBJ_NUMERIC_ID_TABLE,
   sup_sites                OBJ_NUMERIC_ID_TABLE,
   --
   pos                      OBJ_NUMERIC_ID_TABLE,
   asns                     OBJ_VARCHAR_ID_TABLE,
   tsfs                     OBJ_NUMERIC_ID_TABLE,
   bols                     OBJ_VARCHAR_ID_TABLE,
   appts                    OBJ_NUMERIC_ID_TABLE,
   allocs                   OBJ_NUMERIC_ID_TABLE,
   alc_allocs               OBJ_NUMERIC_ID_TABLE,
   --
   whs                      OBJ_NUMERIC_ID_TABLE,
   --
   start_date               DATE,
   end_date                 DATE
);
/