DROP TYPE alc_retail_info_tbl FORCE
/

DROP TYPE alc_retail_info_rec FORCE
/

CREATE OR REPLACE TYPE alc_retail_info_rec AS OBJECT
(
 ITEM                               VARCHAR2(25),
 LOCATION                           NUMBER(10),
 --
 ACTION_DATE                        DATE,
 ACTION_SELLING_RETAIL              NUMBER(20,4),
 ACTION_SELLING_UOM                 VARCHAR2(4),
 ACTION_SELLING_RETAIL_CURRENCY     VARCHAR2(3),
 --
 CURR_SELLING_RETAIL                NUMBER(20,4),
 CURR_SELLING_UOM                   VARCHAR2(4),
 CURR_SELLING_RETAIL_CURRENCY       VARCHAR2(3)


)
/

CREATE OR REPLACE TYPE alc_retail_info_tbl AS TABLE OF alc_retail_info_rec
/
