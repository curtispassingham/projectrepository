----------------------------------------------------------------------------
-- Copyright (c) 2013, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
----------------------------------------------------------------------------


----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------

PROMPT Dropping Record Type 'ALC_OI_PO_ARRIVAL_RESULT_REC'
DROP TYPE ALC_OI_PO_ARRIVAL_RESULT_REC FORCE;
/


PROMPT Creating Record Type 'ALC_OI_PO_ARRIVAL_RESULT_REC'
CREATE OR REPLACE TYPE ALC_OI_PO_ARRIVAL_RESULT_REC AS OBJECT
(
   START_DATE            DATE,
   END_DATE              DATE,
   FULLY_ALLOCATED       NUMBER(20,4),
   PARTIALLY_ALLOCATED   NUMBER(20,4),
   UNALLOCATED           NUMBER(20,4),
   TOTAL                 NUMBER(20,4)
)
/


----------------------------------------
PROMPT Dropping Record Type 'ALC_OI_STOCK_TO_SALES_REC'
DROP TYPE ALC_OI_STOCK_TO_SALES_REC FORCE;
/


PROMPT Dropping Record Type 'ALC_OI_STOCK_TO_SALES_TBL'
DROP TYPE ALC_OI_STOCK_TO_SALES_TBL FORCE;
/


PROMPT Creating Record Type 'ALC_OI_STOCK_TO_SALES_REC'
CREATE OR REPLACE TYPE ALC_OI_STOCK_TO_SALES_REC AS OBJECT
(
   EOW_DATE         DATE,
   STOCK            NUMBER(20,4),
   SALES            NUMBER(20,4),
   STOCK_TO_SALES   NUMBER(20,4)
)
/

PROMPT Creating Record Type 'ALC_OI_STOCK_TO_SALES_TBL'
CREATE OR REPLACE TYPE ALC_OI_STOCK_TO_SALES_TBL AS TABLE OF ALC_OI_STOCK_TO_SALES_REC
/

----------------------------------------
PROMPT Dropping Record Type 'ALC_OI_TOP_BOTTOM_SALES_REC'
DROP TYPE ALC_OI_TOP_BOTTOM_SALES_REC FORCE;
/

PROMPT Creating Record Type 'ALC_OI_TOP_BOTTOM_SALES_REC'
CREATE OR REPLACE TYPE ALC_OI_TOP_BOTTOM_SALES_REC AS OBJECT
(
   ITEM             VARCHAR2(25),
   DIFF_1           VARCHAR2(10),
   DIFF_2           VARCHAR2(10),
   DIFF_3           VARCHAR2(10),
   DIFF_4           VARCHAR2(10),
   ITEM_DESC        VARCHAR2(250),
   IMAGE_NAME       VARCHAR2(120),
   IMAGE_ADDR       VARCHAR2(255),
   UNITS            NUMBER(20,4),
   UOM              VARCHAR2(4),
   SALES            NUMBER(20,4),
   CURRENCY_CODE    VARCHAR2(3),
   MARGIN_PERCENT   NUMBER(20,4)
)
/

----------------------------------------
PROMPT Dropping Record Type 'ALC_OI_PLAN_FORECAST_REC'
DROP TYPE ALC_OI_PLAN_FORECAST_REC FORCE;
/

PROMPT Dropping Record Type 'ALC_OI_PLAN_FORECAST_TBL'
DROP TYPE ALC_OI_PLAN_FORECAST_TBL FORCE;
/

PROMPT Creating Record Type 'ALC_OI_PLAN_FORECAST_REC'
CREATE OR REPLACE TYPE ALC_OI_PLAN_FORECAST_REC AS OBJECT
(
   EOW_DATE            DATE,
   ALLOCATED_QTY       NUMBER(20,4),
   PLAN_FORECAST_QTY   NUMBER(20,4)
)
/

PROMPT Creating Record Type 'ALC_OI_PLAN_FORECAST_TBL'
CREATE OR REPLACE TYPE ALC_OI_PLAN_FORECAST_TBL AS TABLE OF ALC_OI_PLAN_FORECAST_REC
/


----------------------------------------
PROMPT Dropping Record Type 'ALC_OI_OTB_REC'
DROP TYPE ALC_OI_OTB_REC FORCE;
/

PROMPT Creating Record Type 'ALC_OI_OTB_REC'
CREATE OR REPLACE TYPE ALC_OI_OTB_REC AS OBJECT
(
   OTB_BUDGET_AMOUNT        NUMBER(20,4),
   OTB_APPROVED_PO_AMOUNT   NUMBER(20,4),
   OTB_RECEIPT_AMOUNT       NUMBER(20,4),
   OTB_AVAILABLE_AMOUNT     NUMBER(20,4)
)
/