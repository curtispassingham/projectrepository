-- ==========================================================================
-- Copyright (c) 2012, Oracle and/or its affiliates. All rights reserved. 
-- ==========================================================================
-- $Header: /cvs/CVS_REPOSIT/rms/rms-base/dbserver/required_patches/mom-dbpatch/13.2.5/alloc_rms/objects/alc_uda_id_uda_value_rec.sql,v 1.1 2012/09/14 22:40:12 waschwar Exp $
-- ==========================================================================
-- NOTES
-- <other useful comments, qualifications, etc.>
--
-- MODIFIED    (MM/DD/YY)
-- eebbesen    02/22/12 - Merging updated code for 13335330
-- eebbesen    02/11/12 - Updating object names
-- eebbesen    02/11/12 - co
-- eebbesen    01/31/12 - Modifying DDL scripts to have entire create or
--                        replace clause on one line to avoid build errors
-- eebbesen    01/31/12 - co
-- eebbesen    01/30/12 - Moving DaoUtilityTest.java to the proper location;
--                        adding UtilityTest.java changes; creating new files
--                        for non-table DDL
-- eebbesen    01/30/12 - new
-- eebbesen    01/30/12 - Creation
-- ==========================================================================
drop type ALC_UDA_ID_UDA_VALUE_REC force
/

create or replace TYPE ALC_UDA_ID_UDA_VALUE_REC AS OBJECT
(
   uda_id       NUMBER(5),
   uda_value    NUMBER(5)
)
/
