DROP TYPE alc_calc_packs_tbl FORCE
/

DROP TYPE alc_calc_packs_rec FORCE
/

CREATE OR REPLACE TYPE alc_calc_packs_rec AS OBJECT
(
   fashion_item_id varchar2(25),
   diff1_id        varchar2(100),
   diff2_id        varchar2(100),
   wh_id           number(10),
   order_no        varchar2(40),
   source_type     varchar2(1),
   --
   pack_no         varchar2(25)
)
/

CREATE OR REPLACE TYPE alc_calc_packs_tbl AS TABLE OF alc_calc_packs_rec
/
