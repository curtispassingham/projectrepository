drop TYPE ALC_SYNC_DETAIL_TBL force;
drop TYPE ALC_SYNC_DETAIL_REC force;

create or replace TYPE ALC_SYNC_DETAIL_REC AS OBJECT
(
   ALLOC_ID          NUMBER(15),
   --
   TO_LOC            NUMBER(10),
   TO_LOC_TYPE       VARCHAR2(1),
   QTY_TRANSFERRED   NUMBER(12,4),
   QTY_ALLOCATED     NUMBER(12,4),
   QTY_PRESCALED     NUMBER(12,4),
   NON_SCALE_IND     VARCHAR2(1),
   IN_STORE_DATE     DATE,       
   RUSH_FLAG         VARCHAR2(1)
)
/
create or replace TYPE ALC_SYNC_DETAIL_TBL AS TABLE OF ALC_SYNC_DETAIL_REC
/

---

drop TYPE ALC_SYNC_HEADER_TBL force;
drop TYPE ALC_SYNC_HEADER_REC force;

create or replace TYPE ALC_SYNC_HEADER_REC AS OBJECT
(
   ALLOC_ID              NUMBER(15),
   --
   ORDER_NO              NUMBER(10),
   WH                    NUMBER(10),
   ITEM                  VARCHAR2(25),
   STATUS                VARCHAR2(1),
   ALLOC_DESC            VARCHAR2(300),
   PO_TYPE               VARCHAR2(4),
   ALLOC_METHOD          VARCHAR2(1),
   RELEASE_DATE          DATE,
   ORDER_TYPE            VARCHAR2(9),
   CONTEXT_TYPE          VARCHAR2(6),
   CONTEXT_VALUE         VARCHAR2(25),
   COMMENT_DESC          VARCHAR2(2000),
   DOC                   VARCHAR2(30),
   DOC_TYPE              VARCHAR2(5),
   ORIGIN_IND            VARCHAR2(6),
   ALLOC_DETAILS         ALC_SYNC_DETAIL_TBL
)
/
create or replace TYPE ALC_SYNC_HEADER_TBL AS TABLE OF ALC_SYNC_HEADER_REC
/