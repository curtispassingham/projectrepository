DROP TYPE OBJ_SIZEPROFILE FORCE
/
DROP TYPE OBJ_SIZEPROFILE_TABLE FORCE
/
DROP TYPE OBJ_STORE FORCE
/
DROP TYPE OBJ_GID_PROFILE_HEADERS FORCE
/

CREATE OR REPLACE TYPE OBJ_SIZEPROFILE AS OBJECT
(
   size_profile_id       NUMBER(20),
   new_size_profile_id   NUMBER(20),
   gid_profile_id        NUMBER(15),
   store                 VARCHAR2(40),
   dept                  VARCHAR2(40),
   class                 VARCHAR2(40),
   subclass              VARCHAR2(40),
   style                 VARCHAR2(25),
   styleDesc             VARCHAR2(250),
   child_diff1           VARCHAR2(40),
   diff_group_id1        VARCHAR2(40),
   child_diff2           VARCHAR2(40),
   diff_group_id2        VARCHAR2(40),
   child_diff3           VARCHAR2(40),
   diff_group_id3        VARCHAR2(40),
   child_diff4           VARCHAR2(40),
   diff_group_id4        VARCHAR2(40),
   display_seq           NUMBER(4),
   qty                   NUMBER(12,4),
   size_profile          VARCHAR2(512) 
)
/

CREATE TYPE OBJ_SIZEPROFILE_TABLE IS 
TABLE OF OBJ_SIZEPROFILE
/

CREATE OR REPLACE TYPE OBJ_STORE AS OBJECT 
(
   store NUMBER(10)
)
/

CREATE OR REPLACE TYPE OBJ_STORE_TABLE AS 
TABLE OF OBJ_STORE
/

CREATE OR REPLACE TYPE OBJ_GID_PROFILE_HEADERS AS OBJECT
(
  seq_id               NUMBER(4),
  gid                  VARCHAR2(40),
  gid_desc             VARCHAR2(40),
  gid_profile_ids      OBJ_NUMERIC_ID_TABLE,
  size_profile_desc    VARCHAR2(2048),
  size_profile_level   NUMBER(1)
)
/

CREATE OR REPLACE TYPE OBJ_GID_PROFILE_HEADERS_TABLE IS TABLE OF OBJ_GID_PROFILE_HEADERS
/