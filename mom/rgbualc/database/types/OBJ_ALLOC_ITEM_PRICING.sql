DROP TYPE OBJ_ALLOC_ITEM_PRICING_TBL FORCE
/
DROP TYPE OBJ_ALLOC_ITEM_PRICING_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_ALLOC_ITEM_PRICING_REC AS OBJECT
(
 ITEM                              VARCHAR2(25),
 ITEM_DESC                         VARCHAR2(250),
 IS_PARENT                         NUMBER(1),
 DIFF_ID                           VARCHAR2(10),
 LOCATION                          NUMBER(10),
 LOCATION_DISPLAY_ID               NUMBER(10),
 LOCATION_NAME                     VARCHAR2(150),
 CUST_SEG_PROMO_IND                NUMBER(1),
 PRIMARY_ZONE                      NUMBER(1),
 IS_ZONE                           NUMBER(1),
 ACTION_DATE                       DATE,
 SELLING_RETAIL                    NUMBER(20,4),
 SELLING_UOM                       VARCHAR2(4),
 SELLING_RETAIL_CURRENCY           VARCHAR2(3),
 MULTI_UNITS                       NUMBER(12,4),
 MULTI_UNIT_RETAIL                 NUMBER(20,4),
 MULTI_SELLING_UOM                 VARCHAR2(4),
 MULTI_SELLING_RETAIL_CURRENCY     VARCHAR2(3),
 CLEAR_RETAIL                      NUMBER(20,4),
 CLEAR_UOM                         VARCHAR2(4),
 CLEAR_RETAIL_CURRENCY             VARCHAR2(3),
 SIMPLE_PROMO_RETAIL               NUMBER(20,4),
 SIMPLE_PROMO_UOM                  VARCHAR2(4),
 SIMPLE_PROMO_RETAIL_CURRENCY      VARCHAR2(3),
 COMPLEX_PROMO                     NUMBER(1),
 REG_RETAIL_AVG                    NUMBER(1),
 REG_MULTI_UNIT_AVG                NUMBER(1),
 REG_MULTI_UNIT_RETAIL_AVG         NUMBER(1),
 CLEAR_RETAIL_AVG                  NUMBER(1),
 PROMO_RETAIL_AVG                  NUMBER(1),
 COST                              NUMBER(20,4),
 MARKUP_PERCENT                    NUMBER(12,4),
 PROMO_ID                          NUMBER(10),
 PROMO_DISPLAY_ID                  NUMBER(10), 
 PROMO_NAME                        VARCHAR2(160),
 PROMO_DESC                        VARCHAR2(640),
 START_DATE                        DATE,
 END_DATE                          DATE 
)
/

CREATE OR REPLACE TYPE OBJ_ALLOC_ITEM_PRICING_TBL AS TABLE OF OBJ_ALLOC_ITEM_PRICING_REC
/