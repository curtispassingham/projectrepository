create or replace 
TRIGGER ALC_ALH_BUR1
 BEFORE UPDATE
 ON ALLOC_HEADER
 FOR EACH ROW
DECLARE

   L_alloc_no         ALLOC_HEADER.ALLOC_NO%TYPE;
   L_status           ALLOC_HEADER.STATUS%TYPE;
   L_alloc_id         ALC_ALLOC.ALLOC_ID%TYPE;
   L_error_message    RTK_ERRORS.RTK_TEXT%TYPE;
   L_text   		  VARCHAR2(255) := NULL;
   ---
   cursor C_ALC_XREF is
      select alloc_id
        from alc_xref
       where xref_alloc_no = L_alloc_no;


   cursor C_CLOSE_MASTER_ALLOC is
       select count(*) as not_close_line_cnt
		from alc_xref
		where alloc_id = L_alloc_id
		and nvl(close_ind,'N') != 'Y';
BEGIN

   if UPDATING then
      L_alloc_no := nvl(:new.alloc_no, :old.alloc_no);
      L_status   := :new.status;
      ---
	  if L_status = 'C' then

	  	 ---------------------------------------
		 -- mark this alloc_no as closed
		 ---------------------------------------
		 update alc_xref
		    set close_ind = 'Y'
		  where xref_alloc_no = L_alloc_no;

		 --------------------------------------
		 -- Find the master alloc id
		 --------------------------------------
         FOR rec IN C_ALC_XREF LOOP
            L_alloc_id  := rec.alloc_id;

		 END LOOP;

		 -- if all the xref records for the master alloc id are closed,
		 -- then close the master allocation

		 FOR rec IN C_CLOSE_MASTER_ALLOC LOOP
		  	  if rec.not_close_line_cnt = 0 then
			  	 update ALC_ALLOC
		            set status = '4' --Closed Status for ALC_ALLOC
		          where alloc_id    = L_alloc_id;
		      end if;
		 END LOOP;

      end if;
   end if;

EXCEPTION
   when OTHERS then
      if L_text is NULL then
	     L_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
		                           SQLERRM,
					   'ALLOC_STATUS_TRIGGER',
					   NULL);
	  end if;
	  raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END;
/