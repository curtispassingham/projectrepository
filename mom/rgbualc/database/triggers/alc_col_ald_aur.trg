CREATE OR REPLACE TRIGGER ALC_TABLE_ALD_AUR
 AFTER UPDATE OF QTY_TRANSFERRED, QTY_DISTRO, QTY_SELECTED, QTY_RECEIVED
 ON ALLOC_DETAIL
 FOR EACH ROW
BEGIN

   if (:new.qty_transferred > 0 and (nvl(:old.qty_transferred,0) = 0)) or
      (:new.qty_distro      > 0 and (nvl(:old.qty_distro,0) = 0)) or
      (:new.qty_selected    > 0 and (nvl(:old.qty_selected,0) = 0)) or
      (:new.qty_received    > 0 and (nvl(:old.qty_received,0) = 0)) then

      -- Only updating Allocation specific tables.  NOTE: RMS alloc_header should have its status updated too.
      update alc_alloc
         set status = '3'
       where alloc_id in (select alloc_id 
                            from alc_xref 
                           where xref_alloc_no = :new.alloc_no);
   end if;

END;
/
