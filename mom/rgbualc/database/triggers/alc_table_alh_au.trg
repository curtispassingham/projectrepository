create or replace 
TRIGGER ALC_ALH_AU
 AFTER UPDATE
 ON ALLOC_HEADER
DECLARE


   L_status        ALLOC_HEADER.STATUS%TYPE;
   L_alloc_id      ALC_ALLOC.ALLOC_ID%TYPE;

   cursor c_status(alloc_id number) is
      select distinct ah.status
        from alloc_header ah,
             alc_xref ax
       where ax.xref_alloc_no = ah.alloc_no
         and ax.alloc_id = L_alloc_id
         and ah.status !='C';
BEGIN

   FOR rec IN 1..ALLOC_PKG.ALLOC_ID.COUNT

   loop
      L_alloc_id  := alloc_pkg.alloc_id(rec);

      open c_status(L_alloc_id);
      fetch c_status into L_status;
      close c_status;
      --
      if L_status is null then
         update ALC_ALLOC
            set status = '4' --Closed Status for ALC_ALLOC
          where alloc_id    = L_alloc_id;
      else
            NULL;
      end if;
       ---
    end loop;
    alloc_pkg.alloc_id.delete; --Removing the data from pl/sql array type
END;
/