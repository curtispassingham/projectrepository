create or replace 
TRIGGER ALC_ALH_BUR2
 BEFORE UPDATE
 ON ALLOC_HEADER
 FOR EACH ROW
DECLARE


   L_alloc_no      ALLOC_HEADER.ALLOC_NO%TYPE;
   L_status        ALLOC_HEADER.STATUS%TYPE;
   L_alloc_id      ALC_ALLOC.ALLOC_ID%TYPE;
   L_cnt number;

   ---
   cursor C_ALC_XREF is
      select alloc_id
        from alc_xref
       where xref_alloc_no = L_alloc_no;
BEGIN
   if UPDATING then
      L_alloc_no := nvl(:new.alloc_no, :old.alloc_no);
      L_status   := :new.status;
      ---
      if L_status = 'C' then

         FOR rec IN C_ALC_XREF LOOP
            L_cnt := alloc_pkg.alloc_id.count+1;
            alloc_pkg.alloc_id(L_cnt) := rec.alloc_id;
         END LOOP;
      elsif L_status = 'A' then
         FOR rec IN C_ALC_XREF LOOP
            L_alloc_id  := rec.alloc_id;
            ---
            update ALC_ALLOC
               set status = '2' --Open Status for ALC_ALLOC
             where alloc_id    = L_alloc_id;
            ---
         END LOOP;
      end if;

   end if;
END;
/