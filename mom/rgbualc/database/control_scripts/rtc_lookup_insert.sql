 DECLARE
   L_owner                   VARCHAR2(30);
 BEGIN
     DELETE FROM RTC_LOOKUP_VALUES_TL WHERE LOOKUP_VALUE_ID IN (SELECT LOOKUP_VALUE_ID FROM RTC_LOOKUP_VALUES_B WHERE LOOKUP_TYPE_ID IN (SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'ALLOC')) ;
	 DELETE FROM RTC_LOOKUP_TYPES_TL WHERE LOOKUP_TYPE_ID IN (SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'ALLOC') ;
     DELETE
     FROM RTC_LOOKUP_VALUES_B
     WHERE LOOKUP_TYPE_ID IN
       (SELECT LOOKUP_TYPE_ID FROM RTC_LOOKUP_TYPES_B WHERE APPLICATION_ID = 'ALLOC');
       
     DELETE FROM RTC_LOOKUP_TYPES_B WHERE application_id = 'ALLOC';
     
     
     -- INSERTING into RTC_LOOKUP_TYPES_B (1)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1000,
         'Source',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (1-1)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1000,        
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source'),  --1000,
         'Source',
         'PO',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (1-2)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1001,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source'),  --1000,
         'Source',
         'Warehouse',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (1-3)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1002,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source'),  --1000,
         'Source',
         'What If',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (1-4)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3022,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source'),  --1000,
         'Source',
         'ASN',
         'Y',
         to_date('09-DEC-14','DD-MON-RR'),
         to_date('09-DEC-14','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (1-5)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3023,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source'),  --1000,
         'Source',
         'BOL',
         'Y',
         to_date('09-DEC-14','DD-MON-RR'),
         to_date('09-DEC-14','DD-MON-RR'),
         5,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (1-6)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3024,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source'),  --1000,
         'Source',
         'Transfer',
         'Y',
         to_date('09-DEC-14','DD-MON-RR'),
         to_date('09-DEC-14','DD-MON-RR'),
         6,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (1-7)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3025,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source'),  --1000,
         'Source',
         'Allocation',
         'Y',
         to_date('09-DEC-14','DD-MON-RR'),
         to_date('09-DEC-14','DD-MON-RR'),
         7,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (2)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1001,
         'Status',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-8)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1003,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Not Calculated',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         10,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-9)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1004,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Calculation Waiting',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         6,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-10)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1005,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Calculating',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-11)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1006,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Calculated',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-12)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1008,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Calculation Error',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         5,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-13)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1009,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Size Profile Calculation Error',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         13,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-14)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1010,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'IWOS Calculation Error',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         8,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-15)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1011,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Status Error',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         14,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-16)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1012,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Status Waiting',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         17,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-17)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1013,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Status Processing',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         16,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-18)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1014,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Status Processed',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         15,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-19)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1015,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Available Inventory Error',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-20)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1016,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Next Destination Error',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         9,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-21)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1017,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Supply Chain Error',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         18,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-22)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1019,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Scheduled',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         12,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (2-23)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1020,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Status'),  --1001,
         'Status',
         'Schedule Error',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         11,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (3)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1002,
         'Allocation Status',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (3-24)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1021,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Allocation Status'),  --1002,
         'Allocation Status',
         'Worksheet',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (3-25)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1022,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Allocation Status'),  --1002,
         'Allocation Status',
         'Submitted',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (3-26)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1023,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Allocation Status'),  --1002,
         'Allocation Status',
         'Reserved',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (3-27)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1024,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Allocation Status'),  --1002,
         'Allocation Status',
         'Approved',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (3-28)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1025,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Allocation Status'),  --1002,
         'Allocation Status',
         'Processed',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         5,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (3-29)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1026,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Allocation Status'),  --1002,
         'Allocation Status',
         'Closed',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         6,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (3-30)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1027,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Allocation Status'),  --1002,
         'Allocation Status',
         'Cancelled',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         7,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (3-31)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1028,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Allocation Status'),  --1002,
         'Allocation Status',
         'PO Created',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         8,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (3-32)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1029,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Allocation Status'),  --1002,
         'Allocation Status',
         'Scheduled',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         9,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (4)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1003,        
         'Size Profile Level',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (4-33)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1030,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Size Profile Level'),  --1003,
         'Size Profile Level',
         'Department',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (4-34)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1031,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Size Profile Level'),  --1003,
         'Size Profile Level',
         'Class',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (4-35)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1032,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Size Profile Level'),  --1003,
         'Size Profile Level',
         'Subclass',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (4-36)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1033,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Size Profile Level'),  --1003,
         'Size Profile Level',
         'Parent',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (4-37)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1034,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Size Profile Level'),  --1003,
         'Size Profile Level',
         'Parent/Diff',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         5,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B  (5)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1004,
         'Need Is',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (5-38)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1043,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Need Is'),  --1004,
         'Need Is',
         'Exact',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (5-39)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1044,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Need Is'),  --1004,
         'Need Is',
         'Proportional',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (6)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1005,
         'Mode',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (6-40)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1045,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Mode'),  --1005,
         'Mode',
         'Simple',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (6-41)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1047,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Mode'),  --1005,
         'Mode',
         'Pack Distribution',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (6-42)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1090,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Mode'),  --1005,
         'Mode',
         'Spread Demand',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (7)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1006,
         'Allocate To',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (7-43)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1048,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Allocate To'),  --1006,
         'Allocate To',
         'Net Need',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (7-44)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1049,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Allocate To'),  --1006,
         'Allocate To',
         'Gross Need',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (8)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1007,
         'Hierarchy',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (8-45)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1050,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Hierarchy'),  --1007,
         'Hierarchy',
         'Department',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (8-46)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1051,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Hierarchy'),  --1007,
         'Hierarchy',
         'Class',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (8-47)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1052,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Hierarchy'),  --1007,
         'Hierarchy',
         'Subclass',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (8-48)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1053,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Hierarchy'),  --1007,
         'Hierarchy',
         'Style',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (8-49)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1054,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Hierarchy'),  --1007,
         'Hierarchy',
         'Style/Diff',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         5,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (8-50)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1055,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Hierarchy'),  --1007,
         'Hierarchy',
         'Item',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         6,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (8-51)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3014,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Hierarchy'),  --1007,
         'Hierarchy',
         'Pack',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         11,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (9)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1008,
         'Demand Source',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (9-52)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1035,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Demand Source'),  --1008,
         'Demand Source',
         'History',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (9-53)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1036,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Demand Source'),  --1008,
         'Demand Source',
         'Forecast',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (9-54)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1037,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Demand Source'),  --1008,
         'Demand Source',
         'Plan',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (9-55)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1038,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Demand Source'),  --1008,
         'Demand Source',
         'Plan/History',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (9-56)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1039,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Demand Source'),  --1008,
         'Demand Source',
         'Plan Re-Project',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         5,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (9-57)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1040,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Demand Source'),  --1008,
         'Demand Source',
         'Receipt Plan',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         6,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (9-58)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1041,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Demand Source'),  --1008,
         'Demand Source',
         'Corporate Rules',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         7,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (9-59)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1042,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Demand Source'),  --1008,
         'Demand Source',
         'Manual',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         8,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (10)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1009,
         'Hierarchy Type',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (10-60)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1057,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Hierarchy Type'),  --1009,
         'Hierarchy Type',
         'Hierarchy',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (10-61)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1058,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Hierarchy Type'),  --1009,
         'Hierarchy Type',
         'Alternate Hierarchy',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (11)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1010,
         'Store Size Logic Type',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (11-62)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1059,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Store Size Logic Type'),  --1010,
         'Store Size Logic Type',
         'Size Profile',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (11-63)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1061,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Store Size Logic Type'),  --1010,
         'Store Size Logic Type',
         'Selling Curve',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (12)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1011,
         'Policy Date Range Type',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (12-64)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1062,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Policy Date Range Type'),  --1011,
         'Policy Date Range Type',
         'Start/End Week Ending Dates:',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (12-65)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1063,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Policy Date Range Type'),  --1011,
         'Policy Date Range Type',
         'Weeks From Today:',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (13)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1012,
         'Policy Fulfillement Type',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (13-66)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1064,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Policy Fulfillement Type'),  --1012,
         'Policy Fulfillement Type',
         'On Order Commit Date',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (13-67)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1065,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Policy Fulfillement Type'),  --1012,
         'Policy Fulfillement Type',
         'Weeks From Today',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (13-68)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1107,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Policy Fulfillement Type'),  --1012,
         'Policy Fulfillement Type',
         'Date Range',
         'Y',
         to_date('10-MAR-14','DD-MON-RR'),
         to_date('10-MAR-14','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (14)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1013,
         'QLimits Item Level',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (14-69)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1066,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'QLimits Item Level'),  --1013,
         'QLimits Item Level',
         'Department',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (14-70)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1067,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'QLimits Item Level'),  --1013,
         'QLimits Item Level',
         'Class',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (14-71)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1068,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'QLimits Item Level'),  --1013,
         'QLimits Item Level',
         'Subclass',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (14-72)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1069,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'QLimits Item Level'),  --1013,
         'QLimits Item Level',
         'Item',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (15)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1014,
         'QLimits Loc Aggregate',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (15-73)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1070,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'QLimits Loc Aggregate'),  --1014,
         'QLimits Loc Aggregate',
         'Store',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (15-74)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1071,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'QLimits Loc Aggregate'),  --1014,
         'QLimits Loc Aggregate',
         'Group',
         'Y',
         to_date('22-OCT-12','DD-MON-RR'),
         to_date('22-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (16)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1015,
         'Group Type',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (16-75)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1072,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Group Type'),  --1015,
         'Group Type',
         'Location List',
         'Y',
         to_date('02-NOV-12','DD-MON-RR'),
         to_date('02-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (16-76)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1073,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Group Type'),  --1015,
         'Group Type',
         'Location Trait',
         'Y',
         to_date('02-NOV-12','DD-MON-RR'),
         to_date('02-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (16-77)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1074,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Group Type'),  --1015,
         'Group Type',
         'Store Grade',
         'Y',
         to_date('02-NOV-12','DD-MON-RR'),
         to_date('02-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (16-78)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1075,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Group Type'),  --1015,
         'Group Type',
         'Allocation Group',
         'Y',
         to_date('02-NOV-12','DD-MON-RR'),
         to_date('02-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (16-79)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1076,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Group Type'),  --1015,
         'Group Type',
         'Single Store',
         'Y',
         to_date('02-NOV-12','DD-MON-RR'),
         to_date('02-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (16-80)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1077,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Group Type'),  --1015,
         'Group Type',
         'All Stores',
         'Y',
         to_date('02-NOV-12','DD-MON-RR'),
         to_date('02-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (16-81)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2000,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Group Type'),  --1015,
         'Group Type',
         'Single Warehouse',
         'Y',
         to_date('07-JULY-14','DD-MON-RR'),
         to_date('07-JULY-14','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (16-82)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3000,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Group Type'),  --1015,
         'Group Type',
         'All Warehouses',
         'Y',
         to_date('07-JULY-14','DD-MON-RR'),
         to_date('07-JULY-14','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (17)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1016,
         'Alternate Hierarchy Date Range',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (17-83)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1078,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Alternate Hierarchy Date Range'),  --1016,
         'Alternate Hierarchy Date Range',
         'Weight Percentage',
         'Y',
         to_date('05-NOV-12','DD-MON-RR'),
         to_date('05-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (17-84)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1079,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Alternate Hierarchy Date Range'),  --1016,
         'Alternate Hierarchy Date Range',
         'Start and End Date',
         'Y',
         to_date('05-NOV-12','DD-MON-RR'),
         to_date('05-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (17-85)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1080,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Alternate Hierarchy Date Range'),  --1016,
         'Alternate Hierarchy Date Range',
         'Weeks From Today',
         'Y',
         to_date('05-NOV-12','DD-MON-RR'),
         to_date('05-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (18)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1017,
         'Weight Percent Date Range Type',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (18-86)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1081,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Weight Percent Date Range Type'),  --1017,
         'Weight Percent Date Range Type',
         'Weight %',
         'Y',
         to_date('07-NOV-12','DD-MON-RR'),
         to_date('07-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (18-87)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1082,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Weight Percent Date Range Type'),  --1017,
         'Weight Percent Date Range Type',
         'Weight % Start and End Date',
         'Y',
         to_date('07-NOV-12','DD-MON-RR'),
         to_date('07-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (18-88)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1083,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Weight Percent Date Range Type'),  --1017,
         'Weight Percent Date Range Type',
         'Weight % Weeks From Today',
         'Y',
         to_date('07-NOV-12','DD-MON-RR'),
         to_date('07-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (19)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1018,
         'Location Aggr Type',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (19-89)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1084,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Location Aggr Type'),  --1018,
         'Location Aggr Type',
         'Store',
         'Y',
         to_date('15-NOV-12','DD-MON-RR'),
         to_date('15-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (19-90)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1085,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Location Aggr Type'),  --1018,
         'Location Aggr Type',
         'Group',
         'Y',
         to_date('15-NOV-12','DD-MON-RR'),
         to_date('15-NOV-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (20)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1019,
         'Store Calc Multiple',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (20-91)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1086,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Store Calc Multiple'),  --1019,
         'Store Calc Multiple',
         'Each',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (20-92)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1087,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Store Calc Multiple'),  --1019,
         'Store Calc Multiple',
         'Inner',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (20-93)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1088,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Store Calc Multiple'),  --1019,
         'Store Calc Multiple',
         'Case',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (20-94)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1089,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Store Calc Multiple'),  --1019,
         'Store Calc Multiple',
         'Pallet',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (21)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1020,
         'Purchase Order Type',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (21-95)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1092,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Purchase Order Type'),  --1020,
         'Purchase Order Type',
         'Bulk',
         'Y',
         to_date('21-DEC-12','DD-MON-RR'),
         to_date('21-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (21-96)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1093,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Purchase Order Type'),  --1020,
         'Purchase Order Type',
         'Warehouse',
         'Y',
         to_date('21-DEC-12','DD-MON-RR'),
         to_date('21-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (21-97)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1094,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Purchase Order Type'),  --1020,
         'Purchase Order Type',
         'Cross dock',
         'Y',
         to_date('21-DEC-12','DD-MON-RR'),
         to_date('21-DEC-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (21-98)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1095,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Purchase Order Type'),  --1020,
         'Purchase Order Type',
         'Direct to location',
         'Y',
         to_date('21-DEC-12','DD-MON-RR'),
         to_date('21-DEC-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (22)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1021,
         'Pack Rounding',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (22-99)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1096,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Pack Rounding'),  --1021,
         'Pack Rounding',
         '0.25',
         'Y',
         to_date('31-DEC-12','DD-MON-RR'),
         to_date('31-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (22-100)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1097,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Pack Rounding'),  --1021,
         'Pack Rounding',
         '0.50',
         'Y',
         to_date('31-DEC-12','DD-MON-RR'),
         to_date('31-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (22-101)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1098,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Pack Rounding'),  --1021,
         'Pack Rounding',
         '0.75',
         'Y',
         to_date('31-DEC-12','DD-MON-RR'),
         to_date('31-DEC-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (22-102)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1099,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Pack Rounding'),  --1021,
         'Pack Rounding',
         '1.0',
         'Y',
         to_date('31-DEC-12','DD-MON-RR'),
         to_date('31-DEC-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (23)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1022,
         'ItemSourceTier',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (23-103)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1100,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'ItemSourceTier'),  --1022,
         'ItemSourceTier',
         'T1',
         'Y',
         to_date('01-MAR-12','DD-MON-RR'),
         to_date('31-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (23-104)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1101,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'ItemSourceTier'),  --1022,
         'ItemSourceTier',
         'T2',
         'Y',
         to_date('31-MAR-12','DD-MON-RR'),
         to_date('31-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (23-105)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1102,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'ItemSourceTier'),  --1022,
         'ItemSourceTier',
         'T3',
         'Y',
         to_date('31-MAR-12','DD-MON-RR'),
         to_date('31-DEC-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (24)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1023,
         'POSourceTier',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (24-106)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1103,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'POSourceTier'),  --1023,
         'POSourceTier',
         'T1',
         'Y',
         to_date('01-MAR-12','DD-MON-RR'),
         to_date('31-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (24-107)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1104,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'POSourceTier'),  --1023,
         'POSourceTier',
         'T2',
         'Y',
         to_date('31-MAR-12','DD-MON-RR'),
         to_date('31-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (25)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --1024,
         'Ideal Weeks Of Supply',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (25-108)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1105,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Ideal Weeks Of Supply'),  --1024,
         'Ideal Weeks Of Supply',
         'Weeks',
         'Y',
         to_date('18-APR-13','DD-MON-RR'),
         to_date('18-APR-13','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (25-109)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --1106,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Ideal Weeks Of Supply'),  --1024,
         'Ideal Weeks Of Supply',
         'Use Table',
         'Y',
         to_date('18-APR-13','DD-MON-RR'),
         to_date('18-APR-13','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (26)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2001,
         'AlcBooleanTrueFalse',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (26-110)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2001,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcBooleanTrueFalse'),  --2001,
         'AlcBooleanTrueFalse',
         'TRUE',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (26-111)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2002,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcBooleanTrueFalse'),  --2001,
         'AlcBooleanTrueFalse',
         'FALSE',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (27)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2002,
         'AlcBooleanYesNo',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (27-112)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2003,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcBooleanYesNo'),  --2002,
         'AlcBooleanYesNo',
         'YES',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (27-113)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2004,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcBooleanYesNo'),  --2002,
         'AlcBooleanYesNo',
         'NO',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (28)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2003,
         'AlcDaysOfWeek',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (28-114)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2005,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcDaysOfWeek'),  --2003,
         'AlcDaysOfWeek',
         'SUNDAY',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (28-115)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2006,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcDaysOfWeek'),  --2003,
         'AlcDaysOfWeek',
         'MONDAY',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (28-116)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2007,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcDaysOfWeek'),  --2003,
         'AlcDaysOfWeek',
         'TUESDAY',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (28-117)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2008,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcDaysOfWeek'),  --2003,
         'AlcDaysOfWeek',
         'WEDNESDAY',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (28-118)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2009,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcDaysOfWeek'),  --2003,
         'AlcDaysOfWeek',        
         'THURSDAY',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         5,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (28-119)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2010,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcDaysOfWeek'),  --2003,
         'AlcDaysOfWeek',
         'FRIDAY',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         6,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (28-120)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2011,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcDaysOfWeek'),  --2003,
         'AlcDaysOfWeek',
         'SATURDAY',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         7,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (29)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2004,
         'AlcDefaultActions',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (29-121)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2012,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcDefaultActions'),  --2004,
         'AlcDefaultActions',
         'CREATE',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (29-122)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2013,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcDefaultActions'),  --2004,
         'AlcDefaultActions',
         'UPDATE',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (30)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2005,
         'AlcLoggerLevels',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (30-123)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2014,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcLoggerLevels'),  --2005,
         'AlcLoggerLevels',
         'ERROR',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (30-124)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2015,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcLoggerLevels'),  --2005,
         'AlcLoggerLevels',
         'DEBUG',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (30-125)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2016,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcLoggerLevels'),  --2005,
         'AlcLoggerLevels',
         'WARN',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (30-126)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2017,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcLoggerLevels'),  --2005,
         'AlcLoggerLevels',
         'INFO',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (31)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2006,
         'AlcRuleVisibility',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (31-127)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2018,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcRuleVisibility'),  --2006,
         'AlcRuleVisibility',
         'PLAN',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (31-128)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2019,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcRuleVisibility'),  --2006,
         'AlcRuleVisibility',
         'FORECAST',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (32)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2007,
         'AlcTradeItemUnits',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (32-129)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2020,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcTradeItemUnits'),  --2007,
         'AlcTradeItemUnits',
         'EA',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (32-130)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2021,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcTradeItemUnits'),  --2007,
         'AlcTradeItemUnits',
         'IN',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (32-131)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2022,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcTradeItemUnits'),  --2007,
         'AlcTradeItemUnits',
         'CA',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (32-132)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2023,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcTradeItemUnits'),  --2007,
         'AlcTradeItemUnits',
         'PA',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (33)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2008,
         'AlcMLDLevels',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (33-133)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2024,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcMLDLevels'),  --2008,
         'AlcMLDLevels',
         'MLD 0',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (33-134)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2025,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcMLDLevels'),  --2008,
         'AlcMLDLevels',
         'MLD 1',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (33-135)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2026,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcMLDLevels'),  --2008,
         'AlcMLDLevels',
         'MLD 2',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (34)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2009,
         'AlcQLDistributionMethods',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (34-136)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2027,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcQLDistributionMethods'),  --2009,
         'AlcQLDistributionMethods',
         'COPY',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (34-137)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2028,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcQLDistributionMethods'),  --2009,
         'AlcQLDistributionMethods',
         'SPREAD',
         'Y',
         to_date('11-DEC-12','DD-MON-RR'),
         to_date('11-DEC-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (35)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2029,
         'Source Alloc Quick Create',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (35-138)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2029,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source Alloc Quick Create'),  --2029,
         'Source Alloc Quick Create',
         'PO',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (35-139)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2030,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source Alloc Quick Create'),  --2029,
         'Source Alloc Quick Create',
         'WAREHOUSE',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (35-140)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2031,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source Alloc Quick Create'),  --2029,
         'Source Alloc Quick Create',
         'ASN',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (35-141)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2032,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source Alloc Quick Create'),  --2029,
         'Source Alloc Quick Create',
         'WHAT IF',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (35-142)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2033,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source Alloc Quick Create'),  --2029,
         'Source Alloc Quick Create',
         'TRANSFER',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         5,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (35-143)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2034,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source Alloc Quick Create'),  --2029,
         'Source Alloc Quick Create',
         'BOL',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         6,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (35-144)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2043,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Source Alloc Quick Create'),  --2029,
         'Source Alloc Quick Create',
         'ALLOCATION',
         'Y',
         to_date('07-JUL-14','DD-MON-RR'),
         to_date('07-JUL-14','DD-MON-RR'),
         7,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (36)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2030,
         'Next Alloc Quick Create',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (36-145)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2035,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Next Alloc Quick Create'),  --2030,
         'Next Alloc Quick Create',
         'Allocation Maintenance',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (36-146)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2036,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Next Alloc Quick Create'),  --2030,
         'Next Alloc Quick Create',
         'Worksheet',
         'Y',
         to_date('23-OCT-12','DD-MON-RR'),
         to_date('23-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (37)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2031,
         'AlcFrequency',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (37-147)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2037,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcFrequency'),  --2031,
         'AlcFrequency',
         'Weekly',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (37-148)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2038,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcFrequency'),  --2031,
         'AlcFrequency',
         'Biweekly',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (38)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --2032,
         'AlcScheduleAction',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (38-149)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2039,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcScheduleAction'),  --2032,
         'AlcScheduleAction',
         'Worksheet',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (38-150)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2040,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcScheduleAction'),  --2032,
         'AlcScheduleAction',
         'Submitted',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (38-151)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2041,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcScheduleAction'),  --2032,
         'AlcScheduleAction',
         'Reserved',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (38-152)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --2042,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AlcScheduleAction'),  --2032,
         'AlcScheduleAction',
         'Approved',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (39)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --3001,
         'MLD Demand Source',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (39-153)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3001,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'MLD Demand Source'),  --3001,
         'MLD Demand Source',
         'Forecast',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-29','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (39-154)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3002,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'MLD Demand Source'),  --3001,
         'MLD Demand Source',
         'History',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-29','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (39-155)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3003,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'MLD Demand Source'),  --3001,
         'MLD Demand Source',
         'Plan',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-29','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (40)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         APPLICATION_ID,
         BU_ID,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --3002,
         'Rule Level On Hand',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (40-156)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3011,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Rule Level On Hand'),  --3002,
         'Rule Level On Hand',
         'Do Not Use',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-29','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (40-157)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3012,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Rule Level On Hand'),  --3002,
         'Rule Level On Hand',
         'Snap Shot',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-29','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (40-158)
     INSERT
     INTO RTC_LOOKUP_VALUES_B
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3013,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Rule Level On Hand'),  --3002,
         'Rule Level On Hand',
         'Real Time',
         'Y',
         to_date('15-OCT-12','DD-MON-RR'),
         to_date('15-OCT-29','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (41)
     INSERT
     INTO RTC_LOOKUP_TYPES_B
       (
         Lookup_Type_Id,
         Lookup_Type,
         Created_By,
         Creation_Date,
         Last_Updated_By,
         Last_Update_Date,
         Last_Update_Login,
         Application_Id,
         Bu_Id,
         Object_Version_Number
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --3003,
         'AllocType',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (41-159)
     INSERT
     INTO Rtc_Lookup_Values_b
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3015,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AllocType'),  --3003,
         'AllocType',
         'Fashion Group',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (41-160)
     INSERT
     INTO Rtc_Lookup_Values_b
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3016,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AllocType'),  --3003,
         'AllocType',
         'Fashion',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (41-161)
     INSERT
     INTO Rtc_Lookup_Values_b
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3017,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AllocType'),  --3003,
         'AllocType',
         'Staple',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (41-162)
     INSERT
     INTO Rtc_Lookup_Values_b
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3018,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AllocType'),  --3003,
         'AllocType',
         'Fashion Pack',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         4,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_TYPES_B (42)
     INSERT
     INTO Rtc_Lookup_Types_B
       (
         Lookup_Type_Id,
         Lookup_Type,
         Created_By,
         Creation_Date,
         Last_Updated_By,
         Last_Update_Date,
         Last_Update_Login,
         Application_Id,
         Bu_Id,
         Object_Version_Number
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, --3004,
         'AllocFlow',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (42-163)
     INSERT
     INTO Rtc_Lookup_Values_b
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3019,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AllocFlow'),  --3004,
         'AllocFlow',
         'STANDARD',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (42-164)
     INSERT
     INTO Rtc_Lookup_Values_b
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3020,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AllocFlow'),  --3004,
         'AllocFlow',
         'WHAT-IF',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         2,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
     -- INSERTING into RTC_LOOKUP_VALUES_B (42-165)
     INSERT
     INTO Rtc_Lookup_Values_b
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3021,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'AllocFlow'),  --3004,
         'AllocFlow',
         'SCHEDULED',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         3,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );     


     -- INSERTING into RTC_LOOKUP_TYPES_B 
     INSERT
     INTO Rtc_Lookup_Types_B
       (
         Lookup_Type_Id,
         Lookup_Type,
         Created_By,
         Creation_Date,
         Last_Updated_By,
         Last_Update_Date,
         Last_Update_Login,
         Application_Id,
         Bu_Id,
         Object_Version_Number
       )
       VALUES
       (
         RTC_LOOKUP_TYPES_B_S1.NEXTVAL, 
         'Store Size Profile Ratio',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         'ALLOC',
         NULL,
         1
       );


     -- INSERTING into RTC_LOOKUP_VALUES_B 
     INSERT
     INTO Rtc_Lookup_Values_b
       (
         LOOKUP_VALUE_ID,
         LOOKUP_TYPE_ID,
         LOOKUP_TYPE,
         LOOKUP_CODE,
         ENABLED_FLAG,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         DISPLAY_SEQUENCE,
         CREATED_BY,
         CREATION_DATE,
         LAST_UPDATED_BY,
         LAST_UPDATE_DATE,
         LAST_UPDATE_LOGIN,
         TAG,
         REQUIRED,
         OBJECT_VERSION_NUMBER
       )
       VALUES
       (
         RTC_LOOKUP_VALUES_B_S1.NEXTVAL, --3019,
         (select lookup_type_id from RTC_LOOKUP_TYPES_B where application_id = 'ALLOC' and lookup_type = 'Store Size Profile Ratio'),  
         'Store Size Profile Ratio',
         'Hierarchy',
         'Y',
         to_date('26-SEP-12','DD-MON-RR'),
         to_date('26-SEP-12','DD-MON-RR'),
         1,
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         NULL,
         NULL,
         NULL,
         1
       );
 
     COMMIT;   
 END;
/