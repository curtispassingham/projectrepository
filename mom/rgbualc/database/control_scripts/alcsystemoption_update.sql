SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------
-- Copyright (c) 2014, Oracle Retail  All rights reserved.
-- $Workfile:   000223_alcsizeprofile_update.sql  $
--------------------------------------------------------

----------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--  This script will update existing entries in the ALC_SYSTEM_OPTIONS table for
--  TP_NOTIFICATION_POLL_INTERVAL
--  TP_NOTIFICATION_POLL_TIMEOUT
------------------------------------------------------------------------

PROMPT Updating 'ALC_SYSTEM_OPTIONS'

UPDATE ALC_SYSTEM_OPTIONS
   SET TP_NOTIFICATION_POLL_INTERVAL=90000,
       TP_NOTIFICATION_POLL_TIMEOUT=600000
/

commit
/

