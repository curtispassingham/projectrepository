DECLARE
   L_facet_cfg_id            NUMBER;
   L_facet_item_loc_cfg_id   NUMBER;	   
   L_owner                   VARCHAR2(30);
   
   
BEGIN
        
   select table_owner           
     into L_owner  
    from system_options; 
    
    
   DELETE FROM RAF_FACET_SESSION_ATTRIBUTE where facet_attribute_cfg_id in  
      (select facet_attribute_cfg_id from raf_facet_attribute_cfg where facet_cfg_id in 
         (select facet_cfg_id from RAF_FACET_CFG where query_name in ('ALC_WORK_ITEM_SOURCE','ALC_ITEM_LOC')));    
           
   DELETE FROM RAF_FACET_SESSION where facet_cfg_id in 
      (select facet_cfg_id From RAF_FACET_CFG where query_name in ('ALC_WORK_ITEM_SOURCE','ALC_ITEM_LOC'));     
    
   
   DELETE FROM raf_facet_attribute_cfg_tl 
      where facet_attribute_cfg_id in 
         (select facet_attribute_cfg_id 
            from raf_facet_attribute_cfg
           where facet_cfg_id in 
             (select facet_cfg_id 
                from raf_facet_cfg
               where query_name in ('ALC_WORK_ITEM_SOURCE','ALC_ITEM_LOC')));
    
   DELETE FROM raf_facet_attribute_cfg 
      where facet_cfg_id in 
         (select facet_cfg_id 
            from raf_facet_cfg 
           where query_name in ('ALC_WORK_ITEM_SOURCE','ALC_ITEM_LOC'));                                 
     
   DELETE FROM raf_facet_cfg 
      where query_name in ('ALC_WORK_ITEM_SOURCE','ALC_ITEM_LOC');

   select RAF_FACET_CFG_SEQ.NEXTVAL
     into L_facet_cfg_id
     from dual;
   
   -- INSERTING into RAF_FACET_CFG
   insert into 
      raf_facet_cfg (
         facet_cfg_id, 
         query_name, 
         facet_cfg_function,
         created_by, 
         updated_by,
         created_date,
         updated_date)
   select L_facet_cfg_id,     
          'ALC_WORK_ITEM_SOURCE', 
          'ALC_HUB_FACET_CFG_SQL',       
          USER,
          USER,
          SYSDATE,
          SYSDATE
     from dual;
    
    
   -- INSERTING into RAF_FACET_ATTRIBUTE_CFG
   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL,
         L_facet_cfg_id,     
         'PARENT',
         1,
         NULL,
         NULL,
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'Style',
         'STYLE DESCRIPTION',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;
         
    
   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_cfg_id,     
         'PARENTDIFF',
         2,
         NULL,
         NULL,
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'Style/Color',
         'STYLECOLOR DESCRIPTION',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;
    
   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_cfg_id,     
         'PACK',
         3,
         NULL,
         NULL,
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'PACK',
         'PACK DESCRIPTION',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;
    
   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_cfg_id,     
         'SKU',
         4,
         NULL,
         NULL,
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'SKU',
         'SKU DESCRIPTION',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;
    
   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_cfg_id,     
         'WH',
         5,
         NULL,
         NULL,
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'Warehouse',
         'Warehouse DESCRIPTION',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;
    
   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_cfg_id,     
         'SOURCE_TYPE',
         6,
         NULL,
         NULL,
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'Inventory',
         'SOURCE_TYPE DESCRIPTION',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;
    
   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_cfg_id,     
         'DOC_NO',
         7,
         (select facet_attribute_cfg_id
            from raf_facet_attribute_cfg
           where facet_cfg_id = L_facet_cfg_id
             and attribute_name = 'SOURCE_TYPE'
             and rownum = 1),
         'PO',
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'PO',
         'PO DESCRIPTION',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;
     
   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_cfg_id,     
         'DOC_NO',
         8,
         (select facet_attribute_cfg_id
            from raf_facet_attribute_cfg
           where facet_cfg_id = L_facet_cfg_id
             and attribute_name = 'SOURCE_TYPE'
             and rownum = 1),
         'ASN',
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values ( 
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'ASN',
         'ASN DESCRIPTION',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;
    
   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_cfg_id,     
         'DOC_NO',
         9,
         (select facet_attribute_cfg_id
            from raf_facet_attribute_cfg
           where facet_cfg_id = L_facet_cfg_id
             and attribute_name = 'SOURCE_TYPE'
             and rownum = 1),
         'BOL',
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'BOL',
         'BOL DESCRIPTION',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;
    
   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_cfg_id,     
         'DOC_NO',
         10,
         (select facet_attribute_cfg_id
            from raf_facet_attribute_cfg
           where facet_cfg_id = L_facet_cfg_id
             and attribute_name = 'SOURCE_TYPE'
             and rownum = 1),
         'TSF',
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'TSF',
         'TSF DESCRIPTION',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;

   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_cfg_id,     
         'WH',
         11,
         (select facet_attribute_cfg_id
            from raf_facet_attribute_cfg
           where facet_cfg_id = L_facet_cfg_id
             and attribute_name = 'SOURCE_TYPE'
             and rownum = 1),
         'OH',
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'On Hand',
         'OH DESCRIPTION',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;

   -- Adding Facet Data for RAF Facets
   select RAF_FACET_CFG_SEQ.NEXTVAL
     into L_facet_item_loc_cfg_id
     from dual;

   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_cfg_id,     
         'DOC_NO',
         12,
         (select facet_attribute_cfg_id
            from raf_facet_attribute_cfg
           where facet_cfg_id = L_facet_cfg_id
             and attribute_name = 'SOURCE_TYPE'
             and rownum = 1),
         'ALLOC',
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'Allocation',
         'ALLOC DESCRIPTION',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;


   -- INSERTING into RAF_FACET_CFG
   insert into 
      raf_facet_cfg (
         facet_cfg_id, 
         query_name, 
         facet_cfg_function,
         created_by, 
         updated_by,
         created_date,
         updated_date)
      select L_facet_item_loc_cfg_id,     
         'ALC_ITEM_LOC', 
         'ALC_ITEM_LOC_FACET_SQL',       
         USER,
         USER,
         SYSDATE,
         SYSDATE
      from dual;
  
  
   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_item_loc_cfg_id,     
         'LOC_LIST',
         12,
         null,
         null,
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values ( 
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'Location List',
         'Location List',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;  

   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_item_loc_cfg_id,     
         'LOC_TRAIT',
         13,
         null,
         null,
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values ( 
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'Location Trait',
         'Location Trait',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;

   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_item_loc_cfg_id,     
         'STORE_GRADE',
         14,
         null,
         null,
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'Store Grade',
         'Store Grade',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;

   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_item_loc_cfg_id,     
         'ALLOCATION_GROUP',
         15,
         null,
         null,
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'Allocation Group',
         'Allocation Group',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;

   insert all
      into raf_facet_attribute_cfg (
         facet_attribute_cfg_id, 
         facet_cfg_id, 
         attribute_name, 
         seq_name, 
         parent_facet_attr_cfg_id,
         parent_attribute_value,    
         created_by,
         updated_by, 
         created_date,
         updated_date)
      values (
         RAF_FACET_ATTRIBUTE_CFG_SEQ.NEXTVAL, 
         L_facet_item_loc_cfg_id,     
         'STANDALONE_STORE',
         16,
         null,
         null,
         USER,
         USER,
         SYSDATE,
         SYSDATE)
      into raf_facet_attribute_cfg_tl (
         facet_attribute_cfg_id,
         language,
         meaning,
         description,
         source_lang,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date,
         last_update_login,
         object_version_number) 
      values ( 
         RAF_FACET_ATTRIBUTE_CFG_SEQ.CURRVAL,
         'US',
         'Standalone Store',
         'Standalone Store',
         'US',
         USER,
         SYSDATE,
         USER,
         SYSDATE,
         null,
         null)
      select * from dual;

   COMMIT;

END;
/