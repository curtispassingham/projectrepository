--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------
whenever sqlerror exit

--------------------------------------
--       Modifying Table
--------------------------------------

PROMPT DROPPING COMPRESSION ON 'PS_TXN'
ALTER TABLE PS_TXN NOCOMPRESS
/
