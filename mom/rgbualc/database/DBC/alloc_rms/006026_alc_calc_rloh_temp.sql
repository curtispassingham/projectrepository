--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  TABLE MODIFIED:             ALC_CALC_RLOH_TEMP
----------------------------------------------------------------------------

whenever sqlerror exit
----------------------------------------------------------------------------
--       CHANGING TO A GLOBAL TEMP TABLE
----------------------------------------------------------------------------
PROMPT DROPPING ALC_CALC_RLOH_TEMP TABLE
DECLARE
  L_table_calcrloh_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_calcrloh_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_CALC_RLOH_TEMP';

  if (L_table_calcrloh_exists != 0) then
      execute immediate 'DROP TABLE ALC_CALC_RLOH_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT RE-CREATING THE TABLE ALC_CALC_RLOH_TEMP AS GTT TABLE
CREATE GLOBAL TEMPORARY TABLE ALC_CALC_RLOH_TEMP
(
  ALLOC_ID      NUMBER(15),
  ITEM          VARCHAR2(25),
  LOC           NUMBER(10),
  PACK_IND      VARCHAR2(1),
  ITEM_LEVEL    NUMBER(1),
  TRAN_LEVEL    NUMBER(1),
  CURR_AVAIL    NUMBER(13,4),
  FUTURE_AVAIL  NUMBER(13,4)
)
ON COMMIT DELETE ROWS
NOCACHE;

COMMENT ON TABLE ALC_CALC_RLOH_TEMP IS 'Temporary table used in the calculation process.  This table holds information about RLOH inventory positions when rule level on hand is being used.';

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.ALLOC_ID IS 'The allocation being calculated.';

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.ITEM IS 'The item to consider in the RLOH calculation.';

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.LOC IS 'The location to consider in the RLOH calculation.';

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.PACK_IND IS 'The pack indicator of the item.';

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.ITEM_LEVEL IS 'The item level of the item.';

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.TRAN_LEVEL IS 'The tran level of the item.';

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.CURR_AVAIL IS 'The current inventory of the item/location.';

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.FUTURE_AVAIL IS 'The future inventory of the item/location.  Based on the on order commit values from ALC_RULE.';
/