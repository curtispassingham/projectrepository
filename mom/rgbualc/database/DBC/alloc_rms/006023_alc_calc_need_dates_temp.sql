--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE MODIFIED:				ALC_CALC_NEED_DATES_TEMP
----------------------------------------------------------------------------

whenever sqlerror exit

----------------------------------------------------------------------------
--       CHANGING TO A GLOBAL TEMP TABLE
----------------------------------------------------------------------------
PROMPT DROPPING ALC_CALC_NEED_DATES_TEMP TABLE
DECLARE
  L_table_dates_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_dates_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_CALC_NEED_DATES_TEMP';

  if (L_table_dates_exists != 0) then
      execute immediate 'DROP TABLE ALC_CALC_NEED_DATES_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT RE-CREATING THE TABLE ALC_CALC_NEED_DATES_TEMP AS GTT TABLE
CREATE GLOBAL TEMPORARY TABLE ALC_CALC_NEED_DATES_TEMP
(
  ALLOC_ID             NUMBER(15),
  RULE_MANY_TO_ONE_ID  NUMBER(20),
  EOW_DATE             DATE,
  WEIGHT               NUMBER(12,4)
)
ON COMMIT DELETE ROWS
NOCACHE;

COMMENT ON TABLE ALC_CALC_NEED_DATES_TEMP IS 'Temporary table used in the calculation process.  This table holds information about the dates that are to be used when looking up need.';

COMMENT ON COLUMN ALC_CALC_NEED_DATES_TEMP.ALLOC_ID IS 'The allocation being calculated.';

COMMENT ON COLUMN ALC_CALC_NEED_DATES_TEMP.RULE_MANY_TO_ONE_ID IS 'When alternative hierarchies are being use, the ID of the alternate hierarchy row.  When alternative hierarchies are not being used, this will be -1.';

COMMENT ON COLUMN ALC_CALC_NEED_DATES_TEMP.EOW_DATE IS 'The end of week date to consider need on.';

COMMENT ON COLUMN ALC_CALC_NEED_DATES_TEMP.WEIGHT IS 'The weight to apply to the EOW_DATE.';
/
