--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE ADDED:				ALC_CALC_SOURCE_TEMP
----------------------------------------------------------------------------

whenever sqlerror exit
----------------------------------------------------------------------------
--       CHANGING TO A GLOBAL TEMP TABLE
----------------------------------------------------------------------------
PROMPT DROPPING ALC_CALC_SOURCE_TEMP TABLE
DECLARE
  L_table_source_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_source_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_CALC_SOURCE_TEMP';

  if (L_table_source_exists != 0) then
      execute immediate 'DROP TABLE ALC_CALC_SOURCE_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT RE-CREATING THE TABLE ALC_CALC_SOURCE_TEMP AS GTT TABLE
CREATE GLOBAL TEMPORARY TABLE ALC_CALC_SOURCE_TEMP
(
  ALLOC_ID           NUMBER(15),
  ITEM_SOURCE_ID     NUMBER(20),
  RELEASE_DATE       DATE,
  ITEM_TYPE          VARCHAR2(10),
  SOURCE_ITEM        VARCHAR2(25),
  SOURCE_ITEM_LEVEL  NUMBER(1),
  SOURCE_TRAN_LEVEL  NUMBER(1),
  SOURCE_PACK_IND    VARCHAR2(1),
  SOURCE_DIFF1_ID    VARCHAR2(10),
  SOURCE_DIFF2_ID    VARCHAR2(10),
  SOURCE_DIFF3_ID    VARCHAR2(10),
  SOURCE_DIFF4_ID    VARCHAR2(10),
  TRAN_ITEM          VARCHAR2(25),
  TRAN_ITEM_LEVEL    NUMBER(1),
  TRAN_TRAN_LEVEL    NUMBER(1),
  TRAN_PACK_IND      VARCHAR2(1),
  TRAN_DIFF1_ID      VARCHAR2(10),
  TRAN_DIFF2_ID      VARCHAR2(10),
  TRAN_DIFF3_ID      VARCHAR2(10),
  TRAN_DIFF4_ID      VARCHAR2(10),
  DEPT               NUMBER(4),
  CLASS              NUMBER(4),
  SUBCLASS           NUMBER(4),
  PACK_NO            VARCHAR2(25)
)
ON COMMIT DELETE ROWS
NOCACHE;

COMMENT ON TABLE ALC_CALC_SOURCE_TEMP IS 'Temporary table used in the calculation process.  This table holds information about the data in the ALC_ITEM_SOURCE table.  It explodes the various item types contained on ALC_ITEM_SOURCE down to the transaction level items that they cover.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.ALLOC_ID IS 'The allocation being calculated.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.ITEM_SOURCE_ID IS 'The ALC_ITEM_SOURCE reference.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.RELEASE_DATE IS 'The release date from the ALC_ITEM_SOURCE record.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.ITEM_TYPE IS 'The item_type from ALC_ITEM_SOURCE.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_ITEM IS 'The parent level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent item, it will be populated here.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_ITEM_LEVEL IS 'The item level of the item in the SOURCE_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_TRAN_LEVEL IS 'The tran level of the item in the SOURCE_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_PACK_IND IS 'The pack indicator of the item in the SOURCE_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_DIFF1_ID IS 'The DIFF_1 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_1_AGGREGATE_IND is Y.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_DIFF2_ID IS 'The DIFF_2 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_2_AGGREGATE_IND is Y.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_DIFF3_ID IS 'The DIFF_3 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_3_AGGREGATE_IND is Y.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_DIFF4_ID IS 'The DIFF_4 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_4_AGGREGATE_IND is Y.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_ITEM IS 'The transaction level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent/diff item, it will be populated by its children.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_ITEM_LEVEL IS 'The item level of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_TRAN_LEVEL IS 'The tran level of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_PACK_IND IS 'The pack indicator of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_DIFF1_ID IS 'The DIFF_1 of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_DIFF2_ID IS 'The DIFF_2 of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_DIFF3_ID IS 'The DIFF_3 of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_DIFF4_ID IS 'The DIFF_4 of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.DEPT IS 'The DEPT of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.CLASS IS 'The CLASS of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SUBCLASS IS 'The SUBCLASS of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.PACK_NO IS 'The pack associated to the items in the TRAN_ITEM column.';
/