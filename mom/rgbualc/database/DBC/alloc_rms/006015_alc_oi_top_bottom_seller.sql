----------------------------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
----------------------------------------------------------------------------


----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
whenever sqlerror exit

--------------------------------------
--       Dropping MATERIALIZED VIEW               
--------------------------------------
PROMPT Dropping Table 'ALC_OI_TOP_BOTTOM_SELLER' 
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_OI_TOP_BOTTOM_SELLER';

  if (L_table_exists != 0) then
      execute immediate 'DROP MATERIALIZED VIEW ALC_OI_TOP_BOTTOM_SELLER';
  end if;
end;
/

-------------------------------------------
--         Creating MATERIALIZED VIEW              
-------------------------------------------
PROMPT Creating Materialized View 'ALC_OI_TOP_BOTTOM_SELLER'
CREATE MATERIALIZED VIEW ALC_OI_TOP_BOTTOM_SELLER
REFRESH FORCE USING TRUSTED CONSTRAINTS
ON DEMAND
AS
select im.item,
       im.item_desc,
       im.dept,
       im.class,
       im.subclass,
       im.alc_item_type,
       im.item_parent,
       im.diff_1,
       im.diff_2,
       im.diff_3,
       im.diff_4,
       tdh.location,
       tdh.loc_type,
       tdh.tran_date,
       tdh.units,
       im.standard_uom uom,
       tdh.total_retail,
       tdh.total_cost
  from tran_data_history tdh,
       item_master im,
       period p
 where tdh.item      = im.item
   and tdh.dept      = im.dept
   and tdh.class     = im.class
   and tdh.subclass  = im.subclass
   and tdh.tran_date = p.vdate - 1
   and tdh.tran_code = 2
/
