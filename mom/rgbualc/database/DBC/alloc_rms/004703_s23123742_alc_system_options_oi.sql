--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ALC_SYSTEM_OPTIONS_OI'
ALTER TABLE ALC_SYSTEM_OPTIONS_OI DROP COLUMN ASN_ALC_TIME_THRESHOLD
/

ALTER TABLE ALC_SYSTEM_OPTIONS_OI DROP COLUMN ALLOCATE_BY
/

ALTER TABLE ALC_SYSTEM_OPTIONS_OI DROP COLUMN SIMP_PROMO_ONLY
/

ALTER TABLE ALC_SYSTEM_OPTIONS_OI DROP COLUMN ALLOCATED_ASN_THRESHOLD
/

ALTER TABLE ALC_SYSTEM_OPTIONS_OI DROP COLUMN INV_TOL_TO_FORECAST_PLAN
/

ALTER TABLE ALC_SYSTEM_OPTIONS_OI ADD IMAGE_URL VARCHAR2 (4000 ) NULL
/

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS_OI.IMAGE_URL is 'This holds the defualt url for an image to be displayed when there are no 
images for a particular item in database'
/

