--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       MATERIALIZED VIEW                
--------------------------------------
PROMPT DROPPING MATERIALIZED VIEW 'ALC_OI_STOCK_TO_SALES'
DECLARE
  L_mv_exists number := 0;
BEGIN
  SELECT count(*) INTO L_mv_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_OI_STOCK_TO_SALES';

  if (L_mv_exists != 0) then
      execute immediate 'DROP MATERIALIZED VIEW ALC_OI_STOCK_TO_SALES';
  end if;
end;
/

PROMPT CREATING MATERIALIZED VIEW 'ALC_OI_STOCK_TO_SALES'
CREATE MATERIALIZED VIEW ALC_OI_STOCK_TO_SALES
REFRESH FORCE USING TRUSTED CONSTRAINTS
ON DEMAND
AS
select eow_date,
       subclass,
       class,
       dept,
       NVL(stock,0) as stock,
       NVL(sales,0) as sales
  FROM (with sec_locs as
           (select loc 
              from v_loc_comm_attrib_sec 
             where loc_type IN( 'W','S'))
            select  /*+ INDEX(ILH,ITEM_LOC_HIST_I1)  */
                   ilh.eow_date as eow_date,
                   SUM(NVL(ilh.stock , 0)) as stock,
                   SUM(ilh.sales_issues) as sales,
                   vim.subclass,
                   vim.class,
                   vim.dept
              from sec_locs vloc,
                   v_item_master vim,
                   item_loc_hist ilh
             where (    ilh.loc = vloc.loc
                    and ilh.item = vim.item
                    and ilh.eow_date BETWEEN (    select vdate +(7 - curr_454_day) - 49 
                                                    from period) 
                                              and (select vdate +(7-curr_454_day) - 7 
                                                     from period))
             group by ilh.eow_date,
                      vim.subclass,
                      vim.class,
                      vim.dept)
  order by eow_date ASC
/
