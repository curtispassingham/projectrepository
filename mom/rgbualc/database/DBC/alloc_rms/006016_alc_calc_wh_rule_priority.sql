--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------
PROMPT Modifying Primary Key on 'ALC_CALC_WH_RULE_PRIORITY'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'PK_ALC_CALC_WH_RULE_PRIORITY'
     AND CONSTRAINT_TYPE = 'P';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE ALC_CALC_WH_RULE_PRIORITY DROP CONSTRAINT PK_ALC_CALC_WH_RULE_PRIORITY DROP INDEX';
  end if;
end;
/
