--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 ALC_CALC_FLEX_SUPPLY_CHAIN_GTT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'ALC_CALC_FLEX_SUPPLY_CHAIN_GTT'
CREATE GLOBAL TEMPORARY TABLE ALC_CALC_FLEX_SUPPLY_CHAIN_GTT
 (ALLOC_ID NUMBER(15,0) NOT NULL,
  SOURCE_ITEM VARCHAR2(25 ),
  SOURCE_DIFF1_ID VARCHAR2(10 ),
  SOURCE_DIFF2_ID VARCHAR2(10 ),
  SOURCE_DIFF3_ID VARCHAR2(10 ),
  SOURCE_DIFF4_ID VARCHAR2(10 ),
  TRAN_ITEM VARCHAR2(25 ),
  TRAN_DIFF1_ID VARCHAR2(10 ),
  TRAN_DIFF2_ID VARCHAR2(10 ),
  TRAN_DIFF3_ID VARCHAR2(10 ),
  TRAN_DIFF4_ID VARCHAR2(10 ),
  TO_LOC NUMBER(10,0),
  TO_LOC_TYPE VARCHAR2(1 ),
  ALLOC_SOURCE_WH_ID NUMBER(10),
  ASSIGNED_RANGED_SRC_WH NUMBER(10),
  TO_LOC_DEFAULT_WH NUMBER(10),
  ORG_UNIT_ID NUMBER(15),
  CHANNEL_ID NUMBER(4),
  TSF_ENTITY_ID NUMBER(10),
  SET_OF_BOOKS_ID NUMBER(15),
  TRANSFER_BASIS VARCHAR2(1 ),
  PACK_NO VARCHAR2(25 )
 )
ON COMMIT DELETE ROWS
/

COMMENT ON TABLE ALC_CALC_FLEX_SUPPLY_CHAIN_GTT is 'Temporary staging table used in the warehouse priorization logic within the PL/SQL Calculation Package.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.ALLOC_ID is 'The ID of the allocation from which prioritization is being determined.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.SOURCE_ITEM is 'The source item record that is derived from the ALC_CALC_DESTINATION_TEMP table. If ALC_CALC_DESTINATION_TEMP holds a parent item, it will be populated here. If ALC_CALC_DESTINATION_TEMP holds a staple or pack item, the staple or pack will be populated here.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.SOURCE_DIFF1_ID is 'Child item diff1 value of the style if the style diff1 is aggregated.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.SOURCE_DIFF2_ID is 'Child item diff2 value of the style if the style diff1 is aggregated.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.SOURCE_DIFF3_ID is 'Child item diff3 value of the style if the style diff1 is aggregated.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.SOURCE_DIFF4_ID is 'Child item diff4 value of the style if the style diff1 is aggregated.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.TRAN_ITEM is 'The transaction level item associated with the ALC_ITEM_SOURCE record. If ALC_ITEM_SOURCE holds a parent/diff item, it will be populated by its children. If ALC_CALC_DESTINATION_TEMP holds a staple or pack item, the staple or pack will be populated here.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.TRAN_DIFF1_ID is 'The DIFF_1 of the item in the TRAN_ITEM column.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.TRAN_DIFF2_ID is 'The DIFF_2 of the item in the TRAN_ITEM column.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.TRAN_DIFF3_ID is 'The DIFF_3 of the item in the TRAN_ITEM column.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.TRAN_DIFF4_ID is 'The DIFF_4 of the item in the TRAN_ITEM column.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.TO_LOC is 'The location being allocated to.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.TO_LOC_TYPE is 'The type of location being allocated to.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.ALLOC_SOURCE_WH_ID is 'Contains the Warehouse ID that was selected from the Allocation UI to source the allocation'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.ASSIGNED_RANGED_SRC_WH is 'Contains the source warehouse from ITEM_LOC when the source_type is W'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.TO_LOC_DEFAULT_WH is 'Contains the default warehouse for the location. Value is derived from DEFAULT_WH column from STORE or WH table.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.ORG_UNIT_ID is 'Contains the Organizational Unit ID from which the TO_LOC belongs to.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.CHANNEL_ID is 'Contains the ID of the channel from which the TO_LOC is assigned'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.TSF_ENTITY_ID is 'ID of the transfer entity with which the TO_LOC is associated. A transfer entity is a group of locations that share legal requirements around product management.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.SET_OF_BOOKS_ID is 'ID of the books that the TO_LOC is associated.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.TRANSFER_BASIS is 'Indicates whether the intercompany transfer basis is based on ''T''- transfer entities or ''B'' - set of books indicator.'
/

COMMENT ON COLUMN ALC_CALC_FLEX_SUPPLY_CHAIN_GTT.PACK_NO is 'Indicates the pack no that is associated to the tran_item (if any)'
/

