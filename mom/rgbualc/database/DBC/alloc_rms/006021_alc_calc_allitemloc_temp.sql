----------------------------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
----------------------------------------------------------------------------


----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  TABLE MODIFIED:             ALC_CALC_ALLITEMLOC_TEMP
----------------------------------------------------------------------------

whenever sqlerror exit

----------------------------------------------------------------------------
--       CHANGING TO A GLOBAL TEMP TABLE
----------------------------------------------------------------------------
PROMPT DROPPING ALC_CALC_ALLITEMLOC_TEMP TABLE
DECLARE
  L_table_itemloc_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_itemloc_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_CALC_ALLITEMLOC_TEMP';

  if (L_table_itemloc_exists != 0) then
      execute immediate 'DROP TABLE ALC_CALC_ALLITEMLOC_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT RE-CREATING THE TABLE ALC_CALC_ALLITEMLOC_TEMP AS GTT TABLE
CREATE GLOBAL TEMPORARY TABLE ALC_CALC_ALLITEMLOC_TEMP
( ALLOC_ID             NUMBER(15,0)    NOT NULL,
  ITEM_TYPE            VARCHAR2(10),
  SOURCE_ITEM          VARCHAR2(25),
  SOURCE_ITEM_LEVEL    NUMBER(1,0),
  SOURCE_TRAN_LEVEL    NUMBER(1,0),
  SOURCE_PACK_IND      VARCHAR2(1),
  SOURCE_DIFF1_ID      VARCHAR2(10),
  SOURCE_DIFF2_ID      VARCHAR2(10),
  SOURCE_DIFF3_ID      VARCHAR2(10),
  SOURCE_DIFF4_ID      VARCHAR2(10),
  TRAN_ITEM            VARCHAR2(25),
  TRAN_ITEM_LEVEL      NUMBER(1,0),
  TRAN_TRAN_LEVEL      NUMBER(1,0),
  TRAN_PACK_IND        VARCHAR2(1),
  TRAN_DIFF1_ID        VARCHAR2(10),
  TRAN_DIFF2_ID        VARCHAR2(10),
  TRAN_DIFF3_ID        VARCHAR2(10),
  TRAN_DIFF4_ID        VARCHAR2(10),
  DEPT                 NUMBER(4,0),
  CLASS                NUMBER(4,0),
  SUBCLASS             NUMBER(4,0),
  TO_LOC               NUMBER(10,0),
  TO_LOC_TYPE          VARCHAR2(1),
  TO_LOC_NAME          VARCHAR2(150),
  SISTER_STORE         NUMBER(10,0),
  ASSIGN_DEFAULT_WH    NUMBER(10,0),
  CLEAR_IND            VARCHAR2(1),
  ITEM_LOC_STATUS      VARCHAR2(1),
  SIZE_PROFILE_QTY     NUMBER(12,4),
  TOTAL_PROFILE_QTY    NUMBER(12,4),
  STOCK_ON_HAND        NUMBER(12,4),
  ON_ORDER             NUMBER(12,4),
  ON_ALLOC             NUMBER(12,4),
  ALLOC_OUT            NUMBER(12,4),
  IN_TRANSIT_QTY       NUMBER(12,4),
  NEED_VALUE           NUMBER(20,4),
  RLOH_CURRENT_VALUE   NUMBER(20,4),
  RLOH_FUTURE_VALUE    NUMBER(20,4)
)
ON COMMIT DELETE ROWS;

COMMENT ON TABLE ALC_CALC_ALLITEMLOC_TEMP IS 'Temporary table used in the calculation process.  This table holds information about the transaction level items in ALC_CALC_SOURCE_TEMP combined with the stores that the allocation is being shipped to.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.ALLOC_ID IS 'Indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.ITEM_TYPE IS 'The type of the item being allocated.
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_ITEM IS 'The parent level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent item, it will be populated here.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_ITEM_LEVEL IS 'The item level of the item in the SOURCE_ITEM column.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_TRAN_LEVEL IS 'The tran level of the item in the SOURCE_ITEM column.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_PACK_IND IS 'The pack indicator of the item in the SOURCE_ITEM column.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_DIFF1_ID IS 'Child item diff1 value of the style if the style diff1 is aggregated.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_DIFF2_ID IS 'Child item diff2 value of the style if the style diff2 is aggregated.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_DIFF3_ID IS 'Child item diff3 value of the style if the style diff3 is aggregated.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_DIFF4_ID IS 'Child item diff4 value of the style if the style diff4 is aggregated.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_ITEM IS 'The transaction level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent/diff item, it will be populated by its children.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_ITEM_LEVEL IS 'The item level of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_TRAN_LEVEL IS 'The tran level of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_PACK_IND IS 'The pack indicator of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_DIFF1_ID IS 'The DIFF_1 of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_DIFF2_ID IS 'The DIFF_2 of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_DIFF3_ID IS 'The DIFF_3 of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_DIFF4_ID IS 'The DIFF_4 of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.DEPT IS 'Department number';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.CLASS IS 'Class number';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SUBCLASS IS 'Subclass number';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TO_LOC_TYPE IS 'The type of location being allocated to.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TO_LOC IS 'The store id the size profile is relevant for.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TO_LOC_NAME IS 'The name of the store being allocated to.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SISTER_STORE IS 'The sister_store of the store being allocated to.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.ASSIGN_DEFAULT_WH IS 'The default warehouse of the store.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.CLEAR_IND IS 'this columns contains the indicator if the item is on clearance.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.ITEM_LOC_STATUS IS 'The status of the item/store combination on the ITEM_LOC table.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SIZE_PROFILE_QTY IS ' the size profile quantity for the child item';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TOTAL_PROFILE_QTY IS ' the sum of the size profile quantities for a style/color (at item parent aggregate level)';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.STOCK_ON_HAND IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.ON_ORDER IS 'The quantity currently on order for the item/location';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.ON_ALLOC IS 'The quantity currently allocated for the item/location';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.ALLOC_OUT IS 'The quantity currently allocated from the item/store';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.IN_TRANSIT_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.NEED_VALUE IS 'The calculated need quantity for the item/store.  The method used is based on the allocation policy.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.RLOH_CURRENT_VALUE IS 'The current rule level on hand value.';

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.RLOH_FUTURE_VALUE IS 'The future rule level on hand value.';
/
