--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE MODIFIED:				ALC_CALC_NEED_TEMP
----------------------------------------------------------------------------

whenever sqlerror exit

----------------------------------------------------------------------------
--       CHANGING TO A GLOBAL TEMP TABLE
----------------------------------------------------------------------------
PROMPT DROPPING ALC_CALC_NEED_TEMP TABLE
DECLARE
  L_table_calc_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_calc_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_CALC_NEED_TEMP';

  if (L_table_calc_exists != 0) then
      execute immediate 'DROP TABLE ALC_CALC_NEED_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT RE-CREATING THE TABLE ALC_CALC_NEED_TEMP AS GTT TABLE
CREATE GLOBAL TEMPORARY TABLE ALC_CALC_NEED_TEMP
(
   ALLOC_ID              NUMBER(15)  NOT NULL,
   RULE_MANY_TO_ONE_ID   NUMBER(20),
   RULE_LEVEL            NUMBER(1),
   DEPT                  NUMBER(4),
   CLASS                 NUMBER(4),
   SUBCLASS              NUMBER(4),
   SOURCE_ITEM           VARCHAR2(25),
   SOURCE_ITEM_LEVEL     NUMBER(1),
   SOURCE_TRAN_LEVEL     NUMBER(1),
   SOURCE_PACK_IND       VARCHAR2(1),
   SOURCE_DIFF1_ID       VARCHAR2(10),
   SOURCE_DIFF2_ID       VARCHAR2(10),
   SOURCE_DIFF3_ID       VARCHAR2(10),
   SOURCE_DIFF4_ID       VARCHAR2(10),
   TRAN_ITEM             VARCHAR2(25),
   TRAN_ITEM_LEVEL       NUMBER(1),
   TRAN_TRAN_LEVEL       NUMBER(1),
   TRAN_PACK_IND         VARCHAR2(1),
   TRAN_DIFF1_ID         VARCHAR2(10),
   TRAN_DIFF2_ID         VARCHAR2(10),
   TRAN_DIFF3_ID         VARCHAR2(10),
   TRAN_DIFF4_ID         VARCHAR2(10),
   PACK_QTY              NUMBER(12,4),
   TO_LOC                NUMBER(10),
   SISTER_STORE          NUMBER(10),
   SIZE_PROFILE_QTY      NUMBER(12,4),
   TOTAL_PROFILE_QTY     NUMBER(12,4),
   EOW_DATE              DATE,
   WEIGHT                NUMBER(12,4),
   IWOS_WEEKS            NUMBER(12,4),
   SALES_HIST_NEED       NUMBER(12,4),
   FORECAST_NEED         NUMBER(12,4),
   REPLAN_NEED           NUMBER(12,4),
   PLAN_NEED             NUMBER(12,4),
   PLAN_REPROJECT_NEED   NUMBER(12,4),
   RECEIPT_PLAN_NEED     NUMBER(12,4),
   CORP_RULE_NEED        NUMBER(12,4)
)
ON COMMIT DELETE ROWS
NOCACHE;

COMMENT ON TABLE ALC_CALC_NEED_TEMP IS 'Temporary table used in the calculation process.  This table holds information about the need.  The data in this table is at the hierarchy (dept/class/subclass) level or at the transaction item level depending on the RULE_LEVEL on the ALC_RULE table for the allocation.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.ALLOC_ID IS 'The allocation being calculated.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.RULE_MANY_TO_ONE_ID IS 'When alternative hierarchies are being use, the ID of the alternate hierarchy row.  When alternative hierarchies are not being used, this will be -1.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.RULE_LEVEL IS 'The rule level from the policy or the level of the alternative hierarchy entry of alternative hierarchies are being used.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.DEPT IS 'The department from which the TRAN_ITEM belongs to.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.CLASS IS 'The class from which the TRAN_ITEM belongs to.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SUBCLASS IS 'The subclass from which the TRAN_ITEM belongs to.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_ITEM IS 'The parent item of the TRAN_ITEM.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_ITEM_LEVEL IS 'The item level of the item in the SOURCE_ITEM column.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_TRAN_LEVEL IS 'The transaction level of the item in the SOURCE_ITEM column.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_PACK_IND IS 'The pack indicator of the item in the SOURCE_ITEM column.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_DIFF1_ID IS 'The DIFF_1 value of the item in the SOURCE_ITEM column. This is only populated if the DIFF_1_AGGREGATE_IND is Y.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_DIFF2_ID IS 'The DIFF_2 value of the item in the SOURCE_ITEM column. This is only populated if the DIFF_2_AGGREGATE_IND is Y.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_DIFF3_ID IS 'The DIFF_3 value of the item in the SOURCE_ITEM column. This is only populated if the DIFF_3_AGGREGATE_IND is Y.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_DIFF4_ID IS 'The DIFF_4 value of the item in the SOURCE_ITEM column. This is only populated if the DIFF_4_AGGREGATE_IND is Y.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_ITEM IS 'The transaction level item associated with the ALC_ITEM_SOURCE record. If ALC_ITEM_SOURCE holds a parent/diff item, it will be populated by its children. If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_ITEM_LEVEL IS 'The item level of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_TRAN_LEVEL IS 'The transaction level of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_PACK_IND IS 'The pack indicator of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_DIFF1_ID IS 'The DIFF_1 identifier of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_DIFF2_ID IS 'The DIFF_2 identifier of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_DIFF3_ID IS 'The DIFF_3 identifier of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_DIFF4_ID IS 'The DIFF_4 identifier of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.PACK_QTY IS 'Contains the quantity of the TRAN_ITEM in the pack.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TO_LOC IS 'The location being allocated to.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SISTER_STORE IS 'The sister store of the location being allocated to.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SIZE_PROFILE_QTY IS 'The calculated size profile quantity for the item/location.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TOTAL_PROFILE_QTY IS 'The calculated total size profile quantity for the item/location.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.EOW_DATE IS 'The end of week date to consider need on.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.WEIGHT IS 'The weight to apply to the EOW_DATE.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.IWOS_WEEKS IS 'The ideal weeks of supply for this calculation.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SALES_HIST_NEED IS 'The sales history based need if any for the allocation.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.FORECAST_NEED IS 'The forecast based need if any for the allocation.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.REPLAN_NEED IS 'The plan reproject based need if any for the allocation.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.PLAN_NEED IS 'The sales plan based need if any for the allocation.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.PLAN_REPROJECT_NEED IS 'The forecast based need if any for the allocation.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.RECEIPT_PLAN_NEED IS 'The receipt plan based need if any for the allocation.';

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.CORP_RULE_NEED IS 'The corporate rule based need if any for the allocation.';
/
