--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE MODIFIED:				ALC_CALC_RLOH_ITEM_TEMP
----------------------------------------------------------------------------

whenever sqlerror exit
----------------------------------------------------------------------------
--       CHANGING TO A GLOBAL TEMP TABLE
----------------------------------------------------------------------------
PROMPT DROPPING ALC_CALC_RLOH_ITEM_TEMP TABLE
DECLARE
  L_table_rloh_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_rloh_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_CALC_RLOH_ITEM_TEMP';

  if (L_table_rloh_exists != 0) then
      execute immediate 'DROP TABLE ALC_CALC_RLOH_ITEM_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT RE-CREATING THE TABLE ALC_CALC_RLOH_ITEM_TEMP AS GTT TABLE
CREATE GLOBAL TEMPORARY TABLE ALC_CALC_RLOH_ITEM_TEMP
(
  ALLOC_ID    NUMBER(15),
  ITEM        VARCHAR2(25),
  PACK_IND    VARCHAR2(1),
  ITEM_LEVEL  NUMBER(1),
  TRAN_LEVEL  NUMBER(1),
  PACK_QTY    NUMBER(12,4)
)
ON COMMIT DELETE ROWS
NOCACHE;

COMMENT ON TABLE ALC_CALC_RLOH_ITEM_TEMP IS 'Temporary table used in the calculation process.  This table holds information about items to be used for RLOH inventory positions when rule level on hand is being used.';

COMMENT ON COLUMN ALC_CALC_RLOH_ITEM_TEMP.ALLOC_ID IS 'The allocation being calculated.';

COMMENT ON COLUMN ALC_CALC_RLOH_ITEM_TEMP.ITEM IS 'The item to consider in the RLOH calculation.';

COMMENT ON COLUMN ALC_CALC_RLOH_ITEM_TEMP.PACK_IND IS 'The pack indicator of the item.';

COMMENT ON COLUMN ALC_CALC_RLOH_ITEM_TEMP.ITEM_LEVEL IS 'The item level of the item.';

COMMENT ON COLUMN ALC_CALC_RLOH_ITEM_TEMP.TRAN_LEVEL IS 'The tran level of the item.';

COMMENT ON COLUMN ALC_CALC_RLOH_ITEM_TEMP.PACK_QTY IS 'The qty if the item in a pack.';
/
