--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       MODIFYING SEQUENCE            ALC_ALLOC_SEQ
--------------------------------------
PROMPT MODIFYING MAXVALUE ON SEQUENCE 'ALC_ALLOC_SEQ'
ALTER SEQUENCE ALC_ALLOC_SEQ MAXVALUE 999999999999999;