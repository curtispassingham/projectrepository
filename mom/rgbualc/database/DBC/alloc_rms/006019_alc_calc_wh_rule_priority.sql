--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ALC_CALC_WH_RULE_PRIORITY'

PROMPT Creating Unique Key on 'ALC_CALC_WH_RULE_PRIORITY'
ALTER TABLE ALC_CALC_WH_RULE_PRIORITY
 ADD CONSTRAINT ALC_CALC_WH_RULE_PRIORITY_I1 UNIQUE
  (ALLOC_ID,
   TO_LOC,
   TRAN_ITEM,
   SOURCE_WH,
   RULE_PRIORITY
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

