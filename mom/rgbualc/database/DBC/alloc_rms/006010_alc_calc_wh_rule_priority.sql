--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 ALC_CALC_WH_RULE_PRIORITY
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'ALC_CALC_WH_RULE_PRIORITY'
CREATE TABLE ALC_CALC_WH_RULE_PRIORITY
 (ALLOC_ID NUMBER(15,0) NOT NULL,
  SOURCE_ITEM VARCHAR2(25 ),
  SOURCE_DIFF1_ID VARCHAR2(10 ),
  SOURCE_DIFF2_ID VARCHAR2(10 ),
  SOURCE_DIFF3_ID VARCHAR2(10 ),
  SOURCE_DIFF4_ID VARCHAR2(10 ),
  TRAN_ITEM VARCHAR2(25 ),
  TRAN_DIFF1_ID VARCHAR2(10 ),
  TRAN_DIFF2_ID VARCHAR2(10 ),
  TRAN_DIFF3_ID VARCHAR2(10 ),
  TRAN_DIFF4_ID VARCHAR2(10 ),
  TO_LOC NUMBER(10,0),
  TO_LOC_TYPE VARCHAR2(1 ),
  SOURCE_WH NUMBER(10,0),
  RULE_PRIORITY VARCHAR2(2 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE ALC_CALC_WH_RULE_PRIORITY is 'This table will hold the priority from which warehouses will be picked up to meet the destination demand.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.ALLOC_ID is 'The ID of the allocation from which prioritization is being determined.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.SOURCE_ITEM is 'The source item record that is derived from the ALC_CALC_DESTINATION_TEMP table.  If
ALC_CALC_DESTINATION_TEMP holds a parent item, it will be populated here. If
ALC_CALC_DESTINATION_TEMP holds a staple or pack item, the staple or pack will
be populated here. '
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.SOURCE_DIFF1_ID is 'Child item diff1 value of the style if the style diff1 is aggregated.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.SOURCE_DIFF2_ID is 'Child item diff2 value of the style if the style diff1 is aggregated.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.SOURCE_DIFF3_ID is 'Child item diff3 value of the style if the style diff1 is aggregated.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.SOURCE_DIFF4_ID is 'Child item diff4 value of the style if the style diff1 is aggregated.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.TRAN_ITEM is 'The transaction level item associated with the ALC_ITEM_SOURCE
record. If ALC_ITEM_SOURCE holds a parent/diff item, it will be
populated by its children. If ALC_CALC_DESTINATION_TEMP holds a staple or pack
item, the staple or pack will be populated here.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.TRAN_DIFF1_ID is 'The DIFF_1 of the item in the TRAN_ITEM column.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.TRAN_DIFF2_ID is 'The DIFF_2 of the item in the TRAN_ITEM column.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.TRAN_DIFF3_ID is 'The DIFF_3 of the item in the TRAN_ITEM column.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.TRAN_DIFF4_ID is 'The DIFF_4 of the item in the TRAN_ITEM column.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.TO_LOC is 'The location being allocated to.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.TO_LOC_TYPE is 'The type of location being allocated to.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.SOURCE_WH is 'The warehouse that has been identified based on specific rules that will be used to fulfill the destination demand.'
/

COMMENT ON COLUMN ALC_CALC_WH_RULE_PRIORITY.RULE_PRIORITY is 'The order of priority from which the source warehouse will be considered.'
/


PROMPT Creating Primary Key on 'ALC_CALC_WH_RULE_PRIORITY'
ALTER TABLE ALC_CALC_WH_RULE_PRIORITY
 ADD CONSTRAINT PK_ALC_CALC_WH_RULE_PRIORITY PRIMARY KEY
  (ALLOC_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

