--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  TABLE MODIFIED:                ALC_MERCH_HIER_RLOH_TEMP
----------------------------------------------------------------------------

whenever sqlerror exit

----------------------------------------------------------------------------
--  ADDING INDEX
----------------------------------------------------------------------------

PROMPT DROPPING ALC_MERCH_HIER_RLOH_TEMP TABLE
DECLARE
  L_table_heir_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_heir_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_MERCH_HIER_RLOH_TEMP';

  if (L_table_heir_exists != 0) then
      execute immediate 'DROP TABLE ALC_MERCH_HIER_RLOH_TEMP';
  end if;
end;
/

PROMPT RE-CREATING THE TABLE ALC_MERCH_HIER_RLOH_TEMP 
CREATE TABLE ALC_MERCH_HIER_RLOH_TEMP
(
   ALLOC_ID       NUMBER(15,0),
   DEPT           NUMBER(4,0),
   CLASS          NUMBER(4,0),
   SUBCLASS       NUMBER(4,0),
   LOC            NUMBER(10,0),
   LOC_TYPE       VARCHAR2(1),
   CURR_AVAIL     NUMBER(24,4),
   FUTURE_AVAIL   NUMBER(24,4)
)
NOCOMPRESS
TABLESPACE RETAIL_DATA
PCTUSED    0
PCTFREE    10
INITRANS   6
MAXTRANS   255
STORAGE    (
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARTITION BY HASH (ALLOC_ID)
  PARTITIONS 512
  STORE IN (RETAIL_DATA);

COMMENT ON TABLE ALC_MERCH_HIER_RLOH_TEMP IS 'Temporary table used in the calculation process.  This table holds information about RLOH inventory positions when rule level on hand is being used at the merchadise hierarchy snapshot level..';

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.ALLOC_ID IS 'The allocation being calculated.';

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.DEPT IS 'The department to consider in the RLOH calculation.';

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.CLASS IS 'The class to consider in the RLOH calculation.';

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.SUBCLASS IS 'The subclass to consider in the RLOH calculation.';

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.LOC IS 'The location to consider in the RLOH calculation.';

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.LOC_TYPE IS 'The location type to consider in the RLOH calculation.';

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.CURR_AVAIL IS 'The current inventory of the merchandise hiearchy/location.';

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.FUTURE_AVAIL IS 'The future inventory of the merchandise hiearchy/location.  Based on the on order commit values from ALC_RULE.';

CREATE INDEX ALC_MERCH_HIER_RLOH_TEMP_I1 ON ALC_MERCH_HIER_RLOH_TEMP
(ALLOC_ID, DEPT)
LOGGING
TABLESPACE RETAIL_INDEX
LOCAL
PCTFREE    10
INITRANS   12
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
/