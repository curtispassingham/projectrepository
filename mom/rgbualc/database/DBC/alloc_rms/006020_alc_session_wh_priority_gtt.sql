--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------
PROMPT Modifying Table 'ALC_SESSION_WH_PRIORITY_GTT'
ALTER TABLE ALC_SESSION_WH_PRIORITY_GTT MODIFY ITEM_ID VARCHAR2(70)
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.ITEM_ID is 'Indicates the item identifier'
/
