--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ALC_ALLOC_SEARCH_TEMP'
ALTER TABLE ALC_ALLOC_SEARCH_TEMP DROP COLUMN LOCKED_BY_USER_ID
/

ALTER TABLE ALC_ALLOC_SEARCH_TEMP DROP COLUMN LOCKED_TIMESTAMP
/

ALTER TABLE ALC_ALLOC_SEARCH_TEMP RENAME COLUMN CREATION_DATE to CREATED_DATE
/

COMMENT ON COLUMN ALC_ALLOC_SEARCH_TEMP.CREATED_DATE is 'The date and time stamp of the record creation date.'
/

