--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Drop SEQUENCES               
--------------------------------------
PROMPT dropping SEQUENCE 'ALC_CALC_QUEUE_SEQ'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_CALC_QUEUE_SEQ';

if (L_table_exists != 0) then
      execute immediate 'DROP SEQUENCE ALC_CALC_QUEUE_SEQ';
  end if;
end;
/

PROMPT dropping SEQUENCE 'ALC_FLEXIBLE_COLUMNS_SEQ'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_FLEXIBLE_COLUMNS_SEQ';

if (L_table_exists != 0) then
      execute immediate 'DROP SEQUENCE ALC_FLEXIBLE_COLUMNS_SEQ';
  end if;
end;
/

PROMPT dropping SEQUENCE 'ALC_USERS_SEQ'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_USERS_SEQ';

if (L_table_exists != 0) then
      execute immediate 'DROP SEQUENCE ALC_USERS_SEQ';
  end if;
end;
/

PROMPT dropping SEQUENCE 'ALC_USER_DEPTS_SEQ'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_USER_DEPTS_SEQ';

if (L_table_exists != 0) then
      execute immediate 'DROP SEQUENCE ALC_USER_DEPTS_SEQ';
  end if;
end;
/