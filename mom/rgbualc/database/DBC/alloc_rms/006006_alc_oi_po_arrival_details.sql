--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 ALC_OI_PO_ARRIVAL_DETAILS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'ALC_OI_PO_ARRIVAL_DETAILS'
CREATE TABLE ALC_OI_PO_ARRIVAL_DETAILS
 (SESSION_ID NUMBER(15) NOT NULL,
  WEEK_NO NUMBER(2) NOT NULL,
  ORDER_NO NUMBER(12),
  SUPPLIER_SITE NUMBER(10),
  SUPPLIER_SITE_NAME VARCHAR2(240 ),
  NOT_BEFORE_DATE DATE,
  NOT_AFTER_DATE DATE,
  NEXT_SHIP_DATE DATE,
  ALLOC_ID NUMBER(15),
  ALLOC_PERCENT NUMBER(20,10),
  ALLOCATED_QTY NUMBER(20,4),
  WH_ORDER_QTY NUMBER(20,4),
  PROMO_IND VARCHAR2(1 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE ALC_OI_PO_ARRIVAL_DETAILS is 'This table will contain the incoming POs and the order quantities that are expected to be received based on their delivery date'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.SESSION_ID is 'The session ID of the  user that''s logged into OI.'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.WEEK_NO is 'The number of weeks out of the current week.  Valid values are:
   0 - Current week
   1 - Next week
   2 - 2 weeks out
   3 - 3 weeks out'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.ORDER_NO is 'The Purchase Order number to be alocated'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.SUPPLIER_SITE is 'The supplier related to the order'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.SUPPLIER_SITE_NAME is 'The name of the supplier related to the order.'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.NOT_BEFORE_DATE is 'The first date that the delivery of order will be accepted.'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.NOT_AFTER_DATE is 'The last date that the delivery of order will be accepted.'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.NEXT_SHIP_DATE is 'The date the Purchase Order was shipped'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.ALLOC_ID is 'The ID of the allocation tied to the PO'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.ALLOC_PERCENT is 'The percentage of WH Order Qty on the PO that is on an Approved or Reserved allocation'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.ALLOCATED_QTY is 'The total qty that has already been allocated from the PO.'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.WH_ORDER_QTY is 'The ordered qty summarized for all warehouses on the order.'
/

COMMENT ON COLUMN ALC_OI_PO_ARRIVAL_DETAILS.PROMO_IND is 'Indicates if any one of the item present on the PO is on promotion'
/


PROMPT Creating Primary Key on 'ALC_OI_PO_ARRIVAL_DETAILS'
ALTER TABLE ALC_OI_PO_ARRIVAL_DETAILS
 ADD CONSTRAINT PK_ALC_OI_PO_ARRIVAL_DETAILS PRIMARY KEY
  (SESSION_ID,
   WEEK_NO
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

