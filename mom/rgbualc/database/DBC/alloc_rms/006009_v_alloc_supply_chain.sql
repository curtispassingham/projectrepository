----------------------------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
----------------------------------------------------------------------------


----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit
----------------------------------------------------------------------------

CREATE OR REPLACE VIEW V_ALLOC_SUPPLY_CHAIN
AS
select s.store location,
       'S' loc_type,
       s.default_wh,
       NULL primary_vwh,
       NULL physical_wh,
       s.channel_id,
       s.org_unit_id,
       s.tsf_entity_id,
       o.set_of_books_id,
       NULL protected_ind,
       s.sister_store
  from store s,
       org_unit o
 where s.org_unit_id = o.org_unit_id
 union all
select w.wh location,
       'W' loc_type,
       w.default_wh,
       w.primary_vwh,
       w.physical_wh,
       w.channel_id,
       w.org_unit_id,
       w.tsf_entity_id,
       o.set_of_books_id,
       w.protected_ind,
       NULL sister_store
  from wh w ,
       org_unit o
 where w.org_unit_id = o.org_unit_id
/
