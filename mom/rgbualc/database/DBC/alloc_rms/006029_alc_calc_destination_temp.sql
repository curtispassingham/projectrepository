--------------------------------------------------------
-- Copyright (c) 2013, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
--	ATTENTION: This script DOES preserve data. It will create a temporary
--	table to hold existing data before the existing table is dropped and
--	recreated.  This script will populate the newly created table with the
--	previous values stored in the temporary table.
--
--	The customer DBA is responsible to review this script to ensure data
--	is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE MODIFIED:				ALC_CALC_DESTINATION_TEMP
----------------------------------------------------------------------------

whenever sqlerror exit
----------------------------------------------------------------------------
--       CHANGING TO A GLOBAL TEMP TABLE and MODIFY INDEX
----------------------------------------------------------------------------
PROMPT DROPPING ALC_CALC_DESTINATION_TEMP TABLE
DECLARE
  L_table_dest_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_dest_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_CALC_DESTINATION_TEMP';

  if (L_table_dest_exists != 0) then
      execute immediate 'DROP TABLE ALC_CALC_DESTINATION_TEMP CASCADE CONSTRAINTS';
  end if;
end;
/

PROMPT RE-CREATING THE TABLE ALC_CALC_DESTINATION_TEMP 
CREATE TABLE ALC_CALC_DESTINATION_TEMP
(
  ALLOC_ID             NUMBER(15)    NOT NULL,
  ITEM_TYPE            VARCHAR2(10),
  SOURCE_ITEM          VARCHAR2(25),
  SOURCE_ITEM_LEVEL    NUMBER(1),
  SOURCE_TRAN_LEVEL    NUMBER(1),
  SOURCE_PACK_IND      VARCHAR2(1),
  SOURCE_DIFF1_ID      VARCHAR2(10),
  SOURCE_DIFF2_ID      VARCHAR2(10),
  SOURCE_DIFF3_ID      VARCHAR2(10),
  SOURCE_DIFF4_ID      VARCHAR2(10),
  TRAN_ITEM            VARCHAR2(25),
  TRAN_ITEM_LEVEL      NUMBER(1),
  TRAN_TRAN_LEVEL      NUMBER(1),
  TRAN_PACK_IND        VARCHAR2(1),
  TRAN_DIFF1_ID        VARCHAR2(10),
  TRAN_DIFF2_ID        VARCHAR2(10),
  TRAN_DIFF3_ID        VARCHAR2(10),
  TRAN_DIFF4_ID        VARCHAR2(10),
  DEPT                 NUMBER(4),
  CLASS                NUMBER(4),
  SUBCLASS             NUMBER(4),
  TO_LOC               NUMBER(10),
  TO_LOC_TYPE          VARCHAR2(1),
  TO_LOC_NAME          VARCHAR2(150),
  SISTER_STORE         NUMBER(10),
  ASSIGN_DEFAULT_WH    NUMBER(10),
  CLEAR_IND            VARCHAR2(1),
  ITEM_LOC_STATUS      VARCHAR2(1),
  SIZE_PROFILE_QTY     NUMBER(12,4),
  TOTAL_PROFILE_QTY    NUMBER(12,4),
  STOCK_ON_HAND        NUMBER(12,4),
  ON_ORDER             NUMBER(12,4),
  ON_ALLOC             NUMBER(12,4),
  ALLOC_OUT            NUMBER(12,4),
  IN_TRANSIT_QTY       NUMBER(12,4),
  BACKORDER_QTY        NUMBER(12,4),
  NEED_VALUE           NUMBER(20,4),
  RLOH_CURRENT_VALUE   NUMBER(20,4),
  RLOH_FUTURE_VALUE    NUMBER(20,4)
)
NOCOMPRESS
TABLESPACE RETAIL_DATA
PCTUSED    0
PCTFREE    10
INITRANS   6
MAXTRANS   255
STORAGE    (
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARTITION BY HASH (ALLOC_ID)
  PARTITIONS 512
  STORE IN (RETAIL_DATA)
;

COMMENT ON TABLE ALC_CALC_DESTINATION_TEMP IS 'Temporary table used in the calculation process.  This table holds information about the transaction level items in ALC_CALC_SOURCE_GTT combined with the stores that the allocation is being shipped to.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.ALLOC_ID IS 'The allocation being calculated.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.ITEM_TYPE IS 'The item_type from ALC_ITEM_SOURCE.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_ITEM IS 'The parent level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent item, it will be populated here.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_ITEM_LEVEL IS 'The item level of the item in the SOURCE_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_TRAN_LEVEL IS 'The tran level of the item in the SOURCE_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_PACK_IND IS 'The pack indicator of the item in the SOURCE_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_DIFF1_ID IS 'The DIFF_1 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_1_AGGREGATE_IND is Y.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_DIFF2_ID IS 'The DIFF_2 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_2_AGGREGATE_IND is Y.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_DIFF3_ID IS 'The DIFF_3 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_3_AGGREGATE_IND is Y.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_DIFF4_ID IS 'The DIFF_4 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_4_AGGREGATE_IND is Y.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_ITEM IS 'The transaction level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent/diff item, it will be populated by its children.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_ITEM_LEVEL IS 'The item level of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_TRAN_LEVEL IS 'The tran level of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_PACK_IND IS 'The pack indicator of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_DIFF1_ID IS 'The DIFF_1 of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_DIFF2_ID IS 'The DIFF_2 of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_DIFF3_ID IS 'The DIFF_3 of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_DIFF4_ID IS 'The DIFF_4 of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.DEPT IS 'The DEPT of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.CLASS IS 'The CLASS of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SUBCLASS IS 'The SUBCLASS of the item in the TRAN_ITEM column.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TO_LOC IS 'The location being allocated to.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TO_LOC_TYPE IS 'The type of location being allocated to.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TO_LOC_NAME IS 'The name of the location being allocated to.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SISTER_STORE IS 'The sister_store of the store being allocated to.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.ASSIGN_DEFAULT_WH IS 'The default warehouse of the location.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.CLEAR_IND IS 'The clearance indicator for the item/location combination.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.ITEM_LOC_STATUS IS 'The status of the item/location combination on the ITEM_LOC table.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SIZE_PROFILE_QTY IS 'The calculated size profile quantity for the child item.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TOTAL_PROFILE_QTY IS 'The sum of the size profile quantities for a style/color (at item parent aggregate level)';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.STOCK_ON_HAND IS 'The stock on hand for the item/store.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.ON_ORDER IS 'The quantity currently on order for the item/location.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.ON_ALLOC IS 'The quantity currently allocated for the item/location.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.ALLOC_OUT IS 'The quantity currently allocated from the item/location.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.IN_TRANSIT_QTY IS 'The quantity current in transit to the item/location.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.BACKORDER_QTY IS 'The backorder quantity for the item/location combination.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.NEED_VALUE IS 'The calculated need quantity for the item/location.  The method used is based on the allocation policy.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.RLOH_CURRENT_VALUE IS 'The current rule level on hand value.';

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.RLOH_FUTURE_VALUE IS 'The future rule level on hand value.';


CREATE UNIQUE INDEX ALC_CALC_DESTINATION_TEMP_I1 ON ALC_CALC_DESTINATION_TEMP
(ALLOC_ID,
 TO_LOC,
 TRAN_ITEM,
 ASSIGN_DEFAULT_WH)
LOGGING
TABLESPACE RETAIL_INDEX
LOCAL
PCTFREE    10
INITRANS   12
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
/
