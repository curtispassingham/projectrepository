--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
whenever sqlerror exit failure

--------------------------------------
--       Dropping Table               
--------------------------------------
PROMPT Dropping Table 'ALC_OI_TOP_BOTTOM_SALES_GTT' 
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ALC_OI_TOP_BOTTOM_SALES_GTT';

  if (L_table_exists != 0) then
      execute immediate 'DROP TABLE ALC_OI_TOP_BOTTOM_SALES_GTT';
  end if;
end;
/

--------------------------------------
--         Creating Table             
--------------------------------------
CREATE GLOBAL TEMPORARY TABLE ALC_OI_TOP_BOTTOM_SALES_GTT
 (ITEM VARCHAR(25 ),
  ITEM_DESC VARCHAR(250 ),
  DEPT NUMBER(4,0),
  CLASS NUMBER(4,0),
  SUBCLASS NUMBER(4,0),
  ALC_ITEM_TYPE VARCHAR(15 ),
  ITEM_PARENT VARCHAR(25 ),
  DIFF_1 VARCHAR(10 ),
  DIFF_2 VARCHAR(10 ),
  DIFF_3 VARCHAR(10 ),
  DIFF_4 VARCHAR(10 ),
  MARKUP_CALC_TYPE VARCHAR(2 ),
  IMAGE_NAME VARCHAR(120 ),
  IMAGE_ADDR VARCHAR(255 ),
  UNITS NUMBER(20,4),
  UOM VARCHAR2(4),
  RETAIL NUMBER(20,4),
  COST NUMBER(20,4),
  SALES NUMBER(20,4),
  MARGIN NUMBER(20,4),
  TOP_BOTTOM_FLAG VARCHAR(6 )
 )
ON COMMIT DELETE ROWS
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.ITEM is 'Contains the item identifier'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.ITEM_DESC is 'Contains the description of the item.  For Fashion items, this will contain the parent item description with the diff descriptions of all aggregatable diffs'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.DEPT is 'The department from which the item belongs to'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.CLASS is 'The class from which the item belongs to'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.SUBCLASS is 'The subclass from which the item belongs to'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.ALC_ITEM_TYPE is 'Contains the item type'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.ITEM_PARENT is 'Contains the Parent Item identifier'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.DIFF_1 is 'Contains the first item differentiator for the item'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.DIFF_2 is 'Contains the second item differentiator for the item'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.DIFF_3 is 'Contains the third item differentiator for the item'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.DIFF_4 is 'Contains the fourth item differentiator for the item'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.MARKUP_CALC_TYPE is 'Contains how markup is calculated for the department.
Valid values are: C = Cost, R = Retail'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.IMAGE_NAME is 'Contains the name of the image of the item.'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.IMAGE_ADDR is 'Contains the actual path where the file of the image of the item is stored.'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.UNITS is 'Contains the total number of units of the item involved in the transaction'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.UOM is 'Unit of measure in which stock of the item is tracked at a corporate level.'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.RETAIL is 'Contains the total retail value of the transaction in the local currency of the location where the transaction took place'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.COST is 'Contains the total cost associated of the transaction in the local currency of the location where the transaction took place'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.SALES is 'Contains the total sales associated of the transaction in the local currency of the location where the transaction took place'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.MARGIN is 'The profit margin in percentage of the retailer for the given item'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.TOP_BOTTOM_FLAG is 'The indicator that describes if the item is a top or bottom seller'
/
