--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ALC_ALLOC'
ALTER TABLE ALC_ALLOC DROP COLUMN LOCKED_BY_USER_ID
/

ALTER TABLE ALC_ALLOC DROP COLUMN LOCKED_TIMESTAMP
/

ALTER TABLE ALC_ALLOC MODIFY CREATION_DATE DATE
/

ALTER TABLE ALC_ALLOC RENAME COLUMN CREATION_DATE to CREATED_DATE
/

COMMENT ON COLUMN ALC_ALLOC.CREATED_DATE is 'Time allocation was created'
/

