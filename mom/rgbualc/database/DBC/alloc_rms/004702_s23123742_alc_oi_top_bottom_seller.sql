----------------------------------------------------------------------------
-- Copyright (c) 2013, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
----------------------------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit
----------------------------------------------------------------------------

CREATE MATERIALIZED VIEW ALC_OI_TOP_BOTTOM_SELLER
REFRESH FORCE USING TRUSTED CONSTRAINTS
ON DEMAND
AS
    select item_desc,
           currency,
           image_url,
           dept,
           class,
           subclass,
           margin,sales_units,
           sales,cost,
           diff1,
           diff2,
           diff3,
           diff4,
           diff1u,
           diff2u,
           diff3u,
           diff4u
      from (select --Item description--
                   case
                      when tran_cst_rtl.item_desc is NOT NULL then
                         tran_cst_rtl.item_desc
                      when item_image.item_desc is NOT NULL then
                         item_image.item_desc
                      when tran_sls.item_desc is NOT NULL then
                         tran_sls.item_desc
                      end  as ITEM_DESC,
                   'USD' as CURRENCY,
                   --image url--
                   item_image.image_url as image_url,
                   item_image.dept as dept,
                   item_image.class as class,
                   item_image.subclass as subclass,
                   --margin--
                   case
                      when tran_cst_rtl.markup_calc_type = 'R' then
                         ROUND((tran_cst_rtl.total_retail - tran_cst_rtl.total_cost) / NULLIF( tran_cst_rtl.total_retail, 0) * 100 , 2)
                      when tran_cst_rtl.MARKUP_CALC_TYPE = 'C' then
                         ROUND((tran_cst_rtl.total_retail - tran_cst_rtl.total_cost) / NULLIF( tran_cst_rtl.total_cost, 0) * 100 , 2)
                      end as margin,
                   --units--
                   tran_sls.units as sales_units,
                   --sales--
                   ROUND(tran_cst_rtl.total_retail ,2) as sales,
                   --total cost--
                   ROUND(tran_cst_rtl.total_retail,2) as cost,
                   -- item diffs-
                   case
                      when tran_cst_rtl.diff1 is NOT NULL then
                         tran_cst_rtl.diff1
                      when item_image.diff1 is NOT NULL then
                         item_image.diff1
                      when tran_sls.diff1 is NOT NULL then
                         tran_sls.diff1
                      end as diff1,
                   ---
                   case
                      when tran_cst_rtl.diff2 is NOT NULL then
                         tran_cst_rtl.diff2
                      when item_image.diff2 is NOT NULL then
                         item_image.diff2
                      when tran_sls.diff2 is NOT NULL then
                         tran_sls.diff2
                      end as diff2,
                   ---
                   case
                      when tran_cst_rtl.diff3 is NOT NULL then
                         tran_cst_rtl.diff3
                      when item_image.diff3 is NOT NULL then
                         item_image.diff3
                      when tran_sls.diff3 is NOT NULL then
                         tran_sls.diff3
                      end as diff3,
                   ---
                   case
                      when tran_cst_rtl.diff4 is NOT NULL then
                         tran_cst_rtl.diff4
                      when item_image.diff4 is NOT NULL then
                         item_image.diff4
                      when tran_sls.diff4 is NOT NULL then
                         tran_sls.diff4
                      end as diff4,
                   ---
                   case
                      when tran_cst_rtl.diff1u is NOT NULL then
                         tran_cst_rtl.diff1u
                      when item_image.diff1u is NOT NULL then
                         item_image.diff1u
                      when tran_sls.diff1u is NOT NULL then
                         tran_sls.diff1u
                      end as diff1u,
                   ---
                   case
                      when tran_cst_rtl.diff2u is NOT NULL then
                         tran_cst_rtl.diff2u
                      when item_image.diff2u is NOT NULL then
                         item_image.diff2u
                      when tran_sls.diff2u is NOT NULL then
                         tran_sls.diff2u
                   end as diff2u,
                   ---
                   case
                      when tran_cst_rtl.diff3u is NOT NULL then
                         tran_cst_rtl.diff3u
                      when item_image.diff3u is NOT NULL then
                         item_image.diff3u
                      when tran_sls.diff3u is NOT NULL then
                         tran_sls.diff3u
                      end  as diff3u,
                   ---
                   case
                      when tran_cst_rtl.diff4u is NOT NULL then
                         tran_cst_rtl.diff4u
                      when item_image.diff4u is NOT NULL then
                         item_image.diff4u
                      when tran_sls.diff4u is NOT NULL then
                         tran_sls.diff4u
                      end as diff4u
                   ---
             from ((select DISTINCT SUM(tdrs.TOTAL_RETAIL) OVER (PARTITION BY tdrs.markup_calc_type,
                                                                              tdrs.diff1,
                                                                              tdrs.diff2,
                                                                              tdrs.diff3,
                                                                              tdrs.diff4,
                                                                              tdrs.item_desc) as total_retail1,
                           tdrs.markup_calc_type as markup_calc_type,
                           tdrs.diff1 as diff1,
                           tdrs.diff2 as diff2,
                           tdrs.diff3 as diff3,
                           tdrs.diff4 as diff4,
                           tdrs.item_desc as item_desc,
                           SUM(tdrs.TOTAL_RETAIL) OVER (PARTITION BY tdrs.diff1,
                                                                     tdrs.diff2,
                                                                     tdrs.diff3,
                                                                     tdrs.diff4,
                                                                     tdrs.item_desc) as total_retail,
                           SUM(tdrs.TOTAL_COST) OVER (PARTITION BY tdrs.diff1,
                                                                   tdrs.diff2,
                                                                   tdrs.diff3,
                                                                   tdrs.diff4,
                                                                   tdrs.item_desc) as total_cost,
                           tdrs.diff1u as diff1u,
                           tdrs.diff2u as diff2u,
                           tdrs.diff3u as diff3u,
                           tdrs.diff4u as diff4u
                      from (select /*+ PK_ITEM_MASTER PK_ITEM_MASTER */
                                   SUM(vccr.exchange_rate * NVL(tdh.total_retail, 0)) as total_retail,
                                   vdeps.markup_calc_type as markup_calc_type,
                                   case
                                      when
                                         case
                                            when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                               vimp.diff_1_aggregate_ind
                                            else
                                               vim.diff_1_aggregate_ind
                                            end  = 'Y' then
                                         vim.diff_1
                                      end as DIFF1,
                                   ---
                                   case
                                      when
                                         case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                 vimp.diff_2_aggregate_ind
                                              else
                                                 vim.diff_2_aggregate_ind
                                              end  = 'Y' then
                                         vim.diff_2
                                      end as DIFF2,
                                   ---
                                   case
                                      when
                                         case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                 vimp.diff_3_aggregate_ind
                                              else
                                                 vim.diff_3_aggregate_ind
                                              end  = 'Y' then
                                            VIM.DIFF_3
                                      end as DIFF3,
                                   ---
                                   case
                                      when
                                         case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                 vimp.diff_4_aggregate_ind
                                              else vim.diff_4_aggregate_ind end  = 'Y' then
                                                 vim.diff_4
                                      end as diff4,
                                   ---
                                   case when vimp.item_aggregate_ind = 'N' then
                                      vim.item_desc
                                   else
                                      case
                                         when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                            NVL(vimp.item_desc , 'No Description')
                                         else
                                            NVL(vim.item_desc , 'No Description')
                                         end
                                      end as ITEM_DESC,
                                   ---
                                   SUM(vccr.exchange_rate * NVL(tdh.total_cost , 0)) as total_cost,
                                   UPPER(case
                                            when
                                               case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                       vimp.diff_1_aggregate_ind
                                                    else vim.diff_1_aggregate_ind
                                                    end  = 'Y' then
                                                  vim.diff_1
                                            end) as diff1u,
                                   UPPER(case
                                            when
                                               case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                       vimp.diff_2_aggregate_ind
                                                    else vim.diff_2_aggregate_ind
                                                    end  = 'Y' then
                                               vim.diff_2
                                            end) as diff2u,
                                   UPPER(case
                                            when case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                               vimp.diff_3_aggregate_ind
                                            else vim.diff_3_aggregate_ind end  = 'Y' then
                                               vim.diff_3
                                            end ) as diff3u,
                                   UPPER(case
                                            when case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                               vimp.diff_4_aggregate_ind
                                            else vim.diff_4_aggregate_ind end  = 'Y' then
                                               vim.diff_4
                                            end ) as diff4u
                              from tran_data_codes tdc,
                                   (select dept,
                                           markup_calc_type
                                      from v_deps) vdeps,
                                   v_item_master vim /* V_Item_Master */  left outer join v_item_master vimp /* V_Item_Master_Parent */ on vimp.item = vim.item_parent,
                                   tran_data_history tdh,
                                   v_loc_comm_attrib_sec vloc,
                                   (select from_currency,
                                           to_currency,
                                           effective_date,
                                           exchange_type,
                                           exchange_rate
                                      from (select from_currency,
                                                   to_currency,
                                                   effective_date,
                                                   exchange_type,
                                                   exchange_rate,
                                                   RANK() OVER (PARTITION BY from_currency,
                                                                             to_currency,
                                                                             exchange_type
                                                                       order by effective_date DESC) as rank
                                              from mv_currency_conversion_rates
                                             where exchange_type = 'O'
                                               and effective_date <= (select vdate
                                                                        from period))
                                     where rank =1
                                   ) vccr
                             where (    tdh.tran_code      = tdc.code
                                    and vloc.currency_code = vccr.from_currency
                                    and tdh.item           = vim.item
                                    and tdh.location       = vloc.loc
                                    and tdh.tran_code      = 2
                                    and tdh.tran_date      = (select vdate-1
                                                                from period)
                                    and vim.dept           = vdeps.dept
                                    and vccr.to_currency   = (select NVL(currency_code,'USD')
                                                                from system_options)
                                                                 and vccr.exchange_type = 'O')
                              group by vdeps.markup_calc_type,
                                       case
                                          when
                                             case
                                                when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                   vimp.diff_1_aggregate_ind
                                                else
                                                   vim.diff_1_aggregate_ind
                                                end  = 'Y' then
                                             vim.diff_1
                                          end,
                                       ---
                                       case
                                          when
                                             case
                                                when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                   vimp.diff_2_aggregate_ind
                                             else
                                                vim.diff_2_aggregate_ind
                                             end  = 'Y' then
                                             vim.diff_2
                                          end,
                                       ---
                                       case
                                          when
                                             case
                                                when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                   vimp.diff_3_aggregate_ind
                                             else
                                                vim.diff_3_aggregate_ind
                                             end  = 'Y' then
                                             vim.diff_3
                                          end,
                                       ---
                                       case
                                          when
                                             case
                                                when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                   vimp.diff_4_aggregate_ind
                                                else
                                                   vim.diff_4_aggregate_ind
                                                end  = 'Y' then
                                             vim.diff_4
                                          end,
                                       ---
                                       case
                                          when vimp.item_aggregate_ind = 'N' then
                                             vim.item_desc
                                          else
                                             case
                                                when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                   NVL(vimp.item_desc , 'No Description')
                                             else
                                                NVL(vim.item_desc , 'No Description')
                                             end
                                          end,
                                       ---
                                       UPPER(case
                                                when
                                                   case
                                                      when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                         vimp.diff_1_aggregate_ind
                                                      else
                                                         vim.diff_1_aggregate_ind
                                                      end  = 'Y' then
                                                vim.diff_1
                                             end),
                                       ---
                                       UPPER(case
                                                when
                                                   case
                                                      when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                         vimp.diff_2_aggregate_ind
                                                      else
                                                         vim.diff_2_aggregate_ind
                                                      end  = 'Y' then
                                                vim.diff_2
                                             end ),
                                       ---
                                       UPPER(case
                                                when
                                                   case
                                                      when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                         vimp.diff_3_aggregate_ind
                                                      else
                                                         vim.diff_3_aggregate_ind
                                                      end  = 'Y' then
                                                   vim.diff_3
                                             end ),
                                       ---
                                       UPPER(case when case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then VIMP.DIFF_4_AGGREGATE_IND else VIM.DIFF_4_AGGREGATE_IND end  = 'Y' then VIM.DIFF_4 end )
                                ) tdrs
                    ) tran_cst_rtl
                    ---
                    full outer join (select /*+ PK_ITEM_MASTER PK_ITEM_MASTER */
                                            MAX(viimg.image_url) as image_url,
                                            case
                                               when vimp.item_aggregate_ind = 'N' then
                                                  vim.item_desc
                                               else
                                                  case
                                                     when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                        NVL(vimp.item_desc , 'No Description')
                                                     else NVL(vim.item_desc , 'No Description')
                                                  end
                                               end as item_desc,
                                            ---
                                            case
                                               when
                                                  case
                                                     when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                        vimp.diff_1_aggregate_ind
                                                     else
                                                        vim.diff_1_aggregate_ind
                                                     end  = 'Y' then
                                                  vim.diff_1
                                               end as diff1,
                                            ---
                                            case
                                               when
                                                  case
                                                     when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                        vimp.diff_2_aggregate_ind
                                                     else
                                                        vim.diff_2_aggregate_ind
                                                     end  = 'Y' then
                                                  vim.diff_2
                                               end as diff2,
                                            ---
                                            case
                                               when
                                                  case
                                                     when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                        vimp.diff_3_aggregate_ind
                                                     else
                                                        vim.diff_3_aggregate_ind
                                                     end  = 'Y' then
                                                  vim.diff_3
                                               end as diff3,
                                            ---
                                            case
                                               when
                                                  case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                     vimp.diff_4_aggregate_ind
                                                  else
                                                     vim.diff_4_aggregate_ind
                                                  end  = 'Y' then
                                                  vim.diff_4
                                               end as diff4,
                                            ---
                                            UPPER(case
                                                     when
                                                        case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                           vimp.diff_1_aggregate_ind
                                                        else vim.diff_1_aggregate_ind
                                                        end  = 'Y' then
                                                     vim.diff_1
                                                     end) as diff1u,
                                            ---
                                            UPPER(case
                                                     when
                                                        case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                           vimp.diff_2_aggregate_ind
                                                        else
                                                           vim.diff_2_aggregate_ind
                                                        end  = 'Y' then
                                                        vim.diff_2
                                                     end) as diff2u,
                                            UPPER(case when case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then vimp.diff_3_aggregate_ind else vim.diff_3_aggregate_ind end  = 'Y' then vim.diff_3 end ) as DIFF3U,
                                            UPPER(case when case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then vimp.diff_4_aggregate_ind else vim.diff_4_aggregate_ind end  = 'Y' then vim.diff_4 end ) as DIFF4U,
                                            vim.dept as dept,
                                            vim.class as class,
                                            vim.subclass  as subclass
                                       from (v_item_master vim /* V_Item_Master */ LEFT OUTER JOIN v_item_master vimp /* V_Item_Master_Parent */ ON vimp.item = vim.item_parent)
                                       left outer join (select item,
                                                               image_addr||image_name as image_url,
                                                               primary_ind
                                                          from item_image
                                                         where primary_ind = 'Y') viimg
                                         on vim.item    = viimg.item
                                      where (1=1)
                                      group by vim.dept ,
                                               vim.class,
                                               vim.subclass,
                                               ---
                                               case
                                                  when
                                                     case
                                                        when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                           vimp.diff_1_aggregate_ind
                                                        else
                                                           vim.diff_1_aggregate_ind
                                                        end  = 'Y' then
                                                     vim.diff_1
                                                  end,
                                               ---
                                               case
                                                  when
                                                     case
                                                        when vim.item_level = vim.tran_level and NOT VIM.ITEM_PARENT is NULL then
                                                           vimp.diff_2_aggregate_ind
                                                        else
                                                           vim.diff_2_aggregate_ind
                                                        end  = 'Y' then
                                                     vim.diff_2
                                                  end,
                                               ---
                                               case
                                                  when
                                                     case
                                                        when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                           vimp.diff_3_aggregate_ind
                                                        else
                                                           vim.diff_3_aggregate_ind
                                                        end  = 'Y' then
                                                     vim.diff_3
                                                  end ,
                                               ---
                                               case
                                                  when
                                                     case when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                        vimp.diff_4_aggregate_ind
                                                     else
                                                        vim.diff_4_aggregate_ind
                                                     end  = 'Y' then
                                                     vim.diff_4
                                                  end ,
                                               ---
                                               case
                                                  when vimp.item_aggregate_ind = 'N' then
                                                     vim.item_desc
                                                  else
                                                     case
                                                        when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                                           NVL(vimp.item_desc , 'No Description')
                                                        else
                                                           NVL(VIM.ITEM_DESC , 'No Description')
                                                        end
                                                     end
                                    ) item_image
                                    on     SYS_OP_MAP_NONNULL(tran_cst_rtl.item_desc) = SYS_OP_MAP_NONNULL(item_image.item_desc)
                                       and SYS_OP_MAP_NONNULL(tran_cst_rtl.diff1)     = SYS_OP_MAP_NONNULL(item_image.diff1)
                                       and SYS_OP_MAP_NONNULL(tran_cst_rtl.diff2)     = SYS_OP_MAP_NONNULL(item_image.diff2)
                                       and SYS_OP_MAP_NONNULL(tran_cst_rtl.diff3)     = SYS_OP_MAP_NONNULL(item_image.diff3)
                                       and SYS_OP_MAP_NONNULL(tran_cst_rtl.diff4)     = SYS_OP_MAP_NONNULL(item_image.diff4)
                  )
                  -- FULL OUTER JOIN ON TRAN_SLS
  full outer join (select /*+ PK_ITEM_MASTER PK_ITEM_MASTER */
                          SUM(tdh.units) as units,
                          case
                             when vimp.item_aggregate_ind = 'N' then
                                vim.item_desc
                             else
                                case
                                   when vim.item_level = vim.tran_level and NOT VIM.ITEM_PARENT is NULL then
                                      NVL(vimp.item_desc , 'No Description')
                                   else NVL(vim.item_desc , 'No Description')
                                end
                             end as item_desc,
                          ---
                          case
                             when case
                                     when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                        vimp.diff_1_aggregate_ind
                                     else
                                        vim.diff_1_aggregate_ind
                                     end  = 'Y' then
                                vim.diff_1
                             end as diff1,
                          ---
                          case
                             when case
                                     when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                        vimp.diff_2_aggregate_ind
                                     else
                                        vim.diff_2_aggregate_ind
                                     end  = 'Y' then
                                vim.diff_2
                             end as diff2,
                          ---
                          case
                             when case
                                     when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                        vimp.diff_3_aggregate_ind
                                     else
                                        vim.diff_3_aggregate_ind
                                     end  = 'Y' then
                                vim.diff_3
                             end as diff3,
                          ---
                          case
                             when case
                                     when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                         vimp.diff_4_aggregate_ind
                                     else
                                         vim.diff_4_aggregate_ind
                                     end  = 'Y' then
                                vim.diff_4
                             end as diff4,
                          ---
                          UPPER(case
                                   when case
                                           when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                              vimp.diff_1_aggregate_ind
                                           else
                                              vim.diff_1_aggregate_ind
                                           end  = 'Y' then
                                      vim.diff_1
                                   end ) as diff1u,
                          ---
                          UPPER(case
                                   when
                                      case
                                         when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                            vimp.diff_2_aggregate_ind
                                         else
                                            vim.diff_2_aggregate_ind
                                         end  = 'Y' then
                                     vim.diff_2
                                   end ) as diff2u,
                          ---
                          UPPER(case
                                   when
                                      case
                                         when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                            vimp.diff_3_aggregate_ind
                                         else
                                            vim.diff_3_aggregate_ind
                                         end  = 'Y' then
                                      vim.diff_3
                                   end ) as diff3u,
                          ---
                          UPPER(case
                                   when
                                      case
                                         when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                            vimp.diff_4_aggregate_ind
                                         else
                                            vim.diff_4_aggregate_ind
                                         end  = 'Y' then
                                      vim.diff_4
                                   end ) as diff4u
                     from tran_data_codes tdc,
                          v_item_master vim /* Dim_V_Item_Master */
                     left outer join v_item_master vimp /* Dim_V_Item_Master_Parent */
                       on vimp.item = vim.item_parent,
                          tran_data_history tdh
                    where (    tdh.item = vim.item
                           and tdh.tran_code = tdc.code
                           and tdh.tran_code = 2
                           and tdh.tran_date = (select vdate-1
                                                  from period))
                  group by case
                              when
                                 case
                                    when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                       vimp.diff_1_aggregate_ind
                                    else
                                       vim.diff_1_aggregate_ind
                                    end  = 'Y' then
                                 vim.diff_1
                              end,
                           ---
                           case
                              when
                                 case
                                    when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                       vimp.diff_2_aggregate_ind
                                    else
                                       vim.diff_2_aggregate_ind
                                    end  = 'Y' then
                                 vim.diff_2
                              end,
                           ---
                           case
                              when
                                 case
                                    when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                       vimp.diff_3_aggregate_ind
                                    else
                                       vim.diff_3_aggregate_ind
                                    end  = 'Y' then
                                 vim.diff_3
                              end,
                           ---
                           case
                              when
                                 case
                                    when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                       vimp.diff_4_aggregate_ind
                                    else
                                       vim.diff_4_aggregate_ind
                                    end  = 'Y' then
                                 vim.diff_4
                              end,
                           ---
                           case
                              when vimp.item_aggregate_ind = 'N' then
                                 vim.item_desc
                              else
                                 case
                                    when vim.item_level = vim.tran_level and NOT vim.item_parent is NULL then
                                       NVL(vimp.item_desc , 'No Description')
                                    else
                                       NVL(vim.item_desc , 'No Description')
                                    end
                               end
                    ) tran_sls
               on     SYS_OP_MAP_NONNULL(tran_sls.ITEM_DESC) = SYS_OP_MAP_NONNULL(case
                                                                                     when tran_cst_rtl.item_desc is NOT NULL then
                                                                                        tran_cst_rtl.item_desc
                                                                                     when item_image.item_desc is NOT NULL then
                                                                                        item_image.item_desc
                                                                                     end)
                  and SYS_OP_MAP_NONNULL(tran_sls.DIFF1)     = SYS_OP_MAP_NONNULL(case
                                                                                     when tran_cst_rtl.diff1 is NOT NULL then
                                                                                        tran_cst_rtl.diff1
                                                                                     when item_image.diff1 is NOT NULL then
                                                                                        item_image.diff1
                                                                                     end)
                  and SYS_OP_MAP_NONNULL(tran_sls.DIFF2)     = SYS_OP_MAP_NONNULL(case
                                                                                     when tran_cst_rtl.diff2 is NOT NULL then
                                                                                        tran_cst_rtl.diff2
                                                                                     when item_image.diff2 is NOT NULL then
                                                                                        item_image.diff2
                                                                                     end)
                  and SYS_OP_MAP_NONNULL(tran_sls.DIFF3)     = SYS_OP_MAP_NONNULL(case
                                                                                     when tran_cst_rtl.diff3 is NOT NULL then
                                                                                        tran_cst_rtl.diff3
                                                                                     when item_image.diff3 is NOT NULL then
                                                                                        item_image.diff3
                                                                                     end)
                  and SYS_OP_MAP_NONNULL(tran_sls.DIFF4)     = SYS_OP_MAP_NONNULL(case
                                                                                     when tran_cst_rtl.diff4 is NOT NULL then
                                                                                        tran_cst_rtl.diff4
                                                                                     when item_image.diff4 is NOT NULL then
                                                                                        item_image.diff4
                                                                                     end)
            order by item_desc,
                     diff1,
                     diff2,
                     diff3,
                     diff4,
                     dept,
                     class,
                     subclass,
                     diff1u,
                     diff2u,
                     diff3u,
                     diff4u
           );
