--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 ALC_SESSION_WH_PRIORITY_GTT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'ALC_SESSION_WH_PRIORITY_GTT'
CREATE GLOBAL TEMPORARY TABLE ALC_SESSION_WH_PRIORITY_GTT
 (ITEM_LOC_SESSION_ID NUMBER(15,0) NOT NULL,
  FACET_SESSION_ID VARCHAR2(50 ) NOT NULL,
  PARENT_ITEM_LOC_SESSION_ID NUMBER(15,0),
  ALLOC_ID NUMBER(15,0),
  LOCATION_ID NUMBER(10,0),
  LOCATION_DESC VARCHAR2(150 ),
  LOC_TYPE VARCHAR2(1 ),
  WH_ID NUMBER(10,0),
  ITEM_ID VARCHAR2(25 ),
  ITEM_DESC VARCHAR2(250 ),
  SOURCE_WH NUMBER(10,0),
  RULE_PRIORITY VARCHAR2(2 )
 )
ON COMMIT DELETE ROWS
/

COMMENT ON TABLE ALC_SESSION_WH_PRIORITY_GTT is 'This table stores the Source warehouse and Rule Priority for a specific Item/Location/Session Information for an Allocation'
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.ITEM_LOC_SESSION_ID is 'Unique identifier of the item/location session record.'
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.FACET_SESSION_ID is 'Indicates the Entity Session ID per view'
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.PARENT_ITEM_LOC_SESSION_ID is 'Indicates the parent session ID for the child record'
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.ALLOC_ID is 'ID of the allocation. This is the same allocation ID from ALC_ALLOC
table.'
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.LOCATION_ID is 'This column contains the store or warehouse identifier.'
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.LOCATION_DESC is 'This column contains the store or warehouse description.'
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.LOC_TYPE is 'Indicates the location type for the location.  Valid values are:
   S - Store
   W - Warehouse'
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.WH_ID is 'The source warehouse that was initially selected by the user'
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.ITEM_ID is 'Indicates the item identifier'
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.ITEM_DESC is 'Indicates the description of the item.'
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.SOURCE_WH is 'The warehouse that has been identified based on specific rules that will be used to fulfill the destination demand.'
/

COMMENT ON COLUMN ALC_SESSION_WH_PRIORITY_GTT.RULE_PRIORITY is 'The order of priority from which the source warehouse will be considered.'
/

