--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 ALC_OI_TOP_BOTTOM_SALES_GTT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'ALC_OI_TOP_BOTTOM_SALES_GTT'
CREATE GLOBAL TEMPORARY TABLE ALC_OI_TOP_BOTTOM_SALES_GTT
 (ITEM VARCHAR2(25 ),
  ITEM_DESC VARCHAR2(250 ),
  DEPT NUMBER(4),
  CLASS NUMBER(4),
  SUBCLASS NUMBER(4),
  ALC_ITEM_TYPE VARCHAR2(15 ),
  ITEM_PARENT VARCHAR2(25 ),
  DIFF_1 VARCHAR2(10 ),
  DIFF_2 VARCHAR2(10 ),
  DIFF_3 VARCHAR2(10 ),
  DIFF_4 VARCHAR2(10 ),
  MARKUP_CALC_TYPE VARCHAR2(2 ),
  IMAGE_NAME VARCHAR2(120 ),
  IMAGE_ADDR VARCHAR2(255 ),
  UNITS NUMBER(20,4),
  RETAIL NUMBER(20,4),
  COST NUMBER(20,4),
  SALES NUMBER(20,4),
  MARGIN NUMBER(20,4),
  TOP_BOTTOM_FLAG VARCHAR2(6 )
 )
ON COMMIT DELETE ROWS
/

COMMENT ON TABLE ALC_OI_TOP_BOTTOM_SALES_GTT is 'This will hold Item sales information to be used by the Allocation OI Reports'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.ITEM is 'Contains the item identifier'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.ITEM_DESC is 'Contains the description of the item.  For Fashion items, this will contain the parent item description with the diff descriptions of all aggregatable diffs'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.DEPT is 'The department from which the item belongs to'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.CLASS is 'The class from which the item belongs to'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.SUBCLASS is 'The subclass from which the item belongs to'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.ALC_ITEM_TYPE is 'Contains the item type'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.ITEM_PARENT is 'Contains the Parent Item identifier'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.DIFF_1 is 'Contains the first item differentiator for the item'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.DIFF_2 is 'Contains the second item differentiator for the item'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.DIFF_3 is 'Contains the third item differentiator for the item'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.DIFF_4 is 'Contains the fourth item differentiator for the item'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.MARKUP_CALC_TYPE is 'Contains how markup is calculated for the department.
Valid values are: C = Cost, R = Retail'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.IMAGE_NAME is 'Contains the name of the image of the item.'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.IMAGE_ADDR is 'Contains the actual path where the file of the image of the item is stored.'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.UNITS is 'Contains the total number of units of the item involved in the transaction'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.RETAIL is 'Contains the total retail value of the transaction in the local currency of the location where the transaction took place'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.COST is 'Contains the total cost associated of the transaction in the local currency of the location where the transaction took place'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.SALES is 'Contains the total sales associated of the transaction in the local currency of the location where the transaction took place'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.MARGIN is 'The profit margin in percentage of the retailer for the given item'
/

COMMENT ON COLUMN ALC_OI_TOP_BOTTOM_SALES_GTT.TOP_BOTTOM_FLAG is 'The indicator that describes if the item is a top or bottom seller'
/

