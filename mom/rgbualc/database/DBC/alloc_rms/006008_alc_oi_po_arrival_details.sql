--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ALC_OI_PO_ARRIVAL_DETAILS'

PROMPT Modifying Primary Key on 'ALC_OI_PO_ARRIVAL_DETAILS'

DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'PK_ALC_OI_PO_ARRIVAL_DETAILS'
     AND constraint_TYPE = 'P';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE ALC_OI_PO_ARRIVAL_DETAILS
 DROP CONSTRAINT PK_ALC_OI_PO_ARRIVAL_DETAILS DROP INDEX';
  end if;
end;
/

ALTER TABLE ALC_OI_PO_ARRIVAL_DETAILS
 ADD CONSTRAINT PK_ALC_OI_PO_ARRIVAL_DETAILS PRIMARY KEY
  (SESSION_ID,
   WEEK_NO,
   ORDER_NO
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/




