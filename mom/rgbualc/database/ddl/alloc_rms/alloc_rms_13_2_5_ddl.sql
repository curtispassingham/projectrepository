--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------
whenever sqlerror exit

PROMPT CREATING SEQUENCE 'ALC_ALLOC_SEQ';
CREATE SEQUENCE ALC_ALLOC_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_APPROVAL_QUANTITY_SEQ';
CREATE SEQUENCE ALC_APPROVAL_QUANTITY_SEQ 
    INCREMENT BY 1 
    MAXVALUE 9999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_AUTO_QUANTITY_LIMITS_SEQ';
CREATE SEQUENCE ALC_AUTO_QUANTITY_LIMITS_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 10000 
;


PROMPT CREATING SEQUENCE 'ALC_CALC_QUEUE_SEQ';
CREATE SEQUENCE ALC_CALC_QUEUE_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_CORPORATE_RULE_DETAIL_SEQ';
CREATE SEQUENCE ALC_CORPORATE_RULE_DETAIL_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_CORPORATE_RULE_HEAD_SEQ';
CREATE SEQUENCE ALC_CORPORATE_RULE_HEAD_SEQ
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_FLEXIBLE_COLUMNS_SEQ';
CREATE SEQUENCE ALC_FLEXIBLE_COLUMNS_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_FREIGHT_COST_SEQ';
CREATE SEQUENCE ALC_FREIGHT_COST_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_GENERATED_PO_SEQ';
CREATE SEQUENCE ALC_GENERATED_PO_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_GID_HEADER_SEQ';
CREATE SEQUENCE ALC_GID_HEADER_SEQ 
    INCREMENT BY 1 
    MAXVALUE 99999999999999 
    MINVALUE 1 
    CYCLE 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_GID_PROFILE_SEQ';
CREATE SEQUENCE ALC_GID_PROFILE_SEQ 
    INCREMENT BY 1 
    MAXVALUE 99999999999999 
    MINVALUE 1 
    CYCLE 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_IDEAL_WEEKS_OF_SUPPLY_SEQ';
CREATE SEQUENCE ALC_IDEAL_WEEKS_OF_SUPPLY_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_ITEM_LOC_EXCLUSION_SEQ';
CREATE SEQUENCE ALC_ITEM_LOC_EXCLUSION_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 10000 
;


PROMPT CREATING SEQUENCE 'ALC_ITEM_LOC_SEQ';
CREATE SEQUENCE ALC_ITEM_LOC_SEQ 
    INCREMENT BY 1 
    MAXVALUE 9999999999999999999999999999 
    MINVALUE 1 
    CACHE 10000 
;


PROMPT CREATING SEQUENCE 'ALC_ITEM_LOC_TEMP_SEQ';
CREATE SEQUENCE ALC_ITEM_LOC_TEMP_SEQ 
    INCREMENT BY 1 
    MAXVALUE 99999999999999 
    MINVALUE 1 
    CYCLE 
    CACHE 20 
;


PROMPT CREATING SEQUENCE 'ALC_ITEM_PARENT_LOC_SEQ';
CREATE SEQUENCE ALC_ITEM_PARENT_LOC_SEQ 
    INCREMENT BY 1 
    MAXVALUE 99999999999999 
    MINVALUE 1 
    CYCLE 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_ITEM_SOURCE_SEQ';
CREATE SEQUENCE ALC_ITEM_SOURCE_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_LOCATION_SEQ';
CREATE SEQUENCE ALC_LOCATION_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 10000 
;


PROMPT CREATING SEQUENCE 'ALC_LOC_GROUP_DETAIL_SEQ';
CREATE SEQUENCE ALC_LOC_GROUP_DETAIL_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_LOC_GROUP_SEQ';
CREATE SEQUENCE ALC_LOC_GROUP_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_PLAN_SEQ';
CREATE SEQUENCE ALC_PLAN_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_PREPACK_CALC_RESULTS_SEQ';
CREATE SEQUENCE ALC_PREPACK_CALC_RESULTS_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_PREPACK_SET_ITEM_SEQ';
CREATE SEQUENCE ALC_PREPACK_SET_ITEM_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_PREPACK_SET_SEQ';
CREATE SEQUENCE ALC_PREPACK_SET_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_QUANTITY_LIMITS_SEQ';
CREATE SEQUENCE ALC_QUANTITY_LIMITS_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 10000 
;


PROMPT CREATING SEQUENCE 'ALC_QUEUE_SEQ';
CREATE SEQUENCE ALC_QUEUE_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_RECEIPT_PLAN_SEQ';
CREATE SEQUENCE ALC_RECEIPT_PLAN_SEQ 
    INCREMENT BY 1 
    MAXVALUE 99999999999999 
    MINVALUE 1 
    CYCLE 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_RULE_DATE_SEQ';
CREATE SEQUENCE ALC_RULE_DATE_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_RULE_MANY_TO_ONE_SEQ';
CREATE SEQUENCE ALC_RULE_MANY_TO_ONE_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_RULE_SEQ';
CREATE SEQUENCE ALC_RULE_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_SCHEDULE_SEQ';
CREATE SEQUENCE ALC_SCHEDULE_SEQ 
    INCREMENT BY 1 
    MAXVALUE 9999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_SESSION_GIDPROFILEIDS_SEQ';
CREATE SEQUENCE ALC_SESSION_GIDPROFILEIDS_SEQ 
    INCREMENT BY 1 
    MAXVALUE 9999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_SESSION_ITEM_LOC_EXCL_SEQ';
CREATE SEQUENCE ALC_SESSION_ITEM_LOC_EXCL_SEQ 
    INCREMENT BY 1 
    MAXVALUE 9999999999999999999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_SESSION_ITEM_LOC_SEQ';
CREATE SEQUENCE ALC_SESSION_ITEM_LOC_SEQ 
    INCREMENT BY 1 
    MAXVALUE 99999999999999 
    MINVALUE 1 
    CYCLE 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_SESSION_QTY_LIMITS_SEQ';
CREATE SEQUENCE ALC_SESSION_QTY_LIMITS_SEQ 
    INCREMENT BY 1 
    MAXVALUE 99999999999999 
    MINVALUE 1 
    CYCLE 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_SHIPPING_SCHEDULE_SEQ';
CREATE SEQUENCE ALC_SHIPPING_SCHEDULE_SEQ 
    INCREMENT BY 1 
    MAXVALUE 9999999999 
    MINVALUE 1 
    CYCLE 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_SIZE_PROFILE_SEQ';
CREATE SEQUENCE ALC_SIZE_PROFILE_SEQ 
    INCREMENT BY 1 
    MAXVALUE 99999999999999999999 
    MINVALUE 1 
    CACHE 10000 
;


PROMPT CREATING SEQUENCE 'ALC_SYNC_PROCESS_ID_SEQ';
CREATE SEQUENCE ALC_SYNC_PROCESS_ID_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999999999 
    MINVALUE 1 
    CYCLE 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_TASK_ID_SEQ';
CREATE SEQUENCE ALC_TASK_ID_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_TEMPLATE_SEQ';
CREATE SEQUENCE ALC_TEMPLATE_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_USERS_SEQ';
CREATE SEQUENCE ALC_USERS_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_USER_DEPTS_SEQ';
CREATE SEQUENCE ALC_USER_DEPTS_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_WHAT_IF_SEQ';
CREATE SEQUENCE ALC_WHAT_IF_SEQ 
    INCREMENT BY 1 
    MAXVALUE 9999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_WORK_ALLOC_SEQ';
CREATE SEQUENCE ALC_WORK_ALLOC_SEQ 
    INCREMENT BY 1 
    MAXVALUE 9999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_WORK_HEADER_SEQ';
CREATE SEQUENCE ALC_WORK_HEADER_SEQ 
    INCREMENT BY 1 
    MAXVALUE 9999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_WORK_ITEM_SOURCE_ALLOC_SEQ';
CREATE SEQUENCE ALC_WORK_ITEM_SOURCE_ALLOC_SEQ 
    INCREMENT BY 1 
    MAXVALUE 9999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_WORK_ITEM_SOURCE_SEQ';
CREATE SEQUENCE ALC_WORK_ITEM_SOURCE_SEQ 
    INCREMENT BY 1 
    MAXVALUE 9999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_WORK_SESSION_ITEM_LOC_SEQ';
CREATE SEQUENCE ALC_WORK_SESSION_ITEM_LOC_SEQ 
    INCREMENT BY 1 
    MAXVALUE 99999999999999 
    MINVALUE 1 
    CYCLE 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_WORK_SESSION_ITEM_SEQ';
CREATE SEQUENCE ALC_WORK_SESSION_ITEM_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999999999 
    MINVALUE 1 
    CYCLE 
    CACHE 5000 
;


PROMPT CREATING SEQUENCE 'ALC_XREF_SEQ';
CREATE SEQUENCE ALC_XREF_SEQ 
    INCREMENT BY 1 
    MAXVALUE 999999999 
    MINVALUE 1 
    CACHE 5000 
;


PROMPT CREATING TABLE 'ALC_ALLOC';
CREATE TABLE ALC_ALLOC 
    ( 
     STATUS                         VARCHAR2 (2)  NOT NULL , 
     ALLOC_ID                       NUMBER (15)  NOT NULL , 
     ALLOC_DESC                     VARCHAR2 (300) , 
     PROCESS_STATUS                 VARCHAR2 (2)  NOT NULL , 
     RULE_TEMPLATE_NAME             VARCHAR2 (300) , 
     LOCATION_TEMPLATE_NAME         VARCHAR2 (300) , 
     ENFORCE_WH_STORE_REL_IND       VARCHAR2 (1)  NOT NULL , 
     LOCKED_BY_USER_ID              VARCHAR2 (64) , 
     LOCKED_TIMESTAMP               NUMBER (38) , 
     NEVER_UPDATE_GROUP_IND         VARCHAR2 (1) DEFAULT 'N'  NOT NULL , 
     CONTEXT                        VARCHAR2 (10) , 
     PROMOTION                      NUMBER (10) , 
     PROMO_DESC                     VARCHAR2 (160) , 
     ALLOC_COMMENT                  VARCHAR2 (2000) , 
     MLD_APPROVAL_LEVEL             VARCHAR2 (2) , 
     RELEASE_DATE_FROM_IN_STORE_IND VARCHAR2 (1) , 
     MLD_MODIFIED_LEVEL             VARCHAR2 (2) , 
     PARENT_ID                      NUMBER (15) , 
     PARENT_ALLOCATION              VARCHAR2 (1) , 
     DEAGGREGATED_FASHION           VARCHAR2 (1) , 
     NON_SELL_FASHION_PACK_ONLY     VARCHAR2 (2) DEFAULT 'N' , 
     CREATED_BY                     VARCHAR2 (64)  NOT NULL , 
     CREATION_DATE                  TIMESTAMP  NOT NULL , 
     LAST_UPDATE_DATE               TIMESTAMP  NOT NULL , 
     LAST_UPDATED_BY                VARCHAR2 (64)  NOT NULL , 
     TYPE                           VARCHAR2 (5) DEFAULT 'SA'  NOT NULL , 
     AUTO_QTY_LIMITS_IND            VARCHAR2 (1) DEFAULT 'N'  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ALLOC';
ALTER TABLE ALC_ALLOC 
    ADD CONSTRAINT CHK_ALC_ALLOC_NV_UPD_GRP_IND 
    CHECK ( NEVER_UPDATE_GROUP_IND IN ('Y', 'N')) 
;

PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ALLOC';
ALTER TABLE ALC_ALLOC 
    ADD CONSTRAINT CHK_ALC_ALLOC_RDFISI 
    CHECK ( RELEASE_DATE_FROM_IN_STORE_IND IN ('Y', 'N')) 
;

PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ALLOC';
ALTER TABLE ALC_ALLOC 
    ADD CONSTRAINT CHK_ALC_ALLOC_MLD_APPROVAL_LEV 
    CHECK (MLD_APPROVAL_LEVEL IN ('ST','T1')) 
;

COMMENT ON TABLE ALC_ALLOC IS 'this table contains a record for all allocations created within oracle retail allocation. this table is the primary place for header level allocation information.'
;


COMMENT ON COLUMN ALC_ALLOC.STATUS IS 'this column contains the status of allocation within oracle retail allocation. valid values for this column are:0 - "WORKSHEET",  1 - "SUBMITTED",   2 - "APPROVED?????,    3 - "PROCESSED",    4 - "CLOSED",    5 - "CANCELLED",    6 - "RESERVED",    7 - "DELETED",    8 - "APPROVED_IN_PROCESS",    9 - "RESERVED_IN_PROCESS",    10 - "PO_CREATED",    11 - "SCHEDULED"' 
;

COMMENT ON COLUMN ALC_ALLOC.ALLOC_ID IS 'This column contains the unique identifier for allocation within the application. this value is derived from the sequence alc_alloc_seq.' 
;

COMMENT ON COLUMN ALC_ALLOC.ALLOC_DESC IS 'This column contains the description of the allocation as created by the user.' 
;

COMMENT ON COLUMN ALC_ALLOC.PROCESS_STATUS IS 'this column contains the calculation status of the allocations.     1, "NOT_CALCULATED"    2, "CALCULATION_WAITING"    3, "CALCULATING"    4, "CALCULATED"    5, "CALCULATE_LATER"    6, "CALCULATION_ERROR"    7, "SIZE_PROFILE_CALCULATION_ERROR"    9, "QUANTITY_LIMITS_CONFLICT"    10, "STATUS_ERROR"    11, "STATUS_WAITING"    12, STATUS_PROCESSING"    13, "STATUS_PROCESSED"    14, "AVAILABLE_INVENTORY_ERROR"    15, "NEXT_DESTINATION_ERROR"    16, "SUPPLY_CHAIN_ERROR"    17, "ITEM_SOURCE_CONFLICT"    18, "SCHEDULED"    19, "SCHEDULE_ERROR"' 
;

COMMENT ON COLUMN ALC_ALLOC.RULE_TEMPLATE_NAME IS 'this column contains the rule template name, if a rule template was used in the creation of the allocation.' 
;

COMMENT ON COLUMN ALC_ALLOC.LOCATION_TEMPLATE_NAME IS 'this column contains the location template name, if a location template was used in the creation of the allocation.' 
;

COMMENT ON COLUMN ALC_ALLOC.ENFORCE_WH_STORE_REL_IND IS 'this column contains an indicator if this allocation must enforce the warehouse/store relationship defined within rms.  When this indicator is selected, stores can only be sources from a valid default warehouse, as defined in the RMS STORE table or the WH_STORE_ASSIGN table.  valid values are:yes = y, no = n' 
;

COMMENT ON COLUMN ALC_ALLOC.LOCKED_BY_USER_ID IS 'this column contains the user_id of the user who has the allocation locked.' 
;

COMMENT ON COLUMN ALC_ALLOC.LOCKED_TIMESTAMP IS 'this column contains the date/time stamp of the lock.' 
;

COMMENT ON COLUMN ALC_ALLOC.NEVER_UPDATE_GROUP_IND IS 'this column contains never update group indicator.' 
;

COMMENT ON COLUMN ALC_ALLOC.CONTEXT IS 'used by rms transfers.  informational field only. this column contains the context information from rms, which maps to code_head and code_detail with code_type as cntx' 
;

COMMENT ON COLUMN ALC_ALLOC.PROMOTION IS 'this column contains the rms promotion number.' 
;

COMMENT ON COLUMN ALC_ALLOC.PROMO_DESC IS 'this column contains rms promotion description.' 
;

COMMENT ON COLUMN ALC_ALLOC.ALLOC_COMMENT IS 'this column contains allocation comments.' 
;

COMMENT ON COLUMN ALC_ALLOC.MLD_APPROVAL_LEVEL IS 'this column contains the mld allocation approval level. this will be null for non mld allocations.' 
;

COMMENT ON COLUMN ALC_ALLOC.RELEASE_DATE_FROM_IN_STORE_IND IS 'this field has a value of y if the release date needs to be calculated based on in store date.' 
;

COMMENT ON COLUMN ALC_ALLOC.MLD_MODIFIED_LEVEL IS 'this column stores the mld level the user modifies on the allocation details screen.' 
;

COMMENT ON COLUMN ALC_ALLOC.PARENT_ID IS 'this column stores the parent id of the allocation.' 
;

COMMENT ON COLUMN ALC_ALLOC.PARENT_ALLOCATION IS 'this column stores information on whether the allocation is a parent allocation or not.' 
;

COMMENT ON COLUMN ALC_ALLOC.DEAGGREGATED_FASHION IS 'This column is no longer used.' 
;

COMMENT ON COLUMN ALC_ALLOC.NON_SELL_FASHION_PACK_ONLY IS 'This column is no longer used.' 
;

COMMENT ON COLUMN ALC_ALLOC.CREATED_BY IS 'ID of user who created the allocation' 
;

COMMENT ON COLUMN ALC_ALLOC.CREATION_DATE IS 'Time allocation was created' 
;

COMMENT ON COLUMN ALC_ALLOC.LAST_UPDATE_DATE IS 'Time allocation was last updated' 
;

COMMENT ON COLUMN ALC_ALLOC.LAST_UPDATED_BY IS 'ID of user who created the allocation' 
;

COMMENT ON COLUMN ALC_ALLOC.TYPE IS 'Refers the Allocation Type, which is set systematically based on the items seleccted and drives rules and logic about available allocation functionality in the application. There can be three values for this field
FA - Fashion Allocation
FPA - Fashion Pack Allocation
FPG - Fashion Pack Grouping Allocation
SA - Staple Allocation.' 
;

COMMENT ON COLUMN ALC_ALLOC.AUTO_QTY_LIMITS_IND IS 'Indicates whether the default auto quantity limits are used in this allocation.' 
;
PROMPT CREATING INDEX 'PK_ALC_ALLOC';
CREATE UNIQUE INDEX PK_ALC_ALLOC ON ALC_ALLOC 
    ( 
     ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
	
PROMPT CREATING INDEX 'ALC_ALLOC_I1';
CREATE INDEX ALC_ALLOC_I1 ON ALC_ALLOC 
    ( 
     PARENT_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_ALLOC';
ALTER TABLE ALC_ALLOC 
    ADD CONSTRAINT PK_ALC_ALLOC PRIMARY KEY ( ALLOC_ID ) 
    USING INDEX PK_ALC_ALLOC ;



PROMPT CREATING TABLE 'ALC_ALLOC_PURGE_HELPER';
CREATE TABLE ALC_ALLOC_PURGE_HELPER 
    ( 
     ALLOC_ID  NUMBER (15)  NOT NULL , 
     THREAD_ID NUMBER (10)  NOT NULL 
    ) 
        INITRANS 12 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_ALLOC_PURGE_HELPER IS 'Helper table used during the allocation purge process.'
;


COMMENT ON COLUMN ALC_ALLOC_PURGE_HELPER.ALLOC_ID IS 'Unique ID of the allocation that will be purged.' 
;

COMMENT ON COLUMN ALC_ALLOC_PURGE_HELPER.THREAD_ID IS 'Thread number of the thread responsible for deleting the allocation.' 
;



PROMPT CREATING TABLE 'ALC_ALLOC_SEARCH_TEMP';
CREATE GLOBAL TEMPORARY TABLE ALC_ALLOC_SEARCH_TEMP 
    ( 
     STATUS                         VARCHAR2 (2) , 
     ALLOC_ID                       NUMBER (15)  NOT NULL , 
     ALLOC_DESC                     VARCHAR2 (300) , 
     PROCESS_STATUS                 VARCHAR2 (2) , 
     RULE_TEMPLATE_NAME             VARCHAR2 (300) , 
     LOCATION_TEMPLATE_NAME         VARCHAR2 (300) , 
     ENFORCE_WH_STORE_REL_IND       VARCHAR2 (1) , 
     LOCKED_BY_USER_ID              VARCHAR2 (64) , 
     LOCKED_TIMESTAMP               NUMBER (38) , 
     NEVER_UPDATE_GROUP_IND         VARCHAR2 (1) , 
     CONTEXT                        VARCHAR2 (10) , 
     PROMOTION                      NUMBER (10) , 
     PROMO_DESC                     VARCHAR2 (160) , 
     ALLOC_COMMENT                  VARCHAR2 (2000) , 
     MLD_APPROVAL_LEVEL             VARCHAR2 (2) , 
     RELEASE_DATE_FROM_IN_STORE_IND VARCHAR2 (1) , 
     MLD_MODIFIED_LEVEL             VARCHAR2 (2) , 
     CODE                           VARCHAR2 (40) , 
     ROW_NUMBER                     NUMBER (10) , 
     PARENT_ID                      NUMBER (15) , 
     CREATED_BY                     VARCHAR2 (64) , 
     CREATION_DATE                  DATE , 
     ALLOC_TYPE                     VARCHAR2 (64) , 
     ALLOC_FLOW                     VARCHAR2 (64) 
    ) 
    ON COMMIT PRESERVE ROWS 
;



COMMENT ON TABLE ALC_ALLOC_SEARCH_TEMP IS 'this table is the global temporary table for oracle retail allocation.'
;


COMMENT ON COLUMN ALC_ALLOC_SEARCH_TEMP.PARENT_ID IS 'stores the parent id of the allocation.' 
;

COMMENT ON COLUMN ALC_ALLOC_SEARCH_TEMP.ALLOC_TYPE IS 'Stores allocation type' 
;

COMMENT ON COLUMN ALC_ALLOC_SEARCH_TEMP.ALLOC_FLOW IS 'Stores the allocation flow' 
;
PROMPT CREATING INDEX 'PK_ALC_ALLOC_SEARCH_TEMP';
CREATE UNIQUE INDEX PK_ALC_ALLOC_SEARCH_TEMP ON ALC_ALLOC_SEARCH_TEMP 
    ( 
     ALLOC_ID ASC 
    ) 
;

PROMPT CREATING PRIMARY KEY ON 'ALC_ALLOC_SEARCH_TEMP';
ALTER TABLE ALC_ALLOC_SEARCH_TEMP 
    ADD CONSTRAINT PK_ALC_ALLOC_SEARCH_TEMP PRIMARY KEY ( ALLOC_ID ) 
    USING INDEX PK_ALC_ALLOC_SEARCH_TEMP ;




PROMPT CREATING TABLE 'ALC_APPROVAL_QUANTITY';
CREATE TABLE ALC_APPROVAL_QUANTITY 
    ( 
     APPROVAL_QUANTITY_ID NUMBER (20)  NOT NULL , 
     ITEM_SOURCE_ID       NUMBER (20)  NOT NULL , 
     DOC_NO               VARCHAR2 (40)  NOT NULL , 
     DOC_TYPE             VARCHAR2 (1)  NOT NULL , 
     QUANTITY             NUMBER (12,4)  NOT NULL , 
     ITEM_ID              VARCHAR2 (40) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_APPROVAL_QUANTITY';
ALTER TABLE ALC_APPROVAL_QUANTITY 
    ADD CONSTRAINT CHK_ALC_APPROVAL_QNTY_DOC_TYPE 
    CHECK ( DOC_TYPE IN ('1', '6', '7')) 
;


COMMENT ON TABLE ALC_APPROVAL_QUANTITY IS 'this table holds the po or transfer details of asn or bol source types on the alc_item_source.'
;


COMMENT ON COLUMN ALC_APPROVAL_QUANTITY.APPROVAL_QUANTITY_ID IS 'This columns contains a unique row identifier. this value is derived from the sequence alc_approval_quantity_seq.' 
;

COMMENT ON COLUMN ALC_APPROVAL_QUANTITY.ITEM_SOURCE_ID IS 'This column contains a unique item source identifier.' 
;

COMMENT ON COLUMN ALC_APPROVAL_QUANTITY.DOC_NO IS 'This column contains a po or transfer number.' 
;

COMMENT ON COLUMN ALC_APPROVAL_QUANTITY.DOC_TYPE IS 'This column contains the document type. possible values are 1 for po and 2 for transfer.' 
;

COMMENT ON COLUMN ALC_APPROVAL_QUANTITY.QUANTITY IS 'This column contains the portion of the available quantity specific to the sku on the document.' 
;

COMMENT ON COLUMN ALC_APPROVAL_QUANTITY.ITEM_ID IS 'This column holds the approval sku level item id.' 
;
PROMPT CREATING INDEX 'PK_ALC_APPROVAL_QUANTITY';
CREATE UNIQUE INDEX PK_ALC_APPROVAL_QUANTITY ON ALC_APPROVAL_QUANTITY 
    ( 
     APPROVAL_QUANTITY_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_APPROVAL_QUANTITY_I1';
CREATE INDEX ALC_APPROVAL_QUANTITY_I1 ON ALC_APPROVAL_QUANTITY 
    ( 
     ITEM_SOURCE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_APPROVAL_QUANTITY';
ALTER TABLE ALC_APPROVAL_QUANTITY 
    ADD CONSTRAINT PK_ALC_APPROVAL_QUANTITY PRIMARY KEY ( APPROVAL_QUANTITY_ID ) 
    USING INDEX PK_ALC_APPROVAL_QUANTITY ;




PROMPT CREATING TABLE 'ALC_AUTO_QUANTITY_LIMITS';
CREATE TABLE ALC_AUTO_QUANTITY_LIMITS 
    ( 
     AUTO_QUANTITY_LIMITS_ID NUMBER (15)  NOT NULL , 
     LOCATION_ID             NUMBER (10)  NOT NULL , 
     DEPT                    NUMBER (4) , 
     CLASS                   NUMBER (4) , 
     SUBCLASS                NUMBER (4) , 
     ITEM_ID                 VARCHAR2 (25) , 
     MIN                     NUMBER (12,4) , 
     MAX                     NUMBER (12,4) , 
     THRESHOLD               NUMBER (12,4) , 
     TREND                   NUMBER (12,4) , 
     WOS                     NUMBER (12,4) , 
     MIN_NEED                NUMBER (12,4) , 
     START_DATE              DATE  NOT NULL , 
     END_DATE                DATE , 
     MIN_PACK                NUMBER (12,4) , 
     MAX_PACK                NUMBER (12,4) , 
     DIFF_1                  VARCHAR2 (10) , 
     DIFF_2                  VARCHAR2 (10) , 
     DIFF_3                  VARCHAR2 (10) , 
     DIFF_4                  VARCHAR2 (10) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA 
    CACHE ;



COMMENT ON TABLE ALC_AUTO_QUANTITY_LIMITS IS 'this table holds default values for quantity limits (alc_quantity_limits) at an item or subclass or class or dept level.'
;


COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.AUTO_QUANTITY_LIMITS_ID IS 'This column contains a unique auto quantity limits identifier. this value is meaningless and is derived from the sequence alc_auto_quantity_limits_seq.' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.LOCATION_ID IS 'this column contains the store identifier for this rule detail.' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.DEPT IS 'Department number' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.CLASS IS 'Class number' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.SUBCLASS IS 'Subclass number' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.ITEM_ID IS 'this column contains the item identifier, if populated.' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.MIN IS 'This column contains the minimum quantity to allocate if populated. this value constrains the allocation to require a minimum value to the specified location.' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.MAX IS 'This column contains the maximum quantity to allocate if populated. this value constrains the allocation to require a maximum value to the specified location.' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.THRESHOLD IS 'This column contains the threshold quantity to allocate if populated. this value forces the allocation to allocate the threshold quantity or nothing at all atthe specified location.' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.TREND IS 'This column, if populated, contains the trend percentage to apply to an allocation. this value modifies the gross need at the specified location by the percentage entered. this value can be a negative number.' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.WOS IS 'This column contains the weeks-of-supply quantity to allocate if populated. a weekly average is calculated from the gross need selection. this average is multiplied by the wos quantity entered and the result is treated as a minimum allocation. example, rule history - select 4 wks = 100, apply 3 wos = 75.' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.MIN_NEED IS 'This column contains the minimum need quantity to allocate. The Min Need value is compared to the Gross Need value (raw or calculated) and whichever is the greater value will be used as the Gross Need for calculating allocated quantity.' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.START_DATE IS 'The date in which the values become valid' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.END_DATE IS 'This column contains the end date for which the quantity limits are effective.' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.DIFF_1 IS 'Value for Style Diff  level of auto quantity limits' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.DIFF_2 IS 'Value for Style Diff  level of auto quantity limits' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.DIFF_3 IS 'Value for Style Diff  level of auto quantity limits' 
;

COMMENT ON COLUMN ALC_AUTO_QUANTITY_LIMITS.DIFF_4 IS 'Value for Style Diff  level of auto quantity limits' 
;
PROMPT CREATING INDEX 'PK_ALC_AUTO_QUANTITY_LIMITS';
CREATE UNIQUE INDEX PK_ALC_AUTO_QUANTITY_LIMITS ON ALC_AUTO_QUANTITY_LIMITS 
    ( 
     AUTO_QUANTITY_LIMITS_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'CHK_AQL_RECORD_UNIQUE';
CREATE UNIQUE INDEX CHK_AQL_RECORD_UNIQUE ON ALC_AUTO_QUANTITY_LIMITS 
    ( 
     LOCATION_ID ASC , 
     DEPT ASC , 
     CLASS ASC , 
     SUBCLASS ASC , 
     ITEM_ID ASC , 
     DIFF_1 ASC , 
     DIFF_2 ASC , 
     DIFF_3 ASC , 
     DIFF_4 ASC , 
     START_DATE ASC 
    ) 
    TABLESPACE RETAIL_DATA;
PROMPT CREATING INDEX 'ALC_AUTO_QUANTITY_LIMITS_I2';
CREATE INDEX ALC_AUTO_QUANTITY_LIMITS_I2 ON ALC_AUTO_QUANTITY_LIMITS 
    ( 
     ITEM_ID ASC , 
     LOCATION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_AUTO_QUANTITY_LIMITS_I1';
CREATE INDEX ALC_AUTO_QUANTITY_LIMITS_I1 ON ALC_AUTO_QUANTITY_LIMITS 
    ( 
     LOCATION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_AUTO_QUANTITY_LIMITS';
ALTER TABLE ALC_AUTO_QUANTITY_LIMITS 
    ADD CONSTRAINT PK_ALC_AUTO_QUANTITY_LIMITS PRIMARY KEY ( AUTO_QUANTITY_LIMITS_ID ) 
    USING INDEX PK_ALC_AUTO_QUANTITY_LIMITS ;


PROMPT CREATING UNIQUE KEY ON 'ALC_AUTO_QUANTITY_LIMITS';
ALTER TABLE ALC_AUTO_QUANTITY_LIMITS 
    ADD CONSTRAINT CHK_AQL_RECORD_UNIQUE UNIQUE ( LOCATION_ID , DEPT , CLASS , SUBCLASS , ITEM_ID , DIFF_1 , DIFF_2 , DIFF_3 , DIFF_4 , START_DATE ) 
    USING INDEX CHK_AQL_RECORD_UNIQUE ;




PROMPT CREATING TABLE 'ALC_CALC_ALLITEMLOC_TEMP';
CREATE TABLE ALC_CALC_ALLITEMLOC_TEMP 
    ( 
     ALLOC_ID           NUMBER (15)  NOT NULL , 
     ITEM_TYPE          VARCHAR2 (10) , 
     SOURCE_ITEM        VARCHAR2 (25) , 
     SOURCE_ITEM_LEVEL  NUMBER (1) , 
     SOURCE_TRAN_LEVEL  NUMBER (1) , 
     SOURCE_PACK_IND    VARCHAR2 (1) , 
     SOURCE_DIFF1_ID    VARCHAR2 (10) , 
     SOURCE_DIFF2_ID    VARCHAR2 (10) , 
     SOURCE_DIFF3_ID    VARCHAR2 (10) , 
     SOURCE_DIFF4_ID    VARCHAR2 (10) , 
     TRAN_ITEM          VARCHAR2 (25) , 
     TRAN_ITEM_LEVEL    NUMBER (1) , 
     TRAN_TRAN_LEVEL    NUMBER (1) , 
     TRAN_PACK_IND      VARCHAR2 (1) , 
     TRAN_DIFF1_ID      VARCHAR2 (10) , 
     TRAN_DIFF2_ID      VARCHAR2 (10) , 
     TRAN_DIFF3_ID      VARCHAR2 (10) , 
     TRAN_DIFF4_ID      VARCHAR2 (10) , 
     DEPT               NUMBER (4) , 
     CLASS              NUMBER (4) , 
     SUBCLASS           NUMBER (4) , 
     TO_LOC             NUMBER (10) , 
     TO_LOC_TYPE        VARCHAR2 (1) , 
     TO_LOC_NAME        VARCHAR2 (150) , 
     SISTER_STORE       NUMBER (10) , 
     ASSIGN_DEFAULT_WH  NUMBER (10) , 
     CLEAR_IND          VARCHAR2 (1) , 
     ITEM_LOC_STATUS    VARCHAR2 (1) , 
     SIZE_PROFILE_QTY   NUMBER (12,4) , 
     TOTAL_PROFILE_QTY  NUMBER (12,4) , 
     STOCK_ON_HAND      NUMBER (12,4) , 
     ON_ORDER           NUMBER (12,4) , 
     ON_ALLOC           NUMBER (12,4) , 
     ALLOC_OUT          NUMBER (12,4) , 
     IN_TRANSIT_QTY     NUMBER (12,4) , 
     NEED_VALUE         NUMBER (20,4) , 
     RLOH_CURRENT_VALUE NUMBER (20,4) , 
     RLOH_FUTURE_VALUE  NUMBER (20,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA         
    PARTITION BY HASH ( ALLOC_ID ) 
    PARTITIONS 128 
;



COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.ALLOC_ID IS 'indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.ITEM_TYPE IS 'The type of the item being allocated.
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_ITEM IS 'The parent level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent item, it will be populated here.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_ITEM_LEVEL IS 'The item level of the item in the SOURCE_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_TRAN_LEVEL IS 'The tran level of the item in the SOURCE_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_PACK_IND IS 'The pack indicator of the item in the SOURCE_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_DIFF1_ID IS ' child item diff1 value of the style if the style diff1 is aggregated.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_DIFF2_ID IS ' child item diff2 value of the style if the style diff2 is aggregated.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_DIFF3_ID IS ' child item diff3 value of the style if the style diff3 is aggregated.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SOURCE_DIFF4_ID IS ' child item diff4 value of the style if the style diff4 is aggregated.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_ITEM IS 'The transaction level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent/diff item, it will be populated by its children.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_ITEM_LEVEL IS 'The item level of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_TRAN_LEVEL IS 'The tran level of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_PACK_IND IS 'The pack indicator of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_DIFF1_ID IS 'The DIFF_1 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_DIFF2_ID IS 'The DIFF_2 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_DIFF3_ID IS 'The DIFF_3 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TRAN_DIFF4_ID IS 'The DIFF_4 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.DEPT IS 'Department number' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.CLASS IS 'Class number' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SUBCLASS IS 'Subclass number' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TO_LOC IS 'The store id the size profile is relevant for.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TO_LOC_TYPE IS 'The type of location being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TO_LOC_NAME IS 'The name of the store being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SISTER_STORE IS 'The sister_store of the store being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.ASSIGN_DEFAULT_WH IS 'The default warehouse of the store.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.CLEAR_IND IS 'this columns contains the indicator if the item is on clearance.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.ITEM_LOC_STATUS IS 'The status of the item/store combination on the ITEM_LOC table.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.SIZE_PROFILE_QTY IS ' the size profile quantity for the child item' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.TOTAL_PROFILE_QTY IS ' the sum of the size profile quantities for a style/color (at item parent aggregate level)' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.STOCK_ON_HAND IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.ALLOC_OUT IS 'The quantity currently allocated from the item/store' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.IN_TRANSIT_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.NEED_VALUE IS 'The calculated need quantity for the item/store.  The method used is based on the allocation policy.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.RLOH_CURRENT_VALUE IS 'The current rule level on hand value.' 
;

COMMENT ON COLUMN ALC_CALC_ALLITEMLOC_TEMP.RLOH_FUTURE_VALUE IS 'The future rule level on hand value.' 
;
PROMPT CREATING INDEX 'ALC_CALC_ALLITEMLOC_TEMP_I1';
CREATE INDEX ALC_CALC_ALLITEMLOC_TEMP_I1 ON ALC_CALC_ALLITEMLOC_TEMP 
    ( 
     ALLOC_ID ASC , 
     TO_LOC ASC , 
     TRAN_ITEM ASC 
    ) 
    LOCAL INITRANS 12 TABLESPACE  RETAIL_INDEX;
	
PROMPT CREATING INDEX 'ALC_CALC_ALLITEMLOC_TEMP_I3';
CREATE INDEX ALC_CALC_ALLITEMLOC_TEMP_I3 ON ALC_CALC_ALLITEMLOC_TEMP 
    ( 
     ALLOC_ID ASC , 
     SIZE_PROFILE_QTY ASC , 
     SOURCE_ITEM ASC 
    ) 
    LOCAL INITRANS 16 TABLESPACE  RETAIL_INDEX;
	
PROMPT CREATING INDEX 'ALC_CALC_ALLITEMLOC_TEMP_I2';
CREATE INDEX ALC_CALC_ALLITEMLOC_TEMP_I2 ON ALC_CALC_ALLITEMLOC_TEMP 
    ( 
     DEPT ASC , 
     CLASS ASC , 
     SUBCLASS ASC , 
     TO_LOC ASC 
    ) 
    LOCAL INITRANS 12 TABLESPACE  RETAIL_INDEX;
	
PROMPT CREATING INDEX 'ALC_CALC_ALLITEMLOC_TEMP_I4';
CREATE INDEX ALC_CALC_ALLITEMLOC_TEMP_I4 ON ALC_CALC_ALLITEMLOC_TEMP 
    ( 
     TRAN_ITEM ASC , 
     ALLOC_ID ASC , 
     TO_LOC ASC 
    ) 
    LOCAL INITRANS 16 TABLESPACE RETAIL_INDEX;



PROMPT CREATING TABLE 'ALC_CALC_DESTINATION_TEMP';
CREATE TABLE ALC_CALC_DESTINATION_TEMP 
    ( 
     ALLOC_ID           NUMBER (15)  NOT NULL , 
     ITEM_TYPE          VARCHAR2 (10) , 
     SOURCE_ITEM        VARCHAR2 (25) , 
     SOURCE_ITEM_LEVEL  NUMBER (1) , 
     SOURCE_TRAN_LEVEL  NUMBER (1) , 
     SOURCE_PACK_IND    VARCHAR2 (1) , 
     SOURCE_DIFF1_ID    VARCHAR2 (10) , 
     SOURCE_DIFF2_ID    VARCHAR2 (10) , 
     SOURCE_DIFF3_ID    VARCHAR2 (10) , 
     SOURCE_DIFF4_ID    VARCHAR2 (10) , 
     TRAN_ITEM          VARCHAR2 (25) , 
     TRAN_ITEM_LEVEL    NUMBER (1) , 
     TRAN_TRAN_LEVEL    NUMBER (1) , 
     TRAN_PACK_IND      VARCHAR2 (1) , 
     TRAN_DIFF1_ID      VARCHAR2 (10) , 
     TRAN_DIFF2_ID      VARCHAR2 (10) , 
     TRAN_DIFF3_ID      VARCHAR2 (10) , 
     TRAN_DIFF4_ID      VARCHAR2 (10) , 
     DEPT               NUMBER (4) , 
     CLASS              NUMBER (4) , 
     SUBCLASS           NUMBER (4) , 
     TO_LOC             NUMBER (10) , 
     TO_LOC_TYPE        VARCHAR2 (1) , 
     TO_LOC_NAME        VARCHAR2 (150) , 
     SISTER_STORE       NUMBER (10) , 
     ASSIGN_DEFAULT_WH  NUMBER (10) , 
     CLEAR_IND          VARCHAR2 (1) , 
     ITEM_LOC_STATUS    VARCHAR2 (1) , 
     SIZE_PROFILE_QTY   NUMBER (12,4) , 
     TOTAL_PROFILE_QTY  NUMBER (12,4) , 
     STOCK_ON_HAND      NUMBER (12,4) , 
     ON_ORDER           NUMBER (12,4) , 
     ON_ALLOC           NUMBER (12,4) , 
     ALLOC_OUT          NUMBER (12,4) , 
     IN_TRANSIT_QTY     NUMBER (12,4) , 
     BACKORDER_QTY      NUMBER (12,4) , 
     NEED_VALUE         NUMBER (20,4) , 
     RLOH_CURRENT_VALUE NUMBER (20,4) , 
     RLOH_FUTURE_VALUE  NUMBER (20,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA         
    PARTITION BY HASH ( ALLOC_ID ) 
    PARTITIONS 128 
;



COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.ALLOC_ID IS 'indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.ITEM_TYPE IS 'The type of the item being allocated.
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_ITEM IS 'The parent level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent item, it will be populated here.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_ITEM_LEVEL IS 'The item level of the item in the SOURCE_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_TRAN_LEVEL IS 'The tran level of the item in the SOURCE_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_PACK_IND IS 'The pack indicator of the item in the SOURCE_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_DIFF1_ID IS ' child item diff1 value of the style if the style diff1 is aggregated.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_DIFF2_ID IS ' child item diff2 value of the style if the style diff2 is aggregated.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_DIFF3_ID IS ' child item diff3 value of the style if the style diff3 is aggregated.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SOURCE_DIFF4_ID IS ' child item diff4 value of the style if the style diff4 is aggregated.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_ITEM IS 'The transaction level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent/diff item, it will be populated by its children.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_ITEM_LEVEL IS 'The item level of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_TRAN_LEVEL IS 'The tran level of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_PACK_IND IS 'The pack indicator of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_DIFF1_ID IS 'The DIFF_1 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_DIFF2_ID IS 'The DIFF_2 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_DIFF3_ID IS 'The DIFF_3 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TRAN_DIFF4_ID IS 'The DIFF_4 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.DEPT IS 'Department number' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.CLASS IS 'Class number' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SUBCLASS IS 'Subclass number' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TO_LOC IS 'The store id the size profile is relevant for.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TO_LOC_TYPE IS 'The type of location being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TO_LOC_NAME IS 'The name of the store being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SISTER_STORE IS 'The sister_store of the store being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.ASSIGN_DEFAULT_WH IS 'The default warehouse of the store.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.CLEAR_IND IS 'this columns contains the indicator if the item is on clearance.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.ITEM_LOC_STATUS IS 'The status of the item/store combination on the ITEM_LOC table.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.SIZE_PROFILE_QTY IS ' the size profile quantity for the child item' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.TOTAL_PROFILE_QTY IS ' the sum of the size profile quantities for a style/color (at item parent aggregate level)' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.STOCK_ON_HAND IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.ALLOC_OUT IS 'The quantity currently allocated from the item/store' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.IN_TRANSIT_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.BACKORDER_QTY IS 'Used to store backorder quantity information used by Allocation Maintenance Item Review section.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.NEED_VALUE IS 'The calculated need quantity for the item/store.  The method used is based on the allocation policy.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.RLOH_CURRENT_VALUE IS 'The current rule level on hand value.' 
;

COMMENT ON COLUMN ALC_CALC_DESTINATION_TEMP.RLOH_FUTURE_VALUE IS 'The future rule level on hand value.' 
;
PROMPT CREATING INDEX 'ALC_CALC_DESTINATION_TEMP_I2';
CREATE INDEX ALC_CALC_DESTINATION_TEMP_I2 ON ALC_CALC_DESTINATION_TEMP 
    ( 
     DEPT ASC , 
     CLASS ASC , 
     SUBCLASS ASC , 
     TO_LOC ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_CALC_DESTINATION_TEMP_I1';
CREATE UNIQUE INDEX ALC_CALC_DESTINATION_TEMP_I1 ON ALC_CALC_DESTINATION_TEMP 
    ( 
     ALLOC_ID ASC , 
     TO_LOC ASC , 
     TRAN_ITEM ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_CALC_DESTINATION_TEMP_I5';
CREATE INDEX ALC_CALC_DESTINATION_TEMP_I5 ON ALC_CALC_DESTINATION_TEMP 
    ( 
     SIZE_PROFILE_QTY ASC , 
     ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 16;
PROMPT CREATING INDEX 'ALC_CALC_DESTINATION_TEMP_I4';
CREATE INDEX ALC_CALC_DESTINATION_TEMP_I4 ON ALC_CALC_DESTINATION_TEMP 
    ( 
     TRAN_ITEM ASC , 
     ALLOC_ID ASC , 
     TO_LOC ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
	
PROMPT CREATING INDEX 'ALC_CALC_DESTINATION_TEMP_I3';
CREATE INDEX ALC_CALC_DESTINATION_TEMP_I3 ON ALC_CALC_DESTINATION_TEMP 
    ( 
     ALLOC_ID ASC , 
     SIZE_PROFILE_QTY ASC , 
     SOURCE_ITEM ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;



PROMPT CREATING TABLE 'ALC_CALC_NEED_DATES_TEMP';
CREATE TABLE ALC_CALC_NEED_DATES_TEMP 
    ( 
     ALLOC_ID            NUMBER (15) , 
     RULE_MANY_TO_ONE_ID NUMBER (20) , 
     EOW_DATE            DATE , 
     WEIGHT              NUMBER (12,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_CALC_NEED_DATES_TEMP IS 'Temporary table used in the calculation process.  This table holds information about the dates that are to be used when looking up need.'
;


COMMENT ON COLUMN ALC_CALC_NEED_DATES_TEMP.ALLOC_ID IS 'The allocation being calculated  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_CALC_NEED_DATES_TEMP.RULE_MANY_TO_ONE_ID IS 'When alternative hierarchies are being use, the ID of the alternate hierarchy row.  When alternative hierarchies are not being used, this will be -1.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_DATES_TEMP.EOW_DATE IS 'The end of week date to consider need on.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_DATES_TEMP.WEIGHT IS 'The weight to apply to the EOW_DATE.' 
;
PROMPT CREATING INDEX 'ALC_CALC_NEED_DATES_TEMP_I1';
CREATE INDEX ALC_CALC_NEED_DATES_TEMP_I1 ON ALC_CALC_NEED_DATES_TEMP 
    ( 
     ALLOC_ID ASC , 
     RULE_MANY_TO_ONE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 16;



PROMPT CREATING TABLE 'ALC_CALC_NEED_TEMP';
CREATE TABLE ALC_CALC_NEED_TEMP 
    ( 
     ALLOC_ID            NUMBER (15)  NOT NULL , 
     RULE_MANY_TO_ONE_ID NUMBER (20) , 
     RULE_LEVEL          NUMBER (1) , 
     DEPT                NUMBER (4) , 
     CLASS               NUMBER (4) , 
     SUBCLASS            NUMBER (4) , 
     SOURCE_ITEM         VARCHAR2 (25) , 
     SOURCE_ITEM_LEVEL   NUMBER (1) , 
     SOURCE_TRAN_LEVEL   NUMBER (1) , 
     SOURCE_PACK_IND     VARCHAR2 (1) , 
     SOURCE_DIFF1_ID     VARCHAR2 (10) , 
     SOURCE_DIFF2_ID     VARCHAR2 (10) , 
     SOURCE_DIFF3_ID     VARCHAR2 (10) , 
     SOURCE_DIFF4_ID     VARCHAR2 (10) , 
     TRAN_ITEM           VARCHAR2 (25) , 
     TRAN_ITEM_LEVEL     NUMBER (1) , 
     TRAN_TRAN_LEVEL     NUMBER (1) , 
     TRAN_PACK_IND       VARCHAR2 (1) , 
     TRAN_DIFF1_ID       VARCHAR2 (10) , 
     TRAN_DIFF2_ID       VARCHAR2 (10) , 
     TRAN_DIFF3_ID       VARCHAR2 (10) , 
     TRAN_DIFF4_ID       VARCHAR2 (10) , 
     PACK_QTY            NUMBER (12,4) , 
     TO_LOC              NUMBER (10) , 
     SISTER_STORE        NUMBER (10) , 
     SIZE_PROFILE_QTY    NUMBER (12,4) , 
     TOTAL_PROFILE_QTY   NUMBER (12,4) , 
     EOW_DATE            DATE , 
     WEIGHT              NUMBER (12,4) , 
     IWOS_WEEKS          NUMBER (12,4) , 
     SALES_HIST_NEED     NUMBER (12,4) , 
     FORECAST_NEED       NUMBER (12,4) , 
     REPLAN_NEED         NUMBER (12,4) , 
     PLAN_NEED           NUMBER (12,4) , 
     PLAN_REPROJECT_NEED NUMBER (12,4) , 
     RECEIPT_PLAN_NEED   NUMBER (12,4) , 
     CORP_RULE_NEED      NUMBER (12,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA        
    PARTITION BY HASH ( ALLOC_ID ) 
    PARTITIONS 128;



COMMENT ON TABLE ALC_CALC_NEED_TEMP IS 'Temporary table used in the calculation process.  This table holds information about the need.  The data in this table is at the hierarchy (dept/class/subclass) level or at the transaction item level depending on the RULE_LEVEL on the ALC_RULE table for the allocation.'
;


COMMENT ON COLUMN ALC_CALC_NEED_TEMP.ALLOC_ID IS 'The allocation being calculated.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.RULE_MANY_TO_ONE_ID IS 'When alternative hierarchies are being use, the ID of the alternate hierarchy row.  When alternative hierarchies are not being used, this will be -1.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.RULE_LEVEL IS 'The rule level from the policy or the level of the alternative hierarchy entry of alternative hierarchies are being used.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.DEPT IS 'The DEPT of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.CLASS IS 'The CLASS of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SUBCLASS IS 'The SUBCLASS of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_ITEM IS 'The item level of the item in the SOURCE_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_ITEM_LEVEL IS 'The tran level of the item in the SOURCE_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_TRAN_LEVEL IS 'The pack indicator of the item in the SOURCE_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_PACK_IND IS 'The DIFF_1 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_1_AGGREGATE_IND is Y.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_DIFF1_ID IS 'The DIFF_2 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_2_AGGREGATE_IND is Y.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_DIFF2_ID IS 'The DIFF_3 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_3_AGGREGATE_IND is Y.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_DIFF3_ID IS 'The DIFF_4 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_4_AGGREGATE_IND is Y.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SOURCE_DIFF4_ID IS 'The DIFF_4 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_4_AGGREGATE_IND is Y.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_ITEM IS 'The transaction level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent/diff item, it will be populated by its children.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_ITEM_LEVEL IS 'The item level of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_TRAN_LEVEL IS 'The tran level of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_PACK_IND IS 'The pack indicator of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_DIFF1_ID IS 'The DIFF_1 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_DIFF2_ID IS 'The DIFF_2 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_DIFF3_ID IS 'The DIFF_3 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TRAN_DIFF4_ID IS 'The DIFF_4 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.PACK_QTY IS 'The qty if the item in a pack.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TO_LOC IS 'The location being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SISTER_STORE IS 'The sister_store of the store being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SIZE_PROFILE_QTY IS 'The sister_store of the store being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.TOTAL_PROFILE_QTY IS 'The calculated total size profile quantity for the item/store.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.EOW_DATE IS 'The end of week date to consider need on.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.WEIGHT IS 'The weight to apply to the EOW_DATE.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.IWOS_WEEKS IS 'The ideal weeks of supply for this calculation.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.SALES_HIST_NEED IS 'The sales history based need if any for the allocation.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.FORECAST_NEED IS 'The forecast based need if any for the allocation.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.REPLAN_NEED IS 'The plan reproject based need if any for the allocation.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.PLAN_NEED IS 'The sales plan based need if any for the allocation.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.PLAN_REPROJECT_NEED IS 'The forecast based need if any for the allocation.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.RECEIPT_PLAN_NEED IS 'The receipt plan based need if any for the allocation.' 
;

COMMENT ON COLUMN ALC_CALC_NEED_TEMP.CORP_RULE_NEED IS 'The corporate rule based need if any for the allocation.' 
;
PROMPT CREATING INDEX 'ALC_CALC_NEED_TEMP_I5';
CREATE INDEX ALC_CALC_NEED_TEMP_I5 ON ALC_CALC_NEED_TEMP 
    ( 
     ALLOC_ID ASC , 
     SOURCE_ITEM ASC , 
     TO_LOC ASC 
    ) 
    LOCAL 
    TABLESPACE RETAIL_INDEX 
    INITRANS 16;
PROMPT CREATING INDEX 'ALC_CALC_NEED_TEMP_I2';
CREATE INDEX ALC_CALC_NEED_TEMP_I2 ON ALC_CALC_NEED_TEMP 
    ( 
     RULE_LEVEL ASC , 
     PLAN_NEED ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_CALC_NEED_TEMP_I3';
CREATE INDEX ALC_CALC_NEED_TEMP_I3 ON ALC_CALC_NEED_TEMP 
    ( 
     RULE_LEVEL ASC , 
     ALLOC_ID ASC , 
     TRAN_ITEM ASC , 
     EOW_DATE ASC 
    ) 
    LOCAL 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_CALC_NEED_TEMP_I1';
CREATE INDEX ALC_CALC_NEED_TEMP_I1 ON ALC_CALC_NEED_TEMP 
    ( 
     ALLOC_ID ASC , 
     TO_LOC ASC , 
     TRAN_ITEM ASC , 
     EOW_DATE ASC 
    ) 
    LOCAL 
    TABLESPACE RETAIL_INDEX 
    INITRANS 16;
PROMPT CREATING INDEX 'ALC_CALC_NEED_TEMP_I4';
CREATE INDEX ALC_CALC_NEED_TEMP_I4 ON ALC_CALC_NEED_TEMP 
    ( 
     ALLOC_ID ASC , 
     TO_LOC ASC , 
     SUBCLASS ASC , 
     CLASS ASC , 
     DEPT ASC , 
     EOW_DATE ASC , 
     RULE_MANY_TO_ONE_ID ASC 
    ) 
    LOCAL 
    TABLESPACE RETAIL_INDEX 
    INITRANS 16;



PROMPT CREATING TABLE 'ALC_CALC_PACK_LEVEL_TEMP';
CREATE TABLE ALC_CALC_PACK_LEVEL_TEMP 
    ( 
     ALLOC_ID          NUMBER (15) , 
     ITEM_TYPE         VARCHAR2 (10) , 
     ITEM              VARCHAR2 (25) , 
     DEPT              NUMBER (4) , 
     CLASS             NUMBER (4) , 
     SUBCLASS          NUMBER (4) , 
     TO_LOC            NUMBER (10) , 
     TO_LOC_TYPE       VARCHAR2 (1) , 
     TO_LOC_NAME       VARCHAR2 (150) , 
     SISTER_STORE      NUMBER (10) , 
     RELEASE_DATE      DATE , 
     ASSIGN_DEFAULT_WH NUMBER (10) , 
     CLEAR_IND         VARCHAR2 (1) , 
     ITEM_LOC_STATUS   VARCHAR2 (1) , 
     NEED_VALUE        NUMBER (20,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.ALLOC_ID IS 'indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.ITEM_TYPE IS 'The type of the item being allocated.
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.ITEM IS 'The item to consider in the RLOH calculation.' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.DEPT IS 'Department number' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.CLASS IS 'Class number' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.SUBCLASS IS 'Subclass number' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.TO_LOC IS 'The location being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.TO_LOC_TYPE IS 'The type of location being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.TO_LOC_NAME IS 'The name of the location being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.SISTER_STORE IS 'The sister_store of the store being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.RELEASE_DATE IS 'this column contains the release date for this item/allocation.' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.ASSIGN_DEFAULT_WH IS 'The default warehouse of the store.' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.CLEAR_IND IS 'this columns contains the indicator if the item is on clearance.' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.ITEM_LOC_STATUS IS 'The status of the item/store combination on the ITEM_LOC table.' 
;

COMMENT ON COLUMN ALC_CALC_PACK_LEVEL_TEMP.NEED_VALUE IS 'The calculated need quantity for the item/store.  The method used is based on the allocation policy.' 
;
PROMPT CREATING INDEX 'ALC_CALC_PACK_LEVEL_TEMP_I1';
CREATE INDEX ALC_CALC_PACK_LEVEL_TEMP_I1 ON ALC_CALC_PACK_LEVEL_TEMP 
    ( 
     ALLOC_ID ASC , 
     TO_LOC ASC , 
     ITEM ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_CALC_PACK_LEVEL_TEMP_I2';
CREATE INDEX ALC_CALC_PACK_LEVEL_TEMP_I2 ON ALC_CALC_PACK_LEVEL_TEMP 
    ( 
     DEPT ASC , 
     CLASS ASC , 
     SUBCLASS ASC , 
     TO_LOC ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_CALC_PACK_LEVEL_TEMP_I3';
CREATE INDEX ALC_CALC_PACK_LEVEL_TEMP_I3 ON ALC_CALC_PACK_LEVEL_TEMP 
    ( 
     ITEM ASC , 
     ALLOC_ID ASC , 
     TO_LOC ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;



PROMPT CREATING TABLE 'ALC_CALC_QTY_LIMITS_TEMP';
CREATE TABLE ALC_CALC_QTY_LIMITS_TEMP 
    ( 
     ALLOC_ID       NUMBER (15) , 
     STORE          NUMBER (10) , 
     ITEM_SOURCE_ID NUMBER (20) , 
     MIN            NUMBER (12,4) , 
     MAX            NUMBER (12,4) , 
     TRESHOLD       NUMBER (12,4) , 
     TREND          NUMBER (12,4) , 
     WOS            NUMBER (12,4) , 
     MIN_NEED       NUMBER (12,4) ,
	 MIN_PACK		NUMBER (12,4) ,
	 MAX_PACK		NUMBER (12,4)
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_CALC_QTY_LIMITS_TEMP IS 'Temporary table used in the calculation process.  This table holds information about quantity limits at the ALC_ITEM_SOURCE level.'
;


COMMENT ON COLUMN ALC_CALC_QTY_LIMITS_TEMP.ALLOC_ID IS 'The allocation being calculated  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_CALC_QTY_LIMITS_TEMP.STORE IS 'The store being allocated to.' 
;

COMMENT ON COLUMN ALC_CALC_QTY_LIMITS_TEMP.ITEM_SOURCE_ID IS 'The ALC_ITEM_SOURCE reference.' 
;

COMMENT ON COLUMN ALC_CALC_QTY_LIMITS_TEMP.MIN IS 'The min quantity limit value.' 
;

COMMENT ON COLUMN ALC_CALC_QTY_LIMITS_TEMP.MAX IS 'The max quantity limit value.' 
;

COMMENT ON COLUMN ALC_CALC_QTY_LIMITS_TEMP.TRESHOLD IS 'The threshold quantity limit value.' 
;

COMMENT ON COLUMN ALC_CALC_QTY_LIMITS_TEMP.TREND IS 'The trend quantity limit value.' 
;

COMMENT ON COLUMN ALC_CALC_QTY_LIMITS_TEMP.WOS IS 'The wos quantity limit value.' 
;

COMMENT ON COLUMN ALC_CALC_QTY_LIMITS_TEMP.MIN_NEED IS 'The min_need quantity limit value.' 
;

COMMENT ON COLUMN ALC_CALC_QTY_LIMITS_TEMP.MIN_PACK is 'The min_pack quantity limit value'
;

COMMENT ON COLUMN ALC_CALC_QTY_LIMITS_TEMP.MAX_PACK is 'The max_pack quantity limit value'
;



PROMPT CREATING TABLE 'ALC_CALC_RLOH_ITEM_TEMP';
CREATE TABLE ALC_CALC_RLOH_ITEM_TEMP 
    ( 
     ALLOC_ID   NUMBER (15) , 
     ITEM       VARCHAR2 (25) , 
     PACK_IND   VARCHAR2 (1) , 
     ITEM_LEVEL NUMBER (1) , 
     TRAN_LEVEL NUMBER (1) , 
     PACK_QTY   NUMBER (12,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_CALC_RLOH_ITEM_TEMP IS 'Temporary table used in the calculation process.  This table holds information about items to be used for RLOH inventory positions when rule level on hand is being used.'
;


COMMENT ON COLUMN ALC_CALC_RLOH_ITEM_TEMP.ALLOC_ID IS 'The allocation being calculated  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_CALC_RLOH_ITEM_TEMP.ITEM IS 'The item to consider in the RLOH calculation.' 
;

COMMENT ON COLUMN ALC_CALC_RLOH_ITEM_TEMP.PACK_IND IS 'The pack indicator of the item.' 
;

COMMENT ON COLUMN ALC_CALC_RLOH_ITEM_TEMP.ITEM_LEVEL IS 'The item level of the item.' 
;

COMMENT ON COLUMN ALC_CALC_RLOH_ITEM_TEMP.TRAN_LEVEL IS 'The tran level of the item.' 
;

COMMENT ON COLUMN ALC_CALC_RLOH_ITEM_TEMP.PACK_QTY IS 'The qty if the item in a pack.' 
;



PROMPT CREATING TABLE 'ALC_CALC_RLOH_TEMP';
CREATE TABLE ALC_CALC_RLOH_TEMP 
    ( 
     ALLOC_ID     NUMBER (15) , 
     ITEM         VARCHAR2 (25) , 
     LOC          NUMBER (10) , 
     PACK_IND     VARCHAR2 (1) , 
     ITEM_LEVEL   NUMBER (1) , 
     TRAN_LEVEL   NUMBER (1) , 
     CURR_AVAIL   NUMBER (13,4) , 
     FUTURE_AVAIL NUMBER (13,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_CALC_RLOH_TEMP IS 'Temporary table used in the calculation process.  This table holds information about RLOH inventory positions when rule level on hand is being used.'
;


COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.ALLOC_ID IS 'The allocation being calculated  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.ITEM IS 'The item to consider in the RLOH calculation.' 
;

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.LOC IS 'The location to consider in the RLOH calculation.' 
;

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.PACK_IND IS 'The pack indicator of the item.' 
;

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.ITEM_LEVEL IS 'The item level of the item.' 
;

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.TRAN_LEVEL IS 'The tran level of the item.' 
;

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.CURR_AVAIL IS 'The current inventory of the item/loc.' 
;

COMMENT ON COLUMN ALC_CALC_RLOH_TEMP.FUTURE_AVAIL IS 'The future inventory of the item/loc.  Based on the on order commit values from ALC_RULE.' 
;
PROMPT CREATING INDEX 'ALC_CALC_RLOH_TEMP_I1';
CREATE INDEX ALC_CALC_RLOH_TEMP_I1 ON ALC_CALC_RLOH_TEMP 
    ( 
     ALLOC_ID ASC , 
     LOC ASC , 
     ITEM ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;



PROMPT CREATING TABLE 'ALC_CALC_SOURCE_TEMP';
CREATE TABLE ALC_CALC_SOURCE_TEMP 
    ( 
     ALLOC_ID          NUMBER (15) , 
     ITEM_SOURCE_ID    NUMBER (20) , 
     RELEASE_DATE      DATE , 
     ITEM_TYPE         VARCHAR2 (10) , 
     SOURCE_ITEM       VARCHAR2 (25) , 
     SOURCE_ITEM_LEVEL NUMBER (1) , 
     SOURCE_TRAN_LEVEL NUMBER (1) , 
     SOURCE_PACK_IND   VARCHAR2 (1) , 
     SOURCE_DIFF1_ID   VARCHAR2 (10) , 
     SOURCE_DIFF2_ID   VARCHAR2 (10) , 
     SOURCE_DIFF3_ID   VARCHAR2 (10) , 
     SOURCE_DIFF4_ID   VARCHAR2 (10) , 
     TRAN_ITEM         VARCHAR2 (25) , 
     TRAN_ITEM_LEVEL   NUMBER (1) , 
     TRAN_TRAN_LEVEL   NUMBER (1) , 
     TRAN_PACK_IND     VARCHAR2 (1) , 
     TRAN_DIFF1_ID     VARCHAR2 (10) , 
     TRAN_DIFF2_ID     VARCHAR2 (10) , 
     TRAN_DIFF3_ID     VARCHAR2 (10) , 
     TRAN_DIFF4_ID     VARCHAR2 (10) , 
     DEPT              NUMBER (4) , 
     CLASS             NUMBER (4) , 
     SUBCLASS          NUMBER (4) ,
	 PACK_NO		   VARCHAR2 (25)
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_CALC_SOURCE_TEMP IS 'Temporary table used in the calculation process.  This table holds information about the data in the ALC_ITEM_SOURCE table.  It explodes the various item types contained on ALC_ITEM_SOURCE down to the transaction level items that they cover.'
;


COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.ALLOC_ID IS 'The allocation being calculated  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.ITEM_SOURCE_ID IS 'The ALC_ITEM_SOURCE reference.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.RELEASE_DATE IS 'The release date from the ALC_ITEM_SOURCE record.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.ITEM_TYPE IS 'The item_type from ALC_ITEM_SOURCE
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_ITEM IS 'The parent level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent item, it will be populated here.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_ITEM_LEVEL IS 'The item level of the item in the SOURCE_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_TRAN_LEVEL IS 'The tran level of the item in the SOURCE_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_PACK_IND IS 'The pack indicator of the item in the SOURCE_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_DIFF1_ID IS 'The DIFF_1 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_1_AGGREGATE_IND is Y.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_DIFF2_ID IS 'The DIFF_2 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_2_AGGREGATE_IND is Y.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_DIFF3_ID IS 'The DIFF_3 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_3_AGGREGATE_IND is Y.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SOURCE_DIFF4_ID IS 'The DIFF_4 value of the of the item in the SOURCE_ITEM column.  This is only populated if the DIFF_4_AGGREGATE_IND is Y.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_ITEM IS 'The transaction level item associated with the ALC_ITEM_SOURCE record.  If ALC_ITEM_SOURCE holds a parent/diff item, it will be populated by its children.  If ALC_ITEM_SOURCE holds a staple or pack item, the staple or pack will be populated here.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_ITEM_LEVEL IS 'The item level of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_TRAN_LEVEL IS 'The tran level of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_PACK_IND IS 'The pack indicator of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_DIFF1_ID IS 'The DIFF_1 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_DIFF2_ID IS 'The DIFF_2 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_DIFF3_ID IS 'The DIFF_3 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.TRAN_DIFF4_ID IS 'The DIFF_4 of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.DEPT IS 'The DEPT of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.CLASS IS 'The CLASS of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.SUBCLASS IS 'The SUBCLASS of the item in the TRAN_ITEM column.' 
;

COMMENT ON COLUMN ALC_CALC_SOURCE_TEMP.PACK_NO is 'The pack associated to the items in the TRAN_ITEM column'
;
PROMPT CREATING INDEX 'ALC_CALC_SOURCE_TEMP_I1';
CREATE INDEX ALC_CALC_SOURCE_TEMP_I1 ON ALC_CALC_SOURCE_TEMP 
    ( 
     ALLOC_ID ASC , 
     TRAN_ITEM ASC , 
     ITEM_SOURCE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_CALC_SOURCE_TEMP_I2';
CREATE INDEX ALC_CALC_SOURCE_TEMP_I2 ON ALC_CALC_SOURCE_TEMP 
    ( 
     RELEASE_DATE ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;



PROMPT CREATING TABLE 'ALC_CODE_DETAIL';
CREATE TABLE ALC_CODE_DETAIL 
    ( 
     CODE_TYPE    VARCHAR2 (4)  NOT NULL , 
     CODE         VARCHAR2 (6)  NOT NULL , 
     CODE_DESC    VARCHAR2 (120)  NOT NULL , 
     REQUIRED_IND VARCHAR2 (1)  NOT NULL , 
     CODE_SEQ     NUMBER (4)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_CODE_DETAIL';
ALTER TABLE ALC_CODE_DETAIL 
    ADD CONSTRAINT ALC_CODE_DETAIL_REQUIRED_IND 
    CHECK (REQUIRED_IND IN ('N', 'Y')) 
;


COMMENT ON TABLE ALC_CODE_DETAIL IS 'this table contains the code and decoded descriptions for each code type defined in the alc_code_head table.  this table will have a foreign key constraint back to the code_type on the alc_code_head table.  all columns are required.'
;


COMMENT ON COLUMN ALC_CODE_DETAIL.CODE_TYPE IS 'this field contains a valid code type for the row. the valid code types are defined in the alc_code_head table. valid values are alphanumeric.' 
;

COMMENT ON COLUMN ALC_CODE_DETAIL.CODE IS 'this field contains the code used in allocations which must be decoded for display in the user interface. valid values are alphanumeric.' 
;

COMMENT ON COLUMN ALC_CODE_DETAIL.CODE_DESC IS 'this filed contains the description associated with the code and code type.' 
;

COMMENT ON COLUMN ALC_CODE_DETAIL.REQUIRED_IND IS 'This field indicates whether or not the code is required. valid values are (Y, N).' 
;

COMMENT ON COLUMN ALC_CODE_DETAIL.CODE_SEQ IS 'This column contains the number used to order elements so that they appear consistently when using them to populate a list. valid values are numbers.' 
;
PROMPT CREATING INDEX 'PK_ALC_CODE_DETAIL';
CREATE UNIQUE INDEX PK_ALC_CODE_DETAIL ON ALC_CODE_DETAIL 
    ( 
     CODE_TYPE ASC , 
     CODE ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_CODE_DETAIL';
ALTER TABLE ALC_CODE_DETAIL 
    ADD CONSTRAINT PK_ALC_CODE_DETAIL PRIMARY KEY ( CODE_TYPE, CODE ) 
    USING INDEX PK_ALC_CODE_DETAIL ;




PROMPT CREATING TABLE 'ALC_CODE_HEAD';
CREATE TABLE ALC_CODE_HEAD 
    ( 
     CODE_TYPE      VARCHAR2 (4)  NOT NULL , 
     CODE_TYPE_DESC VARCHAR2 (120)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_CODE_HEAD IS 'this table contains one row for each different set of codes that are being defined.'
;


COMMENT ON COLUMN ALC_CODE_HEAD.CODE_TYPE IS 'this column contains a three letter description of the code. valid values are alphanumeric.' 
;

COMMENT ON COLUMN ALC_CODE_HEAD.CODE_TYPE_DESC IS 'this column contains the legible value that will be displayed within the system when the user is selecting between code types. valid values are alphanumeric.' 
;
PROMPT CREATING INDEX 'PK_ALC_CODE_HEAD';
CREATE UNIQUE INDEX PK_ALC_CODE_HEAD ON ALC_CODE_HEAD 
    ( 
     CODE_TYPE ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_CODE_HEAD';
ALTER TABLE ALC_CODE_HEAD 
    ADD CONSTRAINT PK_ALC_CODE_HEAD PRIMARY KEY ( CODE_TYPE ) 
    USING INDEX PK_ALC_CODE_HEAD ;




PROMPT CREATING TABLE 'ALC_CORPORATE_RULE_DETAIL';
CREATE TABLE ALC_CORPORATE_RULE_DETAIL 
    ( 
     CORPORATE_RULE_DETAIL_ID NUMBER (15)  NOT NULL , 
     CORPORATE_RULE_ID        NUMBER (10)  NOT NULL , 
     LOCATION_ID              VARCHAR2 (40)  NOT NULL , 
     DEPT                     VARCHAR2 (40) , 
     CLASS                    VARCHAR2 (40) , 
     SUBCLASS                 VARCHAR2 (40) , 
     ITEM_ID                  VARCHAR2 (40) , 
     DIFF1_ID                 VARCHAR2 (40) , 
     DIFF2_ID                 VARCHAR2 (40) , 
     DIFF3_ID                 VARCHAR2 (40) , 
     DIFF4_ID                 VARCHAR2 (40) , 
     NEED_QTY                 NUMBER (12,4)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_CORPORATE_RULE_DETAIL IS 'this table contains a unique corporate rule value for each location of the corporate rules defined in alc_corporate_rule_head. this data is expected to be populated from an external system.'
;


COMMENT ON COLUMN ALC_CORPORATE_RULE_DETAIL.CORPORATE_RULE_DETAIL_ID IS 'this column contains a unique corporate rule detail identifier. this value is this column contains a unique corporate rule detail identifier. this value is' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_DETAIL.CORPORATE_RULE_ID IS 'this column contains a unique corporate rule head identifier. this is associated to the corresponding records in the alc_corporate_rule_head table.' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_DETAIL.LOCATION_ID IS 'this column contains the store identifier for this rule detail.' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_DETAIL.DEPT IS 'this column contains the department identifier, if populated.' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_DETAIL.CLASS IS 'this column contains the class identifier, if populated.' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_DETAIL.SUBCLASS IS 'this column contains the subclass identifier, if populated.' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_DETAIL.ITEM_ID IS 'this column contains the item identifier, if populated.' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_DETAIL.DIFF1_ID IS 'this column contains the diff1 identifier, if populated.' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_DETAIL.DIFF2_ID IS 'this column contains the diff2 identifier, if populated.' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_DETAIL.DIFF3_ID IS 'this column contains the diff3 identifier, if populated.' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_DETAIL.DIFF4_ID IS 'this column contains the diff4 identifier, if populated.' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_DETAIL.NEED_QTY IS 'this column contains the need quantity for the store/rule.' 
;
PROMPT CREATING INDEX 'PK_ALC_CORPORATE_RULE_DETAIL';
CREATE UNIQUE INDEX PK_ALC_CORPORATE_RULE_DETAIL ON ALC_CORPORATE_RULE_DETAIL 
    ( 
     CORPORATE_RULE_DETAIL_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_CORPORATE_RULE_DETAIL_I1';
CREATE INDEX ALC_CORPORATE_RULE_DETAIL_I1 ON ALC_CORPORATE_RULE_DETAIL 
    ( 
     CORPORATE_RULE_ID ASC , 
     LOCATION_ID ASC , 
     DEPT ASC , 
     CLASS ASC , 
     SUBCLASS ASC , 
     ITEM_ID ASC , 
     DIFF1_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_CORPORATE_RULE_DETAIL';
ALTER TABLE ALC_CORPORATE_RULE_DETAIL 
    ADD CONSTRAINT PK_ALC_CORPORATE_RULE_DETAIL PRIMARY KEY ( CORPORATE_RULE_DETAIL_ID ) 
    USING INDEX PK_ALC_CORPORATE_RULE_DETAIL ;




PROMPT CREATING TABLE 'ALC_CORPORATE_RULE_HEAD';
CREATE TABLE ALC_CORPORATE_RULE_HEAD 
    ( 
     CORPORATE_RULE_ID   NUMBER (10)  NOT NULL , 
     CORPORATE_RULE_NAME VARCHAR2 (100)  NOT NULL , 
     NET_NEED_IND        VARCHAR2 (1)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_CORPORATE_RULE_HEAD IS 'this table contains all the possible corporate rules defined within oracle retail allocation. this data is expected to be populated from an external system.'
;


COMMENT ON COLUMN ALC_CORPORATE_RULE_HEAD.CORPORATE_RULE_ID IS 'this column contains a unique corporate rule head identifier. this value is derived from the sequence alc_corporate_rule_head_seq.' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_HEAD.CORPORATE_RULE_NAME IS 'this column contains a description for the corporate rule.' 
;

COMMENT ON COLUMN ALC_CORPORATE_RULE_HEAD.NET_NEED_IND IS 'this column contains the identifier to determine if net need is used in the calculation process. valid values are:yes = y, no = n' 
;
PROMPT CREATING INDEX 'PK_ALC_CORPORATE_RULE_HEAD';
CREATE UNIQUE INDEX PK_ALC_CORPORATE_RULE_HEAD ON ALC_CORPORATE_RULE_HEAD 
    ( 
     CORPORATE_RULE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_CORPORATE_RULE_HEAD';
ALTER TABLE ALC_CORPORATE_RULE_HEAD 
    ADD CONSTRAINT PK_ALC_CORPORATE_RULE_HEAD PRIMARY KEY ( CORPORATE_RULE_ID ) 
    USING INDEX PK_ALC_CORPORATE_RULE_HEAD ;




PROMPT CREATING TABLE 'ALC_FREIGHT_COST';
CREATE TABLE ALC_FREIGHT_COST 
    ( 
     NUMBER_OF_DAYS       NUMBER (3)  NOT NULL , 
     WEIGHT_UOM           VARCHAR2 (4)  NOT NULL , 
     ESTIMATED_COST       NUMBER (20,4)  NOT NULL , 
     ESTIMATED_STORE_COST NUMBER (20,4)  NOT NULL , 
     CURRENCY_CODE        VARCHAR2 (3)  NOT NULL 
    ) 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_FREIGHT_COST IS 'this table holds information such as number of days and estimated cost to calculate the cost of getting an item to a specific location.'
;


COMMENT ON COLUMN ALC_FREIGHT_COST.NUMBER_OF_DAYS IS 'this column contains the number of days in transit.' 
;

COMMENT ON COLUMN ALC_FREIGHT_COST.WEIGHT_UOM IS 'this column contains the unit of measure for the item in transit for calculating freight cost.' 
;

COMMENT ON COLUMN ALC_FREIGHT_COST.ESTIMATED_COST IS 'this column contains the cost per uom of freight.' 
;

COMMENT ON COLUMN ALC_FREIGHT_COST.ESTIMATED_STORE_COST IS 'this column contains the estimated cost per store.' 
;

COMMENT ON COLUMN ALC_FREIGHT_COST.CURRENCY_CODE IS 'this column represents the currency in the estimated_cost column and the estimated_store_cost column.' 
;
PROMPT CREATING INDEX 'PK_ALC_FREIGHT_COST';
CREATE UNIQUE INDEX PK_ALC_FREIGHT_COST ON ALC_FREIGHT_COST 
    ( 
     NUMBER_OF_DAYS ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_FREIGHT_COST';
ALTER TABLE ALC_FREIGHT_COST 
    ADD CONSTRAINT PK_ALC_FREIGHT_COST PRIMARY KEY ( NUMBER_OF_DAYS ) 
    USING INDEX PK_ALC_FREIGHT_COST ;




PROMPT CREATING TABLE 'ALC_GENERATED_PO';
CREATE TABLE ALC_GENERATED_PO 
    ( 
     GENERATED_PO_ID   NUMBER (20)  NOT NULL , 
     ALLOC_ID          NUMBER (15)  NOT NULL , 
     WH_ID             VARCHAR2 (40) , 
     ITEM_ID           VARCHAR2 (40)  NOT NULL , 
     SUPPLIER_ID       VARCHAR2 (40) , 
     ORDER_ID          VARCHAR2 (40)  NOT NULL , 
     ORIGIN_COUNTRY_ID VARCHAR2 (3) , 
     CALC_MULTIPLE     VARCHAR2 (2) , 
     AGGREGATE_DIFF_ID VARCHAR2 (100) 
    ) 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_GENERATED_PO';
ALTER TABLE ALC_GENERATED_PO 
    ADD CONSTRAINT CHK_ALC_GENERATED_PO_CALC_MULT 
    CHECK ( CALC_MULTIPLE IN ('CA', 'EA', 'IN', 'PA')) 
;


COMMENT ON TABLE ALC_GENERATED_PO IS 'this table contains a record of each rms purchase order that has been created from a what-if allocation.'
;


COMMENT ON COLUMN ALC_GENERATED_PO.GENERATED_PO_ID IS 'this column represents the sequence id for the purchase order.' 
;

COMMENT ON COLUMN ALC_GENERATED_PO.ALLOC_ID IS 'indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_GENERATED_PO.WH_ID IS 'this column represents the warehouse number.' 
;

COMMENT ON COLUMN ALC_GENERATED_PO.ITEM_ID IS 'this column represents the item number.' 
;

COMMENT ON COLUMN ALC_GENERATED_PO.SUPPLIER_ID IS 'this column represents the supplier id.' 
;

COMMENT ON COLUMN ALC_GENERATED_PO.ORDER_ID IS 'this column represents the order number id.' 
;

COMMENT ON COLUMN ALC_GENERATED_PO.ORIGIN_COUNTRY_ID IS 'this column represents origin country of the item' 
;

COMMENT ON COLUMN ALC_GENERATED_PO.CALC_MULTIPLE IS 'purchase order multiple of the item. it can be either Inner(IN), Case(C),Palet(P).' 
;

COMMENT ON COLUMN ALC_GENERATED_PO.AGGREGATE_DIFF_ID IS 'this column contains a key of the item parent aggregate diffs. this is for fashion items only.' 
;
PROMPT CREATING INDEX 'PK_ALC_GENERATED_PO';
CREATE UNIQUE INDEX PK_ALC_GENERATED_PO ON ALC_GENERATED_PO 
    ( 
     GENERATED_PO_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;
PROMPT CREATING INDEX 'ALC_GENERATED_PO_I2';
CREATE UNIQUE INDEX ALC_GENERATED_PO_I2 ON ALC_GENERATED_PO 
    ( 
     ALLOC_ID ASC , 
     WH_ID ASC , 
     SUPPLIER_ID ASC , 
     ORDER_ID ASC , 
     ITEM_ID ASC , 
     AGGREGATE_DIFF_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;
PROMPT CREATING INDEX 'ALC_GENERATED_PO_I1';
CREATE INDEX ALC_GENERATED_PO_I1 ON ALC_GENERATED_PO 
    ( 
     ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;

PROMPT CREATING PRIMARY KEY ON 'ALC_GENERATED_PO';
ALTER TABLE ALC_GENERATED_PO 
    ADD CONSTRAINT PK_ALC_GENERATED_PO PRIMARY KEY ( GENERATED_PO_ID ) 
    USING INDEX PK_ALC_GENERATED_PO ;




PROMPT CREATING TABLE 'ALC_GID_HEADER';
CREATE TABLE ALC_GID_HEADER 
    ( 
     ID                    NUMBER (15)  NOT NULL , 
     GID                   VARCHAR2 (20)  NOT NULL , 
     GID_DESC              VARCHAR2 (100)  NOT NULL , 
     CREATED_BY            VARCHAR2 (64)  NOT NULL , 
     CREATION_DATE         TIMESTAMP  NOT NULL , 
     LAST_UPDATED_BY       VARCHAR2 (64)  NOT NULL , 
     LAST_UPDATE_DATE      TIMESTAMP  NOT NULL , 
     LAST_UPDATE_LOGIN     VARCHAR2 (32)  NOT NULL , 
     OBJECT_VERSION_NUMBER NUMBER (9) DEFAULT 1  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON COLUMN ALC_GID_HEADER.ID IS 'This is the primary unique id generated from the sequence.' 
;

COMMENT ON COLUMN ALC_GID_HEADER.GID IS 'This is the GID season code, populated from SPO.' 
;

COMMENT ON COLUMN ALC_GID_HEADER.GID_DESC IS 'The description for the GID' 
;

COMMENT ON COLUMN ALC_GID_HEADER.CREATED_BY IS 'Indicates the user who created the record.' 
;

COMMENT ON COLUMN ALC_GID_HEADER.CREATION_DATE IS 'The timestamp of the record creation date.' 
;

COMMENT ON COLUMN ALC_GID_HEADER.LAST_UPDATED_BY IS 'Indicates the user who last updated the record.' 
;

COMMENT ON COLUMN ALC_GID_HEADER.LAST_UPDATE_DATE IS 'The timestamp of the record last updated date.' 
;

COMMENT ON COLUMN ALC_GID_HEADER.LAST_UPDATE_LOGIN IS 'Indicates the session login associated to the user who last updated the row.' 
;

COMMENT ON COLUMN ALC_GID_HEADER.OBJECT_VERSION_NUMBER IS 'This column indicates Object version number.' 
;
PROMPT CREATING INDEX 'PK_ALC_GID_HEADER';
CREATE UNIQUE INDEX PK_ALC_GID_HEADER ON ALC_GID_HEADER 
    ( 
     ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_GID_HEADER';
ALTER TABLE ALC_GID_HEADER 
    ADD CONSTRAINT PK_ALC_GID_HEADER PRIMARY KEY ( ID ) 
    USING INDEX PK_ALC_GID_HEADER ;




PROMPT CREATING TABLE 'ALC_GID_PROFILE';
CREATE TABLE ALC_GID_PROFILE 
    ( 
     GID_ID                NUMBER (15)  NOT NULL , 
     GID_PROFILE_ID        NUMBER (15)  NOT NULL , 
     CREATED_BY            VARCHAR2 (64)  NOT NULL , 
     CREATION_DATE         TIMESTAMP  NOT NULL , 
     LAST_UPDATED_BY       VARCHAR2 (64)  NOT NULL , 
     LAST_UPDATE_DATE      TIMESTAMP  NOT NULL , 
     LAST_UPDATE_LOGIN     VARCHAR2 (32)  NOT NULL , 
     OBJECT_VERSION_NUMBER NUMBER (9) DEFAULT 1  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON COLUMN ALC_GID_PROFILE.GID_ID IS 'This is the foriegn key to the primary key of  ALC_GID_HEADER table' 
;

COMMENT ON COLUMN ALC_GID_PROFILE.GID_PROFILE_ID IS 'This is the primary unique id generated from the sequence.' 
;

COMMENT ON COLUMN ALC_GID_PROFILE.CREATED_BY IS 'Indicates the user who created the record.' 
;

COMMENT ON COLUMN ALC_GID_PROFILE.CREATION_DATE IS 'The timestamp of the record creation date.' 
;

COMMENT ON COLUMN ALC_GID_PROFILE.LAST_UPDATED_BY IS 'Indicates the user who last updated the record.' 
;

COMMENT ON COLUMN ALC_GID_PROFILE.LAST_UPDATE_DATE IS 'The timestamp of the record last updated date.' 
;

COMMENT ON COLUMN ALC_GID_PROFILE.LAST_UPDATE_LOGIN IS 'Indicates the session login associated to the user who last updated the row.' 
;

COMMENT ON COLUMN ALC_GID_PROFILE.OBJECT_VERSION_NUMBER IS 'This column indicates Object version number.' 
;
PROMPT CREATING INDEX 'ALC_GID_PROFILE_PK';
CREATE UNIQUE INDEX ALC_GID_PROFILE_PK ON ALC_GID_PROFILE 
    ( 
     GID_PROFILE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_GID_PROFILE_I1';
CREATE INDEX ALC_GID_PROFILE_I1 ON ALC_GID_PROFILE 
    ( 
     GID_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_GID_PROFILE';
ALTER TABLE ALC_GID_PROFILE 
    ADD CONSTRAINT ALC_GID_PROFILE_PK PRIMARY KEY ( GID_PROFILE_ID ) 
    USING INDEX ALC_GID_PROFILE_PK ;




PROMPT CREATING TABLE 'ALC_IDEAL_WEEKS_OF_SUPPLY';
CREATE TABLE ALC_IDEAL_WEEKS_OF_SUPPLY 
    ( 
     IDEAL_WEEKS_OF_SUPPLY_ID NUMBER (20)  NOT NULL , 
     LOCATION_ID              VARCHAR2 (40)  NOT NULL , 
     DEPT                     VARCHAR2 (40) , 
     CLASS                    VARCHAR2 (40) , 
     SUBCLASS                 VARCHAR2 (40) , 
     ITEM_ID                  VARCHAR2 (40) , 
     DIFF1_ID                 VARCHAR2 (40) , 
     IWOS_WEEKS               NUMBER (12,4)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_IDEAL_WEEKS_OF_SUPPLY IS 'this table contains a unique ideal weeks-of-supply value for each location. this data is expected to be populated from an external system.'
;


COMMENT ON COLUMN ALC_IDEAL_WEEKS_OF_SUPPLY.IDEAL_WEEKS_OF_SUPPLY_ID IS 'The primary key which is  derived from the sequence alc_ideal_weeks_of_supply_seq.' 
;

COMMENT ON COLUMN ALC_IDEAL_WEEKS_OF_SUPPLY.LOCATION_ID IS 'The store location' 
;

COMMENT ON COLUMN ALC_IDEAL_WEEKS_OF_SUPPLY.DEPT IS 'Department ID' 
;

COMMENT ON COLUMN ALC_IDEAL_WEEKS_OF_SUPPLY.CLASS IS 'Class Id' 
;

COMMENT ON COLUMN ALC_IDEAL_WEEKS_OF_SUPPLY.SUBCLASS IS 'Subclass Id' 
;

COMMENT ON COLUMN ALC_IDEAL_WEEKS_OF_SUPPLY.ITEM_ID IS 'The Item Id' 
;

COMMENT ON COLUMN ALC_IDEAL_WEEKS_OF_SUPPLY.DIFF1_ID IS 'aggregate of all diff Ids' 
;

COMMENT ON COLUMN ALC_IDEAL_WEEKS_OF_SUPPLY.IWOS_WEEKS IS 'this column contains the ideal weeks of supply value.' 
;
PROMPT CREATING INDEX 'PK_ALC_IDEAL_WEEKS_OF_SUPPLY';
CREATE UNIQUE INDEX PK_ALC_IDEAL_WEEKS_OF_SUPPLY ON ALC_IDEAL_WEEKS_OF_SUPPLY 
    ( 
     IDEAL_WEEKS_OF_SUPPLY_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_IDEAL_WEEKS_OF_SUPPLY_I1';
CREATE UNIQUE INDEX ALC_IDEAL_WEEKS_OF_SUPPLY_I1 ON ALC_IDEAL_WEEKS_OF_SUPPLY 
    ( 
     LOCATION_ID ASC , 
     DEPT ASC , 
     CLASS ASC , 
     SUBCLASS ASC , 
     ITEM_ID ASC , 
     DIFF1_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_IDEAL_WEEKS_OF_SUPPLY';
ALTER TABLE ALC_IDEAL_WEEKS_OF_SUPPLY 
    ADD CONSTRAINT PK_ALC_IDEAL_WEEKS_OF_SUPPLY PRIMARY KEY ( IDEAL_WEEKS_OF_SUPPLY_ID ) 
    USING INDEX PK_ALC_IDEAL_WEEKS_OF_SUPPLY ;




PROMPT CREATING TABLE 'ALC_ITEMSEARCH_INV_GTT';
CREATE GLOBAL TEMPORARY TABLE ALC_ITEMSEARCH_INV_GTT 
    ( 
     ITEM                 VARCHAR2 (25)  NOT NULL , 
     DIFF_1               VARCHAR2 (10) , 
     DIFF_2               VARCHAR2 (10) , 
     DIFF_3               VARCHAR2 (10) , 
     DIFF_4               VARCHAR2 (10) , 
     ITEM_DESC            VARCHAR2 (250) , 
     ROLLUP_TYPE          VARCHAR2 (20) , 
     ROLLUP_ITEM          VARCHAR2 (25) , 
     ROLLUP_ITEM_DESC     VARCHAR2 (250) , 
     MULTI_COLOR_PACK_IND VARCHAR2 (1) , 
     LOCATION             NUMBER (10)  NOT NULL , 
     DOC_NO               VARCHAR2 (30) , 
     SOURCE_TYPE          VARCHAR2 (5)  NOT NULL , 
     AVAIL_QTY            NUMBER (12,4)  NOT NULL , 
     BACKORDER_QTY        NUMBER (12,4) 
    ) 
    ON COMMIT DELETE ROWS 
;



COMMENT ON TABLE ALC_ITEMSEARCH_INV_GTT IS 'Internal helper table for the ALC_ITEM_SEARCH_SQL package.'
;


COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.ITEM IS 'The item for the inventory lookup.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.DIFF_1 IS 'The diff_1 of the item.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.DIFF_2 IS 'The diff_2 of the item.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.DIFF_3 IS 'The diff_3 of the item.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.DIFF_4 IS 'The diff_4 of the item.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.ITEM_DESC IS 'The description of the item.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.ROLLUP_TYPE IS 'The type of the item family.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.ROLLUP_ITEM IS 'The highest level item in the family.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.ROLLUP_ITEM_DESC IS 'The description of the highest level item in the family.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.MULTI_COLOR_PACK_IND IS 'Indicates if the item is a multi color pack or not.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.LOCATION IS 'The location for the inventory lookup.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.DOC_NO IS 'The document for the inventory lookup.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.SOURCE_TYPE IS 'The type of inventory lookup being performed which could be  1- PO, 2-ASO, 3-warehouse, 4-Whatif, 5-BOL, 6-TSF.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.AVAIL_QTY IS 'The inventory value.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_INV_GTT.BACKORDER_QTY IS 'The backorder value for the item/location combination.' 
;



PROMPT CREATING TABLE 'ALC_ITEMSEARCH_ITEMS_GTT';
CREATE GLOBAL TEMPORARY TABLE ALC_ITEMSEARCH_ITEMS_GTT 
    ( 
     ITEM                 VARCHAR2 (25) , 
     LOCATION             NUMBER (10) , 
     ROLLUP_TYPE          VARCHAR2 (20) , 
     ROLLUP_ITEM          VARCHAR2 (25) , 
     ROLLUP_ITEM_DESC     VARCHAR2 (250) , 
     MULTI_COLOR_PACK_IND VARCHAR2 (1) 
    ) 
    ON COMMIT DELETE ROWS 
;



COMMENT ON TABLE ALC_ITEMSEARCH_ITEMS_GTT IS 'Working table to help the PLSQL item search logic.  This table is used to hold the transaction level item warehouse combinations that meet the search criteria.'
;


COMMENT ON COLUMN ALC_ITEMSEARCH_ITEMS_GTT.ITEM IS 'Transaction level items that meet the search criteria.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_ITEMS_GTT.LOCATION IS 'Wh locations that meet the search criteria.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_ITEMS_GTT.ROLLUP_TYPE IS 'The type of top level item that the transaction level item belongs to.  Possible values are STYLE, STAPLE, SELLPACK, NONSELLPACK' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_ITEMS_GTT.ROLLUP_ITEM IS 'The value of the top level item that the item belongs under.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_ITEMS_GTT.ROLLUP_ITEM_DESC IS 'The description of the top level item.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_ITEMS_GTT.MULTI_COLOR_PACK_IND IS 'For pack items, indicates if it has multiple colors.' 
;



PROMPT CREATING TABLE 'ALC_ITEMSEARCH_ITEMS_RES_GTT';
CREATE GLOBAL TEMPORARY TABLE ALC_ITEMSEARCH_ITEMS_RES_GTT 
    ( 
     ROLLUP_TYPE      VARCHAR2 (20) , 
     ROLLUP_ITEM      VARCHAR2 (25) , 
     ROLLUP_ITEM_DESC VARCHAR2 (250) 
    ) 
    ON COMMIT DELETE ROWS 
;



COMMENT ON TABLE ALC_ITEMSEARCH_ITEMS_RES_GTT IS 'Working table used in item search.  This table is used to pass the top level items that meet the search criteria and have quantity available to allocate from the PLSQL search code to the java layer.'
;


COMMENT ON COLUMN ALC_ITEMSEARCH_ITEMS_RES_GTT.ROLLUP_TYPE IS 'The type of top level item that the transaction level item belongs to.  Possible values are STYLE, STAPLE, SELLPACK, NONSELLPACK' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_ITEMS_RES_GTT.ROLLUP_ITEM IS 'The value of the top level item that the item belongs under.' 
;

COMMENT ON COLUMN ALC_ITEMSEARCH_ITEMS_RES_GTT.ROLLUP_ITEM_DESC IS 'The description of the top level item.' 
;



PROMPT CREATING TABLE 'ALC_ITEMSEARCH_WHS_GTT';
CREATE GLOBAL TEMPORARY TABLE ALC_ITEMSEARCH_WHS_GTT 
    ( 
     WH NUMBER (10) 
    ) 
    ON COMMIT DELETE ROWS 
;



COMMENT ON TABLE ALC_ITEMSEARCH_WHS_GTT IS 'Helper table for the item search plsql logic.  Holds the WHs that are valid to be included in the search results.'
;


COMMENT ON COLUMN ALC_ITEMSEARCH_WHS_GTT.WH IS 'WHs that are valid to be returned by the search.' 
;



PROMPT CREATING TABLE 'ALC_ITEM_LOC';
CREATE TABLE ALC_ITEM_LOC 
    ( 
     ITEM_LOC_ID         NUMBER (25)  NOT NULL , 
     ALLOC_ID            NUMBER (15)  NOT NULL , 
     ITEM_ID             VARCHAR2 (60)  NOT NULL , 
     WH_ID               VARCHAR2 (40)  NOT NULL , 
     RELEASE_DATE        DATE  NOT NULL , 
     LOCATION_ID         VARCHAR2 (40)  NOT NULL , 
     LOCATION_DESC       VARCHAR2 (150)  NOT NULL , 
     ALLOCATED_QTY       NUMBER (12,4) , 
     CALCULATED_QTY      NUMBER (12,4) , 
     NEED_QTY            NUMBER (12,4) , 
     TOTAL_ON_HAND_QTY   NUMBER (12,4) , 
     SOM_QTY             NUMBER (12,4) , 
     BACKORDER_QTY       NUMBER (12,4) , 
     FREEZE_IND          VARCHAR2 (1)  NOT NULL , 
     DIFF1_ID            VARCHAR2 (100) , 
     DIFF1_DESC          VARCHAR2 (120) , 
     DIFF2_ID            VARCHAR2 (100) , 
     DIFF2_DESC          VARCHAR2 (120) , 
     PARENT_ITEM_ID      VARCHAR2 (40) , 
     CREATED_ORDER_NO    VARCHAR2 (40) , 
     CREATED_SUPPLIER_ID VARCHAR2 (40) , 
     PARENT_DIFF1_ID     VARCHAR2 (100) , 
     FUTURE_UNIT_RETAIL  NUMBER (20,4) , 
     RUSH_FLAG           VARCHAR2 (1) , 
     COST                NUMBER (10,4) , 
     IN_STORE_DATE       DATE , 
     ORDER_NO            VARCHAR2 (40) , 
     SOURCE_TYPE         NUMBER (1)  NOT NULL , 
     GROSS_NEED_QTY      NUMBER (12,4) DEFAULT 0 , 
     RLOH_QTY            NUMBER (12,4) DEFAULT 0 , 
     ITEM_DESC           VARCHAR2 (250) , 
     ITEM_TYPE           VARCHAR2 (10) , 
     DIFF3_ID            VARCHAR2 (10) , 
     DIFF3_DESC          VARCHAR2 (120) , 
     STOCK_ON_HAND       NUMBER (12,4) , 
     IN_TRANSIT          NUMBER (12,4) , 
     ON_ORDER            NUMBER (12,4) , 
     ON_ALLOC            NUMBER (12,4) , 
     LOC_TYPE            VARCHAR2 (1) , 
     ALLOC_OUT           NUMBER (12,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ITEM_LOC';
ALTER TABLE ALC_ITEM_LOC 
    ADD CONSTRAINT CHK_ALC_ITEM_LOC_RUSH_FLAG 
    CHECK (RUSH_FLAG IN ('Y','N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ITEM_LOC';
ALTER TABLE ALC_ITEM_LOC 
    ADD CONSTRAINT CHK_ALC_ITEM_LOC_SOURCE_TYPE 
    CHECK (SOURCE_TYPE IN ('1','2','3','4','5','6','7')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ITEM_LOC';
ALTER TABLE ALC_ITEM_LOC 
    ADD CONSTRAINT CHK_ALC_ITEM_LOC_LOC_TYP 
    CHECK (LOC_TYPE IN ('S','W')) 
;


COMMENT ON TABLE ALC_ITEM_LOC IS 'this table contains the allocation values for each unique item/location on the allocation. this data is created via the calculation process and updated by the user.'
;


COMMENT ON COLUMN ALC_ITEM_LOC.ITEM_LOC_ID IS 'this column contains a unique item location identifier. this value is derived from the sequence alc_item_loc_seq.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.ALLOC_ID IS 'this column contains the unique identifier for the allocation within the application.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.ITEM_ID IS 'this column contains the item identifier.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.WH_ID IS 'this column contains the warehouse identifier.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.RELEASE_DATE IS 'this column contains the release date for this item/warehouse.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.LOCATION_ID IS 'this column contains the store identifier.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.LOCATION_DESC IS 'this column contains the store description.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.ALLOCATED_QTY IS 'this column contains the allocated quantity for this item/warehouse/release date/store. this quantity will be a whole number.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.CALCULATED_QTY IS 'this column contains the calculated allocation quantity derived from the algorithm. this number will be zero for manual allocations.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.NEED_QTY IS 'this column contains the need quantity derived from the algorithm. this value would be known as the gross need. this quantity can be a number that contains decimals.this number will be zero for manual allocations.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.TOTAL_ON_HAND_QTY IS 'this column contains the stock on hand for this item/store. this number will be zero for manual allocations.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.SOM_QTY IS 'this column contains the store order multiple for this item.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.BACKORDER_QTY IS 'Used to store backorder quantity information used by Allocation Maintenance Results section.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.FREEZE_IND IS 'this column contains the freeze values indicator. valid values are:yes = yno = n' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.DIFF1_ID IS 'this column would contain the diff1 identifier, if populated.this field will be populated for fashion items.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.DIFF1_DESC IS 'this column would contain the diff1 description, if diff1 identifier is populated.this field will be populated for fashion items.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.DIFF2_ID IS 'this column would contain the diff2 identifier, if populated.this field will be populated for fashion items that have multiple diffs.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.DIFF2_DESC IS 'this column would contain the diff2 description, if diff2 identifier is populated.this field will be populated for fashion items that have multiple diffs.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.PARENT_ITEM_ID IS 'this column would contain the parent item identifier, if populated.this field will be populated for fashion items.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.CREATED_ORDER_NO IS 'this column will contain the purchase order identifier, if populated.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.CREATED_SUPPLIER_ID IS 'this column will contain the supplier identifier, if populated.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.PARENT_DIFF1_ID IS 'this column would contain the parent item diff1 identifier, if populated.this field will be populated for fashion items.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.FUTURE_UNIT_RETAIL IS 'contains the future unit retail price in the standard unit of measure for the item/location/release date. this field is stored in the local currency' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.RUSH_FLAG IS 'indicates the item need to be rushed for to the location corresponding to itemloc' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.COST IS 'cost of freight' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.IN_STORE_DATE IS 'the date on which the freight need to reach the location' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.ORDER_NO IS 'po or asn number item is sourced from.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.SOURCE_TYPE IS 'This column is used to determain the Source type of the item and works with the order_no and wh_id column, Valid values are 1- PO, 2-ASO, 3-warehouse, 4-Whatif, 5-BOL, 6-TSF' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.GROSS_NEED_QTY IS 'holds gross need value' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.RLOH_QTY IS 'holds rule level on hand value' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.ITEM_DESC IS 'Item Description.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.ITEM_TYPE IS 'ITEM TYPES - FashionSKU, ST,FA,SELLPACK,PACKCOMP,STYLE,NSFSP,, NSFMCP, NSFSCP,NSSSP, NSSCP.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.DIFF3_ID IS 'Aggregated Dif 3 ID of the Item' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.DIFF3_DESC IS 'Diff3 description.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.STOCK_ON_HAND IS 'STOCK_ON_HAND is one of four values that will make up TOTAL_ON_HAND_QTY.  Break out across on hand bucket is shown in the hover feature.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.IN_TRANSIT IS 'IN_TRANSIT is one of four values that will make up TOTAL_ON_HAND_QTY.    Break out across on hand bucket is shown in the hover feature.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.ON_ORDER IS 'ON_ORDER is one of four values that will make up TOTAL_ON_HAND_QTY.    Break out across on hand bucket is shown in the hover feature.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.ON_ALLOC IS 'ON_ALLOC is one of four values that will make up TOTAL_ON_HAND_QTY.   Break out across on hand bucket is shown in the hover feature.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.LOC_TYPE IS 'Determines whether the location is a store or warehouse. Valid values are S and W' 
;

COMMENT ON COLUMN ALC_ITEM_LOC.ALLOC_OUT IS 'The quantity currently allocated from the item/store' 
;
PROMPT CREATING INDEX 'PK_ALC_ITEM_LOC';
CREATE UNIQUE INDEX PK_ALC_ITEM_LOC ON ALC_ITEM_LOC 
    ( 
     ITEM_LOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
	
PROMPT CREATING PRIMARY KEY ON 'ALC_ITEM_LOC';
ALTER TABLE ALC_ITEM_LOC ADD CONSTRAINT PK_ALC_ITEM_LOC PRIMARY KEY ( ITEM_LOC_ID ) USING INDEX PK_ALC_ITEM_LOC ;


PROMPT CREATING INDEX 'ALC_ITEM_LOC_I4';
CREATE INDEX ALC_ITEM_LOC_I4 ON ALC_ITEM_LOC 
    ( 
     ALLOC_ID ASC , 
     ITEM_TYPE ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 16;
	
PROMPT CREATING INDEX 'ALC_ITEM_LOC_I3';
CREATE INDEX ALC_ITEM_LOC_I3 ON ALC_ITEM_LOC 
    ( 
     ALLOC_ID,ITEM_TYPE,LOCATION_ID,WH_ID,SOURCE_TYPE,NVL("ORDER_NO",'-999'),NVL(SUBSTR("DIFF1_ID",INSTR("DIFF1_ID",'~')+1),'-9999999999'),NVL(SUBSTR("DIFF2_ID",INSTR("DIFF2_ID",'~')+1),'-9999999999'),ITEM_ID
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_ITEM_LOC_I2';
CREATE INDEX ALC_ITEM_LOC_I2 ON ALC_ITEM_LOC 
    ( 
     LOCATION_ID ASC , 
     ITEM_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_ITEM_LOC_I1';
CREATE UNIQUE INDEX ALC_ITEM_LOC_I1 ON ALC_ITEM_LOC 
    ( 
     ALLOC_ID ASC , 
     ITEM_ID ASC , 
     WH_ID ASC , 
     RELEASE_DATE ASC , 
     LOCATION_ID ASC , 
     ORDER_NO ASC , 
     SOURCE_TYPE ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;


PROMPT CREATING TABLE 'ALC_ITEM_LOC_EXCLUSION';
CREATE TABLE ALC_ITEM_LOC_EXCLUSION 
    ( 
     ITEM_LOC_EXCLUSION_ID NUMBER (25)  NOT NULL , 
     ALLOC_ID              NUMBER (15)  NOT NULL , 
     ITEM_ID               VARCHAR2 (40)  NOT NULL , 
     ITEM_DESC             VARCHAR2 (440)  NOT NULL , 
     LOCATION_ID           VARCHAR2 (40)  NOT NULL , 
     LOCATION_DESC         VARCHAR2 (150)  NOT NULL , 
     REASON_CODE           NUMBER (5)  NOT NULL , 
     DIFF1_ID              VARCHAR2 (100) , 
     SOURCE_LOCATION_ID    VARCHAR2 (40) , 
     ORDER_NO              VARCHAR2 (40) , 
     SOURCE_TYPE           NUMBER (1)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_ITEM_LOC_EXCLUSION IS 'holds the item location exception information'
;


COMMENT ON COLUMN ALC_ITEM_LOC_EXCLUSION.ITEM_LOC_EXCLUSION_ID IS 'indicates the primary key which gets generated by the sequence.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_EXCLUSION.ALLOC_ID IS 'indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_EXCLUSION.ITEM_ID IS 'The item Id. Populated from item master table.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_EXCLUSION.ITEM_DESC IS 'The item description. Populated from item master table.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_EXCLUSION.LOCATION_ID IS 'The location Id.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_EXCLUSION.LOCATION_DESC IS 'The location description' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_EXCLUSION.REASON_CODE IS 'Reason code that caused the location exception
1 - MLD exclusion.
2 - Item/Location status exclusion.
3 - Allocation split exclusion.
4 - MLD Supply chain exclusion.
5 - Stop shipment exclusion.
6 - Store closed exclusion.
7 - Size profile exclusion.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_EXCLUSION.DIFF1_ID IS 'aggregate of all diff ids' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_EXCLUSION.SOURCE_LOCATION_ID IS 'the source location id is the source location for the item/location being excluded.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_EXCLUSION.ORDER_NO IS 'The inventory transaction for the row.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_EXCLUSION.SOURCE_TYPE IS 'The inventory source for the row.  PO=1,ASN=2,OH=3 (Warehouse Sourced),WHATIF=4,BOL=5, AND TSF=6.' 
;
PROMPT CREATING INDEX 'PK_ALC_ITEM_LOC_EXCLUSION';
CREATE UNIQUE INDEX PK_ALC_ITEM_LOC_EXCLUSION ON ALC_ITEM_LOC_EXCLUSION 
    ( 
     ITEM_LOC_EXCLUSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_ITEM_LOC_EXCLUSION_I1';
CREATE INDEX ALC_ITEM_LOC_EXCLUSION_I1 ON ALC_ITEM_LOC_EXCLUSION 
    ( 
     ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_ITEM_LOC_EXCLUSION';
ALTER TABLE ALC_ITEM_LOC_EXCLUSION 
    ADD CONSTRAINT PK_ALC_ITEM_LOC_EXCLUSION PRIMARY KEY ( ITEM_LOC_EXCLUSION_ID ) 
    USING INDEX PK_ALC_ITEM_LOC_EXCLUSION ;




PROMPT CREATING TABLE 'ALC_ITEM_LOC_TEMP';
CREATE GLOBAL TEMPORARY TABLE ALC_ITEM_LOC_TEMP 
    ( 
     SOURCE_ID         VARCHAR2 (40) , 
     STORE             NUMBER (10) , 
     CLEAR_IND         VARCHAR2 (1) , 
     SOURCE_DIFF1_ID   VARCHAR2 (40) , 
     SOURCE_DIFF2_ID   VARCHAR2 (40) , 
     SOURCE_DIFF3_ID   VARCHAR2 (40) , 
     SOURCE_DIFF4_ID   VARCHAR2 (40) , 
     DEPT              VARCHAR2 (40) , 
     CLASS             VARCHAR2 (40) , 
     SUBCLASS          VARCHAR2 (40) , 
     CHILD_ID          VARCHAR2 (25) , 
     CHILD_DIFF1_ID    VARCHAR2 (40) , 
     CHILD_DIFF2_ID    VARCHAR2 (40) , 
     CHILD_DIFF3_ID    VARCHAR2 (40) , 
     CHILD_DIFF4_ID    VARCHAR2 (40) , 
     SIZE_PROFILE_QTY  NUMBER (12,4) , 
     TOTAL_PROFILE_QTY NUMBER (12,4) , 
     RUSH_FLAG         VARCHAR2 (1) , 
     COST              NUMBER (20,4) , 
     IN_STORE_DATE     DATE , 
     ITEM_LEVEL        NUMBER (1) , 
     TRAN_LEVEL        NUMBER (1) , 
     PACK_IND          VARCHAR2 (1) , 
     STOCK_ON_HAND     NUMBER (12,4) , 
     IN_TRANSIT        NUMBER (12,4) , 
     ON_ORDER          NUMBER (12,4) , 
     ON_ALLOC          NUMBER (12,4) , 
     ALLOC_OUT         NUMBER (12,4) , 
     IN_TRANSIT_QTY    NUMBER (12,4) , 
     TSF_EXPECTED_QTY  NUMBER (12,4) , 
     ITEM_LOC_TEMP_ID  NUMBER (10) 
    ) 
    ON COMMIT PRESERVE ROWS 
;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ITEM_LOC_TEMP';
ALTER TABLE ALC_ITEM_LOC_TEMP ADD CONSTRAINT CHK_ALC_ITEM_LOC_TEMP_RUSH_FLA CHECK (RUSH_FLAG IN ('Y','N')) 
;


COMMENT ON TABLE ALC_ITEM_LOC_TEMP IS 'this temporary table is used in the process of calculating allocations. no data will be permanently kept on this table.'
;


COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.SOURCE_ID IS 'this column contains the style id for fashion allocation, sku id for staple allocations' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.STORE IS 'the columns contains the store identifier' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.CLEAR_IND IS 'this columns contains the indicator if the item is on clearance.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.SOURCE_DIFF1_ID IS ' child item diff1 value of the style if the style diff1 is aggregated.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.SOURCE_DIFF2_ID IS ' child item diff2 value of the style if the style diff2 is aggregated.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.SOURCE_DIFF3_ID IS ' child item diff3 value of the style if the style diff3 is aggregated.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.SOURCE_DIFF4_ID IS ' child item diff4 value of the style if the style diff4 is aggregated.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.DEPT IS 'this column would contain the department identifier, if populated.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.CLASS IS 'this column would contain the class identifier, if populated' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.SUBCLASS IS 'this column would contain the subclass identifier, if populated' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.CHILD_ID IS 'this column contains the  child item id of the source id.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.CHILD_DIFF1_ID IS 'the child item diff1 value of style item' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.CHILD_DIFF2_ID IS 'the child item diff2 value of style item' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.CHILD_DIFF3_ID IS 'the child item diff3 value of style item' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.CHILD_DIFF4_ID IS 'the child item diff4 value of style item' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.SIZE_PROFILE_QTY IS ' the size profile quantity for the child item' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.TOTAL_PROFILE_QTY IS ' the sum of the size profile quantities for a style/color (at item parent aggregate level)' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.RUSH_FLAG IS 'indicates the item need to be rushed for to the location corresponding to itemloc' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.COST IS 'cost of freight' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.IN_STORE_DATE IS 'the date on which the freight need to reach the location' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.ITEM_LEVEL IS 'The item level of the item.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.TRAN_LEVEL IS 'The tran level of the item.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.PACK_IND IS 'The pack indicator of the item.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.STOCK_ON_HAND IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.ALLOC_OUT IS 'The quantity currently allocated from the item/store' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.IN_TRANSIT_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.TSF_EXPECTED_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_ITEM_LOC_TEMP.ITEM_LOC_TEMP_ID IS 'Unique ID for the table, this column is populated using sequnece alc_item_loc_temp table' 
;



PROMPT CREATING TABLE 'ALC_ITEM_PARENT_LOC';
CREATE TABLE ALC_ITEM_PARENT_LOC 
    ( 
     ITEM_PARENT_LOC_ID NUMBER (15)  NOT NULL , 
     ALLOC_ID           NUMBER (15)  NOT NULL , 
     ITEM_NODE          VARCHAR2 (60)  NOT NULL , 
     WH_ID              VARCHAR2 (40)  NOT NULL , 
     LOCATION_ID        VARCHAR2 (40)  NOT NULL , 
     RELEASE_DATE       DATE  NOT NULL , 
     ITEM_DESC          VARCHAR2 (250) , 
     ITEM_TYPE          VARCHAR2 (10) , 
     PARENT_ITEM_ID     VARCHAR2 (40) , 
     ALLOCATED_QTY      NUMBER (12,4) , 
     CALCULATED_QTY     NUMBER (12,4) , 
     NEED_QTY           NUMBER (12,4) , 
     TOTAL_ON_HAND_QTY  NUMBER (12,4) , 
     SOM_QTY            NUMBER (12,4) , 
     BACKORDER_QTY      NUMBER (12,4) , 
     GROSS_NEED_QTY     NUMBER (12,4) , 
     RLOH_QTY           NUMBER (12,4) , 
     STOCK_ON_HAND      NUMBER (12,4) , 
     IN_TRANSIT         NUMBER (12,4) , 
     ON_ORDER           NUMBER (12,4) , 
     ON_ALLOC           NUMBER (12,4) , 
     ALLOC_OUT          NUMBER (12,4) , 
     FUTURE_UNIT_RETAIL NUMBER (12,4) , 
     COST               NUMBER (12,4) , 
     IN_STORE_DATE      DATE , 
     ORDER_NO           VARCHAR2 (40) , 
     SOURCE_TYPE        NUMBER (1)  NOT NULL , 
     RUSH_FLAG          VARCHAR2 (1) , 
     FREEZE_IND         VARCHAR2 (1) , 
     LOC_TYPE           VARCHAR2 (1) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ITEM_PARENT_LOC';
ALTER TABLE ALC_ITEM_PARENT_LOC 
    ADD CONSTRAINT CHK_ALC_ITEM_PA_LOC_SOURCE_TYP 
    CHECK (SOURCE_TYPE IN ('1','2','3','4','5','6','7')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ITEM_PARENT_LOC';
ALTER TABLE ALC_ITEM_PARENT_LOC 
    ADD CONSTRAINT CHK_ALC_ITEM_PA_LOC_LOC_TYP CHECK (LOC_TYPE IN ('S','W')) 
;


COMMENT ON TABLE ALC_ITEM_PARENT_LOC IS 'Recalculation Table, This table stores information at Style/Color or Fashion Pack Group level when Recalculation is the action'
;


COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.ITEM_PARENT_LOC_ID IS 'Primary key for the tableR' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.ALLOC_ID IS 'Reference Allocation ID.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.ITEM_NODE IS 'FA Style/Color Item ID : ItemID 1~AggDiff FPG : Fashion Pack Group Name' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.WH_ID IS 'Warehouse number' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.LOCATION_ID IS 'Store Number' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.RELEASE_DATE IS 'Release Date' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.ITEM_DESC IS 'The description of the item.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.ITEM_TYPE IS 'The type of the item being allocated.
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.PARENT_ITEM_ID IS 'this column would contain the parent item identifier, if populated' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.ALLOCATED_QTY IS 'Final Allocated Qty' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.CALCULATED_QTY IS 'Final Calculated Qty' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.NEED_QTY IS 'this column contains the need quantity for the store/rule.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.TOTAL_ON_HAND_QTY IS 'Total on hand Qty is the result of adding  STOCK_ON_HAND + IN_TRANSIT + ON_ORDER + ON_ALLOC Qtys' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.SOM_QTY IS 'this column contains the store order multiple for this item.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.BACKORDER_QTY IS 'Used to store backorder quantity information used by calculation process.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.GROSS_NEED_QTY IS 'The calculated Gross quantity for the item/store.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.RLOH_QTY IS 'Rule Level On hand value' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.STOCK_ON_HAND IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.IN_TRANSIT IS 'The quantity current in transit to the item/store. IN_TRANSIT is one of four values that will make up TOTAL_ON_HAND_QTY.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.ON_ORDER IS 'The quantity currently on order for the item/store. ON_ORDER is one of four values that will make up TOTAL_ON_HAND_QTY.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.ON_ALLOC IS 'The quantity currently allocated for the item/store. ON_ALLOC is one of four values that will make up TOTAL_ON_HAND_QTY.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.ALLOC_OUT IS 'The quantity currently allocated from the item/store' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.FUTURE_UNIT_RETAIL IS 'The expected future unit retailof the item.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.COST IS 'The cost of teh item.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.IN_STORE_DATE IS 'In Store DAte.' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.ORDER_NO IS 'Order Number' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.SOURCE_TYPE IS 'This column is used to determain the Source type of the item and works with the order_no and wh_id column, Valid values are 1- PO, 2-ASO, 3-warehouse, 4-Whatif, 5-BOL, 6-TSF' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.RUSH_FLAG IS 'Rush Flag' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.FREEZE_IND IS 'Freeze Indicator' 
;

COMMENT ON COLUMN ALC_ITEM_PARENT_LOC.LOC_TYPE IS 'determines whether the location is a store or warehouse. Valid values are S and W' 
;
PROMPT CREATING INDEX 'ALC_ITEM_PARENT_LOC_I1';
CREATE INDEX ALC_ITEM_PARENT_LOC_I1 ON ALC_ITEM_PARENT_LOC 
    ( 
     ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'PK_ALC_ITEM_PARENT_LOC';
CREATE UNIQUE INDEX PK_ALC_ITEM_PARENT_LOC ON ALC_ITEM_PARENT_LOC 
    ( 
     ITEM_PARENT_LOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_ITEM_PARENT_LOC';
ALTER TABLE ALC_ITEM_PARENT_LOC 
    ADD CONSTRAINT PK_ALC_ITEM_PARENT_LOC PRIMARY KEY ( ITEM_PARENT_LOC_ID ) 
    USING INDEX PK_ALC_ITEM_PARENT_LOC ;




PROMPT CREATING TABLE 'ALC_ITEM_SOURCE';
CREATE TABLE ALC_ITEM_SOURCE 
    ( 
     ITEM_SOURCE_ID     NUMBER (20)  NOT NULL , 
     ALLOC_ID           NUMBER (15)  NOT NULL , 
     ITEM_ID            VARCHAR2 (40)  NOT NULL , 
     DEFAULT_LEVEL      VARCHAR2 (1) , 
     PACK_IND           VARCHAR2 (2) , 
     HOLD_BACK_PCT_FLAG VARCHAR2 (1) , 
     HOLD_BACK_VALUE    NUMBER (12,4) , 
     SOM_QTY            NUMBER (12,4) , 
     AVAIL_QTY          NUMBER (12,4) , 
     BACKORDER_QTY      NUMBER (12,4) , 
     RELEASE_DATE       DATE , 
     SOURCE_TYPE        VARCHAR2 (1) , 
     ORDER_NO           VARCHAR2 (40) , 
     WH_ID              VARCHAR2 (40) , 
     DIFF1_ID           VARCHAR2 (100) , 
     DIFF1_DESC         VARCHAR2 (120) , 
     DIFF2_ID           VARCHAR2 (100) , 
     INNER_SIZE         NUMBER (12,4) , 
     CASE_SIZE          NUMBER (12,4) , 
     PALLET             NUMBER (12,4) , 
     CALC_MULTIPLE      VARCHAR2 (2) , 
     ON_HAND_QTY        NUMBER (12,4) , 
     FUTURE_ON_HAND_QTY NUMBER (12,4) , 
     MIN_AVAIL_QTY      NUMBER (12,4) , 
     THRESHOLD_PERCENT  NUMBER (4,2) , 
     ITEM_TYPE          VARCHAR2 (10) , 
     PACK_ROUND         NUMBER (4,3) , 
     TSR_IND            VARCHAR2 (1) DEFAULT 'N' 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ITEM_SOURCE';
ALTER TABLE ALC_ITEM_SOURCE 
    ADD CONSTRAINT CHK_ALC_ITEM_SOURCE_PACK_IND 
    CHECK ( PACK_IND IN ('SP', 'NS' ,'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ITEM_SOURCE';
ALTER TABLE ALC_ITEM_SOURCE 
    ADD CONSTRAINT CHK_ALC_ITEM_SOURCE_CALC_MULT 
    CHECK ( CALC_MULTIPLE IN ('IN', 'CA', 'PA', 'EA')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ITEM_SOURCE';
ALTER TABLE ALC_ITEM_SOURCE 
    ADD CONSTRAINT CHK_ALC_ITEM_SOURCE_TSR_IND 
    CHECK (TSR_IND IN ('Y','N')) 
;


COMMENT ON TABLE ALC_ITEM_SOURCE IS 'this table contains the item source information used to create the allocation. a unique record for each item/wh/purchase order/release date will be available on this table, this resembles the selections made by the user on the item search page.'
;


COMMENT ON COLUMN ALC_ITEM_SOURCE.ITEM_SOURCE_ID IS 'this column contains a unique item source identifier. this value is derived from the sequence alc_item_source_seq.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.ALLOC_ID IS 'this column contains the unique identifier for the allocation within the application.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.ITEM_ID IS 'this column contains the item identifier.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.DEFAULT_LEVEL IS 'this column contains the default level for this item. valid values are:style_color_default_level= 1transaction_default_level= t' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.PACK_IND IS 'this column contains the pack indicator. valid values are:not_pack = nnonsellable_pack = nssellable_pack = sp' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.HOLD_BACK_PCT_FLAG IS 'this column contains the holdback percentage indicator, which defines if the holdback is a percentage value. valid values are:yes = yno = n' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.HOLD_BACK_VALUE IS 'this column contains the holdback value, in percentage if hold_back_pct_flag=y, otherwise it will be whole number.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.SOM_QTY IS 'this column contains the store order multiple for this item.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.AVAIL_QTY IS 'this column contains the available quantity to allocate for this item/allocation' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.BACKORDER_QTY IS 'Used to store backorder quantity information used by Allocation Maintenance Item Review section.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.RELEASE_DATE IS 'this column contains the release date for this item/allocation.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.SOURCE_TYPE IS 'this column contains the source of the item.  PO=1,ASN=2,OH=3,WHATIF=4,BOL=5, AND TSF=6.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.ORDER_NO IS 'this column contains the purchase order identifier for this item. this value will only be populated if the source_type=1.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.WH_ID IS 'this column contains the warehouse identifier for this item. this value will be populated if the source_type is not equal to 4.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.DIFF1_ID IS 'this column contains a key of the item parent aggregate diffs. this is for fashion items only.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.DIFF1_DESC IS 'this column would contain the diff1 description. this value will only be populated for fashion items.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.DIFF2_ID IS 'diff 2 value for the item being allocated.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.INNER_SIZE IS 'inner or each  size for the product source' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.CASE_SIZE IS 'case  size for the product source' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.PALLET IS 'pallet size for the product source' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.CALC_MULTIPLE IS 'possible multiples for the product- i =inner or each, p=pallet and c=case' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.ON_HAND_QTY IS 'this column will store the current stock on hand for the source the item at this location.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.FUTURE_ON_HAND_QTY IS 'this column will store the future quantity for an item at a source location.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.MIN_AVAIL_QTY IS 'stores the min available quantity for the parent allocation.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.THRESHOLD_PERCENT IS 'stores the threshold percent value for the parent allocation.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.ITEM_TYPE IS 'The type of the item being allocated.
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.PACK_ROUND IS 'Pack rounding logic to be used by the calculation engine. Valid values are .25, .5, .75 and 1.' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE.TSR_IND IS 'This column will indicate if Target Stock Ratio was applied to the item. Valid values are Y and N' 
;
PROMPT CREATING INDEX 'PK_ALC_ITEM_SOURCE';
CREATE UNIQUE INDEX PK_ALC_ITEM_SOURCE ON ALC_ITEM_SOURCE 
    ( 
     ITEM_SOURCE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_ITEM_SOURCE_I1';
CREATE INDEX ALC_ITEM_SOURCE_I1 ON ALC_ITEM_SOURCE 
    ( 
     ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_ITEM_SOURCE';
ALTER TABLE ALC_ITEM_SOURCE 
    ADD CONSTRAINT PK_ALC_ITEM_SOURCE PRIMARY KEY ( ITEM_SOURCE_ID ) 
    USING INDEX PK_ALC_ITEM_SOURCE ;




PROMPT CREATING TABLE 'ALC_ITEM_SOURCE_SHIP_SCHEDULE';
CREATE TABLE ALC_ITEM_SOURCE_SHIP_SCHEDULE 
    ( 
     ITEM_SOURCE_ID             NUMBER (20)  NOT NULL , 
     ORIGIN                     NUMBER (10)  NOT NULL , 
     DESTINATION                NUMBER (10)  NOT NULL , 
     ORIGIN_TYPE                VARCHAR2 (2)  NOT NULL , 
     DESTINATION_TYPE           VARCHAR2 (2)  NOT NULL , 
     DEPARTURE_DATE             DATE  NOT NULL , 
     ARRIVAL_DATE               DATE  NOT NULL , 
     ORIGIN_BREAK_PACK_IND      VARCHAR2 (1)  NOT NULL , 
     DESTINATION_BREAK_PACK_IND VARCHAR2 (1)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ITEM_SOURCE_SHIP_SCHEDULE';
ALTER TABLE ALC_ITEM_SOURCE_SHIP_SCHEDULE 
    ADD CONSTRAINT CHK_ASH_ORIGIN_BREAK_PACK_IND 
    CHECK ( ORIGIN_BREAK_PACK_IND IN ('N', 'Y')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_ITEM_SOURCE_SHIP_SCHEDULE';
ALTER TABLE ALC_ITEM_SOURCE_SHIP_SCHEDULE 
    ADD CONSTRAINT CHK_ASH_DESTINATION_BREAK_PI 
    CHECK ( DESTINATION_BREAK_PACK_IND IN ('N', 'Y')) 
;


COMMENT ON TABLE ALC_ITEM_SOURCE_SHIP_SCHEDULE IS 'this table stores the alc_shipping_schedule per alc_item_source.'
;


COMMENT ON COLUMN ALC_ITEM_SOURCE_SHIP_SCHEDULE.ITEM_SOURCE_ID IS 'indicates the item_source_id  and the foriegn key to the primary key of alc_item_source table' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE_SHIP_SCHEDULE.ORIGIN IS 'origin' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE_SHIP_SCHEDULE.DESTINATION IS 'destination' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE_SHIP_SCHEDULE.ORIGIN_TYPE IS 'origin type' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE_SHIP_SCHEDULE.DESTINATION_TYPE IS 'destination type' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE_SHIP_SCHEDULE.DEPARTURE_DATE IS 'departure date' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE_SHIP_SCHEDULE.ARRIVAL_DATE IS 'arrival date' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE_SHIP_SCHEDULE.ORIGIN_BREAK_PACK_IND IS 'origin break pack indicator' 
;

COMMENT ON COLUMN ALC_ITEM_SOURCE_SHIP_SCHEDULE.DESTINATION_BREAK_PACK_IND IS 'destination break pack indicator' 
;
PROMPT CREATING INDEX 'PK_ALC_ITEM_SOURCE_SHIP_SCHEDU';
CREATE UNIQUE INDEX PK_ALC_ITEM_SOURCE_SHIP_SCHEDU ON ALC_ITEM_SOURCE_SHIP_SCHEDULE 
    ( 
     ITEM_SOURCE_ID ASC , 
     ORIGIN ASC , 
     DESTINATION ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_ITEM_SOURCE_SHIP_SCHEDULE';
ALTER TABLE ALC_ITEM_SOURCE_SHIP_SCHEDULE 
    ADD CONSTRAINT PK_ALC_ITEM_SOURCE_SHIP_SCHEDU PRIMARY KEY ( ITEM_SOURCE_ID, ORIGIN, DESTINATION ) 
    USING INDEX PK_ALC_ITEM_SOURCE_SHIP_SCHEDU ;




PROMPT CREATING TABLE 'ALC_ITEM_TYPE_GTT';
CREATE GLOBAL TEMPORARY TABLE ALC_ITEM_TYPE_GTT 
    ( 
     ITEM             VARCHAR2 (38) , 
     ITEM_TYPE        VARCHAR2 (10) , 
     STATUS           VARCHAR2 (1) , 
     TYPE_DESCRIPTION VARCHAR2 (42) , 
     PARENT_COUNT     NUMBER (10) , 
     FA_DIFF_POS      NUMBER (2) , 
     FA_ITEM          VARCHAR2 (25) , 
     FA_DIFF_VALUE    VARCHAR2 (10) 
    ) 
    ON COMMIT DELETE ROWS 
;



COMMENT ON TABLE ALC_ITEM_TYPE_GTT IS 'Helper table for determining allocation item types.'
;


COMMENT ON COLUMN ALC_ITEM_TYPE_GTT.ITEM IS 'The RMS item.' 
;

COMMENT ON COLUMN ALC_ITEM_TYPE_GTT.ITEM_TYPE IS 'The Allocation type of the RMS item.' 
;

COMMENT ON COLUMN ALC_ITEM_TYPE_GTT.STATUS IS 'The status of the RMS item.' 
;

COMMENT ON COLUMN ALC_ITEM_TYPE_GTT.TYPE_DESCRIPTION IS 'The description of the Allocation type.' 
;

COMMENT ON COLUMN ALC_ITEM_TYPE_GTT.PARENT_COUNT IS 'The count of parent items of transactions items contained in a pack.  Only populated for non-sellable fashion packs' 
;

COMMENT ON COLUMN ALC_ITEM_TYPE_GTT.FA_DIFF_POS IS 'The diff position portion of concatenated FA items.' 
;

COMMENT ON COLUMN ALC_ITEM_TYPE_GTT.FA_ITEM IS 'The item portion of concatenated FA items.' 
;

COMMENT ON COLUMN ALC_ITEM_TYPE_GTT.FA_DIFF_VALUE IS 'The diff value portion of concatenated FA items.' 
;



PROMPT CREATING TABLE 'ALC_LOAD_TEMP';
CREATE TABLE ALC_LOAD_TEMP 
    ( 
     ALLOC_ID             NUMBER (15) , 
     ITEM_SOURCE_ID       NUMBER (20) , 
     ITEM                 VARCHAR2 (25) , 
     ITEM_TYPE            VARCHAR2 (10) , 
     ITEM_DESC            VARCHAR2 (250) , 
     WH                   NUMBER (10) , 
     SOURCE_TYPE          NUMBER (1) , 
     DOC_NO               VARCHAR2 (40) , 
     DEPT                 NUMBER (4) , 
     CLASS                NUMBER (4) , 
     SUBCLASS             NUMBER (4) , 
     DEPT_NAME            VARCHAR2 (120) , 
     CLASS_NAME           VARCHAR2 (120) , 
     SUBCLASS_NAME        VARCHAR2 (120) , 
     ITEM_AGGREGATE_IND   VARCHAR2 (1) , 
     DIFF_1_AGGREGATE_IND VARCHAR2 (1) , 
     DIFF_2_AGGREGATE_IND VARCHAR2 (1) , 
     DIFF_3_AGGREGATE_IND VARCHAR2 (1) , 
     DIFF_4_AGGREGATE_IND VARCHAR2 (1) , 
     DIFF_1               VARCHAR2 (250) , 
     DIFF_2               VARCHAR2 (250) , 
     DIFF_3               VARCHAR2 (250) , 
     DIFF_4               VARCHAR2 (250) , 
     DIFF_1_DESC          VARCHAR2 (500) , 
     DIFF_2_DESC          VARCHAR2 (500) , 
     DIFF_3_DESC          VARCHAR2 (500) , 
     DIFF_4_DESC          VARCHAR2 (500) , 
     ITEM_PARENT          VARCHAR2 (25) , 
     ITEM_PARENT_DESC     VARCHAR2 (250) , 
     ITEM_GRANDPARENT     VARCHAR2 (25) , 
     PACK_IND             VARCHAR2 (1) , 
     SELLABLE_IND         VARCHAR2 (1) , 
     TRAN_LEVEL           NUMBER (1) , 
     ITEM_LEVEL           NUMBER (1) , 
     INNER_PACK_SIZE      NUMBER (12,4) , 
     SUPP_PACK_SIZE       NUMBER (12,4) , 
     TI                   NUMBER (12,4) , 
     HI                   NUMBER (12,4) , 
     AVAIL_QTY            NUMBER (12,4) , 
     BACKORDER_QTY        NUMBER (12,4) , 
     BREAK_PACK_IND       VARCHAR2 (1) , 
     DEFAULT_LEVEL        VARCHAR2 (1) , 
     RELEASE_DATE         DATE , 
     HOLD_BACK_PCT_FLAG   VARCHAR2 (1) , 
     HOLD_BACK_VALUE      NUMBER (12,4) , 
     MIN_AVAIL_QTY        NUMBER (12,4) , 
     THRESHOLD_PERCENT    NUMBER (4,2) , 
     CALC_MULTIPLE        VARCHAR2 (2) , 
     ON_HAND_QTY          NUMBER (12,4) , 
     FUTURE_ON_HAND_QTY   NUMBER (12,4) , 
     PACK_ROUND           NUMBER (4,3) , 
     PO_NOT_AFTER_DATE    DATE , 
     QTY_ORDERED          NUMBER (12,4) , 
     PROPORTION           NUMBER (12,4) , 
     SELLPACK_FASHION_IND VARCHAR2 (1) , 
     IMAGE_NAME           VARCHAR2 (120) , 
     IMAGE_ADDR           VARCHAR2 (255) , 
     TSR_IND              VARCHAR2 (1) DEFAULT 'N' 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_LOAD_TEMP IS 'This table is used during the Allocation load process.  The ALC_LOAD_ITEM_SOURCE PLSQL package gathers information into this table that is in turn loaded into Java.'
;


COMMENT ON COLUMN ALC_LOAD_TEMP.ALLOC_ID IS 'The allocation being loaded and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.ITEM_SOURCE_ID IS 'The item source id loaded from alc_item_source.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.ITEM IS 'The transaction level item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.ITEM_TYPE IS 'The type of the item being allocated.
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.ITEM_DESC IS 'The description of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.WH IS 'The wh being allocated from.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.SOURCE_TYPE IS 'The type of inventory being allocated. Valid values are: PO=1,ASN=2,OH=3 (Warehouse source),WHATIF=4,BOL=5, AND TSF=6.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DOC_NO IS 'The identifier of the inventory transaction the inventory is coming from.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DEPT IS 'The dept of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.CLASS IS 'The class of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.SUBCLASS IS 'The subclass of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DEPT_NAME IS 'The name of the department.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.CLASS_NAME IS 'The name of the class.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.SUBCLASS_NAME IS 'The name of the subclass.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.ITEM_AGGREGATE_IND IS 'The aggregate indicator of the parent item of the item being allocated.  Only populated for FA allocations or SA allocations that need size profile applied.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DIFF_1_AGGREGATE_IND IS 'The diff 1 aggregate indicator of the parent item of the item being allocated.  Only populated for FA allocations or SA allocations that need size profile applied.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DIFF_2_AGGREGATE_IND IS 'The diff 2 aggregate indicator of the parent item of the item being allocated.  Only populated for FA allocations or SA allocations that need size profile applied.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DIFF_3_AGGREGATE_IND IS 'The diff 3 aggregate indicator of the parent item of the item being allocated.  Only populated for FA allocations or SA allocations that need size profile applied.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DIFF_4_AGGREGATE_IND IS 'The diff 4 aggregate indicator of the parent item of the item being allocated.  Only populated for FA allocations or SA allocations that need size profile applied.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DIFF_1 IS 'The diff_1 value of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DIFF_2 IS 'The diff_2 value of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DIFF_3 IS 'The diff_3 value of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DIFF_4 IS 'The diff_4 value of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DIFF_1_DESC IS 'The description of the diff 1 identifier.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DIFF_2_DESC IS 'The description of the diff 2 identifier.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DIFF_3_DESC IS 'The description of the diff 3 identifier.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DIFF_4_DESC IS 'The description of the diff 4 identifier.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.ITEM_PARENT IS 'The parent of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.ITEM_PARENT_DESC IS 'The description of the parent of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.ITEM_GRANDPARENT IS 'The grand parent of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.PACK_IND IS 'The pack_ind of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.SELLABLE_IND IS 'The sellable_ind of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.TRAN_LEVEL IS 'The transaction level of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.ITEM_LEVEL IS 'The item level of the item being allocated.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.INNER_PACK_SIZE IS 'The inner pack size of the item for its primary supplier / country.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.SUPP_PACK_SIZE IS 'The supplier pack size of the item for its primary supplier / country.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.TI IS 'The ti of the item for its primary supplier / country.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.HI IS 'The hi of the item for its primary supplier / country.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.AVAIL_QTY IS 'The available qty for the item/wh/source_type/doc_no combination.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.BACKORDER_QTY IS 'The backorder qty for the item/wh/source_type/doc_no combination.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.BREAK_PACK_IND IS 'The break pack indicator of the wh being allocated from.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.DEFAULT_LEVEL IS 'Populated from alc_item_source.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.RELEASE_DATE IS 'Populated from alc_item_source.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.HOLD_BACK_PCT_FLAG IS 'Populated from alc_item_source.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.HOLD_BACK_VALUE IS 'Populated from alc_item_source.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.MIN_AVAIL_QTY IS 'Populated from alc_item_source.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.THRESHOLD_PERCENT IS 'Populated from alc_item_source.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.CALC_MULTIPLE IS 'Populated from alc_item_source.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.ON_HAND_QTY IS 'Populated from alc_item_source.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.FUTURE_ON_HAND_QTY IS 'Populated from alc_item_source.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.PACK_ROUND IS 'Populated from alc_item_source.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.PO_NOT_AFTER_DATE IS 'Populated from ordhead.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.QTY_ORDERED IS 'Populated from ordloc.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.PROPORTION IS 'Populated from ordloc.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.SELLPACK_FASHION_IND IS 'Y if item is a sellable pack that contains only fashion items.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.IMAGE_NAME IS 'This field contains the name of the image of the item.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.IMAGE_ADDR IS 'This field contains the actual path where the file of the image of the item is stored.' 
;

COMMENT ON COLUMN ALC_LOAD_TEMP.TSR_IND IS 'Populated from alc_item_source.' 
;



PROMPT CREATING TABLE 'ALC_LOCATION';
CREATE TABLE ALC_LOCATION 
    ( 
     ALC_LOCATION_ID       NUMBER (25)  NOT NULL , 
     LOC_GROUP_ID          NUMBER (20)  NOT NULL , 
     LOCATION_ID           VARCHAR2 (40)  NOT NULL , 
     LOCATION_DESC         VARCHAR2 (150) , 
     GROUP_ID              VARCHAR2 (120)  NOT NULL , 
     SUBGROUP_ID           VARCHAR2 (150) , 
     IN_STORE_DATE         DATE , 
     CREATED_BY            VARCHAR2 (64) , 
     CREATION_DATE         TIMESTAMP , 
     LAST_UPDATED_DATE     TIMESTAMP , 
     LAST_UPDATED_BY       VARCHAR2 (64) , 
     OBJECT_VERSION_NUMBER NUMBER (9) , 
     LOCATION_TYPE         VARCHAR2 (1)  NOT NULL , 
     STORE_TYPE            VARCHAR2 (1) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_LOCATION IS 'this table contains all of the locations that are present on each allocation. each location will appear on this table a unique allocation/location combination no matter how the location was added to the allocation, via group or individually.'
;


COMMENT ON COLUMN ALC_LOCATION.ALC_LOCATION_ID IS 'this column contains a unique location identifier. this value is derived from the sequence alc_location_seq.' 
;

COMMENT ON COLUMN ALC_LOCATION.LOC_GROUP_ID IS 'this column contains a unique location group identifier' 
;

COMMENT ON COLUMN ALC_LOCATION.LOCATION_ID IS 'this column contains the store identifier.' 
;

COMMENT ON COLUMN ALC_LOCATION.LOCATION_DESC IS 'this column contains the store description.' 
;

COMMENT ON COLUMN ALC_LOCATION.GROUP_ID IS 'this column contains the group identifier' 
;

COMMENT ON COLUMN ALC_LOCATION.SUBGROUP_ID IS 'this column contains the subgroup identifier.' 
;

COMMENT ON COLUMN ALC_LOCATION.IN_STORE_DATE IS 'the date the allocator wants the item at this location' 
;

COMMENT ON COLUMN ALC_LOCATION.CREATED_BY IS 'Indicates the user who created the record.' 
;

COMMENT ON COLUMN ALC_LOCATION.CREATION_DATE IS 'Indicates the user who last updated the record.' 
;

COMMENT ON COLUMN ALC_LOCATION.LAST_UPDATED_DATE IS 'The timestamp of the record creation date.' 
;

COMMENT ON COLUMN ALC_LOCATION.LAST_UPDATED_BY IS 'The timestamp of the record last updated date.' 
;

COMMENT ON COLUMN ALC_LOCATION.OBJECT_VERSION_NUMBER IS 'This column indicates Object version number.' 
;

COMMENT ON COLUMN ALC_LOCATION.LOCATION_TYPE IS 'Represents the type of the location, whether it''s a warehouse or a store.' 
;

COMMENT ON COLUMN ALC_LOCATION.STORE_TYPE IS 'Represents the type of the store, whether it''s a Franchisee or a corporate store.' 
;

PROMPT CREATING INDEX 'PK_ALC_LOCATION';
CREATE UNIQUE INDEX PK_ALC_LOCATION ON ALC_LOCATION 
    ( 
     ALC_LOCATION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_LOCATION';
ALTER TABLE ALC_LOCATION 
ADD CONSTRAINT PK_ALC_LOCATION PRIMARY KEY ( ALC_LOCATION_ID ) 
USING INDEX PK_ALC_LOCATION ;
	
PROMPT CREATING INDEX 'ALC_LOCATION_I1';
CREATE UNIQUE INDEX ALC_LOCATION_I1 ON ALC_LOCATION 
    ( 
     LOCATION_ID ASC , 
     LOC_GROUP_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_LOCATION_I2';
CREATE INDEX ALC_LOCATION_I2 ON ALC_LOCATION 
    ( 
     LOC_GROUP_ID ASC , 
     GROUP_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 16;


PROMPT CREATING TABLE 'ALC_LOC_GROUP';
CREATE TABLE ALC_LOC_GROUP 
    ( 
     LOC_GROUP_ID          NUMBER (20)  NOT NULL , 
     ALLOC_ID              NUMBER (15) , 
     TEMPLATE_ID           NUMBER (15) , 
     GROUP_DESC            VARCHAR2 (600) , 
     COMPLEX_IND           VARCHAR2 (1) DEFAULT 'N'  NOT NULL , 
     CREATED_BY            VARCHAR2 (64) , 
     CREATION_DATE         TIMESTAMP , 
     LAST_UPDATED_DATE     TIMESTAMP , 
     LAST_UPDATED_BY       VARCHAR2 (64) , 
     OBJECT_VERSION_NUMBER NUMBER (9) , 
     UPDATEABLE            VARCHAR2 (1) DEFAULT 'Y'  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_LOC_GROUP';
ALTER TABLE ALC_LOC_GROUP 
    ADD CONSTRAINT CHK_ALC_LOCGRP_COMPLEX_IND 
    CHECK ( COMPLEX_IND IN ('E', 'I', 'N', 'U', 'X')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_LOC_GROUP';
ALTER TABLE ALC_LOC_GROUP 
    ADD CONSTRAINT CHK_ALC_LOCGRP_UPDATEABLE 
    CHECK ( UPDATEABLE IN ('Y', 'N')) 
;


COMMENT ON TABLE ALC_LOC_GROUP IS 'this table contains the location group information used to build the locations used on the allocation.'
;


COMMENT ON COLUMN ALC_LOC_GROUP.LOC_GROUP_ID IS 'this column contains a unique location group identifier. this value is derived from the sequence alc_loc_group_seq.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP.ALLOC_ID IS 'indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_LOC_GROUP.TEMPLATE_ID IS 'this column would contain a unique template identifier, if a template is used for this allocation.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP.GROUP_DESC IS 'this column contains the group description' 
;

COMMENT ON COLUMN ALC_LOC_GROUP.COMPLEX_IND IS 'this column contains the complex group indicator. valid values are:no = n union u intersection = iexclude = e the default value is n.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP.CREATED_BY IS 'Indicates the user who created the record.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP.CREATION_DATE IS 'Indicates the user who last updated the record.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP.LAST_UPDATED_DATE IS 'The timestamp of the record creation date.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP.LAST_UPDATED_BY IS 'The timestamp of the record last updated date.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP.OBJECT_VERSION_NUMBER IS 'This column indicates Object version number.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP.UPDATEABLE IS 'Describes whether the location group is updateable or not' 
;
PROMPT CREATING INDEX 'ALC_LOC_GROUP_I3';
CREATE INDEX ALC_LOC_GROUP_I3 ON ALC_LOC_GROUP 
    ( 
     LOC_GROUP_ID ASC , 
     TEMPLATE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_LOC_GROUP_I2';
CREATE INDEX ALC_LOC_GROUP_I2 ON ALC_LOC_GROUP 
    ( 
     TEMPLATE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_LOC_GROUP_I1';
CREATE INDEX ALC_LOC_GROUP_I1 ON ALC_LOC_GROUP 
    ( 
     ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'PK_ALC_LOC_GROUP';
CREATE UNIQUE INDEX PK_ALC_LOC_GROUP ON ALC_LOC_GROUP 
    ( 
     LOC_GROUP_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_LOC_GROUP';
ALTER TABLE ALC_LOC_GROUP 
    ADD CONSTRAINT PK_ALC_LOC_GROUP PRIMARY KEY ( LOC_GROUP_ID ) 
    USING INDEX PK_ALC_LOC_GROUP ;




PROMPT CREATING TABLE 'ALC_LOC_GROUP_DETAIL';
CREATE TABLE ALC_LOC_GROUP_DETAIL 
    ( 
     LOC_GROUP_DETAIL_ID   NUMBER (20)  NOT NULL , 
     LOC_GROUP_ID          NUMBER (20)  NOT NULL , 
     GROUP_TYPE            VARCHAR2 (2)  NOT NULL , 
     GROUP_ID              VARCHAR2 (120) , 
     GROUP_DESC            VARCHAR2 (600) , 
     SUBGROUP_ID           VARCHAR2 (150) , 
     SUBGROUP_DESC         VARCHAR2 (600) , 
     CREATED_BY            VARCHAR2 (64) , 
     CREATION_DATE         TIMESTAMP , 
     LAST_UPDATED_DATE     TIMESTAMP , 
     LAST_UPDATED_BY       VARCHAR2 (64) , 
     OBJECT_VERSION_NUMBER NUMBER (9) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_LOC_GROUP_DETAIL IS 'this table contains the locations in each location group.'
;


COMMENT ON COLUMN ALC_LOC_GROUP_DETAIL.LOC_GROUP_DETAIL_ID IS 'this column contains a unique location group identifier. this value is derived from the sequence alc_loc_group_Detail_seq.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP_DETAIL.LOC_GROUP_ID IS 'this column contains a unique location group identifier.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP_DETAIL.GROUP_TYPE IS 'this column contains the group type. valid values are:loc_list = 1 store_grade = 2 price_zone = 3 promo_zone = 4 loc_trait = 5 all_store = 6 store = 7' 
;

COMMENT ON COLUMN ALC_LOC_GROUP_DETAIL.GROUP_ID IS 'this column contains the group identifier' 
;

COMMENT ON COLUMN ALC_LOC_GROUP_DETAIL.GROUP_DESC IS 'this column contains the group description' 
;

COMMENT ON COLUMN ALC_LOC_GROUP_DETAIL.SUBGROUP_ID IS 'this column contains the subgroup identifier' 
;

COMMENT ON COLUMN ALC_LOC_GROUP_DETAIL.SUBGROUP_DESC IS 'this column contains the subgroup description.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP_DETAIL.CREATED_BY IS 'Indicates the user who created the record.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP_DETAIL.CREATION_DATE IS 'The timestamp of the record creation date.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP_DETAIL.LAST_UPDATED_DATE IS 'The timestamp of the record last updated date.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP_DETAIL.LAST_UPDATED_BY IS 'The user who last updated the record.' 
;

COMMENT ON COLUMN ALC_LOC_GROUP_DETAIL.OBJECT_VERSION_NUMBER IS 'This column indicates Object version number.' 
;
PROMPT CREATING INDEX 'ALC_LOC_GROUP_DETAIL_I1';
CREATE UNIQUE INDEX ALC_LOC_GROUP_DETAIL_I1 ON ALC_LOC_GROUP_DETAIL 
    ( 
     LOC_GROUP_ID ASC , 
     GROUP_ID ASC , 
     SUBGROUP_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'PK_ALC_LOC_GROUP_DTL';
CREATE UNIQUE INDEX PK_ALC_LOC_GROUP_DTL ON ALC_LOC_GROUP_DETAIL 
    ( 
     LOC_GROUP_DETAIL_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_LOC_GROUP_DETAIL';
ALTER TABLE ALC_LOC_GROUP_DETAIL 
    ADD CONSTRAINT PK_ALC_LOC_GROUP_DTL PRIMARY KEY ( LOC_GROUP_DETAIL_ID ) 
    USING INDEX PK_ALC_LOC_GROUP_DTL ;




PROMPT CREATING TABLE 'ALC_MERCH_HIER_RLOH_TEMP';
CREATE TABLE ALC_MERCH_HIER_RLOH_TEMP 
    ( 
     ALLOC_ID     NUMBER (15) , 
     DEPT         NUMBER (4) , 
     CLASS        NUMBER (4) , 
     SUBCLASS     NUMBER (4) , 
     LOC          NUMBER (10) , 
     LOC_TYPE     VARCHAR2 (1) , 
     CURR_AVAIL   NUMBER (24,4) , 
     FUTURE_AVAIL NUMBER (24,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_MERCH_HIER_RLOH_TEMP IS 'Temporary table used in the calculation process.  This table holds information about RLOH inventory positions when rule level on hand is being used at the merchadise hierarchy snapshot level..'
;


COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.ALLOC_ID IS 'The allocation being calculated and  the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.DEPT IS 'The department to consider in the RLOH calculation.' 
;

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.CLASS IS 'The class to consider in the RLOH calculation.' 
;

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.SUBCLASS IS 'The subclass to consider in the RLOH calculation.' 
;

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.LOC IS 'The location to consider in the RLOH calculation.' 
;

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.LOC_TYPE IS 'The location type to consider in the RLOH calculation.' 
;

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.CURR_AVAIL IS 'The current inventory of the merchandise hiearchy/loc.' 
;

COMMENT ON COLUMN ALC_MERCH_HIER_RLOH_TEMP.FUTURE_AVAIL IS 'The future inventory of the merchandise hiearchy/loc.  Based on the on order commit values from ALC_RULE.' 
;



PROMPT CREATING TABLE 'ALC_PLAN';
CREATE TABLE ALC_PLAN 
    ( 
     PLAN_ID  NUMBER (20)  NOT NULL , 
     LOC      VARCHAR2 (40)  NOT NULL , 
     DEPT     VARCHAR2 (40) , 
     CLASS    VARCHAR2 (40) , 
     SUBCLASS VARCHAR2 (40) , 
     ITEM_ID  VARCHAR2 (40) , 
     DIFF1_ID VARCHAR2 (40) , 
     DIFF2_ID VARCHAR2 (40) , 
     DIFF3_ID VARCHAR2 (40) , 
     DIFF4_ID VARCHAR2 (40) , 
     EOW_DATE DATE  NOT NULL , 
     QTY      NUMBER (12,4)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_PLAN IS 'this table holds all plan information used by the allocation system for gathering need. a record can exist at any of the following levels by week/location/quantity: department, department-class, department-class-subclass, item-diff and sku. this data is expected to be populated from an external system.'
;


COMMENT ON COLUMN ALC_PLAN.PLAN_ID IS 'this column contains a unique plan identifier. this value is derived from the sequence alc_plan_seq.' 
;

COMMENT ON COLUMN ALC_PLAN.LOC IS 'The location the plan applies to.' 
;

COMMENT ON COLUMN ALC_PLAN.DEPT IS 'Department ID, this is not a mandatory field but atleast one of the below has to be provided  (Dept, Dept/Class, Dept / Class/ Subclass, Item)' 
;

COMMENT ON COLUMN ALC_PLAN.CLASS IS 'Class ID, this is not a mandatory field but atleast one of the below has to be provided  (Dept, Dept/Class, Dept / Class/ Subclass, Item)' 
;

COMMENT ON COLUMN ALC_PLAN.SUBCLASS IS 'Subclass ID, this is not a mandatory field but atleast one of the below has to be provided  (Dept, Dept/Class, Dept / Class/ Subclass, Item)' 
;

COMMENT ON COLUMN ALC_PLAN.ITEM_ID IS 'Item ID, this is not a mandatory field but atleast one of the below has to be provided  (Dept, Dept/Class, Dept / Class/ Subclass, Item). For fashion items, Diff ID has to be provided.' 
;

COMMENT ON COLUMN ALC_PLAN.DIFF1_ID IS 'this column would contain the diff1 identifier, if populated' 
;

COMMENT ON COLUMN ALC_PLAN.DIFF2_ID IS 'this column would contain the diff2 identifier, if populated.' 
;

COMMENT ON COLUMN ALC_PLAN.DIFF3_ID IS 'this column would contain the diff3 identifier, if populated.' 
;

COMMENT ON COLUMN ALC_PLAN.DIFF4_ID IS 'this column would contain the diff4 identifier, if populated' 
;

COMMENT ON COLUMN ALC_PLAN.EOW_DATE IS 'End of week date.  The end of week date provided should fall on the weekday specificed in ALC_SYSTEM_OPTIONS.TP_END_OF_WEEK_DAY.' 
;

COMMENT ON COLUMN ALC_PLAN.QTY IS 'this column contains the plan quantity for this entry.' 
;
PROMPT CREATING INDEX 'PK_ALC_PLAN';
CREATE UNIQUE INDEX PK_ALC_PLAN ON ALC_PLAN 
    ( 
     PLAN_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_PLAN_I3';
CREATE INDEX ALC_PLAN_I3 ON ALC_PLAN 
    ( 
     ITEM_ID ASC , 
     EOW_DATE ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_PLAN_I1';
CREATE INDEX ALC_PLAN_I1 ON ALC_PLAN 
    ( 
     LOC ASC , 
     ITEM_ID ASC , 
     EOW_DATE ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_PLAN_I2';
CREATE INDEX ALC_PLAN_I2 ON ALC_PLAN 
    ( 
     LOC ASC , 
     ITEM_ID ASC , 
     DIFF1_ID ASC , 
     DIFF2_ID ASC , 
     DIFF3_ID ASC , 
     DIFF4_ID ASC , 
     EOW_DATE ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_PLAN';
ALTER TABLE ALC_PLAN 
    ADD CONSTRAINT PK_ALC_PLAN PRIMARY KEY ( PLAN_ID ) 
    USING INDEX PK_ALC_PLAN ;




PROMPT CREATING TABLE 'ALC_PREPACK_CALC_RESULTS';
CREATE TABLE ALC_PREPACK_CALC_RESULTS 
    ( 
     PREPACK_CALC_RESULTS_ID NUMBER (25)  NOT NULL , 
     PREPACK_SET_ID          NUMBER (20)  NOT NULL , 
     ALLOC_ID                NUMBER (15)  NOT NULL , 
     SET_ID                  NUMBER (10)  NOT NULL , 
     PACK_ID                 NUMBER (10)  NOT NULL , 
     ITEM_ID                 VARCHAR2 (40)  NOT NULL , 
     DIFF1_ID                VARCHAR2 (40) , 
     DIFF1_DESC              VARCHAR2 (120) , 
     DIFF2_ID                VARCHAR2 (40) , 
     DIFF2_DESC              VARCHAR2 (120) , 
     PARENT_ITEM_ID          VARCHAR2 (40) , 
     QTY                     NUMBER (12,4) , 
     TOTAL_PACK_NEED         NUMBER (10) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_PREPACK_CALC_RESULTS IS 'this table contains the results of the prepack optimization process. this information is unique to each allocation/set/pack.'
;


COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.PREPACK_CALC_RESULTS_ID IS 'this column contains a unique prepack calculation results identifier. this value  is derived from the sequence alc_prepack_calc_results_seq.' 
;

COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.PREPACK_SET_ID IS 'this column contains a unique prepack set identifier' 
;

COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.ALLOC_ID IS 'indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table is derived from the sequence alc_prepack_calc_results_seq.' 
;

COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.SET_ID IS 'this column contains the set identifier.' 
;

COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.PACK_ID IS 'this column contains the pack identifier.' 
;

COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.ITEM_ID IS 'this column would contain the item identifier' 
;

COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.DIFF1_ID IS 'this column would contain the diff1 identifier, if populated.' 
;

COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.DIFF1_DESC IS 'this column would contain the diff1 description, if populated' 
;

COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.DIFF2_ID IS 'this column would contain the diff2 identifier, if populated.' 
;

COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.DIFF2_DESC IS 'this column would contain the diff2 description, if populated' 
;

COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.PARENT_ITEM_ID IS 'this column would contain the parent item identifier, if populated' 
;

COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.QTY IS 'this column contains the quantity.' 
;

COMMENT ON COLUMN ALC_PREPACK_CALC_RESULTS.TOTAL_PACK_NEED IS 'the sum of pack needed for all stores' 
;
PROMPT CREATING INDEX 'PK_ALC_PREPACK_CALC_RESULTS';
CREATE UNIQUE INDEX PK_ALC_PREPACK_CALC_RESULTS ON ALC_PREPACK_CALC_RESULTS 
    ( 
     PREPACK_CALC_RESULTS_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_PREPACK_CALC_RESULTS_I1';
CREATE UNIQUE INDEX ALC_PREPACK_CALC_RESULTS_I1 ON ALC_PREPACK_CALC_RESULTS 
    ( 
     ALLOC_ID ASC , 
     SET_ID ASC , 
     PACK_ID ASC , 
     ITEM_ID ASC , 
     DIFF1_ID ASC , 
     DIFF2_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_PREPACK_CALC_RESULTS_I2';
CREATE INDEX ALC_PREPACK_CALC_RESULTS_I2 ON ALC_PREPACK_CALC_RESULTS 
    ( 
     PREPACK_SET_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_PREPACK_CALC_RESULTS';
ALTER TABLE ALC_PREPACK_CALC_RESULTS 
    ADD CONSTRAINT PK_ALC_PREPACK_CALC_RESULTS PRIMARY KEY ( PREPACK_CALC_RESULTS_ID ) 
    USING INDEX PK_ALC_PREPACK_CALC_RESULTS ;




PROMPT CREATING TABLE 'ALC_PREPACK_SET';
CREATE TABLE ALC_PREPACK_SET 
    ( 
     PREPACK_SET_ID NUMBER (20)  NOT NULL , 
     ALLOC_ID       NUMBER (15)  NOT NULL , 
     SET_ID         NUMBER (10)  NOT NULL , 
     NBR_OF_PACKS   NUMBER (10) , 
     MIN            NUMBER (12,4) , 
     MAX            NUMBER (12,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_PREPACK_SET IS 'this table contains the constraints used to create the optimal prepacks within the system. this information is unique to the allocation and set.'
;


COMMENT ON COLUMN ALC_PREPACK_SET.PREPACK_SET_ID IS 'this column contains a unique prepack set identifier. this value is derived from the sequence alc_prepack_set_seq.' 
;

COMMENT ON COLUMN ALC_PREPACK_SET.ALLOC_ID IS 'indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_PREPACK_SET.SET_ID IS 'this column contains the set identifier' 
;

COMMENT ON COLUMN ALC_PREPACK_SET.NBR_OF_PACKS IS 'this column contains the number of packs to be configured in the prepack' 
;

COMMENT ON COLUMN ALC_PREPACK_SET.MIN IS 'this column contains the minimum number of items per pack in the prep optimization' 
;

COMMENT ON COLUMN ALC_PREPACK_SET.MAX IS 'this column contains the maximum number of items per pack in the prep optimization' 
;
PROMPT CREATING INDEX 'PK_ALC_PREPACK_SET';
CREATE UNIQUE INDEX PK_ALC_PREPACK_SET ON ALC_PREPACK_SET 
    ( 
     PREPACK_SET_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_PREPACK_SET_I1';
CREATE UNIQUE INDEX ALC_PREPACK_SET_I1 ON ALC_PREPACK_SET 
    ( 
     ALLOC_ID ASC , 
     SET_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_PREPACK_SET';
ALTER TABLE ALC_PREPACK_SET 
    ADD CONSTRAINT PK_ALC_PREPACK_SET PRIMARY KEY ( PREPACK_SET_ID ) 
    USING INDEX PK_ALC_PREPACK_SET ;




PROMPT CREATING TABLE 'ALC_PREPACK_SET_ITEM';
CREATE TABLE ALC_PREPACK_SET_ITEM 
    ( 
     PREPACK_SET_ITEM_ID NUMBER (25)  NOT NULL , 
     PREPACK_SET_ID      NUMBER (20)  NOT NULL , 
     ALLOC_ID            NUMBER (15)  NOT NULL , 
     SET_ID              NUMBER (10)  NOT NULL , 
     ITEM_ID             VARCHAR2 (40)  NOT NULL , 
     DIFF1_ID            VARCHAR2 (40) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_PREPACK_SET_ITEM IS 'this table contains the items that are to be included in each of the optimal prepack sets defined by each allocation.'
;


COMMENT ON COLUMN ALC_PREPACK_SET_ITEM.PREPACK_SET_ITEM_ID IS 'this column contains a unique prepack set item identifier. this value is derived from the sequence alc_prepack_set_item_seq.' 
;

COMMENT ON COLUMN ALC_PREPACK_SET_ITEM.PREPACK_SET_ID IS 'this column contains a unique prepack set identifier' 
;

COMMENT ON COLUMN ALC_PREPACK_SET_ITEM.ALLOC_ID IS 'indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_PREPACK_SET_ITEM.SET_ID IS 'this column contains the set identifier' 
;

COMMENT ON COLUMN ALC_PREPACK_SET_ITEM.ITEM_ID IS 'this column would contain the item identifier.' 
;

COMMENT ON COLUMN ALC_PREPACK_SET_ITEM.DIFF1_ID IS 'this column would contain the diff1 identifier, if populated.' 
;
PROMPT CREATING INDEX 'PK_ALC_PREPACK_SET_ITEM';
CREATE UNIQUE INDEX PK_ALC_PREPACK_SET_ITEM ON ALC_PREPACK_SET_ITEM 
    ( 
     PREPACK_SET_ITEM_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_PREPACK_SET_ITEM_I1';
CREATE INDEX ALC_PREPACK_SET_ITEM_I1 ON ALC_PREPACK_SET_ITEM 
    ( 
     PREPACK_SET_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_PREPACK_SET_ITEM';
ALTER TABLE ALC_PREPACK_SET_ITEM 
    ADD CONSTRAINT PK_ALC_PREPACK_SET_ITEM PRIMARY KEY ( PREPACK_SET_ITEM_ID ) 
    USING INDEX PK_ALC_PREPACK_SET_ITEM ;




PROMPT CREATING TABLE 'ALC_QUANTITY_LIMITS';
CREATE TABLE ALC_QUANTITY_LIMITS 
    ( 
     QUANTITY_LIMITS_ID NUMBER (20)  NOT NULL , 
     ALLOC_ID           NUMBER (15)  NOT NULL , 
     LOCATION_ID        VARCHAR2 (40)  NOT NULL , 
     LOCATION_DESC      VARCHAR2 (150)  NOT NULL , 
     DEPT               VARCHAR2 (40) , 
     CLASS              VARCHAR2 (40) , 
     SUBCLASS           VARCHAR2 (40) , 
     ITEM_ID            VARCHAR2 (60) , 
     MIN                NUMBER (12,4) , 
     MAX                NUMBER (12,4) , 
     TRESHOLD           NUMBER (12,4) , 
     TREND              NUMBER (12,4) , 
     WOS                NUMBER (12,4) , 
     MIN_NEED           NUMBER (12,4) ,
     MIN_PACK           NUMBER (12,4) ,
	 MAX_PACK           NUMBER (12,4)
	) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_QUANTITY_LIMITS IS 'this table contains the user restrictions that are to be placed on the calculation process to determine the need. this data can be exist at any of the following levels by allocation/location: department, department-class, department-class-subclass and sku'
;


COMMENT ON COLUMN ALC_QUANTITY_LIMITS.QUANTITY_LIMITS_ID IS 'this column contains a unique quantity limits identifier. this value is derived from the sequence alc_quantity_limits_seq.' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.ALLOC_ID IS 'indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.LOCATION_ID IS 'this column contains the store identifier' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.LOCATION_DESC IS 'this column contains the store description.' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.DEPT IS 'this column would contain the department identifier, if populated.' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.CLASS IS 'this column would contain the class identifier, if populated.' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.SUBCLASS IS 'this column would contain the subclass identifier, if populated.' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.ITEM_ID IS 'this column would contain the item identifier, if populated.' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.MIN IS 'this column contains the minimum quantity to allocate. this value constrains the allocation to require a minimum value to this location.' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.MAX IS 'this column contains the maximum quantity to allocate. this value constrains the allocation to a maximum value to this location.' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.TRESHOLD IS 'this column contains the threshold quantity to allocate. this value forces the allocation to allocate the threshold quantity or nothing at all.' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.TREND IS 'this column contains the trend quantity to allocate. this value will modify the gross need by the percentage entered; this value can be a negative number.' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.WOS IS 'this column contains the weeks-of-supply quantity to allocate if populated. a weekly average is calculated from the gross need selection. this average is multiplied by the wos quantity entered and the result is treated as a minimum allocation. example, rule history -' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.MIN_NEED IS 'this column contains the minimum need quantity to allocate. this value will override the gross need unless the gross need is greater than the min_need.' 
;
COMMENT ON COLUMN ALC_QUANTITY_LIMITS.MIN_PACK is 'This column contains the Minimum Pack Value. This value will ensure that a minimum number of packs will be allocated' 
;

COMMENT ON COLUMN ALC_QUANTITY_LIMITS.MAX_PACK is 'This column contains the Maximum Pack Value. This value will ensure that the number of allocated packs will not exceed this value' 
;
PROMPT CREATING INDEX 'PK_ALC_QUANTITY_LIMITS';
CREATE UNIQUE INDEX PK_ALC_QUANTITY_LIMITS ON ALC_QUANTITY_LIMITS 
    ( 
     QUANTITY_LIMITS_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_QUANTITY_LIMITS_I1';
CREATE INDEX ALC_QUANTITY_LIMITS_I1 ON ALC_QUANTITY_LIMITS 
    ( 
     ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_QUANTITY_LIMITS';
ALTER TABLE ALC_QUANTITY_LIMITS 
    ADD CONSTRAINT PK_ALC_QUANTITY_LIMITS PRIMARY KEY ( QUANTITY_LIMITS_ID ) 
    USING INDEX PK_ALC_QUANTITY_LIMITS ;




PROMPT CREATING TABLE 'ALC_RECEIPT_PLAN';
CREATE TABLE ALC_RECEIPT_PLAN 
    ( 
     RECEIPT_PLAN_ID NUMBER (22)  NOT NULL , 
     LOC             VARCHAR2 (40)  NOT NULL , 
     DEPT            VARCHAR2 (40) , 
     CLASS           VARCHAR2 (40) , 
     SUBCLASS        VARCHAR2 (40) , 
     ITEM            VARCHAR2 (40) , 
     DIFF1           VARCHAR2 (40) , 
     DIFF2           VARCHAR2 (40) , 
     DIFF3           VARCHAR2 (40) , 
     DIFF4           VARCHAR2 (40) , 
     EOW_DATE        DATE  NOT NULL , 
     QTY             NUMBER (12,4) 
    ) 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_RECEIPT_PLAN IS 'Receipt and Plan table.  Table is used to generate Need at different hiearchy level when Receipt and Plan is used in Allocation as rule.'
;


COMMENT ON COLUMN ALC_RECEIPT_PLAN.RECEIPT_PLAN_ID IS 'A Primary key for the table.    It uses sequence ALC_RECEIPT_PLAN_SEQ' 
;

COMMENT ON COLUMN ALC_RECEIPT_PLAN.LOC IS 'The location the plan applies to.' 
;

COMMENT ON COLUMN ALC_RECEIPT_PLAN.DEPT IS 'Department ID, this is not a mandatory field but atleast one of the below has to be provided  (Dept, Dept/Class, Dept / Class/ Subclass, Item)' 
;

COMMENT ON COLUMN ALC_RECEIPT_PLAN.CLASS IS 'Class ID, this is not a mandatory field but atleast one of the below has to be provided  (Dept, Dept/Class, Dept / Class/ Subclass, Item)' 
;

COMMENT ON COLUMN ALC_RECEIPT_PLAN.SUBCLASS IS 'Subclass ID, this is not a mandatory field but atleast one of the below has to be provided  (Dept, Dept/Class, Dept / Class/ Subclass, Item)' 
;

COMMENT ON COLUMN ALC_RECEIPT_PLAN.ITEM IS 'Item ID, this is not a mandatory field but atleast one of the below has to be provided  (Dept, Dept/Class, Dept / Class/ Subclass, Item). For fashion items, Diff ID has to be provided.' 
;

COMMENT ON COLUMN ALC_RECEIPT_PLAN.DIFF1 IS 'First Diff for the fashion item, this field is not mandatory but has to be provided when item is fashion' 
;

COMMENT ON COLUMN ALC_RECEIPT_PLAN.DIFF2 IS 'Second Diff for the fashion item, this field is not mandatory but has to be provided when item is fashion' 
;

COMMENT ON COLUMN ALC_RECEIPT_PLAN.DIFF3 IS 'Third Diff for the fashion item, this field is not mandatory but has to be provided when item is fashion' 
;

COMMENT ON COLUMN ALC_RECEIPT_PLAN.DIFF4 IS 'Four Diff for the fashion item, this field is not mandatory but has to be provided when item is fashion' 
;

COMMENT ON COLUMN ALC_RECEIPT_PLAN.EOW_DATE IS 'End of week date.  The end of week date provided should fall on the weekday specificed in ALC_SYSTEM_OPTIONS.TP_END_OF_WEEK_DAY.' 
;

COMMENT ON COLUMN ALC_RECEIPT_PLAN.QTY IS 'Receipt quantity' 
;
PROMPT CREATING INDEX 'PK_ALC_RECEIPT_PLAN';
CREATE UNIQUE INDEX PK_ALC_RECEIPT_PLAN ON ALC_RECEIPT_PLAN 
    ( 
     RECEIPT_PLAN_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;
PROMPT CREATING INDEX 'ALC_RECEIPT_PLAN_I1';
CREATE INDEX ALC_RECEIPT_PLAN_I1 ON ALC_RECEIPT_PLAN 
    ( 
     LOC ASC , 
     ITEM ASC , 
     EOW_DATE ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_RECEIPT_PLAN_I2';
CREATE INDEX ALC_RECEIPT_PLAN_I2 ON ALC_RECEIPT_PLAN 
    ( 
     LOC ASC , 
     ITEM ASC , 
     DIFF1 ASC , 
     DIFF2 ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_RECEIPT_PLAN_I4';
CREATE INDEX ALC_RECEIPT_PLAN_I4 ON ALC_RECEIPT_PLAN 
    ( 
     LOC ASC , 
     EOW_DATE ASC , 
     ITEM ASC , 
     DEPT ASC , 
     CLASS ASC , 
     SUBCLASS ASC , 
     DIFF1 ASC , 
     DIFF2 ASC , 
     DIFF3 ASC , 
     DIFF4 ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 16;
	
PROMPT CREATING INDEX 'ALC_RECEIPT_PLAN_I3';
CREATE INDEX ALC_RECEIPT_PLAN_I3 ON ALC_RECEIPT_PLAN 
    ( 
     ITEM ASC , 
     EOW_DATE ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_RECEIPT_PLAN';
ALTER TABLE ALC_RECEIPT_PLAN 
    ADD CONSTRAINT PK_ALC_RECEIPT_PLAN PRIMARY KEY ( RECEIPT_PLAN_ID ) 
    USING INDEX PK_ALC_RECEIPT_PLAN ;




PROMPT CREATING TABLE 'ALC_RULE';
CREATE TABLE ALC_RULE 
    ( 
     RULE_ID                       NUMBER (20)  NOT NULL , 
     ALLOC_ID                      NUMBER (15) , 
     TEMPLATE_ID                   NUMBER (15) , 
     RULE_TYPE                     VARCHAR2 (1)  NOT NULL , 
     RULE_LEVEL                    VARCHAR2 (2) , 
     EXACT_IND                     VARCHAR2 (1)  NOT NULL , 
     NET_NEED_IND                  VARCHAR2 (1)  NOT NULL , 
     USE_RULE_LEVEL_ON_HAND_IND    VARCHAR2 (1)  NOT NULL , 
     INCLUDE_CLEARANCE_STOCK_IND   VARCHAR2 (1)  NOT NULL , 
     REGULAR_SALES_IND             VARCHAR2 (1)  NOT NULL , 
     PROMO_SALES_IND               VARCHAR2 (1)  NOT NULL , 
     CLEARANCE_SALES_IND           VARCHAR2 (1)  NOT NULL , 
     INCLUDE_INV_IN_MIN_IND        VARCHAR2 (1)  NOT NULL , 
     INCLUDE_INV_IN_MAX_IND        VARCHAR2 (1)  NOT NULL , 
     ON_ORDER_COMMIT_DATE          DATE , 
     ON_ORDER_COMMIT_WEEKS         NUMBER (12,4) , 
     IWOS_WEEKS                    NUMBER (12,4) , 
     WEEKS_THIS_YEAR               NUMBER (2) , 
     WEEKS_LAST_YEAR               NUMBER (2) , 
     WEEKS_FUTURE                  NUMBER (4) , 
     START_DATE1                   DATE , 
     END_DATE1                     DATE , 
     START_DATE2                   DATE , 
     END_DATE2                     DATE , 
     CORPORATE_RULE_ID             NUMBER (10) , 
     INCLUDE_MID_TIER_ON_HAND_IND  VARCHAR2 (1) , 
     ENFORCE_PRES_MIN_IND          VARCHAR2 (1) , 
     LEAD_TIME_NEED_IND            VARCHAR2 (1) , 
     LEAD_TIME_NEED_RULE_TYPE      VARCHAR2 (1) , 
     LEAD_TIME_NEED_START_DATE     DATE , 
     LEAD_TIME_NEED_END_DATE       DATE , 
     CONVERT_TO_PACK               VARCHAR2 (1) , 
     SIZE_PROFILE_TYPE             VARCHAR2 (2) , 
     ON_HAND_IND                   VARCHAR2 (1) , 
     ON_ORDER_IND                  VARCHAR2 (1) , 
     IN_TRANSIT_IND                VARCHAR2 (1) , 
     INBOUND_ALLOCATION_IND        VARCHAR2 (1) , 
     OUTBOUND_ALLOCATION_IND       VARCHAR2 (1) , 
     BACK_ORDER_IND                VARCHAR2 (1) , 
     PACK_THRESHOLD                NUMBER (3) , 
     SIZE_PROFILE                  VARCHAR2 (20) , 
     RULE_MODE                     VARCHAR2 (1) , 
     ON_ORDER_COMMIT_RANGE_START   DATE DEFAULT NULL , 
     ON_ORDER_COMMIT_RANGE_END     DATE DEFAULT NULL , 
     SIZE_PROFILE_WH_AVAIL_PCT_IND VARCHAR2 (1) , 
     SIZE_PROFILE_DEFAULT_HIER_IND VARCHAR2 (1) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_RULE';
ALTER TABLE ALC_RULE 
    ADD CONSTRAINT CHK_INCLUDE_MID_TIER_OH_IND 
    CHECK (INCLUDE_MID_TIER_ON_HAND_IND IN ('Y','N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_RULE';
ALTER TABLE ALC_RULE 
    ADD CONSTRAINT CHK_ALC_RULE_EPMI 
    CHECK (ENFORCE_PRES_MIN_IND IN ('Y','N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_RULE';
ALTER TABLE ALC_RULE 
    ADD CONSTRAINT CHK_ALC_RULE_LEADTIME_NEED_IND 
    CHECK ( LEAD_TIME_NEED_IND IN ('Y','N')) 
;


COMMENT ON COLUMN ALC_RULE.RULE_ID IS 'this column contains the rule identifier.' 
;

COMMENT ON COLUMN ALC_RULE.ALLOC_ID IS 'indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_RULE.TEMPLATE_ID IS 'this column would contain a unique template identifier, if a template is used for this allocation.' 
;

COMMENT ON COLUMN ALC_RULE.RULE_TYPE IS 'this column contains the rule type for this allocation. valid values are:history = 1forecast = 2plan = 3history_and_plan = 4plan_reproject = 5corporate_rules = 6manual = 7    RECIEPT_AND_PLAN=8' 
;

COMMENT ON COLUMN ALC_RULE.RULE_LEVEL IS 'this column contains the level for the rule chosen for this allocation. valid values are: dept = 1 class = 2 subclass = 3 style= 4 sku = 5 STYLELEVEL =6 UDA=8 UDA_VALUE=9 ITEMLIST    =10' 
;

COMMENT ON COLUMN ALC_RULE.EXACT_IND IS 'this column contains the exact indicator. valid values are:yes = yno = n' 
;

COMMENT ON COLUMN ALC_RULE.NET_NEED_IND IS 'this column contains the identifier to determine if net need is used in the calculation process. valid values are:yes = y, no = n' 
;

COMMENT ON COLUMN ALC_RULE.USE_RULE_LEVEL_ON_HAND_IND IS 'this column contains the use rule level on hand indicator. valid values are:yes = yno = n' 
;

COMMENT ON COLUMN ALC_RULE.INCLUDE_CLEARANCE_STOCK_IND IS 'this column contains the include clearance stock indicator. valid values are:yes = yno = n' 
;

COMMENT ON COLUMN ALC_RULE.REGULAR_SALES_IND IS 'this column contains the regular sales indicator. valid values are:yes = yno = n' 
;

COMMENT ON COLUMN ALC_RULE.PROMO_SALES_IND IS 'this column contains the promotion sales indicator valid values are:yes = yno = n' 
;

COMMENT ON COLUMN ALC_RULE.CLEARANCE_SALES_IND IS 'this column contains the clearance sales indicator valid values are:yes = yno = n' 
;

COMMENT ON COLUMN ALC_RULE.INCLUDE_INV_IN_MIN_IND IS 'this column contains the include inventory in minimum indicator valid values are:yes = yno = n' 
;

COMMENT ON COLUMN ALC_RULE.INCLUDE_INV_IN_MAX_IND IS 'this column contains the include inventory in maximum indicator valid values are:yes = yno = n' 
;

COMMENT ON COLUMN ALC_RULE.ON_ORDER_COMMIT_DATE IS 'this column would contain the on-order commit date, if populated.' 
;

COMMENT ON COLUMN ALC_RULE.ON_ORDER_COMMIT_WEEKS IS 'this column would contain the on order commit weeks, if populated.' 
;

COMMENT ON COLUMN ALC_RULE.IWOS_WEEKS IS 'this column would contain the ideal-weeks-of-supply weeks value, if populated.' 
;

COMMENT ON COLUMN ALC_RULE.WEEKS_THIS_YEAR IS 'this column would contain the weeks this year quantity. this value will only be populated if the start_date1, end_date1 are null.' 
;

COMMENT ON COLUMN ALC_RULE.WEEKS_LAST_YEAR IS 'this column would contain the number of weeks for last year to be used in the calculation. this value will only be populated if the start_date1, end_date1 are null.' 
;

COMMENT ON COLUMN ALC_RULE.WEEKS_FUTURE IS 'this column would contain the number of future weeks to be used in the calculation.' 
;

COMMENT ON COLUMN ALC_RULE.START_DATE1 IS 'this column would contain the start date for the date range used in this rule.  this value will be null if weeks_this_year is not null.' 
;

COMMENT ON COLUMN ALC_RULE.END_DATE1 IS 'this column would contain the end date for the date range used in this rule. this value will be null if weeks_this_year is not null.' 
;

COMMENT ON COLUMN ALC_RULE.START_DATE2 IS 'this column would contain the start date for the date range used in this rule. this value will not be populated for the following rules: corporate rules, plan re-project and history and plan.' 
;

COMMENT ON COLUMN ALC_RULE.END_DATE2 IS 'this column would contain the end date for the date range used in this rule. this value will not be populated for the following rules: corporate rules, plan re-project and history and plan.' 
;

COMMENT ON COLUMN ALC_RULE.CORPORATE_RULE_ID IS 'this column contains a unique corporate rule head identifier. this is associated to the corresponding records in the alc_corporate_rule_head table.' 
;

COMMENT ON COLUMN ALC_RULE.INCLUDE_MID_TIER_ON_HAND_IND IS 'holds the flag whether to include mid-tier onhand or not for an mld allocation' 
;

COMMENT ON COLUMN ALC_RULE.ENFORCE_PRES_MIN_IND IS 'when this indicator is y, values from alc_auto_quantity_limits are defaulted into the quantity limits for the allocation.' 
;

COMMENT ON COLUMN ALC_RULE.LEAD_TIME_NEED_IND IS 'when this indicator is y, include the need generated during the lead time in the total need.' 
;

COMMENT ON COLUMN ALC_RULE.LEAD_TIME_NEED_RULE_TYPE IS 'indicates the rule type (e.g. history) used to predict the sales that are used to extrapolate the lead time need' 
;

COMMENT ON COLUMN ALC_RULE.LEAD_TIME_NEED_START_DATE IS 'the start date of the period used to get the sales that are used to extrapolate the lead time need' 
;

COMMENT ON COLUMN ALC_RULE.LEAD_TIME_NEED_END_DATE IS 'the end date of the period used to get the sales that are used to extrapolate the lead time need' 
;

COMMENT ON COLUMN ALC_RULE.CONVERT_TO_PACK IS 'column details whether the sellable staple pack has to be allocated in terms of packs or not.' 
;

COMMENT ON COLUMN ALC_RULE.SIZE_PROFILE_TYPE IS 'Allowed Values  -  PR, PO and SO PR -  Profile Ratio Logic  :  PO -  Profile Ratio Optimal Logic SO - Selling Curve Optimal Logic' 
;

COMMENT ON COLUMN ALC_RULE.ON_HAND_IND IS 'Allowed values - Y, N' 
;

COMMENT ON COLUMN ALC_RULE.ON_ORDER_IND IS 'Allowed values - Y, N' 
;

COMMENT ON COLUMN ALC_RULE.IN_TRANSIT_IND IS 'Allowed values - Y, N' 
;

COMMENT ON COLUMN ALC_RULE.INBOUND_ALLOCATION_IND IS 'Allowed values - Y, N' 
;

COMMENT ON COLUMN ALC_RULE.OUTBOUND_ALLOCATION_IND IS 'Indicates if out bound allocation quantities will be used in the inventory calculations on this allocation.' 
;

COMMENT ON COLUMN ALC_RULE.BACK_ORDER_IND IS 'This column holds Back Order Indicator value' 
;

COMMENT ON COLUMN ALC_RULE.PACK_THRESHOLD IS 'Adding Column to meet CR-36 requirement. This column will hold Pack Variance Acceptance Threshold value that is populated from the Policy Maintenance screen.' 
;

COMMENT ON COLUMN ALC_RULE.SIZE_PROFILE IS 'Represents the size profile details' 
;

COMMENT ON COLUMN ALC_RULE.RULE_MODE IS 'Column Holding Mode 1- Simple 2-Cascade 3-Pack Distribution 4-Spread Demand' 
;

COMMENT ON COLUMN ALC_RULE.ON_ORDER_COMMIT_RANGE_START IS 'The start of the date range when the date range options is being used for the include inventory dates section.' 
;

COMMENT ON COLUMN ALC_RULE.ON_ORDER_COMMIT_RANGE_END IS 'The end of the date range when the date range options is being used for the include inventory dates section.' 
;

COMMENT ON COLUMN ALC_RULE.SIZE_PROFILE_WH_AVAIL_PCT_IND IS 'Use available inventory for wh location size profile population when normal size profile values are not available.' 
;

COMMENT ON COLUMN ALC_RULE.SIZE_PROFILE_DEFAULT_HIER_IND IS 'Indicates if Hierarchy size profile information should be used when SIZE_PROFILE is set to a GID but no GID data is available.' 
;
PROMPT CREATING INDEX 'PK_ALC_RULE';
CREATE UNIQUE INDEX PK_ALC_RULE ON ALC_RULE 
    ( 
     RULE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_RULE_I2';
CREATE INDEX ALC_RULE_I2 ON ALC_RULE 
    ( 
     TEMPLATE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_RULE_I1';
CREATE INDEX ALC_RULE_I1 ON ALC_RULE 
    ( 
     ALLOC_ID ASC , 
     TEMPLATE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_RULE';
ALTER TABLE ALC_RULE 
    ADD CONSTRAINT PK_ALC_RULE PRIMARY KEY ( RULE_ID ) 
    USING INDEX PK_ALC_RULE ;




PROMPT CREATING TABLE 'ALC_RULE_DATE';
CREATE TABLE ALC_RULE_DATE 
    ( 
     RULE_DATE_ID NUMBER (20)  NOT NULL , 
     ALLOC_ID     NUMBER (15)  NOT NULL , 
     EOW_DATE     DATE , 
     TY_LY_IND    VARCHAR2 (2) , 
     WEEK_INDEX   NUMBER (5) , 
     WEIGHT       NUMBER (12,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_RULE_DATE IS 'this table contains the date range information for the allocation rule. records will not exist on this table for manual allocations.'
;


COMMENT ON COLUMN ALC_RULE_DATE.RULE_DATE_ID IS 'this column contains a unique rule date identifier. this value is derived from the sequence alc_rule_date_seq.' 
;

COMMENT ON COLUMN ALC_RULE_DATE.ALLOC_ID IS 'indicates the Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_RULE_DATE.EOW_DATE IS 'this column contains the end-of-week date.' 
;

COMMENT ON COLUMN ALC_RULE_DATE.TY_LY_IND IS 'this column would contains the this year or last year indicator.' 
;

COMMENT ON COLUMN ALC_RULE_DATE.WEEK_INDEX IS 'this column would contain the week index.' 
;

COMMENT ON COLUMN ALC_RULE_DATE.WEIGHT IS 'this column contains the weight of each week.' 
;
PROMPT CREATING INDEX 'PK_ALC_RULE_DATE';
CREATE UNIQUE INDEX PK_ALC_RULE_DATE ON ALC_RULE_DATE 
    ( 
     RULE_DATE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_RULE_DATE_I1';
CREATE UNIQUE INDEX ALC_RULE_DATE_I1 ON ALC_RULE_DATE 
    ( 
     ALLOC_ID ASC , 
     EOW_DATE ASC , 
     TY_LY_IND ASC , 
     WEEK_INDEX ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_RULE_DATE';
ALTER TABLE ALC_RULE_DATE 
    ADD CONSTRAINT PK_ALC_RULE_DATE PRIMARY KEY ( RULE_DATE_ID ) 
    USING INDEX PK_ALC_RULE_DATE ;




PROMPT CREATING TABLE 'ALC_RULE_MANY_TO_ONE';
CREATE TABLE ALC_RULE_MANY_TO_ONE 
    ( 
     MANY_TO_ONE_ID        NUMBER (20)  NOT NULL , 
     ALLOC_ID              NUMBER (15) , 
     DEPT                  VARCHAR2 (40) , 
     CLASS                 VARCHAR2 (40) , 
     SUBCLASS              VARCHAR2 (40) , 
     ITEM_ID               VARCHAR2 (40) , 
     ITEMLIST_ID           VARCHAR2 (40) , 
     UDA_ID                VARCHAR2 (40) , 
     UDA_VALUE_ID          VARCHAR2 (40) , 
     TEMPLATE_ID           NUMBER (15) , 
     RULE_ID               NUMBER (20) , 
     DIFF1_ID              VARCHAR2 (40) , 
     TYPE                  VARCHAR2 (2)  NOT NULL , 
     WEIGHT_PCT            NUMBER (12,4)  NOT NULL , 
     START_DATE_PERIOD_ONE DATE , 
     END_DATE_PERIOD_ONE   DATE , 
     START_DATE_PERIOD_TWO DATE , 
     END_DATE_PERIOD_TWO   DATE , 
     WK_FROM_TODAY_THIS_YR NUMBER (2) , 
     WK_FROM_TODAY_LAST_YR NUMBER (2) , 
     ALT_HIER_RULE_LEVEL   NUMBER (2)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_RULE_MANY_TO_ONE';
ALTER TABLE ALC_RULE_MANY_TO_ONE 
    ADD CONSTRAINT CHK_ARMTO_WK_FROM_TODAY_THIS_Y 
    CHECK ( WK_FROM_TODAY_THIS_YR BETWEEN 0 AND 52) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_RULE_MANY_TO_ONE';
ALTER TABLE ALC_RULE_MANY_TO_ONE 
    ADD CONSTRAINT CHK_ARMTO_WK_FROM_TODAY_LAST_Y 
    CHECK ( WK_FROM_TODAY_LAST_YR BETWEEN 0 AND 52) 
;


COMMENT ON TABLE ALC_RULE_MANY_TO_ONE IS 'this table contains the data used to determine the need of an allocation when a user has defined specific points of data relevant to this allocation. this data can only use a single criteria define as: department, department-class, department-class-subclass, style-color, sku, user defined attributes or item lists. however, the user can select multiple of the values of the single criteria, eg multiple departments or multiple item lists on the allocation. records will not exist on this table for manual allocations.'
;


COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.MANY_TO_ONE_ID IS 'this column contains a unique rule many to one identifier. this value is derived from the sequence alc_rule_many_to_one_seq.' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.ALLOC_ID IS 'indicates the allocation id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.DEPT IS 'Department Id' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.CLASS IS 'Class Id' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.SUBCLASS IS 'Subclass Id' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.ITEM_ID IS 'The item id. populated from Item master table.' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.ITEMLIST_ID IS 'this column contains the template identifier, if a template is used on this allocation' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.UDA_ID IS 'this column would contain the user defined attribute identifier, if populated.' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.UDA_VALUE_ID IS 'this column would contain the user defined attribute value, if populated' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.TEMPLATE_ID IS 'this column contains the template identifier, if a template is used on this allocation.' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.RULE_ID IS 'this column contains the rule identifier.' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.DIFF1_ID IS 'this column would contain the diff1 identifier, if populated' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.TYPE IS 'represents the user selected date range editing' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.WEIGHT_PCT IS 'the percentage of the total sales/forecast/plan information for that period' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.START_DATE_PERIOD_ONE IS 'the start date for period one' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.END_DATE_PERIOD_ONE IS 'the end date for period one' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.START_DATE_PERIOD_TWO IS 'the start date for period two' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.END_DATE_PERIOD_TWO IS 'the end date for period two' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.WK_FROM_TODAY_THIS_YR IS 'number of weeks from today left in year' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.WK_FROM_TODAY_LAST_YR IS 'number of weeks into the current calendar year based on current day' 
;

COMMENT ON COLUMN ALC_RULE_MANY_TO_ONE.ALT_HIER_RULE_LEVEL IS 'Indicates the Rule level of the row' 
;
PROMPT CREATING INDEX 'PK_ALC_RULE_MANY_TO_ONE';
CREATE UNIQUE INDEX PK_ALC_RULE_MANY_TO_ONE ON ALC_RULE_MANY_TO_ONE 
    ( 
     MANY_TO_ONE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_RULE_MANY_TO_ONE_I1';
CREATE INDEX ALC_RULE_MANY_TO_ONE_I1 ON ALC_RULE_MANY_TO_ONE 
    ( 
     RULE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_RULE_MANY_TO_ONE';
ALTER TABLE ALC_RULE_MANY_TO_ONE 
    ADD CONSTRAINT PK_ALC_RULE_MANY_TO_ONE PRIMARY KEY ( MANY_TO_ONE_ID ) 
    USING INDEX PK_ALC_RULE_MANY_TO_ONE ;




PROMPT CREATING TABLE 'ALC_SCHEDULE';
CREATE TABLE ALC_SCHEDULE 
    ( 
     SCHEDULE_ID        NUMBER (10)  NOT NULL , 
     PARENT_ALLOC_ID    NUMBER (15)  NOT NULL , 
     START_DATE         DATE  NOT NULL , 
     END_DATE           DATE  NOT NULL , 
     FREQUENCY          VARCHAR2 (1)  NOT NULL , 
     DAYS_OF_WEEK       VARCHAR2 (7)  NOT NULL , 
     ACTION             NUMBER (2)  NOT NULL , 
     LAST_MODIFIED_DATE NUMBER (38)  NOT NULL , 
     CREATED_DATE       DATE  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SCHEDULE';
ALTER TABLE ALC_SCHEDULE 
    ADD CONSTRAINT CHK_ALC_SCHEDULE_FREQUENCY 
    CHECK ( FREQUENCY IN ('B', 'W')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SCHEDULE';
ALTER TABLE ALC_SCHEDULE 
    ADD CONSTRAINT CHK_ALC_SCHEDULE_DATE 
    CHECK (end_date >= start_date)
;


COMMENT ON TABLE ALC_SCHEDULE IS 'this table stores schedule information of parent allocation.'
;


COMMENT ON COLUMN ALC_SCHEDULE.SCHEDULE_ID IS 'schedule id of the schedule. primary key.' 
;

COMMENT ON COLUMN ALC_SCHEDULE.PARENT_ALLOC_ID IS 'allocation id to which this schedule belongs. it is a not null and unique field. there is a referential integrity of this column to the column alloc_id in the table alc_alloc' 
;

COMMENT ON COLUMN ALC_SCHEDULE.START_DATE IS 'start date of the schedule. this is a not null field. there is a check constraint between start_date and end_date: i.e.: end_date >= start_date' 
;

COMMENT ON COLUMN ALC_SCHEDULE.END_DATE IS 'end date of the schedule information. this is a not null field. there is a check constraint between start_date and end_date: i.e.: end_date >= start_date' 
;

COMMENT ON COLUMN ALC_SCHEDULE.FREQUENCY IS 'frequency information: weekly or bi-weekly. it is a not null field. there is a check on this column for valid values being w or b.' 
;

COMMENT ON COLUMN ALC_SCHEDULE.DAYS_OF_WEEK IS 'stores the days of week as selected for the scheduler as a string of 1s and 0  os. this is a not null field.' 
;

COMMENT ON COLUMN ALC_SCHEDULE.ACTION IS 'stores the action type for the schedule. this is a not null field' 
;

COMMENT ON COLUMN ALC_SCHEDULE.LAST_MODIFIED_DATE IS 'stores the last modified date in terms of milliseconds. this is a not null filed' 
;

COMMENT ON COLUMN ALC_SCHEDULE.CREATED_DATE IS 'stores the date information when this schedule information is/was created. this' 
;
PROMPT CREATING INDEX 'PK_ALC_SCHEDULE';
CREATE UNIQUE INDEX PK_ALC_SCHEDULE ON ALC_SCHEDULE 
    ( 
     SCHEDULE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_SCHEDULE_I1';
CREATE INDEX ALC_SCHEDULE_I1 ON ALC_SCHEDULE 
    ( 
     PARENT_ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_SCHEDULE';
ALTER TABLE ALC_SCHEDULE 
    ADD CONSTRAINT PK_ALC_SCHEDULE PRIMARY KEY ( SCHEDULE_ID ) 
    USING INDEX PK_ALC_SCHEDULE ;




PROMPT CREATING TABLE 'ALC_SESSION_GID_PROFILE';
CREATE TABLE ALC_SESSION_GID_PROFILE 
    ( 
     SESSION_ID          VARCHAR2 (64)  NOT NULL , 
     RESULT_TYPE         NUMBER (1)  NOT NULL , 
     SEQ_ID              NUMBER (4)  NOT NULL , 
     GID                 VARCHAR2 (40) , 
     GID_DESC            VARCHAR2 (40) , 
     GID_PROFILE_LIST_ID NUMBER (20) , 
     SIZE_PROFILE_DESC   VARCHAR2 (2048) , 
     SIZE_PROFILE_LEVEL  NUMBER (1) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_SESSION_GID_PROFILE IS 'This temporary table, holds the GID header results (Parent) of all the size profiles as per user search criteria.'
;


COMMENT ON COLUMN ALC_SESSION_GID_PROFILE.SESSION_ID IS 'Represents a unique size profile session id.' 
;

COMMENT ON COLUMN ALC_SESSION_GID_PROFILE.RESULT_TYPE IS 'Represents the type of row whether it is derived from search(1) or copy(2), to simulate two instances of rows.' 
;

COMMENT ON COLUMN ALC_SESSION_GID_PROFILE.SEQ_ID IS 'Represents the row id or the index of results.' 
;

COMMENT ON COLUMN ALC_SESSION_GID_PROFILE.GID IS 'Represents the size profile generation id referring to alc_gid_header.gid' 
;

COMMENT ON COLUMN ALC_SESSION_GID_PROFILE.GID_DESC IS 'Represents the size profile generation desc referring to alc_gid_header.gid_desc' 
;

COMMENT ON COLUMN ALC_SESSION_GID_PROFILE.GID_PROFILE_LIST_ID IS 'This column maps to alc_session_gid_profile_list.gid_profile_list_id to store a list of gid_profile_ids, one result of gid header can maps to multiple gid profile ids.' 
;

COMMENT ON COLUMN ALC_SESSION_GID_PROFILE.SIZE_PROFILE_DESC IS 'Represents the derived level of size profile.' 
;
PROMPT CREATING INDEX 'PK_ALC_SESSION_GID_PROFILE';
CREATE UNIQUE INDEX PK_ALC_SESSION_GID_PROFILE ON ALC_SESSION_GID_PROFILE 
    ( 
     SESSION_ID ASC , 
     RESULT_TYPE ASC , 
     SEQ_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'UK_ALC_SESSION_GID_PROFILE';
CREATE UNIQUE INDEX UK_ALC_SESSION_GID_PROFILE ON ALC_SESSION_GID_PROFILE 
    ( 
     GID_PROFILE_LIST_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_SESSION_GID_PROFILE';
ALTER TABLE ALC_SESSION_GID_PROFILE 
    ADD CONSTRAINT PK_ALC_SESSION_GID_PROFILE PRIMARY KEY ( SESSION_ID, RESULT_TYPE, SEQ_ID ) 
    USING INDEX PK_ALC_SESSION_GID_PROFILE ;


PROMPT CREATING UNIQUE KEY ON 'ALC_SESSION_GID_PROFILE';
ALTER TABLE ALC_SESSION_GID_PROFILE 
    ADD CONSTRAINT UK_ALC_SESSION_GID_PROFILE UNIQUE ( GID_PROFILE_LIST_ID ) 
    USING INDEX UK_ALC_SESSION_GID_PROFILE ;




PROMPT CREATING TABLE 'ALC_SESSION_GID_PROFILE_LIST';
CREATE TABLE ALC_SESSION_GID_PROFILE_LIST 
    ( 
     SESSION_ID          VARCHAR2 (64)  NOT NULL , 
     GID_PROFILE_LIST_ID NUMBER (20)  NOT NULL , 
     GID_PROFILE_ID      NUMBER (15)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_SESSION_GID_PROFILE_LIST IS 'This temporary table, holds the list of gid profiles for the gid header results, to simulate a table of tables'
;


COMMENT ON COLUMN ALC_SESSION_GID_PROFILE_LIST.SESSION_ID IS 'Indicates the session login associated to the user who last updated the row.' 
;

COMMENT ON COLUMN ALC_SESSION_GID_PROFILE_LIST.GID_PROFILE_LIST_ID IS 'Represents a unique id, populated from the sequence alc_session_gidprofileids_seq' 
;

COMMENT ON COLUMN ALC_SESSION_GID_PROFILE_LIST.GID_PROFILE_ID IS 'Represents the gid profile id for the gid header results, one gid header results can have multiple gid profile ids' 
;
PROMPT CREATING INDEX 'PK_ALC_SESSION_GID_PRO_LIST';
CREATE UNIQUE INDEX PK_ALC_SESSION_GID_PRO_LIST ON ALC_SESSION_GID_PROFILE_LIST 
    ( 
     GID_PROFILE_LIST_ID ASC , 
     GID_PROFILE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_SESSION_GID_PROFILE_LIST';
ALTER TABLE ALC_SESSION_GID_PROFILE_LIST 
    ADD CONSTRAINT PK_ALC_SESSION_GID_PRO_LIST PRIMARY KEY ( GID_PROFILE_LIST_ID, GID_PROFILE_ID ) 
    USING INDEX PK_ALC_SESSION_GID_PRO_LIST ;




PROMPT CREATING TABLE 'ALC_SESSION_ITEM_LOC';
CREATE TABLE ALC_SESSION_ITEM_LOC 
    ( 
     ITEM_LOC_SESSION_ID        NUMBER (15)  NOT NULL , 
     FACET_SESSION_ID           VARCHAR2 (50)  NOT NULL , 
     ITEM_LOC_ID                NUMBER (25) , 
     ALLOC_ID                   NUMBER (15) , 
     ITEM_ID                    VARCHAR2 (70) , 
     ITEM_DESC                  VARCHAR2 (250) , 
     ITEM_TYPE                  VARCHAR2 (10) , 
     WH_ID                      NUMBER (10) , 
     RELEASE_DATE               DATE , 
     LOCATION_ID                NUMBER (10) , 
     LOCATION_DESC              VARCHAR2 (150) , 
     GROUP_ID                   VARCHAR2 (120) , 
     GROUP_DESC                 VARCHAR2 (600) , 
     GROUP_TYPE                 NUMBER (2) , 
     LOC_GROUP_ID               NUMBER (10) , 
     ALLOCATED_QTY              NUMBER (12,4) , 
     CALCULATED_QTY             NUMBER (12,4) , 
     NEED_QTY                   NUMBER (12,4) , 
     ON_HAND_QTY                NUMBER (12,4) , 
     IN_TRANSIT                 NUMBER (12,4) , 
     ON_ORDER                   NUMBER (12,4) , 
     ON_ALLOC                   NUMBER (12,4) , 
     ALLOC_OUT                  NUMBER (12,4) , 
     SOM_QTY                    NUMBER (12,4) , 
     BACKORDER_QTY              NUMBER (12,4) , 
     FREEZE_IND                 VARCHAR2 (1) , 
     NEXT_1_WEEK_QTY            NUMBER (15) , 
     NEXT_2_WEEK_QTY            NUMBER (15) , 
     NEXT_3_WEEK_QTY            NUMBER (15) , 
     DIFF1_ID                   VARCHAR2 (10) , 
     DIFF1_DESC                 VARCHAR2 (120) , 
     DIFF2_ID                   VARCHAR2 (10) , 
     DIFF2_DESC                 VARCHAR2 (120) , 
     DIFF3_ID                   VARCHAR2 (10) , 
     DIFF3_DESC                 VARCHAR2 (120) , 
     PARENT_ITEM_ID             VARCHAR2 (25) , 
     CREATED_ORDER_NO           VARCHAR2 (40) , 
     CREATED_SUPPLIER_ID        VARCHAR2 (40) , 
     FUTURE_UNIT_RETAIL         NUMBER (20,4) , 
     RUSH_FLAG                  VARCHAR2 (1) , 
     COST                       NUMBER (20,4) , 
     IN_STORE_DATE              DATE , 
     FUTURE_ON_HAND_QTY         NUMBER (12,4) , 
     ORDER_NO                   VARCHAR2 (40) , 
     SOURCE_TYPE                NUMBER (1) , 
     GROSS_NEED_QTY             NUMBER (12,4) DEFAULT 0 , 
     RLOH_QTY                   NUMBER (12,4) DEFAULT 0 , 
     FILTERED_IND               VARCHAR2 (1) DEFAULT 'N' , 
     RESULT_FILTER_IND          VARCHAR2 (1) DEFAULT 'N' , 
     QUANTITY_LIMITS_FILTER_IND VARCHAR2 (1) DEFAULT 'N' , 
     PARENT_ITEM_LOC_SESSION_ID NUMBER (15) , 
     PACK_COMP_QTY              NUMBER (12,4) , 
     CREATED_BY                 VARCHAR2 (20) , 
     UPDATED_BY                 VARCHAR2 (20) , 
     CREATED_DATE               DATE , 
     UPDATE_DATE                DATE , 
     OBJECT_VERSION_ID          VARCHAR2 (20) , 
     QBE_FILTER_IND             VARCHAR2 (1) DEFAULT 'N' , 
     LOC_TYPE                   VARCHAR2 (1) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SESSION_ITEM_LOC';
ALTER TABLE ALC_SESSION_ITEM_LOC 
    ADD CONSTRAINT CHK_ALC_SESS_ITEM_LOC_LOC_TYP 
    CHECK (LOC_TYPE IN ('S','W')) 
;


COMMENT ON TABLE ALC_SESSION_ITEM_LOC IS 'Table stores Item and Location session information for an Allocation.  On Load this table will be loaded from Alc_item_source and Alc_item_loc tables.'
;


COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.ITEM_LOC_SESSION_ID IS 'Primery key for the table.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.FACET_SESSION_ID IS 'Entity Session ID per view' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.ITEM_LOC_ID IS 'Column refering item_loc column in alc_item_loc table.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.ALLOC_ID IS 'Allocation Id and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.ITEM_ID IS 'Item_id from ALC_ITEM_SOURCE table and ALC_ITEM_LOC table.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.ITEM_DESC IS 'Item Description.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.ITEM_TYPE IS 'Item Type valid values include:
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.WH_ID IS 'Warehouse Number.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.RELEASE_DATE IS 'Release Date.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.LOCATION_ID IS 'Store ID' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.LOCATION_DESC IS 'Store Description.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.GROUP_ID IS 'Store Group ID' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.GROUP_DESC IS 'Store Group Description.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.GROUP_TYPE IS 'Group Type ID' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.LOC_GROUP_ID IS 'Location Gropu Unique Id, used to refer the Complex group operation ID in Location screen.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.ALLOCATED_QTY IS 'Final Allocated Quantity.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.CALCULATED_QTY IS 'Calculated quantity given by Calculation Engine.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.NEED_QTY IS 'Net Need' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.ON_HAND_QTY IS 'Store ON Hand' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.IN_TRANSIT IS 'The in transit inventory position.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.ON_ORDER IS 'The on order inventory position.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.ON_ALLOC IS 'The on allocation inventory position.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.ALLOC_OUT IS 'The quantity currently allocated from the item/store' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.SOM_QTY IS 'Store Order Multiple Quantity' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.BACKORDER_QTY IS 'Used to store backorder quantity information used by Allocation Maintenance Results section.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.FREEZE_IND IS 'Freeze_ind refers if the Item_loc column in the UI is frozen (not to be changed when submitting another calculation). Frozen records will have the freeze_ind set to Y. Valid values are (Y, N).' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.NEXT_1_WEEK_QTY IS 'Next 1st Week Plan/Forecast Sales data.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.NEXT_2_WEEK_QTY IS 'Next 2nd Week Plan/Forecast Sales data.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.NEXT_3_WEEK_QTY IS 'Next 4th Week Plan/Forecast Sales data.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.DIFF1_ID IS 'Aggregate Diff 1 ID' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.DIFF1_DESC IS 'Aggregate Diff 1 Description' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.DIFF2_ID IS 'Aggregate Diff 2 ID' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.DIFF2_DESC IS 'Aggregate Diff 2 Description' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.DIFF3_ID IS 'Aggregate Diff 3 ID' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.DIFF3_DESC IS 'Aggregate Diff 3 Description' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.PARENT_ITEM_ID IS 'The Item node Id to refer the Style ID for a Fashion SKU.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.CREATED_ORDER_NO IS 'Allocation can create a Purchase order from What-if Allocation, the Purchase order created will be stored in this table.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.CREATED_SUPPLIER_ID IS 'Created Supplier ID.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.FUTURE_UNIT_RETAIL IS 'RPM Price for Item/Loc/Release Date' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.RUSH_FLAG IS 'A marker column to refer if a Item Loc is a Rush shipment.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.COST IS 'Item costing data for the Item' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.IN_STORE_DATE IS 'Allocation Instore Date' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.FUTURE_ON_HAND_QTY IS 'Future On hand quantity consists of In transi, In bound quantity' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.ORDER_NO IS 'Purchase Order Number' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.SOURCE_TYPE IS 'The type of the source transaction held in the order_no column.  Valid values are:
PO=1
ASN=2
OH=3 (Warehouse sourced)
WHATIF=4
BOL=5
AND TSF=6' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.GROSS_NEED_QTY IS 'Gross Need Quantity' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.RLOH_QTY IS 'Rule Level On Hand quantity' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.FILTERED_IND IS 'Filtered Indicator' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.RESULT_FILTER_IND IS 'Indicates whether or not the row is visible in the results UI' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.QUANTITY_LIMITS_FILTER_IND IS 'Indicates whether or not the row is visible in the quantity limits UI. ' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.PARENT_ITEM_LOC_SESSION_ID IS 'The column that refers the Item Loc Session Id in this table, this maintian the Parent and child relation in single table' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.PACK_COMP_QTY IS 'For non-sellable packs this column will hold the total number of components items in the pack.  For fasion pack groups this row will have the sum of components of all packs that make up the group.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.CREATED_BY IS 'Indicates the user who created the record.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.UPDATED_BY IS 'Indicates the user who last updated the record.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.CREATED_DATE IS 'The timestamp of the record creation date.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.UPDATE_DATE IS 'The timestamp of the record last updated date.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.OBJECT_VERSION_ID IS 'This column indicates Object version ID' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC.LOC_TYPE IS 'determines whether the location is a store or warehouse. Valid values are S and W' 
;
PROMPT CREATING INDEX 'PK_ALC_SESSION_ITEM_LOC';
CREATE UNIQUE INDEX PK_ALC_SESSION_ITEM_LOC ON ALC_SESSION_ITEM_LOC 
    ( 
     ITEM_LOC_SESSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_SESSION_ITEM_LOC_I2';
CREATE INDEX ALC_SESSION_ITEM_LOC_I2 ON ALC_SESSION_ITEM_LOC 
    ( 
     PARENT_ITEM_LOC_SESSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_SESSION_ITEM_LOC_I1';
CREATE INDEX ALC_SESSION_ITEM_LOC_I1 ON ALC_SESSION_ITEM_LOC 
    ( 
     FACET_SESSION_ID ASC , 
     ITEM_ID ASC , 
     WH_ID ASC , 
     LOCATION_ID ASC , 
     ITEM_LOC_SESSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_SESSION_ITEM_LOC';
ALTER TABLE ALC_SESSION_ITEM_LOC 
    ADD CONSTRAINT PK_ALC_SESSION_ITEM_LOC PRIMARY KEY ( ITEM_LOC_SESSION_ID ) 
    USING INDEX PK_ALC_SESSION_ITEM_LOC ;




PROMPT CREATING TABLE 'ALC_SESSION_ITEM_LOC_EXCL';
CREATE TABLE ALC_SESSION_ITEM_LOC_EXCL 
    ( 
     ITEM_LOC_EXCL_SESSION_ID NUMBER (25)  NOT NULL , 
     FACET_SESSION_ID         VARCHAR2 (50) , 
     ALLOC_ID                 NUMBER (15) , 
     ITEM_LOC_EXCL_ID         NUMBER (15) , 
     ITEM_ID                  VARCHAR2 (70) , 
     ITEM_DESC                VARCHAR2 (440) , 
     LOCATION_ID              VARCHAR2 (40) , 
     LOCATION_DESC            VARCHAR2 (150) , 
     REASON_CODE              NUMBER (5) , 
     DIFF1_ID                 VARCHAR2 (100) , 
     SOURCE_LOCATION_ID       VARCHAR2 (40) , 
     ORDER_NO                 VARCHAR2 (40) , 
     SOURCE_TYPE              NUMBER (1)  NOT NULL , 
     LOC_TYPE                 VARCHAR2 (1) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SESSION_ITEM_LOC_EXCL';
ALTER TABLE ALC_SESSION_ITEM_LOC_EXCL 
    ADD CONSTRAINT CHK_SESS_ITEM_LOC_EXCL_LOC_TYP 
    CHECK (LOC_TYPE IN ('S','W')) 
;


COMMENT ON TABLE ALC_SESSION_ITEM_LOC_EXCL IS 'This table holds item-location exclusion information for a session id view'
;


COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.ITEM_LOC_EXCL_SESSION_ID IS 'Primary key for the table' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.FACET_SESSION_ID IS 'Entity Session ID per view' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.ALLOC_ID IS 'Allocation Id  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.ITEM_LOC_EXCL_ID IS 'Column referring ITEM_LOC_EXCLUSION_ID column in ALC_ITEM_LOC_EXCLUSION table' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.ITEM_ID IS 'ITEM_ID from ALC_ITEM_SOURCE table' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.ITEM_DESC IS 'Item Description' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.LOCATION_ID IS 'Store ID' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.LOCATION_DESC IS 'Store Description' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.REASON_CODE IS 'Reason Code for Item-Location exclusion' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.DIFF1_ID IS 'Aggregate Diff 1 ID' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.SOURCE_LOCATION_ID IS 'Source warehouse id for the Excluded Item-Location' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.ORDER_NO IS 'The inventory transaction for the row.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.SOURCE_TYPE IS 'This column is used to determain the inventory Source type of the item which could be  1- PO, 2-ASO, 3-warehouse, 4-Whatif, 5-BOL, 6-TSF.' 
;

COMMENT ON COLUMN ALC_SESSION_ITEM_LOC_EXCL.LOC_TYPE IS 'determines whether the location is a store or warehouse. Valid values are S and W' 
;
PROMPT CREATING INDEX 'PK_ALC_SESSION_ITEM_LOC_EXCL';
CREATE UNIQUE INDEX PK_ALC_SESSION_ITEM_LOC_EXCL ON ALC_SESSION_ITEM_LOC_EXCL 
    ( 
     ITEM_LOC_EXCL_SESSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_SESSION_ITEM_LOC_EXCL_I1';
CREATE INDEX ALC_SESSION_ITEM_LOC_EXCL_I1 ON ALC_SESSION_ITEM_LOC_EXCL 
    ( 
     FACET_SESSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;

PROMPT CREATING PRIMARY KEY ON 'ALC_SESSION_ITEM_LOC_EXCL';
ALTER TABLE ALC_SESSION_ITEM_LOC_EXCL 
    ADD CONSTRAINT PK_ALC_SESSION_ITEM_LOC_EXCL PRIMARY KEY ( ITEM_LOC_EXCL_SESSION_ID ) 
    USING INDEX PK_ALC_SESSION_ITEM_LOC_EXCL ;




PROMPT CREATING TABLE 'ALC_SESSION_QUANTITY_LIMITS';
CREATE TABLE ALC_SESSION_QUANTITY_LIMITS 
    ( 
     QUANTITY_LIMITS_SESSION_ID NUMBER (15)  NOT NULL , 
     FACET_SESSION_ID           VARCHAR2 (50) , 
     QUANTITY_LIMITS_ID         NUMBER (20) , 
     ALLOC_ID                   NUMBER (15) , 
     DEPT                       NUMBER (4) , 
     CLASS                      NUMBER (4) , 
     SUBCLASS                   NUMBER (4) , 
     MIN                        NUMBER (12,4) , 
     MAX                        NUMBER (12,4) , 
     THRESHOLD                  NUMBER (12,4) , 
     TREND                      NUMBER (12,4) , 
     WOS                        NUMBER (12,4) , 
     MIN_NEED                   NUMBER (12,4) , 
     ITEM_ID                    VARCHAR2 (70)  NOT NULL , 
     LOCATION_ID                NUMBER (10)  NOT NULL , 
     LOC_TYPE                   VARCHAR2 (1) ,
	 MIN_PACK                   NUMBER (12,4) ,
	 MAX_PACK                   NUMBER (12,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SESSION_QUANTITY_LIMITS';
ALTER TABLE ALC_SESSION_QUANTITY_LIMITS 
    ADD CONSTRAINT CHK_ALC_SESS_QTY_LMTS_LOC_TYP 
    CHECK (LOC_TYPE IN ('S','W')) 
;


COMMENT ON TABLE ALC_SESSION_QUANTITY_LIMITS IS 'Table for storing Quantity limits session detail. On load it would be loaded from alc_quantity_limits table'
;


COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.QUANTITY_LIMITS_SESSION_ID IS 'Primary key for the Table' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.FACET_SESSION_ID IS 'Entity Session ID per View.' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.QUANTITY_LIMITS_ID IS 'Reference ID for Alc_Quantity_limits table' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.ALLOC_ID IS 'Allocation Number  and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.DEPT IS 'Deptment Number' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.CLASS IS 'CLASS NUMBER' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.SUBCLASS IS 'SUBCLASS NUMBER' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.MIN IS 'Min Quantity.' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.MAX IS 'Max Quantity' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.THRESHOLD IS 'Threshold Quantity.' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.TREND IS 'Trends' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.WOS IS 'Weeks of Supply' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.MIN_NEED IS 'Min Need.' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.ITEM_ID IS 'The item to apply the quantity limits to.' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.LOCATION_ID IS 'The location to apply the quantity limits to' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.LOC_TYPE IS 'Determines whether the location is a store or warehouse. Valid values are S and W' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.MIN_PACK is 'This column contains the Minimum Pack Value. This value will ensure that a minimum number of packs will be allocated' 
;

COMMENT ON COLUMN ALC_SESSION_QUANTITY_LIMITS.MAX_PACK is 'This column contains the Maximum Pack Value. This value will ensure that the number of allocated packs will not exceed this value'
;
PROMPT CREATING INDEX 'PK_ALC_SESSION_QUANTITY_LIMITS';
CREATE UNIQUE INDEX PK_ALC_SESSION_QUANTITY_LIMITS ON ALC_SESSION_QUANTITY_LIMITS 
    ( 
     QUANTITY_LIMITS_SESSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_SESSION_QUANTITY_LIMITS_I1';
CREATE INDEX ALC_SESSION_QUANTITY_LIMITS_I1 ON ALC_SESSION_QUANTITY_LIMITS 
    ( 
     FACET_SESSION_ID ASC , 
     ITEM_ID ASC , 
     LOCATION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_SESSION_QUANTITY_LIMITS';
ALTER TABLE ALC_SESSION_QUANTITY_LIMITS 
    ADD CONSTRAINT PK_ALC_SESSION_QUANTITY_LIMITS PRIMARY KEY ( QUANTITY_LIMITS_SESSION_ID ) 
    USING INDEX PK_ALC_SESSION_QUANTITY_LIMITS ;




PROMPT CREATING TABLE 'ALC_SESSION_SIZE_PROFILE';
CREATE TABLE ALC_SESSION_SIZE_PROFILE 
    ( 
     SESSION_ID          VARCHAR2 (64)  NOT NULL , 
     RESULT_TYPE         NUMBER (1)  NOT NULL , 
     SEQ_ID              NUMBER (4)  NOT NULL , 
     SIZE_PROFILE_ID     NUMBER (20), 
     NEW_SIZE_PROFILE_ID NUMBER (20) , 
     GID_PROFILE_ID      NUMBER (15) , 
     STORE               VARCHAR2 (40) , 
     DEPT                VARCHAR2 (40) , 
     CLASS               VARCHAR2 (40) , 
     SUBCLASS            VARCHAR2 (40) , 
     STYLE               VARCHAR2 (25) , 
     STYLEDESC           VARCHAR2 (250) , 
     CHILD_DIFF1         VARCHAR2 (40) , 
     DIFF_GROUP_ID1      VARCHAR2 (40) , 
     CHILD_DIFF2         VARCHAR2 (40) , 
     DIFF_GROUP_ID2      VARCHAR2 (40) , 
     CHILD_DIFF3         VARCHAR2 (40) , 
     DIFF_GROUP_ID3      VARCHAR2 (40) , 
     CHILD_DIFF4         VARCHAR2 (40) , 
     DIFF_GROUP_ID4      VARCHAR2 (40) , 
     DISPLAY_SEQ         NUMBER (4) , 
     QTY                 NUMBER (12,4) , 
     SIZE_PROFILE        VARCHAR2 (512) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_SESSION_SIZE_PROFILE IS 'This temporary table, holds the list of size profiles (child rows of gid headers) derived from the user search criteria.'
;


COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.SESSION_ID IS 'Represents a unique size profile session id.' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.RESULT_TYPE IS 'Represents the type of row whether it is derived from search(1) or copy(2), to simulate two instances of rows.' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.SEQ_ID IS 'Represents the row id or the index of results.' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.SIZE_PROFILE_ID IS 'Represents the size profile id as in alc_size_profile.size_profile_id' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.NEW_SIZE_PROFILE_ID IS 'Represents size profile id in case of new size profile' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.GID_PROFILE_ID IS 'Represents the gid profile id of size profile as in alc_size_profile.gid_profile_id' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.STORE IS 'Represents the store of size profile as in alc_size_profile.store' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.DEPT IS 'Represents the dept of size profile as in alc_size_profile.dept' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.CLASS IS 'Represents the class of size profile as in alc_size_profile.class' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.SUBCLASS IS 'Represents the subclass of size profile as in alc_size_profile.subclass' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.STYLE IS 'Represents the style of size profile as in alc_size_profile.style' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.STYLEDESC IS 'Represents the style desc of size profile' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.CHILD_DIFF1 IS 'Represents the child diff1 of size profile as in alc_size_profile.size1' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.DIFF_GROUP_ID1 IS 'Represents the diff group id1 of size profile' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.CHILD_DIFF2 IS 'Represents the chid diff2 of size profile as in alc_size_profile.size2' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.DIFF_GROUP_ID2 IS 'Represents the diff group id2 of size profile' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.CHILD_DIFF3 IS 'Represents the chid diff3 of size profile as in alc_size_profile.size3' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.DIFF_GROUP_ID3 IS 'Represents the diff group id3 of size profile' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.CHILD_DIFF4 IS 'Represents the child diff4 of size profile as in alc_size_profile.size4' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.DIFF_GROUP_ID4 IS 'Represents the diff group id4 of size profile' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.DISPLAY_SEQ IS 'Represents the sequence of diffs for displaying' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.QTY IS 'Represents the size profile ratio' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE.SIZE_PROFILE IS 'Represents the level of size profile' 
;
PROMPT CREATING INDEX 'ALC_SESSION_SIZE_PROFILE_PK';
CREATE UNIQUE INDEX ALC_SESSION_SIZE_PROFILE_PK ON ALC_SESSION_SIZE_PROFILE 
    ( 
     SESSION_ID ASC , 
     RESULT_TYPE ASC , 
     SEQ_ID ASC , 
     SIZE_PROFILE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_SESSION_SIZE_PROFILE';
ALTER TABLE ALC_SESSION_SIZE_PROFILE 
    ADD CONSTRAINT ALC_SESSION_SIZE_PROFILE_PK PRIMARY KEY ( SESSION_ID, RESULT_TYPE, SEQ_ID, SIZE_PROFILE_ID ) 
    USING INDEX ALC_SESSION_SIZE_PROFILE_PK ;




PROMPT CREATING TABLE 'ALC_SESSION_SIZE_PROFILE_RATIO';
CREATE TABLE ALC_SESSION_SIZE_PROFILE_RATIO 
    ( 
     SESSION_ID         VARCHAR2 (64)  NOT NULL , 
     RESULT_TYPE        NUMBER (1)  NOT NULL , 
     SEQ_ID             NUMBER (4)  NOT NULL , 
     SIZE_PROFILE_LEVEL VARCHAR2 (512) , 
     SIZE_PROFILE       VARCHAR2 (512) , 
     RATIO              VARCHAR2 (16) , 
     AGGREGATEDDIFF     VARCHAR2 (512) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_SESSION_SIZE_PROFILE_RATIO IS 'This temporary table holds the rolled up size profile results'
;


COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE_RATIO.SESSION_ID IS 'Represents a unique size profile session id.' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE_RATIO.RESULT_TYPE IS 'Represents the type of row whether it is derived from search(1) or copy(2), to simulate two instances of rows.' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE_RATIO.SEQ_ID IS 'Represents the row id or the index of results.' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE_RATIO.SIZE_PROFILE_LEVEL IS 'Represents the size profile level' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE_RATIO.SIZE_PROFILE IS 'Represents the size profile details' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE_RATIO.RATIO IS 'Represents the ratio of size profile' 
;

COMMENT ON COLUMN ALC_SESSION_SIZE_PROFILE_RATIO.AGGREGATEDDIFF IS 'Represents the aggregated diff of size profile' 
;
PROMPT CREATING INDEX 'ALC_SESSION_SP_RATIO_PK';
CREATE UNIQUE INDEX ALC_SESSION_SP_RATIO_PK ON ALC_SESSION_SIZE_PROFILE_RATIO 
    ( 
     SESSION_ID ASC , 
     RESULT_TYPE ASC , 
     SEQ_ID ASC , 
     SIZE_PROFILE ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_SESSION_SIZE_PROFILE_RATIO';
ALTER TABLE ALC_SESSION_SIZE_PROFILE_RATIO 
    ADD CONSTRAINT ALC_SESSION_SP_RATIO_PK PRIMARY KEY ( SESSION_ID, RESULT_TYPE, SEQ_ID, SIZE_PROFILE ) 
    USING INDEX ALC_SESSION_SP_RATIO_PK ;


PROMPT CREATING TABLE 'ALC_SHIPPING_SCHEDULE';
CREATE TABLE ALC_SHIPPING_SCHEDULE 
    ( 
     SHIPPING_SCHEDULE_ID NUMBER (10)  NOT NULL , 
     DEPT                 NUMBER (4)  NOT NULL , 
     CLASS                NUMBER (4) , 
     SUBCLASS             NUMBER (4) , 
     ITEM                 VARCHAR2 (25) , 
     ORIGIN               NUMBER (10)  NOT NULL , 
     DESTINATION          NUMBER (10)  NOT NULL , 
     ORIGIN_TYPE          VARCHAR2 (2)  NOT NULL , 
     DESTINATION_TYPE     VARCHAR2 (1)  NOT NULL , 
     DEPARTURE_DATE       DATE  NOT NULL , 
     ARRIVAL_DATE         DATE  NOT NULL 
    ) 
        TABLESPACE RETAIL_DATA
		INITRANS 6 
    PARTITION BY HASH ( ORIGIN, DEPT ) 
    PARTITIONS 16;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SHIPPING_SCHEDULE';
ALTER TABLE ALC_SHIPPING_SCHEDULE 
    ADD CONSTRAINT CHK_ASS_ORIGIN_TYPE 
    CHECK ( ORIGIN_TYPE IN ('W')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SHIPPING_SCHEDULE';
ALTER TABLE ALC_SHIPPING_SCHEDULE 
    ADD CONSTRAINT CHK_ASS_DESTINATION_TYPE 
    CHECK ( DESTINATION_TYPE IN ('S', 'W')) 
;


COMMENT ON TABLE ALC_SHIPPING_SCHEDULE IS 'this table holds the shipping schedule information.this data is expected to be populated from an external system.'
;


COMMENT ON COLUMN ALC_SHIPPING_SCHEDULE.SHIPPING_SCHEDULE_ID IS 'primary key for the table, automatically generated by the system' 
;

COMMENT ON COLUMN ALC_SHIPPING_SCHEDULE.DEPT IS 'Department number' 
;

COMMENT ON COLUMN ALC_SHIPPING_SCHEDULE.CLASS IS 'Class number' 
;

COMMENT ON COLUMN ALC_SHIPPING_SCHEDULE.SUBCLASS IS 'Subclass number' 
;

COMMENT ON COLUMN ALC_SHIPPING_SCHEDULE.ITEM IS 'The Item id bieng used. populated from item master table' 
;

COMMENT ON COLUMN ALC_SHIPPING_SCHEDULE.ORIGIN IS 'the origin is the from location for an inventory movement. the origin values recognized by allocations will always be warehouses the warehouses entered in this column will be the virtual warehouses th exist within the retek merchandising systems wh table.at' 
;

COMMENT ON COLUMN ALC_SHIPPING_SCHEDULE.DESTINATION IS 'the destination is the to location for an inventory movement. the destination recognized by allocations will always be warehouses the warehouses entered in this column will be the virtual warehouses that exist within the retek merchandising systems wh table.' 
;

COMMENT ON COLUMN ALC_SHIPPING_SCHEDULE.ORIGIN_TYPE IS 'the origin type will contain a warehouse value.' 
;

COMMENT ON COLUMN ALC_SHIPPING_SCHEDULE.DESTINATION_TYPE IS 'the destination type will contain a store or warehouse value. stores are final destinations. warehouses are multi level distribution destination.' 
;

COMMENT ON COLUMN ALC_SHIPPING_SCHEDULE.DEPARTURE_DATE IS 'this is the date that inventory leaves the origin location.' 
;

COMMENT ON COLUMN ALC_SHIPPING_SCHEDULE.ARRIVAL_DATE IS 'this is the date that inventory arrives at the destination location given the  departure date associated with the table record.' 
;
PROMPT CREATING INDEX 'PK_ALC_SHIPPING_SCHEDULE';
CREATE UNIQUE INDEX PK_ALC_SHIPPING_SCHEDULE ON ALC_SHIPPING_SCHEDULE 
    ( 
     SHIPPING_SCHEDULE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'UK_ALC_SHIPPING_SCHEDULE';
CREATE UNIQUE INDEX UK_ALC_SHIPPING_SCHEDULE ON ALC_SHIPPING_SCHEDULE 
    ( 
     DEPT ASC , 
     CLASS ASC , 
     SUBCLASS ASC , 
     ITEM ASC , 
     ORIGIN ASC , 
     DESTINATION ASC , 
     DEPARTURE_DATE ASC , 
     DESTINATION_TYPE ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_SHIPPING_SCHEDULE';
ALTER TABLE ALC_SHIPPING_SCHEDULE 
    ADD CONSTRAINT PK_ALC_SHIPPING_SCHEDULE PRIMARY KEY ( SHIPPING_SCHEDULE_ID ) 
    USING INDEX PK_ALC_SHIPPING_SCHEDULE ;


PROMPT CREATING UNIQUE KEY ON 'ALC_SHIPPING_SCHEDULE';
ALTER TABLE ALC_SHIPPING_SCHEDULE 
    ADD CONSTRAINT UK_ALC_SHIPPING_SCHEDULE UNIQUE ( DEPT , CLASS , SUBCLASS , ITEM , ORIGIN , DESTINATION , DEPARTURE_DATE , DESTINATION_TYPE ) 
    USING INDEX UK_ALC_SHIPPING_SCHEDULE ;




PROMPT CREATING TABLE 'ALC_SIZE_PROFILE';
CREATE TABLE ALC_SIZE_PROFILE 
    ( 
     SIZE_PROFILE_ID       NUMBER (20)  NOT NULL , 
     LOC                   VARCHAR2 (40)  NOT NULL , 
     DEPT                  VARCHAR2 (40) , 
     CLASS                 VARCHAR2 (40) , 
     SUBCLASS              VARCHAR2 (40) , 
     STYLE                 VARCHAR2 (40) , 
     SIZE1                 VARCHAR2 (40) , 
     SIZE2                 VARCHAR2 (40) , 
     SIZE3                 VARCHAR2 (40) , 
     SIZE4                 VARCHAR2 (40) , 
     SIZE_GROUP1           VARCHAR2 (40)  NOT NULL , 
     SIZE_GROUP2           VARCHAR2 (40) , 
     QTY                   NUMBER (12,4)  NOT NULL , 
     CREATED_BY            VARCHAR2 (64)  NOT NULL , 
     CREATION_DATE         TIMESTAMP  NOT NULL , 
     LAST_UPDATED_BY       VARCHAR2 (64)  NOT NULL , 
     LAST_UPDATE_DATE      TIMESTAMP  NOT NULL , 
     LAST_UPDATE_LOGIN     VARCHAR2 (32)  NOT NULL , 
     OBJECT_VERSION_NUMBER NUMBER (9)  NOT NULL , 
     GID_PROFILE_ID        NUMBER (15) , 
     SIZE_PROFILE_LEVEL    NUMBER (1) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SIZE_PROFILE';
ALTER TABLE ALC_SIZE_PROFILE 
    ADD CONSTRAINT CHK_HEIR_FOR_STYLE 
    CHECK (((STYLE IS NOT NULL AND DEPT IS NULL AND CLASS IS NULL
AND SUBCLASS IS NULL)
OR
(STYLE IS NULL AND DEPT IS NOT NULL)))
;


COMMENT ON TABLE ALC_SIZE_PROFILE IS 'this table holds all size profile information used within allocation. this data is used to spread the aggregate need to the sku level within the calculation process. this data can be provided to the system at the following levels: department-size, department-class-size, department-class-subclass-size, style-size, sku-size.this data can be either populated from an external system or entered through size profile set up.'
;


COMMENT ON COLUMN ALC_SIZE_PROFILE.SIZE_PROFILE_ID IS 'Indicates the primary key the gets generated from the sequence' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.LOC IS 'The location the size profile applies to.' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.DEPT IS 'Department number' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.CLASS IS 'Class number' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.SUBCLASS IS 'Subclass number' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.STYLE IS 'this column would contain the style identifier (RMS parent or Grandparent item), if populated.' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.SIZE1 IS 'this column contains the size1 differentiator and is mapped to diff1 from rms item_master. the size1 value is aggregate or non-aggregate for style level. other levels (dept/class/subclass) are non-aggregate.' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.SIZE2 IS 'this column contains the size2 differentiator and is mapped to diff2 from rms item_master. the size2 value is aggregate or non-aggregate for style level. other levels (dept/class/subclass) are non-aggregate.' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.SIZE3 IS 'this column contains the size3 differentiator and is mapped to diff3 from rms item_master. the size3 value is aggregate or non-aggregate for style level. other levels (dept/class/subclass) are non-aggregate.' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.SIZE4 IS 'this column contains the size4 differentiator and is mapped to diff4 from rms item_master. the size4 value is aggregate or non-aggregate for style level. other levels (dept/class/subclass) are non-aggregate.' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.SIZE_GROUP1 IS 'this column contains the size1 group identifier. (diff group of diff1 in rms)' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.SIZE_GROUP2 IS 'this column would contain the size2 group identifier. (diff group of diff2 in rms)' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.QTY IS 'this column contains the value for this size profile. this value will converted into a proportional value so the value entered for group do not need to add up to 1.' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.CREATED_BY IS 'Indicates the user who created the record' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.CREATION_DATE IS 'Indicates the user who last updated the record' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.LAST_UPDATED_BY IS 'The timestamp of the record creation date' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.LAST_UPDATE_DATE IS 'The timestamp of the record last updated date' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.LAST_UPDATE_LOGIN IS 'The session login associated to the user who last updated the row.' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.OBJECT_VERSION_NUMBER IS 'This column indicates Object version number.' 
;

COMMENT ON COLUMN ALC_SIZE_PROFILE.GID_PROFILE_ID IS 'If a GID profile (Season numerical code) was assigned to the size profile when interfaced from SPO, it will be captured here. GID is not required.' 
;
PROMPT CREATING INDEX 'PK_ALC_SIZE_PROFILE';
CREATE UNIQUE INDEX PK_ALC_SIZE_PROFILE ON ALC_SIZE_PROFILE 
    ( 
     SIZE_PROFILE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'UK_ALC_SIZE_PROFILE';
CREATE UNIQUE INDEX UK_ALC_SIZE_PROFILE ON ALC_SIZE_PROFILE 
    ( 
     LOC ASC , 
     DEPT ASC , 
     CLASS ASC , 
     SUBCLASS ASC , 
     STYLE ASC , 
     SIZE1 ASC , 
     SIZE2 ASC , 
     SIZE3 ASC , 
     SIZE4 ASC , 
     SIZE_GROUP1 ASC , 
     SIZE_GROUP2 ASC , 
     GID_PROFILE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_SIZE_PROFILE_I4';
CREATE INDEX ALC_SIZE_PROFILE_I4 ON ALC_SIZE_PROFILE 
    ( 
     LOC,STYLE,NVL("SIZE1",'0'),NVL("SIZE2",'0'),DEPT,SUBCLASS,CLASS,GID_PROFILE_ID,NVL("SIZE3",'0'),NVL("SIZE4",'0')
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 16;
PROMPT CREATING INDEX 'ALC_SIZE_PROFILE_I2';
CREATE INDEX ALC_SIZE_PROFILE_I2 ON ALC_SIZE_PROFILE 
    ( 
     STYLE ASC , 
     LOC ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_SIZE_PROFILE_I3';
CREATE INDEX ALC_SIZE_PROFILE_I3 ON ALC_SIZE_PROFILE 
    ( 
     GID_PROFILE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_SIZE_PROFILE_I1';
CREATE INDEX ALC_SIZE_PROFILE_I1 ON ALC_SIZE_PROFILE 
    ( 
     LOC ASC , 
     STYLE ASC , 
     SIZE1 ASC , 
     SIZE2 ASC , 
     SIZE3 ASC , 
     SIZE4 ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_SIZE_PROFILE';
ALTER TABLE ALC_SIZE_PROFILE 
    ADD CONSTRAINT PK_ALC_SIZE_PROFILE PRIMARY KEY ( SIZE_PROFILE_ID ) 
    USING INDEX PK_ALC_SIZE_PROFILE ;


PROMPT CREATING UNIQUE KEY ON 'ALC_SIZE_PROFILE';
ALTER TABLE ALC_SIZE_PROFILE 
    ADD CONSTRAINT UK_ALC_SIZE_PROFILE UNIQUE ( LOC , DEPT , CLASS , SUBCLASS , STYLE , SIZE1 , SIZE2 , SIZE3 , SIZE4 , SIZE_GROUP1 , SIZE_GROUP2 , GID_PROFILE_ID ) 
    USING INDEX UK_ALC_SIZE_PROFILE ;




PROMPT CREATING TABLE 'ALC_SUBCLASS_ALLOC_IN_EOD';
CREATE TABLE ALC_SUBCLASS_ALLOC_IN_EOD 
    ( 
     DEPT                          NUMBER (4) , 
     CLASS                         NUMBER (4) , 
     SUBCLASS                      NUMBER (4) , 
     LOC                           NUMBER (10) , 
     LOC_TYPE                      VARCHAR2 (1) , 
     ALLOC_IN_DATE                 DATE , 
     ALL_ORDERS_ALLOC_IN_QTY       NUMBER (24,4) , 
     NOT_ALL_ORDERS_ALLOC_IN_QTY   NUMBER (24,4) , 
     NO_CLR_ALL_ORD_ALC_IN_QTY     NUMBER (24,4) , 
     NO_CLR_NOT_ALL_ORD_ALC_IN_QTY NUMBER (24,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_SUBCLASS_ALLOC_IN_EOD IS 'This table pre-aggregates allocation in values from RMS to the subclass/store/end of week date level.  It is meant to aid the performance of the calculation process.  It is accessed by the calculation process when the RLOH setting is snapshot.'
;


COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_IN_EOD.DEPT IS 'The department of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_IN_EOD.CLASS IS 'The class of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_IN_EOD.SUBCLASS IS 'The subclass of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_IN_EOD.LOC IS 'The location of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_IN_EOD.LOC_TYPE IS 'The location type of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_IN_EOD.ALLOC_IN_DATE IS 'The end of week date of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_IN_EOD.ALL_ORDERS_ALLOC_IN_QTY IS 'The allocation in value for all orders in rms.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_IN_EOD.NOT_ALL_ORDERS_ALLOC_IN_QTY IS 'The allocation in value for orders in rms that have their include_on_order_ind set to Y.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_IN_EOD.NO_CLR_ALL_ORD_ALC_IN_QTY IS 'The allocation in value for all orders in rms.  This is the same as ALL_ORDERS_ALLOC_IN_QTY except that item/locations that are on clearance are not included.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_IN_EOD.NO_CLR_NOT_ALL_ORD_ALC_IN_QTY IS 'The allocation in value for orders in rms that have their include_on_order_ind set to Y.  This is the same as NOT_ALL_ORDERS_ALLOC_IN_QTY except that item/locations that are on clearance are not included.' 
;



PROMPT CREATING TABLE 'ALC_SUBCLASS_ALLOC_OUT_EOD';
CREATE TABLE ALC_SUBCLASS_ALLOC_OUT_EOD 
    ( 
     DEPT                 NUMBER (4) , 
     CLASS                NUMBER (4) , 
     SUBCLASS             NUMBER (4) , 
     LOC                  NUMBER (10) , 
     LOC_TYPE             VARCHAR2 (1) , 
     ALLOC_OUT_DATE       DATE , 
     ALLOC_OUT_QTY        NUMBER (24,4) , 
     NO_CLR_ALLOC_OUT_QTY NUMBER (24,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_SUBCLASS_ALLOC_OUT_EOD IS 'This table pre-aggregates allocation out data from RMS to the subclass/store level.  It is meant to aid the performance of the calculation process.  It is accessed by the calculation process when the RLOH setting is snapshot.'
;


COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_OUT_EOD.DEPT IS 'The department of the rolled up alloc out.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_OUT_EOD.CLASS IS 'The class of the rolled up alloc out.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_OUT_EOD.SUBCLASS IS 'The subclass of the rolled up alloc out.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_OUT_EOD.LOC IS 'The location of the rolled up alloc out.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_OUT_EOD.LOC_TYPE IS 'The location type of the rolled up alloc out.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_OUT_EOD.ALLOC_OUT_DATE IS 'The date of the rolled up alloc out.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_OUT_EOD.ALLOC_OUT_QTY IS 'Allocation out value rolled up to the subclass/location level.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ALLOC_OUT_EOD.NO_CLR_ALLOC_OUT_QTY IS 'Allocation out value rolled up to the subclass/location level excluding item location combinations that are on clearance.' 
;



PROMPT CREATING TABLE 'ALC_SUBCLASS_CROSSLINK_EOD';
CREATE TABLE ALC_SUBCLASS_CROSSLINK_EOD 
    ( 
     DEPT                 NUMBER (4) , 
     CLASS                NUMBER (4) , 
     SUBCLASS             NUMBER (4) , 
     LOC                  NUMBER (10) , 
     LOC_TYPE             VARCHAR2 (1) , 
     CROSSLINK_QTY        NUMBER (24,4) , 
     NO_CLR_CROSSLINK_QTY NUMBER (24,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_SUBCLASS_CROSSLINK_EOD IS 'This table pre-aggregates crosslink values from RMS to the subclass/store level.  It is meant to aid the performance of the calculation process.  It is accessed by the calculation process when the RLOH setting is snapshot.'
;


COMMENT ON COLUMN ALC_SUBCLASS_CROSSLINK_EOD.DEPT IS 'The department of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CROSSLINK_EOD.CLASS IS 'The class of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CROSSLINK_EOD.SUBCLASS IS 'The subclass of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CROSSLINK_EOD.LOC IS 'The location of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CROSSLINK_EOD.LOC_TYPE IS 'The location type of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CROSSLINK_EOD.CROSSLINK_QTY IS 'Crosslink quantity value rolled up to the subclass/location level.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CROSSLINK_EOD.NO_CLR_CROSSLINK_QTY IS 'Crosslink quantity value rolled up to the subclass/location level.  This is the same as CROSSLINK_QTY except that item/locations that are on clearance are not included.' 
;



PROMPT CREATING TABLE 'ALC_SUBCLASS_CUST_ORDER_EOD';
CREATE TABLE ALC_SUBCLASS_CUST_ORDER_EOD 
    ( 
     DEPT                   NUMBER (4) , 
     CLASS                  NUMBER (4) , 
     SUBCLASS               NUMBER (4) , 
     LOC                    NUMBER (10) , 
     LOC_TYPE               VARCHAR2 (1) , 
     CO_ALLOC_IN_QTY        NUMBER (20,4) , 
     CO_INTRAN_QTY          NUMBER (20,4) , 
     NO_CLR_CO_ALLOC_IN_QTY NUMBER (20,4) , 
     NO_CLR_CO_INTRAN_QTY   NUMBER (20,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON COLUMN ALC_SUBCLASS_CUST_ORDER_EOD.DEPT IS 'The department of the rolled up customer order.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CUST_ORDER_EOD.CLASS IS 'The class of the rolled up customer order.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CUST_ORDER_EOD.SUBCLASS IS 'The subclass of the rolled up customer order.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CUST_ORDER_EOD.LOC IS 'The location of the rolled up customer order.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CUST_ORDER_EOD.LOC_TYPE IS 'The location type of the rolled up customer order.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CUST_ORDER_EOD.CO_ALLOC_IN_QTY IS 'Customer order expected transfer value rolled up to the subclass/location level from CO type transfers.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CUST_ORDER_EOD.CO_INTRAN_QTY IS 'Customer order in transit transfer value rolled up to the subclass/location level from CO type transfers.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CUST_ORDER_EOD.NO_CLR_CO_ALLOC_IN_QTY IS 'Customer order expected transfer value rolled up to the subclass/location level from CO type transfers.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_CUST_ORDER_EOD.NO_CLR_CO_INTRAN_QTY IS 'Customer order in transit transfer value rolled up to the subclass/location level from CO type transfers excluding item location combinations that are on clearance.' 
;



PROMPT CREATING TABLE 'ALC_SUBCLASS_ITEM_LOC_SOH_EOD';
CREATE TABLE ALC_SUBCLASS_ITEM_LOC_SOH_EOD 
    ( 
     DEPT                          NUMBER (4) , 
     CLASS                         NUMBER (4) , 
     SUBCLASS                      NUMBER (4) , 
     LOC                           NUMBER (10) , 
     LOC_TYPE                      VARCHAR2 (1) , 
     STOCK_ON_HAND                 NUMBER (20,4) , 
     IN_TRANSIT_QTY                NUMBER (20,4) , 
     PACK_COMP_INTRAN              NUMBER (20,4) , 
     PACK_COMP_SOH                 NUMBER (20,4) , 
     TSF_RESERVED_QTY              NUMBER (20,4) , 
     PACK_COMP_RESV                NUMBER (20,4) , 
     TSF_EXPECTED_QTY              NUMBER (20,4) , 
     PACK_COMP_EXP                 NUMBER (20,4) , 
     RTV_QTY                       NUMBER (20,4) , 
     NON_SELLABLE_QTY              NUMBER (20,4) , 
     CUSTOMER_RESV                 NUMBER (20,4) , 
     CUSTOMER_BACKORDER            NUMBER (20,4) , 
     PACK_COMP_CUST_RESV           NUMBER (20,4) , 
     PACK_COMP_CUST_BACK           NUMBER (20,4) , 
     PACK_COMP_NON_SELLABLE        NUMBER (20,4) , 
     NO_CLR_STOCK_ON_HAND          NUMBER (20,4) , 
     NO_CLR_IN_TRANSIT_QTY         NUMBER (20,4) , 
     NO_CLR_PACK_COMP_INTRAN       NUMBER (20,4) , 
     NO_CLR_PACK_COMP_SOH          NUMBER (20,4) , 
     NO_CLR_TSF_RESERVED_QTY       NUMBER (20,4) , 
     NO_CLR_PACK_COMP_RESV         NUMBER (20,4) , 
     NO_CLR_TSF_EXPECTED_QTY       NUMBER (20,4) , 
     NO_CLR_PACK_COMP_EXP          NUMBER (20,4) , 
     NO_CLR_RTV_QTY                NUMBER (20,4) , 
     NO_CLR_NON_SELLABLE_QTY       NUMBER (20,4) , 
     NO_CLR_CUSTOMER_RESV          NUMBER (20,4) , 
     NO_CLR_CUSTOMER_BACKORDER     NUMBER (20,4) , 
     NO_CLR_PACK_COMP_CUST_RESV    NUMBER (20,4) , 
     NO_CLR_PACK_COMP_CUST_BACK    NUMBER (20,4) , 
     NO_CLR_PACK_COMP_NON_SELLABLE NUMBER (20,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_SUBCLASS_ITEM_LOC_SOH_EOD IS 'This table pre-aggregates inventory buckets from RMS to the subclass/store level.  It is meant to aid the performance of the calculation process.  It is accessed by the calculation process when the RLOH setting is snapshot.'
;


COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.DEPT IS 'The department of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.CLASS IS 'The class of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.SUBCLASS IS 'The subclass of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.LOC IS 'The location of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.LOC_TYPE IS 'The location type of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.STOCK_ON_HAND IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.IN_TRANSIT_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.PACK_COMP_INTRAN IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.PACK_COMP_SOH IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.TSF_RESERVED_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.PACK_COMP_RESV IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.TSF_EXPECTED_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.PACK_COMP_EXP IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.RTV_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NON_SELLABLE_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.CUSTOMER_RESV IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.CUSTOMER_BACKORDER IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.PACK_COMP_CUST_RESV IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.PACK_COMP_CUST_BACK IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.PACK_COMP_NON_SELLABLE IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_STOCK_ON_HAND IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_IN_TRANSIT_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_PACK_COMP_INTRAN IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_PACK_COMP_SOH IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_TSF_RESERVED_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_PACK_COMP_RESV IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_TSF_EXPECTED_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_PACK_COMP_EXP IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_RTV_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_NON_SELLABLE_QTY IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_CUSTOMER_RESV IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_CUSTOMER_BACKORDER IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_PACK_COMP_CUST_RESV IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_PACK_COMP_CUST_BACK IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ITEM_LOC_SOH_EOD.NO_CLR_PACK_COMP_NON_SELLABLE IS 'Inventory value rolled up to the subclass/location level from ITEM_LOC_SOH excluding item location combinations that are on clearance' 
;



PROMPT CREATING TABLE 'ALC_SUBCLASS_ON_ORDER_EOD';
CREATE TABLE ALC_SUBCLASS_ON_ORDER_EOD 
    ( 
     DEPT                          NUMBER (4) , 
     CLASS                         NUMBER (4) , 
     SUBCLASS                      NUMBER (4) , 
     LOC                           NUMBER (10) , 
     LOC_TYPE                      VARCHAR2 (1) , 
     ON_ORDER_DATE                 DATE , 
     ALL_ORDERS_ON_ORDER_QTY       NUMBER (24,4) , 
     NOT_ALL_ORDERS_ON_ORDER_QTY   NUMBER (24,4) , 
     NO_CLR_ALL_ORD_ON_ORD_QTY     NUMBER (24,4) , 
     NO_CLR_NOT_ALL_ORD_ON_ORD_QTY NUMBER (24,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_SUBCLASS_ON_ORDER_EOD IS 'This table pre-aggregates on order values from RMS to the subclass/store/end of week date level.  It is meant to aid the performance of the calculation process.  It is accessed by the calculation process when the RLOH setting is snapshot.'
;


COMMENT ON COLUMN ALC_SUBCLASS_ON_ORDER_EOD.DEPT IS 'The department of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ON_ORDER_EOD.CLASS IS 'The class of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ON_ORDER_EOD.SUBCLASS IS 'The subclass of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ON_ORDER_EOD.LOC IS 'The location of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ON_ORDER_EOD.LOC_TYPE IS 'The location type of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ON_ORDER_EOD.ON_ORDER_DATE IS 'The end of week date of the rolled up inventory.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ON_ORDER_EOD.ALL_ORDERS_ON_ORDER_QTY IS 'The on order value for all orders in rms.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ON_ORDER_EOD.NOT_ALL_ORDERS_ON_ORDER_QTY IS 'The on order value for orders in rms that have their include_on_order_ind set to Y.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ON_ORDER_EOD.NO_CLR_ALL_ORD_ON_ORD_QTY IS 'The on order value for all orders in rms.  This is the same as ALL_ORDERS_ON_ORDER_QTY except that item/locations that are on clearance are not included.' 
;

COMMENT ON COLUMN ALC_SUBCLASS_ON_ORDER_EOD.NO_CLR_NOT_ALL_ORD_ON_ORD_QTY IS 'The on order value for orders in rms that have their include_on_order_ind set to Y.  This is the same as NOT_ALL_ORDERS_ON_ORDER_QTY except that item/locations that are on clearance are not included.' 
;



PROMPT CREATING TABLE 'ALC_SYNC_DETAIL_TEMP';
CREATE TABLE ALC_SYNC_DETAIL_TEMP 
    ( 
     ALC_SYNC_PROCESS_ID NUMBER (15) , 
     RMS_ALLOC_NO        NUMBER (15) , 
     ALC_ALLOC_ID        NUMBER (15) , 
     TO_LOC              NUMBER (10) , 
     TO_LOC_TYPE         VARCHAR2 (1) , 
     QTY_TRANSFERRED     NUMBER (12,4) , 
     QTY_ALLOCATED       NUMBER (12,4) , 
     QTY_PRESCALED       NUMBER (12,4) , 
     NON_SCALE_IND       VARCHAR2 (1) , 
     IN_STORE_DATE       DATE , 
     RUSH_FLAG           VARCHAR2 (1) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_SYNC_DETAIL_TEMP IS 'This table is used by Allocation during the process of creating and maintaining RMS allocations.'
;


COMMENT ON COLUMN ALC_SYNC_DETAIL_TEMP.ALC_SYNC_PROCESS_ID IS 'Unique for each call to CREATE_ALLOC.' 
;

COMMENT ON COLUMN ALC_SYNC_DETAIL_TEMP.RMS_ALLOC_NO IS 'The RMS allocation number.' 
;

COMMENT ON COLUMN ALC_SYNC_DETAIL_TEMP.ALC_ALLOC_ID IS 'The ALC allocation number and the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_SYNC_DETAIL_TEMP.TO_LOC IS 'The destination location of the allocation.' 
;

COMMENT ON COLUMN ALC_SYNC_DETAIL_TEMP.TO_LOC_TYPE IS 'The destination location type of the allocation.' 
;

COMMENT ON COLUMN ALC_SYNC_DETAIL_TEMP.QTY_TRANSFERRED IS 'The transfer quantity of this allocation to this location.' 
;

COMMENT ON COLUMN ALC_SYNC_DETAIL_TEMP.QTY_ALLOCATED IS 'The allocation quantity of this allocation to this location.' 
;

COMMENT ON COLUMN ALC_SYNC_DETAIL_TEMP.QTY_PRESCALED IS 'The prescaled quantity of this allocation to this location.' 
;

COMMENT ON COLUMN ALC_SYNC_DETAIL_TEMP.NON_SCALE_IND IS 'The scaling indicator for this location.' 
;

COMMENT ON COLUMN ALC_SYNC_DETAIL_TEMP.IN_STORE_DATE IS 'The in store date for this location.' 
;

COMMENT ON COLUMN ALC_SYNC_DETAIL_TEMP.RUSH_FLAG IS 'The rush flag for this location.' 
;



PROMPT CREATING TABLE 'ALC_SYNC_HEADER_TEMP';
CREATE TABLE ALC_SYNC_HEADER_TEMP 
    ( 
     ALC_SYNC_PROCESS_ID NUMBER (15) , 
     RMS_ALLOC_NO        NUMBER (15) , 
     ALC_ALLOC_ID        NUMBER (15) , 
     ORDER_NO            NUMBER (12) , 
     WH                  NUMBER (10) , 
     ITEM                VARCHAR2 (25) , 
     STATUS              VARCHAR2 (1) , 
     ALLOC_DESC          VARCHAR2 (300) , 
     PO_TYPE             VARCHAR2 (4) , 
     ALLOC_METHOD        VARCHAR2 (1) , 
     RELEASE_DATE        DATE , 
     ORDER_TYPE          VARCHAR2 (9) , 
     CONTEXT_TYPE        VARCHAR2 (6) , 
     CONTEXT_VALUE       VARCHAR2 (25) , 
     COMMENT_DESC        VARCHAR2 (2000) , 
     DOC                 VARCHAR2 (30) , 
     DOC_TYPE            VARCHAR2 (5) , 
     ORIGIN_IND          VARCHAR2 (6) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_SYNC_HEADER_TEMP IS 'This table is used by Allocation during the process of creating and maintaining RMS allocations.'
;


COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.ALC_SYNC_PROCESS_ID IS 'Unique for each call to CREATE_ALLOC.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.RMS_ALLOC_NO IS 'The RMS allocation number.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.ALC_ALLOC_ID IS 'The ALC allocation number AND  the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.ORDER_NO IS 'This column contains the order number to which the allocation applies.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.WH IS 'The source wh for the allocation.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.ITEM IS 'The item being allocated.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.STATUS IS 'The status of the allocation.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.ALLOC_DESC IS 'The allocation description.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.PO_TYPE IS 'This column contains the value associated with the PO_TYPE for the order.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.ALLOC_METHOD IS 'This column contains the preferred allocation method which is used to distribute goods when the stock received at a warehouse cannot immediately fill all requested allocations to stores. Valid values are: A - Allocation quantity based, P - Prorate method, C - Custom' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.RELEASE_DATE IS 'This column contains the date on which the allocation should be released from the warehouse for delivery to the store locations.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.ORDER_TYPE IS 'This column identifies the type of the order. Allocations created against Purchase Orders will be marked as PREDIST order types. Allocations created against Warehouse stock will be populated with the DEFAULT_ORDER_TYPE from the SYSTEM_OPTIONS table which can be AUTOMATIC, MANUAL or WAVE.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.CONTEXT_TYPE IS 'This column contains the functional area code to which the transfer relates to, for example, Promotions. Valid values are: PROM - Promotion' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.CONTEXT_VALUE IS 'This column contains the value relating to the context type, for example, Promotion Number.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.COMMENT_DESC IS 'This column contains additional information concerning the allocation.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.DOC IS 'This column contains the ASN or BOL number for an ASN or BOL sourced allocation. This will be populated for the product source of the tier one allocation.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.DOC_TYPE IS 'This column contains the type of allocation product source.' 
;

COMMENT ON COLUMN ALC_SYNC_HEADER_TEMP.ORIGIN_IND IS 'The source of the allocation.' 
;



PROMPT CREATING TABLE 'ALC_SYSTEM_OPTIONS';
CREATE TABLE ALC_SYSTEM_OPTIONS 
    ( 
     SYSTEM_OPTION_ID              NUMBER (15)  NOT NULL , 
     INST_CALC_INPUT_LOG           VARCHAR2 (1)  NOT NULL , 
     INST_SCHEMA_OWNER             VARCHAR2 (50) , 
     TP_CALC_Q_POLL_INTVAL         NUMBER (4)  NOT NULL , 
     TP_ITEM_LOC_WARN              VARCHAR2 (1)  NOT NULL , 
     TP_END_OF_WEEK_DAY            VARCHAR2 (9)  NOT NULL , 
     TP_BULK_WH                    NUMBER (10) , 
     TP_AUTO_UPD_GRPS              VARCHAR2 (1)  NOT NULL , 
     TP_ALL_ORDERS                 VARCHAR2 (1)  NOT NULL , 
     TP_FREIGHT_COST_ENBL          VARCHAR2 (1)  NOT NULL , 
     TP_FUT_RETAIL_ENBL            VARCHAR2 (1)  NOT NULL , 
     TP_PROMOS_ENBL                VARCHAR2 (1)  NOT NULL , 
     TP_DISTRIBUTION_LEVEL         VARCHAR2 (1)  NOT NULL , 
     TP_WHTIF_IT_SRC_TIER_LVL      VARCHAR2 (1)  NOT NULL , 
     TP_INC_MID_TIER_SOH           VARCHAR2 (1)  NOT NULL , 
     TP_MLD_TIER_DELETION_ENBL     VARCHAR2 (1)  NOT NULL , 
     TP_FLEX_IMPORT_WH_PATHS       VARCHAR2 (1)  NOT NULL , 
     TP_WHTIF_IMPORT_WH_DEF        NUMBER (10) , 
     TP_INC_LEADTIME_NEED          VARCHAR2 (1)  NOT NULL , 
     TP_LOC_EXCP_RSN_PRD_SRCD      VARCHAR2 (20) , 
     TP_LOC_EXCP_RSN_WHAT_IF       VARCHAR2 (20) , 
     TP_IMPORT_WH                  VARCHAR2 (4000) , 
     TP_WHTIF_SUM_DEF_ACTION       VARCHAR2 (1)  NOT NULL , 
     TP_FUT_AVAIL_WHTIF_ALC_ENB    VARCHAR2 (1)  NOT NULL , 
     TP_SIZEPROF_VALIDN_ENBL       VARCHAR2 (1)  NOT NULL , 
     TP_SIZEPROF_VALID_LEVELS      VARCHAR2 (40) , 
     TP_SHIP_SCHD_PATH_LEVELS      VARCHAR2 (10) , 
     TP_SISTER_STORE_NULL          VARCHAR2 (1)  NOT NULL , 
     TP_LOC_LIST_THRESHOLD         NUMBER (3) , 
     TP_BATCH_PROVIDER_URL         VARCHAR2 (2083) , 
     TP_UNLOCK_MINS                NUMBER (3)  NOT NULL , 
     TP_ITEM_SEARCH_MAX            NUMBER (4) DEFAULT 300  NOT NULL , 
     TP_ALLOC_RETENTION_DAYS       NUMBER (4) , 
     TP_WORKSHEET_RETENTION_DAYS   NUMBER (4) , 
     FP_DAYS_BEFORE_RLS_DATE       NUMBER (2)  NOT NULL , 
     FP_PLAN_SENSITIVITY           NUMBER (2,1)  NOT NULL , 
     FP_SECONDARY                  VARCHAR2 (1)  NOT NULL , 
     FP_ENFORCE_MLE                VARCHAR2 (1)  NOT NULL , 
     FP_BREAK_PACK_ENFORCED        VARCHAR2 (1)  NOT NULL , 
     FP_PRESENTATION_MIN_DEF       VARCHAR2 (1)  NOT NULL , 
     FP_STORE_CALC_MULT            VARCHAR2 (2)  NOT NULL , 
     FP_ITEM_SOURCE_DEF            VARCHAR2 (1)  NOT NULL , 
     FP_DESC_LENGTH                NUMBER (2) , 
     FP_RULE_VISIBILITY            VARCHAR2 (1)  NOT NULL , 
     FP_PURGE_NO_DAYS              NUMBER (4)  NOT NULL , 
     FP_PACK_THRESHOLD             NUMBER (2)  NOT NULL , 
     FP_COPY_QL_GROUP              VARCHAR2 (1)  NOT NULL , 
     FP_ALT_HIER_ITEM_MAX          NUMBER (10) DEFAULT 300  NOT NULL , 
     CREATED_BY                    VARCHAR2 (64)  NOT NULL , 
     CREATION_DATE                 TIMESTAMP  NOT NULL , 
     LAST_UPDATED_BY               VARCHAR2 (64)  NOT NULL , 
     LAST_UPDATE_DATE              TIMESTAMP  NOT NULL , 
     LAST_UPDATE_LOGIN             VARCHAR2 (64) , 
     OBJECT_VERSION_NUMBER         NUMBER (9)  NOT NULL , 
     TP_NOTIFICATION_POLL_INTERVAL NUMBER (9) , 
     TP_NOTIFICATION_POLL_TIMEOUT  NUMBER (9) , 
     INST_CALC_INPUT_LOG_DIRECTORY VARCHAR2 (2000) , 
     FP_DEFAULT_RELEASE_DATE       VARCHAR2 (1) DEFAULT 'Y'  NOT NULL , 
     FP_DEFAULT_AUTO_QTY_LIMITS    VARCHAR2 (1) DEFAULT 'N'  NOT NULL , 
     FP_SCHED_RLS_DATE_CONFIG      NUMBER (3) DEFAULT 0  NOT NULL ,
	 FP_PACK_RANGING			   VARCHAR2 (1) DEFAULT 'P' NOT NULL ,	
	 TP_OBIEE_INTEGRATION_ENABLED  VARCHAR2 (1) DEFAULT 'N' NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_ID 
    CHECK ( SYSTEM_OPTION_ID IN ('1')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_CALC_INPUTLOG 
    CHECK ( INST_CALC_INPUT_LOG IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_CALC_Q_POLL_INTVL 
    CHECK ( TP_CALC_Q_POLL_INTVAL >0 AND TP_CALC_Q_POLL_INTVAL <10000) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_ITEM_LOCWARN 
    CHECK ( TP_ITEM_LOC_WARN IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_END_OF_WEEK 
    CHECK (TP_END_OF_WEEK_DAY IN ('1','2','3','4','5','6','7')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_AUTO_UPDATE_GRPS 
    CHECK ( TP_AUTO_UPD_GRPS IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_REIGHT_COST_ENBL 
    CHECK ( TP_FREIGHT_COST_ENBL IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_FUT_RETAIL_ENBL 
    CHECK ( TP_FUT_RETAIL_ENBL IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_PRMOTIONS_ENB 
    CHECK ( TP_PROMOS_ENBL IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_DISTRIBUTION_LEVEL 
    CHECK ( TP_DISTRIBUTION_LEVEL IN ('0', '1', '2')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_INC_MID_TIER_SOH 
    CHECK ( TP_INC_MID_TIER_SOH IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SOPS_MLD_TIER_DEL_ENBL 
    CHECK ( TP_MLD_TIER_DELETION_ENBL IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SOPS_FLEXIMPORT_WH_PATHS 
    CHECK ( TP_FLEX_IMPORT_WH_PATHS IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_INC_LEADTIME_NEED 
    CHECK ( TP_INC_LEADTIME_NEED IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_WHTIF_SUM_DEF_ACTN 
    CHECK ( TP_WHTIF_SUM_DEF_ACTION IN ('1', '2')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SOPS_FUT_WHTIF_ALC_ENB 
    CHECK ( TP_FUT_AVAIL_WHTIF_ALC_ENB IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_SIZPROF_VALID_ENB 
    CHECK ( TP_SIZEPROF_VALIDN_ENBL IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_SISTER_STORE_NULL 
    CHECK ( TP_SISTER_STORE_NULL IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SOP_LOCLIST_THRESHOLD 
    CHECK ( TP_LOC_LIST_THRESHOLD > 0 AND TP_LOC_LIST_THRESHOLD < 1000) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SOP_UNLOCK_MINS 
    CHECK ( TP_UNLOCK_MINS > 0 AND TP_UNLOCK_MINS < 1000) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SOP_DAYS_BEFORE_RLS_DATE 
    CHECK ( FP_DAYS_BEFORE_RLS_DATE >0 AND FP_DAYS_BEFORE_RLS_DATE <100) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_PLAN_SENSITIVITY 
    CHECK ( FP_PLAN_SENSITIVITY >= 0 AND FP_PLAN_SENSITIVITY <= 1) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_SECONDARY 
    CHECK ( FP_SECONDARY IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SOPS_ENFORCE_MLE 
    CHECK ( FP_ENFORCE_MLE IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_BREAK_PACK_ENF 
    CHECK ( FP_BREAK_PACK_ENFORCED IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_PRESTN_MIN_DEFAULT 
    CHECK ( FP_PRESENTATION_MIN_DEF IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_STORE_CALC_MULT 
    CHECK ( FP_STORE_CALC_MULT IN ('EA', 'IN', 'CA', 'PA')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_ITEM_SRC_DEF 
    CHECK ( FP_ITEM_SOURCE_DEF = 'A' OR FP_ITEM_SOURCE_DEF = 'B' OR FP_ITEM_SOURCE_DEF = 'P' OR FP_ITEM_SOURCE_DEF = 'T' OR FP_ITEM_SOURCE_DEF = 'W' OR FP_ITEM_SOURCE_DEF = 'S') 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_DESCRIPTION_LENGTH 
    CHECK ( FP_DESC_LENGTH > 0 AND FP_DESC_LENGTH < 100) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SYSOPS_RULE_VISIBILITY 
    CHECK ( FP_RULE_VISIBILITY IN ('1', '2')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SOP_PURGE_NO_DAYS 
    CHECK ( FP_PURGE_NO_DAYS > 0 AND FP_PURGE_NO_DAYS < 10000) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SOP_COPY_QL_GROUP 
    CHECK ( FP_COPY_QL_GROUP IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_TP_NOTIFICATION_POLL_INTVL 
    CHECK ( TP_NOTIFICATION_POLL_INTERVAL>=30000) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_TP_NOTIFICATION_POLL_TMOUT 
    CHECK ( TP_NOTIFICATION_POLL_TIMEOUT>=600000) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SOP_DEFAULT_RELEASE_DATE 
    CHECK (FP_DEFAULT_RELEASE_DATE IN ('Y','N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT CHK_SOP_SCHED_RLS_DATE_CONFIG 
    CHECK ( FP_SCHED_RLS_DATE_CONFIG >= 0 AND FP_SCHED_RLS_DATE_CONFIG  < 1000) 
;


COMMENT ON TABLE ALC_SYSTEM_OPTIONS IS 'This tables holds information about all Allocation System Options.'
;


COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.SYSTEM_OPTION_ID IS 'This column is the identifier for ALC_SYSTEM_OPTIONS table.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.INST_CALC_INPUT_LOG IS 'This column specifies calculation input mode on/off switch for writing .dat files' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.INST_SCHEMA_OWNER IS 'This column holds Schema Owner information.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_CALC_Q_POLL_INTVAL IS 'This column holds information about calculation queue polling interval in milliseconds.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_ITEM_LOC_WARN IS 'This column indicates whether a warning message needs to be displayed to the user in case of selection of an invalid item location combination.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_END_OF_WEEK_DAY IS 'This column indicates the day to be treated as the end of the week.  All rule data must be set up with an end of week date that corresponds to the end of week day setting (i.e. the EOW_DATE on the rules tables should be a date that matches the correct day of the week).' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_BULK_WH IS 'This column indicates the Non- finisher virtual bulk warehouse id for PO creation for What If allocations.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_AUTO_UPD_GRPS IS 'This column indicates whether the location groups need to get updated for worksheet allocations.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_ALL_ORDERS IS 'This column indicates whether the On Order quantities against open purchase orders need to be considered while calculating item stock on hand.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_FREIGHT_COST_ENBL IS 'This column indicates whether the Freight cost checkbox is to be displayed in the application UI  and freight calculations need to be performed.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_FUT_RETAIL_ENBL IS 'This column indicates if the user will be allowed to view the future unit retail for items present in an allocation.    This setting is in place to support future enhancements, and is not used by the application in this release.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_PROMOS_ENBL IS 'This column indicates whether or not the system should allow the user to link promotions with an allocation during the creation process.  This setting is in place to support future enhancements, and is not used by the application in this release.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_DISTRIBUTION_LEVEL IS 'This column hold information related to Multi Level Distribution Settings.  This setting is in place to support future MLD extenstions, and is not currently used by the application.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_WHTIF_IT_SRC_TIER_LVL IS 'This column holds indicator D for Department or C for Class or S for Subclass or Ifor Item.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_INC_MID_TIER_SOH IS 'This column indicates whether the lead time need is to be considered during quantity calculations while allocating.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_MLD_TIER_DELETION_ENBL IS 'This column identifier allows the user to delete individual distribution tiers in an MLD environment.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_FLEX_IMPORT_WH_PATHS IS 'This column indicates whether flexible Import Warehouse paths will be allowed.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_WHTIF_IMPORT_WH_DEF IS 'This column indicates the default warehouse for import based purchase orders from What If allocations.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_INC_LEADTIME_NEED IS 'This column indicates whether lead time need to be considered during quantity calculations while allocating.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_LOC_EXCP_RSN_PRD_SRCD IS 'This column indicates the statuses of the item location relationships to be excluded from product sourced allocations.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_LOC_EXCP_RSN_WHAT_IF IS 'This column indicates the statuses of the item location relationships to be exclude from product sourced in what-if allocations.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_IMPORT_WH IS 'This column indicates the set of warehouses to be used for import based purchase orders.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_WHTIF_SUM_DEF_ACTION IS 'This column Indicates the What If Summary Default Action.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_FUT_AVAIL_WHTIF_ALC_ENB IS 'This column indicates whether or not to consider Future Available inventory for What If Allocations.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_SIZEPROF_VALIDN_ENBL IS 'This column indicates if the size profile validation should be done when the user hits the Calculate button.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_SIZEPROF_VALID_LEVELS IS 'This column indicates the levels at which the validation should be done.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_SHIP_SCHD_PATH_LEVELS IS 'This column indicates the levels which would be queried in case the user has selected the distribution level 2 which is linked with the table ALC_SHIPPING_SCHEDULE.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_SISTER_STORE_NULL IS 'This column indicates whether the need of a like store can be used during allocation calculation.  If a customer is not using like store functionality, it is recommended to set this value to N, to bypass this logic during calculation for performance reasons.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_LOC_LIST_THRESHOLD IS 'This column identifies location IN list threshold value.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_BATCH_PROVIDER_URL IS 'This column specifies batch provider url info. This must be set correctly for calculations to work with the JMS queue. After changing this setting, you need to restart the application server.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_UNLOCK_MINS IS 'This column indicates the locking time out in minutes.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_ITEM_SEARCH_MAX IS 'This limits the number of rows that will be returned by the item search.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_ALLOC_RETENTION_DAYS IS 'Number of days to keep allocations not linked to RMS allocations in the system after they are no longer being modified' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_WORKSHEET_RETENTION_DAYS IS 'Number of days to keep worksheets not linked to allocations in the system' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_DAYS_BEFORE_RLS_DATE IS 'This column identifies number of days before the release date. i.e. Ship date = Release Date - Days before release date.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY IS 'This column indicates the plan sensitivity value to be used while using the Plan Reproject policy.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_SECONDARY IS 'This column holds secondary indicator flag Y or N.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_ENFORCE_MLE IS 'This column indicates if user can cross legal entities.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_BREAK_PACK_ENFORCED IS 'This column indicates whether the break pack functionality is enabled or not.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_PRESENTATION_MIN_DEF IS 'This column indicates if presentation minimums are to be initially defaulted into the quantity limits UI.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_STORE_CALC_MULT IS 'This column indicates default Store Calculation Multiple.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_ITEM_SOURCE_DEF IS 'This column indicates the Item Source that will be checked by default when entering the Item Search page.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_DESC_LENGTH IS 'This column indicates the maximum length to be used for display of Item descriptions in the UI.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_RULE_VISIBILITY IS 'This column indicates the rule type for which the need value would be displayed in the Allocation Maintenance UI.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_PURGE_NO_DAYS IS 'This column specifies the number of purge days.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_PACK_THRESHOLD IS 'This column indicates the Pack Variance Acceptance Threshold value.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_COPY_QL_GROUP IS 'This column indicates whether values entered against Group in QL will be copied to Store  or spread to store in the group.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_ALT_HIER_ITEM_MAX IS 'Maximum number of items per alternate hierarchy selection' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.CREATED_BY IS 'The user who created the record.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.CREATION_DATE IS 'The date the record was created' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.LAST_UPDATED_BY IS 'The user who last updated the record.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.LAST_UPDATE_DATE IS 'The date the record was last updated.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.LAST_UPDATE_LOGIN IS 'Indicates the session login associated to the user who last updated the row.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.OBJECT_VERSION_NUMBER IS 'This column indicates Object version number.' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_NOTIFICATION_POLL_INTERVAL IS 'This column specifies notification polling interval  in millisec' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_NOTIFICATION_POLL_TIMEOUT IS 'This column specifies notification polling timeout value in millisec' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.INST_CALC_INPUT_LOG_DIRECTORY IS 'This column indicates the directory to hold calculation dat files' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_DEFAULT_RELEASE_DATE IS 'Value for enabling or disabling setting the default release date on a newly created allocation, valid values are ''Y'' and ''N''' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_DEFAULT_AUTO_QTY_LIMITS IS 'Value for enabling or disabling setting the auto quantity limits on an allocation by default, valid values are ''Y'' and ''N''' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_SCHED_RLS_DATE_CONFIG IS 'Number of days beyond the release date of a schedule allocation' 
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.FP_PACK_RANGING is 'This column specifies the Pack Ranging level for a store location. Valid values are ''C'' component level and ''P'' pack level. The Default Value is ''P''. '
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS.TP_OBIEE_INTEGRATION_ENABLED is 'Indicate whether OBIEE is Enabled or not'
;
PROMPT CREATING INDEX 'PK_ALC_SYSTEM_OPTIONS';
CREATE UNIQUE INDEX PK_ALC_SYSTEM_OPTIONS ON ALC_SYSTEM_OPTIONS 
    ( 
     SYSTEM_OPTION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;

PROMPT CREATING PRIMARY KEY ON 'ALC_SYSTEM_OPTIONS';
ALTER TABLE ALC_SYSTEM_OPTIONS 
    ADD CONSTRAINT PK_ALC_SYSTEM_OPTIONS PRIMARY KEY ( SYSTEM_OPTION_ID ) 
    USING INDEX PK_ALC_SYSTEM_OPTIONS ;




PROMPT CREATING TABLE 'ALC_TASK';
CREATE TABLE ALC_TASK 
    ( 
     TASK_ID          NUMBER (18)  NOT NULL , 
     ALLOC_ID         NUMBER (15)  NOT NULL , 
     PREV_STATE       NUMBER (2) , 
     CURRENT_STATE    NUMBER (2) , 
     FINAL_STATE      NUMBER (2) , 
     CREATED_BY       VARCHAR2 (64)  NOT NULL , 
     CREATE_DATE      DATE  NOT NULL , 
     LAST_UPDATED_BY  VARCHAR2 (64) , 
     LAST_UPDATE_DATE DATE 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_TASK IS 'This table is used to hold task id and state of the allocation for use during asynchronous processing.'
;


COMMENT ON COLUMN ALC_TASK.TASK_ID IS 'Task Id which is the unique Id' 
;

COMMENT ON COLUMN ALC_TASK.ALLOC_ID IS 'Holds the allocation Id' 
;

COMMENT ON COLUMN ALC_TASK.PREV_STATE IS 'Holds the prev state of the allocation' 
;

COMMENT ON COLUMN ALC_TASK.CURRENT_STATE IS 'Holds the current state of the allocation' 
;

COMMENT ON COLUMN ALC_TASK.FINAL_STATE IS 'Holds the final state of the allocation' 
;

COMMENT ON COLUMN ALC_TASK.CREATED_BY IS 'Created by the user' 
;

COMMENT ON COLUMN ALC_TASK.CREATE_DATE IS 'Created date' 
;

COMMENT ON COLUMN ALC_TASK.LAST_UPDATED_BY IS 'Last updated user' 
;

COMMENT ON COLUMN ALC_TASK.LAST_UPDATE_DATE IS 'Last updated timestamp' 
;
PROMPT CREATING INDEX 'PK_ALC_TASK';
CREATE UNIQUE INDEX PK_ALC_TASK ON ALC_TASK 
    ( 
     TASK_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_TASK_I1';
CREATE INDEX ALC_TASK_I1 ON ALC_TASK 
    ( 
     ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_TASK';
ALTER TABLE ALC_TASK 
    ADD CONSTRAINT PK_ALC_TASK PRIMARY KEY ( TASK_ID ) 
    USING INDEX PK_ALC_TASK ;




PROMPT CREATING TABLE 'ALC_TEMPLATE';
CREATE TABLE ALC_TEMPLATE 
    ( 
     TEMPLATE_ID              NUMBER (15)  NOT NULL , 
     TEMPLATE_NAME            VARCHAR2 (300)  NOT NULL , 
     TEMPLATE_TYPE            VARCHAR2 (1)  NOT NULL , 
     USER_TYPE                VARCHAR2 (1)  NOT NULL , 
     CREATED_DATE             DATE  NOT NULL , 
     CREATED_BY_USER_ID       NUMBER (15)  NOT NULL , 
     LAST_APPLIED_DATE        DATE , 
     ENFORCE_WH_STORE_REL_IND VARCHAR2 (1)  NOT NULL , 
     NEVER_UPDATE_GROUP_IND   VARCHAR2 (1) DEFAULT 'N'  NOT NULL , 
     CREATED_BY               VARCHAR2 (64) , 
     CREATION_DATE            TIMESTAMP , 
     LAST_UPDATED_DATE        TIMESTAMP , 
     LAST_UPDATED_BY          VARCHAR2 (64) , 
     OBJECT_VERSION_NUMBER    NUMBER (9) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_TEMPLATE';
ALTER TABLE ALC_TEMPLATE 
    ADD CONSTRAINT CHK_ALC_TEMPLATE_NV_UPD_GRP_I 
    CHECK ( NEVER_UPDATE_GROUP_IND IN ('N', 'Y')) 
;


COMMENT ON TABLE ALC_TEMPLATE IS 'History Column : Creation date'
;


COMMENT ON COLUMN ALC_TEMPLATE.TEMPLATE_ID IS 'This column contains a unique template identifier. this value is derived from the sequence alc_template_seq.' 
;

COMMENT ON COLUMN ALC_TEMPLATE.TEMPLATE_NAME IS 'This column contains the rule template name.' 
;

COMMENT ON COLUMN ALC_TEMPLATE.TEMPLATE_TYPE IS 'This column contains the type of template this is. valid values are: Location = l or Rule = r.' 
;

COMMENT ON COLUMN ALC_TEMPLATE.USER_TYPE IS 'This column contains the user type of template.' 
;

COMMENT ON COLUMN ALC_TEMPLATE.CREATED_DATE IS 'This column indicates the date and time of the creation of the template.' 
;

COMMENT ON COLUMN ALC_TEMPLATE.CREATED_BY_USER_ID IS 'This column  indicates the user who created the template.' 
;

COMMENT ON COLUMN ALC_TEMPLATE.LAST_APPLIED_DATE IS 'This column indicates the date and time of  the template last applied.' 
;

COMMENT ON COLUMN ALC_TEMPLATE.ENFORCE_WH_STORE_REL_IND IS 'This column contains the enforce warehouse store relationship indicator. When this indicator is selected, stores can only be sources from a valid default warehouse, as defined in the RMS STORE table or the WH_STORE_ASSIGN table.  Valid values are:yes = y no = n. This value can only be y if the template_type = l.' 
;

COMMENT ON COLUMN ALC_TEMPLATE.NEVER_UPDATE_GROUP_IND IS 'This column contains the never update groups indicator. valid values are: yes =y no = n. This value can only be y if the template_type = l.' 
;

COMMENT ON COLUMN ALC_TEMPLATE.CREATED_BY IS 'Indicates the user who created the record' 
;

COMMENT ON COLUMN ALC_TEMPLATE.CREATION_DATE IS 'Indicates the user who last updated the record' 
;

COMMENT ON COLUMN ALC_TEMPLATE.LAST_UPDATED_DATE IS 'The timestamp of the record creation date' 
;

COMMENT ON COLUMN ALC_TEMPLATE.LAST_UPDATED_BY IS 'The timestamp of the record last updated date' 
;

COMMENT ON COLUMN ALC_TEMPLATE.OBJECT_VERSION_NUMBER IS 'This column indicates Object version number.' 
;
PROMPT CREATING INDEX 'PK_ALC_TEMPLATE';
CREATE UNIQUE INDEX PK_ALC_TEMPLATE ON ALC_TEMPLATE 
    ( 
     TEMPLATE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_TEMPLATE';
ALTER TABLE ALC_TEMPLATE 
    ADD CONSTRAINT PK_ALC_TEMPLATE PRIMARY KEY ( TEMPLATE_ID ) 
    USING INDEX PK_ALC_TEMPLATE ;




PROMPT CREATING TABLE 'ALC_WHAT_IF';
CREATE TABLE ALC_WHAT_IF 
    ( 
     ALC_WHATIF_ID     NUMBER (20)  NOT NULL , 
     ALLOC_ID          NUMBER (15)  NOT NULL , 
     WH_ID             VARCHAR2 (40) , 
     ITEM_ID           VARCHAR2 (40)  NOT NULL , 
     SUPPLIER_ID       VARCHAR2 (40) , 
     ORDER_ID          VARCHAR2 (40) , 
     ORIGIN_COUNTRY_ID VARCHAR2 (3) , 
     AGGREGATE_DIFF_ID VARCHAR2 (100) , 
     ORDER_UPDATE_IND  VARCHAR2 (1) , 
     PO_QUANTITY       NUMBER (12,4) , 
     FREEZE_IND        VARCHAR2 (1)  NOT NULL , 
     PO_MULTIPLE       VARCHAR2 (2)  NOT NULL , 
     TYPE              NUMBER (1) , 
     MLD_PO_LEVEL      VARCHAR2 (2) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_WHAT_IF';
ALTER TABLE ALC_WHAT_IF 
    ADD CONSTRAINT CHK_AWI_ORDER_UPDATE_IND 
    CHECK ( ORDER_UPDATE_IND IN ('Y', 'N')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_WHAT_IF';
ALTER TABLE ALC_WHAT_IF 
    ADD CONSTRAINT CHK_AWI_PO_MULTIPLE 
    CHECK ( PO_MULTIPLE IN ('CA', 'EA', 'IN', 'PA')) 
;


PROMPT CREATING CHECK CONSTRAINT ON 'ALC_WHAT_IF';
ALTER TABLE ALC_WHAT_IF 
    ADD CONSTRAINT CHK_AWI_TYPE 
    CHECK ( TYPE IN (1, 2, 3, 4)) 
;


COMMENT ON TABLE ALC_WHAT_IF IS 'this table contains what-if information'
;


COMMENT ON COLUMN ALC_WHAT_IF.ALC_WHATIF_ID IS 'This is the primary unique id generated from the sequence.' 
;

COMMENT ON COLUMN ALC_WHAT_IF.ALLOC_ID IS 'indicates the allocation Id  and  the foriegn key to the primary key of  ALC_ALLOC table' 
;

COMMENT ON COLUMN ALC_WHAT_IF.WH_ID IS 'virtual warehouse to be sent to rms' 
;

COMMENT ON COLUMN ALC_WHAT_IF.ITEM_ID IS 'item number to be sent to rms' 
;

COMMENT ON COLUMN ALC_WHAT_IF.SUPPLIER_ID IS 'Supplier ID used for the RMS Purchase order' 
;

COMMENT ON COLUMN ALC_WHAT_IF.ORDER_ID IS 'RMS Purchase Order created for the What If allocation number' 
;

COMMENT ON COLUMN ALC_WHAT_IF.ORIGIN_COUNTRY_ID IS 'Origin country for the supplier used in the RMS PO' 
;

COMMENT ON COLUMN ALC_WHAT_IF.AGGREGATE_DIFF_ID IS 'Concatenated diff id fields used in Allocation for this item' 
;

COMMENT ON COLUMN ALC_WHAT_IF.ORDER_UPDATE_IND IS 'Purchase order create/update indicator' 
;

COMMENT ON COLUMN ALC_WHAT_IF.PO_QUANTITY IS 'PO amount based upon the user selected action type and to location' 
;

COMMENT ON COLUMN ALC_WHAT_IF.FREEZE_IND IS 'Freeze indicator value depending on the update of po_quantity column' 
;

COMMENT ON COLUMN ALC_WHAT_IF.PO_MULTIPLE IS 'PO multiple selected by the user' 
;

COMMENT ON COLUMN ALC_WHAT_IF.TYPE IS 'Ttype of action that occurred during po creation or update' 
;

COMMENT ON COLUMN ALC_WHAT_IF.MLD_PO_LEVEL IS 'this holds the po location level for mld whatif allocation, for non-mld' 
;
PROMPT CREATING INDEX 'PK_ALC_WHAT_IF';
CREATE UNIQUE INDEX PK_ALC_WHAT_IF ON ALC_WHAT_IF 
    ( 
     ALC_WHATIF_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_WHAT_IF';
ALTER TABLE ALC_WHAT_IF 
    ADD CONSTRAINT PK_ALC_WHAT_IF PRIMARY KEY ( ALC_WHATIF_ID ) 
    USING INDEX PK_ALC_WHAT_IF ;




PROMPT CREATING TABLE 'ALC_WH_SUPPLY_PATH';
CREATE TABLE ALC_WH_SUPPLY_PATH 
    ( 
     SOURCE_WH      NUMBER (10)  NOT NULL , 
     DESTINATION_WH NUMBER (10)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_WH_SUPPLY_PATH IS 'This column holds the supply chain relationships between warehouses.  It is used during location exclusion logic when the Enforce Supply Chain check box is checked.  It applies when the ITEM_LOC.SOURCE_METHOD / ITEM_LOC.SOURCE_WH is not populated.'
;


COMMENT ON COLUMN ALC_WH_SUPPLY_PATH.SOURCE_WH IS 'The source warehouse of the allocation.' 
;

COMMENT ON COLUMN ALC_WH_SUPPLY_PATH.DESTINATION_WH IS 'The valid destination warehouses for an allocation where the source is the SOURCE_WH.' 
;
PROMPT CREATING INDEX 'PK_ALC_WH_SUPPLY_PATH';
CREATE UNIQUE INDEX PK_ALC_WH_SUPPLY_PATH ON ALC_WH_SUPPLY_PATH 
    ( 
     DESTINATION_WH ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_WH_SUPPLY_PATH';
ALTER TABLE ALC_WH_SUPPLY_PATH 
    ADD CONSTRAINT PK_ALC_WH_SUPPLY_PATH PRIMARY KEY ( DESTINATION_WH ) 
    USING INDEX PK_ALC_WH_SUPPLY_PATH ;




PROMPT CREATING TABLE 'ALC_WORKSHEET_PURGE_HELPER';
CREATE TABLE ALC_WORKSHEET_PURGE_HELPER 
    ( 
     WORK_HEADER_ID NUMBER (15)  NOT NULL , 
     THREAD_ID      NUMBER (10)  NOT NULL 
    ) 
        INITRANS 12 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_WORKSHEET_PURGE_HELPER IS 'Helper table used during the worksheet purge process which deletes ''WK'' type worksheets.'
;


COMMENT ON COLUMN ALC_WORKSHEET_PURGE_HELPER.WORK_HEADER_ID IS 'Unique ID of the worksheet that will be purged.' 
;

COMMENT ON COLUMN ALC_WORKSHEET_PURGE_HELPER.THREAD_ID IS 'Thread number of the thread responsible for deleting the worksheet.' 
;



PROMPT CREATING TABLE 'ALC_WORK_ALLOC';
CREATE TABLE ALC_WORK_ALLOC 
    ( 
     WORK_ALLOC_ID  NUMBER (15)  NOT NULL , 
     WORK_HEADER_ID NUMBER (15)  NOT NULL , 
     ALLOC_ID       NUMBER (15)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_WORK_ALLOC IS 'This holds worksheet allocation data.'
;


COMMENT ON COLUMN ALC_WORK_ALLOC.WORK_ALLOC_ID IS 'This is the primary unique id generated from the sequence.' 
;

COMMENT ON COLUMN ALC_WORK_ALLOC.WORK_HEADER_ID IS 'This is the foriegn key to the primary key of  ALC_WORK_HEADER table' 
;

COMMENT ON COLUMN ALC_WORK_ALLOC.ALLOC_ID IS 'This is the foriegn key to the primary key of  ALC_ALLOC table' 
;
PROMPT CREATING INDEX 'PK_ALC_WORK_ALLOC';
CREATE UNIQUE INDEX PK_ALC_WORK_ALLOC ON ALC_WORK_ALLOC 
    ( 
     WORK_ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;
	
PROMPT CREATING INDEX 'ALC_WORK_ALLOC_I2';
CREATE INDEX ALC_WORK_ALLOC_I2 ON ALC_WORK_ALLOC 
    ( 
     WORK_HEADER_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_WORK_ALLOC_I1';
CREATE INDEX ALC_WORK_ALLOC_I1 ON ALC_WORK_ALLOC 
    ( 
     ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_WORK_ALLOC';
ALTER TABLE ALC_WORK_ALLOC 
    ADD CONSTRAINT PK_ALC_WORK_ALLOC PRIMARY KEY ( WORK_ALLOC_ID ) 
    USING INDEX PK_ALC_WORK_ALLOC ;




PROMPT CREATING TABLE 'ALC_WORK_HEADER';
CREATE TABLE ALC_WORK_HEADER 
    ( 
     WORK_HEADER_ID NUMBER (15)  NOT NULL , 
     WORK_TYPE      VARCHAR2 (2)  NOT NULL , 
     WORK_DESC      VARCHAR2 (20) , 
     USER_NAME      NUMBER (10)  NOT NULL , 
     CREATED_BY     VARCHAR2 (20)  NOT NULL , 
     UPDATED_BY     VARCHAR2 (20)  NOT NULL , 
     CREATED_DATE   DATE  NOT NULL , 
     UPDATED_DATE   DATE  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_WORK_HEADER';
ALTER TABLE ALC_WORK_HEADER 
    ADD CONSTRAINT CHK_ALC_WH_WORK_TYPE 
    CHECK ( WORK_TYPE IN ('WK','WG','WD')) 
;


COMMENT ON TABLE ALC_WORK_HEADER IS 'This is the table which holds informaiton'
;


COMMENT ON COLUMN ALC_WORK_HEADER.WORK_HEADER_ID IS 'This is the primary unique id generated from the sequence.' 
;

COMMENT ON COLUMN ALC_WORK_HEADER.WORK_TYPE IS 'This column indicates the work type. Valid values are :  WORKSHEET_TYPE_WORK  = WK (Standard Worksheet)  WORKSHEET_TYPE_ALLOC =WD (Tied to Allocation)' 
;

COMMENT ON COLUMN ALC_WORK_HEADER.WORK_DESC IS 'The user entered description for their worksheet.' 
;

COMMENT ON COLUMN ALC_WORK_HEADER.USER_NAME IS 'The user who created the worksheet' 
;

COMMENT ON COLUMN ALC_WORK_HEADER.CREATED_BY IS 'Indicates the user who created the record' 
;

COMMENT ON COLUMN ALC_WORK_HEADER.UPDATED_BY IS 'Indicates the user who last updated the record' 
;

COMMENT ON COLUMN ALC_WORK_HEADER.CREATED_DATE IS 'The timestamp of the record creation date' 
;

COMMENT ON COLUMN ALC_WORK_HEADER.UPDATED_DATE IS 'The timestamp of the record last updated date' 
;
PROMPT CREATING INDEX 'PK_ALC_WORK_HEADER';
CREATE UNIQUE INDEX PK_ALC_WORK_HEADER ON ALC_WORK_HEADER 
    ( 
     WORK_HEADER_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;

PROMPT CREATING PRIMARY KEY ON 'ALC_WORK_HEADER';
ALTER TABLE ALC_WORK_HEADER 
    ADD CONSTRAINT PK_ALC_WORK_HEADER PRIMARY KEY ( WORK_HEADER_ID ) 
    USING INDEX PK_ALC_WORK_HEADER ;




PROMPT CREATING TABLE 'ALC_WORK_ITEM_SOURCE';
CREATE TABLE ALC_WORK_ITEM_SOURCE 
    ( 
     WORK_ITEM_SOURCE_ID NUMBER (15)  NOT NULL , 
     WORK_HEADER_ID      NUMBER (15)  NOT NULL , 
     ITEM                VARCHAR2 (70)  NOT NULL , 
     ITEM_TYPE           VARCHAR2 (10)  NOT NULL , 
     ANCESTOR_ID         NUMBER (15) , 
     ITEM_SOURCE_DESC    VARCHAR2 (250)  NOT NULL , 
     AVAILABLE_QUANTITY  NUMBER (15) , 
     BACKORDER_QTY       NUMBER (12,4) , 
     WH                  NUMBER (15) , 
     DOC_NO              VARCHAR2 (30) , 
     SOURCE_TYPE         VARCHAR2 (5) , 
     REFRESH_IND         VARCHAR2 (5)  NOT NULL , 
     DELETED_IND         VARCHAR2 (1) , 
     EXPECTED_TO_DATE    DATE , 
     CREATED_BY          VARCHAR2 (20)  NOT NULL , 
     UPDATED_BY          VARCHAR2 (20)  NOT NULL , 
     CREATED_DATE        DATE  NOT NULL , 
     UPDATED_DATE        DATE  NOT NULL , 
     ITEM_ID             VARCHAR2 (25) , 
     DIFF_1              VARCHAR2 (10) , 
     DIFF_2              VARCHAR2 (10) , 
     DIFF_3              VARCHAR2 (10) , 
     DISABLE_IND         VARCHAR2 (1) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_WORK_ITEM_SOURCE';
ALTER TABLE ALC_WORK_ITEM_SOURCE 
    ADD CONSTRAINT CHK_ALC_WORK_ITEM_SOURCE 
    CHECK (DISABLE_IND IN ('Y','N')) 
;


COMMENT ON TABLE ALC_WORK_ITEM_SOURCE IS 'This tables holds item source details for a worksheet'
;


COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.WORK_ITEM_SOURCE_ID IS 'This  id is generated from sequence' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.WORK_HEADER_ID IS 'This is a foreign key of ALC_WORK_HEADER primary key' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.ITEM IS 'This column  indicates the Id of the Item. Populated from item master.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.ITEM_TYPE IS 'Item types - FashionSKU, ST,FA,SELLPACK,PACKCOMP,STYLE,NSFSP,, NSFMCP, NSFSCP,NSSSP, NSSCP.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.ANCESTOR_ID IS 'This column stores the ancestor id of the' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.ITEM_SOURCE_DESC IS 'This column  indicates the description of the Item. Populated from item master.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.AVAILABLE_QUANTITY IS 'The available qty for the item/wh/source_type/doc_no combination.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.BACKORDER_QTY IS 'Used to store backorder quantity information used by calculation process.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.WH IS 'The source wh for the worksheet.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.DOC_NO IS 'The identifier of the inventory transaction and where it is coming from.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.SOURCE_TYPE IS 'This column is used to determain the Source type of the item which could be  1- PO, 2-ASO, 3-warehouse, 4-Whatif, 5-BOL, 6-TSF.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.CREATED_BY IS 'Indicates the user who created the record' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.UPDATED_BY IS 'Indicates the user who last updated the record' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.CREATED_DATE IS 'The timestamp of the record creation date' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.UPDATED_DATE IS 'The timestamp of the record last updated date' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.ITEM_ID IS 'RMS Item Id for this Item Node' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.DIFF_1 IS 'First Diff Aggregation' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.DIFF_2 IS 'Second Diff Aggregation' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.DIFF_3 IS 'Third Diff Aggregation' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE.DISABLE_IND IS 'This indicator is used for determining if the Item can be Allocated From Worksheet' 
;
PROMPT CREATING INDEX 'PK_ALC_WORK_ITEM_SOURCE';
CREATE UNIQUE INDEX PK_ALC_WORK_ITEM_SOURCE ON ALC_WORK_ITEM_SOURCE 
    ( 
     WORK_ITEM_SOURCE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_WORK_ITEM_SOURCE_I1';
CREATE INDEX ALC_WORK_ITEM_SOURCE_I1 ON ALC_WORK_ITEM_SOURCE 
    ( 
     WORK_HEADER_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_WORK_ITEM_SOURCE';
ALTER TABLE ALC_WORK_ITEM_SOURCE 
    ADD CONSTRAINT PK_ALC_WORK_ITEM_SOURCE PRIMARY KEY ( WORK_ITEM_SOURCE_ID ) 
    USING INDEX PK_ALC_WORK_ITEM_SOURCE ;




PROMPT CREATING TABLE 'ALC_WORK_ITEM_SOURCE_ALLOC';
CREATE TABLE ALC_WORK_ITEM_SOURCE_ALLOC 
    ( 
     WORK_ITEM_SOURCE_ALLOC_ID NUMBER (15)  NOT NULL , 
     WORK_ITEM_SOURCE_ID       NUMBER (15)  NOT NULL , 
     ALLOC_ID                  NUMBER (15)  NOT NULL 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_WORK_ITEM_SOURCE_ALLOC IS 'This holds item source and allocation data for worksheet.'
;


COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_ALLOC.WORK_ITEM_SOURCE_ALLOC_ID IS 'This is the Primary unique id for the table.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_ALLOC.WORK_ITEM_SOURCE_ID IS 'This is the foriegn key to the primary key of  ALC_WORK_ITEM_SOURCE table' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_ALLOC.ALLOC_ID IS 'This is the foriegn key to the primary key of  ALC_ALLOC table' 
;
PROMPT CREATING INDEX 'PK_ALC_WORK_ITEM_SOURCE_ALLOC';
CREATE UNIQUE INDEX PK_ALC_WORK_ITEM_SOURCE_ALLOC ON ALC_WORK_ITEM_SOURCE_ALLOC 
    ( 
     WORK_ITEM_SOURCE_ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;
PROMPT CREATING INDEX 'ALC_WORK_ITEM_SOURCE_ALLOC_I2';
CREATE INDEX ALC_WORK_ITEM_SOURCE_ALLOC_I2 ON ALC_WORK_ITEM_SOURCE_ALLOC 
    ( 
     WORK_ITEM_SOURCE_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_WORK_ITEM_SOURCE_ALLOC_I1';
CREATE INDEX ALC_WORK_ITEM_SOURCE_ALLOC_I1 ON ALC_WORK_ITEM_SOURCE_ALLOC 
    ( 
     ALLOC_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_WORK_ITEM_SOURCE_ALLOC';
ALTER TABLE ALC_WORK_ITEM_SOURCE_ALLOC 
    ADD CONSTRAINT PK_ALC_WORK_ITEM_SOURCE_ALLOC PRIMARY KEY ( WORK_ITEM_SOURCE_ALLOC_ID ) 
    USING INDEX PK_ALC_WORK_ITEM_SOURCE_ALLOC ;




PROMPT CREATING TABLE 'ALC_WORK_ITEM_SOURCE_DIFF';
CREATE TABLE ALC_WORK_ITEM_SOURCE_DIFF 
    ( 
     WORK_HEADER_ID       NUMBER (15)  NOT NULL , 
     ITEM                 VARCHAR2 (25)  NOT NULL , 
     ITEM_TYPE            VARCHAR2 (10) , 
     ITEM_AGGREGATE_IND   VARCHAR2 (1) , 
     DIFF_1_AGGREGATE_IND VARCHAR2 (1) , 
     DIFF_2_AGGREGATE_IND VARCHAR2 (1) , 
     DIFF_3_AGGREGATE_IND VARCHAR2 (1) , 
     DIFF_4_AGGREGATE_IND VARCHAR2 (1) , 
     DIFF_1               VARCHAR2 (4000) , 
     DIFF_2               VARCHAR2 (4000) , 
     DIFF_3               VARCHAR2 (4000) , 
     DIFF_4               VARCHAR2 (4000) , 
     DIFF_1_DESC          VARCHAR2 (4000) , 
     DIFF_2_DESC          VARCHAR2 (4000) , 
     DIFF_3_DESC          VARCHAR2 (4000) , 
     DIFF_4_DESC          VARCHAR2 (4000) , 
     ITEM_PARENT_DESC     VARCHAR2 (250) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_WORK_ITEM_SOURCE_DIFF IS 'Hold differentiator information for items on the worksheet.'
;


COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.WORK_HEADER_ID IS 'The worksheets unique identifier.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.ITEM IS 'The item on the worksheet.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.ITEM_TYPE IS 'The type of the item on the worksheet.
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.ITEM_AGGREGATE_IND IS 'Indicator for the item. With the advent of Diffs in RMS 10.0, aggregating up to specific diffs such as `Style/ColorA? is achieved by adding this indicator for each diff on the table. The item aggregate indicator allows the user to specifiy if the item may aggregrate by numbers. Aggregation allow the system to support Allocations at a Parent/Diff level. The remainder of the Diffs not a part of the aggregate group represent the Curve portion of the allo' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.DIFF_1_AGGREGATE_IND IS 'Indicator for the corresponding diff. With the advent of Diffs in RMS 10.0, aggregating up to specific diffs such as `Style/Color? is achieved by adding this indicator for each diff on the table. Aggregation allow the system to support Allocations at a Parent/Diff level. The remainder of the Diffs not a part of the aggregate group represent the Curve portion of the allocation algorithm.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.DIFF_2_AGGREGATE_IND IS 'Indicator for the corresponding diff. With the advent of Diffs in RMS 10.0, aggregating up to specific diffs such as `Style/Color? is achieved by adding this indicator for each diff on the table. Aggregation allow the system to support Allocations at a Parent/Diff level. The remainder of the Diffs not a part of the aggregate group represent the Curve portion of the allocation algorithm.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.DIFF_3_AGGREGATE_IND IS 'Indicator for the corresponding diff. With the advent of Diffs in RMS 10.0, aggregating up to specific diffs such as `Style/Color? is achieved by adding this indicator for each diff on the table. Aggregation allow the system to support Allocations at a Parent/Diff level. The remainder of the Diffs not a part of the aggregate group represent the Curve portion of the allocation algorithm.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.DIFF_4_AGGREGATE_IND IS 'Indicator for the corresponding diff. With the advent of Diffs in RMS 10.0, aggregating up to specific diffs such as `Style/Color? is achieved by adding this indicator for each diff on the table. Aggregation allow the system to support Allocations at a Parent/Diff level. The remainder of the Diffs not a part of the aggregate group represent the Curve portion of the allocation algorithm.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.DIFF_1 IS 'Diff_group or diff_id that differentiates the current item from its item_parent.  For an item that is a parent, this field may be either a group (i.e. Mens pant sizes) or a value (6 oz).  For an item that is not a parent, this field may contain a value  (34X34, Red, etc.)  Valid values are found on the diff_group and diff_id tables.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.DIFF_2 IS 'Diff_group or diff_id that differentiates the current item from its item_parent.  For an item that is a parent, this field may be either a group (i.e. Mens pant sizes) or a value (6 oz).  For an item that is not a parent, this field may contain a value  (34X34, Red, etc.)  Valid values are found on the diff_group and diff_id tables.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.DIFF_3 IS 'Diff_group or diff_id that differentiates the current item from its item_parent.  For an item that is a parent, this field may be either a group (i.e. Mens pant sizes) or a value (6 oz).  For an item that is not a parent, this field may contain a value  (34X34, Red, etc.)  Valid values are found on the diff_group and diff_id tables.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.DIFF_4 IS 'Diff_group or diff_id that differentiates the current item from its item_parent.  For an item that is a parent, this field may be either a group (i.e. Mens pant sizes) or a value (6 oz).  For an item that is not a parent, this field may contain a value  (34X34, Red, etc.)  Valid values are found on the diff_group and diff_id tables.' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.DIFF_1_DESC IS 'Description of the differential number. (for example, Blueberry, Shower Fresh, Red, etc.)' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.DIFF_2_DESC IS 'Description of the differential number. (for example, Blueberry, Shower Fresh, Red, etc.)' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.DIFF_3_DESC IS 'Description of the differential number. (for example, Blueberry, Shower Fresh, Red, etc.)' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.DIFF_4_DESC IS 'Description of the differential number. (for example, Blueberry, Shower Fresh, Red, etc.)' 
;

COMMENT ON COLUMN ALC_WORK_ITEM_SOURCE_DIFF.ITEM_PARENT_DESC IS 'Long description of the item''s parent.
' 
;
PROMPT CREATING INDEX 'PK_ALC_WORK_ITEM_SOURCE_DIFF';
CREATE UNIQUE INDEX PK_ALC_WORK_ITEM_SOURCE_DIFF ON ALC_WORK_ITEM_SOURCE_DIFF 
    ( 
     WORK_HEADER_ID ASC , 
     ITEM ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_WORK_ITEM_SOURCE_DIFF';
ALTER TABLE ALC_WORK_ITEM_SOURCE_DIFF 
    ADD CONSTRAINT PK_ALC_WORK_ITEM_SOURCE_DIFF PRIMARY KEY ( WORK_HEADER_ID, ITEM ) 
    USING INDEX PK_ALC_WORK_ITEM_SOURCE_DIFF ;




PROMPT CREATING TABLE 'ALC_WORK_SESSION_ITEM';
CREATE TABLE ALC_WORK_SESSION_ITEM 
    ( 
     WORK_ITEM_SESSION_ID NUMBER (15)  NOT NULL , 
     FACET_SESSION_ID     VARCHAR2 (50)  NOT NULL , 
     ITEM                 VARCHAR2 (70)  NOT NULL , 
     THUMBNAIL            VARCHAR2 (255) , 
     PACK_CONFIGURATION   NUMBER (15) , 
     ANCESTOR_WORK_ID     NUMBER (15) , 
     ITEM_SOURCE_DESC     VARCHAR2 (250) , 
     ITEM_TYPE            VARCHAR2 (10) , 
     ITEM_ID              VARCHAR2 (25) , 
     DIFF_1               VARCHAR2 (10) , 
     DIFF_2               VARCHAR2 (10) , 
     DIFF_3               VARCHAR2 (10) , 
     DISABLE_IND          VARCHAR2 (1) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



PROMPT CREATING CHECK CONSTRAINT ON 'ALC_WORK_SESSION_ITEM';
ALTER TABLE ALC_WORK_SESSION_ITEM 
    ADD CONSTRAINT CHK_ALC_WORK_SESSION_ITEM 
    CHECK (DISABLE_IND IN ('Y','N')) 
;


COMMENT ON TABLE ALC_WORK_SESSION_ITEM IS 'This tables holds information about facet session item and pack configuration details.'
;


COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.WORK_ITEM_SESSION_ID IS 'Thi column is the Identifier for ALC_WORK_SESSION_ITEM table.  It will be popluated with the value from ALC_WORK_ITEM_SOURCE.WORK_ITEM_SOURCE_ID' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.FACET_SESSION_ID IS 'This holds data for facet session id sent from user interface.' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.ITEM IS 'The item from item master and may  be appended with the diff ids' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.THUMBNAIL IS 'This holds image address  information from  item_image table' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.PACK_CONFIGURATION IS 'This holds the pack configuration details from packitem_breakout table' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.ANCESTOR_WORK_ID IS 'This is the  WORK_ITEM_SESSION_ID  for its parent item' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.ITEM_SOURCE_DESC IS 'Populated from ALC_WORK_ITEM_SOURCE' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.ITEM_TYPE IS 'Item Node Type.
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.ITEM_ID IS 'RMS Item ID' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.DIFF_1 IS 'First Diff Aggregation' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.DIFF_2 IS 'Second Diff Aggregation' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.DIFF_3 IS 'Third Diff Aggregation' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM.DISABLE_IND IS 'This indicator is used for determining if the Item can be Allocated From Worksheet Sessions table' 
;
PROMPT CREATING INDEX 'PK_ALC_WORK_SESSION_ITEM';
CREATE UNIQUE INDEX PK_ALC_WORK_SESSION_ITEM ON ALC_WORK_SESSION_ITEM 
    ( 
     WORK_ITEM_SESSION_ID ASC , 
     FACET_SESSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_WORK_SESSION_ITEM_I1';
CREATE INDEX ALC_WORK_SESSION_ITEM_I1 ON ALC_WORK_SESSION_ITEM 
    ( 
     FACET_SESSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;

PROMPT CREATING PRIMARY KEY ON 'ALC_WORK_SESSION_ITEM';
ALTER TABLE ALC_WORK_SESSION_ITEM 
    ADD CONSTRAINT PK_ALC_WORK_SESSION_ITEM PRIMARY KEY ( WORK_ITEM_SESSION_ID, FACET_SESSION_ID ) 
    USING INDEX PK_ALC_WORK_SESSION_ITEM ;




PROMPT CREATING TABLE 'ALC_WORK_SESSION_ITEM_ALL';
CREATE TABLE ALC_WORK_SESSION_ITEM_ALL 
    ( 
     WORK_ITEM_SESSION_ID NUMBER (15)  NOT NULL , 
     FACET_SESSION_ID     VARCHAR2 (50)  NOT NULL , 
     ITEM                 VARCHAR2 (70)  NOT NULL , 
     THUMBNAIL            VARCHAR2 (255) , 
     PACK_CONFIGURATION   NUMBER (15) , 
     ANCESTOR_WORK_ID     NUMBER (15) , 
     ITEM_TYPE            VARCHAR2 (10) , 
     ITEM_ID              VARCHAR2 (25) , 
     DIFF_1               VARCHAR2 (10) , 
     DIFF_2               VARCHAR2 (10) , 
     DIFF_3               VARCHAR2 (10) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_WORK_SESSION_ITEM_ALL IS 'This tables holds information about ALL facet session item and pack configuration details.'
;


COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_ALL.WORK_ITEM_SESSION_ID IS 'This column is the Identifier for ALC_WORK_SESSION_ITEM_ALL table.' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_ALL.FACET_SESSION_ID IS 'This holds facet session id sent from user interface.' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_ALL.ITEM IS 'This column contains the Item Node (Parent, Parent-Diff, SKU, PACK etc) of the worksheet.' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_ALL.THUMBNAIL IS 'This holds image address  information from  item_image table' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_ALL.PACK_CONFIGURATION IS 'This holds the pack configuration details from packitem_breakout table' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_ALL.ANCESTOR_WORK_ID IS 'This is the  WORK_ITEM_SESSION_ID  for its parent item	' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_ALL.ITEM_TYPE IS 'Item Node Type .
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_ALL.ITEM_ID IS 'RMS Item ID' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_ALL.DIFF_1 IS 'First Diff Aggregation' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_ALL.DIFF_2 IS 'Second Diff Aggregation' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_ALL.DIFF_3 IS 'Third Diff Aggregation' 
;
PROMPT CREATING INDEX 'PK_ALC_WORK_SESSION_ITEM_ALL';
CREATE UNIQUE INDEX PK_ALC_WORK_SESSION_ITEM_ALL ON ALC_WORK_SESSION_ITEM_ALL 
    ( 
     WORK_ITEM_SESSION_ID ASC , 
     FACET_SESSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;
PROMPT CREATING INDEX 'ALC_WORK_SESSION_ITEM_ALL_I1';
CREATE INDEX ALC_WORK_SESSION_ITEM_ALL_I1 ON ALC_WORK_SESSION_ITEM_ALL 
    ( 
     FACET_SESSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;

PROMPT CREATING PRIMARY KEY ON 'ALC_WORK_SESSION_ITEM_ALL';
ALTER TABLE ALC_WORK_SESSION_ITEM_ALL 
    ADD CONSTRAINT PK_ALC_WORK_SESSION_ITEM_ALL PRIMARY KEY ( WORK_ITEM_SESSION_ID, FACET_SESSION_ID ) 
    USING INDEX PK_ALC_WORK_SESSION_ITEM_ALL ;




PROMPT CREATING TABLE 'ALC_WORK_SESSION_ITEM_LOC';
CREATE TABLE ALC_WORK_SESSION_ITEM_LOC 
    ( 
     WORK_ITEM_LOC_SESSION_ID NUMBER (15)  NOT NULL , 
     WORK_ITEM_SESSION_ID     NUMBER (15)  NOT NULL , 
     FACET_SESSION_ID         VARCHAR2 (50)  NOT NULL , 
     WH                       NUMBER (15)  NOT NULL , 
     WH_AVAIL_QTY             NUMBER (12,4) , 
     PO_AVAIL_QTY             NUMBER (12,4) , 
     TSF_AVAIL_QTY            NUMBER (12,4) , 
     BOL_AVAIL_QTY            NUMBER (12,4) , 
     ASN_AVAIL_QTY            NUMBER (12,4) , 
     ALLOC_AVAIL_QTY          NUMBER (12,4) 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_WORK_SESSION_ITEM_LOC IS 'This table holds the item location informaton and their available quantity for different sources'
;


COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_LOC.WORK_ITEM_LOC_SESSION_ID IS 'This is the primary unique id generated from the sequence.' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_LOC.WORK_ITEM_SESSION_ID IS 'This is the foriegn key to the primary key of  ALC_WORK_SESSION_ITEM table' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_LOC.FACET_SESSION_ID IS 'This holds session id from UI and is FK with WORK_ITEM_SESSION_ID' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_LOC.WH IS 'This holds wh information for an item' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_LOC.WH_AVAIL_QTY IS 'This holds inventory for WH sourced  items' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_LOC.PO_AVAIL_QTY IS 'This holds inventory for PO sourced  items' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_LOC.TSF_AVAIL_QTY IS 'This holds inventory for TSF sourced  items' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_LOC.BOL_AVAIL_QTY IS 'This holds inventory for BOL sourced  items' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_LOC.ASN_AVAIL_QTY IS 'This holds inventory for ASN sourced  items' 
;

COMMENT ON COLUMN ALC_WORK_SESSION_ITEM_LOC.ALLOC_AVAIL_QTY IS 'This holds inventory for ALLOC sourced  items' 
;
PROMPT CREATING INDEX 'PK_ALC_WORK_SESSION_ITEM_LOC';
CREATE UNIQUE INDEX PK_ALC_WORK_SESSION_ITEM_LOC ON ALC_WORK_SESSION_ITEM_LOC 
    ( 
     WORK_ITEM_LOC_SESSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_WORK_SESSION_ITEM_LOC_I1';
CREATE INDEX ALC_WORK_SESSION_ITEM_LOC_I1 ON ALC_WORK_SESSION_ITEM_LOC 
    ( 
     FACET_SESSION_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX;

PROMPT CREATING PRIMARY KEY ON 'ALC_WORK_SESSION_ITEM_LOC';
ALTER TABLE ALC_WORK_SESSION_ITEM_LOC 
    ADD CONSTRAINT PK_ALC_WORK_SESSION_ITEM_LOC PRIMARY KEY ( WORK_ITEM_LOC_SESSION_ID ) 
    USING INDEX PK_ALC_WORK_SESSION_ITEM_LOC ;




PROMPT CREATING TABLE 'ALC_XREF';
CREATE TABLE ALC_XREF 
    ( 
     XREF_ID        NUMBER (25)  NOT NULL , 
     ALLOC_ID       NUMBER (15)  NOT NULL , 
     ITEM_ID        VARCHAR2 (40)  NOT NULL , 
     WH_ID          VARCHAR2 (40)  NOT NULL , 
     RELEASE_DATE   DATE  NOT NULL , 
     PARENT_ITEM_ID VARCHAR2 (40) , 
     DIFF1_ID       VARCHAR2 (40) , 
     ORDER_NO       VARCHAR2 (40) , 
     ALLOCATED_QTY  NUMBER (12,4)  NOT NULL , 
     XREF_ALLOC_NO  NUMBER (12)  NOT NULL , 
     CLOSE_IND      VARCHAR2 (1) DEFAULT 'N' 
    ) 
        INITRANS 6 
        TABLESPACE RETAIL_DATA;



COMMENT ON TABLE ALC_XREF IS 'this table contains the cross-reference data for the publishing of the information to the base rms allocation tables (alloc_header and alloc_detail). this table will contain a record for each allocation/item/wh/release date that is in reserved, approved, processed and closed status. this table allows the allocation data structure to merge with the rms allocation data structure.'
;


COMMENT ON COLUMN ALC_XREF.XREF_ID IS 'This column contains a unique cross reference identifier. this value is derived from the sequence alc_xref_seq.' 
;

COMMENT ON COLUMN ALC_XREF.ALLOC_ID IS 'This column contains the unique identifier for the allocation within the application' 
;

COMMENT ON COLUMN ALC_XREF.ITEM_ID IS 'This column contains the item identifier.' 
;

COMMENT ON COLUMN ALC_XREF.WH_ID IS 'This column contains the warehouse identifier.' 
;

COMMENT ON COLUMN ALC_XREF.RELEASE_DATE IS 'This column contains the release date for this allocation.' 
;

COMMENT ON COLUMN ALC_XREF.PARENT_ITEM_ID IS 'This column contains the parent item identifier, if populated.' 
;

COMMENT ON COLUMN ALC_XREF.DIFF1_ID IS 'This column contains the diff1 identifier.' 
;

COMMENT ON COLUMN ALC_XREF.ORDER_NO IS 'This column contains the purchase order identifier.' 
;

COMMENT ON COLUMN ALC_XREF.ALLOCATED_QTY IS 'This column contains the allocated quantity.' 
;

COMMENT ON COLUMN ALC_XREF.XREF_ALLOC_NO IS 'This column contains the allocation number in RMS alloc_header. All RMS tables will reference this number as the allocation number, not the Allocation ID used in Allocation' 
;

COMMENT ON COLUMN ALC_XREF.CLOSE_IND IS 'This column contains the allocation closed indicator' 
;
PROMPT CREATING INDEX 'PK_ALC_XREF';
CREATE UNIQUE INDEX PK_ALC_XREF ON ALC_XREF 
    ( 
     XREF_ID ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;
PROMPT CREATING INDEX 'ALC_XREF_I1';
CREATE UNIQUE INDEX ALC_XREF_I1 ON ALC_XREF 
    ( 
     ALLOC_ID ASC , 
     ITEM_ID ASC , 
     WH_ID ASC , 
     RELEASE_DATE ASC , 
     ORDER_NO ASC , 
     XREF_ALLOC_NO ASC 
    ) 
    TABLESPACE RETAIL_INDEX 
    INITRANS 12;

PROMPT CREATING PRIMARY KEY ON 'ALC_XREF';
ALTER TABLE ALC_XREF 
    ADD CONSTRAINT PK_ALC_XREF PRIMARY KEY ( XREF_ID ) 
    USING INDEX PK_ALC_XREF ;




PROMPT CREATING TABLE 'GTT_ALC_ITEMS';
CREATE GLOBAL TEMPORARY TABLE GTT_ALC_ITEMS 
    ( 
     ITEM       VARCHAR2 (25) , 
     PACK_IND   VARCHAR2 (1) , 
     ITEM_LEVEL NUMBER (1) , 
     TRAN_LEVEL NUMBER (1) 
    ) 
    ON COMMIT DELETE ROWS 
;


PROMPT CREATING INDEX 'SMR_GTT_ALC_ITEMS_I1';
CREATE INDEX SMR_GTT_ALC_ITEMS_I1 ON GTT_ALC_ITEMS 
    ( 
     ITEM ASC 
    ) 
;




PROMPT Creating Table 'ALC_SESSION_ITEMLOC_GTT'
CREATE GLOBAL TEMPORARY TABLE ALC_SESSION_ITEMLOC_GTT
    (
	 ITEM_LOC_SESSION_ID 		NUMBER(15) 	NOT NULL,
     FACET_SESSION_ID 			VARCHAR2(50 )  NOT NULL,
     ITEM_LOC_ID 				NUMBER(25),
     ALLOC_ID 					NUMBER(15),
     ITEM_ID 					VARCHAR2(70 ),
     ITEM_DESC 					VARCHAR2(250 ),
     ITEM_TYPE 					VARCHAR2(10 ),
     WH_ID 						NUMBER(10),
     RELEASE_DATE 				DATE,
     LOCATION_ID 				NUMBER(10),
     LOCATION_DESC 				VARCHAR2(150 ),
     GROUP_ID 					VARCHAR2(40 ),
     GROUP_DESC 				VARCHAR2(600 ),
     GROUP_TYPE 				NUMBER(2),
     LOC_GROUP_ID 				NUMBER(10),
     ALLOCATED_QTY 				NUMBER(12,4),
     CALCULATED_QTY 			NUMBER(12,4),
     NEED_QTY 					NUMBER(12,4),
     ON_HAND_QTY 				NUMBER(12,4),
     IN_TRANSIT 				NUMBER(12,4),
     ON_ORDER 					NUMBER(12,4),
     ON_ALLOC 					NUMBER(12,4),
     ALLOC_OUT 					NUMBER(12,4),
     SOM_QTY 					NUMBER(12,4),
     BACKORDER_QTY 				NUMBER(12,4),
     FREEZE_IND 				VARCHAR2(1 ),
     NEXT_1_WEEK_QTY 			NUMBER(15),
     NEXT_2_WEEK_QTY 			NUMBER(15),
     NEXT_3_WEEK_QTY 			NUMBER(15),
     DIFF1_ID 					VARCHAR2(10 ),
     DIFF1_DESC 				VARCHAR2(120 ),
     DIFF2_ID 					VARCHAR2(10 ),
     DIFF2_DESC 				VARCHAR2(120 ),
     DIFF3_ID 					VARCHAR2(10 ),
     DIFF3_DESC 				VARCHAR2(120 ),
     PARENT_ITEM_ID 			VARCHAR2(25 ),
     CREATED_ORDER_NO 			VARCHAR2(40 ),
     CREATED_SUPPLIER_ID 		VARCHAR2(40 ),
     FUTURE_UNIT_RETAIL 		NUMBER(20,4),
     RUSH_FLAG 					VARCHAR2(1 ),
     COST 						NUMBER(20,4),
     IN_STORE_DATE 				DATE,
     FUTURE_ON_HAND_QTY 		NUMBER(12,4),
     ORDER_NO 					VARCHAR2(40 ),
     SOURCE_TYPE 				NUMBER(1),
     GROSS_NEED_QTY 			NUMBER(12,4) DEFAULT 0,
     RLOH_QTY 					NUMBER(12,4) DEFAULT 0,
     FILTERED_IND 				VARCHAR2(1 ) DEFAULT 'N',
     RESULT_FILTER_IND 			VARCHAR2(1 ) DEFAULT 'N',
     QUANTITY_LIMITS_FILTER_IND VARCHAR2(1 ) DEFAULT 'N',
     PARENT_ITEM_LOC_SESSION_ID NUMBER(15),
     PACK_COMP_QTY 				NUMBER(12,4),
     CREATED_BY 				VARCHAR2(20 ),
     UPDATED_BY 				VARCHAR2(20 ),
     CREATED_DATE 				DATE,
     UPDATE_DATE 				DATE,
     OBJECT_VERSION_ID			VARCHAR2(20 ),
     QBE_FILTER_IND 			VARCHAR2(1 ) DEFAULT 'N',
     LOC_TYPE 					VARCHAR2(1 )
    )
    ON COMMIT DELETE ROWS
;




COMMENT ON TABLE ALC_SESSION_ITEMLOC_GTT is 'Working table to help the PLSQL allocation session package.  This table will hold all Item and Location session information before they are finally moved into the ALC_SESSION_ITEM_LOC table.'
;


COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.ITEM_LOC_SESSION_ID is 'Primary key for the table.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.FACET_SESSION_ID is 'Entity Session ID per view'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.ITEM_LOC_ID is 'Column refering item_loc column in alc_item_loc table.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.ALLOC_ID is 'Allocation Id and the foriegn key to the primary key of  ALC_ALLOC table'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.ITEM_ID is 'Item_id from ALC_ITEM_SOURCE table and ALC_ITEM_LOC table.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.ITEM_DESC is 'Item Description.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.ITEM_TYPE is 'Item Type valid values include:
STYLE - Style
FA - Fashion Item or Style/Color
ST - Staple Item
FASHIONSKU - Fashion Item
PACKCOMP - Pack Component
NSFSP - Non-Sellable Fashion Simple Pack
NSSSP - Non-Sellable Staple Simple Pack
NSSCP - Non-Sellable Staple Complex Pack
NSFMCP - Non-Sellable Fashion Multi-Color Pack
NSFSCP - Non-Sellable Fashion Single Color Pack
SELLPACK - Sellable Pack'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.WH_ID is 'Warehouse Number.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.RELEASE_DATE is 'Release Date.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.LOCATION_ID is 'Store ID'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.LOCATION_DESC is 'Store Description.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.GROUP_ID is 'Store Group ID'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.GROUP_DESC is 'Store Group Description.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.GROUP_TYPE is 'Group Type ID'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.LOC_GROUP_ID is 'Location Gropu Unique Id, used to refer the Complex group operation ID in Location screen.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.ALLOCATED_QTY is 'Final Allocated Quantity.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.CALCULATED_QTY is 'Calculated quantity given by Calculation Engine.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.NEED_QTY is 'Net Need'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.ON_HAND_QTY is 'Store ON Hand'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.IN_TRANSIT is 'The in transit inventory position.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.ON_ORDER is 'The on order inventory position.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.ON_ALLOC is 'The on allocation inventory position.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.ALLOC_OUT is 'The quantity currently allocated from the item/store'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.SOM_QTY is 'Store Order Multiple Quantity'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.BACKORDER_QTY is 'Used to store backorder quantity information used by Allocation Maintenance Results section.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.FREEZE_IND is 'Freeze_ind refers if the Item_loc column in the UI is frozen (not to be changed when submitting another calculation). Frozen records will have the freeze_ind set to Y. Valid values are (Y, N).'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.NEXT_1_WEEK_QTY is 'Next 1st Week Plan/Forecast Sales data.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.NEXT_2_WEEK_QTY is 'Next 2nd Week Plan/Forecast Sales data.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.NEXT_3_WEEK_QTY is 'Next 4th Week Plan/Forecast Sales data.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.DIFF1_ID is 'Aggregate Diff 1 ID'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.DIFF1_DESC is 'Aggregate Diff 1 Description'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.DIFF2_ID is 'Aggregate Diff 2 ID'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.DIFF2_DESC is 'Aggregate Diff 2 Description'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.DIFF3_ID is 'Aggregate Diff 3 ID'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.DIFF3_DESC is 'Aggregate Diff 3 Description'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.PARENT_ITEM_ID is 'The Item node Id to refer the Style ID for a Fashion SKU.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.CREATED_ORDER_NO is 'Allocation can create a Purchase order from What-if Allocation, the Purchase order created will be stored in this table.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.CREATED_SUPPLIER_ID is 'Created Supplier ID.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.FUTURE_UNIT_RETAIL is 'RPM Price for Item/Loc/Release Date'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.RUSH_FLAG is 'A marker column to refer if a Item Loc is a Rush shipment.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.COST is 'Item costing data for the Item'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.IN_STORE_DATE is 'Allocation Instore Date'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.FUTURE_ON_HAND_QTY is 'Future On hand quantity consists of In transi, In bound quantity'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.ORDER_NO is 'Purchase Order Number'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.SOURCE_TYPE is 'The type of the source transaction held in the order_no column.  Valid values are:
PO=1
ASN=2
OH=3 (Warehouse sourced)
WHATIF=4
BOL=5
AND TSF=6'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.GROSS_NEED_QTY is 'Gross Need Quantity'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.RLOH_QTY is 'Rule Level On Hand quantity'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.FILTERED_IND is 'Filtered Indicator'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.RESULT_FILTER_IND is 'Indicates whether or not the row is visible in the results UI'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.QUANTITY_LIMITS_FILTER_IND is 'Indicates whether or not the row is visible in the quantity limits UI. '
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.PARENT_ITEM_LOC_SESSION_ID is 'The column that refers the Item Loc Session Id in this table, this maintian the Parent and child relation in single table'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.PACK_COMP_QTY is 'For non-sellable packs this column will hold the total number of components items in the pack.  For fasion pack groups this row will have the sum of components of all packs that make up the group.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.CREATED_BY is 'Indicates the user who created the record.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.UPDATED_BY is 'Indicates the user who last updated the record.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.CREATED_DATE is 'The timestamp of the record creation date.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.UPDATE_DATE is 'The timestamp of the record last updated date.'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.OBJECT_VERSION_ID is 'This column indicates Object version ID'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.QBE_FILTER_IND is 'Indicates rows filtered by query by example'
;

COMMENT ON COLUMN ALC_SESSION_ITEMLOC_GTT.LOC_TYPE is 'Determines whether the location is a store or warehouse. Valid values are S and W'
;



PROMPT Creating Table 'ALC_SYSTEM_OPTIONS_OI'
CREATE TABLE ALC_SYSTEM_OPTIONS_OI
    (
	 PO_ALC_TIME_THRESHOLD    NUMBER(5) DEFAULT 0 NOT NULL,
     ASN_ALC_TIME_THRESHOLD   NUMBER(5) DEFAULT 0 NOT NULL,
     NEED_CALC_TYPE           CHAR(1 ) DEFAULT 'F' NOT NULL,
     ALLOCATE_BY              CHAR(10 ) DEFAULT 'PO' NOT NULL,
     SIMP_PROMO_ONLY          CHAR(1 ) DEFAULT 'Y' NOT NULL,
     ALLOCATED_PO_THRESHOLD   NUMBER(10,2) DEFAULT 0 NOT NULL,
     ALLOCATED_ASN_THRESHOLD  NUMBER(10,2) DEFAULT 0 NOT NULL,
     INV_TOL_TO_FORECAST_PLAN NUMBER(10,2) DEFAULT 0 NOT NULL
    )
        INITRANS 6
        TABLESPACE RETAIL_DATA;

		

COMMENT ON TABLE ALC_SYSTEM_OPTIONS_OI is 'This is system options for allocation operation insights reporting configuration.'
;


COMMENT ON COLUMN ALC_SYSTEM_OPTIONS_OI.PO_ALC_TIME_THRESHOLD is 'Time period in days before the expected last date of PO arrival against an order within which allocation must be done to ensure that the products are on the sales floor in time'
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS_OI.ASN_ALC_TIME_THRESHOLD is 'Time period in days before the expected last date of shipment against an order within which allocation must be done to ensure that the products are on the sales floor in time'
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS_OI.NEED_CALC_TYPE is 'Indicator to show plan or forecast in reports.If P plan metrics and if F forecast metrics'
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS_OI.ALLOCATE_BY is 'Indicates if the retailer uses PO or ASN to allocate primarily, this will drive the default source used in the reports.'
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS_OI.SIMP_PROMO_ONLY is 'A value of Y or N to show simple promotions only or all promotions'
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS_OI.ALLOCATED_PO_THRESHOLD is 'A percentage tolerance for PO'
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS_OI.ALLOCATED_ASN_THRESHOLD is 'A percentage tolerance for ASN'
;

COMMENT ON COLUMN ALC_SYSTEM_OPTIONS_OI.INV_TOL_TO_FORECAST_PLAN is 'A percentage tolerane value'
;



PROMPT CREATING FOREIGN KEY ON 'ALC_CORPORATE_RULE_DETAIL';
ALTER TABLE ALC_CORPORATE_RULE_DETAIL 
    ADD CONSTRAINT ACD_ACR_FK FOREIGN KEY 
    ( 
     CORPORATE_RULE_ID
    ) 
    REFERENCES ALC_CORPORATE_RULE_HEAD 
    ( 
     CORPORATE_RULE_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_CODE_DETAIL';
ALTER TABLE ALC_CODE_DETAIL 
    ADD CONSTRAINT ACL_ACH_FK FOREIGN KEY 
    ( 
     CODE_TYPE
    ) 
    REFERENCES ALC_CODE_HEAD 
    ( 
     CODE_TYPE
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_SCHEDULE';
ALTER TABLE ALC_SCHEDULE 
    ADD CONSTRAINT ACS_AAL_FK FOREIGN KEY 
    ( 
     PARENT_ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_GENERATED_PO';
ALTER TABLE ALC_GENERATED_PO 
    ADD CONSTRAINT AGP_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_ITEM_LOC';
ALTER TABLE ALC_ITEM_LOC 
    ADD CONSTRAINT AIL_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_ITEM_SOURCE';
ALTER TABLE ALC_ITEM_SOURCE 
    ADD CONSTRAINT AIS_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_ITEM_LOC_EXCLUSION';
ALTER TABLE ALC_ITEM_LOC_EXCLUSION 
    ADD CONSTRAINT AIX_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_GID_PROFILE';
ALTER TABLE ALC_GID_PROFILE 
    ADD CONSTRAINT ALC_GID_PROFILE_FK1 FOREIGN KEY 
    ( 
     GID_ID
    ) 
    REFERENCES ALC_GID_HEADER 
    ( 
     ID
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_SESSION_GID_PROFILE_LIST';
ALTER TABLE ALC_SESSION_GID_PROFILE_LIST 
    ADD CONSTRAINT ALC_SESSION_GIDPROFILELIST_FK FOREIGN KEY 
    ( 
     GID_PROFILE_LIST_ID
    ) 
    REFERENCES ALC_SESSION_GID_PROFILE 
    ( 
     GID_PROFILE_LIST_ID
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_SESSION_SIZE_PROFILE_RATIO';
ALTER TABLE ALC_SESSION_SIZE_PROFILE_RATIO 
    ADD CONSTRAINT ALC_SESSION_SP_RATIO_FK FOREIGN KEY 
    ( 
     SESSION_ID,
     RESULT_TYPE,
     SEQ_ID
    ) 
    REFERENCES ALC_SESSION_GID_PROFILE 
    ( 
     SESSION_ID,
     RESULT_TYPE,
     SEQ_ID
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_SIZE_PROFILE';
ALTER TABLE ALC_SIZE_PROFILE 
    ADD CONSTRAINT ALC_SIZE_PROFILE_FK1 FOREIGN KEY 
    ( 
     GID_PROFILE_ID
    ) 
    REFERENCES ALC_GID_PROFILE 
    ( 
     GID_PROFILE_ID
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_WORK_ALLOC';
ALTER TABLE ALC_WORK_ALLOC 
    ADD CONSTRAINT ALC_WA_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_WORK_ALLOC';
ALTER TABLE ALC_WORK_ALLOC 
    ADD CONSTRAINT ALC_WA_ALC_WH_FK FOREIGN KEY 
    ( 
     WORK_HEADER_ID
    ) 
    REFERENCES ALC_WORK_HEADER 
    ( 
     WORK_HEADER_ID
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_WORK_ITEM_SOURCE_ALLOC';
ALTER TABLE ALC_WORK_ITEM_SOURCE_ALLOC 
    ADD CONSTRAINT ALC_WISA_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_WORK_ITEM_SOURCE_ALLOC';
ALTER TABLE ALC_WORK_ITEM_SOURCE_ALLOC 
    ADD CONSTRAINT ALC_WISA_ALC_WIS_FK FOREIGN KEY 
    ( 
     WORK_ITEM_SOURCE_ID
    ) 
    REFERENCES ALC_WORK_ITEM_SOURCE 
    ( 
     WORK_ITEM_SOURCE_ID
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_WORK_ITEM_SOURCE';
ALTER TABLE ALC_WORK_ITEM_SOURCE 
    ADD CONSTRAINT ALC_WIS_ALC_WH_FK FOREIGN KEY 
    ( 
     WORK_HEADER_ID
    ) 
    REFERENCES ALC_WORK_HEADER 
    ( 
     WORK_HEADER_ID
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_LOC_GROUP_DETAIL';
ALTER TABLE ALC_LOC_GROUP_DETAIL 
    ADD CONSTRAINT ALGD_ALG_FK FOREIGN KEY 
    ( 
     LOC_GROUP_ID
    ) 
    REFERENCES ALC_LOC_GROUP 
    ( 
     LOC_GROUP_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_LOC_GROUP';
ALTER TABLE ALC_LOC_GROUP 
    ADD CONSTRAINT ALG_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_LOC_GROUP';
ALTER TABLE ALC_LOC_GROUP 
    ADD CONSTRAINT ALG_ATE_FK FOREIGN KEY 
    ( 
     TEMPLATE_ID
    ) 
    REFERENCES ALC_TEMPLATE 
    ( 
     TEMPLATE_ID
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_LOCATION';
ALTER TABLE ALC_LOCATION 
    ADD CONSTRAINT ALO_ALG_FK FOREIGN KEY 
    ( 
     LOC_GROUP_ID
    ) 
    REFERENCES ALC_LOC_GROUP 
    ( 
     LOC_GROUP_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_PREPACK_SET';
ALTER TABLE ALC_PREPACK_SET 
    ADD CONSTRAINT ALP_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_XREF';
ALTER TABLE ALC_XREF 
    ADD CONSTRAINT ALX_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_PREPACK_CALC_RESULTS';
ALTER TABLE ALC_PREPACK_CALC_RESULTS 
    ADD CONSTRAINT APC_ALP_FK FOREIGN KEY 
    ( 
     PREPACK_SET_ID
    ) 
    REFERENCES ALC_PREPACK_SET 
    ( 
     PREPACK_SET_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_APPROVAL_QUANTITY';
ALTER TABLE ALC_APPROVAL_QUANTITY 
    ADD CONSTRAINT APQ_AIS_FK FOREIGN KEY 
    ( 
     ITEM_SOURCE_ID
    ) 
    REFERENCES ALC_ITEM_SOURCE 
    ( 
     ITEM_SOURCE_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_PREPACK_SET_ITEM';
ALTER TABLE ALC_PREPACK_SET_ITEM 
    ADD CONSTRAINT APS_ALP_FK FOREIGN KEY 
    ( 
     PREPACK_SET_ID
    ) 
    REFERENCES ALC_PREPACK_SET 
    ( 
     PREPACK_SET_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_QUANTITY_LIMITS';
ALTER TABLE ALC_QUANTITY_LIMITS 
    ADD CONSTRAINT AQL_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_RULE_DATE';
ALTER TABLE ALC_RULE_DATE 
    ADD CONSTRAINT ARD_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_RULE_MANY_TO_ONE';
ALTER TABLE ALC_RULE_MANY_TO_ONE 
    ADD CONSTRAINT ARE_ARU_FK FOREIGN KEY 
    ( 
     RULE_ID
    ) 
    REFERENCES ALC_RULE 
    ( 
     RULE_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_RULE';
ALTER TABLE ALC_RULE 
    ADD CONSTRAINT ARU_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_RULE';
ALTER TABLE ALC_RULE 
    ADD CONSTRAINT ARU_ATE_FK FOREIGN KEY 
    ( 
     TEMPLATE_ID
    ) 
    REFERENCES ALC_TEMPLATE 
    ( 
     TEMPLATE_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_ITEM_SOURCE_SHIP_SCHEDULE';
ALTER TABLE ALC_ITEM_SOURCE_SHIP_SCHEDULE 
    ADD CONSTRAINT ASH_AIS_FK FOREIGN KEY 
    ( 
     ITEM_SOURCE_ID
    ) 
    REFERENCES ALC_ITEM_SOURCE 
    ( 
     ITEM_SOURCE_ID
    ) 
    ON DELETE CASCADE 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_TASK';
ALTER TABLE ALC_TASK 
    ADD CONSTRAINT ATK_AAL_FK FOREIGN KEY 
    ( 
     ALLOC_ID
    ) 
    REFERENCES ALC_ALLOC 
    ( 
     ALLOC_ID
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_WH_SUPPLY_PATH';
ALTER TABLE ALC_WH_SUPPLY_PATH 
    ADD CONSTRAINT AWS_WAH_FK FOREIGN KEY 
    ( 
     SOURCE_WH
    ) 
    REFERENCES WH 
    ( 
     WH
    ) 
    NOT DEFERRABLE 
;



PROMPT CREATING FOREIGN KEY ON 'ALC_WH_SUPPLY_PATH';
ALTER TABLE ALC_WH_SUPPLY_PATH 
    ADD CONSTRAINT AWS_WAH_FK2 FOREIGN KEY 
    ( 
     DESTINATION_WH
    ) 
    REFERENCES WH 
    ( 
     WH
    ) 
    NOT DEFERRABLE 
;

PROMPT CREATING VIEW 'ALC_ITEM_TYPE_VIEW';
CREATE OR REPLACE FORCE VIEW ALC_ITEM_TYPE_VIEW ( ITEM, ITEM_TYPE ) AS
select distinct p.item ||
                decode(p.diff_1_aggregate_ind, 'Y', ' 1~'||c.diff_1, null) ||
                decode(p.diff_2_aggregate_ind, 'Y', ' 2~'||c.diff_2, null) ||
                decode(p.diff_3_aggregate_ind, 'Y', ' 3~'||c.diff_3, null) ||
                decode(p.diff_4_aggregate_ind, 'Y', ' 4~'||c.diff_4, null) item,
                'FA' item_type
  from item_master p,
       item_master c
 where p.item_aggregate_ind = 'Y'
   and p.item               = c.item_parent
UNION ALL
select item,
       alc_item_type
  from item_master ;



COMMENT ON TABLE ALC_ITEM_TYPE_VIEW IS 'View to help map RMS items to ALlocation item types.'
;


COMMENT ON COLUMN ALC_ITEM_TYPE_VIEW.ITEM IS 'The RMS item.' 
;

COMMENT ON COLUMN ALC_ITEM_TYPE_VIEW.ITEM_TYPE IS 'The Allocation type of the RMS item.' 
;



PROMPT CREATING VIEW 'V_ALC_PACK_TEMP';
CREATE OR REPLACE FORCE VIEW V_ALC_PACK_TEMP ( ALLOC_ID, NEED_QTY, TOTAL_ON_HAND_QTY, ALLOCATED_QTY, CALCULATED_QTY, ITEM_ID, GROUP_DESC, GROUP_ID, SUBGROUP_DESC, SUBGROUP_ID, LOCATION_ID, LOCATION_DESC, STOP_SHIP, DESC_UP, FREEZE_IND, DIFF1_ID, DIFF1_DESC, PARENT_ITEM_ID, WH_ID, SOM_QTY, BACKORDER_QTY, ITEM_LOC_ID, RELEASE_DATE, CREATED_ORDER_NO, CREATED_SUPPLIER_ID, DIFF2_ID, DIFF2_DESC, PARENT_DIFF1_ID, FUTURE_UNIT_RETAIL, RUSH_FLAG, COST, IN_STORE_DATE, ORDER_NO, SOURCE_TYPE, GROSS_NEED_QTY, RLOH_QTY, STOCK_ON_HAND, IN_TRANSIT, ON_ORDER, ON_ALLOC ) AS
SELECT DISTINCT
    /*+ index(d,pk_item_master) index(d,item_master_i1) index(d,item_master_i2) index(lg, alc_loc_group_i1) */
    il.alloc_id                                         ALLOC_ID,
    NVL(il.need_qty,0)                     NEED_QTY,
    NVL(il.total_on_hand_qty,0)  TOTAL_ON_HAND_QTY,
    il.allocated_qty                             ALLOCATED_QTY,
    NVL(il.calculated_qty,0)            CALCULATED_QTY,
    il.item_id                                         ITEM_ID,
    l.group_desc                                  GROUP_DESC,
    l.GROUP_ID                                   GROUP_ID,
    l.subgroup_desc                          SUBGROUP_DESC,
    l.subgroup_id                                SUBGROUP_ID,
    il.location_id                                  LOCATION_ID,
    l.location_desc                              LOCATION_DESC,
    'N'                                                       STOP_SHIP,
    d.desc_up                                       DESC_UP,
    il.freeze_ind                                  FREEZE_IND,
    il.diff1_id                                         DIFF1_ID,
    il.diff1_desc                                   DIFF1_DESC,
    il.parent_item_id                         PARENT_ITEM_ID,
    il.wh_id                                            WH_ID,
    il.som_qty                                       SOM_QTY,
    il.backorder_qty                           BACKORDER_QTY,
    il.item_loc_id                                 ITEM_LOC_ID,
    il.release_date                              RELEASE_DATE,
    il.created_order_no                   CREATED_ORDER_NO,
    il.created_supplier_id                CREATED_SUPPLIER_ID,
    il.diff2_id                                         DIFF2_ID,
    il.diff2_desc                                   DIFF2_DESC,
    il.parent_diff1_id                         PARENT_DIFF1_ID,
    il.future_unit_retail                    FUTURE_UNIT_RETAIL,
    il.rush_flag                                      RUSH_FLAG,
    il.cost                                                COST,
    il.in_store_date                            IN_STORE_DATE,
    il.order_no                                     ORDER_NO,
    il.source_type                               SOURCE_TYPE,
    NVL(il.gross_need_qty,0)        GROSS_NEED_QTY,
    il.rloh_qty                                       RLOH_QTY,
    il.stock_on_hand                         STOCK_ON_HAND,
    il.in_transit                                     IN_TRANSIT,
    il.on_order                                     ON_ORDER,
    il.on_alloc                                        ON_ALLOC
  FROM alc_item_loc il,
    item_master d,
    (SELECT alg.alloc_id,
      alg.loc_group_id,
      al.location_desc,
      al.subgroup_id,
      al.location_id,
      al.group_id,
      alg.group_desc,
      algd.subgroup_desc,
      algd.group_type
    FROM alc_loc_group alg,
      alc_location al,
      alc_loc_group_detail algd
    WHERE algd.loc_group_id = alg.loc_group_id
    AND al.loc_group_id     = alg.loc_group_id
    AND alg.alloc_id       IS NOT NULL
    ) l
  WHERE il.alloc_id  = l.alloc_id (+)
  AND il.location_id = l.location_id (+)
  AND il.item_id     = d.item ;



COMMENT ON COLUMN V_ALC_PACK_TEMP.ALLOC_ID IS ' this column contains the unique identifier for the allocation within the application.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.ALLOCATED_QTY IS 'this column contains the allocated quantity for this item/warehouse/release date/store. this quantity will be a whole number.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.ITEM_ID IS 'this column contains the item identifier.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.LOCATION_ID IS ' this column contains the store identifier.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.FREEZE_IND IS 'this column contains the freeze values indicator. valid values are:yes = yno = n' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.DIFF1_ID IS ' this column would contain the diff1 identifier, if populated.this field will be populated for fashion items.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.DIFF1_DESC IS 'this column would contain the diff1 description, if diff1 identifier is populated.this field will be populated for fashion items.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.PARENT_ITEM_ID IS ' this column would contain the parent item identifier, if populated.this field will be populated for fashion items.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.WH_ID IS ' this column contains the warehouse identifier.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.SOM_QTY IS 'this column contains the store order multiple for this item.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.BACKORDER_QTY IS 'This column contains backorder quantity informatiion from ALC_ITEM_LOC used by the calculation process.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.ITEM_LOC_ID IS ' this column contains a unique item location identifier. this value is derived from the sequence alc_item_loc_seq.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.RELEASE_DATE IS ' this column contains the release date for this item/warehouse.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.CREATED_ORDER_NO IS 'this column will contain the purchase order identifier, if populated.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.CREATED_SUPPLIER_ID IS ' this column will contain the supplier identifier, if populated.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.DIFF2_ID IS ' this column would contain the diff2 identifier, if populated.this field will be populated for fashion items that have multiple diffs.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.DIFF2_DESC IS ' this column would contain the diff2 description, if diff2 identifier is populated.this field will be populated for fashion items that have multiple diffs.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.PARENT_DIFF1_ID IS ' this column would contain the parent item diff1 identifier, if populated.this field will be populated for fashion items.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.FUTURE_UNIT_RETAIL IS 'contains the future unit retail price in the standard unit of measure for the item/location/release date. this field is stored in the local currency' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.RUSH_FLAG IS 'indicates the item need to be rushed for to the location corresponding to itemloc' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.COST IS 'cost of freight' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.IN_STORE_DATE IS 'the date on which the freight need to reach the location' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.ORDER_NO IS 'po or asn number item is sourced from.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.SOURCE_TYPE IS 'order type the item is sourced from.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.RLOH_QTY IS 'holds rule level on hand value' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.STOCK_ON_HAND IS 'Adding column to meet  13.3 On-Hands requirement. STOCK_ON_HAND is one of four values that will make up TOTAL_ON_HAND_QTY.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.IN_TRANSIT IS 'Adding column to meet  13.3 On-Hands requirement. IN_TRANSIT is one of four values that will make up TOTAL_ON_HAND_QTY.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.ON_ORDER IS 'Adding column to meet  13.3 On-Hands requirement. ON_ORDER is one of four values that will make up TOTAL_ON_HAND_QTY.' 
;

COMMENT ON COLUMN V_ALC_PACK_TEMP.ON_ALLOC IS 'Adding column to meet  13.3 On-Hands requirement. ON_ALLOC is one of four values that will make up TOTAL_ON_HAND_QTY.' 
;
