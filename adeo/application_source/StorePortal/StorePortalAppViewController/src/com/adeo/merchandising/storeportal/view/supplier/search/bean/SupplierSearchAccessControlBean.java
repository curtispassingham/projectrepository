package com.adeo.merchandising.storeportal.view.supplier.search.bean;

import oracle.retail.apps.rms.common.view.security.accesscontrol.AbstractAccessControlBean;

public class SupplierSearchAccessControlBean extends AbstractAccessControlBean {
    public SupplierSearchAccessControlBean() {
        super();
    }    
    
    @Override
    public boolean isVisibile(String componentName) {
        return false;
    }

    @Override
    public boolean isRendered(String componentName) {
        return false;
    }
    
    @Override
    public boolean isDisabled(String componentName) {
        return false;
    }

    @Override
    public boolean isSelectiveExpressionEnable(String methodName) {
        return false;
    }
}