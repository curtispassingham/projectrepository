package com.adeo.merchandising.storeportal.view.supplier.search.bean;

import com.adeo.merchandising.storeportal.model.common.SPViewObjectImpl;

import javax.faces.event.ActionEvent;

import oracle.binding.AttributeBinding;

import oracle.jbo.VariableValueManager;

import oracle.retail.apps.rms.common.view.cfas.AbstractBackingBean;
import oracle.retail.apps.rms.common.view.util.RmsUIUtils;

public class SupplierSearchBackingBean extends AbstractBackingBean {
    private static final String CLOSE_TAB = "closeTab";
    private static final String OPEN_CONTENT = "openContent";
    private static final String FLOW_ID = "PMSupplierDetails";
    private static final String TASK_FLOW = "taskFlow";
    private static final String SUPPLIER_DETAILS_URL = 
        "/WEB-INF/com/adeo/merchandising/storeportal/view/supplier/details/flow/SupplierDetailsFlow.xml#SupplierDetailsFlow";
    
    public SupplierSearchBackingBean() {
        super();
    }

    public SPViewObjectImpl getVOFromIterator(String iterator) {
        return (SPViewObjectImpl)RmsUIUtils.findIterator(iterator).getViewObject();
    }

    public void searchSupplier(ActionEvent actionEvent) {
        String supplierId = null;
        if (RmsUIUtils.resolveExpression("#{bindings.SupplierId}") !=
            null) {
            supplierId =
                RmsUIUtils.resolveExpression("#{bindings.SupplierId}").toString();
        }

        String supName = null;
        if (RmsUIUtils.resolveExpression("#{bindings.SupplierName}") != null) {
            supName =
                RmsUIUtils.resolveExpression("#{bindings.SupplierName}").toString();
        }   
        
        String supplierSiteId = null;
        if (RmsUIUtils.resolveExpression("#{bindings.SupplierSiteId}") !=
            null) {
            supplierSiteId =
                RmsUIUtils.resolveExpression("#{bindings.SupplierSiteId}").toString();
        }

        String supSiteName = null;
        if (RmsUIUtils.resolveExpression("#{bindings.SupplierSiteName}") != null) {
            supSiteName =
                RmsUIUtils.resolveExpression("#{bindings.SupplierSiteName}").toString();
        }
        
        if ((supplierId != null && !supplierId.isEmpty()) 
            || (supName != null && !supName.isEmpty())
            || (supplierSiteId != null && !supplierSiteId.isEmpty())
            || (supSiteName != null && !supSiteName.isEmpty())){
        
            SPViewObjectImpl supsVO = getVOFromIterator("SupsIterator");
            VariableValueManager vmM = supsVO.ensureVariableManager();
            
            vmM.setVariableValue("bind_supplier_id", supplierId);
            vmM.setVariableValue("bind_sup_name", supName);
            vmM.setVariableValue("bind_supplier_site_id", supplierSiteId);
            vmM.setVariableValue("bind_sup_site_name", supSiteName);
            
            supsVO.executeQuery();
        }
        else{
            RmsUIUtils.addFacesWarningMessage("Please select at least one field!");
        }
    }

    public void resetSearchParams(ActionEvent actionEvent) {
        AttributeBinding supplierId = RmsUIUtils.findAttributeBinding("SupplierId");
        supplierId.setInputValue("");
        
        AttributeBinding supplierName = RmsUIUtils.findAttributeBinding("SupplierName");
        supplierName.setInputValue("");
        
        AttributeBinding supplierSiteId = RmsUIUtils.findAttributeBinding("SupplierSiteId");
        supplierSiteId.setInputValue("");
        
        AttributeBinding supplierSiteName = RmsUIUtils.findAttributeBinding("SupplierSiteName");
        supplierSiteName.setInputValue("");
    }

    public void selectSupplierActionListener(ActionEvent actionEvent) {
        String supplierSiteId = RmsUIUtils.resolveExpression("#{bindings.SupplierSiteId}").toString();
        
        String[] paramKey1 = { "contentType", "url", "id", "title", "parametersList", "keysList" };
        String[] paramValue1 = {
            TASK_FLOW, 
            SUPPLIER_DETAILS_URL, 
            FLOW_ID, 
            "#{adfBundle['com.adeo.merchandising.storeportal.view.StorePortalAppViewControllerBundle'].SUPPLIER_SITE} " 
                + supplierSiteId,
            "reuseInstance=false",
            null
        };

        RmsUIUtils.executeOperation(OPEN_CONTENT, paramKey1, paramValue1);
    }

    public void onClickDone(ActionEvent actionEvent) {
        RmsUIUtils.findOperation(CLOSE_TAB).execute();
    }
}
