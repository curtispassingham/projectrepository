package com.adeo.merchandising.storeportal.view.supplier.details.bean;

import javax.faces.event.ActionEvent;

import oracle.retail.apps.rms.common.view.cfas.AbstractBackingBean;
import oracle.retail.apps.rms.common.view.util.RmsUIUtils;

public class SupplierDetailsBackingBean extends AbstractBackingBean {
    private static final String CLOSE_TAB = "closeTab";
    
    public SupplierDetailsBackingBean() {
        super();
    }

    public void onClickDone(ActionEvent actionEvent) {
        RmsUIUtils.findOperation(CLOSE_TAB).execute();
    }
}
