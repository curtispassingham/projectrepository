package com.adeo.merchandising.storeportal.view.supplier.search.bean;

import oracle.binding.OperationBinding;

import oracle.retail.apps.rms.common.view.security.accesscontrol.AbstractAccessControlFlowBean;
import oracle.retail.apps.rms.common.view.util.RmsUIUtils;

public class SupplierSearchFlowBean extends AbstractAccessControlFlowBean {
    public SupplierSearchFlowBean() {
        super();
    }
    
    public void initTaskFlow(){
        OperationBinding executeInit =
            RmsUIUtils.getBindings().getOperationBinding("supplierSearchInitTF");
        executeInit.execute();        
    }
}
