package com.adeo.merchandising.storeportal.view.item.details.bean;

import oracle.retail.apps.rms.common.view.security.accesscontrol.AbstractAccessControlBean;

public class ItemDetailsAccessControlBean extends AbstractAccessControlBean {
    public ItemDetailsAccessControlBean() {
        super();
    }

    @Override
    public boolean isVisibile(String componentName) {
        // TODO Implement this method
        return false;
    }

    @Override
    public boolean isRendered(String componentName) {
        // TODO Implement this method
        return false;
    }

    @Override
    public boolean isDisabled(String componentName) {
        // TODO Implement this method
        return false;
    }

    @Override
    public boolean isSelectiveExpressionEnable(String methodName) {
        // TODO Implement this method
        return false;
    }
}
