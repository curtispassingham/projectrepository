package com.adeo.merchandising.storeportal.model.common;

import oracle.jbo.server.ViewDefImpl;

import oracle.retail.apps.rms.common.model.adfbc.RmsViewObjectImpl;

public class SPViewObjectImpl extends RmsViewObjectImpl {
    public SPViewObjectImpl() {
        super();
    }

    public SPViewObjectImpl(String string, ViewDefImpl viewDefImpl) {
        super(string, viewDefImpl);
    }
}
