#! /usr/bin/ksh
# +--------------------------------+-----------------------------------------------+
# | ADEO_batchs_init_status.ksh  | Init status launch batchs
# +--------------------------------+-----------------------------------------------+
# +--------------------------------------------------------------------------------+
#########################################################
# Parameters
#########################################################
if [[ $# -ne 0 ]]
then
        echo "Wrong syntax"
        echo "Command syntax is : "
        echo "    ADEO_batch_init_status.ksh"
        exit  255
fi

#########################################################
# Launch
#########################################################
READY=${BATCH_HOME}/config/sql/xxadeo_ready_job.sql
sqlplus -s ${UP} @${READY}
RETOUR=$?
if [ ${RETOUR} -ne 0 ]
then
        echo "Script ${READY} terminated in error"
        exit 255
fi

exit 0
