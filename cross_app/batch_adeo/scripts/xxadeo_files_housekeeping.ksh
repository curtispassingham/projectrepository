#CP EDIT 9

#!/bin/ksh
#

# $Revision: 0.1
# $Date: 2018/09/27
# $Author: Hassan Kishk
#
# Version              Change                        Author
# 0.1                  Initial Create                Hassan Kishk
#
#########################################################################
#
# File:  xxadeo_files_housekeeping.ksh
# Application Version: none
#
# Syntax: xxadeo_files_housekeeping.ksh control_file
#
# Description:
#   Removes old files based on parameters informed in the control filed passed as parameter to save disk space.
#   parameters; the given control file specifies which directories / files should be removed.
#
#
########################################################################

### PROGRAM DEFINITIONS #######################################################

export PROGRAM_NAME="xxadeo_files_housekeeping"

### INCLUDES ##################################################################

#RETLforXXADEO=<uncomment and set as needed>
##########[[ -z $RETLforXXADEO ]] && RETLforXXADEO=$BATCH_HOME/RETLforXXADEO
##########export RETL_ENV=$RETLforXXADEO

## Environment configuration script
#. ${RETL_ENV}/rfx/etc/rmse_rpas_config.env
##########. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env     # (Adeo ENV - RPAS)

## Script Library Collection
#. ${LIB_DIR}/rmse_rpas_lib.ksh
#. ${LIB_DIR}/rmse_rpas_error_check.ksh
##########. ${LIB_DIR}/xxadeo_rpas_lib.ksh              # (Adeo ENV - RPAS)

### INPUT / OUTPUT DEFINITIONS ################################################

export INBOUND_DIR=${RETL_IN}            # Adeo ENV (Base RPAS Input Dir)
export INBOUND_DIR_CUST=${RETL_IN}      # Adeo ENV (Custom RPAS Input Dir)

export OUTBOUND_DIR=${RETL_OUT}           # Adeo ENV (Base RPAS Output Dir)
export OUTBOUND_DIR_CUST=${RETL_OUT}     # Adeo ENV (Custom RPAS Output Dir)

scriptName=${0##/*/}
numParams=$#

adeoUtilLibFile="${BATCH_HOME}/lib/adeo_util_library.ksh"

echo "lib: ${adeoUtilLibFile}"

if [ -f ${adeoUtilLibFile} ]
then
    . ${adeoUtilLibFile}
else
    echo "Could not find adeo_util_library.ksh, please ensure that this file exists."
    exit 1
fi

#Print the Usage Info based on the input parameter
Usage()
{
    echo "*************************************************************"
    echo "  Script to remove old files specified by given control file "
    echo "  Usage:                                                     "
    echo "*************************************************************"
    echo "  ${scriptName} [controlFileName]                            "
    echo "  ${scriptName} [-h |-help]                                  "
    echo "  -help  - print usage information and exit                  "
    echo "*************************************************************"
}

if [[ $numParams -gt 0 && "$1" = -* ]]
then
    case "$1" in
    # the -help option displays usage
    -[Hh]|-[Hh][Ee][Ll][Pp]) Usage "Display help"
              _clean_exit SUCCESS
                          ;;
                       *)
                          ;;
    esac
fi

#
# Validate the parameters
#

controlFile=$1

#
# Begin main processing
#

#read list of filenames in the control file
processList=`grep -vE '^(#|$)' "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -f1 -d "|"  `

echo "The list of directories/files to be processed is: "
echo "$processList"

#start processing the list of directories/files
if [[ -z ${processList} ]]
then
   echo "No directories/files found in the control file ${BATCH_HOME}/config/filetransfer/${controlFile} to be processed"
   exit 1
else
   #for each directory/file, get all parameters from the control file to prepare for the find / rm command.
   for idFile in $processList; do
       echo "Start processing directory/file $idFile"

       #Set empty to the file base to use parameters set in the new line.
       fileBaseDir=""

       #get base directory from the control file (second parameter)
       baseDir=`grep -w ${idFile} "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -d "|" -f 2 `

       #Verify if source directory variable is empty.
       if [[ -z ${baseDir} ]]
       then
          echo "Base Dir cannot be empty. it must reference an environment variable that exists in the profile"
          handleError
       else
          setBaseDirValue "${baseDir}"
          fileBaseDir=$baseDirValue
       fi

       #get complement directory from the control file (third parameter) to add to the baseDir
       complDir=`grep -w ${idFile} "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -d "|" -f 3`

       if [[ -z ${complDir} ]]
       then
           echo "file base directory is ${fileBaseDir}"
       else
           fileBaseDir=${fileBaseDir}/${complDir}
           echo "file base directory is ${fileBaseDir}"
       fi

       #get file name from the control file (fourth parameter) to be processed
       prcFileName=`grep -w ${idFile} "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -d "|" -f 4`

       #get number of days (fifth parameter) to delete files older than today - this parameter
       daysNumber=`grep -w ${idFile} "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -d "|" -f 5`

       #call function to remove old files

       echo "executing command: find $fileBaseDir -name \"$prcFileName\" -type f -mtime +`expr $daysNumber` -exec rm -f {} \;"
       STATUS=0
       removeOldFiles $fileBaseDir $prcFileName  $daysNumber
       handleReturnCode
   done # end of loop over directories/files
fi

echo "Finished running ${scriptName}"

### REMOVE STATUS FILE #######################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi
echo "******  FINISHING $PROGRAM_NAME: $(date '+%Y/%m/%d-%H:%M:%S')  ******"