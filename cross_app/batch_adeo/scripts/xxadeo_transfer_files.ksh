#!/bin/ksh
#

# $Revision: 0.1
# $Date: 2018/10/16
# $Author: Hassan Kishk
#
# Version              Change                        Author
# 0.1                  Initial Create                Hassan Kishk
#
#########################################################################
#
# File:  xxadeo_transfer_files.ksh
# Application Version: none
#
# Syntax: xxadeo_transfer_files.ksh control_file
#
# Description:
#   Copy/Move files based on parameters informed in the control filed passed as parameter.
#   parameters: the given control file specifies which files should be copied/moved.
#   - For copy files option, only the latest file will be copied. The script can also add
#     ADD_DATE or ADD_DATETIME to the end of the destination filename.
#   - For move files option, all files that match the filename pattern will be moved.
#     to use the ADD_DATE or ADD_DATETIME option in the destinationfilename, the source file
#     name must be an exact match without "*".
#   ********IMPORTANT: The ADD_DATE or ADD_DATETIME feature must be used only with exact
#           patterns without "*"
#
#
########################################################################

### PROGRAM DEFINITIONS #######################################################

export PROGRAM_NAME="xxadeo_transfer_files"

### INCLUDES ##################################################################

#RETLforXXADEO=<uncomment and set as needed>
##########[[ -z $RETLforXXADEO ]] && RETLforXXADEO=$BATCH_HOME/RETLforXXADEO
##########export RETL_ENV=$RETLforXXADEO

## Environment configuration script
#. ${RETL_ENV}/rfx/etc/rmse_rpas_config.env
##########. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env     # (Adeo ENV - RPAS)

## Script Library Collection
#. ${LIB_DIR}/rmse_rpas_lib.ksh
#. ${LIB_DIR}/rmse_rpas_error_check.ksh
##########. ${LIB_DIR}/xxadeo_rpas_lib.ksh              # (Adeo ENV - RPAS)

### INPUT / OUTPUT DEFINITIONS ################################################

export INBOUND_DIR=${RETL_IN}            # Adeo ENV (Base RPAS Input Dir)
export INBOUND_DIR_CUST=${RETL_IN}      # Adeo ENV (Custom RPAS Input Dir)

export OUTBOUND_DIR=${RETL_OUT}           # Adeo ENV (Base RPAS Output Dir)
export OUTBOUND_DIR_CUST=${RETL_OUT}     # Adeo ENV (Custom RPAS Output Dir)

scriptName=${0##/*/}
numParams=$#

adeoUtilLibFile="${BATCH_HOME}/lib/adeo_util_library.ksh"

echo "lib: ${adeoUtilLibFile}"

if [ -f ${adeoUtilLibFile} ]
then
    . ${adeoUtilLibFile}
else
    echo "Could not find adeo_util_library.ksh, please ensure that this file exists."
    exit 1
fi

#Print the Usage Info based on the input parameter
Usage()
{
    echo "*************************************************************"
    echo "  Script to remove old files specified by given control file "
    echo "  Usage:                                                     "
    echo "*************************************************************"
    echo "  ${scriptName} [controlFileName]                            "
    echo "  ${scriptName} [-h |-help]                                  "
    echo "  -help  - print usage information and exit                  "
    echo "*************************************************************"
}

function addDateToName
{
  originalFilename=$1
  dateFormat=$2
  case ${dateFormat} in
  ADD_DATE)
     currentDate=`date +%Y%m%d`
     newFilename=${originalFilename}_${currentDate}
     ;;
  ADD_DATETIME)
     currentDateTime=`date +%Y%m%d%H%M%S`
     newFilename=${originalFilename}_${currentDateTime}
     ;;
  *)
     newFilename=${originalFilename}
     ;;
  esac
}

if [[ $numParams -gt 0 && "$1" = -* ]]
then
    case "$1" in
    # the -help option displays usage
    -[Hh]|-[Hh][Ee][Ll][Pp]) Usage "Display help"
              exit 0
                          ;;
                       *)
                          ;;
    esac
fi

#
# Validate the parameters
#

controlFile=$1
transferId=$2

#
# Begin main processing
#

#read list of filenames in the control file
processList=`grep -vE '^(#|$)' "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -f1 -d "|"  `


#If a parameter was passed with the file id, the script will just process this file.
if [[ -z ${transferId} ]]
then
   echo "The list of files to be processed is: $processList"
else
   #verify if the file parameter exists in the control file.
   processList=`echo $processList | grep -w $transferId`
   if [[ -z ${processList} ]]
   then
      echo "The transfer ID $transferId record was not found in the control file ${BATCH_HOME}/config/filetransfer/${controlFile}"
      handleError
   else
      processList=$transferId
      echo "The file to be processed is: $processList"
   fi
fi


#start processing the list of directories/files
if [[ -z ${processList} ]]
then
   echo "No files found in the control file ${BATCH_HOME}/config/filetransfer/${controlFile} to be processed"
   handleError
else
   #for each file, get all parameters from the control file to prepare for the copy or move command.
   for idFile in $processList; do
       echo "Start processing file $idFile"

       #fileCommand should be cp or mv
       fileCommand=`grep -w ${idFile} "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -d "|" -f 2 `

       #Set empty to the file base to use parameters set in the new line.
       srcFileBaseDir=""
       destFileBaseDir=""

       #get base and dest directory from the control file (second parameter)
       srcBaseDir=`grep -w ${idFile} "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -d "|" -f 3 `
       destBaseDir=`grep -w ${idFile} "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -d "|" -f 6 `

       #Verify if source directory variable is empty.
       if [[ -z ${srcBaseDir} ]]
       then
          echo "Base Dir cannot be empty. it must reference an environment variable that exists in the profile"
          handleError
       else
          setBaseDirValue "${srcBaseDir}"
          srcFileBaseDir=$baseDirValue
       fi

       #Verify if destination directory variable is empty.
       if [[ -z ${destBaseDir} ]]
       then
          echo "Destination Base Dir cannot be empty. it must reference an environment variable that exists in the profile."
          handleError
       else
          setBaseDirValue "${destBaseDir}"
          destFileBaseDir=$baseDirValue
       fi

       #get complement directory from the control file (third parameter) to add to the srcBaseDir
       srcComplDir=`grep -w ${idFile} "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -d "|" -f 4`
       destComplDir=`grep -w ${idFile} "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -d "|" -f 7`

       if [[ -z ${srcComplDir} ]]
       then
           echo "source file base directory is ${srcFileBaseDir}"
       else
           srcFileBaseDir=${srcFileBaseDir}/${srcComplDir}
           echo "source file base directory is ${srcFileBaseDir}"
       fi

       if [[ -z ${destComplDir} ]]
       then
           echo "destination file base directory is ${destFileBaseDir}"
       else
           destFileBaseDir=${destFileBaseDir}/${destComplDir}
           echo "destination file base directory is ${destFileBaseDir}"
       fi

       #get source and destination file name pattern from the control file to be processed
       srcFilename=`grep -w ${idFile} "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -d "|" -f 5`
       destFilename=`grep -w ${idFile} "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -d "|" -f 8`
       destDateFormat=`grep -w ${idFile} "${BATCH_HOME}/config/filetransfer/${controlFile}" | cut -d "|" -f 9`

       #if [[ -z ${destFilename} ]]
       #then
       #    echo "destination file name is empty, it will be the same as the source filename."
       #else
       #    echo "destination file name is ${destFilename}"
       #fi

       STATUS=0

       #If the command is to copy, then the file will be copied to a temporary filename before it is renamed, to avoid being processed before it is completelly copied.
       if [[ $fileCommand == 'cp' ]]
       then
          #Check if the source file exists, if more than one file with the same name pattern exist, get the latest one.
          lastSourceFile=`find $srcFileBaseDir -maxdepth 1 -name $srcFilename | sort | tail -n 1`
          if [[ -f $lastSourceFile ]]
          then
             #get the complete path of the most recently updated file that matches the pattern
             lastSourceFileName=`basename $lastSourceFile`
             if [[ -z ${destFilename} ]]
             then
                destFilename=$lastSourceFileName
             else
                #use the filename set in the control file as destination file name
                if [[ ! -z ${destDateFormat} ]] then
                   currentDateExp=`date ${destDateFormat}`
                   destFilename=${destFilename}${currentDateExp}
                fi
                #if [[ ${destFilename} = 'ADD_DATE' || ${destFilename} = 'ADD_DATETIME' ]]
                #then
                #   addDateToName ${srcFilename} ${destFilename}
                #   destFilename=${newFilename}
                #fi
                echo "destination file name is ${destFilename}"
             fi

             #copy to a temporary name
             echo "executing command: cp $lastSourceFile $destFileBaseDir/tmp_${destFilename}_tmp "
             cp $lastSourceFile $destFileBaseDir/tmp_${destFilename}_tmp
             handleReturnCode
             #rename destination file
             echo "executing command: mv $destFileBaseDir/tmp_${destFilename}_tmp $destFileBaseDir/$destFilename "
             mv $destFileBaseDir/tmp_${destFilename}_tmp $destFileBaseDir/${destFilename}
          else
             echo "The source file $srcFileBaseDir/$srcFilename doesn't exist"
             handleError
          fi
       #If the command is to move, then it will be directly moved to the destination filename.
       else
          if [[ -z ${destFilename} ]]
          then
             echo "destination file name is empty, it will be the same as the source filename."
          else
             #if [[ ${destFilename} = 'ADD_DATE' || ${destFilename} = 'ADD_DATETIME' ]]
             #then
             #   addDateToName ${srcFilename} ${destFilename}
             #   destFilename=${newFilename}
             #fi
             #use the filename set in the control file as destination file name
             if [[ ! -z ${destDateFormat} ]] then
                currentDateExp=`date ${destDateFormat}`
                destFilename=${destFilename}${currentDateExp}
             fi
             echo "destination file name is ${destFilename}"
          fi
          echo "executing command: $fileCommand $srcFileBaseDir/$srcFilename $destFileBaseDir/$destFilename"
          $fileCommand $srcFileBaseDir/$srcFilename $destFileBaseDir/$destFilename
       fi
   done # end of loop over files
fi

### REMOVE STATUS FILE #######################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi
echo "******  FINISHING $PROGRAM_NAME: $(date '+%Y/%m/%d-%H:%M:%S')  ******"
