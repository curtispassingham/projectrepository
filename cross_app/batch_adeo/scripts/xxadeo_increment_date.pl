#!/usr/bin/perl

my $days = $ARGV[0] * 24;

use strict;
use warnings;

use POSIX qw(strftime);

my $today = time;
my $new_day = $today + $days * 60 * 60;
my $formatted = strftime "%Y%m%d", ( localtime($new_day) );
print "$formatted\n";
