#!/bin/ksh
#

# $Revision: 0.1
# $Date: 2019/02/06
# $Author: Hassan Kishk
#
# Version              Change                        Author
# 0.1                  Initial Create                Hassan Kishk
#
#########################################################################
#
# File:  xxadeo_proc_wrapper.ksh
# Application Version: none
#
# Syntax: xxadeo_proc_wrapper.ksh ProC_ID
#
# Description:
#   This script is a wrapper script to call ProC, the main reason to have this script
#   is to force the output files to be generated in the right directories.
#   parameters: the given ProC ID will identify the ProC to be executed.
#
#
########################################################################

### PROGRAM DEFINITIONS #######################################################

export PROGRAM_NAME="xxadeo_proc_wrapper"

### INCLUDES ##################################################################

export OUTBOUND_DIR=${RETL_OUT}/ESB           # Adeo ENV (Main ProC Output Dir)
export PROC_DIR=${RETAIL_HOME}/oracle/proc/bin

scriptName=${0##/*/}
numParams=$#

adeoUtilLibFile="${BATCH_HOME}/lib/adeo_util_library.ksh"

echo "lib: ${adeoUtilLibFile}"

if [ -f ${adeoUtilLibFile} ]
then
    . ${adeoUtilLibFile}
else
    echo "Could not find adeo_util_library.ksh, please ensure that this file exists."
    exit 1
fi

#Print the Usage Info based on the input parameter
Usage()
{
    echo "*************************************************************"
    echo "  Script to call ProC scripts                                "
    echo "  Usage:                                                     "
    echo "*************************************************************"
    echo "  ${scriptName} [controlFileName]                            "
    echo "  ${scriptName} [-h |-help]                                  "
    echo "  -help  - print usage information and exit                  "
    echo "*************************************************************"
}

if [[ $numParams -gt 0 && "$1" = -* ]]
then
    case "$1" in
    # the -help option displays usage
    -[Hh]|-[Hh][Ee][Ll][Pp]) Usage "Display help"
              exit 0
                          ;;
                       *)
                          ;;
    esac
fi

#
# Validate the parameters
#

procID=$1

#
# Begin main processing
#

STATUS=0

case ${procID} in
   xxadeo_costprice_dnld|xxadeo_deals_dnld|xxadeo_supsites_dnld|xxadeo_merchhier_dnld|xxadeo_ecotax_dnld)
      echo "Moving to the output directory ${OUTBOUND_DIR} before executing the ProC"
	  cd ${OUTBOUND_DIR}
	  handleReturnCode
	
      echo "Executing the ProC ${procID}"
	  . ${PROC_DIR}/${procID} $UP
	  handleReturnCode
	  ;;
   xxadeo_pricechange_dnld_regular)
      echo "Moving to the output directory ${OUTBOUND_DIR} before executing the ProC"
	  cd ${OUTBOUND_DIR}
	  handleReturnCode
	
      echo "Executing the ProC ${procID}"
	  . ${PROC_DIR}/${procID} $UP PRIXVENTEREGULIERS
	  handleReturnCode
	  ;;
   *)
	  echo "Unknown ProC ${procID}"
	  handleError
	  ;;
esac

echo "******  FINISHING $PROGRAM_NAME: $(date '+%Y/%m/%d-%H:%M:%S')  ******"
