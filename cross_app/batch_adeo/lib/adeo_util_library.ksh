#CP EDIT 9

#!/bin/ksh
#
# $Revision: 0.1
# $Date: 2018/09A27/
# $Author: Hassan Kishk
#
# Version              Change                        Author
# 0.1                  Initial Create                Hassan Kishk
#
##############################################################################
# adeo_util_library.ksh
#
# Description:
#   This script contains functions for the adeo scripts.
#
##############################################################################

#
# ADEO function to manage return code
#

function handleError {
    STATUS=1
    exit 1
}

function handleSuccess {
    STATUS=0
}

function handleReturnCode
{
  if [ $? -gt 0 ]; then
    MSG_ERROR_SUCCESS="ERROR"
    STATUS=1
    exit 1
  fi

  if [[ $STATUS -eq 0 ]] ; then
    MSG_ERROR_SUCCESS=""
    STATUS=0
  fi
}

#
# Remove old files to save disk space
#

function removeOldFiles {

   # Set the variables fileBaseDir, prcFileName" and daysNumber before calling this function.
   find $fileBaseDir -name "$prcFileName" -type f -mtime +`expr $daysNumber` -exec rm -f {} \;
}

function setBaseDirValue {
  baseDirVar=$1
  baseDirValue=""
  case ${baseDirVar} in
  EXTERNAL_INBOUND_DIR)
     baseDirValue=${EXTERNAL_INBOUND_DIR}
     ;;
  EXTERNAL_OUTBOUND_DIR)
     baseDirValue=${EXTERNAL_OUTBOUND_DIR}
     ;;
  RETL_IN)
     baseDirValue=${RETL_IN}
     ;;
  RETL_OUT)
     baseDirValue=${RETL_OUT}
     ;;
  INTEGRATION_HOME)
     baseDirValue=${INTEGRATION_HOME}
     ;;
  LEGACY_IN)
     baseDirValue=${LEGACY_IN}
     ;;
  LEGACY_OUT)
     baseDirValue=${LEGACY_OUT}
     ;;
  PROCESSED_IN)
     baseDirValue=${PROCESSED_IN}
     ;;
  PROCESSED_OUT)
     baseDirValue=${PROCESSED_OUT}
     ;;
  O_LOG_HOME)
     baseDirValue=${O_LOG_HOME}
     ;;
  RMS_RPAS_HOME)
     baseDirValue=${RMS_RPAS_HOME}
     ;;
  RETLforXXADEO)
     baseDirValue=${RETLforXXADEO}
     ;;
  GLOBAL_DOMAIN)
     baseDirValue=${GLOBAL_DOMAIN}
     ;;
  *)
     baseDirValue=""
     ;;
  esac
  #return $baseDirValue
}