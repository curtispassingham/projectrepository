# 1 : identifier
# 2 : command to execute cp or mv
# 3 : source directory
# 4 : source subdirectory
# 5 : filename pattern
# 6 : destination directory
# 7 : destination subdirectory
# 8 : destination filename pattern (if different).
# 9 : destination timeformat if necessary, for example: +%Y%m%d_%H%M%S or +%Y%m%d

#incoming files
CTM_STP_CLUSTER_LIST|mv|RETL_IN|STOREPORTAL|XXADEO_ASSRT_CLUSTERS*.dat|PROCESSED_IN|||
CTM_STP_ASST_VAL_TL|mv|RETL_IN|STOREPORTAL|XXADEO_ASSORTMENT_VALUE_TL*.dat|PROCESSED_IN|||
CTM_STP_ASST_MODE_TL|mv|RETL_IN|STOREPORTAL|XXADEO_ASSORTMENT_MODE_TL*.dat|PROCESSED_IN|||
CTM_STP_ITEM_BU|mv|RETL_IN|STOREPORTAL|XXADEO_ITEM_BU_*.dat|PROCESSED_IN|||
CTM_STP_ASST_CLUSTER|mv|RETL_IN|STOREPORTAL|XXADEO_ASSORTMENT_CLUSTER_*.dat|PROCESSED_IN|||
CTM_STP_STR_CLST_LINK|mv|RETL_IN|STOREPORTAL|XXADEO_STORE_CLUSTER_*.dat|PROCESSED_IN|||
CTM_STP_ASST_STORE|mv|RETL_IN|STOREPORTAL|XXADEO_ASSORTMENT_STORE_*.dat|PROCESSED_IN|||

#outgoing files
STP_CTM_CHOIX_MAGASIN|mv|RETL_OUT|CATMAN|XXADEO_ASSORTMENT*.dat|PROCESSED_OUT|||
