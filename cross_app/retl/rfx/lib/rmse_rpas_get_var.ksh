
function rmse_get_var  {

  ##########################################################################
  #
  #  Function rmse_get_var gets the value for an environment variable
  #  from the given ".txt" file located in the $RFX_HOME/rfx/etc
  #  directory. This function is normally called from rmse_rpas_config.env.
  #
  #  CAUTION: Function rmse_get_var can only be used after LOG_FILE has
  #  been defined.
  #
  #  If the user-supplied filename is the name of a file that does
  #  not exist in the etc directory or if the file is empty, the
  #  word "ERROR" will be returned instead of the desired variable
  #  value.
  #
  #  Usage: VAR=$(rmse_get_var  txt_file_name)
  #
  #  Example usage:
  # 
  #      export NEXT_VDATE=$(rmse_get_var  next_vdate.txt)
  #      if [ "$NEXT_VDATE" = "ERROR" ]; then  exit 1; fi
  #
  ##########################################################################

  if [ -s  $ETC_DIR/$1 ]; then
     echo $(cat $ETC_DIR/$1)
  else
     echo "$PROGRAM_NAME "`date +"%T"`        \
          ": $1 doesn't exist under $ETC_DIR"  >> $LOG_FILE
     echo ERROR
  fi

  return

}

