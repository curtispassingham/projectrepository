#! /bin/ksh
#----------------------------------------------------------------------------------------#
#  File   : xxadeo_ld_iindfiles.ksh                                                      #
#  Desc   : This batch will be executed for load of s9t tables with custom data.         #
#  Company: ADEO                                                                         #
#  Created: Elsa Barros                                                                  #
#  Date   : 23-11-2018                                                                   #
#----------------------------------------------------------------------------------------#
# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='xxadeo_ld_iindfiles.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

statusDir="${MMHOME}/log"
badDir="${MMHOME}/log"
dscDir="${MMHOME}/log"
logDir="${MMHOME}/log"
logFile="${logDir}/${exeDate}.log"
ERRORFILE="${MMHOME}/error/err.${pgmName}.${exeDate}"

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <input directory> &

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.
   <input directory>  Directory where the .ods,.ctl and .data files are kept.
   
"
}

function CHECK_FILE_EXIST
{
   inputFile1=$inputDir/xxadeo_template_config.ods
   inputFile2=$inputDir/ODS_SYSTEM_TEMPLATE_FOR_OUTPUT_FILES.ods
   # Check if ODS_SYSTEM_TEMPLATE_FOR_OUTPUT_FILES and TEMPLATE_CONFIG files exist
   if [[ -f $inputFile1 && -r $inputFile1 ]] && [[ -f $inputFile2 && -r $inputFile2 ]]; then
     :
   else
      LOG_ERROR "Template files does not exist" "CHECK_FILE_EXIST" ${FATAL} ${ERRORFILE} ${logFile} ${pgmName}
      exit ${FATAL}
   fi
   return ${OK}
}
function PROCESS_ODS_FILES
{
   ./loadods.ksh $CONNECT $inputFile1
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "Unable to run loadods script for input file ${inputFile1}" "LOADODS" ${FATAL} ${ERRORFILE} ${logFile} ${pgmName}
      exit ${FATAL}
   fi
   ./loadods.ksh $CONNECT $inputFile2
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "Unable to run loadods script for input file ${inputFile2}" "LOADODS" ${FATAL} ${ERRORFILE} ${logFile} ${pgmName}
      exit ${FATAL}
   fi
}
function DELETE_TEMPLATE_TABLES
{
   echo "set feedback off;
   set heading off;
   set long 100000
   BEGIN
      delete from SVC_TMPL_API_MAP WHERE TEMPLATE_KEY like 'XXADEO%';
      delete from S9T_TMPL_COLS_DEF_TL WHERE TEMPLATE_KEY like 'XXADEO%';
      delete from S9T_TMPL_COLS_DEF WHERE TEMPLATE_KEY like 'XXADEO%';
      delete from S9T_TMPL_WKSHT_DEF_TL WHERE TEMPLATE_KEY like 'XXADEO%';
      delete from S9T_TMPL_WKSHT_DEF WHERE TEMPLATE_KEY like 'XXADEO%';
      delete from S9T_TEMPLATE_TL WHERE TEMPLATE_KEY like 'XXADEO%';
      delete from S9T_TEMPLATE WHERE TEMPLATE_KEY like 'XXADEO%';
   END;
/" | sqlplus -s $CONNECT >> $logDir/delete_template_tables.log
}
function UPDATE_S9T_TEMPLATE
{
   echo "set feedback off;
   set heading off;
   set long 100000
   BEGIN
      update s9t_template set template_type = NULL where template_key = 'ITEM_MASTER_DATA';
   END;
/" | sqlplus -s $CONNECT >> $logDir/update_s9t_template.log
}
function LOAD_S9T_TEMPLATE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   CTLFILE="${inputDir}/s9t_template.ctl"
   sqlldrLog=${logDir}/s9t_template.${dtStamp}.log
   sqlldrDsc=${logDir}/s9t_template.dsc
   sqlldrBad=${logDir}/s9t_template.bad
   inputFile="${inputDir}/xxadeo_s9t_template.dat"
   
   sqlldr userid=${CONNECT} silent=feedback,header control=${CTLFILE} log=${sqlldrLog} data=${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_S9T_TEMPLATE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
      return ${FATAL}
   else
      LOG_MESSAGE "Completed loading file to S9T_TEMPLATE table" "LOAD_S9T_TEMPLATE" "${OK}" "${logFile}" "${pgmName}" 
      return ${OK}
   fi
}
function LOAD_S9T_TEMPLATE_TL
{
   inputFile="${inputDir}/xxadeo_s9t_template_tl.dat" 
   dtStamp=`date +"%G%m%d%H%M%S"`
   CTLFILE="${inputDir}/s9t_template_tl.ctl"
   sqlldrLog=${logDir}/s9t_template_tl.${dtStamp}.log
   sqlldrDsc=${logDir}/s9t_template_tl.dsc
   sqlldrBad=${logDir}/s9t_template_tl.bad

   sqlldr userid=${CONNECT} silent=feedback,header control=${CTLFILE} log=${sqlldrLog} data=${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_S9T_TEMPLATE_TL" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
      return ${FATAL}
   else
         LOG_MESSAGE "Completed loading file to S9T_TEMPLATE_TL table" "LOAD_S9T_TEMPLATE_TL" "${OK}" "${logFile}" "${pgmName}" 
         return ${OK}
   fi
}
function LOAD_S9T_TMPL_WKSHT_DEF
{
   inputFile="${inputDir}/xxadeo_s9t_tmpl_wksht_def.dat" 
   dtStamp=`date +"%G%m%d%H%M%S"`
   CTLFILE="${inputDir}/s9t_tmpl_wksht_def.ctl"
   sqlldrLog=${logDir}/s9t_tmpl_wksht_def.${dtStamp}.log
   sqlldrDsc=${logDir}/s9t_tmpl_wksht_def.dsc
   sqlldrBad=${logDir}/s9t_tmpl_wksht_def.bad

   sqlldr userid=${CONNECT} silent=feedback,header control=${CTLFILE} log=${sqlldrLog} data=${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_S9T_TMPL_WKSHT_DEF" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
      return ${FATAL}
   else
         LOG_MESSAGE "Completed loading file to S9T_TMPL_WKSHT_DEF table" "LOAD_S9T_TMPL_WKSHT_DEF" "${OK}" "${logFile}" "${pgmName}" 
         return ${OK}
   fi
}

function LOAD_S9T_TMPL_WKSHT_DEF_TL
{
   inputFile="${inputDir}/xxadeo_s9t_tmpl_wksht_def_tl.dat" 
   dtStamp=`date +"%G%m%d%H%M%S"`
   CTLFILE="${inputDir}/s9t_tmpl_wksht_def_tl.ctl"
   sqlldrLog=${logDir}/s9t_tmpl_wksht_def_tl.${dtStamp}.log
   sqlldrDsc=${logDir}/s9t_tmpl_wksht_def_tl.dsc
   sqlldrBad=${logDir}/s9t_tmpl_wksht_def_tl.bad
  
   sqlldr userid=${CONNECT} silent=feedback,header control=${CTLFILE} log=${sqlldrLog} data=${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_S9T_TMPL_WKSHT_DEF_tl" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
      return ${FATAL}
   else
         LOG_MESSAGE "Completed loading file to S9T_TMPL_WKSHT_DEF_tl table" "LOAD_S9T_TMPL_WKSHT_DEF_tl" "${OK}" "${logFile}" "${pgmName}" 
         return ${OK}
   fi
}
function LOAD_S9T_TMPL_COLS_DEF
{
   inputFile="${inputDir}/xxadeo_s9t_tmpl_cols_def.dat" 
   dtStamp=`date +"%G%m%d%H%M%S"`
   CTLFILE="${inputDir}/s9t_tmpl_cols_def.ctl"
   sqlldrLog=${logDir}/s9t_tmpl_cols_def.${dtStamp}.log
   sqlldrDsc=${logDir}/s9t_tmpl_cols_def.dsc
   sqlldrBad=${logDir}/s9t_tmpl_cols_def.bad

   sqlldr userid=${CONNECT} silent=feedback,header control=${CTLFILE} log=${sqlldrLog} data=${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_S9T_TMPL_COLS_DEF" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
      return ${FATAL}
   else
         LOG_MESSAGE "Completed loading file to S9T_TMPL_COLS_DEF table" "LOAD_S9T_TMPL_COLS_DEF" "${OK}" "${logFile}" "${pgmName}" 
         return ${OK}
   fi
}
function LOAD_S9T_TMPL_COLS_DEF_TL
{
   inputFile="${inputDir}/xxadeo_s9t_tmpl_cols_def_tl.dat" 
   dtStamp=`date +"%G%m%d%H%M%S"`
   CTLFILE="${inputDir}/s9t_tmpl_cols_def_tl.ctl"
   sqlldrLog=${logDir}/s9t_tmpl_cols_def_tl.${dtStamp}.log
   sqlldrDsc=${logDir}/s9t_tmpl_cols_def_tl.dsc
   sqlldrBad=${logDir}/s9t_tmpl_cols_def_tl.bad
   
   sqlldr userid=${CONNECT} silent=feedback,header control=${CTLFILE} log=${sqlldrLog} data=${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_S9T_TMPL_COLS_DEF_TL" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
      return ${FATAL}
   else
         LOG_MESSAGE "Completed loading file to S9T_TMPL_COLS_DEF_TL table" "LOAD_S9T_TMPL_COLS_DEF_TL" "${OK}" "${logFile}" "${pgmName}" 
         return ${OK}
   fi
}
function LOAD_SVC_TMPL_API_MAP
{
   inputFile="${inputDir}/xxadeo_svc_tmpl_api_map.dat" 
   dtStamp=`date +"%G%m%d%H%M%S"`
   CTLFILE="${inputDir}/svc_tmpl_api_map.ctl"
   sqlldrLog=${logDir}/svc_tmpl_api_map.${dtStamp}.log
   sqlldrDsc=${logDir}/svc_tmpl_api_map.dsc
   sqlldrBad=${logDir}/svc_tmpl_api_map.bad
  
   sqlldr userid=${CONNECT} silent=feedback,header control=${CTLFILE} log=${sqlldrLog} data=${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_SVC_TMPL_API_MAP" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
      return ${FATAL}
   else
         LOG_MESSAGE "Completed loading file to SVC_TMPL_API_MAP table" "LOAD_SVC_TMPL_API_MAP" "${OK}" "${logFile}" "${pgmName}" 
         return ${OK}
   fi
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------

#Execution of configuration file to set env variable.
STATUS=$?

if [ $STATUS != 0 ]; then
   LOG_ERROR  "Configuration error"
   LOG_ERROR "Configuration error" >> $logFile
   exit 1
fi

LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" 

# Test for the number of input arguments
if [ $# -lt 2 ]
then
   USAGE
   exit 1
fi

#Validate input parameters
CONNECT=$1
inputDir=$2
if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${logFile} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "${pgmName}.ksh - Started by ${USER}" "" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

#Delete the template tables

CHECK_FILE_EXIST
PROCESS_ODS_FILES
DELETE_TEMPLATE_TABLES

#Load S9T_TEMPLATE file
LOAD_S9T_TEMPLATE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
  LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_S9T_TEMPLATE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
   exit ${FATAL}
fi
# Load S9T_TEMPLATE_TL file
LOAD_S9T_TEMPLATE_TL
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_S9T_TEMPLATE_TL" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
   exit ${FATAL}
fi
# Load S9T_TMPL_WKSHT_DEF file
LOAD_S9T_TMPL_WKSHT_DEF
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_S9T_TMPL_WKSHT_DEF" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
   exit ${FATAL}
fi
# Load S9T_TMPL_WKSHT_DEF_TL file
LOAD_S9T_TMPL_WKSHT_DEF_TL
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_S9T_TMPL_WKSHT_DEF_TL" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
   exit ${FATAL}
fi
# Load S9T_TMPL_COLS_DEF file
LOAD_S9T_TMPL_COLS_DEF
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_S9T_TMPL_COLS_DEF" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
   exit ${FATAL}
fi
# Load S9T_TMPL_COLS_DEF_TL file
LOAD_S9T_TMPL_COLS_DEF_TL
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_S9T_TMPL_COLS_DEF" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
   exit ${FATAL}
fi
# Load SVC_TMPL_API_MAP file
LOAD_SVC_TMPL_API_MAP
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_SVC_TMPL_API_MAP" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" 
   exit ${FATAL}
fi

UPDATE_S9T_TEMPLATE

echo "Job ${pgmName}.ksh completed successfully by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" 
