
package com.adeo.common.model.dbwrappers;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.jpub.runtime.MutableArray;
import oracle.sql.Datum;
import java.sql.SQLException;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.retail.apps.framework.jdbc.DbSchemaResolverFactory;
import oracle.retail.apps.framework.jdbc.OracleTypeAdapter;
/*******************************************************
 * THIS CODE IS AUTO-GENERATED.  DO NOT MODIFY THIS CODE!
 * 
 * Modify the Oracle Type this class was based on and 
 * re-generate the contents of this file using the oracle.retail.apps.framework.jdbc.OracleTypeAdapterGenerator application. 
 * 
 * Generator Version : 1.0
 * Generation Date   : 2018-06-27 11:48:024
 * Oracle Type Name  : XXADEO_ITEM_RESULT_TBL
 * Owned by          : XXADEO_RMS
 * Requested Schema  : XXADEO_RMS
 * Requested DB URL  : jdbc:oracle:thin:@onsdb04:1521/rms16adeo
 * Requested DB User : xxadeo_rms
 *******************************************************/
public class XxadeoItemResultTbl implements ORAData, ORADataFactory, OracleTypeAdapter{
    public static final String _SQL_NAME = "XXADEO_ITEM_RESULT_TBL";
    public static final int _SQL_TYPECODE = OracleTypes.ARRAY;
    MutableArray  _array;
    private static final XxadeoItemResultTbl _XxadeoItemResultTblFactory = new XxadeoItemResultTbl();
    public static ORADataFactory getORADataFactory() {
        return _XxadeoItemResultTblFactory;
    }
    public XxadeoItemResultTbl() {
        this((XxadeoItemResultObj[])null);
    }
    public XxadeoItemResultTbl(XxadeoItemResultObj[] a) {
        XxadeoItemResultObj[] aclone =  null;
        if (a != null) {
            aclone = a.clone();
        }
        _array = new MutableArray(2002, aclone, XxadeoItemResultObj.getORADataFactory());
    }

    public Datum toDatum(Connection c) throws SQLException {
        return _array.toDatum(c, getSQLTypeName());  
    }
    public int length() throws SQLException {
        return _array.length();
    }

    public int getBaseType() throws SQLException {
        return _array.getBaseType();
    }

    public String getBaseTypeName() throws SQLException {
        return _array.getBaseTypeName();
    }

    public ArrayDescriptor getDescriptor() throws SQLException {
        return _array.getDescriptor();
    }

    public ORAData create(Datum d, int sqlType) throws SQLException {
        if (d == null)
            return null;
        XxadeoItemResultTbl a = new XxadeoItemResultTbl();
        a._array = new MutableArray(2002, (ARRAY)d, XxadeoItemResultObj.getORADataFactory());
        return a;
    }

    public XxadeoItemResultObj[] getArray() throws SQLException {
        return (XxadeoItemResultObj[])_array.getObjectArray(new XxadeoItemResultObj[_array.length()]);
    }

    public XxadeoItemResultObj[] getArray(long index, int count) throws SQLException {
        return (XxadeoItemResultObj[])_array.getObjectArray(index, new XxadeoItemResultObj[_array.sliceLength(index,count)]);
    }

    public void setArray(XxadeoItemResultObj[] a) throws SQLException {
        _array.setObjectArray(a);
    }

    public void setArray(XxadeoItemResultObj[] a, long index) throws SQLException {
        _array.setObjectArray(a, index);
    }

    public XxadeoItemResultObj getElement(long index) throws SQLException {
        return (XxadeoItemResultObj)_array.getObjectElement(index);
    }

    public void setElement(XxadeoItemResultObj a, long index) throws SQLException {
        _array.setObjectElement(a, index);
    }

    public static String getSQLTypeName() {    return DbSchemaResolverFactory.getResolver().resolveObjectName(_SQL_NAME);} 
    public static int getSQLTypeCode() {    return _SQL_TYPECODE;} 
} // end class
