/*
* Fábio Andersen
*
* Copyright (c) 2018 Oracle Development Center. All Rights Reserved.
*
* This software is the confidential and proprietary information of Oracle Development Center.
* ("Confidential Information").
*
*/
package com.adeo.common.model;

import oracle.adf.share.logging.ADFLogger;

/**
 * Generate Java classes to map database objects.
 */
public class GenerateJavaWrappersRmsDashBoards {
    // class
    private static final Class CLASS = GenerateJavaWrappersRmsDashBoards.class;
    private static final String CLASS_NAME = CLASS.getName();
    // log
    private static final ADFLogger LOG = ADFLogger.createADFLogger(GenerateJavaWrappersRmsDashBoards.class);

    /**
     * Empty constructor.
     */
    private GenerateJavaWrappersRmsDashBoards() {
    }

    /**
     * Main method.
     *
     * @param args Arguments list.
     */
    public static void main(String[] args) {
        LOG.entering(CLASS_NAME, "main");
        String jdbcUrl = "jdbc:oracle:thin:@onsdb04:1521/rms16adeo";
        String jdbcUser = "xxadeo_rms";
        String jdbcPassword = "oracle";
        String schemaOwner = "xxadeo_rms";
        String plsqlSourceJavaPackage = "com.adeo.common.model.dbwrappers";
        String slfPlsqlSourceNames = "XXADEO_DASHBOARD_ITEMS_SQL";
        String noTypeGenOutput = "no";
        RmsDashboardsJavaWrappersUtil.generateDatabaseObjects(jdbcUrl, jdbcUser, jdbcPassword, schemaOwner,
                                                    slfPlsqlSourceNames, plsqlSourceJavaPackage,
                                                    noTypeGenOutput);
        LOG.exiting(CLASS_NAME, "main");
    }
}
