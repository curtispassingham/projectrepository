/*
* Fábio Andersen
*
* Copyright (c) 2018 Oracle Development Center. All Rights Reserved.
*
* This software is the confidential and proprietary information of Oracle Development Center.
* ("Confidential Information").
*
*/
package com.adeo.common.model;

import java.util.Properties;

import oracle.retail.apps.framework.tools.plsqljavawrappergenerator.GeneratorRequest;
import oracle.retail.apps.framework.tools.plsqljavawrappergenerator.PlsqlJavaWrapperGenerator;


/**
 * Java Wrappers Model utils.
 */
public class RmsDashboardsJavaWrappersUtil {
    private static final String JDBC_URL = "jdbcUrl";
    private static final String JDBC_USER = "jdbcUser";
    private static final String JDBC_PASS = "jdbcPassword";
    private static final String SCHEMA_OWNER = "schemaOwner";
    private static final String PLSQL_SOURCE_NAMES = "plsqlSourceNames";
    private static final String PLSQL_SOURCE_JAVA_PACKAGE = "plsqlSourceJavaPackage";
    private static final String NO_TYPE_GEN_OUTPUT = "noTypeGenOutput";
    private static final String USER_DIR = "user.dir";
    private static final String OUTPUT_DIRECTORY = "outputDirectory";
    private static final String SRC = "src";
    private static final String BACKSLASH = "\\";

    /**
     * Empty constructor.
     */
    private RmsDashboardsJavaWrappersUtil() {
    }

    /**
     * Connect to database and generate schema objects.
     *
     * @param jdbcUrl JDBC URL.
     * @param jdbcUser JDBC user name.
     * @param jdbcPassword JDBC password.
     * @param schemaOwner Database schema owner.
     * @param plsqlSourceNames Database object names to be genereated.
     * @param plsqlSourceJavaPackage Destination package to generated code.
     * @param noTypeGenOutput Indicates if database type objects are generated or not.
     */
    public static void generateDatabaseObjects(String jdbcUrl, String jdbcUser, String jdbcPassword,
                                               String schemaOwner, String plsqlSourceNames,
                                               String plsqlSourceJavaPackage,
                                               String noTypeGenOutput) {
        Properties properties =
            generateProperties(jdbcUrl, jdbcUser, jdbcPassword, schemaOwner, plsqlSourceNames,
                               plsqlSourceJavaPackage, noTypeGenOutput);
        properties.put(OUTPUT_DIRECTORY, System.getProperty(USER_DIR) + BACKSLASH + SRC);
        GeneratorRequest request = GeneratorRequest.loadFromProperties(properties);
        PlsqlJavaWrapperGenerator generator = new PlsqlJavaWrapperGenerator(request);
        generator.execute();
    }

    /**
     * Create properties file to connect to database.
     *
     * @param jdbcUrl JDBC URL.
     * @param jdbcUser JDBC user name.
     * @param jdbcPassword JDBC password.
     * @param schemaOwner Database schema owner.
     * @param plsqlSourceNames Database object names to be genereated.
     * @param plsqlSourceJavaPackage Destination package to generated code.
     * @param noTypeGenOutput Indicates if database type objects are generated or not.
     * @return Properties file.
     */
    private static Properties generateProperties(String jdbcUrl, String jdbcUser,
                                                 String jdbcPassword, String schemaOwner,
                                                 String plsqlSourceNames,
                                                 String plsqlSourceJavaPackage,
                                                 String noTypeGenOutput) {
        Properties properties = System.getProperties();
        properties.put(JDBC_URL, jdbcUrl);
        properties.put(JDBC_USER, jdbcUser);
        properties.put(JDBC_PASS, jdbcPassword);
        properties.put(SCHEMA_OWNER, schemaOwner.toUpperCase());
        properties.put(PLSQL_SOURCE_NAMES, plsqlSourceNames);
        properties.put(PLSQL_SOURCE_JAVA_PACKAGE, plsqlSourceJavaPackage);
        properties.put(NO_TYPE_GEN_OUTPUT, noTypeGenOutput);

        return properties;
    }
    }
