
package com.adeo.common.model.dbwrappers;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.jpub.runtime.MutableStruct;
import oracle.sql.Datum;
import java.sql.SQLException;
import oracle.sql.STRUCT;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.retail.apps.framework.jdbc.DbSchemaResolverFactory;
import oracle.retail.apps.framework.jdbc.OracleTypeAdapter;
/*******************************************************
 * THIS CODE IS AUTO-GENERATED.  DO NOT MODIFY THIS CODE!
 * 
 * Modify the Oracle Type this class was based on and 
 * re-generate the contents of this file using the oracle.retail.apps.framework.jdbc.OracleTypeAdapterGenerator application. 
 * 
 * Generator Version : 1.0
 * Generation Date   : 2018-06-27 11:48:024
 * Oracle Type Name  : XXADEO_DASHBOARD_ITEM_OBJ
 * Owned by          : XXADEO_RMS
 * Requested Schema  : XXADEO_RMS
 * Requested DB URL  : jdbc:oracle:thin:@onsdb04:1521/rms16adeo
 * Requested DB User : xxadeo_rms
 *******************************************************/
public class XxadeoDashboardItemObj implements ORAData, ORADataFactory, OracleTypeAdapter {
    public static final String _SQL_NAME = "XXADEO_DASHBOARD_ITEM_OBJ";

    public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

    public static final int UDACFA_ID_IDX = 0;
    public static final int UDACFA_VALUE_IDX = 1;
    public static final int UDACFA_TYPE_IDX = 2;
    public static final int UDACFA_FLAG_IDX = 3;
    protected MutableStruct _struct;
    protected static int[] _sqlType = {2,12,12,12};
    protected static ORADataFactory[] _factory = new ORADataFactory[4];
    static {
    }
    protected static final XxadeoDashboardItemObj _XxadeoDashboardItemObjFactory = new XxadeoDashboardItemObj();
    public static ORADataFactory getORADataFactory() {
        return _XxadeoDashboardItemObjFactory;
    }

    public XxadeoDashboardItemObj() {
        _struct = new MutableStruct(new Object[4], _sqlType, _factory);
    }

    public Datum toDatum(Connection c) throws SQLException {
        return _struct.toDatum(c, getSQLTypeName()); 
    }

    public ORAData create(Datum d, int sqlType) throws SQLException {
        return create(null, d, sqlType);
    }

    protected ORAData create(XxadeoDashboardItemObj o, Datum d, int sqlType) throws SQLException {
        if (d == null)
            return null;
        if (o == null)
            o = new XxadeoDashboardItemObj();
        o._struct = new MutableStruct((STRUCT)d, _sqlType, _factory);
        return o;
    }
    public final java.math.BigDecimal  getUdacfaId() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(UDACFA_ID_IDX);
    }

    public final void setUdacfaId(java.math.BigDecimal udacfaId) throws SQLException 
    { 
        _struct.setAttribute(UDACFA_ID_IDX, udacfaId);
    }

    public final java.lang.String  getUdacfaValue() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(UDACFA_VALUE_IDX);
    }

    public final void setUdacfaValue(java.lang.String udacfaValue) throws SQLException 
    { 
        _struct.setAttribute(UDACFA_VALUE_IDX, udacfaValue);
    }

    public final java.lang.String  getUdacfaType() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(UDACFA_TYPE_IDX);
    }

    public final void setUdacfaType(java.lang.String udacfaType) throws SQLException 
    { 
        _struct.setAttribute(UDACFA_TYPE_IDX, udacfaType);
    }

    public final java.lang.String  getUdacfaFlag() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(UDACFA_FLAG_IDX);
    }

    public final void setUdacfaFlag(java.lang.String udacfaFlag) throws SQLException 
    { 
        _struct.setAttribute(UDACFA_FLAG_IDX, udacfaFlag);
    }

    public static String getSQLTypeName() {    return DbSchemaResolverFactory.getResolver().resolveObjectName(_SQL_NAME);} 
    public static int getSQLTypeCode() {    return _SQL_TYPECODE;} 
} // end class
