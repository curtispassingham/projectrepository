
package com.adeo.common.model.dbwrappers;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.jpub.runtime.MutableStruct;
import oracle.sql.Datum;
import java.sql.SQLException;
import oracle.sql.STRUCT;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.retail.apps.framework.jdbc.DbSchemaResolverFactory;
import oracle.retail.apps.framework.jdbc.OracleTypeAdapter;
/*******************************************************
 * THIS CODE IS AUTO-GENERATED.  DO NOT MODIFY THIS CODE!
 * 
 * Modify the Oracle Type this class was based on and 
 * re-generate the contents of this file using the oracle.retail.apps.framework.jdbc.OracleTypeAdapterGenerator application. 
 * 
 * Generator Version : 1.0
 * Generation Date   : 2018-06-27 11:48:025
 * Oracle Type Name  : XXADEO_ITEM_RESULT_OBJ
 * Owned by          : XXADEO_RMS
 * Requested Schema  : XXADEO_RMS
 * Requested DB URL  : jdbc:oracle:thin:@onsdb04:1521/rms16adeo
 * Requested DB User : xxadeo_rms
 *******************************************************/
public class XxadeoItemResultObj implements ORAData, ORADataFactory, OracleTypeAdapter {
    public static final String _SQL_NAME = "XXADEO_ITEM_RESULT_OBJ";

    public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

    public static final int GROUP_SET_ID_IDX = 0;
    public static final int FILTER_ORG_ID_IDX = 1;
    public static final int GROUP_NO_IDX = 2;
    public static final int DEPT_IDX = 3;
    public static final int CLASS_IDX = 4;
    public static final int SUBCLASS_IDX = 5;
    public static final int CLASSMENT_BU_IDX = 6;
    public static final int ITEM_IDX = 7;
    public static final int ITEM_DESC_IDX = 8;
    public static final int SOUS_TYPO_STEP_IDX = 9;
    public static final int UNITE_CONSOMMATEUR_IDX = 10;
    public static final int QT_CONTENANCE_IDX = 11;
    public static final int UN_CONTENANCE_IDX = 12;
    public static final int MODE_ASSORT_IDX = 13;
    public static final int TAILLE_GAMME_IDX = 14;
    public static final int CIRC_DELIV_ELIG_IDX = 15;
    public static final int ETAT_CYCLE_VIE_IDX = 16;
    public static final int DATE_SOUH_PASS_ACTIF_COMM_IDX = 17;
    public static final int TOP_1000_IDX = 18;
    public static final int TOP_HYPER_100_IDX = 19;
    public static final int TOP_MDH_IDX = 20;
    public static final int MODIF_PA_MAGASIN_IDX = 21;
    public static final int FORECAST_IND_IDX = 22;
    protected MutableStruct _struct;
    protected static int[] _sqlType = {2,2,2,2,2,2,2,12,12,12,12,2,12,12,12,12,12,93,12,12,12,12,12};
    protected static ORADataFactory[] _factory = new ORADataFactory[23];
    static {
    }
    protected static final XxadeoItemResultObj _XxadeoItemResultObjFactory = new XxadeoItemResultObj();
    public static ORADataFactory getORADataFactory() {
        return _XxadeoItemResultObjFactory;
    }

    public XxadeoItemResultObj() {
        _struct = new MutableStruct(new Object[23], _sqlType, _factory);
    }

    public Datum toDatum(Connection c) throws SQLException {
        return _struct.toDatum(c, getSQLTypeName()); 
    }

    public ORAData create(Datum d, int sqlType) throws SQLException {
        return create(null, d, sqlType);
    }

    protected ORAData create(XxadeoItemResultObj o, Datum d, int sqlType) throws SQLException {
        if (d == null)
            return null;
        if (o == null)
            o = new XxadeoItemResultObj();
        o._struct = new MutableStruct((STRUCT)d, _sqlType, _factory);
        return o;
    }
    public final java.math.BigDecimal  getGroupSetId() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(GROUP_SET_ID_IDX);
    }

    public final void setGroupSetId(java.math.BigDecimal groupSetId) throws SQLException 
    { 
        _struct.setAttribute(GROUP_SET_ID_IDX, groupSetId);
    }

    public final java.math.BigDecimal  getFilterOrgId() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(FILTER_ORG_ID_IDX);
    }

    public final void setFilterOrgId(java.math.BigDecimal filterOrgId) throws SQLException 
    { 
        _struct.setAttribute(FILTER_ORG_ID_IDX, filterOrgId);
    }

    public final java.math.BigDecimal  getGroupNo() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(GROUP_NO_IDX);
    }

    public final void setGroupNo(java.math.BigDecimal groupNo) throws SQLException 
    { 
        _struct.setAttribute(GROUP_NO_IDX, groupNo);
    }

    public final java.math.BigDecimal  getDept() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(DEPT_IDX);
    }

    public final void setDept(java.math.BigDecimal dept) throws SQLException 
    { 
        _struct.setAttribute(DEPT_IDX, dept);
    }

    public final java.math.BigDecimal  getClassAttribute() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(CLASS_IDX);
    }

    public final void setClassAttribute(java.math.BigDecimal classAttribute) throws SQLException 
    { 
        _struct.setAttribute(CLASS_IDX, classAttribute);
    }

    public final java.math.BigDecimal  getSubclass() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(SUBCLASS_IDX);
    }

    public final void setSubclass(java.math.BigDecimal subclass) throws SQLException 
    { 
        _struct.setAttribute(SUBCLASS_IDX, subclass);
    }

    public final java.math.BigDecimal  getClassmentBu() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(CLASSMENT_BU_IDX);
    }

    public final void setClassmentBu(java.math.BigDecimal classmentBu) throws SQLException 
    { 
        _struct.setAttribute(CLASSMENT_BU_IDX, classmentBu);
    }

    public final java.lang.String  getItem() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(ITEM_IDX);
    }

    public final void setItem(java.lang.String item) throws SQLException 
    { 
        _struct.setAttribute(ITEM_IDX, item);
    }

    public final java.lang.String  getItemDesc() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(ITEM_DESC_IDX);
    }

    public final void setItemDesc(java.lang.String itemDesc) throws SQLException 
    { 
        _struct.setAttribute(ITEM_DESC_IDX, itemDesc);
    }

    public final java.lang.String  getSousTypoStep() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(SOUS_TYPO_STEP_IDX);
    }

    public final void setSousTypoStep(java.lang.String sousTypoStep) throws SQLException 
    { 
        _struct.setAttribute(SOUS_TYPO_STEP_IDX, sousTypoStep);
    }

    public final java.lang.String  getUniteConsommateur() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(UNITE_CONSOMMATEUR_IDX);
    }

    public final void setUniteConsommateur(java.lang.String uniteConsommateur) throws SQLException 
    { 
        _struct.setAttribute(UNITE_CONSOMMATEUR_IDX, uniteConsommateur);
    }

    public final java.math.BigDecimal  getQtContenance() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(QT_CONTENANCE_IDX);
    }

    public final void setQtContenance(java.math.BigDecimal qtContenance) throws SQLException 
    { 
        _struct.setAttribute(QT_CONTENANCE_IDX, qtContenance);
    }

    public final java.lang.String  getUnContenance() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(UN_CONTENANCE_IDX);
    }

    public final void setUnContenance(java.lang.String unContenance) throws SQLException 
    { 
        _struct.setAttribute(UN_CONTENANCE_IDX, unContenance);
    }

    public final java.lang.String  getModeAssort() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(MODE_ASSORT_IDX);
    }

    public final void setModeAssort(java.lang.String modeAssort) throws SQLException 
    { 
        _struct.setAttribute(MODE_ASSORT_IDX, modeAssort);
    }

    public final java.lang.String  getTailleGamme() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(TAILLE_GAMME_IDX);
    }

    public final void setTailleGamme(java.lang.String tailleGamme) throws SQLException 
    { 
        _struct.setAttribute(TAILLE_GAMME_IDX, tailleGamme);
    }

    public final java.lang.String  getCircDelivElig() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(CIRC_DELIV_ELIG_IDX);
    }

    public final void setCircDelivElig(java.lang.String circDelivElig) throws SQLException 
    { 
        _struct.setAttribute(CIRC_DELIV_ELIG_IDX, circDelivElig);
    }

    public final java.lang.String  getEtatCycleVie() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(ETAT_CYCLE_VIE_IDX);
    }

    public final void setEtatCycleVie(java.lang.String etatCycleVie) throws SQLException 
    { 
        _struct.setAttribute(ETAT_CYCLE_VIE_IDX, etatCycleVie);
    }

    public final java.sql.Timestamp  getDateSouhPassActifComm() throws SQLException 
    {
        return (java.sql.Timestamp)_struct.getAttribute(DATE_SOUH_PASS_ACTIF_COMM_IDX);
    }

    public final void setDateSouhPassActifComm(java.sql.Timestamp dateSouhPassActifComm) throws SQLException 
    { 
        _struct.setAttribute(DATE_SOUH_PASS_ACTIF_COMM_IDX, dateSouhPassActifComm);
    }

    public final java.lang.String  getTop1000() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(TOP_1000_IDX);
    }

    public final void setTop1000(java.lang.String top1000) throws SQLException 
    { 
        _struct.setAttribute(TOP_1000_IDX, top1000);
    }

    public final java.lang.String  getTopHyper100() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(TOP_HYPER_100_IDX);
    }

    public final void setTopHyper100(java.lang.String topHyper100) throws SQLException 
    { 
        _struct.setAttribute(TOP_HYPER_100_IDX, topHyper100);
    }

    public final java.lang.String  getTopMdh() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(TOP_MDH_IDX);
    }

    public final void setTopMdh(java.lang.String topMdh) throws SQLException 
    { 
        _struct.setAttribute(TOP_MDH_IDX, topMdh);
    }

    public final java.lang.String  getModifPaMagasin() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(MODIF_PA_MAGASIN_IDX);
    }

    public final void setModifPaMagasin(java.lang.String modifPaMagasin) throws SQLException 
    { 
        _struct.setAttribute(MODIF_PA_MAGASIN_IDX, modifPaMagasin);
    }

    public final java.lang.String  getForecastInd() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(FORECAST_IND_IDX);
    }

    public final void setForecastInd(java.lang.String forecastInd) throws SQLException 
    { 
        _struct.setAttribute(FORECAST_IND_IDX, forecastInd);
    }

    public static String getSQLTypeName() {    return DbSchemaResolverFactory.getResolver().resolveObjectName(_SQL_NAME);} 
    public static int getSQLTypeCode() {    return _SQL_TYPECODE;} 
} // end class
