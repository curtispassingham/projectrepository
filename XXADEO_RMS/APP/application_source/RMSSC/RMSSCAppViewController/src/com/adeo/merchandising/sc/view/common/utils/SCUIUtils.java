package com.adeo.merchandising.sc.view.common.utils;

import java.text.MessageFormat;

import oracle.retail.apps.rms.common.view.util.RmsUIUtils;

public class SCUIUtils {
    public SCUIUtils() {
        super();
    }

    public static String getViewControllerBundleValue(String key, Object... params) {
        String paramMessage = RmsUIUtils.getXlifLocalizedString(UIConstants.Bundles.BUNDLE_VC, key);
        if (params != null)
            return MessageFormat.format(paramMessage, params);
        else
            return paramMessage;
    }
}
