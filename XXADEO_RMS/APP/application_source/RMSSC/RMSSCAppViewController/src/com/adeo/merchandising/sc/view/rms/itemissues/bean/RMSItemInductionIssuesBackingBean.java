package com.adeo.merchandising.sc.view.rms.itemissues.bean;

import com.adeo.merchandising.sc.view.common.utils.SCUIUtils;

import com.adeo.merchandising.sc.view.common.utils.UIConstants;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.share.ADFContext;

import oracle.retail.apps.rms.common.view.properties.RmsAppsProperties;
import oracle.retail.apps.rms.common.view.util.ReportConstants;
import oracle.retail.apps.rms.common.view.util.RmsUIUtils;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class RMSItemInductionIssuesBackingBean {

    public static final String RMS_SUCCESS_REPORT_MSG = "RMS_SUCCESS_REPORT_MSG";
    public static final String RMS_FAIL_REPORT_MSG = "RMS_FAIL_REPORT_MSG";
    public static final String RMS_FAIL_REPORT_KEY_MSG = "RMS_FAIL_REPORT_KEY_MSG";
    public static final String REPORT_KEY =
        "BI_ITEM_ISSUES_REPORT"; // the report has a report key defined in properties file - common jar

    public RMSItemInductionIssuesBackingBean() {
        super();
    }

    public void getLaunchReportRMS(ActionEvent ae) {
        Boolean openContent = false;
        String pmProcessId =
            RmsUIUtils.resolveExpression(UIConstants.CustomUtils.PM_PROCESS_ID_PARAM_EXPR).toString();
        StringBuffer reportUrl = new StringBuffer();
        String reportsWebServer = RmsUIUtils.getReportsWebServer();
        if (RmsUIUtils.isBIAvailable()) {
            if (reportsWebServer != null && !reportsWebServer.isEmpty()) {
                try {
                    reportUrl.append(reportsWebServer).append(ReportConstants.WEB_URL_SEPARATOR).append((RmsAppsProperties.getInstance()).getReportDetails(REPORT_KEY));
                    RmsUIUtils.addFacesInfoMessage(SCUIUtils.getViewControllerBundleValue(RMS_SUCCESS_REPORT_MSG));
                    openContent = true;
                } catch (Exception e) {
                    RmsUIUtils.addFacesWarningMessage(SCUIUtils.getViewControllerBundleValue(RMS_FAIL_REPORT_KEY_MSG));
                    e.printStackTrace();
                    openContent = false;
                }
                reportUrl.append(ReportConstants.QUESTION_MARK);
                reportUrl.append(UIConstants.CustomUtils.USER).append(UIConstants.CustomUtils.EQUALS).append(ADFContext.getCurrent().getSecurityContext().getUserName());
                reportUrl.append(ReportConstants.AMPERSAND).append(UIConstants.CustomUtils.PROCESS_ID).append(UIConstants.CustomUtils.EQUALS).append(pmProcessId);
            }
            if (openContent.equals(Boolean.TRUE)) {
                FacesContext ctx = FacesContext.getCurrentInstance();
                ExtendedRenderKitService erks =
                    Service.getService(ctx.getRenderKit(), ExtendedRenderKitService.class);
                String url = "window.open('" + reportUrl + "','_blank');";
                erks.addScript(FacesContext.getCurrentInstance(), url);
            }
        } else {
            RmsUIUtils.addFacesWarningMessage(SCUIUtils.getViewControllerBundleValue(RMS_FAIL_REPORT_MSG));
        }
    }
}
