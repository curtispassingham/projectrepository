package com.adeo.merchandising.sc.view.common.utils;

public class UIConstants {
    public UIConstants() {
        super();
    }

    public class Bundles {
        public static final String BUNDLE_VC =
            "com.adeo.merchandising.sc.view.RMSSCAppViewControllerBundle";
    }

    public class CustomUtils {
        //Task Flow Parameters
        public static final String PM_PROCESS_ID_PARAM = "pmProcessId";

        //Managed Beans
        public static final String VIEW_ITEM_INDUCTION_ISSUE_FB = "ViewItemInductionIssueFlowBean";
        public static final String VIEW_COSTCHANGE_ISSUES_FB = "ViewCostChangeIssuesFlowBean";

        // Resolve Expressions
        public final static String PM_PROCESS_ID_PARAM_EXPR =
            "#{pageFlowScope." + VIEW_ITEM_INDUCTION_ISSUE_FB + "." + PM_PROCESS_ID_PARAM + "}";

        public final static String PM_PROCESS_ID_COSTCHANGES_PARAM_EXPR =
            "#{pageFlowScope." + VIEW_COSTCHANGE_ISSUES_FB + "." + PM_PROCESS_ID_PARAM + "}";
        // Report Parameters
        public static final String USER = "user";
        public static final String PROCESS_ID = "P_PROCESS_ID";
        public static final String EQUALS = "=";
    }

    public class RAFUtils {
        public static final String OPEN_CONTENT = "openContent";
        public static final String CLOSE_TAB = "closeTab";

        public static final String OC_CONTENT_TYPE = "contentType";
        public static final String OC_URL = "url";
        public static final String OC_ID = "id";
        public static final String OC_TITLE = "title";
        public static final String OC_PARAMETERS_LIST = "parametersList";
        public static final String OC_ATTRIBUTES_LIST = "attributesList";
        public static final String OC_KEYS_LIST = "keysList";
        public static final String OC_ADDITIONAL_PARAMETERS = "additionalParameters";

        public static final String OC_LINK = "link";
    }


}
