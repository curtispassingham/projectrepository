
package com.adeo.merchandising.wrappers.model.dbwrappers;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.jpub.runtime.MutableArray;
import oracle.sql.Datum;
import java.sql.SQLException;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.retail.apps.framework.jdbc.DbSchemaResolverFactory;
import oracle.retail.apps.framework.jdbc.OracleTypeAdapter;
/*******************************************************
 * THIS CODE IS AUTO-GENERATED.  DO NOT MODIFY THIS CODE!
 * 
 * Modify the Oracle Type this class was based on and 
 * re-generate the contents of this file using the oracle.retail.apps.framework.jdbc.OracleTypeAdapterGenerator application. 
 * 
 * Generator Version : 1.0
 * Generation Date   : 2018-09-06 08:56:029
 * Oracle Type Name  : XXADEO_ITEM_MASS_CHANGE_TBL
 * Owned by          : XXADEO_RMS
 * Requested Schema  : XXADEO_RMS
 * Requested DB URL  : jdbc:oracle:thin:@onsdb04:1521/rms16adeo
 * Requested DB User : xxadeo_rms
 *******************************************************/
public class XxadeoItemMassChangeTbl implements ORAData, ORADataFactory, OracleTypeAdapter{
    public static final String _SQL_NAME = "XXADEO_ITEM_MASS_CHANGE_TBL";
    public static final int _SQL_TYPECODE = OracleTypes.ARRAY;
    MutableArray  _array;
    private static final XxadeoItemMassChangeTbl _XxadeoItemMassChangeTblFactory = new XxadeoItemMassChangeTbl();
    public static ORADataFactory getORADataFactory() {
        return _XxadeoItemMassChangeTblFactory;
    }
    public XxadeoItemMassChangeTbl() {
        this((XxadeoItemMassChangeObj[])null);
    }
    public XxadeoItemMassChangeTbl(XxadeoItemMassChangeObj[] a) {
        XxadeoItemMassChangeObj[] aclone =  null;
        if (a != null) {
            aclone = a.clone();
        }
        _array = new MutableArray(2002, aclone, XxadeoItemMassChangeObj.getORADataFactory());
    }

    public Datum toDatum(Connection c) throws SQLException {
        return _array.toDatum(c, getSQLTypeName());  
    }
    public int length() throws SQLException {
        return _array.length();
    }

    public int getBaseType() throws SQLException {
        return _array.getBaseType();
    }

    public String getBaseTypeName() throws SQLException {
        return _array.getBaseTypeName();
    }

    public ArrayDescriptor getDescriptor() throws SQLException {
        return _array.getDescriptor();
    }

    public ORAData create(Datum d, int sqlType) throws SQLException {
        if (d == null)
            return null;
        XxadeoItemMassChangeTbl a = new XxadeoItemMassChangeTbl();
        a._array = new MutableArray(2002, (ARRAY)d, XxadeoItemMassChangeObj.getORADataFactory());
        return a;
    }

    public XxadeoItemMassChangeObj[] getArray() throws SQLException {
        return (XxadeoItemMassChangeObj[])_array.getObjectArray(new XxadeoItemMassChangeObj[_array.length()]);
    }

    public XxadeoItemMassChangeObj[] getArray(long index, int count) throws SQLException {
        return (XxadeoItemMassChangeObj[])_array.getObjectArray(index, new XxadeoItemMassChangeObj[_array.sliceLength(index,count)]);
    }

    public void setArray(XxadeoItemMassChangeObj[] a) throws SQLException {
        _array.setObjectArray(a);
    }

    public void setArray(XxadeoItemMassChangeObj[] a, long index) throws SQLException {
        _array.setObjectArray(a, index);
    }

    public XxadeoItemMassChangeObj getElement(long index) throws SQLException {
        return (XxadeoItemMassChangeObj)_array.getObjectElement(index);
    }

    public void setElement(XxadeoItemMassChangeObj a, long index) throws SQLException {
        _array.setObjectElement(a, index);
    }

    public static String getSQLTypeName() {    return DbSchemaResolverFactory.getResolver().resolveObjectName(_SQL_NAME);} 
    public static int getSQLTypeCode() {    return _SQL_TYPECODE;} 
} // end class
