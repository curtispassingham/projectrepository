package com.adeo.merchandising.wrappers.model.dbwrappers.common;

import oracle.adf.share.logging.ADFLogger;

/**
 * Generate Java classes to map database objects for schema SLF.
 */
public final class GenerateJavaWrappers {
    // class
    private static final Class CLASS = GenerateJavaWrappers.class;
    private static final String CLASS_NAME = CLASS.getName();
    // log
    private static final ADFLogger LOG = ADFLogger.createADFLogger(GenerateJavaWrappers.class);

    /**
     * Empty constructor.
     */
    private GenerateJavaWrappers() {
    }

    /**
     * Main method.
     *
     * @param args Arguments list.
     */
    public static void main(String[] args) {
        LOG.entering(CLASS_NAME, "main");
        String jdbcUrl = "jdbc:oracle:thin:@onsdb04:1521/rms16adeo";
        String jdbcUser = "xxadeo_rms";
        String jdbcPassword = "oracle";
        String schemaOwner = "XXADEO_RMS";
        String plsqlSourceJavaPackage = "com.adeo.merchandising.wrappers.model.dbwrappers";
        String slfPlsqlSourceNames = "XXADEO_DASHBOARDS_MASS_CHG_SQL";
        String noTypeGenOutput = "false";
        JavaWrappersUtil.generateDatabaseObjects(jdbcUrl, jdbcUser, jdbcPassword, schemaOwner,
                                                 slfPlsqlSourceNames, plsqlSourceJavaPackage,
                                                 noTypeGenOutput);
        LOG.exiting(CLASS_NAME, "main");
    }
}
