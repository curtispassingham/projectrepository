
package com.adeo.merchandising.wrappers.model.dbwrappers;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.jpub.runtime.MutableStruct;
import oracle.sql.Datum;
import java.sql.SQLException;
import oracle.sql.STRUCT;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.retail.apps.framework.jdbc.DbSchemaResolverFactory;
import oracle.retail.apps.framework.jdbc.OracleTypeAdapter;
/*******************************************************
 * THIS CODE IS AUTO-GENERATED.  DO NOT MODIFY THIS CODE!
 * 
 * Modify the Oracle Type this class was based on and 
 * re-generate the contents of this file using the oracle.retail.apps.framework.jdbc.OracleTypeAdapterGenerator application. 
 * 
 * Generator Version : 1.0
 * Generation Date   : 2018-09-06 08:56:038
 * Oracle Type Name  : XXADEO_ITEM_MASS_CHANGE_OBJ
 * Owned by          : XXADEO_RMS
 * Requested Schema  : XXADEO_RMS
 * Requested DB URL  : jdbc:oracle:thin:@onsdb04:1521/rms16adeo
 * Requested DB User : xxadeo_rms
 *******************************************************/
public class XxadeoItemMassChangeObj implements ORAData, ORADataFactory, OracleTypeAdapter {
    public static final String _SQL_NAME = "XXADEO_ITEM_MASS_CHANGE_OBJ";

    public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

    public static final int BU_IDX = 0;
    public static final int ITEM_ID_IDX = 1;
    protected MutableStruct _struct;
    protected static int[] _sqlType = {2,12};
    protected static ORADataFactory[] _factory = new ORADataFactory[2];
    static {
    }
    protected static final XxadeoItemMassChangeObj _XxadeoItemMassChangeObjFactory = new XxadeoItemMassChangeObj();
    public static ORADataFactory getORADataFactory() {
        return _XxadeoItemMassChangeObjFactory;
    }

    public XxadeoItemMassChangeObj() {
        _struct = new MutableStruct(new Object[2], _sqlType, _factory);
    }

    public Datum toDatum(Connection c) throws SQLException {
        return _struct.toDatum(c, getSQLTypeName()); 
    }

    public ORAData create(Datum d, int sqlType) throws SQLException {
        return create(null, d, sqlType);
    }

    protected ORAData create(XxadeoItemMassChangeObj o, Datum d, int sqlType) throws SQLException {
        if (d == null)
            return null;
        if (o == null)
            o = new XxadeoItemMassChangeObj();
        o._struct = new MutableStruct((STRUCT)d, _sqlType, _factory);
        return o;
    }
    public final java.math.BigDecimal  getBu() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(BU_IDX);
    }

    public final void setBu(java.math.BigDecimal bu) throws SQLException 
    { 
        _struct.setAttribute(BU_IDX, bu);
    }

    public final java.lang.String  getItemId() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(ITEM_ID_IDX);
    }

    public final void setItemId(java.lang.String itemId) throws SQLException 
    { 
        _struct.setAttribute(ITEM_ID_IDX, itemId);
    }

    public static String getSQLTypeName() {    return DbSchemaResolverFactory.getResolver().resolveObjectName(_SQL_NAME);} 
    public static int getSQLTypeCode() {    return _SQL_TYPECODE;} 
} // end class
