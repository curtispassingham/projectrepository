/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "CODE_HEAD" and "CODE_DETAIL" table for Dashboard     */
/*               item price changes - RR425/426                               */
/******************************************************************************/

 
--DELETE

delete from code_detail where CODE_TYPE = 'PCHG' and CODE = 'PERVAR';  
delete from code_detail where CODE_TYPE = 'PCHG' and CODE = 'PRCCHG'; 

delete from code_head where CODE_TYPE = 'PCHG';



--INSERT

insert into code_head (CODE_TYPE, CODE_TYPE_DESC)
values ('PCHG', 'Item price changes');



insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('PCHG', 'PERVAR', '50%', 'Y', 1);

insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('PCHG', 'PRCCHG', '10', 'Y', 2);
