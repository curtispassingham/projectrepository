--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Paulo Mamede                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of CODE_HEAD table for RB101                             */
/******************************************************************************/
--------------------------------------------------------------------------------
--
delete code_detail where code_type = 'BUID';

delete code_head where code_type = 'BUID';
--
insert into code_head(code_type, code_type_desc) values ('BUID','Business Unit Subscription ID');
--
