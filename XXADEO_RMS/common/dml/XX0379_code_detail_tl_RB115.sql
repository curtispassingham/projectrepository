/******************************************************************************/
/* CREATE DATE - June 2018                                                    */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "CODE_DETAIL_TL" table for RB115                      */
/******************************************************************************/
--
delete from code_detail_tl where code_type IN ('MHST');
--
-- Merch Hier Status
--
insert into code_detail_tl (lang, code_type, code, code_desc, create_datetime, create_id, last_update_datetime, last_update_id)
values (3, 'MHST', '1', 'En preparation', SYSDATE, USER, SYSDATE, USER);

insert into code_detail_tl (lang, code_type, code, code_desc, create_datetime, create_id, last_update_datetime, last_update_id)
values (3, 'MHST', '2', 'Actif', SYSDATE, USER, SYSDATE, USER);

insert into code_detail_tl (lang, code_type, code, code_desc, create_datetime, create_id, last_update_datetime, last_update_id)
values (3, 'MHST', '3', 'Désactiver', SYSDATE, USER, SYSDATE, USER);

