--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - June 2018                                                    */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of RTK_ERROR table for RB34a                             */
/******************************************************************************/
--------------------------------------------------------------------------------
--
delete rtk_errors where rtk_key in ('XXADEO_INV_ZONE_NODE_TYPE','PC_ERROR_VALIDATION','PC_NOT_FOUND','PC_DUPLICATED','PC_STORE_AMOUNT_HIGH');
delete rtk_errors where rtk_key in ('XXADEO_INV_ZONE_NODE_TYPE','XXADEO_PC_ERR_VALIDATION','XXADEO_PC_NOT_FOUND','XXADEO_PC_DUPLICATED','XXADEO_PC_ST_AMOUNT_HIGH','XXADEO_PC_SP_BLOCKED');
--
insert into rtk_errors (rtk_type, rtk_key, rtk_text, rtk_user, rtk_approved)
values ('BL','XXADEO_INV_ZONE_NODE_TYPE','Invalid Zone Node Type.',USER,'Y');
--
insert into rtk_errors (rtk_type, rtk_key, rtk_text, rtk_user, rtk_approved)
values ('BL','XXADEO_PC_ERR_VALIDATION','Errors on validation.',USER,'Y');

insert into rtk_errors (rtk_type, rtk_key, rtk_text, rtk_user, rtk_approved)
values ('BL','XXADEO_PC_NOT_FOUND','Price Change not found.',USER,'Y');

insert into rtk_errors (rtk_type, rtk_key, rtk_text, rtk_user, rtk_approved)
values ('BL','XXADEO_PC_DUPLICATED','Price Change duplicated.',USER,'Y');

insert into rtk_errors (rtk_type, rtk_key, rtk_text, rtk_user, rtk_approved)
values ('BL','XXADEO_PC_ST_AMOUNT_HIGH','Store Price Change value is higher than Zone Price Change.',USER,'Y');

insert into rtk_errors (rtk_type, rtk_key, rtk_text, rtk_user, rtk_approved)
values ('BL','XXADEO_PC_SP_BLOCKED','Store is not allowed to create price change for the item in Store Portal.',USER,'Y');