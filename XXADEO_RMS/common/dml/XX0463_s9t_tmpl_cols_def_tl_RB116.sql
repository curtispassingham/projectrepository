/******************************************************************************/
/* CREATE DATE - November 2018                                                */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_TMPL_COLS_DEF_TL" table for RB116                */
/******************************************************************************/
update s9t_tmpl_cols_def_tl a
   set column_name = replace(column_name, chr(13), '')
 where template_key = 'XXADEO_ITEM_MASTER'
   and exists (select 1
                 from s9t_tmpl_cols_def_tl c
                where template_key = 'ITEM_MASTER_DATA'
                  and c.wksht_key  = a.wksht_key
				  and c.column_key = a.column_key);