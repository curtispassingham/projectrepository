/* *************************************************************************  */
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Paulo Mamede / Tiago Torres                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Install script for XXADEO scripts                            */
/* ************************************************************************** */

delete from rtk_errors_tl 
    where rtk_key in (
        'CFA_DEPARTMENT_SUP_NULL'
        ,'CFA_INV_PREV_ADMIN_ORG_UN'
        ,'CFA_INV_PREV_ADMIN_SUPP'
        ,'CFA_SUP_TYPE_NULL'
        ,'CFA_DUP_PREV_ADMIN_SUPP'
        ,'CFA_PORT_CHARGE'
        ,'CFA_PORT_CHARGE_AMNT'
        ,'CFA_PORT_CHARGE_VALUE'
        ,'CFA_GLN_INV_SUPP'
        ,'CFA_SUP_DEPT_TYPE_NULL'
        ,'CFA_SHIP_COST_BILL_SUPP'
    ); 

-- FR lang=3
    
-- 1a
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(3,'CFA_INV_PREV_ADMIN_SUPP','Le Site-fournisseur d''achat saisi est invalide pour le CFA "Ancien fournisseur administratif"',sysdate,user,sysdate,user);

-- 1b
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(3,'CFA_INV_PREV_ADMIN_ORG_UN','L''ancien fournisseur administratif et le nouveau fournisseur ne sont pas associ�s � la m�me unit� organisationnelle.',sysdate,user,sysdate,user);

-- 1?
Insert into rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id)
values(3,'CFA_DUP_PREV_ADMIN_SUPP','Le ancien fournisseur administratif et le nouveau fournisseur ne sont pas associ�s � la m�me unit� organisationnelle.',sysdate,user,sysdate,user);

-- 2a
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(3,'CFA_SUP_TYPE_NULL','L''attribut "Type fournisseur" est obligatoire pour les sites-fournisseur d''achat',sysdate,user,sysdate,user);

-- 2b
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(3,'CFA_DEPARTMENT_SUP_NULL','L''attribut "Rayon" est obligatoire pour les sites-fournisseur d''achat',sysdate,user,sysdate,user);

-- 2a + 2b
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(3,'CFA_SUP_DEPT_TYPE_NULL','Les attributs "Type fournisseur" et "Rayon" sont obligatoires pour les sites-fournisseur d''achat',sysdate,user,sysdate,user);

-- 3
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(3,'CFA_SHIP_COST_BILL_SUPP', 'Si "Facturation de frais de port" est activ�, "Unit� de Franco de Port", "Valeur de Franco de Port" et "Montant du Frais de Port" doivent �tre renseign�s.',sysdate,user,sysdate,user);

-- 3a - not used, replaced by 3
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(3,'CFA_PORT_CHARGE','Si "Facturation de frais de port" est activ�, l''unit� de franco de port est obligatoire',sysdate,user,sysdate,user);

-- 3b - not used, replaced by 3
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(3,'CFA_PORT_CHARGE_AMNT','Si "Facturation de frais de port" est activ�, le montant du frais de port est obligatoire',sysdate,user,sysdate,user);

-- 3c - not used, replaced by 3
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(3,'CFA_PORT_CHARGE_VALUE','Si "Facturation de frais de port" est activ�, la valeur de franco de port est obligatoire',sysdate,user,sysdate,user);

-- 4
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(3,'CFA_GLN_INV_SUPP','Cl� de contr�le non valide',sysdate,user,sysdate,user);

-- ES lang=4

-- 1a
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(4,'CFA_INV_PREV_ADMIN_SUPP','El Sitio-Proveedor de compra introducido es incorrecto para el CFA "Antiguo proveedor administrativo"',sysdate,user,sysdate,user);

-- 1b
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(4,'CFA_INV_PREV_ADMIN_ORG_UN','El antiguo proveedor administrativo y el nuevo proveedor no est�n asociados a la misma unidad organizativa',sysdate,user,sysdate,user);

-- 1 - translation missing
--Insert into rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id)
--values(4,'CFA_DUP_PREV_ADMIN_SUPP','',sysdate,user,sysdate,user);

-- 2a
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(4,'CFA_SUP_TYPE_NULL','El atributo "Tipo proveedor" es obligatorio para los sitios-proveedor de compra',sysdate,user,sysdate,user);

-- 2b
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(4,'CFA_DEPARTMENT_SUP_NULL','El atributo "Secci�n" es obligatorio para los sitios-proveedor de compra',sysdate,user,sysdate,user);

-- 2a + 2b
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(4,'CFA_SUP_DEPT_TYPE_NULL','Los atributos "Tipo proveedor" y "Secci�n" son obligatorios para los sitios-proveedor de compra',sysdate,user,sysdate,user);

-- 3a
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(4,'CFA_PORT_CHARGE','Si "Facturaci�n de gastos de env�o" est� activado, la unidad de franco de env�o es obligatoria',sysdate,user,sysdate,user);

-- 3b
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(4,'CFA_PORT_CHARGE_AMNT','Si "Facturaci�n de gastos de env�o" est� activado, el montante de gastos de env�o es obligatorio',sysdate,user,sysdate,user);

-- 3c
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(4,'CFA_PORT_CHARGE_VALUE','Si "Facturaci�n de gastos de env�o" est� activado, el valor de franco de env�o es obligatorio',sysdate,user,sysdate,user);

-- 4
INSERT INTO rtk_errors_tl(lang,rtk_key,rtk_text,create_datetime,create_id,last_update_datetime,last_update_id) 
values(4,'CFA_GLN_INV_SUPP','Clave de control no es valida',sysdate,user,sysdate,user);

