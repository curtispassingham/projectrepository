
-- CREATE DATE - Jan 2019
-- CREATE USER - Pedro Miranda
-- PROJECT     - ADEO
-- DESCRIPTION - Identify Code 48 Items
--
delete code_detail where code_type = 'RC48';
--
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('RC48', '2', 'Personnalisé', 'Y', 1);
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('RC48', '5', 'Sur mesure', 'Y', 2);          
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('RC48', '6', 'Catalogue', 'Y', 3);
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('RC48', '10', 'Personnalisable', 'Y', 4);


