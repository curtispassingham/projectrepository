/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "CODE_HEAD" and "CODE_DETAIL" table for KPI Conflict  */
/*               Check Errors - RR427                                         */
/******************************************************************************/

 
--DELETE



delete from code_detail cd where cd.code_type = 'CCHE';
--
delete from code_head ch where ch.code_type = 'CCHE';



--INSERTS

--
--
insert into code_head (CODE_TYPE, CODE_TYPE_DESC)
values ('CCHE', 'Conflict Check Errors Messages');
--
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('CCHE', '1', 'CHANGE_SELLING_UOM_REQUIRED_FOR_FIXED_PRICE', 'Y', 1);

insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('CCHE', '2', 'XXADEO_PC_DUPLICATED', 'Y', 2);

insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('CCHE', '3', 'INVALID_CHANGE_AMOUNT_FIXED_PRICE', 'Y', 3);

insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('CCHE', '4', 'INVALID_CHANGE_SELLING_UOM', 'Y', 4);

insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('CCHE', '5', 'XXADEO_PC_ST_AMOUNT_HIGH', 'Y', 5);

insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('CCHE', '6', 'XXADEO_PC_SP_BLOCKED', 'Y', 6);
