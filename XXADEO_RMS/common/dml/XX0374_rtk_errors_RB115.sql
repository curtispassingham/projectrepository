--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "RTK_ERRORS" table for RB115 BUG 426                  */
/******************************************************************************/
--------------------------------------------------------------------------------
--
delete RTK_ERRORS where RTK_KEY = 'DOMAIN_MERCHHIER_EXISTS';
--
insert into rtk_errors (rtk_type, rtk_key, rtk_text, rtk_user, rtk_approved)
values ('BL','DOMAIN_MERCHHIER_EXISTS','Domain-dept/class/subclass association exists already.',user,'Y');
