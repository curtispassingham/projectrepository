--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of s9t_tmpl_cols_def_tl for RB116                        */
/******************************************************************************/
--------------------------------------------------------------------------------
delete
  from s9t_tmpl_cols_def_tl
 where template_key = 'XXADEO_ITEM_MASTER'
   and column_key not in (select view_col_name from cfa_attrib)
   and wksht_key like 'V\_%' ESCAPE '\'
   and upper(column_key) not in ('ITEM','SUPPLIER');
