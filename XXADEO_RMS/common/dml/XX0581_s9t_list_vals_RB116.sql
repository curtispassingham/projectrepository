/* *************************************************************************  */
/* CREATE DATE - January 2019                                                 */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_LIST_VALS" table for ORACR_00392                 */
/* ************************************************************************** */
delete 
  from s9t_list_vals 
 where template_category = 'IS9T'
   and column_name in ('LMFR_CIRCUIT_ITEM',
                       'LMES_CIRCUIT_ITEM',
                       'LMPL_CIRCUIT_ITEM',
                       'LMIT_CIRCUIT_ITEM',
                       'LMPT_CIRCUIT_ITEM',
                       'LMGR_CIRCUIT_ITEM',
                       'LMCY_CIRCUIT_ITEM',
                       'FRST_AUT_PO_DT_ITEM_SUPP');