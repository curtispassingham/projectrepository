/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_M_VIEW_REFRESH_CFG" table for RB157           */
/******************************************************************************/
--
delete from xxadeo_m_view_refresh_cfg where mv_scope = 'XXADEO_PCS_MV';
--
insert into xxadeo_m_view_refresh_cfg (MV_SCOPE, MV_EXECUTION, MV_NAME, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
values ('XXADEO_PCS_MV', 'XXADEO_REFRESHMVIEW', 'XXADEO_MV_LOCATION_PCS', 'XXADEO_RMS', sysdate, user, sysdate);

insert into xxadeo_m_view_refresh_cfg (MV_SCOPE, MV_EXECUTION, MV_NAME, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
values ('XXADEO_PCS_MV', 'XXADEO_REFRESHMVIEW', 'XXADEO_MV_AREA_PCS', 'XXADEO_RMS', sysdate, user, sysdate);

