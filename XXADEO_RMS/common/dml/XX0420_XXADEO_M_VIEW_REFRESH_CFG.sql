/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_M_VIEW_REFRESH_CFG" table for materialized    */
/*               views that need to be refreshed.                             */
/******************************************************************************/
 
--RR143/144
delete from XXADEO_M_VIEW_REFRESH_CFG
 where mv_name = 'DASHBOARDS_MV'
   and mv_execution = 'XXADEO_INCOMPLETE_ITEMS'
   and mv_name = 'XXADEO_MV_INCOMPLETE_ITEMS';

--
insert into XXADEO_M_VIEW_REFRESH_CFG
  (mv_scope,
   mv_execution,
   mv_name,
   create_id,
   create_datetime,
   last_update_id,
   last_update_datetime)
values
  ('DASHBOARDS_MV',
   'XXADEO_INCOMPLETE_ITEMS',
   'XXADEO_MV_INCOMPLETE_ITEMS',
   user,
   sysdate,
   user,
   sysdate);   



--RR425/426
delete from XXADEO_M_VIEW_REFRESH_CFG
 where mv_name = 'DASHBOARDS_MV'
   and mv_execution = 'XXADEO_ITEMS_PRICE_CHG'
   and mv_name in ('XXADEO_MV_DASHBOARD_PRC_CHG_VR','XXADEO_MV_DASHBOARD_PRC_CHG_NR');
--
insert into XXADEO_M_VIEW_REFRESH_CFG
  (mv_scope,
   mv_execution,
   mv_name,
   create_id,
   create_datetime,
   last_update_id,
   last_update_datetime)
values
  ('DASHBOARDS_MV',
   'XXADEO_ITEMS_PRICE_CHG',
   'XXADEO_MV_DASHBOARD_PRC_CHG_VR',
   user,
   sysdate,
   user,
   sysdate);

insert into XXADEO_M_VIEW_REFRESH_CFG
  (mv_scope,
   mv_execution,
   mv_name,
   create_id,
   create_datetime,
   last_update_id,
   last_update_datetime)
values
  ('DASHBOARDS_MV',
   'XXADEO_ITEMS_PRICE_CHG',
   'XXADEO_MV_DASHBOARD_PRC_CHG_NR',
   user,
   sysdate,
   user,
   sysdate);
