--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of rpm_conflict_query_control table for RB34a            */
/* NOTES       - Fixed values in PK since sequence not used by RPM install    */
/******************************************************************************/


delete from rpm_conflict_query_control where conflict_query_function_name in ('XXADEO_RPM_CC_PC_STORE_PORTAL.VALIDATE', 'XXADEO_RPM_CC_SP_FUTURE_PRICE.VALIDATE');
--
insert into rpm_conflict_query_control (CONFLICT_QUERY_CONTROL_ID, CONFLICT_QUERY_FUNCTION_NAME, CONFLICT_QUERY_DESC, ACTIVE, OVERRIDE_ALLOWED, EXECUTION_ORDER, BASE_GENERATED, BASE_REQUIRED, LOCK_VERSION)
values (2050, 'XXADEO_RPM_CC_PC_STORE_PORTAL.VALIDATE', 'This function checks if a store via Store Portal is allowed to create PC for a specific Item within the BU where it belongs.', 'Y', 'N', 14, 'Y', 'Y', 0);

insert into rpm_conflict_query_control (CONFLICT_QUERY_CONTROL_ID, CONFLICT_QUERY_FUNCTION_NAME, CONFLICT_QUERY_DESC, ACTIVE, OVERRIDE_ALLOWED, EXECUTION_ORDER, BASE_GENERATED, BASE_REQUIRED, LOCK_VERSION)
values (2060, 'XXADEO_RPM_CC_SP_FUTURE_PRICE.VALIDATE', 'This function checks if store future price is higher than zone future price. ', 'Y', 'N', 15, 'Y', 'Y', 0);
--