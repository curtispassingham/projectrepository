/******************************************************************************/
/* CREATE DATE - November 2018                                                */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_TMPL_COLS_DEF_TL" for COST_CHANGE template_key   */
/******************************************************************************/
update s9t_tmpl_cols_def_tl a
   set column_name = replace(column_name, chr(13), '')
 where template_key = 'COST_CHANGE';