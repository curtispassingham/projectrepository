--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of CODE_HEAD table for RB119                             */
/******************************************************************************/
--------------------------------------------------------------------------------
--
delete code_detail where code_type = 'CFAS';

delete code_head where code_type = 'CFAS';
--
insert into code_head values ('CFAS', 'CFA Security');
-- 
