declare
  L_count NUMBER;
Begin

  select count(*)
    into L_count
    from restart_bookmark
   where restart_name in ('xxadeo_pricechange_dnld',
                          'xxadeo_costprice_dnld',
                          'xxadeo_deals_dnld',
                          'xxadeo_ecotax_dnld',
                          'xxadeo_ecotax_dnld_pre',
                          'xxadeo_merchhier_dnld',
                          'xxadeo_supsites_dnld',
                          'xxadeo_item_lfcy');
  if L_count > 0 then
    delete from restart_bookmark
     where restart_name in ('xxadeo_pricechange_dnld',
                            'xxadeo_costprice_dnld',
                            'xxadeo_deals_dnld',
                            'xxadeo_ecotax_dnld',
                            'xxadeo_ecotax_dnld_pre',
                            'xxadeo_merchhier_dnld',
                            'xxadeo_supsites_dnld',
                            'xxadeo_item_lfcy');
  end if;

 -- xxadeo_item_lfcy
  Select count(*)
    into L_count
    from RESTART_CONTROL
   WHERE program_name = 'xxadeo_item_lfcy';

  if L_count = 0 then
    insert into restart_control
      (PROGRAM_NAME,
       PROGRAM_DESC,
       DRIVER_NAME,
       NUM_THREADS,
       UPDATE_ALLOWED,
       PROCESS_FLAG,
       COMMIT_MAX_CTR,
       LOCK_WAIT_TIME,
       RETRY_MAX_CTR)
    values
      ('xxadeo_item_lfcy',
       'ADEO Item Lifecycle extract ',
       'NONE',
       1,
       'Y',
       'T',
       100,
       10,
       3);
  end if;

  -- xxadeo_pricechange_dnld
  Select count(*)
    into L_count
    from RESTART_CONTROL
   WHERE program_name = 'xxadeo_pricechange_dnld';

  if L_count = 0 then
    insert into restart_control
      (PROGRAM_NAME,
       PROGRAM_DESC,
       DRIVER_NAME,
       NUM_THREADS,
       UPDATE_ALLOWED,
       PROCESS_FLAG,
       COMMIT_MAX_CTR,
       LOCK_WAIT_TIME,
       RETRY_MAX_CTR)
    values
      ('xxadeo_pricechange_dnld',
       'ADEO Price Change extract ',
       'NONE',
       1,
       'Y',
       'T',
       100,
       10,
       3);
  end if;

  -- xxadeo_costprice_dnld
  Select count(*)
    into L_count
    from RESTART_CONTROL
   WHERE program_name = 'xxadeo_costprice_dnld';

  if L_count = 0 then
    insert into restart_control
      (PROGRAM_NAME,
       PROGRAM_DESC,
       DRIVER_NAME,
       NUM_THREADS,
       UPDATE_ALLOWED,
       PROCESS_FLAG,
       COMMIT_MAX_CTR,
       LOCK_WAIT_TIME,
       RETRY_MAX_CTR)
    values
      ('xxadeo_costprice_dnld',
       'Prix Achat ',
       'NONE',
       1,
       'Y',
       'T',
       5000,
       10,
       3);
  end if;

  -- xxadeo_deals_dnld
  Select count(*)
    into L_count
    from RESTART_CONTROL
   WHERE program_name = 'xxadeo_deals_dnld';

  if L_count = 0 then
    insert into restart_control
      (PROGRAM_NAME,
       PROGRAM_DESC,
       DRIVER_NAME,
       NUM_THREADS,
       UPDATE_ALLOWED,
       PROCESS_FLAG,
       COMMIT_MAX_CTR,
       LOCK_WAIT_TIME,
       RETRY_MAX_CTR)
    values
      ('xxadeo_deals_dnld',
       'Deals extract which downloads the data to multiple files (one per BU) in ADEO format',
       'NONE',
       1,
       'Y',
       'T',
       100,
       10,
       3);
  end if;

  -- xxadeo_ecotax_dnld
  Select count(*)
    into L_count
    from RESTART_CONTROL
   WHERE program_name = 'xxadeo_ecotax_dnld';

  if L_count = 0 then
    insert into restart_control
      (PROGRAM_NAME,
       PROGRAM_DESC,
       DRIVER_NAME,
       NUM_THREADS,
       UPDATE_ALLOWED,
       PROCESS_FLAG,
       COMMIT_MAX_CTR,
       LOCK_WAIT_TIME,
       RETRY_MAX_CTR)
    values
      ('xxadeo_ecotax_dnld',
       'ADEO Ecotax extract',
       'NONE',
       4,
       'Y',
       'T',
       100,
       10,
       3);
  end if;

  -- xxadeo_ecotax_dnld_pre
  Select count(*)
    into L_count
    from RESTART_CONTROL
   WHERE program_name = 'xxadeo_ecotax_dnld_pre';

  if L_count = 0 then
    insert into restart_control
      (PROGRAM_NAME,
       PROGRAM_DESC,
       DRIVER_NAME,
       NUM_THREADS,
       UPDATE_ALLOWED,
       PROCESS_FLAG,
       COMMIT_MAX_CTR,
       LOCK_WAIT_TIME,
       RETRY_MAX_CTR)
    values
      ('xxadeo_ecotax_dnld_pre',
       'ADEO Ecotax extract prepare',
       'NONE',
       1,
       'Y',
       'T',
       100,
       10,
       3);
  end if;

  -- xxadeo_merchhier_dnld
  Select count(*)
    into L_count
    from RESTART_CONTROL
   WHERE program_name = 'xxadeo_merchhier_dnld';

  if L_count = 0 then
    insert into restart_control
      (PROGRAM_NAME,
       PROGRAM_DESC,
       DRIVER_NAME,
       NUM_THREADS,
       UPDATE_ALLOWED,
       PROCESS_FLAG,
       COMMIT_MAX_CTR,
       LOCK_WAIT_TIME,
       RETRY_MAX_CTR)
    values
      ('xxadeo_merchhier_dnld',
       'Merchandise Hierarchy extract which downloads the data to a file in ADEO format',
       'NONE',
       1,
       'Y',
       'T',
       100,
       10,
       3);
  end if;

  -- xxadeo_supsites_dnld
  Select count(*)
    into L_count
    from RESTART_CONTROL
   WHERE program_name = 'xxadeo_supsites_dnld';

  if L_count = 0 then
    insert into restart_control
      (PROGRAM_NAME,
       PROGRAM_DESC,
       DRIVER_NAME,
       NUM_THREADS,
       UPDATE_ALLOWED,
       PROCESS_FLAG,
       COMMIT_MAX_CTR,
       LOCK_WAIT_TIME,
       RETRY_MAX_CTR)
    values
      ('xxadeo_supsites_dnld',
       'ADEO suppliers sites extract',
       'NONE',
       1,
       'Y',
       'T',
       100,
       10,
       3);
  end if;

  -- restart_program_status

-- xxadeo_item_lfcy
  select count(*)
    into L_count
    from RESTART_PROGRAM_STATUS
   WHERE restart_name = 'xxadeo_item_lfcy';

  if L_count = 0 then
    insert into restart_program_status
      (RESTART_NAME, THREAD_VAL, PROGRAM_NAME, PROGRAM_STATUS)
    values
      ('xxadeo_item_lfcy',
       1,
       'xxadeo_item_lfcy',
       'ready for start');
  else
    UPDATE RESTART_PROGRAM_STATUS
       SET PROGRAM_STATUS = 'ready for start'
     WHERE restart_name = 'xxadeo_item_lfcy';
  
  end if;

  -- xxadeo_merchhier_dnld
  select count(*)
    into L_count
    from RESTART_PROGRAM_STATUS
   WHERE restart_name = 'xxadeo_merchhier_dnld';

  if L_count = 0 then
    insert into restart_program_status
      (RESTART_NAME, THREAD_VAL, PROGRAM_NAME, PROGRAM_STATUS)
    values
      ('xxadeo_merchhier_dnld',
       1,
       'xxadeo_merchhier_dnld',
       'ready for start');
  else
    UPDATE RESTART_PROGRAM_STATUS
       SET PROGRAM_STATUS = 'ready for start'
     WHERE restart_name = 'xxadeo_merchhier_dnld';
  
  end if;

  -- xxadeo_ecotax_dnld_pre
  select count(*)
    into L_count
    from RESTART_PROGRAM_STATUS
   WHERE restart_name = 'xxadeo_ecotax_dnld_pre';

  if L_count = 0 then
    insert into restart_program_status
      (RESTART_NAME, THREAD_VAL, PROGRAM_NAME, PROGRAM_STATUS)
    values
      ('xxadeo_ecotax_dnld_pre',
       1,
       'xxadeo_ecotax_dnld_pre',
       'ready for start');
  else
    UPDATE RESTART_PROGRAM_STATUS
       SET PROGRAM_STATUS = 'ready for start'
     WHERE restart_name = 'xxadeo_ecotax_dnld_pre';
  end if;

  -- xxadeo_ecotax_dnld
  select count(*)
    into L_count
    from RESTART_PROGRAM_STATUS
   WHERE restart_name = 'xxadeo_ecotax_dnld';

  if L_count = 0 then
    for i in 1 .. 4 loop
      insert into restart_program_status
        (RESTART_NAME, THREAD_VAL, PROGRAM_NAME, PROGRAM_STATUS)
      values
        ('xxadeo_ecotax_dnld', i, 'xxadeo_ecotax_dnld', 'ready for start');
    end loop;
  else
    UPDATE RESTART_PROGRAM_STATUS
       SET PROGRAM_STATUS = 'ready for start'
     WHERE restart_name = 'xxadeo_ecotax_dnld';
  end if;

  -- xxadeo_pricechange_dnld
  select count(*)
    into L_count
    from RESTART_PROGRAM_STATUS
   WHERE restart_name = 'xxadeo_pricechange_dnld';

  if L_count = 0 then
    insert into restart_program_status
      (RESTART_NAME, THREAD_VAL, PROGRAM_NAME, PROGRAM_STATUS)
    values
      ('xxadeo_pricechange_dnld',
       1,
       'xxadeo_pricechange_dnld',
       'ready for start');
  else
    UPDATE RESTART_PROGRAM_STATUS
       SET PROGRAM_STATUS = 'ready for start'
     WHERE restart_name = 'xxadeo_pricechange_dnld';
  end if;

  -- xxadeo_deals_dnld
  select count(*)
    into L_count
    from RESTART_PROGRAM_STATUS
   WHERE restart_name = 'xxadeo_deals_dnld';

  if L_count = 0 then
    insert into restart_program_status
      (RESTART_NAME, THREAD_VAL, PROGRAM_NAME, PROGRAM_STATUS)
    values
      ('xxadeo_deals_dnld', 1, 'xxadeo_deals_dnld', 'ready for start');
  else
    UPDATE RESTART_PROGRAM_STATUS
       SET PROGRAM_STATUS = 'ready for start'
     WHERE restart_name = 'xxadeo_deals_dnld';
  end if;

  -- xxadeo_costprice_dnld
  select count(*)
    into L_count
    from RESTART_PROGRAM_STATUS
   WHERE restart_name = 'xxadeo_costprice_dnld';

  if L_count = 0 then
    insert into restart_program_status
      (RESTART_NAME, THREAD_VAL, PROGRAM_NAME, PROGRAM_STATUS)
    values
      ('xxadeo_costprice_dnld',
       1,
       'xxadeo_costprice_dnld',
       'ready for start');
  else
    UPDATE RESTART_PROGRAM_STATUS
       SET PROGRAM_STATUS = 'ready for start'
     WHERE restart_name = 'xxadeo_costprice_dnld';
  end if;

  -- xxadeo_supsites_dnld
  select count(*)
    into L_count
    from RESTART_PROGRAM_STATUS
   WHERE restart_name = 'xxadeo_supsites_dnld';

  if L_count = 0 then
    insert into restart_program_status
      (RESTART_NAME, THREAD_VAL, PROGRAM_NAME, PROGRAM_STATUS)
    values
      ('xxadeo_supsites_dnld',
       1,
       'xxadeo_supsites_dnld',
       'ready for start');
  else
    UPDATE RESTART_PROGRAM_STATUS
       SET PROGRAM_STATUS = 'ready for start'
     WHERE restart_name = 'xxadeo_supsites_dnld';
  end if;

end;
/