/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_TEMPLATE" table for RB116                        */
/******************************************************************************/
delete from s9t_tmpl_cols_def_tl where template_key in ('XXADEO_ITEM_MASTER');
delete from s9t_tmpl_cols_def where template_key in ('XXADEO_ITEM_MASTER');
--
delete from s9t_tmpl_wksht_def_tl where template_key in ('XXADEO_ITEM_MASTER');
delete from s9t_tmpl_wksht_def where template_key in ('XXADEO_ITEM_MASTER');
--
delete from s9t_template_tl where template_key in ('XXADEO_ITEM_MASTER');
delete from s9t_template where template_key in ('XXADEO_ITEM_MASTER');
--
update s9t_template
   set template_type = NULL
 where template_key = 'ITEM_MASTER_DATA';
 
insert into s9t_template (TEMPLATE_KEY, TEMPLATE_NAME, TEMPLATE_DESC, FILE_ID, CREATE_ID, CREATE_DATETIME, TEMPLATE_TYPE, TEMPLATE_CATEGORY)
values ('XXADEO_ITEM_MASTER', 'Adeo Item Master', 'Adeo Item Master', 3, user, sysdate, 'IS9M', 'IS9T');


 