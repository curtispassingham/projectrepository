--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Felipe Villa/Elsa Barros                                     */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "RTK_ERRORS" table for RB115                          */
/******************************************************************************/
--------------------------------------------------------------------------------
-- Delete
delete RTK_ERRORS where RTK_KEY = 'INV_DOMAIN';
delete RTK_ERRORS where RTK_KEY = 'DOMAIN_DEPT_EXISTS';
delete RTK_ERRORS where RTK_KEY = 'DOMAIN_CLASS_EXISTS';
delete RTK_ERRORS where RTK_KEY = 'DOMAIN_SUBCLASS_EXISTS';
delete RTK_ERRORS where RTK_KEY = 'ACT_CLASS_NOT_DEPT';
delete RTK_ERRORS where RTK_KEY = 'ACT_SCLASS_NOT_CLASS';
delete RTK_ERRORS where RTK_KEY = 'DEACT_DEPT_ACT_CLASS';
delete RTK_ERRORS where RTK_KEY = 'DEACT_CLASS_ACT_SCLASS';
delete RTK_ERRORS where RTK_KEY = 'DEACT_SCLASS_WITH_ITEM';
--
-- XXADEO
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_DT_TEXT';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_DT_DATE';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_DT_NUMBER';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_GROUP_NAME';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_DEPT_NAME';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_CLASS_NAME';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_SCLASS_NAME';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_MAND_VALUE';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_HAS_ROWS';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_DEPT_TRANS';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_CLASS_TRANS';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_SCLASS_TRANS';
--
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_DEFAULT';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_ACTION_STATUS';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_CLASS';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_SUBCLASS';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_STATUS';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_DOMAIN_NAME';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_INV_LOAD_SLS_IND';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_REC_NOT_EXISTS';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_DEPT_TL_EXISTS';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_CLASS_TL_EXISTS';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_SUBCLASS_TL_EXISTS';
delete RTK_ERRORS where RTK_KEY = 'XXADEO_VAT_DEPT_EXISTS';
--
-- Insert
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'INV_DOMAIN', 'Invalid Domain.', USER, 'Y');

insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'DOMAIN_DEPT_EXISTS', 'Domain-dept association exists already.', USER, 'Y');

insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'DOMAIN_CLASS_EXISTS', 'Domain-class association exists already.', USER, 'Y');

insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'DOMAIN_SUBCLASS_EXISTS', 'Domain-subclass association exists already.', USER, 'Y');

insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'ACT_CLASS_NOT_DEPT', 'Dept must be active to activate class.', USER, 'Y');

insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'ACT_SCLASS_NOT_CLASS', 'Class must be active to activate subclass.', USER, 'Y');

insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'DEACT_DEPT_ACT_CLASS', 'Cannot deactivate dept with active class.', USER, 'Y');

insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'DEACT_CLASS_ACT_SCLASS', 'Cannot deactivate class with active subclass.', USER, 'Y');

insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'DEACT_SCLASS_WITH_ITEM', 'Cannot deactivate a subclass with items.', USER, 'Y');
--
-- XXADEO
insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_DT_TEXT', 'Invalid alpha-numeric entered.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_DT_DATE', 'Invalid date entered.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_DT_NUMBER', 'Invalid number entered.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_GROUP_NAME', 'Invalid Group Name.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_DEPT_NAME', 'Invalid Dept Name.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_CLASS_NAME', 'Invalid Class Name.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_SCLASS_NAME', 'Invalid Subclass Name.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_MAND_VALUE', 'Mandatory field - value must be entered.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_HAS_ROWS', 'Spreadsheet cannot be empty.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_DEPT_TRANS', 'Invalid translation for a dept.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_CLASS_TRANS', 'Invalid translation for a class.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_SCLASS_TRANS', 'Invalid translation for a subclass.', USER, 'Y');
--
-- 
--
insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_DEFAULT', 'Invalid default value.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_ACTION_STATUS', 'To inactivate a Department/Class/Subclass use the action UPDATE. To initialize the status for a Department/Class/Subclass as being prepared use the action CREATE.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_CLASS', 'Invalid Class.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_SUBCLASS', 'Invalid Subclass.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_STATUS', 'Invalid status for this Department/Class/Subclass.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_DOMAIN_NAME', 'Invalid Domain Name.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_INV_LOAD_SLS_IND', 'Invalid Load Sales Indicator.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_REC_NOT_EXISTS', 'Record does not exists.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_DEPT_TL_EXISTS', 'Department-Lang association already exists.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_CLASS_TL_EXISTS', 'Translation already exists for the Department/Class combination.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUBCLASS_TL_EXISTS', 'Translation already exists for the Department/Class/Subclass combination.', USER, 'Y');

insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_VAT_DEPT_EXISTS', 'VAT-Dept association already exists.', USER, 'Y');