--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_RPM_CUSTOM_OPTIONS" table for RB34a           */
/******************************************************************************/
--------------------------------------------------------------------------------
delete
 from xxadeo_rpm_custom_options
where retention_control_id = 1
  and price_change_type    = 'PC';
--
insert into xxadeo_rpm_custom_options (RETENTION_CONTROL_ID, PRICE_CHANGE_TYPE, PROCESSED_PC_RETENTION_DAYS, ERROR_PC_RETENTION_DAYS, PC_PROCESS_MAX_REQUEST_SIZE, PC_BLOCKING_CFA_VALUE)
values (1, 'PC', null, null, null, 'N');