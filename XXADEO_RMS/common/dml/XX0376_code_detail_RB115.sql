/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "CODE_DETAIL" table for RB115                         */
/******************************************************************************/
--
delete from code_detail where code_type IN ('ADVT','ADLG','ADST');
--
insert into code_detail(code_type, code, code_desc, required_ind, code_seq)
values('ADVT', 'B', 'Both', 'Y', 1);
--
insert into code_detail(code_type, code, code_desc, required_ind, code_seq)
values('ADVT', 'C', 'Cost', 'Y', 2);
--
insert into code_detail(code_type, code, code_desc, required_ind, code_seq)
values('ADVT', 'R', 'Retail', 'Y', 3);

--
-- language
--
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('ADLG', '1', 'English', 'Y', 1);
--
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('ADLG', '20', 'Greek', 'Y', 2);
--
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('ADLG', '22', 'Italian', 'Y', 3);
--
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('ADLG', '26', 'Polish', 'Y', 4);
--
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('ADLG', '27', 'Portuguese', 'Y', 5);
--
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('ADLG', '3', 'French', 'Y', 6);
--
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('ADLG', '4', 'Spanish', 'Y', 7);
--
-- CUSTOM ATTRIB
--
insert into code_detail(code_type,code,code_desc,required_ind,code_seq)
values('ADCA','MHDS','11','Y',1);

insert into code_detail(code_type,code,code_desc,required_ind,code_seq)
values('ADCA','MHCS','21','Y',2);

insert into code_detail(code_type,code,code_desc,required_ind,code_seq)
values('ADCA','MHSS','31','Y',3);
--
-- Load Sales Indicator
--
insert into code_detail(code_type,code,code_desc,required_ind,code_seq)
values('ADSI','Y','Y','Y',1);

insert into code_detail(code_type,code,code_desc,required_ind,code_seq)
values('ADSI','N','N','Y',2);
--
-- VAT_CODE
--
insert into code_detail
  (code_type, code, code_desc, required_ind, code_seq)
  (select 'ADVC', VAT_CODE, VAT_CODE, 'Y', rownum
     from vat_codes);