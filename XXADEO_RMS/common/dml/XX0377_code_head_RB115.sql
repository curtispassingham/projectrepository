/******************************************************************************/
/* CREATE DATE - June 2018                                                    */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "CODE_HEAD" table for RB115                           */
/******************************************************************************/
--
delete from code_detail_tl where code_type IN ('MHST');
--
delete from code_detail where code_type IN ('MHST');
--
delete from code_head where code_type IN ('MHST');
--
-- Merch Hier Status
--
insert into code_head (code_type, code_type_desc)
values ('MHST','CFA Merch Hier Status');
