--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "RTK_ERRORS_TL" table for RB129                       */
/******************************************************************************/
--------------------------------------------------------------------------------
-- Delete
delete from RTK_ERRORS_TL where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG1' and lang in (1,3);
delete from RTK_ERRORS_TL where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG2' and lang in (1,3);
delete from RTK_ERRORS_TL where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG3' and lang in (1,3);
delete from RTK_ERRORS_TL where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG4' and lang in (1,3);
delete from RTK_ERRORS_TL where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG5' and lang in (1,3);
delete from RTK_ERRORS_TL where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG6' and lang in (1,3);
--
-- Insert
-- RG1
insert into RTK_ERRORS_TL (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (1, 'XXADEO_SUP_CHG_STATUS_RG1', 'Parent Suppliers can only be created in the Financial System and not in RMS, this action is not allowed.', SYSDATE, USER, SYSDATE, USER);
insert into RTK_ERRORS_TL (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (3, 'XXADEO_SUP_CHG_STATUS_RG1', 'Les ent�tes fournisseurs ne peuvent pas �tre cr��s dans RMS mais seulement en Finance: l�activation est bloqu�e.', SYSDATE, USER, SYSDATE, USER);
--
-- RG2
insert into RTK_ERRORS_TL (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (1, 'XXADEO_SUP_CHG_STATUS_RG2', 'Parent Supplier status can only be edited in the Financial System and not in RMS, this action is not allowed.', SYSDATE, USER, SYSDATE, USER);
insert into RTK_ERRORS_TL (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (3, 'XXADEO_SUP_CHG_STATUS_RG2', 'Le statut de l�ent�te fournisseur n''est pas modifiable dans RMS car il est g�r� en Finance.', SYSDATE, USER, SYSDATE, USER);
--
-- RG3
insert into RTK_ERRORS_TL (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (1, 'XXADEO_SUP_CHG_STATUS_RG3', 'Payment Supplier-Site status cannot be edited, it must remain in inactive status.', SYSDATE, USER, SYSDATE, USER);
insert into RTK_ERRORS_TL (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (3, 'XXADEO_SUP_CHG_STATUS_RG3', 'Le statut du site-fournisseur de r�glement n''est pas modifiable dans RMS car il doit rester au statut Inactif.', SYSDATE, USER, SYSDATE, USER);
--
-- RG4
insert into RTK_ERRORS_TL (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (1, 'XXADEO_SUP_CHG_STATUS_RG4', 'The supplier-site cannot be activated because it is missing an order address.', SYSDATE, USER, SYSDATE, USER);
insert into RTK_ERRORS_TL (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (3, 'XXADEO_SUP_CHG_STATUS_RG4', 'Le site-fournisseur ne peut pas �tre activ� car il ne poss�de pas d''adresse de commande.', SYSDATE, USER, SYSDATE, USER);
--
-- RG5
insert into RTK_ERRORS_TL (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (1, 'XXADEO_SUP_CHG_STATUS_RG5', 'The supplier-site cannot be activated because there is no payment supplier-site associated with one of its organisational units.', SYSDATE, USER, SYSDATE, USER);
insert into RTK_ERRORS_TL (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (3, 'XXADEO_SUP_CHG_STATUS_RG5', 'Le site-fournisseur ne peut pas �tre activ� car il n''existe pas de site-fournisseur de r�glement pour l''une des unit�s organisationnelles associ�e.', SYSDATE, USER, SYSDATE, USER);
--
-- RG6
insert into RTK_ERRORS_TL (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (1, 'XXADEO_SUP_CHG_STATUS_RG6', 'Payment terms for Payment Supplier Site in the same Organizational Unit for same supplier parent are different from the payment terms of the ordering site to be activated.', SYSDATE, USER, SYSDATE, USER);
insert into RTK_ERRORS_TL (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (3, 'XXADEO_SUP_CHG_STATUS_RG6', 'Les conditions de paiement du site fournisseur de paiement dans la m�me Unit� Organisationnelle du m�me ent�te fournisseur sont diff�rentes de celle du site fournisseur d�achat � activer.', SYSDATE, USER, SYSDATE, USER);
