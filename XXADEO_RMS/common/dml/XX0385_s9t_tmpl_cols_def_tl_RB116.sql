/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_TMPL_COLS_DEF_TL" table for RB116                */
/******************************************************************************/
--
delete from s9t_tmpl_cols_def_tl where template_key = 'XXADEO_ITEM_MASTER';
--
insert into s9t_tmpl_cols_def_tl
  (template_key, wksht_key, column_key, lang, column_name)
  (select 'XXADEO_ITEM_MASTER', 
          wksht_key, 
          column_key, 
          lang, 
          column_name
     from s9t_tmpl_cols_def_tl scd
    where scd.template_key = 'ITEM_MASTER_DATA'
      and exists (select 1
             from s9t_tmpl_cols_def cd
            where cd.template_key = 'XXADEO_ITEM_MASTER'
              and cd.wksht_key = scd.wksht_key
              and cd.column_key = scd.column_key));
     