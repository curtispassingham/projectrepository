--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of CODE_DETAIL table for RB34a                           */
/******************************************************************************/
--------------------------------------------------------------------------------
--
delete code_detail where code_type = 'RSCD';
--
insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('RSCD', 'RERPO', 'RPOREGPC', 'Y', 1);

insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('RSCD', 'EMRPM', 'RPMEMEPC', 'Y', 2);

insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('RSCD', 'RESTPO', 'STPREGPC', 'Y', 3);

insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('RSCD', 'EMSTPO', 'STPEMEPC', 'Y', 4);

insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('RSCD', 'RESIMT', 'SIMTARPC', 'Y', 5);

insert into code_detail (CODE_TYPE, CODE, CODE_DESC, REQUIRED_IND, CODE_SEQ)
values ('RSCD', 'CONVER', 'CONVERPC', 'Y', 6);
--