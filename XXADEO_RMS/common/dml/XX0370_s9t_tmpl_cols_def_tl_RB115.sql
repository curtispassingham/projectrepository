/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_TMPL_COLS_DEF_TL" table for RB115                */
/******************************************************************************/

delete from s9t_tmpl_cols_def_tl 
      where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS');
--
-- XXADEO_DEPT_LEVEL_VAT
--

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','VAT_REGION',1,'VAT Region');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','VAT_REGION',3,'R�gion TVA');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','GROUP_ID',1,'Group ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','GROUP_ID',3,'ID rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','GROUP_NAME',1,'Group Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','GROUP_NAME',3,'Nom rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','DEPT',1,'Department');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','DEPT',3,'Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','DEPT_NAME',1,'Department Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','DEPT_NAME',3,'Nom Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','VAT_TYPE',1,'VAT Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','VAT_TYPE',3,'Type TVA');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','VAT_CODE',1,'VAT Code');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT','VAT_CODE',3,'Code TVA');
--
-- MERCHANDISE HIERARCHY
--
insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','DEPARTMENT_TRANSLATIONS','GROUP_ID',1,'Group ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','DEPARTMENT_TRANSLATIONS','GROUP_ID',3,'ID rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','DEPARTMENT_TRANSLATIONS','GROUP_NAME',1,'Group Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','DEPARTMENT_TRANSLATIONS','GROUP_NAME',3,'Nom rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','DEPARTMENT_TRANSLATIONS','DEPT',1,'Department');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','DEPARTMENT_TRANSLATIONS','DEPT',3,'Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','DEPARTMENT_TRANSLATIONS','DEPT_NAME',1,'Department Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','DEPARTMENT_TRANSLATIONS','DEPT_NAME',3,'Nom Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','DEPARTMENT_TRANSLATIONS','DEPT_TRANSLATION',1,'Department Translation');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','DEPARTMENT_TRANSLATIONS','DEPT_TRANSLATION',3,'Traduction Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','GROUP_ID',1,'Group ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','GROUP_ID',3,'ID rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','GROUP_NAME',1,'Group Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','GROUP_NAME',3,'Nom rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','DEPT',1,'Department');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','DEPT',3,'Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','DEPT_NAME',1,'Department Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','DEPT_NAME',3,'Nom Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','CLASS',1,'Class');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','CLASS',3,'Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','CLASS_NAME',1,'Class Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','CLASS_NAME',3,'Nom Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','CLASS_TRANSLATION',1,'Class Translation');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS','CLASS_TRANSLATION',3,'Traduction Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','GROUP_ID',1,'Group ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','GROUP_ID',3,'ID rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','GROUP_NAME',1,'Group Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','GROUP_NAME',3,'Nom rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','DEPT',1,'Department');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','DEPT',3,'Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','DEPT_NAME',1,'Department Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','DEPT_NAME',3,'Nom Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','CLASS',1,'Class');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','CLASS',3,'Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','CLASS_NAME',1,'Class Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','CLASS_NAME',3,'Nom Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','SUBCLASS',1,'Subclass');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','SUBCLASS',3,'Sous-type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','SUBCLASS_NAME',1,'Subclass Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','SUBCLASS_NAME',3,'Nom Sous-type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','SUBCLASS_TRANSLATION',1,'Subclass Translation');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS','SUBCLASS_TRANSLATION',3,'Traduction Sous-type');
--
-- XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB
--
insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','DEPARTMENT_STATUS','GROUP_ID',1,'Group ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','DEPARTMENT_STATUS','GROUP_ID',3,'ID rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','DEPARTMENT_STATUS','GROUP_NAME',1,'Group Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','DEPARTMENT_STATUS','GROUP_NAME',3,'Nom rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','DEPARTMENT_STATUS','DEPT',1,'Department');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','DEPARTMENT_STATUS','DEPT',3,'Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','DEPARTMENT_STATUS','DEPT_NAME',1,'Department Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','DEPARTMENT_STATUS','DEPT_NAME',3,'Nom Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','DEPARTMENT_STATUS','DEPT_STATUS',1,'Department Status');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','DEPARTMENT_STATUS','DEPT_STATUS',3,'Statut sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','GROUP_ID',1,'Group ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','GROUP_ID',3,'ID rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','GROUP_NAME',1,'Group Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','GROUP_NAME',3,'Nom rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','DEPT',1,'Department');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','DEPT',3,'Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','DEPT_NAME',1,'Department Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','DEPT_NAME',3,'Nom Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','CLASS',1,'Class');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','CLASS',3,'Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','CLASS_NAME',1,'Class Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','CLASS_NAME',3,'Nom Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','CLASS_STATUS',1,'Class Status');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS','CLASS_STATUS',3,'Statut Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','GROUP_ID',1,'Group ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','GROUP_ID',3,'ID rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','GROUP_NAME',1,'Group Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','GROUP_NAME',3,'Nom rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','DEPT',1,'Department');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','DEPT',3,'Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','DEPT_NAME',1,'Department Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','DEPT_NAME',3,'Nom Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','CLASS',1,'Class');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','CLASS',3,'Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','CLASS_NAME',1,'Class Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','CLASS_NAME',3,'Nom Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','SUBCLASS',1,'Subclass');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','SUBCLASS',3,'Sous-type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','SUBCLASS_NAME',1,'Subclass Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','SUBCLASS_NAME',3,'Nom Sous-type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','SUBCLASS_STATUS',1,'Subclass Status');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS','SUBCLASS_STATUS',3,'Statut Sous-type');
--
-- XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS
--
insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS','DOMAIN_ID',1,'Domain ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS','DOMAIN_ID',3,'ID Domaine');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS','DOMAIN_NAME',1,'Domain Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS','DOMAIN_NAME',3,'Nom domaine');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','GROUP_ID',1,'Group ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','GROUP_ID',3,'ID rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','GROUP_NAME',1,'Group Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','GROUP_NAME',3,'Nom rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','DEPT',1,'Department');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','DEPT',3,'Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','DEPT_NAME',1,'Department Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','DEPT_NAME',3,'Nom Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','DOMAIN_ID',1,'Domain ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','DOMAIN_ID',3,'ID Domaine');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','LOAD_SALES_IND',1,'Load Sales Indicator');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','LOAD_SALES_IND',3,'Extraire ventes');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','GROUP_ID',1,'Group ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','GROUP_ID',3,'ID rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','GROUP_NAME',1,'Group Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','GROUP_NAME',3,'Nom rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','DEPT',1,'Department');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','DEPT',3,'Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','DEPT_NAME',1,'Department Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','DEPT_NAME',3,'Nom Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','CLASS',1,'Class');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','CLASS',3,'Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','CLASS_NAME',1,'Class Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','CLASS_NAME',3,'Nom Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','DOMAIN_ID',1,'Domain ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','DOMAIN_ID',3,'ID Domaine');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','LOAD_SALES_IND',1,'Load Sales Indicator');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS','LOAD_SALES_IND',3,'Extraire ventes');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','GROUP_ID',1,'Group ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','GROUP_ID',3,'ID rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','GROUP_NAME',1,'Group Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','GROUP_NAME',3,'Nom rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','DEPT',1,'Department');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','DEPT',3,'Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','DEPT_NAME',1,'Department Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','DEPT_NAME',3,'Nom Sous-rayon');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','CLASS',1,'Class');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','CLASS',3,'Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','CLASS_NAME',1,'Class Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','CLASS_NAME',3,'Nom Type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','SUBCLASS',1,'Subclass');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','SUBCLASS',3,'Sous-type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','SUBCLASS_NAME',1,'Subclass Name');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','SUBCLASS_NAME',3,'Nom Sous-type');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','DOMAIN_ID',1,'Domain ID');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','DOMAIN_ID',3,'ID Domaine');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','LOAD_SALES_IND',1,'Load Sales Indicator');

insert into s9t_tmpl_cols_def_tl(template_key,wksht_key,column_key,lang,column_name)
values ('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS','LOAD_SALES_IND',3,'Extraire ventes');

