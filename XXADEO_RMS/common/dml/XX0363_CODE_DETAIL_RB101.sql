--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Paulo Mamede                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of CODE_DETAIL table for RB101                           */
/******************************************************************************/
--------------------------------------------------------------------------------
--
delete code_detail where code_type = 'BUID';
--
insert into code_detail(code_type,code,code_desc,required_ind,code_seq) values ('BUID',750,'BU UDA ID of the Business Unit Subscription','Y',1);

--
