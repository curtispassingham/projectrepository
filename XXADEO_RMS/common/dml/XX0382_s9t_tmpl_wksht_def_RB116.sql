/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_TMPL_WKSHT_DEF" table for RB116                  */
/******************************************************************************/
--
delete from s9t_tmpl_cols_def_tl where template_key in ('XXADEO_ITEM_MASTER');
delete from s9t_tmpl_cols_def where template_key in ('XXADEO_ITEM_MASTER');
--
delete from s9t_tmpl_wksht_def_tl where template_key in ('XXADEO_ITEM_MASTER');
delete from s9t_tmpl_wksht_def where template_key in ('XXADEO_ITEM_MASTER');
--
insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_IMAGE_TL', 'Item_Image_Translations', 'N', 0);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_MASTER_TL', 'Item_Master_Translations', 'N', 0);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_SUPPLIER_TL', 'Item_Supplier_Translations', 'N', 0);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_XFORM_HEAD_TL', 'Item_Transformation_Trans', 'N', 0);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_MASTER', 'Item_Master', 'Y', 1);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_SUPPLIER', 'Item_Suppliers', 'Y', 2);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_SUPP_COUNTRY_DIM', 'Item_Supp_Ctry_Dimensions', 'N', 4);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_SUPP_COUNTRY', 'Item_Supplier_Countries', 'Y', 3);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_SUPP_MANU_COUNTRY', 'Item_Supp_Manu_Country', 'N', 5);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_SUPP_UOM', 'Item_Supplier_UOMs', 'N', 6);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_XFORM_HEAD', 'Item_Transformation', 'N', 7);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_XFORM_DETAIL', 'Item_Transformation_Details', 'N', 8);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'PACKITEM', 'Pack_Items', 'N', 9);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'RPM_ITEM_ZONE_PRICE', 'Retail_by_Zone', 'N', 10);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'VAT_ITEM', 'Item_VAT', 'N', 11);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'UDA_ITEM_LOV', 'Item_Lov_UDAs', 'Y', 12);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'UDA_ITEM_DATE', 'Item_Date_UDAs', 'Y', 13);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'UDA_ITEM_FF', 'Item_Free_Form_UDAs', 'Y', 14);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'ITEM_IMAGE', 'Item_Images', 'N', 15);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'V_ITEM_AGS1', 'V_ITEM_AGS1', 'Y', 16);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'V_ITEM_AGS2', 'V_ITEM_AGS2', 'Y', 17);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'V_ITEM_AGS3', 'V_ITEM_AGS3', 'Y', 18);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'V_ITEM_AGS4', 'V_ITEM_AGS4', 'Y', 19);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'V_ITEM_AGS5', 'V_ITEM_AGS5', 'Y', 20);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'V_ITEM_AGS6', 'V_ITEM_AGS6', 'Y', 21);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'V_ITEM_AGS7', 'V_ITEM_AGS7', 'Y', 22);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'V_ITEM_AGS8', 'V_ITEM_AGS8', 'Y', 23);

insert into s9t_tmpl_wksht_def (TEMPLATE_KEY, WKSHT_KEY, WKSHT_NAME, MANDATORY, SEQ_NO)
values ('XXADEO_ITEM_MASTER', 'V_ITEM_SUPP_AGS1', 'V_ITEM_SUPP_AGS1', 'Y', 24); 
 
 