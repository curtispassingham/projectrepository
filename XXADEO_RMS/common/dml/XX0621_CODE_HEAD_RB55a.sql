
-- CREATE DATE - Jan 2019
-- CREATE USER - Pedro Miranda
-- PROJECT     - ADEO
-- DESCRIPTION - Identify Code 48 Items
--
delete code_detail where code_type = 'RC48';

delete code_head where code_type = 'RC48';
--
insert into code_head (CODE_TYPE, CODE_TYPE_DESC)
values ('RC48', 'Prevent ranging to cascade down to SKUs for Code 48 items');
--
