--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of s9t_tmpl_wksht_def and s9t_tmpl_cols_def for RB116    */
/******************************************************************************/
--------------------------------------------------------------------------------
-- 
delete from s9t_tmpl_cols_def_tl
 where template_key = 'XXADEO_ITEM_MASTER'
   and wksht_key in ('ITEM_IMAGE',
                     'ITEM_IMAGE_TL',
                     'ITEM_SUPPLIER_TL',
                     'ITEM_MASTER_TL',
                     'ITEM_XFORM_HEAD_TL',
                     'ITEM_SUPP_COUNTRY_DIM',
                     'ITEM_SUPP_MANU_COUNTRY',
                     'ITEM_SUPP_UOM',
                     'ITEM_XFORM_HEAD',
                     'ITEM_XFORM_DETAIL',
                     'PACKITEM',
                     'RPM_ITEM_ZONE_PRICE',
                     'VAT_ITEM');
--
delete from s9t_tmpl_cols_def
 where template_key = 'XXADEO_ITEM_MASTER'
   and wksht_key in ('ITEM_IMAGE',
                     'ITEM_IMAGE_TL',
                     'ITEM_SUPPLIER_TL',
                     'ITEM_MASTER_TL',
                     'ITEM_XFORM_HEAD_TL',
                     'ITEM_SUPP_COUNTRY_DIM',
                     'ITEM_SUPP_MANU_COUNTRY',
                     'ITEM_SUPP_UOM',
                     'ITEM_XFORM_HEAD',
                     'ITEM_XFORM_DETAIL',
                     'PACKITEM',
                     'RPM_ITEM_ZONE_PRICE',
                     'VAT_ITEM');
--
delete from s9t_tmpl_wksht_def_tl
 where template_key = 'XXADEO_ITEM_MASTER'
   and wksht_key in ('ITEM_IMAGE',
                     'ITEM_IMAGE_TL',
                     'ITEM_SUPPLIER_TL',
                     'ITEM_MASTER_TL',
                     'ITEM_XFORM_HEAD_TL',
                     'ITEM_SUPP_COUNTRY_DIM',
                     'ITEM_SUPP_MANU_COUNTRY',
                     'ITEM_SUPP_UOM',
                     'ITEM_XFORM_HEAD',
                     'ITEM_XFORM_DETAIL',
                     'PACKITEM',
                     'RPM_ITEM_ZONE_PRICE',
                     'VAT_ITEM');
--
delete from s9t_tmpl_wksht_def
 where template_key = 'XXADEO_ITEM_MASTER'
   and mandatory = 'N'
   and wksht_key in ('ITEM_IMAGE',
                     'ITEM_IMAGE_TL',
                     'ITEM_SUPPLIER_TL',
                     'ITEM_MASTER_TL',
                     'ITEM_XFORM_HEAD_TL',
                     'ITEM_SUPP_COUNTRY_DIM',
                     'ITEM_SUPP_MANU_COUNTRY',
                     'ITEM_SUPP_UOM',
                     'ITEM_XFORM_HEAD',
                     'ITEM_XFORM_DETAIL',
                     'PACKITEM',
                     'RPM_ITEM_ZONE_PRICE',
                     'VAT_ITEM');
