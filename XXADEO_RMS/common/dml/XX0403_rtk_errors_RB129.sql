--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "RTK_ERRORS" table for RB129                          */
/******************************************************************************/
--------------------------------------------------------------------------------
-- Delete
delete from RTK_ERRORS_TL where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG1';
delete from RTK_ERRORS_TL where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG2';
delete from RTK_ERRORS_TL where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG3';
delete from RTK_ERRORS_TL where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG4';
delete from RTK_ERRORS_TL where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG5';
delete from RTK_ERRORS_TL where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG6';
--
delete from RTK_ERRORS where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG1';
delete from RTK_ERRORS where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG2';
delete from RTK_ERRORS where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG3';
delete from RTK_ERRORS where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG4';
delete from RTK_ERRORS where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG5';
delete from RTK_ERRORS where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG6';
--
-- Insert
-- RG1
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUP_CHG_STATUS_RG1', 'Parent Suppliers can only be created in the Financial System and not in RMS, this action is not allowed.', USER, 'Y');
-- RG2
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUP_CHG_STATUS_RG2', 'Parent Supplier status can only be edited in the Financial System and not in RMS, this action is not allowed.', USER, 'Y');
-- RG3
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUP_CHG_STATUS_RG3', 'Payment Supplier-Site status cannot be edited, it must remain in inactive status.', USER, 'Y');
-- RG4
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUP_CHG_STATUS_RG4', 'The supplier-site cannot be activated because it is missing an order address.', USER, 'Y');
-- RG5
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUP_CHG_STATUS_RG5', 'The supplier-site cannot be activated because there is no payment supplier-site associated with one of its organisational units.', USER, 'Y');
-- RG6
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUP_CHG_STATUS_RG6', 'Payment terms for Payment Supplier Site in the same Organizational Unit for same supplier parent are different from the payment terms of the ordering site to be activated.', USER, 'Y');
