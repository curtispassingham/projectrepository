--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "RTK_ERRORS" table for RB129                          */
/******************************************************************************/
--------------------------------------------------------------------------------
-- Delete
delete from RTK_ERRORS where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG1';
delete from RTK_ERRORS where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG2';
delete from RTK_ERRORS where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG3';
delete from RTK_ERRORS where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG4';
delete from RTK_ERRORS where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG5';
delete from RTK_ERRORS where rtk_key = 'XXADEO_SUP_CHG_STATUS_RG6';
--
-- Insert
-- RG1
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUP_CHG_STATUS_RG1', 'Cannot activate a supplier parent in RMS.', USER, 'Y');
-- RG2
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUP_CHG_STATUS_RG2', 'Cannot change the status of a supplier parent in RMS.', USER, 'Y');
-- RG3
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUP_CHG_STATUS_RG3', 'Cannot change the status of a payment supplier site in RMS.', USER, 'Y');
-- RG4
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUP_CHG_STATUS_RG4', 'Only order supplier site are activated in RMS.', USER, 'Y');
-- RG5
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUP_CHG_STATUS_RG5', 'Payment supplier site must exist before ordering site activated.', USER, 'Y');
-- RG6
insert into RTK_ERRORS (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'XXADEO_SUP_CHG_STATUS_RG6', 'Payment terms for payment site are different from terms of ordering site.', USER, 'Y');
