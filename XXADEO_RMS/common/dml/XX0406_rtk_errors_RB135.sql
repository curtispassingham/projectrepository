/* ****************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Paulo Mamede / Tiago Torres                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Install script for XXADEO scripts                            */
/* ************************************************************************** */

delete from rtk_errors_tl 
    where rtk_key in (
        'CFA_DEPARTMENT_SUP_NULL'
        ,'CFA_INV_PREV_ADMIN_ORG_UN'
        ,'CFA_INV_PREV_ADMIN_SUPP'
        ,'CFA_SUP_TYPE_NULL'
        ,'CFA_DUP_PREV_ADMIN_SUPP'
        ,'CFA_PORT_CHARGE'
        ,'CFA_PORT_CHARGE_AMNT'
        ,'CFA_PORT_CHARGE_VALUE'
        ,'CFA_GLN_INV_SUPP'
        ,'CFA_SUP_DEPT_TYPE_NULL'
        ,'CFA_SHIP_COST_BILL_SUPP'
    ); 

delete from rtk_errors 
    where rtk_key in (
        'CFA_DEPARTMENT_SUP_NULL'
        ,'CFA_INV_PREV_ADMIN_ORG_UN'
        ,'CFA_INV_PREV_ADMIN_SUPP'
        ,'CFA_SUP_TYPE_NULL'
        ,'CFA_DUP_PREV_ADMIN_SUPP'
        ,'CFA_PORT_CHARGE'
        ,'CFA_PORT_CHARGE_AMNT'
        ,'CFA_PORT_CHARGE_VALUE'
        ,'CFA_GLN_INV_SUPP'
        ,'CFA_SUP_DEPT_TYPE_NULL'
        ,'CFA_SHIP_COST_BILL_SUPP'
    );


-- insert rtk_errors
 
-- 1a
insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'CFA_INV_PREV_ADMIN_SUPP', 'Invalid supplier purchasing site entered into CFA Previous administrative supplier.', user, 'Y');

-- 1b
insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'CFA_INV_PREV_ADMIN_ORG_UN', 'Previous administrative supplier and new supplier are not associated to the same organizational unit.', user, 'Y');

-- 1?
insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'CFA_DUP_PREV_ADMIN_SUPP', 'Previous administrative supplier already exists.', user, 'Y');

-- 2a
insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'CFA_SUP_TYPE_NULL', 'Supplier type attribute mandatory for supplier purchasing site', user, 'Y');

-- 2b
insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'CFA_DEPARTMENT_SUP_NULL', 'Department attribute mandatory for supplier purchasing site', user, 'Y');

-- 2a + 2b
insert into rtk_errors (RTK_TYPE, RTK_KEY, RTK_TEXT, RTK_USER, RTK_APPROVED)
values ('BL', 'CFA_SUP_DEPT_TYPE_NULL', 'Supplier Type and Department attributes mandatory for purchasing supplier site.', user, 'Y');

-- 3
INSERT INTO rtk_errors(rtk_type,rtk_key,rtk_text,rtk_user,rtk_approved) 
values('BL','CFA_SHIP_COST_BILL_SUPP', 'If "Shipping costs Billing" is checked, "Unit Free Port", "Value Free Port" and "Freight Charge" must be filled.', user,'Y');

-- 3a - not used, replaced by 3
INSERT INTO rtk_errors(rtk_type,rtk_key,rtk_text,rtk_user,rtk_approved) 
values('BL','CFA_PORT_CHARGE','If "Shipping costs Billing" is due, "Unit free port" is mandatory',user,'Y');

-- 3b - not used, replaced by 3
INSERT INTO rtk_errors(rtk_type,rtk_key,rtk_text,rtk_user,rtk_approved) 
values('BL','CFA_PORT_CHARGE_AMNT','If "Shipping costs Billing" is due, "Freight charge" is mandatory',user,'Y');

-- 3c - not used, replaced by 3
INSERT INTO rtk_errors(rtk_type,rtk_key,rtk_text,rtk_user,rtk_approved) 
values('BL','CFA_PORT_CHARGE_VALUE','If the CFA “Shipping costs Billing” is checked, CFA “Value Free Port” must be filled.',user,'Y');

-- 4
INSERT INTO rtk_errors(rtk_type,rtk_key,rtk_text,rtk_user,rtk_approved) 
values('BL','CFA_GLN_INV_SUPP','Invalid Check Digit.',user,'Y');

