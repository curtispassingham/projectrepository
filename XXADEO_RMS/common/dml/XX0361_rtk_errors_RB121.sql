/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "RTK_ERRORS" table for RB121                          */
/******************************************************************************/
delete from rtk_errors where rtk_key in ('XXADEO_INV_ITEM_SUP','XXADEO_INV_ISUP_COUNTRY','XXADEO_INVALID_ITEM','XXADEO_INVALID_LOC');
--
--
insert into rtk_errors(rtk_type, rtk_key, rtk_text, rtk_user, rtk_approved)
values('BL', 'XXADEO_INV_ITEM_SUP', 'Item-supplier combination does not exists or supplier given is a Parent Supplier. ', user, 'Y');
--
insert into rtk_errors(rtk_type, rtk_key, rtk_text, rtk_user, rtk_approved)
values('BL', 'XXADEO_INV_ISUP_COUNTRY', 'Item-supplier-country of sourcing does not exist.', user, 'Y');
--
insert into rtk_errors(rtk_type, rtk_key, rtk_text, rtk_user, rtk_approved)
values('BL', 'XXADEO_INVALID_ITEM', 'Invalid item.', user, 'Y');
--
insert into rtk_errors(rtk_type, rtk_key, rtk_text, rtk_user, rtk_approved)
values('BL', 'XXADEO_INVALID_LOC', 'Invalid Location.', user, 'Y');