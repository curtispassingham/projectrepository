/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_TEMPLATE_TL" table for RB115                     */
/******************************************************************************/

delete from s9t_tmpl_cols_def_tl 
     where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
--
delete from s9t_tmpl_wksht_def_tl 
      where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
--
delete from s9t_template_tl 
      where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
--
insert into s9t_template_tl(template_key, lang, template_name, template_desc)
values('XXADEO_DEPT_LEVEL_VAT',1,'Department Level VAT','Department Level VAT');

insert into s9t_template_tl(template_key, lang, template_name, template_desc)
values('XXADEO_DEPT_LEVEL_VAT',3,'TVA niveau Sous-rayon','TVA niveau Sous-rayon');

insert into s9t_template_tl(template_key, lang, template_name, template_desc)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB',1,'Merchandise Hierarchy CFA Attributes','Merchandise Hierarchy CFA Attributes');

insert into s9t_template_tl(template_key, lang, template_name, template_desc)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB',3,'Attributs CFA Hiérarchie Marchandise','Attributs CFA Hiérarchie Marchandise');

insert into s9t_template_tl(template_key, lang, template_name, template_desc)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS',1,'Merchandise Hierarchy RDF Domains','Merchandise Hierarchy RDF Domains');

insert into s9t_template_tl(template_key, lang, template_name, template_desc)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS',3,'Domaines RDF Hiérarchie Marchandise','Domaines RDF Hiérarchie Marchandise');
