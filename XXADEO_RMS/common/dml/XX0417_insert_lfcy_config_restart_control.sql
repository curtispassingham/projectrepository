/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - restart control base config for Lifecycle batches            */
/******************************************************************************/
 
WHENEVER SQLERROR CONTINUE;

delete restart_program_status where restart_name in ('xxadeo_item_lfcy','xxadeo_item_lfcy_preproc');
delete restart_control where program_name in ('xxadeo_item_lfcy','xxadeo_item_lfcy_preproc');

insert into restart_control (program_name, program_desc, driver_name, num_threads, update_allowed, process_flag, commit_max_ctr, lock_wait_time, retry_max_ctr)
values ('xxadeo_item_lfcy', 'ADEO Item Lifecycle Extension Processing', 'DEPT', 1, 'Y', 'T', 1000, 10, 3);

insert into restart_program_status (restart_name, thread_val, start_time, program_name, program_status, restart_flag, restart_time, finish_time, current_pid, current_operator_id, err_message, current_oracle_sid, current_shadow_pid)
values ('xxadeo_item_lfcy', 1, null, 'xxadeo_item_lfcy', 'ready for start', null, null, null, null, null, null, null, null);

