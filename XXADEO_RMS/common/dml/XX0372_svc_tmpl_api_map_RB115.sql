/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "SVC_TMPL_API_MAP" table for RB115                    */
/******************************************************************************/
--
delete from svc_tmpl_api_map where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
--
-- XXADEO_DEPT_LEVEL_VAT
--
insert into svc_tmpl_api_map(template_key, dnld_api, upld_api, process_api)
values('XXADEO_DEPT_LEVEL_VAT', 'XXADEO_DEPT_LEVEL_VAT_SQL.CREATE_S9T', 'XXADEO_DEPT_LEVEL_VAT_SQL.PROCESS_S9T', 'XXADEO_DEPT_LEVEL_VAT_SQL.PROCESS');
--
-- XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS
--
insert into svc_tmpl_api_map(template_key, dnld_api, upld_api, process_api)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'XXADEO_MERCHHIER_TRANS_SQL.CREATE_S9T', 'XXADEO_MERCHHIER_TRANS_SQL.PROCESS_S9T', 'XXADEO_MERCHHIER_TRANS_SQL.PROCESS');
--
-- XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB
--
insert into svc_tmpl_api_map(template_key, dnld_api, upld_api, process_api)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'XXADEO_MERCHHIER_ATTRIB_SQL.CREATE_S9T', 'XXADEO_MERCHHIER_ATTRIB_SQL.PROCESS_S9T', 'XXADEO_MERCHHIER_ATTRIB_SQL.PROCESS');
--
-- XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS
--
insert into svc_tmpl_api_map(template_key, dnld_api, upld_api, process_api)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'XXADEO_MERCHHIER_RDF_DOM_SQL.CREATE_S9T', 'XXADEO_MERCHHIER_RDF_DOM_SQL.PROCESS_S9T', 'XXADEO_MERCHHIER_RDF_DOM_SQL.PROCESS');