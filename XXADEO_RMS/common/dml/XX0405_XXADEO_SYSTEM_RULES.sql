/******************************************************************************/
/* CREATE DATE - June 2018                                                    */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_SYSTEM_RULES" table for RB34a                 */
/******************************************************************************/

--
-- DELETE
--

-- STORE PORTAL / RB34a

delete xxadeo_system_rules where rule_id in ('XXADEO_PC_EM_STORE_PORTAL','XXADEO_PC_EMERGENCY','XXADEO_PC_RPO','XXADEO_PC_STORE_PORTAL','XXADEO_PC_SIMTAR');

-- RB107/118

delete from xxadeo_system_rules_detail where rule_id in (select rule_id from xxadeo_system_rules where func_area = 'LIFECYCLE');
delete from xxadeo_system_rules where func_area = 'LIFECYCLE';

-- RB135 + RB129

delete from xxadeo_system_rules where rule_id in ('SUPPLIER_ACTIVATE_UI','SUPPLIER_DEACTIVATE_UI') and func_area = 'RG';


--
-- INSERT
--

-- STORE PORTAL / RB34a

insert into xxadeo_system_rules (rule_id, rule_desc, short_desc, func_area, package_name, function_name, call_seq_no, active_ind, create_datetime, last_update_datetime, last_update_id)
values ('XXADEO_PC_EM_STORE_PORTAL','Price Change - Emergency Store Portal','PC_EM_STORE_PORTAL','PRICE_CHANGE','XXADEO_PRICE_CHANGE_SQL','PROCESS_EM_STORE_PORTAL_PC',1,'Y',SYSDATE,SYSDATE,USER);

insert into xxadeo_system_rules (rule_id, rule_desc, short_desc, func_area, package_name, function_name, call_seq_no, active_ind, create_datetime, last_update_datetime, last_update_id)
values ('XXADEO_PC_EMERGENCY','Price Change - Emergency (except Store Portal)','PC_EMERGENCY','PRICE_CHANGE','XXADEO_PRICE_CHANGE_SQL','PROCESS_EMERGENCY_PC',2,'Y',SYSDATE,SYSDATE,USER);

insert into xxadeo_system_rules (rule_id, rule_desc, short_desc, func_area, package_name, function_name, call_seq_no, active_ind, create_datetime, last_update_datetime, last_update_id)
values ('XXADEO_PC_RPO','Price Change - Regular RPO','PC_RPO','PRICE_CHANGE','XXADEO_PRICE_CHANGE_SQL','PROCESS_RPO_PC',3,'Y',SYSDATE,SYSDATE,USER);

insert into xxadeo_system_rules (rule_id, rule_desc, short_desc, func_area, package_name, function_name, call_seq_no, active_ind, create_datetime, last_update_datetime, last_update_id)
values ('XXADEO_PC_STORE_PORTAL','Price Change - Regular Store Portal','PC_STORE_PORTAL','PRICE_CHANGE','XXADEO_PRICE_CHANGE_SQL','PROCESS_STORE_PORTAL_PC',4,'Y',SYSDATE,SYSDATE,USER);

insert into xxadeo_system_rules (rule_id, rule_desc, short_desc, func_area, package_name, function_name, call_seq_no, active_ind, create_datetime, last_update_datetime, last_update_id)
values ('XXADEO_PC_SIMTAR','Price Change - Regular SIMTAR','PC_SIMTAR','PRICE_CHANGE','XXADEO_PRICE_CHANGE_SQL','PROCESS_SIMTAR_PC',5,'Y',SYSDATE,SYSDATE,USER);


-- RB107/118

WHENEVER SQLERROR CONTINUE

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-ASI-01','IB-ASI-01','IB-ASI-01','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_ASI_01',1,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-ASI-02','IB-ASI-02','IB-ASI-02','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_ASI_02',2,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-ASI-03','Pack components','Packs','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_ASI_03',3,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-ASI-04','Orderable items','Orderable','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_ASI_04',4,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-ASI-05','BASA Complete referencing','BASA','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_ASI_05',5,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-ACOM-01','IB-ACOM-01','IB-ACOM-01','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_ACOM_01',10,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-ACOM-02','IB-ACOM-02','IB-ACOM-02','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_ACOM_02',20,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-ACOM-03','IB-ACOM-03','IB-ACOM-03','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_ACOM_03',30,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-ACOM-031','IB-ACOM-031','IB-ACOM-031','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_ACOM_031',31,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-ACOM-04','IB-ACOM-04','IB-ACOM-04','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_ACOM_04',40,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-DISC-01','IB-DISC-01','IB-DISC-01','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_DISC_01',1,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-DISC-02','IB-DISC-02','IB-DISC-02','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_DISC_02',2,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-DEL-01','IB-DEL-01','IB-DEL-01','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_DEL_01',1,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IB-DEL-02','IB-DEL-02','IB-DEL-02','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IB_SQL','IB_DEL_02',2,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IS-INIT-01','STOP=>INIT if LINK date is null or in future','STOP=>INIT','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IS_SQL','IS_INIT_01',1,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IS-ACT-01','IS-ACT-01','IS-ACT-01','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IS_SQL','IS_ACT_01',1,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IS-ACT-02','IS-ACT-02','IS-ACT-02','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IS_SQL','IS_ACT_02',1,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IS-ACT-03','IS-ACT-03','IS-ACT-03','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IS_SQL','IS_ACT_03',1,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IS-ACT-04','IS-ACT-04','IS-ACT-04','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IS_SQL','IS_ACT_04',1,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IS-ACT-05','IS-ACT-05','IS-ACT-05','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IS_SQL','IS_ACT_05',1,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IS-ACT-06','IS-ACT-06','IS-ACT-06','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IS_SQL','IS_ACT_06',1,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IS-ACT-07','IS-ACT-07','IS-ACT-07','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IS_SQL','IS_ACT_07',1,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IS-ACT-08','Item/supp Link term null or > vdate','IS-ACT-08','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IS_SQL','IS_ACT_08',1,'Y',sysdate,sysdate,'RB107');

Insert into XXADEO_SYSTEM_RULES (RULE_ID,RULE_DESC,SHORT_DESC,FUNC_AREA,PACKAGE_NAME,FUNCTION_NAME,CALL_SEQ_NO,ACTIVE_IND,CREATE_DATETIME,LAST_UPDATE_DATETIME,LAST_UPDATE_ID) 
values ('IS-STOP-01','IS-STOP-01','IS-STOP-01','LIFECYCLE','XXADEO_ITEM_LFCY_VAL_IS_SQL','IS_STOP_01',1,'Y',sysdate,sysdate,'RB107');


Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ACOM-01','LFCY_STATUS','N/D','IB-ACOM',null,10,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ACOM-01','SINGLE_ITEM_EXEC','N/D','Y',null,10,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ACOM-02','LFCY_STATUS','N/D','IB-ACOM',null,20,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ACOM-02','SINGLE_ITEM_EXEC','N/D','Y',null,20,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ACOM-03','LFCY_STATUS','N/D','IB-ACOM',null,30,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ACOM-03','SINGLE_ITEM_EXEC','N/D','Y',null,30,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ACOM-031','SINGLE_ITEM_EXEC','N/D','Y',null,31,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ACOM-031','LFCY_STATUS','N/D','IB-ACOM',null,31,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ACOM-04','LFCY_STATUS','N/D','IB-ACOM',null,40,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ACOM-04','SINGLE_ITEM_EXEC','N/D','Y',null,40,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ASI-01','LFCY_STATUS','N/D','IB-ASI',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ASI-01','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ASI-02','LFCY_STATUS','N/D','IB-ASI',null,2,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ASI-02','SINGLE_ITEM_EXEC','N/D','Y',null,2,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ASI-02','PARAMETER','Parameters: Cost component name','(*TBD*)',null,2,'RB107',sysdate);

    Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ASI-03','LFCY_STATUS','N/D','IB-ASI',null,3,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ASI-03','SINGLE_ITEM_EXEC','N/D','Y',null,3,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ASI-04','LFCY_STATUS','N/D','IB-ASI',null,4,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ASI-04','SINGLE_ITEM_EXEC','N/D','Y',null,4,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ASI-05','LFCY_STATUS','N/D','IB-ASI',null,5,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-ASI-05','SINGLE_ITEM_EXEC','N/D','Y',null,5,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-DISC-01','LFCY_STATUS','N/D','IB-DISC',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-DISC-01','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-DISC-02','SINGLE_ITEM_EXEC','N/D','Y',null,2,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-DISC-02','LFCY_STATUS','N/D','IB-DISC',null,2,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-DEL-01','LFCY_STATUS','N/D','IB-DEL',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-DEL-01','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-DEL-01','PARAMETER','Monthts','24',null,1,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-DEL-02','LFCY_STATUS','N/D','IB-DEL',null,2,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IB-DEL-02','SINGLE_ITEM_EXEC','N/D','Y',null,2,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-INIT-01','LFCY_STATUS','N/D','IS-INIT',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-INIT-01','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-01','LFCY_STATUS','N/D','IS-ACT',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-01','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-02','LFCY_STATUS','N/D','IS-ACT',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-02','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-03','LFCY_STATUS','N/D','IS-ACT',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-03','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-04','LFCY_STATUS','N/D','IS-ACT',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-04','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-05','LFCY_STATUS','N/D','IS-ACT',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-05','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-06','LFCY_STATUS','N/D','IS-ACT',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-06','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-07','LFCY_STATUS','N/D','IS-ACT',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-07','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-08','LFCY_STATUS','N/D','IS-ACT',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-ACT-08','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);

Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-STOP-01','LFCY_STATUS','N/D','IS-STOP',null,1,'RB107',sysdate);
Insert into XXADEO_SYSTEM_RULES_DETAIL (RULE_ID,PARAMETER,DESCRIPTION,VALUE_1,VALUE_2,CALL_SEQ_NO,LAST_UPDATE_ID,LAST_UPDATE_DATETIME) 
    values ('IS-STOP-01','SINGLE_ITEM_EXEC','N/D','Y',null,1,'RB107',sysdate);


-- RB135 + RB129

insert into xxadeo_system_rules (RULE_ID, RULE_DESC, SHORT_DESC, FUNC_AREA, PACKAGE_NAME, FUNCTION_NAME, CALL_SEQ_NO, ACTIVE_IND, CREATE_DATETIME, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('SUPPLIER_ACTIVATE_UI', 'Rule 1 of Custom Rules for Supplier', null, 'RG', 'XXADEO_CUSTOM_RULES_SUPP_SQL', 'RUN_CUSTOM_RULES_SUP_RG1', 1, 'Y', to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), 'XXADEO_RMS');

insert into xxadeo_system_rules (RULE_ID, RULE_DESC, SHORT_DESC, FUNC_AREA, PACKAGE_NAME, FUNCTION_NAME, CALL_SEQ_NO, ACTIVE_IND, CREATE_DATETIME, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('SUPPLIER_ACTIVATE_UI', 'Rule 2 of Custom Rules for Supplier', null, 'RG', 'XXADEO_CUSTOM_RULES_SUPP_SQL', 'RUN_CUSTOM_RULES_SUP_RG2', 2, 'Y', to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), 'XXADEO_RMS');

insert into xxadeo_system_rules (RULE_ID, RULE_DESC, SHORT_DESC, FUNC_AREA, PACKAGE_NAME, FUNCTION_NAME, CALL_SEQ_NO, ACTIVE_IND, CREATE_DATETIME, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('SUPPLIER_ACTIVATE_UI', 'Rule 3 of Custom Rules for Supplier', null, 'RG', 'XXADEO_CUSTOM_RULES_SUPP_SQL', 'RUN_CUSTOM_RULES_SUP_RG3', 3, 'Y', to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), 'XXADEO_RMS');

insert into xxadeo_system_rules (RULE_ID, RULE_DESC, SHORT_DESC, FUNC_AREA, PACKAGE_NAME, FUNCTION_NAME, CALL_SEQ_NO, ACTIVE_IND, CREATE_DATETIME, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('SUPPLIER_ACTIVATE_UI', 'Rule 4 of Custom Rules for Supplier', null, 'RG', 'XXADEO_CUSTOM_RULES_SUPP_SQL', 'RUN_CUSTOM_RULES_SUP_RG4', 4, 'Y', to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), 'XXADEO_RMS');

insert into xxadeo_system_rules (RULE_ID, RULE_DESC, SHORT_DESC, FUNC_AREA, PACKAGE_NAME, FUNCTION_NAME, CALL_SEQ_NO, ACTIVE_IND, CREATE_DATETIME, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('SUPPLIER_ACTIVATE_UI', 'Rule 5 of Custom Rules for Supplier', null, 'RG', 'XXADEO_CUSTOM_RULES_SUPP_SQL', 'RUN_CUSTOM_RULES_SUP_RG5', 5, 'Y', to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), 'XXADEO_RMS');
/* rules 6 and 7 were included at cfa entity level 
insert into xxadeo_system_rules (RULE_ID, RULE_DESC, SHORT_DESC, FUNC_AREA, PACKAGE_NAME, FUNCTION_NAME, CALL_SEQ_NO, ACTIVE_IND, CREATE_DATETIME, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('SUPPLIER_ACTIVATE_UI', 'Rule 6 of Custom Rules for Supplier', null, 'RG', 'XXADEO_CUSTOM_RULES_SUPP_SQL', 'RUN_CUSTOM_RULES_SUP_RG6', 6, 'Y', to_date('15-03-2018 17:50:10', 'dd-mm-yyyy hh24:mi:ss'), to_date('15-03-2018 17:50:10', 'dd-mm-yyyy hh24:mi:ss'), 'XXADEO_RMS');
*/
insert into xxadeo_system_rules (RULE_ID, RULE_DESC, SHORT_DESC, FUNC_AREA, PACKAGE_NAME, FUNCTION_NAME, CALL_SEQ_NO, ACTIVE_IND, CREATE_DATETIME, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('SUPPLIER_DEACTIVATE_UI', 'Rule 2 of Custom Rules for Supplier', null, 'RG', 'XXADEO_CUSTOM_RULES_SUPP_SQL', 'RUN_CUSTOM_RULES_SUP_RG2', 2, 'Y', to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), 'XXADEO_RMS');

insert into xxadeo_system_rules (RULE_ID, RULE_DESC, SHORT_DESC, FUNC_AREA, PACKAGE_NAME, FUNCTION_NAME, CALL_SEQ_NO, ACTIVE_IND, CREATE_DATETIME, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values ('SUPPLIER_DEACTIVATE_UI', 'Rule 3 of Custom Rules for Supplier', null, 'RG', 'XXADEO_CUSTOM_RULES_SUPP_SQL', 'RUN_CUSTOM_RULES_SUP_RG3', 3, 'Y', to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), to_date('15-03-2018 17:50:09', 'dd-mm-yyyy hh24:mi:ss'), 'XXADEO_RMS');
/* rules 6 and 7 were included at cfa entity level 
insert into xxadeo_system_rules (rule_id, rule_desc, short_desc, func_area, package_name, function_name, call_seq_no, active_ind, create_datetime, last_update_datetime, last_update_id)
values ('SUPPLIER_DEACTIVATE_UI', 'Rule 7 of Custom Rules for Supplier', null, 'RG', 'XXADEO_CUSTOM_RULES_SUPP_SQL', 'RUN_CUSTOM_RULES_SUP_RG7', 7, 'Y', sysdate, sysdate, 'XXADEO_RMS');

insert into xxadeo_system_rules (rule_id, rule_desc, short_desc, func_area, package_name, function_name, call_seq_no, active_ind, create_datetime, last_update_datetime, last_update_id)
values ('SUPPLIER_ACTIVATE_UI', 'Rule 7 of Custom Rules for Supplier', null, 'RG', 'XXADEO_CUSTOM_RULES_SUPP_SQL', 'RUN_CUSTOM_RULES_SUP_RG7', 7, 'Y', sysdate, sysdate, 'XXADEO_RMS');
*/