--------------------------------------------------------------------------------
-- CREATE DATE - 15/11/2018                                               
-- CREATE USER - Filipa Neves                                                     
-- PROJECT     - ADEO - O1C                                                 
-- DESCRIPTION - Add valiations at cfa entity level for suppliers                                                    
--------------------------------------------------------------------------------
update cfa_ext_entity
   set validation_func = 'XXADEO_SUPP_CUST_RULES_CFA_SQL.CHECK_ALL_VALIDATIONS'
 where ext_entity_id = 8;
 