--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of CODE_DETAIL table for RB119                           */
/******************************************************************************/
--------------------------------------------------------------------------------
--
delete code_detail where code_type = 'CFAS';
--
insert into code_detail values ('CFAS','1','RMS entity on which will be applied the CFA security.','Y','1');
--
      