/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_LIST_VALS" table for RB116                       */
/******************************************************************************/

update s9t_list_vals 
   set code = 'S9A2'
 where template_category = 'IS9T'
   and column_name = 'ACTION'
   and sheet_name in ('ITEM_MASTER'); 