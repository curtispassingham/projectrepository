--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - June 2018                                                    */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of CODE_HEAD table for RB34a                             */
/******************************************************************************/
--------------------------------------------------------------------------------
--
delete code_detail where code_type = 'RSCD';

delete code_head where code_type = 'RSCD';
--
insert into code_head(code_type, code_type_desc)
values ('RSCD','Reason Code');
--
