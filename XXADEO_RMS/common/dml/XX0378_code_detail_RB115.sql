/******************************************************************************/
/* CREATE DATE - June 2018                                                    */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "CODE_DETAIL" table for RB115                         */
/******************************************************************************/
--
delete from code_detail_tl where code_type IN ('MHST');
--
delete from code_detail where code_type IN ('MHST');
--
-- Merch Hier Status
--
insert into code_detail (code_type,code,code_desc,required_ind,code_seq)
values ('MHST', '1', 'In Preparation', 'Y', 1);

insert into code_detail (code_type,code,code_desc,required_ind,code_seq)
values ('MHST', '2', 'Active', 'Y', 2);

insert into code_detail (code_type,code,code_desc,required_ind,code_seq)
values ('MHST', '3', 'Inactive', 'Y', 3);
