--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of s9t_list_vals for RB116                               */
/******************************************************************************/
delete 
  from s9t_list_vals slv
 where exists (select st.template_category,
          wd.wksht_name,
          cd.column_name,
          ca.code_type
     from s9t_template         st,
          s9t_tmpl_wksht_def   wd,
          s9t_tmpl_cols_def    cd,
          cfa_ext_entity       cee,
          cfa_attrib_group_set cags,
          cfa_attrib_group     cag,
          cfa_attrib           ca
    where st.template_key   = wd.template_key
      and wd.template_key   = cd.template_key 
      and wd.wksht_key      = cd.wksht_key
      and cee.ext_entity_id = cags.ext_entity_id
      and cee.base_rms_table in ('ITEM_MASTER','ITEM_SUPPLIER','ITEM_SUPP_COUNTRY')
      and wd.wksht_key      = cags.group_set_view_name 
      and cd.column_key     = ca.view_col_name 
      and cags.group_set_id = cag.group_set_id
      and cag.group_id      = ca.group_id    
      and ca.code_type is not null
      and st.template_category = slv.template_category
      and wd.wksht_key         = slv.sheet_name
      and cd.column_name        = slv.column_name);
--
delete 
  from s9t_list_vals slv
 where exists (select st.template_category,
          wd.wksht_name,
          cd.column_key,
          ca.code_type
     from s9t_template         st,
          s9t_tmpl_wksht_def   wd,
          s9t_tmpl_cols_def    cd,
          cfa_ext_entity       cee,
          cfa_attrib_group_set cags,
          cfa_attrib_group     cag,
          cfa_attrib           ca
    where st.template_key   = wd.template_key
      and wd.template_key   = cd.template_key 
      and wd.wksht_key      = cd.wksht_key
      and cee.ext_entity_id = cags.ext_entity_id
      and cee.base_rms_table in ('ITEM_MASTER','ITEM_SUPPLIER','ITEM_SUPP_COUNTRY')
      and wd.wksht_key      = cags.group_set_view_name 
      and cd.column_key     = ca.view_col_name 
      and cags.group_set_id = cag.group_set_id
      and cag.group_id      = ca.group_id    
      and ca.code_type is not null
      and st.template_category = slv.template_category
      and wd.wksht_key         = slv.sheet_name
      and cd.column_key        = slv.column_name);
--
insert into s9t_list_vals target 
  (select st.template_category,
          wd.wksht_key,
          cd.column_key,
          ca.code_type
     from s9t_template         st,
          s9t_tmpl_wksht_def   wd,
          s9t_tmpl_cols_def    cd,
          cfa_ext_entity       cee,
          cfa_attrib_group_set cags,
          cfa_attrib_group     cag,
          cfa_attrib           ca
    where st.template_key   = wd.template_key
      and wd.template_key   = cd.template_key 
      and wd.wksht_key      = cd.wksht_key
      and cee.ext_entity_id = cags.ext_entity_id
      and cee.base_rms_table in ('ITEM_MASTER','ITEM_SUPPLIER','ITEM_SUPP_COUNTRY')
      and wd.wksht_key      = cags.group_set_view_name 
      and cd.column_key     = ca.view_col_name 
      and cags.group_set_id = cag.group_set_id
      and cag.group_id      = ca.group_id    
      and ca.code_type is not null);
