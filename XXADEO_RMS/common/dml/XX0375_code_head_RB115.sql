/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "CODE_HEAD" table for RB115                           */
/******************************************************************************/
--
delete from code_detail where code_type IN ('ADVT','ADLG','ADST');
--
delete code_head where code_type IN ('ADVT','ADLG','ADST');
--
insert into code_head(code_type, code_type_desc)
values('ADVT', 'VAT Type - Cost, Retail or Both.');
--
insert into code_head(code_type, code_type_desc)
values('ADVC', 'VAT Code');
--
-- LANGUAGE
--
insert into code_head (CODE_TYPE, CODE_TYPE_DESC)
values ('ADLG', 'List of non-primary languages for maintainence of translation records through spreadsheet API and UI.');
--
-- Custom attribute
--
insert into code_head(code_type, code_type_desc)
values ('ADCA','Adeo Custom Attributes'); 
--
-- Load Sales Indicator
--
insert into code_head(code_type, code_type_desc)
values ('ADSI','Adeo Load Sales Indicator'); 

