/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_LIST_VALS" table for RB115                       */
/******************************************************************************/
--
delete from s9t_list_vals 
 where template_category = 'RMSADM' 
   and sheet_name IN ('DEPT_LEVEL_VAT','DEPARTMENT_TRANSLATIONS','CLASS_TRANSLATIONS','SUBCLASS_TRANSLATIONS','DEPARTMENT_STATUS','CLASS_STATUS','SUBCLASS_STATUS','RDF_DOMAINS','RDF_DOMAINS_BY_DEPT','RDF_DOMAINS_BY_CLASS','RDF_DOMAINS_BY_SUBCLASS');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'DEPT_LEVEL_VAT', 'ACTION', 'S9A4');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'DEPT_LEVEL_VAT', 'VAT_TYPE', 'ADVT');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'DEPT_LEVEL_VAT', 'VAT_CODE', 'ADVC');
--
--
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'DEPARTMENT_TRANSLATIONS', 'ACTION', 'S9A4');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'DEPARTMENT_TRANSLATIONS', 'LANGUAGE', 'ADLG');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'CLASS_TRANSLATIONS', 'ACTION', 'S9A4');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'CLASS_TRANSLATIONS', 'LANGUAGE', 'ADLG');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'SUBCLASS_TRANSLATIONS', 'ACTION', 'S9A4');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'SUBCLASS_TRANSLATIONS', 'LANGUAGE', 'ADLG');
--
--
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'DEPARTMENT_STATUS', 'ACTION', 'S9A4');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'DEPARTMENT_STATUS', 'DEPT_STATUS', 'MHST');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'CLASS_STATUS', 'ACTION', 'S9A4');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'CLASS_STATUS', 'CLASS_STATUS', 'MHST');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'SUBCLASS_STATUS', 'ACTION', 'S9A4');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'SUBCLASS_STATUS', 'SUBCLASS_STATUS', 'MHST');
--
--
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'RDF_DOMAINS', 'ACTION', 'S9A4');
--
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'RDF_DOMAINS_BY_DEPT', 'ACTION', 'S9A4');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'RDF_DOMAINS_BY_DEPT', 'LOAD_SALES_IND', 'ADSI');
--
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'RDF_DOMAINS_BY_CLASS', 'ACTION', 'S9A4');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'RDF_DOMAINS_BY_CLASS', 'LOAD_SALES_IND', 'ADSI');
--
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'RDF_DOMAINS_BY_SUBCLASS', 'ACTION', 'S9A4');
--
insert into s9t_list_vals(template_category, sheet_name, column_name, code)
values('RMSADM', 'RDF_DOMAINS_BY_SUBCLASS', 'LOAD_SALES_IND', 'ADSI');