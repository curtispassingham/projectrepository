/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_TMPL_WKSHT_DEF_TL" table for RB115               */
/******************************************************************************/

delete from s9t_tmpl_wksht_def_tl
      where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS');
--
-- XXADEO_DEPT_LEVEL_VAT
--
insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT',1,'DEPT_LEVEL_VAT');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_DEPT_LEVEL_VAT','DEPT_LEVEL_VAT',3,'TVA_NIVEAU_SOUS_RAYON');
--
-- XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB
--
insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','DEPARTMENT_STATUS',1,'DEPARTMENT_STATUS');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','DEPARTMENT_STATUS',3,'STATUT_SOUS_RAYON');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS',1,'CLASS_STATUS');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','CLASS_STATUS',3,'STATUT_TYPE');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS',1,'SUBCLASS_STATUS');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','SUBCLASS_STATUS',3,'STATUT_SOUS_TYPE');
--
-- XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS
--
insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS',1,'RDF_DOMAINS');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS',3,'RDF_DOMAINES');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT',1,'RDF_DOMAINS_BY_DEPT');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_DEPT',3,'RDF_DOMAINES_POUR_SOUS_RAYON');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS',1,'RDF_DOMAINS_BY_CLASS');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_CLASS',3,'RDF_DOMAINES_POUR_TYPE');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS',1,'RDF_DOMAINS_BY_SUBCLASS');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS','RDF_DOMAINS_BY_SUBCLASS',3,'RDF_DOMAINES_POUR_SOUS_TYPE');
--
-- XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS
--
insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','DEPARTMENT_TRANSLATIONS',1,'DEPARTMENT_TRANSLATIONS');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','DEPARTMENT_TRANSLATIONS',3,'TRADUCTION_SOUS_RAYON');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS',1,'CLASS_TRANSLATIONS');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','CLASS_TRANSLATIONS',3,'TRADUCTION_TYPE');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS',1,'SUBCLASS_TRANSLATIONS');

insert into s9t_tmpl_wksht_def_tl (template_key,wksht_key,lang,wksht_name)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','SUBCLASS_TRANSLATIONS',3,'TRADUCTION_SOUS_TYPE');