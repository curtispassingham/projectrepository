/* ************************************************************************** */
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Paulo Mamede / Tiago Torres                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Install script for XXADEO scripts                            */
/* ************************************************************************** */

update cfa_attrib
   set validation_func = 'XXADEO_SUPP_CUST_RULES_CFA_SQL.VALID_LAST_SUPPLIER_SITE_SUPP'
 where view_col_name = 'LAST_SUPPLIER_SITE_SUPP';
 

 
update cfa_attrib_group_set
   set validation_func = 'XXADEO_SUPP_CUST_RULES_CFA_SQL.VALIDATE_CFA_ATTRIB_GRP_SET'
    where group_set_id = (select a.group_set_id from cfa_attrib_group_set a, cfa_attrib_group b, cfa_attrib c
        where a.group_set_id = b.group_set_id 
          and b.group_id = c.group_id
          and c.attrib_id = (select value_1
                from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                              I_parameter => 'SHIPPING_COST_BILLING_SUPP'))));
											  
