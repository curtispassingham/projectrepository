/******************************************************************************/
/* CREATE DATE - Oct 2018                                                     */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Status changes configuration for ITEM/BU and ITEM/SUPP       */
/*               lifecycle                                                    */
/******************************************************************************/

WHENEVER SQLERROR CONTINUE

delete xxadeo_lfcy_cfg_status_next;

insert into xxadeo_lfcy_cfg_status_next values ('IB-INIT', 'IB-ASI');
insert into xxadeo_lfcy_cfg_status_next values ('IB-INIT', 'IB-DISC');
insert into xxadeo_lfcy_cfg_status_next values ('IB-ASI', 'IB-ACOM');
insert into xxadeo_lfcy_cfg_status_next values ('IB-ASI', 'IB-DISC');
insert into xxadeo_lfcy_cfg_status_next values ('IB-ACOM', 'IB-DISC');
insert into xxadeo_lfcy_cfg_status_next values ('IB-ACOM', 'IB-DEL');
insert into xxadeo_lfcy_cfg_status_next values ('IB-DISC', 'IB-DEL');

insert into xxadeo_lfcy_cfg_status_next values ('IS-INIT', 'IS-ACT');
insert into xxadeo_lfcy_cfg_status_next values ('IS-INIT', 'IS-STOP');
insert into xxadeo_lfcy_cfg_status_next values ('IS-ACT', 'IS-STOP');
insert into xxadeo_lfcy_cfg_status_next values ('IS-STOP', 'IS-INIT');
  