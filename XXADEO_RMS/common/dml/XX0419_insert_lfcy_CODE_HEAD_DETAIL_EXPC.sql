
/******************************************************************************/
/* CREATE DATE - Jul 2018                                                     */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - EXPC values for Ecoparts EXP_CATEGORY                        */
/******************************************************************************/

WHENEVER SQLERROR CONTINUE

-- Insert new CODE_DETAIL entry for EXP_CATEGORY values, identifying an EcoPart expenses
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('EXPC','532','LMFR_ECO_DDS_ITEM','Y',532);
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('EXPC','533','LMFR_DEEE_APPAR_ITEM','Y',533);
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('EXPC','534','LMFR_DEEE_TUBE_ITEM','Y',534);
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('EXPC','535','LMFR_DEEE_PILE_ITEM','Y',535);
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('EXPC','536','LMFR_ECO_MOBIL_ITEM','Y',536);
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('EXPC','537','LMFR_ECO_TLC_ITEM','Y',537);
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('EXPC','538','LMFR_ECO_EMBAL_ITEM','Y',538);

insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('EXPC','552','LMPL_TOP_RECY_ELEC_ITEM','Y',552);

commit;

