/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_TMPL_COLS_DEF" table for RB115                   */
/******************************************************************************/
--
delete from s9t_tmpl_wksht_def 
 where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
--
insert into s9t_tmpl_wksht_def(template_key, wksht_key, wksht_name, mandatory, seq_no)
values('XXADEO_DEPT_LEVEL_VAT', 'DEPT_LEVEL_VAT', 'DEPT_LEVEL_VAT', 'Y', 0);
--
-- XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS
--
insert into s9t_tmpl_wksht_def(template_key, wksht_key, wksht_name, mandatory, seq_no)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'DEPARTMENT_TRANSLATIONS', 'DEPARTMENT_TRANSLATIONS', 'Y', 0);
--
insert into s9t_tmpl_wksht_def(template_key, wksht_key, wksht_name, mandatory, seq_no)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'CLASS_TRANSLATIONS', 'CLASS_TRANSLATIONS', 'Y', 1);
--
insert into s9t_tmpl_wksht_def(template_key, wksht_key, wksht_name, mandatory, seq_no)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'Y', 2);
--
-- XXADEO_MERCHANDISE_HIERARCHY_CFA_ATRIB
--
insert into s9t_tmpl_wksht_def(template_key, wksht_key, wksht_name, mandatory, seq_no)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'DEPARTMENT_STATUS', 'DEPARTMENT_STATUS', 'Y', 0);
--
insert into s9t_tmpl_wksht_def(template_key, wksht_key, wksht_name, mandatory, seq_no)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'CLASS_STATUS', 'CLASS_STATUS', 'Y', 1);
--
insert into s9t_tmpl_wksht_def(template_key, wksht_key, wksht_name, mandatory, seq_no)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'SUBCLASS_STATUS', 'SUBCLASS_STATUS', 'Y', 2);
--
-- XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS
--
insert into s9t_tmpl_wksht_def(template_key, wksht_key, wksht_name, mandatory, seq_no)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS', 'RDF_DOMAINS', 'Y', 0);
--
insert into s9t_tmpl_wksht_def(template_key, wksht_key, wksht_name, mandatory, seq_no)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_DEPT', 'RDF_DOMAINS_BY_DEPT', 'Y', 1);
--
insert into s9t_tmpl_wksht_def(template_key, wksht_key, wksht_name, mandatory, seq_no)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_CLASS', 'RDF_DOMAINS_BY_CLASS', 'Y', 2);
--
insert into s9t_tmpl_wksht_def(template_key, wksht_key, wksht_name, mandatory, seq_no)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_SUBCLASS', 'RDF_DOMAINS_BY_SUBCLASS', 'Y', 3);

