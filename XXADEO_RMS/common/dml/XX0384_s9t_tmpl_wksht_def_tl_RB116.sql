/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_TMPL_WKSHT_DEF_TL" table for RB116               */
/******************************************************************************/
--
delete from s9t_tmpl_wksht_def_tl
 where template_key = 'XXADEO_ITEM_MASTER';
--
insert into s9t_tmpl_wksht_def_tl
  (template_key, wksht_key, lang, wksht_name)
  (select 'XXADEO_ITEM_MASTER', WKSHT_KEY, LANG, WKSHT_NAME
     from s9t_tmpl_wksht_def_tl swdt
    where template_key = 'ITEM_MASTER_DATA'
      and exists (select 1
             from s9t_tmpl_wksht_def wdt
            where wdt.template_key = 'XXADEO_ITEM_MASTER'
              and wdt.wksht_key = swdt.wksht_key));
		 
  