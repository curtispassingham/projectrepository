/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_TMPL_COLS_DEF" table for RB115                   */
/******************************************************************************/
--
delete from s9t_tmpl_cols_def 
 where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_DEPT_LEVEL_VAT', 'DEPT_LEVEL_VAT', 'ACTION', 'Action', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_DEPT_LEVEL_VAT', 'DEPT_LEVEL_VAT', 'VAT_REGION', 'VAT Region', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_DEPT_LEVEL_VAT', 'DEPT_LEVEL_VAT', 'GROUP_ID', 'Group ID', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_DEPT_LEVEL_VAT', 'DEPT_LEVEL_VAT', 'GROUP_NAME', 'Group Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_DEPT_LEVEL_VAT', 'DEPT_LEVEL_VAT', 'DEPT', 'Department', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_DEPT_LEVEL_VAT', 'DEPT_LEVEL_VAT', 'DEPT_NAME', 'Department Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_DEPT_LEVEL_VAT', 'DEPT_LEVEL_VAT', 'VAT_TYPE', 'VAT Type', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_DEPT_LEVEL_VAT', 'DEPT_LEVEL_VAT', 'VAT_CODE', 'VAT Code', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
-- MERCHANDISE HIERARCHY
--
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'DEPARTMENT_TRANSLATIONS', 'ACTION', 'Action', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'DEPARTMENT_TRANSLATIONS', 'LANGUAGE', 'Language', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'DEPARTMENT_TRANSLATIONS', 'GROUP_ID', 'Group ID', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'DEPARTMENT_TRANSLATIONS', 'GROUP_NAME', 'Group Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'DEPARTMENT_TRANSLATIONS', 'DEPT', 'Department', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'DEPARTMENT_TRANSLATIONS', 'DEPT_NAME', 'Department Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'DEPARTMENT_TRANSLATIONS', 'DEPT_TRANSLATION', 'Department Translation', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'CLASS_TRANSLATIONS', 'ACTION', 'Action', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'CLASS_TRANSLATIONS', 'LANGUAGE', 'Language', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'CLASS_TRANSLATIONS', 'GROUP_ID', 'Group ID', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'CLASS_TRANSLATIONS', 'GROUP_NAME', 'Group Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'CLASS_TRANSLATIONS', 'DEPT', 'Department', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'CLASS_TRANSLATIONS', 'DEPT_NAME', 'Department Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'CLASS_TRANSLATIONS', 'CLASS', 'Class', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'CLASS_TRANSLATIONS', 'CLASS_NAME', 'Class Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'CLASS_TRANSLATIONS', 'CLASS_TRANSLATION', 'Class Translation', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'ACTION', 'Action', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'LANGUAGE', 'Language', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'GROUP_ID', 'Group ID', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'GROUP_NAME', 'Group Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'DEPT', 'Department', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'DEPT_NAME', 'Department Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'CLASS', 'Class', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'CLASS_NAME', 'Class Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'SUBCLASS', 'Subclass', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'SUBCLASS_NAME', 'Subclass Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'SUBCLASS_TRANSLATIONS', 'SUBCLASS_TRANSLATION', 'Subclass Translation', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
-- XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'DEPARTMENT_STATUS', 'ACTION', 'Action', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'DEPARTMENT_STATUS', 'GROUP_ID', 'Group ID', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'DEPARTMENT_STATUS', 'GROUP_NAME', 'Group Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'DEPARTMENT_STATUS', 'DEPT', 'Department', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'DEPARTMENT_STATUS', 'DEPT_NAME', 'Department Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'DEPARTMENT_STATUS', 'DEPT_STATUS', 'Department Status', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'CLASS_STATUS', 'ACTION', 'Action', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'CLASS_STATUS', 'GROUP_ID', 'Group ID', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'CLASS_STATUS', 'GROUP_NAME', 'Group Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'CLASS_STATUS', 'DEPT', 'Department', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'CLASS_STATUS', 'DEPT_NAME', 'Department Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'CLASS_STATUS', 'CLASS', 'Class', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'CLASS_STATUS', 'CLASS_NAME', 'Class Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'CLASS_STATUS', 'CLASS_STATUS', 'Class Status', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'SUBCLASS_STATUS', 'ACTION', 'Action', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'SUBCLASS_STATUS', 'GROUP_ID', 'Group ID', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'SUBCLASS_STATUS', 'GROUP_NAME', 'Group Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'SUBCLASS_STATUS', 'DEPT', 'Department', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'SUBCLASS_STATUS', 'DEPT_NAME', 'Department Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'SUBCLASS_STATUS', 'CLASS', 'Class', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'SUBCLASS_STATUS', 'CLASS_NAME', 'Class Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'SUBCLASS_STATUS', 'SUBCLASS', 'Subclass', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'SUBCLASS_STATUS', 'SUBCLASS_NAME', 'Subclass Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'SUBCLASS_STATUS', 'SUBCLASS_STATUS', 'Subclass Status', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
-- XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS', 'ACTION', 'Action', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS', 'DOMAIN_ID', 'Domain ID', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS', 'DOMAIN_NAME', 'Domain Name', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
--
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_DEPT', 'ACTION', 'Action', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_DEPT', 'GROUP_ID', 'Group ID', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_DEPT', 'GROUP_NAME', 'Group Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_DEPT', 'DEPT', 'Department', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_DEPT', 'DEPT_NAME', 'Department Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_DEPT', 'DOMAIN_ID', 'Domain ID', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_DEPT', 'LOAD_SALES_IND', 'Load Sales Indicator', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
--
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_CLASS', 'ACTION', 'Action', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_CLASS', 'GROUP_ID', 'Group ID', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_CLASS', 'GROUP_NAME', 'Group Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_CLASS', 'DEPT', 'Department', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_CLASS', 'DEPT_NAME', 'Department Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_CLASS', 'CLASS', 'Class', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_CLASS', 'CLASS_NAME', 'Class Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_CLASS', 'DOMAIN_ID', 'Domain ID', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_CLASS', 'LOAD_SALES_IND', 'Load Sales Indicator', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
--
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_SUBCLASS', 'ACTION', 'Action', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_SUBCLASS', 'GROUP_ID', 'Group ID', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_SUBCLASS', 'GROUP_NAME', 'Group Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_SUBCLASS', 'DEPT', 'Department', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_SUBCLASS', 'DEPT_NAME', 'Department Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_SUBCLASS', 'CLASS', 'Class', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_SUBCLASS', 'CLASS_NAME', 'Class Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_SUBCLASS', 'SUBCLASS', 'Subclass', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_SUBCLASS', 'SUBCLASS_NAME', 'Subclass Name', 'Y', user, sysdate, user, sysdate, null, 'N');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_SUBCLASS', 'DOMAIN_ID', 'Domain ID', 'Y', user, sysdate, user, sysdate, null, 'Y');
--
insert into s9t_tmpl_cols_def(template_key, wksht_key, column_key, column_name, mandatory, create_id, create_datetime, last_update_id, last_update_datetime, default_value, required_visual_ind)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'RDF_DOMAINS_BY_SUBCLASS', 'LOAD_SALES_IND', 'Load Sales Indicator', 'Y', user, sysdate, user, sysdate, null, 'Y');
--