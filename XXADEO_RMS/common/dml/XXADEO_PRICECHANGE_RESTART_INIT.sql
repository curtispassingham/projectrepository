/******************************************************************************/
/* CREATE DATE - OCT 2018                                                     */
/* CREATE USER - Quentin OBRECHT                                              */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Tables "RESTART_PROGRAM_STATUS", "RESTART_CONTROL"           */
/******************************************************************************/


insert into restart_control
  (PROGRAM_NAME, PROGRAM_DESC, DRIVER_NAME, NUM_THREADS, UPDATE_ALLOWED, PROCESS_FLAG, COMMIT_MAX_CTR, LOCK_WAIT_TIME, RETRY_MAX_CTR)
values
  ('xxadeo_pricechange_dnld', 'ADEO Price Change extract ', 'NONE', 1, 'Y', 'T', 100, 10, 3);
  
insert into restart_program_status
  (RESTART_NAME,THREAD_VAL,START_TIME,PROGRAM_NAME,PROGRAM_STATUS,RESTART_FLAG,RESTART_TIME,FINISH_TIME,CURRENT_PID,CURRENT_OPERATOR_ID,ERR_MESSAGE,CURRENT_ORACLE_SID,CURRENT_SHADOW_PID)
values
  ('xxadeo_pricechange_dnld',1,null,'xxadeo_pricechange_dnld','ready for start',null,null,null,null,null,null,null,null);