/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "S9T_TEMPLATE" table for RB115                        */
/******************************************************************************/
delete from svc_tmpl_api_map where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
--
delete from s9t_tmpl_cols_def_tl where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
delete from s9t_tmpl_cols_def where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
--
delete from s9t_tmpl_wksht_def_tl where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
delete from s9t_tmpl_wksht_def where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
--
delete from s9t_template_tl where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
delete from s9t_template where template_key in ('XXADEO_DEPT_LEVEL_VAT','XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS','XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB','XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS');
--
-- XXADEO_DEPT_LEVEL_VAT
--
insert into s9t_template(template_key, template_name, template_desc, file_id, create_id, create_datetime, template_type, template_category)
values('XXADEO_DEPT_LEVEL_VAT', 'Department Level VAT', 'Department Level VAT', null, user, sysdate, 'RMSADM', null);
--
-- XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS
--
insert into s9t_template(template_key, template_name, template_desc, file_id, create_id, create_datetime, template_type, template_category)
values('XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS', 'Merchandise Hierarchy Translations', 'Merchandise Hierarchy Translations', null, user, sysdate, 'RMSADM', null);
--
-- XXADEO_MERCHANDISE_HIERARCHY_CFA_ATRIB
--
insert into s9t_template(template_key, template_name, template_desc, file_id, create_id, create_datetime, template_type, template_category)
values('XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB', 'Merchandise Hierarchy CFA Attributes', 'Merchandise Hierarchy CFA Attributes', null, user, sysdate, 'RMSADM', null);
--
-- XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS
--
insert into s9t_template(template_key, template_name, template_desc, file_id, create_id, create_datetime, template_type, template_category)
values('XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS', 'Merchandise Hierarchy RDF Domains', 'Merchandise Hierarchy RDF Domains', null, user, sysdate, 'RMSADM', null);



