
/******************************************************************************/
/* CREATE DATE - Jul 2018                                                     */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - ECOP CODE_TYPE identifying Ecopart CFAs and related BU       */
/******************************************************************************/

WHENEVER SQLERROR CONTINUE

-- create ecoparts expense CFAs mappings to BUs
insert into code_head (code_type,code_type_desc) values ('ECOP','Ecopart CFAs/BU');

-- FR
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('ECOP','538','1','Y',1);
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('ECOP','537','1','Y',2);
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('ECOP','536','1','Y',3);
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('ECOP','535','1','Y',4);
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('ECOP','534','1','Y',5);
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('ECOP','533','1','Y',6);
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('ECOP','532','1','Y',7);

-- PL
insert into code_detail (code_type,code,code_desc,required_ind,code_seq) values ('ECOP','552','6','Y',8);

commit;

