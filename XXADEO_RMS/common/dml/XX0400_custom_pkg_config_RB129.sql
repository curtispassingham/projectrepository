/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "CUSTOM_PKG_CONFIG" table for RB129                   */
/******************************************************************************/
delete from custom_pkg_config where function_key in ('SUPPLIER_ACTIVATE_UI','SUPPLIER_DEACTIVATE_UI') and call_seq_no = 1;
--
insert into custom_pkg_config (FUNCTION_KEY, CALL_SEQ_NO, SCHEMA_NAME, PACKAGE_NAME, FUNCTION_NAME, USER_ID, LAST_UPDATE_DATETIME)
values ('SUPPLIER_ACTIVATE_UI', 1, 'XXADEO_RMS', 'XXADEO_CUSTOM_CONFIG_WRP_SQL', 'RUN_SUPPLIER_APPROVE_WRP', user, sysdate);
--
insert into custom_pkg_config (FUNCTION_KEY, CALL_SEQ_NO, SCHEMA_NAME, PACKAGE_NAME, FUNCTION_NAME, USER_ID, LAST_UPDATE_DATETIME)
values ('SUPPLIER_DEACTIVATE_UI', 1, 'XXADEO_RMS', 'XXADEO_CUSTOM_CONFIG_WRP_SQL', 'RUN_SUPPLIER_APPROVE_WRP', user, sysdate);

delete from custom_pkg_config where function_key in ('SUPPLIER_ACTIVATE_UI','SUPPLIER_DEACTIVATE_UI') and call_seq_no = 2;
--
insert into custom_pkg_config (FUNCTION_KEY, CALL_SEQ_NO, SCHEMA_NAME, PACKAGE_NAME, FUNCTION_NAME, USER_ID, LAST_UPDATE_DATETIME)
values ('SUPPLIER_DEACTIVATE_UI', 2, 'XXADEO_RMS', 'XXADEO_CUSTOM_RULES_SUPP_SQL', 'RUN_CUSTOM_RULES_SUPPLIER', user, sysdate);
--
insert into custom_pkg_config (FUNCTION_KEY, CALL_SEQ_NO, SCHEMA_NAME, PACKAGE_NAME, FUNCTION_NAME, USER_ID, LAST_UPDATE_DATETIME)
values ('SUPPLIER_ACTIVATE_UI', 2, 'XXADEO_RMS', 'XXADEO_CUSTOM_RULES_SUPP_SQL', 'RUN_CUSTOM_RULES_SUPPLIER', user, sysdate);

