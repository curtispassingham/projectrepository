/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_V_DASHBOARD_COST_INFO"                          */
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_V_DASHBOARD_COST_INFO AS
SELECT COST_CHANGE,
       ITEM,
       ITEM_DESC,
       SUPPLIER,
       ORIGIN_COUNTRY_ID,
       IMAGE,
       CURRENT_COST,
       NEW_COST,
       PERC_EVO,
       CURRENCY
FROM(
       select  cssd.cost_change COST_CHANGE,
               s.SUPPLIER SUPPLIER,
               cssd.origin_country_id ORIGIN_COUNTRY_ID,
               im.item ITEM,
               im.short_desc ITEM_DESC,
               (select ii.image_addr || ii.image_name from ITEM_IMAGE ii where ii.item = im.ITEM) IMAGE,
               isc.unit_cost CURRENT_COST,
               cssd.unit_cost NEW_COST,
               CONCAT(TO_CHAR(CAST(((1-(isc.unit_cost/cssd.unit_cost))*100) AS DECIMAL(10,2)),'99999999.99'),'%')   PERC_EVO,
               s.currency_code CURRENCY
        from   v_item_master im,
               cost_susp_sup_detail cssd,
               v_sups s,
               item_supp_country isc
        where im.item = cssd.item
          and cssd.supplier = s.supplier
          and im.item = isc.item
          and s.SUPPLIER = isc.supplier
          and cssd.origin_country_id = isc.origin_country_id
    )
ORDER BY ITEM;