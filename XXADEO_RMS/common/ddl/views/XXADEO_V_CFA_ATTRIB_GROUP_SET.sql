/******************************************************************************/
/* CREATE DATE - August2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_CFA_ATTRIB_GROUP_SET" for RB116               */
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_V_CFA_ATTRIB_GROUP_SET AS
SELECT s.group_set_id,
       s.ext_entity_id,
       s.group_set_view_name,
       s.display_seq,
       s.qualifier_func,
       s.default_func,
       s.validation_func,
       s.staging_table_name,
       s.active_ind,
       s.base_ind
  FROM cfa_attrib_group_set S; 