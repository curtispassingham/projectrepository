/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_V_DASHBOARD_PRC_CHG_VR"                         */
/******************************************************************************/
begin
  execute immediate 'drop view XXADEO_V_DASHBOARD_PRC_CHG_VR';
exception
  when others then
    null;
end;
/
create or replace view XXADEO_V_DASHBOARD_PRC_CHG_VR as
select price_change_id, item, item_desc, location, zone_id, bu, selling_retail, change_amount, future_price, effective_date, reason_code, reason_desc, dept_id, dept_name, subdept_id, subdept_name, type, subtype
from (
select rpc.price_change_id,
       rpc.item,
       im.item_desc,
       rpc.location,
       rpc.zone_id,
       (select zone_bu.bu_id from xxadeo_rpm_bu_price_zones zone_bu where zone_bu.zone_id = rpc.zone_id)  as bu,
       rizp.selling_retail,
       rpc.change_amount,
       nvl((select fr.selling_retail from rpm_future_retail fr where fr.ITEM = rpc.item
            and rpc.location = fr.location and fr.action_date = rpc.effective_date),
            (select zfr.selling_retail from rpm_zone_future_retail zfr where zfr.ITEM = rpc.item
       and rpc.zone_id = zfr.zone and zfr.action_date = rpc.effective_date)) as future_price,
       rpc.effective_date,
       rpc.reason_code,
       (select description from rpm_codes where CODE_ID = rpc.reason_code) as reason_desc,
       im.group_no as dept_id,
       (select grp.GROUP_NAME
          from v_groups grp
         where im.group_no = grp.GROUP_NO
           and grp.division = im.division) as dept_name,
       im.dept as subdept_id,
       (select dept_name from v_deps where dept = im.dept and group_no = im.group_no and division = im.division) as subdept_name,
       im.class as type,
       im.SUBCLASS as subtype
  from rpm_price_change rpc, rpm_item_zone_price rizp, v_item_master im, code_detail cd
 where rpc.item = rizp.item
   and rpc.zone_id = rizp.zone_id(+)
   and rpc.item = im.item
   and cd.code_type = 'PCHG'
   and cd.code = 'PERVAR'
   and rpc.effective_date between get_vdate and TRUNC(get_vdate + 7 + 28, 'IW') - 1
   and (rpc.change_amount >= rizp.selling_retail * (1 + (to_number(replace((cd.code_desc), '%'))) / 100)
       or
        rpc.change_amount <= rizp.selling_retail - (rizp.selling_retail * (to_number(replace((cd.code_desc), '%'))
       ) / 100))
union all
select rpc.price_change_id,
       rpc.item,
       im.item_desc,
       rpc.location,
       rpc.zone_id,
       (select st.area from v_store st where st.STORE = rpc.location) as bu,
       il.selling_unit_retail,
       rpc.change_amount,
        nvl((select fr.selling_retail from rpm_future_retail fr where fr.ITEM = rpc.item
            and rpc.location = fr.location and fr.action_date = rpc.effective_date),
            (select zfr.selling_retail from rpm_zone_future_retail zfr where zfr.ITEM = rpc.item
       and rpc.zone_id = zfr.zone and zfr.action_date = rpc.effective_date)) as future_price,
       rpc.effective_date,
       rpc.reason_code,
       (select description from rpm_codes where CODE_ID = rpc.reason_code) as reason_desc,
       im.group_no as dept_id,
       (select grp.GROUP_NAME
          from v_groups grp
         where im.group_no = grp.GROUP_NO
           and grp.division = im.division) as dept_name,
       im.dept as subdept_id,
       (select dept_name from v_deps where dept = im.dept and group_no = im.group_no and division = im.division) as subdept_name,
       im.class as type,
       im.SUBCLASS as subtype
  from rpm_price_change rpc, item_loc il, v_item_master im, code_detail cd
 where rpc.item = il.item
   and rpc.location = il.loc(+)
   and rpc.item = im.item
   and cd.code_type = 'PCHG'
   and cd.code = 'PERVAR'
   and rpc.effective_date between get_vdate and  TRUNC(get_vdate + 7 + 28, 'IW') - 1
   and (rpc.change_amount >= il.selling_unit_retail * (1 + (to_number(replace((cd.code_desc), '%'))) / 100)
       or
        rpc.change_amount <= il.selling_unit_retail - (il.selling_unit_retail * ((to_number(replace((cd.code_desc), '%'))) / 100)))) vr
 where exists (select 1
                 from xxadeo_v_deps     dep,
                      xxadeo_v_class    cla,
                      xxadeo_v_subclass sub
                where dep.DEPT = vr.subdept_id
                  and cla.DEPT = dep.dept
                  and cla.CLASS = vr.type
                  and sub.DEPT = cla.dept
                  and sub.CLASS = cla.CLASS
                  and sub.SUBCLASS = vr.subtype)
 order by effective_date; 