/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_CODE_DETAIL" for RB119                        */
/******************************************************************************/
CREATE OR REPLACE VIEW XXADEO_V_CODE_DETAIL(CODE_TYPE, CODE, CODE_DESC ,CODE_SEQ) AS
SELECT CODE_TYPE, CODE, CODE_DESC, CODE_SEQ
       FROM v_code_detail;   