/******************************************************************************/
/* CREATE DATE - January 2019                                                 */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_DASHBOARD_DERU_D_SUPS" for RB148              */
/******************************************************************************/

create or replace view xxadeo_v_dashboard_deru_d_sups as
select   'DERU_ITEM' as flag,
          du_itm.status,
          du_itm.pack_ind,
          du_itm.pack_type,
          du_itm.item_level,
          du_itm.tran_level,
          du_itm.check_uda_ind,
          supp.supp_ind,
          du_itm.group_no,
          du_itm.group_name,
          du_itm.dept,
          du_itm.dept_name,
          du_itm.class,
          du_itm.class_name,
          du_itm.subclass,
          du_itm.sub_name,
          du_itm.ranking_item,
          du_itm.agg_item,
          du_itm.item as item,
          du_itm.item_desc,
          du_itm.derunit_conv_factor,
          du_itm.qt_capacity,
          du_itm.contain_uom_item,
          du_itm.assortment_mode,
          du_itm.range_size,
          du_itm.orderable_ind,
          du_itm.sellable_ind,
          du_itm.pack_qty,
          du_itm.retail_prc_blocking,
          du_itm.lifecycle_status,
          du_itm.actif_ddate_item,
          du_itm.forecast_ind,
          supp.sous_typo_step,
          supp.standard_uom,
          supp.supplier as supplier_site,
          supp.sup_name as supplier_site_desc,
          supp.manufacturer_code as manufacturer_code,
          supp.ORIGIN_COUNTRY_ID as origin_country,
          supp.STATUT_LIEN_ARTICLE as status_link,
          supp.GTIN as gtin,
          supp.PCB as supplier_pack_size,
          supp.PA_HT as supplier_cost_price,
          supp.CURRENCY_CODE as currency,
          du_itm.std_str_tr_prc_itemprc as std_str_tr_prc_itemprc,
          du_itm.national_price_itmprc as national_price_itmprc,
          du_itm.margin_itmprc as margin_itmprc,
          du_itm.margin_perc as margin_perc,
          supp.MOTIF_BLOCAGE_ACHAT as blocking_reason,
          supp.STATUT_CONFORM_QUALITE as qual_comp_status,
          supp.DATE_ARRET_LIEN as link_end_date,
          supp.BU as SUPP_BU,
          du_itm.FILTER_ORG_ID as ITEM_BU
  from xxadeo_v_dashboard_deruni_sups supp, xxadeo_v_dashboard_deruni_itms du_itm
 where supp.item = du_itm.item;
