/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_V_ITEM_SUPPLIER_DETAILS"                        */
/******************************************************************************/
 
create or replace view xxadeo_v_item_supplier_details as
select bu bu,
       item item,
       SUPPLIER supplier,
       SUP_NAME sup_name,
       GTIN gtin,
       STATUT_LIEN_ARTICLE statut_lien_article,
       BLOCKING_PO_REASON blocking_po_reason,
       PCB pcb,
       UNIT_COST unit_cost,
       COST_CURRENCY cost_currency,
       PCS pcs,
       NATIONAL_ZN_PRC national_price,
       MARGIN margin,
       MARGIN_PRCNT margin_percent
    from (select xbo.bu,
           isup.item,
           s.supplier,
           s.SUP_NAME,
           (select im.item
            from v_item_master im
           where nvl(im.item_grandparent, im.item_parent) = isup.item
             and im.item_level > im.tran_level
             and im.create_datetime =
               (select max(im2.create_datetime)
                from v_item_master im2
               where nvl(im2.item_grandparent, im2.item_parent) =
                   isup.item)
             and rownum = 1) as gtin,
           (select code
            from (select cfa.attrib_id ATTR_ID, cd.code
                from cfa_attrib cfa,
                   (select group_id, cfa_attribute, cfa_value
                    from item_supplier_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                    varchar2_2,
                                                    varchar2_3,
                                                    varchar2_4,
                                                    varchar2_5,
                                                    varchar2_6,
                                                    varchar2_7,
                                                    varchar2_8,
                                                    varchar2_9,
                                                    varchar2_10))
                   where item     = isup.item
                     and supplier = isup.supplier) cfa_unc,
                v_code_detail cd
               where cfa.group_id = cfa_unc.group_id
                 and upper(cfa.storage_col_name) =
                   upper(cfa_unc.cfa_attribute)
                 and cd.code_type = cfa.code_type
                 and cd.code = cfa_value) aux,
               xxadeo_mom_dvm x
           where (x.value_1) = aux.attr_id
             and x.func_area = 'CFA'
             and x.parameter = 'LINK_STATUS_ITEM_SUPP'
             and (x.bu = xbo.bu or x.bu is null or x.bu = -1)) as statut_lien_article_ID,
           (select code_desc
            from (select cfa.attrib_id ATTR_ID, cd.code_desc
                from cfa_attrib cfa,
                   (select group_id, cfa_attribute, cfa_value
                    from item_supplier_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                    varchar2_2,
                                                    varchar2_3,
                                                    varchar2_4,
                                                    varchar2_5,
                                                    varchar2_6,
                                                    varchar2_7,
                                                    varchar2_8,
                                                    varchar2_9,
                                                    varchar2_10))
                   where item     = isup.item
                     and supplier = isup.supplier) cfa_unc,
                v_code_detail cd
               where cfa.group_id = cfa_unc.group_id
                 and upper(cfa.storage_col_name) =
                   upper(cfa_unc.cfa_attribute)
                 and cd.code_type = cfa.code_type
                 and cd.code = cfa_value) aux,
               xxadeo_mom_dvm x
           where (x.value_1) = aux.attr_id
             and x.func_area = 'CFA'
             and x.parameter = 'LINK_STATUS_ITEM_SUPP'
             and (x.bu = xbo.bu or x.bu is null or x.bu = -1)) as statut_lien_article,
           (select code_desc
          from (select cfa.attrib_id ATTR_ID, cd.code_desc
              from cfa_attrib cfa,
                 (select group_id, cfa_attribute, cfa_value
                  from item_supplier_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                  varchar2_2,
                                                  varchar2_3,
                                                  varchar2_4,
                                                  varchar2_5,
                                                  varchar2_6,
                                                  varchar2_7,
                                                  varchar2_8,
                                                  varchar2_9,
                                                  varchar2_10))
                 where item     = isup.item
                   and supplier = isup.supplier) cfa_unc,
                 v_code_detail cd
             where cfa.group_id = cfa_unc.group_id
               and upper(cfa.storage_col_name) =
                 upper(cfa_unc.cfa_attribute)
               and cd.code_type = cfa.code_type
               and cd.code = cfa_value) aux,
             xxadeo_mom_dvm x
         where (x.value_1) = aux.attr_id
           and func_area = 'CFA'
           and parameter = 'BLOCKING_PO_ITEM_SUPP'
           and (x.bu = xbo.bu or x.bu is null or x.bu = -1)) as blocking_po_reason,
           isc.supp_pack_size as pcb,
           isc.unit_cost as unit_cost,
           s.currency_code as cost_currency,
          (select pcs.pcs_ca
             from xxadeo_v_area_pcs pcs
            where pcs.item = isup.item
              and pcs.bu = xbo.bu
              and nvl(pcs.effective_date,sysdate) = nvl((select max(pcs2.effective_date)
                                                           from xxadeo_v_area_pcs pcs2
                                                          where pcs2.item = pcs.item
                                                            and pcs2.bu = pcs.bu),sysdate)
              and rownum = 1) as PCS,
           (select selling_retail
                  from rpm_item_zone_price a inner join xxadeo_rpm_bu_price_zones b on(a.zone_id = b.zone_id)
                 where (bu_zone_type = 'NATZ' and
                    b.bu_id = xbo.bu and
                    item = isup.item)) as national_zn_prc,
          (select pcs.msv_ca_amount
             from xxadeo_v_area_pcs pcs
            where pcs.item = isup.item
              and pcs.bu = xbo.bu
              and nvl(pcs.effective_date,sysdate) = nvl((select max(pcs2.effective_date)
                                                           from xxadeo_v_area_pcs pcs2
                                                          where pcs2.item = pcs.item
                                                            and pcs2.bu = pcs.bu),sysdate)
              and rownum = 1) as margin,
          (select pcs.msv_ca_percent
             from xxadeo_v_area_pcs pcs
            where pcs.item = isup.item
              and pcs.bu = xbo.bu
              and nvl(pcs.effective_date,sysdate) = nvl((select max(pcs2.effective_date)
                                                           from xxadeo_v_area_pcs pcs2
                                                          where pcs2.item = pcs.item
                                                            and pcs2.bu = pcs.bu),sysdate)
              and rownum = 1) as margin_prcnt
        from xxadeo_v_sup_sites       s,
           v_item_supplier_tl  isup,
           v_item_supp_country isc,
           partner_org_unit    pou,
           xxadeo_bu_ou        xbo
       where s.supplier = isup.supplier
         and (isc.supplier(+) = isup.supplier and isc.item(+) = isup.item)
         and pou.partner = s.SUPPLIER
         and pou.org_unit_id = xbo.ou
         and s.addr_type = '04')
     where statut_lien_article_ID in (1,2)
  group by SUPPLIER,
       SUP_NAME,
       GTIN,
       STATUT_LIEN_ARTICLE,
       BLOCKING_PO_REASON,
       PCB,
       UNIT_COST,
       COST_CURRENCY,
       PCS,
       NATIONAL_ZN_PRC,
       MARGIN,
       MARGIN_PRCNT,
       BU,
       ITEM;
