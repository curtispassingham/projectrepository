/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_DEAL_DASHBOARD" for RR405                     */
/******************************************************************************/
begin
  execute immediate 'drop view XXADEO_V_DEAL_DASHBOARD';
exception
  when others then
    null;
end;
/
CREATE OR REPLACE VIEW XXADEO_V_DEAL_DASHBOARD AS
WITH table_deal_head AS(
select dh.deal_id,
       dh.type,
       dh.supplier,
       dh.partner_type,
       dic.group_no,
       dic.dept,
       dic.area,
       dh.status,
       dh.billing_type,
       dh.create_id,
       dh.active_date,
       dh.close_date,
       dh.create_datetime,
       dh.partner_id
  from XXADEO_V_DEAL_HEAD dh,
       XXADEO_V_DEAL_ITEMLOC dic
 where dh.deal_id = dic.deal_id(+)
   and dh.status in ('S', 'W')
   and exists (select '1'
                 from v_store st
                where (((dic.org_level = '5') and (dic.loc_type = 'S') and (dic.location = st.store))
                       or ((dic.org_level = '4') and (dic.district = st.district))
                       or ((dic.org_level = '3') and (dic.region = st.region))
                       or ((dic.org_level = '2') and (dic.area = st.area))
                       or ((dic.org_level = '1') and (dic.chain = st.chain))))
   and exists (select '1' 
                 from XXADEO_V_DEPS deps 
                where (dic.GROUP_NO is null or deps.GROUP_NO = dic.group_no)
                  and (dic.dept is null or deps.dept = dic.dept))
              )
select distinct deal_id,
                type,
                vendor,
                vendor_name,
                vendor_type,
                dept,
                sub_dept,
                area,
                status,
                billing_type,
                create_id,
                active_date,
                close_date,
                create_datetime
  from (select dh.deal_id,
               dh.type,
               to_char(dh.SUPPLIER) as vendor,
               s.sup_name as vendor_name,
               dh.PARTNER_TYPE as vendor_type,
               dh.group_no as dept,
               dh.dept as sub_dept,
               dh.area,
               dh.status,
               dh.billing_type,
               dh.create_id,
               trunc(dh.active_date) as active_date,
               dh.close_date,
               dh.create_datetime
          from table_deal_head    dh,
               xxadeo_v_sups      s
         where dh.supplier = s.supplier
        union all
        select dh.deal_id,
               dh.type,
               dh.PARTNER_ID as vendor,
               prt.partner_desc as vendor_name,
               dh.PARTNER_TYPE as vendor_type,
               dh.group_no as dept,
               dh.dept as sub_dept,
               dh.area,
               dh.status,
               dh.billing_type,
               dh.create_id,
               trunc(dh.active_date) as active_date,
               dh.close_date,
               dh.create_datetime
          from table_deal_head    dh,
               v_partner          prt
         where dh.PARTNER_ID = prt.partner_id
           and dh.PARTNER_TYPE = prt.partner_type); 