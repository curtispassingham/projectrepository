/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_LOC_PCS" for RB157                            */
/******************************************************************************/
create or replace view xxadeo_v_loc_pcs as
select pcs.item,
       pcs.supplier,
       pcs.zone_id,
       pcs.store,
       pcs.pcs,
       pcs.currency,
       pcs.circuit_principal,
       pcs.status,
       pcs.effective_date,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.pcs,
                                                I_item        => pcs.item,
                                                I_location    => pcs.zone_id,
                                                I_loc_type    => 1)) as Decimal(20,2)) msv_zone_amount,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.pcs,
                                                I_item        => pcs.item,
                                                I_location    => pcs.zone_id,
                                                I_loc_type    => 1,
                                                I_margin_type => '%')) as Decimal(20,4)) msv_zone_percent,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.pcs,
                                                I_item        => pcs.item,
                                                I_location    => pcs.zone_id,
                                                I_loc_type    => 1,
                                                I_margin_type => '%')) as Decimal(20,4))*100 msv_zone_percent_display,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.pcs,
                                                I_item        => pcs.item,
                                                I_location    => pcs.store,
                                                I_loc_type    => 0)) as Decimal (20,2)) msv_magasin_amount,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.pcs,
                                                I_item        => pcs.item,
                                                I_location    => pcs.store,
                                                I_loc_type    => 0,
                                                I_margin_type => '%')) as Decimal (20,4)) msv_magasin_percent,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.pcs,
                                                I_item        => pcs.item,
                                                I_location    => pcs.store,
                                                I_loc_type    => 0,
                                                I_margin_type => '%')) as Decimal (20,4))*100 msv_magasin_percent_display
  from (select /*+ LEADING(pcs,st,rzl)  index(pcs PK_XXADEO_LOCATION_PCS)*/
               pcs.item,
               pcs.supplier,
               pcs.store,
               pcs.pcs,
               (select st.currency_code from store st where st.store = pcs.store) currency,
               pcs.circuit_principal,
               pcs.status,
               pcs.effective_date,
               rzl.zone_id
          from xxadeo_location_pcs pcs,
               rpm_zone_location   rzl
         where pcs.store    = rzl.location(+)
           and pcs.status   = 'A') pcs;
comment on column XXADEO_V_LOC_PCS.ITEM is 'Item Identifier.';
comment on column XXADEO_V_LOC_PCS.SUPPLIER is 'Supplier Site Identifier.';
comment on column XXADEO_V_LOC_PCS.ZONE_ID is 'Zone Identifier taken from rpm_zone_location table.';
comment on column XXADEO_V_LOC_PCS.STORE is 'Store Identifier.';
comment on column XXADEO_V_LOC_PCS.PCS is 'PCS Store Amount, Taxes exclusive (VAT exclusive).';
comment on column XXADEO_V_LOC_PCS.CURRENCY is 'Store Currency.';
comment on column XXADEO_V_LOC_PCS.CIRCUIT_PRINCIPAL is 'Circuit Principal identifier Note: Valid values are (''1'', ''5'').';
comment on column XXADEO_V_LOC_PCS.STATUS is 'The Status of the record. Valid values: A - Active record; I - Inactive Record and ready to be purged. For this view, only records where status = ''A''';
comment on column XXADEO_V_LOC_PCS.EFFECTIVE_DATE is 'PCS Effective Date, used to identify actual PCS Store.';
comment on column XXADEO_V_LOC_PCS.MSV_ZONE_AMOUNT is 'Item/Zone Margin amount.';
comment on column XXADEO_V_LOC_PCS.MSV_ZONE_PERCENT is 'Item/Zone Margin percentage.';
comment on column XXADEO_V_LOC_PCS.MSV_ZONE_PERCENT_DISPLAY is 'Item/Zone Margin percentage in percentageformat.';
comment on column XXADEO_V_LOC_PCS.MSV_MAGASIN_AMOUNT is 'Item/Store Margin amount.';
comment on column XXADEO_V_LOC_PCS.MSV_MAGASIN_PERCENT is 'Item/Store Margin percentage.';
comment on column XXADEO_V_LOC_PCS.MSV_MAGASIN_PERCENT_DISPLAY is 'Item/Store Margin percentage format.';



