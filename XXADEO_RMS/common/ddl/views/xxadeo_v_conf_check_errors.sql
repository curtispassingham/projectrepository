/******************************************************************************/
/* CREATE DATE - November 2018                                                */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_V_CONF_CHECK_ERRORS"                            */
/******************************************************************************/


create or replace view xxadeo_v_conf_check_errors as
    select item, location, effective_date
          from (select a.con_check_err_id,
                       a.item,
                       a.location,
                       b.effective_date
                  from rpm_con_check_err a, rpm_con_check_err_detail b
                 where a.con_check_err_id = b.con_check_err_id
                   and exists (select 1
                          from v_store vs
                         where a.location = vs.STORE
                        union
                        select 1
                          from v_wh vw
                         where a.location =  vw.WH)
                   and exists
                 (select 1
                          from code_detail cd
                         where cd.CODE_TYPE = 'CCHE'
                           and lower(cd.code_desc) = lower(a.message_key))
                   and a.error_date between get_vdate - 7 and get_vdate)
         group by item, location, effective_date
        union all
        select item, nvl(zone_id, location) , effective_date
          from xxadeo_rpm_stage_price_change stg
         where lower(error_message) <> 'conflict_exists'
           and exists (select 1
                  from v_store vs
                 where stg.location = vs.STORE
                union
                select 1
                  from v_wh vw
                 where stg.location = vw.WH
                union
                select 1
                  from v_store vs, rpm_zone_location zn
                 where vs.STORE = zn.location
                   and stg.zone_id = zn.zone_id)
           and exists
         (select 1
                  from code_detail cd
                 where cd.CODE_TYPE = 'CCHE'
                   and lower(cd.code_desc) = lower(error_message))
           and stg.create_datetime between get_vdate - 7 and get_vdate
           group by item, nvl(zone_id, location) , effective_date;