/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_MV_INCOMPLETE_ITEMS" for RR143/144              */
/******************************************************************************/

begin
  execute immediate 'drop materialized view xxadeo_mv_incomplete_items';
exception
  when others then
    null;
end;
/

create materialized view xxadeo_mv_incomplete_items
REFRESH FORCE ON DEMAND WITH ROWID USING TRUSTED CONSTRAINTS 
as
select * from XXADEO_V_INCOMPLETE_ITEMS;  

