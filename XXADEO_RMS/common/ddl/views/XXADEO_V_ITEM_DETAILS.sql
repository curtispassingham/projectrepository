/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_V_ITEM_DETAILS"                                 */
/******************************************************************************/
begin
  execute immediate 'drop view XXADEO_V_ITEM_DETAILS';
exception
  when others then
    null;
end;
/
create or replace view XXADEO_V_ITEM_DETAILS as
with item_bs as(
 select item, BU_SUBSCRIPTION  from (
 select im.ITEM,
        decode(uilv.uda_id,
               (select value_1
                  from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                I_parameter => 'BU_SUBSCRIPTION'))
                 where bu is null
                    or bu = -1),
               uv.uda_value,
               null) as BU_SUBSCRIPTION
   from v_item_master im, uda_item_lov uilv, v_uda_values_tl uv
  where im.item = uilv.item
    and uilv.uda_id = uv.uda_id
    and uilv.uda_value = uv.uda_value)
    where BU_SUBSCRIPTION is not null)
select
  MAX(FILTER_ORG_ID) FILTER_ORG_ID,
  ITEM ITEM,
  ITEM_DESC,
  SUB_DEPTID,
  SUB_DEPTDESC,
  CLASS,
  CLASS_DESC,
  SUBCLASS,
  SUBCLASS_DESC,
  MAX(NATIONAL_PRICE) NATIONAL_PRICE,
  MAX(GTIN) GTIN,
  MAX(SUBTYPOLOGY) SUBTYPOLOGY,
  MAX(RANGELETTER) RANGELETTER,
  MAX(UNIT_OF_MEASURE) UNIT_OF_MEASURE,
  MAX(QT_CONTENANCE) QT_CONTENANCE,
  MAX(UN_CONTENANCE) UN_CONTENANCE,
  MAX(LIFE_CYCLE_STATUS) LIFE_CYCLE_STATUS,
  MAX(TOP1000) TOP1000,
  MAX(TOP_MDH) TOP_MDH,
  MAX(TOP_HYPER_100) TOP_HYPER_100,
  MAX(CLASSMENT_BU) CLASSMENT_BU,
  MAX(PRICE_POLICY) PRICE_POLICY,
  MAX(RETAIL_PRC_BLOCK) RETAIL_PRC_BLOCK,
  MAX(MODE_ASSORT) MODE_ASSORT,
  MAX(SALE_START_DT) SALE_START_DT,
  MAX(SALE_END_DT) SALE_END_DT,
  MAX(PRIX_CESSION_STANDARD) PRIX_CESSION_STANDARD,
  MAX(MARGIN) MARGIN,
  MAX(MARGIN_PRCNT) MARGIN_PRCNT,
  MAX(CHANGE_COST_PRICE) CHANGE_COST_PRICE,
  MAX(SUBSCRIBED_BU) SUBSCRIBED_BU,
  MAX(ITEM_URL) ITEM_URL
  from(
  select ibs.bU_SUBSCRIPTION as filter_org_id,              
               im.item,
               im.item_desc,
               im.dept as sub_deptID,
               dep.dept_name as sub_deptDesc,
               im.class,
               (select c.class_name
                  from v_class c
                 where c.class = im.class
                   and c.dept = im.dept) as class_Desc,
               im.subclass,
               (select sc.sub_name
                  from v_subclass sc
                 where sc.subclass = im.subclass
                   and sc.dept = im.dept
                   and sc.class = im.class) as subclass_Desc,
               decode(uilv.uda_id,
                      nvl((select value_1
                            from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                          I_parameter => 'ITEM_SUBTYPE'))
                           where bu is null or bu = -1),
                          (select value_1
                             from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                           I_parameter => 'ITEM_SUBTYPE',
                                                           I_bu        => ibs.bU_SUBSCRIPTION)))), 
                      uv.uda_value_desc,
                      null) as SubTypology,
        decode(uilv.uda_id,
        nvl((select value_1
          from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                          I_parameter => 'RANGE_SIZE',
												  I_bu        => ibs.bU_SUBSCRIPTION))), 
          (select value_1
           from (select value_1, bu
               from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                               I_parameter => 'RANGE_SIZE'))
              where bu is null or bu = -1))),
        uv.uda_value_desc,
        null) as rangeLetter,
        im.standard_uom unit_of_measure,
        (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                            from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(number_11,
                                                                                                  number_12,
                                                                                                  number_13,
                                                                                                  number_14,
                                                                                                  number_15,
                                                                                                  number_16,
                                                                                                  number_17,
                                                                                                  number_18,
                                                                                                  number_19,
                                                                                                  number_20))
                                 where item = im.item ) cfa_unc 
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'QUANTITY_CAPACITY'
                   and (bu = ibs.bU_SUBSCRIPTION or bu is null or bu = -1)) as qt_contenance, 
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item = im.item) cfa_unc 
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'CONTAIN_UOM_ITEM'
                   and (bu = ibs.bU_SUBSCRIPTION or bu is null or bu = -1)) as un_contenance, 
                   decode(uilv.uda_id,
                      nvl((select value_1
                            from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                          I_parameter => 'LIFECYCLE_STATUS',
                                                          I_bu        => ibs.bU_SUBSCRIPTION))), 
                          (select value_1
                             from (select value_1, bu
                                     from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                                   I_parameter => 'LIFECYCLE_STATUS'))
                                    where bu is null or bu = -1))),
                      uv.uda_value_desc,
                      null) as life_cycle_status,
                decode(uilv.uda_id,
                      nvl((select value_1
                            from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                          I_parameter => 'TOP_1000'))
                           where bu is null or bu = -1),
                          (select value_1
                             from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                           I_parameter => 'TOP_1000',
                                                           I_bu        => ibs.bU_SUBSCRIPTION)))), 
                      uv.uda_value_desc,
                      null) as top1000,
               decode(uilv.uda_id,
                      nvl((select value_1
                            from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                          I_parameter => 'TOP_MDH'))
                           where bu is null or bu = -1),
                          (select value_1
                             from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                           I_parameter => 'TOP_MDH',
                                                           I_bu        => ibs.bU_SUBSCRIPTION)))), 
                      uv.uda_value_desc,
                      null) as top_mdh,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(number_11,
                                                                                                  number_12,
                                                                                                  number_13,
                                                                                                  number_14,
                                                                                                  number_15,
                                                                                                  number_16,
                                                                                                  number_17,
                                                                                                  number_18,
                                                                                                  number_19,
                                                                                                  number_20))
                                 where item = im.item
                                   and group_id = imcfa.group_id) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'RANKING_ITEM'
                   and (bu = ibs.bU_SUBSCRIPTION or bu is null or bu = -1)) as classment_bu, 
               (select cfa_value
                  from (select cfa.attrib_id ATTR_ID, cfa_value
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item = im.item
                                   and group_id = imcfa.group_id) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'HYPER_100_ITEM'
                   and (bu = ibs.bU_SUBSCRIPTION or bu is null or bu = -1)) as top_hyper_100,                
               (select cfa_value
                  from (select cfa.attrib_id ATTR_ID, cfa_value
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item = im.item
                                   and group_id = imcfa.group_id) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'PRICE_POLICY_ITEM'
                   and (bu = ibs.bU_SUBSCRIPTION or bu is null or bu = -1)) as price_Policy, 
                   (select cfa_value
                  from (select cfa.attrib_id ATTR_ID, cfa_value
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item = im.item
                                   and group_id = imcfa.group_id) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'RETAIL_PRICE_BLOCKING'
                   and (bu = ibs.bU_SUBSCRIPTION or bu is null or bu = -1)) as retail_prc_block, 
               (select cfa_value
                  from (select cfa.attrib_id ATTR_ID, cfa_value
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item = im.item
                                   and group_id = imcfa.group_id) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'PURCHASE_BLK_ITEM'
                   and (bu = ibs.bU_SUBSCRIPTION or bu is null or bu = -1)) as CHANGE_COST_PRICE,  			    
           decode(uilv.uda_id,
                      nvl((select value_1
                            from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                          I_parameter => 'ASSORTMENT_MODE',
                                                          I_bu        => ibs.bU_SUBSCRIPTION))), 
                          (select value_1
                             from (select value_1, bu
                                     from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                                   I_parameter => 'ASSORTMENT_MODE'))
                                    where bu is null or bu = -1))),
                      uv.uda_value_desc,
                      null) as mode_assort,
               decode(uidt.uda_id,
                      nvl((select value_1
                            from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                          I_parameter => 'SALES_START_DATE',
                                                          I_bu        => ibs.bU_SUBSCRIPTION))), 
                          (select value_1
                             from (select value_1, bu
                                     from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                                   I_parameter => 'SALES_START_DATE'))
                                    where bu is null or bu = -1))),
                      uidt.uda_date,
                      null) as sale_start_dt,
               decode(uidt.uda_id,
                      nvl((select value_1
                            from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                          I_parameter => 'SALES_END_DATE',
                                                          I_bu        => ibs.bU_SUBSCRIPTION))), 
                          (select value_1
                             from (select value_1, bu
                                     from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                                   I_parameter => 'SALES_END_DATE'))
                                    where bu is null or bu = -1))),
                      uidt.uda_date,
                      null) as sale_end_dt,
         (select selling_retail
   from rpm_item_zone_price a inner join xxadeo_rpm_bu_price_zones b on(a.zone_id = b.zone_id)
   where (bu_zone_type = 'NATZ' and
		  bu_id = ibs.bU_SUBSCRIPTION and  
      item = im.item)) as NATIONAL_PRICE,
       (select item 
          from (
                select item,
                       create_datetime 
                  from v_item_master im1
                 where exists (
                               select 1 
                                 from v_item_supplier_tl ist1
                                where exists (
                                              select 1 
                                                from v_item_supplier_tl ist2  
                                               where ist2.item = im.item
                                                 and ist1.item = ist2.item
                                                 and ist1.supplier = ist2.supplier)
                                  and im1.item = ist1.item)
                  and im1.item_level > im1.tran_level 
                  and nvl(im1.item_grandparent,im1.item_parent) = im.item 
                  order by im1.create_datetime desc) 
           where rownum = 1) as gtin,
      (select listagg(uda_value_desc, '; ') within group (order by uda_value)
        from uda_item_lov inner join v_uda_values_tl using(uda_id, uda_value)
        where uda_id = (select value_1
                  from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                I_parameter => 'BU_SUBSCRIPTION'))
                 where bu is null
                    or bu = -1)
          and item = im.item) as SUBSCRIBED_BU,
         (select image_addr || image_name from item_image where item = im.item) as ITEM_URL,
      (select pcs.pcs_ca
         from xxadeo_v_area_pcs pcs
        where pcs.item = im.item
          and pcs.bu = ibs.BU_SUBSCRIPTION
          and nvl(pcs.effective_date,sysdate) = nvl((select max(pcs2.effective_date)
                                                       from xxadeo_v_area_pcs pcs2
                                                      where pcs2.item = pcs.item
                                                        and pcs2.bu = pcs.bu),sysdate)
          and rownum = 1) as prix_cession_standard,
      (select pcs.msv_ca_amount
         from xxadeo_v_area_pcs pcs
        where pcs.item = im.item
          and pcs.bu = ibs.BU_SUBSCRIPTION
          and nvl(pcs.effective_date,sysdate) = nvl((select max(pcs2.effective_date)
                                                       from xxadeo_v_area_pcs pcs2
                                                      where pcs2.item = pcs.item
                                                        and pcs2.bu = pcs.bu),sysdate)
          and rownum = 1)  as margin,
      (select pcs.msv_ca_percent
         from xxadeo_v_area_pcs pcs
        where pcs.item = im.item
          and pcs.bu = ibs.BU_SUBSCRIPTION
          and nvl(pcs.effective_date,sysdate) = nvl((select max(pcs2.effective_date)
                                                       from xxadeo_v_area_pcs pcs2
                                                      where pcs2.item = pcs.item
                                                        and pcs2.bu = pcs.bu),sysdate)
          and rownum = 1) as margin_prcnt
          from v_item_master        im,
               v_deps               dep,
               uda_item_lov         uilv,
               uda_item_date        uidt,
               v_uda_values_tl      uv,
               item_master_cfa_ext  imcfa,
               cfa_attrib           ca,
               cfa_attrib_group     cag,
               cfa_attrib_group_set cags,
               cfa_ext_entity       cee,
               filter_group_org     fgo,
               item_bs              ibs
         where im.item = uilv.item(+)
           and im.item = uidt.item(+)
           and uilv.uda_id = uv.uda_id
           and uilv.uda_value = uv.uda_value
           and im.item = imcfa.item(+)
           and im.item = ibs.item
           and ibs.bU_SUBSCRIPTION = fgo.filter_org_id
           and im.dept = dep.DEPT
           and im.group_no = dep.group_no
           and im.division = dep.DIVISION
           and ca.group_id = imcfa.group_id(+)
           and ca.group_id = cag.group_id(+)
           and cag.group_set_id = cags.group_set_id(+)
           and cags.ext_entity_id = cee.ext_entity_id
           and cee.base_rms_table = 'ITEM_MASTER'
           and fgo.sec_group_id = cags.group_set_id
           and fgo.filter_org_level = 'A')
 group by FILTER_ORG_ID,
          ITEM,
          ITEM_DESC,
          SUB_DEPTID,
          SUB_DEPTDESC,
          CLASS,
          CLASS_DESC,
          SUBCLASS,
          SUBCLASS_DESC;
 