/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_V_COST_SUSP_SUP_DET_LOC"                        */
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_V_COST_SUSP_SUP_DET_LOC
(cost_change, supplier, origin_country_id, item, loc_type, loc, bracket_value1, bracket_uom1, bracket_value2, unit_cost, cost_change_type, cost_change_value, recalc_ord_ind, default_bracket_ind, sub_dept, sup_dept_seq_no, delivery_country_id)
AS
SELECT COST_CHANGE, SUPPLIER, ORIGIN_COUNTRY_ID, ITEM, LOC_TYPE, LOC, BRACKET_VALUE1, BRACKET_UOM1, BRACKET_VALUE2, UNIT_COST, COST_CHANGE_TYPE, COST_CHANGE_VALUE, RECALC_ORD_IND, DEFAULT_BRACKET_IND, DEPT, SUP_DEPT_SEQ_NO, DELIVERY_COUNTRY_ID
FROM COST_SUSP_SUP_DETAIL_LOC;  