/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_V_COST_SUSP_SUP_HEAD"                           */
/******************************************************************************/
begin
  execute immediate 'drop view XXADEO_V_COST_SUSP_SUP_HEAD';
exception
  when others then
    null;
end;
/
create or replace view XXADEO_V_COST_SUSP_SUP_HEAD
(cost_change, cost_change_desc, reason, active_date, status, cost_change_origin, create_date, create_id, approval_date, approval_id)
as
select cost_change,
       cost_change_desc,
       reason,
       trunc(active_date),
       status,
       cost_change_origin,
       create_date,
       create_id,
       approval_date,
       approval_id
  from cost_susp_sup_head xvcssh
 where xvcssh.status in ('W', 'S')
   and exists (select 1
                 from xxadeo_v_cost_susp_sup_det  xvcssd,
                      xxadeo_v_sups xvs
                where xvcssh.cost_change = xvcssd.cost_change
                  and xvcssd.supplier = xvs.supplier
               union all
               select 1
                 from xxadeo_v_cost_susp_sup_det_loc  xvcssdl,
                      xxadeo_v_sups xvs
                where xvcssh.cost_change = xvcssdl.cost_change
                  and xvcssdl.supplier = xvs.supplier)
   and exists (select 1
                 from xxadeo_v_cost_susp_sup_det  xvcssd,
                      xxadeo_v_deps xvd
                where xvcssh.cost_change = xvcssd.cost_change
                  and nvl(xvcssd.sub_dept, xvcssd.sup_dept_seq_no) = xvd.DEPT
               union all
               select 1
                 from xxadeo_v_cost_susp_sup_det_loc  xvcssdl,
                      xxadeo_v_deps xvd
                where xvcssh.cost_change = xvcssdl.cost_change
                  and nvl(xvcssdl.sub_dept, xvcssdl.sup_dept_seq_no) = xvd.DEPT); 