/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_SUBCLASS" for RB119                           */
/******************************************************************************/
CREATE OR REPLACE VIEW XXADEO_V_SUBCLASS AS
SELECT DEPT, CLASS, SUBCLASS, SUB_NAME, GROUP_NO
   FROM (SELECT sub.DEPT,
                sub.CLASS,
                sub.SUBCLASS,
                sub.SUB_NAME,
                (select group_no from XXADEO_V_DEPS xvd where xvd.DEPT = sub.DEPT) as group_no,
                (select code_desc
                   from (select dept,
                                cfa_unc.group_id,
                                cfa.attrib_id ATTR_ID,
                                cd.code_desc
                           from cfa_attrib cfa,
                                (select dept,
                                        group_id,
                                        cfa_attribute,
                                        cfa_value
                                   from subclass_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                varchar2_2,
                                                                                                varchar2_3,
                                                                                                varchar2_4,
                                                                                                varchar2_5,
                                                                                                varchar2_6,
                                                                                                varchar2_7,
                                                                                                varchar2_8,
                                                                                                varchar2_9,
                                                                                                varchar2_10))
                                  where sub.dept     = dept
                                    and sub.class    = class
                                    and sub.SUBCLASS = subclass) cfa_unc,
                                v_code_detail cd
                          where cfa.group_id = cfa_unc.group_id
                            and upper(cfa.storage_col_name) =
                                upper(cfa_unc.cfa_attribute)
                            and cd.code_type = cfa.code_type
                            and cd.code = cfa_value) aux,
                        xxadeo_mom_dvm x
                  where (x.value_1) = aux.attr_id
                    and func_area = 'CFA'
                    and parameter = 'SUBCLASS_STATUS') as status
           FROM v_subclass sub, subclass_cfa_ext sce
          where sub.dept     = sce.dept
            and sub.class    = sce.class
            and sub.SUBCLASS = sce.SUBCLASS)
  where upper(status) = 'ACTIVE';