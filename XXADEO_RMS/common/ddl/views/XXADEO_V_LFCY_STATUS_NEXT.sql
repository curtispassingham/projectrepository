create or replace view xxadeo_v_lfcy_status_next 
as
select item, lfcy_entity_type, lfcy_entity
, rd.value_1 lfcy_status
  , sum(case 
    when ic.val_status is null then 0
    when ic.val_status = 'Y' then 1
    when ic.val_status = 'N' then 0
  end) as YES
  , sum(case 
    when ic.val_status is null then 0
    when ic.val_status = 'Y' then 0
    when ic.val_status = 'N' then 1
  end) as NO
  , sum(case 
    when ic.val_status is null then 1
    when ic.val_status = 'Y' then 0
    when ic.val_status = 'N' then 0
  end) as NEV
  , sum(case 
    when ic.val_status is null then 0
    when ic.val_status = 'Y' then 0
    when ic.val_status = 'N' then 0
    when ic.val_status = 'E' then 1
  end) as ERR
  , min(case 
    when ic.val_status is null then r.call_seq_no
    when ic.val_status = 'Y' then null
    when ic.val_status = 'N' then r.call_seq_no
    when ic.val_status = 'E' then r.call_seq_no
  end) as next_rule_no
  , count(1) NRULES
  , ic.process_id
from xxadeo_lfcy_item_cond ic
, xxadeo_system_rules r
, xxadeo_system_rules_detail rd
where r.rule_id = ic.lfcy_cond_id
and rd.rule_id = ic.lfcy_cond_id
and rd.parameter = 'LFCY_STATUS'
group by ic.item, ic.lfcy_entity_type, ic.lfcy_entity, rd.value_1, ic.process_id;
