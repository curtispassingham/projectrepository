/**********************************************************************************/
/* CREATE DATE - October 2018                                                     */
/* CREATE USER - Elsa Barros                                                      */
/* PROJECT     - ADEO                                                             */
/* DESCRIPTION - MATERIALIZED VIEW "XXADEO_MV_AREA_PCS" for RR157                 */
/**********************************************************************************/
begin
  execute immediate 'drop materialized view xxadeo_mv_area_pcs';
exception
  when others then
    null;
end;
/
create materialized view xxadeo_mv_area_pcs
refresh force on demand
as
select pcs.item,
       pcs.supplier,
       pcs.zone_id,
       pcs.bu,
       pcs.supplier_pcs_ca,
       pcs.pcs_ca,
       pcs.currency,
       pcs.circuit_principal,
       pcs.status,
       pcs.effective_date,
       pcs.msv_ca_zone_amount,
       pcs.msv_ca_zone_percent,
       pcs.msv_ca_zone_percent_display,
       pcs.msv_ca_amount,
       pcs.msv_ca_percent,
       pcs.msv_ca_percent_display,
       pcs.msv_ca_supp_amount,
       pcs.msv_ca_supp_percent,
       pcs.msv_ca_supp_percent_display 
  from xxadeo_v_bu_pcs pcs;
-- Create index 
create index PK_XXADEO_MV_AREA_PCS 
  on XXADEO_MV_AREA_PCS(item, supplier, bu, zone_id);  
