/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_ITEMS_DASHBOARD" for RB119                    */
/******************************************************************************/
begin
  execute immediate 'drop view xxadeo_v_items_dashboard';
exception
  when others then
    null;
end;
/
create or replace view xxadeo_v_items_dashboard
(filter_org_id, status, pack_ind, pack_type, item_level, tran_level, check_uda_ind, group_no, group_name, dept, dept_name, class, class_name, subclass, sub_name, classment_bu, item, item_desc, sous_typo_step, unite_consommateur, qt_contenance, un_contenance, mode_assort, taille_gamme, prix_cession_standard, prix_vente_conseil, margin, pv_principal_pub, modif_pv_magasin, etat_cycle_vie, date_souh_pass_actif_comm, top_1000, top_hyper_100, top_mdh, modif_pa_magasin, actif_comm_date_eff, forecast_ind)
as
with 
item_bs as
 (select item, BU_SUBSCRIPTION
    from (select im.ITEM, uv.uda_value as BU_SUBSCRIPTION
            from item_master     im,
                 uda_item_lov    uilv,
                 v_uda_values_tl uv
           where im.item = uilv.item
             and uilv.uda_id = uv.uda_id
             and uilv.uda_value = uv.uda_value
             and im.item_level = 1
             and uilv.uda_id = (select value_1
                                  from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                                I_parameter => 'BU_SUBSCRIPTION'))
                                 where bu is null
                                    or bu = -1))
            )
,
udas as
 (select parameter, bu, to_number(value_1) value_1
    from xxadeo_mom_dvm dvm, uda uda
   where uda.uda_id = dvm.value_1
     and func_area = 'UDA'
     )
select FILTER_ORG_ID,
       STATUS,
       PACK_IND,
       PACK_TYPE,
       ITEM_LEVEL,
       TRAN_LEVEL,
       CHECK_UDA_IND,
       GROUP_NO,
       group_name,
       DEPT,
       dept_name,
       CLASS,
       class_name,
       SUBCLASS,
       sub_name,
       classment_bu as classment_bu,
       ITEM,
       ITEM_DESC,
       sous_typo_step as sous_typo_step,
       standard_uom as UNITE_CONSOMMATEUR,
       qt_contenance as QT_CONTENANCE,
       un_contenance as UN_CONTENANCE,
       mode_assort as mode_assort,
       taille_gamme as taille_gamme,
       prix_cession_standard as PRIX_CESSION_STANDARD,
       prix_vente_conseil as PRIX_VENTE_CONSEIL,
       margin as MARGIN,
       pv_principal_pub as PV_PRINCIPAL_PUB,
       modif_pv_magasin as MODIF_PV_MAGASIN,
       etat_cycle_vie as etat_cycle_vie,
       date_souh_pass_actif_comm as DATE_SOUH_PASS_ACTIF_COMM,
       top1000 as TOP_1000,
       hyper_100_or_20_80 as TOP_HYPER_100,
       mdd_mdh as TOP_MDH,
       prix_achat_modif_magasin as MODIF_PA_MAGASIN,
       actif_comm_date_eff as ACTIF_COMM_DATE_EFF,
       FORECAST_IND
  from (select ibs.BU_SUBSCRIPTION as filter_org_id,
               im.status,
               im.pack_ind,
               im.pack_type,
               im.item_level,
               im.tran_level,
               im.check_uda_ind,
               dep.group_no,
               (select g.group_name
                  from v_groups g
                 where g.group_no = dep.group_no) as group_name,
               im.dept,
               dep.dept_name,
               im.class,
               (select c.class_name
                  from v_class c
                 where c.class = im.class
                   and c.dept = im.dept) as class_name,
               im.subclass,
               (select sc.sub_name
                  from v_subclass sc
                 where sc.subclass = im.subclass
                   and sc.dept = im.dept
                   and sc.class = im.class) as sub_name,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(number_11,
                                                                                                  number_12,
                                                                                                  number_13,
                                                                                                  number_14,
                                                                                                  number_15,
                                                                                                  number_16,
                                                                                                  number_17,
                                                                                                  number_18,
                                                                                                  number_19,
                                                                                                  number_20))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'RANKING_ITEM'
                   and (bu = ibs.BU_SUBSCRIPTION or bu is null or bu = -1)) as classment_bu,
               im.item,
               im.item_desc,
             (select udaval.uda_value_desc
          from (select item, uda_value, uda_id
                  from uda_item_lov uiv, udas
                 where uiv.uda_id = value_1
                   and udas.parameter = 'ITEM_SUBTYPE'
                   and uiv.item = im.item
                   and (udas.bu = ibs.bu_subscription or udas.bu is null or udas.bu = -1)) uil1,
               v_uda_values_tl udaval
         where udaval.uda_id = uil1.uda_id
           and udaval.uda_value = uil1.uda_value) as sous_typo_step,
       (select udaval.uda_value_desc
          from (select item, uda_value, uda_id
                  from uda_item_lov uiv, udas
                 where uiv.uda_id = value_1
                   and udas.parameter = 'ASSORTMENT_MODE'
                   and uiv.item = im.item
                   and (udas.bu = ibs.bu_subscription or udas.bu is null or udas.bu = -1)) uil1,
               v_uda_values_tl udaval
         where udaval.uda_id = uil1.uda_id
           and udaval.uda_value = uil1.uda_value) as mode_assort,
       (select udaval.uda_value_desc
          from (select item, uda_value, uda_id
                  from uda_item_lov uiv, udas
                 where uiv.uda_id = value_1
                   and udas.parameter = 'RANGE_SIZE'
                   and uiv.item = im.item
                   and (udas.bu = ibs.bu_subscription or udas.bu is null or udas.bu = -1)) uil1,
               v_uda_values_tl udaval
         where udaval.uda_id = uil1.uda_id
           and udaval.uda_value = uil1.uda_value
           ) as taille_gamme,
       (select udaval.uda_value_desc
          from (select item, uda_value, uda_id
                  from uda_item_lov uiv, udas
                 where uiv.uda_id = value_1
                   and udas.parameter = 'LIFECYCLE_STATUS'
                   and uiv.item = im.item
                   and (udas.bu = ibs.bu_subscription or udas.bu is null or udas.bu = -1)) uil1,
               v_uda_values_tl udaval
         where udaval.uda_id = uil1.uda_id
           and udaval.uda_value = uil1.uda_value) as etat_cycle_vie,
       (select udaval.uda_value_desc
          from (select item, uda_value, uda_id
                  from uda_item_lov uiv, udas
                 where uiv.uda_id = value_1
                   and udas.parameter = 'TOP_1000'
                   and uiv.item = im.item
                   and (udas.bu = ibs.bu_subscription or udas.bu is null or udas.bu = -1)) uil1,
               v_uda_values_tl udaval
         where udaval.uda_id = uil1.uda_id
           and udaval.uda_value = uil1.uda_value) as top1000,
       (select udaval.uda_value_desc
          from (select item, uda_value, uda_id
                  from uda_item_lov uiv, udas
                 where uiv.uda_id = udas.value_1
                   and udas.parameter = 'TOP_MDH'
                   and uiv.item = im.item
                   and (udas.bu = ibs.bu_subscription or udas.bu is null or udas.bu = -1)) uil1,
               v_uda_values_tl udaval
         where udaval.uda_id = uil1.uda_id
           and udaval.uda_value = uil1.uda_value) as mdd_mdh,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext ext unpivot(cfa_value for cfa_attribute in(number_11,
                                                                                                  number_12,
                                                                                                  number_13,
                                                                                                  number_14,
                                                                                                  number_15,
                                                                                                  number_16,
                                                                                                  number_17,
                                                                                                  number_18,
                                                                                                  number_19,
                                                                                                  number_20))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and x.func_area = 'CFA'
                   and x.parameter = 'QUANTITY_CAPACITY'
                   and (x.bu = ibs.BU_SUBSCRIPTION or x.bu is null or x.bu = -1)) as qt_contenance,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and x.func_area = 'CFA'
                   and x.parameter = 'CONTAIN_UOM_ITEM'
                   and (x.bu = ibs.BU_SUBSCRIPTION or x.bu is null or x.bu = -1)) as un_contenance,
         (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and x.func_area = 'CFA'
                   and x.parameter = 'RETAIL_PRICE_BLOCKING'
                   and (x.bu = ibs.BU_SUBSCRIPTION or x.bu is null or x.bu = -1)) as modif_pv_magasin,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext ext unpivot(cfa_value for cfa_attribute in(date_21,
                                                                                                  date_22,
                                                                                                  date_23,
                                                                                                  date_24,
                                                                                                  date_25))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and x.func_area = 'CFA'
                   and x.parameter = 'ACTIF_DDATE_ITEM'
                   and (x.bu = ibs.BU_SUBSCRIPTION or x.bu is null or x.bu = -1)) as date_souh_pass_actif_comm,
               (select cfa_value
                  from (select cfa.attrib_id ATTR_ID, cfa_value
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and x.func_area = 'CFA'
                   and x.parameter = 'HYPER_100_ITEM'
                   and (x.bu = ibs.BU_SUBSCRIPTION or x.bu is null or x.bu = -1)) as hyper_100_or_20_80,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and x.func_area = 'CFA'
                   and x.parameter = 'PURCHASE_BLK_ITEM'
                   and (x.bu = ibs.BU_SUBSCRIPTION or x.bu is null or x.bu = -1)) as prix_achat_modif_magasin,
               im.standard_uom,
               im.forecast_ind,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(date_21,
                                                                                                  date_22,
                                                                                                  date_23,
                                                                                                  date_24,
                                                                                                  date_25))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'ACTIF_COM_DATE_ITEM'
                   and (bu = ibs.BU_SUBSCRIPTION or bu is null or bu = -1)) as actif_comm_date_eff,
               (select pcs.pcs_ca
                  from xxadeo_v_area_pcs pcs
                 where pcs.item = im.item
                   and pcs.bu = ibs.BU_SUBSCRIPTION
                   and nvl(pcs.effective_date,sysdate) = nvl((select max(pcs2.effective_date)
                                                                from xxadeo_v_area_pcs pcs2
                                                               where pcs2.item = pcs.item
                                                                 and pcs2.bu = pcs.bu),sysdate)
                   and rownum = 1) as prix_cession_standard,
               (select selling_retail
                  from rpm_item_zone_price a inner join xxadeo_rpm_bu_price_zones b on(a.zone_id = b.zone_id)
                 where (bu_zone_type = 'NATZ' and
                    bu_id = ibs.BU_SUBSCRIPTION and
                    item = im.item)) as prix_vente_conseil,
               (select pcs.msv_ca_amount
                  from xxadeo_v_area_pcs pcs
                 where pcs.item = im.item
                   and pcs.bu = ibs.BU_SUBSCRIPTION
                   and nvl(pcs.effective_date,sysdate) = nvl((select max(pcs2.effective_date)
                                                                from xxadeo_v_area_pcs pcs2
                                                               where pcs2.item = pcs.item
                                                                 and pcs2.bu = pcs.bu),sysdate)
                   and rownum = 1) as MARGIN,
                (select code_desc
                  from (select cfa.attrib_id ATTR_ID, cd.code_desc
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item = im.item) cfa_unc,
                               v_code_detail cd
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)
                           and cd.code_type = cfa.code_type
                           and cd.code = cfa_value) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and x.func_area = 'CFA'
                   and x.parameter = 'PV_PUBLICATION_ITEM'
                   and (x.bu = ibs.BU_SUBSCRIPTION or x.bu is null or x.bu = -1)) as pv_principal_pub
          from v_item_master im,
               item_bs ibs,
               xxadeo_v_deps dep,
               xxadeo_v_class cla,
               xxadeo_v_subclass sub
          where im.dept = dep.dept
            and dep.group_no = im.group_no 
            and dep.DEPT = im.dept 
            and cla.DEPT = dep.dept 
            and cla.CLASS = im.CLASS 
            and sub.DEPT = cla.dept 
            and sub.CLASS = cla.CLASS 
            and sub.SUBCLASS = im.SUBCLASS 
            and im.item = ibs.item)
     order by GROUP_NO, dept, class, subclass, classment_bu, item;
