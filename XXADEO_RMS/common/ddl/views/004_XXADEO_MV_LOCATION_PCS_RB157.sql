/**********************************************************************************/
/* CREATE DATE - October 2018                                                     */
/* CREATE USER - Elsa Barros                                                      */
/* PROJECT     - ADEO                                                             */
/* DESCRIPTION - MATERIALIZED VIEW "XXADEO_MV_LOCATION_PCS" for RR157             */
/**********************************************************************************/
begin
  execute immediate 'drop materialized view xxadeo_mv_location_pcs';
exception
  when others then
    null;
end;
/
create materialized view xxadeo_mv_location_pcs
refresh force on demand
as
select /*+ LEADING(pcs)  index(pcs PK_XXADEO_LOCATION_PCS)*/
       pcs.item,
       pcs.supplier,
       pcs.zone_id,
       pcs.store,
       pcs.pcs,
       pcs.currency,
       pcs.circuit_principal,
       pcs.status,
       pcs.effective_date,
       pcs.msv_zone_amount,
       pcs.msv_zone_percent,
       pcs.msv_zone_percent_display,
       pcs.msv_magasin_amount,
       pcs.msv_magasin_percent,
       pcs.msv_magasin_percent_display
  from xxadeo_v_loc_pcs pcs;
-- Create index 
create index PK_XXADEO_MV_LOCATION_PCS 
  on XXADEO_MV_LOCATION_PCS(item, supplier, store, zone_id);

