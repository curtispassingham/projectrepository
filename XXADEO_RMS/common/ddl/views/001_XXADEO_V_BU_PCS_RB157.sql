/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_BU_PCS" for RB157                             */
/******************************************************************************/
create or replace view xxadeo_v_bu_pcs as
select pcs.item,
       pcs.supplier,
       pcs.zone_id,
       pcs.bu,
       pcs.pcs supplier_pcs_ca,
       cast(pcs.max_pcs as Decimal (20,4)) pcs_ca,
       pcs.currency_code currency,
       pcs.circuit_principal,
       pcs.status,
       pcs.effective_date,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.max_pcs,
                                                I_item        => pcs.item,
                                                I_location    => pcs.zone_id,
                                                I_loc_type    => 1,
                                                I_bu          => pcs.bu)) as Decimal (20,2)) msv_ca_zone_amount,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.max_pcs,
                                                I_item        => pcs.item,
                                                I_location    => pcs.zone_id,
                                                I_loc_type    => 1,
                                                I_bu          => pcs.bu,
                                                I_margin_type => '%')) as Decimal (20,4)) msv_ca_zone_percent,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.max_pcs,
                                                I_item        => pcs.item,
                                                I_location    => pcs.zone_id,
                                                I_loc_type    => 1,
                                                I_bu          => pcs.bu,
                                                I_margin_type => '%')) as Decimal (20,4))*100 msv_ca_zone_percent_display,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs      => pcs.max_pcs,
                                                I_item     => pcs.item,
                                                I_bu       => pcs.bu)) as Decimal (20,2)) msv_ca_amount,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.max_pcs,
                                                I_item        => pcs.item,
                                                I_bu          => pcs.bu,
                                                I_margin_type => '%')) as Decimal (20,4)) msv_ca_percent,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.max_pcs,
                                                I_item        => pcs.item,
                                                I_bu          => pcs.bu,
                                                I_margin_type => '%')) as Decimal (20,4))*100 msv_ca_percent_display,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs      => pcs.pcs,
                                                I_item     => pcs.item,
                                                I_bu       => pcs.bu)) as Decimal (20,2)) msv_ca_supp_amount,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.pcs,
                                                I_item        => pcs.item,
                                                I_bu          => pcs.bu,
                                                I_margin_type => '%')) as Decimal (20,4)) msv_ca_supp_percent,
       cast((xxadeo_pcs_utils_sql.return_margin(I_pcs         => pcs.pcs,
                                                I_item        => pcs.item,
                                                I_bu          => pcs.bu,
                                                I_margin_type => '%')) as Decimal (20,4))*100 msv_ca_supp_percent_display
  from (select /*+ LEADING(pcs,area,rbpz)  index(pcs PK_XXADEO_AREA_PCS)*/
               pcs.item,
               pcs.supplier,
               pcs.bu,
               pcs.pcs,
               (select max(pcs_max.pcs) pcs
                  from (select pcs,
                               ROW_NUMBER() over (partition by item, supplier, bu order by effective_date asc) rnk
                          from xxadeo_area_pcs pcs_max
                         where pcs_max.item           = pcs.item
                           and pcs_max.bu             = pcs.bu
                           and pcs_max.status = 'A') pcs_max
                 where rnk = 1) max_pcs,
               area.currency_code,
               pcs.circuit_principal,
               pcs.effective_date,
               pcs.status,
               rbpz.zone_id
          from xxadeo_area_pcs pcs,
               area,
               (select zone_id, bu_id from xxadeo_rpm_bu_price_zones xrbpz where xrbpz.bu_zone_type = 'NATZ') rbpz
         where area.area          = pcs.bu
           and pcs.bu             = rbpz.bu_id
           and pcs.status         = 'A') pcs;
comment on column XXADEO_V_BU_PCS.ITEM is 'Item Identifier.';
comment on column XXADEO_V_BU_PCS.SUPPLIER is 'Supplier Site Identifier.';
comment on column XXADEO_V_BU_PCS.ZONE_ID is 'Zone Identifier taken XXADEO_RPM_BU_PRICE_ZONES table for National Zone of the BU.';
comment on column XXADEO_V_BU_PCS.BU is 'BU Identifier.';
comment on column XXADEO_V_BU_PCS.SUPPLIER_PCS_CA is 'PCS CA Supplier Amount, taxes exclusive (VAT exclusive).';
comment on column XXADEO_V_BU_PCS.PCS_CA is 'PCS CA Amount, MAX(PCS) of all PCS CA Fourn., taxes exclusive (VAT exclusive).';
comment on column XXADEO_V_BU_PCS.CURRENCY is 'AREA (BU) currency.';
comment on column XXADEO_V_BU_PCS.CIRCUIT_PRINCIPAL is 'Circuit Principal identifier Note: Valid values are (''1'', ''7'').';
comment on column XXADEO_V_BU_PCS.STATUS is 'The Status of the record. Valid values: A - Active record; I - Inactive Record and ready to be purged. For this view, only records where status = ''A''';
comment on column XXADEO_V_BU_PCS.EFFECTIVE_DATE is 'PCS Effective Date, used to identify actual PCS CA.';
comment on column XXADEO_V_BU_PCS.MSV_CA_ZONE_AMOUNT is 'Item/Zone Margin amount.';
comment on column XXADEO_V_BU_PCS.MSV_CA_ZONE_PERCENT is 'Item/Zone Margin percentage.';
comment on column XXADEO_V_BU_PCS.MSV_CA_ZONE_PERCENT_DISPLAY is 'Item/Zone Margin percentage in percentage format.';
comment on column XXADEO_V_BU_PCS.MSV_CA_AMOUNT is 'Item/BU Margin amount.';
comment on column XXADEO_V_BU_PCS.MSV_CA_PERCENT is 'Item/BU Margin percentage.';
comment on column XXADEO_V_BU_PCS.MSV_CA_PERCENT_DISPLAY is 'Item/BU Margin percentage in percentage format.';
comment on column XXADEO_V_BU_PCS.MSV_CA_SUPP_AMOUNT is 'Item/Supplier/BU Margin amount.';
comment on column XXADEO_V_BU_PCS.MSV_CA_SUPP_PERCENT is 'Item/Supplier/BU Margin percentage.';
comment on column XXADEO_V_BU_PCS.MSV_CA_SUPP_PERCENT_DISPLAY is 'Item/Supplier/BU Margin percentage in percentage format.';


