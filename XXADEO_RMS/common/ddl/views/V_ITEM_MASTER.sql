--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - November 2018                                                */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML drop view XXADEO_RMS.V_ITEM_MASTER                       */
/******************************************************************************/
--------------------------------------------------------------------------------
-- # DROP XXADEO_RMS.V_ITEM_MASTER # --
begin
  execute immediate 'drop view XXADEO_RMS.V_ITEM_MASTER';
exception
  when others then
    null;
end;
/