----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--View Updated:  XXADEO_V_SUPPLIER_PARENT_BU
----------------------------------------------------------------------------
--
whenever sqlerror exit  
--
--------------------------------------
--       UPDATING VIEW               
--------------------------------------
PROMPT CREATING VIEW 'XXADEO_V_SUPPLIER_PARENT_BU';
  CREATE OR REPLACE VIEW XXADEO_V_SUPPLIER_PARENT_BU (SUPPLIER_PARENT, LISTE_BU) AS 
  select tab.supplier_parent,
       listagg(tab.bu3,', ') WITHIN GROUP (order by tab.bu3) as "bu3"
  from (with tab_all_site as (select supplier,
                                     supplier_parent
                                from sups
                               where supplier_parent in (select supplier_parent
                                                           from sups
                                                        )
                             )
        select distinct ats.supplier_parent       supplier_parent,
                        ar.area_name              bu3
          from partner_org_unit pou,
               org_unit ou,
               xxadeo_bu_ou xbo,
               area ar,
               tab_all_site ats
         where pou.partner = ats.supplier  -- BU du fournisseur
           and pou.org_unit_id = ou.org_unit_id
           and ou.org_unit_id = xbo.ou
           and xbo.bu = ar.area
       ) tab
group by supplier_parent
/