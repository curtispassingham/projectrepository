/******************************************************************************/
/* CREATE DATE - January 2019                                                 */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_DASHBOARD_DERUNI_SUPS" for RB148              */
/******************************************************************************/

create or replace view xxadeo_v_dashboard_deruni_sups as
WITH
udas as
 (select parameter, bu, to_number(value_1) value_1
    from xxadeo_mom_dvm dvm, uda uda
   where uda.uda_id = dvm.value_1
     and func_area = 'UDA')
select ITEM,
       ITEM_DESC,
       sous_typo_step,
       standard_uom,
       SUPPLIER,
       SUP_NAME,
       SUPP_IND,
       MANUFACTURER_CODE,
       MANUF_DESC,
       SUPP_SITE_COUNTRY,
       SUPP_COUNTRY,
       STATUT_LIEN_ARTICLE,
       GTIN,
       PCB,
       PA_HT,
       CURRENCY_CODE,
       MOTIF_BLOCAGE_ACHAT,
       STATUT_CONFORM_QUALITE,
       DATE_ARRET_LIEN,
       ORIGIN_COUNTRY_ID,
       BU
  from (select bu,
               isup.item,
               im.item_desc,
               (select udaval.uda_value_desc
          from (select item, uda_value, uda_id
                  from uda_item_lov uiv, udas
                 where uiv.uda_id = value_1
                   and udas.parameter = 'ITEM_SUBTYPE'
                   and item = isup.item
                   and (udas.bu = BU or udas.bu is null or udas.bu = -1)) uil1,
               v_uda_values_tl udaval
         where udaval.uda_id = uil1.uda_id
           and udaval.uda_value = uil1.uda_value) as sous_typo_step,
               im.standard_uom,
               isup.supplier,
               (select itsup.primary_supp_ind from item_supplier itsup where itsup.item = isup.item and itsup.supplier = isup.supplier) as supp_ind,
               s.sup_name,
               isc.supp_hier_lvl_1 as manufacturer_code,
               (select p.partner_desc
                  from v_partner p
                 where p.partner_id = isc.supp_hier_lvl_1) as manuf_desc,
               isc.origin_country_id,
               (select country_id from addr where key_value_1 = to_char(isup.supplier) and addr_type = '04') as supp_site_country,
               (select country_id from addr where key_value_1 = to_char(s.SUPPLIER_PARENT) and addr_type = '04') as supp_country,
               (select code_desc
                  from (select cfa.attrib_id ATTR_ID, cd.code_desc
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_supplier_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item     = isup.item
                                   and supplier = isup.supplier) cfa_unc,
                               v_code_detail cd
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)
                           and cd.code_type = cfa.code_type
                           and cd.code = cfa_value) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and x.func_area = 'CFA'
                   and x.parameter = 'LINK_STATUS_ITEM_SUPP'
                   and (x.bu = xbo.bu or x.bu is null or x.bu = -1)) as statut_lien_article,
               (select im.item
                  from v_item_master im
                 where nvl(im.item_grandparent, im.item_parent) = isup.item
                   and im.item_level > im.tran_level
                   and im.create_datetime =
                       (select max(im2.create_datetime)
                          from v_item_master im2
                         where nvl(im2.item_grandparent, im2.item_parent) =
                               isup.item)
                   and rownum = 1) as gtin,
               isc.supp_pack_size as pcb,
               isc.unit_cost as pa_ht,
               s.currency_code,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_supplier_cfa_ext unpivot(cfa_value for cfa_attribute in(date_21,
                                                                                                  date_22,
                                                                                                  date_23,
                                                                                                  date_24,
                                                                                                  date_25))
                                 where item     = isup.item
                                   and supplier = isup.supplier) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'FRST_AUT_PO_DT_ITEM_SUPP'
                   and (x.bu = xbo.bu or x.bu is null or x.bu = -1)) as date_prem_bloc_qualite,
               (select code_desc
                  from (select cfa.attrib_id ATTR_ID, cd.code_desc
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_supplier_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item     = isup.item
                                   and supplier = isup.supplier) cfa_unc,
                               v_code_detail cd
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)
                           and cd.code_type = cfa.code_type
                           and cd.code = cfa_value) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'BLOCKING_PO_ITEM_SUPP'
                   and (x.bu = xbo.bu or x.bu is null or x.bu = -1)) as motif_blocage_achat,
               (select code_desc
                  from (select cfa.attrib_id ATTR_ID, cd.code_desc
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_supplier_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                  varchar2_2,
                                                                                                  varchar2_3,
                                                                                                  varchar2_4,
                                                                                                  varchar2_5,
                                                                                                  varchar2_6,
                                                                                                  varchar2_7,
                                                                                                  varchar2_8,
                                                                                                  varchar2_9,
                                                                                                  varchar2_10))
                                 where item     = isup.item
                                   and supplier = isup.supplier) cfa_unc,
                               v_code_detail cd
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)
                           and cd.code_type = cfa.code_type
                           and cd.code = cfa_value) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'STATUS_QC_COMP_ITEM_SUPP'
                   and (x.bu = xbo.bu or x.bu is null or x.bu = -1)) as statut_conform_qualite,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_supplier_cfa_ext unpivot(cfa_value for cfa_attribute in(date_21,
                                                                                                  date_22,
                                                                                                  date_23,
                                                                                                  date_24,
                                                                                                  date_25))
                                 where item     = isup.item
                                   and supplier = isup.supplier) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'LINK_TERM_DT_ITEM_SUPP'
                   and (x.bu = xbo.bu or x.bu is null or x.bu = -1)) as date_arret_lien
          from xxadeo_v_sups       s,
               v_item_supplier_tl  isup,
               v_item_supp_country isc,
               partner_org_unit    pou,
               xxadeo_bu_ou        xbo,
               item_master im
         where s.supplier = isup.supplier
           and im.item = isup.item
           and (isc.supplier(+) = isup.supplier and isc.item(+) = isup.item)
           and pou.partner = s.SUPPLIER
           and pou.org_unit_id = xbo.ou
           and im.ORDERABLE_IND = 'Y');
