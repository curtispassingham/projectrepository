/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_V_ITEM_LIST"                                    */
/******************************************************************************/


create or replace view XXADEO_V_ITEM_LIST as
select SKULIST,
       SKULIST_DESC,
       CREATE_DATE,
       CREATE_ID,
       STATIC_IND,
       LAST_REBUILD_DATE,
       USER_SECURITY_IND,
       COMMENT_DESC,
       FILTER_ORG_ID from v_skulist_head
       where user_security_ind = 'N'
union
select SKULIST,
       SKULIST_DESC,
       CREATE_DATE,
       CREATE_ID,
       STATIC_IND,
       LAST_REBUILD_DATE,
       USER_SECURITY_IND,
       COMMENT_DESC,
       FILTER_ORG_ID from v_skulist_head
 where user_security_ind = 'Y'
   and upper(create_id) = upper(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'));  