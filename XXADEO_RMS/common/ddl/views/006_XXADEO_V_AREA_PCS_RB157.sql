/**********************************************************************************/
/* CREATE DATE - October 2018                                                     */
/* CREATE USER - Elsa Barros                                                      */
/* PROJECT     - ADEO                                                             */
/* DESCRIPTION - MATERIALIZED VIEW "XXADEO_V_AREA_PCS" for RR157                  */
/**********************************************************************************/
create or replace view xxadeo_v_area_pcs as
select mvpcs.item,
       mvpcs.supplier,
       mvpcs.zone_id,
       mvpcs.bu,
       mvpcs.supplier_pcs_ca,
       mvpcs.pcs_ca,
       mvpcs.currency,
       mvpcs.circuit_principal,
       mvpcs.status,
       mvpcs.effective_date,
       mvpcs.msv_ca_zone_amount,
       mvpcs.msv_ca_zone_percent,
       mvpcs.msv_ca_zone_percent_display,
       mvpcs.msv_ca_amount,
       mvpcs.msv_ca_percent,
       mvpcs.msv_ca_percent_display,
       mvpcs.msv_ca_supp_amount,
       mvpcs.msv_ca_supp_percent,
       mvpcs.msv_ca_supp_percent_display
  from xxadeo_mv_area_pcs     mvpcs,
       v_item_master          vim,
       v_sups                 vsups,
       v_area                 varea
 where mvpcs.item     = vim.item
   and mvpcs.supplier = vsups.supplier
   and mvpcs.bu       = varea.area;
comment on column XXADEO_V_AREA_PCS.ITEM is 'Item Identifier.';
comment on column XXADEO_V_AREA_PCS.SUPPLIER is 'Supplier Site Identifier.';
comment on column XXADEO_V_AREA_PCS.ZONE_ID is 'Zone Identifier taken XXADEO_RPM_BU_PRICE_ZONES table for National Zone of the BU.';
comment on column XXADEO_V_AREA_PCS.BU is 'BU Identifier.';
comment on column XXADEO_V_AREA_PCS.SUPPLIER_PCS_CA is 'PCS CA Supplier Amount, taxes exclusive (VAT exclusive).';
comment on column XXADEO_V_AREA_PCS.PCS_CA is 'PCS CA Amount, MAX(PCS) of all PCS CA Fourn., taxes exclusive (VAT exclusive).';
comment on column XXADEO_V_AREA_PCS.CURRENCY is 'AREA (BU) currency.';
comment on column XXADEO_V_AREA_PCS.CIRCUIT_PRINCIPAL is 'Circuit Principal identifier Note: Valid values are (''1'', ''7'').';
comment on column XXADEO_V_AREA_PCS.STATUS is 'The Status of the record. Valid values: A - Active record; I - Inactive Record and ready to be purged. For this view, only records where status = ''A''';
comment on column XXADEO_V_AREA_PCS.EFFECTIVE_DATE is 'PCS Effective Date, used to identify actual PCS CA.';
comment on column XXADEO_V_AREA_PCS.MSV_CA_ZONE_AMOUNT is 'Item/Zone Margin amount.';
comment on column XXADEO_V_AREA_PCS.MSV_CA_ZONE_PERCENT is 'Item/Zone Margin percentage.';
comment on column XXADEO_V_AREA_PCS.MSV_CA_ZONE_PERCENT_DISPLAY is 'Item/Zone Margin percentage in percentage format.';
comment on column XXADEO_V_AREA_PCS.MSV_CA_AMOUNT is 'Item/BU Margin amount.';
comment on column XXADEO_V_AREA_PCS.MSV_CA_PERCENT is 'Item/BU Margin percentage.';
comment on column XXADEO_V_AREA_PCS.MSV_CA_PERCENT_DISPLAY is 'Item/BU Margin percentage in percentage format.';
comment on column XXADEO_V_AREA_PCS.MSV_CA_SUPP_AMOUNT is 'Item/Supplier/BU Margin amount.';
comment on column XXADEO_V_AREA_PCS.MSV_CA_SUPP_PERCENT is 'Item/Supplier/BU Margin percentage.';
comment on column XXADEO_V_AREA_PCS.MSV_CA_SUPP_PERCENT_DISPLAY is 'Item/Supplier/BU Margin percentage in percentage format.';   