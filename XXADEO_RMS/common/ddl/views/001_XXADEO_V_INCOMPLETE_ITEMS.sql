/******************************************************************************/
/* CREATE DATE - November 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_INCOMPLETE_ITEMS" for RR143/144               */
/******************************************************************************/
begin
  execute immediate 'drop view xxadeo_v_incomplete_items';
exception
  when others then
    null;
end;
/

create or replace view xxadeo_v_incomplete_items as
select bu,
       dept_id,
       (select group_name from groups where group_no = dept_id) as dept_name,
       subdept_id,
       (select dept_name from deps where dept = subdept_id) as subdept_name,
       type_id,
       (select class_name from class where class = type_id and dept = subdept_id) as type_name,
       subtype_id,
       (select sub_name from subclass where subclass = subtype_id and class = type_id and dept = subdept_id) as subtype_name,
       item,
       item_desc,
       supp_ind,
       classment_bu,
       subtypology,
       lfcy_entity,
       lfcy_status,
       lfcy_entity_type,
       active_commerce_date,
       cond_flag,
       completedpercent,
       cond_ib_asi_01,
       cond_ib_asi_02,
       cond_ib_asi_03,
       cond_ib_asi_04,
       cond_ib_asi_05,
       cond_ib_acom_01,
       cond_ib_acom_02,
       cond_ib_acom_03,
       cond_ib_acom_04,
       cond_is_act_01,
       cond_is_act_02,
       cond_is_act_03,
       cond_is_act_04,
       cond_is_act_05,
       cond_is_act_06,
       cond_is_act_07
  from (
       select w.LFCY_ENTITY as bu,
              w.LFCY_ENTITY,
              w.LFCY_STATUS,
              w.LFCY_ENTITY_TYPE,
              im.group_no as dept_id,
              im.dept as subdept_id,
              im.CLASS as type_id,
              im.SUBCLASS as subtype_id,
              (select cfa_value
                 from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                         from cfa_attrib cfa,
                              (select group_id, cfa_attribute, cfa_value
                                 from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(number_11,
                                                                                                 number_12,
                                                                                                 number_13,
                                                                                                 number_14,
                                                                                                 number_15,
                                                                                                 number_16,
                                                                                                 number_17,
                                                                                                 number_18,
                                                                                                 number_19,
                                                                                                 number_20))
                                where item = w.ITEM) cfa_unc
                        where cfa.group_id = cfa_unc.group_id
                          and upper(cfa.storage_col_name) =
                              upper(cfa_unc.cfa_attribute)) aux,
                      xxadeo_mom_dvm x
                where (x.value_1) = aux.attr_id
                  and func_area = 'CFA'
                  and parameter = 'RANKING_ITEM'
                  and (bu = w.lfcy_entity or
                      bu is null or bu = -1)) as classment_bu,
              (select uda_val.uda_value_desc
                 from uda_item_lov uil, v_uda_values_tl uda_val
                where uda_val.uda_id = uil.uda_id
                  and uda_val.uda_value = uil.uda_value
                  and uda_val.lang =
                      nvl(language_sql.get_user_language(), 1)
                  and uil.uda_id =
                      nvl((select value_1
                            from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                          I_parameter => 'ITEM_SUBTYPE'))
                           where bu is null
                              or bu = -1),
                          (select value_1
                             from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                           I_parameter => 'ITEM_SUBTYPE',
                                                           I_bu        => w.lfcy_entity))))
                  and uil.item = w.item) as subtypology,
              w.ITEM,
              im.item_desc,
              null as supp_ind,
              (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(date_21,
                                                                                                  date_22,
                                                                                                  date_23,
                                                                                                  date_24,
                                                                                                  date_25))
                                 where item = w.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'ACTIF_DDATE_ITEM'
                   and (bu = w.lfcy_entity or bu is null or bu = -1)) as active_commerce_date,
              'IB-ASI' as cond_flag,
              (round(sum(case WHEN c.VAL_STATUS = 'Y' then 1 else 0 end) / count(c.LFCY_COND_ID),2)*100) as completedPercent,
              max(case when c.LFCY_COND_ID = 'IB-ASI-01' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IB-ASI-01' then 0 else -1 end end ) as cond_IB_ASI_01,
              max(case when c.LFCY_COND_ID = 'IB-ASI-02' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IB-ASI-02' then 0 else -1 end end ) as cond_IB_ASI_02,
              max(case when c.LFCY_COND_ID = 'IB-ASI-03' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IB-ASI-03' then 0 else -1 end end ) as cond_IB_ASI_03,
              max(case when c.LFCY_COND_ID = 'IB-ASI-04' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IB-ASI-04' then 0 else -1 end end ) as cond_IB_ASI_04,
              max(case when c.LFCY_COND_ID = 'IB-ASI-05' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IB-ASI-05' then 0 else -1 end end ) as cond_IB_ASI_05,
              null as cond_IB_ACOM_01,
              null as cond_IB_ACOM_02,
              null as cond_IB_ACOM_03,
              null as cond_IB_ACOM_04,
              null as cond_IS_ACT_01,
              null as cond_IS_ACT_02,
              null as cond_IS_ACT_03,
              null as cond_IS_ACT_04,
              null as cond_IS_ACT_05,
              null as cond_IS_ACT_06,
              null as cond_IS_ACT_07
         from xxadeo_lfcy_work      w,
              xxadeo_lfcy_item_cond c,
              xxadeo_system_rules   xsr,
              v_item_master         im
        where w.item = c.Item(+)
          and w.item = im.ITEM
          and w.lfcy_entity = c.lfcy_entity(+)
          and c.LFCY_COND_ID = xsr.rule_id
          and xsr.active_ind = 'Y'
          and w.lfcy_status = 'IB-INIT'
          and c.LFCY_COND_ID in ('IB-ASI-01', 'IB-ASI-02', 'IB-ASI-03', 'IB-ASI-04', 'IB-ASI-05')
          and im.ITEM_LEVEL = 1
        group by w.ITEM,
                 im.item_desc,
                 w.LFCY_ENTITY,
                 w.LFCY_STATUS,
                 w.LFCY_ENTITY_TYPE,
                 im.group_no,
                 im.dept,
                 im.CLASS,
                 im.SUBCLASS
union all
select w.LFCY_ENTITY as bu,
              w.LFCY_ENTITY,
              w.LFCY_STATUS,
              w.LFCY_ENTITY_TYPE,
              im.group_no as dept_id,
              im.dept as subdept_id,
              im.CLASS as type_id,
              im.SUBCLASS as subtype_id,
              (select cfa_value
                 from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                         from cfa_attrib cfa,
                              (select group_id, cfa_attribute, cfa_value
                                 from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(number_11,
                                                                                                 number_12,
                                                                                                 number_13,
                                                                                                 number_14,
                                                                                                 number_15,
                                                                                                 number_16,
                                                                                                 number_17,
                                                                                                 number_18,
                                                                                                 number_19,
                                                                                                 number_20))
                                where item = w.ITEM) cfa_unc
                        where cfa.group_id = cfa_unc.group_id
                          and upper(cfa.storage_col_name) =
                              upper(cfa_unc.cfa_attribute)) aux,
                      xxadeo_mom_dvm x
                where (x.value_1) = aux.attr_id
                  and func_area = 'CFA'
                  and parameter = 'RANKING_ITEM'
                  and (bu = w.lfcy_entity or
                      bu is null or bu = -1)) as classment_bu,
              (select uda_val.uda_value_desc
                 from uda_item_lov uil, v_uda_values_tl uda_val
                where uda_val.uda_id = uil.uda_id
                  and uda_val.uda_value = uil.uda_value
                  and uda_val.lang =
                      nvl(language_sql.get_user_language(), 1)
                  and uil.uda_id =
                      nvl((select value_1
                            from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                          I_parameter => 'ITEM_SUBTYPE'))
                           where bu is null
                              or bu = -1),
                          (select value_1
                             from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                           I_parameter => 'ITEM_SUBTYPE',
                                                           I_bu        => w.lfcy_entity))))
                  and uil.item = w.item) as subtypology,
              w.ITEM,
              im.item_desc,
              null as supp_ind,
              (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(date_21,
                                                                                                  date_22,
                                                                                                  date_23,
                                                                                                  date_24,
                                                                                                  date_25))
                                 where item = w.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'ACTIF_DDATE_ITEM'
                   and (bu = w.lfcy_entity or bu is null or bu = -1)) as active_commerce_date,
              'IB-ACOM' as cond_flag,
              (round(sum(case WHEN c.VAL_STATUS = 'Y' then 1 else 0 end) / count(c.LFCY_COND_ID),2)*100) as completedPercent,
              null as cond_IB_ASI_01,
              null as cond_IB_ASI_02,
              null as cond_IB_ASI_03,
              null as cond_IB_ASI_04,
              null as cond_IB_ASI_05,
              max(case when c.LFCY_COND_ID = 'IB-ACOM-01' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IB-ACOM-01' then 0 else -1 end end ) as cond_IB_ACOM_01,
              max(case when c.LFCY_COND_ID = 'IB-ACOM-02' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IB-ACOM-02' then 0 else -1 end end ) as cond_IB_ACOM_02,
              max(case when c.LFCY_COND_ID = 'IB-ACOM-03' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IB-ACOM-03' then 0 else -1 end end ) as cond_IB_ACOM_03,
              max(case when c.LFCY_COND_ID = 'IB-ACOM-04' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IB-ACOM-04' then 0 else -1 end end ) as cond_IB_ACOM_04,
              null as cond_IS_ACT_01,
              null as cond_IS_ACT_02,
              null as cond_IS_ACT_03,
              null as cond_IS_ACT_04,
              null as cond_IS_ACT_05,
              null as cond_IS_ACT_06,
              null as cond_IS_ACT_07
         from xxadeo_lfcy_work      w,
              xxadeo_lfcy_item_cond c,
              xxadeo_system_rules   xsr,
              v_item_master         im
        where w.item = c.Item(+)
          and w.item = im.ITEM
          and w.lfcy_entity = c.lfcy_entity(+)
          and c.LFCY_COND_ID = xsr.rule_id
          and xsr.active_ind = 'Y'
          and w.lfcy_status = 'IB-ASI'
          and c.LFCY_COND_ID in ('IB-ACOM-01','IB-ACOM-02','IB-ACOM-03','IB-ACOM-04')
          and im.ITEM_LEVEL = 1
        group by w.ITEM,
                 im.item_desc,
                 w.LFCY_ENTITY,
                 w.LFCY_STATUS,
                 w.LFCY_ENTITY_TYPE,
                 im.group_no,
                 im.dept,
                 im.CLASS,
                 im.SUBCLASS
union all
select        xbo.bu as bu,
              w.LFCY_ENTITY,
              w.LFCY_STATUS,
              w.LFCY_ENTITY_TYPE,
              im.group_no as dept_id,
              im.dept as subdept_id,
              im.CLASS as type_id,
              im.SUBCLASS as subtype_id,
              (select cfa_value
                 from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                         from cfa_attrib cfa,
                              (select group_id, cfa_attribute, cfa_value
                                 from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(number_11,
                                                                                                 number_12,
                                                                                                 number_13,
                                                                                                 number_14,
                                                                                                 number_15,
                                                                                                 number_16,
                                                                                                 number_17,
                                                                                                 number_18,
                                                                                                 number_19,
                                                                                                 number_20))
                                where item = w.ITEM) cfa_unc
                        where cfa.group_id = cfa_unc.group_id
                          and upper(cfa.storage_col_name) =
                              upper(cfa_unc.cfa_attribute)) aux,
                      xxadeo_mom_dvm x
                where (x.value_1) = aux.attr_id
                  and func_area = 'CFA'
                  and parameter = 'RANKING_ITEM'
                  and (bu = xbo.bu or
                      bu is null or bu = -1)) as classment_bu,
              (select uda_val.uda_value_desc
                 from uda_item_lov uil, v_uda_values_tl uda_val
                where uda_val.uda_id = uil.uda_id
                  and uda_val.uda_value = uil.uda_value
                  and uda_val.lang =
                      nvl(language_sql.get_user_language(), 1)
                  and uil.uda_id =
                      nvl((select value_1
                            from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                          I_parameter => 'ITEM_SUBTYPE'))
                           where bu is null
                              or bu = -1),
                          (select value_1
                             from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                           I_parameter => 'ITEM_SUBTYPE',
                                                           I_bu        => xbo.bu))))
                  and uil.item = w.item) as subtypology,
              w.ITEM,
              im.item_desc,
              (select isup.primary_supp_ind from item_supplier isup where isup.item = w.item and isup.supplier = w.lfcy_entity) as supp_ind,
              (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(date_21,
                                                                                                  date_22,
                                                                                                  date_23,
                                                                                                  date_24,
                                                                                                  date_25))
                                 where item = w.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'ACTIF_DDATE_ITEM'
                   and (bu = xbo.bu or bu is null or bu = -1)) as active_commerce_date,
              'IS-ACT' as cond_flag,
              (round(sum(case WHEN c.VAL_STATUS = 'Y' then 1 else 0 end) / count(c.LFCY_COND_ID),2)*100) as completedPercent,
              null as cond_IB_ASI_01,
              null as cond_IB_ASI_02,
              null as cond_IB_ASI_03,
              null as cond_IB_ASI_04,
              null as cond_IB_ASI_05,
              null as cond_IB_ACOM_01,
              null as cond_IB_ACOM_02,
              null as cond_IB_ACOM_03,
              null as cond_IB_ACOM_04,
              max(case when c.LFCY_COND_ID = 'IS-ACT-01' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IS-ACT-01' then 0 else -1 end end ) as cond_IS_ACT_01,
              max(case when c.LFCY_COND_ID = 'IS-ACT-02' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IS-ACT-02' then 0 else -1 end end ) as cond_IS_ACT_02,
              max(case when c.LFCY_COND_ID = 'IS-ACT-03' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IS-ACT-03' then 0 else -1 end end ) as cond_IS_ACT_03,
              max(case when c.LFCY_COND_ID = 'IS-ACT-04' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IS-ACT-04' then 0 else -1 end end ) as cond_IS_ACT_04,
              max(case when c.LFCY_COND_ID = 'IS-ACT-05' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IS-ACT-05' then 0 else -1 end end ) as cond_IS_ACT_05,
              max(case when c.LFCY_COND_ID = 'IS-ACT-06' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IS-ACT-06' then 0 else -1 end end ) as cond_IS_ACT_06,
              max(case when c.LFCY_COND_ID = 'IS-ACT-07' and c.VAL_STATUS = 'Y' then 1 else case when c.LFCY_COND_ID = 'IS-ACT-07' then 0 else -1 end end ) as cond_IS_ACT_07
         from xxadeo_lfcy_work      w,
              xxadeo_lfcy_item_cond c,
              xxadeo_system_rules   xsr,
              v_item_master         im,
              partner_org_unit      pou,
              xxadeo_bu_ou          xbo,
              filter_group_org      fgo
        where w.item = c.Item(+)
          and w.item = im.ITEM
          and w.lfcy_entity = c.lfcy_entity(+)
          and c.LFCY_COND_ID = xsr.rule_id
          and pou.partner = w.lfcy_entity
          and pou.org_unit_id = xbo.ou
          and pou.org_unit_id = fgo.filter_org_id
          and fgo.filter_org_level = 'O'
          and xsr.active_ind = 'Y'
          and w.lfcy_status = 'IS-INIT'
          and c.LFCY_COND_ID in ('IS-ACT-01', 'IS-ACT-02', 'IS-ACT-03', 'IS-ACT-04', 'IS-ACT-05', 'IS-ACT-06', 'IS-ACT-07')
          and im.ITEM_LEVEL = 1
        group by xbo.bu,
                 w.ITEM,
                 im.item_desc,
                 w.LFCY_ENTITY,
                 w.LFCY_STATUS,
                 w.LFCY_ENTITY_TYPE,
                 im.group_no,
                 im.dept,
                 im.CLASS,
                 im.SUBCLASS
                 )
    where completedPercent < 100
      and exists
         (select 1
                  from xxadeo_v_deps dep, xxadeo_v_class cla, xxadeo_v_subclass sub
                 where dep.DEPT = subdept_id
                   and cla.DEPT = dep.dept
                   and cla.CLASS = type_id
                   and sub.DEPT = cla.dept
                   and sub.CLASS = cla.CLASS
                   and sub.SUBCLASS = subtype_id)
order by dept_id,
         subdept_id,
         type_id,
         subtype_id,
         classment_bu,
         item,
         case when cond_flag = 'IS-ACT'then LFCY_ENTITY end
         nulls last
;