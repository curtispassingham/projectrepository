/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_DEAL_HEAD" for RR405                          */
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_V_DEAL_HEAD AS
SELECT DEAL_ID, PARTNER_TYPE, PARTNER_ID, SUPPLIER, TYPE, STATUS, CURRENCY_CODE, ACTIVE_DATE, CLOSE_DATE, CLOSE_ID, CREATE_DATETIME, CREATE_ID, APPROVAL_DATE, APPROVAL_ID, REJECT_DATE, REJECT_ID, EXT_REF_NO, ORDER_NO, RECALC_APPROVED_ORDERS, COMMENTS, LAST_UPDATE_ID, LAST_UPDATE_DATETIME, BILLING_TYPE, BILL_BACK_PERIOD, DEAL_APPL_TIMING, THRESHOLD_LIMIT_TYPE, THRESHOLD_LIMIT_UOM, REBATE_IND, REBATE_CALC_TYPE, GROWTH_REBATE_IND, HISTORICAL_COMP_START_DATE, HISTORICAL_COMP_END_DATE, REBATE_PURCH_SALES_IND, DEAL_REPORTING_LEVEL, BILL_BACK_METHOD, DEAL_INCOME_CALCULATION, INVOICE_PROCESSING_LOGIC, STOCK_LEDGER_IND, INCLUDE_VAT_IND, BILLING_PARTNER_TYPE, BILLING_PARTNER_ID, BILLING_SUPPLIER_ID, GROWTH_RATE_TO_DATE, TURNOVER_TO_DATE, ACTUAL_MONIES_EARNED_TO_DATE, SECURITY_IND, EST_NEXT_INVOICE_DATE, LAST_INVOICE_DATE, TRACK_PACK_LEVEL_IND, BBD_ADD_REP_DAYS, RPM_DEAL_IND
         FROM DEAL_HEAD
         where deal_id in
         (
           (select distinct deal_id
             from deal_itemloc dil, v_store s
            where (
                   ((dil.org_level = 5) and (dil.loc_type = 'S') and
                   (dil.location = s.store))
                   or
                   ((dil.org_level = 4) and (dil.district = s.district))
                   or
                   ((dil.org_level = 3) and (dil.region = s.region)) or
                   ((dil.org_level = 2) and (dil.area = s.area)) or
                   ((dil.org_level = 1) and (dil.chain = s.chain))
                  )
           )
         union
           (select distinct deal_id
             from deal_itemloc dil,
                  v_wh wh
             where (((dil.org_level = 5)
                 and (dil.loc_type = 'W')
                 and (dil.location = wh.wh)))
            )
         );