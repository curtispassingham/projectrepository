/******************************************************************************/
/* CREATE DATE - January 2019                                                 */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_DASHBOARD_PACKITM_CMP" for RB148              */
/******************************************************************************/

create or replace view xxadeo_v_dashboard_packitm_cmp as
with act_items as
 (select im.item, im.dept, im.class, im.subclass
    from item_master       im,
         xxadeo_v_deps     dep,
         xxadeo_v_class    cla,
         xxadeo_v_subclass sub
   where dep.DEPT = im.dept
     and cla.DEPT = dep.dept
     and cla.CLASS = im.CLASS
     and sub.DEPT = cla.dept
     and sub.CLASS = cla.CLASS
     and sub.SUBCLASS = im.SUBCLASS),
item_bs as
 (select item, BU_SUBSCRIPTION, dept, class, subclass, group_no
    from (select actim.ITEM, uv.uda_value as BU_SUBSCRIPTION, actim.dept, actim.class, actim.subclass, im.group_no
            from v_item_master     im,
                 uda_item_lov    uilv,
                 uda_values      uv,
                 act_items       actim
           where im.item = actim.item
             and im.item = uilv.item
             and uilv.uda_id = uv.uda_id
             and uilv.uda_value = uv.uda_value
             and im.item_level = 1
             and uilv.uda_id = (select value_1
                                  from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                                I_parameter => 'BU_SUBSCRIPTION'))
                                 where bu is null
                                    or bu = -1)))
,
udas as
 (select parameter, bu, to_number(value_1) value_1
    from xxadeo_mom_dvm dvm, uda uda
   where uda.uda_id = dvm.value_1
     and func_area = 'UDA')
select FILTER_ORG_ID,
       STATUS,
       PACK_IND,
       PACK_TYPE,
       ITEM_LEVEL,
       TRAN_LEVEL,
       CHECK_UDA_IND,
       GROUP_NO,
       group_name,
       DEPT,
       dept_name,
       CLASS,
       class_name,
       SUBCLASS,
       sub_name,
       ranking_item,
       ITEM,
       ITEM_DESC,
       'PACK_COMP' as item_type,
       agg_item,
       pack_qty,
       sub_typology,
       null as personalized_ind,
       standard_uom,
       derunit_conv_factor,
       qt_capacity,
       contain_uom_item,
       assortment_mode,
       range_size as range_size,
       orderable_ind,
       sellable_ind,
       (case when personalized_ind = 'Y' then std_str_tr_prc_itemprc else null end) as std_str_tr_prc_itemprc,
       std_str_tr_prc_prccntrl,
       national_price_itmprc,
       national_price_prccntrl,
       national_price_check,
       margin_itmprc,
       margin_prccntrl,
       margin_perc,
       retail_prc_blocking,
       lifecycle_status,
       actif_ddate_item,
       FORECAST_IND from (
select ibs.bu_subscription as filter_org_id,
       im.status,
       im.pack_ind,
       im.pack_type,
       im.item_level,
       im.tran_level,
       im.check_uda_ind,
       ibs.group_no,
       (select g.group_name from v_groups g where g.group_no = ibs.group_no) as group_name,
       im.dept,
       (select d.dept_name from deps d where d.group_no = ibs.group_no  and d.dept = ibs.dept) as dept_name,
       im.class,
       (select c.class_name
          from v_class c
         where c.class = im.class
           and c.dept = im.dept) as class_name,
       im.subclass,
       (select sc.sub_name
          from v_subclass sc
         where sc.subclass = im.subclass
           and sc.dept = im.dept
           and sc.class = im.class) as sub_name,
      (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext unpivot(cfa_value for cfa_attribute in(number_11,
                                                                                                  number_12,
                                                                                                  number_13,
                                                                                                  number_14,
                                                                                                  number_15,
                                                                                                  number_16,
                                                                                                  number_17,
                                                                                                  number_18,
                                                                                                  number_19,
                                                                                                  number_20))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'RANKING_ITEM'
                   and (bu = ibs.BU_SUBSCRIPTION or bu is null or bu = -1)) as ranking_item,
       im.item,
       im.item_desc,
       pkitm.pack_no as agg_item,
       pkitm.pack_qty,
       (select udaval.uda_value_desc
                  from (select item, uda_value, uda_id
                          from uda_item_lov uiv, udas
                         where uiv.uda_id = value_1
                           and udas.parameter = 'ITEM_SUBTYPE'
                           and item = im.item
                           and (udas.bu = ibs.bu_subscription or
                               udas.bu is null or udas.bu = -1)) uil1,
                       v_uda_values_tl udaval
                 where udaval.uda_id = uil1.uda_id
                   and udaval.uda_value = uil1.uda_value) as sub_typology,
               (select case
                         when uda_value = '10' then
                          'Y'
                         else
                          'N'
                       end
                  from uda_item_lov uiv, udas
                 where uiv.uda_id = value_1
                   and udas.parameter = 'ITEM_SUBTYPE'
                   and item = im.item
                   and (udas.bu = ibs.bu_subscription or udas.bu is null or
                       udas.bu = -1)) as personalized_ind,
               (select udaval.uda_value_desc
                  from (select item, uda_value, uda_id
                          from uda_item_lov uiv, udas
                         where uiv.uda_id = value_1
                           and udas.parameter = 'ASSORTMENT_MODE'
                           and item = im.item
                           and (udas.bu = ibs.bu_subscription or
                               udas.bu is null or udas.bu = -1)) uil1,
                       v_uda_values_tl udaval
                 where udaval.uda_id = uil1.uda_id
                   and udaval.uda_value = uil1.uda_value) as assortment_mode,
               (select udaval.uda_value_desc
                  from (select item, uda_value, uda_id
                          from uda_item_lov uiv, udas
                         where uiv.uda_id = value_1
                           and udas.parameter = 'RANGE_SIZE'
                           and item = im.item
                           and (udas.bu = ibs.bu_subscription or
                               udas.bu is null or udas.bu = -1)) uil1,
                       v_uda_values_tl udaval
                 where udaval.uda_id = uil1.uda_id
                   and udaval.uda_value = uil1.uda_value) as range_size,
               (select udaval.uda_value_desc
                  from (select item, uda_value, uda_id
                          from uda_item_lov uiv, udas
                         where uiv.uda_id = value_1
                           and udas.parameter = 'LIFECYCLE_STATUS'
                           and item = im.item
                           and (udas.bu = ibs.bu_subscription or
                               udas.bu is null or udas.bu = -1)) uil1,
                       v_uda_values_tl udaval
                 where udaval.uda_id = uil1.uda_id
                   and udaval.uda_value = uil1.uda_value) as lifecycle_status,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext ext unpivot(cfa_value for cfa_attribute in(number_11,
                                                                                                      number_12,
                                                                                                      number_13,
                                                                                                      number_14,
                                                                                                      number_15,
                                                                                                      number_16,
                                                                                                      number_17,
                                                                                                      number_18,
                                                                                                      number_19,
                                                                                                      number_20))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and x.func_area = 'CFA'
                   and x.parameter = 'QUANTITY_CAPACITY'
                   and (x.bu = ibs.BU_SUBSCRIPTION or x.bu is null or
                        x.bu = -1)) as qt_capacity,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                      varchar2_2,
                                                                                                      varchar2_3,
                                                                                                      varchar2_4,
                                                                                                      varchar2_5,
                                                                                                      varchar2_6,
                                                                                                      varchar2_7,
                                                                                                      varchar2_8,
                                                                                                      varchar2_9,
                                                                                                      varchar2_10))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and x.func_area = 'CFA'
                   and x.parameter = 'CONTAIN_UOM_ITEM'
                   and (x.bu = ibs.BU_SUBSCRIPTION or x.bu is null or
                        x.bu = -1)) as contain_uom_item,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                      varchar2_2,
                                                                                                      varchar2_3,
                                                                                                      varchar2_4,
                                                                                                      varchar2_5,
                                                                                                      varchar2_6,
                                                                                                      varchar2_7,
                                                                                                      varchar2_8,
                                                                                                      varchar2_9,
                                                                                                      varchar2_10))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and x.func_area = 'CFA'
                   and x.parameter = 'RETAIL_PRICE_BLOCKING'
                   and (x.bu = ibs.BU_SUBSCRIPTION or x.bu is null or
                        x.bu = -1)) as retail_prc_blocking,
               (select cfa_value
                  from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                          from cfa_attrib cfa,
                               (select group_id, cfa_attribute, cfa_value
                                  from item_master_cfa_ext ext unpivot(cfa_value for cfa_attribute in(date_21,
                                                                                                      date_22,
                                                                                                      date_23,
                                                                                                      date_24,
                                                                                                      date_25))
                                 where item = im.item) cfa_unc
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)) aux,
                       xxadeo_mom_dvm x
                 where to_number(x.value_1) = aux.attr_id
                   and x.func_area = 'CFA'
                   and x.parameter = 'ACTIF_DDATE_ITEM'
                   and (x.bu = ibs.BU_SUBSCRIPTION or x.bu is null or x.bu = -1)) as actif_ddate_item,
       null as derunit_conv_factor,
       im.standard_uom,
       im.forecast_ind,
       im.orderable_ind,
       im.sellable_ind,
	   (select pcs.pcs_ca
	      from xxadeo_v_area_pcs pcs
                 where pcs.item = im.item
                   and pcs.bu = ibs.BU_SUBSCRIPTION
                   and nvl(pcs.effective_date,sysdate) = nvl((select max(pcs2.effective_date)
                                                                from xxadeo_v_area_pcs pcs2
                                                               where pcs2.item = pcs.item
                                                                 and pcs2.bu = pcs.bu),sysdate)
                   and rownum = 1) as std_str_tr_prc_itemprc,
       null as std_str_tr_prc_prccntrl,
       (select selling_retail
          from rpm_item_zone_price a
         inner join xxadeo_rpm_bu_price_zones b
            on (a.zone_id = b.zone_id)
         where (bu_zone_type = 'NATZ' and bu_id = ibs.BU_SUBSCRIPTION and
               item = im.item)) as national_price_itmprc,
       null as national_price_prccntrl,
       null as national_price_check,
	   (select pcs.msv_ca_amount
	      from xxadeo_v_area_pcs pcs
                 where pcs.item = im.item
                   and pcs.bu = ibs.BU_SUBSCRIPTION
                   and nvl(pcs.effective_date,sysdate) = nvl((select max(pcs2.effective_date)
                                                                from xxadeo_v_area_pcs pcs2
                                                               where pcs2.item = pcs.item
                                                                 and pcs2.bu = pcs.bu),sysdate)
                   and rownum = 1) as margin_itmprc,
       null as margin_prccntrl,
	   (select pcs.msv_ca_percent
	      from xxadeo_v_area_pcs pcs
                 where pcs.item = im.item
                   and pcs.bu = ibs.BU_SUBSCRIPTION
                   and nvl(pcs.effective_date,sysdate) = nvl((select max(pcs2.effective_date)
                                                                from xxadeo_v_area_pcs pcs2
                                                               where pcs2.item = pcs.item
                                                                 and pcs2.bu = pcs.bu),sysdate)
                   and rownum = 1) as margin_perc
from item_master im, item_bs ibs, packitem pkitm
 where im.item = ibs.item
   and im.item = pkitm.item
);
