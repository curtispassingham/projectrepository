/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_BLOCK_PO_REASON_LIST" for RB119               */
/******************************************************************************/
create or replace view xxadeo_v_block_po_reason_list (code_type, code, code_desc,required_ind, code_seq) as
select code_type, code, code_desc,required_ind, code_seq
  from v_code_detail
 where code_type =
       (select code_type
          from cfa_attrib
         where view_col_name = 'BLOCKING_PO_ITEM_SUPP');   