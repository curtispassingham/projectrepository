/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_AREA_BU" for RB119b                           */
/******************************************************************************/
CREATE OR REPLACE VIEW XXADEO_V_AREA_BU AS
SELECT b.area,
       case
         when tl.lang is not null then
          tl.area_name
         else
          b.area_name
       end area_name,
       NVL(tl.lang, GET_PRIMARY_LANG()) lang
  FROM AREA b, AREA_TL tl
 WHERE b.area = tl.area(+)
   AND tl.lang(+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
   AND (exists (SELECT 'Y'
                  FROM SEC_USER_GROUP sug, SEC_USER su, FILTER_GROUP_ORG fgo
                 WHERE sug.user_seq = su.user_seq
                   AND (su.application_user_id = NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), SYS_CONTEXT('USERENV', 'CLIENT_INFO'))
                    or SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID') is NULL and su.database_user_id = NVL(SYS_CONTEXT('USERENV', 'CLIENT_INFO'), SYS_CONTEXT('USERENV', 'SESSION_USER')))
                   and sug.group_id = fgo.sec_group_id
                   AND fgo.filter_org_id = b.area
                   AND EXISTS (SELECT 'x'
                          FROM FILTER_GROUP_ORG fgo
                         WHERE fgo.sec_group_id = sug.group_id
                           AND ROWNUM = 1))
        or
        exists (SELECT 'Y'
                  FROM SEC_USER_GROUP sug, SEC_USER su
                 WHERE sug.user_seq = su.user_seq
                   AND (su.application_user_id = NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), SYS_CONTEXT('USERENV', 'CLIENT_INFO'))
                    or SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID') is NULL and su.database_user_id = NVL(SYS_CONTEXT('USERENV', 'CLIENT_INFO'), SYS_CONTEXT('USERENV', 'SESSION_USER')))
                   AND NOT EXISTS (SELECT 'x'
                                     FROM FILTER_GROUP_ORG fgo
                                    WHERE fgo.sec_group_id = sug.group_id
                                     AND ROWNUM = 1)
                   AND NOT EXISTS (SELECT 'x'
                                     FROM FILTER_GROUP_MERCH fgm
                                    WHERE FGM.SEC_GROUP_ID = sug.group_id
                                      AND ROWNUM = 1)))
order by b.area ASC;
