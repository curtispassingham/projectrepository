/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_CLASS" for RB119                              */
/******************************************************************************/
CREATE OR REPLACE VIEW XXADEO_V_CLASS AS
SELECT DEPT, CLASS, CLASS_NAME, GROUP_NO
  from (SELECT cl.DEPT,
               cl.CLASS,
               cl.CLASS_NAME,
               (select group_no from XXADEO_V_DEPS xvd where cl.DEPT = xvd.DEPT) as group_no,
               (select code_desc
                  from (select dept,
                               cfa_unc.group_id,
                               cfa.attrib_id ATTR_ID,
                               cd.code_desc
                          from cfa_attrib cfa,
                               (select dept, group_id, cfa_attribute, cfa_value
                                  from class_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                            varchar2_2,
                                                                                            varchar2_3,
                                                                                            varchar2_4,
                                                                                            varchar2_5,
                                                                                            varchar2_6,
                                                                                            varchar2_7,
                                                                                            varchar2_8,
                                                                                            varchar2_9,
                                                                                            varchar2_10))
                                 where cl.dept  = dept                                          
                                   and cl.CLASS = class) cfa_unc,
                               v_code_detail cd
                         where cfa.group_id = cfa_unc.group_id
                           and upper(cfa.storage_col_name) =
                               upper(cfa_unc.cfa_attribute)
                           and cd.code_type = cfa.code_type
                           and cd.code = cfa_value) aux,
                       xxadeo_mom_dvm x
                 where (x.value_1) = aux.attr_id
                   and func_area = 'CFA'
                   and parameter = 'CLASS_STATUS') as status
          FROM V_CLASS cl, class_cfa_ext cce
         where cl.dept  = cce.dept
           and cl.class = cce.class)
 where upper(status) = 'ACTIVE';
