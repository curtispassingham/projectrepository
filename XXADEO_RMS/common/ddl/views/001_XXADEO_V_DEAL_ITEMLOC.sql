/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_DEAL_ITEMLOC" for RR405                       */
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_V_DEAL_ITEMLOC AS
SELECT DEAL_ID, DEAL_DETAIL_ID, SEQ_NO, MERCH_LEVEL, COMPANY_IND, DIVISION, GROUP_NO, DEPT, CLASS, SUBCLASS, ITEM_PARENT, ITEM_GRANDPARENT, DIFF_1, DIFF_2, DIFF_3, DIFF_4, ORG_LEVEL, CHAIN, AREA, REGION, DISTRICT, LOCATION, ORIGIN_COUNTRY_ID, LOC_TYPE, ITEM, EXCL_IND, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME
FROM DEAL_ITEMLOC;