/**********************************************************************************/
/* CREATE DATE - October 2018                                                     */
/* CREATE USER - Elsa Barros                                                      */
/* PROJECT     - ADEO                                                             */
/* DESCRIPTION - MATERIALIZED VIEW "XXADEO_V_LOCATION_PCS" for RR157              */
/**********************************************************************************/
create or replace view xxadeo_v_location_pcs as
select mvpcs.item,
       mvpcs.supplier,
       mvpcs.zone_id,
       mvpcs.store,
       mvpcs.pcs,
       mvpcs.currency,
       mvpcs.circuit_principal,
       mvpcs.status,
       mvpcs.effective_date,
       mvpcs.msv_zone_amount,
       mvpcs.msv_zone_percent,
       mvpcs.msv_zone_percent_display,
       mvpcs.msv_magasin_amount,
       mvpcs.msv_magasin_percent,
       mvpcs.msv_magasin_percent_display
  from xxadeo_mv_location_pcs mvpcs,
       v_item_master          vim,
       v_sups                 vsups,
       v_store                vst
 where mvpcs.item     = vim.item
   and mvpcs.supplier = vsups.supplier
   and mvpcs.store    = vst.store;
comment on column XXADEO_V_LOCATION_PCS.ITEM is 'Item Identifier.';
comment on column XXADEO_V_LOCATION_PCS.SUPPLIER is 'Supplier Site Identifier.';
comment on column XXADEO_V_LOCATION_PCS.ZONE_ID is 'Zone Identifier taken from rpm_zone_location table.';
comment on column XXADEO_V_LOCATION_PCS.STORE is 'Store Identifier.';
comment on column XXADEO_V_LOCATION_PCS.PCS is 'PCS Store Amount, Taxes exclusive (VAT exclusive).';
comment on column XXADEO_V_LOCATION_PCS.CURRENCY is 'Store Currency.';
comment on column XXADEO_V_LOCATION_PCS.CIRCUIT_PRINCIPAL is 'Circuit Principal identifier Note: Valid values are (''1'', ''5'').';
comment on column XXADEO_V_LOCATION_PCS.STATUS is 'The Status of the record. Valid values: A - Active record; I - Inactive Record and ready to be purged. For this view, only records where status = ''A''';
comment on column XXADEO_V_LOCATION_PCS.EFFECTIVE_DATE is 'PCS Effective Date, used to identify actual PCS Store.';
comment on column XXADEO_V_LOCATION_PCS.MSV_ZONE_AMOUNT is 'Item/Zone Margin amount.';
comment on column XXADEO_V_LOCATION_PCS.MSV_ZONE_PERCENT is 'Item/Zone Margin percentage.';
comment on column XXADEO_V_LOCATION_PCS.MSV_ZONE_PERCENT_DISPLAY is 'Item/Zone Margin percentage in percentageformat.';
comment on column XXADEO_V_LOCATION_PCS.MSV_MAGASIN_AMOUNT is 'Item/Store Margin amount.';
comment on column XXADEO_V_LOCATION_PCS.MSV_MAGASIN_PERCENT is 'Item/Store Margin percentage.';
comment on column XXADEO_V_LOCATION_PCS.MSV_MAGASIN_PERCENT_DISPLAY is 'Item/Store Margin percentage format.';
   