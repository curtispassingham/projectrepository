/**********************************************************************************/
/* CREATE DATE - September 2018                                                   */
/* CREATE USER - Tiago Torres                                                     */
/* PROJECT     - ADEO                                                             */
/* DESCRIPTION - MATERIALIZED VIEW "XXADEO_MV_DASHBOARD_PRC_CHG_NR" for RR425/426 */
/**********************************************************************************/

begin
  execute immediate 'drop materialized view xxadeo_mv_dashboard_prc_chg_nr';
exception
  when others then
    null;
end;
/

create materialized view xxadeo_mv_dashboard_prc_chg_nr
REFRESH FORCE ON DEMAND WITH ROWID USING TRUSTED CONSTRAINTS 
as
select * from xxadeo_v_dashboard_prc_chg_nr;  