/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_DEPS" for RB119                               */
/******************************************************************************/
CREATE OR REPLACE VIEW XXADEO_V_DEPS (DIVISION, GROUP_NO, DEPT, DEPT_NAME, PURCHASE_TYPE, MARKUP_CALC_TYPE, OTB_CALC_TYPE) 
  AS  
    SELECT DIVISION,
          GROUP_NO,
          DEPT,
          DEPT_NAME,
          PURCHASE_TYPE,
          MARKUP_CALC_TYPE,
          OTB_CALC_TYPE
     from (select dep.DIVISION,
                  dep.GROUP_NO,
                  dep.DEPT,
                  dep.DEPT_NAME,
                  dep.PURCHASE_TYPE,
                  dep.MARKUP_CALC_TYPE,
                  dep.OTB_CALC_TYPE,
                  (select code_desc
                     from (select dept,
                                  cfa_unc.group_id,
                                  cfa.attrib_id ATTR_ID,
                                  cd.code_desc
                             from cfa_attrib cfa,
                                  (select dept,
                                          group_id,
                                          cfa_attribute,
                                          cfa_value
                                     from deps_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                              varchar2_2,
                                                                                              varchar2_3,
                                                                                              varchar2_4,
                                                                                              varchar2_5,
                                                                                              varchar2_6,
                                                                                              varchar2_7,
                                                                                              varchar2_8,
                                                                                              varchar2_9,
                                                                                              varchar2_10))
                                    where dep.DEPT = dept) cfa_unc,
                                  v_code_detail cd
                            where cfa.group_id = cfa_unc.group_id
                              and upper(cfa.storage_col_name) =
                                  upper(cfa_unc.cfa_attribute)
                              and cd.code_type = cfa.code_type
                              and cd.code = cfa_value) aux,
                          xxadeo_mom_dvm x
                    where (x.value_1) = aux.attr_id
                      and func_area = 'CFA'
                      and parameter = 'DEPARTMENT_STATUS') as status
             FROM V_DEPS dep, deps_cfa_ext dce
            where dep.DEPT = dce.dept)
    where upper(status) = 'ACTIVE';  