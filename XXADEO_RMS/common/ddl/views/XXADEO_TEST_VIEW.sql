/******************************************************************************/
/* CREATE DATE - January 2019                                                 */
/* CREATE USER - Pedro Lucas Farinha                                          */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_TEST_VIEW" for CI testing                       */
/******************************************************************************/
 
 create view XXADEO_TEST_VIEW as select dummy || 'test' as test2  from dual;
