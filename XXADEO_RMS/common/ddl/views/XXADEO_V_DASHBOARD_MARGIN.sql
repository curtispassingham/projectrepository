/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_V_DASHBOARD_MARGIN"                             */
/******************************************************************************/

create or replace view xxadeo_v_dashboard_margin as
with item_bs as
 (select item, bu_subscription
    from (select im.ITEM,
                 decode(uilv.uda_id,
                        (select value_1
                           from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                         I_parameter => 'BU_SUBSCRIPTION'))
                          where bu is null
                             or bu = -1),
                        uv.uda_value,
                        null) as bu_subscription
            from v_item_master im, uda_item_lov uilv, v_uda_values_tl uv
           where im.item = uilv.item
             and uilv.uda_id = uv.uda_id
             and uilv.uda_value = uv.uda_value)
   where bu_subscription is not null)
select ITEM,
       COST_CHANGE,
       SUPPLIER,
       origin_country_id,
       bu_subscription,
       bu_description,
       NATIONAL_PRC,
       CURR_STR_PRC,
       CURR_MARGIN,
       CURR_MARGIN_PERC,
       CURRENCY,
       (CURR_MARGIN + CURRENT_COST - NEW_COST) EST_MARG,
       ((CURR_MARGIN + CURRENT_COST - NEW_COST) - CURR_MARGIN) IMPACT,
       CONCAT(to_char(CAST(((CURR_MARGIN + CURRENT_COST - NEW_COST) / NATIONAL_PRC * 100) AS DECIMAL(10,2)),'99999999.999'),'%') EST_MARG_PERC,
       CONCAT(to_char(CAST((((CURR_MARGIN + CURRENT_COST - NEW_COST) / NATIONAL_PRC * 100) -
       CURR_MARGIN_PERC) AS DECIMAL(10,2)),'99999999.999'),'%') IMPACT_PERC
  FROM (Select im.item ITEM,
               cssd.cost_change,
               cssd.SUPPLIER,
               cssd.origin_country_id,
               ibu.bu_subscription bu_subscription,
               (select uv.uda_value_desc
                  from uda_item_lov uilv, v_uda_values_tl uv
                 where im.item = uilv.item
                   and uilv.uda_id = uv.uda_id
                   and uilv.uda_value = uv.uda_value
                   and uilv.uda_id = (select value_1
                                        from table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                                                      I_parameter => 'BU_SUBSCRIPTION'))
                                       where bu is null
                                          or bu = -1)
                   and uv.uda_value = ibu.bu_subscription) as bu_description,
               (select a.selling_retail
                  from rpm_item_zone_price a
                 inner join xxadeo_rpm_bu_price_zones b
                    on (a.zone_id = b.zone_id)
                 where (bu_zone_type = 'NATZ' and bu_id = ibu.bu_subscription and
                       item = im.item)) NATIONAL_PRC,
               (select pcs.pcs_ca
                  from xxadeo_v_area_pcs pcs
                 where pcs.item = im.item
                   and pcs.bu = ibu.bu_subscription
                   and nvl(pcs.effective_date, sysdate) =
                       nvl((select max(pcs2.effective_date)
                             from xxadeo_v_area_pcs pcs2
                            where pcs2.item = pcs.item
                              and pcs2.bu = pcs.bu),
                           sysdate)
                   and rownum = 1) CURR_STR_PRC,
               (select pcs.msv_ca_amount
                  from xxadeo_v_area_pcs pcs
                 where pcs.item = im.item
                   and pcs.bu = ibu.bu_subscription
                   and nvl(pcs.effective_date, sysdate) =
                       nvl((select max(pcs2.effective_date)
                             from xxadeo_v_area_pcs pcs2
                            where pcs2.item = pcs.item
                              and pcs2.bu = pcs.bu),
                           sysdate)
                   and rownum = 1) CURR_MARGIN,
               (select pcs.msv_ca_percent
                  from xxadeo_v_area_pcs pcs
                 where pcs.item = im.item
                   and pcs.bu = ibu.bu_subscription
                   and nvl(pcs.effective_date, sysdate) =
                       nvl((select max(pcs2.effective_date)
                             from xxadeo_v_area_pcs pcs2
                            where pcs2.item = pcs.item
                              and pcs2.bu = pcs.bu),
                           sysdate)
                   and rownum = 1) CURR_MARGIN_PERC,
               null CURRENCY,
               (select isc.unit_cost
                  from v_sups s, item_supp_country isc
                 where cssd.supplier = s.supplier
                   and im.item = isc.item
                   and s.SUPPLIER = isc.supplier
                   and cssd.origin_country_id = isc.origin_country_id) as current_cost,
               cssd.unit_cost as new_cost
          FROM v_item_master              im,
               xxadeo_v_cost_susp_sup_det cssd,
               item_bs                    ibu
         where im.item = cssd.item
           and im.item = ibu.item);