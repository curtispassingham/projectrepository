/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_MANUFACTURER_LIST" for RB119                  */
/******************************************************************************/
create or replace view xxadeo_v_manufacturer_list (partner_id,
       partner_desc,
       contact_name,
       contact_phone,
       contact_email,
       principle_country_id) as
select p.partner_id,
       p.partner_desc,
       p.contact_name,
       p.contact_phone,
       p.contact_email,
       p.principle_country_id
  from v_partner p
 where p.partner_type = 'S1'
 and p.status = 'A';   