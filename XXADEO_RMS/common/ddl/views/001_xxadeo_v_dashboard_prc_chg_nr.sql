/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_V_DASHBOARD_PRC_CHG_NR"                         */
/******************************************************************************/
begin
  execute immediate 'drop view XXADEO_V_DASHBOARD_PRC_CHG_NR';
exception
  when others then
    null;
end;
/
create or replace view XXADEO_V_DASHBOARD_PRC_CHG_NR as
select rpc.price_change_id,
       rpc.item,
       im.item_desc,
       rpc.location,
       rpc.zone_id,
       rpc.effective_date,
       rpc.reason_code,
       nvl((select iloc.selling_unit_retail
             from v_item_loc iloc
            where rpc.ITEM = iloc.item
              and rpc.location = iloc.loc),
           (select ripz.selling_retail
              from rpm_item_zone_price ripz
             where rpc.item = ripz.item(+)
               and rpc.zone_id = ripz.zone_id)) as current_price,
       nvl((select fr.selling_retail
             from rpm_future_retail fr
            where rpc.ITEM = fr.item
              and rpc.location = fr.location
              and fr.action_date = rpc.effective_date),
           (select zfr.selling_retail
              from rpm_zone_future_retail zfr
             where rpc.ITEM = zfr.item
               and rpc.zone_id = zfr.zone
               and zfr.action_date = rpc.effective_date)) as future_price,
       (select description from rpm_codes where CODE_ID = rpc.reason_code) as reason_desc,
       nvl((select st.area from v_store st where st.STORE = rpc.location),
           (select zone_bu.bu_id
              from xxadeo_rpm_bu_price_zones zone_bu
             where zone_bu.zone_id = rpc.zone_id)) as bu,
       (select group_no from v_deps where dept = im.dept) as dept_id,
       (select grp.GROUP_NAME
          from v_deps dept, v_groups grp
         where dept.group_no = grp.GROUP_NO
           and dept = im.dept) as dept_name,
       im.dept as subdept_id,
       (select dept_name from v_deps where dept = im.dept) as subdept_name,
       im.class as type,
       im.SUBCLASS as subtype
  from rpm_price_change rpc, v_item_master im
 where rpc.item = im.item
   and exists
 (select 1
          from xxadeo_v_deps dep, xxadeo_v_class cla, xxadeo_v_subclass sub
         where dep.DEPT = im.dept
           and cla.DEPT = dep.dept
           and cla.CLASS = im.CLASS
           and sub.DEPT = cla.dept
           and sub.CLASS = cla.CLASS
           and sub.SUBCLASS = im.SUBCLASS)
   and exists
 (select bu, subdept_id, effective_date
          from (select bu, subdept_id, effective_date, count(1) as total
                  from (select item, bu, subdept_id, effective_date
                          from (select im.item,
                                       nvl((select st.area
                                             from v_store st
                                            where st.STORE = rpc.location),
                                           (select zone_bu.bu_id
                                              from xxadeo_rpm_bu_price_zones zone_bu
                                             where zone_bu.zone_id = rpc.zone_id)) as bu,
                                       im.dept as subdept_id,
                                       rpc.effective_date
                                  from rpm_price_change rpc, item_master im
                                 where rpc.item = im.item
                                   and rpc.effective_date between get_vdate and
                                       TRUNC(get_vdate + 7 + 28, 'IW') - 1)
                         group by item, bu, subdept_id, effective_date)
                 group by bu, subdept_id, effective_date),
               code_detail cd
         where cd.code_type = 'PCHG'
           and cd.code = 'PRCCHG'
           and total >= to_number(cd.code_desc)
           and (bu =
               (select st.area from v_store st where st.STORE = rpc.location) or
               bu = (select zone_bu.bu_id
                        from xxadeo_rpm_bu_price_zones zone_bu
                       where zone_bu.zone_id = rpc.zone_id))
           and subdept_id = im.dept
           and effective_date = rpc.effective_date)
 order by effective_date; 