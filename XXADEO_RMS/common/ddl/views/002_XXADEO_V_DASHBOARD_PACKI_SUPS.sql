/******************************************************************************/
/* CREATE DATE - January 2019                                                 */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_DASHBOARD_PACKI_SUPS" for RB148               */
/******************************************************************************/

create or replace view xxadeo_v_dashboard_packi_sups as
select    'PACK' as flag,
          null as agg_item,
          pck.ITEM as item,
          pck.status,
          pck.pack_ind,
          pck.pack_type,
          pck.item_level,
          pck.tran_level,
          pck.check_uda_ind,
          supp.supp_ind,
          pck.group_no,
          pck.group_name,
          pck.dept,
          pck.dept_name,
          pck.class,
          pck.class_name,
          pck.subclass,
          pck.sub_name,
          pck.ranking_item,
          pck.item_desc,
          to_number(pck.derunit_conv_factor) as derunit_conv_factor,
          pck.qt_capacity,
          pck.contain_uom_item,
          pck.assortment_mode,
          pck.range_size,
          pck.orderable_ind,
          pck.sellable_ind,
          pck.pack_qty,
          pck.retail_prc_blocking,
          pck.lifecycle_status,
          pck.actif_ddate_item,
          pck.forecast_ind,
          supp.sous_typo_step,
          pck.standard_uom,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.supplier end) as supplier_site,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.sup_name end) as supplier_site_desc,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.MANUFACTURER_CODE end) as manufacturer_code,--,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.ORIGIN_COUNTRY_ID end) as origin_country,--,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.STATUT_LIEN_ARTICLE end) as status_link,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.GTIN end) as gtin,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.PCB end) as supplier_pack_size,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.PA_HT end) as supplier_cost_price,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.CURRENCY_CODE end) as currency,--,
          null as std_str_tr_prc_itemprc,
          (case when pck.orderable_ind = 'Y' and pck.sellable_ind = 'N' then null else pck.national_price_itmprc end) as national_price_itmprc,
          null as margin_itmprc,
          null as margin_perc,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.MOTIF_BLOCAGE_ACHAT end) as blocking_reason,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.STATUT_CONFORM_QUALITE end) as qual_comp_status,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.DATE_ARRET_LIEN end) as link_end_date,
          (case when pck.sellable_ind = 'Y' and pck.personalized_ind = 'Y' then null else supp.BU end) as SUPP_BU,
          pck.FILTER_ORG_ID as ITEM_BU
  from xxadeo_v_dashboard_pack_sups supp, xxadeo_v_dashboard_packitm pck
 where supp.item = pck.item
;
