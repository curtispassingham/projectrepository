/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_V_GROUPS" for RB119                             */
/******************************************************************************/
CREATE OR REPLACE VIEW XXADEO_V_GROUPS (GROUP_NO, GROUP_NAME, DIVISION) 
  AS SELECT GROUP_NO, GROUP_NAME, DIVISION
         FROM V_GROUPS;    