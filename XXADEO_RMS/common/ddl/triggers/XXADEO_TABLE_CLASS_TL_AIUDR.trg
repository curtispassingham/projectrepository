CREATE OR REPLACE TRIGGER XXADEO_TABLE_CLASS_TL_AIUDR
/*---------------------------------------------------------------------*/
  /*
  * Object Name:   XXADEO_TABLE_CLASS_TL_AIUDR.trg
  * Description:   Trigger that will be used everytime that occur a 
  *                change at class_tl table from merchandise hierarchy.
  *                This custom trigger makes use of the vanilla 
  *                functionallity and enhances its capability by 
  *                enriching the base information with the additional 
  *                information requested by ADEO.
  * Version:       1.0
  * Author:        ORC - Oracle Retail Consulting
  * Creation Date: 23/05/2018
  * Last Modified: 23/05/2018
  * History:
  *               1.0 - Initial version
  */
  /*------------------------------------------------------------------------*/
  AFTER DELETE OR INSERT OR UPDATE ON CLASS_TL

  REFERENCING OLD AS old NEW AS new
  FOR EACH ROW

DECLARE
  L_message_type  MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
  L_class         CLASS.CLASS%TYPE;
  L_dept          CLASS.DEPT%TYPE;
  L_class_name    CLASS.CLASS_NAME%TYPE;
  L_class_vat_ind CLASS.CLASS_VAT_IND%TYPE;
  L_class_rec     CLASS%ROWTYPE;
  L_error_msg     VARCHAR2(255) := NULL;
  L_status        VARCHAR2(1);
  program_error EXCEPTION;
BEGIN

  if inserting then
    select class_vat_ind, class_name
      into L_class_vat_ind, L_class_name
      from class
     where class = :new.class
       and dept = :new.dept;
  
    L_message_type            := XXADEO_RMSMFM_MERCHHIER.CLS_UPD;
    L_class                   := :new.class;
    L_dept                    := :new.dept;
    L_class_rec.class         := :new.class;
    L_class_rec.dept          := :new.dept;
    L_class_rec.class_name    := L_class_name;
    L_class_rec.class_vat_ind := L_class_vat_ind;
  elsif updating then
  
    select class_vat_ind, class_name
      into L_class_vat_ind, L_class_name
      from class
     where class = :old.class
       and dept = :old.dept;
  
    L_message_type            := XXADEO_RMSMFM_MERCHHIER.CLS_UPD;
    L_class                   := :old.class;
    L_dept                    := :old.dept;
    L_class_rec.class         := :old.class;
    L_class_rec.dept          := :old.dept;
    L_class_rec.class_name    := L_class_name;
    L_class_rec.class_vat_ind := L_class_vat_ind;
  else
    -- Deleting
      select class_vat_ind, class_name
      into L_class_vat_ind, L_class_name
      from class
     where class = :old.class
       and dept = :old.dept;
  
    L_message_type            := XXADEO_RMSMFM_MERCHHIER.CLS_UPD;
    L_class                   := :old.class;
    L_dept                    := :old.dept;
    L_class_rec.class         := :old.class;
    L_class_rec.dept          := :old.dept;
    L_class_rec.class_name    := L_class_name;
    L_class_rec.class_vat_ind := L_class_vat_ind;
  end if;

  if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                        L_message_type,
                                        null,
                                        null,
                                        null,
                                        null,
                                        L_dept,
                                        null,
                                        L_class,
                                        L_class_rec,
                                        null,
                                        null) THEN
    RAISE program_error;
  end if;

EXCEPTION
  when OTHERS then
    L_status := API_CODES.UNHANDLED_ERROR;
    API_LIBRARY.HANDLE_ERRORS(L_status,
                              L_error_msg,
                              API_LIBRARY.FATAL_ERROR,
                              'XXADEO_TABLE_CLASS_TL_AIUDR');
  
    raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
