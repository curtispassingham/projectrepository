create or replace TRIGGER XXADEO_TBL_RPM_PC_OUT_AIUDR
AFTER INSERT OR UPDATE ON RPM_PRICE_CHANGE
FOR EACH ROW
BEGIN

		-- In case a PC is created in RPM already in approved Status (external source)
        IF INSERTING THEN

            IF  :new.state LIKE '%approved%' THEN

				INSERT INTO XXADEO_STG_PC_HDR_OUT ( PRICE_CHANGE_ID,
                                                ITEM,
                                                ZONE,
                                                LOCATION,
                                                CREATE_DATETIME,
                                                LAST_UPDATE_DATETIME,
                                                EFFECTIVE_DATE,
                                                REASON_CODE,
                                                CHANGE_AMOUNT,
                                                CHANGE_CURRENCY,
                                                MULTI_UNITS,
                                                MULTI_UNIT_RETAIL,
                                                MULTI_UNIT_SELLING_UOM,
                                                MULTI_UNIT_RETAIL_CURRENCY,
                                                EVENT_TYPE,
                                                STATUS,
                                                PUB_INDICATOR,
                                                SEQ_NO
                                                ) VALUES (
                                                    :new.PRICE_CHANGE_ID,
                                                    :new.ITEM,
                                                    :new.ZONE_ID,
                                                    :new.LOCATION,
                                                    :new.CREATE_DATE,
                                                    SYSDATE,
                                                    :new.EFFECTIVE_DATE,
                                                    :new.REASON_CODE,
                                                    :new.CHANGE_AMOUNT, --SELLING_RETAIL
                                                    :new.CHANGE_CURRENCY, --SELLING_RETAIL_CURRENCY,
                                                    nvl(:new.MULTI_UNITS, :old.MULTI_UNITS),
                                                    nvl(:new.MULTI_UNIT_RETAIL, :old.MULTI_UNIT_RETAIL),
                                                    nvl(:new.MULTI_SELLING_UOM, :old.MULTI_SELLING_UOM),
                                                    nvl(:new.MULTI_UNIT_RETAIL_CURRENCY, :old.MULTI_UNIT_RETAIL_CURRENCY),
                                                    'CRE', --EVENT_TYPE (CRE, DEL),
                                                    'N',
                                                    'N',
                                                    XXADEO_PC_OUT_SEQ.Nextval
                                                );
                                             END IF;

		-- If an update has occured
        elsif UPDATING THEN
			-- If the PC has been Approved
            IF :new.state LIKE '%approved%'
                and :old.state not LIKE '%approved%' THEN

				INSERT INTO XXADEO_STG_PC_HDR_OUT ( PRICE_CHANGE_ID,
                                                ITEM,
                                                ZONE,
                                                LOCATION,
                                                CREATE_DATETIME,
                                                LAST_UPDATE_DATETIME,
                                                EFFECTIVE_DATE,
                                                REASON_CODE,
                                                CHANGE_AMOUNT,
                                                CHANGE_CURRENCY,
                                                MULTI_UNITS,
                                                MULTI_UNIT_RETAIL,
                                                MULTI_UNIT_SELLING_UOM,
                                                MULTI_UNIT_RETAIL_CURRENCY,
                                                EVENT_TYPE,
                                                STATUS,
                                                PUB_INDICATOR,
                                                SEQ_NO
                                                ) VALUES (
                                                    :new.PRICE_CHANGE_ID,
                                                    :new.ITEM,
                                                    :new.ZONE_ID,
                                                    :new.LOCATION,
                                                    :new.CREATE_DATE,
                                                    SYSDATE,
                                                    :new.EFFECTIVE_DATE,
                                                    :new.REASON_CODE,
                                                    :new.CHANGE_AMOUNT, --SELLING_RETAIL
                                                    :new.CHANGE_CURRENCY, --SELLING_RETAIL_CURRENCY,
                                                    nvl(:new.MULTI_UNITS, :old.MULTI_UNITS),
                                                    nvl(:new.MULTI_UNIT_RETAIL, :old.MULTI_UNIT_RETAIL),
                                                    nvl(:new.MULTI_SELLING_UOM, :old.MULTI_SELLING_UOM),
                                                    nvl(:new.MULTI_UNIT_RETAIL_CURRENCY, :old.MULTI_UNIT_RETAIL_CURRENCY),
                                                    'CRE', --EVENT_TYPE (CRE, DEL),
                                                    'N',
                                                    'N',
                                                    XXADEO_PC_OUT_SEQ.Nextval
                                                );


			-- If the PC has been reverted to Worksheet status (for the purposes of the interface in scope, only valid from Approved Status and if not yet executed)
			elsIF :new.state like '%worksheet%' and (:old.state like '%conflict%' or
                     :old.state like '%approved%') and :old.approval_date is not null THEN

				INSERT INTO XXADEO_STG_PC_HDR_OUT ( PRICE_CHANGE_ID,
                                                ITEM,
                                                ZONE,
                                                LOCATION,
                                                CREATE_DATETIME,
                                                LAST_UPDATE_DATETIME,
                                                EFFECTIVE_DATE,
                                                REASON_CODE,
                                                CHANGE_AMOUNT,
                                                CHANGE_CURRENCY,
                                                MULTI_UNITS,
                                                MULTI_UNIT_RETAIL,
                                                MULTI_UNIT_SELLING_UOM,
                                                MULTI_UNIT_RETAIL_CURRENCY,
                                                EVENT_TYPE,
                                                STATUS,
                                                PUB_INDICATOR,
                                                SEQ_NO
                                                ) VALUES (
                                                    :new.PRICE_CHANGE_ID,
                                                    :new.ITEM,
                                                    :new.ZONE_ID,
                                                    :new.LOCATION,
                                                    :new.CREATE_DATE,
                                                    SYSDATE,
                                                    :new.EFFECTIVE_DATE,
                                                    :new.REASON_CODE,
                                                    :new.CHANGE_AMOUNT, --SELLING_RETAIL
                                                    :new.CHANGE_CURRENCY, --SELLING_RETAIL_CURRENCY,
                                                    nvl(:new.MULTI_UNITS, :old.MULTI_UNITS),
                                                    nvl(:new.MULTI_UNIT_RETAIL, :old.MULTI_UNIT_RETAIL),
                                                    nvl(:new.MULTI_SELLING_UOM, :old.MULTI_SELLING_UOM),
                                                    nvl(:new.MULTI_UNIT_RETAIL_CURRENCY, :old.MULTI_UNIT_RETAIL_CURRENCY),
                                                    'DEL', --EVENT_TYPE (CRE, DEL),
                                                    'N',
                                                    'N',
                                                    XXADEO_PC_OUT_SEQ.Nextval
                                                );
                                                END IF;
        END IF;
END;
/
ALTER TRIGGER XXADEO_TBL_RPM_PC_OUT_AIUDR ENABLE;