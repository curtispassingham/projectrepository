--------------------------------------------------------
--  File created - Thursday-May-24-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Trigger XXADEO_EC_TABLE_DEP_AIUDR
--------------------------------------------------------

CREATE OR REPLACE TRIGGER XXADEO_EC_TABLE_DEP_AIUDR
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_EC_TABLE_DEP_AIUDR.trg
* Description:   Trigger that will be used everytime that occur a 
*                change at deps table from merchandise hierarchy.
*                This custom trigger makes use of the vanilla 
*                functionallity and enhances its capability by 
*                enriching the base information with the additional 
*                information requested by ADEO.
* Version:       1.0
* Author:        ORC - Oracle Retail Consulting
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
AFTER DELETE OR INSERT OR UPDATE ON DEPS 
FOR EACH ROW
DECLARE


   L_message_type       MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
   L_dept               DEPS.DEPT%TYPE;
   L_dept_rec           DEPS%ROWTYPE;
   L_error_msg          VARCHAR2(255) := NULL;
   L_status             VARCHAR2(1);
   program_error        EXCEPTION;
   L_dep_division       varchar2(20);
   type classe_TBL      is table of CLASS%ROWTYPE;
   L_classes            classe_TBL;
   type subclasse_TBL      is table of SUBCLASS%ROWTYPE;
   L_subclasses            subclasse_TBL;
   

   
   CURSOR C_get_classes IS
    SELECT * 
    from class c
    where c.Dept = :old.dept;
    
    CURSOR C_get_subclasses IS
    SELECT  *
    from subclass c
    where c.dept = :old.dept;
   
BEGIN       

  if inserting then
    L_message_type := RMSMFM_MERCHHIER.DEP_ADD;
    L_dept := :new.dept;
    L_dept_rec.dept := :new.dept;
    L_dept_rec.dept_name := :new.dept_name;
    L_dept_rec.buyer := :new.buyer;
    L_dept_rec.merch := :new.merch;
    L_dept_rec.profit_calc_type := :new.profit_calc_type;
    L_dept_rec.purchase_type := :new.purchase_type;
    L_dept_rec.group_no := :new.group_no;
    L_dept_rec.bud_int := :new.bud_int;
    L_dept_rec.bud_mkup := :new.bud_mkup;
    L_dept_rec.total_market_amt := :new.total_market_amt;
    L_dept_rec.markup_calc_type := :new.markup_calc_type;
    L_dept_rec.otb_calc_type := :new.otb_calc_type;
    L_dept_rec.dept_vat_incl_ind := :new.dept_vat_incl_ind;
  elsif updating then
    L_message_type := RMSMFM_MERCHHIER.DEP_UPD;
    L_dept := :old.dept;
    L_dept_rec.dept := :old.dept;
    L_dept_rec.dept_name := :new.dept_name;
    L_dept_rec.buyer := :new.buyer;
    L_dept_rec.merch := :new.merch;
    L_dept_rec.profit_calc_type := :new.profit_calc_type;
    L_dept_rec.purchase_type := :new.purchase_type;
    L_dept_rec.group_no := :new.group_no;
    L_dept_rec.bud_int := :new.bud_int;
    L_dept_rec.bud_mkup := :new.bud_mkup;
    L_dept_rec.total_market_amt := :new.total_market_amt;
    L_dept_rec.markup_calc_type := :new.markup_calc_type;
    L_dept_rec.otb_calc_type := :new.otb_calc_type;
    L_dept_rec.dept_vat_incl_ind := :new.dept_vat_incl_ind;
  else -- Deleting
    L_message_type := RMSMFM_MERCHHIER.DEP_DEL;
    L_dept := :old.dept;
    L_dept_rec.group_no := :old.group_no;
    IF MERCH_NOTIFY_API_SQL.DELETE_DEPARTMENT( L_error_msg,
                                               L_dept) = FALSE THEN
       RAISE program_error;
    END IF;

  end if;

  if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                 L_message_type,
                                 null,
                                 null,
                                 null,
                                 null,
                                 L_dept,
                                 L_dept_rec,
                                 null,
                                 null,
                                 null,
                                 null) THEN
     RAISE program_error;
  end if;
  
  if updating and :new.group_no != :old.group_no then
  
  OPEN C_get_classes;
  FETCH C_get_classes BULK COLLECT INTO L_classes;
  CLOSE C_get_classes;
  
  OPEN C_get_subclasses;
  FETCH C_get_subclasses BULK COLLECT INTO L_subclasses;
  CLOSE C_get_subclasses;

  -- loop through all the classes and subclasses
    for i in 1..L_classes.count loop        

        if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                 RMSMFM_MERCHHIER.CLS_UPD,
                                 null,
                                 null,
                                 :new.group_no,
                                 null,
                                 L_dept,
                                 null,
                                 L_classes(i).class,
                                 L_classes(i),
                                 null,
                                 null) THEN
     RAISE program_error;
     
    end if;    
    end loop;
    
    for i in 1..L_subclasses.count loop        

        if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                 RMSMFM_MERCHHIER.SUB_UPD,
                                 null,
                                 null,
                                 :new.group_no,
                                 null,
                                 L_dept,
                                 null,
                                 L_subclasses(i).class,                             
                                 null,
                                 L_subclasses(i).subclass,
                                 L_subclasses(i)) THEN
     RAISE program_error;
     
    end if;
    end loop;

  end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_EC_TABLE_DEP_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
ALTER TRIGGER XXADEO_EC_TABLE_DEP_AIUDR ENABLE;
/