CREATE OR REPLACE TRIGGER XXADEO_TABLE_DIVISION_TL_AIUDR
/*---------------------------------------------------------------------*/
  /*
  * Object Name:   XXADEO_TABLE_DIVISION_TL_AIUDR.trg
  * Description:   Trigger that will be used everytime that occur a 
  *                change at division_tl table from merchandise hierarchy.
  *                This custom trigger makes use of the vanilla 
  *                functionallity and enhances its capability by 
  *                enriching the base information with the additional 
  *                information requested by ADEO.
  * Version:       1.0
  * Author:        ORC - Oracle Retail Consulting
  * Creation Date: 23/05/2018
  * Last Modified: 23/05/2018
  * History:
  *               1.0 - Initial version
  */
  /*------------------------------------------------------------------------*/
  AFTER DELETE OR INSERT OR UPDATE ON DIVISION_TL

  REFERENCING OLD AS old NEW AS new
  FOR EACH ROW

DECLARE
  L_message_type     MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
  L_division         DIVISION.DIVISION%TYPE;
  L_div_name         DIVISION.DIV_NAME%TYPE;
  L_buyer            DIVISION.BUYER%TYPE;
  L_merch            DIVISION.MERCH%TYPE;
  L_total_market_amt DIVISION.TOTAL_MARKET_AMT%TYPE;
  L_division_rec     DIVISION%ROWTYPE;
  L_error_msg        VARCHAR2(255) := NULL;
  L_status           VARCHAR2(1);
  program_error EXCEPTION;

BEGIN
  L_message_type := XXADEO_RMSMFM_MERCHHIER.DIV_UPD;

  if inserting then
    select div_name, buyer, merch, total_market_amt
      into L_div_name, L_buyer, L_merch, L_total_market_amt
      from division
     where division = :new.division;
  
    L_division                      := :new.division;
    L_division_rec.division         := :new.division;
    L_division_rec.div_name         := L_div_name;
    L_division_rec.buyer            := L_buyer;
    L_division_rec.merch            := L_merch;
    L_division_rec.total_market_amt := L_total_market_amt;
  
  elsif updating then
    select div_name, buyer, merch, total_market_amt
      into L_div_name, L_buyer, L_merch, L_total_market_amt
      from division
     where division = :old.division;
    L_division                      := :old.division;
    L_division_rec.division         := :old.division;
    L_division_rec.div_name         := L_div_name;
    L_division_rec.buyer            := L_buyer;
    L_division_rec.merch            := L_merch;
    L_division_rec.total_market_amt := L_total_market_amt;
  
  else
    -- Deleting
    select div_name, buyer, merch, total_market_amt
      into L_div_name, L_buyer, L_merch, L_total_market_amt
      from division
     where division = :old.division;
    L_division                      := :old.division;
    L_division_rec.division         := :old.division;
    L_division_rec.div_name         := L_div_name;
    L_division_rec.buyer            := L_buyer;
    L_division_rec.merch            := L_merch;
    L_division_rec.total_market_amt := L_total_market_amt;
    
  end if;

  if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                        L_message_type,
                                        L_division,
                                        L_division_rec,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null) THEN
    RAISE program_error;
  end if;

EXCEPTION
  when OTHERS then
    L_status := API_CODES.UNHANDLED_ERROR;
    API_LIBRARY.HANDLE_ERRORS(L_status,
                              L_error_msg,
                              API_LIBRARY.FATAL_ERROR,
                              'XXADEO_TABLE_DIVISION_TL_AIUDR');
  
    raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
  
END;
/
