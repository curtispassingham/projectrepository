
CREATE OR REPLACE TRIGGER XXADEO_EC_TABLE_IIM_AIUDR
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_EC_TABLE_IIM_AIUDR.trg
* Description:   Trigger that will be used everytime that occur a 
*                change at item_image table.
*				 This trigger will capture any change for items when	
*				 item_level < tran_level (Style items)
*                
*
* Version:       1.0
* Author:        ORC - Oracle Retail Consulting
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
 AFTER DELETE OR INSERT OR UPDATE OF IMAGE_ADDR
, IMAGE_TYPE
, PRIMARY_IND
, DISPLAY_PRIORITY
 ON ITEM_IMAGE
 FOR EACH ROW
DECLARE

   L_queue_rec     ITEM_MFQUEUE%ROWTYPE := NULL;
   L_status        VARCHAR2(1) := NULL;
   L_text          VARCHAR2(255) := NULL;
   L_item_row      ITEM_MASTER%ROWTYPE := NULL;

   PROGRAM_ERROR   EXCEPTION;
BEGIN
   if DELETING then
      L_queue_rec.message_type := RMSMFM_ITEMS.IMG_DEL;
      L_queue_rec.item := :old.item;
      L_queue_rec.image_name := :old.image_name;
   else
      if INSERTING then
         L_queue_rec.message_type := RMSMFM_ITEMS.IMG_ADD;
      else
         L_queue_rec.message_type := RMSMFM_ITEMS.IMG_UPD;
      end if;

      L_queue_rec.item := :new.item;
      L_queue_rec.image_name := :new.image_name;

   end if;

   if not ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                          L_item_row,
                                          L_queue_rec.item) then
      raise PROGRAM_ERROR;
   end if;

   if L_item_row.item_level < L_item_row.tran_level then
      if RMSMFM_ITEMS.ADDTOQ(L_text,
                             L_queue_rec,
                             NULL,               ---I_sellable_ind
                             NULL) = FALSE then  ---I_tran_level_ind
         raise PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_EC_TABLE_IIM_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END;
/