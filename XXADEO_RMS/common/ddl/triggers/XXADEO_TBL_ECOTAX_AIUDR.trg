CREATE OR REPLACE TRIGGER XXADEO_TBL_ECOTAX_AIUDR
  BEFORE DELETE OR INSERT OR UPDATE ON ITEM_EXP_DETAIL
  FOR EACH ROW

DECLARE
  L_country_id  ITEM_EXP_HEAD.ORIGIN_COUNTRY_ID%TYPE;
  L_zone_id     ITEM_EXP_HEAD.ZONE_ID%TYPE;
  L_lading_port ITEM_EXP_HEAD.LADING_PORT%TYPE;
  L_count       NUMBER(10);

  CURSOR C_GET_ECOTAXES(P_ecotax_id IN ELC_COMP.COMP_ID%TYPE) is
    SELECT XXADEO_CFAS_UTILS.GET_CFA_VALUE_NUM('ELC_COMP',
                                                     P_ecotax_id,
                                                     (SELECT VALUE_1
                                                        FROM XXADEO_MOM_DVM
                                                       WHERE FUNC_AREA =
                                                             'CFA'
                                                         AND PARAMETER =
                                                             'ECOTAX_ELC_COMP'))
      from dual;
BEGIN

  IF INSERTING THEN
  
    OPEN C_GET_ECOTAXES(:new.COMP_ID);
    FETCH C_GET_ECOTAXES
      INTO L_count;
    CLOSE C_GET_ECOTAXES;
  
    IF L_count is not null THEN
      INSERT INTO XXADEO_STG_ECOTAX_OUT
        (ITEM,
         SUPPLIER,
         ITEM_EXP_TYPE,
         ITEM_EXP_SEQ,
         COMP_ID,
         EVENT_TYPE,
         RECORD_STATUS,
         LAST_UPDATE_TS,
         SEQ_NO)
      VALUES
        (:new.ITEM,
         :new.SUPPLIER,
         :new.ITEM_EXP_TYPE,
         :new.ITEM_EXP_SEQ,
         :new.COMP_ID,
         'C',
         'N',
         SYSDATE,
         XXADEO_ECOTAX_OUT_SEQ.nextval);
    END IF;
  
  ELSIF UPDATING THEN
  
    OPEN C_GET_ECOTAXES(:new.COMP_ID);
    FETCH C_GET_ECOTAXES
      INTO L_count;
    CLOSE C_GET_ECOTAXES;
  
    IF L_count is not null THEN
      INSERT INTO XXADEO_STG_ECOTAX_OUT
        (ITEM,
         SUPPLIER,
         ITEM_EXP_TYPE,
         ITEM_EXP_SEQ,
         COMP_ID,
         EVENT_TYPE,
         RECORD_STATUS,
         LAST_UPDATE_TS,
         SEQ_NO)
      VALUES
        (:new.ITEM,
         :new.SUPPLIER,
         :new.ITEM_EXP_TYPE,
         :new.ITEM_EXP_SEQ,
         :new.COMP_ID,
         'U',
         'N',
         SYSDATE,
         XXADEO_ECOTAX_OUT_SEQ.nextval);
    END IF;
  
  ELSE
  
    OPEN C_GET_ECOTAXES(:old.COMP_ID);
    FETCH C_GET_ECOTAXES
      INTO L_count;
    CLOSE C_GET_ECOTAXES;
  
    IF L_count is not null THEN
      SELECT ORIGIN_COUNTRY_ID, ZONE_ID, LADING_PORT
        INTO L_country_id, L_zone_id, L_lading_port
        FROM ITEM_EXP_HEAD ieh
       WHERE ieh.ITEM = :old.ITEM
         AND ieh.SUPPLIER = :old.SUPPLIER
         AND ieh.ITEM_EXP_TYPE = :old.ITEM_EXP_TYPE
         AND ieh.ITEM_EXP_SEQ = :old.ITEM_EXP_SEQ;
    
      INSERT INTO XXADEO_STG_ECOTAX_OUT
        (ITEM,
         SUPPLIER,
         ITEM_EXP_TYPE,
         ITEM_EXP_SEQ,
         COMP_ID,
         ORIGIN_COUNTRY_ID,
         ZONE_ID,
         LADING_PORT,
         EVENT_TYPE,
         RECORD_STATUS,
         LAST_UPDATE_TS,
         SEQ_NO)
      VALUES
        (:old.ITEM,
         :old.SUPPLIER,
         :old.ITEM_EXP_TYPE,
         :old.ITEM_EXP_SEQ,
         :old.COMP_ID,
         L_country_id,
         L_zone_id,
         L_lading_port,
         'D',
         'N',
         SYSDATE,
         XXADEO_ECOTAX_OUT_SEQ.nextval);
    END IF;
  END IF;
END;
/