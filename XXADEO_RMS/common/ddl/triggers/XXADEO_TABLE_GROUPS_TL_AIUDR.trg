create or replace TRIGGER XXADEO_TABLE_GROUPS_TL_AIUDR
/*---------------------------------------------------------------------*/
  /*
  * Object Name:   XXADEO_TABLE_GROUPS_TL_AIUDR.trg
  * Description:   Trigger that will be used everytime that occur a 
  *                change at groups_tl table from merchandise hierarchy.
  *                This custom trigger makes use of the vanilla 
  *                functionallity and enhances its capability by 
  *                enriching the base information with the additional 
  *                information requested by ADEO.
  * Version:       1.0
  * Author:        ORC - Oracle Retail Consulting
  * Creation Date: 23/05/2018
  * Last Modified: 23/05/2018
  * History:
  *               1.0 - Initial version
  */
  /*------------------------------------------------------------------------*/
  AFTER DELETE OR INSERT OR UPDATE ON GROUPS_TL

  REFERENCING OLD AS old NEW AS new
  FOR EACH ROW

DECLARE

  L_message_type MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
  L_group_no     GROUPS.GROUP_NO%TYPE;
  L_buyer        GROUPS.BUYER%TYPE;
  L_merch        GROUPS.MERCH%TYPE;
  L_division     GROUPS.DIVISION%TYPE;
  L_group_name   GROUPS.GROUP_NAME%TYPE;
  L_group_rec    GROUPS%ROWTYPE;
  L_error_msg    VARCHAR2(255) := NULL;
  L_status       VARCHAR2(1);
  program_error EXCEPTION;
BEGIN

  if inserting then
  
    select buyer, merch, division, group_name
      into L_buyer, L_merch, L_division, L_group_name
      from groups
     where Group_No = :new.group_no;
  
    L_message_type         := XXADEO_RMSMFM_MERCHHIER.GRP_UPD;
    L_group_no             := :new.group_no;
    L_group_rec.group_no   := :new.group_no;
    L_group_rec.group_name := L_group_name;
    L_group_rec.buyer      := L_buyer;
    L_group_rec.merch      := L_merch;
    L_group_rec.division   := L_division;
  elsif updating then
    select buyer, merch, division, group_name
      into L_buyer, L_merch, L_division, L_group_name
      from groups
     where Group_No = :old.group_no;
  
    L_message_type         := XXADEO_RMSMFM_MERCHHIER.GRP_UPD;
    L_group_no             := :old.group_no;
    L_group_rec.group_no   := :old.group_no;
    L_group_rec.group_name := L_group_name;
    L_group_rec.buyer      := L_buyer;
    L_group_rec.merch      := L_merch;
    L_group_rec.division   := L_division;
  else
    -- Deleting
    select buyer, merch, division, group_name
      into L_buyer, L_merch, L_division, L_group_name
      from groups
     where Group_No = :old.group_no;
  
    L_message_type         := XXADEO_RMSMFM_MERCHHIER.GRP_UPD;
    L_group_no             := :old.group_no;
    L_group_rec.group_no   := :old.group_no;
    L_group_rec.group_name := L_group_name;
    L_group_rec.buyer      := L_buyer;
    L_group_rec.merch      := L_merch;
    L_group_rec.division   := L_division;
  end if;

  if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                        L_message_type,
                                        null,
                                        null,
                                        L_group_no,
                                        L_group_rec,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null) THEN
    RAISE program_error;
  end if;

EXCEPTION
  when OTHERS then
    L_status := API_CODES.UNHANDLED_ERROR;
    API_LIBRARY.HANDLE_ERRORS(L_status,
                              L_error_msg,
                              API_LIBRARY.FATAL_ERROR,
                              'XXADEO_TABLE_GROUPS_TL_AIUDR');
  
    raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
