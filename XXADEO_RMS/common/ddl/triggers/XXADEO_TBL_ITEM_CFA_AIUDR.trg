CREATE OR REPLACE TRIGGER XXADEO_TBL_ITEM_CFA_AIUDR
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_TBL_ITEM_CFA_AIUDR.trg
* Description:   Trigger that will be used everytime that occur a 
*                change at item_master_cfa_ext table.
*				 This trigger will capture any change of CFAS  
*				 

* Version:       1.0
* Author:        ORC - Oracle Retail Consulting
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
  AFTER DELETE OR INSERT OR UPDATE ON ITEM_MASTER_CFA_EXT
  FOR EACH ROW
DECLARE

  L_item           ITEM_MASTER.ITEM%TYPE;
  L_error_message  VARCHAR2(4000);
  L_queue_rec      ITEM_MFQUEUE%ROWTYPE := NULL;
  L_status         VARCHAR2(1) := NULL;
  L_text           VARCHAR2(255) := NULL;
  L_item_level     ITEM_MASTER.ITEM_LEVEL%TYPE;
  L_tran_level     ITEM_MASTER.TRAN_LEVEL%TYPE;
  L_tran_level_ind ITEM_PUB_INFO.TRAN_LEVEL_IND%TYPE;
  L_sellable_ind   ITEM_PUB_INFO.SELLABLE_IND%TYPE;
  L_status_item    ITEM_MASTER.STATUS%TYPE;

  PROGRAM_ERROR EXCEPTION;
BEGIN
  if inserting then
    L_item := :new.item;
  else
    L_item := :old.item;
  end if;

  select item_level, tran_level, sellable_ind, status
    into L_item_level, L_tran_level, L_sellable_ind, L_status_item
    from item_master
   where item = L_item;

  -- item is a transaction-level item or above --

  if L_item_level = L_tran_level then
    L_tran_level_ind := 'Y';
  else
    L_tran_level_ind := 'N';
  end if;

  if DELETING then
    /* For deletes, the row identifier will be published. */

    L_queue_rec.message_type := RMSMFM_ITEMS.ITEM_UPD;
    L_queue_rec.item         := :old.item;
    L_queue_rec.approve_ind  := 'N';
    L_queue_rec.item         := L_item;
    if RMSMFM_ITEMS.ADDTOQ(L_text,
                           L_queue_rec,
                           L_sellable_ind,
                           L_tran_level_ind) = FALSE then
      raise PROGRAM_ERROR;
    end if;
  else
    if INSERTING then
      L_queue_rec.message_type := RMSMFM_ITEMS.ITEM_UPD;
      if L_status = 'A' then
        L_queue_rec.approve_ind := 'Y';
      else
        L_queue_rec.approve_ind := 'N';
      end if;
      L_queue_rec.item := L_item;
      if RMSMFM_ITEMS.ADDTOQ(L_text,
                             L_queue_rec,
                             L_sellable_ind,
                             L_tran_level_ind) = FALSE then
        raise PROGRAM_ERROR;
      end if;
    else
      L_queue_rec.message_type := RMSMFM_ITEMS.ITEM_UPD;
      if :new.varchar2_1 != :old.varchar2_1 or
         :new.varchar2_2 != :old.varchar2_2 or
         :new.varchar2_3 != :old.varchar2_3 or
         :new.varchar2_4 != :old.varchar2_4 or
         :new.varchar2_5 != :old.varchar2_5 or
         :new.varchar2_6 != :old.varchar2_6 or
         :new.varchar2_7 != :old.varchar2_7 or
         :new.varchar2_8 != :old.varchar2_8 or
         :new.varchar2_9 != :old.varchar2_9 or
         :new.varchar2_10 != :old.varchar2_10 or
         :new.number_11 != :old.number_11 or
         :new.number_12 != :old.number_12 or
         :new.number_13 != :old.number_13 or
         :new.number_14 != :old.number_14 or
         :new.number_15 != :old.number_15 or
         :new.number_16 != :old.number_16 or
         :new.number_17 != :old.number_17 or
         :new.number_18 != :old.number_18 or
         :new.number_19 != :old.number_19 or
         :new.number_20 != :old.number_20 or :new.date_21 != :old.date_21 or
         :new.date_22 != :old.date_22 or :new.date_23 != :old.date_23 or
         :new.date_24 != :old.date_24 or :new.date_25 != :old.date_25 then
        if L_status = 'A' then
          L_queue_rec.approve_ind := 'Y';
        else
          L_queue_rec.approve_ind := 'N';
        end if;
        L_queue_rec.item := L_item;
        if RMSMFM_ITEMS.ADDTOQ(L_text,
                               L_queue_rec,
                               L_sellable_ind,
                               L_tran_level_ind) = FALSE then
          raise PROGRAM_ERROR;
        end if;
      end if;
    end if;

  end if;

EXCEPTION
  when OTHERS then
    if L_error_message is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'XXADEO_TBL_ITEM_CFA_AIUDR',
                                            to_char(SQLCODE));
    end if;
    raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END XXADEO_TBL_ITEM_CFA_AIUDR;
/