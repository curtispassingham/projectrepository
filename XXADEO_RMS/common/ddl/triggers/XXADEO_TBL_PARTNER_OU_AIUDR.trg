--------------------------------------------------------
--  DDL for Trigger XXADEO_TBL_PARTNER_OU_AIUDR
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER XXADEO_TBL_PARTNER_OU_AIUDR

AFTER INSERT OR UPDATE ON PARTNER_ORG_UNIT 
REFERENCING
       OLD AS old
       NEW AS new
FOR EACH ROW

DECLARE

BEGIN

if (inserting and :new.partner_type = 'U') then     

  INSERT INTO Xxadeo_Stg_Sups_out (  Supplier,
                                            Supp_Change,
                                            Addr_Change,
                                            Pou_Change,
                                            Cfa_Sup_Change,
                                            Pub_Status,
                                            Creation_Date,
                                            Last_Update_Date
                                ) VALUES (:new.Partner,
                                          null,
                                          null,
                                          'Y',
                                          null,
                                          'N',
                                          sysdate,
                                          sysdate
                                        );
    elsif (updating AND :new.partner_type = 'U') then
    
     INSERT INTO Xxadeo_Stg_Sups_out (  Supplier,
                                            Supp_Change,
                                            Addr_Change,
                                            Pou_Change,
                                            Cfa_Sup_Change,
                                            Pub_Status,
                                            Creation_Date,
                                            Last_Update_Date
                                ) VALUES (:new.Partner,
                                          null,
                                          null,
                                          'Y',
                                          null,
                                          'N',
                                          sysdate,
                                          sysdate
                                        );
    Else
        NULL;
    end if;
END;
/
ALTER TRIGGER XXADEO_TBL_PARTNER_OU_AIUDR ENABLE;
/
