CREATE OR REPLACE TRIGGER XXADEO_EC_TABLE_UIL_AIDR
/*---------------------------------------------------------------------*/
  /*
  * Object Name:   XXADEO_EC_TABLE_UIL_AIDR.trg
  * Description:   Trigger that will be used everytime that occur a 
  *                change at uda_item_lov table.
  *        This trigger will capture any change of Udas when  
  *        item_level != tran_level (Style items)
  *        
  
  * Version:       1.0
  * Author:        ORC - Oracle Retail Consulting
  * Creation Date: 04/07/2018
  * Last Modified: 04/07/2018
  * History:
  *               1.0 - Initial version
  */
  /*------------------------------------------------------------------------*/
  AFTER DELETE OR INSERT OR UPDATE ON UDA_ITEM_LOV
  FOR EACH ROW
DECLARE

  L_record       UDA_VALUES%ROWTYPE := NULL;
  L_queue_rec    ITEM_MFQUEUE%ROWTYPE := NULL;
  L_action_type  VARCHAR2(1) := NULL;
  L_message      CLOB;
  L_message_type ITEM_MFQUEUE.MESSAGE_TYPE%TYPE;
  L_status       VARCHAR2(1) := NULL;
  L_text         VARCHAR2(255) := NULL;
  L_item_row     ITEM_MASTER%ROWTYPE := NULL;

  PROGRAM_ERROR EXCEPTION;
BEGIN
  if DELETING then
    L_action_type      := 'D';
    L_message_type     := RMSMFM_ITEMS.UDAL_DEL;
    L_record.uda_id    := :old.uda_id;
    L_record.uda_value := :old.uda_value;
  
    L_queue_rec.message_type := RMSMFM_ITEMS.UDAL_DEL;
    L_queue_rec.item         := :old.item;
    L_queue_rec.uda_id       := :old.uda_id;
    L_queue_rec.uda_value    := :old.uda_value;
  end if; --end of if DELETING

  if INSERTING then
    L_action_type      := 'A';
    L_message_type     := RMSMFM_ITEMS.UDAL_ADD;
    L_record.UDA_ID    := :new.UDA_ID;
    L_record.UDA_VALUE := :new.UDA_VALUE;
  
    L_queue_rec.message_type := RMSMFM_ITEMS.UDAL_ADD;
    L_queue_rec.item         := :new.item;
    L_queue_rec.uda_id       := :new.uda_id;
    L_queue_rec.uda_value    := :new.uda_value;
  end if; --end of inserting

  if UPDATING then
    L_action_type      := 'D';
    L_message_type     := RMSMFM_ITEMS.UDAL_DEL;
    L_record.uda_id    := :old.uda_id;
    L_record.uda_value := :old.uda_value;
  
    L_queue_rec.message_type := RMSMFM_ITEMS.UDAL_DEL;
    L_queue_rec.item         := :old.item;
    L_queue_rec.uda_id       := :old.uda_id;
    L_queue_rec.uda_value    := :old.uda_value;
  
    if not
        ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text, L_item_row, L_queue_rec.item) then
      raise PROGRAM_ERROR;
    end if;
  
    if L_item_row.item_level != L_item_row.tran_level then
      if not UDA_XML.BUILD_UDAV_MSG(L_status,
                                    L_text,
                                    L_message,
                                    L_record,
                                    L_action_type) then
        raise PROGRAM_ERROR;
      end if;
    
      if RMSMFM_ITEMS.ADDTOQ(L_text,
                             L_queue_rec,
                             NULL, ---I_sellable_ind
                             NULL) = FALSE then
        ---I_tran_level_ind
        raise PROGRAM_ERROR;
      end if;
    
      if L_status = API_CODES.UNHANDLED_ERROR then
        raise PROGRAM_ERROR;
      end if;
    end if; -- end of L_item_row.item_level = L_item_row.tran_level
  
    L_action_type      := 'A';
    L_message_type     := RMSMFM_ITEMS.UDAL_ADD;
    L_record.UDA_ID    := :new.UDA_ID;
    L_record.UDA_VALUE := :new.UDA_VALUE;
  
    L_queue_rec.message_type := RMSMFM_ITEMS.UDAL_ADD;
    L_queue_rec.item         := :new.item;
    L_queue_rec.uda_id       := :new.uda_id;
    L_queue_rec.uda_value    := :new.uda_value;
  end if; -- end of if UPDATING

  if not
      ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text, L_item_row, L_queue_rec.item) then
    raise PROGRAM_ERROR;
  end if;

  if L_item_row.item_level != L_item_row.tran_level then
    if not UDA_XML.BUILD_UDAV_MSG(L_status,
                                  L_text,
                                  L_message,
                                  L_record,
                                  L_action_type) then
      raise PROGRAM_ERROR;
    end if;
  
    if RMSMFM_ITEMS.ADDTOQ(L_text,
                           L_queue_rec,
                           NULL, ---I_sellable_ind
                           NULL) = FALSE then
      ---I_tran_level_ind
      raise PROGRAM_ERROR;
    end if;
  
    if L_status = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
    end if;
  end if; -- end of L_item_row.item_level = L_item_row.tran_level

EXCEPTION
  when OTHERS then
    API_LIBRARY.HANDLE_ERRORS(L_status,
                              L_text,
                              API_LIBRARY.FATAL_ERROR,
                              'XXADEO_EC_TABLE_UIL_AIUDR');
  
    raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);
END;

/
