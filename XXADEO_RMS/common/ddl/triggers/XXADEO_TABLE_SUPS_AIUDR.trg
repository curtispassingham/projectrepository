--------------------------------------------------------
--  DDL for Trigger XXADEO_TABLE_SUPS_AIUDR
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER XXADEO_TABLE_SUPS_AIUDR
  
AFTER INSERT OR UPDATE ON SUPS 
REFERENCING
       OLD AS old
       NEW AS new
FOR EACH ROW

DECLARE

BEGIN

if inserting then     

  INSERT INTO Xxadeo_Stg_Sups_out (  Supplier,
                                            Supp_Change,
                                            Addr_Change,
                                            Pou_Change,
                                            Cfa_Sup_Change,
                                            Pub_Status,
                                            Creation_Date,
                                            Last_Update_Date
                                ) VALUES (:new.Supplier,
                                          'Y',
                                          null,
                                          null,
                                          null,
                                          'N',
                                          sysdate,
                                          sysdate
                                        );
    else
     INSERT INTO Xxadeo_Stg_Sups_out (  Supplier,
                                            Supp_Change,
                                            Addr_Change,
                                            Pou_Change,
                                            Cfa_Sup_Change,
                                            Pub_Status,
                                            Creation_Date,
                                            Last_Update_Date
                                ) VALUES (:new.Supplier,
                                          'Y',
                                          null,
                                          null,
                                          null,
                                          'N',
                                          sysdate,
                                          sysdate
                                        );
    end if;
END;
/
ALTER TRIGGER XXADEO_TABLE_SUPS_AIUDR ENABLE;
/
