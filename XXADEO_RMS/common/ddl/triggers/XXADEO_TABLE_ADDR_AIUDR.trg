--------------------------------------------------------
--  DDL for Trigger XXADEO_TABLE_ADDR_AIUDR
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER XXADEO_TABLE_ADDR_AIUDR 

AFTER INSERT OR UPDATE ON ADDR 
REFERENCING
       OLD AS old
       NEW AS new
FOR EACH ROW

DECLARE

BEGIN
if (inserting AND :new.module = 'SUPP') then 

    INSERT INTO Xxadeo_Stg_Sups_out (Supplier,
                                   Supp_Change,
                                   Addr_Change,
                                   Pou_Change,
                                   Cfa_Sup_Change,
                                   Pub_Status,
                                   Creation_Date,
                                   Last_Update_Date
                                ) VALUES (
                                :new.key_value_1,
                                null,
                                'Y',
                                null,
                                null,
                                'N',
                                sysdate,
                                sysdate
                                        );
    
    elsif (updating AND :new.module = 'SUPP') then
    
     INSERT INTO Xxadeo_Stg_Sups_out (Supplier,
                                   Supp_Change,
                                   Addr_Change,
                                   Pou_Change,
                                   Cfa_Sup_Change,
                                   Pub_Status,
                                   Creation_Date,
                                   Last_Update_Date
                                ) VALUES (:new.key_value_1,
                                          null,
                                          'Y',
                                          null,
                                          null,
                                          'N',
                                          sysdate,
                                          sysdate
                                        );
    else
        NULL;
end if;
END;
/
ALTER TRIGGER XXADEO_TABLE_ADDR_AIUDR ENABLE;
/
