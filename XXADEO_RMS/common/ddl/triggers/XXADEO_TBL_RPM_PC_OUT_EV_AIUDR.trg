CREATE OR REPLACE TRIGGER XXADEO_TBL_RPM_PC_OUT_EV_AIUDR
AFTER INSERT OR UPDATE ON RPM_PRICE_EVENT_PAYLOAD
FOR EACH ROW
BEGIN

INSERT INTO XXADEO_STG_PC_EVT_OUT  (PRICE_EVENT_PAYLOAD_ID,
                                        RIB_ACTION_TYPE,
                                        CREATE_DATETIME,
                                        LAST_UPDATE_DATETIME
                                        ) VALUES (
                                            :new.PRICE_EVENT_PAYLOAD_ID,
                                            :new.RIB_TYPE,
                                            SYSDATE,
                                            SYSDATE
                                        );

END;
/
ALTER TRIGGER XXADEO_TBL_RPM_PC_OUT_EV_AIUDR ENABLE;
