CREATE OR REPLACE TRIGGER XXADEO_TBL_ITEM_SUPP_CFA_AIUDR
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_TBL_ITEM_SUPP_CFA_AIUDR.trg
* Description:   Trigger that will be used everytime that occur a 
*                change at item_supplier_cfa_ext table.
*				 This trigger will capture any change of CFAS when	
*				 item_level <= tran_level (Style and SKU items)
*				 

* Version:       1.0
* Author:        ORC - Oracle Retail Consulting
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
  AFTER DELETE OR INSERT OR UPDATE ON ITEM_SUPPLIER_CFA_EXT
  FOR EACH ROW
DECLARE
  L_queue_rec    ITEM_MFQUEUE%ROWTYPE := NULL;
  L_status       VARCHAR2(1) := NULL;
  L_text         VARCHAR2(255) := NULL;

  L_item_row     ITEM_MASTER%ROWTYPE := NULL;

  PROGRAM_ERROR EXCEPTION;
BEGIN
  L_queue_rec.message_type := RMSMFM_ITEMS.ISUP_UPD;
  if deleting then
    L_queue_rec.item     := :old.item;
    L_queue_rec.supplier := :old.supplier;
       if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                         L_item_row,
                                         L_queue_rec.item) = FALSE then
        raise PROGRAM_ERROR;
      end if;


    if L_item_row.item_level <= L_item_row.tran_level then
      if RMSMFM_ITEMS.ADDTOQ(L_text,
                                    L_queue_rec,
                                    NULL, ---I_sellable_ind
                                    NULL) = FALSE then

        raise PROGRAM_ERROR;
      end if;
    end if;

  elsif INSERTING then

    L_queue_rec.item     := :new.item;
    L_queue_rec.supplier := :new.supplier;

       if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                         L_item_row,
                                         L_queue_rec.item) = FALSE then
        raise PROGRAM_ERROR;
      end if;

    end if;

    if L_item_row.item_level <= L_item_row.tran_level then
      if RMSMFM_ITEMS.ADDTOQ(L_text,
                                    L_queue_rec,
                                    NULL, ---I_sellable_ind
                                    NULL) = FALSE then

        raise PROGRAM_ERROR;
      end if;

  elsif updating then
    if :new.varchar2_1 != :old.varchar2_1 or
       :new.varchar2_2 != :old.varchar2_2 or
       :new.varchar2_3 != :old.varchar2_3 or
       :new.varchar2_4 != :old.varchar2_4 or
       :new.varchar2_5 != :old.varchar2_5 or
       :new.varchar2_6 != :old.varchar2_6 or
       :new.varchar2_7 != :old.varchar2_7 or
       :new.varchar2_8 != :old.varchar2_8 or
       :new.varchar2_9 != :old.varchar2_9 or
       :new.varchar2_10 != :old.varchar2_10 or
       :new.number_11 != :old.number_11 or :new.number_12 != :old.number_12 or
       :new.number_13 != :old.number_13 or :new.number_14 != :old.number_14 or
       :new.number_15 != :old.number_15 or :new.number_16 != :old.number_16 or
       :new.number_17 != :old.number_17 or :new.number_18 != :old.number_18 or
       :new.number_19 != :old.number_19 or :new.number_20 != :old.number_20 or
       :new.date_21 != :old.date_21 or :new.date_22 != :old.date_22 or
       :new.date_23 != :old.date_23 or :new.date_24 != :old.date_24 or
       :new.date_25 != :old.date_25 then

         L_queue_rec.item     := :new.item;
         L_queue_rec.supplier := :new.supplier;

      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                         L_item_row,
                                         L_queue_rec.item) = FALSE then
        raise PROGRAM_ERROR;
      end if;

    end if;

    if L_item_row.item_level <= L_item_row.tran_level then
      if RMSMFM_ITEMS.ADDTOQ(L_text,
                                    L_queue_rec,
                                    NULL, ---I_sellable_ind
                                    NULL) = FALSE then

        raise PROGRAM_ERROR;
      end if;
    end if;

  end if;

  ---

EXCEPTION
  when OTHERS then
    API_LIBRARY.HANDLE_ERRORS(L_status,
                              L_text,
                              API_LIBRARY.FATAL_ERROR,
                              'XXADEO_TBL_ITEM_SUPP_CFA_AIUDR');

    raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END XXADEO_TBL_ITEM_SUPP_CFA_AIUDR;
/