CREATE OR REPLACE TRIGGER XXADEO_TBL_UIL_ASSOR_AIUDR
  AFTER INSERT OR UPDATE OR DELETE ON UDA_ITEM_LOV
  FOR EACH ROW

DECLARE
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  L_status          VARCHAR2(1);
  L_item            ITEM_LOC_CFA_EXT.ITEM%TYPE;
  L_loc             ITEM_LOC_CFA_EXT.LOC%TYPE;
  L_action_type     ITEM_EXPORT_STG.ACTION_TYPE%TYPE;
  L_uda_id          UDA_ITEM_LOV.UDA_ID%TYPE;
  L_assortment_id   UDA_ITEM_LOV.UDA_ID%TYPE;
  L_bu              NUMBER;
  L_merchandise_ind VARCHAR2(1);
  PROGRAM_ERROR EXCEPTION;

  type location is record(
    loc      ITEM_LOC.LOC%TYPE,
    loc_type ITEM_LOC.LOC_TYPE%TYPE);

  cursor C_SELL_ITEM_EXIST is
    select merchandise_ind
      from item_master
     where item = L_item
       and sellable_ind = 'Y';

  cursor C_assortment_mode is
    select to_number(value_1), bu
      from xxadeo_mom_dvm
     where func_area = 'UDA'
       and parameter = 'ASSORTMENT_MODE'
       and value_1 = L_uda_id;

  cursor C_locations is
    select loc, loc_type
      from item_loc
     where loc in (select location
                     from (select store location, org_unit_id org_unit
                             from store
                           union
                           select wh location, org_unit_id org_unit
                             from wh
                           union
                           select TO_NUMBER(partner_id) location,
                                  org_unit_id org_unit
                             from partner
                            where partner_type = 'E') loc,
                          xxadeo_bu_ou
                    where loc.org_unit = ou
                      and bu = L_bu)
       and item = L_item;

BEGIN

  L_uda_id      := NVL(:new.uda_id, :old.uda_id);
  L_item        := NVL(:new.item, :old.item);
  L_action_type := DATA_EXPORT_SQL.ITLOC_UPD;

  open C_assortment_mode;
  fetch C_assortment_mode
    into L_assortment_id, L_bu;
  close C_assortment_mode;

  if L_assortment_id is not null then
    open C_SELL_ITEM_EXIST;
    fetch C_SELL_ITEM_EXIST
      into L_merchandise_ind;
    close C_SELL_ITEM_EXIST;
  
    if L_merchandise_ind is NOT NULL then
      for L_loc in C_locations loop
        if DATA_EXPORT_SQL.INS_ITEM_EXPORT_STG(L_error_message,
                                               L_action_type,
                                               L_item,
                                               NULL, -- item_parent
                                               NULL, -- item_grandparent
                                               NULL, -- item_level
                                               NULL, -- tran_level
                                               NULL, -- vat_region
                                               NULL, -- vat_code
                                               NULL, -- vat_type
                                               L_merchandise_ind,
                                               NULL, -- vat_active_date
                                               L_loc.loc,
                                               L_loc.loc_type) = FALSE then
          RAISE PROGRAM_ERROR;
        end if;
      end loop;
    
    end if;
  
  end if;

EXCEPTION
  when OTHERS then
    L_status := API_CODES.UNHANDLED_ERROR;
    API_LIBRARY.HANDLE_ERRORS(L_status,
                              L_error_message,
                              API_LIBRARY.FATAL_ERROR,
                              'XXADEO_TBL_UIL_ASSOR_AIUDR');
  
    raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/