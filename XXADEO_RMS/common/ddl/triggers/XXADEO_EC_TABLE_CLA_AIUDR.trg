--------------------------------------------------------
--  File created - Thursday-May-24-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Trigger XXADEO_EC_TABLE_CLA_AIUDR
--------------------------------------------------------

CREATE OR REPLACE TRIGGER XXADEO_EC_TABLE_CLA_AIUDR
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_EC_TABLE_CLA_AIUDR.trg
* Description:   Trigger that will be used everytime that occur a 
*                change at class table from merchandise hierarchy.
*                This custom trigger makes use of the vanilla 
*                functionallity and enhances its capability by 
*                enriching the base information with the additional 
*                information requested by ADEO.
* Version:       1.0
* Author:        ORC - Oracle Retail Consulting
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
AFTER DELETE OR INSERT OR UPDATE ON CLASS 
FOR EACH ROW
DECLARE


   L_message_type       MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
   L_class              CLASS.CLASS%TYPE;
   L_dept               CLASS.DEPT%TYPE;
   L_class_rec          CLASS%ROWTYPE;
   L_error_msg          VARCHAR2(255) := NULL;
   L_status             VARCHAR2(1);
   program_error EXCEPTION;
BEGIN

  if inserting then
    L_message_type := RMSMFM_MERCHHIER.CLS_ADD;
    L_class := :new.class;
    L_dept := :new.dept;
    L_class_rec.class := :new.class;
    L_class_rec.dept := :new.dept;
    L_class_rec.class_name := :new.class_name;
    L_class_rec.class_vat_ind := :new.class_vat_ind;
  elsif updating then
    L_message_type := RMSMFM_MERCHHIER.CLS_UPD;
    L_class := :old.class;
    L_dept := :old.dept;
    L_class_rec.class := :old.class;
    L_class_rec.dept := :old.dept;
    L_class_rec.class_name := :new.class_name;
    L_class_rec.class_vat_ind := :new.class_vat_ind;
  else -- Deleting
    L_message_type := RMSMFM_MERCHHIER.CLS_DEL;
    L_class := :old.class;
    L_dept := :old.dept;
    L_class_rec.dept := :old.dept;
    L_class_rec.class_name := :old.class_name;
    L_class_rec.class_vat_ind := :old.class_vat_ind;
  end if;

  if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                 L_message_type,
                                 null,
                                 null,
                                 null,
                                 null,
                                 L_dept,
                                 null,
                                 L_class,
                                 L_class_rec,
                                 null,
                                 null) THEN
     RAISE program_error;
  end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_EC_TABLE_CLA_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
ALTER TRIGGER XXADEO_EC_TABLE_CLA_AIUDR ENABLE;
/