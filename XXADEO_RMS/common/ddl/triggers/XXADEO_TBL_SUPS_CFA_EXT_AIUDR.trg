--------------------------------------------------------
--  DDL for Trigger XXADEO_TBL_SUPS_CFA_EXT_AIUDR
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER XXADEO_TBL_SUPS_CFA_EXT_AIUDR

AFTER INSERT OR UPDATE ON SUPS_CFA_EXT 
REFERENCING
       OLD AS old
       NEW AS new
FOR EACH ROW

DECLARE

BEGIN

if inserting then     

  INSERT INTO Xxadeo_Stg_Sups_out (  Supplier,
                                            Supp_Change,
                                            Addr_Change,
                                            Pou_Change,
                                            Cfa_Sup_Change,
                                            Pub_Status,
                                            Creation_Date,
                                            Last_Update_Date
                                ) VALUES (:new.Supplier,
                                          null,
                                          null,
                                          null,
                                          'Y',
                                          'N',
                                          sysdate,
                                          sysdate
                                        );
    else
     INSERT INTO Xxadeo_Stg_Sups_out (  Supplier,
                                            Supp_Change,
                                            Addr_Change,
                                            Pou_Change,
                                            Cfa_Sup_Change,
                                            Pub_Status,
                                            Creation_Date,
                                            Last_Update_Date
                                ) VALUES (:new.Supplier,
                                          null,
                                          null,
                                          null,
                                          'Y',
                                          'N',
                                          sysdate,
                                          sysdate
                                        );
    end if;
END;
/
ALTER TRIGGER XXADEO_TBL_SUPS_CFA_EXT_AIUDR ENABLE;
/
