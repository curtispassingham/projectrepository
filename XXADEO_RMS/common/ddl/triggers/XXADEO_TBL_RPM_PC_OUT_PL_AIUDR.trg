CREATE OR REPLACE TRIGGER XXADEO_TBL_RPM_PC_OUT_PL_AIUDR 
AFTER INSERT OR UPDATE ON RPM_PRICE_CHG_PAYLOAD
FOR EACH ROW
BEGIN

        INSERT INTO XXADEO_STG_PC_DTL_OUT  (PRICE_CHANGE_ID,
                                            ITEM,
                                            LOCATION,
                                            LOCATION_TYPE,
                                            SELLING_RETAIL,
                                            SELLING_UNIT_CHANGE_IND,
                                            SELLING_RETAIL_CURRENCY,
                                            MULTI_UNITS_CHANGE_IND,
                                            SELLING_RETAIL_UOM,
                                            MULTI_UNITS,
                                            MULTI_UNITS_RETAIL,
                                            MULTI_UNITS_UOM,
                                            MULTI_UNITS_CURRENCY,
                                            CREATE_DATETIME,
                                            LAST_UPDATE_DATETIME,
                                            EFFECTIVE_DATE,
                                            PRICE_EVENT_PAYLOAD_ID,
                                            PUB_INDICATOR,
                                            SEQ_NO
                                        ) VALUES (
                                            :new.PRICE_CHANGE_ID,
                                            :new.ITEM,
                                            :new.LOCATION,
                                            :new.LOCATION_TYPE,
                                            :new.SELLING_RETAIL,
                                            :new.SELLING_UNIT_CHANGE_IND,
                                            :new.SELLING_RETAIL_CURRENCY,
                                            :new.MULTI_UNIT_CHANGE_IND,
                                            :new.SELLING_RETAIL_UOM,
                                            :new.MULTI_UNITS,
                                            :new.MULTI_UNITS_RETAIL,
                                            :new.MULTI_UNITS_UOM,
                                            :new.MULTI_UNITS_CURRENCY,
                                            SYSDATE, --CREATION_DATE
                                            SYSDATE,
                                            :new.EFFECTIVE_DATE,
                                            :new.PRICE_EVENT_PAYLOAD_ID,
                                            'N',
                                            XXADEO_PC_OUT_SEQ.Nextval
                                        );

END;
/
ALTER TRIGGER XXADEO_TBL_RPM_PC_OUT_PL_AIUDR ENABLE;
