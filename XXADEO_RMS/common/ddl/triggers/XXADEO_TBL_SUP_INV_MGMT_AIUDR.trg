CREATE OR REPLACE TRIGGER XXADEO_TBL_SUP_INV_MGMT_AIUDR
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_TBL_SUP_INV_MGMT_AIUDR.trg
* Description:   Trigger that will be used everytime that occur a 
*                change at SUP_INV_MGMT table.
*				 This trigger will capture any change of fields purchase_type and pickup_loc,
*				 verify if this supplier is supplier site and add one new message 
*				 to addtoq
*                
*
* Version:       1.0
* Author:        ORC - Oracle Retail Consulting
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
  AFTER DELETE OR INSERT OR UPDATE OF purchase_type, pickup_loc ON SUP_INV_MGMT
  FOR EACH ROW
DECLARE

  L_message_type       VARCHAR2(50);
  L_supplier           SUPS.SUPPLIER%TYPE;
  L_error_message      VARCHAR2(4000);
  O_status             VARCHAR2(100);
  O_text               VARCHAR2(255);
  L_parent_exist       NUMBER;
  L_system_options_rec SYSTEM_OPTIONS%ROWTYPE := NULL;

BEGIN

  if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_text, L_system_options_rec) =
     FALSE then
    raise PROGRAM_ERROR;
  end if;

  if inserting then
    Select count(supplier_parent)
      into L_parent_exist
      from sups
     where supplier = :new.supplier;
  
  else
    Select count(supplier_parent)
      into L_parent_exist
      from sups
     where supplier = :old.supplier;
  end if;

  -- Do not process Vendor level (supplier parent) information if supplier sites are used
  if L_system_options_rec.supplier_sites_ind = 'Y' and L_parent_exist = 0 then
    return;
  end if;

  L_message_type := 'VendorModADEO';

  if inserting then
    L_supplier := :new.supplier;
  
  else
    L_supplier := :old.supplier;
  
  end if;

  XXADEO_RMSMFM_SUPPLIER.ADDTOQ(L_message_type,
                                L_supplier,
                                null,
                                null,
                                null,
                                null,
                                null,
                                O_status,
                                O_text);

EXCEPTION
  when OTHERS then
    if L_error_message is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'XXADEO_TBL_SUP_INV_MGMT_AIUDR',
                                            to_char(SQLCODE));
    end if;
    raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
  
END XXADEO_TBL_SUP_INV_MGMT_AIUDR;
/