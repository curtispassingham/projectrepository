
CREATE OR REPLACE TRIGGER XXADEO_TBL_ITEM_SUPP_CTY_AIUDR
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_TBL_ITEM_SUPP_CTY_AIUDR.trg
* Description:   Trigger that will be used everytime that occur a 
*                change at item_supp_country table.
*				 This trigger will capture any change for items when	
*				 item_level < tran_level (Style items)
*				 

* Version:       1.0
* Author:        ORC - Oracle Retail Consulting
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
  AFTER DELETE OR INSERT OR UPDATE OF PRIMARY_SUPP_IND, PRIMARY_COUNTRY_IND, UNIT_COST, LEAD_TIME, PICKUP_LEAD_TIME, SUPP_PACK_SIZE, INNER_PACK_SIZE, ROUND_LVL, MIN_ORDER_QTY, MAX_ORDER_QTY, PACKING_METHOD, DEFAULT_UOP, TI, HI, COST_UOM, TOLERANCE_TYPE, MAX_TOLERANCE, MIN_TOLERANCE, supp_hier_type_1, supp_hier_lvl_1, supp_hier_type_2, supp_hier_lvl_2, supp_hier_type_3, supp_hier_lvl_3 ON ITEM_SUPP_COUNTRY
  FOR EACH ROW
DECLARE

  L_queue_rec ITEM_MFQUEUE%ROWTYPE := NULL;
  L_status    VARCHAR2(1) := NULL;
  L_text      VARCHAR2(255) := NULL;
  L_item_row  ITEM_MASTER%ROWTYPE := NULL;

  PROGRAM_ERROR EXCEPTION;
BEGIN

  if DELETING then
    /* For deletes, the row identifier will be published. */

    L_queue_rec.message_type := RMSMFM_ITEMS.ISC_DEL;

    L_queue_rec.item       := :old.item;
    L_queue_rec.supplier   := :old.supplier;
    L_queue_rec.country_id := :old.origin_country_id;

    if not
        ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text, L_item_row, L_queue_rec.item) then
      raise PROGRAM_ERROR;
    end if;

    if L_item_row.item_level = L_item_row.tran_level then
      if RMSMFM_ITEMS.ADDTOQ(L_text,
                             L_queue_rec,
                             NULL, ---I_sellable_ind
                             NULL) = FALSE then
        ---I_tran_level_ind
        raise PROGRAM_ERROR;
      end if;
    end if;
  else
    if INSERTING then
      L_queue_rec.message_type := RMSMFM_ITEMS.ISC_ADD;
      L_queue_rec.item         := :new.item;
      L_queue_rec.supplier     := :new.supplier;
      L_queue_rec.country_id   := :new.origin_country_id;

      if not ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                             L_item_row,
                                             L_queue_rec.item) then
        raise PROGRAM_ERROR;
      end if;

      if L_item_row.item_level < L_item_row.tran_level then
        if RMSMFM_ITEMS.ADDTOQ(L_text,
                               L_queue_rec,
                               NULL, ---I_sellable_ind
                               NULL) = FALSE then
          ---I_tran_level_ind
          raise PROGRAM_ERROR;
        end if;
      end if;

    elsif UPDATING then
      if :new.primary_supp_ind != :old.primary_supp_ind or
         :new.primary_country_ind != :old.primary_country_ind or
         :new.unit_cost != :old.unit_cost or
         :new.LEAD_TIME != :old.LEAD_TIME or
         :new.PICKUP_LEAD_TIME != :old.PICKUP_LEAD_TIME or
         :new.SUPP_PACK_SIZE != :old.SUPP_PACK_SIZE or
         :new.INNER_PACK_SIZE != :old.INNER_PACK_SIZE or
         :new.ROUND_LVL != :old.ROUND_LVL or
         :new.MIN_ORDER_QTY != :old.MIN_ORDER_QTY or
         :new.MAX_ORDER_QTY != :old.MAX_ORDER_QTY or
         :new.PACKING_METHOD != :old.PACKING_METHOD or
         :new.DEFAULT_UOP != :old.DEFAULT_UOP or :new.TI != :old.TI or
         :new.HI != :old.HI or :new.COST_UOM != :old.COST_UOM or
         :new.TOLERANCE_TYPE != :old.TOLERANCE_TYPE or
         :new.MAX_TOLERANCE != :old.MAX_TOLERANCE or
         :new.MIN_TOLERANCE != :old.MIN_TOLERANCE or
         :new.supp_hier_type_1 != :old.supp_hier_type_1 or
         :new.supp_hier_lvl_1 != :old.supp_hier_lvl_1 or
         :new.supp_hier_type_2 != :old.supp_hier_type_2 or
         :new.supp_hier_lvl_2 != :old.supp_hier_lvl_2 or
         :new.supp_hier_type_3 != :old.supp_hier_type_3 or
         :new.supp_hier_lvl_3 != :old.supp_hier_lvl_3 then

        L_queue_rec.message_type := RMSMFM_ITEMS.ISC_UPD;
        L_queue_rec.item         := :new.item;
        L_queue_rec.supplier     := :new.supplier;
        L_queue_rec.country_id   := :new.origin_country_id;

        if not ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                               L_item_row,
                                               L_queue_rec.item) then
          raise PROGRAM_ERROR;
        end if;

        if L_item_row.item_level = L_item_row.tran_level then
          if RMSMFM_ITEMS.ADDTOQ(L_text,
                                 L_queue_rec,
                                 NULL, ---I_sellable_ind
                                 NULL) = FALSE then
            ---I_tran_level_ind
            raise PROGRAM_ERROR;
          end if;
        end if;

      end if;
    end if;
  end if;

EXCEPTION
  when OTHERS then
    API_LIBRARY.HANDLE_ERRORS(L_status,
                              L_text,
                              API_LIBRARY.FATAL_ERROR,
                              'XXADEO_TBL_ITEM_SUPP_CTY_AIUDR');

    raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END XXADEO_TBL_ITEM_SUPP_CTY_AIUDR;
/