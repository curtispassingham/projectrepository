CREATE OR REPLACE TRIGGER XXADEO_TBL_SUPS_CFAS_EXT_AIUDR
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_TBL_SUPS_CFAS_EXT_AIUDR.trg
* Description:   Trigger that will be used everytime that occur a 
*                change at sup_cfas_ext table.
*				 This trigger will capture any change of CFAS for Supplier,
*				 verify if this supplier is supplier site and add one new message 
*				 to addtoq
*                
*
* Version:       1.0
* Author:        ORC - Oracle Retail Consulting
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
  AFTER DELETE OR INSERT OR UPDATE ON sups_cfa_ext
  REFERENCING OLD AS old NEW AS new
  FOR EACH ROW
DECLARE

  L_supplier      SUPS.SUPPLIER%TYPE;
  O_status        VARCHAR2(100);
  O_text          VARCHAR2(255);
  L_message_type  VARCHAR2(50);
  L_error_message VARCHAR2(4000);
  L_parent_exist  NUMBER;
  L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE := NULL;

BEGIN

  if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_text, L_system_options_rec) =
     FALSE then
    raise PROGRAM_ERROR;
  end if;

  if inserting then  
    Select count(supplier_parent)
      into L_parent_exist
      from sups
     where supplier = :new.supplier;
  
  else
    Select count(supplier_parent)
      into L_parent_exist
      from sups
     where supplier = :old.supplier;
  end if;

  -- Do not process Vendor level (supplier parent) information if supplier sites are used
  if L_system_options_rec.supplier_sites_ind = 'Y' and
     L_parent_exist = 0 then
    return;
  end if;

  L_message_type := 'VendorModADEO';

  if inserting then
    L_supplier := :new.supplier;
  
    xxadeo_rmsmfm_supplier.addtoq(L_message_type,
                                  L_supplier,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  O_status,
                                  O_text);
  elsif deleting then
    L_supplier := :old.supplier;
  
    xxadeo_rmsmfm_supplier.addtoq(L_message_type,
                                  L_supplier,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  O_status,
                                  O_text);
  
  elsif updating then
    L_supplier := :old.supplier;
    if :new.varchar2_1 != :old.varchar2_1 or
       :new.varchar2_2 != :old.varchar2_2 or
       :new.varchar2_3 != :old.varchar2_3 or
       :new.varchar2_4 != :old.varchar2_4 or
       :new.varchar2_5 != :old.varchar2_5 or
       :new.varchar2_6 != :old.varchar2_6 or
       :new.varchar2_7 != :old.varchar2_7 or
       :new.varchar2_8 != :old.varchar2_8 or
       :new.varchar2_9 != :old.varchar2_9 or
       :new.varchar2_10 != :old.varchar2_10 or
       :new.number_11 != :old.number_11 or :new.number_12 != :old.number_12 or
       :new.number_13 != :old.number_13 or :new.number_14 != :old.number_14 or
       :new.number_15 != :old.number_15 or :new.number_16 != :old.number_16 or
       :new.number_17 != :old.number_17 or :new.number_18 != :old.number_18 or
       :new.number_19 != :old.number_19 or :new.number_20 != :old.number_20 or
       :new.date_21 != :old.date_21 or :new.date_22 != :old.date_22 or
       :new.date_23 != :old.date_23 or :new.date_24 != :old.date_24 or
       :new.date_25 != :old.date_25 then
    
      xxadeo_rmsmfm_supplier.addtoq(L_message_type,
                                    L_supplier,
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL,
                                    O_status,
                                    O_text);
    
    end if;
  
  end if;

EXCEPTION
  when OTHERS then
    if L_error_message is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'XXADEO_TBL_SUPS_CFAS_EXT_AIUDR',
                                            to_char(SQLCODE));
    end if;
    raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
  
END;
/