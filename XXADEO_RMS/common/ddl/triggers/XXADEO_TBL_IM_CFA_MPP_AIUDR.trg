CREATE OR REPLACE TRIGGER XXADEO_TBL_IM_CFA_MPP_AIUDR
  AFTER INSERT OR UPDATE OR DELETE ON ITEM_MASTER_CFA_EXT
  FOR EACH ROW

DECLARE
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  L_status          VARCHAR2(1);
  L_item            ITEM_LOC_CFA_EXT.ITEM%TYPE;
  L_loc             ITEM_LOC_CFA_EXT.LOC%TYPE;
  L_action_type     ITEM_EXPORT_STG.ACTION_TYPE%TYPE;
  L_bu              NUMBER;
  L_merchandise_ind VARCHAR2(1);
  PROGRAM_ERROR EXCEPTION;

  type location is record(
    loc      ITEM_LOC.LOC%TYPE,
    loc_type ITEM_LOC.LOC_TYPE%TYPE);

  cursor C_SELL_ITEM_EXIST is
    select merchandise_ind
      from item_master
     where item = L_item
       and sellable_ind = 'Y';

  cursor C_bu is
    select bu
      from xxadeo_mom_dvm
     where func_area = 'CFA'
       and parameter = 'MODIFY_PRICE_POS'
       and value_1 in
           (select attrib_id from cfa_attrib where group_id = :new.group_id);

  cursor C_locations is
    select loc, loc_type
      from item_loc
     where loc in (select location
                     from (select store location, org_unit_id org_unit
                             from store
                           union
                           select wh location, org_unit_id org_unit
                             from wh
                           union
                           select TO_NUMBER(partner_id) location,
                                  org_unit_id org_unit
                             from partner
                            where partner_type = 'E') loc,
                          xxadeo_bu_ou
                    where loc.org_unit = ou
                      and bu = L_bu)
       and item = L_item;

  function validate_col(I_group_id number, I_col_name varchar2)
    return boolean is
    L_group ITEM_MASTER_CFA_EXT.GROUP_ID%TYPE;
  
    cursor C_group is
      select group_id
        from cfa_attrib
       where attrib_id in
             (select to_number(value_1)
                from xxadeo_mom_dvm
               where func_area = 'CFA'
                 and parameter = 'MODIFY_PRICE_POS')
         and group_id = I_group_id
         and storage_col_name = I_col_name;
  
  Begin
  
    open C_group;
    fetch C_group
      into L_group;
    close C_group;
  
    if L_group is not null then
      return true;
    else
      return false;
    end if;
  
  end validate_col;

BEGIN

  L_item        := NVL(:new.item, :old.item);
  L_action_type := DATA_EXPORT_SQL.ITLOC_UPD;

  open C_SELL_ITEM_EXIST;
  fetch C_SELL_ITEM_EXIST
    into L_merchandise_ind;
  close C_SELL_ITEM_EXIST;

  if L_merchandise_ind is not null then
  
    if inserting then
      if (:new.varchar2_1 is not null and
         validate_col(:new.group_id, 'VARCHAR2_1')) or
         (:new.varchar2_2 is not null and
         validate_col(:new.group_id, 'VARCHAR2_2')) or
         (:new.varchar2_3 is not null and
         validate_col(:new.group_id, 'VARCHAR2_3')) or
         (:new.varchar2_4 is not null and
         validate_col(:new.group_id, 'VARCHAR2_4')) or
         (:new.varchar2_5 is not null and
         validate_col(:new.group_id, 'VARCHAR2_5')) or
         (:new.varchar2_6 is not null and
         validate_col(:new.group_id, 'VARCHAR2_6')) or
         (:new.varchar2_7 is not null and
         validate_col(:new.group_id, 'VARCHAR2_7')) or
         (:new.varchar2_8 is not null and
         validate_col(:new.group_id, 'VARCHAR2_8')) or
         (:new.varchar2_9 is not null and
         validate_col(:new.group_id, 'VARCHAR2_9')) or
         (:new.varchar2_10 is not null and
         validate_col(:new.group_id, 'VARCHAR2_10')) or
         (:new.number_11 is not null and
         validate_col(:new.group_id, 'NUMBER_11')) or
         (:new.number_12 is not null and
         validate_col(:new.group_id, 'NUMBER_12')) or
         (:new.number_13 is not null and
         validate_col(:new.group_id, 'NUMBER_13')) or
         (:new.number_14 is not null and
         validate_col(:new.group_id, 'NUMBER_14')) or
         (:new.number_15 is not null and
         validate_col(:new.group_id, 'NUMBER_15')) or
         (:new.number_16 is not null and
         validate_col(:new.group_id, 'NUMBER_16')) or
         (:new.number_17 is not null and
         validate_col(:new.group_id, 'NUMBER_17')) or
         (:new.number_18 is not null and
         validate_col(:new.group_id, 'NUMBER_18')) or
         (:new.number_19 is not null and
         validate_col(:new.group_id, 'NUMBER_19')) or
         (:new.number_20 is not null and
         validate_col(:new.group_id, 'NUMBER_20')) or
         (:new.date_21 is not null and
         validate_col(:new.group_id, 'DATE_21')) or
         (:new.date_22 is not null and
         validate_col(:new.group_id, 'DATE_22')) or
         (:new.date_23 is not null and
         validate_col(:new.group_id, 'DATE_23')) or
         (:new.date_24 is not null and
         validate_col(:new.group_id, 'DATE_24')) or
         (:new.date_25 is not null and
         validate_col(:new.group_id, 'DATE_25')) then
      
        open C_bu;
        fetch C_bu
          into L_bu;
        close C_bu;
      
        for L_loc in C_locations loop
          if DATA_EXPORT_SQL.INS_ITEM_EXPORT_STG(L_error_message,
                                                 L_action_type,
                                                 L_item,
                                                 NULL, -- item_parent
                                                 NULL, -- item_grandparent
                                                 NULL, -- item_level
                                                 NULL, -- tran_level
                                                 NULL, -- vat_region
                                                 NULL, -- vat_code
                                                 NULL, -- vat_type
                                                 L_merchandise_ind,
                                                 NULL, -- vat_active_date
                                                 L_loc.loc,
                                                 L_loc.loc_type) = FALSE then
            RAISE PROGRAM_ERROR;
          end if;
        end loop;
      
      end if;
    
    elsif updating then
      if (:new.varchar2_1 != :old.varchar2_1 and
         validate_col(:old.group_id, 'VARCHAR2_1')) or
         (:new.varchar2_2 != :old.varchar2_2 and
         validate_col(:old.group_id, 'VARCHAR2_2')) or
         (:new.varchar2_3 != :old.varchar2_3 and
         validate_col(:old.group_id, 'VARCHAR2_3')) or
         (:new.varchar2_4 != :old.varchar2_4 and
         validate_col(:old.group_id, 'VARCHAR2_4')) or
         (:new.varchar2_5 != :old.varchar2_5 and
         validate_col(:old.group_id, 'VARCHAR2_5')) or
         (:new.varchar2_6 != :old.varchar2_6 and
         validate_col(:old.group_id, 'VARCHAR2_6')) or
         (:new.varchar2_7 != :old.varchar2_7 and
         validate_col(:old.group_id, 'VARCHAR2_7')) or
         (:new.varchar2_8 != :old.varchar2_8 and
         validate_col(:old.group_id, 'VARCHAR2_8')) or
         (:new.varchar2_9 != :old.varchar2_9 and
         validate_col(:old.group_id, 'VARCHAR2_9')) or
         (:new.varchar2_10 != :old.varchar2_10 and
         validate_col(:old.group_id, 'VARCHAR2_10')) or
         (:new.number_11 != :old.number_11 and
         validate_col(:old.group_id, 'NUMBER_11')) or
         (:new.number_12 != :old.number_12 and
         validate_col(:old.group_id, 'NUMBER_12')) or
         (:new.number_13 != :old.number_13 and
         validate_col(:old.group_id, 'NUMBER_13')) or
         (:new.number_14 != :old.number_14 and
         validate_col(:old.group_id, 'NUMBER_14')) or
         (:new.number_15 != :old.number_15 and
         validate_col(:old.group_id, 'NUMBER_15')) or
         (:new.number_16 != :old.number_16 and
         validate_col(:old.group_id, 'NUMBER_16')) or
         (:new.number_17 != :old.number_17 and
         validate_col(:old.group_id, 'NUMBER_17')) or
         (:new.number_18 != :old.number_18 and
         validate_col(:old.group_id, 'NUMBER_18')) or
         (:new.number_19 != :old.number_19 and
         validate_col(:old.group_id, 'NUMBER_19')) or
         (:new.number_20 != :old.number_20 and
         validate_col(:old.group_id, 'NUMBER_20')) or
         (:new.date_21 != :old.date_21 and
         validate_col(:old.group_id, 'DATE_21')) or
         (:new.date_22 != :old.date_22 and
         validate_col(:old.group_id, 'DATE_22')) or
         (:new.date_23 != :old.date_23 and
         validate_col(:old.group_id, 'DATE_23')) or
         (:new.date_24 != :old.date_24 and
         validate_col(:old.group_id, 'DATE_24')) or
         (:new.date_25 != :old.date_25 and
         validate_col(:old.group_id, 'DATE_25')) then
        open C_bu;
        fetch C_bu
          into L_bu;
        close C_bu;
                
        for L_loc in C_locations loop         
          if DATA_EXPORT_SQL.INS_ITEM_EXPORT_STG(L_error_message,
                                                 L_action_type,
                                                 L_item,
                                                 NULL, -- item_parent
                                                 NULL, -- item_grandparent
                                                 NULL, -- item_level
                                                 NULL, -- tran_level
                                                 NULL, -- vat_region
                                                 NULL, -- vat_code
                                                 NULL, -- vat_type
                                                 L_merchandise_ind,
                                                 NULL, -- vat_active_date
                                                 L_loc.loc,
                                                 L_loc.loc_type) = FALSE then
            RAISE PROGRAM_ERROR;
          end if;
        end loop;
      
      end if;
    end if;
  end if;

EXCEPTION
  when OTHERS then
    L_status := API_CODES.UNHANDLED_ERROR;
    API_LIBRARY.HANDLE_ERRORS(L_status,
                              L_error_message,
                              API_LIBRARY.FATAL_ERROR,
                              'XXADEO_TBL_IM_CFA_MPP_AIUDR');
  
    raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/