--------------------------------------------------------
--  File created - Thursday-May-24-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Trigger XXADEO_EC_TABLE_GRO_AIUDR
--------------------------------------------------------

CREATE OR REPLACE TRIGGER XXADEO_EC_TABLE_GRO_AIUDR
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_EC_TABLE_GRO_AIUDR.trg
* Description:   Trigger that will be used everytime that occur a 
*                change at groups table from merchandise hierarchy.
*                This custom trigger makes use of the vanilla 
*                functionallity and enhances its capability by 
*                enriching the base information with the additional 
*                information requested by ADEO.
* Version:       1.0
* Author:        ORC - Oracle Retail Consulting
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
AFTER DELETE OR INSERT OR UPDATE ON GROUPS 
 FOR EACH ROW
DECLARE

   L_message_type       MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
   L_group_no           GROUPS.GROUP_NO%TYPE;
   L_group_rec          GROUPS%ROWTYPE;
   L_error_msg          VARCHAR2(255) := NULL;
   L_status             VARCHAR2(1);
   program_error EXCEPTION;
   type classe_TBL      is table of CLASS%ROWTYPE;
   L_classes            classe_TBL;
   type subclasse_TBL      is table of SUBCLASS%ROWTYPE;
   L_subclasses            subclasse_TBL;
   type deps_TBL        is table of DEPS%ROWTYPE;
   L_deps                 deps_TBL;
   
   
   CURSOR C_get_deps IS
    SELECT * 
    from deps c
    where c.group_no = :old.group_no;
   
   CURSOR C_get_classes IS
    SELECT c.* 
    from class c, deps d
    where c.Dept = d.dept 
    and d.group_no = :old.group_no;
    
    CURSOR C_get_subclasses IS
    SELECT  s.*
    from subclass s, deps d
    where s.Dept = d.dept 
    and d.group_no = :old.group_no;

BEGIN
 
  if inserting then
    L_message_type := RMSMFM_MERCHHIER.GRP_ADD;
    L_group_no := :new.group_no;
    L_group_rec.group_no := :new.group_no;
    L_group_rec.group_name := :new.group_name;
    L_group_rec.buyer := :new.buyer;
    L_group_rec.merch := :new.merch;
    L_group_rec.division := :new.division;
  elsif updating then
    L_message_type := RMSMFM_MERCHHIER.GRP_UPD;
    L_group_no := :old.group_no;
    L_group_rec.group_no := :old.group_no;
    L_group_rec.group_name := :new.group_name;
    L_group_rec.buyer := :new.buyer;
    L_group_rec.merch := :new.merch;
    L_group_rec.division := :new.division;
  else -- Deleting
    L_message_type := RMSMFM_MERCHHIER.GRP_DEL;
    L_group_no := :old.group_no;
    L_group_rec.division := :old.division;
  end if;

  if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                 L_message_type,
                                 null,
                                 null,
                                 L_group_no,
                                 L_group_rec,
                                 null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 null) THEN
     RAISE program_error;
  end if;
  
  if updating and :new.division != :old.division then
  
  OPEN C_get_deps;
  FETCH C_get_deps BULK COLLECT INTO L_deps;
  CLOSE C_get_deps;
  
  OPEN C_get_classes;
  FETCH C_get_classes BULK COLLECT INTO L_classes;
  CLOSE C_get_classes;
  
  OPEN C_get_subclasses;
  FETCH C_get_subclasses BULK COLLECT INTO L_subclasses;
  CLOSE C_get_subclasses;
  
  -- loop through all the classes and subclasses
    for i in 1..L_deps.count loop        

        if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                 RMSMFM_MERCHHIER.DEP_UPD,
                                 :new.division,
                                 null,
                                 null,
                                 null,
                                 L_deps(i).dept,
                                 L_deps(i),
                                 null,
                                 null,
                                 null,
                                 null) THEN
     RAISE program_error;
     
    end if;    
    end loop;

  -- loop through all the classes and subclasses
    for i in 1..L_classes.count loop        

        if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                 RMSMFM_MERCHHIER.CLS_UPD,
                                 :new.division,
                                 null,
                                 :new.group_no,
                                 null,
                                 L_classes(i).dept,
                                 null,
                                 L_classes(i).class,
                                 L_classes(i),
                                 null,
                                 null) THEN
     RAISE program_error;
     
    end if;    
    end loop;
    
    for i in 1..L_subclasses.count loop        

        if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                 RMSMFM_MERCHHIER.SUB_UPD,
                                 :new.division,
                                 null,
                                 :new.group_no,
                                 null,
                                 L_subclasses(i).dept,
                                 null,
                                 L_subclasses(i).class,                             
                                 null,
                                 L_subclasses(i).subclass,
                                 L_subclasses(i)) THEN
     RAISE program_error;
     
    end if;
    end loop;

  end if;
  
EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_EC_TABLE_GRO_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
ALTER TRIGGER XXADEO_EC_TABLE_GRO_AIUDR ENABLE;
/
