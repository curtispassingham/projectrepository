/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_EC_TABLE_ISMC_AIUDR.trg
* Description:   Trigger that will be used everytime that occur a 
*                change at item_supp_manu_country table.
*				 This trigger will capture any change for items when	
*				 item_level < tran_level (Style items)
*                
*
* Version:       1.0
* Author:        ORC - Oracle Retail Consulting
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/

CREATE OR REPLACE TRIGGER XXADEO_EC_TABLE_ISMC_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON ITEM_SUPP_MANU_COUNTRY
 FOR EACH ROW
DECLARE

   L_queue_rec       ITEM_MFQUEUE%ROWTYPE := NULL;
   L_status          VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_item_row        ITEM_MASTER%ROWTYPE := NULL;

   PROGRAM_ERROR   EXCEPTION;
BEGIN

   if DELETING then
      /* For deletes, the row identifier will be published. */

      L_queue_rec.message_type := RMSMFM_ITEMS.ISMC_DEL;

      L_queue_rec.item := :old.item;
      L_queue_rec.supplier := :old.supplier;
      L_queue_rec.country_id := :old.manu_country_id;
   else
      if INSERTING then
         L_queue_rec.message_type := RMSMFM_ITEMS.ISMC_ADD;
      else
         L_queue_rec.message_type := RMSMFM_ITEMS.ISMC_UPD;
      end if;

      /* For inserts and updates, more of the values from the table
         will be published.
      */

      L_queue_rec.item := :new.item;
      L_queue_rec.supplier := :new.supplier;
      L_queue_rec.country_id := :new.manu_country_id;

   end if;

   if not ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_error_message,
                                          L_item_row,
                                          L_queue_rec.item) then
     raise PROGRAM_ERROR;
   end if;

   if L_item_row.item_level < L_item_row.tran_level then
      if RMSMFM_ITEMS.ADDTOQ(L_error_message,
                             L_queue_rec,
                             NULL,               ---I_sellable_ind
                             NULL) = FALSE then  ---I_tran_level_ind
         raise PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_EC_TABLE_ISMC_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/