create or replace TRIGGER XXADEO_TABLE_DEPS_TL_AIUDR
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_TABLE_DEPS_TL_AIUDR.trg
* Description:   Trigger that will be used everytime that occur a 
*                change at deps_tl table from merchandise hierarchy.
*                This custom trigger makes use of the vanilla 
*                functionallity and enhances its capability by 
*                enriching the base information with the additional 
*                information requested by ADEO.
* Version:       1.0
* Author:        ORC - Oracle Retail Consulting
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
AFTER DELETE OR INSERT OR UPDATE ON DEPS_TL 

REFERENCING
       OLD AS old
       NEW AS new
FOR EACH ROW

DECLARE
   L_message_type       MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
   L_dept               DEPS.DEPT%TYPE;
   L_buyer              DEPS.BUYER%TYPE;
   L_merch              DEPS.MERCH%TYPE;
   L_profit_calc_type   DEPS.PROFIT_CALC_TYPE%TYPE;
   L_purchase_type      DEPS.PURCHASE_TYPE%TYPE;
   L_group_no           DEPS.GROUP_NO%TYPE;
   L_bud_int            DEPS.BUD_INT%TYPE;
   L_bud_mkup           DEPS.BUD_MKUP%TYPE;
   L_total_market_amt   DEPS.TOTAL_MARKET_AMT%TYPE;
   L_markup_calc_type   DEPS.MARKUP_CALC_TYPE%TYPE;
   L_otb_calc_type      DEPS.OTB_CALC_TYPE%TYPE;
   L_dept_vat_incl_ind  DEPS.DEPT_VAT_INCL_IND%TYPE;
   L_dept_rec           DEPS%ROWTYPE;
   L_dept_name          DEPS.DEPT_NAME%TYPE;
   L_error_msg          VARCHAR2(255) := NULL;
   L_status             VARCHAR2(1);
   program_error EXCEPTION;
   

BEGIN
   
  if inserting then
     
     Select dept_name,
            buyer,
            merch,
            profit_calc_type,
            purchase_type,
            group_no,
            bud_int,
            bud_mkup,
            total_market_amt,
            markup_calc_type,
            otb_calc_type,
            dept_vat_incl_ind
        into L_dept_name,
             L_buyer,
             L_merch,
             L_profit_calc_type,
             L_purchase_type,
             L_group_no,
             L_bud_int,
             L_bud_mkup,
             L_total_market_amt,
             L_markup_calc_type,
             L_otb_calc_type,
             L_dept_vat_incl_ind
        from DEPS
        where DEPT = :new.dept;
     
      L_message_type := XXADEO_RMSMFM_MERCHHIER.DEP_UPD;
    L_dept := :new.dept;
    L_dept_rec.dept := :new.dept;
    L_dept_rec.dept_name := L_dept_name;
    L_dept_rec.buyer := L_buyer;
    L_dept_rec.merch := L_merch;
    L_dept_rec.profit_calc_type := L_profit_calc_type;
    L_dept_rec.purchase_type := L_purchase_type;
    L_dept_rec.group_no := L_group_no;
    L_dept_rec.bud_int := L_bud_int;
    L_dept_rec.bud_mkup := L_bud_mkup;
    L_dept_rec.total_market_amt := L_total_market_amt;
    L_dept_rec.markup_calc_type := L_markup_calc_type;
    L_dept_rec.otb_calc_type := L_otb_calc_type;
    L_dept_rec.dept_vat_incl_ind := L_dept_vat_incl_ind;
  elsif updating then 
     Select dept_name,
            buyer,
            merch,
            profit_calc_type,
            purchase_type,
            group_no,
            bud_int,
            bud_mkup,
            total_market_amt,
            markup_calc_type,
            otb_calc_type,
            dept_vat_incl_ind
        into L_dept_name,
             L_buyer,
             L_merch,
             L_profit_calc_type,
             L_purchase_type,
             L_group_no,
             L_bud_int,
             L_bud_mkup,
             L_total_market_amt,
             L_markup_calc_type,
             L_otb_calc_type,
             L_dept_vat_incl_ind
        from DEPS
        where DEPT = :old.dept;
  
    L_message_type := XXADEO_RMSMFM_MERCHHIER.DEP_UPD;
    L_dept := :old.dept;
    L_dept_rec.dept := :old.dept;
    L_dept_rec.dept_name := L_dept_name;
    L_dept_rec.buyer := L_buyer;
    L_dept_rec.merch := L_merch;
    L_dept_rec.profit_calc_type := L_profit_calc_type;
    L_dept_rec.purchase_type := L_purchase_type;
    L_dept_rec.group_no := L_group_no;
    L_dept_rec.bud_int := L_bud_int;
    L_dept_rec.bud_mkup := L_bud_mkup;
    L_dept_rec.total_market_amt := L_total_market_amt;
    L_dept_rec.markup_calc_type := L_markup_calc_type;
    L_dept_rec.otb_calc_type := L_otb_calc_type;
    L_dept_rec.dept_vat_incl_ind := L_dept_vat_incl_ind;
  else -- Deleting
     Select dept_name,
            buyer,
            merch,
            profit_calc_type,
            purchase_type,
            group_no,
            bud_int,
            bud_mkup,
            total_market_amt,
            markup_calc_type,
            otb_calc_type,
            dept_vat_incl_ind
        into L_dept_name,
             L_buyer,
             L_merch,
             L_profit_calc_type,
             L_purchase_type,
             L_group_no,
             L_bud_int,
             L_bud_mkup,
             L_total_market_amt,
             L_markup_calc_type,
             L_otb_calc_type,
             L_dept_vat_incl_ind
        from DEPS
        where DEPT = :old.dept;
  
    L_message_type := XXADEO_RMSMFM_MERCHHIER.DEP_UPD;
    L_dept := :old.dept;
    L_dept_rec.dept := :old.dept;
    L_dept_rec.dept_name := L_dept_name;
    L_dept_rec.buyer := L_buyer;
    L_dept_rec.merch := L_merch;
    L_dept_rec.profit_calc_type := L_profit_calc_type;
    L_dept_rec.purchase_type := L_purchase_type;
    L_dept_rec.group_no := L_group_no;
    L_dept_rec.bud_int := L_bud_int;
    L_dept_rec.bud_mkup := L_bud_mkup;
    L_dept_rec.total_market_amt := L_total_market_amt;
    L_dept_rec.markup_calc_type := L_markup_calc_type;
    L_dept_rec.otb_calc_type := L_otb_calc_type;
    L_dept_rec.dept_vat_incl_ind := L_dept_vat_incl_ind;

  end if;

  if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                 L_message_type,
                                 null,
                                 null,
                                 null,
                                 null,
                                 L_dept,
                                 L_dept_rec,
                                 null,
                                 null,
                                 null,
                                 null) THEN
     RAISE program_error;
  end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_TABLE_DEPS_TL_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
      
END;
/