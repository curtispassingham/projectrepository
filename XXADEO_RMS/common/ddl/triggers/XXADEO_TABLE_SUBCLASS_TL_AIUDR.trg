create or replace TRIGGER XXADEO_TABLE_SUBCLASS_TL_AIUDR
/*---------------------------------------------------------------------*/
  /*
  * Object Name:   XXADEO_TABLE_SUBCLASS_TL_AIUDR.trg
  * Description:   Trigger that will be used everytime that occur a 
  *                change at subclass_tl table from merchandise hierarchy.
  *                This custom trigger makes use of the vanilla 
  *                functionallity and enhances its capability by 
  *                enriching the base information with the additional 
  *                information requested by ADEO.
  * Version:       1.0
  * Author:        ORC - Oracle Retail Consulting
  * Creation Date: 23/05/2018
  * Last Modified: 23/05/2018
  * History:
  *               1.0 - Initial version
  */
  /*------------------------------------------------------------------------*/
  AFTER DELETE OR INSERT OR UPDATE ON SUBCLASS_TL

  REFERENCING OLD AS old NEW AS new
  FOR EACH ROW

DECLARE
  L_message_type MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
  L_dept         SUBCLASS.DEPT%TYPE;
  L_class        SUBCLASS.CLASS%TYPE;
  L_subclass     SUBCLASS.SUBCLASS%TYPE;
  L_sub_name     SUBCLASS.SUB_NAME%TYPE;
  L_subclass_rec SUBCLASS%ROWTYPE;
  L_error_msg    VARCHAR2(255) := NULL;
  L_status       VARCHAR2(1);
  program_error EXCEPTION;
BEGIN

  if inserting then
    select sub_name
      into L_sub_name
      from subclass
      where dept = :new.dept
           and class = :new.class
           and subclass =:new.subclass;
  
    L_message_type          := XXADEO_RMSMFM_MERCHHIER.SUB_UPD;
    L_dept                  := :new.dept;
    L_class                 := :new.class;
    L_subclass              := :new.subclass;
    L_subclass_rec.dept     := :new.dept;
    L_subclass_rec.class    := :new.class;
    L_subclass_rec.subclass := :new.subclass;
    L_subclass_rec.sub_name := L_sub_name;
  elsif updating then
     select sub_name
      into L_sub_name
      from subclass
      where dept = :old.dept
           and class = :old.class
           and subclass =:old.subclass;
  
    L_message_type          := XXADEO_RMSMFM_MERCHHIER.SUB_UPD;
    L_dept                  := :old.dept;
    L_class                 := :old.class;
    L_subclass              := :old.subclass;
    L_subclass_rec.dept     := :old.dept;
    L_subclass_rec.class    := :old.class;
    L_subclass_rec.subclass := :old.subclass;
    L_subclass_rec.sub_name := L_sub_name;
  else
    -- Deleting
    
     select sub_name
      into L_sub_name
      from subclass
      where dept = :old.dept
           and class = :old.class
           and subclass =:old.subclass;
    L_message_type          := XXADEO_RMSMFM_MERCHHIER.SUB_UPD;
    L_dept                  := :old.dept;
    L_class                 := :old.class;
    L_subclass              := :old.subclass;
    L_subclass_rec.dept     := :old.dept;
    L_subclass_rec.class    := :old.class;
    L_subclass_rec.subclass := :old.subclass;
    L_subclass_rec.sub_name := L_sub_name;
  end if;

  if NOT XXADEO_RMSMFM_MERCHHIER.ADDTOQ(L_error_msg,
                                        L_message_type,
                                        null,
                                        null,
                                        null,
                                        null,
                                        L_dept,
                                        null,
                                        L_class,
                                        null,
                                        L_subclass,
                                        L_subclass_rec) THEN
    RAISE program_error;
  end if;

EXCEPTION
  when OTHERS then
    L_status := API_CODES.UNHANDLED_ERROR;
    API_LIBRARY.HANDLE_ERRORS(L_status,
                              L_error_msg,
                              API_LIBRARY.FATAL_ERROR,
                              'XXADEO_TABLE_SUBCLASS_TL_AIUDR');
  
    raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/