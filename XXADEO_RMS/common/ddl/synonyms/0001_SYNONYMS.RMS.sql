set serveroutput on size unlimited
set escape on

declare

    param_1 varchar2(30):=sys.dbms_assert.schema_name(upper('&1'));
    param_2 varchar2(30):=sys.dbms_assert.schema_name(upper('&2'));
	
BEGIN
     
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.SUPS_CFA_EXT FOR '||param_2||'.SUPS_CFA_EXT';
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.ITEM_LOC_CFA_EXT FOR '||param_2||'.ITEM_LOC_CFA_EXT';

END;
/   
   