/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Synonyms                                                     */
/******************************************************************************/
set serveroutput on size unlimited
set escape on

declare
  adeo_owner varchar2(30) := sys.dbms_assert.schema_name(upper('&adeo_schema_owner'));
  rms_owner varchar2(30) := sys.dbms_assert.schema_name(upper('&rms_schema_owner'));
begin
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'.LOGGER_LOGS FOR '||adeo_owner||'.LOGGER_LOGS';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'.CUSTOM_OBJ_REC FOR '||adeo_owner||'.CUSTOM_OBJ_REC';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'.CUSTOM_PKG_CONFIG FOR '||adeo_owner||'.CUSTOM_PKG_CONFIG';
  execute immediate 'CREATE OR REPLACE SYNONYM '||rms_owner||'.SQL_LIB FOR '||adeo_owner||'.SQL_LIB';
  --
end;
/
