--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - November 2018                                                */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML to create a Filter Policy for V_ITEM_MASTER and          */
/*               drop the old.                                                */
/******************************************************************************/
--------------------------------------------------------------------------------
-- # DROP XXADEO_RMS.V_ITEM_MASTER # --
begin
  execute immediate 'drop view XXADEO_RMS.V_ITEM_MASTER';
exception
  when others then
    null;
end;
/

-- # CREATE SYNONYM # --
create or replace synonym V_ITEM_MASTER for RMS.V_ITEM_MASTER;

-- # DROP POLICY # --
begin
  DBMS_RLS.DROP_POLICY(object_schema => 'XXADEO_RMS',
                       object_name   => 'V_ITEM_MASTER',
                       policy_name   => 'XXADEO_V_ITEM_MASTER_S');
exception
  when others then
    null;
end;
/

-- # CREATE POLICY # --
begin
 DBMS_RLS.ADD_POLICY(object_schema   => 'RMS', 
                     object_name     => 'V_ITEM_MASTER', 
                     policy_name     => 'XXADEO_V_ITEM_MASTER_S', 
                     function_schema => 'XXADEO_RMS', 
                     policy_function => 'FILTER_POLICY_SQL.XXADEO_V_ITEM_MASTER_S', 
                     statement_types => 'SELECT', 
                     enable          => TRUE);
end;
/

