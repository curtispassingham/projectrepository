/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Synonyms                                                     */
/******************************************************************************/
set serveroutput on size unlimited
set escape on

declare
  adeo_owner varchar2(30) := sys.dbms_assert.schema_name(upper('&1'));
  rms_owner varchar2(30) := sys.dbms_assert.schema_name(upper('&2'));
begin
  execute immediate 'CREATE OR REPLACE SYNONYM '||adeo_owner||'.LOGGER_LOGS FOR '||rms_owner||'.LOGGER_LOGS';
  execute immediate 'CREATE OR REPLACE SYNONYM '||adeo_owner||'.CUSTOM_OBJ_REC FOR '||rms_owner||'.CUSTOM_OBJ_REC';
  execute immediate 'CREATE OR REPLACE SYNONYM '||adeo_owner||'.CUSTOM_PKG_CONFIG FOR '||rms_owner||'.CUSTOM_PKG_CONFIG';
  execute immediate 'CREATE OR REPLACE SYNONYM '||adeo_owner||'.SQL_LIB FOR '||rms_owner||'.SQL_LIB';
  execute immediate 'CREATE OR REPLACE SYNONYM '||adeo_owner||'.V_ITEM_MASTER FOR '||rms_owner||'.V_ITEM_MASTER';
  --
end;
/