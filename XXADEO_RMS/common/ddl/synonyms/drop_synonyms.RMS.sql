/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Synonyms                                                     */
/******************************************************************************/
set serveroutput on
declare
  synonym_schema      varchar2(30);
  owning_schema       varchar2(30);
  run_schema          varchar2(30);
  prefix1             varchar2(128);
  prefix2             varchar2(128);
  --
begin
  --
  synonym_schema := sys.dbms_assert.schema_name(upper('&1'));
  owning_schema  := sys.dbms_assert.schema_name(upper('&2'));
  
  prefix1:=sys.dbms_assert.enquote_name(synonym_schema,FALSE)||'.';
  prefix2:=sys.dbms_assert.enquote_name(owning_schema,FALSE)||'.';

  --
  begin
    --
    execute immediate 'drop synonym '||prefix1||'ITEM_INDUCT_SQL';
    dbms_output.put_line('Drop synonym ITEM_INDUCT_SQL '||SQLCODE||' - '||SQLERRM);
  exception
    --
    when OTHERS then
      --
      dbms_output.put_line('Drop synonym ITEM_INDUCT_SQL FAILED '||SQLCODE||' - '||SQLERRM);
      --
  END;
  --
  begin
    --
	execute immediate 'drop synonym '||prefix1||'FILTER_POLICY_SQL';
    dbms_output.put_line('Drop synonym FILTER_POLICY_SQL '||SQLCODE||' - '||SQLERRM);
  exception
    --
    when OTHERS then
      --
      dbms_output.put_line('Drop synonym FILTER_POLICY_SQL FAILED '||SQLCODE||' - '||SQLERRM);
      --
  END;
  --
  begin
    --
	execute immediate 'drop synonym '||prefix1||'v_item_master';
    dbms_output.put_line('Drop synonym v_item_master '||SQLCODE||' - '||SQLERRM);
  exception
    --
    when OTHERS then
      --
      dbms_output.put_line('Drop synonym v_item_master FAILED '||SQLCODE||' - '||SQLERRM);
      --
  END;
  --
EXCEPTION
  WHEN OTHERS THEN
    raise;
end;