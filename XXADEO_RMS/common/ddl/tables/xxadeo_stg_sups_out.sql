--------------------------------------------------------
--  Table XXADEO_STG_SUPS_OUT
--------------------------------------------------------
CREATE TABLE XXADEO_STG_SUPS_OUT
(
	SUPPLIER VARCHAR2(10) NOT NULL, 
	SUPP_CHANGE VARCHAR2(1), 
	ADDR_CHANGE VARCHAR2(1), 
	POU_CHANGE VARCHAR2(1), 
	CFA_SUP_CHANGE VARCHAR2(1), 
	PUB_STATUS VARCHAR2(1) NOT NULL, 
	CREATION_DATE DATE NOT NULL, 
	LAST_UPDATE_DATE DATE NOT NULL, 
	ERROR_DESC VARCHAR2(200)
);

COMMENT ON COLUMN XXADEO_STG_SUPS_OUT.SUPPLIER IS 'Unique identifying number for a supplier within the system.  The user determines this number when a new supplier is first added to the system.';
COMMENT ON COLUMN XXADEO_STG_SUPS_OUT.SUPP_CHANGE IS 'Supplier Change';
COMMENT ON COLUMN XXADEO_STG_SUPS_OUT.ADDR_CHANGE IS 'Address Change';
COMMENT ON COLUMN XXADEO_STG_SUPS_OUT.POU_CHANGE IS 'Partner Org Unit Change';
COMMENT ON COLUMN XXADEO_STG_SUPS_OUT.CFA_SUP_CHANGE IS 'CFA Change';
COMMENT ON COLUMN XXADEO_STG_SUPS_OUT.PUB_STATUS IS 'Publication Status';
COMMENT ON COLUMN XXADEO_STG_SUPS_OUT.CREATION_DATE IS 'Creation Date';
COMMENT ON COLUMN XXADEO_STG_SUPS_OUT.LAST_UPDATE_DATE IS 'Last Update Date';
COMMENT ON COLUMN XXADEO_STG_SUPS_OUT.ERROR_DESC IS 'Error message detail';

--------------------------------------------------------
--  Index XXADEO_STG_SUPS_OUT_I1
--------------------------------------------------------
CREATE INDEX XXADEO_STG_SUPS_OUT_I1 ON XXADEO_STG_SUPS_OUT (SUPPLIER);