/******************************************************************************/
/* CREATE DATE - Jul 2018                                                     */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table "XXADEO_LFCY_CFG_STATUS"                              */
/******************************************************************************/

begin
  execute immediate 'drop table XXADEO_LFCY_CFG_STATUS cascade constraints';
exception
  when others then
    null;
end;
/

create table XXADEO_LFCY_CFG_STATUS
(
	LFCY_STATUS			        VARCHAR2(10) NOT NULL, -- IB-ASI-01, ...
    LFCY_STATUS_DESC            VARCHAR2(50) NOT NULL,
    LFCY_ENTITY_TYPE            VARCHAR2(2) NOT NULL, -- IB, IS
	LFCY_STATUS_SEQ		        NUMBER(3,0) NOT NULL,
    LFCY_STATUS_NEXT            VARCHAR2(10) NULL,
    LFCY_STATUS_UDACFA_VALUE    NUMBER(5,0) NOT NULL,
    LFCY_INIT_IND               VARCHAR2(1) NOT NULL,
    LAST_UPDATE_DATETIME        DATE DEFAULT sysdate NOT NULL,
	CONSTRAINT XXADEO_LFCY_CFG_ST_PK PRIMARY KEY (LFCY_STATUS)
)
/

COMMENT ON TABLE XXADEO_LFCY_CFG_STATUS                               
    IS 'Lifecycle statuses for Item/Business Units and Item/Supplier relations';
COMMENT ON COLUMN XXADEO_LFCY_CFG_STATUS.LFCY_STATUS
    IS 'LifeCycle Status code';
COMMENT ON COLUMN XXADEO_LFCY_CFG_STATUS.LFCY_STATUS_DESC
    IS 'LifeCycle Status description';
COMMENT ON COLUMN XXADEO_LFCY_CFG_STATUS.LFCY_ENTITY_TYPE
    IS 'Entity type to which status applies: IB, IS';
COMMENT ON COLUMN XXADEO_LFCY_CFG_STATUS.LFCY_STATUS_SEQ
    IS 'Sequence of status rules evaluation';
COMMENT ON COLUMN XXADEO_LFCY_CFG_STATUS.LFCY_STATUS_NEXT
    IS 'Next status code (helper)';
COMMENT ON COLUMN XXADEO_LFCY_CFG_STATUS.LFCY_STATUS_UDACFA_VALUE
    IS 'CFA or UDA value that corresponds to this status';
COMMENT ON COLUMN XXADEO_LFCY_CFG_STATUS.LFCY_INIT_IND
    IS 'Indicator if Status is initial, related to the associated entity (IS or IB): Y/N';
COMMENT ON COLUMN XXADEO_LFCY_CFG_STATUS.LAST_UPDATE_DATETIME
    IS 'Last datetime when this record was updated';
