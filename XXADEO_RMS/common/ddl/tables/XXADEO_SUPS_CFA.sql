--------------------------------------------------------
--  DDL for Table XXADEO_SUPS_CFA
--------------------------------------------------------

CREATE TABLE XXADEO_SUPS_CFA 
   (	XX_SUPPLIER_ID NUMBER(10), 
	XX_NAME VARCHAR2(30), 
	XX_VALUE VARCHAR2(250), 
	XX_VALUE_NUMBER NUMBER, 
	XX_VALUE_DATE DATE, 
	XX_BATCH_ID NUMBER(10) DEFAULT 0
   );

   COMMENT ON COLUMN XXADEO_SUPS_CFA.XX_SUPPLIER_ID IS 'Unique identifier for a supplier or supplier site within RMS';
   COMMENT ON COLUMN XXADEO_SUPS_CFA.XX_NAME IS 'Identifier of the attribute';
   COMMENT ON COLUMN XXADEO_SUPS_CFA.XX_VALUE IS 'Value of the attribute';
   COMMENT ON COLUMN XXADEO_SUPS_CFA.XX_VALUE_NUMBER IS 'Value of the attribute';
   COMMENT ON COLUMN XXADEO_SUPS_CFA.XX_VALUE_DATE IS 'Value of the attribute';
   COMMENT ON COLUMN XXADEO_SUPS_CFA.XX_BATCH_ID IS 'Batch identifier';