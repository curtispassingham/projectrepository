/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL of "XXADEO_AREA_PCS" table for RB157                     */
/******************************************************************************/
--
begin
  execute immediate 'drop table XXADEO_AREA_PCS cascade constraints';
exception
  when others then
    null;
end;
/
-- Create table
create table XXADEO_AREA_PCS
(
  item                 VARCHAR2(25) not null,
  supplier             NUMBER(10) not null,
  bu                   NUMBER(10) not null,
  pcs                  NUMBER(20,4) not null,
  circuit_principal    NUMBER(2) not null,
  pcs_source           VARCHAR2(10) not null,
  status               VARCHAR2(1) not null,
  effective_date       DATE not null,
  create_datetime      DATE default sysdate not null,
  last_update_datetime DATE default sysdate,
  last_update_id       VARCHAR2(30) default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) not null
);
-- Add comments to the table 
comment on table XXADEO_AREA_PCS
  is 'Table to be used to hold required data related to PCS at BU level.';
-- Add comments to the columns 
comment on column XXADEO_AREA_PCS.item
  is 'Unique alphanumeric value that identifies the Item.';
comment on column XXADEO_AREA_PCS.supplier
  is 'Numeric identifier of the supplier site.';
comment on column XXADEO_AREA_PCS.bu
  is 'Numeric identifier of ADEO Business Unit level, represented by ''area'' level of RMS Organization Hierarchy.';
comment on column XXADEO_AREA_PCS.pcs
  is 'Central Achat PCS amount Taxes exclusive (VAT exclusive).';
comment on column XXADEO_AREA_PCS.circuit_principal
  is 'Identifier of the Circuit Principal. If the store is sourced directly from the supplier, the Circuit Principal value is ''7''; If the store is sourced via a Warehouse, the Circuit Principal value is ''1''.';
comment on column XXADEO_AREA_PCS.effective_date
  is 'Indicate the date when the PCS become active.';
comment on column XXADEO_AREA_PCS.pcs_source
  is 'Identify the source of the PCS amounts.';
comment on column XXADEO_AREA_PCS.status
  is 'Indicate the status of the PCS records. ''A'' for active or future PCS records; ''I'' for inactive PCS records to be deleted';
comment on column XXADEO_AREA_PCS.create_datetime
  is 'Date/time stamp of when the record was created. This date/time will be used for audit purpose. This value should only be populated on insert - it should never be updated.';
comment on column XXADEO_AREA_PCS.last_update_datetime
  is 'Holds the date time stamp of the most recent update by the last_update_id. This field is required by the database.';
comment on column XXADEO_AREA_PCS.last_update_id
  is 'Holds the ID of the entity who most recently updated this record. This field is required by the database.';
-- Create/Recreate primary and foreign key constraints 
alter table XXADEO_AREA_PCS
  add constraint PK_XXADEO_AREA_PCS primary key (item, supplier, bu, effective_date);
alter table XXADEO_AREA_PCS
  add constraint FK_XXADEO_AREA_PCS_ITEM foreign key (item)
  references ITEM_MASTER (item);
alter table XXADEO_AREA_PCS
  add constraint FK_XXADEO_AREA_PCS_SUP foreign key (supplier)
  references sups (supplier);  
alter table XXADEO_AREA_PCS
  add constraint FK_XXADEO_AREA_PCS_AREA foreign key (bu)
  references area (area);    
-- Create/Recreate check constraints 
alter table XXADEO_AREA_PCS
  add constraint CHK_AREA_PCS_SOURCE
  check (PCS_SOURCE in ('CONVER','BASA','SIMTAR'));
alter table XXADEO_AREA_PCS
  add constraint CHK_AREA_PCS_STATUS
  check (STATUS IN ('I', 'A'));
alter table XXADEO_AREA_PCS
  add constraint CHK_AREA_PCS_NEGATIVE
  check (PCS >= 0);  
