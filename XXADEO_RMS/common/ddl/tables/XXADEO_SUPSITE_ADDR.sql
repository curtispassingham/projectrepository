--------------------------------------------------------
--  DDL for Table XXADEO_SUPSITE_ADDR
--------------------------------------------------------

  CREATE TABLE XXADEO_SUPSITE_ADDR 
   (	XX_SUPPLIER_SITE_ID NUMBER(10), 
	XX_ADDR_XREF_KEY VARCHAR2(32), 
	XX_ADDR_KEY NUMBER(11), 
	XX_ADDR_TYPE VARCHAR2(2), 
	XX_PRIMARY_ADDR_IND VARCHAR2(1), 
	XX_ADD_1 VARCHAR2(240), 
	XX_ADD_2 VARCHAR2(240), 
	XX_ADD_3 VARCHAR2(240), 
	XX_CITY VARCHAR2(120), 
	XX_STATE VARCHAR2(3), 
	XX_COUNTRY_ID VARCHAR2(3), 
	XX_POST VARCHAR2(30), 
	XX_CONTACT_NAME VARCHAR2(120), 
	XX_CONTACT_PHONE VARCHAR2(20), 
	XX_CONTACT_FAX VARCHAR2(20), 
	XX_CONTACT_EMAIL VARCHAR2(100), 
	XX_JURISDICTION_CODE VARCHAR2(10), 
	XX_BATCH_ID NUMBER(10) DEFAULT 0
   );

   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_SUPPLIER_SITE_ID IS 'Unique identifier for a supplier site within RMS';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_ADDR_XREF_KEY IS 'Unique identifier used by ERP Cloud';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_ADDR_KEY IS 'Unique address identifier';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_ADDR_TYPE IS 'Type for the address';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_PRIMARY_ADDR_IND IS 'Indicates if the address is the primary one for the type';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_ADD_1 IS 'The first line of the address';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_ADD_2 IS 'The second line of the address';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_ADD_3 IS 'The third line of the address';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_CITY IS 'The city associated with the address';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_STATE IS 'The state for the address';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_COUNTRY_ID IS 'The contry of the address';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_POST IS 'The post code of the address';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_CONTACT_NAME IS 'The contact''s name for the supplier at this address';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_CONTACT_PHONE IS 'The phone number at this address';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_CONTACT_FAX IS 'The fax number at this address';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_CONTACT_EMAIL IS 'The email address of the representative contact';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_JURISDICTION_CODE IS 'ID associated to the tax jurisdiction of the country';
   COMMENT ON COLUMN XXADEO_SUPSITE_ADDR.XX_BATCH_ID IS 'Batch identifier';