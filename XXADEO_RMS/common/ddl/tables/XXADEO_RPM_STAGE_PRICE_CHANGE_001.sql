--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - November 2018                                                */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL to change comments of XXADEO_RPM_STAGE_PRICE_CHANGE      */
/*               table (include 'R'ejected, 'A'pproved, 'W'orksheet           */
/*               and 'F'ailed status)                                         */
/******************************************************************************/
--------------------------------------------------------------------------------
/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-11-13 - Elsa Barros - change comments of XXADEO_RPM_STAGE_PRICE_CHANGE*/ 
/*                            table (change sequence name in                  */
/*                            stage_price_change_id and xxadeo_process_is     */
/*                            column desciption                               */
/******************************************************************************/
comment on column XXADEO_RPM_STAGE_PRICE_CHANGE.STATUS
  is 'The status of the staging table records during the price event injector process.  valid values :  n  = new   u = update   d = delete   e = error   r = rejected   p = processed   w = worksheet   a = approved   f  = failed with unexpected error during conflict check';
-- 
comment on column XXADEO_RPM_STAGE_PRICE_CHANGE.xxadeo_process_id
  is 'Identifier of the XXADEO Process. This should be generated using the xxadeo_price_process_id_seq sequence or timestamp in YYYYMMDDHH24MISS format(by ESB).';
--
comment on column XXADEO_RPM_STAGE_PRICE_CHANGE.stage_price_change_id
  is 'Unique indentifier to identify the record in this table.  This should be generated using the RPM_STAGE_PRICE_CHANGE_seq sequence.';
