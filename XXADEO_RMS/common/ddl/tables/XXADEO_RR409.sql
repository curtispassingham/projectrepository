--------------------------------------------------------
--  Table XXADEO_RR409
-- edit : enclose drop into plql block
--------------------------------------------------------
begin
 execute immediate 'drop table XXADEO_RMS.XXADEO_RR409 cascade constraints';
exception
 when others then
   null;
end;
/


CREATE TABLE XXADEO_RMS.XXADEO_RR409 (user_id         VARCHAR2(30),
                                      type_entite     VARCHAR2(40),
                                      type_event      VARCHAR2(40),
                                      supplier        NUMBER (10, 0),
                                      sup_name        VARCHAR2(240),
                                      site_supplier   NUMBER,
                                      site_sup_name   VARCHAR2(240),
                                      info_modif      VARCHAR2(40),
                                      old_value       VARCHAR2(240),
                                      new_value       VARCHAR2(240),
                                      notification    VARCHAR2(2000),
                                      user_modif      VARCHAR2(240),
                                      date_modif      DATE
                                     )                           
/