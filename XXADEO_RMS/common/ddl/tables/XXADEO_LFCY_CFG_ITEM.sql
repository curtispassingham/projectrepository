
/******************************************************************************/
/* CREATE DATE - Jul 2018                                                     */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table "XXADEO_LFCY_CFG_ITEM"                              */
/******************************************************************************/

begin
  execute immediate 'drop table XXADEO_LFCY_CFG_ITEM cascade constraints';
exception
  when others then
    null;
end;
/

create table XXADEO_LFCY_CFG_ITEM
(
	LFCY_CFG_ITEM_ID		NUMBER(6,0) NOT NULL, 
    LFCY_COND_ID            VARCHAR2(12) NOT NULL,
    PACK_IND                VARCHAR2(1) NULL,
    ORDERABLE_IND           VARCHAR2(1) NULL,
    INVENTORY_IND           VARCHAR2(1) NULL,
    SELLABLE_IND            VARCHAR2(1) NULL,
    FORECAST_IND            VARCHAR2(1) NULL,
    ITEM_TYPE               VARCHAR2(10) NULL,
    ITEM_SUBTYPE            VARCHAR2(10) NULL,
    ACTIVE                  VARCHAR2(1) DEFAULT 'Y' NOT NULL,
    LAST_UPDATE_DATETIME    DATE DEFAULT sysdate NOT NULL,
	CONSTRAINT 			    XXADEO_LFCY_CFG_ITEM_PK PRIMARY KEY (LFCY_CFG_ITEM_ID)
)
/

COMMENT ON TABLE XXADEO_LFCY_CFG_ITEM                               
    IS 'Defines configuration of conditions applicability to ITEMS';
COMMENT ON COLUMN XXADEO_LFCY_CFG_ITEM.LFCY_CFG_ITEM_ID
    IS 'Surrogate primary key';
COMMENT ON COLUMN XXADEO_LFCY_CFG_ITEM.LFCY_COND_ID
    IS 'Condition to apply configuration';
COMMENT ON COLUMN XXADEO_LFCY_CFG_ITEM.PACK_IND
    IS 'ITEM_MASTER.PACK_IND';
COMMENT ON COLUMN XXADEO_LFCY_CFG_ITEM.ORDERABLE_IND
    IS 'ITEM_MASTER.ORDERABLE_IND';
COMMENT ON COLUMN XXADEO_LFCY_CFG_ITEM.INVENTORY_IND
    IS 'ITEM_MASTER.INVENTORY_IND';
COMMENT ON COLUMN XXADEO_LFCY_CFG_ITEM.SELLABLE_IND
    IS 'ITEM_MASTER.SELLABLE_IND';
COMMENT ON COLUMN XXADEO_LFCY_CFG_ITEM.FORECAST_IND
    IS 'ITEM_MASTER.FORECAST_IND';
COMMENT ON COLUMN XXADEO_LFCY_CFG_ITEM.ITEM_TYPE
    IS 'ADEO Item type value (UDA)';
COMMENT ON COLUMN XXADEO_LFCY_CFG_ITEM.ITEM_SUBTYPE
    IS 'ADEO Item sub-type value (UDA)';
COMMENT ON COLUMN XXADEO_LFCY_CFG_ITEM.ACTIVE
    IS 'Defines if filter is active';
COMMENT ON COLUMN XXADEO_LFCY_CFG_ITEM.LAST_UPDATE_DATETIME
    IS 'Last datetime when this record was updated';
