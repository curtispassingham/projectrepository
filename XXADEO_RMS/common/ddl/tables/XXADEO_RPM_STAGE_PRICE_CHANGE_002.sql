--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - November 2018                                                */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL to change comments of XXADEO_RPM_STAGE_PRICE_CHANGE      */
/*               table (include 'R'ejected status)                            */
/******************************************************************************/
--------------------------------------------------------------------------------

comment on column XXADEO_RPM_STAGE_PRICE_CHANGE.STATUS
  is 'the status of the staging table records during the price event injector process.  valid values :  n  = new   e = error   u = update   d = delete   r = rejected   p = processed with unexpected error during conflict check';
