-- This script will remove obsolete tables for Item Assortment integration
drop table XXADEO_STG_ASSORT_ITEMBU_ONOF;
drop table XXADEO_STG_ASSORT_ITEMBU_RS;
drop table XXADEO_STG_ASSORT_ITEMLOC;
drop table XXADEO_STG_ASSORT_ITEMBU_AM;
