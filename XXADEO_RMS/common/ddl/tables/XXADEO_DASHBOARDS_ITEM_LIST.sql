
/******************************************************************************/
/* CREATE DATE - Sep 2018                                                     */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table "XXADEO_DASHBOARDS_ITEM_LIST"                          */
/******************************************************************************/

begin
  execute immediate 'drop table XXADEO_DASHBOARDS_ITEM_LIST cascade constraints';
exception
  when others then
    null;
end;
/

CREATE TABLE XXADEO_DASHBOARDS_ITEM_LIST
(
  PROCESS_ID             NUMBER NOT NULL,
  BU                     NUMBER,
  ITEM_ID                VARCHAR2(250) NOT NULL,
  CREATE_ID              VARCHAR2(30) DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NOT NULL,
  CREATE_DATETIME        DATE NOT NULL
);
--
COMMENT ON TABLE XXADEO_DASHBOARDS_ITEM_LIST IS 'Table used to store item ids to be used for static report.';
--
COMMENT ON COLUMN XXADEO_DASHBOARDS_ITEM_LIST.PROCESS_ID is 'Specifies the process id associated to the item list.';
COMMENT ON COLUMN XXADEO_DASHBOARDS_ITEM_LIST.BU is 'Identify the Business Unit.';
COMMENT ON COLUMN XXADEO_DASHBOARDS_ITEM_LIST.ITEM_ID is 'Identify the item ID.';
COMMENT ON COLUMN XXADEO_DASHBOARDS_ITEM_LIST.CREATE_ID is 'User that created the record.';
COMMENT ON COLUMN XXADEO_DASHBOARDS_ITEM_LIST.CREATE_DATETIME is 'Date when the record was created.';


