--------------------------------------------------------
--  DDL for Table XXADEO_SUPS
--------------------------------------------------------
CREATE TABLE XXADEO_SUPS
   (	XX_SUP_XREF_KEY VARCHAR2(32), 
	XX_SUPPLIER_ID NUMBER(10), 
	XX_SUPSITE_XREF_KEY VARCHAR2(32), 
	XX_SUPPLIER_SITE_ID NUMBER(10), 
	XX_SUP_NAME VARCHAR2(240), 
	XX_SUP_NAME_SECONDARY VARCHAR2(240), 
	XX_CONTACT_NAME VARCHAR2(120), 
	XX_CONTACT_PHONE VARCHAR2(20), 
	XX_CONTACT_FAX VARCHAR2(20), 
	XX_CONTACT_PAGER VARCHAR2(20), 
	XX_SUP_STATUS VARCHAR2(1), 
	XX_QC_IND VARCHAR2(1), 
	XX_QC_PCT NUMBER(12,4), 
	XX_QC_FREQ NUMBER(2), 
	XX_VC_IND VARCHAR2(1), 
	XX_VC_PCT NUMBER(12,4), 
	XX_VC_FREQ NUMBER(2), 
	XX_CURRENCY_CODE VARCHAR2(3), 
	XX_LANG NUMBER(6), 
	XX_TERMS VARCHAR2(15), 
	XX_FREIGHT_TERMS VARCHAR2(30), 
	XX_RET_ALLOW_IND VARCHAR2(1), 
	XX_RET_AUTH_REQ VARCHAR2(1), 
	XX_RET_MIN_DOL_AMT NUMBER(20,4), 
	XX_RET_COURIER VARCHAR2(250), 
	XX_HANDLING_PCT NUMBER(12,4), 
	XX_EDI_PO_IND VARCHAR2(1), 
	XX_EDI_PO_CHG VARCHAR2(1), 
	XX_EDI_PO_CONFIRM VARCHAR2(1), 
	XX_EDI_ASN VARCHAR2(1), 
	XX_EDI_SALES_RPT_FREQ VARCHAR2(1), 
	XX_EDI_SUPP_AVAIL_IND VARCHAR2(1), 
	XX_EDI_CONTRACT_IND VARCHAR2(1), 
	XX_EDI_INVC_IND VARCHAR2(1), 
	XX_EDI_CHANNEL_ID NUMBER(4), 
	XX_COST_CHG_PCT_VAR NUMBER(12,4), 
	XX_COST_CHG_AMT_VAR NUMBER(20,4), 
	XX_REPLEN_APPROVAL_IND VARCHAR2(1), 
	XX_SHIP_METHOD VARCHAR2(6), 
	XX_PAYMENT_METHOD VARCHAR2(6), 
	XX_CONTACT_TELEX VARCHAR2(20), 
	XX_CONTACT_EMAIL VARCHAR2(100), 
	XX_SETTLEMENT_CODE VARCHAR2(1), 
	XX_PRE_MARK_IND VARCHAR2(1), 
	XX_AUTO_APPR_INVC_IND VARCHAR2(1), 
	XX_DBT_MEMO_CODE VARCHAR2(1), 
	XX_FREIGHT_CHARGE_IND VARCHAR2(1), 
	XX_AUTO_APPR_MEMO_IND VARCHAR2(1), 
	XX_PREPAY_INVC_IND VARCHAR2(1), 
	XX_BACKORDER_IND VARCHAR2(1), 
	XX_VAT_REGION NUMBER(4), 
	XX_INV_MGMT_LVL VARCHAR2(6), 
	XX_SERVICE_PERF_REQ_IND VARCHAR2(1), 
	XX_INVC_PAY_LOC VARCHAR2(6), 
	XX_INVC_RECEIVE_LOC VARCHAR2(6), 
	XX_ADDINVC_GROSS_NET VARCHAR2(6), 
	XX_DELIVERY_POLICY VARCHAR2(6), 
	XX_COMMENT_DESC VARCHAR2(2000), 
	XX_DEF_ITEM_LEAD_TIME NUMBER(4), 
	XX_DUNS_NUMBER VARCHAR2(9), 
	XX_DUNS_LOC VARCHAR2(4), 
	XX_BRACKET_COSTING_IND VARCHAR2(1), 
	XX_VMI_ORDER_STATUS VARCHAR2(6), 
	XX_DSD_IND VARCHAR2(1), 
	XX_SCALE_AIP_ORDERS VARCHAR2(1), 
	XX_SUP_QTY_LEVEL VARCHAR2(6), 
	XX_SUPPLIER_PARENT_ID NUMBER(10), 
	XX_RECORD_STATUS VARCHAR2(1), 
	XX_PROCESSING_TS DATE, 
	XX_ERROR_MSG VARCHAR2(1000), 
	XX_LAST_UPDATE_TS TIMESTAMP (6), 
	XX_BATCH_ID NUMBER(10) DEFAULT 0
   );

   COMMENT ON COLUMN XXADEO_SUPS.XX_SUP_XREF_KEY IS 'ID for the supplier used in ERP Cloud';
   COMMENT ON COLUMN XXADEO_SUPS.XX_SUPPLIER_ID IS 'Unique identifier for a supplier within RMS';
   COMMENT ON COLUMN XXADEO_SUPS.XX_SUPSITE_XREF_KEY IS 'ID for the supplier site used in ERP Cloud';
   COMMENT ON COLUMN XXADEO_SUPS.XX_SUPPLIER_SITE_ID IS 'Unique identifier for a supplier site within RMS';
   COMMENT ON COLUMN XXADEO_SUPS.XX_SUP_NAME IS 'Supplier''s trading name';
   COMMENT ON COLUMN XXADEO_SUPS.XX_SUP_NAME_SECONDARY IS 'Supplier''s secondary name';
   COMMENT ON COLUMN XXADEO_SUPS.XX_CONTACT_NAME IS 'Supplier''s representative contact';
   COMMENT ON COLUMN XXADEO_SUPS.XX_CONTACT_PHONE IS 'Phone number for the representative contact';
   COMMENT ON COLUMN XXADEO_SUPS.XX_CONTACT_FAX IS 'Fax number for the representative contact';
   COMMENT ON COLUMN XXADEO_SUPS.XX_CONTACT_PAGER IS 'Pager for the representative contact';
   COMMENT ON COLUMN XXADEO_SUPS.XX_SUP_STATUS IS 'Supplier status';
   COMMENT ON COLUMN XXADEO_SUPS.XX_QC_IND IS 'Quality control indicator';
   COMMENT ON COLUMN XXADEO_SUPS.XX_QC_PCT IS 'Quality control percentage';
   COMMENT ON COLUMN XXADEO_SUPS.XX_QC_FREQ IS 'Quality checking frequency';
   COMMENT ON COLUMN XXADEO_SUPS.XX_VC_IND IS 'Vendor control indicator';
   COMMENT ON COLUMN XXADEO_SUPS.XX_VC_PCT IS 'Vendor control percentage';
   COMMENT ON COLUMN XXADEO_SUPS.XX_VC_FREQ IS 'Vendor checking frequency';
   COMMENT ON COLUMN XXADEO_SUPS.XX_CURRENCY_CODE IS 'Code for the supplier''s currency';
   COMMENT ON COLUMN XXADEO_SUPS.XX_LANG IS 'Supplier''s preferred language';
   COMMENT ON COLUMN XXADEO_SUPS.XX_TERMS IS 'Sales terms to default to orders of that supplier';
   COMMENT ON COLUMN XXADEO_SUPS.XX_FREIGHT_TERMS IS 'Freight terms to default to orders of that supplier';
   COMMENT ON COLUMN XXADEO_SUPS.XX_RET_ALLOW_IND IS 'Returns indicator';
   COMMENT ON COLUMN XXADEO_SUPS.XX_RET_AUTH_REQ IS 'Indicates need of an authorization number for returns';
   COMMENT ON COLUMN XXADEO_SUPS.XX_RET_MIN_DOL_AMT IS 'Minimum $ amount to accept returns';
   COMMENT ON COLUMN XXADEO_SUPS.XX_RET_COURIER IS 'Name of courier for returns';
   COMMENT ON COLUMN XXADEO_SUPS.XX_HANDLING_PCT IS 'Handling cost for return';
   COMMENT ON COLUMN XXADEO_SUPS.XX_EDI_PO_IND IS 'EDI indicator for sending PO to supplier';
   COMMENT ON COLUMN XXADEO_SUPS.XX_EDI_PO_CHG IS 'EDI indicator for sending PO changes to supplier`';
   COMMENT ON COLUMN XXADEO_SUPS.XX_EDI_PO_CONFIRM IS 'EDI indicator for supplier acknowledgment';
   COMMENT ON COLUMN XXADEO_SUPS.XX_EDI_ASN IS 'ASN indicator for supplier';
   COMMENT ON COLUMN XXADEO_SUPS.XX_EDI_SALES_RPT_FREQ IS 'EDI sales report frequency';
   COMMENT ON COLUMN XXADEO_SUPS.XX_EDI_SUPP_AVAIL_IND IS 'EDI availability indicator';
   COMMENT ON COLUMN XXADEO_SUPS.XX_EDI_CONTRACT_IND IS 'EDI indicator to send contracts to the supplier';
   COMMENT ON COLUMN XXADEO_SUPS.XX_EDI_INVC_IND IS 'EDI indicator for invoices, debit memos and credit notes';
   COMMENT ON COLUMN XXADEO_SUPS.XX_EDI_CHANNEL_ID IS 'Channel ID';
   COMMENT ON COLUMN XXADEO_SUPS.XX_COST_CHG_PCT_VAR IS 'Cost change variance by percent';
   COMMENT ON COLUMN XXADEO_SUPS.XX_COST_CHG_AMT_VAR IS 'Cost change variance by amount';
   COMMENT ON COLUMN XXADEO_SUPS.XX_REPLEN_APPROVAL_IND IS 'Indicator to create orders in Approved status';
   COMMENT ON COLUMN XXADEO_SUPS.XX_SHIP_METHOD IS 'The method used to ship the order';
   COMMENT ON COLUMN XXADEO_SUPS.XX_PAYMENT_METHOD IS 'The method of payment';
   COMMENT ON COLUMN XXADEO_SUPS.XX_CONTACT_TELEX IS 'Telex number of the representative contact';
   COMMENT ON COLUMN XXADEO_SUPS.XX_CONTACT_EMAIL IS 'Email address of the representative contact';
   COMMENT ON COLUMN XXADEO_SUPS.XX_SETTLEMENT_CODE IS 'The payment process method used';
   COMMENT ON COLUMN XXADEO_SUPS.XX_PRE_MARK_IND IS 'Indicator of breaking orders into separate boxes';
   COMMENT ON COLUMN XXADEO_SUPS.XX_AUTO_APPR_INVC_IND IS 'Indicator of automatically approving invoice matches for payment';
   COMMENT ON COLUMN XXADEO_SUPS.XX_DBT_MEMO_CODE IS 'Indicates when a debit memo will be sent to the supplier to resolve a discrepancy';
   COMMENT ON COLUMN XXADEO_SUPS.XX_FREIGHT_CHARGE_IND IS 'Indicator of freight costs charges';
   COMMENT ON COLUMN XXADEO_SUPS.XX_AUTO_APPR_MEMO_IND IS 'Indicates if debit memos can be auto approved on creation';
   COMMENT ON COLUMN XXADEO_SUPS.XX_PREPAY_INVC_IND IS 'Indicates if invoices are considered pre-paid';
   COMMENT ON COLUMN XXADEO_SUPS.XX_BACKORDER_IND IS 'Acceptance of back orders/partial shipments indicator';
   COMMENT ON COLUMN XXADEO_SUPS.XX_VAT_REGION IS 'Unique number for the VAT region';
   COMMENT ON COLUMN XXADEO_SUPS.XX_INV_MGMT_LVL IS 'Supplier inventory management information setup level';
   COMMENT ON COLUMN XXADEO_SUPS.XX_SERVICE_PERF_REQ_IND IS 'Supplier service performed required indicator';
   COMMENT ON COLUMN XXADEO_SUPS.XX_INVC_PAY_LOC IS 'Indicates where invoices are paid';
   COMMENT ON COLUMN XXADEO_SUPS.XX_INVC_RECEIVE_LOC IS 'Indicates where invoices are received';
   COMMENT ON COLUMN XXADEO_SUPS.XX_ADDINVC_GROSS_NET IS 'Indicates if items are listed at gross or net cost';
   COMMENT ON COLUMN XXADEO_SUPS.XX_DELIVERY_POLICY IS 'The delivery policy of the supplier';
   COMMENT ON COLUMN XXADEO_SUPS.XX_COMMENT_DESC IS 'Comments';
   COMMENT ON COLUMN XXADEO_SUPS.XX_DEF_ITEM_LEAD_TIME IS 'The default lead time for the supplier';
   COMMENT ON COLUMN XXADEO_SUPS.XX_DUNS_NUMBER IS 'Number of digits count number';
   COMMENT ON COLUMN XXADEO_SUPS.XX_DUNS_LOC IS 'The Dun and Bradstreet number';
   COMMENT ON COLUMN XXADEO_SUPS.XX_BRACKET_COSTING_IND IS 'Indicates if supplier uses bracket costing';
   COMMENT ON COLUMN XXADEO_SUPS.XX_VMI_ORDER_STATUS IS 'The status of any inbound PO created';
   COMMENT ON COLUMN XXADEO_SUPS.XX_DSD_IND IS 'Direct to store shipping indicator';
   COMMENT ON COLUMN XXADEO_SUPS.XX_SCALE_AIP_ORDERS IS 'Scaling for AIP orders';
   COMMENT ON COLUMN XXADEO_SUPS.XX_SUP_QTY_LEVEL IS 'The supplier order quantity level';
   COMMENT ON COLUMN XXADEO_SUPS.XX_SUPPLIER_PARENT_ID IS 'Unique identifier of the supplier parent for a supplier site';
   COMMENT ON COLUMN XXADEO_SUPS.XX_RECORD_STATUS IS 'Indicates the status of the record : ''N''ew , ''P''rocessed, ''E''rror';
   COMMENT ON COLUMN XXADEO_SUPS.XX_PROCESSING_TS IS 'Timestamp the record was processed';
   COMMENT ON COLUMN XXADEO_SUPS.XX_ERROR_MSG IS 'Details of the processing error if any';
   COMMENT ON COLUMN XXADEO_SUPS.XX_LAST_UPDATE_TS IS 'Timestamp of the last update of the record.';
   COMMENT ON COLUMN XXADEO_SUPS.XX_BATCH_ID IS 'Identifier of the batch the record was loaded from';
   COMMENT ON TABLE XXADEO_SUPS  IS 'This staging table will store suppliers and supplier sites created in ERP Cloud';