/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - TABLE "XXADEO_M_VIEW_REFRESH_CFG" for RR143/144              */
/******************************************************************************/

begin
  execute immediate 'drop table XXADEO_M_VIEW_REFRESH_CFG cascade constraints';
exception
  when others then
    null;
end;
/


create table XXADEO_M_VIEW_REFRESH_CFG(
  mv_scope             VARCHAR2(50) not null,
  mv_execution         VARCHAR2(50) not null,
  mv_name              VARCHAR2(50) not null,
  create_id            VARCHAR2(30) default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) not null,
  create_datetime      DATE not null,
  last_update_id       VARCHAR2(30) not null,
  last_update_datetime DATE not null
);
--
COMMENT ON TABLE XXADEO_M_VIEW_REFRESH_CFG IS 'Table used to store materialized views to be refreshed after a batch execution';
--
comment on column XXADEO_M_VIEW_REFRESH_CFG.mv_scope is 'Specifies the scope where materialized view is used';
comment on column XXADEO_M_VIEW_REFRESH_CFG.mv_execution is 'Used to identify the batch where the refresh of materialized view should be done.';
comment on column XXADEO_M_VIEW_REFRESH_CFG.mv_name is 'Materialized view name';
comment on column XXADEO_M_VIEW_REFRESH_CFG.create_id is 'User that created the record.';
comment on column XXADEO_M_VIEW_REFRESH_CFG.create_datetime is 'Date when the record was created.';
comment on column XXADEO_M_VIEW_REFRESH_CFG.last_update_id is 'User that last updated the record.';
comment on column XXADEO_M_VIEW_REFRESH_CFG.last_update_datetime is 'Date when the record was last updated.';   