/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table "XXADEO_MOM_DVM"                                       */
/******************************************************************************/
begin
  execute immediate 'drop table xxadeo_mom_dvm cascade constraints';
exception
  when others then
    null;
end;
/
create table xxadeo_mom_dvm(
  func_area            VARCHAR2(30)  NOT NULL,
  parameter            VARCHAR2(30)  NOT NULL,
  bu                   NUMBER(10),
  value_1              VARCHAR(250),
  value_2              VARCHAR(250),
  description          VARCHAR2(250),
  last_update_id       VARCHAR2(30)  NOT NULL,
  last_update_datetime DATE          NOT NULL);
--
comment on table xxadeo_mom_dvm is 'Table used to store any required cross references. The table is specified in a generic format so it’s able to accommodate multiple needs.';
--
comment on column xxadeo_mom_dvm.func_area is 'Specifies the functional area of the record. E.g. UDA, CFA';
comment on column xxadeo_mom_dvm.parameter is 'Identifies the parameter that is known by the program which needs to read/write the information.';
comment on column xxadeo_mom_dvm.bu is 'ADEO Business Unit (If the record is BU specific identifies the BU attached to it). The code used corresponds to the one present in the Organization Hierarchy – Level Area.';
comment on column xxadeo_mom_dvm.value_1 is 'Holds the first value which relates to the parameter.';
comment on column xxadeo_mom_dvm.value_2 is 'Holds the second value which relates to the parameter.';
comment on column xxadeo_mom_dvm.description is 'Description of the purposes of the record being Cross Referenced.';
comment on column xxadeo_mom_dvm.last_update_id is 'User that last updated the record.';
comment on column xxadeo_mom_dvm.last_update_datetime is 'Date when the record was last updated.';

alter table xxadeo_mom_dvm add constraint xxadeo_mom_dvm_i0 unique (func_area,parameter,value_1,value_2);