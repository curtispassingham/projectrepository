--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL of "XXADEO_SUBCLASS_STATUS_STG" table for RB115          */
/******************************************************************************/
--------------------------------------------------------------------------------
-- Add/modify columns 
alter table XXADEO_SUBCLASS_STATUS_STG modify group_id null;
alter table XXADEO_SUBCLASS_STATUS_STG modify group_name null;