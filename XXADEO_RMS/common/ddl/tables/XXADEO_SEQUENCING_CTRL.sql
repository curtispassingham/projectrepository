/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_BARCODE_SEQ.sql
* Description:   Sequence to generate Barcodes identifier
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/

  CREATE TABLE XXADEO_SEQUENCING_CTRL
   (	
    ID VARCHAR2(25 BYTE) NOT NULL , 
	BU VARCHAR2(2 BYTE) NOT NULL , 
	LAST_UPDATE DATE NOT NULL 
	);
	/