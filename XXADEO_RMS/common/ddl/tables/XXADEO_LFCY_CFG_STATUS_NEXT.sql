/******************************************************************************/
/* CREATE DATE - Oct 2018                                                     */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table "XXADEO_LFCY_CFG_STATUS_NEXT"                          */
/******************************************************************************/

begin
  execute immediate 'drop table XXADEO_LFCY_CFG_STATUS_NEXT cascade constraints';
exception
  when others then
    null;
end;
/

create table xxadeo_lfcy_cfg_status_next
(
  lfcy_status varchar2(10)
  , lfcy_status_next varchar2(10)
  , constraint xxadeo_lfcy_cfg_stats_next_pk primary key  (lfcy_status, lfcy_status_next)
  );
