/******************************************************************************/
/* CREATE DATE - MAY 2018                                                     */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table "XXADEO_ITEM_SALES_DATES"                              */
/******************************************************************************/

begin
  execute immediate 'drop table XXADEO_ITEM_SALES_DATES cascade constraints';
exception
  when others then
    null;
end;
/

create table XXADEO_ITEM_SALES_DATES
(
	ITEM						VARCHAR2(25 BYTE) NOT NULL,
	BUSINESS_UNIT_ID			NUMBER(10,0) NOT NULL,
	SALES_START_DATE			DATE NOT NULL,
	SALES_END_DATE				DATE NULL,
	RECORD_UPLOAD_DATETIME		DATE DEFAULT sysdate NOT NULL,
	READY_FOR_EXPORT			VARCHAR2(1) DEFAULT 'N' NOT NULL,
	EXTRACTED_TO_RMS_DATETIME	DATE NULL,
	ERROR_MESSAGE				VARCHAR2(500),
	LAST_UPDATE_DATETIME		DATE DEFAULT sysdate NOT NULL,
	CONSTRAINT 					XXADEO_ITEMSALESDT_pk PRIMARY KEY (ITEM,BUSINESS_UNIT_ID)
)
/

COMMENT ON TABLE XXADEO_ITEM_SALES_DATES                            
    IS 'Data for integration of SALES_START_DATE and SALES_END_DATE attributes to ADEO business units';
COMMENT ON COLUMN XXADEO_ITEM_SALES_DATES.ITEM                      
    IS 'ITEM from ITEM_MASTER to where the attributes will apply';
COMMENT ON COLUMN XXADEO_ITEM_SALES_DATES.BUSINESS_UNIT_ID          
    IS 'ADEO business unit ID';
COMMENT ON COLUMN XXADEO_ITEM_SALES_DATES.SALES_START_DATE          
    IS 'Sales start date attribute value';
COMMENT ON COLUMN XXADEO_ITEM_SALES_DATES.SALES_END_DATE            
    IS 'Sales end date attribute value';
COMMENT ON COLUMN XXADEO_ITEM_SALES_DATES.RECORD_UPLOAD_DATETIME    
    IS 'Date/time when this record was created';
COMMENT ON COLUMN XXADEO_ITEM_SALES_DATES.READY_FOR_EXPORT          
    IS 'Processing status of record: N - New, Y - Preprocessed, E - Error, S - Sent to RMS';
COMMENT ON COLUMN XXADEO_ITEM_SALES_DATES.EXTRACTED_TO_RMS_DATETIME 
    IS 'Date/time when record was integrated to RMS';
COMMENT ON COLUMN XXADEO_ITEM_SALES_DATES.ERROR_MESSAGE             
    IS 'Validation or integration error message';
COMMENT ON COLUMN XXADEO_ITEM_SALES_DATES.LAST_UPDATE_DATETIME      
    IS 'Date/time of record changes';
