/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL of "XXADEO_RPM_PRICE_ZONES_BU" table for RB34a       	  */
/******************************************************************************/
--
begin
  execute immediate 'drop table XXADEO_RPM_PRICE_ZONES_BU cascade constraints';
exception
  when others then
    null;
end;
/
-- Create table
create table XXADEO_RPM_PRICE_ZONES_BU
(
  zone                 NUMBER(10) not null,
  bu                   NUMBER(10) not null,
  ref_zone_bu          NUMBER(2) not null,
  description          VARCHAR2(250),
  last_update_id       VARCHAR2(30) not null,
  last_update_datetime DATE not null
);
-- Add comments to the table 
comment on table XXADEO_RPM_PRICE_ZONES_BU
  is 'Table used to store the relationship between ADEO RPM PRICE ZONES and BU.';
-- Add comments to the columns 
comment on column XXADEO_RPM_PRICE_ZONES_BU.zone
  is 'ADEO RPM Price Zone.';
comment on column XXADEO_RPM_PRICE_ZONES_BU.bu
  is 'ADEO Business Unit (If the record is BU specific identifies the BU attached to it). The code used corresponds to the one present in the Organization Hierarchy � Level Area.';
comment on column XXADEO_RPM_PRICE_ZONES_BU.ref_zone_bu
  is 'Indicate if the Zone is a reference zone for the BU associated. For Normal Zone the value is ''0''. For Reference Zone value is ''>=1'' (to be define).';
comment on column XXADEO_RPM_PRICE_ZONES_BU.description
  is 'Description of the purposes of the record being Cross Referenced.';
comment on column XXADEO_RPM_PRICE_ZONES_BU.last_update_id
  is 'User that last updated the record.';
comment on column XXADEO_RPM_PRICE_ZONES_BU.last_update_datetime
  is 'Date when the record was last updated.';
-- Create/Recreate primary, unique and foreign key constraints 
alter table XXADEO_RPM_PRICE_ZONES_BU
  add constraint PK_RPM_PRICE_ZONES_BU primary key (ZONE);
