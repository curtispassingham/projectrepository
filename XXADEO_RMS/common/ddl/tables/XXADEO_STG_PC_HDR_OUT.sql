--------------------------------------------------------
--  Table XXADEO_STG_PC_HDR_OUT
--------------------------------------------------------
DECLARE

  L_exist NUMBER;

BEGIN

  select count(*)
    into L_exist
    from all_objects
   where object_type in ('TABLE')
     and object_name = 'XXADEO_STG_PC_HDR_OUT';

  if L_exist = 0 then
  
    execute immediate '  CREATE TABLE XXADEO_STG_PC_HDR_OUT(PRICE_CHANGE_ID NUMBER(15) NOT NULL,
                                       ITEM VARCHAR2(25) NOT NULL,
                                       ZONE NUMBER(10),
                                       LOCATION NUMBER(10),
                                       CREATE_DATETIME DATE NOT NULL,
                                       LAST_UPDATE_DATETIME DATE NOT NULL,
                                       EFFECTIVE_DATE DATE NOT NULL,
                                       REASON_CODE VARCHAR2(6) NOT NULL,
                                       CHANGE_AMOUNT NUMBER(20, 4),
                                       CHANGE_CURRENCY VARCHAR2(3),
                                       MULTI_UNITS NUMBER(12),
                                       MULTI_UNIT_RETAIL NUMBER(20),
                                       MULTI_UNIT_SELLING_UOM VARCHAR2(4),
                                       MULTI_UNIT_RETAIL_CURRENCY
                                       VARCHAR2(3),
                                       EVENT_TYPE VARCHAR2(3),
                                       STATUS VARCHAR2(1) NOT NULL,
                                       PUB_INDICATOR VARCHAR2(1) NOT NULL,
                                       ERROR_DETAIL VARCHAR2(20),
                                       PRICE_TYPE VARCHAR2(3),
                                       ZONE_PRICE_TYPE VARCHAR2(6),
                                       ITEM_TYPE NUMBER(5),
                                       SEQ_NO NUMBER(20) NOT NULL);
  
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.PRICE_CHANGE_ID IS ''Price Change identifier'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.ITEM IS ''item the price change is being applied to.  can be at the transaction level or one level above the transaction level.'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.ZONE IS ''price changes are optionally setup at either the zone level or at the location level.  when a price change is at the zone level, this field will be populated.'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.LOCATION IS ''price changes are optionally setup at either the zone level or at the location level.  when a price change is at the location level, this field will be populated.'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.CREATE_DATETIME IS ''Creation Date'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.LAST_UPDATE_DATETIME IS ''Last Update Date'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.EFFECTIVE_DATE IS ''the date the price change will go into effect.'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.REASON_CODE IS ''Reason code for the price change'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.CHANGE_AMOUNT IS ''when the change type is change by amount or change to amount this field will hold the amount.'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.CHANGE_CURRENCY IS ''when the change type is change by amount or change to amount this field will hold the currency of the amount.'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.MULTI_UNITS IS ''contains the new multi units determined by the price change.'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.MULTI_UNIT_RETAIL IS ''this field contains the new multi unit retail price in the selling unit of measure determined by the price change.'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.MULTI_UNIT_SELLING_UOM IS ''this field holds the selling unit of measure for an items multi-unit retail.'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.MULTI_UNIT_RETAIL_CURRENCY IS ''this field contains the currency of the new multi unit retail price in the selling unit of measure determined by the price change.'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.EVENT_TYPE IS ''�CRE� = Create, �MOD� = Modify,  �DEL� = Delete'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.STATUS IS ''Price Change Status'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.PUB_INDICATOR IS ''Publication Status'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.ERROR_DETAIL IS ''Error message details'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.PRICE_TYPE IS ''"NAT" = National, "ZON" = Zone, "LOC" = Location'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.ZONE_PRICE_TYPE IS ''"NATZ" = National Zone, "REGZ" = Regular Zone, "WEBZ" = Web Zone'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.ITEM_TYPE IS ''Contains the unique identified of the item type attribute group'';
    COMMENT ON COLUMN XXADEO_STG_PC_HDR_OUT.SEQ_NO IS ''Sequence Number'';
  ';
  
  end if;
  
  
  select count(*)
    into L_exist
    from all_objects
   where object_type in ('INDEX')
     and object_name = 'XXADEO_STG_PC_HDR_OUT_I1';
     
     if L_exist = 1 then
       execute immediate 'DROP INDEX XXADEO_STG_PC_HDR_OUT_I1 '; 
     
     end if;
 
END;
/

ALTER TABLE XXADEO_STG_PC_HDR_OUT MODIFY ERROR_DETAIL VARCHAR2(400);


--------------------------------------------------------
--  DDL for Index XXADEO_STG_PC_HDR_OUT_I1
--------------------------------------------------------



CREATE INDEX XXADEO_STG_PC_HDR_OUT_I1 ON XXADEO_STG_PC_HDR_OUT(PRICE_CHANGE_ID,
                                                               ITEM,
                                                               LOCATION);

     
