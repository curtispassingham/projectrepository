begin
  execute immediate 'drop table XXADEO_STG_ITEMLOC_IN cascade constraints';
exception
  when others then
    null;
end;
/
-- Create table
create table XXADEO_STG_ITEMLOC_IN
(
  item                 VARCHAR2(25),
  loc                  NUMBER(10),
  loc_type             VARCHAR2(1),
  primary_sup          NUMBER(10),
  origin_country_id    VARCHAR2(3),
  src_method           VARCHAR2(1),
  source_wh            NUMBER(10),
  attrib_1             VARCHAR2(250),
  attrib_2             VARCHAR2(250),
  attrib_3             VARCHAR2(250),
  attrib_4             VARCHAR2(250),
  attrib_5             VARCHAR2(250),
  store_ord_mult       VARCHAR2(1),
  op_type              VARCHAR2(25),
  pub_status           VARCHAR2(1),
  create_datetime      DATE,
  last_update_datetime DATE,
  error_message        VARCHAR2(400),
  message_type         VARCHAR2(15)
)
;
-- Add comments to the columns 
comment on column XXADEO_STG_ITEMLOC_IN.item
  is 'Article''s unique identifier';
comment on column XXADEO_STG_ITEMLOC_IN.loc
  is 'The Location Identifier (Store)';
comment on column XXADEO_STG_ITEMLOC_IN.loc_type
  is 'Type of location in the location field.  Valid values are S (store), W (warehouse)';
comment on column XXADEO_STG_ITEMLOC_IN.primary_sup
  is 'Supplier site ''s unique identifier';
comment on column XXADEO_STG_ITEMLOC_IN.src_method
  is 'This value will be used to specify how the ad-hoc PO/TSF creation process should source the item/location request';
comment on column XXADEO_STG_ITEMLOC_IN.source_wh
  is 'This value will be used by the ad-hoc PO/Transfer creation process to determine which warehouse to fill the stores request from.';
comment on column XXADEO_STG_ITEMLOC_IN.attrib_1
  is 'Used to communicate a custom attribute 1';
comment on column XXADEO_STG_ITEMLOC_IN.attrib_2
  is 'Used to communicate a custom attribute 2';
comment on column XXADEO_STG_ITEMLOC_IN.attrib_3
  is 'Used to communicate a custom attribute 3';
comment on column XXADEO_STG_ITEMLOC_IN.attrib_4
  is 'Used to communicate a custom attribute 4';
comment on column XXADEO_STG_ITEMLOC_IN.attrib_5
  is 'Used to communicate a custom attribute 5';
comment on column XXADEO_STG_ITEMLOC_IN.op_type
  is 'This field identifies the type of information communicated:
  CORE - Process only core information, as such the Item Supplier Primary Supplier
  ATTR - Process only the custom Attributes information';
comment on column XXADEO_STG_ITEMLOC_IN.pub_status
  is 'Can be: - ''N''ew, ''R''eady, ''E''rror, ''P''rocessed ';
comment on column XXADEO_STG_ITEMLOC_IN.create_datetime
  is 'Creation date';
comment on column XXADEO_STG_ITEMLOC_IN.last_update_datetime
  is 'Last update date';
comment on column XXADEO_STG_ITEMLOC_IN.error_message
  is 'Error message';
comment on column XXADEO_STG_ITEMLOC_IN.message_type
  is 'Message type';
