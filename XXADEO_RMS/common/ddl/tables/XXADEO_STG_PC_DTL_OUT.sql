--------------------------------------------------------
--  Table XXADEO_STG_PC_DTL_OUT
--------------------------------------------------------
DECLARE

  L_exist NUMBER;

BEGIN

  select count(*)
    into L_exist
    from all_objects
   where object_type in ('TABLE')
     and object_name = 'XXADEO_STG_PC_DTL_OUT';

  if L_exist = 0 then
execute immediate '  

CREATE TABLE XXADEO_STG_PC_DTL_OUT
   (PRICE_CHANGE_ID NUMBER(15) NOT NULL, 
  ITEM VARCHAR2(25), 
  LOCATION NUMBER(10), 
  LOCATION_TYPE VARCHAR2(30), 
  SELLING_RETAIL NUMBER(20,4), 
  SELLING_UNIT_CHANGE_IND NUMBER(6), 
  SELLING_RETAIL_CURRENCY VARCHAR2(25), 
  MULTI_UNITS_CHANGE_IND NUMBER(6), 
  SELLING_RETAIL_UOM VARCHAR2(25), 
  MULTI_UNITS NUMBER(18,4), 
  MULTI_UNITS_RETAIL NUMBER(20,4), 
  MULTI_UNITS_UOM VARCHAR2(25), 
  MULTI_UNITS_CURRENCY VARCHAR2(25), 
  CREATE_DATETIME DATE NOT NULL, 
  LAST_UPDATE_DATETIME DATE NOT NULL, 
  EFFECTIVE_DATE DATE NOT NULL, 
  PRICE_EVENT_PAYLOAD_ID NUMBER(10) NOT NULL, 
  PUB_INDICATOR VARCHAR2(1) NOT NULL, 
  SEQ_NO NUMBER(30) NOT NULL
   );

   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.PRICE_CHANGE_ID IS ''primary key.'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.ITEM IS ''item the price change is being applied to.  can be at the transaction level or one level above the transaction level.'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.LOCATION IS ''price changes are optionally setup at either the zone level or at the location level.  when a price change is at the location level, this field will be populated.'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.LOCATION_TYPE IS ''location type'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.SELLING_RETAIL IS ''selling retail with price change applied'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.SELLING_UNIT_CHANGE_IND IS ''did selling unit retail change with this price event'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.SELLING_RETAIL_CURRENCY IS ''selling retail currency'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.MULTI_UNITS_CHANGE_IND IS ''did multi unit retail change with this price event'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.SELLING_RETAIL_UOM IS ''selling retail unit of measure'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.MULTI_UNITS IS ''number multi units'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.MULTI_UNITS_RETAIL IS ''multi unit retail'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.MULTI_UNITS_UOM IS ''multi unit retail unit of measure'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.MULTI_UNITS_CURRENCY IS ''multi unit retail currency'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.CREATE_DATETIME IS ''Creation Date'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.LAST_UPDATE_DATETIME IS ''Last Update Date'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.EFFECTIVE_DATE IS ''effective date of price change'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.PRICE_EVENT_PAYLOAD_ID IS ''primary key of price change price event'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.PUB_INDICATOR IS ''Publication Status'';
   COMMENT ON COLUMN XXADEO_STG_PC_DTL_OUT.SEQ_NO IS ''Sequence Number'';
';

end if;



select count(*)
    into L_exist
    from all_objects
   where object_type in ('INDEX')
     and object_name = 'XXADEO_STG_PC_DTL_OUT_I1';
     
     
     if L_exist =1 then
      execute immediate 'DROP INDEX XXADEO_STG_PC_DTL_OUT_I1';  
     
     end if;
EXCEPTION
  When Others then
    NULL;

END;

/
--------------------------------------------------------
--  DDL for Index XXADEO_STG_PC_DTL_OUT_I1
--------------------------------------------------------

 

CREATE INDEX XXADEO_STG_PC_DTL_OUT_I1 ON XXADEO_STG_PC_DTL_OUT(PRICE_CHANGE_ID,
                                                               ITEM,
                                                               LOCATION);