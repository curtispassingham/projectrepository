/******************************************************************************/
/* CREATE DATE - MAY 2018                                                     */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table "XXADEO_SYSTEM_RULES_DETAIL"                           */
/******************************************************************************/

begin
  execute immediate 'drop table XXADEO_SYSTEM_RULES_DETAIL cascade constraints';
exception
  when others then
    null;
end;
/
-- Create table
create table XXADEO_SYSTEM_RULES_DETAIL
(
  rule_id              VARCHAR2(30)  not null,
  parameter            VARCHAR2(30)  not null,
  description          VARCHAR2(120) not null,
  value_1              VARCHAR2(20)  not null,
  value_2              VARCHAR2(20),
  call_seq_no          NUMBER(3)     not null,
  last_update_id       VARCHAR2(30)  not null,
  last_update_datetime DATE DEFAULT SYSDATE
);
-- Add comments to the columns 
comment on column XXADEO_SYSTEM_RULES_DETAIL.rule_id
  is 'The unique sequence generated for the rule record.';
comment on column XXADEO_SYSTEM_RULES_DETAIL.parameter
  is 'Contains the parameter value.';
comment on column XXADEO_SYSTEM_RULES_DETAIL.description
  is 'Description of the purposes of the record being Cross Referenced.';
comment on column XXADEO_SYSTEM_RULES_DETAIL.value_1
  is 'Holds the first value which relates to the parameter.';
comment on column XXADEO_SYSTEM_RULES_DETAIL.value_2
  is 'Holds the second value which relates to the parameter.';
comment on column XXADEO_SYSTEM_RULES_DETAIL.call_seq_no
  is 'This column contains the order the rules will be executed.';
comment on column XXADEO_SYSTEM_RULES_DETAIL.last_update_id
  is 'User that last updated the record.';
comment on column XXADEO_SYSTEM_RULES_DETAIL.last_update_datetime
  is 'Date when the record was last updated.';
-- Create/Recreate primary, unique and foreign key constraints 
alter table XXADEO_SYSTEM_RULES_DETAIL
  add constraint XXADEO_SYSTEM_RULES_DETAIL_UK1 unique (RULE_ID, PARAMETER, VALUE_1, VALUE_2);
-- Create/Recreate primary, unique and foreign key constraints 
alter table XXADEO_SYSTEM_RULES_DETAIL
  add constraint FK_XXADEO_RULES_DETAIL foreign key (RULE_ID,CALL_SEQ_NO)
  references XXADEO_SYSTEM_RULES (RULE_ID,CALL_SEQ_NO);