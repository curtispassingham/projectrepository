/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_RDF_DOMAINS_DEPT_STG" table for RB115         */
/******************************************************************************/
-- Create table
create table XXADEO_RDF_DOMAINS_DEPT_STG
(
  process_id           NUMBER(20) not null,
  action         	   VARCHAR2(10) not null,
  group_id       	   NUMBER(4) not null,
  group_name           VARCHAR2(120) not null,
  dept                 NUMBER(4) not null,
  dept_name            VARCHAR2(120),
  domain_id            NUMBER(3) not null,
  load_sales_ind       VARCHAR2(1) not null,
  create_id            VARCHAR2(30) default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) not null,
  create_datetime      DATE not null,
  last_update_id       VARCHAR2(30) not null,
  last_update_datetime DATE not null,
  row_seq              NUMBER not null
);
-- Add comments to the table 
comment on table XXADEO_RDF_DOMAINS_DEPT_STG
  is 'Table to be used as a staging area while performing and processing the uploads or downloads into and from RMS.';
-- Add comments to the columns 
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.process_id
  is 'Unique identifier for the upload process.';
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.action
  is 'Type of action: Create or Update.';
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.group_id
  is 'This field contains the ID of the group.';
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.group_name
  is 'This field contains the name of the group.';
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.dept
  is 'This field contains the Department ID.';
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.dept_name
  is 'This field contains the name of the department in the language of the user if exists. If no translation, name in primary language.';
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.domain_id
  is 'This field contains the name of the RDF domain.';
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.load_sales_ind
  is 'This field indicates whether or not all the sales history for all SKUs in the department have to be interfaced to an external system.';
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.create_id
  is 'This column holds the User id of the user who created the record.';
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.create_datetime
  is 'Date/time stamp of when the record was created.  This date/time will be used in export processing.  This value should only be populated on insert  - it should never be updated.';
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.last_update_id
  is 'Holds the Oracle user-id of the user who most recently updated this record.  This field is required by the database.';
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.last_update_datetime
  is 'Holds the date time stamp of the most recent update by the last_update_id.  This field is required by the database.';
comment on column XXADEO_RDF_DOMAINS_DEPT_STG.row_seq
  is 'Unique identifier for the row.';
-- Create/Recreate primary, unique and foreign key constraints 
alter table XXADEO_RDF_DOMAINS_DEPT_STG
  add constraint PK_XXADEO_RDF_DOMAINS_DEPT_STG primary key (PROCESS_ID, ROW_SEQ);
alter table XXADEO_RDF_DOMAINS_DEPT_STG
  add constraint FK_XXADEO_RDF_DOMAINS_DEPT_STG foreign key (PROCESS_ID)
  references SVC_PROCESS_TRACKER (PROCESS_ID);
