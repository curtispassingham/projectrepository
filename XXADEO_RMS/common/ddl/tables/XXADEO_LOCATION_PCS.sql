/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL of "XXADEO_LOCATION_PCS" table for RB157                 */
/******************************************************************************/
--
begin
  execute immediate 'drop table XXADEO_LOCATION_PCS cascade constraints';
exception
  when others then
    null;
end;
/
-- Create table
create table XXADEO_LOCATION_PCS
(
  item                 VARCHAR2(25) not null,
  supplier             NUMBER(10) not null,
  store                NUMBER(10) not null,
  pcs                  NUMBER(20,4) not null,
  circuit_principal    NUMBER(2) not null,
  pcs_source           VARCHAR2(10) not null,
  status               VARCHAR2(1) not null,
  effective_date       DATE not null,
  create_datetime      DATE default sysdate not null,
  last_update_datetime DATE default sysdate,
  last_update_id       VARCHAR2(30) default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) not null
); 
-- Add comments to the table 
comment on table XXADEO_LOCATION_PCS
  is 'Table to be used to hold required data related to PCS at Store level.';
-- Add comments to the columns 
comment on column XXADEO_LOCATION_PCS.item
  is 'Unique alphanumeric value that identifies the Item.';
comment on column XXADEO_LOCATION_PCS.supplier
  is 'Numeric identifier of the supplier site.';
comment on column XXADEO_LOCATION_PCS.store
  is 'Numeric identifier of the store in which the Item is to be found.';
comment on column XXADEO_LOCATION_PCS.pcs
  is 'Store PCS amount, taxes exclusive (VAT exclusive).';
comment on column XXADEO_LOCATION_PCS.circuit_principal
  is 'Identifier of the Circuit Principal (Circuit taken for having an item reaching the store). Possible values: 1-Stock; 2-Consigned Stock; 3-Events Stock; 4-Allocated cross-docking; 5-Non Allocated cross-docking; 6-Transit; 7-Direct; 8-Delivery to customer from warehouse; 9-Delivery to customer from supplier. Note: BASA is only expected to communicate values 1 and 5.';
comment on column XXADEO_LOCATION_PCS.effective_date
  is 'Indicate the date when the PCS become active.';
comment on column XXADEO_LOCATION_PCS.pcs_source
  is 'Identify the source of the PCS amounts.';
comment on column XXADEO_LOCATION_PCS.status
  is 'Indicate the status of the PCS records. ''A'' for active or future PCS records; ''I'' for inactive PCS records to be deleted';
comment on column XXADEO_LOCATION_PCS.create_datetime
  is 'Date/time stamp of when the record was created. This date/time will be used for audit purpose. This value should only be populated on insert - it should never be updated.';
comment on column XXADEO_LOCATION_PCS.last_update_datetime
  is 'Holds the date time stamp of the most recent update by the last_update_id. This field is required by the database.';
comment on column XXADEO_LOCATION_PCS.last_update_id
  is 'Holds the ID of the entity who most recently updated this record. This field is required by the database.';
-- Create/Recreate primary and foreign key constraints 
alter table XXADEO_LOCATION_PCS
  add constraint PK_XXADEO_LOCATION_PCS primary key (ITEM, STORE, EFFECTIVE_DATE); 
alter table XXADEO_LOCATION_PCS
  add constraint FK_XXADEO_LOCATION_PCS_ITEM foreign key (item)
  references ITEM_MASTER (item);
alter table XXADEO_LOCATION_PCS
  add constraint FK_XXADEO_LOCATION_PCS_SUP foreign key (supplier)
  references sups (supplier);  
alter table XXADEO_LOCATION_PCS
  add constraint FK_XXADEO_LOCATION_PCS_STORE foreign key (store)
  references store (store);    
-- Create/Recreate check constraints 
alter table XXADEO_LOCATION_PCS
  add constraint CHK_LOCATION_PCS_SOURCE
  check (PCS_SOURCE in ('CONVER','BASA','SIMTAR'));
alter table XXADEO_LOCATION_PCS
  add constraint CHK_LOCATION_PCS_STATUS
  check (STATUS IN ('I', 'A'));
alter table XXADEO_LOCATION_PCS
  add constraint CHK_LOCATION_PCS_NEGATIVE
  check (PCS >= 0);  
