/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL of "XXADEO_PRICE_REQUEST" table for RB34a                */
/******************************************************************************/
--
begin
  execute immediate 'drop table XXADEO_PRICE_REQUEST cascade constraints';
exception
  when others then
    null;
end;
/
-- Create table
create table XXADEO_PRICE_REQUEST
(
  request_process_id  NUMBER(20) not null,
  price_change_id     NUMBER(15),
  status              VARCHAR2(5) not null,
  type                VARCHAR2(10) not null,
  action              VARCHAR2(10) not null,
  request_size        NUMBER(20) not null,
  dependency          NUMBER(20),
  dependency_type     NUMBER(6),
  request_user        VARCHAR2(40) not null,
  request_datetime    DATE not null,
  start_proc_datetime DATE,
  process_order       NUMBER(10) not null,
  pe_process_id       NUMBER(20) not null
);
-- Add comments to the columns 
comment on column XXADEO_PRICE_REQUEST.dependency_type
  is 'Indicates whether the dependecy origin is rpm or staging area. Valid values include: 0 - RPM, 1 - STAGE.';
-- Create/Recreate primary, unique and foreign key constraints 
alter table XXADEO_PRICE_REQUEST
  add constraint XXADEO_PROCESS_REQUEST_PK1 primary key (REQUEST_PROCESS_ID, PROCESS_ORDER, PE_PROCESS_ID);

