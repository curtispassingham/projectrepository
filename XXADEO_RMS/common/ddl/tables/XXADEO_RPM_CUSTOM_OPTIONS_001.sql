/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL of "XXADEO_RPM_CUSTOM_OPTIONS" table for RB34a           */
/******************************************************************************/

create table xxadeo_rpm_custom_options_bck as 
(select * 
   from xxadeo_rpm_custom_options);
   
delete from XXADEO_RPM_CUSTOM_OPTIONS;   

alter table XXADEO_RPM_CUSTOM_OPTIONS modify pc_blocking_cfa_value default 'Y';

insert into xxadeo_rpm_custom_options
  (retention_control_id,
   price_change_type,
   processed_pc_retention_days,
   error_pc_retention_days,
   pc_process_max_request_size)
  (select retention_control_id,
          price_change_type,
          processed_pc_retention_days,
          error_pc_retention_days,
          pc_process_max_request_size
     from xxadeo_rpm_custom_options_bck);
	 
drop table xxadeo_rpm_custom_options_bck;