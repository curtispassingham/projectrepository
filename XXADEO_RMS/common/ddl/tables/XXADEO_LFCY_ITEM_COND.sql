
/******************************************************************************/
/* CREATE DATE - Jul 2018                                                     */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table "XXADEO_LFCY_ITEM_COND"                              */
/******************************************************************************/

begin
  execute immediate 'drop table XXADEO_LFCY_ITEM_COND cascade constraints';
exception
  when others then
    null;
end;
/

create table XXADEO_LFCY_ITEM_COND
(
    ITEM			        VARCHAR2(25 BYTE) NOT NULL,
    LFCY_COND_ID            VARCHAR2(10) NOT NULL,
    LFCY_ENTITY             NUMBER(10,0) NOT NULL,      -- BU (area) ou supplier (sups)
    LFCY_ENTITY_TYPE        VARCHAR2(2) NOT NULL,      -- IB / IS
    VAL_STATUS		        VARCHAR2(2) NULL,   
    NEXT_VAL_DT             DATE,
    LAST_VAL_DT             DATE,
    DELETE_IND              VARCHAR2(1) NULL,
    ERROR_MESSAGE           VARCHAR2(1000) NULL,
    LAST_UPDATE_DATETIME    DATE DEFAULT sysdate NOT NULL,
    PROCESS_ID              NUMBER(8,0) NULL,
    CONSTRAINT 			    XXADEO_LFCY_ITEM_COND_PK PRIMARY KEY (ITEM,LFCY_COND_ID,LFCY_ENTITY)
)
/

COMMENT ON TABLE XXADEO_LFCY_ITEM_COND                               
    IS 'Conditions to evaluate for lifecycle management of ITEM/SUPPLIER and ITEM/Business Units relations';
COMMENT ON COLUMN XXADEO_LFCY_ITEM_COND.ITEM                         
    IS 'ITEM from ITEM_MASTER';
COMMENT ON COLUMN XXADEO_LFCY_ITEM_COND.LFCY_COND_ID
    IS 'Condition to evaluate from XXADE_SYSTEM_RULES';
COMMENT ON COLUMN XXADEO_LFCY_ITEM_COND.LFCY_ENTITY
    IS 'Entity associated with ITEM (supplier or business unit)';
COMMENT ON COLUMN XXADEO_LFCY_ITEM_COND.LFCY_ENTITY_TYPE
    IS 'Entity/Condition Type: IB – Business unit, IS - Supplier';
COMMENT ON COLUMN XXADEO_LFCY_ITEM_COND.VAL_STATUS
    IS 'Validation status of condition: Null – Not evaluated, Y – Condition valid, N – Condition not valid, E – Error in condition validation';
COMMENT ON COLUMN XXADEO_LFCY_ITEM_COND.NEXT_VAL_DT
    IS 'Next date for condition to be evaluated';
COMMENT ON COLUMN XXADEO_LFCY_ITEM_COND.LAST_VAL_DT
    IS 'Last date when condition was evaluated';
COMMENT ON COLUMN XXADEO_LFCY_ITEM_COND.DELETE_IND
    IS 'Process control: Conditions to be eliminated on state transition. Null/Y/N';
COMMENT ON COLUMN XXADEO_LFCY_ITEM_COND.ERROR_MESSAGE
    IS 'The error message returned by condition evaluation';
COMMENT ON COLUMN XXADEO_LFCY_ITEM_COND.LAST_UPDATE_DATETIME
    IS 'Last datetime when this record was updated';
COMMENT ON COLUMN XXADEO_LFCY_ITEM_COND.PROCESS_ID
    IS 'Process control: Process ID for current thread of execution that is processing this record';
