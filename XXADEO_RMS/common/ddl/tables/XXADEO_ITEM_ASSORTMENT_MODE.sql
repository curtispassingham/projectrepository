/******************************************************************************/
/* CREATE DATE - MAY 2018                                                     */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table "XXADEO_ITEM_ASSORTMENT_MODE"                              */
/******************************************************************************/

begin
  execute immediate 'drop table XXADEO_ITEM_ASSORTMENT_MODE cascade constraints';
exception
  when others then
    null;
end;
/

create table XXADEO_ITEM_ASSORTMENT_MODE
(
	ITEM						VARCHAR2(25 BYTE) NOT NULL,
	BUSINESS_UNIT_ID			NUMBER(10,0) NOT NULL,
	ASSORTMENT_MODE				NUMBER(5,0) NOT NULL,
	EFFECTIVE_DATE				DATE NOT NULL,
	RECORD_TYPE					VARCHAR2(1) NOT NULL, -- C = Create/Update, D = Delete
	RECORD_UPLOAD_DATETIME		DATE DEFAULT sysdate NOT NULL,
	READY_FOR_EXPORT			VARCHAR2(1) DEFAULT 'N' NOT NULL,
	EXTRACTED_TO_RMS_DATETIME	DATE NULL,
	ERROR_MESSAGE				VARCHAR2(500),
	LAST_UPDATE_DATETIME		DATE DEFAULT sysdate NOT NULL,
	CONSTRAINT 					XXADEO_ITEMASSORTMENTMODE_pk PRIMARY KEY (ITEM,BUSINESS_UNIT_ID,EFFECTIVE_DATE)
)
/

COMMENT ON TABLE XXADEO_ITEM_ASSORTMENT_MODE                               
    IS 'Data for integration of ASSORTMENT_MODE attribute to ADEO business units';
COMMENT ON COLUMN XXADEO_ITEM_ASSORTMENT_MODE.ITEM                         
    IS 'ITEM from ITEM_MASTER to where the attribute will apply';
COMMENT ON COLUMN XXADEO_ITEM_ASSORTMENT_MODE.BUSINESS_UNIT_ID             
    IS 'ADEO business unit ID';
COMMENT ON COLUMN XXADEO_ITEM_ASSORTMENT_MODE.ASSORTMENT_MODE              
    IS 'Assortment mode attribute value';
COMMENT ON COLUMN XXADEO_ITEM_ASSORTMENT_MODE.EFFECTIVE_DATE               
    IS 'Date when the changes should be applied';
COMMENT ON COLUMN XXADEO_ITEM_ASSORTMENT_MODE.RECORD_TYPE                  
    IS 'Action to be executed: C - Create, D - Delete';
COMMENT ON COLUMN XXADEO_ITEM_ASSORTMENT_MODE.RECORD_UPLOAD_DATETIME       
    IS 'Date/time when this record was created';
COMMENT ON COLUMN XXADEO_ITEM_ASSORTMENT_MODE.READY_FOR_EXPORT             
    IS 'Processing status of record: N - New, Y - Preprocessed, E - Error, S - Sent to RMS';
COMMENT ON COLUMN XXADEO_ITEM_ASSORTMENT_MODE.EXTRACTED_TO_RMS_DATETIME    
    IS 'Date/time when record was integrated to RMS';
COMMENT ON COLUMN XXADEO_ITEM_ASSORTMENT_MODE.ERROR_MESSAGE                
    IS 'Validation or integration error message';
COMMENT ON COLUMN XXADEO_ITEM_ASSORTMENT_MODE.LAST_UPDATE_DATETIME         
    IS 'Date/time of record changes';

