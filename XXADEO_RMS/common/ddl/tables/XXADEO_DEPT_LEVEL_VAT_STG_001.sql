--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL of "XXADEO_DEPT_LEVEL_VAT_STG" table for RB115           */
/******************************************************************************/
--------------------------------------------------------------------------------
-- Add/modify columns 
alter table XXADEO_DEPT_LEVEL_VAT_STG modify group_id null;
alter table XXADEO_DEPT_LEVEL_VAT_STG modify group_name null;