begin
  execute immediate 'drop table XXADEO_TEST_TABLE2';
exception
  when others then
    null;
end;
/
-- Create table
create table XXADEO_TEST_TABLE2
(
  id number,
  name varchar2(200),
  datetime timestamp
);