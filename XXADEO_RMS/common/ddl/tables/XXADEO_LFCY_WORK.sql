/******************************************************************************/
/* CREATE DATE - Jul 2018                                                     */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table "XXADEO_LFCY_WORK"                              */
/******************************************************************************/

begin
  execute immediate 'drop table XXADEO_LFCY_WORK cascade constraints';
exception
  when others then
    null;
end;
/


create table XXADEO_LFCY_WORK
(
	ITEM			            VARCHAR2(25 BYTE) NOT NULL,
    lfcy_entity_type            VARCHAR2(2) NOT NULL CHECK (lfcy_entity_type in ('IS','IB')),
    lfcy_entity                 NUMBER(10,0) NOT NULL,
    item_dept                   NUMBER(4,0) NULL,
    item_class                  NUMBER(4,0) null,
    PACK_IND                    VARCHAR2(1) NULL,
    ORDERABLE_IND               VARCHAR2(1) NULL,
    INVENTORY_IND               VARCHAR2(1) NULL,
    SELLABLE_IND                VARCHAR2(1) NULL,
    FORECAST_IND                VARCHAR2(1) NULL,
    item_type                   NUMBER(5,0) NULL,
    item_subtype                NUMBER(5,0) NULL,
    item_entity_status          NUMBER(5,0) NULL,
    lfcy_status                 VARCHAR2(10) NULL,
    lfcy_status_new             VARCHAR2(10) NULL,
    LAST_UPDATE_DATETIME        DATE NOT NULL,
    PROCESS_ID                  NUMBER(8,0),
	CONSTRAINT XXADEO_LFCY_WORK_PK PRIMARY KEY (ITEM,lfcy_entity_type,lfcy_entity)
)
/

COMMENT ON TABLE XXADEO_LFCY_WORK                               
    IS 'Conditions to evaluate for lifecycle management of ITEM/SUPPLIER and ITEM/Business Units relations';
COMMENT ON COLUMN XXADEO_LFCY_WORK.ITEM
    IS 'ITEM from ITEM_MASTER';
COMMENT ON COLUMN XXADEO_LFCY_WORK.LFCY_ENTITY_TYPE
    IS 'Relation Type: IB – Business unit, IS - Supplier';
COMMENT ON COLUMN XXADEO_LFCY_WORK.LFCY_ENTITY
    IS 'Entity associated with ITEM (supplier or business unit)';
COMMENT ON COLUMN XXADEO_LFCY_WORK.ITEM_DEPT
    IS 'ITEM_MASTER.DEPT';
COMMENT ON COLUMN XXADEO_LFCY_WORK.ITEM_CLASS
    IS 'ITEM_MASTER.CLASS';
COMMENT ON COLUMN XXADEO_LFCY_WORK.PACK_IND
    IS 'ITEM_MASTER.PACK_IND';
COMMENT ON COLUMN XXADEO_LFCY_WORK.ORDERABLE_IND
    IS 'ITEM_MASTER.ORDERABLE_IND';
COMMENT ON COLUMN XXADEO_LFCY_WORK.INVENTORY_IND
    IS 'ITEM_MASTER.INVENTORY_IND';
COMMENT ON COLUMN XXADEO_LFCY_WORK.SELLABLE_IND
    IS 'ITEM_MASTER.SELLABLE_IND';
COMMENT ON COLUMN XXADEO_LFCY_WORK.FORECAST_IND
    IS 'ITEM_MASTER.FORECAST_IND';
COMMENT ON COLUMN XXADEO_LFCY_WORK.ITEM_TYPE
    IS 'ADEO Item type value (UDA)';
COMMENT ON COLUMN XXADEO_LFCY_WORK.ITEM_SUBTYPE
    IS 'ADEO Item sub-type value (UDA)';
COMMENT ON COLUMN XXADEO_LFCY_WORK.ITEM_ENTITY_STATUS
    IS 'Status (CFA/UDA) value currently associated with relation';
COMMENT ON COLUMN XXADEO_LFCY_WORK.LFCY_STATUS
    IS 'Current lifecycle status code, from XXADEO_LFCY_CFG_STATUS';
COMMENT ON COLUMN XXADEO_LFCY_WORK.LFCY_STATUS_NEW
    IS 'New or updated lifecycle status code (helper)';
COMMENT ON COLUMN XXADEO_LFCY_WORK.LAST_UPDATE_DATETIME
    IS 'Last datetime when this record was updated';
COMMENT ON COLUMN XXADEO_LFCY_WORK.PROCESS_ID
    IS 'Process control: Process ID for current thread of execution that is processing this record';
