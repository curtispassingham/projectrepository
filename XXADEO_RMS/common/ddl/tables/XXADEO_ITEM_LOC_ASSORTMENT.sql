/******************************************************************************/
/* CREATE DATE - MAY 2018                                                     */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table "XXADEO_ITEM_LOC_ASSORTMENT"                              */
/******************************************************************************/

begin
  execute immediate 'drop table XXADEO_ITEM_LOC_ASSORTMENT cascade constraints';
exception
  when others then
    null;
end;
/

create table XXADEO_ITEM_LOC_ASSORTMENT
(
	ITEM						VARCHAR2(25 BYTE) NOT NULL,
	HIER_LEVEL					VARCHAR2(2) NOT NULL, 				-- Valid values are CH (chain), AR (area), RE (region), DI (district), S (store), and W (warehouse).
	HIER_VALUE					NUMBER(10,0) NOT NULL, 				-- Location, CatMan will communicate at the location level, store only.
	RMS_STATUS					VARCHAR2(1) NULL, 				    -- CALC => A / I / C / D
	ASSORTMENT_TYPE				VARCHAR2(1) NOT NULL, 				-- R (CatMan), P (Promotional Planning / PP)
	VISIBILITY					VARCHAR2(1) NULL, 					-- Null, P (PHYSICAL), V (VIRTUAL), T (TEMPOUT), C (PERMOUT)
	AVAILABILITY				VARCHAR2(1) NULL, 					-- Null, 1 (In Stock), 2 (On order), T (TEMPOUT), C (PERMOUT)
	REAL_AVAILABILITY			VARCHAR2(1) NULL, 					-- CALC => Null, 1 (In Stock), 2 (On order), T (TEMPOUT)
	EFFECTIVE_DATE				DATE NOT NULL,						-- Must be in the future
	RECORD_TYPE					VARCHAR2(1) NULL, 					-- C (Create/Update), D (Delete)
	RECORD_UPLOAD_DATETIME		DATE DEFAULT sysdate NOT NULL,
	READY_FOR_EXPORT			VARCHAR2(1) DEFAULT 'N' NOT NULL,
	ERROR_MESSAGE				VARCHAR2(500),
	EXTRACTED_TO_RMS_DATETIME	DATE DEFAULT sysdate NULL,
	LAST_UPDATE_DATETIME		DATE DEFAULT sysdate NOT NULL,
	CONSTRAINT 					XXADEO_ASSORTMENT_pk PRIMARY KEY (ITEM,HIER_VALUE,ASSORTMENT_TYPE,EFFECTIVE_DATE)
)
/

COMMENT ON TABLE XXADEO_ITEM_LOC_ASSORTMENT                              
    IS 'Data for integration of Visibility and Availability (and dates) attributes (CFAs) to ADEO stores';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.ITEM                         
    IS 'ITEM to where the attributes will apply';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.HIER_LEVEL                   
    IS 'Location type - Filled in PRE PROCESS';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.HIER_VALUE                   
    IS 'Store to apply the ranging and attributes';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.RMS_STATUS                   
    IS 'Status of ITEM in RMS - Filled in PRE PROCESS';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.ASSORTMENT_TYPE              
    IS 'Assortment type from CatMan';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.VISIBILITY                   
    IS 'Visibility from CatMan/PP';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.AVAILABILITY                 
    IS 'Availability from CatMan/PP';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.REAL_AVAILABILITY            
    IS 'Calculated availability - PRE PROCESS';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.EFFECTIVE_DATE               
    IS 'Date when the changes should be applied';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.RECORD_TYPE                  
    IS 'Action to be executed: C - Create, D - Delete';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.RECORD_UPLOAD_DATETIME       
    IS 'Date/time when this record was created';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.READY_FOR_EXPORT             
    IS 'Processing status of record: N - New, Y - Preprocessed, E - Error, S - Sent to RMS';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.EXTRACTED_TO_RMS_DATETIME    
    IS 'Date/time when record was integrated to RMS';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.ERROR_MESSAGE                
    IS 'Validation or integration error message';
COMMENT ON COLUMN XXADEO_ITEM_LOC_ASSORTMENT.LAST_UPDATE_DATETIME         
    IS 'Date/time of record changes';
