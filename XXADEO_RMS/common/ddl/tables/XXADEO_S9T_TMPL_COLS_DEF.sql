/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_S9T_TMPL_COLS_DEF" table for RB115            */
/******************************************************************************/
-- Create table
create table XXADEO_S9T_TMPL_COLS_DEF
(
  template_key VARCHAR2(255) not null,
  wksht_key    VARCHAR2(255) not null,
  column_key   VARCHAR2(255) not null,
  column_index NUMBER(2) not null
);
-- Add comments to the columns 
comment on column XXADEO_S9T_TMPL_COLS_DEF.template_key
  is 'The custom template key';
comment on column XXADEO_S9T_TMPL_COLS_DEF.wksht_key
  is 'The custom worksheet key';
comment on column XXADEO_S9T_TMPL_COLS_DEF.column_key
  is 'The custom column key';
comment on column XXADEO_S9T_TMPL_COLS_DEF.column_index
  is 'The custom column index (for sorting purpose)';
-- Create/Recreate primary, unique and foreign key constraints 
alter table XXADEO_S9T_TMPL_COLS_DEF
  add constraint PK_XXADEO_S9T_TEMPL_COLS_DEF primary key (TEMPLATE_KEY, WKSHT_KEY, COLUMN_KEY);