--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - November 2018                                                */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL to change comments of XXADEO_PRICE_REQUEST table         */
/******************************************************************************/
--------------------------------------------------------------------------------
comment on column XXADEO_PRICE_REQUEST.request_process_id
  is 'Identifier of the request.';
comment on column XXADEO_PRICE_REQUEST.price_change_id
  is 'The id of the generated price change.';
comment on column XXADEO_PRICE_REQUEST.status
  is 'Status of the request. Could be A - Approved, W - Worksheet, R - Rejected, P-Processed, E-Error.';
comment on column XXADEO_PRICE_REQUEST.type
  is 'Type of the request. i.e. PC - PRICE_CHANGE (PC_R - REGULAR PRICE CHANGE; PC_E - EMERGENCY PRICE_CHANGE; PC - ALL PRICE CHANGE), CL - CLEARANCE, SP - SIMPLE_PROMOTION';
comment on column XXADEO_PRICE_REQUEST.action
  is 'Action of the request to describe type of action. i.e. N = NEW, U = MOD, D = DEL, R = REJECT, PE = Process error';
comment on column XXADEO_PRICE_REQUEST.request_size
  is 'Max number of the records that will processed.';
comment on column XXADEO_PRICE_REQUEST.dependency
  is 'If the price change is an exception of a parent/zone level price change, this field holds the id of the parent price change the exception was pulled from.';
comment on column XXADEO_PRICE_REQUEST.request_user
  is 'User that created the Price Change event.';
comment on column XXADEO_PRICE_REQUEST.request_datetime
  is 'Date/time of when the record was created';
comment on column XXADEO_PRICE_REQUEST.start_proc_datetime
  is 'Date to proceed with the request.';
comment on column XXADEO_PRICE_REQUEST.process_order
  is 'Order execution of the requests.';
comment on column XXADEO_PRICE_REQUEST.pe_process_id
  is 'Unique identifier for each process.';
