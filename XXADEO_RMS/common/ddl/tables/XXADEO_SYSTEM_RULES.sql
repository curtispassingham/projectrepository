/******************************************************************************/
/* CREATE DATE - MAY 2018                                                     */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table "XXADEO_SYSTEM_RULES"                                  */
/******************************************************************************/

begin
  execute immediate 'drop table XXADEO_SYSTEM_RULES_DETAIL cascade constraints';
  execute immediate 'drop table XXADEO_SYSTEM_RULES cascade constraints';
exception
  when others then
    null;
end;
/
-- Create table
create table XXADEO_SYSTEM_RULES
(
  rule_id              VARCHAR2(30)  not null,
  rule_desc            VARCHAR2(255) not null,
  short_desc           VARCHAR2(50),
  func_area            VARCHAR2(255) not null,
  package_name         VARCHAR2(255),
  function_name        VARCHAR2(255) not null,
  call_seq_no          NUMBER(3)     not null,
  active_ind           VARCHAR2(1)   not null,
  create_datetime      DATE          not null,
  last_update_datetime DATE          not null,
  last_update_id       VARCHAR2(30)  not null
);
-- Add comments to the table 
comment on table XXADEO_SYSTEM_RULES
  is 'This table holds the list of rules that should be executed dynamically for each key and call_seq_num.';
-- Add comments to the columns 
comment on column XXADEO_SYSTEM_RULES.rule_id
  is 'The unique sequence generated for the rule.';
comment on column XXADEO_SYSTEM_RULES.rule_desc
  is 'The description of the rule';
comment on column XXADEO_SYSTEM_RULES.short_desc
  is 'The column contains the short description of the rule.';
comment on column XXADEO_SYSTEM_RULES.func_area
  is 'This column contains the identifier for the functional area.';
comment on column XXADEO_SYSTEM_RULES.package_name
  is 'This column holds the name of package where the custom code resides. This is not a mandatory field when the custom code is a stored function.';
comment on column XXADEO_SYSTEM_RULES.function_name
  is 'This column will hold the name of the function which should be executed for the rule_id and call_seq_no.';
comment on column XXADEO_SYSTEM_RULES.call_seq_no
  is 'This column contains the order the rules will be executed.';
comment on column XXADEO_SYSTEM_RULES.active_ind
  is 'This column contains the value which indicates if the rule is active or not. The valid values are ''Y'', ''N''.';
comment on column XXADEO_SYSTEM_RULES.create_datetime
  is 'This column contain the date of the creation of the rule.';
comment on column XXADEO_SYSTEM_RULES.last_update_datetime
  is 'This column holds the date  of the most recent update by the user_id.';
comment on column XXADEO_SYSTEM_RULES.last_update_id
  is 'User that last updated the record.';
-- Create/Recreate primary, unique and foreign key constraints 
alter table XXADEO_SYSTEM_RULES
  add constraint PK_XXADEO_SYSTEM_RULES primary key (RULE_ID, CALL_SEQ_NO);
