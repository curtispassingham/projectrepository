--------------------------------------------------------
--  DDL for Table XXADEO_SUPSITE_ORG_UNIT
--------------------------------------------------------

CREATE TABLE XXADEO_SUPSITE_ORG_UNIT 
   (	XX_SUPPLIER_SITE_ID NUMBER(10), 
	XX_ORG_UNIT_ID NUMBER(15), 
	XX_PRIMARY_PAY_SITE VARCHAR2(1), 
	XX_BATCH_ID NUMBER(10) DEFAULT 0
   );

   COMMENT ON COLUMN XXADEO_SUPSITE_ORG_UNIT.XX_SUPPLIER_SITE_ID IS 'Unique identifier for a supplier within RMS';
   COMMENT ON COLUMN XXADEO_SUPSITE_ORG_UNIT.XX_ORG_UNIT_ID IS 'Organizational unit ID';
   COMMENT ON COLUMN XXADEO_SUPSITE_ORG_UNIT.XX_PRIMARY_PAY_SITE IS 'Primary payment site indicator';
   COMMENT ON COLUMN XXADEO_SUPSITE_ORG_UNIT.XX_BATCH_ID IS 'Batch identifier';
   COMMENT ON TABLE XXADEO_SUPSITE_ORG_UNIT  IS 'This staging table will hold the organization units linked to a supplier site';