/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL of "XXADEO_RPM_CUSTOM_OPTIONS" table for RB34a           */
/******************************************************************************/
--
begin
  execute immediate 'drop table XXADEO_RPM_CUSTOM_OPTIONS cascade constraints';
exception
  when others then
    null;
end;
/
-- Create table
create table XXADEO_RPM_CUSTOM_OPTIONS
(
  retention_control_id        NUMBER(5) not null,
  price_change_type           VARCHAR2(5),
  processed_pc_retention_days NUMBER(5),
  error_pc_retention_days     NUMBER(5),
  pc_process_max_request_size NUMBER(6),
  pc_blocking_cfa_value       VARCHAR2(1) default 1
);
-- Add comments to the table 
comment on table XXADEO_RPM_CUSTOM_OPTIONS
  is 'Entity to hold the parameters for Price Change events purge.';
-- Add comments to the columns 
comment on column XXADEO_RPM_CUSTOM_OPTIONS.retention_control_id
  is 'Identifies the record.';
comment on column XXADEO_RPM_CUSTOM_OPTIONS.price_change_type
  is 'Identify the Price Change Type.';
comment on column XXADEO_RPM_CUSTOM_OPTIONS.processed_pc_retention_days
  is 'Identify the number of retention days for processed Price Change.';
comment on column XXADEO_RPM_CUSTOM_OPTIONS.error_pc_retention_days
  is 'Identify the number of retention days for Price Change in Error Status.';
comment on column XXADEO_RPM_CUSTOM_OPTIONS.pc_process_max_request_size
  is 'Identify the max number of process request size.';
comment on column XXADEO_RPM_CUSTOM_OPTIONS.pc_blocking_cfa_value
  is 'Identify the cfa value that will be used to blocking price change event creation in Store Portal.';
-- Create/Recreate primary, unique and foreign key constraints 
alter table XXADEO_RPM_CUSTOM_OPTIONS
  add constraint PK_XXADEO_RPM_CUSTOM_OPTIONS primary key (RETENTION_CONTROL_ID);
