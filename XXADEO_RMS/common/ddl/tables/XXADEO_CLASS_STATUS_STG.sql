/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_CLASS_STATUS_STG" table for RB115         	  */
/******************************************************************************/
-- Create table
begin
  execute immediate 'drop table XXADEO_CLASS_STATUS_STG cascade constraints';
exception
  when others then
    null;
end;
/

create table XXADEO_CLASS_STATUS_STG
(
  process_id           NUMBER(20) not null,
  xxadeo_process_id    NUMBER(20) not null,
  action              VARCHAR2(10) not null,
  group_id            NUMBER(4),
  group_name           VARCHAR2(120),
  dept                 NUMBER(4) not null,
  dept_name            VARCHAR2(120),
  class                NUMBER(4) not null,
  class_name           VARCHAR2(120),
  class_status         VARCHAR2(120) not null,
  create_id            VARCHAR2(30) default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) not null,
  create_datetime      DATE not null,
  last_update_id       VARCHAR2(30) not null,
  last_update_datetime DATE not null,
  row_seq              NUMBER not null
);
-- Add comments to the table 
comment on table XXADEO_CLASS_STATUS_STG
  is 'Table to be used as a staging area while performing and processing the uploads or downloads into and from RMS.';
-- Add comments to the columns 
comment on column XXADEO_CLASS_STATUS_STG.process_id
  is 'Unique identifier for the upload process.';
comment on column XXADEO_CLASS_STATUS_STG.action
  is 'Type of action: Create or Update.';
comment on column XXADEO_CLASS_STATUS_STG.group_id
  is 'This field contains the ID of the group.';
comment on column XXADEO_CLASS_STATUS_STG.group_name
  is 'This field contains the name of the group.';
comment on column XXADEO_CLASS_STATUS_STG.dept
  is 'This field contains the Department ID.';
comment on column XXADEO_CLASS_STATUS_STG.dept_name
  is 'This field contains the name of the department in the language of the user if exists. If no translation, name in primary language.';
comment on column XXADEO_CLASS_STATUS_STG.class
  is 'This field contains the Class ID.';
comment on column XXADEO_CLASS_STATUS_STG.class_name
  is 'This field contains the name of the class in the language of the user. If no translation exists, it will contain the name in primary language.';
comment on column XXADEO_CLASS_STATUS_STG.class_status
  is 'This field is to define the value to be uploaded for the CFA ''Status''.';
comment on column XXADEO_CLASS_STATUS_STG.create_id
  is 'This column holds the User id of the user who created the record.';
comment on column XXADEO_CLASS_STATUS_STG.create_datetime
  is 'Date/time stamp of when the record was created.  This date/time will be used in export processing.  This value should only be populated on insert  - it should never be updated.';
comment on column XXADEO_CLASS_STATUS_STG.last_update_id
  is 'Holds the Oracle user-id of the user who most recently updated this record.  This field is required by the database.';
comment on column XXADEO_CLASS_STATUS_STG.last_update_datetime
  is 'Holds the date time stamp of the most recent update by the last_update_id.  This field is required by the database.';
comment on column XXADEO_CLASS_STATUS_STG.row_seq
  is 'Unique identifier for the row.';
-- Create/Recreate primary, unique and foreign key constraints 
alter table XXADEO_CLASS_STATUS_STG
  add constraint PK_XXADEO_CLASS_STATUS_STG primary key (PROCESS_ID, ROW_SEQ);
alter table XXADEO_CLASS_STATUS_STG
  add constraint FK_XXADEO_CLASS_STATUS_STG foreign key (PROCESS_ID)
  references SVC_PROCESS_TRACKER (PROCESS_ID);