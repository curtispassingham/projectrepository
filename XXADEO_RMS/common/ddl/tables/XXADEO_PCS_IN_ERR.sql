/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Wilson Sturaro                                               */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL of "XXADEO_PCS_IN_ERR" table for RB157                   */
/******************************************************************************/
--
begin
  execute immediate 'drop table XXADEO_PCS_IN_ERR';
exception
  when others then
    null;
end;
/
-- Create table
CREATE TABLE "XXADEO_PCS_IN_ERR" 
   (	"ORA_ERR_NUMBER$" NUMBER, 
	"ORA_ERR_MESG$" VARCHAR2(2000 BYTE), 
	"ORA_ERR_ROWID$" UROWID (4000), 
	"ORA_ERR_OPTYP$" VARCHAR2(2 BYTE), 
	"ORA_ERR_TAG$" VARCHAR2(2000 BYTE), 
	"ITEM" VARCHAR2(4000 BYTE), 
	"SUPPLIER" VARCHAR2(4000 BYTE), 
    "BU" VARCHAR2(4000 BYTE), 
	"STORE" VARCHAR2(4000 BYTE), 
	"PCS" VARCHAR2(4000 BYTE), 
	"CIRCUIT_PRINCIPAL" VARCHAR2(4000 BYTE), 
	"PCS_SOURCE" VARCHAR2(4000 BYTE), 
	"STATUS" VARCHAR2(4000 BYTE), 
    "EFFECTIVE_DATE" VARCHAR2(4000 BYTE), 
	"CREATE_DATETIME" VARCHAR2(4000 BYTE), 
	"LAST_UPDATE_DATETIME" VARCHAR2(4000 BYTE), 
	"LAST_UPDATE_ID" VARCHAR2(4000 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXADEO_RMS_DATA" ;

   COMMENT ON TABLE "XXADEO_PCS_IN_ERR"  IS 'DML Error Logging table for "XXADEO_AREA_PCS and XXADEO_LOCATION_PCS"';
