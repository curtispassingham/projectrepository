-------------------------------------------------------
--  Table XXADEO_STG_PC_DTL_OUT
--------------------------------------------------------
DECLARE

  L_exist NUMBER;

BEGIN

  select count(*)
    into L_exist
    from all_objects
   where object_type in ('TABLE')
     and object_name = 'XXADEO_STG_PC_EVT_OUT';

  if L_exist = 0 then
execute immediate '  

CREATE TABLE XXADEO_STG_PC_EVT_OUT
   (PRICE_EVENT_PAYLOAD_ID NUMBER(10) NOT NULL, 
  RIB_ACTION_TYPE VARCHAR2(30) NOT NULL, 
  CREATE_DATETIME DATE NOT NULL, 
  LAST_UPDATE_DATETIME DATE NOT NULL
   );

   COMMENT ON COLUMN XXADEO_STG_PC_EVT_OUT.PRICE_EVENT_PAYLOAD_ID IS ''primary key of price event'';
   COMMENT ON COLUMN XXADEO_STG_PC_EVT_OUT.RIB_ACTION_TYPE IS ''Rib message type'';
   COMMENT ON COLUMN XXADEO_STG_PC_EVT_OUT.CREATE_DATETIME IS ''Creation Date'';
   COMMENT ON COLUMN XXADEO_STG_PC_EVT_OUT.LAST_UPDATE_DATETIME IS ''Last Update Date'';

';

end if;

END;
/