/******************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Queue "XXADEO_ITEM_LIFE_CYCLE_Q" for RB107/118               */
/******************************************************************************/
-- create queue table
begin
  DBMS_AQADM.CREATE_QUEUE_TABLE(
    queue_table        => 'XXADEO_ITEM_LFCY_Q_TBL',
    queue_payload_type => 'XXADEO_UPLOAD_PLAYLOAD',
    multiple_consumers => true
  );
end;
/
--
begin
  -- create queue
  DBMS_AQADM.CREATE_QUEUE(
    queue_name  => 'XXADEO_ITEM_LFCY_Q',
    queue_table => 'XXADEO_ITEM_LFCY_Q_TBL'
  );
  -- start queue
  DBMS_AQADM.START_QUEUE(queue_name => 'XXADEO_ITEM_LFCY_Q');
end;
/
--
begin
  -- add subscriber
  DBMS_AQADM.ADD_SUBSCRIBER(
    queue_name => 'XXADEO_ITEM_LFCY_Q',
    subscriber => SYS.AQ$_AGENT(
      'XXADEO_ITEM_LFCY_Q_SUBSCRIBER',
      NULL,
      NULL
    )
  );
  -- register
  DBMS_AQ.REGISTER(
    SYS.AQ$_REG_INFO_LIST(
      SYS.AQ$_REG_INFO(
        'XXADEO_ITEM_LFCY_Q:XXADEO_ITEM_LFCY_Q_SUBSCRIBER',
        DBMS_AQ.NAMESPACE_AQ,
        'plsql://XXADEO_ITEM_LIFE_CYCLE_SQL.DEQUEUE_ITEM_LIFE_CYCLE_CALLBACK',
        HEXTORAW('FF')
      )
    ), 1
  );
end;
/