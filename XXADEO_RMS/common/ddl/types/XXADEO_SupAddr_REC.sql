--------------------------------------------------------
--  DDL for Type XXADEO_SupAddr_REC
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE TYPE "XXADEO_SupAddr_REC" UNDER RIB_OBJECT (
  --------------------------------------------------------------------------------------------------------------------------------------------
   --
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload.
   --
   --------------------------------------------------------------------------------------------------------------------------------------------
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   --
   --  These variables are the payload variables which are used to construct the payload.
   --
   --------------------------------------------------------------------------------------------------------------------------------------------
   addr_xref_key varchar2(32),
   addr_key number(11),
   Addr "RIB_Addr_REC",
   ExtOfSupSiteAddr "RIB_ExtOfSupSiteAddr_REC",
   LocOfSupSiteAddr_TBL "RIB_LocOfSupSiteAddr_TBL",   -- Size of "RIB_LocOfSupSiteAddr_TBL" is unbounded
   OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "XXADEO_SupAddr_REC" (
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, Addr "RIB_Addr_REC"
) return self as result

,constructor function "XXADEO_SupAddr_REC" (
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, Addr "RIB_Addr_REC"
, ExtOfSupSiteAddr "RIB_ExtOfSupSiteAddr_REC"
) return self as result

,constructor function "XXADEO_SupAddr_REC" (
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, Addr "RIB_Addr_REC"
,ExtOfSupSiteAddr "RIB_ExtOfSupSiteAddr_REC"
,LocOfSupSiteAddr_TBL "RIB_LocOfSupSiteAddr_TBL"
) return self as result
);
/

CREATE OR REPLACE EDITIONABLE TYPE BODY "XXADEO_SupAddr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierDesc') := "ns_name_SupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_xref_key') := addr_xref_key;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_key') := addr_key;
  l_new_pre :=i_prefix||'Addr.';
  Addr.appendNodeValues( i_prefix||'Addr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ExtOfSupSiteAddr.';
  ExtOfSupSiteAddr.appendNodeValues( i_prefix||'ExtOfSupSiteAddr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfSupSiteAddr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfSupSiteAddr_TBL.';
    FOR INDX IN LocOfSupSiteAddr_TBL.FIRST()..LocOfSupSiteAddr_TBL.LAST() LOOP
      LocOfSupSiteAddr_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfSupSiteAddr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "XXADEO_SupAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, Addr "RIB_Addr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_xref_key := addr_xref_key;
self.addr_key := addr_key;
self.Addr := Addr;
RETURN;
end;
constructor function "XXADEO_SupAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, Addr "RIB_Addr_REC"
, ExtOfSupSiteAddr "RIB_ExtOfSupSiteAddr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_xref_key := addr_xref_key;
self.addr_key := addr_key;
self.Addr := Addr;
self.ExtOfSupSiteAddr := ExtOfSupSiteAddr;
RETURN;
end;
constructor function "XXADEO_SupAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, Addr "RIB_Addr_REC"
, ExtOfSupSiteAddr "RIB_ExtOfSupSiteAddr_REC"
, LocOfSupSiteAddr_TBL "RIB_LocOfSupSiteAddr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_xref_key := addr_xref_key;
self.addr_key := addr_key;
self.Addr := Addr;
self.ExtOfSupSiteAddr := ExtOfSupSiteAddr;
self.LocOfSupSiteAddr_TBL := LocOfSupSiteAddr_TBL;
RETURN;
end;
END;
/