--------------------------------------------------------
--  DDL for Type XXADEO_SUP_CFA_REC
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE TYPE XXADEO_SUP_CFA_REC AS OBJECT
(
  CFA_NAME        varchar2(30),
  VALUE           varchar2(250),
  VALUE_NUMBER    NUMBER,
  VALUE_DATE      DATE
)

/