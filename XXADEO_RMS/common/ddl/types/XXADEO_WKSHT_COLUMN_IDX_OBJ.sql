/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Object type "XXADEO_WKSHT_COLUMN_IDX_OBJ"                    */
/******************************************************************************/

begin
  execute immediate 'drop type XXADEO_WKSHT_COLUMN_IDX_TBL';
  execute immediate 'drop type XXADEO_WKSHT_COLUMN_IDX_OBJ';
exception
  when others then
    null;
end;
/

create or replace type xxadeo_wksht_column_idx_obj as object (template_key varchar2(255),
                                                              wksht_key    varchar2(255),
                                                              column_key   varchar2(255),
                                                              column_index number(2));
/