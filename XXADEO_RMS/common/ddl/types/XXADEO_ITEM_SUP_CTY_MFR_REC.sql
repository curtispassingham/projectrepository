/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_ITEM_SUP_CTY_MFR_REC
* Description:   Type used by XITEM
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
DROP TYPE XXADEO_ITEM_SUP_CTY_MFR_REC FORCE;
CREATE OR REPLACE TYPE XXADEO_ITEM_SUP_CTY_MFR_REC FORCE IS OBJECT(
MANU_COUNTRY_ID VARCHAR2(3),
PRIMARY_MANU_COUNTRY_ID VARCHAR2(1)
);
/
