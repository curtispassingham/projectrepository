DROP TYPE "RIB_CustFlexAttriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustFlexAttriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ExtOfCustFlexAttriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_custom" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  id number(10),
  name varchar2(30),
  value varchar2(250),
  value_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustFlexAttriVo_REC"
(
  rib_oid number
, id number
) return self as result
,constructor function "RIB_CustFlexAttriVo_REC"
(
  rib_oid number
, id number
, name varchar2
) return self as result
,constructor function "RIB_CustFlexAttriVo_REC"
(
  rib_oid number
, id number
, name varchar2
, value varchar2
) return self as result
,constructor function "RIB_CustFlexAttriVo_REC"
(
  rib_oid number
, id number
, name varchar2
, value varchar2
, value_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustFlexAttriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ExtOfCustFlexAttriVo') := "ns_name_ExtOfCustFlexAttriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_custom') := "ns_location_custom";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'id') := id;
  rib_obj_util.g_RIB_element_values(i_prefix||'name') := name;
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
  rib_obj_util.g_RIB_element_values(i_prefix||'value_date') := value_date;
END appendNodeValues;
constructor function "RIB_CustFlexAttriVo_REC"
(
  rib_oid number
, id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
RETURN;
end;
constructor function "RIB_CustFlexAttriVo_REC"
(
  rib_oid number
, id number
, name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.name := name;
RETURN;
end;
constructor function "RIB_CustFlexAttriVo_REC"
(
  rib_oid number
, id number
, name varchar2
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.name := name;
self.value := value;
RETURN;
end;
constructor function "RIB_CustFlexAttriVo_REC"
(
  rib_oid number
, id number
, name varchar2
, value varchar2
, value_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.name := name;
self.value := value;
self.value_date := value_date;
RETURN;
end;
END;
/
