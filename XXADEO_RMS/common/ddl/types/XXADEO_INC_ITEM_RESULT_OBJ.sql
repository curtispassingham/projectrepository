/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Object type "XXADEO_INC_ITEM_RESULT_OBJ"                     */
/******************************************************************************/
 
begin
  execute immediate 'drop type XXADEO_INC_ITEM_RESULT_TBL';
exception
  when others then
    null;
end;
/

begin
  execute immediate 'drop type XXADEO_INC_ITEM_RESULT_OBJ';
exception
  when others then
    null;
end;
/ 

create or replace type XXADEO_INC_ITEM_RESULT_OBJ as OBJECT
( 
  BU                   NUMBER,
  DEPT_ID              NUMBER,
  DEPT_NAME            VARCHAR2(250),
  SUBDEPT_ID           NUMBER(4),
  SUBDEPT_NAME         VARCHAR2(250),
  TYPE_ID              NUMBER(4),
  TYPE_NAME            VARCHAR2(250),
  SUBTYPE_ID           NUMBER(4),
  SUBTYPE_NAME         VARCHAR2(250),
  ITEM                 VARCHAR2(25),
  ITEM_DESC            VARCHAR2(250),
  SUPP_IND             VARCHAR2(1),
  CLASSMENT_BU         NUMBER(12,4),
  SUBTYPOLOGY          VARCHAR2(250),
  LFCY_ENTITY          NUMBER(10),
  LFCY_STATUS          VARCHAR2(10),
  LFCY_ENTITY_TYPE     VARCHAR2(2),
  ACTIVE_COMMERCE_DATE DATE,
  COND_FLAG            VARCHAR2(20),
  SELLABLE_IND         VARCHAR2(1),
  ORDERABLE_IND        VARCHAR2(1),
  STATUS               VARCHAR2(1),
  COMPLETEDPERCENT     NUMBER,
  COND_IB_ASI_01       NUMBER,
  COND_IB_ASI_02       NUMBER,
  COND_IB_ASI_03       NUMBER,
  COND_IB_ASI_04       NUMBER,
  COND_IB_ASI_05       NUMBER,
  COND_IB_ACOM_01      NUMBER,
  COND_IB_ACOM_02      NUMBER,
  COND_IB_ACOM_03      NUMBER,
  COND_IB_ACOM_04      NUMBER,
  COND_IS_ACT_01       NUMBER,
  COND_IS_ACT_02       NUMBER,
  COND_IS_ACT_03       NUMBER,
  COND_IS_ACT_04       NUMBER,
  COND_IS_ACT_05       NUMBER,
  COND_IS_ACT_06       NUMBER,
  COND_IS_ACT_07       NUMBER
);
/
    