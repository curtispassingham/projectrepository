/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_VAT_ITEM_TBL
* Description:   Table type used by XITEM
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
DROP TYPE XXADEO_VAT_ITEM_TBL FORCE;
CREATE OR REPLACE TYPE XXADEO_VAT_ITEM_TBL IS TABLE OF XXADEO_VAT_ITEM_REC;
/