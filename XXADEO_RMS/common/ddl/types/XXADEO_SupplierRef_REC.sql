--------------------------------------------------------
--  DDL for Type XXADEO_SupplierRef_REC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TYPE "XXADEO_SupplierRef_REC" FORCE UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   --
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload.
   --
   --------------------------------------------------------------------------------------------------------------------------------------------
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   --
   --  These variables are the payload variables which are used to construct the payload.
   --
   --------------------------------------------------------------------------------------------------------------------------------------------
  sup_xref_key varchar2(32),
  supplier_id number(10),
  supsite_xref_key varchar2(32),
  supsite_id number(10),
  SuppAddr "XXADEO_SupAddr_TBL",   -- Size of "RIB_SupplierSite_TBL" is unbounded
  SupOrgUnit "RIB_SupSiteOrgUnit_TBL",
  SupCFA "XXADEO_CFA_DETAILS_TBL",
  ExtOfSupplierRef "RIB_ExtOfSupplierRef_REC",
  LocOfSupplierRef_TBL "RIB_LocOfSupplierRef_TBL",   -- Size of "RIB_LocOfSupplierRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "XXADEO_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
) return self as result
,constructor function "XXADEO_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
,  SuppAddr "XXADEO_SupAddr_TBL"   -- Size of "RIB_SupplierSite_TBL" is unbounded
,  SupOrgUnit "RIB_SupSiteOrgUnit_TBL"
,  SupCFA "XXADEO_CFA_DETAILS_TBL"
) return self as result
,constructor function "XXADEO_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
,  SuppAddr "XXADEO_SupAddr_TBL"   -- Size of "RIB_SupplierSite_TBL" is unbounded
,  SupOrgUnit "RIB_SupSiteOrgUnit_TBL"
,  SupCFA "XXADEO_CFA_DETAILS_TBL"
, ExtOfSupplierRef "RIB_ExtOfSupplierRef_REC"
) return self as result
,constructor function "XXADEO_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
,  SuppAddr "XXADEO_SupAddr_TBL"   -- Size of "RIB_SupplierSite_TBL" is unbounded
,  SupOrgUnit "RIB_SupSiteOrgUnit_TBL"
,  SupCFA "XXADEO_CFA_DETAILS_TBL"
, ExtOfSupplierRef "RIB_ExtOfSupplierRef_REC"
, LocOfSupplierRef_TBL "RIB_LocOfSupplierRef_TBL"  -- Size of "RIB_LocOfSupplierRef_TBL" is unbounded
) return self as result
);

/

CREATE OR REPLACE EDITIONABLE TYPE BODY "XXADEO_SupplierRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierRef') := "ns_name_SupplierRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'sup_xref_key') := sup_xref_key;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  l_new_pre :=i_prefix||'ExtOfSupplierRef.';
  ExtOfSupplierRef.appendNodeValues( i_prefix||'ExtOfSupplierRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfSupplierRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfSupplierRef_TBL.';
    FOR INDX IN LocOfSupplierRef_TBL.FIRST()..LocOfSupplierRef_TBL.LAST() LOOP
      LocOfSupplierRef_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfSupplierRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "XXADEO_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
RETURN;
end;
constructor function "XXADEO_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
,  SuppAddr "XXADEO_SupAddr_TBL"   -- Size of "RIB_SupplierSite_TBL" is unbounded
,  SupOrgUnit "RIB_SupSiteOrgUnit_TBL"
,  SupCFA "XXADEO_CFA_DETAILS_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
  self.supsite_xref_key := supsite_xref_key;
  self.supsite_id := supsite_id;
self.SuppAddr := SuppAddr;
self.SupOrgUnit := SupOrgUnit;
self.SupCFA := SupCFA;
RETURN;
end;
constructor function "XXADEO_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
,  SuppAddr "XXADEO_SupAddr_TBL"   -- Size of "RIB_SupplierSite_TBL" is unbounded
,  SupOrgUnit "RIB_SupSiteOrgUnit_TBL"
,  SupCFA "XXADEO_CFA_DETAILS_TBL"
, ExtOfSupplierRef "RIB_ExtOfSupplierRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
  self.supsite_xref_key := supsite_xref_key;
  self.supsite_id := supsite_id;
self.SuppAddr := SuppAddr;
self.SupOrgUnit := SupOrgUnit;
self.SupCFA := SupCFA;
self.ExtOfSupplierRef := ExtOfSupplierRef;
RETURN;
end;
constructor function "XXADEO_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
,  SuppAddr "XXADEO_SupAddr_TBL"   -- Size of "RIB_SupplierSite_TBL" is unbounded
,  SupOrgUnit "RIB_SupSiteOrgUnit_TBL"
,  SupCFA "XXADEO_CFA_DETAILS_TBL"
, ExtOfSupplierRef "RIB_ExtOfSupplierRef_REC"
, LocOfSupplierRef_TBL "RIB_LocOfSupplierRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
  self.supsite_xref_key := supsite_xref_key;
  self.supsite_id := supsite_id;
self.SuppAddr := SuppAddr;
self.SupOrgUnit := SupOrgUnit;
self.SupCFA := SupCFA;
self.ExtOfSupplierRef := ExtOfSupplierRef;
self.LocOfSupplierRef_TBL := LocOfSupplierRef_TBL;
RETURN;
end;
END;

/