/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Object type "XXADEO_MOM_DV_OBJ"                              */
/******************************************************************************/

begin
  execute immediate 'drop type XXADEO_MOM_DV_TBL';
  execute immediate 'drop type XXADEO_MOM_DV_OBJ';
exception
  when others then
    null;
end;
/

CREATE OR REPLACE TYPE XXADEO_MOM_DVM_OBJ AS OBJECT(
  func_area        VARCHAR2(30),
  parameter        VARCHAR2(30),
  bu               NUMBER(10),
  value_1          VARCHAR2(250),
  value_2          VARCHAR2(250),
  description      VARCHAR2(250),
  --
  CONSTRUCTOR FUNCTION XXADEO_MOM_DVM_OBJ RETURN SELF AS RESULT,
  --
  CONSTRUCTOR FUNCTION XXADEO_MOM_DVM_OBJ(
     func_area        VARCHAR2,
     parameter        VARCHAR2,
     bu               NUMBER,
     value_1          VARCHAR2,
     value_2          VARCHAR2,
     description      VARCHAR2
  ) RETURN SELF AS RESULT,
  --
  CONSTRUCTOR FUNCTION XXADEO_MOM_DVM_OBJ(
     func_area        VARCHAR2,
     parameter        VARCHAR2,
     value_1          VARCHAR2,
     value_2          VARCHAR2
  ) RETURN SELF AS RESULT,
  --
  CONSTRUCTOR FUNCTION XXADEO_MOM_DVM_OBJ(
     func_area        VARCHAR2,
     parameter        VARCHAR2,
     bu               NUMBER,
     value_1          VARCHAR2,
     value_2          VARCHAR2
  ) RETURN SELF AS RESULT,
  --
  CONSTRUCTOR FUNCTION XXADEO_MOM_DVM_OBJ(
     func_area        VARCHAR2,
     parameter        VARCHAR2,
     bu               NUMBER
  ) RETURN SELF AS RESULT
  --
);
/
--
CREATE OR REPLACE TYPE BODY XXADEO_MOM_DVM_OBJ AS
  --
  CONSTRUCTOR FUNCTION XXADEO_MOM_DVM_OBJ
  RETURN SELF AS RESULT IS
  BEGIN
  RETURN;
  END;
  --
  CONSTRUCTOR FUNCTION XXADEO_MOM_DVM_OBJ(
     func_area        VARCHAR2,
     parameter        VARCHAR2,
     bu               NUMBER,
     value_1          VARCHAR2,
     value_2          VARCHAR2,
     description      VARCHAR2
  ) RETURN SELF AS RESULT IS
  BEGIN
   SELF.func_area     := func_area;
   SELF.parameter     := parameter;
   SELF.bu            := bu;
   SELF.value_1       := value_1;
   SELF.value_2       := value_2;
   SELF.description   := description;
   RETURN;
  END;
  --
  CONSTRUCTOR FUNCTION XXADEO_MOM_DVM_OBJ(
     func_area        VARCHAR2,
     parameter        VARCHAR2,
     bu               NUMBER,
     value_1          VARCHAR2,
     value_2          VARCHAR2
  ) RETURN SELF AS RESULT IS
  BEGIN
   SELF.func_area     := func_area;
   SELF.parameter     := parameter;
   SELF.bu            := bu;
   SELF.value_1       := value_1;
   SELF.value_2       := value_2;
   RETURN;
  END;
  --
  CONSTRUCTOR FUNCTION XXADEO_MOM_DVM_OBJ(
     func_area        VARCHAR2,
     parameter        VARCHAR2,
     value_1          VARCHAR2,
     value_2          VARCHAR2
  ) RETURN SELF AS RESULT IS
  BEGIN
   SELF.func_area     := func_area;
   SELF.parameter     := parameter;
   SELF.value_1       := value_1;
   SELF.value_2       := value_2;
   RETURN;
  END;
  --
  CONSTRUCTOR FUNCTION XXADEO_MOM_DVM_OBJ(
     func_area        VARCHAR2,
     parameter        VARCHAR2,
     bu               NUMBER
  ) RETURN SELF AS RESULT IS
  BEGIN
   SELF.func_area     := func_area;
   SELF.parameter     := parameter;
   SELF.bu            := bu;
   RETURN;
  END;
  --
END;
/