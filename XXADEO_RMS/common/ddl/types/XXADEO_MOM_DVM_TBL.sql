/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table type "XXADEO_MOM_DVM_TBL"                              */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_MOM_DVM_TBL';
exception
  when others then
    null;
end;
/
--
CREATE OR REPLACE TYPE XXADEO_MOM_DVM_TBL AS TABLE OF XXADEO_MOM_DVM_OBJ;
/
