/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_CFA_DETAILS
* Description:   Type used by XXADEO_CFAS_UTILS
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/

DROP TYPE XXADEO_CFA_DETAILS FORCE;
CREATE OR REPLACE TYPE XXADEO_CFA_DETAILS AS OBJECT
(
  CFA_ID          number(10),
  CFA_NAME        varchar2(30),
  CFA_GROUP       varchar2(30),
  CFA_COL         varchar2(11),
  CFA_DATA_TYPE   varchar2(10),
  VALUE           varchar2(4000),
  VALUE_DATE      date
  );
/
