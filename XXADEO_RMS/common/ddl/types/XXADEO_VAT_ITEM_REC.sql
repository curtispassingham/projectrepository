/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_VAT_ITEM_REC
* Description:   Type used by XITEM
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
DROP TYPE XXADEO_VAT_ITEM_REC FORCE;
CREATE OR REPLACE TYPE XXADEO_VAT_ITEM_REC IS OBJECT(
VAT_REGION NUMBER(4),
ACTIVE_DATE DATE,
VAT_TYPE VARCHAR2(1),
VAT_CODE VARCHAR2(6),
VAT_RATE NUMBER(20,10)
);
/
