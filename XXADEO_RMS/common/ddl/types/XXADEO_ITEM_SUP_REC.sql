/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_ITEM_SUP_REC
* Description:   Type used by XITEM
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
DROP TYPE XXADEO_ITEM_SUP_REC FORCE;
CREATE OR REPLACE TYPE XXADEO_ITEM_SUP_REC FORCE IS OBJECT(
SUPPLIER NUMBER(10),
VPN VARCHAR2(30)
);
/
