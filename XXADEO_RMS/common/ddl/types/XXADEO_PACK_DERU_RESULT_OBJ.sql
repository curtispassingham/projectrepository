/******************************************************************************/
/* CREATE DATE - January 2019                                                 */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Object type "XXADEO_PACK_DERU_RESULT_OBJ" for RB148          */
/******************************************************************************/

begin
  execute immediate 'drop type XXADEO_PACK_DERU_RESULT_TBL';
exception
  when others then
    null;
end;
/

begin
  execute immediate 'drop type XXADEO_PACK_DERU_RESULT_OBJ';
exception
  when others then
    null;
end;
/

create or replace type XXADEO_PACK_DERU_RESULT_OBJ as OBJECT(
filter_org_id number(5),
status  varchar2(1),
pack_ind  varchar2(1),
pack_type varchar2(1),
item_level  number(1),
tran_level  number(1),
check_uda_ind varchar2(1),
group_no  number,
group_name  varchar2(120),
dept  number(4),
dept_name varchar2(120),
class number(4),
class_name  varchar2(120),
subclass  number(4),
sub_name  varchar2(120),
ranking_item  number,
item  varchar2(25),
item_desc varchar2(250),
item_type varchar2(10),
agg_item varchar2(25),
pack_qty  number,
sub_typology  varchar2(250),
personalized_ind  char(1),
standard_uom  varchar2(4),
derunit_conv_factor number,
qt_capacity number,
contain_uom_item  varchar2(250),
assortment_mode varchar2(250),
range_size  varchar2(250),
orderable_ind varchar2(1),
sellable_ind  varchar2(1),
std_str_tr_prc_itemprc number,
std_str_tr_prc_prccntrl number,
national_price_itmprc number,
national_price_prccntrl number,
national_price_check  number,
margin_itmprc number,
margin_perc number,
margin_prccntrl number,
retail_prc_blocking varchar2(250),
lifecycle_status  varchar2(250),
actif_ddate_item  date,
forecast_ind  varchar2(1)
);
/
   