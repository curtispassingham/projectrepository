/******************************************************************************/
/* CREATE DATE - January 2019                                                 */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table type "XXADEO_PACK_DERU_RESULT_TBL" for RB148           */
/******************************************************************************/

begin
  execute immediate 'drop type XXADEO_PACK_DERU_RESULT_TBL';
exception
  when others then
    null;
end;
/

create or replace type XXADEO_PACK_DERU_RESULT_TBL AS TABLE OF XXADEO_PACK_DERU_RESULT_OBJ; 
/