/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Gustavo Martins                                              */
/* UPDATE USER - Elsa Barros                                                  */
/* PROJECT     - DEV CENTER                                                   */
/* DESCRIPTION - Table type "XXADEO_CFA_SEARCH_TBL"                           */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_CFA_SEARCH_TBL';
exception
  when others then
    null;
end;  
/ 
--
CREATE OR REPLACE TYPE XXADEO_CFA_SEARCH_TBL AS TABLE OF XXADEO_CFA_SEARCH_OBJ;
/ 
