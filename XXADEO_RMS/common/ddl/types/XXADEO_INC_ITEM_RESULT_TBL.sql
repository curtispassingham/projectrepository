/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table type "XXADEO_INC_ITEM_RESULT_TBL"                      */
/******************************************************************************/
 
begin
  execute immediate 'drop type XXADEO_INC_ITEM_RESULT_TBL';
exception 
  when others then
    null;
end;
/

create or replace type XXADEO_INC_ITEM_RESULT_TBL AS TABLE OF XXADEO_INC_ITEM_RESULT_OBJ; 
/  