/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table type "XXADEO_PC_REJECTED_TBL" for RB34a                */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_PC_REJECTED_TBL';
exception
  when others then
    null;
end;
/
--
create or replace type XXADEO_PC_REJECTED_TBL as table of XXADEO_PC_REJECTED_OBJ;
/  
     