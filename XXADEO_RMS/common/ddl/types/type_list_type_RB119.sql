 /******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table type "LIST_TYPE"                                       */
/******************************************************************************/

 CREATE OR REPLACE TYPE list_type AS TABLE OF VARCHAR2(250);    
 /