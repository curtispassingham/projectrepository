/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - DEV CENTER                                                   */
/* DESCRIPTION - Object type "XXADEO_CFA_SEARCH_OBJ"                          */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_CFA_SEARCH_TBL';
  execute immediate 'drop type XXADEO_CFA_SEARCH_OBJ';
exception
  when others then
    null;
end; 
/  
--
CREATE OR REPLACE TYPE XXADEO_CFA_SEARCH_OBJ AS OBJECT(
  key_col_1 VARCHAR2(30),
  key_col_2 VARCHAR2(30),
  key_col_3 VARCHAR2(30),
  key_col_4 VARCHAR2(30),
  group_id  NUMBER(10),
  cfa_value VARCHAR(250),
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_SEARCH_OBJ RETURN SELF AS RESULT,
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_SEARCH_OBJ(
    group_id NUMBER
  ) RETURN SELF AS RESULT,
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_SEARCH_OBJ(
    key_col_1 VARCHAR2,
    group_id  NUMBER,
    cfa_value VARCHAR
  ) RETURN SELF AS RESULT,
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_SEARCH_OBJ(
    key_col_1 VARCHAR2,
    key_col_2 VARCHAR2,
    group_id  NUMBER,
    cfa_value VARCHAR
  ) RETURN SELF AS RESULT,
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_SEARCH_OBJ(
    key_col_1 VARCHAR2,
    key_col_2 VARCHAR2,
    key_col_3 VARCHAR2,
    group_id  NUMBER,
    cfa_value VARCHAR
  ) RETURN SELF AS RESULT,
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_SEARCH_OBJ(
    key_col_1 VARCHAR2,
    key_col_2 VARCHAR2,
    key_col_3 VARCHAR2,
    key_col_4 VARCHAR2,
    group_id  NUMBER,
    cfa_value VARCHAR
  ) RETURN SELF AS RESULT
  --
);
/
--
CREATE OR REPLACE TYPE BODY XXADEO_CFA_SEARCH_OBJ AS
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_SEARCH_OBJ RETURN SELF AS RESULT IS
  BEGIN
    RETURN;
  END;
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_SEARCH_OBJ(
    group_id NUMBER
  ) RETURN SELF AS RESULT IS
  BEGIN
    SELF.group_id := group_id;
    RETURN;
  END;
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_SEARCH_OBJ(
    key_col_1 VARCHAR2,
    group_id  NUMBER,
    cfa_value VARCHAR
  ) RETURN SELF AS RESULT IS
  BEGIN
    SELF.key_col_1 := key_col_1;
    SELF.group_id  := group_id;
    SELF.cfa_value := cfa_value;
    RETURN;
  END;
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_SEARCH_OBJ(
    key_col_1 VARCHAR2,
    key_col_2 VARCHAR2,
    group_id  NUMBER,
    cfa_value VARCHAR
  ) RETURN SELF AS RESULT IS
  BEGIN
    SELF.key_col_1 := key_col_1;
    SELF.key_col_2 := key_col_2;
    SELF.group_id  := group_id;
    SELF.cfa_value := cfa_value;
    RETURN;
  END;
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_SEARCH_OBJ(
    key_col_1 VARCHAR2,
    key_col_2 VARCHAR2,
    key_col_3 VARCHAR2,
    group_id  NUMBER,
    cfa_value VARCHAR
  ) RETURN SELF AS RESULT IS
  BEGIN
    SELF.key_col_1 := key_col_1;
    SELF.key_col_2 := key_col_2;
    SELF.key_col_3 := key_col_3;
    SELF.group_id  := group_id;
    SELF.cfa_value := cfa_value;
    RETURN;
  END;
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_SEARCH_OBJ(
    key_col_1 VARCHAR2,
    key_col_2 VARCHAR2,
    key_col_3 VARCHAR2,
    key_col_4 VARCHAR2,
    group_id  NUMBER,
    cfa_value VARCHAR
  ) RETURN SELF AS RESULT IS
  BEGIN
    SELF.key_col_1 := key_col_1;
    SELF.key_col_2 := key_col_2;
    SELF.key_col_3 := key_col_3;
    SELF.key_col_4 := key_col_4;
    SELF.group_id  := group_id;
    SELF.cfa_value := cfa_value;
    RETURN;
  END;
  --
END;
/
