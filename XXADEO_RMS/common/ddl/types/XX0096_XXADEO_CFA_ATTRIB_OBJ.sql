/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Gustavo Martins                                              */
/* UPDATE USER - Elsa Barros                                                  */
/* PROJECT     - DEV CENTER                                                   */
/* DESCRIPTION - Object type "XXADEO_CFA_ATTRIB_OBJ"                          */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_CFA_ATTRIB_TBL';
  execute immediate 'drop type XXADEO_CFA_ATTRIB_OBJ';
exception
  when others then
    null;
end;
/   
--
CREATE OR REPLACE TYPE XXADEO_CFA_ATTRIB_OBJ AS OBJECT(
  attrib_id         NUMBER(10),
  code_type         VARCHAR2(4),
  code              VARCHAR2(6),
  base_rms_table    VARCHAR2(30),
  custom_ext_table  VARCHAR2(30),
  group_id          NUMBER(10),
  storage_col_name  VARCHAR2(30),
  storage_data_type VARCHAR2(10),
  cfa_key_val_tbl   XXADEO_CFA_KEY_VAL_TBL,
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_ATTRIB_OBJ RETURN SELF AS RESULT,
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_ATTRIB_OBJ(
    code_type       VARCHAR2,
    code            VARCHAR2,
    cfa_key_val_tbl XXADEO_CFA_KEY_VAL_TBL
  ) RETURN SELF AS RESULT,
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_ATTRIB_OBJ(
    attrib_id       NUMBER,
    cfa_key_val_tbl XXADEO_CFA_KEY_VAL_TBL
  ) RETURN SELF AS RESULT
  --
);
/
--
CREATE OR REPLACE TYPE BODY XXADEO_CFA_ATTRIB_OBJ AS
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_ATTRIB_OBJ
  RETURN SELF AS RESULT IS
  BEGIN
    RETURN;
  END;
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_ATTRIB_OBJ(
    code_type       VARCHAR2,
    code            VARCHAR2,
    cfa_key_val_tbl XXADEO_CFA_KEY_VAL_TBL
  ) RETURN SELF AS RESULT IS
  BEGIN
    SELF.code_type       := code_type;
    SELF.code            := code;
    SELF.cfa_key_val_tbl := cfa_key_val_tbl;
    RETURN;
  END;
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_ATTRIB_OBJ(
    attrib_id       NUMBER,
    cfa_key_val_tbl XXADEO_CFA_KEY_VAL_TBL
  ) RETURN SELF AS RESULT IS
  BEGIN
    SELF.attrib_id       := attrib_id;
    SELF.cfa_key_val_tbl := cfa_key_val_tbl;
    RETURN;
  END;
  --
END;
/
