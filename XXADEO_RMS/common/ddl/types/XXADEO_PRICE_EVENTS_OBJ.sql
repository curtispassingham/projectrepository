/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Object type "XXADEO_PRICE_EVENTS_OBJ" for RB34a              */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_PRICE_REQUEST_TBL';
  execute immediate 'drop type XXADEO_PRICE_EVENTS_OBJ';
exception
  when others then
    null;
end;
/
--
CREATE OR REPLACE TYPE XXADEO_PRICE_EVENTS_OBJ FORCE AS OBJECT (xxadeo_process_id         NUMBER(20),
                                                                stage_price_change_id     NUMBER(15),
                                                                reason_code               NUMBER(6),
                                                                item                      VARCHAR2(25),
                                                                diff_id                   VARCHAR2(10),
                                                                zone_id                   NUMBER(10),
                                                                location                  NUMBER(10),
                                                                zone_node_type            NUMBER(1),
                                                                link_code                 NUMBER(10),
                                                                effective_date            DATE,
                                                                change_type               NUMBER(1),
                                                                change_amount             NUMBER(20,4),
                                                                change_currency           VARCHAR2(3),
                                                                change_percent            NUMBER(20,4),
                                                                change_selling_uom        VARCHAR2(4),
                                                                null_multi_ind            NUMBER(1),
                                                                multi_units               NUMBER(12,4),
                                                                multi_unit_retail         NUMBER(20,4),
                                                                multi_selling_uom         VARCHAR2(4),
                                                                price_guide_id            NUMBER(20),
                                                                ignore_constraints        NUMBER(1),
                                                                auto_approve_ind          NUMBER(1),
                                                                status                    VARCHAR2(2),
                                                                error_message             VARCHAR2(255),
                                                                process_id                NUMBER(15),
                                                                price_change_id           NUMBER(15),
                                                                price_change_display_id   VARCHAR2(15),
                                                                skulist                   NUMBER(8),
                                                                thread_num                NUMBER(10),
                                                                exclusion_created         VARCHAR2(1),
                                                                vendor_funded_ind         NUMBER(1),
                                                                funding_type              NUMBER(1),
                                                                funding_amount            NUMBER(20,4),
                                                                funding_amount_currency   VARCHAR2(3),
                                                                funding_percent           NUMBER(20,4),
                                                                deal_id                   NUMBER(10),
                                                                deal_detail_id            NUMBER(10),
                                                                zone_group_id             NUMBER(10),
                                                                stage_cust_attr_id        NUMBER(15),
                                                                cust_attr_id              NUMBER(15),
                                                                processed_date            DATE,
                                                                create_id                 VARCHAR2(30),
                                                                create_datetime           DATE,
                                                                line_item_loc_accumulator NUMBER(6));
/
