--------------------------------------------------------
--  DDL for Type XXADEO_SupplierDesc_REC
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE TYPE "XXADEO_SupplierDesc_REC" FORCE UNDER RIB_OBJECT  (
  --------------------------------------------------------------------------------------------------------------------------------------------
   --
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload.
   --
   --------------------------------------------------------------------------------------------------------------------------------------------
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   --
   --  These variables are the payload variables which are used to construct the payload.
   --
   --------------------------------------------------------------------------------------------------------------------------------------------
  sup_xref_key varchar2(32),
  supplier_id number(10),
  supsite_xref_key varchar2(32),
  supsite_id number(10),
  SupAttr "RIB_SupAttr_REC",
  SupAddr_TBL "XXADEO_SupAddr_TBL",   -- Size is unbounded
  SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL",
  SupCFA "XXADEO_SUP_CFA_TBL",
  ExtOfSupplierDesc "RIB_ExtOfSupplierDesc_REC",
  LocOfSupplierDesc_TBL "RIB_LocOfSupplierDesc_TBL",   -- Size of "RIB_LocOfSupplierDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "XXADEO_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
, SupAttr "RIB_SupAttr_REC"
, SupAddr_TBL "XXADEO_SupAddr_TBL"
, SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL"
, SupCFA "XXADEO_SUP_CFA_TBL"  -- Size of "RIB_SupSite_TBL" is unbounded
) return self as result
,constructor function "XXADEO_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
, SupAttr "RIB_SupAttr_REC"
, SupAddr_TBL "XXADEO_SupAddr_TBL"
, SupSite boolean -- True for SupSite, False for Supplier level
, SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL"
, SupCFA "XXADEO_SUP_CFA_TBL"  -- Size of "RIB_SupSite_TBL" is unbounded
, ExtOfSupplierDesc "RIB_ExtOfSupplierDesc_REC"
) return self as result
,constructor function "XXADEO_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
, SupAttr "RIB_SupAttr_REC"
, SupAddr_TBL "XXADEO_SupAddr_TBL"   -- Size is unbounded
, SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL"
, SupCFA "XXADEO_SUP_CFA_TBL"  -- Size of "RIB_SupSite_TBL" is unbounded
, ExtOfSupplierDesc "RIB_ExtOfSupplierDesc_REC"
, LocOfSupplierDesc_TBL "RIB_LocOfSupplierDesc_TBL"  -- Size of "RIB_LocOfSupplierDesc_TBL" is unbounded
) return self as result
);
/

CREATE OR REPLACE EDITIONABLE TYPE BODY "XXADEO_SupplierDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierDesc') := "ns_name_SupplierDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'sup_xref_key') := sup_xref_key;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  l_new_pre :=i_prefix||'SupAttr.';
  SupAttr.appendNodeValues( i_prefix||'SupAttr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF SupAddr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'Addr_TBL.';
    FOR INDX IN SupAddr_TBL.FIRST()..SupAddr_TBL.LAST() LOOP
      SupAddr_TBL(indx).appendNodeValues( i_prefix||indx||'Addr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'ExtOfSupplierRef.';
  ExtOfSupplierDesc.appendNodeValues( i_prefix||'ExtOfSupplierRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfSupplierDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfSupplierRef_TBL.';
    FOR INDX IN LocOfSupplierDesc_TBL.FIRST()..LocOfSupplierDesc_TBL.LAST() LOOP
      LocOfSupplierDesc_TBL(indx).appendNodeValues(i_prefix||indx||'LocOfSupplierDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;

  constructor function "XXADEO_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
, SupAttr "RIB_SupAttr_REC"
, SupAddr_TBL"XXADEO_SupAddr_TBL"
, SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL"
, SupCFA "XXADEO_SUP_CFA_TBL"  -- Size of "RIB_SupSite_TBL" is unbounded
) return self as result AS
  BEGIN
  self.rib_oid := rib_oid;
  self.sup_xref_key := sup_xref_key;
  self.supplier_id := supplier_id;
  self.supsite_xref_key := supsite_xref_key;
  self.supsite_id := supsite_id;
  self.SupAttr := SupAttr;
  self.SupAddr_TBL:= SupAddr_TBL;
  self.SupSiteOrgUnit_TBL := SupSiteOrgUnit_TBL;
  self.SupCFA := SupCFA;
    RETURN;
  END;

  constructor function "XXADEO_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
, SupAttr "RIB_SupAttr_REC"
, SupAddr_TBL"XXADEO_SupAddr_TBL"
, SupSite boolean -- True for SupSite, False for Supplier level
, SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL"
, SupCFA "XXADEO_SUP_CFA_TBL"  -- Size of "RIB_SupSite_TBL" is unbounded
, ExtOfSupplierDesc "RIB_ExtOfSupplierDesc_REC"
) return self as result AS
  BEGIN
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
self.supsite_xref_key := supsite_xref_key;
self.supsite_id := supsite_id;
self.SupAttr := SupAttr;
self.SupAddr_TBL:= SupAddr_TBL;
self.SupSiteOrgUnit_TBL := SupSiteOrgUnit_TBL;
self.SupCFA := SupCFA;
self.ExtOfSupplierDesc := ExtOfSupplierDesc;
    RETURN;
  END;

  constructor function "XXADEO_SupplierDesc_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, supsite_xref_key varchar2
, supsite_id number
, SupAttr "RIB_SupAttr_REC"
, SupAddr_TBL "XXADEO_SupAddr_TBL"   -- Size is unbounded
, SupSiteOrgUnit_TBL "RIB_SupSiteOrgUnit_TBL"
, SupCFA "XXADEO_SUP_CFA_TBL"  -- Size of "RIB_SupSite_TBL" is unbounded
, ExtOfSupplierDesc "RIB_ExtOfSupplierDesc_REC"
, LocOfSupplierDesc_TBL "RIB_LocOfSupplierDesc_TBL"  -- Size of "RIB_LocOfSupplierDesc_TBL" is unbounded
) return self as result AS
  BEGIN
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
self.supsite_xref_key := supsite_xref_key;
self.supsite_id := supsite_id;
self.SupAttr := SupAttr;
self.SupAddr_TBL:= SupAddr_TBL;
self.SupSiteOrgUnit_TBL := SupSiteOrgUnit_TBL;
self.SupCFA := SupCFA;
self.ExtOfSupplierDesc := ExtOfSupplierDesc;
self.LocOfSupplierDesc_TBL := LocOfSupplierDesc_TBL;
    RETURN;
  END;

END;

/