/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_ITEM_SUP_CTY_REC
* Description:   Type used by XITEM
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
DROP TYPE XXADEO_ITEM_SUP_CTY_REC FORCE;
CREATE OR REPLACE TYPE XXADEO_ITEM_SUP_CTY_REC force IS OBJECT(
ORIGIN_COUNTRY_ID VARCHAR2(3),
PRIMARY_IND VARCHAR2(1),
DEFAULT_UOP VARCHAR2(6)  ,
supp_pack_size NUMBER(12,4),
inner_pack_size NUMBER(12,4),
ti NUMBER(12,4),
hi NUMBER(12,4),
COST_UOM VARCHAR2(4)
);
/
