DROP TYPE "RIB_ExtOfItemSupDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ExtOfItemSupDesc_REC";
/
DROP TYPE "RIB_CustFlexAttriVo_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_CustFlexAttriVo_TBL" AS TABLE OF "RIB_CustFlexAttriVo_REC";
/
DROP TYPE "RIB_ExtOfItemSupDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ExtOfItemSupDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ExtOfItemSupDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_custom" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL",   -- Size of "RIB_CustFlexAttriVo_TBL" is unbounded
  ListOfBU "RIB_ListOfBU_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ExtOfItemSupDesc_REC"
(
  rib_oid number
, CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL"  -- Size of "RIB_CustFlexAttriVo_TBL" is unbounded
) return self as result
,constructor function "RIB_ExtOfItemSupDesc_REC"
(
  rib_oid number
, CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL"  -- Size of "RIB_CustFlexAttriVo_TBL" is unbounded
, ListOfBU "RIB_ListOfBU_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ExtOfItemSupDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ExtOfItemSupDesc') := "ns_name_ExtOfItemSupDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_custom') := "ns_location_custom";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF CustFlexAttriVo_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'CustFlexAttriVo_TBL.';
    FOR INDX IN CustFlexAttriVo_TBL.FIRST()..CustFlexAttriVo_TBL.LAST() LOOP
      CustFlexAttriVo_TBL(indx).appendNodeValues( i_prefix||indx||'CustFlexAttriVo_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'ListOfBU.';
  ListOfBU.appendNodeValues( i_prefix||'ListOfBU');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ExtOfItemSupDesc_REC"
(
  rib_oid number
, CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustFlexAttriVo_TBL := CustFlexAttriVo_TBL;
RETURN;
end;
constructor function "RIB_ExtOfItemSupDesc_REC"
(
  rib_oid number
, CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL"
, ListOfBU "RIB_ListOfBU_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustFlexAttriVo_TBL := CustFlexAttriVo_TBL;
self.ListOfBU := ListOfBU;
RETURN;
end;
END;
/
