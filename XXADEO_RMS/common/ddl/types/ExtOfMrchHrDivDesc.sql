DROP TYPE "RIB_ExtOfMrchHrDivDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ExtOfMrchHrDivDesc_REC";
/
DROP TYPE "RIB_LangOfMrchHrDivDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfMrchHrDivDesc_REC";
/
DROP TYPE "RIB_LangOfMrchHrDivDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfMrchHrDivDesc_TBL" AS TABLE OF "RIB_LangOfMrchHrDivDesc_REC";
/
DROP TYPE "RIB_ExtOfMrchHrDivDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ExtOfMrchHrDivDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ExtOfMrchHrDivDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_custom" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  LangOfMrchHrDivDesc_TBL "RIB_LangOfMrchHrDivDesc_TBL",   -- Size of "RIB_LangOfMrchHrDivDesc_TBL" is unbounded
  create_datetime date,
  last_update_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ExtOfMrchHrDivDesc_REC"
(
  rib_oid number
, LangOfMrchHrDivDesc_TBL "RIB_LangOfMrchHrDivDesc_TBL"  -- Size of "RIB_LangOfMrchHrDivDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_ExtOfMrchHrDivDesc_REC"
(
  rib_oid number
, LangOfMrchHrDivDesc_TBL "RIB_LangOfMrchHrDivDesc_TBL"  -- Size of "RIB_LangOfMrchHrDivDesc_TBL" is unbounded
, create_datetime date
) return self as result
,constructor function "RIB_ExtOfMrchHrDivDesc_REC"
(
  rib_oid number
, LangOfMrchHrDivDesc_TBL "RIB_LangOfMrchHrDivDesc_TBL"  -- Size of "RIB_LangOfMrchHrDivDesc_TBL" is unbounded
, create_datetime date
, last_update_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ExtOfMrchHrDivDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ExtOfMrchHrDivDesc') := "ns_name_ExtOfMrchHrDivDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_custom') := "ns_location_custom";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF LangOfMrchHrDivDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LangOfMrchHrDivDesc_TBL.';
    FOR INDX IN LangOfMrchHrDivDesc_TBL.FIRST()..LangOfMrchHrDivDesc_TBL.LAST() LOOP
      LangOfMrchHrDivDesc_TBL(indx).appendNodeValues( i_prefix||indx||'LangOfMrchHrDivDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_datetime') := create_datetime;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_update_date') := last_update_date;
END appendNodeValues;
constructor function "RIB_ExtOfMrchHrDivDesc_REC"
(
  rib_oid number
, LangOfMrchHrDivDesc_TBL "RIB_LangOfMrchHrDivDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.LangOfMrchHrDivDesc_TBL := LangOfMrchHrDivDesc_TBL;
RETURN;
end;
constructor function "RIB_ExtOfMrchHrDivDesc_REC"
(
  rib_oid number
, LangOfMrchHrDivDesc_TBL "RIB_LangOfMrchHrDivDesc_TBL"
, create_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.LangOfMrchHrDivDesc_TBL := LangOfMrchHrDivDesc_TBL;
self.create_datetime := create_datetime;
RETURN;
end;
constructor function "RIB_ExtOfMrchHrDivDesc_REC"
(
  rib_oid number
, LangOfMrchHrDivDesc_TBL "RIB_LangOfMrchHrDivDesc_TBL"
, create_datetime date
, last_update_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.LangOfMrchHrDivDesc_TBL := LangOfMrchHrDivDesc_TBL;
self.create_datetime := create_datetime;
self.last_update_date := last_update_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_LangOfMrchHrDivDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LangOfMrchHrDivDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ExtOfMrchHrDivDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_custom" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  lang_id number(6),
  lang_iso_code varchar2(3),
  div_name_tl varchar2(120),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LangOfMrchHrDivDesc_REC"
(
  rib_oid number
, lang_id number
) return self as result
,constructor function "RIB_LangOfMrchHrDivDesc_REC"
(
  rib_oid number
, lang_id number
, lang_iso_code varchar2
) return self as result
,constructor function "RIB_LangOfMrchHrDivDesc_REC"
(
  rib_oid number
, lang_id number
, lang_iso_code varchar2
, div_name_tl varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LangOfMrchHrDivDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ExtOfMrchHrDivDesc') := "ns_name_ExtOfMrchHrDivDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_custom') := "ns_location_custom";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'lang_id') := lang_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'lang_iso_code') := lang_iso_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'div_name_tl') := div_name_tl;
END appendNodeValues;
constructor function "RIB_LangOfMrchHrDivDesc_REC"
(
  rib_oid number
, lang_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang_id := lang_id;
RETURN;
end;
constructor function "RIB_LangOfMrchHrDivDesc_REC"
(
  rib_oid number
, lang_id number
, lang_iso_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang_id := lang_id;
self.lang_iso_code := lang_iso_code;
RETURN;
end;
constructor function "RIB_LangOfMrchHrDivDesc_REC"
(
  rib_oid number
, lang_id number
, lang_iso_code varchar2
, div_name_tl varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang_id := lang_id;
self.lang_iso_code := lang_iso_code;
self.div_name_tl := div_name_tl;
RETURN;
end;
END;
/
