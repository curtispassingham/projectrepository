/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Object type "XXADEO_PRICE_REQUEST_OBJ" for RB34a             */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_PRICE_REQUEST_TBL';
  execute immediate 'drop type XXADEO_PRICE_REQUEST_OBJ';
exception
  when others then
    null;
end;
/
--
create or replace type XXADEO_PRICE_REQUEST_OBJ FORCE as object (request_process_id  number(20),
                                                                 type                varchar2(10),
                                                                 action              varchar2(10),
                                                                 process_order       NUMBER(10),
                                                                 dependency          NUMBER(20),
                                                                 pe_process_id       NUMBER(20));
/
