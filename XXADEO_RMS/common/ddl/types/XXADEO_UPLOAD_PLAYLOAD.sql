/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Object type "XXADEO_UPLOAD_PLAYLOAD" for custom upload       */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_UPLOAD_PLAYLOAD';
exception
  when others then
    null;
end;
/
--
create or replace type XXADEO_upload_playload as object(
  xxadeo_process_id NUMBER,
  file_id           VARCHAR2(255)
);
/
