/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table type "XXADEO_PRICE_REQUEST_TBL" for RB34a              */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_PRICE_REQUEST_TBL';
exception
  when others then
    null;
end;
/
--
create or replace type XXADEO_PRICE_REQUEST_TBL as table of XXADEO_PRICE_REQUEST_OBJ;
/
