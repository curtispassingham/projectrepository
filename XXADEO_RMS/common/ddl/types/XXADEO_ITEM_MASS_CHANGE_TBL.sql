/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table type "XXADEO_ITEM_MASS_CHANGE_TBL"                     */
/******************************************************************************/
 
begin
  execute immediate 'drop type XXADEO_ITEM_MASS_CHANGE_TBL';
exception
  when others then
    null;
end;
/

create or replace type XXADEO_ITEM_MASS_CHANGE_TBL AS TABLE OF XXADEO_ITEM_MASS_CHANGE_OBJ;
/  