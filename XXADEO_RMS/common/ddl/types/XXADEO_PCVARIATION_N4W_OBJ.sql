/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Object type "XXADEO_PCVARIATION_N4W_OBJ" for RR425/426       */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_PCVARIATION_N4W_TBL';
exception
  when others then
    null;
end;
/
begin
  execute immediate 'drop type XXADEO_PCVARIATION_N4W_OBJ';
exception
  when others then
    null;
end;
/
create or replace type XXADEO_PCVARIATION_N4W_OBJ FORCE as OBJECT(
  xdate          DATE,
  total          NUMBER,
  col_order      VARCHAR2(1)
);
/
