/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table object "XXADEO_PCAMOUNT_N4W_TBL" for RR425/426         */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_PCAMOUNT_N4W_TBL';
exception
  when others then
    null;
end;
/
create or replace type XXADEO_PCAMOUNT_N4W_TBL as table of XXADEO_PCAMOUNT_N4W_OBJ;
/

