/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_UDA_FF_REC
* Description:   Type used by XITEM
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
DROP TYPE XXADEO_UDA_FF_REC FORCE;
CREATE OR REPLACE TYPE XXADEO_UDA_FF_REC IS OBJECT(
    uda_id               NUMBER(5),
    uda_text             VARCHAR2(250),
    create_datetime      date,
    last_update_datetime date,
    last_update_id       VARCHAR2(30));

	/
	