/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Object type "XXADEO_ITEM_RESULT_OBJ"                         */
/******************************************************************************/

begin
  execute immediate 'drop type XXADEO_ITEM_RESULT_TBL';
exception
  when others then
    null;
end;
/

begin
  execute immediate 'drop type XXADEO_ITEM_RESULT_OBJ';
exception
  when others then
    null;
end;
/

create or replace type XXADEO_ITEM_RESULT_OBJ as OBJECT(
filter_org_id NUMBER(10),
status VARCHAR2(1),
pack_ind VARCHAR2(1),
pack_type VARCHAR2(1),
item_level NUMBER(1),
tran_level NUMBER(1),
check_uda_ind VARCHAR2(1),
group_no NUMBER(4),
group_name VARCHAR2(250),
dept NUMBER(4),
dept_name VARCHAR2(250),
class NUMBER(4),
class_name VARCHAR2(250),
subclass NUMBER(4),
sub_name VARCHAR2(250),
classment_bu NUMBER(12,4),
item VARCHAR2(25),
item_desc VARCHAR2(250),
sous_typo_step VARCHAR2(250),
unite_consommateur VARCHAR2(4),
qt_contenance NUMBER(12,4),
un_contenance VARCHAR2(4),
mode_assort VARCHAR2(250),
taille_gamme VARCHAR2(250),
prix_cession_standard VARCHAR2(250),
prix_vente_conseil VARCHAR2(250),
margin VARCHAR2(250),
pv_principal_pub VARCHAR2(250),
modif_pv_magasin VARCHAR2(250),
etat_cycle_vie VARCHAR2(250),
date_souh_pass_actif_comm DATE,
top_1000  VARCHAR2(250),
top_hyper_100 VARCHAR2(250),
top_mdh  VARCHAR2(250),
modif_pa_magasin VARCHAR2(250),
actif_comm_date_eff DATE,
forecast_ind VARCHAR2(1)
);
/
   