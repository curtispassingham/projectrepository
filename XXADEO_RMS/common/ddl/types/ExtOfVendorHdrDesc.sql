DROP TYPE "RIB_ExtOfVendorHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ExtOfVendorHdrDesc_REC";
/
DROP TYPE "RIB_VendorBU_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VendorBU_REC";
/
DROP TYPE "RIB_VendorBU_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_VendorBU_TBL" AS TABLE OF "RIB_VendorBU_REC";
/
DROP TYPE "RIB_CustFlexAttriVo_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_CustFlexAttriVo_TBL" AS TABLE OF "RIB_CustFlexAttriVo_REC";
/
DROP TYPE "RIB_ExtOfVendorHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ExtOfVendorHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ExtOfVendorHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_custom" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  supplier_parent_name varchar2(240),
  partner_1 varchar2(10),
  partner_type_1 varchar2(6),
  partner_2 varchar2(10),
  partner_type_2 varchar2(6),
  partner_3 varchar2(10),
  partner_type_3 varchar2(6),
  purchase_type varchar2(6),
  pickup_lock varchar2(250),
  create_date date,
  pas_supplier_ind_supp varchar2(1),
  VendorBU_TBL "RIB_VendorBU_TBL",   -- Size of "RIB_VendorBU_TBL" is unbounded
  CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL",   -- Size of "RIB_CustFlexAttriVo_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
) return self as result
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
) return self as result
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
) return self as result
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
) return self as result
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
) return self as result
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
) return self as result
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
) return self as result
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
, purchase_type varchar2
) return self as result
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
, purchase_type varchar2
, pickup_lock varchar2
) return self as result
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
, purchase_type varchar2
, pickup_lock varchar2
, create_date date
) return self as result
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
, purchase_type varchar2
, pickup_lock varchar2
, create_date date
, pas_supplier_ind_supp varchar2
) return self as result
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
, purchase_type varchar2
, pickup_lock varchar2
, create_date date
, pas_supplier_ind_supp varchar2
, VendorBU_TBL "RIB_VendorBU_TBL"  -- Size of "RIB_VendorBU_TBL" is unbounded
) return self as result
,constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
, purchase_type varchar2
, pickup_lock varchar2
, create_date date
, pas_supplier_ind_supp varchar2
, VendorBU_TBL "RIB_VendorBU_TBL"  -- Size of "RIB_VendorBU_TBL" is unbounded
, CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL"  -- Size of "RIB_CustFlexAttriVo_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ExtOfVendorHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ExtOfVendorHdrDesc') := "ns_name_ExtOfVendorHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_custom') := "ns_location_custom";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_parent_name') := supplier_parent_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'partner_1') := partner_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'partner_type_1') := partner_type_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'partner_2') := partner_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'partner_type_2') := partner_type_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'partner_3') := partner_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'partner_type_3') := partner_type_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_type') := purchase_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_lock') := pickup_lock;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'pas_supplier_ind_supp') := pas_supplier_ind_supp;
  IF VendorBU_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'VendorBU_TBL.';
    FOR INDX IN VendorBU_TBL.FIRST()..VendorBU_TBL.LAST() LOOP
      VendorBU_TBL(indx).appendNodeValues( i_prefix||indx||'VendorBU_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF CustFlexAttriVo_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'CustFlexAttriVo_TBL.';
    FOR INDX IN CustFlexAttriVo_TBL.FIRST()..CustFlexAttriVo_TBL.LAST() LOOP
      CustFlexAttriVo_TBL(indx).appendNodeValues( i_prefix||indx||'CustFlexAttriVo_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
RETURN;
end;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
self.partner_1 := partner_1;
RETURN;
end;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
self.partner_1 := partner_1;
self.partner_type_1 := partner_type_1;
RETURN;
end;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
self.partner_1 := partner_1;
self.partner_type_1 := partner_type_1;
self.partner_2 := partner_2;
RETURN;
end;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
self.partner_1 := partner_1;
self.partner_type_1 := partner_type_1;
self.partner_2 := partner_2;
self.partner_type_2 := partner_type_2;
RETURN;
end;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
self.partner_1 := partner_1;
self.partner_type_1 := partner_type_1;
self.partner_2 := partner_2;
self.partner_type_2 := partner_type_2;
self.partner_3 := partner_3;
RETURN;
end;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
self.partner_1 := partner_1;
self.partner_type_1 := partner_type_1;
self.partner_2 := partner_2;
self.partner_type_2 := partner_type_2;
self.partner_3 := partner_3;
self.partner_type_3 := partner_type_3;
RETURN;
end;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
, purchase_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
self.partner_1 := partner_1;
self.partner_type_1 := partner_type_1;
self.partner_2 := partner_2;
self.partner_type_2 := partner_type_2;
self.partner_3 := partner_3;
self.partner_type_3 := partner_type_3;
self.purchase_type := purchase_type;
RETURN;
end;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
, purchase_type varchar2
, pickup_lock varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
self.partner_1 := partner_1;
self.partner_type_1 := partner_type_1;
self.partner_2 := partner_2;
self.partner_type_2 := partner_type_2;
self.partner_3 := partner_3;
self.partner_type_3 := partner_type_3;
self.purchase_type := purchase_type;
self.pickup_lock := pickup_lock;
RETURN;
end;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
, purchase_type varchar2
, pickup_lock varchar2
, create_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
self.partner_1 := partner_1;
self.partner_type_1 := partner_type_1;
self.partner_2 := partner_2;
self.partner_type_2 := partner_type_2;
self.partner_3 := partner_3;
self.partner_type_3 := partner_type_3;
self.purchase_type := purchase_type;
self.pickup_lock := pickup_lock;
self.create_date := create_date;
RETURN;
end;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
, purchase_type varchar2
, pickup_lock varchar2
, create_date date
, pas_supplier_ind_supp varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
self.partner_1 := partner_1;
self.partner_type_1 := partner_type_1;
self.partner_2 := partner_2;
self.partner_type_2 := partner_type_2;
self.partner_3 := partner_3;
self.partner_type_3 := partner_type_3;
self.purchase_type := purchase_type;
self.pickup_lock := pickup_lock;
self.create_date := create_date;
self.pas_supplier_ind_supp := pas_supplier_ind_supp;
RETURN;
end;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
, purchase_type varchar2
, pickup_lock varchar2
, create_date date
, pas_supplier_ind_supp varchar2
, VendorBU_TBL "RIB_VendorBU_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
self.partner_1 := partner_1;
self.partner_type_1 := partner_type_1;
self.partner_2 := partner_2;
self.partner_type_2 := partner_type_2;
self.partner_3 := partner_3;
self.partner_type_3 := partner_type_3;
self.purchase_type := purchase_type;
self.pickup_lock := pickup_lock;
self.create_date := create_date;
self.pas_supplier_ind_supp := pas_supplier_ind_supp;
self.VendorBU_TBL := VendorBU_TBL;
RETURN;
end;
constructor function "RIB_ExtOfVendorHdrDesc_REC"
(
  rib_oid number
, supplier_parent_name varchar2
, partner_1 varchar2
, partner_type_1 varchar2
, partner_2 varchar2
, partner_type_2 varchar2
, partner_3 varchar2
, partner_type_3 varchar2
, purchase_type varchar2
, pickup_lock varchar2
, create_date date
, pas_supplier_ind_supp varchar2
, VendorBU_TBL "RIB_VendorBU_TBL"
, CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_parent_name := supplier_parent_name;
self.partner_1 := partner_1;
self.partner_type_1 := partner_type_1;
self.partner_2 := partner_2;
self.partner_type_2 := partner_type_2;
self.partner_3 := partner_3;
self.partner_type_3 := partner_type_3;
self.purchase_type := purchase_type;
self.pickup_lock := pickup_lock;
self.create_date := create_date;
self.pas_supplier_ind_supp := pas_supplier_ind_supp;
self.VendorBU_TBL := VendorBU_TBL;
self.CustFlexAttriVo_TBL := CustFlexAttriVo_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_VendorBU_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VendorBU_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ExtOfVendorHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_custom" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  bu number(10),
  payment_supplier varchar2(10),
  payment_supplier_name varchar2(240),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VendorBU_REC"
(
  rib_oid number
, bu number
) return self as result
,constructor function "RIB_VendorBU_REC"
(
  rib_oid number
, bu number
, payment_supplier varchar2
) return self as result
,constructor function "RIB_VendorBU_REC"
(
  rib_oid number
, bu number
, payment_supplier varchar2
, payment_supplier_name varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VendorBU_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ExtOfVendorHdrDesc') := "ns_name_ExtOfVendorHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_custom') := "ns_location_custom";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'bu') := bu;
  rib_obj_util.g_RIB_element_values(i_prefix||'payment_supplier') := payment_supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'payment_supplier_name') := payment_supplier_name;
END appendNodeValues;
constructor function "RIB_VendorBU_REC"
(
  rib_oid number
, bu number
) return self as result is
begin
self.rib_oid := rib_oid;
self.bu := bu;
RETURN;
end;
constructor function "RIB_VendorBU_REC"
(
  rib_oid number
, bu number
, payment_supplier varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bu := bu;
self.payment_supplier := payment_supplier;
RETURN;
end;
constructor function "RIB_VendorBU_REC"
(
  rib_oid number
, bu number
, payment_supplier varchar2
, payment_supplier_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bu := bu;
self.payment_supplier := payment_supplier;
self.payment_supplier_name := payment_supplier_name;
RETURN;
end;
END;
/
