/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_ITEM_IMAGE_TL_REC
* Description:   Type used by XITEM
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
DROP TYPE XXADEO_ITEM_IMAGE_TL_REC FORCE;
create or replace type XXADEO_ITEM_IMAGE_TL_REC is object(
lang  number,
image_desc varchar2(250)
);
/