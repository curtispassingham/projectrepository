/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Gustavo Martins                                              */
/* UPDATE USER - Elsa Barros                                                  */
/* PROJECT     - DEV CENTER                                                   */
/* DESCRIPTION - Object type "XXADEO_CFA_KEY_VAL_OBJ"                         */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_CFA_ATTRIB_TBL';
  execute immediate 'drop type XXADEO_CFA_ATTRIB_OBJ';
  execute immediate 'drop type XXADEO_CFA_KEY_VAL_TBL';
  execute immediate 'drop type XXADEO_CFA_KEY_VAL_OBJ';
exception
  when others then
    null;
end; 
/  
--
CREATE OR REPLACE TYPE XXADEO_CFA_KEY_VAL_OBJ AS OBJECT(
  key_col_1            VARCHAR2(30),
  key_col_2            VARCHAR2(30),
  key_col_3            VARCHAR2(30),
  key_col_4            VARCHAR2(30),
  key_value_1          VARCHAR2(250),
  key_value_2          VARCHAR2(250),
  key_value_3          VARCHAR2(250),
  key_value_4          VARCHAR2(250),
  attrib_varchar_value VARCHAR2(250),
  attrib_number_value  NUMBER,
  attrib_date_value    DATE,
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_KEY_VAL_OBJ(
    key_col_1            VARCHAR2 DEFAULT NULL,
    key_col_2            VARCHAR2 DEFAULT NULL,
    key_col_3            VARCHAR2 DEFAULT NULL,
    key_col_4            VARCHAR2 DEFAULT NULL,
    key_value_1          VARCHAR2 DEFAULT NULL,
    key_value_2          VARCHAR2 DEFAULT NULL,
    key_value_3          VARCHAR2 DEFAULT NULL,
    key_value_4          VARCHAR2 DEFAULT NULL,
    attrib_varchar_value VARCHAR2 DEFAULT NULL,
    attrib_number_value  NUMBER   DEFAULT NULL,
    attrib_date_value    DATE     DEFAULT NULL
  ) RETURN SELF AS RESULT
  --
);
/
--
CREATE OR REPLACE TYPE BODY XXADEO_CFA_KEY_VAL_OBJ AS
  --
  CONSTRUCTOR FUNCTION XXADEO_CFA_KEY_VAL_OBJ(
    key_col_1            VARCHAR2 DEFAULT NULL,
    key_col_2            VARCHAR2 DEFAULT NULL,
    key_col_3            VARCHAR2 DEFAULT NULL,
    key_col_4            VARCHAR2 DEFAULT NULL,
    key_value_1          VARCHAR2 DEFAULT NULL,
    key_value_2          VARCHAR2 DEFAULT NULL,
    key_value_3          VARCHAR2 DEFAULT NULL,
    key_value_4          VARCHAR2 DEFAULT NULL,
    attrib_varchar_value VARCHAR2 DEFAULT NULL,
    attrib_number_value  NUMBER   DEFAULT NULL,
    attrib_date_value    DATE     DEFAULT NULL
  ) RETURN SELF AS RESULT IS
  BEGIN
    SELF.key_col_1            := key_col_1;
    SELF.key_col_2            := key_col_2;
    SELF.key_col_3            := key_col_3;
    SELF.key_col_4            := key_col_4;
    SELF.key_value_1          := key_value_1;
    SELF.key_value_2          := key_value_2;
    SELF.key_value_3          := key_value_3;
    SELF.key_value_4          := key_value_4;
    SELF.attrib_varchar_value := attrib_varchar_value;
    SELF.attrib_number_value  := attrib_number_value;
    SELF.attrib_date_value    := attrib_date_value;
    --
    RETURN;
  END;
  --
END;
/
