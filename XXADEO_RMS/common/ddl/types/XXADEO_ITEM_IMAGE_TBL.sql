/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_ITEM_IMAGE_TBL
* Description:   Table type used by XITEM
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
DROP TYPE XXADEO_ITEM_IMAGE_TBL FORCE;
CREATE OR REPLACE TYPE XXADEO_ITEM_IMAGE_TBL is table of XXADEO_ITEM_IMAGE_REC;
/
