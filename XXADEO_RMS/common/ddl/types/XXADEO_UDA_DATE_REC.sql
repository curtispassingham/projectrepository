/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_UDA_DATE_REC
* Description:   Type used by XITEM
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
DROP TYPE XXADEO_UDA_DATE_REC FORCE;
CREATE OR REPLACE TYPE XXADEO_UDA_DATE_REC FORCE AS OBJECT (
    uda_id               NUMBER(5),
    uda_date             date,
    create_datetime      date,
    last_update_datetime date,
    last_update_id       VARCHAR2(30)
	);
	/
	