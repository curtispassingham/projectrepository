/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_ITEM_SUP_CTY_DIM_REC
* Description:   Type used by XITEM 
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
DROP TYPE XXADEO_ITEM_SUP_CTY_DIM_REC FORCE;
create or replace type XXADEO_ITEM_SUP_CTY_DIM_REC as object(
DIM_OBJECT VARCHAR2(6),
lwh_uom VARCHAR2(4),
LENGTH NUMBER(12,4) ,
WIDTH NUMBER(12,4) ,
HEIGHT NUMBER(12,4) ,
stat_cube	varchar2(12),
weight_uom	varchar2(4),
WEIGHT NUMBER(12,4) ,
NET_WEIGHT NUMBER(12,4)
);
/

