/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table type "XXADEO_WKSHT_COLUMN_IDX_TBL"                     */
/******************************************************************************/
begin
  execute immediate 'drop type xxadeo_wksht_column_idx_tbl';
exception
  when others then
    null;
end;
/
--
CREATE OR REPLACE TYPE xxadeo_wksht_column_idx_tbl AS TABLE OF xxadeo_wksht_column_idx_obj;
/
