/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Object type "XXADEO_ITEM_MASS_CHANGE_OBJ"                    */
/******************************************************************************/
 
begin
  execute immediate 'drop type XXADEO_ITEM_MASS_CHANGE_TBL';
exception
  when others then
    null;
end;
/

begin
  execute immediate 'drop type XXADEO_ITEM_MASS_CHANGE_OBJ';
exception
  when others then
    null;
end;
/ 

create or replace type XXADEO_ITEM_MASS_CHANGE_OBJ as OBJECT(
BU NUMBER,
ITEM_ID VARCHAR2(250));
/
    