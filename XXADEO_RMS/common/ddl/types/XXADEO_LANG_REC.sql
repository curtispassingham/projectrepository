--------------------------------------------------------
--  File created - Thursday-May-24-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Type XXADEO_LANG_REC
--------------------------------------------------------

  CREATE OR REPLACE TYPE "XXADEO_LANG_REC" FORCE AS OBJECT 
( 
       lang_id         NUMBER(6),
       lang_iso_code   VARCHAR2(23),
       name_tl     VARCHAR2(120)
)


/
