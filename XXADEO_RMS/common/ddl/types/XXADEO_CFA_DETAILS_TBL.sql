/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_CFA_DETAILS_TBL
* Description:   Table type used by XXADEO_CFAS_UTILS
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/

DROP TYPE XXADEO_CFA_DETAILS_TBL FORCE;	
CREATE OR REPLACE TYPE XXADEO_CFA_DETAILS_TBL IS TABLE OF XXADEO_CFA_DETAILS;
/
