DROP TYPE "RIB_ListOfBU_REC" FORCE;

CREATE OR REPLACE TYPE "RIB_bu_TBL" AS TABLE OF number(10);
/

CREATE OR REPLACE TYPE "RIB_ListOfBU_REC";
/
CREATE OR REPLACE TYPE "RIB_ListOfBU_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ExtOfItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_custom" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  bu "RIB_bu_TBL",   -- Size of "RIB_bu_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ListOfBU_REC"
(
  rib_oid number
, bu "RIB_bu_TBL"  -- Size of "RIB_bu_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ListOfBU_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ExtOfItemDesc') := "ns_name_ExtOfItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_custom') := "ns_location_custom";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF bu IS NOT NULL THEN
    FOR INDX IN bu.FIRST()..bu.LAST() LOOP
      l_new_pre :=i_prefix||indx||'bu'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'bu'||'.'):=bu(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ListOfBU_REC"
(
  rib_oid number
, bu "RIB_bu_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.bu := bu;
RETURN;
end;
END;
/
