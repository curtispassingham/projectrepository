/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_ITEM_IMAGE_REC
* Description:   Type used by XITEM
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
DROP TYPE XXADEO_ITEM_IMAGE_REC FORCE;
CREATE OR REPLACE TYPE XXADEO_ITEM_IMAGE_REC AS OBJECT(
       image_name   VARCHAR2(120),
       image_addr   VARCHAR2(255),
       image_desc   VARCHAR2(40)
  );
  /
  
