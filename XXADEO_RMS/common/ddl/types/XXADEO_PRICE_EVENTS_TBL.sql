/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Table type "XXADEO_PRICE_EVENTS_TBL" for RB34a               */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_PRICE_EVENTS_TBL';
exception
  when others then
    null;
end;
/
--
CREATE OR REPLACE TYPE XXADEO_PRICE_EVENTS_TBL AS TABLE OF XXADEO_PRICE_EVENTS_OBJ;
/
