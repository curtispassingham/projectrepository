/******************************************************************************/
/* CREATE DATE - January 2019                                                 */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Object type "XXADEO_PACK_DERU_SUPS_OBJ" for RB148            */
/******************************************************************************/

begin
  execute immediate 'drop type XXADEO_PACK_DERU_SUPS_TBL';
exception
  when others then
    null;
end;
/

begin
  execute immediate 'drop type XXADEO_PACK_DERU_SUPS_OBJ';
exception
  when others then
    null;
end;
/

create or replace type XXADEO_PACK_DERU_SUPS_OBJ as OBJECT
(
  flag                  varchar2(30),
  agg_item              varchar2(25),
  item                  varchar2(25),
  status                varchar2(1),
  pack_ind              varchar2(1),
  pack_type             varchar2(1),
  item_level            number(1),
  tran_level            number(1),
  check_uda_ind         varchar2(1),
  supp_ind              varchar2(1),
  group_no              number,
  group_name            varchar2(120),
  dept                  number(4),
  dept_name             varchar2(120),
  class                 number(4),
  class_name            varchar2(120),
  subclass              number(4),
  sub_name              varchar2(120),
  ranking_item          number,
  item_desc             varchar2(250),
  derunit_conv_factor   number,
  qt_capacity           number,
  contain_uom_item      varchar2(250),
  assortment_mode       varchar2(250),
  range_size            varchar2(250),
  orderable_ind         varchar2(1),
  sellable_ind          varchar2(1),
  pack_qty              varchar2(25),
  retail_prc_blocking   varchar2(250),
  lifecycle_status      varchar2(250),
  actif_ddate_item      date,
  forecast_ind          varchar2(1),
  sous_typo_step        varchar2(250),
  standard_uom          varchar2(4),
  supplier_site         number,
  supplier_site_desc    varchar2(240),
  manufacturer_code     varchar2(10),
  origin_country        varchar2(3),
  status_link           varchar2(250),  
  gtin                  varchar2(25),
  supplier_pack_size    number,
  supplier_cost_price   number,
  currency              varchar2(3),
  std_str_tr_prc_itemprc varchar2(120),
  national_price_itmprc number,
  margin_itmprc         varchar2(120),
  margin_perc           varchar2(120),
  blocking_reason       varchar2(250),
  qual_comp_status      varchar2(250),
  link_end_date         date,
  supp_bu               number,
  item_bu               number(5)
);
/
   