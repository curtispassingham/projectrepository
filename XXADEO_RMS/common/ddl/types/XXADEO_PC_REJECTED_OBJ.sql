/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Object type "XXADEO_PC_REJECTED_OBJ" for RB34a              */
/******************************************************************************/
begin
  execute immediate 'drop type XXADEO_PC_REJECTED_TBL';
  execute immediate 'drop type XXADEO_PC_REJECTED_OBJ';
exception
  when others then
    null;
end;
/
--
create or replace type XXADEO_PC_REJECTED_OBJ FORCE as object (process_id NUMBER(15),
                                                               price_change_id NUMBER(15),
                                                               price_change_display_id VARCHAR2(15),
                                                               change_type NUMBER(1),
                                                               change_amount NUMBER(20,4),
                                                               change_currency VARCHAR2(3),
                                                               change_percent NUMBER(20,4),
                                                               selling_uom VARCHAR2(4),
                                                               multi_units NUMBER(12,4),
                                                               multi_unit_retail NUMBER(20,4),
                                                               multi_unit_retail_currency VARCHAR2(3),
                                                               multi_selling_uom VARCHAR2(4),
                                                               effective_date DATE,
                                                               item VARCHAR2(25),
                                                               dept NUMBER(4),
                                                               class NUMBER(4), 
                                                               subclass NUMBER(4),
                                                               location NUMBER(10),
                                                               zone_id NUMBER(10),
                                                               zone_node_type NUMBER(1),
                                                               error_message VARCHAR2(255),
                                                               action VARCHAR2(2),
                                                               status VARCHAR2(2),
                                                               user_id VARCHAR2(30),
                                                               create_datetime DATE,
                                                               source VARCHAR2(10))  ;
/   
