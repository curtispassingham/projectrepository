DROP TYPE "RIB_ExtOfBarcodeDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ExtOfBarcodeDesc_REC";
/
DROP TYPE "RIB_ExtOfXItemSupDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ExtOfXItemSupDesc_REC";
/
DROP TYPE "RIB_ExtOfReltdItem_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ExtOfReltdItem_REC";
/
DROP TYPE "RIB_ExtOfXItemDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ExtOfXItemDesc_REC";
/
DROP TYPE "RIB_ExtOfBarcodeDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ExtOfBarcodeDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ExtOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_custom" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  barcode_id varchar2(25),
  type varchar2(6),
  supplier varchar2(10),
  primary_ind varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ExtOfBarcodeDesc_REC"
(
  rib_oid number
, barcode_id varchar2
, type varchar2
, supplier varchar2
, primary_ind varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ExtOfBarcodeDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ExtOfXItemDesc') := "ns_name_ExtOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_custom') := "ns_location_custom";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'barcode_id') := barcode_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'type') := type;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_ind') := primary_ind;
END appendNodeValues;
constructor function "RIB_ExtOfBarcodeDesc_REC"
(
  rib_oid number
, barcode_id varchar2
, type varchar2
, supplier varchar2
, primary_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.barcode_id := barcode_id;
self.type := type;
self.supplier := supplier;
self.primary_ind := primary_ind;
RETURN;
end;
END;
/
DROP TYPE "RIB_CustFlexAttriVo_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_CustFlexAttriVo_TBL" AS TABLE OF "RIB_CustFlexAttriVo_REC";
/
DROP TYPE "RIB_ExtOfXItemSupDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ExtOfXItemSupDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ExtOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_custom" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  supplier varchar2(10),
  CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL",   -- Size of "RIB_CustFlexAttriVo_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ExtOfXItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
) return self as result
,constructor function "RIB_ExtOfXItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL"  -- Size of "RIB_CustFlexAttriVo_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ExtOfXItemSupDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ExtOfXItemDesc') := "ns_name_ExtOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_custom') := "ns_location_custom";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  IF CustFlexAttriVo_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'CustFlexAttriVo_TBL.';
    FOR INDX IN CustFlexAttriVo_TBL.FIRST()..CustFlexAttriVo_TBL.LAST() LOOP
      CustFlexAttriVo_TBL(indx).appendNodeValues( i_prefix||indx||'CustFlexAttriVo_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ExtOfXItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
RETURN;
end;
constructor function "RIB_ExtOfXItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.CustFlexAttriVo_TBL := CustFlexAttriVo_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ExtOfReltdItem_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ExtOfReltdItem_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ExtOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_custom" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  related_item varchar2(25),
  relationship_name varchar2(120),
  relationship_type varchar2(6),
  mandatory_ind varchar2(1), -- mandatory_ind is enumeration field, valid values are [Y, N] (all lower-case)
  priority varchar2(25),
  start_date date,
  end_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
) return self as result
,constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
, relationship_name varchar2
) return self as result
,constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
, relationship_name varchar2
, relationship_type varchar2
) return self as result
,constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
, relationship_name varchar2
, relationship_type varchar2
, mandatory_ind varchar2
) return self as result
,constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
, relationship_name varchar2
, relationship_type varchar2
, mandatory_ind varchar2
, priority varchar2
) return self as result
,constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
, relationship_name varchar2
, relationship_type varchar2
, mandatory_ind varchar2
, priority varchar2
, start_date date
) return self as result
,constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
, relationship_name varchar2
, relationship_type varchar2
, mandatory_ind varchar2
, priority varchar2
, start_date date
, end_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ExtOfReltdItem_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ExtOfXItemDesc') := "ns_name_ExtOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_custom') := "ns_location_custom";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'related_item') := related_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'relationship_name') := relationship_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'relationship_type') := relationship_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'mandatory_ind') := mandatory_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'priority') := priority;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date') := start_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
END appendNodeValues;
constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.related_item := related_item;
RETURN;
end;
constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
, relationship_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.related_item := related_item;
self.relationship_name := relationship_name;
RETURN;
end;
constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
, relationship_name varchar2
, relationship_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.related_item := related_item;
self.relationship_name := relationship_name;
self.relationship_type := relationship_type;
RETURN;
end;
constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
, relationship_name varchar2
, relationship_type varchar2
, mandatory_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.related_item := related_item;
self.relationship_name := relationship_name;
self.relationship_type := relationship_type;
self.mandatory_ind := mandatory_ind;
RETURN;
end;
constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
, relationship_name varchar2
, relationship_type varchar2
, mandatory_ind varchar2
, priority varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.related_item := related_item;
self.relationship_name := relationship_name;
self.relationship_type := relationship_type;
self.mandatory_ind := mandatory_ind;
self.priority := priority;
RETURN;
end;
constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
, relationship_name varchar2
, relationship_type varchar2
, mandatory_ind varchar2
, priority varchar2
, start_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.related_item := related_item;
self.relationship_name := relationship_name;
self.relationship_type := relationship_type;
self.mandatory_ind := mandatory_ind;
self.priority := priority;
self.start_date := start_date;
RETURN;
end;
constructor function "RIB_ExtOfReltdItem_REC"
(
  rib_oid number
, related_item varchar2
, relationship_name varchar2
, relationship_type varchar2
, mandatory_ind varchar2
, priority varchar2
, start_date date
, end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.related_item := related_item;
self.relationship_name := relationship_name;
self.relationship_type := relationship_type;
self.mandatory_ind := mandatory_ind;
self.priority := priority;
self.start_date := start_date;
self.end_date := end_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_ExtOfBarcodeDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ExtOfBarcodeDesc_TBL" AS TABLE OF "RIB_ExtOfBarcodeDesc_REC";
/
DROP TYPE "RIB_ExtOfXItemSupDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ExtOfXItemSupDesc_TBL" AS TABLE OF "RIB_ExtOfXItemSupDesc_REC";
/
DROP TYPE "RIB_ExtOfReltdItem_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ExtOfReltdItem_TBL" AS TABLE OF "RIB_ExtOfReltdItem_REC";
/
DROP TYPE "RIB_CustFlexAttriVo_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_CustFlexAttriVo_TBL" AS TABLE OF "RIB_CustFlexAttriVo_REC";
/
DROP TYPE "RIB_ExtOfXItemDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ExtOfXItemDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ExtOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_custom" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  bu number(10),
  ExtOfBarcodeDesc_TBL "RIB_ExtOfBarcodeDesc_TBL",   -- Size of "RIB_ExtOfBarcodeDesc_TBL" is unbounded
  ExtOfXItemSupDesc_TBL "RIB_ExtOfXItemSupDesc_TBL",   -- Size of "RIB_ExtOfXItemSupDesc_TBL" is unbounded
  ExtOfReltdItem_TBL "RIB_ExtOfReltdItem_TBL",   -- Size of "RIB_ExtOfReltdItem_TBL" is unbounded
  extract_date date,
  CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL",   -- Size of "RIB_CustFlexAttriVo_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ExtOfXItemDesc_REC"
(
  rib_oid number
, bu number
) return self as result
,constructor function "RIB_ExtOfXItemDesc_REC"
(
  rib_oid number
, bu number
, ExtOfBarcodeDesc_TBL "RIB_ExtOfBarcodeDesc_TBL"  -- Size of "RIB_ExtOfBarcodeDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_ExtOfXItemDesc_REC"
(
  rib_oid number
, bu number
, ExtOfBarcodeDesc_TBL "RIB_ExtOfBarcodeDesc_TBL"  -- Size of "RIB_ExtOfBarcodeDesc_TBL" is unbounded
, ExtOfXItemSupDesc_TBL "RIB_ExtOfXItemSupDesc_TBL"  -- Size of "RIB_ExtOfXItemSupDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_ExtOfXItemDesc_REC"
(
  rib_oid number
, bu number
, ExtOfBarcodeDesc_TBL "RIB_ExtOfBarcodeDesc_TBL"  -- Size of "RIB_ExtOfBarcodeDesc_TBL" is unbounded
, ExtOfXItemSupDesc_TBL "RIB_ExtOfXItemSupDesc_TBL"  -- Size of "RIB_ExtOfXItemSupDesc_TBL" is unbounded
, ExtOfReltdItem_TBL "RIB_ExtOfReltdItem_TBL"  -- Size of "RIB_ExtOfReltdItem_TBL" is unbounded
) return self as result
,constructor function "RIB_ExtOfXItemDesc_REC"
(
  rib_oid number
, bu number
, ExtOfBarcodeDesc_TBL "RIB_ExtOfBarcodeDesc_TBL"  -- Size of "RIB_ExtOfBarcodeDesc_TBL" is unbounded
, ExtOfXItemSupDesc_TBL "RIB_ExtOfXItemSupDesc_TBL"  -- Size of "RIB_ExtOfXItemSupDesc_TBL" is unbounded
, ExtOfReltdItem_TBL "RIB_ExtOfReltdItem_TBL"  -- Size of "RIB_ExtOfReltdItem_TBL" is unbounded
, extract_date date
) return self as result
,constructor function "RIB_ExtOfXItemDesc_REC"
(
  rib_oid number
, bu number
, ExtOfBarcodeDesc_TBL "RIB_ExtOfBarcodeDesc_TBL"  -- Size of "RIB_ExtOfBarcodeDesc_TBL" is unbounded
, ExtOfXItemSupDesc_TBL "RIB_ExtOfXItemSupDesc_TBL"  -- Size of "RIB_ExtOfXItemSupDesc_TBL" is unbounded
, ExtOfReltdItem_TBL "RIB_ExtOfReltdItem_TBL"  -- Size of "RIB_ExtOfReltdItem_TBL" is unbounded
, extract_date date
, CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL"  -- Size of "RIB_CustFlexAttriVo_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ExtOfXItemDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ExtOfXItemDesc') := "ns_name_ExtOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_custom') := "ns_location_custom";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'bu') := bu;
  IF ExtOfBarcodeDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ExtOfBarcodeDesc_TBL.';
    FOR INDX IN ExtOfBarcodeDesc_TBL.FIRST()..ExtOfBarcodeDesc_TBL.LAST() LOOP
      ExtOfBarcodeDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ExtOfBarcodeDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ExtOfXItemSupDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ExtOfXItemSupDesc_TBL.';
    FOR INDX IN ExtOfXItemSupDesc_TBL.FIRST()..ExtOfXItemSupDesc_TBL.LAST() LOOP
      ExtOfXItemSupDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ExtOfXItemSupDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ExtOfReltdItem_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ExtOfReltdItem_TBL.';
    FOR INDX IN ExtOfReltdItem_TBL.FIRST()..ExtOfReltdItem_TBL.LAST() LOOP
      ExtOfReltdItem_TBL(indx).appendNodeValues( i_prefix||indx||'ExtOfReltdItem_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'extract_date') := extract_date;
  IF CustFlexAttriVo_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'CustFlexAttriVo_TBL.';
    FOR INDX IN CustFlexAttriVo_TBL.FIRST()..CustFlexAttriVo_TBL.LAST() LOOP
      CustFlexAttriVo_TBL(indx).appendNodeValues( i_prefix||indx||'CustFlexAttriVo_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ExtOfXItemDesc_REC"
(
  rib_oid number
, bu number
) return self as result is
begin
self.rib_oid := rib_oid;
self.bu := bu;
RETURN;
end;
constructor function "RIB_ExtOfXItemDesc_REC"
(
  rib_oid number
, bu number
, ExtOfBarcodeDesc_TBL "RIB_ExtOfBarcodeDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.bu := bu;
self.ExtOfBarcodeDesc_TBL := ExtOfBarcodeDesc_TBL;
RETURN;
end;
constructor function "RIB_ExtOfXItemDesc_REC"
(
  rib_oid number
, bu number
, ExtOfBarcodeDesc_TBL "RIB_ExtOfBarcodeDesc_TBL"
, ExtOfXItemSupDesc_TBL "RIB_ExtOfXItemSupDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.bu := bu;
self.ExtOfBarcodeDesc_TBL := ExtOfBarcodeDesc_TBL;
self.ExtOfXItemSupDesc_TBL := ExtOfXItemSupDesc_TBL;
RETURN;
end;
constructor function "RIB_ExtOfXItemDesc_REC"
(
  rib_oid number
, bu number
, ExtOfBarcodeDesc_TBL "RIB_ExtOfBarcodeDesc_TBL"
, ExtOfXItemSupDesc_TBL "RIB_ExtOfXItemSupDesc_TBL"
, ExtOfReltdItem_TBL "RIB_ExtOfReltdItem_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.bu := bu;
self.ExtOfBarcodeDesc_TBL := ExtOfBarcodeDesc_TBL;
self.ExtOfXItemSupDesc_TBL := ExtOfXItemSupDesc_TBL;
self.ExtOfReltdItem_TBL := ExtOfReltdItem_TBL;
RETURN;
end;
constructor function "RIB_ExtOfXItemDesc_REC"
(
  rib_oid number
, bu number
, ExtOfBarcodeDesc_TBL "RIB_ExtOfBarcodeDesc_TBL"
, ExtOfXItemSupDesc_TBL "RIB_ExtOfXItemSupDesc_TBL"
, ExtOfReltdItem_TBL "RIB_ExtOfReltdItem_TBL"
, extract_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.bu := bu;
self.ExtOfBarcodeDesc_TBL := ExtOfBarcodeDesc_TBL;
self.ExtOfXItemSupDesc_TBL := ExtOfXItemSupDesc_TBL;
self.ExtOfReltdItem_TBL := ExtOfReltdItem_TBL;
self.extract_date := extract_date;
RETURN;
end;
constructor function "RIB_ExtOfXItemDesc_REC"
(
  rib_oid number
, bu number
, ExtOfBarcodeDesc_TBL "RIB_ExtOfBarcodeDesc_TBL"
, ExtOfXItemSupDesc_TBL "RIB_ExtOfXItemSupDesc_TBL"
, ExtOfReltdItem_TBL "RIB_ExtOfReltdItem_TBL"
, extract_date date
, CustFlexAttriVo_TBL "RIB_CustFlexAttriVo_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.bu := bu;
self.ExtOfBarcodeDesc_TBL := ExtOfBarcodeDesc_TBL;
self.ExtOfXItemSupDesc_TBL := ExtOfXItemSupDesc_TBL;
self.ExtOfReltdItem_TBL := ExtOfReltdItem_TBL;
self.extract_date := extract_date;
self.CustFlexAttriVo_TBL := CustFlexAttriVo_TBL;
RETURN;
end;
END;
/

DROP TYPE "RIB_ExtOfXItemDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ExtOfXItemDesc_TBL" AS TABLE OF "RIB_ExtOfXItemDesc_REC";
/

