/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_UDA_FF_TBL
* Description:   Table type used by XITEM
*				 
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 04/07/2018
* Last Modified: 04/07/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
DROP TYPE XXADEO_UDA_FF_TBL FORCE;
CREATE OR REPLACE Type XXADEO_UDA_FF_TBL is table of XXADEO_UDA_FF_REC;
/