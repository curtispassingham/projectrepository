create or replace package body XXADEO_DASHBOARD_PACK_DERU_SQL is


---------------------------------------------------------------------------------
-- Name: PACK_DERUNIT_ITEM_SEARCH
-- Purpose: Function to return pack and derived unit items in the
--          Dashboard, based on the search criteria defined by the user.
---------------------------------------------------------------------------------
  FUNCTION PACK_DERUNIT_ITEM_SEARCH(I_item_type       IN VARCHAR2,
                                    I_bu              IN area_tl.area%TYPE DEFAULT NULL,
                                    I_group           IN groups.group_no%TYPE DEFAULT NULL,
                                    I_dept_list       IN VARCHAR2 DEFAULT NULL,
                                    I_class_list      IN CLOB DEFAULT NULL,
                                    I_subclass_list   IN CLOB DEFAULT NULL,
                                    I_supp_purch_site IN NUMBER DEFAULT NULL,
                                    I_manufacturer    IN NUMBER DEFAULT NULL,
                                    I_item_list       IN VARCHAR2 DEFAULT NULL,
                                    I_item            IN item_master.item%TYPE DEFAULT NULL,
                                    I_item_desc       IN item_master.item_desc%TYPE DEFAULT NULL,
                                    I_gtin            IN NUMBER DEFAULT NULL,
                                    I_block_po_reason IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa1_id      IN NUMBER DEFAULT NULL,
                                    I_udacfa1_value   IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa1_type    IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa1_flag    IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa2_id      IN NUMBER DEFAULT NULL,
                                    I_udacfa2_value   IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa2_type    IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa2_flag    IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa3_id      IN NUMBER DEFAULT NULL,
                                    I_udacfa3_value   IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa3_type    IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa3_flag    IN VARCHAR2 DEFAULT NULL,
                                    I_execute_query   IN VARCHAR2 DEFAULT 'Y')
    RETURN XXADEO_PACK_DERU_RESULT_TBL
    PIPELINED AS
    

    L_program       VARCHAR2(64) := 'XXADEO_DASHBOARD_PACK_DERU_SQL.PACK_DERUNIT_ITEM_SEARCH';
    L_error_message VARCHAR2(2000);
    --
    L_Pack_DerUn_ItemSearchTbl XXADEO_PACK_DERU_RESULT_TBL;
    L_string_query            VARCHAR2(20000);
    L_sys_refcur              SYS_REFCURSOR;
    --
    L_dept_list     list_type := list_type();
    L_class_list    list_type := list_type();
    L_subclass_list list_type := list_type();
    L_item_list     list_type := list_type();
    L_udacfa1_list  list_type := list_type();    
    L_udacfa2_list  list_type := list_type();
    L_udacfa3_list  list_type := list_type();
    L_cfa_type      cfa_attrib.data_type%TYPE;
    --
    L_date1_1       DATE;
    L_date1_2       DATE;
    L_date2_1       DATE;
    L_date2_2       DATE;
    L_date3_1       DATE;
    L_date3_2       DATE;
    --

    --cursor

    --get CFA data type
    cursor C_get_CFA_data_type(L_attrib_id cfa_attrib.attrib_id%TYPE) is
      select data_type from cfa_attrib where attrib_id = L_attrib_id;

  BEGIN
    --
    if I_execute_query = 'N' then
      --
      return;
      --
    end if;
    --
    EXECUTE IMMEDIATE ('ALTER SESSION SET NLS_DATE_FORMAT = ''DD-MM-YYYY''');
    --
    
    if I_item_type = 'PACK_COMP' then
      
       L_string_query := q'{with t_binds as
                            (select :1  db_bu,
                                    :2  db_group,
                                    :3  db_dept,
                                    :4  db_class,
                                    :5  db_subclass,
                                    :6  db_supp_purch_site,
                                    :7  db_manufacturer,
                                    :8  db_item,
                                    :9  db_item_desc,
                                    :10 db_gtin,
                                    :11 db_item_list,
                                    :12 db_block_po_reason,
                                    :13 db_udacfa1_id,
                                    :14 db_udacfa1_value,
                                    :15 db_udacfa2_id,
                                    :16 db_udacfa2_value,
                                    :17 db_udacfa3_id,
                                    :18 db_udacfa3_value,
                                    :19 db_date1_1,
                                    :20 db_date1_2,
                                    :21 db_date2_1,
                                    :22 db_date2_2,
                                    :23 db_date3_1,
                                    :24 db_date3_2
                               from dual)
                           select new XXADEO_PACK_DERU_RESULT_OBJ(filter_org_id,
                                                                  status,
                                                                  pack_ind,
                                                                  pack_type,
                                                                  item_level,
                                                                  tran_level,
                                                                  check_uda_ind,
                                                                  group_no,
                                                                  group_name,
                                                                  dept,
                                                                  dept_name,
                                                                  class,
                                                                  class_name,
                                                                  subclass,
                                                                  sub_name,
                                                                  ranking_item,
                                                                  item,
                                                                  item_desc,
                                                                  item_type,
                                                                  agg_item,
                                                                  pack_qty,
                                                                  sub_typology,
                                                                  personalized_ind,
                                                                  standard_uom,
                                                                  derunit_conv_factor,
                                                                  qt_capacity,
                                                                  contain_uom_item,
                                                                  assortment_mode,
                                                                  range_size,
                                                                  orderable_ind,
                                                                  sellable_ind,
                                                                  std_str_tr_prc_prccntrl,
                                                                  std_str_tr_prc_itemprc,
                                                                  national_price_itmprc,
                                                                  national_price_prccntrl,
                                                                  national_price_check,
                                                                  margin_itmprc,
                                                                  margin_perc,
                                                                  margin_prccntrl,
                                                                  retail_prc_blocking,
                                                                  lifecycle_status,
                                                                  actif_ddate_item,
                                                                  forecast_ind)
                                      from xxadeo_v_dashboard_packitm i,
                                           t_binds b
                                      where 1 = 1
                                      }';    
    
    
      
    elsif I_item_type = 'STD_DERU' then
    
          L_string_query := q'{with t_binds as
                            (select :1  db_bu,
                                    :2  db_group,
                                    :3  db_dept,
                                    :4  db_class,
                                    :5  db_subclass,
                                    :6  db_supp_purch_site,
                                    :7  db_manufacturer,
                                    :8  db_item,
                                    :9  db_item_desc,
                                    :10 db_gtin,
                                    :11 db_item_list,
                                    :12 db_block_po_reason,
                                    :13 db_udacfa1_id,
                                    :14 db_udacfa1_value,
                                    :15 db_udacfa2_id,
                                    :16 db_udacfa2_value,
                                    :17 db_udacfa3_id,
                                    :18 db_udacfa3_value,
                                    :19 db_date1_1,
                                    :20 db_date1_2,
                                    :21 db_date2_1,
                                    :22 db_date2_2,
                                    :23 db_date3_1,
                                    :24 db_date3_2
                               from dual)
                           select new XXADEO_PACK_DERU_RESULT_OBJ(filter_org_id,
                                                                  status,
                                                                  pack_ind,
                                                                  pack_type,
                                                                  item_level,
                                                                  tran_level,
                                                                  check_uda_ind,
                                                                  group_no,
                                                                  group_name,
                                                                  dept,
                                                                  dept_name,
                                                                  class,
                                                                  class_name,
                                                                  subclass,
                                                                  sub_name,
                                                                  ranking_item,
                                                                  item,
                                                                  item_desc,
                                                                  item_type,
                                                                  agg_item,
                                                                  pack_qty,
                                                                  sub_typology,
                                                                  personalized_ind,
                                                                  standard_uom,
                                                                  derunit_conv_factor,
                                                                  qt_capacity,
                                                                  contain_uom_item,
                                                                  assortment_mode,
                                                                  range_size,
                                                                  orderable_ind,
                                                                  sellable_ind,
                                                                  std_str_tr_prc_prccntrl,
                                                                  std_str_tr_prc_itemprc,
                                                                  national_price_itmprc,
                                                                  national_price_prccntrl,
                                                                  national_price_check,
                                                                  margin_itmprc,
                                                                  margin_perc,
                                                                  margin_prccntrl,
                                                                  retail_prc_blocking,
                                                                  lifecycle_status,
                                                                  actif_ddate_item,
                                                                  forecast_ind)
                                      from xxadeo_v_dashboard_deruni i,
                                           t_binds b
                                      where 1 = 1
                                      }';    
    else
      RETURN;
    end if;
    
    
    -- BU
    if I_bu is not null then
      
      L_string_query := L_string_query ||
                        q'{ and (nvl(i.filter_org_id,b.db_bu) = b.db_bu)}';
      --
    end if;
  
    --group
    if I_group is not null then
      
      L_string_query := L_string_query ||
                        q'{ and (i.group_no = b.db_group) }';
      --
    end if;
  
    --department
    if I_dept_list is not null then
      
      select t.*
        bulk collect
        into L_dept_list
        from table(convert_comma_list(I_list => I_dept_list)) t;
      --
      L_string_query := L_string_query ||
                        q'{ and i.dept in (select column_value
                                                                        from table(b.db_dept)) }';
      --
    end if;
  
    --class
    if I_class_list is not null then
      
      select t.*
        bulk collect
        into L_class_list
        from table(convert_comma_list(I_list => I_class_list)) t;
      --
      L_string_query := L_string_query ||
                        q'{ and i.class in (select column_value
                                                                      from table(b.db_class)) }';
      --
    end if;
  
    --subclass
    if I_subclass_list is not null then
      
      select t.*
        bulk collect
        into L_subclass_list
        from table(convert_comma_list(I_list => I_subclass_list)) t;
      --
      L_string_query := L_string_query ||
                        q'{ and i.subclass in (select column_value
                                                                      from table(b.db_subclass)) }';
      --
    end if;
  
    --supp_purch_site
  
    if I_supp_purch_site is not null then
      
      L_string_query := L_string_query ||
                        q'{ and exists (select 1 from item_supplier isup 
                                         where i.item = isup.item 
                                           and isup.supplier = b.db_supp_purch_site) }';
      --
    
    end if;
  
    --manufacturer
  
    if I_manufacturer is not null then
      
      L_string_query := L_string_query ||
                        q'{ and exists ( select 1 
                                                            from item_supp_country isc
                                                           where to_number(isc.supp_hier_lvl_1) = b.db_manufacturer
                                                             and i.item = isc.item) }';
    
    end if;
  
    --item
    if I_item is not null and I_item_type = 'PACK_COMP' then
      
     L_string_query := L_string_query || q'{ and (i.item = (select nvl(nvl(imaster.item_grandparent,imaster.item_parent),b.db_item)
                                                               from item_master imaster 
                                                              where imaster.item = b.db_item)
                                                   or
                                                   exists (select 1 
                                                             from xxadeo_v_dashboard_packitm_cmp itm_cmp
                                                            where itm_cmp.item = b.db_item
                                                              and itm_cmp.filter_org_id = i.filter_org_id
                                                              and itm_cmp.agg_item = i.item))}';

    
    elsif I_item is not null and I_item_type = 'STD_DERU' then
      
      L_string_query := L_string_query || q'{ and (i.item = (select nvl(nvl(imaster.item_grandparent,imaster.item_parent),b.db_item)
                                                               from item_master imaster 
                                                              where imaster.item = b.db_item)
                                                   or
                                                   exists (select 1 
                                                             from xxadeo_v_dashboard_deruni_itms deru_itms
                                                            where deru_itms.item = b.db_item
                                                              and deru_itms.filter_org_id = i.filter_org_id
                                                              and deru_itms.agg_item = i.item))}';
      --
    end if;
  
    --item list
    if I_item_list is not null then
      --
      select distinct (select nvl2(imaster.item_grandparent, imaster.item_parent,t.item) from item_master imaster where imaster.item = t.item) item
        bulk collect
        into L_item_list
        from skulist_detail t
       where t.skulist = I_item_list;
      --
      L_string_query := L_string_query ||
                        /*q'{ and i.item in (select column_value
                                                                      from table(b.db_item_list)) }';*/
                          q'{ and (i.item in (select column_value
                                                from table(b.db_item_list))
                                   or exists (select 1 
                                                from xxadeo_v_dashboard_deruni_itms deru_itms
                                               where deru_itms.item = b.db_item
                                                 and deru_itms.filter_org_id = i.filter_org_id
                                                 and deru_itms.agg_item in (select column_value
                                                                              from table(b.db_item_list)))) }';
      --
    end if;
  
    -- Item_Description
    if I_item_desc is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and upper(i.item_desc) like upper('%}' || I_item_desc || q'{%') }';
      --
    end if;
  
    --gtin
    if I_gtin is not null then
      
    --if the user search for GTIN, the result of the search will be the respective Pack/Component or Standard/Derived Unit Items 
    --for the level 1 item related with the GTIN

     L_string_query := L_string_query || q'{ and (i.item in (select nvl(im2.item_grandparent,im2.item_parent) 
                                                                from item_master im2
                                                               where im2.item = b.db_gtin
                                                                 and im2.item_level > im2.tran_level)}';
       
     --
     if I_item_type = 'PACK_COMP' then                                                                                                                
               
       L_string_query := L_string_query || q'{ or exists( select 1 
                                                            from xxadeo_v_dashboard_packitm_cmp itm_cmp
                                                           where itm_cmp.item = (select nvl(im2.item_grandparent,im2.item_parent) 
                                                                                     from item_master im2
                                                                                    where im2.item = b.db_gtin
                                                                                      and im2.item_level > im2.tran_level)
                                                             and itm_cmp.filter_org_id = i.filter_org_id
                                                             and itm_cmp.agg_item = i.item))}';
     elsif I_item_type = 'STD_DERU' then                                      
           
       L_string_query := L_string_query || q'{or exists( select 1 
                                                           from xxadeo_v_dashboard_deruni_itms deru_itms
                                                          where deru_itms.item = (select nvl(im2.item_grandparent,im2.item_parent) 
                                                                                    from item_master im2
                                                                                   where im2.item = b.db_gtin
                                                                                     and im2.item_level > im2.tran_level)
                                                            and deru_itms.filter_org_id = i.filter_org_id
                                                            and deru_itms.agg_item = i.item))}';  
     end if;                                                         
     --
    end if;
  
    --blocking po reason
    if I_block_po_reason is not null then
       
      L_string_query := L_string_query ||  q'{ and exists ((select 1
                                                              from (select cfa.attrib_id ATTR_ID, cfa_value
                                                                      from cfa_attrib cfa,
                                                                           (select group_id, cfa_attribute, cfa_value
                                                                              from item_supplier_cfa_ext unpivot(cfa_value for cfa_attribute in(varchar2_1,
                                                                                                                                              varchar2_2,
                                                                                                                                              varchar2_3,
                                                                                                                                              varchar2_4,
                                                                                                                                              varchar2_5,
                                                                                                                                              varchar2_6,
                                                                                                                                              varchar2_7,
                                                                                                                                              varchar2_8,
                                                                                                                                              varchar2_9,
                                                                                                                                              varchar2_10))
                                                                             where item = i.item) cfa_unc
                                                                     where cfa.group_id = cfa_unc.group_id
                                                                       and upper(cfa.storage_col_name) =
                                                                           upper(cfa_unc.cfa_attribute)) aux,
                                                                   xxadeo_mom_dvm x
                                                             where (x.value_1) = aux.attr_id 
                                                               and func_area = 'CFA'
                                                               and parameter = 'BLOCKING_PO_ITEM_SUPP'
                                                               and (bu = b.db_bu or bu is null or bu = -1)
                                                               and cfa_value = b.db_block_po_reason))}';
      
      
      --
    end if;
  
      
    --item attr 1
  
    if I_udacfa1_id is not null then
      --
      if I_udacfa1_flag = 'UDA' then
        --
        if I_udacfa1_type = GP_UDA_TYPE_LV then
        
          select t.*
            bulk collect
            into L_udacfa1_list
            from table(convert_comma_list(I_list => I_udacfa1_value)) t;
            
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1 
                                                              from uda_item_lov uda_lov
                                                             where uda_lov.uda_id = b.db_udacfa1_id
                                                               and uda_lov.uda_value in (select column_value
                                                                        from table(b.db_udacfa1_value))
                                                               and i.item = uda_lov.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.filter_org_id or bu is null or bu = -1)
                                                                              and value_1 = uda_lov.uda_id
                                                                              and func_area = 'UDA'))}';
        --
        elsif I_udacfa1_type = GP_UDA_TYPE_DT then
      
        
        select t.*
          bulk collect
          into L_udacfa1_list
          from table(convert_comma_list(I_list => I_udacfa1_value)) t;
          --   
          L_date1_1 := trunc(TO_DATE(TO_CHAR(L_udacfa1_list(1)),'YYYY-MM-DD'));
          L_date1_2 := trunc(TO_DATE(TO_CHAR(L_udacfa1_list(2)),'YYYY-MM-DD'));
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1 
                                                              from uda_item_date uda_date
                                                             where uda_date.uda_id = b.db_udacfa1_id
                                                                and trunc(uda_date.uda_date) between trunc(b.db_date1_1) and trunc(b.db_date1_2)
                                                               and i.item = uda_date.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.filter_org_id or bu is null or bu = -1)
                                                                              and value_1 = uda_date.uda_id
                                                                              and func_area = 'UDA'))}';
        --
        elsif I_udacfa1_type = GP_UDA_TYPE_FF then
          
          L_udacfa1_list.Delete;
          L_udacfa1_list.extend();
          L_udacfa1_list(1) := I_udacfa1_value;
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1 
                                                              from uda_item_ff uda_ff
                                                             where uda_ff.uda_id = b.db_udacfa1_id
                                                               and upper(uda_ff.uda_text) in (select upper(column_value)
                                                                        from table(b.db_udacfa1_value))
                                                               and i.item = uda_ff.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.filter_org_id or bu is null or bu = -1)
                                                                              and value_1 = uda_ff.uda_id
                                                                              and func_area = 'UDA'))}';
        
        end if;
      --
      elsif I_udacfa1_flag = 'CFA' then
        --
        open C_get_CFA_data_type(I_udacfa1_id);
        fetch C_get_CFA_data_type
          into L_cfa_type;
        close C_get_CFA_data_type;
        --
        if L_cfa_type = GP_CFA_TEXT then
          --
          if I_udacfa1_type in (GP_CFA_TYPE_LI, GP_CFA_TYPE_RG) then
          
            select t.*
              bulk collect
              into L_udacfa1_list
              from table(convert_comma_list(I_list => I_udacfa1_value)) t;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (varchar2_1,
                                                                                                              varchar2_2,
                                                                                                              varchar2_3,
                                                                                                              varchar2_4,
                                                                                                              varchar2_5,
                                                                                                              varchar2_6,
                                                                                                              varchar2_7,
                                                                                                              varchar2_8,
                                                                                                              varchar2_9,
                                                                                                              varchar2_10))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa1_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.filter_org_id or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa1_value)))}';
          --
          elsif I_udacfa1_type in (GP_CFA_TYPE_CB, GP_CFA_TYPE_TI) then
          
            L_udacfa1_list.Delete;
            L_udacfa1_list.extend();
            L_udacfa1_list(1) := I_udacfa1_value;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (varchar2_1,
                                                                                                              varchar2_2,
                                                                                                              varchar2_3,
                                                                                                              varchar2_4,
                                                                                                              varchar2_5,
                                                                                                              varchar2_6,
                                                                                                              varchar2_7,
                                                                                                              varchar2_8,
                                                                                                              varchar2_9,
                                                                                                              varchar2_10))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa1_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.filter_org_id or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa1_value)))}';
          
          end if;
        --
        elsif L_cfa_type = GP_CFA_NUMBER then
        
          if I_udacfa1_type in (GP_CFA_TYPE_LI, GP_CFA_TYPE_RG) then
          
            select t.*
              bulk collect
              into L_udacfa1_list
              from table(convert_comma_list(I_list => I_udacfa1_value)) t;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (number_11,
                                                                                                              number_12,
                                                                                                              number_13,
                                                                                                              number_14,
                                                                                                              number_15,
                                                                                                              number_16,
                                                                                                              number_17,
                                                                                                              number_18,
                                                                                                              number_19,
                                                                                                              number_20))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa1_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.filter_org_id or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and cfa_value in (select column_value
                                                                        from table(b.db_udacfa1_value)))}';
          --
          elsif I_udacfa1_type in (GP_CFA_TYPE_TI) then
          
            L_udacfa1_list.Delete;
            L_udacfa1_list.extend();
            L_udacfa1_list(1) := I_udacfa1_value;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (number_11,
                                                                                                              number_12,
                                                                                                              number_13,
                                                                                                              number_14,
                                                                                                              number_15,
                                                                                                              number_16,
                                                                                                              number_17,
                                                                                                              number_18,
                                                                                                              number_19,
                                                                                                              number_20))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa1_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.filter_org_id or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa1_value)))}';
          
          end if;
        --
        elsif L_cfa_type = GP_CFA_DATE then
        
          
          select t.*
              bulk collect
              into L_udacfa1_list
              from table(convert_comma_list(I_list => I_udacfa1_value)) t;
          --  
          L_date1_1 := trunc(TO_DATE(TO_CHAR(L_udacfa1_list(1)),'YYYY-MM-DD'));
          L_date1_2 := trunc(TO_DATE(TO_CHAR(L_udacfa1_list(2)),'YYYY-MM-DD'));
          --       
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (date_21,
                                                                                                              date_22,
                                                                                                              date_23,
                                                                                                              date_24,
                                                                                                              date_25))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa1_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.filter_org_id or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and trunc(cfa_value) between trunc(b.db_date1_1) and trunc(b.db_date1_2))}';
        --
        end if;
      --
      end if;
    --
    end if;
  
    --item attr 2

    if I_udacfa2_id is not null then
      --
      if I_udacfa2_flag = 'UDA' then
        --
        if I_udacfa2_type = GP_UDA_TYPE_LV then

          select t.*
            bulk collect
            into L_udacfa2_list
            from table(convert_comma_list(I_list => I_udacfa2_value)) t;
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1
                                                            from uda_item_lov uda_lov
                                                           where uda_lov.uda_id = b.db_udacfa2_id
                                                             and uda_lov.uda_value in (select column_value
                                                                      from table(b.db_udacfa2_value))
                                                             and i.item = uda_lov.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_lov.uda_id
                                                                              and func_area = 'UDA'))}';
        --
        elsif I_udacfa2_type = GP_UDA_TYPE_DT then



          select t.*
            bulk collect
            into L_udacfa2_list
            from table(convert_comma_list(I_list => I_udacfa2_value)) t;
          --
          L_date2_1 := trunc(TO_DATE(TO_CHAR(L_udacfa2_list(1)),'YYYY-MM-DD'));
          L_date2_2 := trunc(TO_DATE(TO_CHAR(L_udacfa2_list(2)),'YYYY-MM-DD'));
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1
                                                              from uda_item_date uda_date
                                                             where uda_date.uda_id = b.db_udacfa2_id
                                                                and trunc(uda_date.uda_date) between trunc(b.db_date2_1) and trunc(b.db_date2_2)
                                                               and i.item = uda_date.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_date.uda_id
                                                                              and func_area = 'UDA'))}';
        --
        elsif I_udacfa2_type = GP_UDA_TYPE_FF then

          L_udacfa2_list.Delete;
          L_udacfa2_list.extend();
          L_udacfa2_list(1) := I_udacfa2_value;
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1
                                                              from uda_item_ff uda_ff
                                                             where uda_ff.uda_id = b.db_udacfa2_id
                                                               and upper(uda_ff.uda_text) in (select upper(column_value)
                                                                        from table(b.db_udacfa2_value))
                                                               and i.item = uda_ff.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_ff.uda_id
                                                                              and func_area = 'UDA'))}';

        end if;
      --
      elsif I_udacfa2_flag = 'CFA' then
        --
        open C_get_CFA_data_type(I_udacfa2_id);
        fetch C_get_CFA_data_type
          into L_cfa_type;
        close C_get_CFA_data_type;
        --
        if L_cfa_type = GP_CFA_TEXT then

          if I_udacfa2_type in (GP_CFA_TYPE_LI, GP_CFA_TYPE_RG) then

            select t.*
              bulk collect
              into L_udacfa2_list
              from table(convert_comma_list(I_list => I_udacfa2_value)) t;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (varchar2_1,
                                                                                                              varchar2_2,
                                                                                                              varchar2_3,
                                                                                                              varchar2_4,
                                                                                                              varchar2_5,
                                                                                                              varchar2_6,
                                                                                                              varchar2_7,
                                                                                                              varchar2_8,
                                                                                                              varchar2_9,
                                                                                                              varchar2_10))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa2_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa2_value)))}';
          --
          elsif I_udacfa2_type in (GP_CFA_TYPE_CB, GP_CFA_TYPE_TI) then

            L_udacfa2_list.Delete;
            L_udacfa2_list.extend();
            L_udacfa2_list(1) := I_udacfa2_value;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (varchar2_1,
                                                                                                              varchar2_2,
                                                                                                              varchar2_3,
                                                                                                              varchar2_4,
                                                                                                              varchar2_5,
                                                                                                              varchar2_6,
                                                                                                              varchar2_7,
                                                                                                              varchar2_8,
                                                                                                              varchar2_9,
                                                                                                              varchar2_10))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa2_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa2_value)))}';

          end if;
        --
        elsif L_cfa_type = GP_CFA_NUMBER then

          if I_udacfa2_type in (GP_CFA_TYPE_LI, GP_CFA_TYPE_RG) then

            select t.*
              bulk collect
              into L_udacfa2_list
              from table(convert_comma_list(I_list => I_udacfa2_value)) t;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (number_11,
                                                                                                              number_12,
                                                                                                              number_13,
                                                                                                              number_14,
                                                                                                              number_15,
                                                                                                              number_16,
                                                                                                              number_17,
                                                                                                              number_18,
                                                                                                              number_19,
                                                                                                              number_20))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa2_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and cfa_value in (select column_value
                                                                        from table(b.db_udacfa2_value)))}';
          --
          elsif I_udacfa2_type in (GP_CFA_TYPE_TI) then

            L_udacfa2_list.Delete;
            L_udacfa2_list.extend();
            L_udacfa2_list(1) := I_udacfa2_value;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (number_11,
                                                                                                              number_12,
                                                                                                              number_13,
                                                                                                              number_14,
                                                                                                              number_15,
                                                                                                              number_16,
                                                                                                              number_17,
                                                                                                              number_18,
                                                                                                              number_19,
                                                                                                              number_20))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa2_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa2_value)))}';

          end if;
        --
        elsif L_cfa_type = GP_CFA_DATE then

         select t.*
              bulk collect
              into L_udacfa2_list
              from table(convert_comma_list(I_list => I_udacfa2_value)) t;
          --
          L_date2_1 := trunc(TO_DATE(TO_CHAR(L_udacfa2_list(1)),'YYYY-MM-DD'));
          L_date2_2 := trunc(TO_DATE(TO_CHAR(L_udacfa2_list(2)),'YYYY-MM-DD'));
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (date_21,
                                                                                                              date_22,
                                                                                                              date_23,
                                                                                                              date_24,
                                                                                                              date_25))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa2_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and trunc(cfa_value) between trunc(b.db_date2_1) and trunc(b.db_date2_2))}';
        --
        end if;
      --
      end if;
    --
    end if;


    --item attr 3

    if I_udacfa3_id is not null then

      if I_udacfa3_flag = 'UDA' then

        if I_udacfa3_type = GP_UDA_TYPE_LV then

          select t.*
            bulk collect
            into L_udacfa3_list
            from table(convert_comma_list(I_list => I_udacfa3_value)) t;
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1
                                                            from uda_item_lov uda_lov
                                                           where uda_lov.uda_id = b.db_udacfa3_id
                                                             and uda_lov.uda_value in (select column_value
                                                                      from table(b.db_udacfa3_value))
                                                             and i.item = uda_lov.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_lov.uda_id
                                                                              and func_area = 'UDA'))}';
        --
        elsif I_udacfa3_type = GP_UDA_TYPE_DT then

           select t.*
              bulk collect
              into L_udacfa3_list
              from table(convert_comma_list(I_list => I_udacfa3_value)) t;
          --
          L_date3_1 := trunc(TO_DATE(TO_CHAR(L_udacfa3_list(1)),'YYYY-MM-DD'));
          L_date3_2 := trunc(TO_DATE(TO_CHAR(L_udacfa3_list(2)),'YYYY-MM-DD'));
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1
                                                              from uda_item_date uda_date
                                                             where uda_date.uda_id = b.db_udacfa3_id
                                                                and trunc(uda_date.uda_date) between trunc(b.db_date3_1) and trunc(b.db_date3_2)
                                                               and i.item = uda_date.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_date.uda_id
                                                                              and func_area = 'UDA'))}';

        --
        elsif I_udacfa3_type = GP_UDA_TYPE_FF then

          L_udacfa3_list.Delete;
          L_udacfa3_list.extend();
          L_udacfa3_list(1) := I_udacfa3_value;
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1
                                                              from uda_item_ff uda_ff
                                                             where uda_ff.uda_id = b.db_udacfa3_id
                                                               and upper(uda_ff.uda_text) in (select upper(column_value)
                                                                        from table(b.db_udacfa3_value))
                                                               and i.item = uda_ff.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_ff.uda_id
                                                                              and func_area = 'UDA'))}';

        end if;
      --
      elsif I_udacfa3_flag = 'CFA' then
            
        open C_get_CFA_data_type(I_udacfa3_id);
        fetch C_get_CFA_data_type
          into L_cfa_type;
        close C_get_CFA_data_type;
        --
        if L_cfa_type = GP_CFA_TEXT then

          if I_udacfa3_type in (GP_CFA_TYPE_LI, GP_CFA_TYPE_RG) then

            select t.*
              bulk collect
              into L_udacfa3_list
              from table(convert_comma_list(I_list => I_udacfa3_value)) t;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (varchar2_1,
                                                                                                              varchar2_2,
                                                                                                              varchar2_3,
                                                                                                              varchar2_4,
                                                                                                              varchar2_5,
                                                                                                              varchar2_6,
                                                                                                              varchar2_7,
                                                                                                              varchar2_8,
                                                                                                              varchar2_9,
                                                                                                              varchar2_10))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa3_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa3_value)))}';

          elsif I_udacfa3_type in (GP_CFA_TYPE_CB, GP_CFA_TYPE_TI) then

            L_udacfa3_list.Delete;
            L_udacfa3_list.extend();
            L_udacfa3_list(1) := I_udacfa3_value;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (varchar2_1,
                                                                                                              varchar2_2,
                                                                                                              varchar2_3,
                                                                                                              varchar2_4,
                                                                                                              varchar2_5,
                                                                                                              varchar2_6,
                                                                                                              varchar2_7,
                                                                                                              varchar2_8,
                                                                                                              varchar2_9,
                                                                                                              varchar2_10))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa3_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa3_value)))}';

          end if;
        --
        elsif L_cfa_type = GP_CFA_NUMBER then

          if I_udacfa3_type in (GP_CFA_TYPE_LI, GP_CFA_TYPE_RG) then

            select t.*
              bulk collect
              into L_udacfa3_list
              from table(convert_comma_list(I_list => I_udacfa3_value)) t;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (number_11,
                                                                                                              number_12,
                                                                                                              number_13,
                                                                                                              number_14,
                                                                                                              number_15,
                                                                                                              number_16,
                                                                                                              number_17,
                                                                                                              number_18,
                                                                                                              number_19,
                                                                                                              number_20))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa3_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and cfa_value in (select column_value
                                                                        from table(b.db_udacfa3_value)))}';
          --
          elsif I_udacfa3_type in (GP_CFA_TYPE_TI) then

            L_udacfa3_list.Delete;
            L_udacfa3_list.extend();
            L_udacfa3_list(1) := I_udacfa3_value;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (number_11,
                                                                                                              number_12,
                                                                                                              number_13,
                                                                                                              number_14,
                                                                                                              number_15,
                                                                                                              number_16,
                                                                                                              number_17,
                                                                                                              number_18,
                                                                                                              number_19,
                                                                                                              number_20))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa3_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa3_value)))}';

          end if;
        --
        elsif L_cfa_type = GP_CFA_DATE then

          select t.*
              bulk collect
              into L_udacfa3_list
              from table(convert_comma_list(I_list => I_udacfa3_value)) t;
          --
          L_date3_1 := trunc(TO_DATE(TO_CHAR(L_udacfa3_list(1)),'YYYY-MM-DD'));
          L_date3_2 := trunc(TO_DATE(TO_CHAR(L_udacfa3_list(2)),'YYYY-MM-DD'));
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (date_21,
                                                                                                              date_22,
                                                                                                              date_23,
                                                                                                              date_24,
                                                                                                              date_25))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa3_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and trunc(cfa_value) between trunc(b.db_date3_1) and trunc(b.db_date3_2))}';
        --
        end if;
      --
      end if;
    --
    end if;
    
    --
    -- bulk query to table type
    --
    open L_sys_refcur for L_string_query
      using I_bu, I_group, L_dept_list, L_class_list, L_subclass_list, I_supp_purch_site, I_manufacturer, I_item, I_item_desc, I_gtin, L_item_list, I_block_po_reason, I_udacfa1_id, L_udacfa1_list, I_udacfa2_id, L_udacfa2_list, I_udacfa3_id, L_udacfa3_list, L_date1_1, L_date1_2, L_date2_1, L_date2_2, L_date3_1, L_date3_2;
    loop
      --
      fetch L_sys_refcur bulk collect
        into L_Pack_DerUn_ItemSearchTbl;
      exit when L_Pack_DerUn_ItemSearchTbl.count = 0;
      --
      -- pipe data form collection
      --
      for i in 1 .. L_Pack_DerUn_ItemSearchTbl.count loop
        --
       pipe row(XXADEO_PACK_DERU_RESULT_OBJ(L_Pack_DerUn_ItemSearchTbl(i).filter_org_id,
                                            L_Pack_DerUn_ItemSearchTbl(i).status,
                                            L_Pack_DerUn_ItemSearchTbl(i).pack_ind,
                                            L_Pack_DerUn_ItemSearchTbl(i).pack_type,
                                            L_Pack_DerUn_ItemSearchTbl(i).item_level,
                                            L_Pack_DerUn_ItemSearchTbl(i).tran_level,
                                            L_Pack_DerUn_ItemSearchTbl(i).check_uda_ind,
                                            L_Pack_DerUn_ItemSearchTbl(i).group_no,
                                            L_Pack_DerUn_ItemSearchTbl(i).group_name,
                                            L_Pack_DerUn_ItemSearchTbl(i).dept,
                                            L_Pack_DerUn_ItemSearchTbl(i).dept_name,
                                            L_Pack_DerUn_ItemSearchTbl(i).class,
                                            L_Pack_DerUn_ItemSearchTbl(i).class_name,
                                            L_Pack_DerUn_ItemSearchTbl(i).subclass,
                                            L_Pack_DerUn_ItemSearchTbl(i).sub_name,
                                            L_Pack_DerUn_ItemSearchTbl(i).ranking_item,
                                            L_Pack_DerUn_ItemSearchTbl(i).item,
                                            L_Pack_DerUn_ItemSearchTbl(i).item_desc,
                                            L_Pack_DerUn_ItemSearchTbl(i).item_type,
                                            L_Pack_DerUn_ItemSearchTbl(i).agg_item,
                                            L_Pack_DerUn_ItemSearchTbl(i).pack_qty,
                                            L_Pack_DerUn_ItemSearchTbl(i).sub_typology,
                                            L_Pack_DerUn_ItemSearchTbl(i).personalized_ind,
                                            L_Pack_DerUn_ItemSearchTbl(i).standard_uom,
                                            L_Pack_DerUn_ItemSearchTbl(i).derunit_conv_factor,
                                            L_Pack_DerUn_ItemSearchTbl(i).qt_capacity,
                                            L_Pack_DerUn_ItemSearchTbl(i).contain_uom_item,
                                            L_Pack_DerUn_ItemSearchTbl(i).assortment_mode,
                                            L_Pack_DerUn_ItemSearchTbl(i).range_size,
                                            L_Pack_DerUn_ItemSearchTbl(i).orderable_ind,
                                            L_Pack_DerUn_ItemSearchTbl(i).sellable_ind,
                                            L_Pack_DerUn_ItemSearchTbl(i).std_str_tr_prc_itemprc,
                                            L_Pack_DerUn_ItemSearchTbl(i).std_str_tr_prc_prccntrl,
                                            L_Pack_DerUn_ItemSearchTbl(i).national_price_itmprc,
                                            L_Pack_DerUn_ItemSearchTbl(i).national_price_prccntrl,
                                            L_Pack_DerUn_ItemSearchTbl(i).national_price_check,
                                            L_Pack_DerUn_ItemSearchTbl(i).margin_itmprc,
                                            L_Pack_DerUn_ItemSearchTbl(i).margin_prccntrl,
                                            L_Pack_DerUn_ItemSearchTbl(i).margin_perc,
                                            L_Pack_DerUn_ItemSearchTbl(i).retail_prc_blocking,
                                            L_Pack_DerUn_ItemSearchTbl(i).lifecycle_status,
                                            L_Pack_DerUn_ItemSearchTbl(i).actif_ddate_item,
                                            L_Pack_DerUn_ItemSearchTbl(i).forecast_ind));
      end loop;
      
      --
    end loop;
    --
    close L_sys_refcur;
    --
    return;
    --
  EXCEPTION
    --
    WHEN NO_DATA_NEEDED THEN
      --
      RETURN;
      --
    WHEN OTHERS THEN
       L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));  
      --
      dbms_output.put_line(L_error_message);
      --  
      RETURN;
      --
    --
   
  
   
    
  
  END PACK_DERUNIT_ITEM_SEARCH;

---------------------------------------------------------------------------------
-- Name: PACK_DERUNIT_ITM_DETAIL_SEARCH
-- Purpose: Function to return component and derived unit items level in the
--          Dashboard, based on the pack/derived unit selected by the user.
---------------------------------------------------------------------------------

  FUNCTION PACK_DERUNIT_ITM_DETAIL_SEARCH(I_item_type       IN VARCHAR2,
                                          I_bu              IN area_tl.area%TYPE,
                                          I_agg_item     IN item_master.item%TYPE,
                                          I_execute_query   IN VARCHAR2 DEFAULT 'Y')
    RETURN XXADEO_PACK_DERU_RESULT_TBL
    PIPELINED AS
    
    
    L_program       VARCHAR2(64) := 'XXADEO_DASHBOARD_PACK_DERU_SQL.PACK_DERUNIT_ITM_DETAIL_SEARCH';
    L_error_message VARCHAR2(2000);
    --
    L_Pack_DerUn_ItemSearchTbl XXADEO_PACK_DERU_RESULT_TBL;
    L_string_query            VARCHAR2(20000);
    L_sys_refcur              SYS_REFCURSOR;
    --

  BEGIN
    --
    if I_execute_query = 'N' then
      --
      return;
      --
    end if;
    --
    
    if I_item_type = 'PACK_COMP' then
      
       L_string_query := q'{with t_binds as
                            (select :1  db_bu,
                                    :2  db_agg_item
                               from dual)
                           select new XXADEO_PACK_DERU_RESULT_OBJ(filter_org_id,
                                                                  status,
                                                                  pack_ind,
                                                                  pack_type,
                                                                  item_level,
                                                                  tran_level,
                                                                  check_uda_ind,
                                                                  group_no,
                                                                  group_name,
                                                                  dept,
                                                                  dept_name,
                                                                  class,
                                                                  class_name,
                                                                  subclass,
                                                                  sub_name,
                                                                  ranking_item,
                                                                  item,
                                                                  item_desc,
                                                                  item_type,
                                                                  agg_item,
                                                                  pack_qty,
                                                                  sub_typology,
                                                                  personalized_ind,
                                                                  standard_uom,
                                                                  derunit_conv_factor,
                                                                  qt_capacity,
                                                                  contain_uom_item,
                                                                  assortment_mode,
                                                                  range_size,
                                                                  orderable_ind,
                                                                  sellable_ind,
                                                                  std_str_tr_prc_prccntrl,
                                                                  std_str_tr_prc_itemprc,
                                                                  national_price_itmprc,
                                                                  national_price_prccntrl,
                                                                  national_price_check,
                                                                  margin_itmprc,
                                                                  margin_perc,
                                                                  margin_prccntrl,
                                                                  retail_prc_blocking,
                                                                  lifecycle_status,
                                                                  actif_ddate_item,
                                                                  forecast_ind)
                                      from xxadeo_v_dashboard_packitm_cmp i,
                                           t_binds b
                                      where 1 = 1
                                      }';    
    
    
      
    elsif I_item_type = 'STD_DERU' then
    
          L_string_query := q'{with t_binds as
                            (select :1  db_bu,
                                    :2  db_agg_item
                               from dual)
                           select new XXADEO_PACK_DERU_RESULT_OBJ(filter_org_id,
                                                                  status,
                                                                  pack_ind,
                                                                  pack_type,
                                                                  item_level,
                                                                  tran_level,
                                                                  check_uda_ind,
                                                                  group_no,
                                                                  group_name,
                                                                  dept,
                                                                  dept_name,
                                                                  class,
                                                                  class_name,
                                                                  subclass,
                                                                  sub_name,
                                                                  ranking_item,
                                                                  item,
                                                                  item_desc,
                                                                  item_type,
                                                                  agg_item,
                                                                  pack_qty,
                                                                  sub_typology,
                                                                  personalized_ind,
                                                                  standard_uom,
                                                                  derunit_conv_factor,
                                                                  qt_capacity,
                                                                  contain_uom_item,
                                                                  assortment_mode,
                                                                  range_size,
                                                                  orderable_ind,
                                                                  sellable_ind,
                                                                  std_str_tr_prc_prccntrl,
                                                                  std_str_tr_prc_itemprc,
                                                                  national_price_itmprc,
                                                                  national_price_prccntrl,
                                                                  national_price_check,
                                                                  margin_itmprc,
                                                                  margin_perc,
                                                                  margin_prccntrl,
                                                                  retail_prc_blocking,
                                                                  lifecycle_status,
                                                                  actif_ddate_item,
                                                                  forecast_ind)
                                      from xxadeo_v_dashboard_deruni_itms i,
                                           t_binds b
                                      where 1 = 1
                                      }';    
    else
      RETURN;
    end if;
    
    
    -- BU
    if I_bu is not null and I_agg_item is not null then
      
      L_string_query := L_string_query ||
                        q'{ and (nvl(i.filter_org_id,b.db_bu) = b.db_bu)
                            and (i.agg_item = b.db_agg_item)}';
      --
    else
      RETURN;
    end if;
  
  
   
    --
    -- bulk query to table type
    --
    open L_sys_refcur for L_string_query
      using I_bu, I_agg_item;
    loop
      --
      fetch L_sys_refcur bulk collect
        into L_Pack_DerUn_ItemSearchTbl;
      exit when L_Pack_DerUn_ItemSearchTbl.count = 0;
      --
      -- pipe data form collection
      --
      for i in 1 .. L_Pack_DerUn_ItemSearchTbl.count loop
        --
       pipe row(XXADEO_PACK_DERU_RESULT_OBJ(L_Pack_DerUn_ItemSearchTbl(i).filter_org_id,
                                            L_Pack_DerUn_ItemSearchTbl(i).status,
                                            L_Pack_DerUn_ItemSearchTbl(i).pack_ind,
                                            L_Pack_DerUn_ItemSearchTbl(i).pack_type,
                                            L_Pack_DerUn_ItemSearchTbl(i).item_level,
                                            L_Pack_DerUn_ItemSearchTbl(i).tran_level,
                                            L_Pack_DerUn_ItemSearchTbl(i).check_uda_ind,
                                            L_Pack_DerUn_ItemSearchTbl(i).group_no,
                                            L_Pack_DerUn_ItemSearchTbl(i).group_name,
                                            L_Pack_DerUn_ItemSearchTbl(i).dept,
                                            L_Pack_DerUn_ItemSearchTbl(i).dept_name,
                                            L_Pack_DerUn_ItemSearchTbl(i).class,
                                            L_Pack_DerUn_ItemSearchTbl(i).class_name,
                                            L_Pack_DerUn_ItemSearchTbl(i).subclass,
                                            L_Pack_DerUn_ItemSearchTbl(i).sub_name,
                                            L_Pack_DerUn_ItemSearchTbl(i).ranking_item,
                                            L_Pack_DerUn_ItemSearchTbl(i).item,
                                            L_Pack_DerUn_ItemSearchTbl(i).item_desc,
                                            L_Pack_DerUn_ItemSearchTbl(i).item_type,
                                            L_Pack_DerUn_ItemSearchTbl(i).agg_item,
                                            L_Pack_DerUn_ItemSearchTbl(i).pack_qty,
                                            L_Pack_DerUn_ItemSearchTbl(i).sub_typology,
                                            L_Pack_DerUn_ItemSearchTbl(i).personalized_ind,
                                            L_Pack_DerUn_ItemSearchTbl(i).standard_uom,
                                            L_Pack_DerUn_ItemSearchTbl(i).derunit_conv_factor,
                                            L_Pack_DerUn_ItemSearchTbl(i).qt_capacity,
                                            L_Pack_DerUn_ItemSearchTbl(i).contain_uom_item,
                                            L_Pack_DerUn_ItemSearchTbl(i).assortment_mode,
                                            L_Pack_DerUn_ItemSearchTbl(i).range_size,
                                            L_Pack_DerUn_ItemSearchTbl(i).orderable_ind,
                                            L_Pack_DerUn_ItemSearchTbl(i).sellable_ind,
                                            L_Pack_DerUn_ItemSearchTbl(i).std_str_tr_prc_itemprc,
                                            L_Pack_DerUn_ItemSearchTbl(i).std_str_tr_prc_prccntrl,
                                            L_Pack_DerUn_ItemSearchTbl(i).national_price_itmprc,
                                            L_Pack_DerUn_ItemSearchTbl(i).national_price_prccntrl,
                                            L_Pack_DerUn_ItemSearchTbl(i).national_price_check,
                                            L_Pack_DerUn_ItemSearchTbl(i).margin_itmprc,
                                            L_Pack_DerUn_ItemSearchTbl(i).margin_prccntrl,
                                            L_Pack_DerUn_ItemSearchTbl(i).margin_perc,
                                            L_Pack_DerUn_ItemSearchTbl(i).retail_prc_blocking,
                                            L_Pack_DerUn_ItemSearchTbl(i).lifecycle_status,
                                            L_Pack_DerUn_ItemSearchTbl(i).actif_ddate_item,
                                            L_Pack_DerUn_ItemSearchTbl(i).forecast_ind));
      end loop;
      
      --
    end loop;
    --
    close L_sys_refcur;
    --
    return;
    --
  EXCEPTION
    --
    WHEN NO_DATA_NEEDED THEN
      --
      RETURN;
      --
    WHEN OTHERS THEN
       L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));  
      --
      dbms_output.put_line(L_error_message);
      --  
      RETURN;
      --
    --
   
  
   
    
  
  END PACK_DERUNIT_ITM_DETAIL_SEARCH;


---------------------------------------------------------------------------------
-- Name: PACK_DERUNIT_SUPP_SEARCH
-- Purpose: Function to return suppliers in the Dashboard, based on the
--          pack/derived unit selected by the user.
---------------------------------------------------------------------------------

  FUNCTION PACK_DERUNIT_SUPP_SEARCH(I_item_type       IN VARCHAR2,
                                    I_bu              IN area_tl.area%TYPE,
                                    I_agg_item        IN item_master.item%TYPE,
                                    I_execute_query   IN VARCHAR2 DEFAULT 'Y')
    RETURN XXADEO_PACK_DERU_SUPS_TBL
    PIPELINED AS
    
    
    L_program       VARCHAR2(64) := 'XXADEO_DASHBOARD_PACK_DERU_SQL.PACK_DERUNIT_SUPP_SEARCH';
    L_error_message VARCHAR2(2000);
    --
    L_Pack_DerUn_SuppSearchTbl XXADEO_PACK_DERU_SUPS_TBL;
    L_string_query            VARCHAR2(20000);
    L_sys_refcur              SYS_REFCURSOR;
    --

  BEGIN
    --
    if I_execute_query = 'N' then
      --
      return;
      --
    end if;
    --
    
    if I_item_type = 'PACK_COMP' then
      
       L_string_query := q'{with t_binds as
                            (select :1  db_bu,
                                    :2  db_agg_item
                               from dual)
                           select new XXADEO_PACK_DERU_SUPS_OBJ(flag,
                                     agg_item,
                                     item,
                                     status,
                                     pack_ind,
                                     pack_type,
                                     item_level,
                                     tran_level,
                                     check_uda_ind,
                                     supp_ind,
                                     group_no,
                                     group_name,
                                     dept,
                                     dept_name,
                                     class,
                                     class_name,
                                     subclass,
                                     sub_name,
                                     ranking_item,
                                     item_desc,
                                     derunit_conv_factor,
                                     qt_capacity,
                                     contain_uom_item,
                                     assortment_mode,
                                     range_size,
                                     orderable_ind,
                                     sellable_ind,
                                     pack_qty,
                                     retail_prc_blocking,
                                     lifecycle_status,
                                     actif_ddate_item,
                                     forecast_ind,
                                     sous_typo_step,
                                     standard_uom,
                                     supplier_site,
                                     supplier_site_desc,
                                     manufacturer_code,
                                     origin_country,
                                     status_link,
                                     gtin,
                                     supplier_pack_size,
                                     supplier_cost_price,
                                     currency,
                                     std_str_tr_prc_itemprc,
                                     national_price_itmprc,
                                     margin_itmprc,
                                     margin_perc,
                                     blocking_reason,
                                     qual_comp_status,
                                     link_end_date,
                                     supp_bu,
                                     item_bu)
                                from (select flag,
                                             agg_item,
                                             item,
                                             status,
                                             pack_ind,
                                             pack_type,
                                             item_level,
                                             tran_level,
                                             check_uda_ind,
                                             supp_ind,
                                             group_no,
                                             group_name,
                                             dept,
                                             dept_name,
                                             class,
                                             class_name,
                                             subclass,
                                             sub_name,
                                             ranking_item,
                                             item_desc,
                                             derunit_conv_factor,
                                             qt_capacity,
                                             contain_uom_item,
                                             assortment_mode,
                                             range_size,
                                             orderable_ind,
                                             sellable_ind,
                                             pack_qty,
                                             retail_prc_blocking,
                                             lifecycle_status,
                                             actif_ddate_item,
                                             forecast_ind,
                                             sous_typo_step,
                                             standard_uom,
                                             supplier_site,
                                             supplier_site_desc,
                                             manufacturer_code,
                                             origin_country,
                                             status_link,
                                             gtin,
                                             supplier_pack_size,
                                             supplier_cost_price,
                                             currency,
                                             std_str_tr_prc_itemprc,
                                             national_price_itmprc,
                                             margin_itmprc,
                                             margin_perc,
                                             blocking_reason,
                                             qual_comp_status,
                                             link_end_date,
                                             supp_bu,
                                             item_bu
                                        from xxadeo_v_dashboard_packi_sups h
                                      union all
                                      select flag,
                                             agg_item,
                                             item,
                                             status,
                                             pack_ind,
                                             pack_type,
                                             item_level,
                                             tran_level,
                                             check_uda_ind,
                                             supp_ind,
                                             group_no,
                                             group_name,
                                             dept,
                                             dept_name,
                                             class,
                                             class_name,
                                             subclass,
                                             sub_name,
                                             ranking_item,
                                             item_desc,
                                             derunit_conv_factor,
                                             qt_capacity,
                                             contain_uom_item,
                                             assortment_mode,
                                             range_size,
                                             orderable_ind,
                                             sellable_ind,
                                             pack_qty,
                                             retail_prc_blocking,
                                             lifecycle_status,
                                             actif_ddate_item,
                                             forecast_ind,
                                             sous_typo_step,
                                             standard_uom,
                                             supplier_site,
                                             supplier_site_desc,
                                             manufacturer_code,
                                             origin_country,
                                             status_link,
                                             gtin,
                                             supplier_pack_size,
                                             supplier_cost_price,
                                             currency,
                                             std_str_tr_prc_itemprc,
                                             national_price_itmprc,
                                             margin_itmprc,
                                             margin_perc,
                                             blocking_reason,
                                             qual_comp_status,
                                             link_end_date,
                                             supp_bu,
                                             item_bu
                                        from xxadeo_v_dashboard_packic_sups d) pack_deru,
                               t_binds b
                                where 1 = 1
                                }';    
    
      
    elsif I_item_type = 'STD_DERU' then
    
          L_string_query := q'{with t_binds as
                            (select :1  db_bu,
                                    :2  db_agg_item
                               from dual)
                           select new XXADEO_PACK_DERU_SUPS_OBJ(flag,
                                     agg_item,
                                     item,
                                     status,
                                     pack_ind,
                                     pack_type,
                                     item_level,
                                     tran_level,
                                     check_uda_ind,
                                     supp_ind,
                                     group_no,
                                     group_name,
                                     dept,
                                     dept_name,
                                     class,
                                     class_name,
                                     subclass,
                                     sub_name,
                                     ranking_item,
                                     item_desc,
                                     derunit_conv_factor,
                                     qt_capacity,
                                     contain_uom_item,
                                     assortment_mode,
                                     range_size,
                                     orderable_ind,
                                     sellable_ind,
                                     pack_qty,
                                     retail_prc_blocking,
                                     lifecycle_status,
                                     actif_ddate_item,
                                     forecast_ind,
                                     sous_typo_step,
                                     standard_uom,
                                     supplier_site,
                                     supplier_site_desc,
                                     manufacturer_code,
                                     origin_country,
                                     status_link,
                                     gtin,
                                     supplier_pack_size,
                                     supplier_cost_price,
                                     currency,
                                     std_str_tr_prc_itemprc,
                                     national_price_itmprc,
                                     margin_itmprc,
                                     margin_perc,
                                     blocking_reason,
                                     qual_comp_status,
                                     link_end_date,
                                     supp_bu,
                                     item_bu)
                            from (select flag,
                                         agg_item,
                                         item,
                                         status,
                                         pack_ind,
                                         pack_type,
                                         item_level,
                                         tran_level,
                                         check_uda_ind,
                                         supp_ind,
                                         group_no,
                                         group_name,
                                         dept,
                                         dept_name,
                                         class,
                                         class_name,
                                         subclass,
                                         sub_name,
                                         ranking_item,
                                         item_desc,
                                         derunit_conv_factor,
                                         qt_capacity,
                                         contain_uom_item,
                                         assortment_mode,
                                         range_size,
                                         orderable_ind,
                                         sellable_ind,
                                         pack_qty,
                                         retail_prc_blocking,
                                         lifecycle_status,
                                         actif_ddate_item,
                                         forecast_ind,
                                         sous_typo_step,
                                         standard_uom,
                                         supplier_site,
                                         supplier_site_desc,
                                         manufacturer_code,
                                         origin_country,
                                         status_link,
                                         gtin,
                                         supplier_pack_size,
                                         supplier_cost_price,
                                         currency,
                                         std_str_tr_prc_itemprc,
                                         national_price_itmprc,
                                         margin_itmprc,
                                         margin_perc,
                                         blocking_reason,
                                         qual_comp_status,
                                         link_end_date,
                                         supp_bu,
                                         item_bu
                                    from xxadeo_v_dashboard_deru_h_sups h
                                  union all
                                  select flag,
                                         agg_item,
                                         item,
                                         status,
                                         pack_ind,
                                         pack_type,
                                         item_level,
                                         tran_level,
                                         check_uda_ind,
                                         supp_ind,
                                         group_no,
                                         group_name,
                                         dept,
                                         dept_name,
                                         class,
                                         class_name,
                                         subclass,
                                         sub_name,
                                         ranking_item,
                                         item_desc,
                                         derunit_conv_factor,
                                         qt_capacity,
                                         contain_uom_item,
                                         assortment_mode,
                                         range_size,
                                         orderable_ind,
                                         sellable_ind,
                                         pack_qty,
                                         retail_prc_blocking,
                                         lifecycle_status,
                                         actif_ddate_item,
                                         forecast_ind,
                                         sous_typo_step,
                                         standard_uom,
                                         supplier_site,
                                         supplier_site_desc,
                                         manufacturer_code,
                                         origin_country,
                                         status_link,
                                         gtin,
                                         supplier_pack_size,
                                         supplier_cost_price,
                                         currency,
                                         std_str_tr_prc_itemprc,
                                         national_price_itmprc,
                                         margin_itmprc,
                                         margin_perc,
                                         blocking_reason,
                                         qual_comp_status,
                                         link_end_date,
                                         supp_bu,
                                         item_bu
                                    from xxadeo_v_dashboard_deru_d_sups d) pack_deru,
                                                                     t_binds b
                                                                where 1 = 1
                                                                }';    
    else
      RETURN;
    end if;
    
    
    -- BU
    if I_bu is not null and I_agg_item is not null then
      
      L_string_query := L_string_query ||
                        q'{ and (pack_deru.item = b.db_agg_item 
                                 or
                                 pack_deru.agg_item = b.db_agg_item)
                            and pack_deru.item_bu = b.db_bu
                            order by flag,supplier_site_desc,item}';
      --
    else
      RETURN;
    end if;
  
  
   
    --
    -- bulk query to table type
    --
    open L_sys_refcur for L_string_query
      using I_bu, I_agg_item;
    loop
      --
      fetch L_sys_refcur bulk collect
        into L_Pack_DerUn_SuppSearchTbl;
      exit when L_Pack_DerUn_SuppSearchTbl.count = 0;
      --
      -- pipe data form collection
      --
      for i in 1 .. L_Pack_DerUn_SuppSearchTbl.count loop
        --
         pipe row(XXADEO_PACK_DERU_SUPS_OBJ(L_Pack_DerUn_SuppSearchTbl(i).flag,
                                            L_Pack_DerUn_SuppSearchTbl(i).agg_item,
                                            L_Pack_DerUn_SuppSearchTbl(i).item,
                                            L_Pack_DerUn_SuppSearchTbl(i).status,
                                            L_Pack_DerUn_SuppSearchTbl(i).pack_ind,
                                            L_Pack_DerUn_SuppSearchTbl(i).pack_type,
                                            L_Pack_DerUn_SuppSearchTbl(i).item_level,
                                            L_Pack_DerUn_SuppSearchTbl(i).tran_level,
                                            L_Pack_DerUn_SuppSearchTbl(i).check_uda_ind,
                                            L_Pack_DerUn_SuppSearchTbl(i).supp_ind,
                                            L_Pack_DerUn_SuppSearchTbl(i).group_no,
                                            L_Pack_DerUn_SuppSearchTbl(i).group_name,
                                            L_Pack_DerUn_SuppSearchTbl(i).dept,
                                            L_Pack_DerUn_SuppSearchTbl(i).dept_name,
                                            L_Pack_DerUn_SuppSearchTbl(i).class,
                                            L_Pack_DerUn_SuppSearchTbl(i).class_name,
                                            L_Pack_DerUn_SuppSearchTbl(i).subclass,
                                            L_Pack_DerUn_SuppSearchTbl(i).sub_name,
                                            L_Pack_DerUn_SuppSearchTbl(i).ranking_item,
                                            L_Pack_DerUn_SuppSearchTbl(i).item_desc,
                                            L_Pack_DerUn_SuppSearchTbl(i).derunit_conv_factor,
                                            L_Pack_DerUn_SuppSearchTbl(i).qt_capacity,
                                            L_Pack_DerUn_SuppSearchTbl(i).contain_uom_item,
                                            L_Pack_DerUn_SuppSearchTbl(i).assortment_mode,
                                            L_Pack_DerUn_SuppSearchTbl(i).range_size,
                                            L_Pack_DerUn_SuppSearchTbl(i).orderable_ind,
                                            L_Pack_DerUn_SuppSearchTbl(i).sellable_ind,
                                            L_Pack_DerUn_SuppSearchTbl(i).pack_qty,
                                            L_Pack_DerUn_SuppSearchTbl(i).retail_prc_blocking,
                                            L_Pack_DerUn_SuppSearchTbl(i).lifecycle_status,
                                            L_Pack_DerUn_SuppSearchTbl(i).actif_ddate_item,
                                            L_Pack_DerUn_SuppSearchTbl(i).forecast_ind,
                                            L_Pack_DerUn_SuppSearchTbl(i).sous_typo_step,
                                            L_Pack_DerUn_SuppSearchTbl(i).standard_uom,
                                            L_Pack_DerUn_SuppSearchTbl(i).supplier_site,
                                            L_Pack_DerUn_SuppSearchTbl(i).supplier_site_desc,
                                            L_Pack_DerUn_SuppSearchTbl(i).manufacturer_code,
                                            L_Pack_DerUn_SuppSearchTbl(i).origin_country,
                                            L_Pack_DerUn_SuppSearchTbl(i).status_link,
                                            L_Pack_DerUn_SuppSearchTbl(i).gtin,
                                            L_Pack_DerUn_SuppSearchTbl(i).supplier_pack_size,
                                            L_Pack_DerUn_SuppSearchTbl(i).supplier_cost_price,
                                            L_Pack_DerUn_SuppSearchTbl(i).currency,
                                            L_Pack_DerUn_SuppSearchTbl(i).std_str_tr_prc_itemprc,
                                            L_Pack_DerUn_SuppSearchTbl(i).national_price_itmprc,
                                            L_Pack_DerUn_SuppSearchTbl(i).margin_itmprc,
                                            L_Pack_DerUn_SuppSearchTbl(i).margin_perc,
                                            L_Pack_DerUn_SuppSearchTbl(i).blocking_reason,
                                            L_Pack_DerUn_SuppSearchTbl(i).qual_comp_status,
                                            L_Pack_DerUn_SuppSearchTbl(i).link_end_date,
                                            L_Pack_DerUn_SuppSearchTbl(i).supp_bu,
                                            L_Pack_DerUn_SuppSearchTbl(i).item_bu));
      end loop;
      
      --
    end loop;
    --
    close L_sys_refcur;
    --
    return;
    --
  EXCEPTION
    --
    WHEN NO_DATA_NEEDED THEN
      --
      RETURN;
      --
    WHEN OTHERS THEN
       L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));  
      --
      dbms_output.put_line(L_error_message);
      --  
      RETURN;
      --
    --
   
  
   
    
  
  END PACK_DERUNIT_SUPP_SEARCH;

end XXADEO_DASHBOARD_PACK_DERU_SQL;
/