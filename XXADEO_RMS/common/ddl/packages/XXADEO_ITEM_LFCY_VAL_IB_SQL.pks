CREATE OR REPLACE PACKAGE XXADEO_ITEM_LFCY_VAL_IB_SQL AUTHID CURRENT_USER AS

/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Jorge Agra                                                   */
/*                                                                            */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package for RB117/RB118                                      */
/*               Validations for ITEM/BU relation                             */
/******************************************************************************/

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-09-04 - Jorge Agra - BUG#365 IB-DISC_01 Compare with vdate+1,         */
/*                           not vdate. Next val date must be set to:         */
/*                           end date - 1                                     */
/* 2018-09-06 - Jorge Agra - Bew rule IB-ASI-05 (ref complete BASA)           */
/* 2018-09-20 - Jorge Agra - BUG#443 IB-ACOM_02 Only check sales start date   */
/*                                              not its value                 */
/* 2018-09-21 - Jorge Agra - BUG#450 IB-ACOM_031                              */
/* 2018-10-02 - Jorge Agra - BUG#520 IB-ASI-03 consider ASI and ACOM for      */
/*                           components                                       */
/* 2018-10-02 - Jorge Agra - include validation of next_val_dt in all rules   */
/* 2018-11-06 - Jorge Agra - IB_DISC_02 should also check STOP DATE           */
/* 2018-11-19 - Jorge Agra - IB_DISC_02 group by to avoid merge unstable      */
/*                           results on duplicate rows                        */
/* 2018-11-26 - Jorge Agra - ORACR_00257 Use CFA ECOTAX_ELC_COMP instead of   */
/*                           EXP_CATEGORY to identify ecotaxes in IB-ASI-02   */
/******************************************************************************/

/******************************************************************************/
/* Passage Actif Service Interne (IB-ASI-01)	                                */
/* Retail price should be different from 0 (Retail price is different from 0  */
/* for the National Retail Zone of the BU)                                    */
/******************************************************************************/
FUNCTION IB_ASI_01( O_error_message   IN OUT  varchar2,
                             O_rule_changes    OUT      number,
                             O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                             I_pid             IN       number,
                             I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                             I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* Passage Actif Service Interne (IS-ASI-02)	                                */
/* Ensure that for a D3E eligible product (ecopart is applicable), there is   */
/* the corresponding D3E cost line (ecopart cost component) provided.         */
/******************************************************************************/
FUNCTION IB_ASI_02( O_error_message   IN OUT  varchar2,
                             O_rule_changes    OUT      number,
                             O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                             I_pid             IN       number,
                             I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                             I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* Passage Actif Service Interne (IS-ASI-03)	                                                  */
/* All components of the pack are in Active for Internal Service status.            */
/******************************************************************************/
FUNCTION IB_ASI_03( O_error_message   IN OUT  varchar2,
                             O_rule_changes    OUT      number,
                             O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                             I_pid             IN       number,
                             I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                             I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;

/******************************************************************************/
/* Passage Actif Service Interne (IB-ASI-04)	                                */
/* For orderable items, at least one active supplier must exist.              */
/******************************************************************************/
FUNCTION IB_ASI_04( O_error_message   IN OUT  varchar2,
                             O_rule_changes    OUT      number,
                             O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                             I_pid             IN       number,
                             I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                             I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;

/******************************************************************************/
/* BASA complete referencing (IB-ASI-05)     	                                */
/*                                                                            */
/******************************************************************************/
FUNCTION IB_ASI_05( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;

/******************************************************************************/
/* Passage Actif Commerce (IB-ACOM-01)      	                                */
/* All components of the pack must be in the Active for Commerce status.      */
/******************************************************************************/
FUNCTION IB_ACOM_01( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;

/******************************************************************************/
/* Passage Actif Commerce (IB-ACOM-02)      	                                */
/* Sales Start Date must be populated                                         */
/******************************************************************************/
FUNCTION IB_ACOM_02( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result           OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid              IN       number,
                     I_param1           IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2           IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* Passage Actif Commerce (IB-ACOM-03)      	                                */
/* The Active for commerce Target Date must be defined. The item will go to   */
/* Actif Commerce when this date has been reached.                            */
/******************************************************************************/
FUNCTION IB_ACOM_031(O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result           OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid              IN       number,
                     I_param1           IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2           IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* Passage Actif Commerce (IB-ACOM-031)      	                                */
/* For sellable items, rhe Active for commerce Target Date must be <= tomorrow*/
/* Set next val date if its in the future                                     */
/******************************************************************************/
FUNCTION IB_ACOM_03( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result           OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid              IN       number,
                     I_param1           IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2           IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* Passage Actif Commerce (IS-ACOM-04)      	                                */
/* Quality certification must be received for ALL the active item-supplier    */
/* combinations                                                               */
/******************************************************************************/
FUNCTION IB_ACOM_04(O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result           OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid              IN       number,
                     I_param1           IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2           IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* Passage en Supression (IB-DISC-01)        	                                */
/* The Sales Stop Date has been reached.                                      */
/******************************************************************************/
FUNCTION IB_DISC_01( O_error_message  IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;

/******************************************************************************/
/* Passage en Supression (IB-DISC-02)        	                                */
/* At least one of the pack components is in the Discontinued Status OR the   */
/* Sales Stop Date of the pack has been reached.                              */
/******************************************************************************/
FUNCTION IB_DISC_02( O_error_message  IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* Passage Suprime (IS-DEL-01)               	                                */
/* No stock movements (stock, orders, sales, returns) have taken place for 2  */
/* years (configurable period). Additionall, it needs to be ensured that the  */
/* item has been created more than 2 years ago (the same configurable period  */
/* of time).                                                                  */
/******************************************************************************/
FUNCTION IB_DEL_01( O_error_message  IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result         OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid            IN       number,
                     I_param1         IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2         IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;

/******************************************************************************/
/* Passage Suprim? (IS-DEL-02)               	                                */
/* One of the pack components has the Deleted status.                         */
/******************************************************************************/
FUNCTION IB_DEL_02( O_error_message  IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result         OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid            IN       number,
                     I_param1         IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2         IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;

END XXADEO_ITEM_LFCY_VAL_IB_SQL;
/


