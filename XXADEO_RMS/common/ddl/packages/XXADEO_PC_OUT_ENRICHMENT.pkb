CREATE OR REPLACE PACKAGE BODY XXADEO_PC_OUT_ENRICHMENT AS
  -------------------------------------------------------------------------------------------------------
  -- PRIVATE PROCEDURE
  -------------------------------------------------------------------------------------------------------
  PROCEDURE POPULATE(o_status OUT NUMBER, o_message OUT VARCHAR2) IS
  
    -- This cursor will get all the price changes in the staging area that have been created or modified and that not published yet
    CURSOR c_unpub_pc IS
      SELECT PRICE_CHANGE_ID,
             ITEM,
             LOCATION,
             ZONE,
             PRICE_TYPE,
             LAST_UPDATE_DATETIME,
             ERROR_DETAIL,
             EVENT_TYPE,
             REASON_CODE,
             STATUS,
             ITEM_TYPE,
             ZONE_PRICE_TYPE,
             SEQ_NO
        FROM XXADEO_STG_PC_HDR_OUT
       WHERE STATUS in ('N') -- Status is unpublished
       order by seq_no asc;
  
    l_unpub_pc            c_unpub_pc%rowtype;
    l_last_msg_event_type varchar2(20);
    l_last_seq_no         number;
  
    -- This cursor will get the last pc change seq_no and event_type for the price change id that is being processed by the main cursor c_unpub_pc
    CURSOR c_get_last_pc(in_price_chg_id number, in_seq number) IS
      SELECT hdr.seq_no, hdr.EVENT_TYPE
        FROM XXADEO_STG_PC_HDR_OUT hdr
       WHERE hdr.price_change_id = in_price_chg_id
         and hdr.seq_no =
             (select max(seq_no)
                from XXADEO_STG_PC_HDR_OUT
               where price_change_id = in_price_chg_id)
         and hdr.seq_no > in_seq -- And the sequence number being found is higher than the one given
         and hdr.status = 'N';
    --and rownum = 1; No need for this since you are alredy gettign the max seq_no
  
    -- This cursor will get the RPM Zone type of a Zone ID. A single record must exist by Zone.
    -- This cursor will get the RPM Zone type of a Zone ID. A single record must exist by Zone. Also for the ADEO extract of this field, if the Zone is National then NAT else is ZON
    CURSOR c_get_price_type(zone number) IS
      SELECT decode(bpz.Bu_Zone_Type, 'NATZ', 'NAT', 'ZON')
        from XXADEO_RPM_BU_PRICE_ZONES bpz
       where bpz.zone_id = zone;
  
    -- This cursor will get the Item type that is defined as an UDA of the Item. This UDA must be defined for every item
    CURSOR c_get_item_type IS
      select uda_value
        from UDA_ITEM_LOV
       WHERE item = l_unpub_pc.item
         and uda_id = (select value_1
                         from xxadeo_mom_dvm
                        where func_area = 'UDA'
                          and parameter = 'ITEM_TYPE');
  
    CURSOR c_get_rpm_zone_type(I_location number) is
      select bpz.Bu_Zone_Type
        from XXADEO_RPM_BU_PRICE_ZONES bpz
       where bpz.zone_id in (select zone_id
                               from rpm_zone_location
                              where location = I_location);
  
    CURSOR c_get_bu_zone_type(I_zone number) is
      select bpz.Bu_Zone_Type
        from XXADEO_RPM_BU_PRICE_ZONES bpz
       where bpz.zone_id = I_zone;
  
  
  
   L_pub_status XXADEO_STG_PC_HDR_OUT.STATUS%TYPE;
  BEGIN
  
    -- This step discard the messages at the XXADEO_STG_PC_DTL_OUT that are not the most recent to be processed;
    -- For the records that are not the max Price_event_payload_id at the XXADEO_STG_PC_DTL_OUT set pub_indicator = 'D' (discarded);
    -- This Update are for the item, location, and pc beeing processed at XXADEO_STG_PC_HDR_OUT.
    update XXADEO_STG_PC_DTL_OUT dtl1
       set dtl1.pub_indicator = 'D'
     where dtl1.pub_indicator = 'N'
       and dtl1.price_event_payload_id not in
           (select max(price_event_payload_id)
              from XXADEO_STG_PC_DTL_OUT dtl2
             where dtl1.price_change_id = dtl2.price_change_id
               and dtl1.location = dtl2.location
               and dtl1.item = dtl2.item
               and dtl2.pub_indicator = 'N');
  
    -- This step will get all the price changes at DTL STG which have no HDR record in status N
    -- This may happen in case a dtl is created but no touch is made to the PC HDR.
    -- It will insert for those records in the HDR table as THEADMOD
    INSERT INTO XXADEO_STG_PC_HDR_OUT
      (PRICE_CHANGE_ID,
       ITEM,
       ZONE,
       LOCATION,
       CREATE_DATETIME,
       LAST_UPDATE_DATETIME,
       EFFECTIVE_DATE,
       REASON_CODE,
       CHANGE_AMOUNT,
       CHANGE_CURRENCY,
       MULTI_UNITS,
       MULTI_UNIT_RETAIL,
       MULTI_UNIT_SELLING_UOM,
       MULTI_UNIT_RETAIL_CURRENCY,
       EVENT_TYPE,
       STATUS,
       PUB_INDICATOR,
       SEQ_NO)
      select PRICE_CHANGE_ID,
             ITEM,
             ZONE_ID,
             LOCATION,
             CREATE_DATE,
             SYSDATE,
             EFFECTIVE_DATE,
             REASON_CODE,
             CHANGE_AMOUNT, --SELLING_RETAIL
             CHANGE_CURRENCY, --SELLING_RETAIL_CURRENCY,
             MULTI_UNITS,
             MULTI_UNIT_RETAIL,
             MULTI_SELLING_UOM,
             MULTI_UNIT_RETAIL_CURRENCY,
             'MOD',
             'N',
             'N',
             XXADEO_PC_OUT_SEQ.Nextval
        from rpm_price_change pc
       where pc.price_change_id in
             (select distinct dtl.price_change_id
                from xxadeo_stg_pc_dtl_out dtl
               where dtl.pub_indicator = 'N'
                 and not exists
               (select 1
                        from xxadeo_stg_pc_hdr_out hdr
                       where hdr.price_change_id = dtl.price_change_id
                         and (status = 'N' or status = 'E')));
  
    -- Open the cursor and loop for each pc record found
   OPEN c_unpub_pc;
    LOOP
      FETCH c_unpub_pc
        INTO l_unpub_pc;
    
      EXIT WHEN c_unpub_pc%notfound;
    
    
      DECLARE
        e_line_error exception;
        l_error_message XXADEO_STG_PC_HDR_OUT.ERROR_DETAIL%TYPE;
      
      BEGIN
        ----------------------------------------------------------------
        -- Check if for a pc record other pc records exist for the same pc.
        -- Depending on the event captured the current price change in the l_unpub_pc may endup beind discarded 'D'
        -- Also addition information will be define dfor the PC in the staging area: ITEM_TYPE and the PRICE_TYPE
      
      l_last_msg_event_type := null;
      l_last_seq_no  := null;
      
      
      Select status 
      into L_pub_status
       FROM XXADEO_STG_PC_HDR_OUT hdr
       WHERE hdr.price_change_id = l_unpub_pc.price_change_id
         and hdr.seq_no = l_unpub_pc.seq_no;
      
      if L_pub_status = 'D' then
         continue;
        end if;  
      
      
        -- Get the ITEM_TYPE
        IF l_unpub_pc.item_type is null then
          OPEN c_get_item_type;
          FETCH c_get_item_type
            INTO l_unpub_pc.item_type;
          CLOSE c_get_item_type;
        
          IF l_unpub_pc.item_type is null then
            l_unpub_pc.ERROR_DETAIL := 'ITEM_TYPE not found';
            raise e_line_error;
          END IF;
        
        END IF;
      
        -- PRICE_TYPE and ZONE_PRICE_TYPE
        IF l_unpub_pc.price_type is null then
        
          -- If a location is defined then PC is at Location level
          IF l_unpub_pc.location is not null then
            l_unpub_pc.price_type := 'LOC';
          
            OPEN c_get_rpm_zone_type(l_unpub_pc.location);
            FETCH c_get_rpm_zone_type
              into l_unpub_pc.zone_price_type;
            CLOSE c_get_rpm_zone_type;
            
            if l_unpub_pc.zone_price_type is null then
               l_unpub_pc.ERROR_DETAIL := 'ZONE_PRICE_TYPE not found';
              raise e_line_error;
            end if;
          
            -- If a Zone is specified we must identify if is a REGZ or NATZ
          ELSIF l_unpub_pc.zone is not null then
            OPEN c_get_price_type(l_unpub_pc.zone);
            FETCH c_get_price_type
              INTO l_unpub_pc.price_type;
            CLOSE c_get_price_type;
          
            OPEN c_get_bu_zone_type(l_unpub_pc.zone);
            FETCH c_get_bu_zone_type
              into l_unpub_pc.zone_price_type;
            CLOSE c_get_bu_zone_type;
          
           if l_unpub_pc.zone_price_type is null then
               l_unpub_pc.ERROR_DETAIL := 'ZONE_PRICE_TYPE not found';
              raise e_line_error;
            end if;
            
            -- If no Zone Price Type tehn raise error
            IF l_unpub_pc.price_type is null then
              l_unpub_pc.ERROR_DETAIL := 'PRICE_TYPE not found';
              raise e_line_error;
            END IF;
          END IF;
        END IF;
      
        -- When the message taken from staging refers to a CRE
        IF l_unpub_pc.event_type like 'CRE' then
          -- Reset local var
          l_last_msg_event_type := null;
        
          -- Check for any other PC with the same ID (so changes made after the record being processed)
          Open c_get_last_pc(l_unpub_pc.price_change_id, l_unpub_pc.Seq_No);
          Fetch c_get_last_pc
            into l_last_seq_no, l_last_msg_event_type;
          close c_get_last_pc;
        
          -- No other changes then just set the record as Ready to be published
          IF l_last_msg_event_type is null then
            UPDATE XXADEO_STG_PC_HDR_OUT hdr
               SET STATUS               = 'R',
                   LAST_UPDATE_DATETIME = sysdate,
                   ITEM_TYPE            = l_unpub_pc.item_type,
                   PRICE_TYPE           = l_unpub_pc.price_type,
                   ZONE_PRICE_TYPE      = l_unpub_pc.zone_price_type
             WHERE hdr.seq_no = l_unpub_pc.seq_no
               and price_change_id = l_unpub_pc.price_change_id
               and status = 'N';
          
            -- If a Delete has been captured for the PC, then ignore its CRE and discard the record
          elsif l_last_msg_event_type like 'DEL' then
            UPDATE XXADEO_STG_PC_HDR_OUT hdr
               SET STATUS = 'D', LAST_UPDATE_DATETIME = sysdate
             WHERE hdr.seq_no >= l_unpub_pc.seq_no
               and hdr.seq_no <= l_last_seq_no
               and price_change_id = l_unpub_pc.price_change_id
               and status = 'N';
               
               -- update dtl to discard related records
               UPDATE XXADEO_STG_PC_DTL_OUT dtl
                  SET pub_indicator = 'D', last_update_datetime = sysdate
                  where price_change_id = l_unpub_pc.price_change_id
                  and pub_indicator = 'N';
               
               
          
            -- If another CRE has been captured for this PC, then discard any previous records (CRE, DEL..) and set this last one as the one to be published. 
          elsif l_last_msg_event_type like 'CRE' then
            UPDATE XXADEO_STG_PC_HDR_OUT hdr
               SET STATUS               = 'R',
                   LAST_UPDATE_DATETIME = sysdate,
                   ITEM_TYPE            = l_unpub_pc.item_type,
                   PRICE_TYPE           = l_unpub_pc.price_type,
                   ZONE_PRICE_TYPE      = l_unpub_pc.zone_price_type
             WHERE hdr.seq_no = l_last_seq_no
               and price_change_id = l_unpub_pc.price_change_id
               and status = 'N';
          
            UPDATE XXADEO_STG_PC_HDR_OUT hdr
               SET STATUS = 'D', LAST_UPDATE_DATETIME = sysdate
             WHERE hdr.seq_no < l_last_seq_no
               and price_change_id = l_unpub_pc.price_change_id
               and status = 'N';
          
          END IF;
        END IF;
      
        -- When the message taken from staging refers to a MOD
        IF l_unpub_pc.event_type like 'MOD' then
        
          -- Check for any other PC with the same ID (so changes made after the record being processed)
          Open c_get_last_pc(l_unpub_pc.price_change_id, l_unpub_pc.Seq_No);
          Fetch c_get_last_pc
            into l_last_seq_no, l_last_msg_event_type;
          close c_get_last_pc;
        
          -- No other changes then just set the record as Ready to be published
          IF l_last_msg_event_type is null then
            UPDATE XXADEO_STG_PC_HDR_OUT hdr
               SET STATUS               = 'R',
                   LAST_UPDATE_DATETIME = sysdate,
                   ITEM_TYPE            = l_unpub_pc.item_type,
                   PRICE_TYPE           = l_unpub_pc.price_type,
                   ZONE_PRICE_TYPE      = l_unpub_pc.zone_price_type
             WHERE hdr.seq_no = l_unpub_pc.seq_no
               and price_change_id = l_unpub_pc.price_change_id
               and status = 'N';
          
          ELSE 
            UPDATE XXADEO_STG_PC_HDR_OUT hdr
               SET STATUS               = 'D',
                   LAST_UPDATE_DATETIME = sysdate
             WHERE hdr.seq_no = l_unpub_pc.seq_no
               and price_change_id = l_unpub_pc.price_change_id
               and status = 'N';
          
          END IF;
        
        END IF;
      
        -- When the message taken from staging refers to a DEL
        IF l_unpub_pc.event_type like 'DEL' then
        
          -- Check for any other PC with the same ID (so changes made after the record being processed)
          Open c_get_last_pc(l_unpub_pc.price_change_id, l_unpub_pc.Seq_No);
          Fetch c_get_last_pc
            into l_last_seq_no, l_last_msg_event_type;
          close c_get_last_pc;
        
          -- No other changes then just set the record as Ready to be published
          IF l_last_msg_event_type is null then
            UPDATE XXADEO_STG_PC_HDR_OUT hdr
               SET STATUS               = 'R',
                   LAST_UPDATE_DATETIME = sysdate,
                   ITEM_TYPE            = l_unpub_pc.item_type,
                   PRICE_TYPE           = l_unpub_pc.price_type,
                   ZONE_PRICE_TYPE      = l_unpub_pc.zone_price_type
             WHERE hdr.seq_no = l_unpub_pc.seq_no
               and price_change_id = l_unpub_pc.price_change_id
               and status = 'N';
          
            -- If a CRE has been captured for this PC, then discard any previous records (DEL, CRE..) and set this last one as the one to be published.
          elsif l_last_msg_event_type like 'CRE' then
            UPDATE XXADEO_STG_PC_HDR_OUT hdr
               SET STATUS               = 'R',
                   EVENT_TYPE           = 'MOD',
                   LAST_UPDATE_DATETIME = sysdate,
                   ITEM_TYPE            = l_unpub_pc.item_type,
                   PRICE_TYPE           = l_unpub_pc.price_type,
                   ZONE_PRICE_TYPE      = l_unpub_pc.zone_price_type
             WHERE hdr.seq_no = l_last_seq_no
               and price_change_id = l_unpub_pc.price_change_id
               and status = 'N';
          
            UPDATE XXADEO_STG_PC_HDR_OUT hdr
               SET STATUS = 'D', LAST_UPDATE_DATETIME = sysdate
             WHERE hdr.seq_no < l_last_seq_no
               and price_change_id = l_unpub_pc.price_change_id
               and status = 'N';
          
          elsif l_last_msg_event_type like 'DEL' then
            UPDATE XXADEO_STG_PC_HDR_OUT hdr
               SET STATUS               = 'R',
                   LAST_UPDATE_DATETIME = sysdate,
                   ITEM_TYPE            = l_unpub_pc.item_type,
                   PRICE_TYPE           = l_unpub_pc.price_type,
                   ZONE_PRICE_TYPE      = l_unpub_pc.zone_price_type
             WHERE hdr.seq_no = l_last_seq_no
               and price_change_id = l_unpub_pc.price_change_id
               and status = 'N';
          
            UPDATE XXADEO_STG_PC_HDR_OUT hdr
               SET STATUS = 'D', LAST_UPDATE_DATETIME = sysdate
             WHERE hdr.seq_no < l_last_seq_no
               and price_change_id = l_unpub_pc.price_change_id
               and status = 'N';
          END IF;
        END IF;
      
      EXCEPTION
        WHEN e_line_error then
          UPDATE XXADEO_STG_PC_HDR_OUT
             SET ERROR_DETAIL         = l_unpub_pc.ERROR_DETAIL,
                 STATUS               = 'E',
                 LAST_UPDATE_DATETIME = sysdate
           WHERE PRICE_CHANGE_ID = l_unpub_pc.PRICE_CHANGE_ID
             AND status = 'N';
          --o_status := 1;
      
        WHEN others then
          l_error_message := sqlerrm;
          UPDATE XXADEO_STG_PC_HDR_OUT
             SET ERROR_DETAIL         = l_error_message,
                 STATUS               = 'E',
                 LAST_UPDATE_DATETIME = sysdate
           WHERE PRICE_CHANGE_ID = l_unpub_pc.PRICE_CHANGE_ID
             AND status = 'N';
          --o_status := 1;
      
      END;
    END LOOP;
    CLOSE c_unpub_pc;
  
    if o_status is null then
      o_status := 0;
    end if;
  
  EXCEPTION
  
    WHEN others then
      o_message := sqlerrm;
      o_status  := 1;
    
  END POPULATE;

--------------------------------------------------------------------------------------------------------
END XXADEO_PC_OUT_ENRICHMENT;
/ 