CREATE OR REPLACE PACKAGE XXADEO_NOTIFICATIONS_SQL IS

--------------------------------------------------------------------------------
/*************************************************************************** **/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package for RB107/118, RB34a                                 */
/******************************************************************************/
--------------------------------------------------------------------------------
FUNCTION CREATE_RAF_NOTIFICATION(O_error_message        OUT VARCHAR2,
                                 I_application_code     IN  RAF_NOTIFICATION.APPLICATION_CODE%TYPE,
                                 I_notification_type    IN  RAF_NOTIFICATION_TYPE_B.NOTIFICATION_TYPE_CODE%TYPE,
                                 I_notification_desc    IN  RAF_NOTIFICATION.NOTIFICATION_DESC%TYPE,
                                 I_notification_context IN  VARCHAR2,
                                 I_user                 IN  RAF_NOTIFICATION_RECIPIENTS.RECIPIENT_ID%TYPE DEFAULT GET_USER,
                                 I_errors_count         IN  NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END XXADEO_NOTIFICATIONS_SQL;
/
