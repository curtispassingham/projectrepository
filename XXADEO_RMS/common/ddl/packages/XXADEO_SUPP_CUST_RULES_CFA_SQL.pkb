CREATE OR REPLACE package body XXADEO_SUPP_CUST_RULES_CFA_SQL is

  -- Author  : Paulo Mamede / Tiago Torres
  -- Created : 24/05/2018
  -- Purpose : Custom Rules for Supplier CFA

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-07-19 - Jorge Agra - BUG#120 Invalid error message (PREV ADM SUPP     */
/*                           DBG_SQL.MSG                                      */
/* 2018-07-27 - Jorge Agra - BUG#179 Invalid error message (GLN)              */
/*                           Fix: Clear CFA after validation fails            */
/*                                no rollback.                                */
/* 2018-08-28 - Jorge Agra - BUG#314 Message keys swap                        */
/* 2018-10-25 - Filipa Neves - Added check all validations and RG7            */
/* 2018-10-25 - Jorge Agra - Added RG6 from XXADEO_CUSTOM_RULES_SUPP_SQL      */
/* 2018-11-15 - Filipa Neves - fix on RG6 to only be triggered for status A   */
/* 2018-12-03 - Jorge Agra - BUG#695 single message for required fields when  */
/*                           shipping cost billing is checked                 */
/******************************************************************************/

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------

  procedure CLEAR_CFA(I_CFA in varchar2) 
  is
    L_program          VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.CLEAR_CFA';
    L_error_message    RTK_ERRORS.RTK_TEXT%TYPE;
  begin
    if CFA_SQL.SET_OUTPUT(
      L_error_message,
      CFA_SQL.CFA_EXT,
      I_CFA) = FALSE 
    then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
    end if;
  end;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------

  FUNCTION ASSOC_SUPPLIER_ORG_UNIT(O_error_message IN OUT VARCHAR2,
                                   L_supplier      IN SUPS_CFA_EXT.SUPPLIER%TYPE,
                                   L_supplier_cfa  IN CFA_SQL.GP_FIELD_VALUE%TYPE)
    RETURN BOOLEAN IS

    --variables
    L_program          VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.ASSOC_SUPPLIER_ORG_UNIT';
    L_total_ou_sup_cfa NUMBER;

    --cursors

    --
    cursor C_check_org_unit_sup_cfa is
      select 1
        from partner_org_unit poucfa
       where poucfa.partner_type = 'U'
         and poucfa.partner = L_supplier_cfa
         and exists (select 1
                from partner_org_unit pousup
               where pousup.org_unit_id = poucfa.org_unit_id
                 and pousup.partner_type = 'U'
                 and pousup.partner = L_SUPPLIER);
    --

  BEGIN

    --
    open C_check_org_unit_sup_cfa;
    fetch C_check_org_unit_sup_cfa
      into L_total_ou_sup_cfa;

    -- supplier org unit should be the same as supplier cfa org unit
    if C_check_org_unit_sup_cfa%notfound then
      --
      close C_check_org_unit_sup_cfa;
      --
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CFA_INV_PREV_ADMIN_ORG_UN');
      --
      return FALSE;
    end if;

    close C_check_org_unit_sup_cfa;

    --
    return TRUE;
    --

    --Exceptions

  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
  END ASSOC_SUPPLIER_ORG_UNIT;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------

  FUNCTION VALID_PREV_ADMIN_SUPPLIER_CFA(O_error_message IN OUT VARCHAR2)
    RETURN BOOLEAN IS

    --variables

    --
    L_program VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.VALID_PREV_ADMIN_SUPPLIER_CFA';
    --
    L_exists             BOOLEAN;
    L_field_value        CFA_SQL.GP_FIELD_VALUE%TYPE;
    L_field_desc_value   CFA_SQL.GP_FIELD_DESC_VALUE%TYPE;
    L_data_type          CFA_SQL.GP_DATA_TYPE%TYPE;
    L_supplier_cfa_field cfa_attrib.view_col_name%TYPE;
    L_supplier_cfa_id    CFA_SQL.GP_FIELD_VALUE%TYPE;
    L_supplier_field     CFA_SQL.GP_FIELD_VALUE%TYPE;
    L_supplier           SUPS.SUPPLIER%TYPE;
    L_group_id           cfa_attrib.group_id%TYPE;
    ---  

    --get supplier cfa field and group_id
    cursor C_get_supplier_cfa_field is
      select view_col_name, group_id
        from cfa_attrib
       where attrib_id =
             (select value_1
                from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                              I_parameter => GP_LAST_SUPPLIER_SITE_SUPP)));

    --get supplier
    cursor C_get_supplier is
      select nvl(supplier_parent, supplier)
        from sups
       where supplier = L_supplier_field;

  BEGIN
    --
    dbms_application_info.set_module('XXADEO_SUPP_CUST_RULES_CFA_SQL', 'VALID_PREV_ADMIN_SUPPLIER_CFA');
    dbms_application_info.set_client_info('SUPPLIER=' || GP_SUPPLIER);
    ---
    DBG_SQL.MSG(L_program,
               'Begin: '||L_program);    
    ---
    open C_get_supplier_cfa_field;
    fetch C_get_supplier_cfa_field
      into L_supplier_cfa_field, L_group_id;
    close C_get_supplier_cfa_field;
    --

    --get supplier CFA value 
    if NOT CFA_SQL.GET_VALUE(O_error_message,
                             L_field_value,
                             L_field_desc_value,
                             L_data_type,
                             L_supplier_cfa_field) then
      return FALSE;
    end if;
    --
    L_supplier_cfa_id := L_field_value;

    DBG_SQL.MSG(L_program,
               'pre-ADDR: L_field_value=' || L_field_value
               || ' L_field_desc_value=' || L_field_desc_value
               || ' L_supplier_cfa_field=' || L_supplier_cfa_field);

    --validate that the entered supplier cfa correspond to a valid order supplier site (associated to an address type 04)
    if L_supplier_cfa_id is NOT NULL then
      if ADDRESS_SQL.VALID_ORD_ADDR(O_error_message,
                                    L_exists,
                                    L_supplier_cfa_id) then
        --
        if L_exists = FALSE then
          clear_cfa(GP_LAST_SUPPLIER_SITE_SUPP);
          O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CFA_INV_PREV_ADMIN_SUPP');
          return FALSE;
        end if;
        --
      else
        clear_cfa(GP_LAST_SUPPLIER_SITE_SUPP);
        return FALSE;
      end if;
    end if;

    --get supplier value 

    L_field_value := NULL;

    if NOT CFA_SQL.GET_VALUE(O_error_message,
                             L_field_value,
                             L_field_desc_value,
                             L_data_type,
                             GP_SUPPLIER) then
      return FALSE;
    end if;

    L_supplier_field := L_field_value;

    --
    open C_get_supplier;
    fetch C_get_supplier
      into L_supplier;
    close C_get_supplier;
    --

    DBG_SQL.MSG(L_program,
               'pos-ADDR: L_field_value=' || L_field_value
               || ' L_supplier_cfa_field=' || L_supplier_cfa_field);

    --validate that the new supplier is associated at least to the same org unit as the old one
    if L_supplier_field is NOT NULL and L_supplier_cfa_id is NOT NULL then  
      if NOT ASSOC_SUPPLIER_ORG_UNIT(O_error_message,
                                     L_field_value,
                                     L_supplier_cfa_id) then
        ---
        clear_cfa(GP_LAST_SUPPLIER_SITE_SUPP);
        return FALSE;
      end if;
    end if;
    --
    return TRUE;
    --

    --Exceptions

  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
  END VALID_PREV_ADMIN_SUPPLIER_CFA;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------

  FUNCTION VALID_LAST_SUPPLIER_SITE_SUPP(O_error_message IN OUT VARCHAR2)
    RETURN BOOLEAN IS

    --variables

    --
    L_program VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.VALID_LAST_SUPPLIER_SITE_SUPP';
    --
  BEGIN

    --validate previous admin supplier

    if NOT VALID_PREV_ADMIN_SUPPLIER_CFA(O_error_message) then
      return FALSE;
    end if;

    --
    return TRUE;
    --

    --Exceptions

  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
  END VALID_LAST_SUPPLIER_SITE_SUPP;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------

  FUNCTION DEPARTMENT_MANDATORY_CFA(O_error_message IN OUT VARCHAR2,
                                    I_supplier_id   IN SUPS.SUPPLIER%TYPE)
    RETURN BOOLEAN IS

    --variables

    --
    L_program                   VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.DEPARTMENT_MANDATORY_CFA';
    --
    L_dep_col_name              CFA_ATTRIB.STORAGE_COL_NAME%TYPE;
    L_sup_dep_cfa               DEPS.DEPT%TYPE;
    --


    cursor C_get_dep_col_name is
    select storage_col_name
      from cfa_attrib
     where attrib_id =
           (select value_1
              from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                            I_parameter => GP_DEPARTMENT_SUPP)));

  BEGIN

    --
    open C_get_dep_col_name;
    fetch C_get_dep_col_name
      into L_dep_col_name;
    close C_get_dep_col_name;
    --

     BEGIN
      --

        execute immediate 'select '|| L_dep_col_name ||' from sups_cfa_ext
                where supplier = '|| I_supplier_id || ' and group_id =
                      (select group_id
                         from cfa_attrib
                        where attrib_id =
                              (select value_1
                                 from table(xxadeo_get_mom_dvm(I_func_area => ''CFA'',
                                                               I_parameter => '''||GP_DEPARTMENT_SUPP||''' ))))'
                          into L_sup_dep_cfa;
             --
     EXCEPTION
      when OTHERS then
        L_sup_dep_cfa := NULL;
     END; 


     --if supplier is a supplier site order then department cfa field is mandatory
     if L_sup_dep_cfa is null then
       --
       O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CFA_DEPARTMENT_SUP_NULL');
       return FALSE;
       --
     else
       --
       return TRUE;
       --
     end if;


    --    
    return TRUE;
    --

    --Exceptions

  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
  END DEPARTMENT_MANDATORY_CFA;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------

  FUNCTION SUPPLIER_TYPE_MANDATORY_CFA(O_error_message IN OUT VARCHAR2,
                                       I_supplier_id   IN SUPS.SUPPLIER%TYPE)
    RETURN BOOLEAN IS

    --variables
    --
    L_program                      VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.SUPPLIER_TYPE_MANDATORY_CFA';
    --
    L_sup_type_col_name            CFA_ATTRIB.STORAGE_COL_NAME%TYPE;
    L_sup_type_cfa                 CODE_DETAIL.CODE_DESC%TYPE;
    --

    cursor C_get_sup_type_col_name is
    select storage_col_name
      from cfa_attrib
     where attrib_id =
           (select value_1
              from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                            I_parameter => GP_SUPPLIER_TYPE_SUPP)));                                         

  BEGIN

    --
    open C_get_sup_type_col_name;
    fetch C_get_sup_type_col_name
      into L_sup_type_col_name;
    close C_get_sup_type_col_name;
    --

     BEGIN
     --

      execute immediate 'select '|| L_sup_type_col_name ||' from sups_cfa_ext
              where supplier = '|| I_supplier_id || ' and group_id =
                    (select group_id
                       from cfa_attrib
                      where attrib_id =
                            (select value_1
                               from table(xxadeo_get_mom_dvm(I_func_area => ''CFA'',
                                                             I_parameter => '''||GP_SUPPLIER_TYPE_SUPP||''' ))))'
                        into L_sup_type_cfa;
           --
     EXCEPTION
      when OTHERS then
        L_sup_type_cfa := NULL;
     END;   


     --if supplier cfa value is a supplier site order then supplier type cfa field is mandatory

     if L_sup_type_cfa is null then
       --
       O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CFA_SUP_TYPE_NULL');
       return FALSE;
       --
     else
       --
       return TRUE;
       --
     end if;

    --
    return TRUE;
    --

    --Exceptions

  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
  END SUPPLIER_TYPE_MANDATORY_CFA;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------

  FUNCTION SUPPLIER_IC_MANDATORY_FIELDS(O_error_message IN OUT VARCHAR2,
                                        I_supplier_id   IN SUPS.SUPPLIER%TYPE)
    RETURN BOOLEAN IS

    -- variables
    L_program             VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.SUPPLIER_IC_MANDATORY_FIELDS';
    --
    L_valid_dep           BOOLEAN := TRUE;
    L_valid_typ           BOOLEAN := TRUE;
    L_error_message_dep   RTK_ERRORS.RTK_TEXT%TYPE;
    L_error_message_typ   RTK_ERRORS.RTK_TEXT%TYPE;

  BEGIN

    DBG_SQL.MSG(L_program,'Begin: '||L_program);

    -- validate supplier department and supplier type mandatory fields

    if NOT DEPARTMENT_MANDATORY_CFA(L_error_message_dep,
                                    I_supplier_id) then
      L_valid_dep := FALSE;
    end if;
    --
    if NOT SUPPLIER_TYPE_MANDATORY_CFA(L_error_message_typ,
                                       I_supplier_id) then
      L_valid_typ := FALSE;
    end if;
    --
    if NOT L_valid_dep and NOT L_valid_typ then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CFA_SUP_DEPT_TYPE_NULL');
      return false;
    end if;
    --
    if NOT L_valid_dep then
      O_error_message := L_error_message_dep;  
      return FALSE;
    end if;
    --  
    if NOT L_valid_typ then
      O_error_message := L_error_message_typ;  
      return FALSE;
    end if;
    --
    return TRUE;
    --

    --Exceptions

  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
  END SUPPLIER_IC_MANDATORY_FIELDS;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------
  FUNCTION CHECK_BARCODE_FORMAT(O_error_message IN OUT LOGGER_LOGS.TEXT%TYPE)
    RETURN BOOLEAN IS
    --
    L_program VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.CHECK_BARCODE_FORMAT';
    --

    L_field_value        CFA_SQL.GP_FIELD_VALUE%TYPE;
    L_field_desc_value   CFA_SQL.GP_FIELD_DESC_VALUE%TYPE;
    L_data_type          CFA_SQL.GP_DATA_TYPE%TYPE;
    L_item               ITEM_MASTER.ITEM%TYPE;
    L_supp_GLN_cfa_id    cfa_attrib.view_col_name%TYPE;



    --get supplier cfa gln field
    cursor C_get_supplier_cfa_gln_field is
      select view_col_name
        from cfa_attrib
       where attrib_id =
             (select value_1
                from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                              I_parameter => GP_GLN_CODE_SUPP)));
    --
  BEGIN


    --
    open C_get_supplier_cfa_gln_field;
    fetch C_get_supplier_cfa_gln_field
      into L_supp_GLN_cfa_id;
    close C_get_supplier_cfa_gln_field;
    --



    --get supplier CFA GLN value 
    if NOT CFA_SQL.GET_VALUE(O_error_message,
                             L_field_value,
                             L_field_desc_value,
                             L_data_type,
                             L_supp_GLN_cfa_id) then
      return FALSE;
    end if;
    --
    L_item := L_field_value;
    --

    if L_item IS NOT NULL then      

      if ITEM_NUMBER_TYPE_SQL.VALIDATE_FORMAT(O_error_message => O_error_message,
                                              I_item_no       => L_item,
                                              I_item_type     => GP_GLN_CODE_SUPP_TYPE) =
         FALSE then
        --
        --rollback;
        clear_cfa(GP_GLN_CODE_SUPP);
        ---
        O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CFA_GLN_INV_SUPP');
        ---
        RETURN FALSE;
        --
      end if;

    end if;

    --
    RETURN TRUE;
    --
  EXCEPTION
    --
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      --
      RETURN FALSE;
      --
  END CHECK_BARCODE_FORMAT;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------

  FUNCTION UNIT_FREE_PORT_MANDATORY_CFA(O_error_message IN OUT VARCHAR2)
    RETURN BOOLEAN IS

    --variables

    --
    L_program           VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.UNIT_FREE_PORT_MANDATORY_CFA';
    L_unit_fp_cfa_field cfa_attrib.view_col_name%TYPE;
    L_field_value       CFA_SQL.GP_FIELD_VALUE%TYPE;
    L_field_desc_value  CFA_SQL.GP_FIELD_DESC_VALUE%TYPE;
    L_data_type         CFA_SQL.GP_DATA_TYPE%TYPE;
    --

    cursor C_get_UNIT_FREE_PORT_SUPP is
      select view_col_name
        from cfa_attrib
       where attrib_id =
             (select value_1
                from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                              I_parameter => GP_UNIT_FREE_PORT_SUPP)));
    --

  BEGIN

    --
    open C_get_UNIT_FREE_PORT_SUPP;
    fetch C_get_UNIT_FREE_PORT_SUPP
      into L_unit_fp_cfa_field;
    close C_get_UNIT_FREE_PORT_SUPP;
    --

    --validate if unit free port field is empty

    if CFA_SQL.GET_VALUE(O_error_message    => O_error_message,
                         O_field_value      => L_field_value,
                         O_field_desc_value => L_field_desc_value,
                         O_data_type        => L_data_type,
                         I_field_name       => L_unit_fp_cfa_field) = FALSE then
      --
      return FALSE;
      --
    else
      if L_field_value is NULL then
        O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CFA_PORT_CHARGE');
        return FALSE;
      end if;
    end if;

    --
    return TRUE;
    --

    --Exceptions

  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
  END UNIT_FREE_PORT_MANDATORY_CFA;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------

  FUNCTION VALUE_FREE_PORT_MANDATORY_CFA(O_error_message IN OUT VARCHAR2)
    RETURN BOOLEAN IS

    --variables

    --
    L_program            VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.VALUE_FREE_PORT_MANDATORY_CFA';
    L_value_fp_cfa_field cfa_attrib.view_col_name%TYPE;
    L_field_value        CFA_SQL.GP_FIELD_VALUE%TYPE;
    L_field_desc_value   CFA_SQL.GP_FIELD_DESC_VALUE%TYPE;
    L_data_type          CFA_SQL.GP_DATA_TYPE%TYPE;
    --

    cursor C_get_VALUE_FREE_PORT_SUPP is
      select view_col_name
        from cfa_attrib
       where attrib_id =
             (select value_1
                from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                              I_parameter => GP_VALUE_FREE_PORT_SUPP)));
    --

  BEGIN

    --
    open C_get_VALUE_FREE_PORT_SUPP;
    fetch C_get_VALUE_FREE_PORT_SUPP
      into L_value_fp_cfa_field;
    close C_get_VALUE_FREE_PORT_SUPP;
    --

    --validate if value free port field is empty

    if CFA_SQL.GET_VALUE(O_error_message    => O_error_message,
                         O_field_value      => L_field_value,
                         O_field_desc_value => L_field_desc_value,
                         O_data_type        => L_data_type,
                         I_field_name       => L_value_fp_cfa_field) =
       FALSE then
      --
      return FALSE;
      --
    else
      if L_field_value is NULL then
        O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CFA_PORT_CHARGE_VALUE'); --
        return FALSE;
      end if;
    end if;

    --
    return TRUE;
    --

    --Exceptions

  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
  END VALUE_FREE_PORT_MANDATORY_CFA;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------

  FUNCTION FREIGHT_CHARGE_MANDATORY_CFA(O_error_message IN OUT VARCHAR2)
    RETURN BOOLEAN IS

    --variables

    --
    L_program                  VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.FREIGHT_CHARGE_MANDATORY_CFA';
    L_freight_charge_cfa_field cfa_attrib.view_col_name%TYPE;
    L_field_value              CFA_SQL.GP_FIELD_VALUE%TYPE;
    L_field_desc_value         CFA_SQL.GP_FIELD_DESC_VALUE%TYPE;
    L_data_type                CFA_SQL.GP_DATA_TYPE%TYPE;
    --

    cursor C_get_FREIGHT_CHARGE_SUPP is
      select view_col_name
        from cfa_attrib
       where attrib_id =
             (select value_1
                from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                              I_parameter => GP_FREIGHT_CHARGE_SUPP)));
    --

  BEGIN

    --
    open C_get_FREIGHT_CHARGE_SUPP;
    fetch C_get_FREIGHT_CHARGE_SUPP
      into L_freight_charge_cfa_field;
    close C_get_FREIGHT_CHARGE_SUPP;
    --

    --validate if freight charge field is empty

    if CFA_SQL.GET_VALUE(O_error_message    => O_error_message,
                         O_field_value      => L_field_value,
                         O_field_desc_value => L_field_desc_value,
                         O_data_type        => L_data_type,
                         I_field_name       => L_freight_charge_cfa_field) =
       FALSE then
      --
      return FALSE;
      --
    else
      if L_field_value is NULL then
        O_error_message := SQL_LIB.GET_MESSAGE_TEXT('CFA_PORT_CHARGE_AMNT');
        return FALSE;
      end if;
    end if;

    --
    return TRUE;
    --

    --Exceptions

  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
  END FREIGHT_CHARGE_MANDATORY_CFA;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------

  FUNCTION SHIPPING_COST_MANDATORY_FIELDS(O_error_message IN OUT VARCHAR2)
    RETURN BOOLEAN IS

    --variables 

    --
    L_program VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.SHIPPING_COST_MANDATORY_FIELDS';
    --

    L_cost_bill_cfa_field cfa_attrib.view_col_name%TYPE;
    L_field_value         CFA_SQL.GP_FIELD_VALUE%TYPE;
    L_field_desc_value    CFA_SQL.GP_FIELD_DESC_VALUE%TYPE;
    L_data_type           CFA_SQL.GP_DATA_TYPE%TYPE;

    L_validate            BOOLEAN;
    IO_error_message      RTK_ERRORS.RTK_TEXT%TYPE;

    --cursors

    cursor C_get_shipping_cost is
      select view_col_name
        from cfa_attrib
       where attrib_id =
             (select value_1
                from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                              I_parameter => GP_SHIPPING_COST_BILLING_SUPP)));
    --

  BEGIN

    --
    open C_get_shipping_cost;
    fetch C_get_shipping_cost
      into L_cost_bill_cfa_field;
    close C_get_shipping_cost;
    --

    --
    if CFA_SQL.GET_VALUE(O_error_message    => O_error_message,
                         O_field_value      => L_field_value,
                         O_field_desc_value => L_field_desc_value,
                         O_data_type        => L_data_type,
                         I_field_name       => L_cost_bill_cfa_field) =
       FALSE then
      --
      RETURN FALSE;
      --
    end if;
    --
    if L_field_value = 'N' then

      --
      return TRUE;
      --

    else

      L_validate := TRUE;

      --validate Unit Free Port      
      if NOT UNIT_FREE_PORT_MANDATORY_CFA(IO_error_message) then
        L_validate      := FALSE;
        O_error_message := O_error_message || chr(10) || IO_error_message;
      end if;

      --validate Value Free Port
      if NOT VALUE_FREE_PORT_MANDATORY_CFA(IO_error_message) then
        L_validate      := FALSE;
        O_error_message := O_error_message || chr(10) || IO_error_message;
      end if;

      --validate Freight Charge
      if NOT FREIGHT_CHARGE_MANDATORY_CFA(IO_error_message) then
        L_validate      := FALSE;
        O_error_message := O_error_message || chr(10) || IO_error_message;
      end if;

      --
      if NOT L_validate then
        O_error_message := SQL_LIB.GET_MESSAGE_TEXT(GP_ERRKEY_SHIP_COST_BILL_SUPP);
        return FALSE;
      end if;
      --

    end if;
    --

    return TRUE;

    --Exceptions

  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
  END SHIPPING_COST_MANDATORY_FIELDS;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------

  FUNCTION VALIDATE_CFA_ATTRIB_GRP_SET(O_error_message IN OUT VARCHAR2)
    RETURN BOOLEAN IS

    --variables

    --
    L_program VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.VALIDATE_CFA_ATTRIB_GRP_SET';
    --

  BEGIN
    ---
    DBG_SQL.MSG(L_program, 'Start');
    ---
    --validate shipping cost and free port mandatory fields
    ---
    if NOT SHIPPING_COST_MANDATORY_FIELDS(O_error_message) then
      return FALSE;
    end if;
    --

    --
    return TRUE;
    --

    --Exceptions

  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
  END VALIDATE_CFA_ATTRIB_GRP_SET;
 
  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------
  -- validate if payment terms of payment site are different from payment terms of order site
  FUNCTION RUN_CUSTOM_RULES_SUP_RG6(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                                    I_supplier        IN SUPS.SUPPLIER%TYPE,
                                    I_supp_parent     IN SUPS.SUPPLIER_PARENT%TYPE)
  RETURN BOOLEAN IS
    --
    L_program            VARCHAR2(64) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.RUN_CUSTOM_RULES_SUP_RG6';
    --
    L_foo number := null;
    --
    --validate that all org_units from order site are in at least one payment site
    cursor c_validate_OU is
      select 1
        from partner_org_unit pouord
       where partner = i_supplier
         and not exists (select 0
                           from partner_org_unit poupay,
                                sups
                          where poupay.org_unit_id   = pouord.org_unit_id
                            and sups.supplier        = poupay.partner
                            and sups.supplier_parent = I_supp_parent
                            and exists (select 0
                                          from addr
                                         where module      = 'SUPP'
                                           and addr_type   = '06'
                                           and key_value_1 = sups.supplier))
         and rownum = 1;
    --
    cursor C_validate is
      select 1 from sups sord
      where sord.supplier = I_supplier
      -- chek is a sup site
      and sord.supplier_parent is not null
      and sord.sup_status  ='A'
      -- check is a ord site
      and exists (
        select 1 
          from addr aord
         where to_char(sord.supplier) = aord.key_value_1
           and aord.addr_type = '04'
           and aord.module = 'SUPP'
      )
      -- check each org units has at least one pay site with same parent and same pay terms
      and exists (
        select 1
          from partner_org_unit pouord
         where pouord.partner = sord.supplier
           and not exists (
            select 1
              from sups spay
             where spay.supplier_parent is not null
               --- same parent, same pay terms
               and spay.supplier_parent = sord.supplier_parent
               and nvl(spay.terms,-1) = nvl(sord.terms,-2)
               --- its a pay supplier site
               and exists (
                select 1 
                  from addr apay
                 where to_char(spay.supplier) = apay.key_value_1
                   and apay.addr_type = '06'
                   and apay.module = 'SUPP'
               )
               --- pay site is at same org unit (might be in others)
               and exists (
                select 1 
                  from partner_org_unit poupay
                 where poupay.partner = spay.supplier
                   and poupay.org_unit_id = pouord.org_unit_id
              )
           )
      )
    ;
    --
  BEGIN
    ---
    DBG_SQL.MSG(L_program,'Begin: '||L_program);
    ---
    l_foo := null;
    --
    open c_validate_OU;
    fetch c_validate_OU into L_foo;
    close c_validate_OU;
    --
    -- if the order site doesn't have all org_units mapped with at least one payment site, then this validation shouldn't be done
    if l_foo is not null then
      return true;
    end if;
    --
    L_foo := null;
    open C_validate;
    fetch C_validate into L_foo;
    close C_validate;
    
    if L_foo is not null then
        O_error_message := XXADEO_GET_RTK_TEXT_TL(O_error_message,
                                                  'XXADEO_SUP_CHG_STATUS_RG6');
        return false;
    end if;
    ---
    RETURN TRUE;
    --
  EXCEPTION
    --
    WHEN OTHERS THEN
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      --
      if C_validate%isopen then
        close C_validate;
      end if;
      --
      RETURN FALSE;
    --
  END RUN_CUSTOM_RULES_SUP_RG6;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------
  FUNCTION CHECK_ALL_VALIDATIONS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN BOOLEAN IS
    --variables
    --
    L_program VARCHAR2(80) := 'XXADEO_SUPP_CUST_RULES_CFA_SQL.CHECK_ALL_VALIDATIONS';
    --
    L_field_desc_value   CFA_SQL.GP_FIELD_DESC_VALUE%TYPE;
    L_data_type          CFA_SQL.GP_DATA_TYPE%TYPE;
    L_supplier           SUPS.SUPPLIER%TYPE;
    L_supp_parent        SUPS.SUPPLIER%TYPE;
    L_exists             BOOLEAN;
    --
    cursor C_check_supp_site is
      select s.supplier_parent
        from sups s
       where s.supplier = L_supplier; 
    --
  BEGIN
    ---
    DBG_SQL.MSG(L_program, 'Start');
    --return false;
    ---
    -- get supplier_id 
    ---
    if NOT CFA_SQL.GET_VALUE(O_error_message,
                             L_supplier,
                             L_field_desc_value,
                             L_data_type,
                             GP_SUPPLIER) then
      return FALSE;
    end if;
    --
    DBG_SQL.MSG(L_program, 'L_supplier=' || L_supplier);
    --
    -- validate department and supplier type
    open C_check_supp_site;
    fetch C_check_supp_site into L_supp_parent;
    close C_check_supp_site;
    ---
    -- should L_supplier null ne an error?
    ---
    if L_supp_parent is null or L_supplier is null then
      return true;
    end if;
    --
    if not ADDRESS_SQL.VALID_ORD_ADDR(O_error_message, L_exists, L_supplier) then
      return false;
    end if;
    ---
    if L_exists = TRUE then
      ---
      DBG_SQL.MSG(L_program, 'Supplier has addr 04 => ordering supplier');
      ---
      -- validate required fields (RG7)
      ---
      if NOT SUPPLIER_IC_MANDATORY_FIELDS(O_error_message,
                                          L_supplier) then
        return FALSE;
      end if;
      ---
      DBG_SQL.MSG(L_program, 'Mandatory fields OK');
      ---
      -- validate payment terms (RG6)
      ---
      if not RUN_CUSTOM_RULES_SUP_RG6(O_error_message   => O_error_message,
                                      I_supplier        => L_supplier,
                                      i_supp_parent     => l_supp_parent) then
        return false;                                                                        
      end if;
      ---
      DBG_SQL.MSG(L_program, 'Payment terms OK');
      ---
    end if;
    --
    return TRUE;
    --
  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      ---
      if C_check_supp_site%isopen then
        close C_check_supp_site;
      end if;
      ---
      return FALSE;
  END CHECK_ALL_VALIDATIONS;

BEGIN
  declare
    O_tbl DBG_SQL.TBL_DBG_OBJ;
  begin
    ---
    DBG_SQL.INIT(O_tbl);
    ---
  exception when others
    then null;
  end;
end XXADEO_SUPP_CUST_RULES_CFA_SQL;
/
