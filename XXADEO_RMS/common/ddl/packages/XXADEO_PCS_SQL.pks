CREATE OR REPLACE PACKAGE XXADEO_PCS_SQL AS
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package "XXADEO_PCS_SQL" for RB157                           */
/*               The RB157 extension includes the following process at store  */
/*               level:                                                       */
/*                 -	Processing of the PCS store level records, e.g. updating*/
/*                    status records;                                         */
/*                 -	Purging of old store level records.                     */
/*               At the BU level:                                             */
/*                 -	Processing of the PCS BU level records, e.g. updating   */
/*                    status records;                                         */
/*                 -	Purging of old BU level records.                        */
/******************************************************************************/
--------------------------------------------------------------------------------
FUNCTION PCS_LIFE_CYCLE(O_error_message OUT LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PURGE_PCS(O_error_message OUT LOGGER_LOGS.TEXT%TYPE,
                   I_purge_days    IN  NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END XXADEO_PCS_SQL;
--
/
