CREATE OR REPLACE PACKAGE XXADEO_BIZ_VALIDATIONS_SQL AS   
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/*               Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package "XXADEO_BIZ_VALIDATIONS_SQL"                         */
/******************************************************************************/
--------------------------------------------------------------------------------
 
  GP_CODE_TYPE_LANG                 VARCHAR2(4)           := 'ADLG';
  GP_CODE_TYPE_VAT                  VARCHAR2(4)           := 'ADVT';

--------------------------------------------------------------------------------
/*Description: This function check if the item is valid.                      */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM(O_error_message           IN OUT   LOGGER_LOGS.TEXT%TYPE,
                       O_item_status                OUT   ITEM_MASTER.STATUS%TYPE,
                       I_item                    IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/*Description: This function checks if item/supplier relations exist for the  */
/*             item.                                                          */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_SUPPLIER(O_error_message  IN OUT   LOGGER_LOGS.TEXT%TYPE,
                                O_valid_item_sup    OUT   VARCHAR2,
                                I_item           IN       ITEM_MASTER.ITEM%TYPE,
                                I_supplier       IN       ITEM_SUPPLIER.SUPPLIER%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/*Description: This function checks if item/location relations exist for the  */
/*             item.                                                          */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_RANGE(O_error_message     IN OUT   LOGGER_LOGS.TEXT%TYPE,
                             O_status               OUT   ITEM_LOC.STATUS%TYPE,
                             O_ranged_ind           OUT   ITEM_LOC.RANGED_IND%TYPE,
                             I_item              IN       ITEM_MASTER.ITEM%TYPE,
                             I_item_parent       IN       ITEM_MASTER.ITEM_PARENT%TYPE DEFAULT NULL,
                             I_loc               IN       ITEM_LOC.LOC%TYPE,
                             I_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/*Description: This function check if merch hierarchy exists.                 */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_MERCH_HIER(O_error_message     IN OUT   LOGGER_LOGS.TEXT%TYPE,
                             I_dept              IN       DEPS.DEPT%TYPE DEFAULT NULL,
                             I_class             IN       CLASS.CLASS%TYPE DEFAULT NULL,
                             I_subclass          IN       SUBCLASS.SUBCLASS%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/*Description: This function check if language exists.                        */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_LANGUAGE(O_error_message       IN OUT LOGGER_LOGS.TEXT%TYPE,
                           IO_error              IN OUT BOOLEAN,
                           I_lang                IN     LANG.LANG%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/*Description: This function check if Dept/Region/Type already exist.         */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_VAT_DEPT_REG_TYPE(O_error_message     IN OUT   LOGGER_LOGS.TEXT%TYPE,
                                    I_dept              IN       VAT_DEPS.DEPT%TYPE,
                                    I_vat_region        IN       VAT_DEPS.VAT_REGION%TYPE,
                                    I_vat_type          IN       VAT_DEPS.VAT_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/*Description: This function check if VAT Region is valid.                    */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_VAT_REGION(O_error_message     IN OUT   LOGGER_LOGS.TEXT%TYPE,
                             I_vat_region        IN       VAT_DEPS.VAT_REGION%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/*Description: This function check if VAT Code is valid.                      */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_VAT_CODE(O_error_message       IN OUT   LOGGER_LOGS.TEXT%TYPE,
                           I_vat_code            IN       VAT_CODES.VAT_CODE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/*Description: This function check if VAT Type is valid.                      */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_VAT_TYPE(O_error_message       IN OUT   LOGGER_LOGS.TEXT%TYPE,
                           I_vat_type            IN       VAT_DEPS.VAT_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/*Description: This function validates DOMAIN ID.  */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DOMAIN(O_error_message         IN OUT   LOGGER_LOGS.TEXT%TYPE,
                         I_domain_id             IN       DOMAIN.DOMAIN_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/*Description: This function validates DOMAIN Association.                    */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DOMAIN_ASSOC(O_error_message         IN OUT   LOGGER_LOGS.TEXT%TYPE,
                               I_process_id            IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                               I_domain_id             IN       DOMAIN.DOMAIN_ID%TYPE,
                               I_dept                  IN       DOMAIN_DEPT.DEPT%TYPE DEFAULT NULL,
                               I_class                 IN       DOMAIN_CLASS.CLASS%TYPE DEFAULT NULL,
                               I_subclass              IN       DOMAIN_SUBCLASS.SUBCLASS%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/*Description: This function validates MERCH HIER ACTIVATE rules.             */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_MERCH_HIER_ACT(O_error_message   IN OUT   LOGGER_LOGS.TEXT%TYPE,
                                 I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                                 I_status          IN       VARCHAR2,
                                 I_dept            IN       DEPS.DEPT%TYPE DEFAULT NULL,
                                 I_class           IN       CLASS.CLASS%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/*Description: This function validates MERCH HIER DEACTIVATE rules.           */
--------------------------------------------------------------------------------
FUNCTION VALIDATE_MERCH_HIER_DEACT(O_error_message   IN OUT   LOGGER_LOGS.TEXT%TYPE,
                                   I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                                   I_status          IN       VARCHAR2,
                                   I_dept            IN       DEPS.DEPT%TYPE DEFAULT NULL,
                                   I_class           IN       CLASS.CLASS%TYPE DEFAULT NULL,
                                   I_subclass        IN       SUBCLASS.SUBCLASS%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END XXADEO_BIZ_VALIDATIONS_SQL;
/
