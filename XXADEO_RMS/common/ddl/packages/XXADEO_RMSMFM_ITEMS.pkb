CREATE OR REPLACE PACKAGE BODY XXADEO_RMSMFM_ITEMS AS
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_RMSMFM_ITEMS.pkb
* Description:   Package that will be used by RIB to publish 
*                item information.
*               
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/

  LP_num_threads NUMBER := NULL;

  -- depending upon the message type, there will come a point in the publication process
  -- when error retry will no longer be possible.  this variable will track whether or
  -- not that point has been reached.
  LP_error_status VARCHAR2(1) := NULL;
  LP_min_seq_no   NUMBER := 0;

  LP_rowids_rec ITEM_ROWID_REC;

  PROGRAM_ERROR EXCEPTION;

  /*** Private Functions/Procedures ***/

  --------------------------------------------------------------------------------
  FUNCTION PROCESS_QUEUE_RECORD(O_error_message  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_break_loop     IN OUT BOOLEAN,
                                O_message        IN OUT NOCOPY RIB_OBJECT,
                                O_message_type   IN OUT VARCHAR2,
                                O_tran_level_ind IN OUT VARCHAR2,
                                I_queue_rec      IN ITEM_MFQUEUE%ROWTYPE,
                                I_queue_rowid    IN ROWID) RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION FIND_PARENT_OR_COMP(O_error_message     OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_parent_comp_found OUT BOOLEAN,
                               I_queue_rec         IN ITEM_MFQUEUE%ROWTYPE)
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION CLEAN_QUEUE(O_error_message  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_queue_rec      IN ITEM_MFQUEUE%ROWTYPE,
                       I_pub_info_rowid IN ROWID) RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION UPDATE_QUEUE_TABLE(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN BOOLEAN;
  -------------------------------------------------------------------------------------
  FUNCTION NESTED_TABLE_UNION(O_error_message   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_union_rowid_tbl IN OUT NOCOPY ROWID_TBL,
                              O_union_seq_tbl   IN OUT NOCOPY SEQ_TBL,
                              I_rowid_tbl       IN ROWID_TBL,
                              I_seq_tbl         IN SEQ_TBL) RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION ADD_ROWID_FOR_DELETE(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rowid         IN ROWID,
                                I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
  -------------------------------------------------------------------------------------
  FUNCTION ADD_ROWID_FOR_UPDATE(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rowid         IN ROWID,
                                I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
  -------------------------------------------------------------------------------------
  PROCEDURE HANDLE_ERRORS(O_status_code   IN OUT VARCHAR2,
                          O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_message       IN OUT NOCOPY RIB_OBJECT,
                          O_bus_obj_id    IN OUT NOCOPY RIB_BUSOBJID_TBL,
                          O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                          I_item          IN ITEMLOC_MFQUEUE.ITEM%TYPE,
                          I_seq_no        IN ITEMLOC_MFQUEUE.SEQ_NO%TYPE);

						
  -------------------------------------------------------------------------------------------------------
  -- Name: GETNXT
  -- Description: Procedure responsible for get next message that will be publshed
  -------------------------------------------------------------------------------------------------------						
  PROCEDURE GETNXT(O_status_code  OUT VARCHAR2,
                   O_error_msg    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_message_type OUT VARCHAR2,
                   O_message      OUT RIB_OBJECT,
                   O_bus_obj_id   OUT RIB_BUSOBJID_TBL,
                   O_routing_info OUT RIB_ROUTINGINFO_TBL,
                   I_num_threads  IN NUMBER DEFAULT 1,
                   I_thread_val   IN NUMBER DEFAULT 1) IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS.GETNXT';
  
    L_queue_rec      ITEM_MFQUEUE%ROWTYPE := NULL;
    L_break_loop     BOOLEAN := FALSE;
    L_hospital_found VARCHAR2(1) := 'N';
    L_rowid          ROWID;
    L_tran_level_ind ITEM_PUB_INFO.TRAN_LEVEL_IND%TYPE := NULL;
    L_old_rowids_rec ITEM_ROWID_REC := NULL;
    L_count          NUMBER;
    ---
    -- DRIVING CURSOR - selects ONE message from the queue
    ---
    cursor C_QUEUE is
      select q.item,
             q.supplier,
             q.country_id,
             q.dim_object,
             q.ref_item,
             q.pack_comp,
             q.image_name,
             q.uda_id,
             q.uda_value,
             q.uda_text,
             q.uda_date,
             q.message_type,
             q.ticket_type_id,
             q.relationship_id,
             q.related_item,
             q.approve_ind,
             q.seq_no,
             q.rowid
        from item_mfqueue q
       where q.seq_no =
             (select min(q2.seq_no)
                from item_mfqueue q2
               where q2.thread_no = I_thread_val
                 and q2.pub_status = 'U'
                 and seq_no > LP_min_seq_no
                 and (EXISTS (select 1
                                from item_master im
                               where im.status = 'A'
                                 and im.item = q2.item) OR
                      (q2.message_type = RMSMFM_ITEMS.ITEM_DEL)))
         and q.thread_no = I_thread_val;
  
    cursor C_CHECK_FOR_HOSPITAL_MSGS is
      select 'Y'
        from item_mfqueue
       where item = L_queue_rec.item
         and pub_status = 'H';
  
  BEGIN
  
    -- status of 'H'ospital
    LP_error_status := API_CODES.HOSPITAL;
  
    -- reset the global variable LP_min_seq_no
    LP_min_seq_no := 0;
  
    -- initialize table
    if (LP_rowids_rec.queue_rowid_tbl is NULL) then
      LP_rowids_rec.queue_rowid_tbl := XXADEO_RMSMFM_ITEMS.ROWID_TBL();
    end if;
    if (LP_rowids_rec.pub_info_rowid_tbl is NULL) then
      LP_rowids_rec.pub_info_rowid_tbl := XXADEO_RMSMFM_ITEMS.ROWID_TBL();
    end if;
    if (LP_rowids_rec.queue_upd_rowid_tbl is NULL) then
      LP_rowids_rec.queue_upd_rowid_tbl := XXADEO_RMSMFM_ITEMS.ROWID_TBL();
    end if;
    if (LP_rowids_rec.itemloc_rowid_tbl is NULL) then
      LP_rowids_rec.itemloc_rowid_tbl := XXADEO_RMSMFM_ITEMS.ROWID_TBL();
    end if;
    -- initialize seq table
    if (LP_rowids_rec.queue_seq_tbl is NULL) then
      LP_rowids_rec.queue_seq_tbl := XXADEO_RMSMFM_ITEMS.SEQ_TBL();
    end if;
    if (LP_rowids_rec.pub_info_item_tbl is NULL) then
      LP_rowids_rec.pub_info_item_tbl := XXADEO_RMSMFM_ITEMS.ITEM_TBL();
    end if;
    if (LP_rowids_rec.queue_upd_seq_tbl is NULL) then
      LP_rowids_rec.queue_upd_seq_tbl := XXADEO_RMSMFM_ITEMS.SEQ_TBL();
    end if;
    if (LP_rowids_rec.itemloc_seq_tbl is NULL) then
      LP_rowids_rec.itemloc_seq_tbl := XXADEO_RMSMFM_ITEMS.SEQ_TBL();
    end if;
  
    LOOP
    
      L_queue_rec    := NULL;
      O_message      := NULL;
      O_message_type := NULL;
      ---
      open C_QUEUE;
      fetch C_QUEUE
        into L_queue_rec.item,
             L_queue_rec.supplier,
             L_queue_rec.country_id,
             L_queue_rec.dim_object,
             L_queue_rec.ref_item,
             L_queue_rec.pack_comp,
             L_queue_rec.image_name,
             L_queue_rec.uda_id,
             L_queue_rec.uda_value,
             L_queue_rec.uda_text,
             L_queue_rec.uda_date,
             L_queue_rec.message_type,
             L_queue_rec.ticket_type_id,
             L_queue_rec.relationship_id,
             L_queue_rec.related_item,
             L_queue_rec.approve_ind,
             L_queue_rec.seq_no,
             L_rowid;
      close C_QUEUE;
    
      if L_queue_rec.item is NULL then
        O_status_code := API_CODES.NO_MSG;
        return;
      end if;
    
      open C_CHECK_FOR_HOSPITAL_MSGS;
      fetch C_CHECK_FOR_HOSPITAL_MSGS
        into L_hospital_found;
      close C_CHECK_FOR_HOSPITAL_MSGS;
    
      if L_hospital_found = 'Y' then
        O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP', NULL, NULL, NULL);
        raise PROGRAM_ERROR;
      end if;
    
      ----
      --- If PROCESS_QUEUE_RECORD fails, the struct that keeps track of which mfqueue
      --- records to delete/update should be reset.  Therefore, a snapshot of the struct
      --- (L_old_rowids_rec) is taken before the call to PROCESS_QUEUE_RECORD.
      --- If the function fails, the struct is reset back to the snapshot.
      ----
      L_old_rowids_rec := LP_rowids_rec;
    
      if PROCESS_QUEUE_RECORD(O_error_msg,
                              L_break_loop,
                              O_message,
                              O_message_type,
                              L_tran_level_ind,
                              L_queue_rec,
                              L_rowid) = FALSE then
        LP_rowids_rec := L_old_rowids_rec;
        raise PROGRAM_ERROR;
      end if;
    
      if O_message IS NULL then
        O_status_code := API_CODES.NO_MSG;
      else
        O_status_code  := API_CODES.NEW_MSG;
        O_bus_obj_id   := RIB_BUSOBJID_TBL(L_queue_rec.item);
        O_routing_info := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('tran_level_ind',
                                                                  L_tran_level_ind,
                                                                  NULL,
                                                                  NULL,
                                                                  NULL,
                                                                  NULL));
        LP_min_seq_no  := 0;
        
         Select count(*)
        into L_count
        from item_mfqueue
       where item = L_queue_rec.item
         and message_type = L_queue_rec.Message_Type
         and seq_no > L_queue_rec.seq_no;
    
      if L_count > 0 then
        delete from item_mfqueue
         where item = L_queue_rec.item
           and message_type = L_queue_rec.Message_Type
           and seq_no > L_queue_rec.seq_no;
      end if;
      end if;
      
      
    
      if L_break_loop = TRUE then
        EXIT;
      end if;
    
    END LOOP;
  
  EXCEPTION
    when OTHERS then
      if O_error_msg is NULL then
        O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_module,
                                          TO_CHAR(SQLCODE));
      end if;
    
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_queue_rec.item,
                    L_queue_rec.seq_no);
  END GETNXT;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: PUB_RETRY
  -- Description: the procedure is similar to get_next but this procedure is invoked by Hospital
  -------------------------------------------------------------------------------------------------------
  PROCEDURE PUB_RETRY(O_status_code  OUT VARCHAR2,
                      O_error_msg    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_message_type IN OUT VARCHAR2,
                      O_message      OUT RIB_OBJECT,
                      O_bus_obj_id   IN OUT RIB_BUSOBJID_TBL,
                      O_routing_info IN OUT RIB_ROUTINGINFO_TBL,
                      I_REF_OBJECT   IN RIB_OBJECT) IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS.PUB_RETRY';
  
    L_queue_rec      ITEM_MFQUEUE%ROWTYPE := NULL;
    L_break_loop     BOOLEAN := FALSE;
    L_rowid          ROWID := NULL;
    L_tran_level_ind ITEM_PUB_INFO.TRAN_LEVEL_IND%TYPE := NULL;
    L_old_rowids_rec ITEM_ROWID_REC := NULL;
  
    cursor C_RETRY_QUEUE is
      select q.item,
             q.supplier,
             q.country_id,
             q.dim_object,
             q.ref_item,
             q.pack_comp,
             q.image_name,
             q.uda_id,
             q.uda_value,
             q.uda_text,
             q.uda_date,
             q.message_type,
             q.relationship_id,
             q.related_item,
             q.approve_ind,
             q.seq_no,
             q.rowid
        from item_mfqueue q
       where q.seq_no = L_queue_rec.seq_no;
  
  BEGIN
  
    -- status of 'H'ostipal
    LP_error_status := API_CODES.HOSPITAL;
  
    -- initialize table
    if (LP_rowids_rec.queue_rowid_tbl is NULL) then
      LP_rowids_rec.queue_rowid_tbl := XXADEO_RMSMFM_ITEMS.ROWID_TBL();
    end if;
    if (LP_rowids_rec.pub_info_rowid_tbl is NULL) then
      LP_rowids_rec.pub_info_rowid_tbl := XXADEO_RMSMFM_ITEMS.ROWID_TBL();
    end if;
    if (LP_rowids_rec.queue_upd_rowid_tbl is NULL) then
      LP_rowids_rec.queue_upd_rowid_tbl := XXADEO_RMSMFM_ITEMS.ROWID_TBL();
    end if;
    if (LP_rowids_rec.itemloc_rowid_tbl is NULL) then
      LP_rowids_rec.itemloc_rowid_tbl := XXADEO_RMSMFM_ITEMS.ROWID_TBL();
    end if;
    -- initialize seq table
    if (LP_rowids_rec.queue_seq_tbl is NULL) then
      LP_rowids_rec.queue_seq_tbl := XXADEO_RMSMFM_ITEMS.SEQ_TBL();
    end if;
    if (LP_rowids_rec.pub_info_item_tbl is NULL) then
      LP_rowids_rec.pub_info_item_tbl := XXADEO_RMSMFM_ITEMS.ITEM_TBL();
    end if;
    if (LP_rowids_rec.queue_upd_seq_tbl is NULL) then
      LP_rowids_rec.queue_upd_seq_tbl := XXADEO_RMSMFM_ITEMS.SEQ_TBL();
    end if;
    if (LP_rowids_rec.itemloc_seq_tbl is NULL) then
      LP_rowids_rec.itemloc_seq_tbl := XXADEO_RMSMFM_ITEMS.SEQ_TBL();
    end if;
  
    --get info from routing info
    ---assuming the only thing in the routing info is the seq_no
    L_queue_rec.seq_no := O_routing_info(1).value;
  
    O_message := null;
  
    --get info from queue table
    open C_RETRY_QUEUE;
    fetch C_RETRY_QUEUE
      into L_queue_rec.item,
           L_queue_rec.supplier,
           L_queue_rec.country_id,
           L_queue_rec.dim_object,
           L_queue_rec.ref_item,
           L_queue_rec.pack_comp,
           L_queue_rec.image_name,
           L_queue_rec.uda_id,
           L_queue_rec.uda_value,
           L_queue_rec.uda_text,
           L_queue_rec.uda_date,
           L_queue_rec.message_type,
           L_queue_rec.relationship_id,
           L_queue_rec.related_item,
           L_queue_rec.approve_ind,
           L_queue_rec.seq_no,
           L_rowid;
    close C_RETRY_QUEUE;
  
    if L_queue_rec.item IS NULL then
      O_status_code := API_CODES.NO_MSG;
      return;
    end if;
  
    ----
    --- If PROCESS_QUEUE_RECORD fails, the struct that keeps track of which mfqueue
    --- records to delete/update should be reset.  Therefore, a snapshot of the struct
    --- (L_old_rowids_rec) is taken before the call to PROCESS_QUEUE_RECORD.
    --- If the function fails, the struct is reset back to the snapshot.
    ----
    L_old_rowids_rec := LP_rowids_rec;
  
    if PROCESS_QUEUE_RECORD(O_error_msg,
                            L_break_loop,
                            O_message,
                            O_message_type,
                            L_tran_level_ind,
                            L_queue_rec,
                            L_rowid) = FALSE then
      LP_rowids_rec := L_old_rowids_rec;
      raise PROGRAM_ERROR;
    end if;
  
    if O_message IS NULL then
      O_status_code := API_CODES.NO_MSG;
    else
      O_status_code  := API_CODES.NEW_MSG;
      O_bus_obj_id   := RIB_BUSOBJID_TBL(L_queue_rec.item);
      O_routing_info := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('tran_level_ind',
                                                                L_tran_level_ind,
                                                                NULL,
                                                                NULL,
                                                                NULL,
                                                                NULL));
    end if;
  
  EXCEPTION
    when OTHERS then
      if O_error_msg is NULL then
        O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_module,
                                          TO_CHAR(SQLCODE));
      end if;
    
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_queue_rec.item,
                    L_queue_rec.seq_no);
  END PUB_RETRY;
  
  
  -------------------------------------------------------------------------------------------------------
  -- Name: PROCESS_QUEUE_RECORD
  -- Description: This private function controls the building of Oracle Objects 
  --              (DESC or REF) given the business transaction's key values and a message type.
  -------------------------------------------------------------------------------------------------------
  FUNCTION PROCESS_QUEUE_RECORD(O_error_message  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_break_loop     IN OUT BOOLEAN,
                                O_message        IN OUT NOCOPY RIB_OBJECT,
                                O_message_type   IN OUT VARCHAR2,
                                O_tran_level_ind IN OUT VARCHAR2,
                                I_queue_rec      IN ITEM_MFQUEUE%ROWTYPE,
                                I_queue_rowid    IN ROWID) RETURN BOOLEAN IS
  
    L_module               VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS.PROCESS_QUEUE_RECORD';
    L_status_code          VARCHAR2(1) := NULL;
    L_message              RIB_OBJECT := NULL;
    L_published            ITEM_PUB_INFO.PUBLISHED%TYPE := NULL;
    L_appr_upon_create_ind ITEM_PUB_INFO.APPR_UPON_CREATE_IND%TYPE := NULL;
    L_parent_comp_found    BOOLEAN := FALSE;
    L_pub_info_rowid       ROWID := NULL;
    L_record_exists        BOOLEAN := FALSE;
    L_create_rowid         ROWID := NULL;
    L_create_seq_no        ITEM_MFQUEUE.SEQ_NO%TYPE := NULL;
    L_message_type         ITEM_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;
  
    cursor C_HDR_INFO is
      select published, tran_level_ind, appr_upon_create_ind, rowid
        from item_pub_info api
       where item = I_queue_rec.item;
  
  BEGIN
  
    O_message      := NULL;
    O_message_type := NULL;
  
    open C_HDR_INFO;
    fetch C_HDR_INFO
      into L_published,
           O_tran_level_ind,
           L_appr_upon_create_ind,
           L_pub_info_rowid;
    close C_HDR_INFO;
  
    if L_published = 'N' then
      ---
      ---
      -- If the approve message for the item's parent or item's component
      -- is found further down the queue, skip the current message.
      ---
      if I_queue_rec.approve_ind = 'Y' then
        if FIND_PARENT_OR_COMP(O_error_message,
                               L_parent_comp_found,
                               I_queue_rec) = FALSE then
          raise PROGRAM_ERROR;
        end if;
      end if;
      ---
      if L_parent_comp_found = FALSE then
        if CLEAN_QUEUE(O_error_message, I_queue_rec, L_pub_info_rowid) =
           FALSE then
          return FALSE;
        end if;
        ---
        -- An Item Create message will be generated if the current
        -- message changes the item status to approved, or if the message is
        -- adding an item/supp/country record to an already approved item.
        ---
        if (I_queue_rec.approve_ind = 'Y' and -- status has changed to approved
           L_appr_upon_create_ind = 'N') or
           (I_queue_rec.message_type IN (ISC_ADD, ISMC_ADD) and -- item/supp/country (sourcing/manufacture) added to approved item
           L_appr_upon_create_ind = 'Y') then
        
          L_message_type := ITEM_ADD;
        else
          ---
          --- Can't create a message yet
          ---
          if ADD_ROWID_FOR_DELETE(O_error_message,
                                  I_queue_rowid,
                                  I_queue_rec.seq_no) = FALSE then
            return FALSE;
          end if;
          ---
          LP_min_seq_no := I_queue_rec.seq_no;
          return TRUE;
        end if;
      else
        --- parent item or component item found further down queue
        --- Can't create a message yet
        --- skip current item so that parent or component can
        --- get published before the current item,
        --- on the next loop iteration
        LP_min_seq_no := I_queue_rec.seq_no;
        return TRUE;
      end if;
    else
      L_message_type := I_queue_rec.message_type;
    end if;
  
    if L_message_type in (ITEM_ADD,
                          ITEM_UPD,
                          ISUP_ADD,
                          ISUP_UPD,
                          ISC_ADD,
                          ISC_UPD,
                          ISMC_ADD,
                          ISMC_UPD,
                          ISCD_ADD,
                          ISCD_UPD,
                          UPC_ADD,
                          UPC_UPD,
                          BOM_ADD,
                          BOM_UPD,
                          UDAF_ADD,
                          UDAD_ADD,
                          UDAL_ADD,
                          IMG_ADD,
                          IMG_UPD,
                          TCKT_ADD,
                          RIH_ADD,
                          RIH_UPD,
                          RID_ADD,
                          RID_UPD) then
      if XXADEO_RMSMFM_ITEMS_BUILD.BUILD_MESSAGE(O_error_message,
                                                 L_message,
                                                 LP_rowids_rec,
                                                 L_record_exists,
                                                 L_message_type,
                                                 O_tran_level_ind,
                                                 I_queue_rec) = FALSE then
        return FALSE;
      end if;
    
      ----
      -- L_record exists is FALSE if a CRE or UPD message is on the queue,
      -- but the record has actually been deleted (i.e. there is a DEL message
      -- further down the queue)
      -- In this case, we want to keep the CRE on the queue, so that it remains
      -- as a marker to tell GETNXT not to publish the DEL message once it is
      -- reached on the queue.
      -- However, the UPD message is not needed on the queue, and is deleted.
      -- The ITEM_ADD message is not needed as a marker, since the published indicator
      -- on the ITEM_PUB_INFO table indicates when to publish the ITEM_DEL message.
      ----
      if L_record_exists = FALSE then
        if L_message_type in (ITEM_ADD,
                              ITEM_UPD,
                              ISUP_UPD,
                              ISC_UPD,
                              ISMC_UPD,
                              ISCD_UPD,
                              UPC_UPD,
                              BOM_UPD,
                              IMG_UPD,
                              RIH_UPD,
                              RID_UPD) then
          ----
          -- The UPD message is not needed on the queue, and is deleted.
          ----
          if ADD_ROWID_FOR_DELETE(O_error_message,
                                  I_queue_rowid,
                                  I_queue_rec.seq_no) = FALSE then
            return FALSE;
          end if;
        else
          ----
          -- The CRE message is needed on the queue, but its pub_status is updated
          -- so that it will be skipped.
          ----
          if ADD_ROWID_FOR_UPDATE(O_error_message,
                                  I_queue_rowid,
                                  I_queue_rec.seq_no) = FALSE then
            return FALSE;
          end if;
        end if;
        ---
        LP_min_seq_no := I_queue_rec.seq_no;
        return TRUE;
      else
        O_message := L_message;
        ---
        if ADD_ROWID_FOR_DELETE(O_error_message,
                                I_queue_rowid,
                                I_queue_rec.seq_no) = FALSE then
          return FALSE;
        end if;
      end if;
    else
      if RMSMFM_ITEMS_BUILD.BUILD_DELETE_MESSAGE(O_error_message,
                                                 O_message,
                                                 L_create_rowid,
                                                 L_create_seq_no,
                                                 I_queue_rec) = FALSE then
        return FALSE;
      end if;
    
      if ADD_ROWID_FOR_DELETE(O_error_message,
                              I_queue_rowid,
                              I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
      ----
      -- If there is a corresponding CRE message for the current DEL
      -- message, neither CRE nor DEL message will be published.
      -- Instead, both messages will be removed from the queue.
      ----
      if L_create_rowid is not NULL then
        if ADD_ROWID_FOR_DELETE(O_error_message,
                                L_create_rowid,
                                L_create_seq_no) = FALSE then
          return FALSE;
        end if;
        ---
        LP_min_seq_no := I_queue_rec.seq_no;
        O_message     := NULL;
        return TRUE;
      end if;
    end if;
  
    -----
    --- perform DML cleanup on the queue if a message has been created
    -----
    if O_message is not NULL then
      if UPDATE_QUEUE_TABLE(O_error_message) = FALSE then
        return FALSE;
      end if;
    
      if L_message_type = ITEM_ADD then
        update item_pub_info
           set published = 'Y'
         where rowid = L_pub_info_rowid
           and item = I_queue_rec.item;
      elsif L_message_type = ITEM_DEL then
        delete from item_pub_info
         where rowid = L_pub_info_rowid
           and item = I_queue_rec.item;
      end if;
    
    end if;
  
    O_message_type := L_message_type;
    O_break_loop   := TRUE;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;
    
  END PROCESS_QUEUE_RECORD;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: FIND_PARENT_OR_COMP
  -- Description: This function is responsible to get parent of component 
  --              and verify if item was published before
  -------------------------------------------------------------------------------------------------------
  FUNCTION FIND_PARENT_OR_COMP(O_error_message     OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_parent_comp_found OUT BOOLEAN,
                               I_queue_rec         IN ITEM_MFQUEUE%ROWTYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS.FIND_PARENT_OR_COMP';
  
    L_item_upd_type   VARCHAR2(20) := XXADEO_RMSMFM_ITEMS.ITEM_UPD;
    L_dummy           VARCHAR2(1) := NULL;
    L_item_parent     ITEM_MASTER.ITEM_PARENT%TYPE := NULL;
    L_comp_item       PACKITEM.ITEM%TYPE := NULL;
    L_simple_pack_ind ITEM_MASTER.SIMPLE_PACK_IND%TYPE := NULL;
  
    L_approve_found BOOLEAN;
  
    cursor C_GET_PARENT_COMP is
      select pi.item, im.item_parent, im.simple_pack_ind
        from packitem pi, item_master im
       where im.item = I_queue_rec.item
         and pi.pack_no(+) = im.item;
  
    cursor C_APPROVE_MSG is
      select 'x'
        from item_mfqueue q
       where q.message_type = L_item_upd_type
         and q.approve_ind = 'Y'
         and ((q.item = L_item_parent) or (q.item = L_comp_item))
         and seq_no > I_queue_rec.seq_no;
  
  BEGIN
    open C_GET_PARENT_COMP;
    fetch C_GET_PARENT_COMP
      into L_comp_item, L_item_parent, L_simple_pack_ind;
    close C_GET_PARENT_COMP;
  
    if L_simple_pack_ind = 'N' then
      L_comp_item := NULL;
    end if;
  
    open C_APPROVE_MSG;
    fetch C_APPROVE_MSG
      into L_dummy;
  
    O_parent_comp_found := C_APPROVE_MSG%FOUND;
  
    close C_APPROVE_MSG;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;
  END FIND_PARENT_OR_COMP;
  
  
   -------------------------------------------------------------------------------------------------------
  -- Name: CLEAN_QUEUE
  -- Description: This function is responsible to clean messages in ITEMLOC_MFQUEUE
  -------------------------------------------------------------------------------------------------------
  FUNCTION CLEAN_QUEUE(O_error_message  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_queue_rec      IN ITEM_MFQUEUE%ROWTYPE,
                       I_pub_info_rowid IN ROWID) RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXDEO_RMSMFM_ITEMS.CLEAN_QUEUE';
  
    L_itemloc_rowid_TBL ROWID_TBL;
    L_itemloc_seq_TBL   SEQ_TBL;
  
    cursor C_ITEMLOC_DEL is
      select rowid, seq_no
        from itemloc_mfqueue
       where item = I_queue_rec.item;
  
  BEGIN
    --
    if I_queue_rec.message_type = XXADEO_RMSMFM_ITEMS.ITEM_DEL then
    
      open C_ITEMLOC_DEL;
      fetch C_ITEMLOC_DEL BULK COLLECT
        into L_itemloc_rowid_TBL, L_itemloc_seq_TBL;
      close C_ITEMLOC_DEL;
    
      ---- delete table = existing delete rowid table UNION new delete rowid table
      if NESTED_TABLE_UNION(O_error_message,
                            LP_rowids_rec.itemloc_rowid_tbl,
                            LP_rowids_rec.itemloc_seq_tbl,
                            L_itemloc_rowid_TBL,
                            L_itemloc_seq_TBL) = FALSE then
        return FALSE;
      end if;
    
      --- add pub_info record to array of rowids to deleted
      LP_rowids_rec.pub_info_rowid_tbl.EXTEND;
      LP_rowids_rec.pub_info_rowid_tbl(LP_rowids_rec.pub_info_rowid_tbl.COUNT) := I_pub_info_rowid;
      LP_rowids_rec.pub_info_item_tbl.EXTEND;
      LP_rowids_rec.pub_info_item_tbl(LP_rowids_rec.pub_info_item_tbl.COUNT) := I_queue_rec.item;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;
  END CLEAN_QUEUE;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: NESTED_TABLE_UNION
  -- Description: This function is responsible to union rowid_tbl and seq_no_tbl
  -------------------------------------------------------------------------------------------------------
  FUNCTION NESTED_TABLE_UNION(O_error_message   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_union_rowid_tbl IN OUT NOCOPY ROWID_TBL,
                              O_union_seq_tbl   IN OUT NOCOPY SEQ_TBL,
                              I_rowid_tbl       IN ROWID_TBL,
                              I_seq_tbl         IN SEQ_TBL) RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS.NESTED_TABLE_UNION';
  
  BEGIN
  
    for i in 1 .. I_rowid_tbl.COUNT LOOP
      O_union_rowid_tbl.EXTEND;
      O_union_rowid_tbl(O_union_rowid_tbl.COUNT) := I_rowid_tbl(i);
      O_union_seq_tbl.EXTEND;
      O_union_seq_tbl(O_union_seq_tbl.COUNT) := I_seq_tbl(i);
    END LOOP;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;
  END NESTED_TABLE_UNION;
  
  
  -------------------------------------------------------------------------------------------------------
  -- Name: ADD_ROWID_FOR_DELETE
  -- Description: This function is responsible to extend two vectors and add new element to delete
  -------------------------------------------------------------------------------------------------------
  FUNCTION ADD_ROWID_FOR_DELETE(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rowid         IN ROWID,
                                I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS.ADD_ROWID_FOR_DELETE';
  
  BEGIN
  
    LP_rowids_rec.queue_rowid_tbl.EXTEND;
    LP_rowids_rec.queue_rowid_tbl(LP_rowids_rec.queue_rowid_tbl.COUNT) := I_rowid;
    LP_rowids_rec.queue_seq_tbl.EXTEND;
    LP_rowids_rec.queue_seq_tbl(LP_rowids_rec.queue_seq_tbl.COUNT) := I_seq_no;
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;
  END ADD_ROWID_FOR_DELETE;
  
  
  -------------------------------------------------------------------------------------------------------
  -- Name: ADD_ROWID_FOR_UPDATE
  -- Description: This function is responsible to extend two vectors and add new element to update
  -------------------------------------------------------------------------------------------------------
  FUNCTION ADD_ROWID_FOR_UPDATE(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rowid         IN ROWID,
                                I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS.ADD_ROWID_FOR_UPDATE';
  
  BEGIN
  
    LP_rowids_rec.queue_upd_rowid_tbl.EXTEND;
    LP_rowids_rec.queue_upd_rowid_tbl(LP_rowids_rec.queue_upd_rowid_tbl.COUNT) := I_rowid;
    LP_rowids_rec.queue_upd_seq_tbl.EXTEND;
    LP_rowids_rec.queue_upd_seq_tbl(LP_rowids_rec.queue_upd_seq_tbl.COUNT) := I_seq_no;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;
  END ADD_ROWID_FOR_UPDATE;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: UPDATE_QUEUE_TABLE
  -- Description:  This responsible to perform DML using the global record that 
  --               keeps track of QUEUE records to update/delete.
  -------------------------------------------------------------------------------------------------------
  FUNCTION UPDATE_QUEUE_TABLE(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS.UPDATE_QUEUE_TABLE';
  
    BULK_ERRORS EXCEPTION;
    PRAGMA exception_init(BULK_ERRORS, -24381);
  
  BEGIN
  
    LP_error_status := API_CODES.UNHANDLED_ERROR;
  
    if LP_rowids_rec.queue_upd_rowid_tbl is not NULL and
       LP_rowids_rec.queue_upd_rowid_tbl.COUNT > 0 then
    
      forall i in 1 .. LP_rowids_rec.queue_upd_rowid_tbl.COUNT SAVE
                       EXCEPTIONS
        update item_mfqueue
           set pub_status = 'N'
         where rowid = LP_rowids_rec.queue_upd_rowid_tbl(i)
           and seq_no = LP_rowids_rec.queue_upd_seq_tbl(i);
      ---
      LP_rowids_rec.queue_upd_rowid_tbl.DELETE;
      LP_rowids_rec.queue_upd_seq_tbl.DELETE;
    end if;
  
    if LP_rowids_rec.queue_rowid_tbl is not NULL and
       LP_rowids_rec.queue_rowid_tbl.COUNT > 0 then
    
      forall i in 1 .. LP_rowids_rec.queue_rowid_tbl.COUNT SAVE EXCEPTIONS
        delete item_mfqueue
         where rowid = LP_rowids_rec.queue_rowid_tbl(i)
           and seq_no = LP_rowids_rec.queue_seq_tbl(i);
      ---
      LP_rowids_rec.queue_rowid_tbl.DELETE;
      LP_rowids_rec.queue_seq_tbl.DELETE;
    end if;
  
    if LP_rowids_rec.itemloc_rowid_tbl is not NULL and
       LP_rowids_rec.itemloc_rowid_tbl.COUNT > 0 then
    
      forall i in 1 .. LP_rowids_rec.itemloc_rowid_tbl.COUNT SAVE
                       EXCEPTIONS
        delete itemloc_mfqueue
         where rowid = LP_rowids_rec.itemloc_rowid_tbl(i)
           and seq_no = LP_rowids_rec.itemloc_seq_tbl(i);
      ---
      LP_rowids_rec.itemloc_rowid_tbl.DELETE;
      LP_rowids_rec.itemloc_seq_tbl.DELETE;
    end if;
  
    if LP_rowids_rec.pub_info_rowid_tbl is not NULL and
       LP_rowids_rec.pub_info_rowid_tbl.COUNT > 0 then
    
      forall i in 1 .. LP_rowids_rec.pub_info_rowid_tbl.COUNT SAVE
                       EXCEPTIONS
        delete item_pub_info
         where rowid = LP_rowids_rec.pub_info_rowid_tbl(i)
           and item = LP_rowids_rec.pub_info_item_tbl(i);
      ---
      LP_rowids_rec.pub_info_rowid_tbl.DELETE;
      LP_rowids_rec.pub_info_item_tbl.DELETE;
    end if;
  
    LP_error_status := API_CODES.HOSPITAL;
  
    return TRUE;
  
  EXCEPTION
    when BULK_ERRORS then
      -- if there was an error in the bulk update it would have occured because the record
      -- to update was already deleted, therefore we can return true
      NULL;
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;
  END UPDATE_QUEUE_TABLE;
  
  
  -------------------------------------------------------------------------------------------------------
  -- Name: HANDLE_ERRORS
  -- Description: This procedure is responsible to register any errors if occurred
  --              and "send" this message to RIB_HOSPITAL
  -------------------------------------------------------------------------------------------------------
  PROCEDURE HANDLE_ERRORS(O_status_code   IN OUT VARCHAR2,
                          O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_message       IN OUT NOCOPY RIB_OBJECT,
                          O_bus_obj_id    IN OUT NOCOPY RIB_BUSOBJID_TBL,
                          O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                          I_item          IN ITEMLOC_MFQUEUE.ITEM%TYPE,
                          I_seq_no        IN ITEMLOC_MFQUEUE.SEQ_NO%TYPE) IS
  
    L_module     VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS.HANDLE_ERRORS';
    L_error_type VARCHAR2(5) := NULL;
  
  BEGIN
  
    O_status_code := LP_error_status;
  
    if LP_error_status = API_CODES.HOSPITAL then
    
      O_bus_obj_id   := RIB_BUSOBJID_TBL(I_item);
      O_routing_info := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no',
                                                                I_seq_no,
                                                                null,
                                                                null,
                                                                null,
                                                                null));
      ---
      O_message := "RIB_ItemRef_REC"(0, I_item);
    
      update item_mfqueue
         set pub_status = LP_error_status
       where seq_no = I_seq_no;
    
    end if;
  
    /* Pass out parsed error message */
    SQL_LIB.API_MSG(L_error_type, O_error_message);
  
  EXCEPTION
    when OTHERS then
      O_status_code   := API_CODES.UNHANDLED_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
  END HANDLE_ERRORS;
  ------------------------------------------------------------------------------------------
END XXADEO_RMSMFM_ITEMS;
/