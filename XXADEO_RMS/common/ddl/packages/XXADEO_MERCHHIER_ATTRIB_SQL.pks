CREATE OR REPLACE PACKAGE XXADEO_MERCHHIER_ATTRIB_SQL AUTHID CURRENT_USER AS    
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/*               Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package "XXADEO_MERCHHIER_ATTRIB_SQL"                        */
/******************************************************************************/
--------------------------------------------------------------------------------
  template_key                     CONSTANT VARCHAR2(255):='XXADEO_MERCHANDISE_HIERARCHY_CFA_ATTRIB';
  action_new                       VARCHAR2(25)          :='NEW';
  action_mod                       VARCHAR2(25)          :='MOD';
  action_del                       VARCHAR2(25)          :='DEL';

  SHEET_NAME_TRANS                 S9T_PKG.TRANS_MAP_TYP;

  GP_upload_create                 VARCHAR2(6)           := 'NEW';
  GP_upload_update                 VARCHAR2(6)           := 'MOD';

  XXADEO_ATTR_STATUS_CODE          CONSTANT VARCHAR2(6)  := 'MHST';
  XXADEO_ACTIVE                    VARCHAR2(10)          := 'ACTIVE';
  XXADEO_INACTIVE                  VARCHAR2(10)          := 'INACTIVE';
  XXADEO_ATTR_ACTIVE               VARCHAR2(10);
  XXADEO_ATTR_INACTIVE             VARCHAR2(10);
  XXADEO_ATTR_IN_PREPARATION       VARCHAR2(10);

  TABLE$DEPS_CFA_EXT               VARCHAR2(255)         := 'DEPS_CFA_EXT';
  TABLE$CLASS_CFA_EXT              VARCHAR2(255)         := 'CLASS_CFA_EXT';
  TABLE$SUBCLASS_CFA_EXT           VARCHAR2(255)         := 'SUBCLASS_CFA_EXT';

  DEPARTMENT_STATUS_SHEET          VARCHAR2(255)         := 'DEPARTMENT_STATUS';
  DEPARTMENT_STATUS$ACTION         VARCHAR2(255)         := 'ACTION';
  DEPARTMENT_STATUS$GROUP_ID       VARCHAR2(255)         := 'GROUP_ID';
  DEPARTMENT_STATUS$GROUP_NAME     VARCHAR2(255)         := 'GROUP_NAME';
  DEPARTMENT_STATUS$DEPT           VARCHAR2(255)         := 'DEPT';
  DEPARTMENT_STATUS$DEPT_NAME      VARCHAR2(255)         := 'DEPT_NAME';
  DEPARTMENT_STATUS$DEPT_STATUS    VARCHAR2(255)         := 'DEPT_STATUS';

  CLASS_STATUS_SHEET               VARCHAR2(255)         := 'CLASS_STATUS';
  CLASS_STATUS$ACTION              VARCHAR2(255)         := 'ACTION';
  CLASS_STATUS$GROUP_ID            VARCHAR2(255)         := 'GROUP_ID';
  CLASS_STATUS$GROUP_NAME          VARCHAR2(255)         := 'GROUP_NAME';
  CLASS_STATUS$DEPT                VARCHAR2(255)         := 'DEPT';
  CLASS_STATUS$DEPT_NAME           VARCHAR2(255)         := 'DEPT_NAME';
  CLASS_STATUS$CLASS               VARCHAR2(255)         := 'CLASS';
  CLASS_STATUS$CLASS_NAME          VARCHAR2(255)         := 'CLASS_NAME';
  CLASS_STATUS$CLASS_STATUS        VARCHAR2(255)         := 'CLASS_STATUS';

  SCLASS_STATUS_SHEET              VARCHAR2(255)         := 'SUBCLASS_STATUS';
  SCLASS_STATUS$ACTION             VARCHAR2(255)         := 'ACTION';
  SCLASS_STATUS$GROUP_ID           VARCHAR2(255)         := 'GROUP_ID';
  SCLASS_STATUS$GROUP_NAME         VARCHAR2(255)         := 'GROUP_NAME';
  SCLASS_STATUS$DEPT               VARCHAR2(255)         := 'DEPT';
  SCLASS_STATUS$DEPT_NAME          VARCHAR2(255)         := 'DEPT_NAME';
  SCLASS_STATUS$CLASS              VARCHAR2(255)         := 'CLASS';
  SCLASS_STATUS$CLASS_NAME         VARCHAR2(255)         := 'CLASS_NAME';
  SCLASS_STATUS$SUBCLASS           VARCHAR2(255)         := 'SUBCLASS';
  SCLASS_STATUS$SUBCLASS_NAME      VARCHAR2(255)         := 'SUBCLASS_NAME';
  SCLASS_STATUS$SUBCLASS_STATUS    VARCHAR2(255)         := 'SUBCLASS_STATUS';

  action_column                    VARCHAR2(255)         :='ACTION';
  template_category                CODE_DETAIL.CODE%TYPE :='RMSADM';
-------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind IN       CHAR DEFAULT 'N')
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
END XXADEO_MERCHHIER_ATTRIB_SQL;
/
