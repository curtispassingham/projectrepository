create or replace package XXADEO_ITEMLOC_IN is

/*---------------------------------------------------------------------*/
  /*
  * Object Name:   XXADEO_ITEMLOC_IN.pks
  * Description:   This package it's part of IR0512 
  *                ItemLoc Inbound
  * Version:       1.0
  * Author:        Liliana Soraia Ferreira
  * Creation Date: 13/12/2018
  * Last Modified: 22/01/2019
  * History: 
  *               1.0 - Initial version
  *               1.1 - Fixed stg table column name
  *--------------------------------------------------------------------*/

  PROCEDURE ENRICH_VALIDATE(O_error_message OUT VARCHAR2,
                            O_status_code   OUT VARCHAR2);

  PROCEDURE PROCESS(I_DEPT          NUMBER,
                    O_error_message OUT VARCHAR2,
                    O_status_code   OUT VARCHAR2);

  PROCEDURE PURGE(O_error_message OUT VARCHAR2, O_status_code OUT VARCHAR2);

end XXADEO_ITEMLOC_IN;
/