CREATE OR REPLACE PACKAGE Body item_induct_sql AS

Type s9t_errors_tab_typ IS TABLE OF s9t_errors%rowtype;
Lp_s9t_errors_tab s9t_errors_tab_typ;
-------------------------------------------------------------------------------------------------------
FUNCTION get_sheet_name_trans(
    I_sheet_name IN VARCHAR)
  RETURN VARCHAR2
IS
BEGIN
  IF sheet_name_trans.exists(I_sheet_name) THEN
    RETURN sheet_name_trans(I_sheet_name);
  ELSE
    RETURN NULL;
  END IF;
END get_sheet_name_trans;
-------------------------------------------------------------------------------------------------------
PROCEDURE write_s9t_error(I_file_id IN s9t_errors.file_id%type,
              I_sheet   IN VARCHAR2,
              I_row_seq IN NUMBER,
              I_col     IN VARCHAR2,
              I_sqlcode IN NUMBER,
              I_sqlerrm IN VARCHAR2)
IS
BEGIN
   Lp_s9t_errors_tab.extend();
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).FILE_ID      := I_file_id;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_SEQ_NO := s9t_errors_seq.nextval;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).TEMPLATE_KEY := template_key;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).WKSHT_KEY    := I_sheet;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).COLUMN_KEY   := I_col;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ROW_SEQ      := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY    :=
   (
    CASE
       WHEN I_sqlcode IS NULL THEN
      I_sqlerrm
       ELSE
      'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_ID        := get_user;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_DATETIME      := sysdate;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_ID       := get_user;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_DATETIME := sysdate;
END write_s9t_error;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_names(I_file_id NUMBER)
IS
   l_sheets s9t_pkg.names_map_typ;
   ICD_cols s9t_pkg.names_map_typ;
   ICH_cols s9t_pkg.names_map_typ;
   IC_cols s9t_pkg.names_map_typ;
   IC_L10_cols s9t_pkg.names_map_typ;
   IM_cols s9t_pkg.names_map_typ;
   IMTL_cols s9t_pkg.names_map_typ;
   IM_CFA_cols s9t_pkg.names_map_typ;
   IS_cols s9t_pkg.names_map_typ;
   ISTL_cols s9t_pkg.names_map_typ;
   IS_CFA_cols s9t_pkg.names_map_typ;
   ISC_cols s9t_pkg.names_map_typ;
   ISCL_cols s9t_pkg.names_map_typ;
   ISC_CFA_cols s9t_pkg.names_map_typ;
   ISCD_cols s9t_pkg.names_map_typ;
   ISMC_cols s9t_pkg.names_map_typ;
   ISU_cols s9t_pkg.names_map_typ;
   IXD_cols s9t_pkg.names_map_typ;
   IXH_cols s9t_pkg.names_map_typ;
   IXHTL_cols s9t_pkg.names_map_typ;
   PI_cols s9t_pkg.names_map_typ;
   IZP_cols s9t_pkg.names_map_typ;
   UID_cols s9t_pkg.names_map_typ;
   UIF_cols s9t_pkg.names_map_typ;
   UIL_cols s9t_pkg.names_map_typ;
   VI_cols s9t_pkg.names_map_typ;
   CSH_cols s9t_pkg.names_map_typ;
   CSH_CFA_cols s9t_pkg.names_map_typ;
   CSDL_cols s9t_pkg.names_map_typ;
   CSD_cols s9t_pkg.names_map_typ;
   IIM_cols s9t_pkg.names_map_typ;
   IIMTL_cols s9t_pkg.names_map_typ;
   ISE_cols s9t_pkg.names_map_typ;
-------------------------------------------------------------------------------------------------------
FUNCTION get_col_indx(I_names   IN s9t_pkg.names_map_typ,
              I_col_key IN VARCHAR2)
RETURN NUMBER IS

BEGIN
   IF I_names.exists(I_col_key) THEN
     RETURN I_names(I_col_key);
   ELSE
     RETURN NULL;
   END IF;
END get_col_indx;

BEGIN
  l_sheets           :=s9t_pkg.get_sheet_names(I_file_id);
  ICD_cols           :=s9t_pkg.get_col_names(I_file_id,ICD_sheet);
  ICD$Action             :=get_col_indx(ICD_cols,action_column);
  ICD$ITEM           :=get_col_indx(ICD_cols,'ITEM');
  ICD$SUPPLIER           :=get_col_indx(ICD_cols,'SUPPLIER');
  ICD$ORIGIN_COUNTRY_ID      :=get_col_indx(ICD_cols,'ORIGIN_COUNTRY_ID');
  ICD$DELIVERY_COUNTRY_ID    :=get_col_indx(ICD_cols,'DELIVERY_COUNTRY_ID');
  ICD$COND_TYPE          :=get_col_indx(ICD_cols,'COND_TYPE');
  ICD$COND_VALUE         :=get_col_indx(ICD_cols,'COND_VALUE');
  ICD$APPLIED_ON         :=get_col_indx(ICD_cols,'APPLIED_ON');
  ICD$COMP_RATE          :=get_col_indx(ICD_cols,'COMP_RATE');
  ICD$CALCULATION_BASIS      :=get_col_indx(ICD_cols,'CALCULATION_BASIS');
  ICD$RECOVERABLE_AMOUNT     :=get_col_indx(ICD_cols,'RECOVERABLE_AMOUNT');
  ICD$MODIFIED_TAXABLE_BASE  :=get_col_indx(ICD_cols,'MODIFIED_TAXABLE_BASE');
  ICH_cols           :=s9t_pkg.get_col_names(I_file_id,ICH_sheet);
  ICH$Action             :=get_col_indx(ICH_cols,action_column);
  ICH$ITEM           :=get_col_indx(ICH_cols,'ITEM');
  ICH$SUPPLIER           :=get_col_indx(ICH_cols,'SUPPLIER');
  ICH$ORIGIN_COUNTRY_ID      :=get_col_indx(ICH_cols,'ORIGIN_COUNTRY_ID');
  ICH$DELIVERY_COUNTRY_ID    :=get_col_indx(ICH_cols,'DELIVERY_COUNTRY_ID');
  ICH$PRIM_DLVY_CTRY_IND     :=get_col_indx(ICH_cols,'PRIM_DLVY_CTRY_IND');
  ICH$NIC_STATIC_IND         :=get_col_indx(ICH_cols,'NIC_STATIC_IND');
  ICH$BASE_COST          :=get_col_indx(ICH_cols,'BASE_COST');
  ICH$NEGOTIATED_ITEM_COST   :=get_col_indx(ICH_cols,'NEGOTIATED_ITEM_COST');
  ICH$EXTENDED_BASE_COST     :=get_col_indx(ICH_cols,'EXTENDED_BASE_COST');
  ICH$INCLUSIVE_COST         :=get_col_indx(ICH_cols,'INCLUSIVE_COST');
  IC_cols            :=s9t_pkg.get_col_names(I_file_id,IC_sheet);
  IC$Action          :=get_col_indx(IC_cols,action_column);
  IC$ITEM            :=get_col_indx(IC_cols,'ITEM');
  IC$COUNTRY_ID          :=get_col_indx(IC_cols,'COUNTRY_ID');
  IC_L10_cols            :=s9t_pkg.get_col_names(I_file_id,IC_L10_sheet);
  IC_L10$Action          :=get_col_indx(IC_L10_cols,action_column);
  IC_L10$ITEM            :=get_col_indx(IC_L10_cols,'ITEM');
  IC_L10$COUNTRY_ID      :=get_col_indx(IC_L10_cols,'COUNTRY_ID');
  IC_L10$L10N_COUNTRY_ID     :=get_col_indx(IC_L10_cols,'L10N_COUNTRY_ID');
  IC_L10$GROUP_ID        :=get_col_indx(IC_L10_cols,'GROUP_ID');
  IC_L10$VARCHAR2_1      :=get_col_indx(IC_L10_cols,'VARCHAR2_1');
  IC_L10$VARCHAR2_2      :=get_col_indx(IC_L10_cols,'VARCHAR2_2');
  IC_L10$VARCHAR2_3      :=get_col_indx(IC_L10_cols,'VARCHAR2_3');
  IC_L10$VARCHAR2_4      :=get_col_indx(IC_L10_cols,'VARCHAR2_4');
  IC_L10$VARCHAR2_5      :=get_col_indx(IC_L10_cols,'VARCHAR2_5');
  IC_L10$VARCHAR2_6      :=get_col_indx(IC_L10_cols,'VARCHAR2_6');
  IC_L10$VARCHAR2_7      :=get_col_indx(IC_L10_cols,'VARCHAR2_7');
  IC_L10$VARCHAR2_8      :=get_col_indx(IC_L10_cols,'VARCHAR2_8');
  IC_L10$VARCHAR2_9      :=get_col_indx(IC_L10_cols,'VARCHAR2_9');
  IC_L10$VARCHAR2_10         :=get_col_indx(IC_L10_cols,'VARCHAR2_10');
  IC_L10$NUMBER_11       :=get_col_indx(IC_L10_cols,'NUMBER_11');
  IC_L10$NUMBER_12       :=get_col_indx(IC_L10_cols,'NUMBER_12');
  IC_L10$NUMBER_13       :=get_col_indx(IC_L10_cols,'NUMBER_13');
  IC_L10$NUMBER_14       :=get_col_indx(IC_L10_cols,'NUMBER_14');
  IC_L10$NUMBER_15       :=get_col_indx(IC_L10_cols,'NUMBER_15');
  IC_L10$NUMBER_16       :=get_col_indx(IC_L10_cols,'NUMBER_16');
  IC_L10$NUMBER_17       :=get_col_indx(IC_L10_cols,'NUMBER_17');
  IC_L10$NUMBER_18       :=get_col_indx(IC_L10_cols,'NUMBER_18');
  IC_L10$NUMBER_19       :=get_col_indx(IC_L10_cols,'NUMBER_19');
  IC_L10$NUMBER_20       :=get_col_indx(IC_L10_cols,'NUMBER_20');
  IC_L10$DATE_21         :=get_col_indx(IC_L10_cols,'DATE_21');
  IC_L10$DATE_22         :=get_col_indx(IC_L10_cols,'DATE_22');
  IM_cols            :=s9t_pkg.get_col_names(I_file_id,IM_sheet);
  IM$Action          :=get_col_indx(IM_cols,action_column);
  IM$ITEM            :=get_col_indx(IM_cols,'ITEM');
  IM$ITEM_NUMBER_TYPE        :=get_col_indx(IM_cols,'ITEM_NUMBER_TYPE');
  IM$FORMAT_ID           :=get_col_indx(IM_cols,'FORMAT_ID');
  IM$PREFIX          :=get_col_indx(IM_cols,'PREFIX');
  IM$ITEM_PARENT         :=get_col_indx(IM_cols,'ITEM_PARENT');
  IM$ITEM_GRANDPARENT        :=get_col_indx(IM_cols,'ITEM_GRANDPARENT');
  IM$PACK_IND            :=get_col_indx(IM_cols,'PACK_IND');
  IM$ITEM_LEVEL          :=get_col_indx(IM_cols,'ITEM_LEVEL');
  IM$TRAN_LEVEL          :=get_col_indx(IM_cols,'TRAN_LEVEL');
  IM$ITEM_AGGREGATE_IND      :=get_col_indx(IM_cols,'ITEM_AGGREGATE_IND');
  IM$DIFF_1          :=get_col_indx(IM_cols,'DIFF_1');
  IM$DIFF_1_AGGREGATE_IND    :=get_col_indx(IM_cols,'DIFF_1_AGGREGATE_IND');
  IM$DIFF_2          :=get_col_indx(IM_cols,'DIFF_2');
  IM$DIFF_2_AGGREGATE_IND    :=get_col_indx(IM_cols,'DIFF_2_AGGREGATE_IND');
  IM$DIFF_3          :=get_col_indx(IM_cols,'DIFF_3');
  IM$DIFF_3_AGGREGATE_IND    :=get_col_indx(IM_cols,'DIFF_3_AGGREGATE_IND');
  IM$DIFF_4          :=get_col_indx(IM_cols,'DIFF_4');
  IM$DIFF_4_AGGREGATE_IND    :=get_col_indx(IM_cols,'DIFF_4_AGGREGATE_IND');
  IM$DEPT            :=get_col_indx(IM_cols,'DEPT');
  IM$CLASS           :=get_col_indx(IM_cols,'CLASS');
  IM$SUBCLASS            :=get_col_indx(IM_cols,'SUBCLASS');
  IM$STATUS          :=get_col_indx(IM_cols,'STATUS');
  IM$ITEM_DESC           :=get_col_indx(IM_cols,'ITEM_DESC');
  IM$ITEM_DESC_SECONDARY     :=get_col_indx(IM_cols,'ITEM_DESC_SECONDARY');
  IM$SHORT_DESC          :=get_col_indx(IM_cols,'SHORT_DESC');
  IM$PRIMARY_REF_ITEM_IND    :=get_col_indx(IM_cols,'PRIMARY_REF_ITEM_IND');
  IM$COST_ZONE_GROUP_ID      :=get_col_indx(IM_cols,'COST_ZONE_GROUP_ID');
  IM$STANDARD_UOM        :=get_col_indx(IM_cols,'STANDARD_UOM');
  IM$UOM_CONV_FACTOR         :=get_col_indx(IM_cols,'UOM_CONV_FACTOR');
  IM$PACKAGE_SIZE        :=get_col_indx(IM_cols,'PACKAGE_SIZE');
  IM$PACKAGE_UOM         :=get_col_indx(IM_cols,'PACKAGE_UOM');
  IM$MERCHANDISE_IND         :=get_col_indx(IM_cols,'MERCHANDISE_IND');
  IM$STORE_ORD_MULT      :=get_col_indx(IM_cols,'STORE_ORD_MULT');
  IM$FORECAST_IND        :=get_col_indx(IM_cols,'FORECAST_IND');
  IM$MFG_REC_RETAIL      :=get_col_indx(IM_cols,'MFG_REC_RETAIL');
  IM$RETAIL_LABEL_TYPE       :=get_col_indx(IM_cols,'RETAIL_LABEL_TYPE');
  IM$RETAIL_LABEL_VALUE      :=get_col_indx(IM_cols,'RETAIL_LABEL_VALUE');
  IM$HANDLING_TEMP       :=get_col_indx(IM_cols,'HANDLING_TEMP');
  IM$HANDLING_SENSITIVITY    :=get_col_indx(IM_cols,'HANDLING_SENSITIVITY');
  IM$CATCH_WEIGHT_IND        :=get_col_indx(IM_cols,'CATCH_WEIGHT_IND');
  IM$WASTE_TYPE          :=get_col_indx(IM_cols,'WASTE_TYPE');
  IM$WASTE_PCT           :=get_col_indx(IM_cols,'WASTE_PCT');
  IM$DEFAULT_WASTE_PCT       :=get_col_indx(IM_cols,'DEFAULT_WASTE_PCT');
  IM$CONST_DIMEN_IND         :=get_col_indx(IM_cols,'CONST_DIMEN_IND');
  IM$SIMPLE_PACK_IND         :=get_col_indx(IM_cols,'SIMPLE_PACK_IND');
  IM$CONTAINS_INNER_IND      :=get_col_indx(IM_cols,'CONTAINS_INNER_IND');
  IM$SELLABLE_IND        :=get_col_indx(IM_cols,'SELLABLE_IND');
  IM$ORDERABLE_IND       :=get_col_indx(IM_cols,'ORDERABLE_IND');
  IM$PACK_TYPE           :=get_col_indx(IM_cols,'PACK_TYPE');
  IM$ORDER_AS_TYPE       :=get_col_indx(IM_cols,'ORDER_AS_TYPE');
  IM$COMMENTS            :=get_col_indx(IM_cols,'COMMENTS');
  IM$ITEM_SERVICE_LEVEL      :=get_col_indx(IM_cols,'ITEM_SERVICE_LEVEL');
  IM$GIFT_WRAP_IND       :=get_col_indx(IM_cols,'GIFT_WRAP_IND');
  IM$SHIP_ALONE_IND      :=get_col_indx(IM_cols,'SHIP_ALONE_IND');
  IM$ITEM_XFORM_IND      :=get_col_indx(IM_cols,'ITEM_XFORM_IND');
  IM$INVENTORY_IND       :=get_col_indx(IM_cols,'INVENTORY_IND');
  IM$ORDER_TYPE          :=get_col_indx(IM_cols,'ORDER_TYPE');
  IM$SALE_TYPE           :=get_col_indx(IM_cols,'SALE_TYPE');
  IM$DEPOSIT_ITEM_TYPE       :=get_col_indx(IM_cols,'DEPOSIT_ITEM_TYPE');
  IM$CONTAINER_ITEM      :=get_col_indx(IM_cols,'CONTAINER_ITEM');
  IM$DEPOSIT_IN_PRICE_PER_UOM    :=get_col_indx(IM_cols,'DEPOSIT_IN_PRICE_PER_UOM');
  IM$AIP_CASE_TYPE       :=get_col_indx(IM_cols,'AIP_CASE_TYPE');
  IM$PERISHABLE_IND      :=get_col_indx(IM_cols,'PERISHABLE_IND');
  IM$NOTIONAL_PACK_IND       :=get_col_indx(IM_cols,'NOTIONAL_PACK_IND');
  IM$SOH_INQUIRY_AT_PACK_IND     :=get_col_indx(IM_cols,'SOH_INQUIRY_AT_PACK_IND');
  IM$PRODUCT_CLASSIFICATION  :=get_col_indx(IM_cols,'PRODUCT_CLASSIFICATION');
  IM$BRAND_NAME          :=get_col_indx(IM_cols,'BRAND_NAME');
  IM$PRE_RESERVED_IND        :=get_col_indx(IM_cols,'PRE_RESERVED_IND');
  IM$NEXT_UPD_ID         :=get_col_indx(IM_cols,'NEXT_UPD_ID');
  IM$ORIG_REF_NO         :=get_col_indx(IM_cols,'ORIG_REF_NO');
  IM_CFA_cols            :=s9t_pkg.get_col_names(I_file_id,IM_CFA_sheet);
  IM_CFA$Action          :=get_col_indx(IM_CFA_cols,action_column);
  IM_CFA$ITEM            :=get_col_indx(IM_CFA_cols,'ITEM');
  IM_CFA$GROUP_ID        :=get_col_indx(IM_CFA_cols,'GROUP_ID');
  IM_CFA$VARCHAR2_1      :=get_col_indx(IM_CFA_cols,'VARCHAR2_1');
  IM_CFA$VARCHAR2_2      :=get_col_indx(IM_CFA_cols,'VARCHAR2_2');
  IM_CFA$VARCHAR2_3      :=get_col_indx(IM_CFA_cols,'VARCHAR2_3');
  IM_CFA$VARCHAR2_4      :=get_col_indx(IM_CFA_cols,'VARCHAR2_4');
  IM_CFA$VARCHAR2_5      :=get_col_indx(IM_CFA_cols,'VARCHAR2_5');
  IM_CFA$VARCHAR2_6      :=get_col_indx(IM_CFA_cols,'VARCHAR2_6');
  IM_CFA$VARCHAR2_7      :=get_col_indx(IM_CFA_cols,'VARCHAR2_7');
  IM_CFA$VARCHAR2_8      :=get_col_indx(IM_CFA_cols,'VARCHAR2_8');
  IM_CFA$VARCHAR2_9      :=get_col_indx(IM_CFA_cols,'VARCHAR2_9');
  IM_CFA$VARCHAR2_10         :=get_col_indx(IM_CFA_cols,'VARCHAR2_10');
  IM_CFA$NUMBER_11       :=get_col_indx(IM_CFA_cols,'NUMBER_11');
  IM_CFA$NUMBER_12       :=get_col_indx(IM_CFA_cols,'NUMBER_12');
  IM_CFA$NUMBER_13       :=get_col_indx(IM_CFA_cols,'NUMBER_13');
  IM_CFA$NUMBER_14       :=get_col_indx(IM_CFA_cols,'NUMBER_14');
  IM_CFA$NUMBER_15       :=get_col_indx(IM_CFA_cols,'NUMBER_15');
  IM_CFA$NUMBER_16       :=get_col_indx(IM_CFA_cols,'NUMBER_16');
  IM_CFA$NUMBER_17       :=get_col_indx(IM_CFA_cols,'NUMBER_17');
  IM_CFA$NUMBER_18       :=get_col_indx(IM_CFA_cols,'NUMBER_18');
  IM_CFA$NUMBER_19       :=get_col_indx(IM_CFA_cols,'NUMBER_19');
  IM_CFA$NUMBER_20       :=get_col_indx(IM_CFA_cols,'NUMBER_20');
  IM_CFA$DATE_21         :=get_col_indx(IM_CFA_cols,'DATE_21');
  IM_CFA$DATE_22         :=get_col_indx(IM_CFA_cols,'DATE_22');
  IM_CFA$DATE_23         :=get_col_indx(IM_CFA_cols,'DATE_23');
  IM_CFA$DATE_24         :=get_col_indx(IM_CFA_cols,'DATE_24');
  IM_CFA$DATE_25         :=get_col_indx(IM_CFA_cols,'DATE_25');
  IMTL_cols                     := s9t_pkg.get_col_names(I_file_id,IMTL_sheet);
  IMTL$Action                   := get_col_indx(IMTL_cols,action_column);
  IMTL$LANG                     := get_col_indx(IMTL_cols,'LANG');
  IMTL$ITEM                     := get_col_indx(IMTL_cols,'ITEM');
  IMTL$ITEM_DESC                := get_col_indx(IMTL_cols,'ITEM_DESC');
  IMTL$ITEM_DESC_SECONDARY      := get_col_indx(IMTL_cols,'ITEM_DESC_SECONDARY');
  IMTL$SHORT_DESC               := get_col_indx(IMTL_cols,'SHORT_DESC');
  IS_cols            :=s9t_pkg.get_col_names(I_file_id,IS_sheet);
  IS$Action          :=get_col_indx(IS_cols,action_column);
  IS$ITEM            :=get_col_indx(IS_cols,'ITEM');
  IS$SUPPLIER            :=get_col_indx(IS_cols,'SUPPLIER');
  IS$PRIMARY_SUPP_IND        :=get_col_indx(IS_cols,'PRIMARY_SUPP_IND');
  IS$VPN             :=get_col_indx(IS_cols,'VPN');
  IS$SUPP_LABEL          :=get_col_indx(IS_cols,'SUPP_LABEL');
  IS$CONSIGNMENT_RATE        :=get_col_indx(IS_cols,'CONSIGNMENT_RATE');
  IS$SUPP_DIFF_1         :=get_col_indx(IS_cols,'SUPP_DIFF_1');
  IS$SUPP_DIFF_2         :=get_col_indx(IS_cols,'SUPP_DIFF_2');
  IS$SUPP_DIFF_3         :=get_col_indx(IS_cols,'SUPP_DIFF_3');
  IS$SUPP_DIFF_4         :=get_col_indx(IS_cols,'SUPP_DIFF_4');
  IS$PALLET_NAME         :=get_col_indx(IS_cols,'PALLET_NAME');
  IS$CASE_NAME           :=get_col_indx(IS_cols,'CASE_NAME');
  IS$INNER_NAME          :=get_col_indx(IS_cols,'INNER_NAME');
  IS$SUPP_DISCONTINUE_DATE   :=get_col_indx(IS_cols,'SUPP_DISCONTINUE_DATE');
  IS$DIRECT_SHIP_IND         :=get_col_indx(IS_cols,'DIRECT_SHIP_IND');
  IS$CONCESSION_RATE         :=get_col_indx(IS_cols,'CONCESSION_RATE');
  IS$PRIMARY_CASE_SIZE       :=get_col_indx(IS_cols,'PRIMARY_CASE_SIZE');
  ISTL_cols                     := s9t_pkg.get_col_names(I_file_id,ISTL_sheet);
  ISTL$Action                   := get_col_indx(ISTL_cols,action_column);
  ISTL$LANG                     := get_col_indx(ISTL_cols,'LANG');
  ISTL$ITEM                     := get_col_indx(ISTL_cols,'ITEM');
  ISTL$SUPPLIER                 := get_col_indx(ISTL_cols,'SUPPLIER');
  ISTL$SUPP_LABEL               := get_col_indx(ISTL_cols,'SUPP_LABEL');
  ISTL$SUPP_DIFF_1              := get_col_indx(ISTL_cols,'SUPP_DIFF_1');
  ISTL$SUPP_DIFF_2              := get_col_indx(ISTL_cols,'SUPP_DIFF_2');
  ISTL$SUPP_DIFF_3              := get_col_indx(ISTL_cols,'SUPP_DIFF_3');
  ISTL$SUPP_DIFF_4              := get_col_indx(ISTL_cols,'SUPP_DIFF_4');
  IS_CFA_cols            :=s9t_pkg.get_col_names(I_file_id,IS_CFA_sheet);
  IS_CFA$Action          :=get_col_indx(IS_CFA_cols,action_column);
  IS_CFA$ITEM            :=get_col_indx(IS_CFA_cols,'ITEM');
  IS_CFA$SUPPLIER        :=get_col_indx(IS_CFA_cols,'SUPPLIER');
  IS_CFA$GROUP_ID        :=get_col_indx(IS_CFA_cols,'GROUP_ID');
  IS_CFA$VARCHAR2_1      :=get_col_indx(IS_CFA_cols,'VARCHAR2_1');
  IS_CFA$VARCHAR2_2      :=get_col_indx(IS_CFA_cols,'VARCHAR2_2');
  IS_CFA$VARCHAR2_3      :=get_col_indx(IS_CFA_cols,'VARCHAR2_3');
  IS_CFA$VARCHAR2_4      :=get_col_indx(IS_CFA_cols,'VARCHAR2_4');
  IS_CFA$VARCHAR2_5      :=get_col_indx(IS_CFA_cols,'VARCHAR2_5');
  IS_CFA$VARCHAR2_6      :=get_col_indx(IS_CFA_cols,'VARCHAR2_6');
  IS_CFA$VARCHAR2_7      :=get_col_indx(IS_CFA_cols,'VARCHAR2_7');
  IS_CFA$VARCHAR2_8      :=get_col_indx(IS_CFA_cols,'VARCHAR2_8');
  IS_CFA$VARCHAR2_9      :=get_col_indx(IS_CFA_cols,'VARCHAR2_9');
  IS_CFA$VARCHAR2_10     :=get_col_indx(IS_CFA_cols,'VARCHAR2_10');
  IS_CFA$NUMBER_11       :=get_col_indx(IS_CFA_cols,'NUMBER_11');
  IS_CFA$NUMBER_12       :=get_col_indx(IS_CFA_cols,'NUMBER_12');
  IS_CFA$NUMBER_13       :=get_col_indx(IS_CFA_cols,'NUMBER_13');
  IS_CFA$NUMBER_14       :=get_col_indx(IS_CFA_cols,'NUMBER_14');
  IS_CFA$NUMBER_15       :=get_col_indx(IS_CFA_cols,'NUMBER_15');
  IS_CFA$NUMBER_16       :=get_col_indx(IS_CFA_cols,'NUMBER_16');
  IS_CFA$NUMBER_17       :=get_col_indx(IS_CFA_cols,'NUMBER_17');
  IS_CFA$NUMBER_18       :=get_col_indx(IS_CFA_cols,'NUMBER_18');
  IS_CFA$NUMBER_19       :=get_col_indx(IS_CFA_cols,'NUMBER_19');
  IS_CFA$NUMBER_20       :=get_col_indx(IS_CFA_cols,'NUMBER_20');
  IS_CFA$DATE_21         :=get_col_indx(IS_CFA_cols,'DATE_21');
  IS_CFA$DATE_22         :=get_col_indx(IS_CFA_cols,'DATE_22');
  IS_CFA$DATE_23         :=get_col_indx(IS_CFA_cols,'DATE_23');
  IS_CFA$DATE_24         :=get_col_indx(IS_CFA_cols,'DATE_24');
  IS_CFA$DATE_25         :=get_col_indx(IS_CFA_cols,'DATE_25');
  ISC_cols               :=s9t_pkg.get_col_names(I_file_id,ISC_sheet);
  ISC$Action             :=get_col_indx(ISC_cols,action_column);
  ISC$ITEM               :=get_col_indx(ISC_cols,'ITEM');
  ISC$SUPPLIER           :=get_col_indx(ISC_cols,'SUPPLIER');
  ISC$ORIGIN_COUNTRY_ID  :=get_col_indx(ISC_cols,'ORIGIN_COUNTRY_ID');
  ISC$UNIT_COST          :=get_col_indx(ISC_cols,'UNIT_COST');
  ISC$LEAD_TIME          :=get_col_indx(ISC_cols,'LEAD_TIME');
  ISC$PICKUP_LEAD_TIME   :=get_col_indx(ISC_cols,'PICKUP_LEAD_TIME');
  ISC$SUPP_PACK_SIZE     :=get_col_indx(ISC_cols,'SUPP_PACK_SIZE');
  ISC$INNER_PACK_SIZE    :=get_col_indx(ISC_cols,'INNER_PACK_SIZE');
  ISC$ROUND_LVL          :=get_col_indx(ISC_cols,'ROUND_LVL');
  ISC$ROUND_TO_INNER_PCT :=get_col_indx(ISC_cols,'ROUND_TO_INNER_PCT');
  ISC$ROUND_TO_CASE_PCT   :=get_col_indx(ISC_cols,'ROUND_TO_CASE_PCT');
  ISC$ROUND_TO_LAYER_PCT  :=get_col_indx(ISC_cols,'ROUND_TO_LAYER_PCT');
  ISC$ROUND_TO_PALLET_PCT :=get_col_indx(ISC_cols,'ROUND_TO_PALLET_PCT');
  ISC$MIN_ORDER_QTY      :=get_col_indx(ISC_cols,'MIN_ORDER_QTY');
  ISC$MAX_ORDER_QTY      :=get_col_indx(ISC_cols,'MAX_ORDER_QTY');
  ISC$PACKING_METHOD         :=get_col_indx(ISC_cols,'PACKING_METHOD');
  ISC$PRIMARY_SUPP_IND       :=get_col_indx(ISC_cols,'PRIMARY_SUPP_IND');
  ISC$PRIMARY_COUNTRY_IND    :=get_col_indx(ISC_cols,'PRIMARY_COUNTRY_IND');
  ISC$DEFAULT_UOP            :=get_col_indx(ISC_cols,'DEFAULT_UOP');
  ISC$TI                     :=get_col_indx(ISC_cols,'TI');
  ISC$HI                     :=get_col_indx(ISC_cols,'HI');
  ISC$SUPP_HIER_LVL_1        :=get_col_indx(ISC_cols,'SUPP_HIER_LVL_1');
  ISC$SUPP_HIER_LVL_2        :=get_col_indx(ISC_cols,'SUPP_HIER_LVL_2');
  ISC$SUPP_HIER_LVL_3        :=get_col_indx(ISC_cols,'SUPP_HIER_LVL_3');
  ISC$COST_UOM               :=get_col_indx(ISC_cols,'COST_UOM');
  ISC$TOLERANCE_TYPE         :=get_col_indx(ISC_cols,'TOLERANCE_TYPE');
  ISC$MAX_TOLERANCE          :=get_col_indx(ISC_cols,'MAX_TOLERANCE');
  ISC$MIN_TOLERANCE          :=get_col_indx(ISC_cols,'MIN_TOLERANCE');
  ISCL_cols                  :=s9t_pkg.get_col_names(I_file_id,ISCL_sheet);
  ISCL$Action                :=get_col_indx(ISCL_cols,action_column);
  ISCL$ITEM                  :=get_col_indx(ISCL_cols,'ITEM');
  ISCL$SUPPLIER              :=get_col_indx(ISCL_cols,'SUPPLIER');
  ISCL$ORIGIN_COUNTRY_ID     :=get_col_indx(ISCL_cols,'ORIGIN_COUNTRY_ID');
  ISCL$LOC                   :=get_col_indx(ISCL_cols,'LOC');
  ISCL$LOC_TYPE              :=get_col_indx(ISCL_cols,'LOC_TYPE');
  ISCL$PRIMARY_LOC_IND       :=get_col_indx(ISCL_cols,'PRIMARY_LOC_IND');
  ISCL$UNIT_COST              :=get_col_indx(ISCL_cols,'UNIT_COST');
  ISCL$NEGOTIATED_ITEM_COST   :=get_col_indx(ISCL_cols,'NEGOTIATED_ITEM_COST');
  ISCL$ROUND_LVL              :=get_col_indx(ISCL_cols,'ROUND_LVL');
  ISCL$ROUND_TO_INNER_PCT     :=get_col_indx(ISCL_cols,'ROUND_TO_INNER_PCT');
  ISCL$ROUND_TO_CASE_PCT      :=get_col_indx(ISCL_cols,'ROUND_TO_CASE_PCT');
  ISCL$ROUND_TO_LAYER_PCT     :=get_col_indx(ISCL_cols,'ROUND_TO_LAYER_PCT');
  ISCL$ROUND_TO_PALLET_PCT    :=get_col_indx(ISCL_cols,'ROUND_TO_PALLET_PCT');
  ISCL$SUPP_HIER_LVL_1        :=get_col_indx(ISCL_cols,'SUPP_HIER_LVL_1');
  ISCL$SUPP_HIER_LVL_2         :=get_col_indx(ISCL_cols,'SUPP_HIER_LVL_2');
  ISCL$SUPP_HIER_LVL_3         :=get_col_indx(ISCL_cols,'SUPP_HIER_LVL_3');
  ISCL$PICKUP_LEAD_TIME        :=get_col_indx(ISCL_cols,'PICKUP_LEAD_TIME');
  ISC_CFA_cols           :=s9t_pkg.get_col_names(I_file_id,ISC_CFA_sheet);
  ISC_CFA$Action         :=get_col_indx(ISC_CFA_cols,action_column);
  ISC_CFA$ITEM           :=get_col_indx(ISC_CFA_cols,'ITEM');
  ISC_CFA$SUPPLIER       :=get_col_indx(ISC_CFA_cols,'SUPPLIER');
  ISC_CFA$ORIGIN_COUNTRY_ID  :=get_col_indx(ISC_CFA_cols,'ORIGIN_COUNTRY_ID');
  ISC_CFA$GROUP_ID       :=get_col_indx(ISC_CFA_cols,'GROUP_ID');
  ISC_CFA$VARCHAR2_1         :=get_col_indx(ISC_CFA_cols,'VARCHAR2_1');
  ISC_CFA$VARCHAR2_2         :=get_col_indx(ISC_CFA_cols,'VARCHAR2_2');
  ISC_CFA$VARCHAR2_3         :=get_col_indx(ISC_CFA_cols,'VARCHAR2_3');
  ISC_CFA$VARCHAR2_4         :=get_col_indx(ISC_CFA_cols,'VARCHAR2_4');
  ISC_CFA$VARCHAR2_5         :=get_col_indx(ISC_CFA_cols,'VARCHAR2_5');
  ISC_CFA$VARCHAR2_6         :=get_col_indx(ISC_CFA_cols,'VARCHAR2_6');
  ISC_CFA$VARCHAR2_7         :=get_col_indx(ISC_CFA_cols,'VARCHAR2_7');
  ISC_CFA$VARCHAR2_8         :=get_col_indx(ISC_CFA_cols,'VARCHAR2_8');
  ISC_CFA$VARCHAR2_9         :=get_col_indx(ISC_CFA_cols,'VARCHAR2_9');
  ISC_CFA$VARCHAR2_10        :=get_col_indx(ISC_CFA_cols,'VARCHAR2_10');
  ISC_CFA$NUMBER_11      :=get_col_indx(ISC_CFA_cols,'NUMBER_11');
  ISC_CFA$NUMBER_12      :=get_col_indx(ISC_CFA_cols,'NUMBER_12');
  ISC_CFA$NUMBER_13      :=get_col_indx(ISC_CFA_cols,'NUMBER_13');
  ISC_CFA$NUMBER_14      :=get_col_indx(ISC_CFA_cols,'NUMBER_14');
  ISC_CFA$NUMBER_15      :=get_col_indx(ISC_CFA_cols,'NUMBER_15');
  ISC_CFA$NUMBER_16      :=get_col_indx(ISC_CFA_cols,'NUMBER_16');
  ISC_CFA$NUMBER_17      :=get_col_indx(ISC_CFA_cols,'NUMBER_17');
  ISC_CFA$NUMBER_18      :=get_col_indx(ISC_CFA_cols,'NUMBER_18');
  ISC_CFA$NUMBER_19      :=get_col_indx(ISC_CFA_cols,'NUMBER_19');
  ISC_CFA$NUMBER_20      :=get_col_indx(ISC_CFA_cols,'NUMBER_20');
  ISC_CFA$DATE_21        :=get_col_indx(ISC_CFA_cols,'DATE_21');
  ISC_CFA$DATE_22        :=get_col_indx(ISC_CFA_cols,'DATE_22');
  ISC_CFA$DATE_23        :=get_col_indx(ISC_CFA_cols,'DATE_23');
  ISC_CFA$DATE_24        :=get_col_indx(ISC_CFA_cols,'DATE_24');
  ISC_CFA$DATE_25        :=get_col_indx(ISC_CFA_cols,'DATE_25');
  ISCD_cols          :=s9t_pkg.get_col_names(I_file_id,ISCD_sheet);
  ISCD$Action            :=get_col_indx(ISCD_cols,action_column);
  ISCD$ITEM          :=get_col_indx(ISCD_cols,'ITEM');
  ISCD$SUPPLIER          :=get_col_indx(ISCD_cols,'SUPPLIER');
  ISCD$ORIGIN_COUNTRY        :=get_col_indx(ISCD_cols,'ORIGIN_COUNTRY');
  ISCD$DIM_OBJECT        :=get_col_indx(ISCD_cols,'DIM_OBJECT');
  ISCD$PRESENTATION_METHOD   :=get_col_indx(ISCD_cols,'PRESENTATION_METHOD');
  ISCD$LENGTH            :=get_col_indx(ISCD_cols,'LENGTH');
  ISCD$WIDTH             :=get_col_indx(ISCD_cols,'WIDTH');
  ISCD$HEIGHT            :=get_col_indx(ISCD_cols,'HEIGHT');
  ISCD$LWH_UOM           :=get_col_indx(ISCD_cols,'LWH_UOM');
  ISCD$WEIGHT            :=get_col_indx(ISCD_cols,'WEIGHT');
  ISCD$NET_WEIGHT        :=get_col_indx(ISCD_cols,'NET_WEIGHT');
  ISCD$WEIGHT_UOM        :=get_col_indx(ISCD_cols,'WEIGHT_UOM');
  ISCD$LIQUID_VOLUME         :=get_col_indx(ISCD_cols,'LIQUID_VOLUME');
  ISCD$LIQUID_VOLUME_UOM     :=get_col_indx(ISCD_cols,'LIQUID_VOLUME_UOM');
  ISCD$STAT_CUBE         :=get_col_indx(ISCD_cols,'STAT_CUBE');
  ISCD$TARE_WEIGHT       :=get_col_indx(ISCD_cols,'TARE_WEIGHT');
  ISCD$TARE_TYPE         :=get_col_indx(ISCD_cols,'TARE_TYPE');
  ISMC_cols          :=s9t_pkg.get_col_names(I_file_id,ISMC_sheet);
  ISMC$Action            :=get_col_indx(ISMC_cols,action_column);
  ISMC$ITEM          :=get_col_indx(ISMC_cols,'ITEM');
  ISMC$SUPPLIER          :=get_col_indx(ISMC_cols,'SUPPLIER');
  ISMC$MANU_COUNTRY_ID       :=get_col_indx(ISMC_cols,'MANU_COUNTRY_ID');
  ISMC$PRIMARY_MANU_CTRY_IND     :=get_col_indx(ISMC_cols,'PRIMARY_MANU_CTRY_IND');
  ISU_cols           :=s9t_pkg.get_col_names(I_file_id,ISU_sheet);
  ISU$Action             :=get_col_indx(ISU_cols,action_column);
  ISU$ITEM           :=get_col_indx(ISU_cols,'ITEM');
  ISU$SUPPLIER           :=get_col_indx(ISU_cols,'SUPPLIER');
  ISU$UOM            :=get_col_indx(ISU_cols,'UOM');
  ISU$VALUE          :=get_col_indx(ISU_cols,'VALUE');
  IXD_cols           :=s9t_pkg.get_col_names(I_file_id,IXD_sheet);
  IXD$Action             :=get_col_indx(IXD_cols,action_column);
  IXD$HEAD_ITEM          :=get_col_indx(IXD_cols,'HEAD_ITEM');
  IXD$DETAIL_ITEM        :=get_col_indx(IXD_cols,'DETAIL_ITEM');
  IXD$ITEM_QUANTITY_PCT      :=get_col_indx(IXD_cols,'ITEM_QUANTITY_PCT');
  IXD$YIELD_FROM_HEAD_ITEM_PCT   :=get_col_indx(IXD_cols,'YIELD_FROM_HEAD_ITEM_PCT');
  IXH_cols           :=s9t_pkg.get_col_names(I_file_id,IXH_sheet);
  IXH$Action             :=get_col_indx(IXH_cols,action_column);
  IXH$ITEM_XFORM_HEAD_ID     :=get_col_indx(IXH_cols,'ITEM_XFORM_HEAD_ID');
  IXH$HEAD_ITEM          :=get_col_indx(IXH_cols,'HEAD_ITEM');
  IXH$ITEM_XFORM_TYPE        :=get_col_indx(IXH_cols,'ITEM_XFORM_TYPE');
  IXH$ITEM_XFORM_DESC        :=get_col_indx(IXH_cols,'ITEM_XFORM_DESC');
  IXH$PRODUCTION_LOSS_PCT    :=get_col_indx(IXH_cols,'PRODUCTION_LOSS_PCT');
  IXH$COMMENTS_DESC      :=get_col_indx(IXH_cols,'COMMENTS_DESC');
  IXHTL_cols                     := s9t_pkg.get_col_names(I_file_id,IXHTL_sheet);
  IXHTL$Action                   := get_col_indx(IXHTL_cols,action_column);
  IXHTL$LANG                     := get_col_indx(IXHTL_cols,'LANG');
  IXHTL$HEAD_ITEM                     := get_col_indx(IXHTL_cols,'HEAD_ITEM');
  IXHTL$ITEM_XFORM_HEAD_ID       := get_col_indx(IXHTL_cols,'ITEM_XFORM_HEAD_ID');
  IXHTL$ITEM_XFORM_DESC                := get_col_indx(IXHTL_cols,'ITEM_XFORM_DESC');
  PI_cols            :=s9t_pkg.get_col_names(I_file_id,PI_sheet);
  PI$Action          :=get_col_indx(PI_cols,action_column);
  PI$PACK_NO             :=get_col_indx(PI_cols,'PACK_NO');
  PI$SEQ_NO          :=get_col_indx(PI_cols,'SEQ_NO');
  PI$ITEM            :=get_col_indx(PI_cols,'ITEM');
  PI$ITEM_PARENT         :=get_col_indx(PI_cols,'ITEM_PARENT');
  PI$PACK_TMPL_ID        :=get_col_indx(PI_cols,'PACK_TMPL_ID');
  PI$PACK_QTY            :=get_col_indx(PI_cols,'PACK_QTY');
  IZP_cols           :=s9t_pkg.get_col_names(I_file_id,IZP_sheet);
  IZP$Action             :=get_col_indx(IZP_cols,action_column);
  IZP$ITEM_ZONE_PRICE_ID     :=get_col_indx(IZP_cols,'ITEM_ZONE_PRICE_ID');
  IZP$ITEM           :=get_col_indx(IZP_cols,'ITEM');
  IZP$ZONE_ID            :=get_col_indx(IZP_cols,'ZONE_ID');
  IZP$STANDARD_RETAIL        :=get_col_indx(IZP_cols,'STANDARD_RETAIL');
  IZP$STANDARD_RETAIL_CURRENCY   :=get_col_indx(IZP_cols,'STANDARD_RETAIL_CURRENCY');
  IZP$STANDARD_UOM       :=get_col_indx(IZP_cols,'STANDARD_UOM');
  IZP$SELLING_RETAIL         :=get_col_indx(IZP_cols,'SELLING_RETAIL');
  IZP$SELLING_UOM        :=get_col_indx(IZP_cols,'SELLING_UOM');
  IZP$MULTI_UNITS        :=get_col_indx(IZP_cols,'MULTI_UNITS');
  IZP$MULTI_UNIT_RETAIL      :=get_col_indx(IZP_cols,'MULTI_UNIT_RETAIL');
  IZP$MULTI_UNIT_RETAIL_CURRENCY :=get_col_indx(IZP_cols,'MULTI_UNIT_RETAIL_CURRENCY');
  IZP$MULTI_SELLING_UOM      :=get_col_indx(IZP_cols,'MULTI_SELLING_UOM');
  IZP$LOCK_VERSION       :=get_col_indx(IZP_cols,'LOCK_VERSION');
  UID_cols           :=s9t_pkg.get_col_names(I_file_id,UID_sheet);
  UID$Action             :=get_col_indx(UID_cols,action_column);
  UID$ITEM           :=get_col_indx(UID_cols,'ITEM');
  UID$UDA_ID             :=get_col_indx(UID_cols,'UDA_ID');
  UID$UDA_DATE           :=get_col_indx(UID_cols,'UDA_DATE');
  UIF_cols           :=s9t_pkg.get_col_names(I_file_id,UIF_sheet);
  UIF$Action             :=get_col_indx(UIF_cols,action_column);
  UIF$ITEM           :=get_col_indx(UIF_cols,'ITEM');
  UIF$UDA_ID             :=get_col_indx(UIF_cols,'UDA_ID');
  UIF$UDA_TEXT           :=get_col_indx(UIF_cols,'UDA_TEXT');
  UIL_cols           :=s9t_pkg.get_col_names(I_file_id,UIL_sheet);
  UIL$Action             :=get_col_indx(UIL_cols,action_column);
  UIL$ITEM           :=get_col_indx(UIL_cols,'ITEM');
  UIL$UDA_ID             :=get_col_indx(UIL_cols,'UDA_ID');
  UIL$UDA_VALUE          :=get_col_indx(UIL_cols,'UDA_VALUE');
  VI_cols            :=s9t_pkg.get_col_names(I_file_id,VI_sheet);
  VI$Action          :=get_col_indx(VI_cols,action_column);
  VI$ITEM            :=get_col_indx(VI_cols,'ITEM');
  VI$VAT_REGION          :=get_col_indx(VI_cols,'VAT_REGION');
  VI$ACTIVE_DATE         :=get_col_indx(VI_cols,'ACTIVE_DATE');
  VI$VAT_TYPE            :=get_col_indx(VI_cols,'VAT_TYPE');
  VI$VAT_CODE            :=get_col_indx(VI_cols,'VAT_CODE');
  VI$REVERSE_VAT_IND         :=get_col_indx(VI_cols,'REVERSE_VAT_IND');
  CSH_cols           :=s9t_pkg.get_col_names(I_file_id,CSH_sheet);
  CSH$Action             := get_col_indx(CSH_cols,action_column);
  CSH$APPROVAL_ID        := get_col_indx(CSH_cols,'APPROVAL_ID');
  CSH$APPROVAL_DATE      := get_col_indx(CSH_cols,'APPROVAL_DATE');
  CSH$COST_CHANGE_ORIGIN     := get_col_indx(CSH_cols,'COST_CHANGE_ORIGIN');
  CSH$STATUS             := get_col_indx(CSH_cols,'STATUS');
  CSH$ACTIVE_DATE        := get_col_indx(CSH_cols,'ACTIVE_DATE');
  CSH$REASON             := get_col_indx(CSH_cols,'REASON');
  CSH$COST_CHANGE_DESC       := get_col_indx(CSH_cols,'COST_CHANGE_DESC');
  CSH$COST_CHANGE        := get_col_indx(CSH_cols,'COST_CHANGE');
  CSH_CFA_cols            :=s9t_pkg.get_col_names(I_file_id,CSH_CFA_sheet);
  CSH_CFA$Action          :=get_col_indx(CSH_CFA_cols,action_column);
  CSH_CFA$COST_CHANGE     :=get_col_indx(CSH_CFA_cols,'COST_CHANGE');
  CSH_CFA$GROUP_ID        :=get_col_indx(CSH_CFA_cols,'GROUP_ID');
  CSH_CFA$VARCHAR2_1      :=get_col_indx(CSH_CFA_cols,'VARCHAR2_1');
  CSH_CFA$VARCHAR2_2      :=get_col_indx(CSH_CFA_cols,'VARCHAR2_2');
  CSH_CFA$VARCHAR2_3      :=get_col_indx(CSH_CFA_cols,'VARCHAR2_3');
  CSH_CFA$VARCHAR2_4      :=get_col_indx(CSH_CFA_cols,'VARCHAR2_4');
  CSH_CFA$VARCHAR2_5      :=get_col_indx(CSH_CFA_cols,'VARCHAR2_5');
  CSH_CFA$VARCHAR2_6      :=get_col_indx(CSH_CFA_cols,'VARCHAR2_6');
  CSH_CFA$VARCHAR2_7      :=get_col_indx(CSH_CFA_cols,'VARCHAR2_7');
  CSH_CFA$VARCHAR2_8      :=get_col_indx(CSH_CFA_cols,'VARCHAR2_8');
  CSH_CFA$VARCHAR2_9      :=get_col_indx(CSH_CFA_cols,'VARCHAR2_9');
  CSH_CFA$VARCHAR2_10     :=get_col_indx(CSH_CFA_cols,'VARCHAR2_10');
  CSH_CFA$NUMBER_11       :=get_col_indx(CSH_CFA_cols,'NUMBER_11');
  CSH_CFA$NUMBER_12       :=get_col_indx(CSH_CFA_cols,'NUMBER_12');
  CSH_CFA$NUMBER_13       :=get_col_indx(CSH_CFA_cols,'NUMBER_13');
  CSH_CFA$NUMBER_14       :=get_col_indx(CSH_CFA_cols,'NUMBER_14');
  CSH_CFA$NUMBER_15       :=get_col_indx(CSH_CFA_cols,'NUMBER_15');
  CSH_CFA$NUMBER_16       :=get_col_indx(CSH_CFA_cols,'NUMBER_16');
  CSH_CFA$NUMBER_17       :=get_col_indx(CSH_CFA_cols,'NUMBER_17');
  CSH_CFA$NUMBER_18       :=get_col_indx(CSH_CFA_cols,'NUMBER_18');
  CSH_CFA$NUMBER_19       :=get_col_indx(CSH_CFA_cols,'NUMBER_19');
  CSH_CFA$NUMBER_20       :=get_col_indx(CSH_CFA_cols,'NUMBER_20');
  CSH_CFA$DATE_21         :=get_col_indx(CSH_CFA_cols,'DATE_21');
  CSH_CFA$DATE_22         :=get_col_indx(CSH_CFA_cols,'DATE_22');
  CSH_CFA$DATE_23         :=get_col_indx(CSH_CFA_cols,'DATE_23');
  CSH_CFA$DATE_24         :=get_col_indx(CSH_CFA_cols,'DATE_24');
  CSH_CFA$DATE_25         :=get_col_indx(CSH_CFA_cols,'DATE_25');
  CSDL_cols          :=s9t_pkg.get_col_names(I_file_id,CSDL_sheet);
  CSDL$Action            := get_col_indx(CSDL_cols,action_column);
  CSDL$BRACKET_VALUE1        := get_col_indx(CSDL_cols,'BRACKET_VALUE1');
  CSDL$LOC           := get_col_indx(CSDL_cols,'LOC');
  CSDL$LOC_TYPE          := get_col_indx(CSDL_cols,'LOC_TYPE');
  CSDL$ITEM          := get_col_indx(CSDL_cols,'ITEM');
  CSDL$ORIGIN_COUNTRY_ID     := get_col_indx(CSDL_cols,'ORIGIN_COUNTRY_ID');
  CSDL$SUPPLIER          := get_col_indx(CSDL_cols,'SUPPLIER');
  CSDL$COST_CHANGE       := get_col_indx(CSDL_cols,'COST_CHANGE');
  CSDL$DELIVERY_COUNTRY_ID   := get_col_indx(CSDL_cols,'DELIVERY_COUNTRY_ID');
  CSDL$SUP_DEPT_SEQ_NO       := get_col_indx(CSDL_cols,'SUP_DEPT_SEQ_NO');
  CSDL$DEPT          := get_col_indx(CSDL_cols,'DEPT');
  CSDL$DEFAULT_BRACKET_IND   := get_col_indx(CSDL_cols,'DEFAULT_BRACKET_IND');
  CSDL$RECALC_ORD_IND        := get_col_indx(CSDL_cols,'RECALC_ORD_IND');
  CSDL$COST_CHANGE_VALUE     := get_col_indx(CSDL_cols,'COST_CHANGE_VALUE');
  CSDL$COST_CHANGE_TYPE      := get_col_indx(CSDL_cols,'COST_CHANGE_TYPE');
  CSDL$UNIT_COST         := get_col_indx(CSDL_cols,'UNIT_COST');
  CSDL$BRACKET_VALUE2        := get_col_indx(CSDL_cols,'BRACKET_VALUE2');
  CSDL$BRACKET_UOM1      := get_col_indx(CSDL_cols,'BRACKET_UOM1');
  CSD_cols           :=s9t_pkg.get_col_names(I_file_id,CSD_sheet);
  CSD$Action             := get_col_indx(CSD_cols,action_column);
  CSD$DELIVERY_COUNTRY_ID    := get_col_indx(CSD_cols,'DELIVERY_COUNTRY_ID');
  CSD$SUP_DEPT_SEQ_NO        := get_col_indx(CSD_cols,'SUP_DEPT_SEQ_NO');
  CSD$DEPT           := get_col_indx(CSD_cols,'DEPT');
  CSD$DEFAULT_BRACKET_IND    := get_col_indx(CSD_cols,'DEFAULT_BRACKET_IND');
  CSD$RECALC_ORD_IND         := get_col_indx(CSD_cols,'RECALC_ORD_IND');
  CSD$COST_CHANGE_VALUE      := get_col_indx(CSD_cols,'COST_CHANGE_VALUE');
  CSD$COST_CHANGE_TYPE       := get_col_indx(CSD_cols,'COST_CHANGE_TYPE');
  CSD$UNIT_COST          := get_col_indx(CSD_cols,'UNIT_COST');
  CSD$BRACKET_VALUE2         := get_col_indx(CSD_cols,'BRACKET_VALUE2');
  CSD$BRACKET_UOM1       := get_col_indx(CSD_cols,'BRACKET_UOM1');
  CSD$BRACKET_VALUE1         := get_col_indx(CSD_cols,'BRACKET_VALUE1');
  CSD$ITEM           := get_col_indx(CSD_cols,'ITEM');
  CSD$ORIGIN_COUNTRY_ID      := get_col_indx(CSD_cols,'ORIGIN_COUNTRY_ID');
  CSD$SUPPLIER           := get_col_indx(CSD_cols,'SUPPLIER');
  CSD$COST_CHANGE        := get_col_indx(CSD_cols,'COST_CHANGE');
  IIM_cols                       := s9t_pkg.get_col_names(I_file_id,IIM_sheet);
  IIM$Action                     := get_col_indx(IIM_cols,action_column);
  IIM$ITEM                       := get_col_indx(IIM_cols,'ITEM');
  IIM$IMAGE_NAME                 := get_col_indx(IIM_cols,'IMAGE_NAME');
  IIM$IMAGE_ADDR                 := get_col_indx(IIM_cols,'IMAGE_ADDR');
  IIM$IMAGE_DESC                 := get_col_indx(IIM_cols,'IMAGE_DESC');
  IIM$IMAGE_TYPE                 := get_col_indx(IIM_cols,'IMAGE_TYPE');
  IIM$PRIMARY_IND                := get_col_indx(IIM_cols,'PRIMARY_IND');
  IIM$DISPLAY_PRIORITY           := get_col_indx(IIM_cols,'DISPLAY_PRIORITY');
  IIMTL_cols                     := s9t_pkg.get_col_names(I_file_id,IIMTL_sheet);
  IIMTL$Action                   := get_col_indx(IIMTL_cols,action_column);
  IIMTL$LANG                     := get_col_indx(IIMTL_cols,'LANG');
  IIMTL$ITEM                     := get_col_indx(IIMTL_cols,'ITEM');
  IIMTL$IMAGE_NAME               := get_col_indx(IIMTL_cols,'IMAGE_NAME');
  IIMTL$IMAGE_DESC               := get_col_indx(IIMTL_cols,'IMAGE_DESC');
  ISE_cols                       := s9t_pkg.get_col_names(I_file_id,ISE_sheet);
  ISE$Action                     := get_col_indx(ISE_cols,action_column);
  ISE$ITEM                       := get_col_indx(ISE_cols,'ITEM');
  ISE$SEASON_ID                  := get_col_indx(ISE_cols,'SEASON_ID');
  ISE$PHASE_ID                   := get_col_indx(ISE_cols,'PHASE_ID');
  ISE$ITEM_SEASON_SEQ_NO         := get_col_indx(ISE_cols,'ITEM_SEASON_SEQ_NO');
  ISE$DIFF_ID                    := get_col_indx(ISE_cols,'DIFF_ID');
END populate_names;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_ICD(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ICD_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,ORIGIN_COUNTRY_ID ,DELIVERY_COUNTRY_ID ,COND_TYPE ,COND_VALUE ,APPLIED_ON ,COMP_RATE ,CALCULATION_BASIS ,RECOVERABLE_AMOUNT ,MODIFIED_TAXABLE_BASE ))
  FROM ITEM_COST_DETAIL
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_ICD;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_ICH(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ICH_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,ORIGIN_COUNTRY_ID ,DELIVERY_COUNTRY_ID ,PRIM_DLVY_CTRY_IND ,NIC_STATIC_IND ,BASE_COST ,NEGOTIATED_ITEM_COST ,EXTENDED_BASE_COST ,INCLUSIVE_COST ))
  FROM ITEM_COST_HEAD
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_ICH;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_IC(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = IC_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,COUNTRY_ID ))
  FROM ITEM_COUNTRY
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_IC;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_IM(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = IM_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,ITEM_NUMBER_TYPE ,FORMAT_ID ,PREFIX ,ITEM_PARENT ,ITEM_GRANDPARENT ,PACK_IND ,ITEM_LEVEL ,TRAN_LEVEL ,ITEM_AGGREGATE_IND ,DIFF_1 ,DIFF_1_AGGREGATE_IND ,DIFF_2 ,DIFF_2_AGGREGATE_IND ,DIFF_3 ,DIFF_3_AGGREGATE_IND ,DIFF_4 ,DIFF_4_AGGREGATE_IND ,DEPT ,CLASS ,SUBCLASS ,STATUS ,ITEM_DESC ,ITEM_DESC_SECONDARY ,SHORT_DESC  ,PRIMARY_REF_ITEM_IND ,COST_ZONE_GROUP_ID ,STANDARD_UOM ,UOM_CONV_FACTOR ,PACKAGE_SIZE ,PACKAGE_UOM ,MERCHANDISE_IND ,STORE_ORD_MULT ,FORECAST_IND ,MFG_REC_RETAIL ,RETAIL_LABEL_TYPE ,RETAIL_LABEL_VALUE ,HANDLING_TEMP ,HANDLING_SENSITIVITY ,CATCH_WEIGHT_IND ,WASTE_TYPE ,WASTE_PCT ,DEFAULT_WASTE_PCT ,CONST_DIMEN_IND ,SIMPLE_PACK_IND ,CONTAINS_INNER_IND ,SELLABLE_IND ,ORDERABLE_IND ,PACK_TYPE ,ORDER_AS_TYPE ,COMMENTS ,ITEM_SERVICE_LEVEL ,GIFT_WRAP_IND ,SHIP_ALONE_IND  ,ITEM_XFORM_IND ,INVENTORY_IND ,ORDER_TYPE ,SALE_TYPE ,DEPOSIT_ITEM_TYPE ,CONTAINER_ITEM ,DEPOSIT_IN_PRICE_PER_UOM ,AIP_CASE_TYPE ,
     PERISHABLE_IND ,NOTIONAL_PACK_IND ,SOH_INQUIRY_AT_PACK_IND ,PRODUCT_CLASSIFICATION ,BRAND_NAME,NULL,LAST_UPDATE_ID,NULL,NULL ))
  FROM ITEM_MASTER
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_IM;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_IM_TL(I_file_id      IN   NUMBER,
                         I_process_id   IN   NUMBER)
IS

BEGIN
   insert into table (select ss.s9t_rows
                        from s9t_folder sf,
                             table(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = IMTL_sheet)
              select s9t_row(s9t_cells('MOD',
                                       lang,
                                       item,
                                       item_desc,
                                       item_desc_secondary,
                                       short_desc)
                            )
                from item_master_tl
               where item in (select item
                                from svc_item_search_temp
                               where process_id = I_process_id);

END POPULATE_IM_TL;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_IS(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = IS_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,PRIMARY_SUPP_IND ,VPN ,SUPP_LABEL ,CONSIGNMENT_RATE ,SUPP_DIFF_1 ,SUPP_DIFF_2 ,SUPP_DIFF_3 ,SUPP_DIFF_4 ,PALLET_NAME ,CASE_NAME ,INNER_NAME ,SUPP_DISCONTINUE_DATE ,DIRECT_SHIP_IND ,CONCESSION_RATE ,PRIMARY_CASE_SIZE ))
  FROM ITEM_SUPPLIER
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_IS;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_IS_TL(I_file_id      IN   NUMBER,
                         I_process_id   IN   NUMBER)
IS

BEGIN
   insert into table (select ss.s9t_rows
                        from s9t_folder sf,
                             table(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = ISTL_sheet)
              select s9t_row(s9t_cells('MOD',
                                       lang,
                                       item,
                                       supplier,
                                       supp_label,
                                       supp_diff_1,
                                       supp_diff_2,
                                       supp_diff_3,
                                       supp_diff_4)
                            )
                from item_supplier_tl
               where item in (select item
                                from svc_item_search_temp
                               where process_id = I_process_id);

END POPULATE_IS_TL;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_ISC(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ISC_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,isc.ITEM ,isc.SUPPLIER ,isc.ORIGIN_COUNTRY_ID ,isc.UNIT_COST ,isc.LEAD_TIME ,isc.PICKUP_LEAD_TIME ,isc.SUPP_PACK_SIZE ,isc.INNER_PACK_SIZE ,isc.ROUND_LVL ,isc.ROUND_TO_INNER_PCT ,isc.ROUND_TO_CASE_PCT ,isc.ROUND_TO_LAYER_PCT ,isc.ROUND_TO_PALLET_PCT ,isc.MIN_ORDER_QTY ,isc.MAX_ORDER_QTY ,isc.PACKING_METHOD ,isc.PRIMARY_SUPP_IND ,isc.PRIMARY_COUNTRY_IND ,
                            decode(isc.default_uop,'C',isup.case_name,'P', isup.pallet_name, NVL(uom_sql.get_uom(isc.default_uop,'Download'),isc.default_uop)),
                             isc.TI ,isc.HI ,isc.SUPP_HIER_LVL_1 ,isc.SUPP_HIER_LVL_2 ,isc.SUPP_HIER_LVL_3 ,NVL(uom_sql.get_uom(isc.COST_UOM,'Download'),isc.COST_UOM) ,isc.TOLERANCE_TYPE ,isc.MAX_TOLERANCE ,isc.MIN_TOLERANCE))
  FROM ITEM_SUPP_COUNTRY isc, ITEM_SUPPLIER isup
  WHERE isc.item = isup.item
       AND isc.supplier = isup.supplier
       AND isc.item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_ISC;
---------------------------------------------------------------------------------------------------
PROCEDURE populate_ISCL(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
        TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ISCL_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,ORIGIN_COUNTRY_ID ,LOC,LOC_TYPE,PRIMARY_LOC_IND,UNIT_COST ,NEGOTIATED_ITEM_COST,ROUND_LVL ,ROUND_TO_INNER_PCT ,ROUND_TO_CASE_PCT ,ROUND_TO_LAYER_PCT ,ROUND_TO_PALLET_PCT ,SUPP_HIER_LVL_1 ,SUPP_HIER_LVL_2 ,SUPP_HIER_LVL_3 ,PICKUP_LEAD_TIME))
  FROM ITEM_SUPP_COUNTRY_LOC
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_ISCL;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_ISCD(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ISCD_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,ORIGIN_COUNTRY ,DIM_OBJECT ,PRESENTATION_METHOD ,LENGTH ,WIDTH ,HEIGHT ,NVL(uom_sql.get_uom(LWH_UOM,'Download'),LWH_UOM) ,WEIGHT ,NET_WEIGHT ,NVL(uom_sql.get_uom(WEIGHT_UOM,'Download'),WEIGHT_UOM ),LIQUID_VOLUME , NVL(uom_sql.get_uom(LIQUID_VOLUME_UOM,'Download'),LIQUID_VOLUME_UOM ),STAT_CUBE ,TARE_WEIGHT ,TARE_TYPE ))
  FROM ITEM_SUPP_COUNTRY_DIM
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_ISCD;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_ISMC(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ISMC_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,MANU_COUNTRY_ID ,PRIMARY_MANU_CTRY_IND ))
  FROM ITEM_SUPP_MANU_COUNTRY
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_ISMC;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_ISU(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ISU_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,NVL(uom_sql.get_uom(UOM,'Download'),UOM),VALUE ))
  FROM ITEM_SUPP_UOM
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_ISU;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_IXD(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = IXD_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,HEAD_ITEM ,DETAIL_ITEM ,ITEM_QUANTITY_PCT ,YIELD_FROM_HEAD_ITEM_PCT ))
  FROM ITEM_XFORM_DETAIL ixd,
    item_xform_head ixh
  WHERE ixh.ITEM_XFORM_HEAD_ID = ixd.ITEM_XFORM_HEAD_ID
  AND ixh.HEAD_ITEM       IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_IXD;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_IXH(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = IXH_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM_XFORM_HEAD_ID ,HEAD_ITEM ,ITEM_XFORM_TYPE ,ITEM_XFORM_DESC ,PRODUCTION_LOSS_PCT ,COMMENTS_DESC ))
  FROM ITEM_XFORM_HEAD
  WHERE head_item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_IXH;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_IXH_TL(I_file_id      IN   NUMBER,
                          I_process_id   IN   NUMBER)
IS

BEGIN
   insert into table (select ss.s9t_rows
                        from s9t_folder sf,
                             table(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = IXHTL_sheet)
              select s9t_row(s9t_cells('MOD',
                                       ixhtl.lang,
                                       ixh.head_item,
                                       ixhtl.item_xform_head_id,
                                       ixhtl.item_xform_desc)
                            )
                from item_xform_head_tl ixhtl,
                     item_xform_head ixh
               where ixhtl.item_xform_head_id = ixh.item_xform_head_id
                 and ixh.head_item in (select item
                                from svc_item_search_temp
                               where process_id = I_process_id);

END POPULATE_IXH_TL;
---------------------------------------------------------------------------------------
PROCEDURE populate_PI(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = PI_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,PACK_NO ,SEQ_NO ,ITEM ,ITEM_PARENT ,PACK_TMPL_ID ,PACK_QTY ))
  FROM PACKITEM
  WHERE pack_no IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_PI;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_IZP(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = IZP_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,izp.ITEM_ZONE_PRICE_ID ,izp.ITEM ,rz.ZONE_DISPLAY_ID ,izp.STANDARD_RETAIL ,izp.STANDARD_RETAIL_CURRENCY ,izp.STANDARD_UOM ,izp.SELLING_RETAIL ,izp.SELLING_UOM ,izp.MULTI_UNITS ,izp.MULTI_UNIT_RETAIL ,izp.MULTI_UNIT_RETAIL_CURRENCY ,izp.MULTI_SELLING_UOM ,izp.LOCK_VERSION ))
  FROM RPM_ITEM_ZONE_PRICE izp,
    rpm_zone rz
  WHERE izp.zone_id = rz.zone_id
  AND item     IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_IZP;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_UID(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = UID_sheet
    )
  SELECT s9t_row(s9t_cells( NULL ,uidd.ITEM ,uidd.UDA_ID ,uidd.UDA_DATE ))
  FROM UDA_ITEM_DATE uidd
  WHERE uidd.item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id)
  AND EXISTS (select 'x' from v_uda vu where vu.uda_id = uidd.uda_id);
END populate_UID;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_UIF(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = UIF_sheet
    )
  SELECT s9t_row(s9t_cells( NULL ,uif.ITEM ,uif.UDA_ID ,uif.UDA_TEXT ))
  FROM UDA_ITEM_FF uif
  WHERE uif.item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id)
    and exists (select 'x' from v_uda vu where vu.uda_id = uif.uda_id);
END populate_UIF;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_UIL(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = UIL_sheet
    )
  SELECT s9t_row(s9t_cells( NULL ,uil.ITEM ,uil.UDA_ID ,uil.UDA_VALUE ))
  FROM UDA_ITEM_LOV uil
  WHERE uil.item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id)
  AND EXISTS (select 'x' from v_uda vu where vu.uda_id = uil.uda_id);
END populate_UIL;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_VI(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = VI_sheet
    )
  SELECT s9t_row(s9t_cells( NULL ,ITEM ,VAT_REGION ,ACTIVE_DATE ,VAT_TYPE ,VAT_CODE ,REVERSE_VAT_IND))
  FROM VAT_ITEM
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_VI;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_csh(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = CSH_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,COST_CHANGE, COST_CHANGE_DESC, REASON, ACTIVE_DATE, STATUS, COST_CHANGE_ORIGIN, APPROVAL_DATE, APPROVAL_ID))
  FROM COST_SUSP_SUP_HEAD
  WHERE cost_change IN
    (SELECT cost_change
    FROM SVC_COST_CHG_SEARCH_TEMP
    WHERE process_id = I_process_id
    );
END populate_csh;
---------------------------------------------------------------------------------------
PROCEDURE populate_csd(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = CSD_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,COST_CHANGE, SUPPLIER, ORIGIN_COUNTRY_ID, ITEM, COST_CHANGE_TYPE, COST_CHANGE_VALUE, RECALC_ORD_IND, DELIVERY_COUNTRY_ID ))
  FROM COST_SUSP_SUP_DETAIL
  WHERE cost_change IN
    (SELECT cost_change
    FROM SVC_COST_CHG_SEARCH_TEMP
    WHERE process_id = I_process_id
    );
END populate_csd;
---------------------------------------------------------------------------------------
PROCEDURE POPULATE_CSDL(I_file_id      IN   NUMBER,
            I_process_id   IN   NUMBER)
IS
BEGIN
   insert into TABLE(select ss.s9t_rows
               from s9t_folder sf,
                TABLE(sf.s9t_file_obj.sheets) ss
              where sf.file_id  = I_file_id
            and ss.sheet_name = CSDL_sheet)
           select s9t_row(s9t_cells('MOD',
                    cssd.cost_change,
                    cssd.supplier,
                    cssd.origin_country_id,
                    cssd.item,
                    cssd.loc_type,
                    locs.physical_loc,
                    cssd.cost_change_type,
                    cssd.cost_change_value,
                    cssd.recalc_ord_ind,
                    cssd.delivery_country_id))
         from cost_susp_sup_detail_loc cssd,
              (select physical_wh physical_loc,
                  wh loc,
                  'W' loc_type
             from wh
            union all
               select store physical_loc,
                  store loc,
                  'S' loc_type
             from store) locs
        where locs.loc = cssd.loc
          and locs.loc_type = cssd.loc_type
          and cssd.cost_change in (select cost_change
                         from svc_cost_chg_search_temp
                        where process_id = I_process_id);
END POPULATE_CSDL;
---------------------------------------------------------------------------------------
PROCEDURE POPULATE_IIM(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER)
IS

BEGIN
   insert into table (select ss.s9t_rows
                        from s9t_folder sf,
                             table(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = IIM_sheet)
              select s9t_row(s9t_cells('MOD',
                                       item,
                                       image_name,
                                       image_addr,
                                       image_desc,
                                       image_type,
                                       primary_ind,
                                       display_priority)
                            )
                from item_image
               where item in (select item
                                from svc_item_search_temp
                               where process_id = I_process_id);

END POPULATE_IIM;
---------------------------------------------------------------------------------------
PROCEDURE POPULATE_IIM_TL(I_file_id      IN   NUMBER,
                          I_process_id   IN   NUMBER)
IS

BEGIN
   insert into table (select ss.s9t_rows
                        from s9t_folder sf,
                             table(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = IIMTL_sheet)
              select s9t_row(s9t_cells('MOD',
                                       lang,
                                       item,
                                       image_name,
                                       image_desc)
                            )
                from item_image_tl
               where item in (select item
                                from svc_item_search_temp
                               where process_id = I_process_id);

END POPULATE_IIM_TL;
---------------------------------------------------------------------------------------
PROCEDURE populate_ISE(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ISE_sheet
    )
  SELECT s9t_row(s9t_cells( NULL ,ITEM ,SEASON_ID ,PHASE_ID ,ITEM_SEASON_SEQ_NO ,DIFF_ID ))
  FROM ITEM_SEASONS
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_ISE;
---------------------------------------------------------------------------------------
PROCEDURE init_s9t(
    O_file_id IN OUT NUMBER)
IS
  l_file s9t_file;
  l_file_name s9t_folder.file_name%type;
BEGIN
  l_file          := NEW s9t_file();
  O_file_id       := s9t_folder_seq.nextval;
  l_file.file_id      := O_file_id;
  l_file_name         := ITEM_INDUCT_SQL.file_name;
  l_file.file_name    := l_file_name;
  l_file.template_key := template_key;
  SELECT get_user_lang INTO l_file.user_lang FROM dual;
  --l_file.user_lang    := 'EN';
  l_file.add_sheet(ICD_sheet);
  l_file.sheets(l_file.get_sheet_index(ICD_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'SUPPLIER' ,'ORIGIN_COUNTRY_ID' ,'DELIVERY_COUNTRY_ID' ,'COND_TYPE' ,'COND_VALUE' ,'APPLIED_ON' ,'COMP_RATE' ,'CALCULATION_BASIS' ,'RECOVERABLE_AMOUNT' ,'MODIFIED_TAXABLE_BASE' );
  l_file.add_sheet(ICH_sheet);
  l_file.sheets(l_file.get_sheet_index(ICH_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'SUPPLIER' ,'ORIGIN_COUNTRY_ID' ,'DELIVERY_COUNTRY_ID' ,'PRIM_DLVY_CTRY_IND' ,'NIC_STATIC_IND' ,'BASE_COST' ,'NEGOTIATED_ITEM_COST' ,'EXTENDED_BASE_COST' ,'INCLUSIVE_COST' );
  l_file.add_sheet(IC_sheet);
  l_file.sheets(l_file.get_sheet_index(IC_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'COUNTRY_ID' );
  l_file.add_sheet(IM_sheet);
  l_file.sheets(l_file.get_sheet_index(IM_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'ITEM_NUMBER_TYPE' ,'FORMAT_ID' ,'PREFIX' ,'ITEM_PARENT' ,'ITEM_GRANDPARENT' ,'PACK_IND' ,'ITEM_LEVEL' ,'TRAN_LEVEL' ,'ITEM_AGGREGATE_IND' ,'DIFF_1' ,'DIFF_1_AGGREGATE_IND' ,'DIFF_2' ,'DIFF_2_AGGREGATE_IND' ,'DIFF_3' ,'DIFF_3_AGGREGATE_IND' ,'DIFF_4' ,'DIFF_4_AGGREGATE_IND' ,'DEPT' ,'CLASS' ,'SUBCLASS' ,'STATUS' ,'ITEM_DESC' ,'ITEM_DESC_SECONDARY' ,'SHORT_DESC' ,'PRIMARY_REF_ITEM_IND' ,'COST_ZONE_GROUP_ID' ,'STANDARD_UOM' ,'UOM_CONV_FACTOR' ,'PACKAGE_SIZE' ,'PACKAGE_UOM' ,'MERCHANDISE_IND' ,'STORE_ORD_MULT' ,'FORECAST_IND' ,'MFG_REC_RETAIL' ,'RETAIL_LABEL_TYPE' ,'RETAIL_LABEL_VALUE' ,'HANDLING_TEMP' ,'HANDLING_SENSITIVITY' ,'CATCH_WEIGHT_IND' ,'WASTE_TYPE' ,'WASTE_PCT' ,'DEFAULT_WASTE_PCT' ,'CONST_DIMEN_IND' ,'SIMPLE_PACK_IND' ,'CONTAINS_INNER_IND' ,'SELLABLE_IND' ,'ORDERABLE_IND' ,'PACK_TYPE' ,'ORDER_AS_TYPE' ,'COMMENTS' ,'ITEM_SERVICE_LEVEL' ,
  'GIFT_WRAP_IND' ,'SHIP_ALONE_IND' ,'ITEM_XFORM_IND' ,'INVENTORY_IND' ,'ORDER_TYPE' ,'SALE_TYPE' ,'DEPOSIT_ITEM_TYPE' ,'CONTAINER_ITEM' ,'DEPOSIT_IN_PRICE_PER_UOM' ,'AIP_CASE_TYPE' ,'PERISHABLE_IND' ,'NOTIONAL_PACK_IND' ,'SOH_INQUIRY_AT_PACK_IND' ,'PRODUCT_CLASSIFICATION' ,'BRAND_NAME','PRE_RESERVED_IND','LAST_UPDATE_ID','NEXT_UPD_ID','ORIG_REF_NO' );
  l_file.add_sheet(IMTL_sheet);
  l_file.sheets(l_file.get_sheet_index(IMTL_sheet)).column_headers := s9t_cells(action_column, 'LANG', 'ITEM', 'ITEM_DESC', 'ITEM_DESC_SECONDARY', 'SHORT_DESC');
  l_file.add_sheet(IS_sheet);
  l_file.sheets(l_file.get_sheet_index(IS_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'SUPPLIER' ,'PRIMARY_SUPP_IND' ,'VPN' ,'SUPP_LABEL' ,'CONSIGNMENT_RATE' ,'SUPP_DIFF_1' ,'SUPP_DIFF_2' ,'SUPP_DIFF_3' ,'SUPP_DIFF_4' ,'PALLET_NAME' ,'CASE_NAME' ,'INNER_NAME' ,'SUPP_DISCONTINUE_DATE' ,'DIRECT_SHIP_IND' ,'CONCESSION_RATE' ,'PRIMARY_CASE_SIZE' );
  l_file.add_sheet(ISTL_sheet);
  l_file.sheets(l_file.get_sheet_index(ISTL_sheet)).column_headers := s9t_cells(action_column, 'LANG', 'ITEM', 'SUPPLIER', 'SUPP_LABEL', 'SUPP_DIFF_1', 'SUPP_DIFF_2', 'SUPP_DIFF_3', 'SUPP_DIFF_4');
  l_file.add_sheet(ISC_sheet);
  l_file.sheets(l_file.get_sheet_index(ISC_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'SUPPLIER' ,'ORIGIN_COUNTRY_ID' ,'UNIT_COST' ,'LEAD_TIME' ,'PICKUP_LEAD_TIME' ,'SUPP_PACK_SIZE' ,'INNER_PACK_SIZE' ,'ROUND_LVL' ,'ROUND_TO_INNER_PCT' ,'ROUND_TO_CASE_PCT' ,'ROUND_TO_LAYER_PCT' ,'ROUND_TO_PALLET_PCT' ,'MIN_ORDER_QTY' ,'MAX_ORDER_QTY' ,'PACKING_METHOD' ,'PRIMARY_SUPP_IND' ,'PRIMARY_COUNTRY_IND' ,'DEFAULT_UOP' ,'TI' ,'HI' ,'SUPP_HIER_LVL_1' ,'SUPP_HIER_LVL_2' ,'SUPP_HIER_LVL_3' ,'COST_UOM' ,'TOLERANCE_TYPE' ,'MAX_TOLERANCE' ,'MIN_TOLERANCE');
  l_file.add_sheet(ISCL_sheet);
  l_file.sheets(l_file.get_sheet_index(ISCL_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'SUPPLIER' ,'ORIGIN_COUNTRY_ID' ,'LOC','LOC_TYPE','PRIMARY_LOC_IND','UNIT_COST' ,'NEGOTIATED_ITEM_COST','ROUND_LVL' ,'ROUND_TO_INNER_PCT' ,'ROUND_TO_CASE_PCT' ,'ROUND_TO_LAYER_PCT' ,'ROUND_TO_PALLET_PCT' ,'SUPP_HIER_LVL_1' ,'SUPP_HIER_LVL_2' ,'SUPP_HIER_LVL_3' ,'PICKUP_LEAD_TIME');
  l_file.add_sheet(ISCD_sheet);
  l_file.sheets(l_file.get_sheet_index(ISCD_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'SUPPLIER' ,'ORIGIN_COUNTRY' ,'DIM_OBJECT' ,'PRESENTATION_METHOD' ,'LENGTH' ,'WIDTH' ,'HEIGHT' ,'LWH_UOM' ,'WEIGHT' ,'NET_WEIGHT' ,'WEIGHT_UOM' ,'LIQUID_VOLUME' ,'LIQUID_VOLUME_UOM' ,'STAT_CUBE' ,'TARE_WEIGHT' ,'TARE_TYPE' );
  l_file.add_sheet(ISMC_sheet);
  l_file.sheets(l_file.get_sheet_index(ISMC_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'SUPPLIER' ,'MANU_COUNTRY_ID' ,'PRIMARY_MANU_CTRY_IND' );
  l_file.add_sheet(ISU_sheet);
  l_file.sheets(l_file.get_sheet_index(ISU_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'SUPPLIER' ,'UOM' ,'VALUE' );
  l_file.add_sheet(IXD_sheet);
  l_file.sheets(l_file.get_sheet_index(IXD_sheet)).column_headers := s9t_cells( action_column ,'HEAD_ITEM' ,'DETAIL_ITEM' ,'ITEM_QUANTITY_PCT' ,'YIELD_FROM_HEAD_ITEM_PCT' );
  l_file.add_sheet(IXH_sheet);
  l_file.sheets(l_file.get_sheet_index(IXH_sheet)).column_headers := s9t_cells( action_column ,'ITEM_XFORM_HEAD_ID' ,'HEAD_ITEM' ,'ITEM_XFORM_TYPE' ,'ITEM_XFORM_DESC' ,'PRODUCTION_LOSS_PCT' ,'COMMENTS_DESC' );
  l_file.add_sheet(IXHTL_sheet);
  l_file.sheets(l_file.get_sheet_index(IXHTL_sheet)).column_headers := s9t_cells(action_column, 'LANG', 'HEAD_ITEM', 'ITEM_XFORM_HEAD_ID', 'ITEM_XFORM_DESC');
  l_file.add_sheet(PI_sheet);
  l_file.sheets(l_file.get_sheet_index(PI_sheet)).column_headers := s9t_cells( action_column ,'PACK_NO' ,'SEQ_NO' ,'ITEM' ,'ITEM_PARENT' ,'PACK_TMPL_ID' ,'PACK_QTY' );
  l_file.add_sheet(IZP_sheet);
  l_file.sheets(l_file.get_sheet_index(IZP_sheet)).column_headers := s9t_cells( action_column ,'ITEM_ZONE_PRICE_ID' ,'ITEM' ,'ZONE_ID' ,'STANDARD_RETAIL' ,'STANDARD_RETAIL_CURRENCY' ,'STANDARD_UOM' ,'SELLING_RETAIL' ,'SELLING_UOM' ,'MULTI_UNITS' ,'MULTI_UNIT_RETAIL' ,'MULTI_UNIT_RETAIL_CURRENCY' ,'MULTI_SELLING_UOM' ,'LOCK_VERSION' );
  l_file.add_sheet(UID_sheet);
  l_file.sheets(l_file.get_sheet_index(UID_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'UDA_ID' ,'UDA_DATE' );
  l_file.add_sheet(UIF_sheet);
  l_file.sheets(l_file.get_sheet_index(UIF_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'UDA_ID' ,'UDA_TEXT' );
  l_file.add_sheet(UIL_sheet);
  l_file.sheets(l_file.get_sheet_index(UIL_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'UDA_ID' ,'UDA_VALUE' );
  l_file.add_sheet(VI_sheet);
  l_file.sheets(l_file.get_sheet_index(VI_sheet)).column_headers := s9t_cells( action_column ,'ITEM' ,'VAT_REGION' ,'ACTIVE_DATE' ,'VAT_TYPE' ,'VAT_CODE' ,'REVERSE_VAT_IND');
  l_file.add_sheet(CSH_sheet);
  l_file.sheets(l_file.get_sheet_index(CSH_sheet)).column_headers := s9t_cells( action_column ,'COST_CHANGE','COST_CHANGE_DESC','REASON','ACTIVE_DATE','STATUS','COST_CHANGE_ORIGIN','APPROVAL_DATE','APPROVAL_ID' );
  l_file.add_sheet(CSDL_sheet);
  l_file.sheets(l_file.get_sheet_index(CSDL_sheet)).column_headers := s9t_cells( action_column ,'COST_CHANGE','SUPPLIER','ORIGIN_COUNTRY_ID','ITEM','LOC_TYPE','LOC','COST_CHANGE_TYPE','COST_CHANGE_VALUE','RECALC_ORD_IND','DELIVERY_COUNTRY_ID' );
  l_file.add_sheet(CSD_sheet);
  l_file.sheets(l_file.get_sheet_index(CSD_sheet)).column_headers := s9t_cells( action_column ,'COST_CHANGE','SUPPLIER','ORIGIN_COUNTRY_ID','ITEM','COST_CHANGE_TYPE','COST_CHANGE_VALUE','RECALC_ORD_IND','DELIVERY_COUNTRY_ID' );
  l_file.add_sheet(IIM_sheet);
  l_file.sheets(l_file.get_sheet_index(IIM_sheet)).column_headers := s9t_cells(action_column, 'ITEM', 'IMAGE_NAME', 'IMAGE_ADDR', 'IMAGE_DESC', 'IMAGE_TYPE', 'PRIMARY_IND', 'DISPLAY_PRIORITY');
  l_file.add_sheet(IIMTL_sheet);
  l_file.sheets(l_file.get_sheet_index(IIMTL_sheet)).column_headers := s9t_cells(action_column, 'LANG', 'ITEM', 'IMAGE_NAME', 'IMAGE_DESC');
  l_file.add_sheet(ISE_sheet);
  l_file.sheets(l_file.get_sheet_index(ISE_sheet)).column_headers := s9t_cells(action_column, 'ITEM', 'SEASON_ID', 'PHASE_ID', 'ITEM_SEASON_SEQ_NO', 'DIFF_ID');
  --l_file.translate_to_user_lang;
  s9t_pkg.save_obj(l_file);
END init_s9t;
FUNCTION create_s9t(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    O_file_id       IN OUT s9t_folder.file_id%type,
    I_process_id    IN NUMBER,
    I_template_only_ind IN CHAR DEFAULT 'N')
  RETURN BOOLEAN
IS
  l_file s9t_file;
  L_program VARCHAR2(255):='ITEM_INDUCT_SQL.create_s9t';
BEGIN
  init_s9t(O_file_id);

  IF I_template_only_ind = 'N' THEN
    IF is_cost_change_template(template_key) THEN
      populate_csh(O_file_id,I_process_id);
      populate_csd(O_file_id,I_process_id);
      populate_csdl(O_file_id,I_process_id);
    ELSE
      populate_ICD(O_file_id,I_process_id);
      populate_ICH(O_file_id,I_process_id);
      populate_IC(O_file_id,I_process_id);
      populate_IM(O_file_id,I_process_id);
      POPULATE_IM_TL(O_file_id,
                     I_process_id);
      populate_IS(O_file_id,I_process_id);
      POPULATE_IS_TL(O_file_id,
                     I_process_id);
      populate_ISC(O_file_id,I_process_id);
      populate_ISCL(O_file_id,I_process_id);
      populate_ISCD(O_file_id,I_process_id);
      populate_ISMC(O_file_id,I_process_id);
      populate_ISU(O_file_id,I_process_id);
      populate_IXD(O_file_id,I_process_id);
      populate_IXH(O_file_id,I_process_id);
      POPULATE_IXH_TL(O_file_id,
                      I_process_id);
      populate_PI(O_file_id,I_process_id);
      populate_IZP(O_file_id,I_process_id);
      populate_UID(O_file_id,I_process_id);
      populate_UIF(O_file_id,I_process_id);
      populate_UIL(O_file_id,I_process_id);
      populate_VI(O_file_id,I_process_id);
      POPULATE_IIM(O_file_id,
                   I_process_id);
      POPULATE_IIM_TL(O_file_id,
                      I_process_id);
      populate_ISE(O_file_id,I_process_id);
    END IF;
    COMMIT;
  END IF;
  if is_cost_change_template(template_key) THEN
     if CORESVC_CFLEX.ADD_COST_CHNG_CFLEX_SHEETS(O_error_message,
                                                 O_file_id,
                                                 I_process_id,
                                                 template_key,
                                                 I_template_only_ind) = FALSE then
        return FALSE;
     end if;
  else
     IF CORESVC_CFLEX.ADD_ITEM_CFLEX_SHEETS(O_error_message,
                                            O_file_id,
                                            I_process_id,
                                            template_key,
                                            I_template_only_ind) = FALSE THEN
        return FALSE;
     end if;
  end if;
  IF item_induct_sql.populate_lists(O_error_message,O_file_id,template_key)=false THEN
    RETURN false;
  END IF;
  s9t_pkg.translate_to_user_lang(O_file_id);
  -- Apply template
  s9t_pkg.apply_template(O_file_id,template_key);
  l_file:=s9t_file(O_file_id);
  if s9t_pkg.code2desc(O_error_message,
                       'IS9T',
                                     l_file)=FALSE then
     return FALSE;
  end if;
  s9t_pkg.save_obj(l_file);
  s9t_pkg.update_ods(l_file);
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END create_s9t;
-------------------------------------------------------------------------------------------------------
  FUNCTION get_new_action(
      I_old_action IN VARCHAR2,
      I_new_action IN VARCHAR2)
    RETURN VARCHAR2
  IS
    O_new_action svc_item_master.action%type;
  BEGIN
    CASE
    WHEN I_old_action = coresvc_item.action_new AND I_new_action = coresvc_item.action_del THEN
      O_new_action   := coresvc_item.action_del ;
    WHEN I_old_action = coresvc_item.action_new AND I_new_action = coresvc_item.action_mod THEN
      O_new_action   := coresvc_item.action_new ;
    WHEN I_old_action = coresvc_item.action_del AND I_new_action = coresvc_item.action_del THEN
      O_new_action   := coresvc_item.action_del ;
    WHEN I_old_action = coresvc_item.action_del AND I_new_action = coresvc_item.action_mod THEN
      O_new_action   := coresvc_item.action_del ;
    WHEN I_old_action = coresvc_item.action_mod AND I_new_action = coresvc_item.action_del THEN
      O_new_action   := coresvc_item.action_del ;
    WHEN I_old_action = coresvc_item.action_mod AND I_new_action = coresvc_item.action_mod THEN
      O_new_action   := coresvc_item.action_mod ;
    WHEN I_old_action = coresvc_item.action_new AND I_new_action is NULL THEN
      O_new_action   := coresvc_item.action_new ;
    WHEN I_old_action = coresvc_item.action_mod AND I_new_action is NULL THEN
      O_new_action   := coresvc_item.action_mod ;
    WHEN I_old_action = coresvc_item.action_del AND I_new_action is NULL THEN
      O_new_action   := coresvc_item.action_del ;
    ELSE
      O_new_action := I_new_action;
    END CASE;
    RETURN O_new_action;
  END get_new_action;
-------------------------------------------------------------------------------------------------------
FUNCTION get_prc_dest(
    I_process_id svc_process_tracker.process_id%type)
  RETURN VARCHAR2
IS
  CURSOR c_get_dest
  IS
    SELECT process_destination
    FROM svc_process_tracker
    WHERE process_id = I_process_id;
  O_dest svc_process_tracker.process_destination%type;
BEGIN
  OPEN c_get_dest;
  FETCH c_get_dest INTO O_dest;
  CLOSE c_get_dest;
  RETURN O_dest;
END get_prc_dest;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_ICD(
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_COST_DETAIL.process_id%type )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_COST_DETAIL%rowtype;
  l_temp_rec SVC_ITEM_COST_DETAIL%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_COST_DETAIL.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_COST_DETAIL%rowtype;
  L_sheet_name  S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT MODIFIED_TAXABLE_BASE_mi,
      RECOVERABLE_AMOUNT_mi,
      CALCULATION_BASIS_mi,
      COMP_RATE_mi,
      APPLIED_ON_mi,
      COND_VALUE_mi,
      COND_TYPE_mi,
      DELIVERY_COUNTRY_ID_mi,
      ORIGIN_COUNTRY_ID_mi,
      SUPPLIER_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_COST_DETAIL'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'MODIFIED_TAXABLE_BASE' AS MODIFIED_TAXABLE_BASE, 'RECOVERABLE_AMOUNT' AS RECOVERABLE_AMOUNT, 'CALCULATION_BASIS' AS CALCULATION_BASIS, 'COMP_RATE' AS COMP_RATE, 'APPLIED_ON' AS APPLIED_ON, 'COND_VALUE' AS COND_VALUE, 'COND_TYPE' AS COND_TYPE, 'DELIVERY_COUNTRY_ID' AS DELIVERY_COUNTRY_ID, 'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item, supplier, origin country id, delivery country id, cond type';
  L_table    VARCHAR2(30)  := 'SVC_ITEM_COST_DETAIL';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT MODIFIED_TAXABLE_BASE_dv,
    RECOVERABLE_AMOUNT_dv,
    CALCULATION_BASIS_dv,
    COMP_RATE_dv,
    APPLIED_ON_dv,
    COND_VALUE_dv,
    COND_TYPE_dv,
    DELIVERY_COUNTRY_ID_dv,
    ORIGIN_COUNTRY_ID_dv,
    SUPPLIER_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_COST_DETAIL'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'MODIFIED_TAXABLE_BASE' AS MODIFIED_TAXABLE_BASE, 'RECOVERABLE_AMOUNT' AS RECOVERABLE_AMOUNT, 'CALCULATION_BASIS' AS CALCULATION_BASIS, 'COMP_RATE' AS COMP_RATE, 'APPLIED_ON' AS APPLIED_ON, 'COND_VALUE' AS COND_VALUE, 'COND_TYPE' AS COND_TYPE, 'DELIVERY_COUNTRY_ID' AS DELIVERY_COUNTRY_ID, 'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.MODIFIED_TAXABLE_BASE := rec.MODIFIED_TAXABLE_BASE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_DETAIL',NULL,'MODIFIED_TAXABLE_BASE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.RECOVERABLE_AMOUNT := rec.RECOVERABLE_AMOUNT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_DETAIL',NULL,'RECOVERABLE_AMOUNT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.CALCULATION_BASIS := rec.CALCULATION_BASIS_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_DETAIL',NULL,'CALCULATION_BASIS','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COMP_RATE := rec.COMP_RATE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_DETAIL',NULL,'COMP_RATE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.APPLIED_ON := rec.APPLIED_ON_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_DETAIL',NULL,'APPLIED_ON','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COND_VALUE := rec.COND_VALUE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_DETAIL',NULL,'COND_VALUE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COND_TYPE := rec.COND_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_DETAIL',NULL,'COND_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.DELIVERY_COUNTRY_ID := rec.DELIVERY_COUNTRY_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_DETAIL',NULL,'DELIVERY_COUNTRY_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_DETAIL',NULL,'ORIGIN_COUNTRY_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPPLIER := rec.SUPPLIER_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_DETAIL',NULL,'SUPPLIER','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_DETAIL',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(IC_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(ICD_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(ICD$Action)      AS Action,
    r.get_cell(ICD$MODIFIED_TAXABLE_BASE) AS MODIFIED_TAXABLE_BASE,
    r.get_cell(ICD$RECOVERABLE_AMOUNT)    AS RECOVERABLE_AMOUNT,
    r.get_cell(ICD$CALCULATION_BASIS)     AS CALCULATION_BASIS,
    r.get_cell(ICD$COMP_RATE)         AS COMP_RATE,
    r.get_cell(ICD$APPLIED_ON)        AS APPLIED_ON,
    r.get_cell(ICD$COND_VALUE)        AS COND_VALUE,
    r.get_cell(ICD$COND_TYPE)         AS COND_TYPE,
    r.get_cell(ICD$DELIVERY_COUNTRY_ID)   AS DELIVERY_COUNTRY_ID,
    r.get_cell(ICD$ORIGIN_COUNTRY_ID)     AS ORIGIN_COUNTRY_ID,
    r.get_cell(ICD$SUPPLIER)          AS SUPPLIER,
    r.get_cell(ICD$ITEM)          AS ITEM,
    r.get_row_seq()           AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.MODIFIED_TAXABLE_BASE := rec.MODIFIED_TAXABLE_BASE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,'MODIFIED_TAXABLE_BASE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.RECOVERABLE_AMOUNT := rec.RECOVERABLE_AMOUNT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,'RECOVERABLE_AMOUNT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.CALCULATION_BASIS := rec.CALCULATION_BASIS;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,'CALCULATION_BASIS',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COMP_RATE := rec.COMP_RATE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,'COMP_RATE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.APPLIED_ON := rec.APPLIED_ON;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,'APPLIED_ON',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COND_VALUE := rec.COND_VALUE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,'COND_VALUE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COND_TYPE := rec.COND_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,'COND_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DELIVERY_COUNTRY_ID := rec.DELIVERY_COUNTRY_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,'DELIVERY_COUNTRY_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,'ORIGIN_COUNTRY_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPPLIER := rec.SUPPLIER;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,'SUPPLIER',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_ITEM_COST_DETAIL%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_ITEM_COST_DETAIL
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND ORIGIN_COUNTRY_ID = l_temp_rec.ORIGIN_COUNTRY_ID
         AND DELIVERY_COUNTRY_ID = l_temp_rec.DELIVERY_COUNTRY_ID
         AND COND_TYPE = l_temp_rec.COND_TYPE
;
    l_rms_rec ITEM_COST_DETAIL%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM ITEM_COST_DETAIL
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND ORIGIN_COUNTRY_ID = l_temp_rec.ORIGIN_COUNTRY_ID
         AND DELIVERY_COUNTRY_ID = l_temp_rec.DELIVERY_COUNTRY_ID
         AND COND_TYPE = l_temp_rec.COND_TYPE
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_ITEM_COST_DETAIL
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.COND_VALUE_mi <> 'Y' THEN
        l_temp_rec.COND_VALUE   := l_stg_rec.COND_VALUE;
      END IF;
      IF l_mi_rec.APPLIED_ON_mi <> 'Y' THEN
        l_temp_rec.APPLIED_ON   := l_stg_rec.APPLIED_ON;
      END IF;
      IF l_mi_rec.COMP_RATE_mi <> 'Y' THEN
        l_temp_rec.COMP_RATE   := l_stg_rec.COMP_RATE;
      END IF;
      IF l_mi_rec.CALCULATION_BASIS_mi <> 'Y' THEN
        l_temp_rec.CALCULATION_BASIS   := l_stg_rec.CALCULATION_BASIS;
      END IF;
      IF l_mi_rec.RECOVERABLE_AMOUNT_mi <> 'Y' THEN
        l_temp_rec.RECOVERABLE_AMOUNT   := l_stg_rec.RECOVERABLE_AMOUNT;
      END IF;
      IF l_mi_rec.MODIFIED_TAXABLE_BASE_mi <> 'Y' THEN
        l_temp_rec.MODIFIED_TAXABLE_BASE   := l_stg_rec.MODIFIED_TAXABLE_BASE;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.COND_VALUE_mi <> 'Y' THEN
        l_temp_rec.COND_VALUE   := l_rms_rec.COND_VALUE;
      END IF;
      IF l_mi_rec.APPLIED_ON_mi <> 'Y' THEN
        l_temp_rec.APPLIED_ON   := l_rms_rec.APPLIED_ON;
      END IF;
      IF l_mi_rec.COMP_RATE_mi <> 'Y' THEN
        l_temp_rec.COMP_RATE   := l_rms_rec.COMP_RATE;
      END IF;
      IF l_mi_rec.CALCULATION_BASIS_mi <> 'Y' THEN
        l_temp_rec.CALCULATION_BASIS   := l_rms_rec.CALCULATION_BASIS;
      END IF;
      IF l_mi_rec.RECOVERABLE_AMOUNT_mi <> 'Y' THEN
        l_temp_rec.RECOVERABLE_AMOUNT   := l_rms_rec.RECOVERABLE_AMOUNT;
      END IF;
      IF l_mi_rec.MODIFIED_TAXABLE_BASE_mi <> 'Y' THEN
        l_temp_rec.MODIFIED_TAXABLE_BASE   := l_rms_rec.MODIFIED_TAXABLE_BASE;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,ICD_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.SUPPLIER := NVL( l_temp_rec.SUPPLIER,l_default_rec.SUPPLIER);
      l_temp_rec.ORIGIN_COUNTRY_ID := NVL( l_temp_rec.ORIGIN_COUNTRY_ID,l_default_rec.ORIGIN_COUNTRY_ID);
      l_temp_rec.DELIVERY_COUNTRY_ID := NVL( l_temp_rec.DELIVERY_COUNTRY_ID,l_default_rec.DELIVERY_COUNTRY_ID);
      l_temp_rec.COND_TYPE := NVL( l_temp_rec.COND_TYPE,l_default_rec.COND_TYPE);
      l_temp_rec.COND_VALUE := NVL( l_temp_rec.COND_VALUE,l_default_rec.COND_VALUE);
      l_temp_rec.APPLIED_ON := NVL( l_temp_rec.APPLIED_ON,l_default_rec.APPLIED_ON);
      l_temp_rec.COMP_RATE := NVL( l_temp_rec.COMP_RATE,l_default_rec.COMP_RATE);
      l_temp_rec.CALCULATION_BASIS := NVL( l_temp_rec.CALCULATION_BASIS,l_default_rec.CALCULATION_BASIS);
      l_temp_rec.RECOVERABLE_AMOUNT := NVL( l_temp_rec.RECOVERABLE_AMOUNT,l_default_rec.RECOVERABLE_AMOUNT);
      l_temp_rec.MODIFIED_TAXABLE_BASE := NVL( l_temp_rec.MODIFIED_TAXABLE_BASE,l_default_rec.MODIFIED_TAXABLE_BASE);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.SUPPLIER IS NOT NULL
        AND l_temp_rec.ORIGIN_COUNTRY_ID IS NOT NULL
        AND l_temp_rec.DELIVERY_COUNTRY_ID IS NOT NULL
        AND l_temp_rec.COND_TYPE IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,ICD_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_COST_DETAIL VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,ICD_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_COST_DETAIL
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND SUPPLIER = svc_upd_col(i).SUPPLIER
     AND ORIGIN_COUNTRY_ID = svc_upd_col(i).ORIGIN_COUNTRY_ID
     AND DELIVERY_COUNTRY_ID = svc_upd_col(i).DELIVERY_COUNTRY_ID
     AND COND_TYPE = svc_upd_col(i).COND_TYPE
;
END process_s9t_ICD;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_ICH
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_COST_HEAD.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_COST_HEAD%rowtype;
  l_temp_rec SVC_ITEM_COST_HEAD%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_COST_HEAD.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_COST_HEAD%rowtype;
  L_sheet_name  S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT INCLUSIVE_COST_mi,
      EXTENDED_BASE_COST_mi,
      NEGOTIATED_ITEM_COST_mi,
      BASE_COST_mi,
      NIC_STATIC_IND_mi,
      PRIM_DLVY_CTRY_IND_mi,
      DELIVERY_COUNTRY_ID_mi,
      ORIGIN_COUNTRY_ID_mi,
      SUPPLIER_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_COST_HEAD'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'INCLUSIVE_COST' AS INCLUSIVE_COST, 'EXTENDED_BASE_COST' AS EXTENDED_BASE_COST, 'NEGOTIATED_ITEM_COST' AS NEGOTIATED_ITEM_COST, 'BASE_COST' AS BASE_COST, 'NIC_STATIC_IND' AS NIC_STATIC_IND, 'PRIM_DLVY_CTRY_IND' AS PRIM_DLVY_CTRY_IND, 'DELIVERY_COUNTRY_ID' AS DELIVERY_COUNTRY_ID, 'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec   c_mandatory_ind%rowtype;
  dml_errors     EXCEPTION;
  L_pk_columns   VARCHAR2(255) := 'item, supplier, origin country id, delivery country id';
  L_table    VARCHAR2(30) := 'SVC_ITEM_COST_HEAD';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT INCLUSIVE_COST_dv,
    EXTENDED_BASE_COST_dv,
    NEGOTIATED_ITEM_COST_dv,
    BASE_COST_dv,
    NIC_STATIC_IND_dv,
    PRIM_DLVY_CTRY_IND_dv,
    DELIVERY_COUNTRY_ID_dv,
    ORIGIN_COUNTRY_ID_dv,
    SUPPLIER_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_COST_HEAD'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'INCLUSIVE_COST' AS INCLUSIVE_COST, 'EXTENDED_BASE_COST' AS EXTENDED_BASE_COST, 'NEGOTIATED_ITEM_COST' AS NEGOTIATED_ITEM_COST, 'BASE_COST' AS BASE_COST, 'NIC_STATIC_IND' AS NIC_STATIC_IND, 'PRIM_DLVY_CTRY_IND' AS PRIM_DLVY_CTRY_IND, 'DELIVERY_COUNTRY_ID' AS DELIVERY_COUNTRY_ID, 'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.INCLUSIVE_COST := rec.INCLUSIVE_COST_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_HEAD',NULL,'INCLUSIVE_COST','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.EXTENDED_BASE_COST := rec.EXTENDED_BASE_COST_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_HEAD',NULL,'EXTENDED_BASE_COST','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.NEGOTIATED_ITEM_COST := rec.NEGOTIATED_ITEM_COST_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_HEAD',NULL,'NEGOTIATED_ITEM_COST','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.BASE_COST := rec.BASE_COST_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_HEAD',NULL,'BASE_COST','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.NIC_STATIC_IND := rec.NIC_STATIC_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_HEAD',NULL,'NIC_STATIC_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PRIM_DLVY_CTRY_IND := rec.PRIM_DLVY_CTRY_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_HEAD',NULL,'PRIM_DLVY_CTRY_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.DELIVERY_COUNTRY_ID := rec.DELIVERY_COUNTRY_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_HEAD',NULL,'DELIVERY_COUNTRY_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_HEAD',NULL,'ORIGIN_COUNTRY_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPPLIER := rec.SUPPLIER_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_HEAD',NULL,'SUPPLIER','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COST_HEAD',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(IC_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(ICH_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(ICH$Action)     AS Action,
    r.get_cell(ICH$INCLUSIVE_COST)   AS INCLUSIVE_COST,
    r.get_cell(ICH$EXTENDED_BASE_COST)   AS EXTENDED_BASE_COST,
    r.get_cell(ICH$NEGOTIATED_ITEM_COST) AS NEGOTIATED_ITEM_COST,
    r.get_cell(ICH$BASE_COST)        AS BASE_COST,
    r.get_cell(ICH$NIC_STATIC_IND)   AS NIC_STATIC_IND,
    r.get_cell(ICH$PRIM_DLVY_CTRY_IND)   AS PRIM_DLVY_CTRY_IND,
    r.get_cell(ICH$DELIVERY_COUNTRY_ID)  AS DELIVERY_COUNTRY_ID,
    r.get_cell(ICH$ORIGIN_COUNTRY_ID)    AS ORIGIN_COUNTRY_ID,
    r.get_cell(ICH$SUPPLIER)         AS SUPPLIER,
    r.get_cell(ICH$ITEM)         AS ITEM,
    r.get_row_seq()          AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICH_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.INCLUSIVE_COST := rec.INCLUSIVE_COST;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICH_sheet,rec.row_seq,'INCLUSIVE_COST',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.EXTENDED_BASE_COST := rec.EXTENDED_BASE_COST;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICH_sheet,rec.row_seq,'EXTENDED_BASE_COST',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.NEGOTIATED_ITEM_COST := rec.NEGOTIATED_ITEM_COST;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICH_sheet,rec.row_seq,'NEGOTIATED_ITEM_COST',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.BASE_COST := rec.BASE_COST;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICH_sheet,rec.row_seq,'BASE_COST',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.NIC_STATIC_IND := rec.NIC_STATIC_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICH_sheet,rec.row_seq,'NIC_STATIC_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PRIM_DLVY_CTRY_IND := rec.PRIM_DLVY_CTRY_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICH_sheet,rec.row_seq,'PRIM_DLVY_CTRY_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DELIVERY_COUNTRY_ID := rec.DELIVERY_COUNTRY_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICH_sheet,rec.row_seq,'DELIVERY_COUNTRY_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICH_sheet,rec.row_seq,'ORIGIN_COUNTRY_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPPLIER := rec.SUPPLIER;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICH_sheet,rec.row_seq,'SUPPLIER',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ICH_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_ITEM_COST_HEAD%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_ITEM_COST_HEAD
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND ORIGIN_COUNTRY_ID = l_temp_rec.ORIGIN_COUNTRY_ID
         AND DELIVERY_COUNTRY_ID = l_temp_rec.DELIVERY_COUNTRY_ID
;
    l_rms_rec ITEM_COST_HEAD%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM ITEM_COST_HEAD
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND ORIGIN_COUNTRY_ID = l_temp_rec.ORIGIN_COUNTRY_ID
         AND DELIVERY_COUNTRY_ID = l_temp_rec.DELIVERY_COUNTRY_ID
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_ITEM_COST_HEAD
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.PRIM_DLVY_CTRY_IND_mi <> 'Y' THEN
        l_temp_rec.PRIM_DLVY_CTRY_IND   := l_stg_rec.PRIM_DLVY_CTRY_IND;
      END IF;
      IF l_mi_rec.NIC_STATIC_IND_mi <> 'Y' THEN
        l_temp_rec.NIC_STATIC_IND   := l_stg_rec.NIC_STATIC_IND;
      END IF;
      IF l_mi_rec.BASE_COST_mi <> 'Y' THEN
        l_temp_rec.BASE_COST   := l_stg_rec.BASE_COST;
      END IF;
      IF l_mi_rec.NEGOTIATED_ITEM_COST_mi <> 'Y' THEN
        l_temp_rec.NEGOTIATED_ITEM_COST   := l_stg_rec.NEGOTIATED_ITEM_COST;
      END IF;
      IF l_mi_rec.EXTENDED_BASE_COST_mi <> 'Y' THEN
        l_temp_rec.EXTENDED_BASE_COST   := l_stg_rec.EXTENDED_BASE_COST;
      END IF;
      IF l_mi_rec.INCLUSIVE_COST_mi <> 'Y' THEN
        l_temp_rec.INCLUSIVE_COST   := l_stg_rec.INCLUSIVE_COST;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.PRIM_DLVY_CTRY_IND_mi <> 'Y' THEN
        l_temp_rec.PRIM_DLVY_CTRY_IND   := l_rms_rec.PRIM_DLVY_CTRY_IND;
      END IF;
      IF l_mi_rec.NIC_STATIC_IND_mi <> 'Y' THEN
        l_temp_rec.NIC_STATIC_IND   := l_rms_rec.NIC_STATIC_IND;
      END IF;
      IF l_mi_rec.BASE_COST_mi <> 'Y' THEN
        l_temp_rec.BASE_COST   := l_rms_rec.BASE_COST;
      END IF;
      IF l_mi_rec.NEGOTIATED_ITEM_COST_mi <> 'Y' THEN
        l_temp_rec.NEGOTIATED_ITEM_COST   := l_rms_rec.NEGOTIATED_ITEM_COST;
      END IF;
      IF l_mi_rec.EXTENDED_BASE_COST_mi <> 'Y' THEN
        l_temp_rec.EXTENDED_BASE_COST   := l_rms_rec.EXTENDED_BASE_COST;
      END IF;
      IF l_mi_rec.INCLUSIVE_COST_mi <> 'Y' THEN
        l_temp_rec.INCLUSIVE_COST   := l_rms_rec.INCLUSIVE_COST;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,ICH_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.SUPPLIER := NVL( l_temp_rec.SUPPLIER,l_default_rec.SUPPLIER);
      l_temp_rec.ORIGIN_COUNTRY_ID := NVL( l_temp_rec.ORIGIN_COUNTRY_ID,l_default_rec.ORIGIN_COUNTRY_ID);
      l_temp_rec.DELIVERY_COUNTRY_ID := NVL( l_temp_rec.DELIVERY_COUNTRY_ID,l_default_rec.DELIVERY_COUNTRY_ID);
      l_temp_rec.PRIM_DLVY_CTRY_IND := NVL( l_temp_rec.PRIM_DLVY_CTRY_IND,l_default_rec.PRIM_DLVY_CTRY_IND);
      l_temp_rec.NIC_STATIC_IND := NVL( l_temp_rec.NIC_STATIC_IND,l_default_rec.NIC_STATIC_IND);
      l_temp_rec.BASE_COST := NVL( l_temp_rec.BASE_COST,l_default_rec.BASE_COST);
      l_temp_rec.NEGOTIATED_ITEM_COST := NVL( l_temp_rec.NEGOTIATED_ITEM_COST,l_default_rec.NEGOTIATED_ITEM_COST);
      l_temp_rec.EXTENDED_BASE_COST := NVL( l_temp_rec.EXTENDED_BASE_COST,l_default_rec.EXTENDED_BASE_COST);
      l_temp_rec.INCLUSIVE_COST := NVL( l_temp_rec.INCLUSIVE_COST,l_default_rec.INCLUSIVE_COST);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.SUPPLIER IS NOT NULL
        AND l_temp_rec.ORIGIN_COUNTRY_ID IS NOT NULL
        AND l_temp_rec.DELIVERY_COUNTRY_ID IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,ICH_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_COST_HEAD VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,ICH_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;
  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_COST_HEAD
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND SUPPLIER = svc_upd_col(i).SUPPLIER
     AND ORIGIN_COUNTRY_ID = svc_upd_col(i).ORIGIN_COUNTRY_ID
     AND DELIVERY_COUNTRY_ID = svc_upd_col(i).DELIVERY_COUNTRY_ID
;
END process_s9t_ICH;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_IC
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_COUNTRY.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_COUNTRY%rowtype;
  l_temp_rec SVC_ITEM_COUNTRY%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_COUNTRY.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_COUNTRY%rowtype;
  L_sheet_name  S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT COUNTRY_ID_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_COUNTRY'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'COUNTRY_ID' AS COUNTRY_ID, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item and country id';
  L_table    VARCHAR2(30) := 'SVC_ITEM_COUNTRY';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT COUNTRY_ID_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_COUNTRY'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'COUNTRY_ID' AS COUNTRY_ID, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.COUNTRY_ID := rec.COUNTRY_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COUNTRY',NULL,'COUNTRY_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_COUNTRY',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(IC_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(IC_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(IC$Action) AS Action,
    r.get_cell(IC$COUNTRY_ID)   AS COUNTRY_ID,
    r.get_cell(IC$ITEM)     AS ITEM,
    r.get_row_seq()     AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IC_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COUNTRY_ID := rec.COUNTRY_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IC_sheet,rec.row_seq,'COUNTRY_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IC_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_ITEM_COUNTRY%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_ITEM_COUNTRY
       WHERE ITEM     = l_temp_rec.ITEM
         AND COUNTRY_ID = l_temp_rec.COUNTRY_ID
;
    l_rms_rec ITEM_COUNTRY%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM ITEM_COUNTRY
       WHERE ITEM     = l_temp_rec.ITEM
         AND COUNTRY_ID = l_temp_rec.COUNTRY_ID
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_ITEM_COUNTRY
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,IC_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.COUNTRY_ID := NVL( l_temp_rec.COUNTRY_ID,l_default_rec.COUNTRY_ID);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.COUNTRY_ID IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,IC_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_COUNTRY VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,IC_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;
  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_COUNTRY
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND COUNTRY_ID = svc_upd_col(i).COUNTRY_ID
;
END process_s9t_IC;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_IM
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_MASTER.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_MASTER%rowtype;
  l_temp_rec SVC_ITEM_MASTER%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_MASTER.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_MASTER%rowtype;
  im_default_rec SVC_ITEM_MASTER%rowtype;
  L_sheet_name  S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT PRE_RESERVED_IND_mi,
      BRAND_NAME_mi,
      PRODUCT_CLASSIFICATION_mi,
      SOH_INQUIRY_AT_PACK_IND_mi,
      NOTIONAL_PACK_IND_mi,
      PERISHABLE_IND_mi,
      AIP_CASE_TYPE_mi,
      DEPOSIT_IN_PRICE_PER_UOM_mi,
      CONTAINER_ITEM_mi,
      DEPOSIT_ITEM_TYPE_mi,
      SALE_TYPE_mi,
      ORDER_TYPE_mi,
      INVENTORY_IND_mi,
      ITEM_XFORM_IND_mi,
      SHIP_ALONE_IND_mi,
      GIFT_WRAP_IND_mi,
      ITEM_SERVICE_LEVEL_mi,
      COMMENTS_mi,
      ORDER_AS_TYPE_mi,
      PACK_TYPE_mi,
      ORDERABLE_IND_mi,
      SELLABLE_IND_mi,
      CONTAINS_INNER_IND_mi,
      SIMPLE_PACK_IND_mi,
      CONST_DIMEN_IND_mi,
      DEFAULT_WASTE_PCT_mi,
      WASTE_PCT_mi,
      WASTE_TYPE_mi,
      CATCH_WEIGHT_IND_mi,
      HANDLING_SENSITIVITY_mi,
      HANDLING_TEMP_mi,
      RETAIL_LABEL_VALUE_mi,
      RETAIL_LABEL_TYPE_mi,
      MFG_REC_RETAIL_mi,
      FORECAST_IND_mi,
      STORE_ORD_MULT_mi,
      MERCHANDISE_IND_mi,
      PACKAGE_UOM_mi,
      PACKAGE_SIZE_mi,
      UOM_CONV_FACTOR_mi,
      STANDARD_UOM_mi,
      COST_ZONE_GROUP_ID_mi,
      PRIMARY_REF_ITEM_IND_mi,
      SHORT_DESC_mi,
      ITEM_DESC_SECONDARY_mi,
      ITEM_DESC_mi,
      STATUS_mi,
      SUBCLASS_mi,
      CLASS_mi,
      DEPT_mi,
      DIFF_4_AGGREGATE_IND_mi,
      DIFF_4_mi,
      DIFF_3_AGGREGATE_IND_mi,
      DIFF_3_mi,
      DIFF_2_AGGREGATE_IND_mi,
      DIFF_2_mi,
      DIFF_1_AGGREGATE_IND_mi,
      DIFF_1_mi,
      ITEM_AGGREGATE_IND_mi,
      TRAN_LEVEL_mi,
      ITEM_LEVEL_mi,
      PACK_IND_mi,
      ITEM_GRANDPARENT_mi,
      ITEM_PARENT_mi,
      PREFIX_mi,
      FORMAT_ID_mi,
      ITEM_NUMBER_TYPE_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_MASTER'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'PRE_RESERVED_IND' AS PRE_RESERVED_IND, 'BRAND_NAME' AS BRAND_NAME, 'PRODUCT_CLASSIFICATION' AS PRODUCT_CLASSIFICATION, 'SOH_INQUIRY_AT_PACK_IND' AS SOH_INQUIRY_AT_PACK_IND, 'NOTIONAL_PACK_IND' AS NOTIONAL_PACK_IND, 'PERISHABLE_IND' AS PERISHABLE_IND, 'AIP_CASE_TYPE' AS AIP_CASE_TYPE, 'DEPOSIT_IN_PRICE_PER_UOM' AS DEPOSIT_IN_PRICE_PER_UOM, 'CONTAINER_ITEM' AS CONTAINER_ITEM, 'DEPOSIT_ITEM_TYPE' AS DEPOSIT_ITEM_TYPE, 'SALE_TYPE' AS SALE_TYPE, 'ORDER_TYPE' AS ORDER_TYPE, 'INVENTORY_IND' AS INVENTORY_IND, 'ITEM_XFORM_IND' AS ITEM_XFORM_IND, 'SHIP_ALONE_IND' AS SHIP_ALONE_IND, 'GIFT_WRAP_IND' AS GIFT_WRAP_IND, 'ITEM_SERVICE_LEVEL' AS ITEM_SERVICE_LEVEL, 'COMMENTS' AS COMMENTS, 'ORDER_AS_TYPE' AS ORDER_AS_TYPE, 'PACK_TYPE' AS PACK_TYPE, 'ORDERABLE_IND' AS ORDERABLE_IND, 'SELLABLE_IND' AS SELLABLE_IND,
    'CONTAINS_INNER_IND' AS CONTAINS_INNER_IND, 'SIMPLE_PACK_IND' AS SIMPLE_PACK_IND, 'CONST_DIMEN_IND' AS CONST_DIMEN_IND, 'DEFAULT_WASTE_PCT' AS DEFAULT_WASTE_PCT, 'WASTE_PCT' AS WASTE_PCT, 'WASTE_TYPE' AS WASTE_TYPE, 'CATCH_WEIGHT_IND' AS CATCH_WEIGHT_IND, 'HANDLING_SENSITIVITY' AS HANDLING_SENSITIVITY, 'HANDLING_TEMP' AS HANDLING_TEMP, 'RETAIL_LABEL_VALUE' AS RETAIL_LABEL_VALUE, 'RETAIL_LABEL_TYPE' AS RETAIL_LABEL_TYPE, 'MFG_REC_RETAIL' AS MFG_REC_RETAIL,'FORECAST_IND' AS FORECAST_IND, 'STORE_ORD_MULT' AS STORE_ORD_MULT, 'MERCHANDISE_IND' AS MERCHANDISE_IND, 'PACKAGE_UOM' AS PACKAGE_UOM, 'PACKAGE_SIZE' AS PACKAGE_SIZE, 'UOM_CONV_FACTOR' AS UOM_CONV_FACTOR, 'STANDARD_UOM' AS STANDARD_UOM, 'COST_ZONE_GROUP_ID' AS COST_ZONE_GROUP_ID, 'PRIMARY_REF_ITEM_IND' AS PRIMARY_REF_ITEM_IND, 'SHORT_DESC' AS SHORT_DESC, 'ITEM_DESC_SECONDARY' AS ITEM_DESC_SECONDARY, 'ITEM_DESC' AS ITEM_DESC, 'STATUS' AS STATUS, 'SUBCLASS' AS SUBCLASS,
    'CLASS'      AS CLASS, 'DEPT' AS DEPT, 'DIFF_4_AGGREGATE_IND' AS DIFF_4_AGGREGATE_IND, 'DIFF_4' AS DIFF_4, 'DIFF_3_AGGREGATE_IND' AS DIFF_3_AGGREGATE_IND, 'DIFF_3' AS DIFF_3, 'DIFF_2_AGGREGATE_IND' AS DIFF_2_AGGREGATE_IND, 'DIFF_2' AS DIFF_2, 'DIFF_1_AGGREGATE_IND' AS DIFF_1_AGGREGATE_IND, 'DIFF_1' AS DIFF_1, 'ITEM_AGGREGATE_IND' AS ITEM_AGGREGATE_IND, 'TRAN_LEVEL' AS TRAN_LEVEL, 'ITEM_LEVEL' AS ITEM_LEVEL, 'PACK_IND' AS PACK_IND, 'ITEM_GRANDPARENT' AS ITEM_GRANDPARENT, 'ITEM_PARENT' AS ITEM_PARENT, 'PREFIX' AS PREFIX, 'FORMAT_ID' AS FORMAT_ID, 'ITEM_NUMBER_TYPE' AS ITEM_NUMBER_TYPE, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item';
  L_table    VARCHAR2(30) := 'SVC_ITEM_MASTER';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  L_rpm_ind      VARCHAR2(1);
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT PRE_RESERVED_IND_dv,
    BRAND_NAME_dv,
    PRODUCT_CLASSIFICATION_dv,
    SOH_INQUIRY_AT_PACK_IND_dv,
    NOTIONAL_PACK_IND_dv,
    PERISHABLE_IND_dv,
    AIP_CASE_TYPE_dv,
    DEPOSIT_IN_PRICE_PER_UOM_dv,
    CONTAINER_ITEM_dv,
    DEPOSIT_ITEM_TYPE_dv,
    SALE_TYPE_dv,
    ORDER_TYPE_dv,
    INVENTORY_IND_dv,
    ITEM_XFORM_IND_dv,
    SHIP_ALONE_IND_dv,
    GIFT_WRAP_IND_dv,
    ITEM_SERVICE_LEVEL_dv,
    COMMENTS_dv,
    ORDER_AS_TYPE_dv,
    PACK_TYPE_dv,
    ORDERABLE_IND_dv,
    SELLABLE_IND_dv,
    CONTAINS_INNER_IND_dv,
    SIMPLE_PACK_IND_dv,
    CONST_DIMEN_IND_dv,
    DEFAULT_WASTE_PCT_dv,
    WASTE_PCT_dv,
    WASTE_TYPE_dv,
    CATCH_WEIGHT_IND_dv,
    HANDLING_SENSITIVITY_dv,
    HANDLING_TEMP_dv,
    RETAIL_LABEL_VALUE_dv,
    RETAIL_LABEL_TYPE_dv,
    MFG_REC_RETAIL_dv,
    FORECAST_IND_dv,
    STORE_ORD_MULT_dv,
    MERCHANDISE_IND_dv,
    PACKAGE_UOM_dv,
    PACKAGE_SIZE_dv,
    UOM_CONV_FACTOR_dv,
    STANDARD_UOM_dv,
    COST_ZONE_GROUP_ID_dv,
    PRIMARY_REF_ITEM_IND_dv,
    SHORT_DESC_dv,
    ITEM_DESC_SECONDARY_dv,
    ITEM_DESC_dv,
    STATUS_dv,
    SUBCLASS_dv,
    CLASS_dv,
    DEPT_dv,
    DIFF_4_AGGREGATE_IND_dv,
    DIFF_4_dv,
    DIFF_3_AGGREGATE_IND_dv,
    DIFF_3_dv,
    DIFF_2_AGGREGATE_IND_dv,
    DIFF_2_dv,
    DIFF_1_AGGREGATE_IND_dv,
    DIFF_1_dv,
    ITEM_AGGREGATE_IND_dv,
    TRAN_LEVEL_dv,
    ITEM_LEVEL_dv,
    PACK_IND_dv,
    ITEM_GRANDPARENT_dv,
    ITEM_PARENT_dv,
    PREFIX_dv,
    FORMAT_ID_dv,
    ITEM_NUMBER_TYPE_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_MASTER'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'PRE_RESERVED_IND' AS PRE_RESERVED_IND, 'BRAND_NAME' AS BRAND_NAME, 'PRODUCT_CLASSIFICATION' AS PRODUCT_CLASSIFICATION,'SOH_INQUIRY_AT_PACK_IND' AS SOH_INQUIRY_AT_PACK_IND, 'NOTIONAL_PACK_IND' AS NOTIONAL_PACK_IND, 'PERISHABLE_IND' AS PERISHABLE_IND, 'AIP_CASE_TYPE' AS AIP_CASE_TYPE, 'DEPOSIT_IN_PRICE_PER_UOM' AS DEPOSIT_IN_PRICE_PER_UOM, 'CONTAINER_ITEM' AS CONTAINER_ITEM, 'DEPOSIT_ITEM_TYPE' AS DEPOSIT_ITEM_TYPE, 'SALE_TYPE' AS SALE_TYPE, 'ORDER_TYPE' AS ORDER_TYPE, 'INVENTORY_IND' AS INVENTORY_IND, 'ITEM_XFORM_IND' AS ITEM_XFORM_IND, 'SHIP_ALONE_IND' AS SHIP_ALONE_IND, 'GIFT_WRAP_IND' AS GIFT_WRAP_IND, 'ITEM_SERVICE_LEVEL' AS ITEM_SERVICE_LEVEL, 'COMMENTS' AS COMMENTS, 'ORDER_AS_TYPE' AS ORDER_AS_TYPE, 'PACK_TYPE' AS PACK_TYPE, 'ORDERABLE_IND' AS ORDERABLE_IND, 'SELLABLE_IND' AS SELLABLE_IND,
    'CONTAINS_INNER_IND' AS CONTAINS_INNER_IND, 'SIMPLE_PACK_IND' AS SIMPLE_PACK_IND, 'CONST_DIMEN_IND' AS CONST_DIMEN_IND, 'DEFAULT_WASTE_PCT' AS DEFAULT_WASTE_PCT, 'WASTE_PCT' AS WASTE_PCT, 'WASTE_TYPE' AS WASTE_TYPE, 'CATCH_WEIGHT_IND' AS CATCH_WEIGHT_IND, 'HANDLING_SENSITIVITY' AS HANDLING_SENSITIVITY, 'HANDLING_TEMP' AS HANDLING_TEMP, 'RETAIL_LABEL_VALUE' AS RETAIL_LABEL_VALUE, 'RETAIL_LABEL_TYPE' AS RETAIL_LABEL_TYPE, 'MFG_REC_RETAIL' AS MFG_REC_RETAIL, 'FORECAST_IND' AS FORECAST_IND, 'STORE_ORD_MULT' AS STORE_ORD_MULT, 'MERCHANDISE_IND' AS MERCHANDISE_IND, 'PACKAGE_UOM' AS PACKAGE_UOM, 'PACKAGE_SIZE' AS PACKAGE_SIZE, 'UOM_CONV_FACTOR' AS UOM_CONV_FACTOR, 'STANDARD_UOM' AS STANDARD_UOM, 'COST_ZONE_GROUP_ID' AS COST_ZONE_GROUP_ID, 'PRIMARY_REF_ITEM_IND' AS PRIMARY_REF_ITEM_IND, 'SHORT_DESC' AS SHORT_DESC, 'ITEM_DESC_SECONDARY' AS ITEM_DESC_SECONDARY, 'ITEM_DESC' AS ITEM_DESC, 'STATUS' AS STATUS, 'SUBCLASS' AS SUBCLASS,
    'CLASS'      AS CLASS, 'DEPT' AS DEPT, 'DIFF_4_AGGREGATE_IND' AS DIFF_4_AGGREGATE_IND, 'DIFF_4' AS DIFF_4, 'DIFF_3_AGGREGATE_IND' AS DIFF_3_AGGREGATE_IND, 'DIFF_3' AS DIFF_3, 'DIFF_2_AGGREGATE_IND' AS DIFF_2_AGGREGATE_IND, 'DIFF_2' AS DIFF_2, 'DIFF_1_AGGREGATE_IND' AS DIFF_1_AGGREGATE_IND, 'DIFF_1' AS DIFF_1, 'ITEM_AGGREGATE_IND' AS ITEM_AGGREGATE_IND, 'TRAN_LEVEL' AS TRAN_LEVEL, 'ITEM_LEVEL' AS ITEM_LEVEL, 'PACK_IND' AS PACK_IND, 'ITEM_GRANDPARENT' AS ITEM_GRANDPARENT, 'ITEM_PARENT' AS ITEM_PARENT, 'PREFIX' AS PREFIX, 'FORMAT_ID' AS FORMAT_ID, 'ITEM_NUMBER_TYPE' AS ITEM_NUMBER_TYPE, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      im_default_rec.PRE_RESERVED_IND := rec.PRE_RESERVED_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'PRE_RESERVED_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.BRAND_NAME := rec.BRAND_NAME_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'BRAND_NAME','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.PRODUCT_CLASSIFICATION := rec.PRODUCT_CLASSIFICATION_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'PRODUCT_CLASSIFICATION','INV_DEFAULT',SQLERRM);
    END;

    BEGIN
      im_default_rec.SOH_INQUIRY_AT_PACK_IND := rec.SOH_INQUIRY_AT_PACK_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'SOH_INQUIRY_AT_PACK_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.NOTIONAL_PACK_IND := rec.NOTIONAL_PACK_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'NOTIONAL_PACK_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.PERISHABLE_IND := rec.PERISHABLE_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'PERISHABLE_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.AIP_CASE_TYPE := rec.AIP_CASE_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'AIP_CASE_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.DEPOSIT_IN_PRICE_PER_UOM := rec.DEPOSIT_IN_PRICE_PER_UOM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'DEPOSIT_IN_PRICE_PER_UOM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.CONTAINER_ITEM := rec.CONTAINER_ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'CONTAINER_ITEM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.DEPOSIT_ITEM_TYPE := rec.DEPOSIT_ITEM_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'DEPOSIT_ITEM_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.SALE_TYPE := rec.SALE_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'SALE_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ORDER_TYPE := rec.ORDER_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ORDER_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.INVENTORY_IND := rec.INVENTORY_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'INVENTORY_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ITEM_XFORM_IND := rec.ITEM_XFORM_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ITEM_XFORM_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.SHIP_ALONE_IND := rec.SHIP_ALONE_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'SHIP_ALONE_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.GIFT_WRAP_IND := rec.GIFT_WRAP_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'GIFT_WRAP_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ITEM_SERVICE_LEVEL := rec.ITEM_SERVICE_LEVEL_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ITEM_SERVICE_LEVEL','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.COMMENTS := rec.COMMENTS_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'COMMENTS','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ORDER_AS_TYPE := rec.ORDER_AS_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ORDER_AS_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.PACK_TYPE := rec.PACK_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'PACK_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ORDERABLE_IND := rec.ORDERABLE_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ORDERABLE_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.SELLABLE_IND := rec.SELLABLE_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'SELLABLE_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.CONTAINS_INNER_IND := rec.CONTAINS_INNER_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'CONTAINS_INNER_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.SIMPLE_PACK_IND := rec.SIMPLE_PACK_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'SIMPLE_PACK_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.CONST_DIMEN_IND := rec.CONST_DIMEN_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'CONST_DIMEN_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.DEFAULT_WASTE_PCT := rec.DEFAULT_WASTE_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'DEFAULT_WASTE_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.WASTE_PCT := rec.WASTE_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'WASTE_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.WASTE_TYPE := rec.WASTE_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'WASTE_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.CATCH_WEIGHT_IND := rec.CATCH_WEIGHT_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'CATCH_WEIGHT_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.HANDLING_SENSITIVITY := rec.HANDLING_SENSITIVITY_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'HANDLING_SENSITIVITY','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.HANDLING_TEMP := rec.HANDLING_TEMP_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'HANDLING_TEMP','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.RETAIL_LABEL_VALUE := rec.RETAIL_LABEL_VALUE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'RETAIL_LABEL_VALUE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.RETAIL_LABEL_TYPE := rec.RETAIL_LABEL_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'RETAIL_LABEL_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.MFG_REC_RETAIL := rec.MFG_REC_RETAIL_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'MFG_REC_RETAIL','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.FORECAST_IND := rec.FORECAST_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'FORECAST_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.STORE_ORD_MULT := rec.STORE_ORD_MULT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'STORE_ORD_MULT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.MERCHANDISE_IND := rec.MERCHANDISE_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'MERCHANDISE_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.PACKAGE_UOM := rec.PACKAGE_UOM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'PACKAGE_UOM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.PACKAGE_SIZE := rec.PACKAGE_SIZE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'PACKAGE_SIZE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.UOM_CONV_FACTOR := rec.UOM_CONV_FACTOR_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'UOM_CONV_FACTOR','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.STANDARD_UOM := rec.STANDARD_UOM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'STANDARD_UOM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.COST_ZONE_GROUP_ID := rec.COST_ZONE_GROUP_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'COST_ZONE_GROUP_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.PRIMARY_REF_ITEM_IND := rec.PRIMARY_REF_ITEM_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'PRIMARY_REF_ITEM_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.SHORT_DESC := rec.SHORT_DESC_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'SHORT_DESC','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ITEM_DESC_SECONDARY := rec.ITEM_DESC_SECONDARY_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ITEM_DESC_SECONDARY','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ITEM_DESC := rec.ITEM_DESC_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ITEM_DESC','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.STATUS := rec.STATUS_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'STATUS','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.SUBCLASS := rec.SUBCLASS_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'SUBCLASS','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.CLASS := rec.CLASS_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'CLASS','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.DEPT := rec.DEPT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'DEPT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.DIFF_4_AGGREGATE_IND := rec.DIFF_4_AGGREGATE_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'DIFF_4_AGGREGATE_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.DIFF_4 := rec.DIFF_4_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'DIFF_4','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.DIFF_3_AGGREGATE_IND := rec.DIFF_3_AGGREGATE_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'DIFF_3_AGGREGATE_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.DIFF_3 := rec.DIFF_3_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'DIFF_3','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.DIFF_2_AGGREGATE_IND := rec.DIFF_2_AGGREGATE_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'DIFF_2_AGGREGATE_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.DIFF_2 := rec.DIFF_2_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'DIFF_2','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.DIFF_1_AGGREGATE_IND := rec.DIFF_1_AGGREGATE_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'DIFF_1_AGGREGATE_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.DIFF_1 := rec.DIFF_1_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'DIFF_1','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ITEM_AGGREGATE_IND := rec.ITEM_AGGREGATE_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ITEM_AGGREGATE_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.TRAN_LEVEL := rec.TRAN_LEVEL_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'TRAN_LEVEL','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ITEM_LEVEL := rec.ITEM_LEVEL_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ITEM_LEVEL','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.PACK_IND := rec.PACK_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'PACK_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ITEM_GRANDPARENT := rec.ITEM_GRANDPARENT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ITEM_GRANDPARENT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ITEM_PARENT := rec.ITEM_PARENT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ITEM_PARENT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.PREFIX := rec.PREFIX_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'PREFIX','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.FORMAT_ID := rec.FORMAT_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'FORMAT_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ITEM_NUMBER_TYPE := rec.ITEM_NUMBER_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ITEM_NUMBER_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      im_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_MASTER',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(IC_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(IM_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(IM$Action)         AS Action,
    r.get_cell(IM$NEXT_UPD_ID)          AS NEXT_UPD_ID,
    r.get_cell(IM$ORIG_REF_NO)          AS ORIG_REF_NO,
    r.get_cell(IM$PRE_RESERVED_IND)     AS PRE_RESERVED_IND,
    r.get_cell(IM$BRAND_NAME)           AS BRAND_NAME,
    r.get_cell(IM$PRODUCT_CLASSIFICATION)   AS PRODUCT_CLASSIFICATION,
    r.get_cell(IM$SOH_INQUIRY_AT_PACK_IND)  AS SOH_INQUIRY_AT_PACK_IND,
    r.get_cell(IM$NOTIONAL_PACK_IND)        AS NOTIONAL_PACK_IND,
    r.get_cell(IM$PERISHABLE_IND)       AS PERISHABLE_IND,
    r.get_cell(IM$AIP_CASE_TYPE)        AS AIP_CASE_TYPE,
    r.get_cell(IM$DEPOSIT_IN_PRICE_PER_UOM) AS DEPOSIT_IN_PRICE_PER_UOM,
    r.get_cell(IM$CONTAINER_ITEM)       AS CONTAINER_ITEM,
    r.get_cell(IM$DEPOSIT_ITEM_TYPE)        AS DEPOSIT_ITEM_TYPE,
    r.get_cell(IM$SALE_TYPE)            AS SALE_TYPE,
    r.get_cell(IM$ORDER_TYPE)           AS ORDER_TYPE,
    r.get_cell(IM$INVENTORY_IND)        AS INVENTORY_IND,
    r.get_cell(IM$ITEM_XFORM_IND)       AS ITEM_XFORM_IND,
    r.get_cell(IM$SHIP_ALONE_IND)       AS SHIP_ALONE_IND,
    r.get_cell(IM$GIFT_WRAP_IND)        AS GIFT_WRAP_IND,
    r.get_cell(IM$ITEM_SERVICE_LEVEL)       AS ITEM_SERVICE_LEVEL,
    r.get_cell(IM$COMMENTS)         AS COMMENTS,
    r.get_cell(IM$ORDER_AS_TYPE)        AS ORDER_AS_TYPE,
    r.get_cell(IM$PACK_TYPE)            AS PACK_TYPE,
    r.get_cell(IM$ORDERABLE_IND)        AS ORDERABLE_IND,
    r.get_cell(IM$SELLABLE_IND)         AS SELLABLE_IND,
    r.get_cell(IM$CONTAINS_INNER_IND)       AS CONTAINS_INNER_IND,
    r.get_cell(IM$SIMPLE_PACK_IND)      AS SIMPLE_PACK_IND,
    r.get_cell(IM$CONST_DIMEN_IND)      AS CONST_DIMEN_IND,
    r.get_cell(IM$DEFAULT_WASTE_PCT)        AS DEFAULT_WASTE_PCT,
    r.get_cell(IM$WASTE_PCT)            AS WASTE_PCT,
    r.get_cell(IM$WASTE_TYPE)           AS WASTE_TYPE,
    r.get_cell(IM$CATCH_WEIGHT_IND)     AS CATCH_WEIGHT_IND,
    r.get_cell(IM$HANDLING_SENSITIVITY)     AS HANDLING_SENSITIVITY,
    r.get_cell(IM$HANDLING_TEMP)        AS HANDLING_TEMP,
    r.get_cell(IM$RETAIL_LABEL_VALUE)       AS RETAIL_LABEL_VALUE,
    r.get_cell(IM$RETAIL_LABEL_TYPE)        AS RETAIL_LABEL_TYPE,
    r.get_cell(IM$MFG_REC_RETAIL)       AS MFG_REC_RETAIL,
    r.get_cell(IM$FORECAST_IND)         AS FORECAST_IND,
    r.get_cell(IM$STORE_ORD_MULT)       AS STORE_ORD_MULT,
    r.get_cell(IM$MERCHANDISE_IND)      AS MERCHANDISE_IND,
    r.get_cell(IM$PACKAGE_UOM)          AS PACKAGE_UOM,
    r.get_cell(IM$PACKAGE_SIZE)         AS PACKAGE_SIZE,
    r.get_cell(IM$UOM_CONV_FACTOR)      AS UOM_CONV_FACTOR,
    r.get_cell(IM$STANDARD_UOM)         AS STANDARD_UOM,
    r.get_cell(IM$COST_ZONE_GROUP_ID)       AS COST_ZONE_GROUP_ID,
    r.get_cell(IM$PRIMARY_REF_ITEM_IND)     AS PRIMARY_REF_ITEM_IND,
    r.get_cell(IM$SHORT_DESC)           AS SHORT_DESC,
    r.get_cell(IM$ITEM_DESC_SECONDARY)      AS ITEM_DESC_SECONDARY,
    r.get_cell(IM$ITEM_DESC)            AS ITEM_DESC,
    r.get_cell(IM$STATUS)           AS STATUS,
    r.get_cell(IM$SUBCLASS)         AS SUBCLASS,
    r.get_cell(IM$CLASS)            AS CLASS,
    r.get_cell(IM$DEPT)             AS DEPT,
    r.get_cell(IM$DIFF_4_AGGREGATE_IND)     AS DIFF_4_AGGREGATE_IND,
    r.get_cell(IM$DIFF_4)           AS DIFF_4,
    r.get_cell(IM$DIFF_3_AGGREGATE_IND)     AS DIFF_3_AGGREGATE_IND,
    r.get_cell(IM$DIFF_3)           AS DIFF_3,
    r.get_cell(IM$DIFF_2_AGGREGATE_IND)     AS DIFF_2_AGGREGATE_IND,
    r.get_cell(IM$DIFF_2)           AS DIFF_2,
    r.get_cell(IM$DIFF_1_AGGREGATE_IND)     AS DIFF_1_AGGREGATE_IND,
    r.get_cell(IM$DIFF_1)           AS DIFF_1,
    r.get_cell(IM$ITEM_AGGREGATE_IND)       AS ITEM_AGGREGATE_IND,
    r.get_cell(IM$TRAN_LEVEL)           AS TRAN_LEVEL,
    r.get_cell(IM$ITEM_LEVEL)           AS ITEM_LEVEL,
    r.get_cell(IM$PACK_IND)         AS PACK_IND,
    r.get_cell(IM$ITEM_GRANDPARENT)     AS ITEM_GRANDPARENT,
    r.get_cell(IM$ITEM_PARENT)          AS ITEM_PARENT,
    r.get_cell(IM$PREFIX)           AS PREFIX,
    r.get_cell(IM$FORMAT_ID)            AS FORMAT_ID,
    r.get_cell(IM$ITEM_NUMBER_TYPE)     AS ITEM_NUMBER_TYPE,
    r.get_cell(IM$ITEM)             AS ITEM,
    r.get_row_seq()             AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id     := I_process_id;
    l_temp_rec.chunk_id       := 1;
    l_temp_rec.row_seq        := rec.row_seq;
    l_temp_rec.process$status     := 'N';
    l_temp_rec.create_id      := get_user;
    l_temp_rec.last_upd_id    := get_user;
    l_temp_rec.create_datetime    := sysdate;
    l_temp_rec.last_upd_datetime  := sysdate;
    l_error           := false;
    l_temp_rec.diff_finalized_ind := 'N';
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.next_upd_id := rec.next_upd_id;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'NEXT_UPD_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.orig_ref_no := rec.orig_ref_no;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ORIG_REF_NO',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PRE_RESERVED_IND := rec.PRE_RESERVED_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'PRE_RESERVED_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.BRAND_NAME := rec.BRAND_NAME;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'BRAND_NAME',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PRODUCT_CLASSIFICATION := rec.PRODUCT_CLASSIFICATION;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'PRODUCT_CLASSIFICATION',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SOH_INQUIRY_AT_PACK_IND := rec.SOH_INQUIRY_AT_PACK_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'SOH_INQUIRY_AT_PACK_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.NOTIONAL_PACK_IND := rec.NOTIONAL_PACK_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'NOTIONAL_PACK_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PERISHABLE_IND := rec.PERISHABLE_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'PERISHABLE_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.AIP_CASE_TYPE := rec.AIP_CASE_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'AIP_CASE_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DEPOSIT_IN_PRICE_PER_UOM := rec.DEPOSIT_IN_PRICE_PER_UOM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'DEPOSIT_IN_PRICE_PER_UOM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.CONTAINER_ITEM := rec.CONTAINER_ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'CONTAINER_ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DEPOSIT_ITEM_TYPE := rec.DEPOSIT_ITEM_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'DEPOSIT_ITEM_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SALE_TYPE := rec.SALE_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'SALE_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ORDER_TYPE := rec.ORDER_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ORDER_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.INVENTORY_IND := rec.INVENTORY_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'INVENTORY_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_XFORM_IND := rec.ITEM_XFORM_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ITEM_XFORM_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SHIP_ALONE_IND := rec.SHIP_ALONE_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'SHIP_ALONE_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.GIFT_WRAP_IND := rec.GIFT_WRAP_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'GIFT_WRAP_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_SERVICE_LEVEL := rec.ITEM_SERVICE_LEVEL;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ITEM_SERVICE_LEVEL',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COMMENTS := rec.COMMENTS;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'COMMENTS',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ORDER_AS_TYPE := rec.ORDER_AS_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ORDER_AS_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PACK_TYPE := rec.PACK_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'PACK_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ORDERABLE_IND := rec.ORDERABLE_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ORDERABLE_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SELLABLE_IND := rec.SELLABLE_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'SELLABLE_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.CONTAINS_INNER_IND := rec.CONTAINS_INNER_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'CONTAINS_INNER_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SIMPLE_PACK_IND := rec.SIMPLE_PACK_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'SIMPLE_PACK_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.CONST_DIMEN_IND := rec.CONST_DIMEN_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'CONST_DIMEN_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DEFAULT_WASTE_PCT := rec.DEFAULT_WASTE_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'DEFAULT_WASTE_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.WASTE_PCT := rec.WASTE_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'WASTE_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.WASTE_TYPE := rec.WASTE_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'WASTE_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.CATCH_WEIGHT_IND := rec.CATCH_WEIGHT_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'CATCH_WEIGHT_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.HANDLING_SENSITIVITY := rec.HANDLING_SENSITIVITY;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'HANDLING_SENSITIVITY',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.HANDLING_TEMP := rec.HANDLING_TEMP;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'HANDLING_TEMP',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.RETAIL_LABEL_VALUE := rec.RETAIL_LABEL_VALUE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'RETAIL_LABEL_VALUE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.RETAIL_LABEL_TYPE := rec.RETAIL_LABEL_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'RETAIL_LABEL_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.MFG_REC_RETAIL := rec.MFG_REC_RETAIL;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'MFG_REC_RETAIL',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.FORECAST_IND := rec.FORECAST_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'FORECAST_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.STORE_ORD_MULT := rec.STORE_ORD_MULT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'STORE_ORD_MULT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.MERCHANDISE_IND := rec.MERCHANDISE_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'MERCHANDISE_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PACKAGE_UOM := rec.PACKAGE_UOM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'PACKAGE_UOM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PACKAGE_SIZE := rec.PACKAGE_SIZE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'PACKAGE_SIZE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.UOM_CONV_FACTOR := rec.UOM_CONV_FACTOR;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'UOM_CONV_FACTOR',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.STANDARD_UOM := rec.STANDARD_UOM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'STANDARD_UOM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COST_ZONE_GROUP_ID := rec.COST_ZONE_GROUP_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'COST_ZONE_GROUP_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PRIMARY_REF_ITEM_IND := rec.PRIMARY_REF_ITEM_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'PRIMARY_REF_ITEM_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SHORT_DESC := rec.SHORT_DESC;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'SHORT_DESC',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_DESC_SECONDARY := rec.ITEM_DESC_SECONDARY;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ITEM_DESC_SECONDARY',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_DESC := rec.ITEM_DESC;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ITEM_DESC',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.STATUS := rec.STATUS;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'STATUS',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUBCLASS := rec.SUBCLASS;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'SUBCLASS',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.CLASS := rec.CLASS;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'CLASS',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DEPT := rec.DEPT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'DEPT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DIFF_4_AGGREGATE_IND := rec.DIFF_4_AGGREGATE_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'DIFF_4_AGGREGATE_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DIFF_4 := rec.DIFF_4;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'DIFF_4',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DIFF_3_AGGREGATE_IND := rec.DIFF_3_AGGREGATE_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'DIFF_3_AGGREGATE_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DIFF_3 := rec.DIFF_3;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'DIFF_3',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DIFF_2_AGGREGATE_IND := rec.DIFF_2_AGGREGATE_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'DIFF_2_AGGREGATE_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DIFF_2 := rec.DIFF_2;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'DIFF_2',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DIFF_1_AGGREGATE_IND := rec.DIFF_1_AGGREGATE_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'DIFF_1_AGGREGATE_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DIFF_1 := rec.DIFF_1;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'DIFF_1',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_AGGREGATE_IND := rec.ITEM_AGGREGATE_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ITEM_AGGREGATE_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.TRAN_LEVEL := rec.TRAN_LEVEL;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'TRAN_LEVEL',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_LEVEL := rec.ITEM_LEVEL;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ITEM_LEVEL',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PACK_IND := rec.PACK_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'PACK_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_GRANDPARENT := rec.ITEM_GRANDPARENT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ITEM_GRANDPARENT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_PARENT := rec.ITEM_PARENT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ITEM_PARENT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PREFIX := rec.PREFIX;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'PREFIX',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.FORMAT_ID := rec.FORMAT_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'FORMAT_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_NUMBER_TYPE := rec.ITEM_NUMBER_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ITEM_NUMBER_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    l_default_rec := im_default_rec;
    -- do not get IM defaults if the item is a reference item
    if l_temp_rec.item_level > l_temp_rec.tran_level then
       l_default_rec.BRAND_NAME := NULL;
       l_default_rec.PRODUCT_CLASSIFICATION := NULL;
       l_default_rec.PACKAGE_SIZE := NULL;
       l_default_rec.PACKAGE_UOM := NULL;
       l_default_rec.RETAIL_LABEL_VALUE := NULL;
       l_default_rec.RETAIL_LABEL_TYPE := NULL;
       l_default_rec.HANDLING_TEMP := NULL;
       l_default_rec.HANDLING_SENSITIVITY := NULL;
       l_default_rec.WASTE_TYPE := NULL;
       l_default_rec.DEFAULT_WASTE_PCT := NULL;
       l_default_rec.WASTE_PCT := NULL;
       l_default_rec.ITEM_SERVICE_LEVEL := NULL;
       l_default_rec.ORDER_TYPE := NULL;
       l_default_rec.SALE_TYPE := NULL;
       l_default_rec.DEPOSIT_IN_PRICE_PER_UOM := NULL;
       l_default_rec.DEPOSIT_ITEM_TYPE := NULL;
       l_default_rec.CONTAINER_ITEM := NULL;
    end if;

    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_ITEM_MASTER%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_ITEM_MASTER
       WHERE ITEM     = l_temp_rec.ITEM
;
    l_rms_rec ITEM_MASTER%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM ITEM_MASTER
       WHERE ITEM     = l_temp_rec.ITEM
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_ITEM_MASTER
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.ITEM_NUMBER_TYPE_mi <> 'Y' THEN
        l_temp_rec.ITEM_NUMBER_TYPE   := l_stg_rec.ITEM_NUMBER_TYPE;
      END IF;
      IF l_mi_rec.FORMAT_ID_mi <> 'Y' THEN
        l_temp_rec.FORMAT_ID   := l_stg_rec.FORMAT_ID;
      END IF;
      IF l_mi_rec.PREFIX_mi <> 'Y' THEN
        l_temp_rec.PREFIX   := l_stg_rec.PREFIX;
      END IF;
      IF l_mi_rec.ITEM_PARENT_mi <> 'Y' THEN
        l_temp_rec.ITEM_PARENT   := l_stg_rec.ITEM_PARENT;
      END IF;
      IF l_mi_rec.ITEM_GRANDPARENT_mi <> 'Y' THEN
        l_temp_rec.ITEM_GRANDPARENT   := l_stg_rec.ITEM_GRANDPARENT;
      END IF;
      IF l_mi_rec.PACK_IND_mi <> 'Y' THEN
        l_temp_rec.PACK_IND   := l_stg_rec.PACK_IND;
      END IF;
      IF l_mi_rec.ITEM_LEVEL_mi <> 'Y' THEN
        l_temp_rec.ITEM_LEVEL   := l_stg_rec.ITEM_LEVEL;
      END IF;
      IF l_mi_rec.TRAN_LEVEL_mi <> 'Y' THEN
        l_temp_rec.TRAN_LEVEL   := l_stg_rec.TRAN_LEVEL;
      END IF;
      IF l_mi_rec.ITEM_AGGREGATE_IND_mi <> 'Y' THEN
        l_temp_rec.ITEM_AGGREGATE_IND   := l_stg_rec.ITEM_AGGREGATE_IND;
      END IF;
      IF l_mi_rec.DIFF_1_mi <> 'Y' THEN
        l_temp_rec.DIFF_1   := l_stg_rec.DIFF_1;
      END IF;
      IF l_mi_rec.DIFF_1_AGGREGATE_IND_mi <> 'Y' THEN
        l_temp_rec.DIFF_1_AGGREGATE_IND   := l_stg_rec.DIFF_1_AGGREGATE_IND;
      END IF;
      IF l_mi_rec.DIFF_2_mi <> 'Y' THEN
        l_temp_rec.DIFF_2   := l_stg_rec.DIFF_2;
      END IF;
      IF l_mi_rec.DIFF_2_AGGREGATE_IND_mi <> 'Y' THEN
        l_temp_rec.DIFF_2_AGGREGATE_IND   := l_stg_rec.DIFF_2_AGGREGATE_IND;
      END IF;
      IF l_mi_rec.DIFF_3_mi <> 'Y' THEN
        l_temp_rec.DIFF_3   := l_stg_rec.DIFF_3;
      END IF;
      IF l_mi_rec.DIFF_3_AGGREGATE_IND_mi <> 'Y' THEN
        l_temp_rec.DIFF_3_AGGREGATE_IND   := l_stg_rec.DIFF_3_AGGREGATE_IND;
      END IF;
      IF l_mi_rec.DIFF_4_mi <> 'Y' THEN
        l_temp_rec.DIFF_4   := l_stg_rec.DIFF_4;
      END IF;
      IF l_mi_rec.DIFF_4_AGGREGATE_IND_mi <> 'Y' THEN
        l_temp_rec.DIFF_4_AGGREGATE_IND   := l_stg_rec.DIFF_4_AGGREGATE_IND;
      END IF;
      IF l_mi_rec.DEPT_mi <> 'Y' THEN
        l_temp_rec.DEPT   := l_stg_rec.DEPT;
      END IF;
      IF l_mi_rec.CLASS_mi <> 'Y' THEN
        l_temp_rec.CLASS   := l_stg_rec.CLASS;
      END IF;
      IF l_mi_rec.SUBCLASS_mi <> 'Y' THEN
        l_temp_rec.SUBCLASS   := l_stg_rec.SUBCLASS;
      END IF;
      IF l_mi_rec.STATUS_mi <> 'Y' THEN
        l_temp_rec.STATUS   := l_stg_rec.STATUS;
      END IF;
      IF l_mi_rec.ITEM_DESC_mi <> 'Y' THEN
        l_temp_rec.ITEM_DESC   := l_stg_rec.ITEM_DESC;
      END IF;
      IF l_mi_rec.ITEM_DESC_SECONDARY_mi <> 'Y' THEN
        l_temp_rec.ITEM_DESC_SECONDARY   := l_stg_rec.ITEM_DESC_SECONDARY;
      END IF;
      IF l_mi_rec.SHORT_DESC_mi <> 'Y' THEN
        l_temp_rec.SHORT_DESC   := l_stg_rec.SHORT_DESC;
      END IF;
      IF l_mi_rec.PRIMARY_REF_ITEM_IND_mi <> 'Y' THEN
        l_temp_rec.PRIMARY_REF_ITEM_IND   := l_stg_rec.PRIMARY_REF_ITEM_IND;
      END IF;
      IF l_mi_rec.COST_ZONE_GROUP_ID_mi <> 'Y' THEN
        l_temp_rec.COST_ZONE_GROUP_ID   := l_stg_rec.COST_ZONE_GROUP_ID;
      END IF;
      IF l_mi_rec.STANDARD_UOM_mi <> 'Y' THEN
        l_temp_rec.STANDARD_UOM   := l_stg_rec.STANDARD_UOM;
      END IF;
      IF l_mi_rec.UOM_CONV_FACTOR_mi <> 'Y' THEN
        l_temp_rec.UOM_CONV_FACTOR   := l_stg_rec.UOM_CONV_FACTOR;
      END IF;
      IF l_mi_rec.PACKAGE_SIZE_mi <> 'Y' THEN
        l_temp_rec.PACKAGE_SIZE   := l_stg_rec.PACKAGE_SIZE;
      END IF;
      IF l_mi_rec.PACKAGE_UOM_mi <> 'Y' THEN
        l_temp_rec.PACKAGE_UOM   := l_stg_rec.PACKAGE_UOM;
      END IF;
      IF l_mi_rec.MERCHANDISE_IND_mi <> 'Y' THEN
        l_temp_rec.MERCHANDISE_IND   := l_stg_rec.MERCHANDISE_IND;
      END IF;
      IF l_mi_rec.STORE_ORD_MULT_mi <> 'Y' THEN
        l_temp_rec.STORE_ORD_MULT   := l_stg_rec.STORE_ORD_MULT;
      END IF;
      IF l_mi_rec.FORECAST_IND_mi <> 'Y' THEN
        l_temp_rec.FORECAST_IND   := l_stg_rec.FORECAST_IND;
      END IF;
      IF l_mi_rec.MFG_REC_RETAIL_mi <> 'Y' THEN
        l_temp_rec.MFG_REC_RETAIL   := l_stg_rec.MFG_REC_RETAIL;
      END IF;
      IF l_mi_rec.RETAIL_LABEL_TYPE_mi <> 'Y' THEN
        l_temp_rec.RETAIL_LABEL_TYPE   := l_stg_rec.RETAIL_LABEL_TYPE;
      END IF;
      IF l_mi_rec.RETAIL_LABEL_VALUE_mi <> 'Y' THEN
        l_temp_rec.RETAIL_LABEL_VALUE   := l_stg_rec.RETAIL_LABEL_VALUE;
      END IF;
      IF l_mi_rec.HANDLING_TEMP_mi <> 'Y' THEN
        l_temp_rec.HANDLING_TEMP   := l_stg_rec.HANDLING_TEMP;
      END IF;
      IF l_mi_rec.HANDLING_SENSITIVITY_mi <> 'Y' THEN
        l_temp_rec.HANDLING_SENSITIVITY   := l_stg_rec.HANDLING_SENSITIVITY;
      END IF;
      IF l_mi_rec.CATCH_WEIGHT_IND_mi <> 'Y' THEN
        l_temp_rec.CATCH_WEIGHT_IND   := l_stg_rec.CATCH_WEIGHT_IND;
      END IF;
      IF l_mi_rec.WASTE_TYPE_mi <> 'Y' THEN
        l_temp_rec.WASTE_TYPE   := l_stg_rec.WASTE_TYPE;
      END IF;
      IF l_mi_rec.WASTE_PCT_mi <> 'Y' THEN
        l_temp_rec.WASTE_PCT   := l_stg_rec.WASTE_PCT;
      END IF;
      IF l_mi_rec.DEFAULT_WASTE_PCT_mi <> 'Y' THEN
        l_temp_rec.DEFAULT_WASTE_PCT   := l_stg_rec.DEFAULT_WASTE_PCT;
      END IF;
      IF l_mi_rec.CONST_DIMEN_IND_mi <> 'Y' THEN
        l_temp_rec.CONST_DIMEN_IND   := l_stg_rec.CONST_DIMEN_IND;
      END IF;
      IF l_mi_rec.SIMPLE_PACK_IND_mi <> 'Y' THEN
        l_temp_rec.SIMPLE_PACK_IND   := l_stg_rec.SIMPLE_PACK_IND;
      END IF;
      IF l_mi_rec.CONTAINS_INNER_IND_mi <> 'Y' THEN
        l_temp_rec.CONTAINS_INNER_IND   := l_stg_rec.CONTAINS_INNER_IND;
      END IF;
      IF l_mi_rec.SELLABLE_IND_mi <> 'Y' THEN
        l_temp_rec.SELLABLE_IND   := l_stg_rec.SELLABLE_IND;
      END IF;
      IF l_mi_rec.ORDERABLE_IND_mi <> 'Y' THEN
        l_temp_rec.ORDERABLE_IND   := l_stg_rec.ORDERABLE_IND;
      END IF;
      IF l_mi_rec.PACK_TYPE_mi <> 'Y' THEN
        l_temp_rec.PACK_TYPE   := l_stg_rec.PACK_TYPE;
      END IF;
      IF l_mi_rec.ORDER_AS_TYPE_mi <> 'Y' THEN
        l_temp_rec.ORDER_AS_TYPE   := l_stg_rec.ORDER_AS_TYPE;
      END IF;
      IF l_mi_rec.COMMENTS_mi <> 'Y' THEN
        l_temp_rec.COMMENTS   := l_stg_rec.COMMENTS;
      END IF;
      IF l_mi_rec.ITEM_SERVICE_LEVEL_mi <> 'Y' THEN
        l_temp_rec.ITEM_SERVICE_LEVEL   := l_stg_rec.ITEM_SERVICE_LEVEL;
      END IF;
      IF l_mi_rec.GIFT_WRAP_IND_mi <> 'Y' THEN
        l_temp_rec.GIFT_WRAP_IND   := l_stg_rec.GIFT_WRAP_IND;
      END IF;
      IF l_mi_rec.SHIP_ALONE_IND_mi <> 'Y' THEN
        l_temp_rec.SHIP_ALONE_IND   := l_stg_rec.SHIP_ALONE_IND;
      END IF;
      IF l_mi_rec.ITEM_XFORM_IND_mi <> 'Y' THEN
        l_temp_rec.ITEM_XFORM_IND   := l_stg_rec.ITEM_XFORM_IND;
      END IF;
      IF l_mi_rec.INVENTORY_IND_mi <> 'Y' THEN
        l_temp_rec.INVENTORY_IND   := l_stg_rec.INVENTORY_IND;
      END IF;
      IF l_mi_rec.ORDER_TYPE_mi <> 'Y' THEN
        l_temp_rec.ORDER_TYPE   := l_stg_rec.ORDER_TYPE;
      END IF;
      IF l_mi_rec.SALE_TYPE_mi <> 'Y' THEN
        l_temp_rec.SALE_TYPE   := l_stg_rec.SALE_TYPE;
      END IF;
      IF l_mi_rec.DEPOSIT_ITEM_TYPE_mi <> 'Y' THEN
        l_temp_rec.DEPOSIT_ITEM_TYPE   := l_stg_rec.DEPOSIT_ITEM_TYPE;
      END IF;
      IF l_mi_rec.CONTAINER_ITEM_mi <> 'Y' THEN
        l_temp_rec.CONTAINER_ITEM   := l_stg_rec.CONTAINER_ITEM;
      END IF;
      IF l_mi_rec.DEPOSIT_IN_PRICE_PER_UOM_mi <> 'Y' THEN
        l_temp_rec.DEPOSIT_IN_PRICE_PER_UOM   := l_stg_rec.DEPOSIT_IN_PRICE_PER_UOM;
      END IF;
      IF l_mi_rec.AIP_CASE_TYPE_mi <> 'Y' THEN
        l_temp_rec.AIP_CASE_TYPE   := l_stg_rec.AIP_CASE_TYPE;
      END IF;
      IF l_mi_rec.PERISHABLE_IND_mi <> 'Y' THEN
        l_temp_rec.PERISHABLE_IND   := l_stg_rec.PERISHABLE_IND;
      END IF;
      IF l_mi_rec.NOTIONAL_PACK_IND_mi <> 'Y' THEN
        l_temp_rec.NOTIONAL_PACK_IND   := l_stg_rec.NOTIONAL_PACK_IND;
      END IF;
      IF l_mi_rec.SOH_INQUIRY_AT_PACK_IND_mi <> 'Y' THEN
        l_temp_rec.SOH_INQUIRY_AT_PACK_IND   := l_stg_rec.SOH_INQUIRY_AT_PACK_IND;
      END IF;
      IF l_mi_rec.PRODUCT_CLASSIFICATION_mi <> 'Y' THEN
        l_temp_rec.PRODUCT_CLASSIFICATION   := l_stg_rec.PRODUCT_CLASSIFICATION;
      END IF;
      IF l_mi_rec.BRAND_NAME_mi <> 'Y' THEN
        l_temp_rec.BRAND_NAME   := l_stg_rec.BRAND_NAME;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.ITEM_NUMBER_TYPE_mi <> 'Y' THEN
        l_temp_rec.ITEM_NUMBER_TYPE   := l_rms_rec.ITEM_NUMBER_TYPE;
      END IF;
      IF l_mi_rec.FORMAT_ID_mi <> 'Y' THEN
        l_temp_rec.FORMAT_ID   := l_rms_rec.FORMAT_ID;
      END IF;
      IF l_mi_rec.PREFIX_mi <> 'Y' THEN
        l_temp_rec.PREFIX   := l_rms_rec.PREFIX;
      END IF;
      IF l_mi_rec.ITEM_PARENT_mi <> 'Y' THEN
        l_temp_rec.ITEM_PARENT   := l_rms_rec.ITEM_PARENT;
      END IF;
      IF l_mi_rec.ITEM_GRANDPARENT_mi <> 'Y' THEN
        l_temp_rec.ITEM_GRANDPARENT   := l_rms_rec.ITEM_GRANDPARENT;
      END IF;
      IF l_mi_rec.PACK_IND_mi <> 'Y' THEN
        l_temp_rec.PACK_IND   := l_rms_rec.PACK_IND;
      END IF;
      IF l_mi_rec.ITEM_LEVEL_mi <> 'Y' THEN
        l_temp_rec.ITEM_LEVEL   := l_rms_rec.ITEM_LEVEL;
      END IF;
      IF l_mi_rec.TRAN_LEVEL_mi <> 'Y' THEN
        l_temp_rec.TRAN_LEVEL   := l_rms_rec.TRAN_LEVEL;
      END IF;
      IF l_mi_rec.ITEM_AGGREGATE_IND_mi <> 'Y' THEN
        l_temp_rec.ITEM_AGGREGATE_IND   := l_rms_rec.ITEM_AGGREGATE_IND;
      END IF;
      IF l_mi_rec.DIFF_1_mi <> 'Y' THEN
        l_temp_rec.DIFF_1   := l_rms_rec.DIFF_1;
      END IF;
      IF l_mi_rec.DIFF_1_AGGREGATE_IND_mi <> 'Y' THEN
        l_temp_rec.DIFF_1_AGGREGATE_IND   := l_rms_rec.DIFF_1_AGGREGATE_IND;
      END IF;
      IF l_mi_rec.DIFF_2_mi <> 'Y' THEN
        l_temp_rec.DIFF_2   := l_rms_rec.DIFF_2;
      END IF;
      IF l_mi_rec.DIFF_2_AGGREGATE_IND_mi <> 'Y' THEN
        l_temp_rec.DIFF_2_AGGREGATE_IND   := l_rms_rec.DIFF_2_AGGREGATE_IND;
      END IF;
      IF l_mi_rec.DIFF_3_mi <> 'Y' THEN
        l_temp_rec.DIFF_3   := l_rms_rec.DIFF_3;
      END IF;
      IF l_mi_rec.DIFF_3_AGGREGATE_IND_mi <> 'Y' THEN
        l_temp_rec.DIFF_3_AGGREGATE_IND   := l_rms_rec.DIFF_3_AGGREGATE_IND;
      END IF;
      IF l_mi_rec.DIFF_4_mi <> 'Y' THEN
        l_temp_rec.DIFF_4   := l_rms_rec.DIFF_4;
      END IF;
      IF l_mi_rec.DIFF_4_AGGREGATE_IND_mi <> 'Y' THEN
        l_temp_rec.DIFF_4_AGGREGATE_IND   := l_rms_rec.DIFF_4_AGGREGATE_IND;
      END IF;
      IF l_mi_rec.DEPT_mi <> 'Y' THEN
        l_temp_rec.DEPT   := l_rms_rec.DEPT;
      END IF;
      IF l_mi_rec.CLASS_mi <> 'Y' THEN
        l_temp_rec.CLASS   := l_rms_rec.CLASS;
      END IF;
      IF l_mi_rec.SUBCLASS_mi <> 'Y' THEN
        l_temp_rec.SUBCLASS   := l_rms_rec.SUBCLASS;
      END IF;
      IF l_mi_rec.STATUS_mi <> 'Y' THEN
        l_temp_rec.STATUS   := l_rms_rec.STATUS;
      END IF;
      IF l_mi_rec.ITEM_DESC_mi <> 'Y' THEN
        l_temp_rec.ITEM_DESC   := l_rms_rec.ITEM_DESC;
      END IF;
      IF l_mi_rec.ITEM_DESC_SECONDARY_mi <> 'Y' THEN
        l_temp_rec.ITEM_DESC_SECONDARY   := l_rms_rec.ITEM_DESC_SECONDARY;
      END IF;
      IF l_mi_rec.SHORT_DESC_mi <> 'Y' THEN
        l_temp_rec.SHORT_DESC   := l_rms_rec.SHORT_DESC;
      END IF;
      IF l_mi_rec.PRIMARY_REF_ITEM_IND_mi <> 'Y' THEN
        l_temp_rec.PRIMARY_REF_ITEM_IND   := l_rms_rec.PRIMARY_REF_ITEM_IND;
      END IF;
      IF l_mi_rec.COST_ZONE_GROUP_ID_mi <> 'Y' THEN
        l_temp_rec.COST_ZONE_GROUP_ID   := l_rms_rec.COST_ZONE_GROUP_ID;
      END IF;
      IF l_mi_rec.STANDARD_UOM_mi <> 'Y' THEN
        l_temp_rec.STANDARD_UOM   := l_rms_rec.STANDARD_UOM;
      END IF;
      IF l_mi_rec.UOM_CONV_FACTOR_mi <> 'Y' THEN
        l_temp_rec.UOM_CONV_FACTOR   := l_rms_rec.UOM_CONV_FACTOR;
      END IF;
      IF l_mi_rec.PACKAGE_SIZE_mi <> 'Y' THEN
        l_temp_rec.PACKAGE_SIZE   := l_rms_rec.PACKAGE_SIZE;
      END IF;
      IF l_mi_rec.PACKAGE_UOM_mi <> 'Y' THEN
        l_temp_rec.PACKAGE_UOM   := l_rms_rec.PACKAGE_UOM;
      END IF;
      IF l_mi_rec.MERCHANDISE_IND_mi <> 'Y' THEN
        l_temp_rec.MERCHANDISE_IND   := l_rms_rec.MERCHANDISE_IND;
      END IF;
      IF l_mi_rec.STORE_ORD_MULT_mi <> 'Y' THEN
        l_temp_rec.STORE_ORD_MULT   := l_rms_rec.STORE_ORD_MULT;
      END IF;
      IF l_mi_rec.FORECAST_IND_mi <> 'Y' THEN
        l_temp_rec.FORECAST_IND   := l_rms_rec.FORECAST_IND;
      END IF;
      IF l_mi_rec.MFG_REC_RETAIL_mi <> 'Y' THEN
        l_temp_rec.MFG_REC_RETAIL   := l_rms_rec.MFG_REC_RETAIL;
      END IF;
      IF l_mi_rec.RETAIL_LABEL_TYPE_mi <> 'Y' THEN
        l_temp_rec.RETAIL_LABEL_TYPE   := l_rms_rec.RETAIL_LABEL_TYPE;
      END IF;
      IF l_mi_rec.RETAIL_LABEL_VALUE_mi <> 'Y' THEN
        l_temp_rec.RETAIL_LABEL_VALUE   := l_rms_rec.RETAIL_LABEL_VALUE;
      END IF;
      IF l_mi_rec.HANDLING_TEMP_mi <> 'Y' THEN
        l_temp_rec.HANDLING_TEMP   := l_rms_rec.HANDLING_TEMP;
      END IF;
      IF l_mi_rec.HANDLING_SENSITIVITY_mi <> 'Y' THEN
        l_temp_rec.HANDLING_SENSITIVITY   := l_rms_rec.HANDLING_SENSITIVITY;
      END IF;
      IF l_mi_rec.CATCH_WEIGHT_IND_mi <> 'Y' THEN
        l_temp_rec.CATCH_WEIGHT_IND   := l_rms_rec.CATCH_WEIGHT_IND;
      END IF;
      IF l_mi_rec.WASTE_TYPE_mi <> 'Y' THEN
        l_temp_rec.WASTE_TYPE   := l_rms_rec.WASTE_TYPE;
      END IF;
      IF l_mi_rec.WASTE_PCT_mi <> 'Y' THEN
        l_temp_rec.WASTE_PCT   := l_rms_rec.WASTE_PCT;
      END IF;
      IF l_mi_rec.DEFAULT_WASTE_PCT_mi <> 'Y' THEN
        l_temp_rec.DEFAULT_WASTE_PCT   := l_rms_rec.DEFAULT_WASTE_PCT;
      END IF;
      IF l_mi_rec.CONST_DIMEN_IND_mi <> 'Y' THEN
        l_temp_rec.CONST_DIMEN_IND   := l_rms_rec.CONST_DIMEN_IND;
      END IF;
      IF l_mi_rec.SIMPLE_PACK_IND_mi <> 'Y' THEN
        l_temp_rec.SIMPLE_PACK_IND   := l_rms_rec.SIMPLE_PACK_IND;
      END IF;
      IF l_mi_rec.CONTAINS_INNER_IND_mi <> 'Y' THEN
        l_temp_rec.CONTAINS_INNER_IND   := l_rms_rec.CONTAINS_INNER_IND;
      END IF;
      IF l_mi_rec.SELLABLE_IND_mi <> 'Y' THEN
        l_temp_rec.SELLABLE_IND   := l_rms_rec.SELLABLE_IND;
      END IF;
      IF l_mi_rec.ORDERABLE_IND_mi <> 'Y' THEN
        l_temp_rec.ORDERABLE_IND   := l_rms_rec.ORDERABLE_IND;
      END IF;
      IF l_mi_rec.PACK_TYPE_mi <> 'Y' THEN
        l_temp_rec.PACK_TYPE   := l_rms_rec.PACK_TYPE;
      END IF;
      IF l_mi_rec.ORDER_AS_TYPE_mi <> 'Y' THEN
        l_temp_rec.ORDER_AS_TYPE   := l_rms_rec.ORDER_AS_TYPE;
      END IF;
      IF l_mi_rec.COMMENTS_mi <> 'Y' THEN
        l_temp_rec.COMMENTS   := l_rms_rec.COMMENTS;
      END IF;
      IF l_mi_rec.ITEM_SERVICE_LEVEL_mi <> 'Y' THEN
        l_temp_rec.ITEM_SERVICE_LEVEL   := l_rms_rec.ITEM_SERVICE_LEVEL;
      END IF;
      IF l_mi_rec.GIFT_WRAP_IND_mi <> 'Y' THEN
        l_temp_rec.GIFT_WRAP_IND   := l_rms_rec.GIFT_WRAP_IND;
      END IF;
      IF l_mi_rec.SHIP_ALONE_IND_mi <> 'Y' THEN
        l_temp_rec.SHIP_ALONE_IND   := l_rms_rec.SHIP_ALONE_IND;
      END IF;
      IF l_mi_rec.ITEM_XFORM_IND_mi <> 'Y' THEN
        l_temp_rec.ITEM_XFORM_IND   := l_rms_rec.ITEM_XFORM_IND;
      END IF;
      IF l_mi_rec.INVENTORY_IND_mi <> 'Y' THEN
        l_temp_rec.INVENTORY_IND   := l_rms_rec.INVENTORY_IND;
      END IF;
      IF l_mi_rec.ORDER_TYPE_mi <> 'Y' THEN
        l_temp_rec.ORDER_TYPE   := l_rms_rec.ORDER_TYPE;
      END IF;
      IF l_mi_rec.SALE_TYPE_mi <> 'Y' THEN
        l_temp_rec.SALE_TYPE   := l_rms_rec.SALE_TYPE;
      END IF;
      IF l_mi_rec.DEPOSIT_ITEM_TYPE_mi <> 'Y' THEN
        l_temp_rec.DEPOSIT_ITEM_TYPE   := l_rms_rec.DEPOSIT_ITEM_TYPE;
      END IF;
      IF l_mi_rec.CONTAINER_ITEM_mi <> 'Y' THEN
        l_temp_rec.CONTAINER_ITEM   := l_rms_rec.CONTAINER_ITEM;
      END IF;
      IF l_mi_rec.DEPOSIT_IN_PRICE_PER_UOM_mi <> 'Y' THEN
        l_temp_rec.DEPOSIT_IN_PRICE_PER_UOM   := l_rms_rec.DEPOSIT_IN_PRICE_PER_UOM;
      END IF;
      IF l_mi_rec.AIP_CASE_TYPE_mi <> 'Y' THEN
        l_temp_rec.AIP_CASE_TYPE   := l_rms_rec.AIP_CASE_TYPE;
      END IF;
      IF l_mi_rec.PERISHABLE_IND_mi <> 'Y' THEN
        l_temp_rec.PERISHABLE_IND   := l_rms_rec.PERISHABLE_IND;
      END IF;
      IF l_mi_rec.NOTIONAL_PACK_IND_mi <> 'Y' THEN
        l_temp_rec.NOTIONAL_PACK_IND   := l_rms_rec.NOTIONAL_PACK_IND;
      END IF;
      IF l_mi_rec.SOH_INQUIRY_AT_PACK_IND_mi <> 'Y' THEN
        l_temp_rec.SOH_INQUIRY_AT_PACK_IND   := l_rms_rec.SOH_INQUIRY_AT_PACK_IND;
      END IF;
      IF l_mi_rec.PRODUCT_CLASSIFICATION_mi <> 'Y' THEN
        l_temp_rec.PRODUCT_CLASSIFICATION   := l_rms_rec.PRODUCT_CLASSIFICATION;
      END IF;
      IF l_mi_rec.BRAND_NAME_mi <> 'Y' THEN
        l_temp_rec.BRAND_NAME   := l_rms_rec.BRAND_NAME;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,IM_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_temp_rec.orig_ref_no);
      l_temp_rec.ITEM_NUMBER_TYPE := NVL( l_temp_rec.ITEM_NUMBER_TYPE,l_default_rec.ITEM_NUMBER_TYPE);
      l_temp_rec.FORMAT_ID := NVL( l_temp_rec.FORMAT_ID,l_default_rec.FORMAT_ID);
      l_temp_rec.PREFIX := NVL( l_temp_rec.PREFIX,l_default_rec.PREFIX);
      l_temp_rec.ITEM_PARENT := NVL( l_temp_rec.ITEM_PARENT,l_default_rec.ITEM_PARENT);
      l_temp_rec.ITEM_GRANDPARENT := NVL( l_temp_rec.ITEM_GRANDPARENT,l_default_rec.ITEM_GRANDPARENT);
      l_temp_rec.PACK_IND := NVL( l_temp_rec.PACK_IND,l_default_rec.PACK_IND);
      l_temp_rec.ITEM_LEVEL := NVL( l_temp_rec.ITEM_LEVEL,l_default_rec.ITEM_LEVEL);
      l_temp_rec.TRAN_LEVEL := NVL( l_temp_rec.TRAN_LEVEL,l_default_rec.TRAN_LEVEL);
      l_temp_rec.ITEM_AGGREGATE_IND := NVL( l_temp_rec.ITEM_AGGREGATE_IND,l_default_rec.ITEM_AGGREGATE_IND);
      l_temp_rec.DIFF_1 := NVL( l_temp_rec.DIFF_1,l_default_rec.DIFF_1);
      l_temp_rec.DIFF_1_AGGREGATE_IND := NVL( l_temp_rec.DIFF_1_AGGREGATE_IND,l_default_rec.DIFF_1_AGGREGATE_IND);
      l_temp_rec.DIFF_2 := NVL( l_temp_rec.DIFF_2,l_default_rec.DIFF_2);
      l_temp_rec.DIFF_2_AGGREGATE_IND := NVL( l_temp_rec.DIFF_2_AGGREGATE_IND,l_default_rec.DIFF_2_AGGREGATE_IND);
      l_temp_rec.DIFF_3 := NVL( l_temp_rec.DIFF_3,l_default_rec.DIFF_3);
      l_temp_rec.DIFF_3_AGGREGATE_IND := NVL( l_temp_rec.DIFF_3_AGGREGATE_IND,l_default_rec.DIFF_3_AGGREGATE_IND);
      l_temp_rec.DIFF_4 := NVL( l_temp_rec.DIFF_4,l_default_rec.DIFF_4);
      l_temp_rec.DIFF_4_AGGREGATE_IND := NVL( l_temp_rec.DIFF_4_AGGREGATE_IND,l_default_rec.DIFF_4_AGGREGATE_IND);
      l_temp_rec.DEPT := NVL( l_temp_rec.DEPT,l_default_rec.DEPT);
      l_temp_rec.CLASS := NVL( l_temp_rec.CLASS,l_default_rec.CLASS);
      l_temp_rec.SUBCLASS := NVL( l_temp_rec.SUBCLASS,l_default_rec.SUBCLASS);
      l_temp_rec.STATUS := NVL( l_temp_rec.STATUS,l_default_rec.STATUS);
      l_temp_rec.ITEM_DESC := NVL( l_temp_rec.ITEM_DESC,l_default_rec.ITEM_DESC);
      l_temp_rec.ITEM_DESC_SECONDARY := NVL( l_temp_rec.ITEM_DESC_SECONDARY,l_default_rec.ITEM_DESC_SECONDARY);
      l_temp_rec.SHORT_DESC := NVL( l_temp_rec.SHORT_DESC,l_default_rec.SHORT_DESC);
      l_temp_rec.PRIMARY_REF_ITEM_IND := NVL( l_temp_rec.PRIMARY_REF_ITEM_IND,l_default_rec.PRIMARY_REF_ITEM_IND);
      l_temp_rec.COST_ZONE_GROUP_ID := NVL( l_temp_rec.COST_ZONE_GROUP_ID,l_default_rec.COST_ZONE_GROUP_ID);
      l_temp_rec.STANDARD_UOM := NVL( l_temp_rec.STANDARD_UOM,l_default_rec.STANDARD_UOM);
      l_temp_rec.UOM_CONV_FACTOR := NVL( l_temp_rec.UOM_CONV_FACTOR,l_default_rec.UOM_CONV_FACTOR);
      l_temp_rec.PACKAGE_SIZE := NVL( l_temp_rec.PACKAGE_SIZE,l_default_rec.PACKAGE_SIZE);
      l_temp_rec.PACKAGE_UOM := NVL( l_temp_rec.PACKAGE_UOM,l_default_rec.PACKAGE_UOM);
      l_temp_rec.MERCHANDISE_IND := NVL( l_temp_rec.MERCHANDISE_IND,l_default_rec.MERCHANDISE_IND);
      l_temp_rec.STORE_ORD_MULT := NVL( l_temp_rec.STORE_ORD_MULT,l_default_rec.STORE_ORD_MULT);
      l_temp_rec.FORECAST_IND := NVL( l_temp_rec.FORECAST_IND,l_default_rec.FORECAST_IND);
      l_temp_rec.ORIGINAL_RETAIL := NVL( l_temp_rec.ORIGINAL_RETAIL,l_default_rec.ORIGINAL_RETAIL);
      l_temp_rec.MFG_REC_RETAIL := NVL( l_temp_rec.MFG_REC_RETAIL,l_default_rec.MFG_REC_RETAIL);
      l_temp_rec.RETAIL_LABEL_TYPE := NVL( l_temp_rec.RETAIL_LABEL_TYPE,l_default_rec.RETAIL_LABEL_TYPE);
      l_temp_rec.RETAIL_LABEL_VALUE := NVL( l_temp_rec.RETAIL_LABEL_VALUE,l_default_rec.RETAIL_LABEL_VALUE);
      l_temp_rec.HANDLING_TEMP := NVL( l_temp_rec.HANDLING_TEMP,l_default_rec.HANDLING_TEMP);
      l_temp_rec.HANDLING_SENSITIVITY := NVL( l_temp_rec.HANDLING_SENSITIVITY,l_default_rec.HANDLING_SENSITIVITY);
      l_temp_rec.CATCH_WEIGHT_IND := NVL( l_temp_rec.CATCH_WEIGHT_IND,l_default_rec.CATCH_WEIGHT_IND);
      l_temp_rec.WASTE_TYPE := NVL( l_temp_rec.WASTE_TYPE,l_default_rec.WASTE_TYPE);
      l_temp_rec.WASTE_PCT := NVL( l_temp_rec.WASTE_PCT,l_default_rec.WASTE_PCT);
      l_temp_rec.DEFAULT_WASTE_PCT := NVL( l_temp_rec.DEFAULT_WASTE_PCT,l_default_rec.DEFAULT_WASTE_PCT);
      l_temp_rec.CONST_DIMEN_IND := NVL( l_temp_rec.CONST_DIMEN_IND,l_default_rec.CONST_DIMEN_IND);
      l_temp_rec.SIMPLE_PACK_IND := NVL( l_temp_rec.SIMPLE_PACK_IND,l_default_rec.SIMPLE_PACK_IND);
      l_temp_rec.CONTAINS_INNER_IND := NVL( l_temp_rec.CONTAINS_INNER_IND,l_default_rec.CONTAINS_INNER_IND);
      l_temp_rec.SELLABLE_IND := NVL( l_temp_rec.SELLABLE_IND,l_default_rec.SELLABLE_IND);
      l_temp_rec.ORDERABLE_IND := NVL( l_temp_rec.ORDERABLE_IND,l_default_rec.ORDERABLE_IND);
      l_temp_rec.PACK_TYPE := NVL( l_temp_rec.PACK_TYPE,l_default_rec.PACK_TYPE);
      l_temp_rec.ORDER_AS_TYPE := NVL( l_temp_rec.ORDER_AS_TYPE,l_default_rec.ORDER_AS_TYPE);
      l_temp_rec.COMMENTS := NVL( l_temp_rec.COMMENTS,l_default_rec.COMMENTS);
      l_temp_rec.ITEM_SERVICE_LEVEL := NVL( l_temp_rec.ITEM_SERVICE_LEVEL,l_default_rec.ITEM_SERVICE_LEVEL);
      l_temp_rec.GIFT_WRAP_IND := NVL( l_temp_rec.GIFT_WRAP_IND,l_default_rec.GIFT_WRAP_IND);
      l_temp_rec.SHIP_ALONE_IND := NVL( l_temp_rec.SHIP_ALONE_IND,l_default_rec.SHIP_ALONE_IND);
      l_temp_rec.ITEM_XFORM_IND := NVL( l_temp_rec.ITEM_XFORM_IND,l_default_rec.ITEM_XFORM_IND);
      l_temp_rec.INVENTORY_IND := NVL( l_temp_rec.INVENTORY_IND,l_default_rec.INVENTORY_IND);
      l_temp_rec.ORDER_TYPE := NVL( l_temp_rec.ORDER_TYPE,l_default_rec.ORDER_TYPE);
      l_temp_rec.SALE_TYPE := NVL( l_temp_rec.SALE_TYPE,l_default_rec.SALE_TYPE);
      l_temp_rec.DEPOSIT_ITEM_TYPE := NVL( l_temp_rec.DEPOSIT_ITEM_TYPE,l_default_rec.DEPOSIT_ITEM_TYPE);
      l_temp_rec.CONTAINER_ITEM := NVL( l_temp_rec.CONTAINER_ITEM,l_default_rec.CONTAINER_ITEM);
      l_temp_rec.DEPOSIT_IN_PRICE_PER_UOM := NVL( l_temp_rec.DEPOSIT_IN_PRICE_PER_UOM,l_default_rec.DEPOSIT_IN_PRICE_PER_UOM);
      l_temp_rec.AIP_CASE_TYPE := NVL( l_temp_rec.AIP_CASE_TYPE,l_default_rec.AIP_CASE_TYPE);
      l_temp_rec.PERISHABLE_IND := NVL( l_temp_rec.PERISHABLE_IND,l_default_rec.PERISHABLE_IND);
      l_temp_rec.NOTIONAL_PACK_IND := NVL( l_temp_rec.NOTIONAL_PACK_IND,l_default_rec.NOTIONAL_PACK_IND);
      l_temp_rec.SOH_INQUIRY_AT_PACK_IND := NVL( l_temp_rec.SOH_INQUIRY_AT_PACK_IND,l_default_rec.SOH_INQUIRY_AT_PACK_IND);
      l_temp_rec.PRODUCT_CLASSIFICATION := NVL( l_temp_rec.PRODUCT_CLASSIFICATION,l_default_rec.PRODUCT_CLASSIFICATION);
      l_temp_rec.BRAND_NAME := NVL( l_temp_rec.BRAND_NAME,l_default_rec.BRAND_NAME);
      l_temp_rec.NEXT_UPD_ID := NVL( l_temp_rec.NEXT_UPD_ID,l_default_rec.NEXT_UPD_ID);
      l_temp_rec.ORIG_REF_NO := NVL( l_temp_rec.ORIG_REF_NO,l_default_rec.ORIG_REF_NO);
      l_temp_rec.PRE_RESERVED_IND := NVL( l_temp_rec.PRE_RESERVED_IND,l_default_rec.PRE_RESERVED_IND);
      l_temp_rec.DIFF_FINALIZED_IND := NVL( l_temp_rec.DIFF_FINALIZED_IND,l_default_rec.DIFF_FINALIZED_IND);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,IM_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- Check is mfg_rec_retail is NULL for rpm_ind 'N'
    select rpm_ind
      into L_rpm_ind
      from system_options;

    IF L_rpm_ind = 'N' then
      IF l_temp_rec.item_level = 1 and l_temp_rec.MFG_REC_RETAIL IS NULL THEN
         L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED','mfg_rec_retail');
         write_s9t_error(I_file_id,IM_sheet,rec.row_seq,NULL,NULL,L_error_msg);
         l_error := true;
      END IF;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_MASTER VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,IM_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_MASTER
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
;
END process_s9t_IM;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_IM_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                            I_process_id   IN   SVC_ITEM_MASTER_TL.PROCESS_ID%TYPE)
IS

   TYPE SVC_COL_TYP is TABLE OF SVC_ITEM_MASTER_TL%ROWTYPE;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   L_error_code    NUMBER;
   L_stg_exists    BOOLEAN;
   L_error         BOOLEAN       := FALSE;
   L_pk_columns    VARCHAR2(255) := 'lang and item';
   L_table         VARCHAR2(30)  := 'SVC_ITEM_MASTER_TL';
   svc_ins_col     SVC_COL_TYP   := NEW SVC_COL_TYP();
   svc_upd_col     SVC_COL_TYP   := NEW SVC_COL_TYP();
   L_process_id    SVC_ITEM_MASTER_TL.PROCESS_ID%TYPE;
   L_temp_rec      SVC_ITEM_MASTER_TL%ROWTYPE;
   L_default_rec   SVC_ITEM_MASTER_TL%ROWTYPE;
   L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
   dml_errors      EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);

   cursor C_MANDATORY_IND is
      select short_desc_mi,
             item_desc_secondary_mi,
             item_desc_mi,
             item_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = ITEM_INDUCT_SQL.TEMPLATE_KEY
                 and wksht_key = 'ITEM_MASTER_TL')
       pivot (MAX(mandatory) as mi FOR (column_key) in ('SHORT_DESC' as short_desc,
                                                        'ITEM_DESC_SECONDARY' as item_desc_secondary,
                                                        'ITEM_DESC' as item_desc,
                                                        'ITEM' as item,
                                                        'LANG' as lang));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;

BEGIN
   -- Get default values.
   FOR rec in (select short_desc_dv,
                      item_desc_secondary_dv,
                      item_desc_dv,
                      item_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = ITEM_INDUCT_SQL.TEMPLATE_KEY
                          and wksht_key = 'ITEM_MASTER_TL')
                pivot (MAX(default_value) as dv FOR (column_key) in ('SHORT_DESC' as short_desc,
                                                                     'ITEM_DESC_SECONDARY' as item_desc_secondary,
                                                                     'ITEM_DESC' as item_desc,
                                                                     'ITEM' as item,
                                                                     'LANG' as lang))
              ) LOOP
      BEGIN
         L_default_rec.short_desc := rec.short_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_MASTER_TL',
                            NULL,
                            'SHORT_DESC',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.item_desc_secondary := rec.item_desc_secondary_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_MASTER_TL',
                            NULL,
                            'ITEM_DESC_SECONDARY',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.item_desc := rec.item_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_MASTER_TL',
                            NULL,
                            'ITEM_DESC',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.item := rec.item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_MASTER_TL',
                            NULL,
                            'ITEM',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_MASTER_TL',
                            NULL,
                            'LANG',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

   --get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   --
   -- XXADEO_RMS: Assigned get_sheet_name_trans(IC_sheet) value to variable L_sheet_name
   --
   L_sheet_name := GET_SHEET_NAME_TRANS(IMTL_sheet);
   --
   FOR rec in (select r.get_cell(imtl$action)                   as action,
                      r.get_cell(imtl$short_desc)               as short_desc,
                      r.get_cell(imtl$item_desc_secondary)      as item_desc_secondary,
                      r.get_cell(imtl$item_desc)                as item_desc,
                      r.get_cell(imtl$item)                     as item,
                      r.get_cell(imtl$lang)                     as lang,
                      r.get_row_seq()                           as row_seq
                 from s9t_folder sf,
                      table(sf.s9t_file_obj.sheets) ss,
                      table(ss.s9t_rows) r
                where sf.file_id = I_file_id
                  and ss.sheet_name = L_sheet_name) LOOP

      L_temp_rec.process_id           := I_process_id;
      L_temp_rec.chunk_id             := 1;
      L_temp_rec.row_seq              := rec.row_seq;
      L_temp_rec.process$status       := 'N';
      L_temp_rec.create_id            := GET_USER;
      L_temp_rec.last_update_id       := GET_USER;
      L_temp_rec.create_datetime      := SYSDATE;
      L_temp_rec.last_update_datetime := SYSDATE;
      L_error                         := FALSE;
      ---
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ISTL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.short_desc := rec.short_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IMTL_sheet,
                            rec.row_seq,
                            'SHORT_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item_desc_secondary := rec.item_desc_secondary;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IMTL_sheet,
                            rec.row_seq,
                            'ITEM_DESC_SECONDARY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item_desc := rec.item_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IMTL_sheet,
                            rec.row_seq,
                            'ITEM_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item := rec.item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IMTL_sheet,
                            rec.row_seq,
                            'ITEM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IMTL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      ---
      if NOT L_error then
         DECLARE
            L_stg_rec      SVC_ITEM_MASTER_TL%ROWTYPE;
            L_rms_rec      ITEM_MASTER_TL%ROWTYPE;
            L_rms_exists   BOOLEAN;

            cursor C_STG_REC is
               select *
                 from svc_item_master_tl
                where item = L_temp_rec.item
                  and lang = L_temp_rec.lang;

            cursor C_RMS_REC is
               select *
                 from item_master_tl
                where item = L_temp_rec.item
                  and lang = L_temp_rec.lang;

         BEGIN
            open C_STG_REC;
            fetch C_STG_REC into L_stg_rec;
            if C_STG_REC%FOUND then
               L_stg_exists := TRUE;
            else
               L_stg_exists := FALSE;
            end if;
            close C_STG_REC;
            ---
            open C_RMS_REC;
            fetch C_RMS_REC into L_rms_rec;
            if C_RMS_REC%FOUND then
               L_rms_exists := TRUE;
            else
               L_rms_exists := FALSE;
            end if;
            close C_RMS_REC;
            -- delete record from staging if it is already uploaded to rms.
            if L_stg_exists and L_stg_rec.process$status = 'P' and GET_PRC_DEST(L_stg_rec.process_id) = 'RMS' then
               delete from svc_item_master_tl
                where process_id = L_stg_rec.process_id
                  and row_seq = L_stg_rec.row_seq;
               L_stg_exists := FALSE;
            end if;
            --resolve new value for action column
            if L_stg_exists then
               -- if it is a create record and record already exists in staging, log an error
               if L_temp_rec.action = CORESVC_ITEM.ACTION_NEW then
                  if L_stg_rec.process$status = 'P' then
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS',
                                                       L_pk_columns);
                  else
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                       L_pk_columns);
                  end if;
                  WRITE_S9T_ERROR (I_file_id,
                                   IMTL_sheet,
                                   L_temp_rec.row_seq,
                                   NULL,
                                   NULL,
                                   L_error_msg );
                  L_error := TRUE;
               else
                  -- if record does not exist in staging then keep the user provided value
                  L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,
                                                      L_temp_rec.action);
               end if;
            end if;
         END;
      end if;
      -- apply defaults if it is a create record and record does not exist in staging already
      if rec.action = CORESVC_ITEM.ACTION_NEW and NOT L_error and NOT L_stg_exists then
         L_temp_rec.lang := NVL( L_temp_rec.lang, L_default_rec.lang);
         L_temp_rec.item := NVL(L_temp_rec.item, L_default_rec.item);
         L_temp_rec.item_desc := NVL( L_temp_rec.item_desc, L_default_rec.item_desc);
         L_temp_rec.item_desc_secondary := NVL( L_temp_rec.item_desc_secondary, L_default_rec.item_desc_secondary);
         L_temp_rec.short_desc := NVL( L_temp_rec.short_desc, L_default_rec.short_desc);
      end if;
      -- check pk cols. log error if any pk col is NULL.
      if NOT (L_temp_rec.item is NOT NULL and L_temp_rec.lang is NOT NULL) then
         L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                           L_pk_columns);
         WRITE_S9T_ERROR(I_file_id,
                         IMTL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         L_error_msg);
         L_error := TRUE;
      end if;
      -- if no error so far then add temp record to insert or update collection depending upon
      -- whether record already exists in staging or not.
      if NOT L_error then
         if L_stg_exists then
            svc_upd_col.extend();
            svc_upd_col(svc_upd_col.COUNT()) := L_temp_rec;
         else
            svc_ins_col.extend();
            svc_ins_col(svc_ins_col.COUNT()) := L_temp_rec;
         end if;
      end if;
   END LOOP;

   -- flush insert collection
   BEGIN
      FORALL i in 1..svc_ins_col.COUNT SAVE EXCEPTIONS
      insert into svc_item_master_tl values svc_ins_col(i);

   EXCEPTION
      when DML_ERRORS then
         for i in 1..sql%bulk_exceptions.COUNT loop
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                 L_pk_columns,
                                                 L_table);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            IMTL_sheet,
                            svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         end loop;
   END;

   -- flush update collection
   FORALL i in 1..svc_upd_col.COUNT
      update svc_item_master_tl
         set row = svc_upd_col(i)
       where item = svc_upd_col(i).item
         and lang = svc_upd_col(i).lang;

END PROCESS_S9T_IM_TL;
-------------------------------------------------------------------------------------
PROCEDURE process_s9t_IS
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_SUPPLIER.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_SUPPLIER%rowtype;
  l_temp_rec SVC_ITEM_SUPPLIER%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_SUPPLIER.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_SUPPLIER%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT PRIMARY_CASE_SIZE_mi,
      CONCESSION_RATE_mi,
      DIRECT_SHIP_IND_mi,
      SUPP_DISCONTINUE_DATE_mi,
      INNER_NAME_mi,
      CASE_NAME_mi,
      PALLET_NAME_mi,
      SUPP_DIFF_4_mi,
      SUPP_DIFF_3_mi,
      SUPP_DIFF_2_mi,
      SUPP_DIFF_1_mi,
      CONSIGNMENT_RATE_mi,
      SUPP_LABEL_mi,
      VPN_mi,
      PRIMARY_SUPP_IND_mi,
      SUPPLIER_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_SUPPLIER'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'PRIMARY_CASE_SIZE' AS PRIMARY_CASE_SIZE, 'CONCESSION_RATE' AS CONCESSION_RATE, 'DIRECT_SHIP_IND' AS DIRECT_SHIP_IND, 'SUPP_DISCONTINUE_DATE' AS SUPP_DISCONTINUE_DATE, 'INNER_NAME' AS INNER_NAME, 'CASE_NAME' AS CASE_NAME, 'PALLET_NAME' AS PALLET_NAME, 'SUPP_DIFF_4' AS SUPP_DIFF_4, 'SUPP_DIFF_3' AS SUPP_DIFF_3, 'SUPP_DIFF_2' AS SUPP_DIFF_2, 'SUPP_DIFF_1' AS SUPP_DIFF_1, 'CONSIGNMENT_RATE' AS CONSIGNMENT_RATE, 'SUPP_LABEL' AS SUPP_LABEL, 'VPN' AS VPN, 'PRIMARY_SUPP_IND' AS PRIMARY_SUPP_IND, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item and supplier';
  L_table    VARCHAR2(30) := 'SVC_ITEM_SUPPLIER';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT PRIMARY_CASE_SIZE_dv,
    CONCESSION_RATE_dv,
    DIRECT_SHIP_IND_dv,
    SUPP_DISCONTINUE_DATE_dv,
    INNER_NAME_dv,
    CASE_NAME_dv,
    PALLET_NAME_dv,
    SUPP_DIFF_4_dv,
    SUPP_DIFF_3_dv,
    SUPP_DIFF_2_dv,
    SUPP_DIFF_1_dv,
    CONSIGNMENT_RATE_dv,
    SUPP_LABEL_dv,
    VPN_dv,
    PRIMARY_SUPP_IND_dv,
    SUPPLIER_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_SUPPLIER'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'PRIMARY_CASE_SIZE' AS PRIMARY_CASE_SIZE, 'CONCESSION_RATE' AS CONCESSION_RATE, 'DIRECT_SHIP_IND' AS DIRECT_SHIP_IND, 'SUPP_DISCONTINUE_DATE' AS SUPP_DISCONTINUE_DATE, 'INNER_NAME' AS INNER_NAME, 'CASE_NAME' AS CASE_NAME, 'PALLET_NAME' AS PALLET_NAME, 'SUPP_DIFF_4' AS SUPP_DIFF_4, 'SUPP_DIFF_3' AS SUPP_DIFF_3, 'SUPP_DIFF_2' AS SUPP_DIFF_2, 'SUPP_DIFF_1' AS SUPP_DIFF_1, 'CONSIGNMENT_RATE' AS CONSIGNMENT_RATE, 'SUPP_LABEL' AS SUPP_LABEL, 'VPN' AS VPN, 'PRIMARY_SUPP_IND' AS PRIMARY_SUPP_IND, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.PRIMARY_CASE_SIZE := rec.PRIMARY_CASE_SIZE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'PRIMARY_CASE_SIZE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.CONCESSION_RATE := rec.CONCESSION_RATE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'CONCESSION_RATE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.DIRECT_SHIP_IND := rec.DIRECT_SHIP_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'DIRECT_SHIP_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_DISCONTINUE_DATE := rec.SUPP_DISCONTINUE_DATE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'SUPP_DISCONTINUE_DATE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.INNER_NAME := rec.INNER_NAME_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'INNER_NAME','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.CASE_NAME := rec.CASE_NAME_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'CASE_NAME','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PALLET_NAME := rec.PALLET_NAME_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'PALLET_NAME','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_DIFF_4 := rec.SUPP_DIFF_4_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'SUPP_DIFF_4','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_DIFF_3 := rec.SUPP_DIFF_3_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'SUPP_DIFF_3','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_DIFF_2 := rec.SUPP_DIFF_2_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'SUPP_DIFF_2','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_DIFF_1 := rec.SUPP_DIFF_1_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'SUPP_DIFF_1','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.CONSIGNMENT_RATE := rec.CONSIGNMENT_RATE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'CONSIGNMENT_RATE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_LABEL := rec.SUPP_LABEL_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'SUPP_LABEL','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.VPN := rec.VPN_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'VPN','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PRIMARY_SUPP_IND := rec.PRIMARY_SUPP_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'PRIMARY_SUPP_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPPLIER := rec.SUPPLIER_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'SUPPLIER','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPPLIER',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(IS_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(IS_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(IS$Action)      AS Action,
    r.get_cell(IS$PRIMARY_CASE_SIZE)     AS PRIMARY_CASE_SIZE,
    r.get_cell(IS$CONCESSION_RATE)   AS CONCESSION_RATE,
    r.get_cell(IS$DIRECT_SHIP_IND)   AS DIRECT_SHIP_IND,
    r.get_cell(IS$SUPP_DISCONTINUE_DATE) AS SUPP_DISCONTINUE_DATE,
    r.get_cell(IS$INNER_NAME)        AS INNER_NAME,
    r.get_cell(IS$CASE_NAME)         AS CASE_NAME,
    r.get_cell(IS$PALLET_NAME)       AS PALLET_NAME,
    r.get_cell(IS$SUPP_DIFF_4)       AS SUPP_DIFF_4,
    r.get_cell(IS$SUPP_DIFF_3)       AS SUPP_DIFF_3,
    r.get_cell(IS$SUPP_DIFF_2)       AS SUPP_DIFF_2,
    r.get_cell(IS$SUPP_DIFF_1)       AS SUPP_DIFF_1,
    r.get_cell(IS$CONSIGNMENT_RATE)  AS CONSIGNMENT_RATE,
    r.get_cell(IS$SUPP_LABEL)        AS SUPP_LABEL,
    r.get_cell(IS$VPN)           AS VPN,
    r.get_cell(IS$PRIMARY_SUPP_IND)  AS PRIMARY_SUPP_IND,
    r.get_cell(IS$SUPPLIER)      AS SUPPLIER,
    r.get_cell(IS$ITEM)          AS ITEM,
    r.get_row_seq()          AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PRIMARY_CASE_SIZE := rec.PRIMARY_CASE_SIZE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'PRIMARY_CASE_SIZE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.CONCESSION_RATE := rec.CONCESSION_RATE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'CONCESSION_RATE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DIRECT_SHIP_IND := rec.DIRECT_SHIP_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'DIRECT_SHIP_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_DISCONTINUE_DATE := rec.SUPP_DISCONTINUE_DATE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'SUPP_DISCONTINUE_DATE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.INNER_NAME := rec.INNER_NAME;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'INNER_NAME',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.CASE_NAME := rec.CASE_NAME;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'CASE_NAME',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PALLET_NAME := rec.PALLET_NAME;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'PALLET_NAME',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_DIFF_4 := rec.SUPP_DIFF_4;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'SUPP_DIFF_4',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_DIFF_3 := rec.SUPP_DIFF_3;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'SUPP_DIFF_3',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_DIFF_2 := rec.SUPP_DIFF_2;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'SUPP_DIFF_2',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_DIFF_1 := rec.SUPP_DIFF_1;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'SUPP_DIFF_1',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.CONSIGNMENT_RATE := rec.CONSIGNMENT_RATE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'CONSIGNMENT_RATE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_LABEL := rec.SUPP_LABEL;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'SUPP_LABEL',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.VPN := rec.VPN;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'VPN',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PRIMARY_SUPP_IND := rec.PRIMARY_SUPP_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'PRIMARY_SUPP_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPPLIER := rec.SUPPLIER;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'SUPPLIER',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_ITEM_SUPPLIER%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_ITEM_SUPPLIER
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
;
    l_rms_rec ITEM_SUPPLIER%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM ITEM_SUPPLIER
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_ITEM_SUPPLIER
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.PRIMARY_SUPP_IND_mi <> 'Y' THEN
        l_temp_rec.PRIMARY_SUPP_IND   := l_stg_rec.PRIMARY_SUPP_IND;
      END IF;
      IF l_mi_rec.VPN_mi <> 'Y' THEN
        l_temp_rec.VPN   := l_stg_rec.VPN;
      END IF;
      IF l_mi_rec.SUPP_LABEL_mi <> 'Y' THEN
        l_temp_rec.SUPP_LABEL   := l_stg_rec.SUPP_LABEL;
      END IF;
      IF l_mi_rec.CONSIGNMENT_RATE_mi <> 'Y' THEN
        l_temp_rec.CONSIGNMENT_RATE   := l_stg_rec.CONSIGNMENT_RATE;
      END IF;
      IF l_mi_rec.SUPP_DIFF_1_mi <> 'Y' THEN
        l_temp_rec.SUPP_DIFF_1   := l_stg_rec.SUPP_DIFF_1;
      END IF;
      IF l_mi_rec.SUPP_DIFF_2_mi <> 'Y' THEN
        l_temp_rec.SUPP_DIFF_2   := l_stg_rec.SUPP_DIFF_2;
      END IF;
      IF l_mi_rec.SUPP_DIFF_3_mi <> 'Y' THEN
        l_temp_rec.SUPP_DIFF_3   := l_stg_rec.SUPP_DIFF_3;
      END IF;
      IF l_mi_rec.SUPP_DIFF_4_mi <> 'Y' THEN
        l_temp_rec.SUPP_DIFF_4   := l_stg_rec.SUPP_DIFF_4;
      END IF;
      IF l_mi_rec.PALLET_NAME_mi <> 'Y' THEN
        l_temp_rec.PALLET_NAME   := l_stg_rec.PALLET_NAME;
      END IF;
      IF l_mi_rec.CASE_NAME_mi <> 'Y' THEN
        l_temp_rec.CASE_NAME   := l_stg_rec.CASE_NAME;
      END IF;
      IF l_mi_rec.INNER_NAME_mi <> 'Y' THEN
        l_temp_rec.INNER_NAME   := l_stg_rec.INNER_NAME;
      END IF;
      IF l_mi_rec.SUPP_DISCONTINUE_DATE_mi <> 'Y' THEN
        l_temp_rec.SUPP_DISCONTINUE_DATE   := l_stg_rec.SUPP_DISCONTINUE_DATE;
      END IF;
      IF l_mi_rec.DIRECT_SHIP_IND_mi <> 'Y' THEN
        l_temp_rec.DIRECT_SHIP_IND   := l_stg_rec.DIRECT_SHIP_IND;
      END IF;
      IF l_mi_rec.CONCESSION_RATE_mi <> 'Y' THEN
        l_temp_rec.CONCESSION_RATE   := l_stg_rec.CONCESSION_RATE;
      END IF;
      IF l_mi_rec.PRIMARY_CASE_SIZE_mi <> 'Y' THEN
        l_temp_rec.PRIMARY_CASE_SIZE   := l_stg_rec.PRIMARY_CASE_SIZE;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.PRIMARY_SUPP_IND_mi <> 'Y' THEN
        l_temp_rec.PRIMARY_SUPP_IND   := l_rms_rec.PRIMARY_SUPP_IND;
      END IF;
      IF l_mi_rec.VPN_mi <> 'Y' THEN
        l_temp_rec.VPN   := l_rms_rec.VPN;
      END IF;
      IF l_mi_rec.SUPP_LABEL_mi <> 'Y' THEN
        l_temp_rec.SUPP_LABEL   := l_rms_rec.SUPP_LABEL;
      END IF;
      IF l_mi_rec.CONSIGNMENT_RATE_mi <> 'Y' THEN
        l_temp_rec.CONSIGNMENT_RATE   := l_rms_rec.CONSIGNMENT_RATE;
      END IF;
      IF l_mi_rec.SUPP_DIFF_1_mi <> 'Y' THEN
        l_temp_rec.SUPP_DIFF_1   := l_rms_rec.SUPP_DIFF_1;
      END IF;
      IF l_mi_rec.SUPP_DIFF_2_mi <> 'Y' THEN
        l_temp_rec.SUPP_DIFF_2   := l_rms_rec.SUPP_DIFF_2;
      END IF;
      IF l_mi_rec.SUPP_DIFF_3_mi <> 'Y' THEN
        l_temp_rec.SUPP_DIFF_3   := l_rms_rec.SUPP_DIFF_3;
      END IF;
      IF l_mi_rec.SUPP_DIFF_4_mi <> 'Y' THEN
        l_temp_rec.SUPP_DIFF_4   := l_rms_rec.SUPP_DIFF_4;
      END IF;
      IF l_mi_rec.PALLET_NAME_mi <> 'Y' THEN
        l_temp_rec.PALLET_NAME   := l_rms_rec.PALLET_NAME;
      END IF;
      IF l_mi_rec.CASE_NAME_mi <> 'Y' THEN
        l_temp_rec.CASE_NAME   := l_rms_rec.CASE_NAME;
      END IF;
      IF l_mi_rec.INNER_NAME_mi <> 'Y' THEN
        l_temp_rec.INNER_NAME   := l_rms_rec.INNER_NAME;
      END IF;
      IF l_mi_rec.SUPP_DISCONTINUE_DATE_mi <> 'Y' THEN
        l_temp_rec.SUPP_DISCONTINUE_DATE   := l_rms_rec.SUPP_DISCONTINUE_DATE;
      END IF;
      IF l_mi_rec.DIRECT_SHIP_IND_mi <> 'Y' THEN
        l_temp_rec.DIRECT_SHIP_IND   := l_rms_rec.DIRECT_SHIP_IND;
      END IF;
      IF l_mi_rec.CONCESSION_RATE_mi <> 'Y' THEN
        l_temp_rec.CONCESSION_RATE   := l_rms_rec.CONCESSION_RATE;
      END IF;
      IF l_mi_rec.PRIMARY_CASE_SIZE_mi <> 'Y' THEN
        l_temp_rec.PRIMARY_CASE_SIZE   := l_rms_rec.PRIMARY_CASE_SIZE;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,IS_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.SUPPLIER := NVL( l_temp_rec.SUPPLIER,l_default_rec.SUPPLIER);
      l_temp_rec.PRIMARY_SUPP_IND := NVL( l_temp_rec.PRIMARY_SUPP_IND,l_default_rec.PRIMARY_SUPP_IND);
      l_temp_rec.VPN := NVL( l_temp_rec.VPN,l_default_rec.VPN);
      l_temp_rec.SUPP_LABEL := NVL( l_temp_rec.SUPP_LABEL,l_default_rec.SUPP_LABEL);
      l_temp_rec.CONSIGNMENT_RATE := NVL( l_temp_rec.CONSIGNMENT_RATE,l_default_rec.CONSIGNMENT_RATE);
      l_temp_rec.SUPP_DIFF_1 := NVL( l_temp_rec.SUPP_DIFF_1,l_default_rec.SUPP_DIFF_1);
      l_temp_rec.SUPP_DIFF_2 := NVL( l_temp_rec.SUPP_DIFF_2,l_default_rec.SUPP_DIFF_2);
      l_temp_rec.SUPP_DIFF_3 := NVL( l_temp_rec.SUPP_DIFF_3,l_default_rec.SUPP_DIFF_3);
      l_temp_rec.SUPP_DIFF_4 := NVL( l_temp_rec.SUPP_DIFF_4,l_default_rec.SUPP_DIFF_4);
      l_temp_rec.PALLET_NAME := NVL( l_temp_rec.PALLET_NAME,l_default_rec.PALLET_NAME);
      l_temp_rec.CASE_NAME := NVL( l_temp_rec.CASE_NAME,l_default_rec.CASE_NAME);
      l_temp_rec.INNER_NAME := NVL( l_temp_rec.INNER_NAME,l_default_rec.INNER_NAME);
      l_temp_rec.SUPP_DISCONTINUE_DATE := NVL( l_temp_rec.SUPP_DISCONTINUE_DATE,l_default_rec.SUPP_DISCONTINUE_DATE);
      l_temp_rec.DIRECT_SHIP_IND := NVL( l_temp_rec.DIRECT_SHIP_IND,l_default_rec.DIRECT_SHIP_IND);
      l_temp_rec.CONCESSION_RATE := NVL( l_temp_rec.CONCESSION_RATE,l_default_rec.CONCESSION_RATE);
      l_temp_rec.PRIMARY_CASE_SIZE := NVL( l_temp_rec.PRIMARY_CASE_SIZE,l_default_rec.PRIMARY_CASE_SIZE);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.SUPPLIER IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,IS_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
           svc_upd_col.extend();
           svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
           svc_ins_col.extend();
           svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_SUPPLIER VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
        L_error_code := NULL;
        L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,IS_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_SUPPLIER
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND SUPPLIER = svc_upd_col(i).SUPPLIER
;
END process_s9t_IS;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_IS_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                            I_process_id   IN   SVC_ITEM_SUPPLIER_TL.PROCESS_ID%TYPE)
IS

   TYPE SVC_COL_TYP is TABLE OF SVC_ITEM_SUPPLIER_TL%ROWTYPE;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   L_error_code    NUMBER;
   L_stg_exists    BOOLEAN;
   L_error         BOOLEAN       := FALSE;
   L_pk_columns    VARCHAR2(255) := 'lang, item and supplier';
   L_table         VARCHAR2(30)  := 'SVC_ITEM_SUPPLIER_TL';
   svc_ins_col     SVC_COL_TYP   := NEW SVC_COL_TYP();
   svc_upd_col     SVC_COL_TYP   := NEW SVC_COL_TYP();
   L_process_id    SVC_ITEM_SUPPLIER_TL.PROCESS_ID%TYPE;
   L_temp_rec      SVC_ITEM_SUPPLIER_TL%ROWTYPE;
   L_default_rec   SVC_ITEM_SUPPLIER_TL%ROWTYPE;
   L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
   dml_errors      EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);

   cursor C_MANDATORY_IND is
      select supp_diff_4_mi,
             supp_diff_3_mi,
             supp_diff_2_mi,
             supp_diff_1_mi,
             supp_label_mi,
             supplier_mi,
             item_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = ITEM_INDUCT_SQL.TEMPLATE_KEY
                 and wksht_key = 'ITEM_SUPPLIER_TL')
       pivot (MAX(mandatory) as mi FOR (column_key) in ('SUPP_DIFF_4' as supp_diff_4,
                                                        'SUPP_DIFF_3' as supp_diff_3,
                                                        'SUPP_DIFF_2' as supp_diff_2,
                                                        'SUPP_DIFF_1' as supp_diff_1,
                                                        'SUPP_LABEL' as supp_label,
                                                        'SUPPLIER' as supplier,
                                                        'ITEM' as item,
                                                        'LANG' as lang));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;

BEGIN
   -- Get default values.
   FOR rec in (select supp_diff_4_dv,
                      supp_diff_3_dv,
                      supp_diff_2_dv,
                      supp_diff_1_dv,
                      supp_label_dv,
                      supplier_dv,
                      item_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = ITEM_INDUCT_SQL.TEMPLATE_KEY
                          and wksht_key = 'ITEM_SUPPLIER_TL')
                pivot (MAX(default_value) as dv FOR (column_key) in ('SUPP_DIFF_4' as supp_diff_4,
                                                                     'SUPP_DIFF_3' as supp_diff_3,
                                                                     'SUPP_DIFF_2' as supp_diff_2,
                                                                     'SUPP_DIFF_1' as supp_diff_1,
                                                                     'SUPP_LABEL' as supp_label,
                                                                     'SUPPLIER' as supplier,
                                                                     'ITEM' as item,
                                                                     'LANG' as lang))
              ) LOOP
      BEGIN
         L_default_rec.supp_diff_4 := rec.supp_diff_4_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_SUPPLIER_TL',
                            NULL,
                            'SUPP_DIFF_4',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.supp_diff_3 := rec.supp_diff_3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_SUPPLIER_TL',
                            NULL,
                            'SUPP_DIFF_3',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.supp_diff_2 := rec.supp_diff_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_SUPPLIER_TL',
                            NULL,
                            'SUPP_DIFF_2',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.supp_diff_1 := rec.supp_diff_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_SUPPLIER_TL',
                            NULL,
                            'SUPP_DIFF_1',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.supp_label := rec.supp_label_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_SUPPLIER_TL',
                            NULL,
                            'SUPP_LABEL',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.supplier := rec.supplier_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_SUPPLIER_TL',
                            NULL,
                            'SUPPLIER',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.item := rec.item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_SUPPLIER_TL',
                            NULL,
                            'ITEM',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_SUPPLIER_TL',
                            NULL,
                            'LANG',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

   --get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   --
   -- XXADEO_RMS: Assigned get_sheet_name_trans(ISTL_sheet) value to variable L_sheet_name
   --
   L_sheet_name := GET_SHEET_NAME_TRANS(ISTL_sheet);
   --
   FOR rec in (select r.get_cell(istl$action)           as action,
                      r.get_cell(istl$supp_diff_4)      as supp_diff_4,
                      r.get_cell(istl$supp_diff_3)      as supp_diff_3,
                      r.get_cell(istl$supp_diff_2)      as supp_diff_2,
                      r.get_cell(istl$supp_diff_1)      as supp_diff_1,
                      r.get_cell(istl$supp_label)       as supp_label,
                      r.get_cell(istl$supplier)         as supplier,
                      r.get_cell(istl$item)             as item,
                      r.get_cell(istl$lang)             as lang,
                      r.get_row_seq()                   as row_seq
                 from s9t_folder sf,
                      table(sf.s9t_file_obj.sheets) ss,
                      table(ss.s9t_rows) r
                where sf.file_id = I_file_id
                  and ss.sheet_name = L_sheet_name) LOOP

      L_temp_rec.process_id           := I_process_id;
      L_temp_rec.chunk_id             := 1;
      L_temp_rec.row_seq              := rec.row_seq;
      L_temp_rec.process$status       := 'N';
      L_temp_rec.create_id            := GET_USER;
      L_temp_rec.last_update_id       := GET_USER;
      L_temp_rec.create_datetime      := SYSDATE;
      L_temp_rec.last_update_datetime := SYSDATE;
      L_error                         := FALSE;
      ---
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ISTL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.supp_diff_4 := rec.supp_diff_4;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ISTL_sheet,
                            rec.row_seq,
                            'SUPP_DIFF_4',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.supp_diff_3 := rec.supp_diff_3;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ISTL_sheet,
                            rec.row_seq,
                            'SUPP_DIFF_3',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.supp_diff_2 := rec.supp_diff_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ISTL_sheet,
                            rec.row_seq,
                            'SUPP_DIFF_2',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.supp_diff_1 := rec.supp_diff_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ISTL_sheet,
                            rec.row_seq,
                            'SUPP_DIFF_1',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.supp_label := rec.supp_label;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ISTL_sheet,
                            rec.row_seq,
                            'SUPP_LABEL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.supplier := rec.supplier;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ISTL_sheet,
                            rec.row_seq,
                            'SUPPLIER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item := rec.item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ISTL_sheet,
                            rec.row_seq,
                            'ITEM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ISTL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      ---
      if NOT L_error then
         DECLARE
            L_stg_rec      SVC_ITEM_SUPPLIER_TL%ROWTYPE;
            L_rms_rec      ITEM_SUPPLIER_TL%ROWTYPE;
            L_rms_exists   BOOLEAN;

            cursor C_STG_REC is
               select *
                 from svc_item_supplier_tl
                where item = L_temp_rec.item
                  and lang = L_temp_rec.lang
                  and supplier = L_temp_rec.supplier;

            cursor C_RMS_REC is
               select *
                 from item_supplier_tl
                where item = L_temp_rec.item
                  and lang = L_temp_rec.lang
                  and supplier = L_temp_rec.supplier;

         BEGIN
            open C_STG_REC;
            fetch C_STG_REC into L_stg_rec;
            if C_STG_REC%FOUND then
               L_stg_exists := TRUE;
            else
               L_stg_exists := FALSE;
            end if;
            close C_STG_REC;
            ---
            open C_RMS_REC;
            fetch C_RMS_REC into L_rms_rec;
            if C_RMS_REC%FOUND then
               L_rms_exists := TRUE;
            else
               L_rms_exists := FALSE;
            end if;
            close C_RMS_REC;
            -- delete record from staging if it is already uploaded to rms.
            if L_stg_exists and L_stg_rec.process$status = 'P' and GET_PRC_DEST(L_stg_rec.process_id) = 'RMS' then
               delete from svc_item_supplier_tl
                where process_id = L_stg_rec.process_id
                  and row_seq = L_stg_rec.row_seq;
               L_stg_exists := FALSE;
            end if;
            --resolve new value for action column
            if L_stg_exists then
               -- if it is a create record and record already exists in staging, log an error
               if L_temp_rec.action = CORESVC_ITEM.ACTION_NEW then
                  if L_stg_rec.process$status = 'P' then
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS',
                                                       L_pk_columns);
                  else
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                       L_pk_columns);
                  end if;
                  WRITE_S9T_ERROR (I_file_id,
                                   ISTL_sheet,
                                   L_temp_rec.row_seq,
                                   NULL,
                                   NULL,
                                   L_error_msg );
                  L_error := TRUE;
               else
                  -- if record does not exist in staging then keep the user provided value
                  L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,
                                                      L_temp_rec.action);
               end if;
            end if;
         END;
      end if;
      -- apply defaults if it is a create record and record does not exist in staging already
      if rec.action = CORESVC_ITEM.ACTION_NEW and NOT L_error and NOT L_stg_exists then
         L_temp_rec.lang := NVL( L_temp_rec.lang, L_default_rec.lang);
         L_temp_rec.item := NVL(L_temp_rec.item, L_default_rec.item);
         L_temp_rec.supplier := NVL(L_temp_rec.supplier, L_default_rec.supplier);
         L_temp_rec.supp_label := NVL( L_temp_rec.supp_label, L_default_rec.supp_label);
         L_temp_rec.supp_diff_1 := NVL( L_temp_rec.supp_diff_1, L_default_rec.supp_diff_1);
         L_temp_rec.supp_diff_2 := NVL( L_temp_rec.supp_diff_2, L_default_rec.supp_diff_2);
         L_temp_rec.supp_diff_3 := NVL( L_temp_rec.supp_diff_3, L_default_rec.supp_diff_3);
         L_temp_rec.supp_diff_4 := NVL( L_temp_rec.supp_diff_4, L_default_rec.supp_diff_4);
      end if;
      -- check pk cols. log error if any pk col is NULL.
      if NOT (L_temp_rec.item is NOT NULL and L_temp_rec.lang is NOT NULL and L_temp_rec.supplier is NOT NULL) then
         L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                           L_pk_columns);
         WRITE_S9T_ERROR(I_file_id,
                         ISTL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         L_error_msg);
         L_error := TRUE;
      end if;
      -- if no error so far then add temp record to insert or update collection depending upon
      -- whether record already exists in staging or not.
      if NOT L_error then
         if L_stg_exists then
            svc_upd_col.extend();
            svc_upd_col(svc_upd_col.COUNT()) := L_temp_rec;
         else
            svc_ins_col.extend();
            svc_ins_col(svc_ins_col.COUNT()) := L_temp_rec;
         end if;
      end if;
   END LOOP;

   -- flush insert collection
   BEGIN
      FORALL i in 1..svc_ins_col.COUNT SAVE EXCEPTIONS
      insert into svc_item_supplier_tl values svc_ins_col(i);

   EXCEPTION
      when DML_ERRORS then
         for i in 1..sql%bulk_exceptions.COUNT loop
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                 L_pk_columns,
                                                 L_table);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            ISTL_sheet,
                            svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         end loop;
   END;

   -- flush update collection
   FORALL i in 1..svc_upd_col.COUNT
      update svc_item_supplier_tl
         set row = svc_upd_col(i)
       where item = svc_upd_col(i).item
         and lang = svc_upd_col(i).lang
         and supplier = svc_upd_col(i).supplier;

END PROCESS_S9T_IS_TL;
-------------------------------------------------------------------------------------
PROCEDURE process_s9t_ISC
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_SUPP_COUNTRY.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_SUPP_COUNTRY%rowtype;
  l_temp_rec SVC_ITEM_SUPP_COUNTRY%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_SUPP_COUNTRY.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_SUPP_COUNTRY%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT MIN_TOLERANCE_mi,
      MAX_TOLERANCE_mi,
      TOLERANCE_TYPE_mi,
      COST_UOM_mi,
      SUPP_HIER_LVL_3_mi,
      SUPP_HIER_LVL_2_mi,
      SUPP_HIER_LVL_1_mi,
      HI_mi,
      TI_mi,
      DEFAULT_UOP_mi,
      PRIMARY_COUNTRY_IND_mi,
      PRIMARY_SUPP_IND_mi,
      PACKING_METHOD_mi,
      MAX_ORDER_QTY_mi,
      MIN_ORDER_QTY_mi,
      ROUND_TO_PALLET_PCT_mi,
      ROUND_TO_LAYER_PCT_mi,
      ROUND_TO_CASE_PCT_mi,
      ROUND_TO_INNER_PCT_mi,
      ROUND_LVL_mi,
      INNER_PACK_SIZE_mi,
      SUPP_PACK_SIZE_mi,
      PICKUP_LEAD_TIME_mi,
      LEAD_TIME_mi,
      UNIT_COST_mi,
      ORIGIN_COUNTRY_ID_mi,
      SUPPLIER_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_SUPP_COUNTRY'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'MIN_TOLERANCE' AS MIN_TOLERANCE, 'MAX_TOLERANCE' AS MAX_TOLERANCE, 'TOLERANCE_TYPE' AS TOLERANCE_TYPE, 'COST_UOM' AS COST_UOM, 'SUPP_HIER_LVL_3' AS SUPP_HIER_LVL_3, 'SUPP_HIER_LVL_2' AS SUPP_HIER_LVL_2, 'SUPP_HIER_LVL_1' AS SUPP_HIER_LVL_1, 'HI' AS HI, 'TI' AS TI, 'DEFAULT_UOP' AS DEFAULT_UOP, 'PRIMARY_COUNTRY_IND' AS PRIMARY_COUNTRY_IND, 'PRIMARY_SUPP_IND' AS PRIMARY_SUPP_IND, 'PACKING_METHOD' AS PACKING_METHOD, 'MAX_ORDER_QTY' AS MAX_ORDER_QTY, 'MIN_ORDER_QTY' AS MIN_ORDER_QTY, 'ROUND_TO_PALLET_PCT' AS ROUND_TO_PALLET_PCT, 'ROUND_TO_LAYER_PCT' AS ROUND_TO_LAYER_PCT, 'ROUND_TO_CASE_PCT' AS ROUND_TO_CASE_PCT, 'ROUND_TO_INNER_PCT' AS ROUND_TO_INNER_PCT,
    'ROUND_LVL' AS ROUND_LVL, 'INNER_PACK_SIZE' AS INNER_PACK_SIZE, 'SUPP_PACK_SIZE' AS SUPP_PACK_SIZE, 'PICKUP_LEAD_TIME' AS PICKUP_LEAD_TIME, 'LEAD_TIME' AS LEAD_TIME, 'UNIT_COST' AS UNIT_COST, 'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item, supplier and orgin country id';
  L_table    VARCHAR2(30) := 'SVC_ITEM_SUPP_COUNTRY';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT MIN_TOLERANCE_dv,
    MAX_TOLERANCE_dv,
    TOLERANCE_TYPE_dv,
    COST_UOM_dv,
    SUPP_HIER_LVL_3_dv,
    SUPP_HIER_LVL_2_dv,
    SUPP_HIER_LVL_1_dv,
    HI_dv,
    TI_dv,
    DEFAULT_UOP_dv,
    PRIMARY_COUNTRY_IND_dv,
    PRIMARY_SUPP_IND_dv,
    PACKING_METHOD_dv,
    MAX_ORDER_QTY_dv,
    MIN_ORDER_QTY_dv,
    ROUND_TO_PALLET_PCT_dv,
    ROUND_TO_LAYER_PCT_dv,
    ROUND_TO_CASE_PCT_dv,
    ROUND_TO_INNER_PCT_dv,
    ROUND_LVL_dv,
    INNER_PACK_SIZE_dv,
    SUPP_PACK_SIZE_dv,
    PICKUP_LEAD_TIME_dv,
    LEAD_TIME_dv,
    UNIT_COST_dv,
    ORIGIN_COUNTRY_ID_dv,
    SUPPLIER_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_SUPP_COUNTRY'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'MIN_TOLERANCE' AS MIN_TOLERANCE, 'MAX_TOLERANCE' AS MAX_TOLERANCE, 'TOLERANCE_TYPE' AS TOLERANCE_TYPE, 'COST_UOM' AS COST_UOM, 'SUPP_HIER_LVL_3' AS SUPP_HIER_LVL_3, 'SUPP_HIER_LVL_2' AS SUPP_HIER_LVL_2, 'SUPP_HIER_LVL_1' AS SUPP_HIER_LVL_1, 'HI' AS HI, 'TI' AS TI, 'DEFAULT_UOP' AS DEFAULT_UOP, 'PRIMARY_COUNTRY_IND' AS PRIMARY_COUNTRY_IND, 'PRIMARY_SUPP_IND' AS PRIMARY_SUPP_IND, 'PACKING_METHOD' AS PACKING_METHOD, 'MAX_ORDER_QTY' AS MAX_ORDER_QTY, 'MIN_ORDER_QTY' AS MIN_ORDER_QTY, 'ROUND_TO_PALLET_PCT' AS ROUND_TO_PALLET_PCT, 'ROUND_TO_LAYER_PCT' AS ROUND_TO_LAYER_PCT, 'ROUND_TO_CASE_PCT' AS ROUND_TO_CASE_PCT, 'ROUND_TO_INNER_PCT' AS ROUND_TO_INNER_PCT,
    'ROUND_LVL' AS ROUND_LVL, 'INNER_PACK_SIZE' AS INNER_PACK_SIZE, 'SUPP_PACK_SIZE' AS SUPP_PACK_SIZE, 'PICKUP_LEAD_TIME' AS PICKUP_LEAD_TIME, 'LEAD_TIME' AS LEAD_TIME, 'UNIT_COST' AS UNIT_COST, 'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.MIN_TOLERANCE := rec.MIN_TOLERANCE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'MIN_TOLERANCE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.MAX_TOLERANCE := rec.MAX_TOLERANCE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'MAX_TOLERANCE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.TOLERANCE_TYPE := rec.TOLERANCE_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'TOLERANCE_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COST_UOM := rec.COST_UOM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'COST_UOM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_HIER_LVL_3 := rec.SUPP_HIER_LVL_3_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'SUPP_HIER_LVL_3','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_HIER_LVL_2 := rec.SUPP_HIER_LVL_2_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'SUPP_HIER_LVL_2','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_HIER_LVL_1 := rec.SUPP_HIER_LVL_1_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'SUPP_HIER_LVL_1','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.HI := rec.HI_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'HI','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.TI := rec.TI_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'TI','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.DEFAULT_UOP := rec.DEFAULT_UOP_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'DEFAULT_UOP','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PRIMARY_COUNTRY_IND := rec.PRIMARY_COUNTRY_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'PRIMARY_COUNTRY_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PRIMARY_SUPP_IND := rec.PRIMARY_SUPP_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'PRIMARY_SUPP_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PACKING_METHOD := rec.PACKING_METHOD_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'PACKING_METHOD','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.MAX_ORDER_QTY := rec.MAX_ORDER_QTY_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'MAX_ORDER_QTY','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.MIN_ORDER_QTY := rec.MIN_ORDER_QTY_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'MIN_ORDER_QTY','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ROUND_TO_PALLET_PCT := rec.ROUND_TO_PALLET_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'ROUND_TO_PALLET_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ROUND_TO_LAYER_PCT := rec.ROUND_TO_LAYER_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'ROUND_TO_LAYER_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ROUND_TO_CASE_PCT := rec.ROUND_TO_CASE_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'ROUND_TO_CASE_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ROUND_TO_INNER_PCT := rec.ROUND_TO_INNER_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'ROUND_TO_INNER_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ROUND_LVL := rec.ROUND_LVL_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'ROUND_LVL','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.INNER_PACK_SIZE := rec.INNER_PACK_SIZE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'INNER_PACK_SIZE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_PACK_SIZE := rec.SUPP_PACK_SIZE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'SUPP_PACK_SIZE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PICKUP_LEAD_TIME := rec.PICKUP_LEAD_TIME_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'PICKUP_LEAD_TIME','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.LEAD_TIME := rec.LEAD_TIME_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'LEAD_TIME','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.UNIT_COST := rec.UNIT_COST_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'UNIT_COST','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'ORIGIN_COUNTRY_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPPLIER := rec.SUPPLIER_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'SUPPLIER','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(ISC_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(ISC_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(ISC$Action)     AS Action,
    r.get_cell(ISC$MIN_TOLERANCE)    AS MIN_TOLERANCE,
    r.get_cell(ISC$MAX_TOLERANCE)    AS MAX_TOLERANCE,
    r.get_cell(ISC$TOLERANCE_TYPE)   AS TOLERANCE_TYPE,
    NVL(uom_sql.get_uom(r.get_cell(ISC$COST_UOM),'Upload'),r.get_cell(ISC$COST_UOM)) AS COST_UOM,
    r.get_cell(ISC$SUPP_HIER_LVL_3)  AS SUPP_HIER_LVL_3,
    r.get_cell(ISC$SUPP_HIER_LVL_2)  AS SUPP_HIER_LVL_2,
    r.get_cell(ISC$SUPP_HIER_LVL_1)  AS SUPP_HIER_LVL_1,
    r.get_cell(ISC$HI)           AS HI,
    r.get_cell(ISC$TI)           AS TI,
    NVL(uom_sql.get_uom(r.get_cell(ISC$DEFAULT_UOP),'Upload'),r.get_cell(ISC$DEFAULT_UOP))      AS DEFAULT_UOP,
    r.get_cell(ISC$PRIMARY_COUNTRY_IND)  AS PRIMARY_COUNTRY_IND,
    r.get_cell(ISC$PRIMARY_SUPP_IND)     AS PRIMARY_SUPP_IND,
    r.get_cell(ISC$PACKING_METHOD)   AS PACKING_METHOD,
    r.get_cell(ISC$MAX_ORDER_QTY)    AS MAX_ORDER_QTY,
    r.get_cell(ISC$MIN_ORDER_QTY)    AS MIN_ORDER_QTY,
    r.get_cell(ISC$ROUND_TO_PALLET_PCT)  AS ROUND_TO_PALLET_PCT,
    r.get_cell(ISC$ROUND_TO_LAYER_PCT)   AS ROUND_TO_LAYER_PCT,
    r.get_cell(ISC$ROUND_TO_CASE_PCT)    AS ROUND_TO_CASE_PCT,
    r.get_cell(ISC$ROUND_TO_INNER_PCT)   AS ROUND_TO_INNER_PCT,
    r.get_cell(ISC$ROUND_LVL)        AS ROUND_LVL,
    r.get_cell(ISC$INNER_PACK_SIZE)  AS INNER_PACK_SIZE,
    r.get_cell(ISC$SUPP_PACK_SIZE)   AS SUPP_PACK_SIZE,
    r.get_cell(ISC$PICKUP_LEAD_TIME)     AS PICKUP_LEAD_TIME,
    r.get_cell(ISC$LEAD_TIME)        AS LEAD_TIME,
    r.get_cell(ISC$UNIT_COST)        AS UNIT_COST,
    r.get_cell(ISC$ORIGIN_COUNTRY_ID)    AS ORIGIN_COUNTRY_ID,
    r.get_cell(ISC$SUPPLIER)         AS SUPPLIER,
    r.get_cell(ISC$ITEM)         AS ITEM,
    r.get_row_seq()          AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.MIN_TOLERANCE := rec.MIN_TOLERANCE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'MIN_TOLERANCE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.MAX_TOLERANCE := rec.MAX_TOLERANCE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'MAX_TOLERANCE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.TOLERANCE_TYPE := rec.TOLERANCE_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'TOLERANCE_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COST_UOM := rec.COST_UOM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'COST_UOM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_HIER_LVL_3 := rec.SUPP_HIER_LVL_3;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'SUPP_HIER_LVL_3',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_HIER_LVL_2 := rec.SUPP_HIER_LVL_2;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'SUPP_HIER_LVL_2',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_HIER_LVL_1 := rec.SUPP_HIER_LVL_1;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'SUPP_HIER_LVL_1',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.HI := rec.HI;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'HI',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.TI := rec.TI;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'TI',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DEFAULT_UOP := rec.DEFAULT_UOP;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'DEFAULT_UOP',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PRIMARY_COUNTRY_IND := rec.PRIMARY_COUNTRY_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'PRIMARY_COUNTRY_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PRIMARY_SUPP_IND := rec.PRIMARY_SUPP_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'PRIMARY_SUPP_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PACKING_METHOD := rec.PACKING_METHOD;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'PACKING_METHOD',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.MAX_ORDER_QTY := rec.MAX_ORDER_QTY;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'MAX_ORDER_QTY',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.MIN_ORDER_QTY := rec.MIN_ORDER_QTY;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'MIN_ORDER_QTY',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ROUND_TO_PALLET_PCT := rec.ROUND_TO_PALLET_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'ROUND_TO_PALLET_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ROUND_TO_LAYER_PCT := rec.ROUND_TO_LAYER_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'ROUND_TO_LAYER_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ROUND_TO_CASE_PCT := rec.ROUND_TO_CASE_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'ROUND_TO_CASE_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ROUND_TO_INNER_PCT := rec.ROUND_TO_INNER_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'ROUND_TO_INNER_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ROUND_LVL := rec.ROUND_LVL;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'ROUND_LVL',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.INNER_PACK_SIZE := rec.INNER_PACK_SIZE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'INNER_PACK_SIZE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_PACK_SIZE := rec.SUPP_PACK_SIZE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'SUPP_PACK_SIZE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PICKUP_LEAD_TIME := rec.PICKUP_LEAD_TIME;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'PICKUP_LEAD_TIME',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.LEAD_TIME := rec.LEAD_TIME;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'LEAD_TIME',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.UNIT_COST := rec.UNIT_COST;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'UNIT_COST',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'ORIGIN_COUNTRY_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPPLIER := rec.SUPPLIER;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'SUPPLIER',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_ITEM_SUPP_COUNTRY%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_ITEM_SUPP_COUNTRY
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND ORIGIN_COUNTRY_ID = l_temp_rec.ORIGIN_COUNTRY_ID
;
    l_rms_rec ITEM_SUPP_COUNTRY%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM ITEM_SUPP_COUNTRY
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND ORIGIN_COUNTRY_ID = l_temp_rec.ORIGIN_COUNTRY_ID
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_ITEM_SUPP_COUNTRY
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.UNIT_COST_mi <> 'Y' THEN
        l_temp_rec.UNIT_COST   := l_stg_rec.UNIT_COST;
      END IF;
      IF l_mi_rec.LEAD_TIME_mi <> 'Y' THEN
        l_temp_rec.LEAD_TIME   := l_stg_rec.LEAD_TIME;
      END IF;
      IF l_mi_rec.PICKUP_LEAD_TIME_mi <> 'Y' THEN
        l_temp_rec.PICKUP_LEAD_TIME   := l_stg_rec.PICKUP_LEAD_TIME;
      END IF;
      IF l_mi_rec.SUPP_PACK_SIZE_mi <> 'Y' THEN
        l_temp_rec.SUPP_PACK_SIZE   := l_stg_rec.SUPP_PACK_SIZE;
      END IF;
      IF l_mi_rec.INNER_PACK_SIZE_mi <> 'Y' THEN
        l_temp_rec.INNER_PACK_SIZE   := l_stg_rec.INNER_PACK_SIZE;
      END IF;
      IF l_mi_rec.ROUND_LVL_mi <> 'Y' THEN
        l_temp_rec.ROUND_LVL   := l_stg_rec.ROUND_LVL;
      END IF;
      IF l_mi_rec.ROUND_TO_INNER_PCT_mi <> 'Y' THEN
        l_temp_rec.ROUND_TO_INNER_PCT   := l_stg_rec.ROUND_TO_INNER_PCT;
      END IF;
      IF l_mi_rec.ROUND_TO_CASE_PCT_mi <> 'Y' THEN
        l_temp_rec.ROUND_TO_CASE_PCT   := l_stg_rec.ROUND_TO_CASE_PCT;
      END IF;
      IF l_mi_rec.ROUND_TO_LAYER_PCT_mi <> 'Y' THEN
        l_temp_rec.ROUND_TO_LAYER_PCT   := l_stg_rec.ROUND_TO_LAYER_PCT;
      END IF;
      IF l_mi_rec.ROUND_TO_PALLET_PCT_mi <> 'Y' THEN
        l_temp_rec.ROUND_TO_PALLET_PCT   := l_stg_rec.ROUND_TO_PALLET_PCT;
      END IF;
      IF l_mi_rec.MIN_ORDER_QTY_mi <> 'Y' THEN
        l_temp_rec.MIN_ORDER_QTY   := l_stg_rec.MIN_ORDER_QTY;
      END IF;
      IF l_mi_rec.MAX_ORDER_QTY_mi <> 'Y' THEN
        l_temp_rec.MAX_ORDER_QTY   := l_stg_rec.MAX_ORDER_QTY;
      END IF;
      IF l_mi_rec.PACKING_METHOD_mi <> 'Y' THEN
        l_temp_rec.PACKING_METHOD   := l_stg_rec.PACKING_METHOD;
      END IF;
      IF l_mi_rec.PRIMARY_SUPP_IND_mi <> 'Y' THEN
        l_temp_rec.PRIMARY_SUPP_IND   := l_stg_rec.PRIMARY_SUPP_IND;
      END IF;
      IF l_mi_rec.PRIMARY_COUNTRY_IND_mi <> 'Y' THEN
        l_temp_rec.PRIMARY_COUNTRY_IND   := l_stg_rec.PRIMARY_COUNTRY_IND;
      END IF;
      IF l_mi_rec.DEFAULT_UOP_mi <> 'Y' THEN
        l_temp_rec.DEFAULT_UOP   := l_stg_rec.DEFAULT_UOP;
      END IF;
      IF l_mi_rec.TI_mi <> 'Y' THEN
        l_temp_rec.TI   := l_stg_rec.TI;
      END IF;
      IF l_mi_rec.HI_mi <> 'Y' THEN
        l_temp_rec.HI   := l_stg_rec.HI;
      END IF;
      IF l_mi_rec.SUPP_HIER_LVL_1_mi <> 'Y' THEN
        l_temp_rec.SUPP_HIER_LVL_1   := l_stg_rec.SUPP_HIER_LVL_1;
      END IF;
      IF l_mi_rec.SUPP_HIER_LVL_2_mi <> 'Y' THEN
        l_temp_rec.SUPP_HIER_LVL_2   := l_stg_rec.SUPP_HIER_LVL_2;
      END IF;
      IF l_mi_rec.SUPP_HIER_LVL_3_mi <> 'Y' THEN
        l_temp_rec.SUPP_HIER_LVL_3   := l_stg_rec.SUPP_HIER_LVL_3;
      END IF;
      IF l_mi_rec.COST_UOM_mi <> 'Y' THEN
        l_temp_rec.COST_UOM   := l_stg_rec.COST_UOM;
      END IF;
      IF l_mi_rec.TOLERANCE_TYPE_mi <> 'Y' THEN
        l_temp_rec.TOLERANCE_TYPE   := l_stg_rec.TOLERANCE_TYPE;
      END IF;
      IF l_mi_rec.MAX_TOLERANCE_mi <> 'Y' THEN
        l_temp_rec.MAX_TOLERANCE   := l_stg_rec.MAX_TOLERANCE;
      END IF;
      IF l_mi_rec.MIN_TOLERANCE_mi <> 'Y' THEN
        l_temp_rec.MIN_TOLERANCE   := l_stg_rec.MIN_TOLERANCE;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.UNIT_COST_mi <> 'Y' THEN
        l_temp_rec.UNIT_COST   := l_rms_rec.UNIT_COST;
      END IF;
      IF l_mi_rec.LEAD_TIME_mi <> 'Y' THEN
        l_temp_rec.LEAD_TIME   := l_rms_rec.LEAD_TIME;
      END IF;
      IF l_mi_rec.PICKUP_LEAD_TIME_mi <> 'Y' THEN
        l_temp_rec.PICKUP_LEAD_TIME   := l_rms_rec.PICKUP_LEAD_TIME;
      END IF;
      IF l_mi_rec.SUPP_PACK_SIZE_mi <> 'Y' THEN
        l_temp_rec.SUPP_PACK_SIZE   := l_rms_rec.SUPP_PACK_SIZE;
      END IF;
      IF l_mi_rec.INNER_PACK_SIZE_mi <> 'Y' THEN
        l_temp_rec.INNER_PACK_SIZE   := l_rms_rec.INNER_PACK_SIZE;
      END IF;
      IF l_mi_rec.ROUND_LVL_mi <> 'Y' THEN
        l_temp_rec.ROUND_LVL   := l_rms_rec.ROUND_LVL;
      END IF;
      IF l_mi_rec.ROUND_TO_INNER_PCT_mi <> 'Y' THEN
        l_temp_rec.ROUND_TO_INNER_PCT   := l_rms_rec.ROUND_TO_INNER_PCT;
      END IF;
      IF l_mi_rec.ROUND_TO_CASE_PCT_mi <> 'Y' THEN
        l_temp_rec.ROUND_TO_CASE_PCT   := l_rms_rec.ROUND_TO_CASE_PCT;
      END IF;
      IF l_mi_rec.ROUND_TO_LAYER_PCT_mi <> 'Y' THEN
        l_temp_rec.ROUND_TO_LAYER_PCT   := l_rms_rec.ROUND_TO_LAYER_PCT;
      END IF;
      IF l_mi_rec.ROUND_TO_PALLET_PCT_mi <> 'Y' THEN
        l_temp_rec.ROUND_TO_PALLET_PCT   := l_rms_rec.ROUND_TO_PALLET_PCT;
      END IF;
      IF l_mi_rec.MIN_ORDER_QTY_mi <> 'Y' THEN
        l_temp_rec.MIN_ORDER_QTY   := l_rms_rec.MIN_ORDER_QTY;
      END IF;
      IF l_mi_rec.MAX_ORDER_QTY_mi <> 'Y' THEN
        l_temp_rec.MAX_ORDER_QTY   := l_rms_rec.MAX_ORDER_QTY;
      END IF;
      IF l_mi_rec.PACKING_METHOD_mi <> 'Y' THEN
        l_temp_rec.PACKING_METHOD   := l_rms_rec.PACKING_METHOD;
      END IF;
      IF l_mi_rec.PRIMARY_SUPP_IND_mi <> 'Y' THEN
        l_temp_rec.PRIMARY_SUPP_IND   := l_rms_rec.PRIMARY_SUPP_IND;
      END IF;
      IF l_mi_rec.PRIMARY_COUNTRY_IND_mi <> 'Y' THEN
        l_temp_rec.PRIMARY_COUNTRY_IND   := l_rms_rec.PRIMARY_COUNTRY_IND;
      END IF;
      IF l_mi_rec.DEFAULT_UOP_mi <> 'Y' THEN
        l_temp_rec.DEFAULT_UOP   := l_rms_rec.DEFAULT_UOP;
      END IF;
      IF l_mi_rec.TI_mi <> 'Y' THEN
        l_temp_rec.TI   := l_rms_rec.TI;
      END IF;
      IF l_mi_rec.HI_mi <> 'Y' THEN
        l_temp_rec.HI   := l_rms_rec.HI;
      END IF;
      IF l_mi_rec.SUPP_HIER_LVL_1_mi <> 'Y' THEN
        l_temp_rec.SUPP_HIER_LVL_1   := l_rms_rec.SUPP_HIER_LVL_1;
      END IF;
      IF l_mi_rec.SUPP_HIER_LVL_2_mi <> 'Y' THEN
        l_temp_rec.SUPP_HIER_LVL_2   := l_rms_rec.SUPP_HIER_LVL_2;
      END IF;
      IF l_mi_rec.SUPP_HIER_LVL_3_mi <> 'Y' THEN
        l_temp_rec.SUPP_HIER_LVL_3   := l_rms_rec.SUPP_HIER_LVL_3;
      END IF;
      IF l_mi_rec.COST_UOM_mi <> 'Y' THEN
        l_temp_rec.COST_UOM   := l_rms_rec.COST_UOM;
      END IF;
      IF l_mi_rec.TOLERANCE_TYPE_mi <> 'Y' THEN
        l_temp_rec.TOLERANCE_TYPE   := l_rms_rec.TOLERANCE_TYPE;
      END IF;
      IF l_mi_rec.MAX_TOLERANCE_mi <> 'Y' THEN
        l_temp_rec.MAX_TOLERANCE   := l_rms_rec.MAX_TOLERANCE;
      END IF;
      IF l_mi_rec.MIN_TOLERANCE_mi <> 'Y' THEN
        l_temp_rec.MIN_TOLERANCE   := l_rms_rec.MIN_TOLERANCE;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,ISC_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.SUPPLIER := NVL( l_temp_rec.SUPPLIER,l_default_rec.SUPPLIER);
      l_temp_rec.ORIGIN_COUNTRY_ID := NVL( l_temp_rec.ORIGIN_COUNTRY_ID,l_default_rec.ORIGIN_COUNTRY_ID);
      l_temp_rec.UNIT_COST := NVL( l_temp_rec.UNIT_COST,l_default_rec.UNIT_COST);
      l_temp_rec.LEAD_TIME := NVL( l_temp_rec.LEAD_TIME,l_default_rec.LEAD_TIME);
      l_temp_rec.PICKUP_LEAD_TIME := NVL( l_temp_rec.PICKUP_LEAD_TIME,l_default_rec.PICKUP_LEAD_TIME);
      l_temp_rec.SUPP_PACK_SIZE := NVL( l_temp_rec.SUPP_PACK_SIZE,l_default_rec.SUPP_PACK_SIZE);
      l_temp_rec.INNER_PACK_SIZE := NVL( l_temp_rec.INNER_PACK_SIZE,l_default_rec.INNER_PACK_SIZE);
      l_temp_rec.ROUND_LVL := NVL( l_temp_rec.ROUND_LVL,l_default_rec.ROUND_LVL);
      l_temp_rec.ROUND_TO_INNER_PCT := NVL( l_temp_rec.ROUND_TO_INNER_PCT,l_default_rec.ROUND_TO_INNER_PCT);
      l_temp_rec.ROUND_TO_CASE_PCT := NVL( l_temp_rec.ROUND_TO_CASE_PCT,l_default_rec.ROUND_TO_CASE_PCT);
      l_temp_rec.ROUND_TO_LAYER_PCT := NVL( l_temp_rec.ROUND_TO_LAYER_PCT,l_default_rec.ROUND_TO_LAYER_PCT);
      l_temp_rec.ROUND_TO_PALLET_PCT := NVL( l_temp_rec.ROUND_TO_PALLET_PCT,l_default_rec.ROUND_TO_PALLET_PCT);
      l_temp_rec.MIN_ORDER_QTY := NVL( l_temp_rec.MIN_ORDER_QTY,l_default_rec.MIN_ORDER_QTY);
      l_temp_rec.MAX_ORDER_QTY := NVL( l_temp_rec.MAX_ORDER_QTY,l_default_rec.MAX_ORDER_QTY);
      l_temp_rec.PACKING_METHOD := NVL( l_temp_rec.PACKING_METHOD,l_default_rec.PACKING_METHOD);
      l_temp_rec.PRIMARY_SUPP_IND := NVL( l_temp_rec.PRIMARY_SUPP_IND,l_default_rec.PRIMARY_SUPP_IND);
      l_temp_rec.PRIMARY_COUNTRY_IND := NVL( l_temp_rec.PRIMARY_COUNTRY_IND,l_default_rec.PRIMARY_COUNTRY_IND);
      l_temp_rec.DEFAULT_UOP := NVL( l_temp_rec.DEFAULT_UOP,l_default_rec.DEFAULT_UOP);
      l_temp_rec.TI := NVL( l_temp_rec.TI,l_default_rec.TI);
      l_temp_rec.HI := NVL( l_temp_rec.HI,l_default_rec.HI);
      l_temp_rec.SUPP_HIER_TYPE_1 := NVL( l_temp_rec.SUPP_HIER_TYPE_1,l_default_rec.SUPP_HIER_TYPE_1);
      l_temp_rec.SUPP_HIER_LVL_1 := NVL( l_temp_rec.SUPP_HIER_LVL_1,l_default_rec.SUPP_HIER_LVL_1);
      l_temp_rec.SUPP_HIER_TYPE_2 := NVL( l_temp_rec.SUPP_HIER_TYPE_2,l_default_rec.SUPP_HIER_TYPE_2);
      l_temp_rec.SUPP_HIER_LVL_2 := NVL( l_temp_rec.SUPP_HIER_LVL_2,l_default_rec.SUPP_HIER_LVL_2);
      l_temp_rec.SUPP_HIER_TYPE_3 := NVL( l_temp_rec.SUPP_HIER_TYPE_3,l_default_rec.SUPP_HIER_TYPE_3);
      l_temp_rec.SUPP_HIER_LVL_3 := NVL( l_temp_rec.SUPP_HIER_LVL_3,l_default_rec.SUPP_HIER_LVL_3);
      l_temp_rec.COST_UOM := NVL( l_temp_rec.COST_UOM,l_default_rec.COST_UOM);
      l_temp_rec.TOLERANCE_TYPE := NVL( l_temp_rec.TOLERANCE_TYPE,l_default_rec.TOLERANCE_TYPE);
      l_temp_rec.MAX_TOLERANCE := NVL( l_temp_rec.MAX_TOLERANCE,l_default_rec.MAX_TOLERANCE);
      l_temp_rec.MIN_TOLERANCE := NVL( l_temp_rec.MIN_TOLERANCE,l_default_rec.MIN_TOLERANCE);
      l_temp_rec.NEGOTIATED_ITEM_COST := NVL( l_temp_rec.NEGOTIATED_ITEM_COST,l_default_rec.NEGOTIATED_ITEM_COST);
      l_temp_rec.EXTENDED_BASE_COST := NVL( l_temp_rec.EXTENDED_BASE_COST,l_default_rec.EXTENDED_BASE_COST);
      l_temp_rec.INCLUSIVE_COST := NVL( l_temp_rec.INCLUSIVE_COST,l_default_rec.INCLUSIVE_COST);
      l_temp_rec.BASE_COST := NVL( l_temp_rec.BASE_COST,l_default_rec.BASE_COST);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.SUPPLIER IS NOT NULL
        AND l_temp_rec.ORIGIN_COUNTRY_ID IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,ISC_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_SUPP_COUNTRY VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,ISC_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_SUPP_COUNTRY
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND SUPPLIER = svc_upd_col(i).SUPPLIER
     AND ORIGIN_COUNTRY_ID = svc_upd_col(i).ORIGIN_COUNTRY_ID
;
END process_s9t_ISC;
----------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_ISCL
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_SUPP_COUNTRY_LOC.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_SUPP_COUNTRY_LOC%rowtype;
  l_temp_rec SVC_ITEM_SUPP_COUNTRY_LOC%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_SUPP_COUNTRY_LOC.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_SUPP_COUNTRY_LOC%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
   SELECT PICKUP_LEAD_TIME_mi,
          SUPP_HIER_LVL_3_mi,
          SUPP_HIER_LVL_2_mi,
          SUPP_HIER_LVL_1_mi,
          ROUND_TO_PALLET_PCT_mi,
          ROUND_TO_LAYER_PCT_mi,
          ROUND_TO_CASE_PCT_mi,
          ROUND_TO_INNER_PCT_mi,
          ROUND_LVL_mi,
          UNIT_COST_mi,
          negotiated_item_cost_mi,
          primary_loc_ind_mi,
          loc_type_mi,
          loc_mi,
          ORIGIN_COUNTRY_ID_mi,
          SUPPLIER_mi,
          ITEM_mi,
          1 AS dummy
    FROM
      (SELECT column_key,
        mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                              = 'ITEM_INDUCT_SQL.template_key'
      AND wksht_key                                   = 'ITEM_SUPP_COUNTRY_LOC'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'PICKUP_LEAD_TIME' AS PICKUP_LEAD_TIME, 'SUPP_HIER_LVL_3' AS SUPP_HIER_LVL_3, 'SUPP_HIER_LVL_2' AS SUPP_HIER_LVL_2, 'SUPP_HIER_LVL_1' AS SUPP_HIER_LVL_1, 'ROUND_TO_PALLET_PCT' AS ROUND_TO_PALLET_PCT, 'ROUND_TO_LAYER_PCT' AS ROUND_TO_LAYER_PCT, 'ROUND_TO_CASE_PCT' AS ROUND_TO_CASE_PCT, 'ROUND_TO_INNER_PCT' AS ROUND_TO_INNER_PCT,
       'ROUND_LVL' AS ROUND_LVL, 'UNIT_COST' AS UNIT_COST,'NEGOTIATED_ITEM_COST' AS NEGOTIATED_ITEM_COST, 'PRIMARY_LOC_IND' AS PRIMARY_LOC_IND,'LOC_TYPE' AS LOC_TYPE,'LOC' as LOC, 'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item, supplier and orgin country id,loc';
  L_table        VARCHAR2(30) := 'SVC_ITEM_SUPP_COUNTRY_LOC';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT PICKUP_LEAD_TIME_dv,
          SUPP_HIER_LVL_3_dv,
          SUPP_HIER_LVL_2_dv,
          SUPP_HIER_LVL_1_dv,
          ROUND_TO_PALLET_PCT_dv,
          ROUND_TO_LAYER_PCT_dv,
          ROUND_TO_CASE_PCT_dv,
          ROUND_TO_INNER_PCT_dv,
          ROUND_LVL_dv,
          UNIT_COST_dv,
          NEGOTIATED_ITEM_COST_dv,
          primary_loc_ind_dv,
          loc_type_dv,
          loc_dv,
          ORIGIN_COUNTRY_ID_dv,
          SUPPLIER_dv,
          ITEM_dv,
          NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                                       = 'ITEM_SUPP_COUNTRY_LOC'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'PICKUP_LEAD_TIME' AS PICKUP_LEAD_TIME, 'SUPP_HIER_LVL_3' AS SUPP_HIER_LVL_3, 'SUPP_HIER_LVL_2' AS SUPP_HIER_LVL_2, 'SUPP_HIER_LVL_1' AS SUPP_HIER_LVL_1, 'ROUND_TO_PALLET_PCT' AS ROUND_TO_PALLET_PCT, 'ROUND_TO_LAYER_PCT' AS ROUND_TO_LAYER_PCT, 'ROUND_TO_CASE_PCT' AS ROUND_TO_CASE_PCT, 'ROUND_TO_INNER_PCT' AS ROUND_TO_INNER_PCT,
             'ROUND_LVL' AS ROUND_LVL, 'UNIT_COST' AS UNIT_COST,'NEGOTIATED_ITEM_COST' AS NEGOTIATED_ITEM_COST,'PRIMARY_LOC_IND' AS PRIMARY_LOC_IND,'LOC_TYPE' AS LOC_TYPE,'LOC' as LOC, 'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN

      l_default_rec.PICKUP_LEAD_TIME := rec.PICKUP_LEAD_TIME_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'PICKUP_LEAD_TIME','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_HIER_LVL_3 := rec.SUPP_HIER_LVL_3_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'SUPP_HIER_LVL_3','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_HIER_LVL_2 := rec.SUPP_HIER_LVL_2_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'SUPP_HIER_LVL_2','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPP_HIER_LVL_1 := rec.SUPP_HIER_LVL_1_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'SUPP_HIER_LVL_1','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ROUND_TO_PALLET_PCT := rec.ROUND_TO_PALLET_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'ROUND_TO_PALLET_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ROUND_TO_LAYER_PCT := rec.ROUND_TO_LAYER_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'ROUND_TO_LAYER_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ROUND_TO_CASE_PCT := rec.ROUND_TO_CASE_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'ROUND_TO_CASE_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ROUND_TO_INNER_PCT := rec.ROUND_TO_INNER_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'ROUND_TO_INNER_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ROUND_LVL := rec.ROUND_LVL_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'ROUND_LVL','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.UNIT_COST := rec.UNIT_COST_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'UNIT_COST','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.NEGOTIATED_ITEM_COST := rec.NEGOTIATED_ITEM_COST_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'NEGOTIATED_ITEM_COST','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'ORIGIN_COUNTRY_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPPLIER := rec.SUPPLIER_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'SUPPLIER','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PRIMARY_LOC_IND := rec.PRIMARY_LOC_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'PRIMARY_LOC_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.LOC_TYPE := rec.LOC_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'LOC_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.LOC := rec.LOC_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_LOC',NULL,'LOC','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(ISCL_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(ISCL_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(ISCL$Action)         AS Action,
    r.get_cell(ISCL$PRIMARY_LOC_IND)      AS PRIMARY_LOC_IND,
    r.get_cell(ISCL$SUPP_HIER_LVL_3)      AS SUPP_HIER_LVL_3,
    r.get_cell(ISCL$SUPP_HIER_LVL_2)      AS SUPP_HIER_LVL_2,
    r.get_cell(ISCL$SUPP_HIER_LVL_1)      AS SUPP_HIER_LVL_1,
    r.get_cell(ISCL$ROUND_TO_PALLET_PCT)  AS ROUND_TO_PALLET_PCT,
    r.get_cell(ISCL$ROUND_TO_LAYER_PCT)   AS ROUND_TO_LAYER_PCT,
    r.get_cell(ISCL$ROUND_TO_CASE_PCT)    AS ROUND_TO_CASE_PCT,
    r.get_cell(ISCL$ROUND_TO_INNER_PCT)   AS ROUND_TO_INNER_PCT,
    r.get_cell(ISCL$ROUND_LVL)            AS ROUND_LVL,
    r.get_cell(ISCL$UNIT_COST)            AS UNIT_COST,
    r.get_cell(ISCL$NEGOTIATED_ITEM_COST) AS NEGOTIATED_ITEM_COST,
    r.get_cell(ISCL$ORIGIN_COUNTRY_ID)    AS ORIGIN_COUNTRY_ID,
    r.get_cell(ISCL$SUPPLIER)             AS SUPPLIER,
    r.get_cell(ISCL$ITEM)                 AS ITEM,
    r.get_cell(ISCL$LOC)                  AS LOC,
    r.get_cell(ISCL$LOC_TYPE)             AS LOC_TYPE,
    r.get_cell(ISCL$PICKUP_LEAD_TIME)     AS PICKUP_LEAD_TIME,
    r.get_row_seq()                      AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id        := I_process_id;
    l_temp_rec.chunk_id          := 1;
    l_temp_rec.row_seq           := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id         := get_user;
    l_temp_rec.last_upd_id       := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error                      := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PICKUP_LEAD_TIME := rec.PICKUP_LEAD_TIME;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'PICKUP_LEAD_TIME',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PRIMARY_LOC_IND := rec.PRIMARY_LOC_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'PRIMARY_LOC_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_HIER_LVL_3 := rec.SUPP_HIER_LVL_3;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'SUPP_HIER_LVL_3',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_HIER_LVL_2 := rec.SUPP_HIER_LVL_2;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'SUPP_HIER_LVL_2',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPP_HIER_LVL_1 := rec.SUPP_HIER_LVL_1;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'SUPP_HIER_LVL_1',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ROUND_TO_PALLET_PCT := rec.ROUND_TO_PALLET_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'ROUND_TO_PALLET_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ROUND_TO_LAYER_PCT := rec.ROUND_TO_LAYER_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'ROUND_TO_LAYER_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ROUND_TO_CASE_PCT := rec.ROUND_TO_CASE_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'ROUND_TO_CASE_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ROUND_TO_INNER_PCT := rec.ROUND_TO_INNER_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'ROUND_TO_INNER_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ROUND_LVL := rec.ROUND_LVL;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'ROUND_LVL',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.UNIT_COST := rec.UNIT_COST;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'UNIT_COST',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.NEGOTIATED_ITEM_COST := rec.NEGOTIATED_ITEM_COST;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'NEGOTIATED_ITEM_COST',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'ORIGIN_COUNTRY_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPPLIER := rec.SUPPLIER;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'SUPPLIER',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.LOC := rec.LOC;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'LOC',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.LOC_TYPE := rec.LOC_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'LOC_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;

    IF NOT l_error THEN

      DECLARE
        l_stg_rec SVC_ITEM_SUPP_COUNTRY_LOC%rowtype;
        CURSOR c_stg_rec
        IS
          SELECT *
            FROM SVC_ITEM_SUPP_COUNTRY_LOC
           WHERE ITEM     = l_temp_rec.ITEM
             AND SUPPLIER = l_temp_rec.SUPPLIER
             AND ORIGIN_COUNTRY_ID = l_temp_rec.ORIGIN_COUNTRY_ID
             AND LOC =l_temp_rec.LOC;

        l_rms_rec ITEM_SUPP_COUNTRY_LOC%rowtype;
        CURSOR c_rms_rec
        IS
          SELECT *
            FROM ITEM_SUPP_COUNTRY_LOC
           WHERE ITEM     = l_temp_rec.ITEM
             AND SUPPLIER = l_temp_rec.SUPPLIER
             AND ORIGIN_COUNTRY_ID = l_temp_rec.ORIGIN_COUNTRY_ID
             AND LOC =l_temp_rec.LOC;

     cursor C_ITEM_LOC_EXIST is
      select 'Y'
        from item_loc
       where item = l_temp_rec.ITEM
         and loc = l_temp_rec.LOC;

        l_rms_exists BOOLEAN;
        l_loc_exists VARCHAR2(1);

      BEGIN
        OPEN C_ITEM_LOC_EXIST;
        FETCH C_ITEM_LOC_EXIST INTO l_loc_exists;
        IF NVL(l_loc_exists,'N')='N' THEN
           L_error_msg  := SQL_LIB.CREATE_MSG('SKU_NOT_LOC');
           write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,'LOC',NULL,L_error_msg);
           l_error:=true;
        END IF;
        CLOSE C_ITEM_LOC_EXIST;
        OPEN c_stg_rec;
        FETCH c_stg_rec INTO l_stg_rec;
        IF c_stg_rec%found THEN
          l_stg_exists := true;
        ELSE
          l_stg_exists := false;
        END IF;
        CLOSE c_stg_rec;
        OPEN c_rms_rec;
        FETCH c_rms_rec INTO l_rms_rec;
        IF c_rms_rec%found THEN
          l_rms_exists := true;
        ELSE
          l_rms_exists := false;
        END IF;
        CLOSE c_rms_rec;

        -- Delete record from staging if it is already uploaded to RMS.
      IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
          DELETE
            FROM SVC_ITEM_SUPP_COUNTRY_LOC
           WHERE process_id = l_stg_rec.process_id
             AND row_seq      = l_stg_rec.row_seq;
          l_stg_exists    := false;
        END IF;
        -- Copy values for non-mandatory columns from Staging.
        IF l_stg_exists THEN
          IF l_mi_rec.UNIT_COST_mi <> 'Y' THEN
            l_temp_rec.UNIT_COST   := l_stg_rec.UNIT_COST;
          END IF;
      IF l_mi_rec.NEGOTIATED_ITEM_COST_mi <> 'Y' THEN
            l_temp_rec.NEGOTIATED_ITEM_COST   := l_stg_rec.NEGOTIATED_ITEM_COST;
          END IF;
          IF l_mi_rec.PICKUP_LEAD_TIME_mi <> 'Y' THEN
            l_temp_rec.PICKUP_LEAD_TIME   := l_stg_rec.PICKUP_LEAD_TIME;
          END IF;
          IF l_mi_rec.ROUND_LVL_mi <> 'Y' THEN
            l_temp_rec.ROUND_LVL   := l_stg_rec.ROUND_LVL;
          END IF;
          IF l_mi_rec.ROUND_TO_INNER_PCT_mi <> 'Y' THEN
            l_temp_rec.ROUND_TO_INNER_PCT   := l_stg_rec.ROUND_TO_INNER_PCT;
          END IF;
          IF l_mi_rec.ROUND_TO_CASE_PCT_mi <> 'Y' THEN
            l_temp_rec.ROUND_TO_CASE_PCT   := l_stg_rec.ROUND_TO_CASE_PCT;
          END IF;
          IF l_mi_rec.ROUND_TO_LAYER_PCT_mi <> 'Y' THEN
            l_temp_rec.ROUND_TO_LAYER_PCT   := l_stg_rec.ROUND_TO_LAYER_PCT;
          END IF;
          IF l_mi_rec.ROUND_TO_PALLET_PCT_mi <> 'Y' THEN
            l_temp_rec.ROUND_TO_PALLET_PCT   := l_stg_rec.ROUND_TO_PALLET_PCT;
          END IF;
          IF l_mi_rec.PRIMARY_LOC_IND_mi <> 'Y' THEN
            l_temp_rec.PRIMARY_LOC_IND   := l_stg_rec.PRIMARY_LOC_IND;
          END IF;
          IF l_mi_rec.SUPP_HIER_LVL_1_mi <> 'Y' THEN
            l_temp_rec.SUPP_HIER_LVL_1   := l_stg_rec.SUPP_HIER_LVL_1;
          END IF;
          IF l_mi_rec.SUPP_HIER_LVL_2_mi <> 'Y' THEN
            l_temp_rec.SUPP_HIER_LVL_2   := l_stg_rec.SUPP_HIER_LVL_2;
          END IF;
          IF l_mi_rec.SUPP_HIER_LVL_3_mi <> 'Y' THEN
            l_temp_rec.SUPP_HIER_LVL_3   := l_stg_rec.SUPP_HIER_LVL_3;
          END IF;

        END IF;

        -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
        IF l_rms_exists AND NOT l_stg_exists THEN
          IF l_mi_rec.UNIT_COST_mi <> 'Y' THEN
            l_temp_rec.UNIT_COST   := l_rms_rec.UNIT_COST;
          END IF;
      IF l_mi_rec.NEGOTIATED_ITEM_COST_mi <> 'Y' THEN
            l_temp_rec.NEGOTIATED_ITEM_COST   := l_rms_rec.NEGOTIATED_ITEM_COST;
          END IF;
          IF l_mi_rec.PICKUP_LEAD_TIME_mi <> 'Y' THEN
            l_temp_rec.PICKUP_LEAD_TIME   := l_rms_rec.PICKUP_LEAD_TIME;
          END IF;
          IF l_mi_rec.ROUND_LVL_mi <> 'Y' THEN
            l_temp_rec.ROUND_LVL   := l_rms_rec.ROUND_LVL;
          END IF;
          IF l_mi_rec.ROUND_TO_INNER_PCT_mi <> 'Y' THEN
            l_temp_rec.ROUND_TO_INNER_PCT   := l_rms_rec.ROUND_TO_INNER_PCT;
          END IF;
          IF l_mi_rec.ROUND_TO_CASE_PCT_mi <> 'Y' THEN
            l_temp_rec.ROUND_TO_CASE_PCT   := l_rms_rec.ROUND_TO_CASE_PCT;
          END IF;
          IF l_mi_rec.ROUND_TO_LAYER_PCT_mi <> 'Y' THEN
            l_temp_rec.ROUND_TO_LAYER_PCT   := l_rms_rec.ROUND_TO_LAYER_PCT;
          END IF;
          IF l_mi_rec.ROUND_TO_PALLET_PCT_mi <> 'Y' THEN
            l_temp_rec.ROUND_TO_PALLET_PCT   := l_rms_rec.ROUND_TO_PALLET_PCT;
          END IF;
          IF l_mi_rec.PRIMARY_LOC_IND_mi <> 'Y' THEN
            l_temp_rec.PRIMARY_LOC_IND   := l_rms_rec.PRIMARY_LOC_IND;
          END IF;
          IF l_mi_rec.SUPP_HIER_LVL_1_mi <> 'Y' THEN
            l_temp_rec.SUPP_HIER_LVL_1   := l_rms_rec.SUPP_HIER_LVL_1;
          END IF;
          IF l_mi_rec.SUPP_HIER_LVL_2_mi <> 'Y' THEN
            l_temp_rec.SUPP_HIER_LVL_2   := l_rms_rec.SUPP_HIER_LVL_2;
          END IF;
          IF l_mi_rec.SUPP_HIER_LVL_3_mi <> 'Y' THEN
            l_temp_rec.SUPP_HIER_LVL_3   := l_rms_rec.SUPP_HIER_LVL_3;
          END IF;

        END IF;
        --Resolve new value for action column
        IF l_stg_exists THEN
          -- if it is a create record and record already exists in staging, log an error
          IF l_temp_rec.action = coresvc_item.action_new THEN
            IF l_stg_rec.process$status = 'P' THEN
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
            ELSE
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
            END IF;
            write_s9t_error ( I_file_id,ISCL_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
            l_error := true;
          ELSE
            -- If record does not exist in staging then keep the user provided value
            l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
          END IF;
        END IF;
      END;
    END IF;

    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action                 = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.SUPPLIER := NVL( l_temp_rec.SUPPLIER,l_default_rec.SUPPLIER);
      l_temp_rec.ORIGIN_COUNTRY_ID := NVL( l_temp_rec.ORIGIN_COUNTRY_ID,l_default_rec.ORIGIN_COUNTRY_ID);
      l_temp_rec.LOC := NVL( l_temp_rec.LOC,l_default_rec.LOC);
      l_temp_rec.LOC_TYPE := NVL( l_temp_rec.LOC_TYPE,l_default_rec.LOC_TYPE);
      l_temp_rec.UNIT_COST := NVL( l_temp_rec.UNIT_COST,l_default_rec.UNIT_COST);
      l_temp_rec.PICKUP_LEAD_TIME := NVL( l_temp_rec.PICKUP_LEAD_TIME,l_default_rec.PICKUP_LEAD_TIME);
      l_temp_rec.ROUND_LVL := NVL( l_temp_rec.ROUND_LVL,l_default_rec.ROUND_LVL);
      l_temp_rec.ROUND_TO_INNER_PCT := NVL( l_temp_rec.ROUND_TO_INNER_PCT,l_default_rec.ROUND_TO_INNER_PCT);
      l_temp_rec.ROUND_TO_CASE_PCT := NVL( l_temp_rec.ROUND_TO_CASE_PCT,l_default_rec.ROUND_TO_CASE_PCT);
      l_temp_rec.ROUND_TO_LAYER_PCT := NVL( l_temp_rec.ROUND_TO_LAYER_PCT,l_default_rec.ROUND_TO_LAYER_PCT);
      l_temp_rec.ROUND_TO_PALLET_PCT := NVL( l_temp_rec.ROUND_TO_PALLET_PCT,l_default_rec.ROUND_TO_PALLET_PCT);
      l_temp_rec.PRIMARY_LOC_IND := NVL( l_temp_rec.PRIMARY_LOC_IND,l_default_rec.PRIMARY_LOC_IND);
      l_temp_rec.SUPP_HIER_TYPE_1 := NVL( l_temp_rec.SUPP_HIER_TYPE_1,l_default_rec.SUPP_HIER_TYPE_1);
      l_temp_rec.SUPP_HIER_LVL_1 := NVL( l_temp_rec.SUPP_HIER_LVL_1,l_default_rec.SUPP_HIER_LVL_1);
      l_temp_rec.SUPP_HIER_TYPE_2 := NVL( l_temp_rec.SUPP_HIER_TYPE_2,l_default_rec.SUPP_HIER_TYPE_2);
      l_temp_rec.SUPP_HIER_LVL_2 := NVL( l_temp_rec.SUPP_HIER_LVL_2,l_default_rec.SUPP_HIER_LVL_2);
      l_temp_rec.SUPP_HIER_TYPE_3 := NVL( l_temp_rec.SUPP_HIER_TYPE_3,l_default_rec.SUPP_HIER_TYPE_3);
      l_temp_rec.SUPP_HIER_LVL_3 := NVL( l_temp_rec.SUPP_HIER_LVL_3,l_default_rec.SUPP_HIER_LVL_3);
      l_temp_rec.NEGOTIATED_ITEM_COST := NVL( l_temp_rec.NEGOTIATED_ITEM_COST,l_default_rec.NEGOTIATED_ITEM_COST);
      l_temp_rec.EXTENDED_BASE_COST := NVL( l_temp_rec.EXTENDED_BASE_COST,l_default_rec.EXTENDED_BASE_COST);
      l_temp_rec.INCLUSIVE_COST := NVL( l_temp_rec.INCLUSIVE_COST,l_default_rec.INCLUSIVE_COST);
      l_temp_rec.BASE_COST := NVL( l_temp_rec.BASE_COST,l_default_rec.BASE_COST);
    END IF;

    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
            l_temp_rec.ITEM IS NOT NULL
            AND l_temp_rec.SUPPLIER IS NOT NULL
            AND l_temp_rec.ORIGIN_COUNTRY_ID IS NOT NULL
            AND l_temp_rec.LOC IS NOT NULL
            ) THEN

      L_error_msg  := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,ISCL_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
        svc_upd_col.extend();
        svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
        svc_ins_col.extend();
        svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_SUPP_COUNTRY_LOC VALUES svc_ins_col(i);
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
        L_error_code := NULL;
        L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                          L_pk_columns,
                                          L_table);
      end if;
      write_s9t_error
      (
        I_file_id,ISCL_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_SUPP_COUNTRY_LOC
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND SUPPLIER = svc_upd_col(i).SUPPLIER
     AND ORIGIN_COUNTRY_ID = svc_upd_col(i).ORIGIN_COUNTRY_ID
     AND LOC =svc_upd_col(i).LOC;
END process_s9t_ISCL;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_ISCD
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_SUPP_COUNTRY_DIM.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_SUPP_COUNTRY_DIM%rowtype;
  l_temp_rec SVC_ITEM_SUPP_COUNTRY_DIM%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_SUPP_COUNTRY_DIM.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_SUPP_COUNTRY_DIM%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT TARE_TYPE_mi,
      TARE_WEIGHT_mi,
      STAT_CUBE_mi,
      LIQUID_VOLUME_UOM_mi,
      LIQUID_VOLUME_mi,
      WEIGHT_UOM_mi,
      NET_WEIGHT_mi,
      WEIGHT_mi,
      LWH_UOM_mi,
      HEIGHT_mi,
      WIDTH_mi,
      LENGTH_mi,
      PRESENTATION_METHOD_mi,
      DIM_OBJECT_mi,
      ORIGIN_COUNTRY_mi,
      SUPPLIER_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_SUPP_COUNTRY_DIM'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'TARE_TYPE' AS TARE_TYPE, 'TARE_WEIGHT' AS TARE_WEIGHT, 'STAT_CUBE' AS STAT_CUBE, 'LIQUID_VOLUME_UOM' AS LIQUID_VOLUME_UOM, 'LIQUID_VOLUME' AS LIQUID_VOLUME, 'WEIGHT_UOM' AS WEIGHT_UOM, 'NET_WEIGHT' AS NET_WEIGHT, 'WEIGHT' AS WEIGHT, 'LWH_UOM' AS LWH_UOM, 'HEIGHT' AS HEIGHT, 'WIDTH' AS WIDTH, 'LENGTH' AS LENGTH, 'PRESENTATION_METHOD' AS PRESENTATION_METHOD, 'DIM_OBJECT' AS DIM_OBJECT, 'ORIGIN_COUNTRY' AS ORIGIN_COUNTRY, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item, supplier, origin country and dim object';
  L_table    VARCHAR2(30) := 'SVC_ITEM_SUPP_COUNTRY_DIM';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT TARE_TYPE_dv,
    TARE_WEIGHT_dv,
    STAT_CUBE_dv,
    LIQUID_VOLUME_UOM_dv,
    LIQUID_VOLUME_dv,
    WEIGHT_UOM_dv,
    NET_WEIGHT_dv,
    WEIGHT_dv,
    LWH_UOM_dv,
    HEIGHT_dv,
    WIDTH_dv,
    LENGTH_dv,
    PRESENTATION_METHOD_dv,
    DIM_OBJECT_dv,
    ORIGIN_COUNTRY_dv,
    SUPPLIER_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_SUPP_COUNTRY_DIM'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'TARE_TYPE' AS TARE_TYPE, 'TARE_WEIGHT' AS TARE_WEIGHT, 'STAT_CUBE' AS STAT_CUBE, 'LIQUID_VOLUME_UOM' AS LIQUID_VOLUME_UOM, 'LIQUID_VOLUME' AS LIQUID_VOLUME, 'WEIGHT_UOM' AS WEIGHT_UOM, 'NET_WEIGHT' AS NET_WEIGHT, 'WEIGHT' AS WEIGHT, 'LWH_UOM' AS LWH_UOM, 'HEIGHT' AS HEIGHT, 'WIDTH' AS WIDTH, 'LENGTH' AS LENGTH, 'PRESENTATION_METHOD' AS PRESENTATION_METHOD, 'DIM_OBJECT' AS DIM_OBJECT, 'ORIGIN_COUNTRY' AS ORIGIN_COUNTRY, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.TARE_TYPE := rec.TARE_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'TARE_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.TARE_WEIGHT := rec.TARE_WEIGHT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'TARE_WEIGHT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.STAT_CUBE := rec.STAT_CUBE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'STAT_CUBE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.LIQUID_VOLUME_UOM := rec.LIQUID_VOLUME_UOM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'LIQUID_VOLUME_UOM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.LIQUID_VOLUME := rec.LIQUID_VOLUME_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'LIQUID_VOLUME','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.WEIGHT_UOM := rec.WEIGHT_UOM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'WEIGHT_UOM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.NET_WEIGHT := rec.NET_WEIGHT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'NET_WEIGHT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.WEIGHT := rec.WEIGHT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'WEIGHT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.LWH_UOM := rec.LWH_UOM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'LWH_UOM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.HEIGHT := rec.HEIGHT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'HEIGHT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.WIDTH := rec.WIDTH_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'WIDTH','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.LENGTH := rec.LENGTH_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'LENGTH','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PRESENTATION_METHOD := rec.PRESENTATION_METHOD_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'PRESENTATION_METHOD','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.DIM_OBJECT := rec.DIM_OBJECT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'DIM_OBJECT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ORIGIN_COUNTRY := rec.ORIGIN_COUNTRY_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'ORIGIN_COUNTRY','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPPLIER := rec.SUPPLIER_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'SUPPLIER','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_COUNTRY_DIM',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(ISCD_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(ISCD_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(ISCD$Action)    AS Action,
    r.get_cell(ISCD$TARE_TYPE)       AS TARE_TYPE,
    r.get_cell(ISCD$TARE_WEIGHT)     AS TARE_WEIGHT,
    r.get_cell(ISCD$STAT_CUBE)       AS STAT_CUBE,
    NVL(uom_sql.get_uom(r.get_cell(ISCD$LIQUID_VOLUME_UOM),'Upload'),r.get_cell(ISCD$LIQUID_VOLUME_UOM))   AS LIQUID_VOLUME_UOM,
    r.get_cell(ISCD$LIQUID_VOLUME)   AS LIQUID_VOLUME,
    NVL(uom_sql.get_uom(r.get_cell(ISCD$WEIGHT_UOM),'Upload'),r.get_cell(ISCD$WEIGHT_UOM))     AS WEIGHT_UOM,
    r.get_cell(ISCD$NET_WEIGHT)      AS NET_WEIGHT,
    r.get_cell(ISCD$WEIGHT)      AS WEIGHT,
    NVL(uom_sql.get_uom(r.get_cell(ISCD$LWH_UOM),'Upload'),r.get_cell(ISCD$LWH_UOM))        AS LWH_UOM,
    r.get_cell(ISCD$HEIGHT)      AS HEIGHT,
    r.get_cell(ISCD$WIDTH)       AS WIDTH,
    r.get_cell(ISCD$LENGTH)      AS LENGTH,
    r.get_cell(ISCD$PRESENTATION_METHOD) AS PRESENTATION_METHOD,
    r.get_cell(ISCD$DIM_OBJECT)      AS DIM_OBJECT,
    r.get_cell(ISCD$ORIGIN_COUNTRY)  AS ORIGIN_COUNTRY,
    r.get_cell(ISCD$SUPPLIER)        AS SUPPLIER,
    r.get_cell(ISCD$ITEM)        AS ITEM,
    r.get_row_seq()          AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.TARE_TYPE := rec.TARE_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'TARE_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.TARE_WEIGHT := rec.TARE_WEIGHT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'TARE_WEIGHT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.STAT_CUBE := rec.STAT_CUBE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'STAT_CUBE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.LIQUID_VOLUME_UOM := rec.LIQUID_VOLUME_UOM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'LIQUID_VOLUME_UOM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.LIQUID_VOLUME := rec.LIQUID_VOLUME;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'LIQUID_VOLUME',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.WEIGHT_UOM := rec.WEIGHT_UOM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'WEIGHT_UOM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.NET_WEIGHT := rec.NET_WEIGHT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'NET_WEIGHT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.WEIGHT := rec.WEIGHT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'WEIGHT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.LWH_UOM := rec.LWH_UOM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'LWH_UOM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.HEIGHT := rec.HEIGHT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'HEIGHT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.WIDTH := rec.WIDTH;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'WIDTH',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.LENGTH := rec.LENGTH;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'LENGTH',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PRESENTATION_METHOD := rec.PRESENTATION_METHOD;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'PRESENTATION_METHOD',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DIM_OBJECT := rec.DIM_OBJECT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'DIM_OBJECT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ORIGIN_COUNTRY := rec.ORIGIN_COUNTRY;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'ORIGIN_COUNTRY',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPPLIER := rec.SUPPLIER;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'SUPPLIER',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_ITEM_SUPP_COUNTRY_DIM%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_ITEM_SUPP_COUNTRY_DIM
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND ORIGIN_COUNTRY = l_temp_rec.ORIGIN_COUNTRY
         AND DIM_OBJECT = l_temp_rec.DIM_OBJECT
;
    l_rms_rec ITEM_SUPP_COUNTRY_DIM%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM ITEM_SUPP_COUNTRY_DIM
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND ORIGIN_COUNTRY = l_temp_rec.ORIGIN_COUNTRY
         AND DIM_OBJECT = l_temp_rec.DIM_OBJECT
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_ITEM_SUPP_COUNTRY_DIM
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.PRESENTATION_METHOD_mi <> 'Y' THEN
        l_temp_rec.PRESENTATION_METHOD   := l_stg_rec.PRESENTATION_METHOD;
      END IF;
      IF l_mi_rec.LENGTH_mi <> 'Y' THEN
        l_temp_rec.LENGTH   := l_stg_rec.LENGTH;
      END IF;
      IF l_mi_rec.WIDTH_mi <> 'Y' THEN
        l_temp_rec.WIDTH   := l_stg_rec.WIDTH;
      END IF;
      IF l_mi_rec.HEIGHT_mi <> 'Y' THEN
        l_temp_rec.HEIGHT   := l_stg_rec.HEIGHT;
      END IF;
      IF l_mi_rec.LWH_UOM_mi <> 'Y' THEN
        l_temp_rec.LWH_UOM   := l_stg_rec.LWH_UOM;
      END IF;
      IF l_mi_rec.WEIGHT_mi <> 'Y' THEN
        l_temp_rec.WEIGHT   := l_stg_rec.WEIGHT;
      END IF;
      IF l_mi_rec.NET_WEIGHT_mi <> 'Y' THEN
        l_temp_rec.NET_WEIGHT   := l_stg_rec.NET_WEIGHT;
      END IF;
      IF l_mi_rec.WEIGHT_UOM_mi <> 'Y' THEN
        l_temp_rec.WEIGHT_UOM   := l_stg_rec.WEIGHT_UOM;
      END IF;
      IF l_mi_rec.LIQUID_VOLUME_mi <> 'Y' THEN
        l_temp_rec.LIQUID_VOLUME   := l_stg_rec.LIQUID_VOLUME;
      END IF;
      IF l_mi_rec.LIQUID_VOLUME_UOM_mi <> 'Y' THEN
        l_temp_rec.LIQUID_VOLUME_UOM   := l_stg_rec.LIQUID_VOLUME_UOM;
      END IF;
      IF l_mi_rec.STAT_CUBE_mi <> 'Y' THEN
        l_temp_rec.STAT_CUBE   := l_stg_rec.STAT_CUBE;
      END IF;
      IF l_mi_rec.TARE_WEIGHT_mi <> 'Y' THEN
        l_temp_rec.TARE_WEIGHT   := l_stg_rec.TARE_WEIGHT;
      END IF;
      IF l_mi_rec.TARE_TYPE_mi <> 'Y' THEN
        l_temp_rec.TARE_TYPE   := l_stg_rec.TARE_TYPE;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.PRESENTATION_METHOD_mi <> 'Y' THEN
        l_temp_rec.PRESENTATION_METHOD   := l_rms_rec.PRESENTATION_METHOD;
      END IF;
      IF l_mi_rec.LENGTH_mi <> 'Y' THEN
        l_temp_rec.LENGTH   := l_rms_rec.LENGTH;
      END IF;
      IF l_mi_rec.WIDTH_mi <> 'Y' THEN
        l_temp_rec.WIDTH   := l_rms_rec.WIDTH;
      END IF;
      IF l_mi_rec.HEIGHT_mi <> 'Y' THEN
        l_temp_rec.HEIGHT   := l_rms_rec.HEIGHT;
      END IF;
      IF l_mi_rec.LWH_UOM_mi <> 'Y' THEN
        l_temp_rec.LWH_UOM   := l_rms_rec.LWH_UOM;
      END IF;
      IF l_mi_rec.WEIGHT_mi <> 'Y' THEN
        l_temp_rec.WEIGHT   := l_rms_rec.WEIGHT;
      END IF;
      IF l_mi_rec.NET_WEIGHT_mi <> 'Y' THEN
        l_temp_rec.NET_WEIGHT   := l_rms_rec.NET_WEIGHT;
      END IF;
      IF l_mi_rec.WEIGHT_UOM_mi <> 'Y' THEN
        l_temp_rec.WEIGHT_UOM   := l_rms_rec.WEIGHT_UOM;
      END IF;
      IF l_mi_rec.LIQUID_VOLUME_mi <> 'Y' THEN
        l_temp_rec.LIQUID_VOLUME   := l_rms_rec.LIQUID_VOLUME;
      END IF;
      IF l_mi_rec.LIQUID_VOLUME_UOM_mi <> 'Y' THEN
        l_temp_rec.LIQUID_VOLUME_UOM   := l_rms_rec.LIQUID_VOLUME_UOM;
      END IF;
      IF l_mi_rec.STAT_CUBE_mi <> 'Y' THEN
        l_temp_rec.STAT_CUBE   := l_rms_rec.STAT_CUBE;
      END IF;
      IF l_mi_rec.TARE_WEIGHT_mi <> 'Y' THEN
        l_temp_rec.TARE_WEIGHT   := l_rms_rec.TARE_WEIGHT;
      END IF;
      IF l_mi_rec.TARE_TYPE_mi <> 'Y' THEN
        l_temp_rec.TARE_TYPE   := l_rms_rec.TARE_TYPE;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,ISCD_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.SUPPLIER := NVL( l_temp_rec.SUPPLIER,l_default_rec.SUPPLIER);
      l_temp_rec.ORIGIN_COUNTRY := NVL( l_temp_rec.ORIGIN_COUNTRY,l_default_rec.ORIGIN_COUNTRY);
      l_temp_rec.DIM_OBJECT := NVL( l_temp_rec.DIM_OBJECT,l_default_rec.DIM_OBJECT);
      l_temp_rec.PRESENTATION_METHOD := NVL( l_temp_rec.PRESENTATION_METHOD,l_default_rec.PRESENTATION_METHOD);
      l_temp_rec.LENGTH := NVL( l_temp_rec.LENGTH,l_default_rec.LENGTH);
      l_temp_rec.WIDTH := NVL( l_temp_rec.WIDTH,l_default_rec.WIDTH);
      l_temp_rec.HEIGHT := NVL( l_temp_rec.HEIGHT,l_default_rec.HEIGHT);
      l_temp_rec.LWH_UOM := NVL( l_temp_rec.LWH_UOM,l_default_rec.LWH_UOM);
      l_temp_rec.WEIGHT := NVL( l_temp_rec.WEIGHT,l_default_rec.WEIGHT);
      l_temp_rec.NET_WEIGHT := NVL( l_temp_rec.NET_WEIGHT,l_default_rec.NET_WEIGHT);
      l_temp_rec.WEIGHT_UOM := NVL( l_temp_rec.WEIGHT_UOM,l_default_rec.WEIGHT_UOM);
      l_temp_rec.LIQUID_VOLUME := NVL( l_temp_rec.LIQUID_VOLUME,l_default_rec.LIQUID_VOLUME);
      l_temp_rec.LIQUID_VOLUME_UOM := NVL( l_temp_rec.LIQUID_VOLUME_UOM,l_default_rec.LIQUID_VOLUME_UOM);
      l_temp_rec.STAT_CUBE := NVL( l_temp_rec.STAT_CUBE,l_default_rec.STAT_CUBE);
      l_temp_rec.TARE_WEIGHT := NVL( l_temp_rec.TARE_WEIGHT,l_default_rec.TARE_WEIGHT);
      l_temp_rec.TARE_TYPE := NVL( l_temp_rec.TARE_TYPE,l_default_rec.TARE_TYPE);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.SUPPLIER IS NOT NULL
        AND l_temp_rec.ORIGIN_COUNTRY IS NOT NULL
        AND l_temp_rec.DIM_OBJECT IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,ISCD_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_SUPP_COUNTRY_DIM VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,ISCD_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_SUPP_COUNTRY_DIM
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND SUPPLIER = svc_upd_col(i).SUPPLIER
     AND ORIGIN_COUNTRY = svc_upd_col(i).ORIGIN_COUNTRY
     AND DIM_OBJECT = svc_upd_col(i).DIM_OBJECT
;
END process_s9t_ISCD;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_ISMC
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_SUPP_MANU_COUNTRY.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_SUPP_MANU_COUNTRY%rowtype;
  l_temp_rec SVC_ITEM_SUPP_MANU_COUNTRY%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_SUPP_MANU_COUNTRY.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_SUPP_MANU_COUNTRY%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT PRIMARY_MANU_CTRY_IND_mi,
      MANU_COUNTRY_ID_mi,
      SUPPLIER_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_SUPP_MANU_COUNTRY'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'PRIMARY_MANU_CTRY_IND' AS PRIMARY_MANU_CTRY_IND, 'MANU_COUNTRY_ID' AS MANU_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item, supplier and manu country id';
  L_table    VARCHAR2(30) := 'SVC_ITEM_SUPP_MANU_COUNTRY';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT PRIMARY_MANU_CTRY_IND_dv,
    MANU_COUNTRY_ID_dv,
    SUPPLIER_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_SUPP_MANU_COUNTRY'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'PRIMARY_MANU_CTRY_IND' AS PRIMARY_MANU_CTRY_IND, 'MANU_COUNTRY_ID' AS MANU_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.PRIMARY_MANU_CTRY_IND := rec.PRIMARY_MANU_CTRY_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_MANU_COUNTRY',NULL,'PRIMARY_MANU_CTRY_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.MANU_COUNTRY_ID := rec.MANU_COUNTRY_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_MANU_COUNTRY',NULL,'MANU_COUNTRY_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPPLIER := rec.SUPPLIER_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_MANU_COUNTRY',NULL,'SUPPLIER','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_MANU_COUNTRY',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(ISMC_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(ISMC_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(ISMC$Action)      AS Action,
    r.get_cell(ISMC$PRIMARY_MANU_CTRY_IND) AS PRIMARY_MANU_CTRY_IND,
    r.get_cell(ISMC$MANU_COUNTRY_ID)       AS MANU_COUNTRY_ID,
    r.get_cell(ISMC$SUPPLIER)          AS SUPPLIER,
    r.get_cell(ISMC$ITEM)          AS ITEM,
    r.get_row_seq()            AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISMC_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PRIMARY_MANU_CTRY_IND := rec.PRIMARY_MANU_CTRY_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISMC_sheet,rec.row_seq,'PRIMARY_MANU_CTRY_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.MANU_COUNTRY_ID := rec.MANU_COUNTRY_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISMC_sheet,rec.row_seq,'MANU_COUNTRY_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPPLIER := rec.SUPPLIER;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISMC_sheet,rec.row_seq,'SUPPLIER',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISMC_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_ITEM_SUPP_MANU_COUNTRY%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_ITEM_SUPP_MANU_COUNTRY
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND MANU_COUNTRY_ID = l_temp_rec.MANU_COUNTRY_ID
;
    l_rms_rec ITEM_SUPP_MANU_COUNTRY%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM ITEM_SUPP_MANU_COUNTRY
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND MANU_COUNTRY_ID = l_temp_rec.MANU_COUNTRY_ID
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_ITEM_SUPP_MANU_COUNTRY
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.PRIMARY_MANU_CTRY_IND_mi <> 'Y' THEN
        l_temp_rec.PRIMARY_MANU_CTRY_IND   := l_stg_rec.PRIMARY_MANU_CTRY_IND;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.PRIMARY_MANU_CTRY_IND_mi <> 'Y' THEN
        l_temp_rec.PRIMARY_MANU_CTRY_IND   := l_rms_rec.PRIMARY_MANU_CTRY_IND;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,ISMC_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.SUPPLIER := NVL( l_temp_rec.SUPPLIER,l_default_rec.SUPPLIER);
      l_temp_rec.MANU_COUNTRY_ID := NVL( l_temp_rec.MANU_COUNTRY_ID,l_default_rec.MANU_COUNTRY_ID);
      l_temp_rec.PRIMARY_MANU_CTRY_IND := NVL( l_temp_rec.PRIMARY_MANU_CTRY_IND,l_default_rec.PRIMARY_MANU_CTRY_IND);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.SUPPLIER IS NOT NULL
        AND l_temp_rec.MANU_COUNTRY_ID IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,ISMC_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_SUPP_MANU_COUNTRY VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,ISMC_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_SUPP_MANU_COUNTRY
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND SUPPLIER = svc_upd_col(i).SUPPLIER
     AND MANU_COUNTRY_ID = svc_upd_col(i).MANU_COUNTRY_ID
;
END process_s9t_ISMC;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_ISU
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_SUPP_UOM.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_SUPP_UOM%rowtype;
  l_temp_rec SVC_ITEM_SUPP_UOM%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_SUPP_UOM.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_SUPP_UOM%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT VALUE_mi,
      UOM_mi,
      SUPPLIER_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_SUPP_UOM'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'VALUE' AS VALUE, 'UOM' AS UOM, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item, supplier and UOM';
  L_table    VARCHAR2(30) := 'SVC_ITEM_SUPP_UOM';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT VALUE_dv,
    UOM_dv,
    SUPPLIER_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_SUPP_UOM'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'VALUE' AS VALUE, 'UOM' AS UOM, 'SUPPLIER' AS SUPPLIER, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.VALUE := rec.VALUE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_UOM',NULL,'VALUE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.UOM := rec.UOM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_UOM',NULL,'UOM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPPLIER := rec.SUPPLIER_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_UOM',NULL,'SUPPLIER','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SUPP_UOM',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(ISU_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(ISU_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(ISU$Action) AS Action,
    r.get_cell(ISU$VALUE)    AS VALUE,
    NVL(uom_sql.get_uom(r.get_cell(ISU$UOM),'Upload'),r.get_cell(ISU$UOM))      AS UOM,
    r.get_cell(ISU$SUPPLIER)     AS SUPPLIER,
    r.get_cell(ISU$ITEM)     AS ITEM,
    r.get_row_seq()      AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISU_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.VALUE := rec.VALUE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISU_sheet,rec.row_seq,'VALUE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.UOM := rec.UOM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISU_sheet,rec.row_seq,'UOM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPPLIER := rec.SUPPLIER;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISU_sheet,rec.row_seq,'SUPPLIER',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISU_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_ITEM_SUPP_UOM%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_ITEM_SUPP_UOM
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND UOM = l_temp_rec.UOM
;
    l_rms_rec ITEM_SUPP_UOM%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM ITEM_SUPP_UOM
       WHERE ITEM     = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND UOM = l_temp_rec.UOM
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_ITEM_SUPP_UOM
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.VALUE_mi <> 'Y' THEN
        l_temp_rec.VALUE   := l_stg_rec.VALUE;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.VALUE_mi <> 'Y' THEN
        l_temp_rec.VALUE   := l_rms_rec.VALUE;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,ISU_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.SUPPLIER := NVL( l_temp_rec.SUPPLIER,l_default_rec.SUPPLIER);
      l_temp_rec.UOM := NVL( l_temp_rec.UOM,l_default_rec.UOM);
      l_temp_rec.VALUE := NVL( l_temp_rec.VALUE,l_default_rec.VALUE);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.SUPPLIER IS NOT NULL
        AND l_temp_rec.UOM IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,ISU_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_SUPP_UOM VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,ISU_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_SUPP_UOM
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND SUPPLIER = svc_upd_col(i).SUPPLIER
     AND UOM = svc_upd_col(i).UOM
;
END process_s9t_ISU;
-------------------------------------------------------------------------------------------------------
 PROCEDURE process_s9t_IXD
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_XFORM_DETAIL.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_XFORM_DETAIL%rowtype;
  l_temp_rec SVC_ITEM_XFORM_DETAIL%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_XFORM_DETAIL.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_XFORM_DETAIL%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT YIELD_FROM_HEAD_ITEM_PCT_mi,
      ITEM_QUANTITY_PCT_mi,
      DETAIL_ITEM_mi,
      HEAD_ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_XFORM_DETAIL'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'YIELD_FROM_HEAD_ITEM_PCT' AS YIELD_FROM_HEAD_ITEM_PCT, 'ITEM_QUANTITY_PCT' AS ITEM_QUANTITY_PCT, 'DETAIL_ITEM' AS DETAIL_ITEM, 'HEAD_ITEM' AS HEAD_ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'head item and detail item';
  L_table    VARCHAR2(30) := 'SVC_ITEM_XFORM_DETAIL';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT YIELD_FROM_HEAD_ITEM_PCT_dv,
    ITEM_QUANTITY_PCT_dv,
    DETAIL_ITEM_dv,
    HEAD_ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_XFORM_DETAIL'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'YIELD_FROM_HEAD_ITEM_PCT' AS YIELD_FROM_HEAD_ITEM_PCT, 'ITEM_QUANTITY_PCT' AS ITEM_QUANTITY_PCT, 'DETAIL_ITEM' AS DETAIL_ITEM, 'HEAD_ITEM' AS HEAD_ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.YIELD_FROM_HEAD_ITEM_PCT := rec.YIELD_FROM_HEAD_ITEM_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_XFORM_DETAIL',NULL,'YIELD_FROM_HEAD_ITEM_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM_QUANTITY_PCT := rec.ITEM_QUANTITY_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_XFORM_DETAIL',NULL,'ITEM_QUANTITY_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.DETAIL_ITEM := rec.DETAIL_ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_XFORM_DETAIL',NULL,'DETAIL_ITEM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.HEAD_ITEM := rec.HEAD_ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_XFORM_DETAIL',NULL,'HEAD_ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(IXD_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(IXD_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(IXD$Action)         AS Action,
    r.get_cell(IXD$YIELD_FROM_HEAD_ITEM_PCT) AS YIELD_FROM_HEAD_ITEM_PCT,
    r.get_cell(IXD$ITEM_QUANTITY_PCT)        AS ITEM_QUANTITY_PCT,
    r.get_cell(IXD$DETAIL_ITEM)          AS DETAIL_ITEM,
    r.get_cell(IXD$HEAD_ITEM)            AS HEAD_ITEM,
    r.get_row_seq()              AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IXD_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.YIELD_FROM_HEAD_ITEM_PCT := rec.YIELD_FROM_HEAD_ITEM_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IXD_sheet,rec.row_seq,'YIELD_FROM_HEAD_ITEM_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_QUANTITY_PCT := rec.ITEM_QUANTITY_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IXD_sheet,rec.row_seq,'ITEM_QUANTITY_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DETAIL_ITEM := rec.DETAIL_ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IXD_sheet,rec.row_seq,'DETAIL_ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.HEAD_ITEM := rec.HEAD_ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IXD_sheet,rec.row_seq,'HEAD_ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_ITEM_XFORM_DETAIL%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_ITEM_XFORM_DETAIL
       WHERE HEAD_ITEM     = l_temp_rec.HEAD_ITEM
         AND DETAIL_ITEM = l_temp_rec.DETAIL_ITEM
;
    l_rms_rec ITEM_XFORM_DETAIL%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT ixd.*
        FROM ITEM_XFORM_DETAIL ixd,
         ITEM_XFORM_HEAD ixh
       WHERE ixh.HEAD_ITEM     = l_temp_rec.HEAD_ITEM
         AND ixd.DETAIL_ITEM = l_temp_rec.DETAIL_ITEM
         AND ixd.ITEM_XFORM_HEAD_ID = ixh.ITEM_XFORM_HEAD_ID
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_ITEM_XFORM_DETAIL
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.ITEM_QUANTITY_PCT_mi <> 'Y' THEN
        l_temp_rec.ITEM_QUANTITY_PCT   := l_stg_rec.ITEM_QUANTITY_PCT;
      END IF;
      IF l_mi_rec.YIELD_FROM_HEAD_ITEM_PCT_mi <> 'Y' THEN
        l_temp_rec.YIELD_FROM_HEAD_ITEM_PCT   := l_stg_rec.YIELD_FROM_HEAD_ITEM_PCT;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.ITEM_QUANTITY_PCT_mi <> 'Y' THEN
        l_temp_rec.ITEM_QUANTITY_PCT   := l_rms_rec.ITEM_QUANTITY_PCT;
      END IF;
      IF l_mi_rec.YIELD_FROM_HEAD_ITEM_PCT_mi <> 'Y' THEN
        l_temp_rec.YIELD_FROM_HEAD_ITEM_PCT   := l_rms_rec.YIELD_FROM_HEAD_ITEM_PCT;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,IXD_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM_XFORM_DETAIL_ID := NVL( l_temp_rec.ITEM_XFORM_DETAIL_ID,l_default_rec.ITEM_XFORM_DETAIL_ID);
      l_temp_rec.HEAD_ITEM := NVL( l_temp_rec.HEAD_ITEM,l_default_rec.HEAD_ITEM);
      l_temp_rec.ITEM_XFORM_HEAD_ID := NVL( l_temp_rec.ITEM_XFORM_HEAD_ID,l_default_rec.ITEM_XFORM_HEAD_ID);
      l_temp_rec.DETAIL_ITEM := NVL( l_temp_rec.DETAIL_ITEM,l_default_rec.DETAIL_ITEM);
      l_temp_rec.ITEM_QUANTITY_PCT := NVL( l_temp_rec.ITEM_QUANTITY_PCT,l_default_rec.ITEM_QUANTITY_PCT);
      l_temp_rec.YIELD_FROM_HEAD_ITEM_PCT := NVL( l_temp_rec.YIELD_FROM_HEAD_ITEM_PCT,l_default_rec.YIELD_FROM_HEAD_ITEM_PCT);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.HEAD_ITEM IS NOT NULL
        AND l_temp_rec.DETAIL_ITEM IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,IXD_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_XFORM_DETAIL VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,IXD_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_XFORM_DETAIL
     SET row         = svc_upd_col(i)
   WHERE HEAD_ITEM     = svc_upd_col(i).HEAD_ITEM
     AND DETAIL_ITEM = svc_upd_col(i).DETAIL_ITEM
;
END process_s9t_IXD;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_IXH
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_XFORM_HEAD.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_XFORM_HEAD%rowtype;
  l_temp_rec SVC_ITEM_XFORM_HEAD%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_XFORM_HEAD.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_XFORM_HEAD%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT COMMENTS_DESC_mi,
      PRODUCTION_LOSS_PCT_mi,
      ITEM_XFORM_DESC_mi,
      ITEM_XFORM_TYPE_mi,
      HEAD_ITEM_mi,
      ITEM_XFORM_HEAD_ID_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_XFORM_HEAD'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'COMMENTS_DESC' AS COMMENTS_DESC, 'PRODUCTION_LOSS_PCT' AS PRODUCTION_LOSS_PCT, 'ITEM_XFORM_DESC' AS ITEM_XFORM_DESC, 'ITEM_XFORM_TYPE' AS ITEM_XFORM_TYPE, 'HEAD_ITEM' AS HEAD_ITEM, 'ITEM_XFORM_HEAD_ID' AS ITEM_XFORM_HEAD_ID, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'head item';
  L_table    VARCHAR2(30) := 'SVC_ITEM_XFORM_HEAD';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT COMMENTS_DESC_dv,
    PRODUCTION_LOSS_PCT_dv,
    ITEM_XFORM_DESC_dv,
    ITEM_XFORM_TYPE_dv,
    HEAD_ITEM_dv,
    ITEM_XFORM_HEAD_ID_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_XFORM_HEAD'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'COMMENTS_DESC' AS COMMENTS_DESC, 'PRODUCTION_LOSS_PCT' AS PRODUCTION_LOSS_PCT, 'ITEM_XFORM_DESC' AS ITEM_XFORM_DESC, 'ITEM_XFORM_TYPE' AS ITEM_XFORM_TYPE, 'HEAD_ITEM' AS HEAD_ITEM, 'ITEM_XFORM_HEAD_ID' AS ITEM_XFORM_HEAD_ID, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.COMMENTS_DESC := rec.COMMENTS_DESC_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_XFORM_HEAD',NULL,'COMMENTS_DESC','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PRODUCTION_LOSS_PCT := rec.PRODUCTION_LOSS_PCT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_XFORM_HEAD',NULL,'PRODUCTION_LOSS_PCT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM_XFORM_DESC := rec.ITEM_XFORM_DESC_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_XFORM_HEAD',NULL,'ITEM_XFORM_DESC','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM_XFORM_TYPE := rec.ITEM_XFORM_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_XFORM_HEAD',NULL,'ITEM_XFORM_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.HEAD_ITEM := rec.HEAD_ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_XFORM_HEAD',NULL,'HEAD_ITEM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM_XFORM_HEAD_ID := rec.ITEM_XFORM_HEAD_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_XFORM_HEAD',NULL,'ITEM_XFORM_HEAD_ID','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(IXH_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(IXH_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(IXH$Action)    AS Action,
    r.get_cell(IXH$COMMENTS_DESC)   AS COMMENTS_DESC,
    r.get_cell(IXH$PRODUCTION_LOSS_PCT) AS PRODUCTION_LOSS_PCT,
    r.get_cell(IXH$ITEM_XFORM_DESC) AS ITEM_XFORM_DESC,
    r.get_cell(IXH$ITEM_XFORM_TYPE) AS ITEM_XFORM_TYPE,
    r.get_cell(IXH$HEAD_ITEM)       AS HEAD_ITEM,
    r.get_cell(IXH$ITEM_XFORM_HEAD_ID)  AS ITEM_XFORM_HEAD_ID,
    r.get_row_seq()         AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IXH_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COMMENTS_DESC := rec.COMMENTS_DESC;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IXH_sheet,rec.row_seq,'COMMENTS_DESC',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PRODUCTION_LOSS_PCT := rec.PRODUCTION_LOSS_PCT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IXH_sheet,rec.row_seq,'PRODUCTION_LOSS_PCT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_XFORM_DESC := rec.ITEM_XFORM_DESC;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IXH_sheet,rec.row_seq,'ITEM_XFORM_DESC',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_XFORM_TYPE := rec.ITEM_XFORM_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IXH_sheet,rec.row_seq,'ITEM_XFORM_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.HEAD_ITEM := rec.HEAD_ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IXH_sheet,rec.row_seq,'HEAD_ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_XFORM_HEAD_ID := rec.ITEM_XFORM_HEAD_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IXH_sheet,rec.row_seq,'ITEM_XFORM_HEAD_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_ITEM_XFORM_HEAD%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_ITEM_XFORM_HEAD
       WHERE HEAD_ITEM     = l_temp_rec.HEAD_ITEM
;
    l_rms_rec ITEM_XFORM_HEAD%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM ITEM_XFORM_HEAD
       WHERE HEAD_ITEM     = l_temp_rec.HEAD_ITEM
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_ITEM_XFORM_HEAD
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.ITEM_XFORM_HEAD_ID_mi <> 'Y' THEN
        l_temp_rec.ITEM_XFORM_HEAD_ID   := l_stg_rec.ITEM_XFORM_HEAD_ID;
      END IF;
      IF l_mi_rec.ITEM_XFORM_TYPE_mi <> 'Y' THEN
        l_temp_rec.ITEM_XFORM_TYPE   := l_stg_rec.ITEM_XFORM_TYPE;
      END IF;
      IF l_mi_rec.ITEM_XFORM_DESC_mi <> 'Y' THEN
        l_temp_rec.ITEM_XFORM_DESC   := l_stg_rec.ITEM_XFORM_DESC;
      END IF;
      IF l_mi_rec.PRODUCTION_LOSS_PCT_mi <> 'Y' THEN
        l_temp_rec.PRODUCTION_LOSS_PCT   := l_stg_rec.PRODUCTION_LOSS_PCT;
      END IF;
      IF l_mi_rec.COMMENTS_DESC_mi <> 'Y' THEN
        l_temp_rec.COMMENTS_DESC   := l_stg_rec.COMMENTS_DESC;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.ITEM_XFORM_HEAD_ID_mi <> 'Y' THEN
        l_temp_rec.ITEM_XFORM_HEAD_ID   := l_rms_rec.ITEM_XFORM_HEAD_ID;
      END IF;
      IF l_mi_rec.ITEM_XFORM_TYPE_mi <> 'Y' THEN
        l_temp_rec.ITEM_XFORM_TYPE   := l_rms_rec.ITEM_XFORM_TYPE;
      END IF;
      IF l_mi_rec.ITEM_XFORM_DESC_mi <> 'Y' THEN
        l_temp_rec.ITEM_XFORM_DESC   := l_rms_rec.ITEM_XFORM_DESC;
      END IF;
      IF l_mi_rec.PRODUCTION_LOSS_PCT_mi <> 'Y' THEN
        l_temp_rec.PRODUCTION_LOSS_PCT   := l_rms_rec.PRODUCTION_LOSS_PCT;
      END IF;
      IF l_mi_rec.COMMENTS_DESC_mi <> 'Y' THEN
        l_temp_rec.COMMENTS_DESC   := l_rms_rec.COMMENTS_DESC;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,IXH_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM_XFORM_HEAD_ID := NVL( l_temp_rec.ITEM_XFORM_HEAD_ID,l_default_rec.ITEM_XFORM_HEAD_ID);
      l_temp_rec.HEAD_ITEM := NVL( l_temp_rec.HEAD_ITEM,l_default_rec.HEAD_ITEM);
      l_temp_rec.ITEM_XFORM_TYPE := NVL( l_temp_rec.ITEM_XFORM_TYPE,l_default_rec.ITEM_XFORM_TYPE);
      l_temp_rec.ITEM_XFORM_DESC := NVL( l_temp_rec.ITEM_XFORM_DESC,l_default_rec.ITEM_XFORM_DESC);
      l_temp_rec.PRODUCTION_LOSS_PCT := NVL( l_temp_rec.PRODUCTION_LOSS_PCT,l_default_rec.PRODUCTION_LOSS_PCT);
      l_temp_rec.COMMENTS_DESC := NVL( l_temp_rec.COMMENTS_DESC,l_default_rec.COMMENTS_DESC);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.HEAD_ITEM IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,IXH_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_XFORM_HEAD VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,IXH_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_XFORM_HEAD
     SET row         = svc_upd_col(i)
   WHERE HEAD_ITEM     = svc_upd_col(i).HEAD_ITEM
;
END process_s9t_IXH;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_IXH_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                             I_process_id   IN   SVC_ITEM_XFORM_HEAD_TL.PROCESS_ID%TYPE)
IS

   TYPE SVC_COL_TYP is TABLE OF SVC_ITEM_XFORM_HEAD_TL%ROWTYPE;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   L_error_code    NUMBER;
   L_stg_exists    BOOLEAN;
   L_error         BOOLEAN       := FALSE;
   L_pk_columns    VARCHAR2(255) := 'lang and item';
   L_table         VARCHAR2(30)  := 'SVC_ITEM_XFORM_HEAD_TL';
   svc_ins_col     SVC_COL_TYP   := NEW SVC_COL_TYP();
   svc_upd_col     SVC_COL_TYP   := NEW SVC_COL_TYP();
   L_process_id    SVC_ITEM_XFORM_HEAD_TL.PROCESS_ID%TYPE;
   L_temp_rec      SVC_ITEM_XFORM_HEAD_TL%ROWTYPE;
   L_default_rec   SVC_ITEM_XFORM_HEAD_TL%ROWTYPE;
   L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
   dml_errors      EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);

   cursor C_MANDATORY_IND is
      select item_xform_desc_mi,
             item_xform_head_id_mi,
             head_item_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = ITEM_INDUCT_SQL.TEMPLATE_KEY
                 and wksht_key = 'ITEM_XFORM_HEAD_TL')
       pivot (MAX(mandatory) as mi FOR (column_key) in ('ITEM_XFORM_DESC' as item_xform_desc,
                                                        'ITEM_XFORM_HEAD_ID' as item_xform_head_id,
                                                        'HEAD_ITEM' as head_item,
                                                        'LANG' as lang));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;

BEGIN
   -- Get default values.
   FOR rec in (select item_xform_desc_dv,
                      item_xform_head_id_dv,
                      head_item_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = ITEM_INDUCT_SQL.TEMPLATE_KEY
                          and wksht_key = 'ITEM_XFORM_HEAD_TL')
                pivot (MAX(default_value) as dv FOR (column_key) in ('ITEM_XFORM_DESC' as item_xform_desc,
                                                                     'ITEM_XFORM_HEAD_ID' as item_xform_head_id,
                                                                     'HEAD_ITEM' as head_item,
                                                                     'LANG' as lang))
              ) LOOP
      BEGIN
         L_default_rec.item_xform_desc := rec.item_xform_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_XFORM_HEAD_TL',
                            NULL,
                            'ITEM_XFORM_DESC',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.item_xform_head_id := rec.item_xform_head_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_XFORM_HEAD_TL',
                            NULL,
                            'ITEM_XFORM_HEAD_ID',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.head_item := rec.head_item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_XFORM_HEAD_TL',
                            NULL,
                            'HEAD_ITEM',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_XFORM_HEAD_TL',
                            NULL,
                            'LANG',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

   --get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   --
   -- XXADEO_RMS: Assigned get_sheet_name_trans(IXHTL_sheet) value to variable L_sheet_name
   --
   L_sheet_name := GET_SHEET_NAME_TRANS(IXHTL_sheet);
   --
   FOR rec in (select r.get_cell(ixhtl$action)                as action,
                      r.get_cell(ixhtl$item_xform_desc)       as item_xform_desc,
                      r.get_cell(ixhtl$item_xform_head_id)    as item_xform_head_id,
                      r.get_cell(ixhtl$head_item)             as head_item,
                      r.get_cell(ixhtl$lang)                  as lang,
                      r.get_row_seq()                         as row_seq
                 from s9t_folder sf,
                      table(sf.s9t_file_obj.sheets) ss,
                      table(ss.s9t_rows) r
                where sf.file_id = I_file_id
                  and ss.sheet_name = L_sheet_name) LOOP

      L_temp_rec.process_id           := I_process_id;
      L_temp_rec.chunk_id             := 1;
      L_temp_rec.row_seq              := rec.row_seq;
      L_temp_rec.process$status       := 'N';
      L_temp_rec.create_id            := GET_USER;
      L_temp_rec.last_update_id       := GET_USER;
      L_temp_rec.create_datetime      := SYSDATE;
      L_temp_rec.last_update_datetime := SYSDATE;
      L_error                         := FALSE;
      ---
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IXHTL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item_xform_desc := rec.item_xform_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IXHTL_sheet,
                            rec.row_seq,
                            'ITEM_XFORM_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item_xform_head_id := rec.item_xform_head_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IXHTL_sheet,
                            rec.row_seq,
                            'ITEM_XFORM_HEAD_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.head_item := rec.head_item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IXHTL_sheet,
                            rec.row_seq,
                            'HEAD_ITEM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IXHTL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      ---
      if NOT L_error then
         DECLARE
            L_stg_rec      SVC_ITEM_XFORM_HEAD_TL%ROWTYPE;
            L_rms_rec      ITEM_XFORM_HEAD_TL%ROWTYPE;
            L_rms_exists   BOOLEAN;

            cursor C_STG_REC is
               select *
                 from svc_item_xform_head_tl
                where head_item = L_temp_rec.head_item
                  and lang = L_temp_rec.lang
                  and item_xform_head_id = L_temp_rec.item_xform_head_id;

            cursor C_RMS_REC is
               select *
                 from item_xform_head_tl
                where item_xform_head_id = L_temp_rec.item_xform_head_id
                  and lang = L_temp_rec.lang;

         BEGIN
            open C_STG_REC;
            fetch C_STG_REC into L_stg_rec;
            if C_STG_REC%FOUND then
               L_stg_exists := TRUE;
            else
               L_stg_exists := FALSE;
            end if;
            close C_STG_REC;
            ---
            open C_RMS_REC;
            fetch C_RMS_REC into L_rms_rec;
            if C_RMS_REC%FOUND then
               L_rms_exists := TRUE;
            else
               L_rms_exists := FALSE;
            end if;
            close C_RMS_REC;
            -- delete record from staging if it is already uploaded to rms.
            if L_stg_exists and L_stg_rec.process$status = 'P' and GET_PRC_DEST(L_stg_rec.process_id) = 'RMS' then
               delete from svc_item_xform_head_tl
                where process_id = L_stg_rec.process_id
                  and row_seq = L_stg_rec.row_seq;
               L_stg_exists := FALSE;
            end if;
            --resolve new value for action column
            if L_stg_exists then
               -- if it is a create record and record already exists in staging, log an error
               if L_temp_rec.action = CORESVC_ITEM.ACTION_NEW then
                  if L_stg_rec.process$status = 'P' then
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS',
                                                       L_pk_columns);
                  else
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                       L_pk_columns);
                  end if;
                  WRITE_S9T_ERROR (I_file_id,
                                   IXHTL_sheet,
                                   L_temp_rec.row_seq,
                                   NULL,
                                   NULL,
                                   L_error_msg );
                  L_error := TRUE;
               else
                  -- if record does not exist in staging then keep the user provided value
                  L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,
                                                      L_temp_rec.action);
               end if;
            end if;
         END;
      end if;
      -- apply defaults if it is a create record and record does not exist in staging already
      if rec.action = CORESVC_ITEM.ACTION_NEW and NOT L_error and NOT L_stg_exists then
         L_temp_rec.lang := NVL( L_temp_rec.lang, L_default_rec.lang);
         L_temp_rec.head_item := NVL(L_temp_rec.head_item, L_default_rec.head_item);
         L_temp_rec.item_xform_head_id := NVL(L_temp_rec.item_xform_head_id, L_default_rec.item_xform_head_id);
         L_temp_rec.item_xform_desc := NVL( L_temp_rec.item_xform_desc, L_default_rec.item_xform_desc);
      end if;
      -- check pk cols. log error if any pk col is NULL.
      if NOT (L_temp_rec.head_item is NOT NULL and L_temp_rec.lang is NOT NULL) then
         L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                           L_pk_columns);
         WRITE_S9T_ERROR(I_file_id,
                         IXHTL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         L_error_msg);
         L_error := TRUE;
      end if;
      -- if no error so far then add temp record to insert or update collection depending upon
      -- whether record already exists in staging or not.
      if NOT L_error then
         if L_stg_exists then
            svc_upd_col.extend();
            svc_upd_col(svc_upd_col.COUNT()) := L_temp_rec;
         else
            svc_ins_col.extend();
            svc_ins_col(svc_ins_col.COUNT()) := L_temp_rec;
         end if;
      end if;
   END LOOP;

   -- flush insert collection
   BEGIN
      FORALL i in 1..svc_ins_col.COUNT SAVE EXCEPTIONS
      insert into svc_item_xform_head_tl values svc_ins_col(i);

   EXCEPTION
      when DML_ERRORS then
         for i in 1..sql%bulk_exceptions.COUNT loop
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                 L_pk_columns,
                                                 L_table);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            IXHTL_sheet,
                            svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         end loop;
   END;

   -- flush update collection
   FORALL i in 1..svc_upd_col.COUNT
      update svc_item_xform_head_tl
         set row = svc_upd_col(i)
       where head_item = svc_upd_col(i).head_item
         and lang = svc_upd_col(i).lang;

END PROCESS_S9T_IXH_TL;
-------------------------------------------------------------------------------------
PROCEDURE process_s9t_PI
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_PACKITEM.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_PACKITEM%rowtype;
  l_temp_rec SVC_PACKITEM%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_PACKITEM.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_PACKITEM%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT PACK_QTY_mi,
      PACK_TMPL_ID_mi,
      ITEM_PARENT_mi,
      ITEM_mi,
      SEQ_NO_mi,
      PACK_NO_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'PACKITEM'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'PACK_QTY' AS PACK_QTY, 'PACK_TMPL_ID' AS PACK_TMPL_ID, 'ITEM_PARENT' AS ITEM_PARENT, 'ITEM' AS ITEM, 'SEQ_NO' AS SEQ_NO, 'PACK_NO' AS PACK_NO, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := ' item or item parent and pack no';
  L_table    VARCHAR2(30) := 'SVC_PACKITEM';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT PACK_QTY_dv,
    PACK_TMPL_ID_dv,
    ITEM_PARENT_dv,
    ITEM_dv,
    SEQ_NO_dv,
    PACK_NO_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'PACKITEM'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'PACK_QTY' AS PACK_QTY, 'PACK_TMPL_ID' AS PACK_TMPL_ID, 'ITEM_PARENT' AS ITEM_PARENT, 'ITEM' AS ITEM, 'SEQ_NO' AS SEQ_NO, 'PACK_NO' AS PACK_NO, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.PACK_QTY := rec.PACK_QTY_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'PACKITEM',NULL,'PACK_QTY','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PACK_TMPL_ID := rec.PACK_TMPL_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'PACKITEM',NULL,'PACK_TMPL_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM_PARENT := rec.ITEM_PARENT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'PACKITEM',NULL,'ITEM_PARENT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'PACKITEM',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SEQ_NO := rec.SEQ_NO_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'PACKITEM',NULL,'SEQ_NO','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PACK_NO := rec.PACK_NO_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'PACKITEM',NULL,'PACK_NO','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(PI_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(PI_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(PI$Action) AS Action,
    r.get_cell(PI$PACK_QTY) AS PACK_QTY,
    r.get_cell(PI$PACK_TMPL_ID) AS PACK_TMPL_ID,
    r.get_cell(PI$ITEM_PARENT)  AS ITEM_PARENT,
    r.get_cell(PI$ITEM)     AS ITEM,
    r.get_cell(PI$SEQ_NO)   AS SEQ_NO,
    r.get_cell(PI$PACK_NO)  AS PACK_NO,
    r.get_row_seq()     AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,PI_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PACK_QTY := rec.PACK_QTY;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,PI_sheet,rec.row_seq,'PACK_QTY',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PACK_TMPL_ID := rec.PACK_TMPL_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,PI_sheet,rec.row_seq,'PACK_TMPL_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_PARENT := rec.ITEM_PARENT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,PI_sheet,rec.row_seq,'ITEM_PARENT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,PI_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SEQ_NO := rec.SEQ_NO;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,PI_sheet,rec.row_seq,'SEQ_NO',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PACK_NO := rec.PACK_NO;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,PI_sheet,rec.row_seq,'PACK_NO',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_PACKITEM%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_PACKITEM
       WHERE PACK_NO     = l_temp_rec.PACK_NO
         AND ITEM = l_temp_rec.ITEM
         AND ITEM_PARENT = l_temp_rec.ITEM_PARENT
         AND PACK_TMPL_ID = l_temp_rec.PACK_TMPL_ID
;
    l_rms_rec PACKITEM%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM PACKITEM
       WHERE PACK_NO     = l_temp_rec.PACK_NO
         AND ITEM = l_temp_rec.ITEM
         AND ITEM_PARENT = l_temp_rec.ITEM_PARENT
         AND PACK_TMPL_ID = l_temp_rec.PACK_TMPL_ID
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_PACKITEM
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.SEQ_NO_mi <> 'Y' THEN
        l_temp_rec.SEQ_NO   := l_stg_rec.SEQ_NO;
      END IF;
      IF l_mi_rec.PACK_QTY_mi <> 'Y' THEN
        l_temp_rec.PACK_QTY   := l_stg_rec.PACK_QTY;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.SEQ_NO_mi <> 'Y' THEN
        l_temp_rec.SEQ_NO   := l_rms_rec.SEQ_NO;
      END IF;
      IF l_mi_rec.PACK_QTY_mi <> 'Y' THEN
        l_temp_rec.PACK_QTY   := l_rms_rec.PACK_QTY;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,PI_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.PACK_NO := NVL( l_temp_rec.PACK_NO,l_default_rec.PACK_NO);
      l_temp_rec.SEQ_NO := NVL( l_temp_rec.SEQ_NO,l_default_rec.SEQ_NO);
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.ITEM_PARENT := NVL( l_temp_rec.ITEM_PARENT,l_default_rec.ITEM_PARENT);
      l_temp_rec.PACK_TMPL_ID := NVL( l_temp_rec.PACK_TMPL_ID,l_default_rec.PACK_TMPL_ID);
      l_temp_rec.PACK_QTY := NVL( l_temp_rec.PACK_QTY,l_default_rec.PACK_QTY);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.PACK_NO IS NOT NULL
        AND NVL(l_temp_rec.ITEM,l_temp_rec.ITEM_PARENT) IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,PI_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_PACKITEM VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,PI_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_PACKITEM
     SET row         = svc_upd_col(i)
   WHERE PACK_NO     = svc_upd_col(i).PACK_NO
     AND ITEM = svc_upd_col(i).ITEM
     AND ITEM_PARENT = svc_upd_col(i).ITEM_PARENT
     AND PACK_TMPL_ID = svc_upd_col(i).PACK_TMPL_ID
;
END process_s9t_PI;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_IZP
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_RPM_ITEM_ZONE_PRICE.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_RPM_ITEM_ZONE_PRICE%rowtype;
  l_temp_rec SVC_RPM_ITEM_ZONE_PRICE%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_RPM_ITEM_ZONE_PRICE.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_RPM_ITEM_ZONE_PRICE%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT LOCK_VERSION_mi,
      MULTI_SELLING_UOM_mi,
      MULTI_UNIT_RETAIL_CURRENCY_mi,
      MULTI_UNIT_RETAIL_mi,
      MULTI_UNITS_mi,
      SELLING_UOM_mi,
      SELLING_RETAIL_mi,
      STANDARD_UOM_mi,
      STANDARD_RETAIL_CURRENCY_mi,
      STANDARD_RETAIL_mi,
      ZONE_ID_mi,
      ITEM_mi,
      ITEM_ZONE_PRICE_ID_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'RPM_ITEM_ZONE_PRICE'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'LOCK_VERSION' AS LOCK_VERSION, 'MULTI_SELLING_UOM' AS MULTI_SELLING_UOM, 'MULTI_UNIT_RETAIL_CURRENCY' AS MULTI_UNIT_RETAIL_CURRENCY, 'MULTI_UNIT_RETAIL' AS MULTI_UNIT_RETAIL, 'MULTI_UNITS' AS MULTI_UNITS, 'SELLING_UOM' AS SELLING_UOM, 'SELLING_RETAIL' AS SELLING_RETAIL, 'STANDARD_UOM' AS STANDARD_UOM, 'STANDARD_RETAIL_CURRENCY' AS STANDARD_RETAIL_CURRENCY, 'STANDARD_RETAIL' AS STANDARD_RETAIL, 'ZONE_ID' AS ZONE_ID, 'ITEM' AS ITEM, 'ITEM_ZONE_PRICE_ID' AS ITEM_ZONE_PRICE_ID, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item and zone id';
  L_table    VARCHAR2(30) := 'SVC_RPM_ITEM_ZONE_PRICE';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT LOCK_VERSION_dv,
    MULTI_SELLING_UOM_dv,
    MULTI_UNIT_RETAIL_CURRENCY_dv,
    MULTI_UNIT_RETAIL_dv,
    MULTI_UNITS_dv,
    SELLING_UOM_dv,
    SELLING_RETAIL_dv,
    STANDARD_UOM_dv,
    STANDARD_RETAIL_CURRENCY_dv,
    STANDARD_RETAIL_dv,
    ZONE_ID_dv,
    ITEM_dv,
    ITEM_ZONE_PRICE_ID_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'RPM_ITEM_ZONE_PRICE'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'LOCK_VERSION' AS LOCK_VERSION, 'MULTI_SELLING_UOM' AS MULTI_SELLING_UOM, 'MULTI_UNIT_RETAIL_CURRENCY' AS MULTI_UNIT_RETAIL_CURRENCY, 'MULTI_UNIT_RETAIL' AS MULTI_UNIT_RETAIL, 'MULTI_UNITS' AS MULTI_UNITS, 'SELLING_UOM' AS SELLING_UOM, 'SELLING_RETAIL' AS SELLING_RETAIL, 'STANDARD_UOM' AS STANDARD_UOM, 'STANDARD_RETAIL_CURRENCY' AS STANDARD_RETAIL_CURRENCY, 'STANDARD_RETAIL' AS STANDARD_RETAIL, 'ZONE_ID' AS ZONE_ID, 'ITEM' AS ITEM, 'ITEM_ZONE_PRICE_ID' AS ITEM_ZONE_PRICE_ID, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.LOCK_VERSION := rec.LOCK_VERSION_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'LOCK_VERSION','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.MULTI_SELLING_UOM := rec.MULTI_SELLING_UOM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'MULTI_SELLING_UOM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.MULTI_UNIT_RETAIL_CURRENCY := rec.MULTI_UNIT_RETAIL_CURRENCY_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'MULTI_UNIT_RETAIL_CURRENCY','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.MULTI_UNIT_RETAIL := rec.MULTI_UNIT_RETAIL_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'MULTI_UNIT_RETAIL','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.MULTI_UNITS := rec.MULTI_UNITS_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'MULTI_UNITS','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SELLING_UOM := rec.SELLING_UOM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'SELLING_UOM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SELLING_RETAIL := rec.SELLING_RETAIL_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'SELLING_RETAIL','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.STANDARD_UOM := rec.STANDARD_UOM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'STANDARD_UOM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.STANDARD_RETAIL_CURRENCY := rec.STANDARD_RETAIL_CURRENCY_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'STANDARD_RETAIL_CURRENCY','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.STANDARD_RETAIL := rec.STANDARD_RETAIL_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'STANDARD_RETAIL','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ZONE_ID := rec.ZONE_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'ZONE_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM_ZONE_PRICE_ID := rec.ITEM_ZONE_PRICE_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'RPM_ITEM_ZONE_PRICE',NULL,'ITEM_ZONE_PRICE_ID','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(IZP_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(IZP_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(IZP$Action)           AS Action,
    r.get_cell(IZP$LOCK_VERSION)           AS LOCK_VERSION,
    r.get_cell(IZP$MULTI_SELLING_UOM)          AS MULTI_SELLING_UOM,
    r.get_cell(IZP$MULTI_UNIT_RETAIL_CURRENCY) AS MULTI_UNIT_RETAIL_CURRENCY,
    r.get_cell(IZP$MULTI_UNIT_RETAIL)          AS MULTI_UNIT_RETAIL,
    r.get_cell(IZP$MULTI_UNITS)            AS MULTI_UNITS,
    r.get_cell(IZP$SELLING_UOM)            AS SELLING_UOM,
    r.get_cell(IZP$SELLING_RETAIL)         AS SELLING_RETAIL,
    r.get_cell(IZP$STANDARD_UOM)           AS STANDARD_UOM,
    r.get_cell(IZP$STANDARD_RETAIL_CURRENCY)   AS STANDARD_RETAIL_CURRENCY,
    r.get_cell(IZP$STANDARD_RETAIL)        AS STANDARD_RETAIL,
    r.get_cell(IZP$ZONE_ID)            AS ZONE_ID,
    r.get_cell(IZP$ITEM)               AS ITEM,
    r.get_cell(IZP$ITEM_ZONE_PRICE_ID)         AS ITEM_ZONE_PRICE_ID,
    r.get_row_seq()                AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.LOCK_VERSION := rec.LOCK_VERSION;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'LOCK_VERSION',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.MULTI_SELLING_UOM := rec.MULTI_SELLING_UOM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'MULTI_SELLING_UOM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.MULTI_UNIT_RETAIL_CURRENCY := rec.MULTI_UNIT_RETAIL_CURRENCY;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'MULTI_UNIT_RETAIL_CURRENCY',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.MULTI_UNIT_RETAIL := rec.MULTI_UNIT_RETAIL;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'MULTI_UNIT_RETAIL',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.MULTI_UNITS := rec.MULTI_UNITS;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'MULTI_UNITS',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SELLING_UOM := rec.SELLING_UOM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'SELLING_UOM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SELLING_RETAIL := rec.SELLING_RETAIL;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'SELLING_RETAIL',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.STANDARD_UOM := rec.STANDARD_UOM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'STANDARD_UOM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.STANDARD_RETAIL_CURRENCY := rec.STANDARD_RETAIL_CURRENCY;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'STANDARD_RETAIL_CURRENCY',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.STANDARD_RETAIL := rec.STANDARD_RETAIL;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'STANDARD_RETAIL',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ZONE_ID := rec.ZONE_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'ZONE_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_ZONE_PRICE_ID := rec.ITEM_ZONE_PRICE_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,'ITEM_ZONE_PRICE_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_RPM_ITEM_ZONE_PRICE%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_RPM_ITEM_ZONE_PRICE
       WHERE ITEM     = l_temp_rec.ITEM
         AND ZONE_ID = l_temp_rec.ZONE_ID
;
    l_rms_rec RPM_ITEM_ZONE_PRICE%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM RPM_ITEM_ZONE_PRICE
       WHERE ITEM     = l_temp_rec.ITEM
         AND ZONE_ID = l_temp_rec.ZONE_ID
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_RPM_ITEM_ZONE_PRICE
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.ITEM_ZONE_PRICE_ID_mi <> 'Y' THEN
        l_temp_rec.ITEM_ZONE_PRICE_ID   := l_stg_rec.ITEM_ZONE_PRICE_ID;
      END IF;
      IF l_mi_rec.STANDARD_RETAIL_mi <> 'Y' THEN
        l_temp_rec.STANDARD_RETAIL   := l_stg_rec.STANDARD_RETAIL;
      END IF;
      IF l_mi_rec.STANDARD_RETAIL_CURRENCY_mi <> 'Y' THEN
        l_temp_rec.STANDARD_RETAIL_CURRENCY   := l_stg_rec.STANDARD_RETAIL_CURRENCY;
      END IF;
      IF l_mi_rec.STANDARD_UOM_mi <> 'Y' THEN
        l_temp_rec.STANDARD_UOM   := l_stg_rec.STANDARD_UOM;
      END IF;
      IF l_mi_rec.SELLING_RETAIL_mi <> 'Y' THEN
        l_temp_rec.SELLING_RETAIL   := l_stg_rec.SELLING_RETAIL;
      END IF;
      IF l_mi_rec.SELLING_UOM_mi <> 'Y' THEN
        l_temp_rec.SELLING_UOM   := l_stg_rec.SELLING_UOM;
      END IF;
      IF l_mi_rec.MULTI_UNITS_mi <> 'Y' THEN
        l_temp_rec.MULTI_UNITS   := l_stg_rec.MULTI_UNITS;
      END IF;
      IF l_mi_rec.MULTI_UNIT_RETAIL_mi <> 'Y' THEN
        l_temp_rec.MULTI_UNIT_RETAIL   := l_stg_rec.MULTI_UNIT_RETAIL;
      END IF;
      IF l_mi_rec.MULTI_UNIT_RETAIL_CURRENCY_mi <> 'Y' THEN
        l_temp_rec.MULTI_UNIT_RETAIL_CURRENCY   := l_stg_rec.MULTI_UNIT_RETAIL_CURRENCY;
      END IF;
      IF l_mi_rec.MULTI_SELLING_UOM_mi <> 'Y' THEN
        l_temp_rec.MULTI_SELLING_UOM   := l_stg_rec.MULTI_SELLING_UOM;
      END IF;
      IF l_mi_rec.LOCK_VERSION_mi <> 'Y' THEN
        l_temp_rec.LOCK_VERSION   := l_stg_rec.LOCK_VERSION;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.ITEM_ZONE_PRICE_ID_mi <> 'Y' THEN
        l_temp_rec.ITEM_ZONE_PRICE_ID   := l_rms_rec.ITEM_ZONE_PRICE_ID;
      END IF;
      IF l_mi_rec.STANDARD_RETAIL_mi <> 'Y' THEN
        l_temp_rec.STANDARD_RETAIL   := l_rms_rec.STANDARD_RETAIL;
      END IF;
      IF l_mi_rec.STANDARD_RETAIL_CURRENCY_mi <> 'Y' THEN
        l_temp_rec.STANDARD_RETAIL_CURRENCY   := l_rms_rec.STANDARD_RETAIL_CURRENCY;
      END IF;
      IF l_mi_rec.STANDARD_UOM_mi <> 'Y' THEN
        l_temp_rec.STANDARD_UOM   := l_rms_rec.STANDARD_UOM;
      END IF;
      IF l_mi_rec.SELLING_RETAIL_mi <> 'Y' THEN
        l_temp_rec.SELLING_RETAIL   := l_rms_rec.SELLING_RETAIL;
      END IF;
      IF l_mi_rec.SELLING_UOM_mi <> 'Y' THEN
        l_temp_rec.SELLING_UOM   := l_rms_rec.SELLING_UOM;
      END IF;
      IF l_mi_rec.MULTI_UNITS_mi <> 'Y' THEN
        l_temp_rec.MULTI_UNITS   := l_rms_rec.MULTI_UNITS;
      END IF;
      IF l_mi_rec.MULTI_UNIT_RETAIL_mi <> 'Y' THEN
        l_temp_rec.MULTI_UNIT_RETAIL   := l_rms_rec.MULTI_UNIT_RETAIL;
      END IF;
      IF l_mi_rec.MULTI_UNIT_RETAIL_CURRENCY_mi <> 'Y' THEN
        l_temp_rec.MULTI_UNIT_RETAIL_CURRENCY   := l_rms_rec.MULTI_UNIT_RETAIL_CURRENCY;
      END IF;
      IF l_mi_rec.MULTI_SELLING_UOM_mi <> 'Y' THEN
        l_temp_rec.MULTI_SELLING_UOM   := l_rms_rec.MULTI_SELLING_UOM;
      END IF;
      IF l_mi_rec.LOCK_VERSION_mi <> 'Y' THEN
        l_temp_rec.LOCK_VERSION   := l_rms_rec.LOCK_VERSION;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,IZP_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_mod AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM_ZONE_PRICE_ID := NVL( l_temp_rec.ITEM_ZONE_PRICE_ID,l_default_rec.ITEM_ZONE_PRICE_ID);
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.ZONE_ID := NVL( l_temp_rec.ZONE_ID,l_default_rec.ZONE_ID);
      l_temp_rec.STANDARD_RETAIL := NVL( l_temp_rec.STANDARD_RETAIL,l_default_rec.STANDARD_RETAIL);
      l_temp_rec.STANDARD_RETAIL_CURRENCY := NVL( l_temp_rec.STANDARD_RETAIL_CURRENCY,l_default_rec.STANDARD_RETAIL_CURRENCY);
      l_temp_rec.STANDARD_UOM := NVL( l_temp_rec.STANDARD_UOM,l_default_rec.STANDARD_UOM);
      l_temp_rec.SELLING_RETAIL := NVL( l_temp_rec.SELLING_RETAIL,l_default_rec.SELLING_RETAIL);
      l_temp_rec.SELLING_UOM := NVL( l_temp_rec.SELLING_UOM,l_default_rec.SELLING_UOM);
      l_temp_rec.MULTI_UNITS := NVL( l_temp_rec.MULTI_UNITS,l_default_rec.MULTI_UNITS);
      l_temp_rec.MULTI_UNIT_RETAIL := NVL( l_temp_rec.MULTI_UNIT_RETAIL,l_default_rec.MULTI_UNIT_RETAIL);
      l_temp_rec.MULTI_UNIT_RETAIL_CURRENCY := NVL( l_temp_rec.MULTI_UNIT_RETAIL_CURRENCY,l_default_rec.MULTI_UNIT_RETAIL_CURRENCY);
      l_temp_rec.MULTI_SELLING_UOM := NVL( l_temp_rec.MULTI_SELLING_UOM,l_default_rec.MULTI_SELLING_UOM);
      l_temp_rec.LOCK_VERSION := NVL( l_temp_rec.LOCK_VERSION,l_default_rec.LOCK_VERSION);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.ZONE_ID IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,IZP_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_RPM_ITEM_ZONE_PRICE VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,IZP_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_RPM_ITEM_ZONE_PRICE
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND ZONE_ID = svc_upd_col(i).ZONE_ID
;
END process_s9t_IZP;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_UID
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_UDA_ITEM_DATE.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_UDA_ITEM_DATE%rowtype;
  l_temp_rec SVC_UDA_ITEM_DATE%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_UDA_ITEM_DATE.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_UDA_ITEM_DATE%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT UDA_DATE_mi,
      UDA_ID_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'UDA_ITEM_DATE'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'UDA_DATE' AS UDA_DATE, 'UDA_ID' AS UDA_ID, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item, uda date and uda id';
  L_table    VARCHAR2(30) := 'SVC_UDA_ITEM_DATE';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT UDA_DATE_dv,
    UDA_ID_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'UDA_ITEM_DATE'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'UDA_DATE' AS UDA_DATE, 'UDA_ID' AS UDA_ID, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.UDA_DATE := rec.UDA_DATE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'UDA_ITEM_DATE',NULL,'UDA_DATE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.UDA_ID := rec.UDA_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'UDA_ITEM_DATE',NULL,'UDA_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'UDA_ITEM_DATE',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(UID_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(UID_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(UID$Action) AS Action,
    r.get_cell(UID$UDA_DATE)     AS UDA_DATE,
    r.get_cell(UID$UDA_ID)   AS UDA_ID,
    r.get_cell(UID$ITEM)     AS ITEM,
    r.get_row_seq()      AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,UID_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.UDA_DATE := rec.UDA_DATE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,UID_sheet,rec.row_seq,'UDA_DATE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.UDA_ID := rec.UDA_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,UID_sheet,rec.row_seq,'UDA_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,UID_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_UDA_ITEM_DATE%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_UDA_ITEM_DATE
       WHERE ITEM     = l_temp_rec.ITEM
         AND UDA_ID = l_temp_rec.UDA_ID
         AND UDA_DATE = l_temp_rec.UDA_DATE
;
    l_rms_rec UDA_ITEM_DATE%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM UDA_ITEM_DATE
       WHERE ITEM     = l_temp_rec.ITEM
         AND UDA_ID = l_temp_rec.UDA_ID
         AND UDA_DATE = l_temp_rec.UDA_DATE
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_UDA_ITEM_DATE
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      --
      IF NOT(l_rms_exists) and l_temp_rec.action = coresvc_item.action_del and l_stg_rec.action = coresvc_item.action_new THEN
         l_stg_rec.action := NULL;
         l_temp_rec.action := NULL;
      END IF;
      --
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new and l_stg_rec.action is not NULL THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,UID_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.UDA_ID := NVL( l_temp_rec.UDA_ID,l_default_rec.UDA_ID);
      l_temp_rec.UDA_DATE := NVL( l_temp_rec.UDA_DATE,l_default_rec.UDA_DATE);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.UDA_ID IS NOT NULL
        AND l_temp_rec.UDA_DATE IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,UID_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_UDA_ITEM_DATE VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,UID_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_UDA_ITEM_DATE
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND UDA_ID = svc_upd_col(i).UDA_ID
     AND UDA_DATE = svc_upd_col(i).UDA_DATE
;
END process_s9t_UID;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_UIF
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_UDA_ITEM_FF.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_UDA_ITEM_FF%rowtype;
  l_temp_rec SVC_UDA_ITEM_FF%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_UDA_ITEM_FF.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_UDA_ITEM_FF%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT UDA_TEXT_mi,
      UDA_ID_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'UDA_ITEM_FF'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'UDA_TEXT' AS UDA_TEXT, 'UDA_ID' AS UDA_ID, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := ' item, uda id and uda text';
  L_table    VARCHAR2(30) := 'SVC_UDA_ITEM_FF';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT UDA_TEXT_dv,
    UDA_ID_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'UDA_ITEM_FF'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'UDA_TEXT' AS UDA_TEXT, 'UDA_ID' AS UDA_ID, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.UDA_TEXT := rec.UDA_TEXT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'UDA_ITEM_FF',NULL,'UDA_TEXT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.UDA_ID := rec.UDA_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'UDA_ITEM_FF',NULL,'UDA_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'UDA_ITEM_FF',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(UIF_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(UIF_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(UIF$Action) AS Action,
    r.get_cell(UIF$UDA_TEXT)     AS UDA_TEXT,
    r.get_cell(UIF$UDA_ID)   AS UDA_ID,
    r.get_cell(UIF$ITEM)     AS ITEM,
    r.get_row_seq()      AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,UIF_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.UDA_TEXT := rec.UDA_TEXT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,UIF_sheet,rec.row_seq,'UDA_TEXT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.UDA_ID := rec.UDA_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,UIF_sheet,rec.row_seq,'UDA_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,UIF_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_UDA_ITEM_FF%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_UDA_ITEM_FF
       WHERE ITEM     = l_temp_rec.ITEM
         AND UDA_ID = l_temp_rec.UDA_ID
         AND replace(UDA_TEXT, chr(32), '')  = replace(l_temp_rec.UDA_TEXT,chr(32), '')
;
    l_rms_rec UDA_ITEM_FF%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM UDA_ITEM_FF
       WHERE ITEM     = l_temp_rec.ITEM
         AND UDA_ID = l_temp_rec.UDA_ID
         AND UDA_TEXT = l_temp_rec.UDA_TEXT
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_UDA_ITEM_FF
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      --
      IF NOT(l_rms_exists) and l_temp_rec.action = coresvc_item.action_del and l_stg_rec.action = coresvc_item.action_new THEN
         l_stg_rec.action := NULL;
         l_temp_rec.action := NULL;
      END IF;
      --
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new and l_stg_rec.action is not NULL THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,UIF_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.UDA_ID := NVL( l_temp_rec.UDA_ID,l_default_rec.UDA_ID);
      l_temp_rec.UDA_TEXT := NVL( l_temp_rec.UDA_TEXT,l_default_rec.UDA_TEXT);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.UDA_ID IS NOT NULL
        AND l_temp_rec.UDA_TEXT IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,UIF_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_UDA_ITEM_FF VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,UIF_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_UDA_ITEM_FF
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND UDA_ID = svc_upd_col(i).UDA_ID
     AND replace(UDA_TEXT, chr(32), '')  = replace(svc_upd_col(i).UDA_TEXT,chr(32), '')
;
END process_s9t_UIF;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_UIL
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_UDA_ITEM_LOV.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_UDA_ITEM_LOV%rowtype;
  l_temp_rec SVC_UDA_ITEM_LOV%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_UDA_ITEM_LOV.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_UDA_ITEM_LOV%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT UDA_VALUE_mi,
      UDA_ID_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'UDA_ITEM_LOV'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'UDA_VALUE' AS UDA_VALUE, 'UDA_ID' AS UDA_ID, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item, uda id and uda value';
  L_table    VARCHAR2(30) := 'SVC_UDA_ITEM_LOV';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT UDA_VALUE_dv,
    UDA_ID_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'UDA_ITEM_LOV'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'UDA_VALUE' AS UDA_VALUE, 'UDA_ID' AS UDA_ID, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.UDA_VALUE := rec.UDA_VALUE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'UDA_ITEM_LOV',NULL,'UDA_VALUE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.UDA_ID := rec.UDA_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'UDA_ITEM_LOV',NULL,'UDA_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'UDA_ITEM_LOV',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(UIL_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(UIL_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(UIL$Action) AS Action,
    r.get_cell(UIL$UDA_VALUE)    AS UDA_VALUE,
    r.get_cell(UIL$UDA_ID)   AS UDA_ID,
    r.get_cell(UIL$ITEM)     AS ITEM,
    r.get_row_seq()      AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,UIL_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.UDA_VALUE := rec.UDA_VALUE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,UIL_sheet,rec.row_seq,'UDA_VALUE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.UDA_ID := rec.UDA_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,UIL_sheet,rec.row_seq,'UDA_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,UIL_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_UDA_ITEM_LOV%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_UDA_ITEM_LOV
       WHERE ITEM     = l_temp_rec.ITEM
         AND UDA_ID = l_temp_rec.UDA_ID
         AND UDA_VALUE = l_temp_rec.UDA_VALUE
;
    l_rms_rec UDA_ITEM_LOV%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM UDA_ITEM_LOV
       WHERE ITEM     = l_temp_rec.ITEM
         AND UDA_ID = l_temp_rec.UDA_ID
         AND UDA_VALUE = l_temp_rec.UDA_VALUE
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_UDA_ITEM_LOV
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      --
      IF NOT(l_rms_exists) and l_temp_rec.action = coresvc_item.action_del and l_stg_rec.action = coresvc_item.action_new THEN
         l_stg_rec.action := NULL;
         l_temp_rec.action := NULL;
      END IF;
      --
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new and l_stg_rec.action is not NULL THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,UIL_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.UDA_ID := NVL( l_temp_rec.UDA_ID,l_default_rec.UDA_ID);
      l_temp_rec.UDA_VALUE := NVL( l_temp_rec.UDA_VALUE,l_default_rec.UDA_VALUE);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.UDA_ID IS NOT NULL
        AND l_temp_rec.UDA_VALUE IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,UIL_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_UDA_ITEM_LOV VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,UIL_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_UDA_ITEM_LOV
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND UDA_ID = svc_upd_col(i).UDA_ID
     AND UDA_VALUE = svc_upd_col(i).UDA_VALUE
;
END process_s9t_UIL;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_VI
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_VAT_ITEM.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_VAT_ITEM%rowtype;
  l_temp_rec SVC_VAT_ITEM%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_VAT_ITEM.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_VAT_ITEM%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT REVERSE_VAT_IND_mi,
      VAT_CODE_mi,
      VAT_TYPE_mi,
      ACTIVE_DATE_mi,
      VAT_REGION_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'VAT_ITEM'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'REVERSE_VAT_IND' AS REVERSE_VAT_IND, 'VAT_CODE' AS VAT_CODE, 'VAT_TYPE' AS VAT_TYPE, 'ACTIVE_DATE' AS ACTIVE_DATE, 'VAT_REGION' AS VAT_REGION, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item, vat region, vat type and active date';
  L_table    VARCHAR2(30) := 'SVC_VAT_ITEM';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT REVERSE_VAT_IND_dv,
    VAT_CODE_dv,
    VAT_TYPE_dv,
    ACTIVE_DATE_dv,
    VAT_REGION_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'VAT_ITEM'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'REVERSE_VAT_IND' AS REVERSE_VAT_IND, 'VAT_CODE' AS VAT_CODE, 'VAT_TYPE' AS VAT_TYPE, 'ACTIVE_DATE' AS ACTIVE_DATE, 'VAT_REGION' AS VAT_REGION, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.REVERSE_VAT_IND := rec.REVERSE_VAT_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'VAT_ITEM',NULL,'REVERSE_VAT_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.VAT_CODE := rec.VAT_CODE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'VAT_ITEM',NULL,'VAT_CODE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.VAT_TYPE := rec.VAT_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'VAT_ITEM',NULL,'VAT_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ACTIVE_DATE := rec.ACTIVE_DATE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'VAT_ITEM',NULL,'ACTIVE_DATE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.VAT_REGION := rec.VAT_REGION_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'VAT_ITEM',NULL,'VAT_REGION','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'VAT_ITEM',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(VI_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(VI_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(VI$Action) AS Action,
    r.get_cell(VI$REVERSE_VAT_IND)  AS REVERSE_VAT_IND,
    r.get_cell(VI$VAT_CODE)     AS VAT_CODE,
    r.get_cell(VI$VAT_TYPE)     AS VAT_TYPE,
    r.get_cell(VI$ACTIVE_DATE)      AS ACTIVE_DATE,
    r.get_cell(VI$VAT_REGION)       AS VAT_REGION,
    r.get_cell(VI$ITEM)         AS ITEM,
    r.get_row_seq()         AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,VI_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.REVERSE_VAT_IND := rec.REVERSE_VAT_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,VI_sheet,rec.row_seq,'REVERSE_VAT_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.VAT_CODE := rec.VAT_CODE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,VI_sheet,rec.row_seq,'VAT_CODE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.VAT_TYPE := rec.VAT_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,VI_sheet,rec.row_seq,'VAT_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ACTIVE_DATE := rec.ACTIVE_DATE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,VI_sheet,rec.row_seq,'ACTIVE_DATE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.VAT_REGION := rec.VAT_REGION;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,VI_sheet,rec.row_seq,'VAT_REGION',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,VI_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_VAT_ITEM%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_VAT_ITEM
       WHERE VAT_TYPE     = l_temp_rec.VAT_TYPE
         AND ACTIVE_DATE = l_temp_rec.ACTIVE_DATE
         AND VAT_REGION = l_temp_rec.VAT_REGION
         AND ITEM = l_temp_rec.ITEM
;
    l_rms_rec VAT_ITEM%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM VAT_ITEM
       WHERE VAT_TYPE     = l_temp_rec.VAT_TYPE
         AND ACTIVE_DATE = l_temp_rec.ACTIVE_DATE
         AND VAT_REGION = l_temp_rec.VAT_REGION
         AND ITEM = l_temp_rec.ITEM
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_VAT_ITEM
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.VAT_CODE_mi <> 'Y' THEN
        l_temp_rec.VAT_CODE   := l_stg_rec.VAT_CODE;
      END IF;
      IF l_mi_rec.REVERSE_VAT_IND_mi <> 'Y' THEN
        l_temp_rec.REVERSE_VAT_IND   := l_stg_rec.REVERSE_VAT_IND;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.VAT_CODE_mi <> 'Y' THEN
        l_temp_rec.VAT_CODE   := l_rms_rec.VAT_CODE;
      END IF;
      IF l_mi_rec.REVERSE_VAT_IND_mi <> 'Y' THEN
        l_temp_rec.REVERSE_VAT_IND   := l_rms_rec.REVERSE_VAT_IND;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,VI_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.VAT_REGION := NVL( l_temp_rec.VAT_REGION,l_default_rec.VAT_REGION);
      l_temp_rec.ACTIVE_DATE := NVL( l_temp_rec.ACTIVE_DATE,l_default_rec.ACTIVE_DATE);
      l_temp_rec.VAT_TYPE := NVL( l_temp_rec.VAT_TYPE,l_default_rec.VAT_TYPE);
      l_temp_rec.VAT_CODE := NVL( l_temp_rec.VAT_CODE,l_default_rec.VAT_CODE);
      l_temp_rec.VAT_RATE := NVL( l_temp_rec.VAT_RATE,l_default_rec.VAT_RATE);
      l_temp_rec.REVERSE_VAT_IND := NVL( l_temp_rec.REVERSE_VAT_IND,l_default_rec.REVERSE_VAT_IND);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.VAT_TYPE IS NOT NULL
        AND l_temp_rec.ACTIVE_DATE IS NOT NULL
        AND l_temp_rec.VAT_REGION IS NOT NULL
        AND l_temp_rec.ITEM IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,VI_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_VAT_ITEM VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,VI_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_VAT_ITEM
     SET row         = svc_upd_col(i)
   WHERE VAT_TYPE     = svc_upd_col(i).VAT_TYPE
     AND ACTIVE_DATE = svc_upd_col(i).ACTIVE_DATE
     AND VAT_REGION = svc_upd_col(i).VAT_REGION
     AND ITEM = svc_upd_col(i).ITEM
;
END process_s9t_VI;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_CSH
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_COST_SUSP_SUP_HEAD.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_COST_SUSP_SUP_HEAD%rowtype;
  l_temp_rec SVC_COST_SUSP_SUP_HEAD%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_COST_SUSP_SUP_HEAD.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_COST_SUSP_SUP_HEAD%rowtype;
  L_sheet_name S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT APPROVAL_ID_mi,
      APPROVAL_DATE_mi,
      COST_CHANGE_ORIGIN_mi,
      STATUS_mi,
      ACTIVE_DATE_mi,
      REASON_mi,
      COST_CHANGE_DESC_mi,
      COST_CHANGE_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'COST_SUSP_SUP_HEAD'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'APPROVAL_ID' AS APPROVAL_ID, 'APPROVAL_DATE' AS APPROVAL_DATE, 'COST_CHANGE_ORIGIN' AS COST_CHANGE_ORIGIN, 'STATUS' AS STATUS, 'ACTIVE_DATE' AS ACTIVE_DATE, 'REASON' AS REASON, 'COST_CHANGE_DESC' AS COST_CHANGE_DESC, 'COST_CHANGE' AS COST_CHANGE, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'cost change';
  L_table    VARCHAR2(30) := 'SVC_COST_SUSP_SUP_HEAD';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT APPROVAL_ID_dv,
    APPROVAL_DATE_dv,
    COST_CHANGE_ORIGIN_dv,
    STATUS_dv,
    ACTIVE_DATE_dv,
    REASON_dv,
    COST_CHANGE_DESC_dv,
    COST_CHANGE_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'COST_SUSP_SUP_HEAD'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'APPROVAL_ID' AS APPROVAL_ID, 'APPROVAL_DATE' AS APPROVAL_DATE, 'COST_CHANGE_ORIGIN' AS COST_CHANGE_ORIGIN, 'STATUS' AS STATUS, 'ACTIVE_DATE' AS ACTIVE_DATE, 'REASON' AS REASON, 'COST_CHANGE_DESC' AS COST_CHANGE_DESC, 'COST_CHANGE' AS COST_CHANGE, NULL AS dummy))
  )
  LOOP
    --
    BEGIN
      l_default_rec.APPROVAL_ID := rec.APPROVAL_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_HEAD',NULL,'APPROVAL_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.APPROVAL_DATE := rec.APPROVAL_DATE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_HEAD',NULL,'APPROVAL_DATE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COST_CHANGE_ORIGIN := rec.COST_CHANGE_ORIGIN_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_HEAD',NULL,'COST_CHANGE_ORIGIN','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.STATUS := rec.STATUS_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_HEAD',NULL,'STATUS','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ACTIVE_DATE := rec.ACTIVE_DATE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_HEAD',NULL,'ACTIVE_DATE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.REASON := rec.REASON_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_HEAD',NULL,'REASON','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COST_CHANGE_DESC := rec.COST_CHANGE_DESC_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_HEAD',NULL,'COST_CHANGE_DESC','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COST_CHANGE := rec.COST_CHANGE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_HEAD',NULL,'COST_CHANGE','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(CSH_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(CSH_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(CSH$Action)       AS Action,
    r.get_cell(CSH$APPROVAL_ID)        AS APPROVAL_ID,
    r.get_cell(CSH$APPROVAL_DATE)      AS APPROVAL_DATE,
    r.get_cell(CSH$COST_CHANGE_ORIGIN) AS COST_CHANGE_ORIGIN,
    r.get_cell(CSH$STATUS)         AS STATUS,
    r.get_cell(CSH$ACTIVE_DATE)        AS ACTIVE_DATE,
    r.get_cell(CSH$REASON)         AS REASON,
    r.get_cell(CSH$COST_CHANGE_DESC)   AS COST_CHANGE_DESC,
    r.get_cell(CSH$COST_CHANGE)        AS COST_CHANGE,
    r.get_row_seq()            AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    --
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSH_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.APPROVAL_ID := rec.APPROVAL_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSH_sheet,rec.row_seq,'APPROVAL_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.APPROVAL_DATE := rec.APPROVAL_DATE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSH_sheet,rec.row_seq,'APPROVAL_DATE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COST_CHANGE_ORIGIN := rec.COST_CHANGE_ORIGIN;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSH_sheet,rec.row_seq,'COST_CHANGE_ORIGIN',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.STATUS := rec.STATUS;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSH_sheet,rec.row_seq,'STATUS',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ACTIVE_DATE := rec.ACTIVE_DATE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSH_sheet,rec.row_seq,'ACTIVE_DATE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.REASON := rec.REASON;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSH_sheet,rec.row_seq,'REASON',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COST_CHANGE_DESC := rec.COST_CHANGE_DESC;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSH_sheet,rec.row_seq,'COST_CHANGE_DESC',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COST_CHANGE := rec.COST_CHANGE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSH_sheet,rec.row_seq,'COST_CHANGE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_COST_SUSP_SUP_HEAD%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_COST_SUSP_SUP_HEAD
       WHERE COST_CHANGE     = l_temp_rec.COST_CHANGE
;
    l_rms_rec COST_SUSP_SUP_HEAD%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM COST_SUSP_SUP_HEAD
       WHERE COST_CHANGE     = l_temp_rec.COST_CHANGE
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_COST_SUSP_SUP_HEAD
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.COST_CHANGE_DESC_mi <> 'Y' THEN
        l_temp_rec.COST_CHANGE_DESC   := l_stg_rec.COST_CHANGE_DESC;
      END IF;
      IF l_mi_rec.REASON_mi <> 'Y' THEN
        l_temp_rec.REASON   := l_stg_rec.REASON;
      END IF;
      IF l_mi_rec.ACTIVE_DATE_mi <> 'Y' THEN
        l_temp_rec.ACTIVE_DATE   := l_stg_rec.ACTIVE_DATE;
      END IF;
      IF l_mi_rec.STATUS_mi <> 'Y' THEN
        l_temp_rec.STATUS   := l_stg_rec.STATUS;
      END IF;
      IF l_mi_rec.COST_CHANGE_ORIGIN_mi <> 'Y' THEN
        l_temp_rec.COST_CHANGE_ORIGIN   := l_stg_rec.COST_CHANGE_ORIGIN;
      END IF;
      IF l_mi_rec.APPROVAL_DATE_mi <> 'Y' THEN
        l_temp_rec.APPROVAL_DATE   := l_stg_rec.APPROVAL_DATE;
      END IF;
      IF l_mi_rec.APPROVAL_ID_mi <> 'Y' THEN
        l_temp_rec.APPROVAL_ID   := l_stg_rec.APPROVAL_ID;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.COST_CHANGE_DESC_mi <> 'Y' THEN
        l_temp_rec.COST_CHANGE_DESC   := l_rms_rec.COST_CHANGE_DESC;
      END IF;
      IF l_mi_rec.REASON_mi <> 'Y' THEN
        l_temp_rec.REASON   := l_rms_rec.REASON;
      END IF;
      IF l_mi_rec.ACTIVE_DATE_mi <> 'Y' THEN
        l_temp_rec.ACTIVE_DATE   := l_rms_rec.ACTIVE_DATE;
      END IF;
      IF l_mi_rec.STATUS_mi <> 'Y' THEN
        l_temp_rec.STATUS   := l_rms_rec.STATUS;
      END IF;
      IF l_mi_rec.COST_CHANGE_ORIGIN_mi <> 'Y' THEN
        l_temp_rec.COST_CHANGE_ORIGIN   := l_rms_rec.COST_CHANGE_ORIGIN;
      END IF;
      IF l_mi_rec.APPROVAL_DATE_mi <> 'Y' THEN
        l_temp_rec.APPROVAL_DATE   := l_rms_rec.APPROVAL_DATE;
      END IF;
      IF l_mi_rec.APPROVAL_ID_mi <> 'Y' THEN
        l_temp_rec.APPROVAL_ID   := l_rms_rec.APPROVAL_ID;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,CSH_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.COST_CHANGE := NVL( l_temp_rec.COST_CHANGE,l_default_rec.COST_CHANGE);
      l_temp_rec.COST_CHANGE_DESC := NVL( l_temp_rec.COST_CHANGE_DESC,l_default_rec.COST_CHANGE_DESC);
      l_temp_rec.REASON := NVL( l_temp_rec.REASON,l_default_rec.REASON);
      l_temp_rec.ACTIVE_DATE := NVL( l_temp_rec.ACTIVE_DATE,l_default_rec.ACTIVE_DATE);
      l_temp_rec.STATUS := NVL( l_temp_rec.STATUS,l_default_rec.STATUS);
      l_temp_rec.COST_CHANGE_ORIGIN := NVL( l_temp_rec.COST_CHANGE_ORIGIN,l_default_rec.COST_CHANGE_ORIGIN);
      l_temp_rec.APPROVAL_DATE := NVL( l_temp_rec.APPROVAL_DATE,l_default_rec.APPROVAL_DATE);
      l_temp_rec.APPROVAL_ID := NVL( l_temp_rec.APPROVAL_ID,l_default_rec.APPROVAL_ID);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.COST_CHANGE IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,CSH_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_COST_SUSP_SUP_HEAD VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,CSH_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_COST_SUSP_SUP_HEAD
     SET row         = svc_upd_col(i)
   WHERE COST_CHANGE     = svc_upd_col(i).COST_CHANGE
;
END process_s9t_CSH;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_CSDL
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_COST_SUSP_SUP_DETAIL_LOC.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_COST_SUSP_SUP_DETAIL_LOC%rowtype;
  l_temp_rec SVC_COST_SUSP_SUP_DETAIL_LOC%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_COST_SUSP_SUP_DETAIL_LOC.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_COST_SUSP_SUP_DETAIL_LOC%rowtype;
  L_sheet_name  S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT BRACKET_VALUE1_mi,
      LOC_mi,
      LOC_TYPE_mi,
      ITEM_mi,
      ORIGIN_COUNTRY_ID_mi,
      SUPPLIER_mi,
      COST_CHANGE_mi,
      DELIVERY_COUNTRY_ID_mi,
      SUP_DEPT_SEQ_NO_mi,
      DEPT_mi,
      DEFAULT_BRACKET_IND_mi,
      RECALC_ORD_IND_mi,
      COST_CHANGE_VALUE_mi,
      COST_CHANGE_TYPE_mi,
      UNIT_COST_mi,
      BRACKET_VALUE2_mi,
      BRACKET_UOM1_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'COST_SUSP_SUP_DETAIL_LOC'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'BRACKET_VALUE1' AS BRACKET_VALUE1, 'LOC' AS LOC, 'LOC_TYPE' AS LOC_TYPE, 'ITEM' AS ITEM, 'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'COST_CHANGE' AS COST_CHANGE, 'DELIVERY_COUNTRY_ID' AS DELIVERY_COUNTRY_ID, 'SUP_DEPT_SEQ_NO' AS SUP_DEPT_SEQ_NO, 'DEPT' AS DEPT, 'DEFAULT_BRACKET_IND' AS DEFAULT_BRACKET_IND, 'RECALC_ORD_IND' AS RECALC_ORD_IND, 'COST_CHANGE_VALUE' AS COST_CHANGE_VALUE, 'COST_CHANGE_TYPE' AS COST_CHANGE_TYPE, 'UNIT_COST' AS UNIT_COST, 'BRACKET_VALUE2' AS BRACKET_VALUE2, 'BRACKET_UOM1' AS BRACKET_UOM1, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'cost change, supplier, origin country id, item and loc';
  L_table    VARCHAR2(30) := 'SVC_COST_SUSP_SUP_DETAIL_LOC';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT BRACKET_VALUE1_dv,
    LOC_dv,
    LOC_TYPE_dv,
    ITEM_dv,
    ORIGIN_COUNTRY_ID_dv,
    SUPPLIER_dv,
    COST_CHANGE_dv,
    DELIVERY_COUNTRY_ID_dv,
    SUP_DEPT_SEQ_NO_dv,
    DEPT_dv,
    DEFAULT_BRACKET_IND_dv,
    RECALC_ORD_IND_dv,
    COST_CHANGE_VALUE_dv,
    COST_CHANGE_TYPE_dv,
    UNIT_COST_dv,
    BRACKET_VALUE2_dv,
    BRACKET_UOM1_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'COST_SUSP_SUP_DETAIL_LOC'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'BRACKET_VALUE1' AS BRACKET_VALUE1, 'LOC' AS LOC, 'LOC_TYPE' AS LOC_TYPE, 'ITEM' AS ITEM, 'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'COST_CHANGE' AS COST_CHANGE, 'DELIVERY_COUNTRY_ID' AS DELIVERY_COUNTRY_ID, 'SUP_DEPT_SEQ_NO' AS SUP_DEPT_SEQ_NO, 'DEPT' AS DEPT, 'DEFAULT_BRACKET_IND' AS DEFAULT_BRACKET_IND, 'RECALC_ORD_IND' AS RECALC_ORD_IND, 'COST_CHANGE_VALUE' AS COST_CHANGE_VALUE, 'COST_CHANGE_TYPE' AS COST_CHANGE_TYPE, 'UNIT_COST' AS UNIT_COST, 'BRACKET_VALUE2' AS BRACKET_VALUE2, 'BRACKET_UOM1' AS BRACKET_UOM1, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.BRACKET_VALUE1 := rec.BRACKET_VALUE1_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'BRACKET_VALUE1','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.LOC := rec.LOC_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'LOC','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.LOC_TYPE := rec.LOC_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'LOC_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'ORIGIN_COUNTRY_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPPLIER := rec.SUPPLIER_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'SUPPLIER','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COST_CHANGE := rec.COST_CHANGE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'COST_CHANGE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.DELIVERY_COUNTRY_ID := rec.DELIVERY_COUNTRY_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'DELIVERY_COUNTRY_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUP_DEPT_SEQ_NO := rec.SUP_DEPT_SEQ_NO_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'SUP_DEPT_SEQ_NO','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.DEPT := rec.DEPT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'DEPT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.DEFAULT_BRACKET_IND := rec.DEFAULT_BRACKET_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'DEFAULT_BRACKET_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.RECALC_ORD_IND := rec.RECALC_ORD_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'RECALC_ORD_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COST_CHANGE_VALUE := rec.COST_CHANGE_VALUE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'COST_CHANGE_VALUE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COST_CHANGE_TYPE := rec.COST_CHANGE_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'COST_CHANGE_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.UNIT_COST := rec.UNIT_COST_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'UNIT_COST','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.BRACKET_VALUE2 := rec.BRACKET_VALUE2_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'BRACKET_VALUE2','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.BRACKET_UOM1 := rec.BRACKET_UOM1_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL_LOC',NULL,'BRACKET_UOM1','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(CSDL_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(CSDL_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(CSDL$Action)    AS Action,
    r.get_cell(CSDL$BRACKET_VALUE1)  AS BRACKET_VALUE1,
    r.get_cell(CSDL$LOC)         AS LOC,
    r.get_cell(CSDL$LOC_TYPE)        AS LOC_TYPE,
    r.get_cell(CSDL$ITEM)        AS ITEM,
    r.get_cell(CSDL$ORIGIN_COUNTRY_ID)   AS ORIGIN_COUNTRY_ID,
    r.get_cell(CSDL$SUPPLIER)        AS SUPPLIER,
    r.get_cell(CSDL$COST_CHANGE)     AS COST_CHANGE,
    r.get_cell(CSDL$DELIVERY_COUNTRY_ID) AS DELIVERY_COUNTRY_ID,
    r.get_cell(CSDL$SUP_DEPT_SEQ_NO)     AS SUP_DEPT_SEQ_NO,
    r.get_cell(CSDL$DEPT)        AS DEPT,
    r.get_cell(CSDL$DEFAULT_BRACKET_IND) AS DEFAULT_BRACKET_IND,
    r.get_cell(CSDL$RECALC_ORD_IND)  AS RECALC_ORD_IND,
    r.get_cell(CSDL$COST_CHANGE_VALUE)   AS COST_CHANGE_VALUE,
    r.get_cell(CSDL$COST_CHANGE_TYPE)    AS COST_CHANGE_TYPE,
    r.get_cell(CSDL$UNIT_COST)       AS UNIT_COST,
    r.get_cell(CSDL$BRACKET_VALUE2)  AS BRACKET_VALUE2,
    r.get_cell(CSDL$BRACKET_UOM1)    AS BRACKET_UOM1,
    r.get_row_seq()          AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    --
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.BRACKET_VALUE1 := rec.BRACKET_VALUE1;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'BRACKET_VALUE1',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.LOC := rec.LOC;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'LOC',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.LOC_TYPE := rec.LOC_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'LOC_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'ORIGIN_COUNTRY_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPPLIER := rec.SUPPLIER;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'SUPPLIER',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COST_CHANGE := rec.COST_CHANGE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'COST_CHANGE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DELIVERY_COUNTRY_ID := rec.DELIVERY_COUNTRY_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'DELIVERY_COUNTRY_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUP_DEPT_SEQ_NO := rec.SUP_DEPT_SEQ_NO;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'SUP_DEPT_SEQ_NO',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DEPT := rec.DEPT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'DEPT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DEFAULT_BRACKET_IND := rec.DEFAULT_BRACKET_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'DEFAULT_BRACKET_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.RECALC_ORD_IND := rec.RECALC_ORD_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'RECALC_ORD_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COST_CHANGE_VALUE := rec.COST_CHANGE_VALUE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'COST_CHANGE_VALUE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COST_CHANGE_TYPE := rec.COST_CHANGE_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'COST_CHANGE_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.UNIT_COST := rec.UNIT_COST;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'UNIT_COST',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.BRACKET_VALUE2 := rec.BRACKET_VALUE2;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'BRACKET_VALUE2',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.BRACKET_UOM1 := rec.BRACKET_UOM1;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,'BRACKET_UOM1',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_COST_SUSP_SUP_DETAIL_LOC%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_COST_SUSP_SUP_DETAIL_LOC
       WHERE COST_CHANGE     = l_temp_rec.COST_CHANGE
         AND ITEM = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND ORIGIN_COUNTRY_ID = l_temp_rec.ORIGIN_COUNTRY_ID
         AND LOC = l_temp_rec.LOC
;
    l_rms_rec COST_SUSP_SUP_DETAIL_LOC%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM COST_SUSP_SUP_DETAIL_LOC
       WHERE COST_CHANGE     = l_temp_rec.COST_CHANGE
         AND ITEM = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND ORIGIN_COUNTRY_ID = l_temp_rec.ORIGIN_COUNTRY_ID
         AND LOC = l_temp_rec.LOC
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_COST_SUSP_SUP_DETAIL_LOC
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.BRACKET_VALUE1_mi <> 'Y' THEN
        l_temp_rec.BRACKET_VALUE1   := l_stg_rec.BRACKET_VALUE1;
      END IF;
      IF l_mi_rec.BRACKET_UOM1_mi <> 'Y' THEN
        l_temp_rec.BRACKET_UOM1   := l_stg_rec.BRACKET_UOM1;
      END IF;
      IF l_mi_rec.BRACKET_VALUE2_mi <> 'Y' THEN
        l_temp_rec.BRACKET_VALUE2   := l_stg_rec.BRACKET_VALUE2;
      END IF;
      IF l_mi_rec.UNIT_COST_mi <> 'Y' THEN
        l_temp_rec.UNIT_COST   := l_stg_rec.UNIT_COST;
      END IF;
      IF l_mi_rec.COST_CHANGE_TYPE_mi <> 'Y' THEN
        l_temp_rec.COST_CHANGE_TYPE   := l_stg_rec.COST_CHANGE_TYPE;
      END IF;
      IF l_mi_rec.COST_CHANGE_VALUE_mi <> 'Y' THEN
        l_temp_rec.COST_CHANGE_VALUE   := l_stg_rec.COST_CHANGE_VALUE;
      END IF;
      IF l_mi_rec.RECALC_ORD_IND_mi <> 'Y' THEN
        l_temp_rec.RECALC_ORD_IND   := l_stg_rec.RECALC_ORD_IND;
      END IF;
      IF l_mi_rec.DEFAULT_BRACKET_IND_mi <> 'Y' THEN
        l_temp_rec.DEFAULT_BRACKET_IND   := l_stg_rec.DEFAULT_BRACKET_IND;
      END IF;
      IF l_mi_rec.DEPT_mi <> 'Y' THEN
        l_temp_rec.DEPT   := l_stg_rec.DEPT;
      END IF;
      IF l_mi_rec.SUP_DEPT_SEQ_NO_mi <> 'Y' THEN
        l_temp_rec.SUP_DEPT_SEQ_NO   := l_stg_rec.SUP_DEPT_SEQ_NO;
      END IF;
      IF l_mi_rec.DELIVERY_COUNTRY_ID_mi <> 'Y' THEN
        l_temp_rec.DELIVERY_COUNTRY_ID   := l_stg_rec.DELIVERY_COUNTRY_ID;
      END IF;
      IF l_mi_rec.LOC_TYPE_mi <> 'Y' THEN
        l_temp_rec.LOC_TYPE   := l_stg_rec.LOC_TYPE;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.BRACKET_UOM1_mi <> 'Y' THEN
        l_temp_rec.BRACKET_UOM1   := l_rms_rec.BRACKET_UOM1;
      END IF;
      IF l_mi_rec.BRACKET_VALUE2_mi <> 'Y' THEN
        l_temp_rec.BRACKET_VALUE2   := l_rms_rec.BRACKET_VALUE2;
      END IF;
      IF l_mi_rec.UNIT_COST_mi <> 'Y' THEN
        l_temp_rec.UNIT_COST   := l_rms_rec.UNIT_COST;
      END IF;
      IF l_mi_rec.COST_CHANGE_TYPE_mi <> 'Y' THEN
        l_temp_rec.COST_CHANGE_TYPE   := l_rms_rec.COST_CHANGE_TYPE;
      END IF;
      IF l_mi_rec.COST_CHANGE_VALUE_mi <> 'Y' THEN
        l_temp_rec.COST_CHANGE_VALUE   := l_rms_rec.COST_CHANGE_VALUE;
      END IF;
      IF l_mi_rec.RECALC_ORD_IND_mi <> 'Y' THEN
        l_temp_rec.RECALC_ORD_IND   := l_rms_rec.RECALC_ORD_IND;
      END IF;
      IF l_mi_rec.DEFAULT_BRACKET_IND_mi <> 'Y' THEN
        l_temp_rec.DEFAULT_BRACKET_IND   := l_rms_rec.DEFAULT_BRACKET_IND;
      END IF;
      IF l_mi_rec.DEPT_mi <> 'Y' THEN
        l_temp_rec.DEPT   := l_rms_rec.DEPT;
      END IF;
      IF l_mi_rec.SUP_DEPT_SEQ_NO_mi <> 'Y' THEN
        l_temp_rec.SUP_DEPT_SEQ_NO   := l_rms_rec.SUP_DEPT_SEQ_NO;
      END IF;
      IF l_mi_rec.DELIVERY_COUNTRY_ID_mi <> 'Y' THEN
        l_temp_rec.DELIVERY_COUNTRY_ID   := l_rms_rec.DELIVERY_COUNTRY_ID;
      END IF;
      IF l_mi_rec.LOC_TYPE_mi <> 'Y' THEN
        l_temp_rec.LOC_TYPE   := l_rms_rec.LOC_TYPE;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,CSDL_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.BRACKET_UOM1 := NVL( l_temp_rec.BRACKET_UOM1,l_default_rec.BRACKET_UOM1);
      l_temp_rec.BRACKET_VALUE2 := NVL( l_temp_rec.BRACKET_VALUE2,l_default_rec.BRACKET_VALUE2);
      l_temp_rec.UNIT_COST := NVL( l_temp_rec.UNIT_COST,l_default_rec.UNIT_COST);
      l_temp_rec.COST_CHANGE_TYPE := NVL( l_temp_rec.COST_CHANGE_TYPE,l_default_rec.COST_CHANGE_TYPE);
      l_temp_rec.COST_CHANGE_VALUE := NVL( l_temp_rec.COST_CHANGE_VALUE,l_default_rec.COST_CHANGE_VALUE);
      l_temp_rec.RECALC_ORD_IND := NVL( l_temp_rec.RECALC_ORD_IND,l_default_rec.RECALC_ORD_IND);
      l_temp_rec.DEFAULT_BRACKET_IND := NVL( l_temp_rec.DEFAULT_BRACKET_IND,l_default_rec.DEFAULT_BRACKET_IND);
      l_temp_rec.DEPT := NVL( l_temp_rec.DEPT,l_default_rec.DEPT);
      l_temp_rec.SUP_DEPT_SEQ_NO := NVL( l_temp_rec.SUP_DEPT_SEQ_NO,l_default_rec.SUP_DEPT_SEQ_NO);
      l_temp_rec.DELIVERY_COUNTRY_ID := NVL( l_temp_rec.DELIVERY_COUNTRY_ID,l_default_rec.DELIVERY_COUNTRY_ID);
      l_temp_rec.COST_CHANGE := NVL( l_temp_rec.COST_CHANGE,l_default_rec.COST_CHANGE);
      l_temp_rec.SUPPLIER := NVL( l_temp_rec.SUPPLIER,l_default_rec.SUPPLIER);
      l_temp_rec.ORIGIN_COUNTRY_ID := NVL( l_temp_rec.ORIGIN_COUNTRY_ID,l_default_rec.ORIGIN_COUNTRY_ID);
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.LOC_TYPE := NVL( l_temp_rec.LOC_TYPE,l_default_rec.LOC_TYPE);
      l_temp_rec.LOC := NVL( l_temp_rec.LOC,l_default_rec.LOC);
      l_temp_rec.BRACKET_VALUE1 := NVL( l_temp_rec.BRACKET_VALUE1,l_default_rec.BRACKET_VALUE1);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.COST_CHANGE IS NOT NULL
        AND l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.SUPPLIER IS NOT NULL
        AND l_temp_rec.ORIGIN_COUNTRY_ID IS NOT NULL
        AND l_temp_rec.LOC IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,CSDL_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_COST_SUSP_SUP_DETAIL_LOC VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,CSDL_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_COST_SUSP_SUP_DETAIL_LOC
     SET row         = svc_upd_col(i)
   WHERE COST_CHANGE     = svc_upd_col(i).COST_CHANGE
     AND ITEM = svc_upd_col(i).ITEM
     AND SUPPLIER = svc_upd_col(i).SUPPLIER
     AND ORIGIN_COUNTRY_ID = svc_upd_col(i).ORIGIN_COUNTRY_ID
     AND LOC = svc_upd_col(i).LOC
     AND NVL(BRACKET_VALUE1,'-1') = NVL(svc_upd_col(i).BRACKET_VALUE1,'-1');
END process_s9t_CSDL;
-------------------------------------------------------------------------------------------------------
PROCEDURE process_s9t_CSD
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_COST_SUSP_SUP_DETAIL.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_COST_SUSP_SUP_DETAIL%rowtype;
  l_temp_rec SVC_COST_SUSP_SUP_DETAIL%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_COST_SUSP_SUP_DETAIL.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_COST_SUSP_SUP_DETAIL%rowtype;
  L_sheet_name  S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT DELIVERY_COUNTRY_ID_mi,
      SUP_DEPT_SEQ_NO_mi,
      DEPT_mi,
      DEFAULT_BRACKET_IND_mi,
      RECALC_ORD_IND_mi,
      COST_CHANGE_VALUE_mi,
      COST_CHANGE_TYPE_mi,
      UNIT_COST_mi,
      BRACKET_VALUE2_mi,
      BRACKET_UOM1_mi,
      BRACKET_VALUE1_mi,
      ITEM_mi,
      ORIGIN_COUNTRY_ID_mi,
      SUPPLIER_mi,
      COST_CHANGE_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'COST_SUSP_SUP_DETAIL'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'DELIVERY_COUNTRY_ID' AS DELIVERY_COUNTRY_ID, 'SUP_DEPT_SEQ_NO' AS SUP_DEPT_SEQ_NO, 'DEPT' AS DEPT, 'DEFAULT_BRACKET_IND' AS DEFAULT_BRACKET_IND, 'RECALC_ORD_IND' AS RECALC_ORD_IND, 'COST_CHANGE_VALUE' AS COST_CHANGE_VALUE, 'COST_CHANGE_TYPE' AS COST_CHANGE_TYPE, 'UNIT_COST' AS UNIT_COST, 'BRACKET_VALUE2' AS BRACKET_VALUE2, 'BRACKET_UOM1' AS BRACKET_UOM1, 'BRACKET_VALUE1' AS BRACKET_VALUE1, 'ITEM' AS ITEM, 'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'COST_CHANGE' AS COST_CHANGE, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item, cost change, supplier and origin country id';
  L_table    VARCHAR2(30) := 'SVC_COST_SUSP_SUP_DETAIL';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT DELIVERY_COUNTRY_ID_dv,
    SUP_DEPT_SEQ_NO_dv,
    DEPT_dv,
    DEFAULT_BRACKET_IND_dv,
    RECALC_ORD_IND_dv,
    COST_CHANGE_VALUE_dv,
    COST_CHANGE_TYPE_dv,
    UNIT_COST_dv,
    BRACKET_VALUE2_dv,
    BRACKET_UOM1_dv,
    BRACKET_VALUE1_dv,
    ITEM_dv,
    ORIGIN_COUNTRY_ID_dv,
    SUPPLIER_dv,
    COST_CHANGE_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'COST_SUSP_SUP_DETAIL'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'DELIVERY_COUNTRY_ID' AS DELIVERY_COUNTRY_ID, 'SUP_DEPT_SEQ_NO' AS SUP_DEPT_SEQ_NO, 'DEPT' AS DEPT, 'DEFAULT_BRACKET_IND' AS DEFAULT_BRACKET_IND, 'RECALC_ORD_IND' AS RECALC_ORD_IND, 'COST_CHANGE_VALUE' AS COST_CHANGE_VALUE, 'COST_CHANGE_TYPE' AS COST_CHANGE_TYPE, 'UNIT_COST' AS UNIT_COST, 'BRACKET_VALUE2' AS BRACKET_VALUE2, 'BRACKET_UOM1' AS BRACKET_UOM1, 'BRACKET_VALUE1' AS BRACKET_VALUE1, 'ITEM' AS ITEM, 'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID, 'SUPPLIER' AS SUPPLIER, 'COST_CHANGE' AS COST_CHANGE, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.DELIVERY_COUNTRY_ID := rec.DELIVERY_COUNTRY_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'DELIVERY_COUNTRY_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUP_DEPT_SEQ_NO := rec.SUP_DEPT_SEQ_NO_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'SUP_DEPT_SEQ_NO','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.DEPT := rec.DEPT_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'DEPT','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.DEFAULT_BRACKET_IND := rec.DEFAULT_BRACKET_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'DEFAULT_BRACKET_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.RECALC_ORD_IND := rec.RECALC_ORD_IND_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'RECALC_ORD_IND','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COST_CHANGE_VALUE := rec.COST_CHANGE_VALUE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'COST_CHANGE_VALUE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COST_CHANGE_TYPE := rec.COST_CHANGE_TYPE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'COST_CHANGE_TYPE','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.UNIT_COST := rec.UNIT_COST_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'UNIT_COST','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.BRACKET_VALUE2 := rec.BRACKET_VALUE2_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'BRACKET_VALUE2','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.BRACKET_UOM1 := rec.BRACKET_UOM1_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'BRACKET_UOM1','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.BRACKET_VALUE1 := rec.BRACKET_VALUE1_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'BRACKET_VALUE1','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'ORIGIN_COUNTRY_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SUPPLIER := rec.SUPPLIER_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'SUPPLIER','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.COST_CHANGE := rec.COST_CHANGE_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'COST_SUSP_SUP_DETAIL',NULL,'COST_CHANGE','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(CSD_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(CSD_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(CSD$Action)    AS Action,
    r.get_cell(CSD$DELIVERY_COUNTRY_ID) AS DELIVERY_COUNTRY_ID,
    r.get_cell(CSD$SUP_DEPT_SEQ_NO) AS SUP_DEPT_SEQ_NO,
    r.get_cell(CSD$DEPT)        AS DEPT,
    r.get_cell(CSD$DEFAULT_BRACKET_IND) AS DEFAULT_BRACKET_IND,
    r.get_cell(CSD$RECALC_ORD_IND)  AS RECALC_ORD_IND,
    r.get_cell(CSD$COST_CHANGE_VALUE)   AS COST_CHANGE_VALUE,
    r.get_cell(CSD$COST_CHANGE_TYPE)    AS COST_CHANGE_TYPE,
    r.get_cell(CSD$UNIT_COST)       AS UNIT_COST,
    r.get_cell(CSD$BRACKET_VALUE2)  AS BRACKET_VALUE2,
    r.get_cell(CSD$BRACKET_UOM1)    AS BRACKET_UOM1,
    r.get_cell(CSD$BRACKET_VALUE1)  AS BRACKET_VALUE1,
    r.get_cell(CSD$ITEM)        AS ITEM,
    r.get_cell(CSD$ORIGIN_COUNTRY_ID)   AS ORIGIN_COUNTRY_ID,
    r.get_cell(CSD$SUPPLIER)        AS SUPPLIER,
    r.get_cell(CSD$COST_CHANGE)     AS COST_CHANGE,
    r.get_row_seq()         AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DELIVERY_COUNTRY_ID := rec.DELIVERY_COUNTRY_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'DELIVERY_COUNTRY_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUP_DEPT_SEQ_NO := rec.SUP_DEPT_SEQ_NO;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'SUP_DEPT_SEQ_NO',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DEPT := rec.DEPT;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'DEPT',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DEFAULT_BRACKET_IND := rec.DEFAULT_BRACKET_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'DEFAULT_BRACKET_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.RECALC_ORD_IND := rec.RECALC_ORD_IND;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'RECALC_ORD_IND',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COST_CHANGE_VALUE := rec.COST_CHANGE_VALUE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'COST_CHANGE_VALUE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COST_CHANGE_TYPE := rec.COST_CHANGE_TYPE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'COST_CHANGE_TYPE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.UNIT_COST := rec.UNIT_COST;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'UNIT_COST',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.BRACKET_VALUE2 := rec.BRACKET_VALUE2;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'BRACKET_VALUE2',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.BRACKET_UOM1 := rec.BRACKET_UOM1;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'BRACKET_UOM1',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.BRACKET_VALUE1 := rec.BRACKET_VALUE1;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'BRACKET_VALUE1',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'ORIGIN_COUNTRY_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SUPPLIER := rec.SUPPLIER;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'SUPPLIER',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.COST_CHANGE := rec.COST_CHANGE;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,'COST_CHANGE',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_COST_SUSP_SUP_DETAIL%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_COST_SUSP_SUP_DETAIL
       WHERE ORIGIN_COUNTRY_ID     = l_temp_rec.ORIGIN_COUNTRY_ID
         AND NVL(BRACKET_VALUE1,'-1') = NVL(l_temp_rec.BRACKET_VALUE1,'-1')
         AND ITEM = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND COST_CHANGE = l_temp_rec.COST_CHANGE;
    l_rms_rec COST_SUSP_SUP_DETAIL%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM COST_SUSP_SUP_DETAIL
       WHERE ORIGIN_COUNTRY_ID     = l_temp_rec.ORIGIN_COUNTRY_ID
         AND NVL(BRACKET_VALUE1,'-1') = NVL(l_temp_rec.BRACKET_VALUE1,'-1')
         AND ITEM = l_temp_rec.ITEM
         AND SUPPLIER = l_temp_rec.SUPPLIER
         AND COST_CHANGE = l_temp_rec.COST_CHANGE;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_COST_SUSP_SUP_DETAIL
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.BRACKET_UOM1_mi <> 'Y' THEN
        l_temp_rec.BRACKET_UOM1   := l_stg_rec.BRACKET_UOM1;
      END IF;
      IF l_mi_rec.BRACKET_VALUE2_mi <> 'Y' THEN
        l_temp_rec.BRACKET_VALUE2   := l_stg_rec.BRACKET_VALUE2;
      END IF;
      IF l_mi_rec.UNIT_COST_mi <> 'Y' THEN
        l_temp_rec.UNIT_COST   := l_stg_rec.UNIT_COST;
      END IF;
      IF l_mi_rec.COST_CHANGE_TYPE_mi <> 'Y' THEN
        l_temp_rec.COST_CHANGE_TYPE   := l_stg_rec.COST_CHANGE_TYPE;
      END IF;
      IF l_mi_rec.COST_CHANGE_VALUE_mi <> 'Y' THEN
        l_temp_rec.COST_CHANGE_VALUE   := l_stg_rec.COST_CHANGE_VALUE;
      END IF;
      IF l_mi_rec.RECALC_ORD_IND_mi <> 'Y' THEN
        l_temp_rec.RECALC_ORD_IND   := l_stg_rec.RECALC_ORD_IND;
      END IF;
      IF l_mi_rec.DEFAULT_BRACKET_IND_mi <> 'Y' THEN
        l_temp_rec.DEFAULT_BRACKET_IND   := l_stg_rec.DEFAULT_BRACKET_IND;
      END IF;
      IF l_mi_rec.DEPT_mi <> 'Y' THEN
        l_temp_rec.DEPT   := l_stg_rec.DEPT;
      END IF;
      IF l_mi_rec.SUP_DEPT_SEQ_NO_mi <> 'Y' THEN
        l_temp_rec.SUP_DEPT_SEQ_NO   := l_stg_rec.SUP_DEPT_SEQ_NO;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.BRACKET_UOM1_mi <> 'Y' THEN
        l_temp_rec.BRACKET_UOM1   := l_rms_rec.BRACKET_UOM1;
      END IF;
      IF l_mi_rec.BRACKET_VALUE2_mi <> 'Y' THEN
        l_temp_rec.BRACKET_VALUE2   := l_rms_rec.BRACKET_VALUE2;
      END IF;
      IF l_mi_rec.UNIT_COST_mi <> 'Y' THEN
        l_temp_rec.UNIT_COST   := l_rms_rec.UNIT_COST;
      END IF;
      IF l_mi_rec.COST_CHANGE_TYPE_mi <> 'Y' THEN
        l_temp_rec.COST_CHANGE_TYPE   := l_rms_rec.COST_CHANGE_TYPE;
      END IF;
      IF l_mi_rec.COST_CHANGE_VALUE_mi <> 'Y' THEN
        l_temp_rec.COST_CHANGE_VALUE   := l_rms_rec.COST_CHANGE_VALUE;
      END IF;
      IF l_mi_rec.RECALC_ORD_IND_mi <> 'Y' THEN
        l_temp_rec.RECALC_ORD_IND   := l_rms_rec.RECALC_ORD_IND;
      END IF;
      IF l_mi_rec.DEFAULT_BRACKET_IND_mi <> 'Y' THEN
        l_temp_rec.DEFAULT_BRACKET_IND   := l_rms_rec.DEFAULT_BRACKET_IND;
      END IF;
      IF l_mi_rec.DEPT_mi <> 'Y' THEN
        l_temp_rec.DEPT   := l_rms_rec.DEPT;
      END IF;
      IF l_mi_rec.SUP_DEPT_SEQ_NO_mi <> 'Y' THEN
        l_temp_rec.SUP_DEPT_SEQ_NO   := l_rms_rec.SUP_DEPT_SEQ_NO;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,CSD_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.COST_CHANGE := NVL( l_temp_rec.COST_CHANGE,l_default_rec.COST_CHANGE);
      l_temp_rec.SUPPLIER := NVL( l_temp_rec.SUPPLIER,l_default_rec.SUPPLIER);
      l_temp_rec.ORIGIN_COUNTRY_ID := NVL( l_temp_rec.ORIGIN_COUNTRY_ID,l_default_rec.ORIGIN_COUNTRY_ID);
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.BRACKET_VALUE1 := NVL( l_temp_rec.BRACKET_VALUE1,l_default_rec.BRACKET_VALUE1);
      l_temp_rec.BRACKET_UOM1 := NVL( l_temp_rec.BRACKET_UOM1,l_default_rec.BRACKET_UOM1);
      l_temp_rec.BRACKET_VALUE2 := NVL( l_temp_rec.BRACKET_VALUE2,l_default_rec.BRACKET_VALUE2);
      l_temp_rec.UNIT_COST := NVL( l_temp_rec.UNIT_COST,l_default_rec.UNIT_COST);
      l_temp_rec.COST_CHANGE_TYPE := NVL( l_temp_rec.COST_CHANGE_TYPE,l_default_rec.COST_CHANGE_TYPE);
      l_temp_rec.COST_CHANGE_VALUE := NVL( l_temp_rec.COST_CHANGE_VALUE,l_default_rec.COST_CHANGE_VALUE);
      l_temp_rec.RECALC_ORD_IND := NVL( l_temp_rec.RECALC_ORD_IND,l_default_rec.RECALC_ORD_IND);
      l_temp_rec.DEFAULT_BRACKET_IND := NVL( l_temp_rec.DEFAULT_BRACKET_IND,l_default_rec.DEFAULT_BRACKET_IND);
      l_temp_rec.DEPT := NVL( l_temp_rec.DEPT,l_default_rec.DEPT);
      l_temp_rec.SUP_DEPT_SEQ_NO := NVL( l_temp_rec.SUP_DEPT_SEQ_NO,l_default_rec.SUP_DEPT_SEQ_NO);
      l_temp_rec.DELIVERY_COUNTRY_ID := NVL( l_temp_rec.DELIVERY_COUNTRY_ID,l_default_rec.DELIVERY_COUNTRY_ID);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ORIGIN_COUNTRY_ID IS NOT NULL
        AND l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.SUPPLIER IS NOT NULL
        AND l_temp_rec.COST_CHANGE IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,CSD_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_COST_SUSP_SUP_DETAIL VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,CSD_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_COST_SUSP_SUP_DETAIL
     SET row         = svc_upd_col(i)
   WHERE ORIGIN_COUNTRY_ID     = svc_upd_col(i).ORIGIN_COUNTRY_ID
     AND NVL(BRACKET_VALUE1,'-1') = NVL(svc_upd_col(i).BRACKET_VALUE1,'-1')
     AND ITEM = svc_upd_col(i).ITEM
     AND SUPPLIER = svc_upd_col(i).SUPPLIER
     AND COST_CHANGE = svc_upd_col(i).COST_CHANGE
     AND DELIVERY_COUNTRY_ID = svc_upd_col(i).DELIVERY_COUNTRY_ID
;
END process_s9t_CSD;
-------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_IIM(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_ITEM_IMAGE.PROCESS_ID%TYPE)
IS

   TYPE SVC_COL_TYP is TABLE OF SVC_ITEM_IMAGE%ROWTYPE;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   L_error_code    NUMBER;
   L_stg_exists    BOOLEAN;
   L_error         BOOLEAN       := FALSE;
   L_pk_columns    VARCHAR2(255) := 'item and image name';
   L_table         VARCHAR2(30)  := 'SVC_ITEM_IMAGE';
   svc_ins_col     SVC_COL_TYP   := NEW SVC_COL_TYP();
   svc_upd_col     SVC_COL_TYP   := NEW SVC_COL_TYP();
   L_process_id    SVC_ITEM_IMAGE.PROCESS_ID%TYPE;
   L_temp_rec      SVC_ITEM_IMAGE%ROWTYPE;
   L_default_rec   SVC_ITEM_IMAGE%ROWTYPE;
   L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
   dml_errors      EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);

   cursor C_MANDATORY_IND is
      select display_priority_mi,
             primary_ind_mi,
             image_type_mi,
             image_desc_mi,
             image_addr_mi,
             image_name_mi,
             item_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = ITEM_INDUCT_SQL.TEMPLATE_KEY
                 and wksht_key = 'ITEM_IMAGE')
       pivot (MAX(mandatory) as mi FOR (column_key) in ('DISPLAY_PRIORITY' as display_priority,
                                                        'PRIMARY_IND' as primary_ind,
                                                        'IMAGE_TYPE' as image_type,
                                                        'IMAGE_DESC' as image_desc,
                                                        'IMAGE_ADDR' as image_addr,
                                                        'IMAGE_NAME' as image_name,
                                                        'ITEM' as item));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;

BEGIN
   -- Get default values.
   FOR rec in (select display_priority_dv,
                      primary_ind_dv,
                      image_type_dv,
                      image_desc_dv,
                      image_addr_dv,
                      image_name_dv,
                      item_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = ITEM_INDUCT_SQL.TEMPLATE_KEY
                          and wksht_key = 'ITEM_IMAGE')
                pivot (MAX(default_value) as dv FOR (column_key) in ('DISPLAY_PRIORITY' as display_priority,
                                                                     'PRIMARY_IND' as primary_ind,
                                                                     'IMAGE_TYPE' as image_type,
                                                                     'IMAGE_DESC' as image_desc,
                                                                     'IMAGE_ADDR' as image_addr,
                                                                     'IMAGE_NAME' as image_name,
                                                                     'ITEM' as item))
              ) LOOP
      BEGIN
         L_default_rec.display_priority := rec.display_priority_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_IMAGE',
                            NULL,
                            'DISPLAY_PRIORITY',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.primary_ind := rec.primary_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_IMAGE',
                            NULL,
                            'PRIMARY_IND',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.image_type := rec.image_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_IMAGE',
                            NULL,
                            'IMAGE_TYPE',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.image_desc := rec.image_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_IMAGE',
                            NULL,
                            'IMAGE_DESC',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.image_addr := rec.image_addr_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_IMAGE',
                            NULL,
                            'IMAGE_ADDR',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.image_name := rec.image_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_IMAGE',
                            NULL,
                            'IMAGE_NAME',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.item := rec.item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_IMAGE',
                            NULL,
                            'ITEM',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

   --get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   --
   -- XXADEO_RMS: Assigned get_sheet_name_trans(IIM_sheet) value to variable L_sheet_name
   --
   L_sheet_name := GET_SHEET_NAME_TRANS(IIM_sheet);
   --
   FOR rec in (select r.get_cell(iim$action)           as action,
                      r.get_cell(iim$display_priority) as display_priority,
                      r.get_cell(iim$primary_ind)      as primary_ind,
                      r.get_cell(iim$image_type)       as image_type,
                      r.get_cell(iim$image_desc)       as image_desc,
                      r.get_cell(iim$image_addr)       as image_addr,
                      r.get_cell(iim$image_name)       as image_name,
                      r.get_cell(iim$item)             as item,
                      r.get_row_seq()                  as row_seq
                 from s9t_folder sf,
                      table(sf.s9t_file_obj.sheets) ss,
                      table(ss.s9t_rows) r
                where sf.file_id = I_file_id
                  and ss.sheet_name = L_sheet_name) LOOP

      L_temp_rec.process_id           := I_process_id;
      L_temp_rec.chunk_id             := 1;
      L_temp_rec.row_seq              := rec.row_seq;
      L_temp_rec.process$status       := 'N';
      L_temp_rec.create_id            := GET_USER;
      L_temp_rec.last_update_id       := GET_USER;
      L_temp_rec.create_datetime      := SYSDATE;
      L_temp_rec.last_update_datetime := SYSDATE;
      L_error                         := FALSE;
      ---
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIM_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.display_priority := rec.display_priority;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIM_sheet,
                            rec.row_seq,
                            'DISPLAY_PRIORITY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.primary_ind := rec.primary_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIM_sheet,
                            rec.row_seq,
                            'PRIMARY_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.image_type := rec.image_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIM_sheet,
                            rec.row_seq,
                            'IMAGE_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.image_desc := rec.image_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIM_sheet,
                            rec.row_seq,
                            'IMAGE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.image_addr := rec.image_addr;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIM_sheet,
                            rec.row_seq,
                            'IMAGE_ADDR',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.image_name := rec.image_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIM_sheet,
                            rec.row_seq,
                            'IMAGE_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item := rec.item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIM_sheet,
                            rec.row_seq,
                            'ITEM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      ---
      if NOT L_error then
         DECLARE
            L_stg_rec      SVC_ITEM_IMAGE%ROWTYPE;
            L_rms_rec      ITEM_IMAGE%ROWTYPE;
            L_rms_exists   BOOLEAN;

            cursor C_STG_REC is
               select *
                 from svc_item_image
                where item = L_temp_rec.item
                  and image_name = L_temp_rec.image_name;

            cursor C_RMS_REC is
               select *
                 from item_image
                where item = L_temp_rec.item
                  and image_name = L_temp_rec.image_name;

         BEGIN
            open C_STG_REC;
            fetch C_STG_REC into L_stg_rec;
            if C_STG_REC%FOUND then
               L_stg_exists := TRUE;
            else
               L_stg_exists := FALSE;
            end if;
            close C_STG_REC;
            ---
            open C_RMS_REC;
            fetch C_RMS_REC into L_rms_rec;
            if C_RMS_REC%FOUND then
               L_rms_exists := TRUE;
            else
               L_rms_exists := FALSE;
            end if;
            close C_RMS_REC;
            -- delete record from staging if it is already uploaded to rms.
            if L_stg_exists and L_stg_rec.process$status = 'P' and GET_PRC_DEST(L_stg_rec.process_id) = 'RMS' then
               delete from svc_item_image
                where process_id = L_stg_rec.process_id
                  and row_seq = L_stg_rec.row_seq;
               L_stg_exists := FALSE;
            end if;
            -- copy values for non-mandatory columns from staging.
            if L_stg_exists then
               if L_mi_rec.primary_ind_mi <> 'Y' then
                 L_temp_rec.primary_ind := L_stg_rec.primary_ind;
               end if;
            end if;
            -- if record exists in rms but not in staging, then copy values for non-mandatory columns from rms.
            if L_rms_exists and NOT L_stg_exists then
               if L_mi_rec.primary_ind_mi <> 'Y' then
                 L_temp_rec.primary_ind := L_rms_rec.primary_ind;
               end if;
            end if;
            --resolve new value for action column
            if L_stg_exists then
               -- if it is a create record and record already exists in staging, log an error
               if L_temp_rec.action = CORESVC_ITEM.ACTION_NEW then
                  if L_stg_rec.process$status = 'P' then
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS',
                                                       L_pk_columns);
                  else
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                       L_pk_columns);
                  end if;
                  WRITE_S9T_ERROR (I_file_id,
                                   IIM_sheet,
                                   L_temp_rec.row_seq,
                                   NULL,
                                   NULL,
                                   L_error_msg );
                  L_error := TRUE;
               else
                  -- if record does not exist in staging then keep the user provided value
                  L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,
                                                      L_temp_rec.action);
               end if;
            end if;
         END;
      end if;
      -- apply defaults if it is a create record and record does not exist in staging already
      if rec.action = CORESVC_ITEM.ACTION_NEW and NOT L_error and NOT L_stg_exists then
         L_temp_rec.item := NVL(L_temp_rec.item, L_default_rec.item);
         L_temp_rec.image_name := NVL(L_temp_rec.image_name, L_default_rec.image_name);
         L_temp_rec.image_addr := NVL( L_temp_rec.image_addr, L_default_rec.image_addr);
         L_temp_rec.image_desc := NVL( L_temp_rec.image_desc, L_default_rec.image_desc);
         L_temp_rec.image_type := NVL( L_temp_rec.image_type, L_default_rec.image_type);
         L_temp_rec.primary_ind := NVL( L_temp_rec.primary_ind, L_default_rec.primary_ind);
         L_temp_rec.display_priority := NVL( L_temp_rec.display_priority, L_default_rec.display_priority);
      end if;
      -- check pk cols. log error if any pk col is NULL.
      if NOT (L_temp_rec.item is NOT NULL and L_temp_rec.image_name is NOT NULL) then
         L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                           L_pk_columns);
         WRITE_S9T_ERROR(I_file_id,
                         IIM_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         L_error_msg);
         L_error := TRUE;
      end if;
      -- if no error so far then add temp record to insert or update collection depending upon
      -- whether record already exists in staging or not.
      if NOT L_error then
         if L_stg_exists then
            svc_upd_col.extend();
            svc_upd_col(svc_upd_col.COUNT()) := L_temp_rec;
         else
            svc_ins_col.extend();
            svc_ins_col(svc_ins_col.COUNT()) := L_temp_rec;
         end if;
      end if;
   END LOOP;

   -- flush insert collection
   BEGIN
      FORALL i in 1..svc_ins_col.COUNT SAVE EXCEPTIONS
      insert into svc_item_image values svc_ins_col(i);

   EXCEPTION
      when DML_ERRORS then
         for i in 1..sql%bulk_exceptions.COUNT loop
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                 L_pk_columns,
                                                 L_table);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            IIM_sheet,
                            svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         end loop;
   END;

   -- flush update collection
   FORALL i in 1..svc_upd_col.COUNT
      update svc_item_image
         set row = svc_upd_col(i)
       where item = svc_upd_col(i).item
         and image_name = svc_upd_col(i).image_name;

END PROCESS_S9T_IIM;
-------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_IIM_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                             I_process_id   IN   SVC_ITEM_IMAGE_TL.PROCESS_ID%TYPE)
IS

   TYPE SVC_COL_TYP is TABLE OF SVC_ITEM_IMAGE_TL%ROWTYPE;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   L_error_code    NUMBER;
   L_stg_exists    BOOLEAN;
   L_error         BOOLEAN       := FALSE;
   L_pk_columns    VARCHAR2(255) := 'lang, item and image name';
   L_table         VARCHAR2(30)  := 'SVC_ITEM_IMAGE_TL';
   svc_ins_col     SVC_COL_TYP   := NEW SVC_COL_TYP();
   svc_upd_col     SVC_COL_TYP   := NEW SVC_COL_TYP();
   L_process_id    SVC_ITEM_IMAGE_TL.PROCESS_ID%TYPE;
   L_temp_rec      SVC_ITEM_IMAGE_TL%ROWTYPE;
   L_default_rec   SVC_ITEM_IMAGE_TL%ROWTYPE;
   L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
   dml_errors      EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);

   cursor C_MANDATORY_IND is
      select image_desc_mi,
             image_name_mi,
             item_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = ITEM_INDUCT_SQL.TEMPLATE_KEY
                 and wksht_key = 'ITEM_IMAGE_TL')
       pivot (MAX(mandatory) as mi FOR (column_key) in ('IMAGE_DESC' as image_desc,
                                                        'IMAGE_NAME' as image_name,
                                                        'ITEM' as item,
                                                        'LANG' as lang));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;

BEGIN
   -- Get default values.
   FOR rec in (select image_desc_dv,
                      image_name_dv,
                      item_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = ITEM_INDUCT_SQL.TEMPLATE_KEY
                          and wksht_key = 'ITEM_IMAGE_TL')
                pivot (MAX(default_value) as dv FOR (column_key) in ('IMAGE_DESC' as image_desc,
                                                                     'IMAGE_NAME' as image_name,
                                                                     'ITEM' as item,
                                                                     'LANG' as lang))
              ) LOOP
      BEGIN
         L_default_rec.image_desc := rec.image_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_IMAGE_TL',
                            NULL,
                            'IMAGE_DESC',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.image_name := rec.image_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_IMAGE_TL',
                            NULL,
                            'IMAGE_NAME',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.item := rec.item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_IMAGE_TL',
                            NULL,
                            'ITEM',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'ITEM_IMAGE_TL',
                            NULL,
                            'LANG',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

   --get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   --
   -- XXADEO_RMS: Assigned get_sheet_name_trans(IIMTL_sheet) value to variable L_sheet_name
   --
   L_sheet_name := GET_SHEET_NAME_TRANS(IIMTL_sheet);
   --
   FOR rec in (select r.get_cell(iimtl$action)           as action,
                      r.get_cell(iimtl$image_desc)       as image_desc,
                      r.get_cell(iimtl$image_name)       as image_name,
                      r.get_cell(iimtl$item)             as item,
                      r.get_cell(iimtl$lang)             as lang,
                      r.get_row_seq()                    as row_seq
                 from s9t_folder sf,
                      table(sf.s9t_file_obj.sheets) ss,
                      table(ss.s9t_rows) r
                where sf.file_id = I_file_id
                  and ss.sheet_name = L_sheet_name) LOOP

      L_temp_rec.process_id           := I_process_id;
      L_temp_rec.chunk_id             := 1;
      L_temp_rec.row_seq              := rec.row_seq;
      L_temp_rec.process$status       := 'N';
      L_temp_rec.create_id            := GET_USER;
      L_temp_rec.last_update_id       := GET_USER;
      L_temp_rec.create_datetime      := SYSDATE;
      L_temp_rec.last_update_datetime := SYSDATE;
      L_error                         := FALSE;
      ---
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIMTL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.image_desc := rec.image_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIMTL_sheet,
                            rec.row_seq,
                            'IMAGE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.image_name := rec.image_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIMTL_sheet,
                            rec.row_seq,
                            'IMAGE_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item := rec.item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIMTL_sheet,
                            rec.row_seq,
                            'ITEM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IIMTL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      ---
      if NOT L_error then
         DECLARE
            L_stg_rec      SVC_ITEM_IMAGE_TL%ROWTYPE;
            L_rms_rec      ITEM_IMAGE_TL%ROWTYPE;
            L_rms_exists   BOOLEAN;

            cursor C_STG_REC is
               select *
                 from svc_item_image_tl
                where item = L_temp_rec.item
                  and lang = L_temp_rec.lang
                  and image_name = L_temp_rec.image_name;

            cursor C_RMS_REC is
               select *
                 from item_image_tl
                where item = L_temp_rec.item
                  and lang = L_temp_rec.lang
                  and image_name = L_temp_rec.image_name;

         BEGIN
            open C_STG_REC;
            fetch C_STG_REC into L_stg_rec;
            if C_STG_REC%FOUND then
               L_stg_exists := TRUE;
            else
               L_stg_exists := FALSE;
            end if;
            close C_STG_REC;
            ---
            open C_RMS_REC;
            fetch C_RMS_REC into L_rms_rec;
            if C_RMS_REC%FOUND then
               L_rms_exists := TRUE;
            else
               L_rms_exists := FALSE;
            end if;
            close C_RMS_REC;
            -- delete record from staging if it is already uploaded to rms.
            if L_stg_exists and L_stg_rec.process$status = 'P' and GET_PRC_DEST(L_stg_rec.process_id) = 'RMS' then
               delete from svc_item_image_tl
                where process_id = L_stg_rec.process_id
                  and row_seq = L_stg_rec.row_seq;
               L_stg_exists := FALSE;
            end if;
            --resolve new value for action column
            if L_stg_exists then
               -- if it is a create record and record already exists in staging, log an error
               if L_temp_rec.action = CORESVC_ITEM.ACTION_NEW then
                  if L_stg_rec.process$status = 'P' then
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS',
                                                       L_pk_columns);
                  else
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                       L_pk_columns);
                  end if;
                  WRITE_S9T_ERROR (I_file_id,
                                   IIMTL_sheet,
                                   L_temp_rec.row_seq,
                                   NULL,
                                   NULL,
                                   L_error_msg );
                  L_error := TRUE;
               else
                  -- if record does not exist in staging then keep the user provided value
                  L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,
                                                      L_temp_rec.action);
               end if;
            end if;
         END;
      end if;
      -- apply defaults if it is a create record and record does not exist in staging already
      if rec.action = CORESVC_ITEM.ACTION_NEW and NOT L_error and NOT L_stg_exists then
         L_temp_rec.lang := NVL( L_temp_rec.lang, L_default_rec.lang);
         L_temp_rec.item := NVL(L_temp_rec.item, L_default_rec.item);
         L_temp_rec.image_name := NVL(L_temp_rec.image_name, L_default_rec.image_name);
         L_temp_rec.image_desc := NVL( L_temp_rec.image_desc, L_default_rec.image_desc);
      end if;
      -- check pk cols. log error if any pk col is NULL.
      if NOT (L_temp_rec.item is NOT NULL and L_temp_rec.lang is NOT NULL and L_temp_rec.image_name is NOT NULL) then
         L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                           L_pk_columns);
         WRITE_S9T_ERROR(I_file_id,
                         IIMTL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         L_error_msg);
         L_error := TRUE;
      end if;
      -- if no error so far then add temp record to insert or update collection depending upon
      -- whether record already exists in staging or not.
      if NOT L_error then
         if L_stg_exists then
            svc_upd_col.extend();
            svc_upd_col(svc_upd_col.COUNT()) := L_temp_rec;
         else
            svc_ins_col.extend();
            svc_ins_col(svc_ins_col.COUNT()) := L_temp_rec;
         end if;
      end if;
   END LOOP;

   -- flush insert collection
   BEGIN
      FORALL i in 1..svc_ins_col.COUNT SAVE EXCEPTIONS
      insert into svc_item_image_tl values svc_ins_col(i);

   EXCEPTION
      when DML_ERRORS then
         for i in 1..sql%bulk_exceptions.COUNT loop
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                 L_pk_columns,
                                                 L_table);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            IIMTL_sheet,
                            svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         end loop;
   END;

   -- flush update collection
   FORALL i in 1..svc_upd_col.COUNT
      update svc_item_image_tl
         set row = svc_upd_col(i)
       where item = svc_upd_col(i).item
         and lang = svc_upd_col(i).lang
         and image_name = svc_upd_col(i).image_name;

END PROCESS_S9T_IIM_TL;
-------------------------------------------------------------------------------------
PROCEDURE process_s9t_ISE
  (
    I_file_id    IN s9t_folder.file_id%type,
    I_process_id IN SVC_ITEM_SEASONS.process_id%type
  )
IS
Type svc_col_typ
IS
  TABLE OF SVC_ITEM_SEASONS%rowtype;
  l_temp_rec SVC_ITEM_SEASONS%rowtype;
  svc_ins_col svc_col_typ :=NEW svc_col_typ();
  svc_upd_col svc_col_typ :=NEW svc_col_typ();
  l_stg_exists BOOLEAN;
  l_process_id SVC_ITEM_SEASONS.process_id%type;
  l_error BOOLEAN:=false;
  l_default_rec SVC_ITEM_SEASONS%rowtype;
  L_sheet_name    S9T_TMPL_WKSHT_DEF.WKSHT_NAME%TYPE := NULL;
  CURSOR c_mandatory_ind
  IS
    SELECT DIFF_ID_mi,
      ITEM_SEASON_SEQ_NO_mi,
      PHASE_ID_mi,
      SEASON_ID_mi,
      ITEM_mi,
      1 AS dummy
    FROM
      (SELECT column_key,
    mandatory
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_SEASONS'
      ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'DIFF_ID' AS DIFF_ID, 'ITEM_SEASON_SEQ_NO' AS ITEM_SEASON_SEQ_NO, 'PHASE_ID' AS PHASE_ID, 'SEASON_ID' AS SEASON_ID, 'ITEM' AS ITEM, NULL AS dummy));
  l_mi_rec c_mandatory_ind%rowtype;
  L_pk_columns   VARCHAR2(255) := 'item and item season sequence number';
  L_table    VARCHAR2(30) := 'SVC_ITEM_SEASONS';
  L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
  L_error_code   NUMBER;
  dml_errors EXCEPTION;
  PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
  FOR rec IN
  (SELECT DIFF_ID_dv,
    ITEM_SEASON_SEQ_NO_dv,
    PHASE_ID_dv,
    SEASON_ID_dv,
    ITEM_dv,
    NULL AS dummy
  FROM
    (SELECT column_key,
      default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key                  = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_SEASONS'
    ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ( 'DIFF_ID' AS DIFF_ID, 'ITEM_SEASON_SEQ_NO' AS ITEM_SEASON_SEQ_NO, 'PHASE_ID' AS PHASE_ID, 'SEASON_ID' AS SEASON_ID, 'ITEM' AS ITEM, NULL AS dummy))
  )
  LOOP
    BEGIN
      l_default_rec.DIFF_ID := rec.DIFF_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SEASONS',NULL,'DIFF_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM_SEASON_SEQ_NO := rec.ITEM_SEASON_SEQ_NO_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SEASONS',NULL,'ITEM_SEASON_SEQ_NO','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.PHASE_ID := rec.PHASE_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SEASONS',NULL,'PHASE_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.SEASON_ID := rec.SEASON_ID_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SEASONS',NULL,'SEASON_ID','INV_DEFAULT',SQLERRM);
    END;
    BEGIN
      l_default_rec.ITEM := rec.ITEM_dv;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,'ITEM_SEASONS',NULL,'ITEM','INV_DEFAULT',SQLERRM);
    END;
  END LOOP;
  --Get mandatory indicators
  OPEN C_mandatory_ind;
  FETCH C_mandatory_ind
  INTO l_mi_rec;
  CLOSE C_mandatory_ind;
  --
  -- XXADEO_RMS: Assigned get_sheet_name_trans(ISE_sheet) value to variable L_sheet_name
  --
  L_sheet_name := get_sheet_name_trans(ISE_sheet);
  --
  FOR rec IN
  (SELECT r.get_cell(ISE$Action)     AS Action,
    r.get_cell(ISE$DIFF_ID)               AS DIFF_ID,
    r.get_cell(ISE$ITEM_SEASON_SEQ_NO)    AS ITEM_SEASON_SEQ_NO,
    r.get_cell(ISE$PHASE_ID)              AS PHASE_ID,
    r.get_cell(ISE$SEASON_ID)             AS SEASON_ID,
    r.get_cell(ISE$ITEM)                  AS ITEM,
    r.get_row_seq()                  AS row_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss,
    TABLE(ss.s9t_rows) r
  WHERE sf.file_id  = I_file_id
  AND ss.sheet_name = L_sheet_name
  )
  LOOP
    l_temp_rec.process_id    := I_process_id;
    l_temp_rec.chunk_id      := 1;
    l_temp_rec.row_seq       := rec.row_seq;
    l_temp_rec.process$status    := 'N';
    l_temp_rec.create_id     := get_user;
    l_temp_rec.last_upd_id   := get_user;
    l_temp_rec.create_datetime   := sysdate;
    l_temp_rec.last_upd_datetime := sysdate;
    l_error          := false;
    BEGIN
      l_temp_rec.Action := rec.Action;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISE_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.DIFF_ID := rec.DIFF_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISE_sheet,rec.row_seq,'DIFF_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM_SEASON_SEQ_NO := rec.ITEM_SEASON_SEQ_NO;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISE_sheet,rec.row_seq,'ITEM_SEASON_SEQ_NO',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.PHASE_ID := rec.PHASE_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISE_sheet,rec.row_seq,'PHASE_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.SEASON_ID := rec.SEASON_ID;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISE_sheet,rec.row_seq,'SEASON_ID',SQLCODE,SQLERRM);
      l_error := true;
    END;
    BEGIN
      l_temp_rec.ITEM := rec.ITEM;
    EXCEPTION
    WHEN OTHERS THEN
      write_s9t_error(I_file_id,ISE_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
      l_error := true;
    END;
    IF NOT l_error THEN
      DECLARE
    l_stg_rec SVC_ITEM_SEASONS%rowtype;
    CURSOR c_stg_rec
    IS
      SELECT *
        FROM SVC_ITEM_SEASONS
       WHERE ITEM               = l_temp_rec.ITEM
         AND ITEM_SEASON_SEQ_NO = l_temp_rec.ITEM_SEASON_SEQ_NO
;
    l_rms_rec ITEM_SEASONS%rowtype;
    CURSOR c_rms_rec
    IS
      SELECT *
        FROM ITEM_SEASONS
       WHERE ITEM               = l_temp_rec.ITEM
         AND ITEM_SEASON_SEQ_NO = l_temp_rec.ITEM_SEASON_SEQ_NO
;
    l_rms_exists BOOLEAN;
      BEGIN
    OPEN c_stg_rec;
    FETCH c_stg_rec INTO l_stg_rec;
    IF c_stg_rec%found THEN
      l_stg_exists := true;
    ELSE
      l_stg_exists := false;
    END IF;
    CLOSE c_stg_rec;
    OPEN c_rms_rec;
    FETCH c_rms_rec INTO l_rms_rec;
    IF c_rms_rec%found THEN
      l_rms_exists := true;
    ELSE
      l_rms_exists := false;
    END IF;
    CLOSE c_rms_rec;
    -- Delete record from staging if it is already uploaded to RMS.
    IF l_stg_exists AND l_stg_rec.process$status = 'P' AND get_prc_dest(l_stg_rec.process_id)='RMS' THEN
      DELETE
        FROM SVC_ITEM_SEASONS
       WHERE process_id = l_stg_rec.process_id
         AND row_seq      = l_stg_rec.row_seq;
      l_stg_exists    := false;
    END IF;
    -- Copy values for non-mandatory columns from Staging.
    IF l_stg_exists THEN
      IF l_mi_rec.SEASON_ID_mi <> 'Y' THEN
        l_temp_rec.SEASON_ID   := l_stg_rec.SEASON_ID;
      END IF;
      IF l_mi_rec.PHASE_ID_mi <> 'Y' THEN
        l_temp_rec.PHASE_ID   := l_stg_rec.PHASE_ID;
      END IF;
      IF l_mi_rec.DIFF_ID_mi <> 'Y' THEN
        l_temp_rec.DIFF_ID   := l_stg_rec.DIFF_ID;
      END IF;
    END IF;
    -- If record exists in RMS but not in staging, then Copy values for non-mandatory columns from RMS.
    IF l_rms_exists AND NOT l_stg_exists THEN
      IF l_mi_rec.SEASON_ID_mi <> 'Y' THEN
        l_temp_rec.SEASON_ID   := l_rms_rec.SEASON_ID;
      END IF;
      IF l_mi_rec.PHASE_ID_mi <> 'Y' THEN
        l_temp_rec.PHASE_ID   := l_rms_rec.PHASE_ID;
      END IF;
      IF l_mi_rec.DIFF_ID_mi <> 'Y' THEN
        l_temp_rec.DIFF_ID   := l_rms_rec.DIFF_ID;
      END IF;
    END IF;
    --Resolve new value for action column
    IF l_stg_exists THEN
      -- if it is a create record and record already exists in staging, log an error
      IF l_temp_rec.action = coresvc_item.action_new THEN
        IF l_stg_rec.process$status = 'P' THEN
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
        ELSE
           L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
        END IF;
        write_s9t_error ( I_file_id,ISE_sheet,l_temp_rec.row_seq,NULL,NULL,L_error_msg );
        l_error := true;
      ELSE
        -- If record does not exist in staging then keep the user provided value
        l_temp_rec.action := get_new_action(l_stg_rec.action,l_temp_rec.action);
      END IF;
    END IF;
      END;
    END IF;
    -- Apply defaults if it is a create record and record does not exist in staging already
    IF rec.action         = CORESVC_ITEM.action_new AND NOT l_error AND NOT l_stg_exists THEN
      l_temp_rec.ITEM := NVL( l_temp_rec.ITEM,l_default_rec.ITEM);
      l_temp_rec.SEASON_ID := NVL( l_temp_rec.SEASON_ID,l_default_rec.SEASON_ID);
      l_temp_rec.PHASE_ID := NVL( l_temp_rec.PHASE_ID,l_default_rec.PHASE_ID);
      l_temp_rec.ITEM_SEASON_SEQ_NO := NVL( l_temp_rec.ITEM_SEASON_SEQ_NO,l_default_rec.ITEM_SEASON_SEQ_NO);
      l_temp_rec.DIFF_ID := NVL( l_temp_rec.DIFF_ID,l_default_rec.DIFF_ID);
    END IF;
    -- Check PK cols. Log error if any PK col is null.
    IF NOT (
        l_temp_rec.ITEM IS NOT NULL
        AND l_temp_rec.ITEM_SEASON_SEQ_NO IS NOT NULL
        ) THEN
      L_error_msg                      := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED', L_pk_columns);
      write_s9t_error(I_file_id,ISE_sheet,rec.row_seq,NULL,NULL,L_error_msg);
      l_error := true;
    END IF;
    -- If no error so far then add temp record to insert or update collection depending upon
    -- whether record already exists in staging or not.
    IF NOT l_error THEN
      IF l_stg_exists THEN
    svc_upd_col.extend();
    svc_upd_col(svc_upd_col.count()):=l_temp_rec;
      ELSE
    svc_ins_col.extend();
    svc_ins_col(svc_ins_col.count()):=l_temp_rec;
      END IF;
    END IF;
  END LOOP;
  --Flush insert collection
  BEGIN
  forall i IN 1..svc_ins_col.count SAVE EXCEPTIONS
  INSERT INTO SVC_ITEM_SEASONS VALUES svc_ins_col
    (i
    );
  EXCEPTION
  WHEN DML_ERRORS THEN
    FOR i IN 1..sql%bulk_exceptions.count
    LOOP
      L_error_code := sql%bulk_exceptions(i).error_code;
      if L_error_code = 1 then
    L_error_code := NULL;
    L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                      L_pk_columns,
                      L_table);
      end if;
      write_s9t_error
      (
    I_file_id,ISE_sheet,svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,L_error_code,L_error_msg
      )
      ;
    END LOOP;
  END;

  -- flush update collection
  forall i IN 1..svc_upd_col.count
  UPDATE SVC_ITEM_SEASONS
     SET row         = svc_upd_col(i)
   WHERE ITEM     = svc_upd_col(i).ITEM
     AND ITEM_SEASON_SEQ_NO = svc_upd_col(i).ITEM_SEASON_SEQ_NO
;
END process_s9t_ISE;
-------------------------------------------------------------------------------------
FUNCTION process_s9t
  (
    O_error_message IN OUT rtk_errors.rtk_text%type ,
    I_file_id       IN s9t_folder.file_id%type,
    I_process_id    IN NUMBER,
    O_error_count OUT NUMBER
  )
  RETURN BOOLEAN
IS
  l_file s9t_file;
  l_sheets s9t_pkg.names_map_typ;
  L_program VARCHAR2(255):='ITEM_INDUCT_SQL.process_s9t';
  l_process_status svc_process_tracker.status%type;

  INVALID_FORMAT   EXCEPTION;
  MAX_CHAR     EXCEPTION;
  PRAGMA       EXCEPTION_INIT(INVALID_FORMAT, -31011);
  PRAGMA       EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
  commit;
  s9t_pkg.ods2obj(I_file_id);
  l_file        := s9t_pkg.get_obj(I_file_id);
  template_key      := l_file.template_key;
  Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
    if s9t_pkg.code2desc(O_error_message,
                       'IS9T',
                       l_file,
                                     true)=FALSE then
     return FALSE;
  end if;
  s9t_pkg.save_obj(l_file);
  --
  IF s9t_pkg.validate_template(I_file_id) = false THEN
    write_s9t_error(I_file_id,NULL,NULL,'ACTIVE_DATE',NULL,'S9T_INVALID_TEMPLATE');
  ELSE
    populate_names(I_file_id);
    sheet_name_trans := s9t_pkg.sheet_trans(l_file.template_key,l_file.user_lang);
    process_s9t_ICD(I_file_id,I_process_id);
    process_s9t_ICH(I_file_id,I_process_id);
    process_s9t_IC(I_file_id,I_process_id);
    process_s9t_IM(I_file_id,I_process_id);
    PROCESS_S9T_IM_TL(I_file_id,
                      I_process_id);
    process_s9t_IS(I_file_id,I_process_id);
    PROCESS_S9T_IS_TL(I_file_id,
                      I_process_id);
    process_s9t_ISC(I_file_id,I_process_id);
    process_s9t_ISCL(I_file_id,I_process_id);
    process_s9t_ISCD(I_file_id,I_process_id);
    process_s9t_ISMC(I_file_id,I_process_id);
    process_s9t_ISU(I_file_id,I_process_id);
    process_s9t_IXD(I_file_id,I_process_id);
    process_s9t_IXH(I_file_id,I_process_id);
    PROCESS_S9T_IXH_TL(I_file_id,
                       I_process_id);
    process_s9t_PI(I_file_id,I_process_id);
    process_s9t_IZP(I_file_id,I_process_id);
    process_s9t_UID(I_file_id,I_process_id);
    process_s9t_UIF(I_file_id,I_process_id);
    process_s9t_UIL(I_file_id,I_process_id);
    process_s9t_VI(I_file_id,I_process_id);
    process_s9t_CSH(I_file_id,I_process_id);
    process_s9t_CSDL(I_file_id,I_process_id);
    process_s9t_CSD(I_file_id,I_process_id);
    PROCESS_S9T_IIM(I_file_id,
                    I_process_id);
    PROCESS_S9T_IIM_TL(I_file_id,
                       I_process_id);
    process_s9t_ISE(I_file_id,I_process_id);
    IF CORESVC_CFLEX.PROCESS_S9T_SHEETS(O_error_message,
                                        I_file_id,
                                        I_process_id,
                                        template_key) = FALSE THEN
       return FALSE;
    END IF;
    FOR i in 1..s9t_pkg.Lp_s9t_errors_tab.count
    LOOP
    Lp_s9t_errors_tab.extend;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count):= s9t_pkg.Lp_s9t_errors_tab(i);
    END LOOP;
    S9T_PKG.Lp_s9t_errors_tab.delete;
    IF item_induct_sql.apply_tmpl_defaults(O_error_message,I_file_id,I_process_id)=false THEN
      RETURN false;
    END IF;

    IF CORESVC_ITEM.COMPLETE_SVC_IM(O_error_message,I_process_id)=false THEN
      RETURN false;
    END IF;

    IF CORESVC_COSTCHG.COMPLETE_SVC_CSH(O_error_message,I_process_id)=false THEN
      RETURN false;
    END IF;
  END IF;
  O_error_count := Lp_s9t_errors_tab.count();
  forall i IN 1..O_error_count
  INSERT INTO s9t_errors VALUES Lp_s9t_errors_tab
    (i
    );
  Lp_s9t_errors_tab  := NEW s9t_errors_tab_typ();
  IF O_error_count    = 0 THEN
    l_process_status := 'PS';
  ELSE
    l_process_status := 'PE';
  END IF;
  IF l_process_status = 'PE' then
     UPDATE svc_process_tracker
    SET status     = l_process_status,
        file_id    = I_file_id
      WHERE process_id = I_process_id;
  ELSIF l_process_status = 'PS' then
     UPDATE svc_process_tracker
    SET status     = l_process_status,
        file_id    = I_file_id
      WHERE process_id = I_process_id
    AND process_destination = 'STG';
  END IF;
  RETURN true;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END process_s9t;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_ICD(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ICD_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,ORIGIN_COUNTRY_ID ,DELIVERY_COUNTRY_ID ,COND_TYPE ,COND_VALUE ,APPLIED_ON ,COMP_RATE ,CALCULATION_BASIS ,RECOVERABLE_AMOUNT ,MODIFIED_TAXABLE_BASE ))
  FROM SVC_ITEM_COST_DETAIL
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_ICD;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_ICH(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ICH_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,ORIGIN_COUNTRY_ID ,DELIVERY_COUNTRY_ID ,PRIM_DLVY_CTRY_IND ,NIC_STATIC_IND ,BASE_COST ,NEGOTIATED_ITEM_COST ,EXTENDED_BASE_COST ,INCLUSIVE_COST ))
  FROM SVC_ITEM_COST_HEAD
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_ICH;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_IC(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = IC_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,COUNTRY_ID ))
  FROM SVC_ITEM_COUNTRY
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_IC;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_IM(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = IM_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,ITEM_NUMBER_TYPE ,FORMAT_ID ,PREFIX ,ITEM_PARENT ,ITEM_GRANDPARENT ,PACK_IND ,ITEM_LEVEL ,TRAN_LEVEL ,ITEM_AGGREGATE_IND ,DIFF_1 ,DIFF_1_AGGREGATE_IND ,DIFF_2 ,DIFF_2_AGGREGATE_IND ,DIFF_3 ,DIFF_3_AGGREGATE_IND ,DIFF_4 ,DIFF_4_AGGREGATE_IND ,DEPT ,CLASS ,SUBCLASS ,STATUS ,ITEM_DESC ,ITEM_DESC_SECONDARY ,SHORT_DESC ,PRIMARY_REF_ITEM_IND ,COST_ZONE_GROUP_ID ,STANDARD_UOM ,UOM_CONV_FACTOR ,PACKAGE_SIZE ,PACKAGE_UOM ,MERCHANDISE_IND ,STORE_ORD_MULT ,FORECAST_IND ,MFG_REC_RETAIL ,RETAIL_LABEL_TYPE ,RETAIL_LABEL_VALUE ,HANDLING_TEMP ,HANDLING_SENSITIVITY ,CATCH_WEIGHT_IND ,WASTE_TYPE ,WASTE_PCT ,DEFAULT_WASTE_PCT ,CONST_DIMEN_IND ,SIMPLE_PACK_IND ,CONTAINS_INNER_IND ,SELLABLE_IND ,ORDERABLE_IND ,PACK_TYPE ,ORDER_AS_TYPE ,COMMENTS ,ITEM_SERVICE_LEVEL ,GIFT_WRAP_IND ,SHIP_ALONE_IND ,ITEM_XFORM_IND ,INVENTORY_IND ,ORDER_TYPE ,SALE_TYPE ,DEPOSIT_ITEM_TYPE ,CONTAINER_ITEM ,DEPOSIT_IN_PRICE_PER_UOM ,AIP_CASE_TYPE ,
     PERISHABLE_IND ,NOTIONAL_PACK_IND ,SOH_INQUIRY_AT_PACK_IND ,PRODUCT_CLASSIFICATION ,BRAND_NAME,PRE_RESERVED_IND,LAST_UPD_ID,NEXT_UPD_ID, ORIG_REF_NO ))
  FROM SVC_ITEM_MASTER
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_IM;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_IM_TL(I_file_id      IN   NUMBER,
                             I_process_id   IN   NUMBER)
IS

BEGIN
   insert into table (select ss.s9t_rows
                        from s9t_folder sf,
                             table(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = IMTL_sheet)
              select s9t_row(s9t_cells('MOD',
                                       lang,
                                       item,
                                       item_desc,
                                       item_desc_secondary,
                                       short_desc)
                            )
                from svc_item_master_tl
               where item in (select item
                                from svc_item_search_temp
                               where process_id = I_process_id);

END POPULATE_SVC_IM_TL;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_IS(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = IS_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,PRIMARY_SUPP_IND ,VPN ,SUPP_LABEL ,CONSIGNMENT_RATE ,SUPP_DIFF_1 ,SUPP_DIFF_2 ,SUPP_DIFF_3 ,SUPP_DIFF_4 ,PALLET_NAME ,CASE_NAME ,INNER_NAME ,SUPP_DISCONTINUE_DATE ,DIRECT_SHIP_IND ,CONCESSION_RATE ,PRIMARY_CASE_SIZE ))
  FROM SVC_ITEM_SUPPLIER
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_IS;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_IS_TL(I_file_id      IN   NUMBER,
                             I_process_id   IN   NUMBER)
IS

BEGIN
   insert into table (select ss.s9t_rows
                        from s9t_folder sf,
                             table(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = ISTL_sheet)
              select s9t_row(s9t_cells('MOD',
                                       lang,
                                       item,
                                       supplier,
                                       supp_label,
                                       supp_diff_1,
                                       supp_diff_2,
                                       supp_diff_3,
                                       supp_diff_4)
                            )
                from svc_item_supplier_tl
               where item in (select item
                                from svc_item_search_temp
                               where process_id = I_process_id);

END POPULATE_SVC_IS_TL;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_ISC(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ISC_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,ORIGIN_COUNTRY_ID ,UNIT_COST ,LEAD_TIME ,PICKUP_LEAD_TIME ,SUPP_PACK_SIZE ,INNER_PACK_SIZE ,ROUND_LVL ,ROUND_TO_INNER_PCT ,ROUND_TO_CASE_PCT ,ROUND_TO_LAYER_PCT ,ROUND_TO_PALLET_PCT ,MIN_ORDER_QTY ,MAX_ORDER_QTY ,PACKING_METHOD ,PRIMARY_SUPP_IND ,PRIMARY_COUNTRY_IND ,DEFAULT_UOP ,TI ,HI ,SUPP_HIER_LVL_1 ,SUPP_HIER_LVL_2 ,SUPP_HIER_LVL_3 ,COST_UOM ,TOLERANCE_TYPE ,MAX_TOLERANCE ,MIN_TOLERANCE ))
  FROM SVC_ITEM_SUPP_COUNTRY
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_ISC;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_ISCL(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
        TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ISCL_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,ORIGIN_COUNTRY_ID ,LOC,LOC_TYPE,PRIMARY_LOC_IND,UNIT_COST ,NEGOTIATED_ITEM_COST,ROUND_LVL ,ROUND_TO_INNER_PCT ,ROUND_TO_CASE_PCT ,ROUND_TO_LAYER_PCT ,ROUND_TO_PALLET_PCT ,SUPP_HIER_LVL_1 ,SUPP_HIER_LVL_2 ,SUPP_HIER_LVL_3 ,PICKUP_LEAD_TIME ))
  FROM SVC_ITEM_SUPP_COUNTRY_LOC
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_ISCL;
--------------------------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_ISCD(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ISCD_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,ORIGIN_COUNTRY ,DIM_OBJECT ,PRESENTATION_METHOD ,LENGTH ,WIDTH ,HEIGHT ,LWH_UOM ,WEIGHT ,NET_WEIGHT ,WEIGHT_UOM ,LIQUID_VOLUME ,LIQUID_VOLUME_UOM ,STAT_CUBE ,TARE_WEIGHT ,TARE_TYPE ))
  FROM SVC_ITEM_SUPP_COUNTRY_DIM
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_ISCD;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_ISMC(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ISMC_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,MANU_COUNTRY_ID ,PRIMARY_MANU_CTRY_IND ))
  FROM SVC_ITEM_SUPP_MANU_COUNTRY
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_ISMC;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_ISU(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ISU_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM ,SUPPLIER ,UOM ,VALUE ))
  FROM SVC_ITEM_SUPP_UOM
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_ISU;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_IXD(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = IXD_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM_XFORM_DETAIL_ID ,ITEM_XFORM_HEAD_ID ,DETAIL_ITEM ,ITEM_QUANTITY_PCT ,YIELD_FROM_HEAD_ITEM_PCT ))
  FROM SVC_ITEM_XFORM_DETAIL
  WHERE HEAD_ITEM IN
    (SELECT HEAD_ITEM
    FROM svc_item_search_temp,
      svc_item_xform_head
    WHERE svc_item_search_temp.process_id = I_process_id
    AND item                  =head_item
    );
END populate_svc_IXD;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_IXH(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = IXH_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,ITEM_XFORM_HEAD_ID ,HEAD_ITEM ,ITEM_XFORM_TYPE ,ITEM_XFORM_DESC ,PRODUCTION_LOSS_PCT ,COMMENTS_DESC ))
  FROM SVC_ITEM_XFORM_HEAD
  WHERE head_item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_IXH;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_IXH_TL(I_file_id      IN   NUMBER,
                              I_process_id   IN   NUMBER)
IS

BEGIN
   insert into table (select ss.s9t_rows
                        from s9t_folder sf,
                             table(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = IXHTL_sheet)
              select s9t_row(s9t_cells('MOD',
                                       lang,
                                       head_item,
                                       item_xform_head_id,
                                       item_xform_desc)
                            )
                from svc_item_xform_head_tl
               where head_item in (select item
                                     from svc_item_search_temp
                                    where process_id = I_process_id);

END POPULATE_SVC_IXH_TL;
---------------------------------------------------------------------------------------
PROCEDURE populate_svc_PI(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = PI_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,PACK_NO ,SEQ_NO ,ITEM ,ITEM_PARENT ,PACK_TMPL_ID ,PACK_QTY ))
  FROM SVC_PACKITEM
  WHERE pack_no IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_PI;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_IZP(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = IZP_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,izp.ITEM_ZONE_PRICE_ID ,izp.ITEM ,rz.ZONE_DISPLAY_ID ,izp.STANDARD_RETAIL ,izp.STANDARD_RETAIL_CURRENCY ,izp.STANDARD_UOM ,izp.SELLING_RETAIL ,izp.SELLING_UOM ,izp.MULTI_UNITS ,izp.MULTI_UNIT_RETAIL ,izp.MULTI_UNIT_RETAIL_CURRENCY ,izp.MULTI_SELLING_UOM ,izp.LOCK_VERSION ))
  FROM SVC_RPM_ITEM_ZONE_PRICE izp,
    rpm_zone rz
  WHERE izp.zone_id = rz.zone_id
  AND item     IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_IZP;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_UID(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = UID_sheet
    )
  SELECT s9t_row(s9t_cells( null ,ITEM ,UDA_ID ,UDA_DATE ))
  FROM SVC_UDA_ITEM_DATE
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_UID;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_UIF(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = UIF_sheet
    )
  SELECT s9t_row(s9t_cells( null ,ITEM ,UDA_ID ,UDA_TEXT ))
  FROM SVC_UDA_ITEM_FF
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_UIF;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_UIL(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = UIL_sheet
    )
  SELECT s9t_row(s9t_cells( null ,ITEM ,UDA_ID ,UDA_VALUE ))
  FROM SVC_UDA_ITEM_LOV
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_UIL;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_VI(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = VI_sheet
    )
  SELECT s9t_row(s9t_cells( null ,ITEM ,VAT_REGION ,ACTIVE_DATE ,VAT_TYPE ,VAT_CODE ,REVERSE_VAT_IND))
  FROM SVC_VAT_ITEM
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_VI;
-------------------------------------------------------------------------------------------------------
PROCEDURE populate_svc_csh(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = CSH_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,COST_CHANGE, COST_CHANGE_DESC, REASON, ACTIVE_DATE, STATUS, COST_CHANGE_ORIGIN, APPROVAL_DATE, APPROVAL_ID))
  FROM SVC_COST_SUSP_SUP_HEAD
  WHERE cost_change IN
    (SELECT cost_change
    FROM SVC_COST_CHG_SEARCH_TEMP
    WHERE process_id = I_process_id
    );
END populate_svc_csh;
---------------------------------------------------------------------------------------
PROCEDURE populate_svc_csd(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = CSD_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,COST_CHANGE, SUPPLIER, ORIGIN_COUNTRY_ID, ITEM, BRACKET_VALUE1, BRACKET_UOM1, BRACKET_VALUE2, UNIT_COST, COST_CHANGE_TYPE, COST_CHANGE_VALUE, RECALC_ORD_IND, DEFAULT_BRACKET_IND, DEPT, SUP_DEPT_SEQ_NO, DELIVERY_COUNTRY_ID ))
  FROM SVC_COST_SUSP_SUP_DETAIL
  WHERE cost_change IN
    (SELECT cost_change
    FROM SVC_COST_CHG_SEARCH_TEMP
    WHERE process_id = I_process_id
    );
END populate_svc_csd;
---------------------------------------------------------------------------------------
PROCEDURE populate_svc_csdl(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = CSDL_sheet
    )
  SELECT s9t_row(s9t_cells( 'MOD' ,COST_CHANGE, SUPPLIER, ORIGIN_COUNTRY_ID, ITEM, LOC_TYPE, LOC, BRACKET_VALUE1, BRACKET_UOM1, BRACKET_VALUE2, UNIT_COST, COST_CHANGE_TYPE, COST_CHANGE_VALUE, RECALC_ORD_IND, DEFAULT_BRACKET_IND, DEPT, SUP_DEPT_SEQ_NO, DELIVERY_COUNTRY_ID ))
  FROM SVC_COST_SUSP_SUP_DETAIL_LOC
  WHERE cost_change IN
    (SELECT cost_change
    FROM SVC_COST_CHG_SEARCH_TEMP
    WHERE process_id = I_process_id
    );
END populate_svc_csdl;
---------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_IIM(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER)
IS

BEGIN
   insert into table (select ss.s9t_rows
                        from s9t_folder sf,
                             table(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = IIM_sheet)
              select s9t_row(s9t_cells('MOD',
                                       item,
                                       image_name,
                                       image_addr,
                                       image_desc,
                                       image_type,
                                       primary_ind,
                                       display_priority)
                            )
                from svc_item_image
               where item in (select item
                                from svc_item_search_temp
                               where process_id = I_process_id);

END POPULATE_SVC_IIM;
---------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_IIM_TL(I_file_id      IN   NUMBER,
                              I_process_id   IN   NUMBER)
IS

BEGIN
   insert into table (select ss.s9t_rows
                        from s9t_folder sf,
                             table(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = IIMTL_sheet)
              select s9t_row(s9t_cells('MOD',
                                       lang,
                                       item,
                                       image_name,
                                       image_desc)
                            )
                from svc_item_image_tl
               where item in (select item
                                from svc_item_search_temp
                               where process_id = I_process_id);

END POPULATE_SVC_IIM_TL;
---------------------------------------------------------------------------------------
PROCEDURE populate_svc_ISE(
    I_file_id    IN NUMBER,
    I_process_id IN NUMBER )
IS
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = ISE_sheet
    )
  SELECT s9t_row(s9t_cells( NULL ,ITEM ,SEASON_ID ,PHASE_ID ,ITEM_SEASON_SEQ_NO ,DIFF_ID ))
  FROM SVC_ITEM_SEASONS
  WHERE item IN
    (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
    );
END populate_svc_ISE;
---------------------------------------------------------------------------------------
FUNCTION create_svc_s9t(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    O_file_id       IN OUT s9t_folder.file_id%type,
    I_process_id    IN NUMBER)
  RETURN BOOLEAN
IS
  l_file s9t_file;
  L_program VARCHAR2(255):='ITEM_INDUCT_SQL.CREATE_SVC_S9T';
BEGIN
  init_s9t(O_file_id);

  IF is_cost_change_template(template_key) THEN
    populate_svc_csh(O_file_id,I_process_id);
    populate_svc_csd(O_file_id,I_process_id);
    populate_svc_csdl(O_file_id,I_process_id);
    if CORESVC_CFLEX.COST_CHANGE_SVC2S9T(O_error_message,
                                         O_file_id,
                                         I_process_id,
                                         template_key)= FALSE THEN
       return FALSE;
    end if;
  ELSE
    populate_svc_ICD(O_file_id,I_process_id);
    populate_svc_ICH(O_file_id,I_process_id);
    populate_svc_IC(O_file_id,I_process_id);
    populate_svc_IM(O_file_id,I_process_id);
    POPULATE_SVC_IM_TL(O_file_id,
                       I_process_id);
    populate_svc_IS(O_file_id,I_process_id);
    POPULATE_SVC_IS_TL(O_file_id,
                       I_process_id);
    populate_svc_ISC(O_file_id,I_process_id);
    populate_svc_ISCL(O_file_id,I_process_id);
    populate_svc_ISCD(O_file_id,I_process_id);
    populate_svc_ISMC(O_file_id,I_process_id);
    populate_svc_ISU(O_file_id,I_process_id);
    populate_svc_IXD(O_file_id,I_process_id);
    populate_svc_IXH(O_file_id,I_process_id);
    POPULATE_SVC_IXH_TL(O_file_id,
                        I_process_id);
    populate_svc_PI(O_file_id,I_process_id);
    populate_svc_IZP(O_file_id,I_process_id);
    populate_svc_UID(O_file_id,I_process_id);
    populate_svc_UIF(O_file_id,I_process_id);
    populate_svc_UIL(O_file_id,I_process_id);
    populate_svc_VI(O_file_id,I_process_id);
    POPULATE_SVC_IIM(O_file_id,
                     I_process_id);
    POPULATE_SVC_IIM_TL(O_file_id,
                        I_process_id);
    populate_svc_ISE(O_file_id,I_process_id);
    IF CORESVC_CFLEX.ITEM_SVC2S9T(O_error_message,
                  O_file_id,
                  I_process_id,
                  template_key)= FALSE THEN
       RETURN false;
    END IF;
  END IF;
  commit;
  IF item_induct_sql.populate_lists(O_error_message,O_file_id,template_key)=false THEN
    RETURN false;
  END IF;
  commit;
  s9t_pkg.translate_to_user_lang(O_file_id);
  -- Apply template
  s9t_pkg.apply_template(O_file_id,template_key);
  IF add_error_4_process IS NOT NULL THEN
    IF add_error_sheet(O_error_message,O_file_id,add_error_4_process)=false THEN
      RETURN false;
    END IF;
  END IF;
  l_file:=s9t_file(O_file_id);
  if s9t_pkg.code2desc(O_error_message ,
                       'IS9T',
                       l_file)=FALSE then
     return FALSE;
  end if;
  s9t_pkg.save_obj(l_file);
  s9t_pkg.update_ods(l_file);
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END create_svc_s9t;
FUNCTION apply_tmpl_defaults(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_file_id       IN NUMBER,
    I_process_id    IN NUMBER)
  RETURN BOOLEAN
IS
  L_program VARCHAR2( 255):='ITEM_INDUCT_SQL.APPLY_TMPL_DEFAULTS';
BEGIN
  /******************************************************************************/
  -- Start Processing for ITEM_COUNTRY
  /******************************************************************************/
  -- Apply default values to new records.
  DECLARE
    l_rec svc_item_country%rowtype;
  BEGIN
    FOR rec IN
    (SELECT item_dv,
      country_id_dv
    FROM
      (SELECT column_key,
    default_value
      FROM s9t_tmpl_cols_def
      WHERE template_key                  = ITEM_INDUCT_SQL.template_key
      AND wksht_key                   = 'ITEM_COUNTRY'
      ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('ITEM' AS item, 'COUNTRY_ID' AS country_id))
    )
    LOOP
      BEGIN
    l_rec.item := rec.item_dv;
      EXCEPTION
      WHEN OTHERS THEN
    write_s9t_error(I_file_id,'ITEM_COUNTRY',NULL,'ITEM',SQLCODE,SQLERRM);
      END;
      BEGIN
    l_rec.country_id := rec.country_id_dv;
      EXCEPTION
      WHEN OTHERS THEN
    write_s9t_error(I_file_id,'ITEM_COUNTRY',NULL,'COUNTRY_ID',SQLCODE,SQLERRM);
      END;
      UPDATE svc_item_country
      SET item = (
    CASE
      WHEN item  IS NULL
      AND l_rec.item IS NOT NULL
      THEN l_rec.item
      ELSE item
    END),
    country_id = (
    CASE
      WHEN country_id      IS NULL
      AND l_rec.country_id IS NOT NULL
      THEN l_rec.country_id
      ELSE country_id
    END)
      WHERE process_id = I_process_id;
    END LOOP;
  END;
  -- Restore existing values for MOD records for columns that
  -- were not part of the template.
  DECLARE
    CURSOR c1
    IS
    WITH mi_vals AS
      (SELECT item_mi,
    country_id_mi
      FROM
    (SELECT column_key,
      mandatory
    FROM s9t_tmpl_cols_def
    WHERE template_key              = ITEM_INDUCT_SQL.template_key
    AND wksht_key                   = 'ITEM_COUNTRY'
    ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ('ITEM' AS item, 'COUNTRY_ID' AS country_id))
      )
    SELECT st.rowid AS rid,
      (
      CASE
    WHEN mv.item_mi='N'
    AND st.item   IS NULL
    AND mt.item   IS NOT NULL
    THEN mt.item
    ELSE st.item
      END) AS item,
      (
      CASE
    WHEN mv.country_id_mi='N'
    AND st.country_id   IS NULL
    AND mt.country_id   IS NOT NULL
    THEN mt.country_id
    ELSE st.country_id
      END) AS country_id
    FROM svc_item_country st,
      item_country mt,
      mi_vals mv
    WHERE st.item           = mt.item
    AND st.country_id           = mt.country_id
    AND st.action           = 'MOD'
    AND st.process_id           = I_process_id
    AND least(item_mi,country_id_mi)='N';
  Type c1_rec_tab_typ
IS
  TABLE OF c1%rowtype;
  c1_rec_tab c1_rec_tab_typ;
BEGIN
  OPEN c1;
  FETCH c1 bulk collect INTO c1_rec_tab;
  CLOSE c1;
  forall i IN 1..c1_rec_tab.count()
  UPDATE svc_item_country
  SET item        = c1_rec_tab(i).item ,
    country_id        = c1_rec_tab(i).country_id,
    last_upd_id       = get_user,
    last_upd_datetime = sysdate
  WHERE rowid         = c1_rec_tab(i).rid;
END;
-- Delete records from old process-IDs.
DECLARE
  CURSOR c1
  IS
    SELECT item,country_id FROM svc_item_country WHERE process_id = I_process_id;
Type c1_rec_tab_typ
IS
  TABLE OF c1%rowtype;
  c1_rec_tab c1_rec_tab_typ;
BEGIN
  OPEN c1;
  FETCH c1 bulk collect INTO c1_rec_tab;
  CLOSE c1;
  forall i IN 1..c1_rec_tab.count()
  DELETE
  FROM svc_item_country
  WHERE item      = c1_rec_tab(i).item
  AND country_id  = c1_rec_tab(i).country_id
  AND process_id <> I_process_id;
END;
/******************************************************************************/
-- End Processing for ITEM_COUNTRY
/******************************************************************************/
RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END apply_tmpl_defaults;
FUNCTION search_items(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_sch_crit_rec  IN item_induct_sch_crit_typ,
    I_source        IN VARCHAR2,
    I_process_id    IN NUMBER,
    O_item_count OUT NUMBER)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75):= 'ITEM_INDUCT_SQL.SEARCH_ITEMS';
BEGIN
   IF I_source = 'RMS' THEN
      IF (I_sch_crit_rec.vpn IS NULL AND I_sch_crit_rec.item_type IS NULL AND I_sch_crit_rec.sup_name IS NULL
          AND I_sch_crit_rec.supplier_site is null) THEN
         INSERT INTO svc_item_search_temp(process_id, item)
         SELECT I_process_id, im.item
           FROM v_item_master im
          WHERE (I_sch_crit_rec.item IS NULL
                 OR im.item                  = I_sch_crit_rec.item )
            AND ( I_sch_crit_rec.item_desc IS NULL
                 OR UPPER(im.item_desc) LIKE UPPER('%'||I_sch_crit_rec.item_desc||'%'))
            AND ( I_sch_crit_rec.division IS NULL
                 OR im.division = I_sch_crit_rec.division)
            AND ( I_sch_crit_rec.group_no IS NULL
                 OR im.group_no = I_sch_crit_rec.group_no)
            AND ( I_sch_crit_rec.dept IS NULL
                 OR im.dept = I_sch_crit_rec.dept)
            AND ( I_sch_crit_rec.class IS NULL
                 OR im.class = I_sch_crit_rec.class)
            AND ( I_sch_crit_rec.subclass IS NULL
                 OR im.subclass = I_sch_crit_rec.subclass)
          AND ( I_sch_crit_rec.status IS NULL
                 OR im.status = I_sch_crit_rec.status)
            AND ( I_sch_crit_rec.brand_name IS NULL
                 OR im.brand_name = I_sch_crit_rec.brand_name)
            AND ( I_sch_crit_rec.item_level IS NULL
                 OR im.item_level = I_sch_crit_rec.item_level)
            AND ( I_sch_crit_rec.tran_level IS NULL
                 OR im.tran_level = I_sch_crit_rec.tran_level)
          AND ( I_sch_crit_rec.uda_id is null
                  OR im.item IN (SELECT item
                                   FROM uda_item_lov
                                  WHERE uda_id=I_sch_crit_rec.uda_id))
            AND ( I_sch_crit_rec.uda_value is null
                  OR im.item IN (SELECT item
                                   FROM uda_item_lov
                                  WHERE uda_value=I_sch_crit_rec.uda_value))
            AND ( I_sch_crit_rec.skulist is null
                  OR im.item IN (SELECT item
                                   FROM SKULIST_DETAIL
                                  WHERE skulist=I_sch_crit_rec.skulist));
      ELSE
         INSERT INTO svc_item_search_temp(process_id, item)
         SELECT I_process_id, im.item
           FROM  V_IINDUCT_SEARCH_RMS im
          WHERE ( I_sch_crit_rec.item IS NULL
                 OR im.item                  = I_sch_crit_rec.item )
            AND ( I_sch_crit_rec.item_desc IS NULL
                 OR UPPER(im.item_desc) LIKE UPPER('%'||I_sch_crit_rec.item_desc||'%'))
            AND ( I_sch_crit_rec.division IS NULL
                 OR im.division = I_sch_crit_rec.division)
            AND ( I_sch_crit_rec.group_no IS NULL
                 OR im.group_no = I_sch_crit_rec.group_no)
            AND ( I_sch_crit_rec.dept IS NULL
                 OR im.dept = I_sch_crit_rec.dept)
            AND ( I_sch_crit_rec.class IS NULL
                 OR im.class = I_sch_crit_rec.class)
            AND ( I_sch_crit_rec.subclass IS NULL
                 OR im.subclass = I_sch_crit_rec.subclass)
            AND ( I_sch_crit_rec.vpn IS NULL
                 OR im.vpn = I_sch_crit_rec.vpn)
            AND ( I_sch_crit_rec.status IS NULL
                 OR im.status = I_sch_crit_rec.status)
            AND ( I_sch_crit_rec.item_type IS NULL
                 OR im.item_type = I_sch_crit_rec.item_type)
            AND ( I_sch_crit_rec.brand_name IS NULL
                 OR im.brand_name = I_sch_crit_rec.brand_name)
            AND ( I_sch_crit_rec.item_level IS NULL
                 OR im.item_level = I_sch_crit_rec.item_level)
            AND ( I_sch_crit_rec.tran_level IS NULL
                 OR im.tran_level = I_sch_crit_rec.tran_level)
            AND ( I_sch_crit_rec.sup_name IS NULL
                 OR UPPER(im.supplier_name) LIKE UPPER('%'||I_sch_crit_rec.sup_name||'%'))
            AND ( I_sch_crit_rec.supplier_site is null
                 OR EXISTS(SELECT 'X'
                             FROM item_supplier
                            WHERE supplier=I_sch_crit_rec.supplier_site
                              AND im.item = item
                              AND im.supplier=supplier
                              AND rownum=1))
            AND ( I_sch_crit_rec.uda_id is null
                 OR EXISTS(SELECT 'X'
                             FROM uda_item_lov
                            WHERE uda_id=I_sch_crit_rec.uda_id
                              AND im.item = item
                              AND rownum=1))
            AND ( I_sch_crit_rec.uda_value is null
                 OR EXISTS(SELECT 'X'
                             FROM uda_item_lov
                            WHERE uda_value=I_sch_crit_rec.uda_value
                              AND im.item = item
                              AND rownum=1))
            AND ( I_sch_crit_rec.skulist is null
                 OR EXISTS(SELECT 'X'
                             FROM SKULIST_DETAIL
                            WHERE skulist=I_sch_crit_rec.skulist
                              AND im.item = item
                              AND rownum=1));
      END IF;
      O_item_count                                                                             := Sql%rowcount;
   END IF;

   IF I_source = 'STG' THEN
      INSERT INTO svc_item_search_temp(process_id,
                                       item)

            SELECT DISTINCT I_process_id,
                           im.item
              FROM  V_IINDUCT_SEARCH_STG im
             WHERE ( I_sch_crit_rec.item IS NULL
                     OR im.item                  = I_sch_crit_rec.item )
               AND ( I_sch_crit_rec.item_desc IS NULL
                     OR UPPER(im.item_desc) LIKE UPPER('%'||I_sch_crit_rec.item_desc||'%'))
               AND ( I_sch_crit_rec.division IS NULL
                     OR im.division = I_sch_crit_rec.division)
               AND ( I_sch_crit_rec.group_no IS NULL
                     OR im.group_no = I_sch_crit_rec.group_no)
               AND ( I_sch_crit_rec.dept IS NULL
                     OR im.dept = I_sch_crit_rec.dept)
               AND ( I_sch_crit_rec.class IS NULL
                     OR im.class = I_sch_crit_rec.class)
               AND ( I_sch_crit_rec.subclass IS NULL
                     OR im.subclass = I_sch_crit_rec.subclass)
               AND ( I_sch_crit_rec.vpn IS NULL
                     OR im.vpn = I_sch_crit_rec.vpn)
               AND ( I_sch_crit_rec.status IS NULL
                     OR im.status = I_sch_crit_rec.status)
               AND ( I_sch_crit_rec.item_type IS NULL
                     OR im.item_type = I_sch_crit_rec.item_type)
               AND ( I_sch_crit_rec.brand_name IS NULL
                     OR im.brand_name = I_sch_crit_rec.brand_name)
               AND ( I_sch_crit_rec.item_level IS NULL
                     OR im.item_level = I_sch_crit_rec.item_level)
               AND ( I_sch_crit_rec.tran_level IS NULL
                     OR im.tran_level = I_sch_crit_rec.tran_level)
               AND ( I_sch_crit_rec.next_upd_id IS NULL
                     OR im.next_upd_id = I_sch_crit_rec.next_upd_id)
               AND ( I_sch_crit_rec.sup_name IS NULL
                     OR UPPER(im.supplier_name) LIKE UPPER('%'||I_sch_crit_rec.sup_name||'%'))
               AND ( I_sch_crit_rec.supplier_site is null
                     OR EXISTS(SELECT 'X'
                                 FROM svc_item_supplier
                                WHERE supplier=I_sch_crit_rec.supplier_site
                                  AND im.item = item
                                  AND im.supplier=supplier
                                  AND rownum=1))
               AND ( I_sch_crit_rec.uda_id is null
                     OR EXISTS(SELECT 'X'
                                 FROM svc_uda_item_lov
                                WHERE uda_id=I_sch_crit_rec.uda_id
                                  AND im.item = item
                                  AND rownum=1))
               AND ( I_sch_crit_rec.uda_value is null
                     OR EXISTS(SELECT 'X'
                                 FROM svc_uda_item_lov
                                WHERE uda_value=I_sch_crit_rec.uda_value
                                  AND im.item = item
                                  AND rownum=1))
               AND ( I_sch_crit_rec.process_id is null
                     OR EXISTS(SELECT 'X'
                                 FROM V_SVC_ITEM_EXIST
                                WHERE process_id=I_sch_crit_rec.process_id
                                  AND im.item = item
                                  AND rownum=1))
               AND ( I_sch_crit_rec.last_upd_id is null
                     OR EXISTS(SELECT 'X'
                                 FROM V_SVC_ITEM_EXIST
                                WHERE last_upd_id=I_sch_crit_rec.last_upd_id
                                  AND im.item = item
                                  AND rownum=1))
               AND (trunc(I_sch_crit_rec.created_date) IS NULL
                      OR EXISTS( SELECT 'X'
                                   FROM V_SVC_ITEM_EXIST
                                  WHERE trunc(im.create_datetime) = trunc(I_sch_crit_rec.created_date)
                                    AND im.item = item
                                    AND rownum=1))
               AND (trunc(I_sch_crit_rec.from_date)     IS NULL
                      OR EXISTS( SELECT 'X'
                                   FROM V_SVC_ITEM_EXIST
                                  WHERE trunc(im.create_datetime) >= trunc(I_sch_crit_rec.from_date)
                                    AND im.item = item
                                    AND rownum=1))
               AND (trunc(I_sch_crit_rec.to_date)       IS NULL
                      OR EXISTS( SELECT 'X'
                                   FROM V_SVC_ITEM_EXIST
                                  WHERE trunc(im.create_datetime) <= trunc(I_sch_crit_rec.to_date)
                                    AND im.item = item
                                    AND rownum=1));


     O_item_count := SQL%ROWCOUNT;
  END IF;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END search_items;
FUNCTION get_item_type(
    I_sellable_ind   IN VARCHAR2,
    I_orderable_ind  IN VARCHAR2,
    I_inventory_ind  IN VARCHAR2,
    I_simple_pack_ind    IN VARCHAR2,
    I_pack_ind       IN VARCHAR2,
    I_dept_purchase_type IN NUMBER,
    I_deposit_item_type  IN VARCHAR2,
    I_item_xform_ind     IN VARCHAR2 )
  RETURN VARCHAR2
IS
BEGIN
  IF I_simple_pack_ind = 'Y' THEN
    RETURN 'S';
  END IF;
  IF I_pack_ind = 'Y' AND I_simple_pack_ind <> 'Y' THEN
    RETURN 'C';
  END IF;
  IF I_deposit_item_type IS NOT NULL THEN
    RETURN I_deposit_item_type;
  END IF;
  IF I_item_xform_ind = 'Y' THEN
    IF I_orderable_ind = 'Y' THEN
      RETURN 'O';
    ELSE
      RETURN 'L';
    END IF;
  END IF;
  IF I_dept_purchase_type IN (1,2) THEN
    RETURN 'I';
  END IF;
  RETURN 'R';
END get_item_type;
FUNCTION init_process(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_process_id    IN svc_process_tracker.process_id%type,
    I_process_desc  IN svc_process_tracker.process_desc%type,
    I_template_key  IN svc_process_tracker.template_key%type,
    I_action_type   IN svc_process_tracker.action_type%type,
    I_source        IN svc_process_tracker.process_source%type,
    I_destination   IN svc_process_tracker.process_destination%type,
    I_rms_async_id  IN svc_process_tracker.rms_async_id%type,
    I_file_id       IN svc_process_tracker.file_id%type,
    I_file_path     IN svc_process_tracker.file_path%type)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75):= 'ITEM_INDUCT_SQL.INIT_PROCESS';
BEGIN
  INSERT
  INTO svc_process_tracker
    (
      process_id,
      process_desc,
      template_key,
      action_type,
      process_source,
      process_destination,
      status,
      rms_async_id,
      file_id,
      file_path,
      user_id
    )
    VALUES
    (
      I_process_id,
      I_process_desc,
      I_template_key,
      I_action_type,
      I_source,
      I_destination,
      'N',
      I_rms_async_id,
      I_file_id,
      I_file_path,
      get_user
    );
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END init_process;
FUNCTION create_file
  (
    O_error_message IN OUT rtk_errors.rtk_text%type,
    O_file_id OUT svc_process_tracker.file_id%type,
    I_process_id    IN svc_process_tracker.process_id%type,
    I_action        IN VARCHAR2,
    I_source        IN VARCHAR2,
    I_template      IN VARCHAR2,
    I_file_path     IN VARCHAR2,
    I_err_4_process IN svc_process_tracker.process_id%type DEFAULT NULL
  )
  RETURN BOOLEAN
IS
  L_program       VARCHAR2(75):= 'ITEM_INDUCT_SQL.CREATE_FILE';
  L_template_only_ind CHAR    :='N';
BEGIN
  IF I_action        = 'DT' THEN
    L_template_only_ind := 'Y';
  END IF;
  ITEM_INDUCT_SQL.template_key                                  := I_template;
  ITEM_INDUCT_SQL.file_name                                 := s9t_pkg.basename(I_file_path);
  ITEM_INDUCT_SQL.add_error_4_process                               := I_err_4_process;
  IF ((I_action                                          = 'D' AND I_source = 'RMS') OR I_action = 'DT')THEN
    IF ITEM_INDUCT_SQL.create_s9t(O_error_message,O_file_id,I_process_id,L_template_only_ind)=FALSE THEN
      RETURN false;
    END IF;
  END IF;
  IF I_source                                    = 'STG' THEN
    IF ITEM_INDUCT_SQL.create_svc_s9t(O_error_message,O_file_id,I_process_id)=FALSE THEN
      RETURN false;
    END IF;
  END IF;
  IF item_induct_sql.delete_search_items(O_error_message,I_process_id)=false THEN
    RETURN false;
  END IF;
  IF item_induct_sql.delete_search_ccs(O_error_message,I_process_id)=false THEN
    RETURN false;
  END IF;
  UPDATE svc_process_tracker
  SET file_id      = O_file_id,
    status     = 'PS'
  WHERE process_id = I_process_id;
  -- Apply template
  --s9t_pkg.apply_template(O_file_id,I_template);
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END create_file;
FUNCTION delete_search_items(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_process_id    IN NUMBER)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75):= 'ITEM_INDUCT_SQL.DELETE_SEARCH_ITEMS';
BEGIN
  DELETE FROM svc_item_search_temp WHERE process_id = I_process_id;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END delete_search_items;
FUNCTION populate_lists(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_file_id       IN NUMBER,
    I_template_key  IN VARCHAR2 DEFAULT NULL)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75) := 'ITEM_INDUCT_SQL.POPULATE_LISTS';
  l_s9t_action s9t_cells := NEW s9t_cells(CORESVC_ITEM.action_new,CORESVC_ITEM.action_mod,CORESVC_ITEM.action_del);
BEGIN
  if I_template_key is not null then
     if S9T_PKG.POPULATE_LISTS(O_error_message,
                               I_file_id,
                               'IS9T',
                               I_template_key)=FALSE then
        return FALSE;
     end if;
  else
     if S9T_PKG.POPULATE_LISTS(O_error_message,
                               I_file_id,
                               'IS9T')=FALSE then
        return FALSE;
     end if;
  end if;
  /*FOR rec IN
  (SELECT wksht_key
  FROM s9t_tmpl_wksht_def
  WHERE template_key = ITEM_INDUCT_SQL.template_key
  )
  LOOP
  s9t_pkg.add_list_validation(I_file_id,rec.wksht_key,action_column,l_s9t_action);
  END LOOP;*/
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END populate_lists;
FUNCTION get_process_desc(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_process_id    IN svc_process_tracker.process_id%type,
    O_process_desc OUT svc_process_tracker.process_desc%type,
    O_found OUT BOOLEAN)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75) := 'ITEM_INDUCT_SQL.get_process_desc';
  CURSOR c_validate_process
  IS
    SELECT process_desc FROM svc_process_tracker WHERE process_id = I_process_id;
BEGIN
  OPEN c_validate_process;
  FETCH c_validate_process INTO O_process_desc;
  IF c_validate_process%notfound THEN
    O_found := false;
  ELSE
    O_found := true;
  END IF;
  CLOSE c_validate_process;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END get_process_desc;

FUNCTION delete_processes(O_error_message  IN OUT   rtk_errors.rtk_text%type,
                          I_process_id_tab IN       num_tab)
  RETURN BOOLEAN IS
  L_program VARCHAR2(75) := 'ITEM_INDUCT_SQL.delete_processes';
  L_file_id  SVC_PROCESS_TRACKER.file_id%type;
  RECORD_LOCKED   EXCEPTION;
  PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
  L_table         VARCHAR2(50);

BEGIN

  L_table := 'SVC_PROCESS_TRACKER';
  for i IN 1..I_process_id_tab.count LOOP
     logger.log('Locking the row on SVC_PROCESS_TRACKER for delete'||I_process_id_tab(i));
     select file_id
       into L_file_id
       from SVC_PROCESS_TRACKER
      where process_id = I_process_id_tab(i)
     FOR UPDATE NOWAIT;

     DELETE FROM S9T_ERRORS WHERE file_id = L_file_id;
     DELETE FROM S9T_FOLDER WHERE file_id = L_file_id;

  END LOOP;

  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_PROCESS_ITEMS WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_COST_DETAIL WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_COST_HEAD WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_COUNTRY WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_COUNTRY_L10N_EXT WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_MASTER WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_MASTER_TL WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_MASTER_CFA_EXT WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_SUPPLIER WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_SUPPLIER_TL WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_SUPPLIER_CFA_EXT WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_SUPP_COUNTRY WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE
  FROM SVC_ITEM_SUPP_COUNTRY_CFA_EXT
  WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_SUPP_COUNTRY_DIM WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_SUPP_MANU_COUNTRY WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_SUPP_UOM WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_XFORM_DETAIL WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_XFORM_HEAD WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_XFORM_HEAD_TL WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_PACKITEM WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_RPM_ITEM_ZONE_PRICE WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM svc_xitem_rizp_locs WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM svc_xitem_rizp WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_SEASONS WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_UDA_ITEM_DATE WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_UDA_ITEM_FF WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_UDA_ITEM_LOV WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_VAT_ITEM WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_IMAGE WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_ITEM_IMAGE_TL WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM CORESVC_ITEM_ERR WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_COST_SUSP_SUP_HEAD WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE
  FROM SVC_COST_SUSP_SUP_DETAIL_LOC
  WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_COST_SUSP_SUP_DETAIL WHERE process_id = I_process_id_tab(i);
  forall i in 1..I_process_id_tab.count
  DELETE FROM SVC_CFA_EXT WHERE process_id = I_process_id_tab(i);


  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_PROCESS_CHUNKS WHERE process_id = I_process_id_tab(i);
  forall i IN 1..I_process_id_tab.count
  DELETE FROM SVC_PROCESS_TRACKER WHERE process_id = I_process_id_tab(i);
  --forall i IN 1..I_process_id_tab.count

  RETURN TRUE;
EXCEPTION
WHEN RECORD_LOCKED THEN
  ROLLBACK;
  O_error_message := SQL_LIB.CREATE_MSG('RECORDS_LOCKED', L_table);
  RETURN FALSE;

WHEN OTHERS THEN
  ROLLBACK;
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);

  RETURN FALSE;
END delete_processes;

FUNCTION process_items(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_process_id    IN svc_process_tracker.process_id%type)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75) := 'ITEM_INDUCT_SQL.process_items';
BEGIN
  merge INTO SVC_ITEM_COST_DETAIL st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_COST_DETAIL s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_ITEM_COST_HEAD st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_COST_HEAD s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_ITEM_COUNTRY st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_COUNTRY s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_ITEM_COUNTRY_L10N_EXT st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_COUNTRY_L10N_EXT s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_ITEM_MASTER st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_MASTER s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge into svc_item_master_tl st
  using (select t.process_id,
                s.rowid as rid,
                rownum  as row_seq
           from svc_item_master_tl s,
                svc_item_search_temp t
          where t.process_id = I_process_id
            and t.item = s.item) sq
     on (st.rowid = sq.rid)
  when matched then
     update set process_id     = sq.process_id,
                process$status = DECODE(process$status, 'E', 'N', process$status),
                row_seq        = sq.row_seq;
  --
  merge INTO SVC_ITEM_MASTER_CFA_EXT st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_MASTER_CFA_EXT s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_ITEM_SUPPLIER st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_SUPPLIER s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge into svc_item_supplier_tl st
  using (select t.process_id,
                s.rowid as rid,
                rownum  as row_seq
           from svc_item_supplier_tl s,
                svc_item_search_temp t
          where t.process_id = I_process_id
            and t.item = s.item) sq
     on (st.rowid = sq.rid)
  when matched then
     update set process_id     = sq.process_id,
                process$status = DECODE(process$status, 'E', 'N', process$status),
                row_seq        = sq.row_seq;
  --
  merge INTO SVC_ITEM_SUPPLIER_CFA_EXT st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_SUPPLIER_CFA_EXT s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_ITEM_SUPP_COUNTRY st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_SUPP_COUNTRY s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
    merge INTO SVC_ITEM_SUPP_COUNTRY_LOC st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_SUPP_COUNTRY_LOC s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq       = sq.row_seq;
    ---
  merge INTO SVC_ITEM_SUPP_COUNTRY_CFA_EXT st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_SUPP_COUNTRY_CFA_EXT s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_ITEM_SUPP_COUNTRY_DIM st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_SUPP_COUNTRY_DIM s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_ITEM_SUPP_MANU_COUNTRY st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_SUPP_MANU_COUNTRY s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_ITEM_SUPP_UOM st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_SUPP_UOM s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_ITEM_XFORM_DETAIL st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_XFORM_DETAIL s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.head_item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_ITEM_XFORM_HEAD st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_XFORM_HEAD s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.head_item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge into svc_item_xform_head_tl st
  using (select t.process_id,
                s.rowid as rid,
                rownum  as row_seq
           from svc_item_xform_head_tl s,
                svc_item_search_temp t
          where t.process_id = I_process_id
            and t.item = s.head_item) sq
     on (st.rowid = sq.rid)
  when matched then
     update set process_id     = sq.process_id,
                process$status = DECODE(process$status, 'E', 'N', process$status),
                row_seq        = sq.row_seq;
  --
  merge INTO SVC_PACKITEM st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_PACKITEM s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.pack_no
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_RPM_ITEM_ZONE_PRICE st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_RPM_ITEM_ZONE_PRICE s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_UDA_ITEM_DATE st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_UDA_ITEM_DATE s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_UDA_ITEM_FF st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_UDA_ITEM_FF s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_UDA_ITEM_LOV st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_UDA_ITEM_LOV s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_VAT_ITEM st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_VAT_ITEM s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge into svc_item_image st
  using (select t.process_id,
                s.rowid as rid,
                rownum  as row_seq
           from svc_item_image s,
                svc_item_search_temp t
          where t.process_id = I_process_id
            and t.item = s.item) sq
     on (st.rowid = sq.rid)
  when matched then
     update set process_id     = sq.process_id,
                process$status = DECODE(process$status, 'E', 'N', process$status),
                row_seq        = sq.row_seq;
  --
  merge into svc_item_image_tl st
  using (select t.process_id,
                s.rowid as rid,
                rownum  as row_seq
           from svc_item_image_tl s,
                svc_item_search_temp t
          where t.process_id = I_process_id
            and t.item = s.item) sq
     on (st.rowid = sq.rid)
  when matched then
     update set process_id     = sq.process_id,
                process$status = DECODE(process$status, 'E', 'N', process$status),
                row_seq        = sq.row_seq;
  --
  merge INTO svc_xitem_rizp st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM svc_xitem_rizp s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO svc_xitem_rizp_locs st USING
  (SELECT t.process_id,
    s.rowid AS rid
  FROM svc_xitem_rizp_locs s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id;
  --
  merge INTO SVC_ITEM_SEASONS st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_ITEM_SEASONS s,
    svc_item_search_temp t
  WHERE t.process_id = I_process_id
  AND t.item         = s.item
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  IF CORESVC_ITEM.COMPLETE_SVC_IM(O_error_message,I_process_id)=false THEN
    RETURN false;
  END IF;
  IF item_induct_sql.delete_search_items(O_error_message,I_process_id)=false THEN
    RETURN false;
  END IF;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END process_items;
-------------------------------------------------------------------------------------------------------
FUNCTION process_cost_changes(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_process_id    IN svc_process_tracker.process_id%type)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75) := 'ITEM_INDUCT_SQL.process_cost_changes';
BEGIN
  merge INTO SVC_COST_SUSP_SUP_HEAD st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_COST_SUSP_SUP_HEAD s,
    SVC_COST_CHG_SEARCH_TEMP t
  WHERE t.process_id = I_process_id
  AND t.cost_change  = s.cost_change
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge into SVC_CFA_EXT svc
     using (select temp.process_id,
                   s.rowid   AS   rid,
                   rownum    AS   row_seq
              from SVC_COST_CHG_SEARCH_TEMP temp,
                   SVC_CFA_EXT s
             where temp.process_id = I_process_id
               and to_char(temp.cost_change) = (key_val_pairs_pkg.get_attr(s.keys_col,'COST_CHANGE'))) sq
     on (svc.rowid = sq.rid)
     when matched then
        update set process_id     = sq.process_id,
                   process$status = DECODE(process$status,'E','N',process$status),
                   row_seq        = sq.row_seq;
  --
  merge INTO SVC_COST_SUSP_SUP_DETAIL st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_COST_SUSP_SUP_DETAIL s,
    SVC_COST_CHG_SEARCH_TEMP t
  WHERE t.process_id = I_process_id
  AND t.cost_change  = s.cost_change
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  merge INTO SVC_COST_SUSP_SUP_DETAIL_LOC st USING
  (SELECT t.process_id,
    s.rowid AS rid,
    rownum  AS row_seq
  FROM SVC_COST_SUSP_SUP_DETAIL_LOC s,
    SVC_COST_CHG_SEARCH_TEMP t
  WHERE t.process_id = I_process_id
  AND t.cost_change  = s.cost_change
  )sq ON (st.rowid   = sq.rid)
WHEN matched THEN
  UPDATE
  SET process_id  = sq.process_id ,
    process$status=DECODE(process$status,'E','N',process$status),
    row_seq   = sq.row_seq;
  --
  IF CORESVC_COSTCHG.COMPLETE_SVC_CSH(O_error_message,I_process_id)=false THEN
    RETURN false;
  END IF;

  IF item_induct_sql.delete_search_ccs(O_error_message,I_process_id)=false THEN
    RETURN false;
  END IF;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END process_cost_changes;
---------------------------------------------------------------------------------------
FUNCTION EXEC_ASYNC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
            I_rms_async_id    IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
   return BOOLEAN IS

   L_program VARCHAR2(75) := 'ITEM_INDUCT_SQL.EXEC_ASYNC';
   L_error_count NUMBER := 0;

   cursor C_PRC is
      select process_id,
         process_desc,
         file_id,
         template_key,
         action_type,
         process_source,
         process_destination,
         action_date,
         status,
         user_id,
         rms_async_id,
         file_path
    from svc_process_tracker
       where rms_async_id = I_rms_async_id;

   c_prc_rec C_PRC%ROWTYPE;
  --
BEGIN
  --
   open C_PRC;
   fetch C_PRC into c_prc_rec;
   close C_PRC;

   if RMS_ASYNC_PROCESS_SQL.CHECK_CONTEXT(O_error_message,
                                          c_prc_rec.file_id,
                                          c_prc_rec.user_id) = FALSE then
       return FALSE;
   end if;

   if c_prc_rec.action_type = 'D' then
      if ITEM_INDUCT_SQL.CREATE_FILE(O_error_message,
                     c_prc_rec.file_id,
                     c_prc_rec.process_id,
                     c_prc_rec.action_type,
                     c_prc_rec.process_source,
                     c_prc_rec.template_key,
                     c_prc_rec.file_path) = FALSE then
     return FALSE;
      end if;
   end if;
   --
   if c_prc_rec.action_type = 'U' and c_prc_rec.process_source = 'S9T' then
     if ITEM_INDUCT_SQL.PROCESS_S9T(O_error_message,
                    c_prc_rec.file_id,
                    c_prc_rec.process_id,
                    L_error_count) = FALSE then
       return FALSE;
     end if;
   end if;
   --
   -- XXADEO (RB121) - This function creates item/loc relation if not already exists.
   --
   if L_error_count = 0 and is_cost_change_template(c_prc_rec.template_key) and c_prc_rec.action_type = 'U'  then
     --
     if XXADEO_CREATE_COST_IL_SQL.PROCESS(O_error_message => O_error_message,
                                          I_process_id    => c_prc_rec.process_id,
                                          I_template_key  => c_prc_rec.template_key) = FALSE then
        --
        RETURN FALSE;
        --
     end if;
     --
   end if;
   --
   if c_prc_rec.action_type = 'U' and c_prc_rec.process_source in ('STG','EXT') then
      if NOT is_cost_change_template(c_prc_rec.template_key) then
         if ITEM_INDUCT_SQL.PROCESS_ITEMS(O_error_message,
                                          c_prc_rec.process_id) = FALSE then
            return FALSE;
         end if;
      end if;
      if c_prc_rec.template_key <> 'RMSSUB_XITEM' and is_cost_change_template(c_prc_rec.template_key) then
         if ITEM_INDUCT_SQL.PROCESS_COST_CHANGES(O_error_message,
                                                 c_prc_rec.process_id) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   if c_prc_rec.action_type = 'U' and c_prc_rec.process_destination = 'RMS' and L_error_count = 0 then
      if NOT is_cost_change_template(c_prc_rec.template_key) then

         if CORESVC_ITEM.PROCESS(O_error_message,
                                 c_prc_rec.process_id) = FALSE then
            return FALSE;
         end if;
      end if;
      if c_prc_rec.template_key <> 'RMSSUB_XITEM' and is_cost_change_template(c_prc_rec.template_key) then

         if CORESVC_COSTCHG.PROCESS(O_error_message,
                                    c_prc_rec.process_id) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        SQLCODE);
      return FALSE;
END EXEC_ASYNC;
-------------------------------------------------------------------------------------------------------
FUNCTION get_config(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    O_rec OUT item_induct_config%rowtype )
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75) := 'ITEM_INDUCT_SQL.get_config';
BEGIN
  SELECT * INTO O_rec FROM item_induct_config;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END get_config;
FUNCTION add_error_sheet(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_file_id       IN s9t_folder.file_id%type,
    I_process_id    IN svc_process_tracker.process_id%type)
  RETURN BOOLEAN
IS
  pragma autonomous_transaction;
  L_program    VARCHAR2(75) := 'ITEM_INDUCT_SQL.add_error_sheet';
  l_err_sheet_name VARCHAR2(255):='ERRORS';
  l_sheet_names s9t_cells;
  l_cols s9t_cells;
  CURSOR c_sheet
  IS
    SELECT wksht_name
    FROM v_s9t_tmpl_wksht_def
    WHERE template_key = 'ITEM_IND_ERRORS'
    AND wksht_key      = 'ERRORS';
  CURSOR c_col(I_col_key IN VARCHAR2)
  IS
    SELECT column_name
    FROM v_s9t_tmpl_cols_def
    WHERE template_key = 'ITEM_IND_ERRORS'
    AND wksht_key      = 'ERRORS'
    AND column_key     = I_col_key;
BEGIN
  SELECT sf.s9t_file_obj.sheet_names
  INTO l_sheet_names
  FROM s9t_folder sf
  WHERE file_id = I_file_id;
  l_cols       := NEW s9t_cells('ITEM','ITEM_DESC_TRANS','SUPPLIER','SUP_NAME_TRANS','COUNTRY_ID','COUNTRY_DESC_TRANS','TABLE_DESC','COLUMN_DESC','ROW_SEQ','ERROR_MSG');
  OPEN c_sheet;
  FETCH c_sheet INTO l_err_sheet_name;
  CLOSE c_sheet;
  l_sheet_names.extend;
  l_sheet_names(l_sheet_names.count()) := l_err_sheet_name;
  FOR i IN 1..l_cols.count
  LOOP
    OPEN c_col(l_cols(i));
    FETCH c_col INTO l_cols(i);
    CLOSE c_col;
  END LOOP;
  UPDATE s9t_folder sf
  SET sf.s9t_file_obj.sheet_names = l_sheet_names
  WHERE file_id           = I_file_id;
  INSERT
  INTO TABLE
    (SELECT sf.s9t_file_obj.sheets FROM s9t_folder sf WHERE file_id = I_file_id
    )
  SELECT s9t_sheet(l_err_sheet_name,l_cols,s9t_row_tab(),s9t_list_val_tab())
  FROM dual;
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = l_err_sheet_name
    )
  SELECT s9t_row(s9t_cells( ITEM, ITEM_DESC_TRANS, SUPPLIER, SUP_NAME_TRANS, COUNTRY_ID, COUNTRY_DESC_TRANS, TABLE_DESC, COLUMN_DESC, ROW_SEQ, ERROR_MSG))
  FROM v_iind_errors_list
  WHERE process_id = I_process_id;
  commit;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  rollback;
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END add_error_sheet;
-------------------------------------------------------------------------------------------------------
FUNCTION chunk_data(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_process_id    IN svc_process_tracker.process_id%type)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75) := 'ITEM_INDUCT_SQL.chunk_data';
  CURSOR C_chunks
  IS
  WITH items AS
    ( SELECT item FROM SVC_ITEM_COST_DETAIL WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_COST_HEAD WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_COUNTRY WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_SUPP_COUNTRY_LOC WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_COUNTRY_L10N_EXT WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_MASTER WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_MASTER_CFA_EXT WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_SUPPLIER WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_SUPPLIER_CFA_EXT WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_SUPP_COUNTRY WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_SUPP_COUNTRY_CFA_EXT WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_SUPP_COUNTRY_DIM WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_SUPP_MANU_COUNTRY WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_SUPP_UOM WHERE process_id = I_process_id
  UNION
  SELECT head_item AS item
  FROM SVC_ITEM_XFORM_DETAIL
  WHERE process_id = I_process_id
  UNION
  SELECT head_item AS item
  FROM SVC_ITEM_XFORM_HEAD
  WHERE process_id = I_process_id
  UNION
  SELECT pack_no FROM SVC_PACKITEM WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_RPM_ITEM_ZONE_PRICE WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_UDA_ITEM_DATE WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_UDA_ITEM_FF WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_UDA_ITEM_LOV WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_VAT_ITEM WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_IMAGE WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_MASTER_TL WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_SUPPLIER_TL WHERE process_id = I_process_id
  UNION
  SELECT head_item as item FROM SVC_ITEM_XFORM_HEAD_TL WHERE process_id = I_process_id
  UNION
  SELECT item FROM SVC_ITEM_IMAGE_TL WHERE process_id = I_process_id
    ),
    items1 AS
    (SELECT il.item,
      NVL(im.item_level,sim.item_level)           AS item_level,
      NVL(im.tran_level,sim.tran_level)           AS tran_level,
      NVL(im.item_xform_ind,sim.item_xform_ind)       AS item_xform_ind ,
      NVL(im.sellable_ind,sim.sellable_ind)       AS sellable_ind ,
      NVL(im.pack_ind,sim.pack_ind)           AS pack_ind,
      NVL(im.deposit_item_type,sim.deposit_item_type) AS deposit_item_type
    FROM items il,
      svc_item_master sim,
      item_master im
    WHERE il.item = sim.item (+)
    AND il.item   = im.item (+)
    ),
    items2 AS
    (SELECT item,
      (
      CASE
    WHEN item_level=1
    AND tran_level > item_level
    THEN 1
    WHEN item_level=2
    AND tran_level > item_level
    THEN 2
    WHEN item_level =tran_level
    THEN (
      CASE
        WHEN ((deposit_item_type = 'A')
        OR(item_xform_ind        = 'Y'
        AND sellable_ind         = 'Y'))
        THEN 3
        ELSE 4
      END)
    WHEN ((item_level > tran_level)
    OR pack_ind   = 'Y')
    THEN 5
    ELSE 6
      END) AS pass
    FROM items1
    ),
    items3 AS
    (SELECT item,
      pass,
      row_number() Over(partition BY pass order by item) AS row_seq
    FROM items2
    )
  SELECT item,
    pass * 1000000 + ceil(row_seq/max_chunk_size) AS chunk_id
  FROM items3,
    coresvc_item_config;
Type c_recs_type
IS
  TABLE OF c_chunks%rowtype;
  c_recs c_recs_type;
BEGIN
  OPEN c_chunks;
  LOOP
    FETCH c_chunks bulk collect INTO c_recs limit 1000;
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_COST_DETAIL
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_COST_HEAD
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_COUNTRY
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_COUNTRY_L10N_EXT
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_MASTER
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_MASTER_CFA_EXT
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_SUPPLIER
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_SUPPLIER_CFA_EXT
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_SUPP_COUNTRY
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_SUPP_COUNTRY_LOC
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---------------
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_SUPP_COUNTRY_CFA_EXT
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_SUPP_COUNTRY_DIM
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_SUPP_MANU_COUNTRY
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_SUPP_UOM
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_XFORM_DETAIL
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND head_item    = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_ITEM_XFORM_HEAD
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND head_item    = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_PACKITEM
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND pack_no      = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_RPM_ITEM_ZONE_PRICE
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_UDA_ITEM_DATE
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_UDA_ITEM_FF
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_UDA_ITEM_LOV
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    forall i IN 1..c_recs.count
    UPDATE svc_VAT_ITEM
    SET chunk_id     = c_recs(i).chunk_id
    WHERE process_id = I_process_id
    AND item         = c_recs(i).item;
    ---
    FORALL i in 1..c_recs.COUNT
       update svc_item_image
          set chunk_id = c_recs(i).chunk_id
        where process_id = I_process_id
          and item = c_recs(i).item;
    ---
    FORALL i in 1..c_recs.COUNT
       update svc_item_master_tl
          set chunk_id = c_recs(i).chunk_id
        where process_id = I_process_id
          and item = c_recs(i).item;
    ---
    FORALL i in 1..c_recs.COUNT
       update svc_item_supplier_tl
          set chunk_id = c_recs(i).chunk_id
        where process_id = I_process_id
          and item = c_recs(i).item;
    ---
    FORALL i in 1..c_recs.COUNT
       update svc_item_xform_head_tl
          set chunk_id = c_recs(i).chunk_id
        where process_id = I_process_id
          and head_item = c_recs(i).item;
    ---
    FORALL i in 1..c_recs.COUNT
       update svc_item_image_tl
          set chunk_id = c_recs(i).chunk_id
        where process_id = I_process_id
          and item = c_recs(i).item;
    ---
    DELETE
    FROM svc_process_chunks
    WHERE process_id = I_process_id;
    ---
    forall i IN 1..c_recs.count merge INTO svc_process_chunks spc USING
    ( SELECT I_process_id AS process_id,c_recs(i).chunk_id AS chunk_id FROM dual
    ) sq ON (sq.process_id = spc.process_id AND sq.chunk_id = spc.chunk_id)
  WHEN matched THEN
    UPDATE SET item_count = item_count +1 WHEN NOT matched THEN
    INSERT
      (
    process_id,
    chunk_id,
    item_count,
    status
      )
      VALUES
      (
    sq.process_id,
    sq.chunk_id,
    1,
    'N'
      );
    ---
    EXIT
  WHEN c_chunks%notfound;
  END LOOP;
  CLOSE c_chunks;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END chunk_data;
---------------------------------------------------------------------------------------
FUNCTION get_error_display
  (
    I_key   IN VARCHAR2,
    I_txt_1 IN VARCHAR2 DEFAULT NULL,
    I_txt_2 IN VARCHAR2 DEFAULT NULL,
    I_txt_3 IN VARCHAR2 DEFAULT NULL
  )
  RETURN VARCHAR2
IS
  L_key        VARCHAR2(255)  := I_key;
  L_txt_1      VARCHAR2(255)  := I_txt_1;
  L_txt_2      VARCHAR2(255)  := I_txt_2;
  L_txt_3      VARCHAR2(255)  := I_txt_3;
  L_disp_msg       VARCHAR2(1000) := NULL; -- made larger to handle %s1,%s2, %s3 emessages
  L_ret_val    NUMBER     := 0;    -- dummy return!
  L_dummy      NUMBER     := 0;
  L_box1       VARCHAR2(500)  := NULL;
  L_box2       VARCHAR2(500)  := NULL;
  L_two_msgboxs    VARCHAR2(1)    := NULL;
  L_curr_pos       NUMBER     := 198;
  L_true       BOOLEAN    := TRUE;
  L_trimmed_length NUMBER;
-------------------------------------------------------------------------------------------------------
PROCEDURE F_PARSE
  (
    IO_key IN OUT VARCHAR2,
    IO_txt_1 OUT VARCHAR2,
    IO_txt_2 OUT VARCHAR2,
    IO_txt_3 OUT VARCHAR2
  )
IS
  L_old_key    VARCHAR2(255) := IO_key;
  L_begin_location NUMBER    := 0;
  L_pos_1      NUMBER    := 0;
  L_pos_2      NUMBER    := 0;
  L_pos_3      NUMBER    := 0;
  L_pos_4      NUMBER    := 0;
BEGIN
  IO_txt_1 := NULL;
  IO_txt_2 := NULL;
  IO_txt_3 := NULL;
  --- make sure a @0 exists
  L_pos_1   := INSTR(L_old_key, '@0');
  IF L_pos_1 = 0 THEN
    RETURN;
  END IF;
  --
  L_pos_2    := INSTR(L_old_key, '@1');
  IF L_pos_2  = 0 THEN -- there are no parameters
    IO_key   := SUBSTR (L_old_key, 3);
    IO_txt_1 := NULL;
    IO_txt_2 := NULL;
    IO_txt_3 := NULL;
    RETURN;
  ELSE
    IO_key := SUBSTR (L_old_key, 3, (L_pos_2-L_pos_1-2));
  END IF;
  --
  L_pos_3    := INSTR(L_old_key, '@2');
  IF L_pos_3  = 0 THEN -- there is only one parameter
    IO_txt_1 := SUBSTR (L_old_key, L_pos_2+2);
    IO_txt_2 := NULL;
    IO_txt_3 := NULL;
    RETURN;
  ELSE
    IO_txt_1 := SUBSTR (L_old_key, L_pos_2+2, (L_pos_3-L_pos_2-2));
  END IF;
  --
  L_pos_4    := INSTR(L_old_key, '@3');
  IF L_pos_4  = 0 THEN -- there are two parameters
    IO_txt_2 := SUBSTR (L_old_key, L_pos_3+2);
    IO_txt_3 := NULL;
    RETURN;
  ELSE
    IO_txt_2 := SUBSTR (L_old_key, L_pos_3+2, (L_pos_4-L_pos_3-2));
  END IF;
  --
  IO_txt_3 := SUBSTR (L_old_key, L_pos_4+2);
END F_PARSE;
BEGIN
  F_PARSE (L_key, L_txt_1, L_txt_2, L_txt_3);
  -- only go to the database if this is a softcoded message
  IF upper(L_key) = L_key THEN
    L_disp_msg   := SQL_LIB.GET_MESSAGE_TEXT (L_key, L_txt_1, L_txt_2, L_txt_3);
  ELSE
    L_disp_msg := L_key;
  END IF;
  --
  L_dummy      := INSTR(upper(L_disp_msg), 'ORA-28115');
  IF L_dummy   != 0 THEN
    L_key      := 'INV_SEC_INSERT';
    L_disp_msg := SQL_LIB.GET_MESSAGE_TEXT (L_key);
  END IF;
  RETURN L_disp_msg;
END get_error_display;
---------------------------------------------------------------------------------------
FUNCTION NEXT_UPDATE_ID_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists       IN OUT   BOOLEAN,
                   I_next_update_id   IN       SVC_ITEM_MASTER.NEXT_UPD_ID%TYPE)
   RETURN BOOLEAN IS
   L_program   VARCHAR2(255) := 'ITEM_INDUCT_SQL.NEXT_UPDATE_ID_EXISTS';
   L_exists    VARCHAR2(1);

   cursor C_NEXT_UPD_ID is
      select 'x'
     from svc_item_master
    where next_upd_id = upper(I_next_update_id);

BEGIN
   open C_NEXT_UPD_ID;
   fetch C_NEXT_UPD_ID into L_exists;
   close C_NEXT_UPD_ID;

   if L_exists is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        to_char(SQLCODE));
      return FALSE;
END NEXT_UPDATE_ID_EXISTS;
---------------------------------------------------------------------------------------
FUNCTION LAST_UPDATE_ID_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists       IN OUT   BOOLEAN,
                   I_last_update_id   IN       SVC_ITEM_MASTER.LAST_UPD_ID%TYPE)
   RETURN BOOLEAN IS
   L_program   VARCHAR2(255) := 'ITEM_INDUCT_SQL.LAST_UPDATE_ID_EXISTS';
   L_exists    VARCHAR2(1);

   cursor C_LAST_UPD_ID is
      select 'x'
     from svc_item_master
    where last_upd_id = upper(I_last_update_id);

BEGIN
   open C_LAST_UPD_ID;
   fetch C_LAST_UPD_ID into L_exists;
   close C_LAST_UPD_ID;

   if L_exists is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        to_char(SQLCODE));
      return FALSE;
END LAST_UPDATE_ID_EXISTS;
---------------------------------------------------------------------------------------
FUNCTION UPDATE_SVC_PROCESS_TRACKER(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id       IN   SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                    I_status           IN   SVC_PROCESS_TRACKER.STATUS%TYPE,
                    I_process_src      IN   SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
                    I_process_dest     IN   SVC_PROCESS_TRACKER.PROCESS_DESTINATION%TYPE,
                    I_action_date      IN   SVC_PROCESS_TRACKER.ACTION_DATE%TYPE,
                    I_rms_async_id     IN   SVC_PROCESS_TRACKER.RMS_ASYNC_ID%TYPE)
   RETURN BOOLEAN IS
   L_program   VARCHAR2(255) := 'ITEM_INDUCT_SQL.UPDATE_SVC_PROCESS_TRACKER';

BEGIN
   update svc_process_tracker
      set status = I_status,
      process_source = I_process_src,
      process_destination = I_process_dest,
      action_date = I_action_date,
      rms_async_id = I_rms_async_id
    where process_id = I_process_id
    and user_id = get_user;

    return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        to_char(SQLCODE));
      return FALSE;
END UPDATE_SVC_PROCESS_TRACKER;
---------------------------------------------------------------------------------------
FUNCTION UPDATE_SVC_PROCESS_TRACKER(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_process_id       IN      SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                                    I_status           IN      SVC_PROCESS_TRACKER.STATUS%TYPE,
                                    I_process_src      IN      SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
                                    I_process_dest     IN      SVC_PROCESS_TRACKER.PROCESS_DESTINATION%TYPE,
                                    I_action_date      IN      SVC_PROCESS_TRACKER.ACTION_DATE%TYPE,
                                    I_rms_async_id     IN      SVC_PROCESS_TRACKER.RMS_ASYNC_ID%TYPE,
                                    I_new_process_id   IN      SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS
   L_program   VARCHAR2(255) := 'ITEM_INDUCT_SQL.UPDATE_SVC_PROCESS_TRACKER';

BEGIN
   update svc_process_tracker
      set status = I_status,
          process_source = I_process_src,
          process_destination = I_process_dest,
          action_date = I_action_date,
          rms_async_id = I_rms_async_id,
          process_id = I_new_process_id,
          user_id = get_user
    where process_id = I_process_id;

    return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
      return FALSE;
END UPDATE_SVC_PROCESS_TRACKER;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_COST_CHANGE(
    O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_COST_CHANGE_DESC IN OUT COST_SUSP_SUP_HEAD.COST_CHANGE_DESC%TYPE,
    I_source           IN SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
    I_COST_CHANGE      IN COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'ITEM_INDUCT_SQL.VALIDATE_COST_CHANGE';
  CURSOR c_check
  IS
    SELECT 'Y',
      cost_change_desc
    FROM svc_cost_susp_sup_head
    WHERE I_source  = 'STG'
    AND cost_change = I_COST_CHANGE
  UNION ALL
  SELECT 'Y',
    cost_change_desc
  FROM cost_susp_sup_head
  WHERE status            IN ('A','S','W')
  AND I_source             = 'RMS'
  AND cost_change          = I_COST_CHANGE;
  l_cost_change_exists VARCHAR2(1):='N';
BEGIN
  OPEN c_check;
  FETCH c_check INTO l_cost_change_exists,O_COST_CHANGE_DESC;
  CLOSE c_check;
  IF l_cost_change_exists = 'N' THEN
    O_error_message  := 'INV_COST_CHANGE';
    RETURN false;
  END IF;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END VALIDATE_COST_CHANGE;
---------------------------------------------------------------------------------------
FUNCTION IS_COST_CHANGE_TEMPLATE(
    I_template IN S9T_TEMPLATE.TEMPLATE_KEY%TYPE)
  RETURN BOOLEAN
IS
  CURSOR c_check
  IS
    SELECT 'Y'
    FROM s9t_tmpl_wksht_def
    WHERE template_key  = I_TEMPLATE
    AND wksht_key      IN ('COST_SUSP_SUP_HEAD', 'COST_SUSP_SUP_DETAIL', 'COST_SUSP_SUP_DETAIL_LOC')
    AND mandatory   = 'Y';
  l_exists VARCHAR2(1) := 'N';
BEGIN
  OPEN c_check;
  FETCH c_check INTO l_exists;
  CLOSE c_check;
  IF l_exists = 'Y' THEN
    RETURN true;
  ELSE
    RETURN false;
  END IF;
END IS_COST_CHANGE_TEMPLATE;
---------------------------------------------------------------------------------------
FUNCTION SEARCH_COST_CHANGES(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_count     IN OUT NUMBER,
    I_sch_crit_rec  IN cost_chg_sch_crit_typ,
    I_source        IN SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
    I_process_id    IN SVC_PROCESS_TRACKER.PROCESS_ID%TYPE )
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'ITEM_INDUCT_SQL.SEARCH_COST_CHANGES';
BEGIN
  IF I_source = 'RMS' THEN
    INSERT INTO SVC_COST_CHG_SEARCH_TEMP
      (PROCESS_ID ,COST_CHANGE
      )
    SELECT I_process_id,
      cost_change
    FROM cost_susp_sup_head ch
    WHERE (I_sch_crit_rec.cost_change IS NULL
    OR I_sch_crit_rec.cost_change      = ch.cost_change)
    AND ( I_sch_crit_rec.cost_change_desc IS NULL
         OR (lower(ch.cost_change_desc )    like '%'|| lower(I_sch_crit_rec.cost_change_desc)||'%'))
    AND ch.status                      = nvl(I_sch_crit_rec.status,ch.status)
    AND ch.reason                      = nvl(I_sch_crit_rec.reason,ch.reason)
    AND trunc(ch.create_date)<=nvl(trunc(I_sch_crit_rec.to_create_date),trunc(ch.create_date))
    AND trunc(ch.create_date)>=nvl(trunc(I_sch_crit_rec.from_create_date),trunc(ch.create_date))
    AND (I_sch_crit_rec.supplier      IS NULL
    OR EXISTS
      (SELECT 1
      FROM cost_susp_sup_detail csd,
    sups sup
      WHERE (sup.supplier    = I_sch_crit_rec.supplier
      OR sup.supplier_parent = I_sch_crit_rec.supplier)
      AND csd.supplier       = sup.supplier
      AND csd.cost_change    = ch.cost_change
      ))
    AND (I_sch_crit_rec.supplier_site IS NULL AND I_sch_crit_rec.supplier_site_name IS NULL
    OR EXISTS
      (SELECT 1
      FROM cost_susp_sup_detail csd , v_sups
      WHERE csd.cost_change = ch.cost_change
      AND csd.supplier      = nvl(I_sch_crit_rec.supplier_site,csd.supplier)
      AND csd. supplier     = v_sups.supplier
      AND  ( I_sch_crit_rec.supplier_site_name IS NULL
         OR (lower(v_sups.sup_name)    like '%'|| lower(I_sch_crit_rec.supplier_site_name)||'%'))
      ))
    AND (I_sch_crit_rec.from_date      IS NULL
    OR ch.active_date              >= I_sch_crit_rec.from_date )
    AND (I_sch_crit_rec.to_date        IS NULL
    OR ch.active_date              <= I_sch_crit_rec.to_date )
    AND (I_sch_crit_rec.last_upd_id    IS NULL
    OR NVL(ch.approval_id,ch.create_id) = I_sch_crit_rec.last_upd_id ) ;
    O_count                := Sql%rowcount;
  END IF;
  IF I_source = 'STG' THEN
    INSERT INTO SVC_COST_CHG_SEARCH_TEMP
      (PROCESS_ID ,COST_CHANGE
      )
      with cost_chg_table as
   ( (SELECT process_id,
            cost_change  ,
            cost_change_desc ,
            status ,
            reason ,
            create_datetime ,
            active_date ,
            last_upd_id
    FROM svc_cost_susp_sup_head )
    union
    (SELECT process_id,
            cost_change  ,
            null cost_change_desc ,
            null status ,
            null reason ,
            create_datetime ,
            null active_date ,
            last_upd_id
    FROM svc_cost_susp_sup_detail )
    union
     (SELECT process_id,
            cost_change  ,
            null cost_change_desc ,
            null status ,
            null reason ,
            create_datetime ,
            null active_date ,
            last_upd_id
    FROM svc_cost_susp_sup_detail_loc ))
    select DISTINCT I_process_id , cost_change
    FROM cost_chg_table ch
    WHERE (I_sch_crit_rec.cost_change IS NULL
    OR I_sch_crit_rec.cost_change      = ch.cost_change)
    AND ( I_sch_crit_rec.cost_change_desc IS NULL
         OR (lower(ch.cost_change_desc )    like '%'|| lower(I_sch_crit_rec.cost_change_desc)||'%'))
    AND ( I_sch_crit_rec.status IS NULL
        OR ch.status                      = I_sch_crit_rec.status )
    AND (I_sch_crit_rec.reason IS NULL
        OR ch.reason                      = I_sch_crit_rec.reason )
      AND  ( I_sch_crit_rec.to_create_date IS  NULL
        OR trunc(ch.create_datetime)<=trunc(I_sch_crit_rec.to_create_date))
    AND (I_sch_crit_rec.from_create_date IS NULL
        OR trunc(ch.create_datetime)>=trunc(I_sch_crit_rec.from_create_date))
    AND (I_sch_crit_rec.supplier      IS NULL
    OR EXISTS
      (SELECT 1
      FROM svc_cost_susp_sup_detail csd,
    sups sup
      WHERE (sup.supplier    = I_sch_crit_rec.supplier
      OR sup.supplier_parent = I_sch_crit_rec.supplier)
      AND csd.supplier       = sup.supplier
      AND csd.cost_change    = ch.cost_change
      ))
    AND (I_sch_crit_rec.supplier_site IS NULL AND I_sch_crit_rec.supplier_site_name IS NULL
    OR EXISTS
      (SELECT 1
      FROM svc_cost_susp_sup_detail csd, v_sups
      WHERE csd.cost_change = ch.cost_change
      AND csd.supplier      = nvl(I_sch_crit_rec.supplier_site,csd.supplier)
      AND csd. supplier     = v_sups.supplier
      AND ( I_sch_crit_rec.supplier_site_name IS NULL
         OR (lower(v_sups.sup_name )    like '%'|| lower(I_sch_crit_rec.supplier_site_name)||'%'))
      ))
    AND (I_sch_crit_rec.from_date   IS NULL
    OR ch.active_date           >= I_sch_crit_rec.from_date )
    AND (I_sch_crit_rec.to_date     IS NULL
    OR ch.active_date           <= I_sch_crit_rec.to_date )
    AND (I_sch_crit_rec.last_upd_id IS NULL
    OR ch.last_upd_id            = I_sch_crit_rec.last_upd_id )
    AND (I_sch_crit_rec.process_id  IS NULL
    OR ch.process_id             = I_sch_crit_rec.process_id );
    O_count             := Sql%rowcount;
  END IF;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END SEARCH_COST_CHANGES;
---------------------------------------------------------------------------------------
FUNCTION delete_search_ccs(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_process_id    IN NUMBER)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75):= 'ITEM_INDUCT_SQL.delete_search_ccs';
BEGIN
  DELETE FROM SVC_COST_CHG_SEARCH_TEMP WHERE process_id = I_process_id;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END delete_search_ccs;
---------------------------------------------------------------------------------------
FUNCTION GET_ITEM_COL(I_table IN VARCHAR2)
  RETURN VARCHAR2 IS
  O_column VARCHAR2(255);
BEGIN
  RETURN
  CASE I_table
  WHEN 'PACKITEM' THEN 'PACK_NO'
  WHEN 'ITEM_XFORM_HEAD' THEN 'HEAD_ITEM'
  ELSE 'ITEM'
  END;
END GET_ITEM_COL;
----------------------------------------------------
FUNCTION SEARCH_ITEMS_WRP(O_error_message IN OUT rtk_errors.rtk_text%type,
                          I_sch_crit_rec  IN     item_induct_sch_crit_typ,
                          I_source        IN     VARCHAR2,
                          I_process_id    IN     NUMBER,
                          O_item_count       OUT NUMBER)
RETURN NUMBER IS
  L_program VARCHAR2(255) := 'ITEM_INDUCT_SQL.SEARCH_ITEMS_WRP';
BEGIN
    if SEARCH_ITEMS(O_error_message,
                    I_sch_crit_rec,
                    I_source,
                    I_process_id,
                    O_item_count ) then
  RETURN 1;
  else RETURN 0;
  end if;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN 0;
END SEARCH_ITEMS_WRP;
----------------------------------------------------
FUNCTION DELETE_PROCESSES_WRP(O_error_message  IN OUT rtk_errors.rtk_text%type,
                              I_process_id_tab IN     num_tab)
RETURN NUMBER IS
  L_program VARCHAR2(255) := 'ITEM_INDUCT_SQL.DELETE_PROCESSES_WRP';
BEGIN
    if delete_processes(O_error_message,
                        I_process_id_tab) then
  RETURN 1;
  else RETURN 0;
  end if;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN 0;
END DELETE_PROCESSES_WRP;
----------------------------------------------------
FUNCTION SEARCH_COST_CHANGES_WRP(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_count     IN OUT NUMBER,
    I_sch_crit_rec  IN cost_chg_sch_crit_typ,
    I_source        IN SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
    I_process_id    IN SVC_PROCESS_TRACKER.PROCESS_ID%TYPE )
  RETURN NUMBER
IS
  L_program VARCHAR2(255) := 'ITEM_INDUCT_SQL.SEARCH_COST_CHANGES_WRP';
BEGIN
    if SEARCH_COST_CHANGES(O_error_message,
                            O_count,
                            I_sch_crit_rec,
                            I_source,
                            I_process_id ) then
  RETURN 1;
  else return 0;
  end if;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN 0;
END SEARCH_COST_CHANGES_WRP;
END item_induct_sql;
/
