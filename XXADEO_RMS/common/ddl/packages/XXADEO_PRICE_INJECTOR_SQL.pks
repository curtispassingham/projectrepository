CREATE OR REPLACE PACKAGE XXADEO_PRICE_INJECTOR_SQL AS
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package "XXADEO_PRICE_INJECTOR_SQL"                          */
/******************************************************************************/
--------------------------------------------------------------------------------

PRICE_CHANGE_APPROVE                CONSTANT VARCHAR2(2)  := 'A';
PRICE_CHANGE_DISAPPROVE             CONSTANT VARCHAR2(2)  := 'W';
--
GP_central_optimized_price          VARCHAR2(40)          := 7;
GP_central_emergency_price          VARCHAR2(40)          := 8;
GP_store_competitor_alig_price      VARCHAR2(40)          := 9;
GP_store_emergency_price            VARCHAR2(40)          := 10;
GP_national_price                   VARCHAR2(40)          := 11;
GP_legacy_price                     VARCHAR2(40)          := 12;
--
GP_emergency_pc                     VARCHAR2(4)           := 'PC_E';
GP_regular_pc                       VARCHAR2(4)           := 'PC_R';

--
TYPE rpm_price_dtl_rec IS RECORD (item           VARCHAR2(25),
                                  reason_code    NUMBER(6),
                                  location       NUMBER(10) DEFAULT NULL,
                                  zone_id        NUMBER(10) DEFAULT NULL,
                                  effective_date DATE);
--------------------------------------------------------------------------------
FUNCTION RUN_BATCH_PRE_INJECTOR(O_error_message       IN OUT LOGGER_LOGS.TEXT%TYPE,
                                IO_price_event_type   IN OUT XXADEO_PRICE_REQUEST.TYPE%TYPE,
                                IO_price_event_action IN OUT XXADEO_PRICE_REQUEST.ACTION%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION RUN_BATCH_POST_INJECTOR(O_error_message      IN OUT LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PRICE_PURGE_PROCESS(O_error_message OUT LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION POPULATE_RPM_CC_ERROR_TABLE(O_cc_error_tbl               OUT CONFLICT_CHECK_ERROR_TBL,
                                     IO_pc_rejected            IN OUT XXADEO_PC_REJECTED_TBL,
                                     I_price_change_event_type IN     VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END XXADEO_PRICE_INJECTOR_SQL;
/
