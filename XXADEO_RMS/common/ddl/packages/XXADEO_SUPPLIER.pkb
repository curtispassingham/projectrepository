--------------------------------------------------------
--  DDL for Package Body XXADEO_SUPPLIER
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE PACKAGE BODY XXADEO_SUPPLIER AS

LP_system_options SYSTEM_OPTIONS%ROWTYPE := NULL;
-------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(IO_status_code     IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2);
-------------------------------------------------------------------------
/* Function and Procedure Bodies */
-------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code        IN OUT   VARCHAR2,
                  O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_outputobject       IN OUT   "XXADEO_SupplierColRef_REC",
                  I_inputobject        IN       "XXADEO_SupplierColDesc_REC",
                  I_inputobject_type   IN       VARCHAR2)
IS

   PROGRAM_ERROR        EXCEPTION;

   L_program            VARCHAR2(64) := 'XXADEO_SUPPLIER.CONSUME';
   L_table_locked       BOOLEAN      := FALSE;

   L_message            "XXADEO_SupplierColDesc_REC";
   L_supp_rec           SUPP_REC;
   L_ref_outputobject   "XXADEO_SupplierColRef_REC";
   L_CFA_SUP_TBL        "XXADEO_CFA_DETAILS_SUP_TBL";

BEGIN

   -- Fetch financial application information.
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- No AIA integration
   if NVL(LP_system_options.financial_ap, '0') <> 'A' then
   
     -- Force its runtime value to 'A'
     LP_system_options.financial_ap := 'A';
   
     /*
     O_error_message := SQL_LIB.CREATE_MSG('NO_AIA_INT',
                                            NULL,
                                            NULL,
                                            NULL);
      raise PROGRAM_ERROR;
	  */
   end if;

   L_message := I_inputobject;

   -- The incoming message is NULL
   if (L_message is NULL or L_message.SupplierDesc_TBL is NULL ) then
      O_error_message := SQL_LIB.CREATE_MSG('XXADEO_INV_MESSAGE',
                                            NULL,
                                            NULL,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   if LP_system_options.financial_ap = 'A' then
      -- Process/validate record by retrieving the value for each associated node.
      if XXADEO_SUPPLIER_VALIDATE.PROCESS_SUPPLIER_RECORD (O_error_message,
                                                              L_supp_rec,
                                                              L_CFA_SUP_TBL,
                                                              L_ref_outputobject,
                                                              I_inputobject,
                                                              I_inputobject_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- Populate RMS tables with incoming data
      if XXADEO_SUPPLIER_SQL.PERSIST(O_error_message,
                                        L_supp_rec,
                                        L_CFA_SUP_TBL) = FALSE then
         raise PROGRAM_ERROR;
      -- Call localization layer.
      elsif L10N_FLEX_API_SQL.PERSIST_L10N_ATTRIB(O_error_message,
                                                  I_inputobject,
                                                  I_inputobject_type) = FALSE then
         raise PROGRAM_ERROR;
      else
         O_outputobject := L_ref_outputobject;
      end if;

      O_status_code := API_CODES.SUCCESS;
   end if;
   ---

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);

   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END CONSUME;
-------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(IO_status_code     IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2)
IS
   L_program   VARCHAR2(64) := 'XXADEO_SUPPLIER.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(IO_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(IO_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;
-------------------------------------------------------------------------
END XXADEO_SUPPLIER;

/