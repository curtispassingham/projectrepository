CREATE OR REPLACE PACKAGE BODY XXADEO_CFA_SQL IS
    
---------------------------------------------------------------------------------------
cursor C_exist_code_detail(I_code_type  CODE_DETAIL.CODE_TYPE%TYPE,
                           I_code       CODE_DETAIL.CODE%TYPE) is
  select trim(code_desc) attribute_id
    from code_detail
   where code_type = I_code_type
     and code      = I_code;
---------------------------------------------------------------------------------------
cursor C_get_cfa_attrib_id(I_attribute_id CFA_ATTRIB.ATTRIB_ID%TYPE,
                           I_code_type    CODE_DETAIL.CODE_TYPE%TYPE,
                           I_code         CODE_DETAIL.CODE%TYPE) is
  select attrib_id,
         group_id,
         storage_col_name,
         data_type
    from cfa_attrib
   where attrib_id = I_attribute_id;
---------------------------------------------------------------------------------------
cursor C_get_keys_col_for_ext_table(I_attribute_id           CFA_ATTRIB.ATTRIB_ID%TYPE,
                                    I_xxadeo_cfa_key_val_tbl XXADEO_CFA_KEY_VAL_TBL) is
  with key_cols as (select key_col
                    from (select key_col_1 key_col
                            from table(I_xxadeo_cfa_key_val_tbl)
                          union all
                          select key_col_2 key_col
                            from table(I_xxadeo_cfa_key_val_tbl)
                          union all
                          select key_col_3 key_col
                            from table(I_xxadeo_cfa_key_val_tbl)
                          union all
                          select key_col_4 key_col
                            from table(I_xxadeo_cfa_key_val_tbl)
                          )
                   where key_col is not null
                  )
 select distinct e.custom_ext_table,
        e.base_rms_table
   from cfa_attrib a,
        cfa_attrib_group g,
        cfa_attrib_group_set gs,
        cfa_ext_entity e
  where a.attrib_id      = I_attribute_id
    and a.group_id       = g.group_id
    and g.group_set_id   = gs.group_set_id
    and gs.ext_entity_id = e.ext_entity_id
    and not exists (select 1
                     from cfa_ext_entity_key ek
                    where ek.base_rms_table = e.base_rms_table
                      and not exists (select 1
                                        from key_cols tbl
                                       where tbl.key_col = ek.key_col))
    and not exists (select 1
                      from key_cols tbl
                     where not exists (select 1
                                         from cfa_ext_entity_key ek
                                        where ek.base_rms_table = e.base_rms_table
                                          and ek.key_col        = tbl.key_col));
---------------------------------------------------------------------------------------
cursor C_get_cust_ext_table(I_attribute_id CFA_ATTRIB.ATTRIB_ID%TYPE) is
  select e.custom_ext_table,
         e.base_rms_table
    from cfa_attrib a,
         cfa_attrib_group g,
         cfa_attrib_group_set gs,
         cfa_ext_entity e
   where a.attrib_id      = I_attribute_id
     and a.group_id       = g.group_id
     and g.group_set_id   = gs.group_set_id
     and gs.ext_entity_id = e.ext_entity_id;
---------------------------------------------------------------------------------------
cursor C_get_key_col_names(I_cfa_key_val_tbl XXADEO_CFA_KEY_VAL_TBL) is
  select new XXADEO_CFA_KEY_VAL_OBJ(key_col_1 => tbl.key_col_1,
                                    key_col_2 => tbl.key_col_2,
                                    key_col_3 => tbl.key_col_3,
                                    key_col_4 => tbl.key_col_4)
    from (select distinct key_col_1,
                          key_col_2,
                          key_col_3,
                          key_col_4
            from table(I_cfa_key_val_tbl))tbl;
---------------------------------------------------------------------------------------
cursor C_exist_cfa_attrib(I_cfa_attrib_id CFA_ATTRIB.ATTRIB_ID%TYPE) is
  select attrib_id
    from cfa_attrib
   where attrib_id = I_cfa_attrib_id;
---------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------
FUNCTION GET_CFA_BY_ATTRIB(O_error_message            OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_xxadeo_cfa_search_obj    OUT XXADEO_CFA_SEARCH_OBJ,
                           I_attrib_id                IN  CFA_ATTRIB.ATTRIB_ID%TYPE DEFAULT NULL,
                           I_code_type                IN  CODE_DETAIL.CODE_TYPE%TYPE DEFAULT NULL,
                           I_code                     IN  CODE_DETAIL.CODE%TYPE DEFAULT NULL,
                           I_xxadeo_cfa_key_val_obj   IN  XXADEO_CFA_KEY_VAL_OBJ)
RETURN BOOLEAN IS
  --
  L_program                 VARCHAR2(64) := 'XXADEO_CFA_SQL.GET_CFA_BY_ATTRIB';
  L_xxadeo_cfa_search_tbl   XXADEO_CFA_SEARCH_TBL;
  I_xxadeo_cfa_key_val_tbl  XXADEO_CFA_KEY_VAL_TBL;
  --
BEGIN
  --
  -- validate input
  --
  if I_attrib_id is not null and (I_code_type is not null or I_code is not null) then
    --
    O_error_message := 'Can only search by attrib id or code_detail';
    RETURN FALSE;
    --
  elsif I_attrib_id is null and (I_code_type is null or I_code is null) then
    --
    O_error_message := 'Attrib id or code_detail inputs are mandatories';
    RETURN FALSE;
    --
  end if;
  --
  -- check exist code detail
  --
  I_xxadeo_cfa_key_val_tbl  := XXADEO_CFA_KEY_VAL_TBL();
  --
  I_xxadeo_cfa_key_val_tbl.extend();
  I_xxadeo_cfa_key_val_tbl(I_xxadeo_cfa_key_val_tbl.count) := I_xxadeo_cfa_key_val_obj;
  --
  if I_attrib_id is null then
    --
    L_xxadeo_cfa_search_tbl := SEARCH_CFA_BY_ATTRIB(I_code_type              => I_code_type,
                                                    I_code                   => I_code,
                                                    I_xxadeo_cfa_key_val_tbl => I_xxadeo_cfa_key_val_tbl);
    --
  else
    --
     L_xxadeo_cfa_search_tbl := SEARCH_CFA_BY_ATTRIB(I_attrib_id          => I_attrib_id,
                                                     I_xxadeo_cfa_key_val_tbl => I_xxadeo_cfa_key_val_tbl);
    --
  end if;
  --
  O_xxadeo_cfa_search_obj := L_xxadeo_cfa_search_tbl(1);
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END GET_CFA_BY_ATTRIB;
---------------------------------------------------------------------------------------
FUNCTION SET_CFA_BY_ATTRIB(O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_cfa_attrib_tbl IN OUT XXADEO_CFA_ATTRIB_TBL)
RETURN BOOLEAN IS
  --
  L_program                   VARCHAR2(64) := 'XXADEO_CFA_SQL.SET_CFA_BY_ATTRIB';
  L_attribute_id              CFA_ATTRIB.ATTRIB_ID%TYPE;
  L_merge_statement           VARCHAR2(32767);
  L_insert_col_string         VARCHAR2(1000);
  L_insert_value_string       VARCHAR2(100);
  L_key_col_string            VARCHAR2(1000);
  L_key_col_values            VARCHAR2(1000);
  L_merge_on_clause           VARCHAR2(1000);
  L_xxadeo_cfa_key_val_tbl    XXADEO_CFA_KEY_VAL_TBL;
  L_valid_key_table_attrs     VARCHAR2(1);
  --
  cursor C_validate_key_tbl_attr(I_xxadeo_cfa_key_val        XXADEO_CFA_KEY_VAL_TBL)  is
    with tbl_cfa as (select (select 'Y'
                               from dual
                              where exists (select 'Y'
                                              from table(I_xxadeo_cfa_key_val)
                                             where attrib_varchar_value is not null)) exist_varchar,
                            (select 'Y'
                               from dual
                              where exists (select 'Y'
                                              from table(I_xxadeo_cfa_key_val)
                                             where attrib_number_value is not null)) exist_number,
                            (select 'Y'
                               from dual
                              where exists (select 'Y'
                                              from table(I_xxadeo_cfa_key_val)
                                             where attrib_date_value is not null)) exist_date
                      from dual)
    select 'Y'
      from tbl_cfa tbl
     where ((exist_varchar = 'Y' and exist_number is null and exist_date is null) or
            (exist_number  = 'Y' and exist_varchar is null and exist_date is null) or
            (exist_date    = 'Y' and exist_varchar is null and exist_number is null));
  --
BEGIN
  --
  -- validate input
  --
  if IO_cfa_attrib_tbl is null or IO_cfa_attrib_tbl.count = 0 then
    --
    O_error_message := 'CFA Attribute table can not be empty';
    RETURN FALSE;
    --
  else
    --
    for i in 1..IO_cfa_attrib_tbl.count loop
      --
      L_attribute_id := null;
      L_valid_key_table_attrs := null;
      --
      -- check exist attribute id or code detail
      --
      if IO_cfa_attrib_tbl(i).attrib_id is null then
        --
        open C_exist_code_detail(I_code_type => IO_cfa_attrib_tbl(i).code_type,
                                 I_code      => IO_cfa_attrib_tbl(i).code);
        fetch C_exist_code_detail into L_attribute_id;
          --
          if C_exist_code_detail%NOTFOUND then
            --
            O_error_message := 'Code Type/Code: '|| IO_cfa_attrib_tbl(i).code_type ||'/'||IO_cfa_attrib_tbl(i).code || ' does not exist in RMS CODE_DETAIL.';
            close C_exist_code_detail;
            RETURN FALSE;
            --
          end if;
          --
        close C_exist_code_detail;
        --
      else
        --
        open C_exist_cfa_attrib(I_cfa_attrib_id => IO_cfa_attrib_tbl(i).attrib_id);
        fetch C_exist_cfa_attrib into L_attribute_id;
          --
          if C_exist_cfa_attrib%NOTFOUND then
            --
            O_error_message := 'CFA Atrib ID '||IO_cfa_attrib_tbl(i).attrib_id || ' is not configured on RMS CFA_ATTRIB.';
            close C_exist_cfa_attrib;
            RETURN FALSE;
            --
          end if;
          --
        close C_exist_cfa_attrib;
        --
      end if;
      --
      -- get attribute info
      --
      open C_get_cfa_attrib_id(I_attribute_id => L_attribute_id,
                               I_code_type    => IO_cfa_attrib_tbl(i).code_type,
                               I_code         => IO_cfa_attrib_tbl(i).code);
      fetch C_get_cfa_attrib_id into L_attribute_id,
                                     IO_cfa_attrib_tbl(i).group_id,
                                     IO_cfa_attrib_tbl(i).storage_col_name,
                                     IO_cfa_attrib_tbl(i).storage_data_type;
        --
        if C_get_cfa_attrib_id%NOTFOUND then
          --
          O_error_message := ' There is no CFA Attrib for Code Type/Code: '|| IO_cfa_attrib_tbl(i).code_type ||'/'||IO_cfa_attrib_tbl(i).code || '.';
          close C_exist_code_detail;
          RETURN FALSE;
          --
        end if;
        --
      close C_get_cfa_attrib_id;
      --
      -- check cfa_keys
      --
      if IO_cfa_attrib_tbl(i).cfa_key_val_tbl is null or IO_cfa_attrib_tbl(i).cfa_key_val_tbl.count = 0 then
        --
        O_error_message := 'CFA keys table can not be empty';
        RETURN FALSE;
        --
      else
        --
        -- check keys for attribute id and retrieve extension table
        --
        open C_get_keys_col_for_ext_table(I_attribute_id           => L_attribute_id,
                                          I_xxadeo_cfa_key_val_tbl => IO_cfa_attrib_tbl(i).cfa_key_val_tbl);
        fetch C_get_keys_col_for_ext_table into IO_cfa_attrib_tbl(i).custom_ext_table,
                                                IO_cfa_attrib_tbl(i).base_rms_table;
          --
          if C_get_keys_col_for_ext_table%NOTFOUND then
            --
            O_error_message := 'Extension Key Column(s) not exist for CFA Attrib '||L_attribute_id || ' or key value(s) are null.';
            close C_get_keys_col_for_ext_table;
            RETURN FALSE;
            --
          end if;
          --
        close C_get_keys_col_for_ext_table;
        --
      end if;
      --
      -- check values
      --   
      open C_validate_key_tbl_attr(I_xxadeo_cfa_key_val       => IO_cfa_attrib_tbl(i).cfa_key_val_tbl);
      fetch C_validate_key_tbl_attr into L_valid_key_table_attrs;
        --
        if C_validate_key_tbl_attr%NOTFOUND then
          --
          O_error_message := 'Can only set one attribute value type for each CFA Attrib ID';
          close C_validate_key_tbl_attr;
          RETURN FALSE;
          --
        end if;
        --
      close C_validate_key_tbl_attr;
      --
    end loop;
    --
  end if;
  --
  -- merge cfa attrib input values into correspondent extension cfa table
  --
  for i in 1..IO_cfa_attrib_tbl.count loop
    --
    L_insert_col_string   := null;
    L_insert_value_string := null;
    L_key_col_string      := null;
    L_key_col_values      := null;
    --
    -- check distinct key_types
    --
    L_xxadeo_cfa_key_val_tbl := XXADEO_CFA_KEY_VAL_TBL();
    --
    open C_get_key_col_names(I_cfa_key_val_tbl => IO_cfa_attrib_tbl(i).cfa_key_val_tbl);
    fetch C_get_key_col_names bulk collect into L_xxadeo_cfa_key_val_tbl;
    close C_get_key_col_names;
    --
    if L_xxadeo_cfa_key_val_tbl.count > 1 then
      --
      O_error_message := 'More than one key type in same process';
      RETURN FALSE;
      --
    end if;
    --
    for key_rec in (select key_col_1,key_col_2,key_col_3,key_col_4
                      from table(L_xxadeo_cfa_key_val_tbl)) loop
      --
      if key_rec.key_col_1 is not null then
        --
        L_merge_on_clause := L_merge_on_clause
                           || key_rec.key_col_1
                           || ' = '
                           || 'tbl.key_value_1'
                           || ' and ';
        --
        L_key_col_string     := L_key_col_string || ' key_value_1' ||',';
        L_insert_col_string  := L_insert_col_string || key_rec.key_col_1 ||',';
        L_key_col_values     := L_key_col_values || 'tbl.key_value_1,';
        --
      end if;
      --
      if key_rec.key_col_2 is not null then
        --
        L_merge_on_clause := L_merge_on_clause
                           || key_rec.key_col_2
                           || ' = '
                           || 'tbl.key_value_2'
                           || ' and ';
        --
        L_key_col_string     := L_key_col_string || ' key_value_2' ||',';
        L_insert_col_string  := L_insert_col_string || key_rec.key_col_2 ||',';
        L_key_col_values     := L_key_col_values || 'tbl.key_value_2,';
        --
      end if;
      --
      if key_rec.key_col_3 is not null then
        --
        L_merge_on_clause := L_merge_on_clause
                           || key_rec.key_col_3
                           || ' = '
                           || 'tbl.key_value_3'
                           || ' and ';
        --
        L_key_col_string     := L_key_col_string || ' key_value_3' ||',';
        L_insert_col_string  := L_insert_col_string || key_rec.key_col_3 ||',';
        L_key_col_values     := L_key_col_values || 'tbl.key_value_3,';
        --
      end if;
      --
      if key_rec.key_col_4 is not null then
        --
        L_merge_on_clause := L_merge_on_clause
                           || key_rec.key_col_4
                           || ' = '
                           || 'tbl.key_value_4'
                           || ' and ';
        --
        L_key_col_string     := L_key_col_string || ' key_value_4' ||',';
        L_insert_col_string  := L_insert_col_string || key_rec.key_col_4 ||',';
        L_key_col_values     := L_key_col_values || 'tbl.key_value_4,';
        --
      end if;
      --
    end loop;
    --
    if IO_cfa_attrib_tbl(i).storage_data_type = 'VARCHAR2' then
      --
      L_insert_value_string := ' tbl.attrib_varchar_value';
      --
    elsif IO_cfa_attrib_tbl(i).storage_data_type = 'NUMBER' then
      --
      L_insert_value_string := ' tbl.attrib_number_value';
      --
    elsif IO_cfa_attrib_tbl(i).storage_data_type = 'DATE' then
      --
      L_insert_value_string := ' tbl.attrib_date_value';
      --
    end if;
    --
    L_merge_statement := 'merge into '|| IO_cfa_attrib_tbl(i).custom_ext_table
                         || ' using (select '
                         || L_key_col_string
                         || 'attrib_varchar_value attrib_varchar_value,
                             attrib_number_value  attrib_number_value,
                             attrib_date_value    attrib_date_value
                              from table(:key_table)) tbl
                              on ('
                         || L_merge_on_clause
                         || ' group_id = ' || IO_cfa_attrib_tbl(i).group_id
                         || ')'
                         ||' when matched then
                              update set '
                         || IO_cfa_attrib_tbl(i).storage_col_name || ' = '
                         || L_insert_value_string
                         || ' when not matched then
                               insert ('
                         || L_insert_col_string
                         || 'group_id,'
                         || IO_cfa_attrib_tbl(i).storage_col_name
                         || ') values ('
                         || L_key_col_values
                         || IO_cfa_attrib_tbl(i).group_id ||','
                         || L_insert_value_string
                         || ')';
    --
    -- execute merge statement
    --
    execute immediate L_merge_statement using IN IO_cfa_attrib_tbl(i).cfa_key_val_tbl;
    --
  end loop;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END SET_CFA_BY_ATTRIB;
---------------------------------------------------------------------------------------
FUNCTION SEARCH_CFA_BY_ATTRIB(I_attrib_id                IN  CFA_ATTRIB.ATTRIB_ID%TYPE DEFAULT NULL,
                              I_code_type                IN  CODE_DETAIL.CODE_TYPE%TYPE DEFAULT NULL,
                              I_code                     IN  CODE_DETAIL.CODE%TYPE DEFAULT NULL,
                              I_xxadeo_cfa_key_val_tbl   IN  XXADEO_CFA_KEY_VAL_TBL DEFAULT NULL)
RETURN XXADEO_CFA_SEARCH_TBL IS
  --
  L_program                 VARCHAR2(64) := 'XXADEO_CFA_SQL.SEARCH_CFA_BY_ATTRIB';
  L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
  L_attribute_id            CFA_ATTRIB.ATTRIB_ID%TYPE;
  L_query_statement         VARCHAR2(32767);
  L_xxadeo_cfa_search_tbl   XXADEO_CFA_SEARCH_TBL := XXADEO_CFA_SEARCH_TBL();
  L_xxadeo_cfa_attrib_obj   XXADEO_CFA_ATTRIB_OBJ := XXADEO_CFA_ATTRIB_OBJ();
  L_xxadeo_cfa_key_val_tbl  XXADEO_CFA_KEY_VAL_TBL;
  --
  cursor C_get_key_col_rms_table is
    select ek.*
      from cfa_ext_entity_key ek
     where ek.base_rms_table = L_xxadeo_cfa_attrib_obj.base_rms_table
    order by ek.key_number;
  --
  TYPE KEY_COL_TYPE IS TABLE OF CFA_EXT_ENTITY_KEY%ROWTYPE;
  L_key_col_tbl KEY_COL_TYPE;
  --
BEGIN
  --
  -- validate input
  --
  if I_attrib_id is not null and (I_code_type is not null or I_code is not null) then
    --
    L_error_message := 'Can only search by attrib id or code_detail';
    RETURN L_xxadeo_cfa_search_tbl;
    --
  elsif I_attrib_id is null and (I_code_type is null or I_code is null) then
    --
    L_error_message := 'Attrib id or code_detail inputs are mandatories';
    RETURN L_xxadeo_cfa_search_tbl;
    --
  end if;
  --
  -- check exist code detail
  --
  if I_attrib_id is null then
    --
    open C_exist_code_detail(I_code_type => I_code_type,
                             I_code      => I_code);
    fetch C_exist_code_detail into L_attribute_id;
      --
      if C_exist_code_detail%NOTFOUND then
        --
        L_error_message := 'Code Type/Code: '|| I_code_type ||'/'|| I_code || ' does not exist in RMS CODE_DETAIL.';
        dbms_output.put_line(L_error_message);
        close C_exist_code_detail;
        RETURN L_xxadeo_cfa_search_tbl;
        --
      end if;
      --
    close C_exist_code_detail;
    --
  else
    --
    open C_exist_cfa_attrib(I_cfa_attrib_id => I_attrib_id);
    fetch C_exist_cfa_attrib into L_attribute_id;
      --
      if C_exist_cfa_attrib%NOTFOUND then
        --
        L_error_message := 'CFA Atrib ID '|| I_attrib_id || ' is not configured on RMS CFA_ATTRIB.';
        dbms_output.put_line(L_error_message);
        close C_exist_cfa_attrib;
        RETURN L_xxadeo_cfa_search_tbl;
        --
      end if;
      --
    close C_exist_cfa_attrib;
    --
  end if;
  --
  -- get attribute_id
  --
  open C_get_cfa_attrib_id(I_attribute_id => L_attribute_id,
                           I_code_type    => I_code_type,
                           I_code         => I_code);
  fetch C_get_cfa_attrib_id into L_attribute_id,
                                 L_xxadeo_cfa_attrib_obj.group_id,
                                 L_xxadeo_cfa_attrib_obj.storage_col_name,
                                 L_xxadeo_cfa_attrib_obj.storage_data_type;
    --
    if C_get_cfa_attrib_id%NOTFOUND then
      --
      L_error_message := ' There is no CFA Attrib for Code Type/Code: '|| I_code_type ||'/'|| I_code || '.';
      dbms_output.put_line(L_error_message);
      close C_get_cfa_attrib_id;
      RETURN L_xxadeo_cfa_search_tbl;
      --
    end if;
    --
  close C_get_cfa_attrib_id;
  --
  -- get custom extension table
  --
  if I_xxadeo_cfa_key_val_tbl is null or I_xxadeo_cfa_key_val_tbl.count = 0 then
    --
    open C_get_cust_ext_table(I_attribute_id => L_attribute_id);
    fetch C_get_cust_ext_table into L_xxadeo_cfa_attrib_obj.custom_ext_table,L_xxadeo_cfa_attrib_obj.base_rms_table;
      --
      if C_get_cust_ext_table%NOTFOUND then
        --
        L_error_message := 'Custom Extension table not found for CFA Attrib '||L_attribute_id;
        dbms_output.put_line(L_error_message);
        close C_get_cust_ext_table;
        RETURN L_xxadeo_cfa_search_tbl;
        --
      end if;
      --
    close C_get_cust_ext_table;
    --
  else
    --
    -- check key table size
    --
    if I_xxadeo_cfa_key_val_tbl.count > 1 then
      --
      L_error_message := 'Search only allow at max one key row';
      dbms_output.put_line(L_error_message);
      --
    end if;
    --
    open C_get_key_col_names(I_cfa_key_val_tbl => I_xxadeo_cfa_key_val_tbl);
    fetch C_get_key_col_names bulk collect into L_xxadeo_cfa_key_val_tbl;
    close C_get_key_col_names;
    --
    if L_xxadeo_cfa_key_val_tbl.count > 1 then
      --
      L_error_message := 'More than one key type in same process';
      RETURN L_xxadeo_cfa_search_tbl;
      --
    end if;
    --
    open C_get_keys_col_for_ext_table(I_attribute_id            => L_attribute_id,
                                      I_xxadeo_cfa_key_val_tbl  => I_xxadeo_cfa_key_val_tbl);
    fetch C_get_keys_col_for_ext_table into L_xxadeo_cfa_attrib_obj.custom_ext_table,L_xxadeo_cfa_attrib_obj.base_rms_table;
      --
      if C_get_keys_col_for_ext_table%NOTFOUND then
        --
        L_error_message := 'Extension Key Column(s) not exist for CFA Attrib '||L_attribute_id || ' or Key Value(s) are null.';
        dbms_output.put_line(L_error_message);
        close C_get_keys_col_for_ext_table;
        RETURN L_xxadeo_cfa_search_tbl;
        --
      end if;
      --
    close C_get_keys_col_for_ext_table;
    --
  end if;
  --
  -- get cfa value
  --
  open C_get_key_col_rms_table;
  fetch C_get_key_col_rms_table bulk collect into L_key_col_tbl;
  close C_get_key_col_rms_table;
  --
  L_query_statement := 'select new XXADEO_CFA_SEARCH_OBJ(';
  --
  for i in 1..L_key_col_tbl.count loop
    --
    L_query_statement := L_query_statement ||'key_col_'|| i || ' => ' || L_key_col_tbl(i).key_col || ',';
    --
  end loop;
  --
  L_query_statement := L_query_statement ||'group_id => group_id,';
  --
  if L_xxadeo_cfa_attrib_obj.storage_data_type != 'DATE' then
    --
    L_query_statement := L_query_statement ||'cfa_value=> ' || L_xxadeo_cfa_attrib_obj.storage_col_name || ') cfa_value'
                       || ' from ' || L_xxadeo_cfa_attrib_obj.custom_ext_table
                       || ' where ';
    --
  else
    --
    L_query_statement := L_query_statement || 'cfa_value=> to_char(' || L_xxadeo_cfa_attrib_obj.storage_col_name || ',''yyyy-mm-dd''))cfa_value'
                       || ' from ' || L_xxadeo_cfa_attrib_obj.custom_ext_table
                       || ' where ';
    --
  end if;
  --
  if I_xxadeo_cfa_key_val_tbl is not null and I_xxadeo_cfa_key_val_tbl.count > 0 then
    --
    for i in 1..I_xxadeo_cfa_key_val_tbl.count loop
      --
      if I_xxadeo_cfa_key_val_tbl(i).key_value_1 is not null then
        --
        L_query_statement := L_query_statement || I_xxadeo_cfa_key_val_tbl(i).key_col_1 || ' = '||''''|| I_xxadeo_cfa_key_val_tbl(i).key_value_1 ||'''' || ' and ';
        --
      end if;
      --
      if I_xxadeo_cfa_key_val_tbl(i).key_value_2 is not null then
        --
        L_query_statement := L_query_statement || I_xxadeo_cfa_key_val_tbl(i).key_col_2 || ' = '||''''|| I_xxadeo_cfa_key_val_tbl(i).key_value_2 ||'''' || ' and ';
        --
      end if;
      --
      if I_xxadeo_cfa_key_val_tbl(i).key_value_3 is not null then
        --
        L_query_statement := L_query_statement || I_xxadeo_cfa_key_val_tbl(i).key_col_3 || ' = '||''''|| I_xxadeo_cfa_key_val_tbl(i).key_value_3 ||'''' || ' and ';
        --
      end if;
      --
      if I_xxadeo_cfa_key_val_tbl(i).key_value_4 is not null then
        --
        L_query_statement := L_query_statement || I_xxadeo_cfa_key_val_tbl(i).key_col_4 || ' = '||''''|| I_xxadeo_cfa_key_val_tbl(i).key_value_4 ||'''' || ' and ';
        --
      end if;
      --
    end loop;
    --
  end if;
  --
  L_query_statement := L_query_statement || ' group_id = ' || L_xxadeo_cfa_attrib_obj.group_id;
  --
  -- execute query
  --
  execute immediate L_query_statement bulk collect into L_xxadeo_cfa_search_tbl;
  --
  RETURN L_xxadeo_cfa_search_tbl;
  --
EXCEPTION
  --
  WHEN NO_DATA_NEEDED THEN
    --
    RETURN L_xxadeo_cfa_search_tbl;
    --
  --
  when OTHERS then
    --
    L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    dbms_output.put_line(L_error_message);
    --
    RETURN L_xxadeo_cfa_search_tbl;
    --
END SEARCH_CFA_BY_ATTRIB;
---------------------------------------------------------------------------------------


END XXADEO_CFA_SQL;
/
