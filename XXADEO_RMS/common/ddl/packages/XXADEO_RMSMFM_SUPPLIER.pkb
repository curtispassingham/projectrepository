/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_RMSMFM_SUPPLIER.pkb
* Description:   Package that will be used by RIB to publish 
*                supplier information.
*                
* Version:       1.2
* Author:        Liliana Soraia Ferreira
* Creation Date: 23/05/2018
* Last Modified: 23/01/2019
* History:
*               1.0 - Initial version
*               1.1 - Only Primary Supplier are published
*               1.2 - [WST] Bug fix VANO1CGFC-75. 
*/
/*------------------------------------------------------------------------*/

create or replace PACKAGE BODY XXADEO_RMSMFM_SUPPLIER AS

  LP_system_options_rec SYSTEM_OPTIONS%ROWTYPE := NULL;
  PROCEDURE_ERROR exception;

  FUNCTION GET_CFA(O_msg    IN OUT XMLDOM.DOMELEMENT,
                   I_CFA    IN XXADEO_CFA_DETAILS,
                   O_status OUT VARCHAR2,
                   O_text   OUT VARCHAR2) RETURN BOOLEAN;

  FUNCTION GET_EXTOF(O_msg    IN OUT XMLDOM.DOMELEMENT,
                     I_sup    IN SUPS.Supplier%TYPE,
                     O_status OUT VARCHAR2,
                     O_text   OUT VARCHAR2) RETURN BOOLEAN;

  FUNCTION GET_VENDOR_BU(I_bu          XXADEO_BU_OU.BU%TYPE,
                         I_key_value_1 SUPS.SUPPLIER%TYPE,
                         I_sup_name    SUPS.SUP_NAME%TYPE,
                         O_msg         IN OUT XMLDOM.DOMELEMENT,
                         O_status      OUT VARCHAR2,
                         O_text        OUT VARCHAR2) RETURN BOOLEAN;

  PROCEDURE GETNXT(O_status       OUT VARCHAR2,
                   O_text         OUT VARCHAR2,
                   O_message_type OUT VARCHAR2,
                   O_supplier     OUT sups.supplier%TYPE,
                   O_addr_seq_no  OUT addr.seq_no%TYPE,
                   O_addr_type    OUT addr.addr_type%TYPE,
                   O_message      OUT CLOB);

  PROCEDURE MAKE_CREATE(O_status    OUT VARCHAR2,
                        O_text      OUT VARCHAR2,
                        O_msg       OUT CLOB,
                        I_queue_rec IN supplier_mfqueue%ROWTYPE);

  PROCEDURE DELETE_QUEUE_REC(O_status   OUT VARCHAR2,
                             O_text     OUT VARCHAR2,
                             I_supplier IN supplier_mfqueue.supplier%TYPE,
                             I_cre_del  IN NUMBER);

  /*** Program Bodies***/

  -------------------------------------------------------------------------------------------------------
  -- Name: ADDTOQ
  -- Description: Procedure responsible for insert information in supplier_mfqueue
  -------------------------------------------------------------------------------------------------------

  PROCEDURE ADDTOQ(I_message_type  IN VARCHAR2,
                   I_supplier      IN sups.supplier%TYPE,
                   I_addr_seq_no   IN addr.seq_no%TYPE,
                   I_addr_type     IN addr.addr_type%TYPE,
                   I_ret_allow_ind IN VARCHAR2,
                   I_org_unit_id   IN VARCHAR2,
                   I_message       IN CLOB,
                   O_status        OUT VARCHAR2,
                   O_text          OUT VARCHAR2) AS

  BEGIN
    -- write the message out.
    insert into supplier_mfqueue
      (seq_no,
       pub_status,
       message_type,
       supplier,
       addr_seq_no,
       addr_type,
       ret_allow_ind,
       org_unit_id,
       message)
      (select supplier_mfsequence.nextval,
              'U',
              I_message_type,
              I_supplier,
              I_addr_seq_no,
              I_addr_type,
              I_ret_allow_ind,
              I_org_unit_id,
              I_message
         from dual);
    ---
    O_status := API_CODES.SUCCESS;

  EXCEPTION
    when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_RMSMFM_SUPPLIER.ADDTOQ');

  END ADDTOQ;

  -------------------------------------------------------------------------------------------------------
  -- Name: GET_VENDOR_BU
  -- Description: Responsible for create xml for one VendorBU
  -------------------------------------------------------------------------------------------------------

  FUNCTION GET_VENDOR_BU(I_bu          XXADEO_BU_OU.BU%TYPE,
                         I_key_value_1 SUPS.SUPPLIER%TYPE,
                         I_sup_name    SUPS.SUP_NAME%TYPE,
                         O_msg         IN OUT XMLDOM.DOMELEMENT,
                         O_status      OUT VARCHAR2,
                         O_text        OUT VARCHAR2) RETURN BOOLEAN AS

    root XMLDOM.DOMELEMENT;

  BEGIN
    if not API_LIBRARY.CREATE_MESSAGE(O_status, O_text, root, VENDOR_BU_MSG) then
      return false;
    end if;

    xxadeo_rib_xml.addElement(root, 'bu', I_bu);
    xxadeo_rib_xml.addElement(root, 'payment_supplier', I_key_value_1);
    xxadeo_rib_xml.addElement(root, 'payment_supplier_name', I_sup_name);

    O_msg := root;

    O_status := API_CODES.SUCCESS;
    ---
    return true;

  EXCEPTION
    when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_RMSMFM_SUPPLIER.ADD_VENDOR_BU');

  END GET_VENDOR_BU;

 -------------------------------------------------------------------------------------------------------
  -- Name: GET_CFA
  -- Description: Responsible for create xml for one CFA
  -------------------------------------------------------------------------------------------------------

  FUNCTION GET_CFA(O_msg    IN OUT XMLDOM.DOMELEMENT,
                   I_CFA    IN XXADEO_CFA_DETAILS,
                   O_status OUT VARCHAR2,
                   O_text   OUT VARCHAR2) RETURN BOOLEAN AS

    root XMLDOM.DOMELEMENT;
  BEGIN
    if not API_LIBRARY.CREATE_MESSAGE(O_status, O_text, root, CFA_MSG) then
      return false;
    end if;

    xxadeo_rib_xml.addElement(root, 'id', I_CFA.cfa_id);
    xxadeo_rib_xml.addElement(root, 'name', I_CFA.cfa_name);
    xxadeo_rib_xml.addElement(root, 'value', I_CFA.value);
    xxadeo_rib_xml.addElement(root, 'value_date', I_CFA.value_date);

    O_msg := root;

    ---
    O_status := API_CODES.SUCCESS;
    ---
    return true;

  EXCEPTION
    when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_SUPPLIER_XML.GET_CFA');
      return FALSE;

  END GET_CFA;

 -------------------------------------------------------------------------------------------------------
  -- Name: GET_EXTOF
  -- Description: Responsible for create xml for ExtOfVendorHdrDesc
  --              Gets information needed to extension and create XML
  -------------------------------------------------------------------------------------------------------

  FUNCTION GET_EXTOF(O_msg    IN OUT XMLDOM.DOMELEMENT,
                     I_sup    IN SUPS.Supplier%TYPE,
                     O_status OUT VARCHAR2,
                     O_text   OUT VARCHAR2) RETURN BOOLEAN AS

    L_supplier_parent SUPS.SUP_NAME%TYPE;
    L_row_attr        sup_import_attr%ROWTYPE;
    L_purchase_type   sup_inv_mgmt.purchase_type%type;
    L_pickup_loc      sup_inv_mgmt.pickup_loc%type;
    L_date            sups.create_datetime%TYPE;
    root              XMLDOM.DOMELEMENT;
    L_exist           NUMBER;
    L_pas_supplier    VARCHAR2(1);
    L_attrib_id       NUMBER;

  BEGIN

    if not API_LIBRARY.CREATE_MESSAGE(O_status, O_text, root, EXTOF_MSG) then
      return false;
    end if;

    select count(sup_name)
      into L_exist
      from sups
     where supplier in
           (select supplier_parent from sups where supplier = I_sup);

    if L_exist > 0 then
      select sup_name
        into L_supplier_parent
        from sups
       where supplier in
             (select supplier_parent from sups where supplier = I_sup);
    end if;

    Select create_datetime into L_date from sups where supplier = I_sup;

    Select count(*)
      into L_exist
      from sup_import_attr
     where supplier = I_sup;

    if L_exist > 0 then
      select * into L_row_attr from sup_import_attr where supplier = I_sup;
    end if;

    Select count(*) into L_exist from sup_inv_mgmt where supplier = I_sup;

    if L_exist > 0 then
      select purchase_type, pickup_loc
        into L_purchase_type, L_pickup_loc
        from sup_inv_mgmt
       where supplier = I_sup;

    end if;

    Select value_1
      into L_attrib_id
      from xxadeo_mom_dvm
     where FUNC_AREA = 'CFA'
       and parameter = 'PAS_SUPPLIER_IND';

    L_pas_supplier := XXADEO_CFAS_UTILS.GET_CFA_VALUE_VARCHAR('SUPS',
                                                              I_sup,
                                                              L_attrib_id);

    xxadeo_rib_xml.addElement(root,
                              'supplier_parent_name',
                              L_supplier_parent);
    xxadeo_rib_xml.addElement(root, 'partner_1', L_row_attr.partner_1);
    xxadeo_rib_xml.addElement(root,
                              'partner_type_1',
                              L_row_attr.partner_type_1);
    xxadeo_rib_xml.addElement(root, 'partner_2', L_row_attr.partner_2);
    xxadeo_rib_xml.addElement(root,
                              'partner_type_2',
                              L_row_attr.partner_type_2);
    xxadeo_rib_xml.addElement(root, 'partner_3', L_row_attr.partner_3);
    xxadeo_rib_xml.addElement(root,
                              'partner_type_3',
                              L_row_attr.partner_type_3);
    xxadeo_rib_xml.addElement(root, 'purchase_type', L_purchase_type);
    xxadeo_rib_xml.addElement(root, 'pickup_lock', L_pickup_loc);
    xxadeo_rib_xml.addElement(root, 'create_date', L_date);
    xxadeo_rib_xml.addElement(root,
                              'pas_supplier_ind_supp',
                              L_pas_supplier);

    O_msg := root;

    ---
    O_status := API_CODES.SUCCESS;
    ---
    return true;

  EXCEPTION
    when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_SUPPLIER_XML.GET_EXTOF');
      return FALSE;

  END GET_EXTOF;
   -------------------------------------------------------------------------------------------------------
  -- Name: GETNXT
  -- Description: Change order of parameters to call GETNXT
  -------------------------------------------------------------------------------------------------------
 PROCEDURE GETNXT(O_status_code  OUT VARCHAR2,
                   O_error_msg    OUT VARCHAR2,
                   O_message_type OUT VARCHAR2,
                   O_message      OUT CLOB,
                   O_supplier     OUT sups.supplier%TYPE,
                   O_addr_seq_no  OUT addr.seq_no%TYPE,
                   O_addr_type    OUT addr.addr_type%TYPE) AS

  BEGIN
    GETNXT(O_status_code,
           O_error_msg,
           O_message_type,
           O_supplier,
           O_addr_seq_no,
           O_addr_type,
           O_message);

  EXCEPTION
    when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_RMSMFM_SUPPLIER.GETNXT');
  END GETNXT;

   -------------------------------------------------------------------------------------------------------
  -- Name: GETNXT
  -- Description: Get next message that will be published
  --            for publish the message must comply:
  --        - if message_type like VEND_ADD:
  --          * if supplier not have addr '04' or '06'  delete all messages except VEND_ADD;
  --                - Verify if supplier have addr '04' or '06'
  --          * if have booths wait for correction;
  --                    * otherwise publish the message  (if message type != VEND_CRE then messa_type = 'VEND_ADEO')
  --          * if supplier has only addr '06' delete all messages except VEND_ADD;
  -------------------------------------------------------------------------------------------------------
  PROCEDURE GETNXT(O_status       OUT VARCHAR2,
                   O_text         OUT VARCHAR2,
                   O_message_type OUT VARCHAR2,
                   O_supplier     OUT sups.supplier%TYPE,
                   O_addr_seq_no  OUT addr.seq_no%TYPE,
                   O_addr_type    OUT addr.addr_type%TYPE,
                   O_message      OUT CLOB) AS
  
    L_queue_rec supplier_mfqueue%ROWTYPE;
    L_exist     number;
    L_exist_04  number;
    L_exist_06  number;
    ---
    cursor C_QUEUE is
      select *
        from supplier_mfqueue
       where pub_status = 'U'
       order by seq_no;
  
  BEGIN
  
    if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_text, LP_system_options_rec) =
       FALSE then
      raise PROGRAM_ERROR;
    end if;
  
    open C_QUEUE;
    ---
    LOOP
      fetch C_QUEUE
        into L_queue_rec;
      ---
      if C_QUEUE%NOTFOUND then
        O_status := API_CODES.NO_MSG;
        exit;
      end if;
      ---
      if L_queue_rec.message_type like VEND_ADD then
        select count(*)
          into L_exist
          from addr
         where module = 'SUPP'
           and (addr_type = '04' or addr_type = '06')
           and key_value_1 = to_char(L_queue_rec.supplier);
       
       if L_exist = 0 then
          DELETE_QUEUE_REC(O_status, O_text, L_queue_rec.supplier, 0);  
          
          if O_status = API_CODES.UNHANDLED_ERROR then
             raise PROGRAM_ERROR;
          end if;
          
          CONTINUE;
        end if;      
      end if;
      
      select count(*)
        into L_exist_04
        from addr
       where module = 'SUPP'
         and addr_type = '04'
         and key_value_1 = to_char(L_queue_rec.supplier);
         
      select count(*)
        into L_exist_06
        from addr
       where module = 'SUPP'
         and addr_type = '06'
         and key_value_1 = to_char(L_queue_rec.supplier);
         
       if L_exist_04 != 0 and L_exist_06 !=0 then
          CONTINUE;
       end if;
         
    
      if L_exist_04 = 0  and L_exist_06 = 0 then -- wait for adress    
        CONTINUE;
        
      elsif L_exist_04 > 0 then -- ONLY A 04 ADDRESS MUST EXIST FOR THE MESSAGE TO BE CREATED      
      
        MAKE_CREATE(O_status, O_text, O_message, L_queue_rec);
      
        if L_queue_rec.Message_Type = VEND_ADD then
          O_message_type := VEND_ADD;
        else
          Select count(*)
            into L_exist
            from supplier_mfqueue
           where supplier = L_queue_rec.supplier
             and message_type = VEND_ADD;
        
          if L_exist > 0 then
            O_message_type := VEND_ADD;
          else
            O_message_type := VEND_ADEO;
          end if;
        end if;
      
        if O_status = API_CODES.UNHANDLED_ERROR then
          raise PROGRAM_ERROR;
        end if;
      
        DELETE_QUEUE_REC(O_status, O_text, L_queue_rec.supplier, 1);
      
        if O_status = API_CODES.UNHANDLED_ERROR then
          raise PROGRAM_ERROR;
        end if;
        exit;
     
     elsif L_exist_06 > 0 then -- IF ONLY A 06 ADDRESS EXISTS, THEN DELETE ALL MESSAGES
                   
        DELETE_QUEUE_REC(O_status, O_text, L_queue_rec.supplier, 2);
      
        if O_status = API_CODES.UNHANDLED_ERROR then
          raise PROGRAM_ERROR;
        end if;
      exit;        
     
      end if;
    
    END LOOP;
  
    ---
    close C_QUEUE;
  
  EXCEPTION
    when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_RMSMFM_SUPPLIER.GETNXT');
  END GETNXT;
  -----------------------------------------------------------------------------------

   -------------------------------------------------------------------------------------------------------
  -- Name: MAKE_CREATE
  -- Description: This procedure is responsible to create XML of message
  -------------------------------------------------------------------------------------------------------

  PROCEDURE MAKE_CREATE(O_status    OUT VARCHAR2,
                        O_text      OUT VARCHAR2,
                        O_msg       OUT CLOB,
                        I_queue_rec IN supplier_mfqueue%ROWTYPE) IS

    cre_root       xmldom.DOMElement;
    hdr_root       xmldom.DOMElement;
    upd_root       xmldom.DOMElement;
    hdr_elem       xmldom.DOMElement;
    addr_elem      xmldom.DOMElement;
    pou_elem       xmldom.DOMElement;
    extof_root     xmldom.DOMElement;
    vendor_bu_root xmldom.DOMElement;
    cfa_root       xmldom.DOMElement;
    cfa_elem       xmldom.DOMElement;
    extof_elem     xmldom.DOMElement;
    extofBU        xmldom.DOMElement;
    L_msg          Clob;
    L_sup_row      SUPS%ROWTYPE;
    L_sup_rec      supplier_mfqueue%ROWTYPE;

    L_list_cfas XXADEO_CFA_DETAILS_TBL;

    cursor C_GET_SUP_MESSAGE is
      select *
        from supplier_mfqueue
       where supplier = I_queue_rec.supplier
         and seq_no = I_queue_rec.seq_no;

    cursor C_GET_ADDR is
      select *
        from addr
       where key_value_1 = to_char(I_queue_rec.supplier)
         and module = 'SUPP'
         and primary_addr_ind = 'Y'
       order by seq_no asc;

    cursor C_GET_POU is
      select * from partner_org_unit where partner = I_queue_rec.supplier;

  BEGIN

    open C_GET_SUP_MESSAGE;
    fetch C_GET_SUP_MESSAGE
      into L_sup_rec;
    close C_GET_SUP_MESSAGE;
    ---

    if not
        API_LIBRARY.CREATE_MESSAGE(O_status, O_text, cre_root, VEND_DESC_MSG) then
      return;
    end if;

    if I_queue_rec.Message_Type = VEND_ADD then
      -- Read the Vendor Header Root into a DOM
      hdr_root := API_LIBRARY.readRoot(L_sup_rec.message, VEND_HDR_DESC_MSG);

      -- Create the VendorHdrDesc subnode on the root
      hdr_elem := rib_xml.addElement(cre_root, VEND_HDR_DESC_MSG);
      ---
      -- entension
      if not GET_EXTOF(extof_root, I_queue_rec.supplier, O_status, O_text) then
        return;

      end if;

      for vendorbu in (select bu, supplier, sup_name
                         from sups
                         join addr
                           on key_value_1 = supplier
                         join partner_org_unit pou
                           ON partner = key_value_1
                         join xxadeo_bu_ou
                           ON ou = pou.org_unit_id
                        where module = 'SUPP'
                          and pou.primary_pay_site = 'Y'
                          and addr_type = 06
                          and supplier_parent =
                              (select supplier_parent
                                 from sups
                                where supplier = L_sup_rec.supplier)
                          and xxadeo_bu_ou.bu in
                              (select bu
                                 from xxadeo_bu_ou
                                 left join partner_org_unit pou
                                   on pou.org_unit_id = ou
                                where partner = L_sup_rec.supplier)) loop

        if not GET_VENDOR_BU(vendorbu.bu,
                             vendorbu.supplier,
                             vendorbu.sup_name,
                             vendor_bu_root,
                             O_status,
                             O_text) then
          return;

        end if;
        -- add VendorBu node to extof node
        extofbu := xxadeo_rib_xml.addElement(extof_root, VENDOR_BU_MSG);
        -- add information to VendorBu node
        rib_xml.addElementContents(extofbu, vendor_bu_root);

      end loop;

      extof_elem := xxadeo_rib_xml.addElement(hdr_root, EXTOF_MSG);
      -- add namespace to extOf
      dbms_xmldom.setattribute(extof_elem,
                               'xmlns:ns4',
                               xxadeo_rib_xml.getXMLNamespaceURL || '/' ||
                               EXTOF_MSG || '/v1');

      -- get CFAS
      XXADEO_CFAS_UTILS.GET_CFAS_EXIST('SUPS',
                                       I_queue_rec.supplier,
                                       L_list_cfas,
                                       O_status,
                                       O_text);

      if O_status = API_CODES.UNHANDLED_ERROR then
        raise PROGRAM_ERROR;
      end if;
      -- create node for each cfa
      if L_list_cfas is not null and L_list_cfas.count() > 0 then

        for i in 1 .. L_list_cfas.count() loop
          if not GET_CFA(cfa_root, L_list_cfas(i), O_status, O_msg) then
            return;
          end if;

          if O_status = API_CODES.UNHANDLED_ERROR then
            raise PROGRAM_ERROR;
          end if;

          cfa_elem := xxadeo_rib_xml.addElement(extof_root, CFA_MSG);

          rib_xml.addElementContents(cfa_elem, cfa_root);

        end loop;

      end if;
      -- add information for extension
      rib_xml.addElementContents(extof_elem, extof_root);
      -- add vendorhdrinfo to node
      rib_xml.addElementContents(hdr_elem, hdr_root);

      -- Add addr nodes
      for addr in C_GET_ADDR loop

        supplier_xml.BUILD_ADDRESS(addr, 'C', L_msg, O_status, O_text);

        upd_root := API_LIBRARY.readRoot(L_msg, VEND_ADDR_DESC_MSG);
        ---
        addr_elem := rib_xml.addElement(cre_root, VEND_ADDR_DESC_MSG);
        ---
        rib_xml.addElementContents(addr_elem, upd_root);

      end loop;

      -- Add the org unit node
      for pou in C_GET_POU loop

        supplier_xml.BUILD_ORG_UNIT(pou, 'C', L_msg, O_status, O_text);

        --dbms_output.put_line(L_msg);
        upd_root := API_LIBRARY.readRoot(L_msg, VEND_OU_DESC_MSG);
        ---
        pou_elem := rib_xml.addElement(cre_root, VEND_OU_DESC_MSG);
        ---
        rib_xml.addElementContents(pou_elem, upd_root);
        ---
      end loop;

    else
      -- message is not VendorCre

      -- create a supplier xml message
      Select *
        into L_sup_row
        from sups
       where supplier = I_queue_rec.supplier;

      supplier_xml.build_supplier(L_sup_row,
                                  'C', -- event
                                  L_msg, -- output xml
                                  O_status,
                                  O_text);

      ---
      if O_status = API_CODES.UNHANDLED_ERROR then
        raise PROGRAM_ERROR;
      end if;
      ---
      -- create the supplier CLOB containing only the supplier number
      if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                        O_text,
                                        cre_root,
                                        VEND_DESC_MSG) then
        return;
      end if;

      -- Read the Vendor Header Root into a DOM
      hdr_root := API_LIBRARY.readRoot(L_msg, VEND_HDR_DESC_MSG);

      -- Create the VendorHdrDesc subnode on the root
      hdr_elem := rib_xml.addElement(cre_root, VEND_HDR_DESC_MSG);
      ---
      -- entension
      if not GET_EXTOF(extof_root, I_queue_rec.supplier, O_status, O_text) then
        return;

      end if;

      for vendorbu in (select bu, supplier, sup_name
                         from sups
                         join addr
                           on key_value_1 = supplier
                         join partner_org_unit pou
                           ON partner = key_value_1
                         join xxadeo_bu_ou
                           ON ou = pou.org_unit_id
                        where module = 'SUPP'
                          and pou.primary_pay_site = 'Y'
                          and addr_type = 06
                          and supplier_parent =
                              (select supplier_parent
                                 from sups
                                where supplier = L_sup_rec.supplier)
                          and xxadeo_bu_ou.bu in
                              (select bu
                                 from xxadeo_bu_ou
                                 left join partner_org_unit pou
                                   on pou.org_unit_id = ou
                                where partner = L_sup_rec.supplier)) loop

        if not GET_VENDOR_BU(vendorbu.bu,
                             vendorbu.supplier,
                             vendorbu.sup_name,
                             vendor_bu_root,
                             O_status,
                             O_text) then
          return;

        end if;
        -- add VendorBu node to extof node
        extofbu := xxadeo_rib_xml.addElement(extof_root, VENDOR_BU_MSG);
        -- add information to VendorBu node
        rib_xml.addElementContents(extofbu, vendor_bu_root);

      end loop;

      extof_elem := xxadeo_rib_xml.addElement(hdr_root, EXTOF_MSG);
      -- add namespace to extOf
      dbms_xmldom.setattribute(extof_elem,
                               'xmlns:ns4',
                               xxadeo_rib_xml.getXMLNamespaceURL || '/' ||
                               EXTOF_MSG || '/v1');

      /*-- add VendorBu node to extof node
      extofbu := xxadeo_rib_xml.addElement(extof_root, 'VendorBU');
      -- add information to VendorBu node
      rib_xml.addElementContents(extofbu, vendor_bu_root);*/

      -- get CFAS
      XXADEO_CFAS_UTILS.GET_CFAS_EXIST('SUPS',
                                       I_queue_rec.supplier,
                                       L_list_cfas,
                                       O_status,
                                       O_text);

      if O_status = API_CODES.UNHANDLED_ERROR then
        raise PROGRAM_ERROR;
      end if;
      -- create node for each cfa
      if L_list_cfas is not null and L_list_cfas.count() > 0 then

        for i in 1 .. L_list_cfas.count() loop
          if not GET_CFA(cfa_root, L_list_cfas(i), O_status, O_msg) then
            return;
          end if;

          if O_status = API_CODES.UNHANDLED_ERROR then
            raise PROGRAM_ERROR;
          end if;

          cfa_elem := xxadeo_rib_xml.addElement(extof_root, CFA_MSG);

          rib_xml.addElementContents(cfa_elem, cfa_root);

        end loop;

      end if;
      -- add information for extension
      rib_xml.addElementContents(extof_elem, extof_root);
      -- add vendorhdrinfo to node
      rib_xml.addElementContents(hdr_elem, hdr_root);

      -- Add addr nodes
      for addr in C_GET_ADDR loop

        supplier_xml.BUILD_ADDRESS(addr, 'C', L_msg, O_status, O_text);

        upd_root := API_LIBRARY.readRoot(L_msg, VEND_ADDR_DESC_MSG);
        ---
        addr_elem := rib_xml.addElement(cre_root, VEND_ADDR_DESC_MSG);
        ---
        rib_xml.addElementContents(addr_elem, upd_root);

      end loop;

      -- Add the org unit node
      for pou in C_GET_POU loop

        supplier_xml.BUILD_ORG_UNIT(pou, 'C', L_msg, O_status, O_text);

        upd_root := API_LIBRARY.readRoot(L_msg, VEND_OU_DESC_MSG);
        ---
        pou_elem := rib_xml.addElement(cre_root, VEND_OU_DESC_MSG);
        ---
        rib_xml.addElementContents(pou_elem, upd_root);
        ---
      end loop;

    end if;

    if API_LIBRARY.WRITE_DOCUMENT(O_status, O_text, O_msg, cre_root) =
       FALSE then
      return;
    end if;

    O_status := API_CODES.NEW_MSG;
    ---

  EXCEPTION
    when others then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_RMSMFM_SUPPLIER.MAKE_CREATE');

  END MAKE_CREATE;


   -------------------------------------------------------------------------------------------------------
  -- Name: DELETE_QUEUE_REC
  -- Description: THis procedure is responsible for purge supplier_mfqueue
  --              If I_cre_del =0 purge all records for this supplier where message_type <> VEND_ADD
  --              If I_cre_del =1 purge all records for this supplier and publish the constructed message
  --              If I_cre_del =2 purge all records for this supplier without publishing any messages
  --              otherwise purge all records for this supplier
  -------------------------------------------------------------------------------------------------------
 PROCEDURE DELETE_QUEUE_REC(O_status   OUT VARCHAR2,
                             O_text     OUT VARCHAR2,
                             I_supplier IN supplier_mfqueue.supplier%TYPE,
                             I_cre_del  IN NUMBER) is

  BEGIN

    if I_cre_del = 0 then
      delete from supplier_mfqueue
       where supplier = I_supplier
         and message_type <> VEND_ADD;
         
         O_status := API_CODES.NEW_MSG;
         
    else
        if I_cre_del = 1 then
            delete from supplier_mfqueue where supplier = I_supplier; 
      
            O_status := API_CODES.NEW_MSG;
        else      
            delete from supplier_mfqueue where supplier = I_supplier; 
      
            O_status := API_CODES.NO_MSG;
        end if;
    end if;

  

  EXCEPTION
    when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'XXADEO_RMSMFM_SUPPLIER.DELETE_QUEUE_REC');
  END DELETE_QUEUE_REC;

--------------------------------------------------------------------------------
END XXADEO_RMSMFM_SUPPLIER;
/