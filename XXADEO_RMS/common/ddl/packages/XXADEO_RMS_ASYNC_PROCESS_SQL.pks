CREATE OR REPLACE PACKAGE XXADEO_RMS_ASYNC_PROCESS_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_NEW_ITEM_LOC
-- Purpose      : This function puts the rms_async_id associated with a new-item-loc event
--                to the asynchronous queue and writes the rms_async_status/retry table to
--                track the status of the async job.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_NEW_ITEM_LOC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_rms_async_id        IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_NEW_ITEM_LOC_RETRY
-- Purpose      : This function puts the rms_async_id associated with a new-item-loc event
--                to the asynchronous queue again for re-processing. It is invoked through
--                the async job form.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_NEW_ITEM_LOC_RETRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_rms_async_id  IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_NEW_ITEM_LOC(context  raw,
                              reginfo  sys.aq$_reg_info,
                              descr    sys.aq$_descriptor,
                              payload  raw,
                              payloadl number);
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_UPDATE_ITEM_LOC
-- Purpose      : This function puts the rms_async_id associated with a update-item-loc event
--                to the asynchronous queue and writes the rms_async_status/retry table to
--                track the status of the async job.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_UPDATE_ITEM_LOC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_rms_async_id        IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_UPDATE_ITEM_LOC_RETRY
-- Purpose      : This function puts the rms_async_id associated with a update-item-loc event
--                to the asynchronous queue again for re-processing. It is invoked through
--                the async job form.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_UPDATE_ITEM_LOC_RETRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_rms_async_id  IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_UPDATE_ITEM_LOC(context  raw,
                                 reginfo  sys.aq$_reg_info,
                                 descr    sys.aq$_descriptor,
                                 payload  raw,
                                 payloadl number);
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_STORE_ADD
-- Purpose      : This function puts the rms_async_id associated with a store_add event
--                to the asynchronous queue and writes the rms_async_status/retry table to
--                track the status of the async job.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_STORE_ADD(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_rms_async_id        IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_STORE_ADD_RETRY
-- Purpose      : This function puts the rms_async_id associated with a store_add event
--                to the asynchronous queue again for re-processing. It is invoked through
--                the async job form.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_STORE_ADD_RETRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_rms_async_id  IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_STORE_ADD(context  raw,
                              reginfo  sys.aq$_reg_info,
                              descr    sys.aq$_descriptor,
                              payload  raw,
                              payloadl number);
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_WH_ADD
-- Purpose      : This function puts the rms_async_id associated with a WH_add event
--                to the asynchronous queue and writes the rms_async_status/retry table to
--                track the status of the async job.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_WH_ADD(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_rms_async_id        IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_WH_ADD_RETRY
-- Purpose      : This function puts the rms_async_id associated with a wh_add event
--                to the asynchronous queue again for re-processing. It is invoked through
--                the async job form.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_WH_ADD_RETRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_rms_async_id  IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_WH_ADD(context  raw,
                              reginfo  sys.aq$_reg_info,
                              descr    sys.aq$_descriptor,
                              payload  raw,
                              payloadl number);
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_STKLGR_INSERT
-- Purpose      : This function puts the rms_async_id associated with a stklgr_insert event
--                to the asynchronous queue and writes the rms_async_status/retry table to
--                track the status of the async job.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_STKLGR_INSERT(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_rms_async_id        IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_STKLGR_INSERT_RETRY
-- Purpose      : This function puts the rms_async_id associated with a stklgr_insert event
--                to the asynchronous queue again for re-processing. It is invoked through
--                the async job form.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_STKLGR_INSERT_RETRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_rms_async_id  IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_STKLGR_INSERT(context  raw,
                              reginfo  sys.aq$_reg_info,
                              descr    sys.aq$_descriptor,
                              payload  raw,
                              payloadl number);
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_ITEM_INDUCT
-- Purpose      : This function puts the rms_async_id associated with a ITEM_INDUCT event
--                to the asynchronous queue and writes the rms_async_status/retry table to
--                track the status of the async job.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_ITEM_INDUCT(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_rms_async_id        IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_ITEM_INDUCT_RETRY
-- Purpose      : This function puts the rms_async_id associated with a ITEM_INDUCT event
--                to the asynchronous queue again for re-processing. It is invoked through
--                the async job form.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_ITEM_INDUCT_RETRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_rms_async_id  IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_ITEM_INDUCT(context  raw,
                              reginfo  sys.aq$_reg_info,
                              descr    sys.aq$_descriptor,
                              payload  raw,
                              payloadl number);
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_PO_INDUCT
-- Purpose      : This function puts the rms_async_id associated with a PO_INDUCT event
--                to the asynchronous queue and writes the rms_async_status/retry table to
--                track the status of the async job.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_PO_INDUCT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_rms_async_id    IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Function Name: ENQUEUE_PO_INDUCT_RETRY
-- Purpose      : This function puts the rms_async_id associated with a PO_INDUCT event
--                to the asynchronous queue again for re-processing. It is invoked through
--                the async job form.
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_PO_INDUCT_RETRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_rms_async_id    IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_PO_INDUCT(context    raw,
                           reginfo    sys.aq$_reg_info,
                           descr      sys.aq$_descriptor,
                           payload    raw,
                           payloadl   number);
------------------------------------------------------------------------------------------------
END XXADEO_RMS_ASYNC_PROCESS_SQL;
/
