CREATE OR REPLACE PACKAGE XXADEO_ITEM_LIFE_CYCLE_SQL AUTHID CURRENT_USER AS

/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Jorge Agra                                                   */
/*                                                                            */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package for RB107/RB118                                      */
/******************************************************************************/

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-09-26 - Jorge Agra - Reset to IS-INIT suppliers in IS-STOP with stop  */
/*                           date null                                        */
/* 2018-09-27 - Jorge Agra - BUG#492 Missing check for CFG_ITEM orderable_ind */
/* 2018-11-26 - Jorge Agra - ORACR_00257 Use CFA ECOTAX_ELC_COMP instead of   */
/*                           EXP_CATEGORY to identify ecotaxes in IB-ASI-02   */
/*                           Global vars                                      */
/******************************************************************************/

--------------------------------------------------------------------------------
-- Public Constants
--------------------------------------------------------------------------------

CONST$PackageName                     CONSTANT VARCHAR2(30)   := 'XXADEO_ITEM_LIFE_CYCLE_SQL';

CONST$ERR_FILLCFACOL                  CONSTANT NUMBER         := -20101;
CONST$ERR_MOMDVMKEYBU                 CONSTANT NUMBER         := -20102;

CONST$UDAKEY_LifeCycleStatus          CONSTANT VARCHAR2(30)   := 'LIFECYCLE_STATUS';
CONST$UDAKEY_BUSubscription           CONSTANT VARCHAR2(30)   := 'BU_SUBSCRIPTION';
CONST$UDAKEY_SalesStartDate           CONSTANT VARCHAR2(30)   := 'SALES_START_DATE';
CONST$UDAKEY_SalesEndDate             CONSTANT VARCHAR2(30)   := 'SALES_END_DATE';

CONST$CFAKEY_LnkStaItSup              CONSTANT VARCHAR2(30)   := 'LINK_STATUS_ITEM_SUPP';

-- CFA LMxx_ACTIF_DDATE_ITEM
CONST$CFAKEY_ActvDDt                  CONSTANT VARCHAR2(30)   := 'ACTIF_DDATE_ITEM';

CONST$CFAKEY_ContrctSignSupp          CONSTANT VARCHAR2(30)   := 'SIGNED_K_IND_SUPP';
CONST$CFAKEY_StatusQCSupp             CONSTANT VARCHAR2(30)   := 'STATUS_QC_COMP_ITEM_SUPP';
CONST$CFAKEY_DepartmentSupp           CONSTANT VARCHAR2(30)   := 'DEPARTMENT_SUPP';

CONST$CFAKEY_LnkTermDtItemSupp        CONSTANT VARCHAR2(30)   := 'LINK_TERM_DT_ITEM_SUPP';

-- CFA LMxx_ABON_START_DATE_ITEM
CONST$CFAKEY_AbonStartDate            CONSTANT VARCHAR2(30)   := 'ABON_START_DATE_ITEM';
-- CFA LMxx_ABON_END_DATE_ITEM
CONST$CFAKEY_AbonEndDate              CONSTANT VARCHAR2(30)   := 'ABON_END_DATE_ITEM';
-- CFA LMxx_ACTIF_STATUS_DATE_ITEM
CONST$CFAKEY_ActifStatusDate          CONSTANT VARCHAR2(30)   := 'ACTIF_STATUS_DATE_ITEM';

CONST$UDAKEY_ItemType                 CONSTANT VARCHAR2(30)   := 'ITEM_TYPE';
CONST$UDAKEY_ItemSubType              CONSTANT VARCHAR2(30)   := 'ITEM_SUBTYPE';

-- code head/detail stuff
CONST$CODE_EcopartCFAS                CONSTANT VARCHAR2(30)   := 'ECOP';

CONST$UDAKEY_BASACompleteRef          CONSTANT VARCHAR2(30)   := 'BASA_COMPLETE_REF';

CONST$CFAKEY_EcotaxElcComp            CONSTANT VARCHAR2(30)   := 'ECOTAX_ELC_COMP';

--------------------------------------------------------------------------------
-- Public Types
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Public Globals
--------------------------------------------------------------------------------

GV_Debug                            boolean := FALSE;

-- Valores do lyfecycle ITEM/BU
GV_UDAVAL_LFCY_status$init          number;
GV_UDAVAL_LFCY_status$asi           number;
GV_UDAVAL_LFCY_status$acom          number;
GV_UDAVAL_LFCY_status$disc          number;
GV_UDAVAL_LFCY_status$del           number;

-- BU Subscription UDA_ID
GV_UDAID_BUSubscription             number := 750;

-- LINK_STATUS_ITEM_SUPP
GV_CFACOL_LnkStaItSup         varchar2(30)  := 'VARCHAR2_1';
GV_CFAGRP_LnkStaItSup         number(2,0)   := 48;
--GV_CFAID_LnkStaItSup          number(5,0)   := 483;
GV_CFAVAL_LnkStaItSup$Ini     varchar2(10)  := '1';
GV_CFAVAL_LnkStaItSup$Act     varchar2(10)  := '2';
GV_CFAVAL_LnkStaItSup$Stop    varchar2(10)  := '3';

-- CFA LMXX_ACTIF_DDATE_ITEM
GV_CFACOL_ActvDDt               VARCHAR2(30)  := 'DATE_24';

-- CFA LMXX_ACTIF_DDATE_ITEM
GV_CFACOL_StatusQC              VARCHAR2(30)  := 'VARCHAR2_1';

GV_VAL_PurchasingSuppSite       VARCHAR2(2)   := '04';
GV_VAL_PaymentSuppSite          VARCHAR2(2)   := '06';

-- CFA SIGNED_K_IND_SUPP
GV_CFACOL_ContrctSignSupp       VARCHAR2(30)  := 'VARCHAR2_1';

-- CFA STATUS_QC_COMP_ITEM_SUPP
GV_CFACOL_StatusQCSupp          VARCHAR2(30)  := 'VARCHAR2_1';
GV_CFAVAL_StatusQCSuppPre       VARCHAR2(30)  := '(''45'',''50'',''55'',''60'',''70'')';
GV_CFAVAL_StatusQCSuppCert      VARCHAR2(30)  := '(''60'',''70'')';

-- CFA DEPARTMENT_SUPP
GV_CFACOL_DepartmentSupp        VARCHAR2(30)  := 'NUMBER_11';

-- CFA LINK_TERM_DT_ITEM_SUPP
GV_CFACOL_LnkTermDtItemSupp     VARCHAR2(30)   := 'DATE_22';

-- CFA LMx_ABON_START_DATE_ITEM
GV_CFACOL_AbonStartDate         VARCHAR2(30)   := 'DATE_21';
-- CFA LMx_ABON_END_DATE_ITEM
GV_CFACOL_AbonEndDate           VARCHAR2(30)   := 'DATE_23';
-- CFA LMxx_ACTIF_STATUS_DATE_ITEM
GV_CFACOL_ActifStatusDate       VARCHAR2(30)   := 'DATE_22';


GV_CFAVAL_BasaCompleteRefYes    NUMBER         := 1;
GV_CFAVAL_BasaCompleteRefNo     NUMBER         := 2;

GV_CFACOL_EcotaxElcComp         VARCHAR2(30)   := 'NUMBER_11';
GV_CFAGRP_EcotaxElcComp         NUMBER(5,0)    := 60;

--------------------------------------------------------------------------------
-- Public globals (debug)
--------------------------------------------------------------------------------

G_debug_item_ind    boolean := FALSE;
G_debug_cond_ind    boolean := FALSE;
G_debug_item        xxadeo_lfcy_work.item%type := null;
G_debug_entity      xxadeo_lfcy_work.lfcy_entity%type := null;
G_debug_entity_type xxadeo_lfcy_work.lfcy_entity_type%type := null;

--------------------------------------------------------------------------------
-- Public Functions
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--Function Name : DBG
--Purpose       : Debug
----------------------------------------------------------------------------------------------------------------------------------------------------------------
procedure DBG(I_program varchar2, I_message varchar2);
--------------------------------------------------------------------------------
--Function Name : PRE_PROCESS
--Purpose       : pre-processing
----------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION PRE_PROCESS(O_error_message    IN OUT VARCHAR2)
  RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : PROCESS
--Purpose       : Main process preparation
--------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message IN OUT  VARCHAR2,
                  I_item          IN      item_master.item%TYPE default null,
                  I_entity_type   IN      xxadeo_lfcy_work.lfcy_entity_type%type default null,
                  I_entity        IN      xxadeo_lfcy_work.lfcy_entity%TYPE default null,
                  I_cleanup       IN      boolean default TRUE,
                  I_loop          IN      boolean default TRUE)
  RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : PROCESS_DEPT
--Purpose       : Main process preparation, based on DEPT segmentation
--                in RESTART_CONTROL
--------------------------------------------------------------------------------
FUNCTION PROCESS_DEPTS(O_error_message IN OUT  VARCHAR2,
                       I_num_threads   IN      varchar2 default null,
                       I_thread_val    IN      varchar2 default null,
                       I_dept          IN      item_master.dept%TYPE default 0,
                       I_cleanup       IN      boolean default TRUE,
                       I_loop          IN      boolean default TRUE)
  RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : PROCESS_CLEANUP
--Purpose       : Clear all process execution control
--                Is always called from pre_process.
--------------------------------------------------------------------------------
FUNCTION PROCESS_CLEANUP( O_error_message IN OUT  VARCHAR2, I_pid number default null)
  RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : PROCESS_EXEC
--Purpose       : Main process execution
----------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_EXEC(O_error_message   IN OUT  VARCHAR2,
                      I_pid             IN      number,
                      I_loop            IN      boolean,
                      I_single_item     IN      boolean)
  RETURN BOOLEAN;
--------------------------------------------------------------------------------
END XXADEO_ITEM_LIFE_CYCLE_SQL;
/


