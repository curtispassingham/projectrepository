--------------------------------------------------------
--  DDL for Package XXADEO_SUPS_INBOUND
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE PACKAGE XXADEO_SUPS_INBOUND AS 
------------------------------------------------------------------------------------
--- Declare private procedure and functions.
------------------------------------------------------------------------------------
--- Public Function Name  : SUPS_PURGE_HIST
--- Purpose               :
---    Purges the records from the supplier/supsite staging tables based on the
---    number of days passed as a parameter (I_days_number)
---
------------------------------------------------------------------------------------

FUNCTION SUPS_PURGE_HIST(O_error_message            IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                         I_days_number              IN            INTEGER)
RETURN BOOLEAN;

------------------------------------------------------------------------------------
--- Public Function Name  : MARK_DISCARD
--- Purpose               :
---     Marks records that are 'E' and for which a newer version has been loaded to the
---     Staging tables
------------------------------------------------------------------------------------

FUNCTION MARK_DISCARD(O_error_message               IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN;

------------------------------------------------------------------------------------
--- Public Function Name  : PROCESS_RECORDS
--- Purpose               :
---    Creates a supplier object and passes it to the validation API which
---      processes the records marked as 'N' in the supplier staging tables and
---      validates them. If successful, records are marked as 'P'rocessed,
---      otherwise as  'E'rror
------------------------------------------------------------------------------------
FUNCTION PROCESS_RECORDS(O_error_message            IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN;

------------------------------------------------------------------------------------
--- Function Name         : CONSUME
--- Purpose               : Consumes the supplier object and passes it to the validation API
---                         Returns qn error message.
------------------------------------------------------------------------------------
FUNCTION CONSUME(I_serviceoperationcontext         IN OUT        "RIB_ServiceOpContext_REC",
                 I_supexist                        IN             VARCHAR2,
                 I_businessobject                   IN            "XXADEO_SupplierDesc_REC",
                 O_serviceoperationstatus           OUT           "RIB_ServiceOpStatus_REC",
                 O_businessobject                   OUT           "XXADEO_SupplierRef_REC",
                 O_Error_Message                    OUT            RTK_ERRORS.RTK_TEXT%TYPE)
RETURN VARCHAR2;

END XXADEO_SUPS_INBOUND;

/