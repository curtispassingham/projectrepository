CREATE OR REPLACE PACKAGE BODY XXADEO_CUSTOM_CONFIG_WRP_SQL AS
   
--------------------------------------------------------------------------------
FUNCTION RUN_SUPPLIER_APPROVE_WRP(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  IO_custom_obj_rec IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(64) := 'XXADEO_CUSTOM_CONFIG_WRP_SQL.RUN_SUPPLIER_APPROVE_WRP';
  --
  L_execute_string VARCHAR2(2000);
  L_return         VARCHAR2(1);
  --
  cursor C_get_functions is
    select decode(package_name,
                  null,
                  schema_name||'.'||function_name,
                  schema_name||'.'||package_name||'.'||function_name) function_name
      from custom_pkg_config
     where function_key = IO_custom_obj_rec.function_key
       and call_seq_no  > 1
     order by call_seq_no asc;
  --
BEGIN
  --
  for i in C_get_functions loop
    --
    L_execute_string := 'begin if '||i.function_name
                        ||' (:O_error_message, :IO_custom_obj_rec) = FALSE then :L_return := ''N''; end if; end;';
    --
    execute immediate L_execute_string using IN OUT O_error_message,
                                             IN OUT IO_custom_obj_rec,
                                             OUT L_return;
    --
    if L_return = 'N' then
      --
      RETURN FALSE;
      --
    end if;
    --
  end loop;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END RUN_SUPPLIER_APPROVE_WRP;
--------------------------------------------------------------------------------
FUNCTION RUN_SYSTEM_RULES_WRP(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                              IO_custom_obj_rec IN OUT XXADEO_CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(64) := 'XXADEO_CUSTOM_CONFIG_WRP_SQL.RUN_SYSTEM_RULES_WRP';
  --
  L_execute_string VARCHAR2(2000);
  L_return         VARCHAR2(1);
  --
  cursor C_get_functions is
    select decode(asr.package_name,
                  null,
                  asr.function_name,
                  asr.package_name||'.'||asr.function_name) function_name
      from xxadeo_system_rules asr
     where asr.active_ind = 'Y'
       and asr.func_area  = IO_custom_obj_rec.function_key
     order by call_seq_no asc;
  --
BEGIN
  --
  --Return error if  CUSTOM_OBJ_REC.Function_key is null or call_seq_no is NULL
  --
   if IO_custom_obj_rec.function_key is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'rule_id', NULL, NULL);
      return FALSE;
   end if;
  --
  for i in C_get_functions loop
    --
    L_execute_string := 'begin if '||i.function_name
                        ||' (:O_error_message, :IO_custom_obj_rec) = FALSE then :L_return := ''N''; end if; end;';
    --
    execute immediate L_execute_string using IN OUT O_error_message,
                                             IN OUT IO_custom_obj_rec,
                                             OUT L_return;
    --
    if L_return = 'N' then
      --
      RETURN FALSE;
      --
    end if;
    --
  end loop;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END RUN_SYSTEM_RULES_WRP;
--------------------------------------------------------------------------------
END XXADEO_CUSTOM_CONFIG_WRP_SQL;
/ 
