--------------------------------------------------------
--  DDL for Package XXADEO_SUPPLIER_VALIDATE
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE PACKAGE XXADEO_SUPPLIER_VALIDATE AUTHID CURRENT_USER AS
-------------------------------------------------------------------------
TYPE ADDRESS_TYPE_DATA IS TABLE OF ADDR.ADDR_TYPE%TYPE
   INDEX BY BINARY_INTEGER;
-------------------------------------------------------------------------
-- Function Name: PROCESS_SUPPLIER_RECORD
-- Purpose      : This function will validate the input supplier record and
--                populate the local tables to be used for processing.
--                Also this populates output ref_object, which will be sent back to
--                AIA for Xref key and Retail key mapping.
-------------------------------------------------------------------------
FUNCTION PROCESS_SUPPLIER_RECORD(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_supplier_object   IN OUT  SUPP_REC,
                                 O_SupCFA_TBL        IN OUT  XXADEO_CFA_DETAILS_SUP_TBL,
                                 O_ref_outputobject  IN OUT  "XXADEO_SupplierColRef_REC",
                                 I_inputobject       IN      "XXADEO_SupplierColDesc_REC",
                                 I_inputobject_type  IN      VARCHAR2)
return BOOLEAN;
-------------------------------------------------------------------------
END XXADEO_SUPPLIER_VALIDATE;

/