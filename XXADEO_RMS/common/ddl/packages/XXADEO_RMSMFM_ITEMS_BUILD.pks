CREATE OR REPLACE PACKAGE XXADEO_RMSMFM_ITEMS_BUILD AS
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_RMSMFM_ITEMS_BUILD.pks
* Description:   This package contains tools that help 
*				 in the implementation of the publisher 
*				 package XXADEO_RMSMFM_ITEMS.
*
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/

  -------------------------------------------------------------------------------------------
  FUNCTION BUILD_MESSAGE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_message        IN OUT NOCOPY RIB_OBJECT,
                         O_rowids_rec     IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                         O_record_exists  IN OUT BOOLEAN,
                         I_message_type   IN VARCHAR2,
                         I_tran_level_ind IN ITEM_PUB_INFO.TRAN_LEVEL_IND%TYPE,
                         I_queue_rec      IN ITEM_MFQUEUE%ROWTYPE)
    RETURN BOOLEAN;

-------------------------------------------------------------------------------------------
END;
/