CREATE OR REPLACE PACKAGE XXADEO_PCS_UTILS_SQL AS
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package "XXADEO_PCS_UTILS_SQL" for RB157                     */
/*               This package is used in the margins calculation              */
/******************************************************************************/
--------------------------------------------------------------------------------
FUNCTION return_margin(I_pcs            IN     XXADEO_LOCATION_PCS.PCS%TYPE,
                       I_item           IN     RPM_PRICE_CHANGE.ITEM%TYPE,
                       I_location       IN     RPM_PRICE_CHANGE.LOCATION%TYPE default null,
                       I_loc_type       IN     RPM_PRICE_CHANGE.ZONE_NODE_TYPE%TYPE default null,
                       I_bu             IN     AREA.AREA%TYPE default null,
                       I_margin_type    IN     VARCHAR2 default null)
RETURN NUMBER;
--------------------------------------------------------------------------------
END XXADEO_PCS_UTILS_SQL;
/
