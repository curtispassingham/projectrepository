CREATE OR REPLACE PACKAGE BODY XXADEO_RPM_CC_PC_STORE_PORTAL AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS
 --
 L_program                   VARCHAR2(60) := 'XXADEO_RPM_CC_PC_STORE_PORTAL.VALIDATE';
 --
 L_error_rec                 CONFLICT_CHECK_ERROR_REC := NULL;
 L_error_tbl                 CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
 L_bu                        XXADEO_RPM_BU_PRICE_ZONES.BU_ID%TYPE  := NULL;
 L_cfa_attrib_id             CFA_ATTRIB.ATTRIB_ID%TYPE             := NULL;
 L_xxadeo_cfa_key_val_obj    XXADEO_CFA_KEY_VAL_OBJ                := NULL;
 L_xxadeo_cfa_search_obj     XXADEO_CFA_SEARCH_OBJ                 := NULL;
 L_pc_blocking_cfa_value     VARCHAR2(1)                           := 'Y';
 --
 cursor C_check is
   with zone_loc as (select st.store location,
                            rzl.zone_id,
                            RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type
                       from store st,
                            rpm_zone_location rzl
                      where st.store = rzl.location
                     union all
                     select wh.wh location,
                            rzl.zone_id,
                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE zone_node_type
                       from wh wh,
                            rpm_zone_location rzl
                      where wh.wh = rzl.location
                     union all
                     select null location,
                            rz.zone_id,
                            RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type
                       from rpm_zone rz),
       rpm_reason_code as (select code_id
                             from rpm_codes rc
                            where exists (select 1
                                            from code_detail cd
                                           where code_type    = XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_TYPE
                                             and code        in (XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_EM_ST_PORTAL, XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_ST_PORTAL)
                                             and cd.code_desc = rc.code))
   select gtt.price_event_id,
          gtt.future_retail_id,
          NULL cs_promo_fr_id,
          xrpzb.bu_id bu,
          rpc.item
     from rpm_future_retail_gtt     gtt,
          rpm_price_change          rpc,
          zone_loc                  zl,
          xxadeo_rpm_bu_price_zones xrpzb,
          rpm_reason_code           rsc
    where gtt.price_event_id              NOT IN (select cc.price_event_id
                                                   from table(cast(L_error_tbl as conflict_check_error_tbl)) cc)
      and gtt.price_change_id = rpc.price_change_id
      and rpc.location        = zl.location
      and zl.zone_id          = xrpzb.zone_id
      and rpc.reason_code     = rsc.code_id;
 --
 cursor C_get_cfa_attrib is
   select value_1 attrib_id
     from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                   I_parameter => XXADEO_RPM_CONSTANTS_SQL.CFA_RETAIL_PRICE_BLOCKING,
                                   I_bu        => L_bu));
 --
 cursor C_get_blocking_cfa_value is
   select xrco.pc_blocking_cfa_value
     from xxadeo_rpm_custom_options xrco
    where rownum = 1;
 --
BEGIN
  --
  if IO_error_table is NOT NULL and
     IO_error_table.COUNT > 0 then
     L_error_tbl := IO_error_table;
  else
     L_error_rec := NEW CONFLICT_CHECK_ERROR_REC(-99999,
                                                 NULL,
                                                 NULL,
                                                 NULL);
     L_error_tbl := NEW CONFLICT_CHECK_ERROR_TBL(L_error_rec);
  end if;
  --
  for rec IN C_check loop
    --
    L_xxadeo_cfa_key_val_obj  := XXADEO_CFA_KEY_VAL_OBJ();
    L_bu                      := rec.bu;
    L_cfa_attrib_id           := NULL;
    --
    open C_get_cfa_attrib;
    fetch C_get_cfa_attrib into L_cfa_attrib_id;
    close C_get_cfa_attrib;
    --
    for pc_blocking_rec in (select *
                              from table(XXADEO_CFA_SQL.SEARCH_CFA_BY_ATTRIB(I_attrib_id          => L_cfa_attrib_id,
                                                                             I_xxadeo_cfa_key_val_tbl => CAST (MULTISET (select new XXADEO_CFA_KEY_VAL_OBJ(key_col_1    => 'ITEM',
                                                                                                                                                           key_value_1  => item)
                                                                                                                           from item_master
                                                                                                                          where item= rec.item)as XXADEO_CFA_KEY_VAL_TBL)))) loop
      --
      L_pc_blocking_cfa_value := null;
      --
      open C_get_blocking_cfa_value;
      fetch C_get_blocking_cfa_value into L_pc_blocking_cfa_value;
      close C_get_blocking_cfa_value;
      --
      if pc_blocking_rec.cfa_value = nvl(L_pc_blocking_cfa_value,'Y') then
        --
        L_error_rec := NEW CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                    rec.future_retail_id,
                                                    RPM_CONSTANTS.CONFLICT_ERROR,
                                                    'xxadeo_pc_sp_blocked',
                                                    rec.cs_promo_fr_id);

        if IO_error_table is NULL then
          --
          IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
          --
        else
          --
          IO_error_table.EXTEND;
          IO_error_table(IO_error_table.COUNT) := L_error_rec;
          --
        end if;
        --
      end if;
      --
    end loop;

  end loop;
  --
  RETURN 1;
  --
EXCEPTION
  --
  when OTHERS then
    --
    L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                            NULL,
                                            RPM_CONSTANTS.PLSQL_ERROR,
                                            SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                               SQLERRM,
                                                               L_program,
                                                               TO_CHAR(SQLCODE)));
    IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
    --
    return 0;
    --
END VALIDATE;
--------------------------------------------------------
END XXADEO_RPM_CC_PC_STORE_PORTAL; 
/
