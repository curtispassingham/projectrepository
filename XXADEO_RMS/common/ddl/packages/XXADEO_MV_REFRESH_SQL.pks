CREATE OR REPLACE PACKAGE XXADEO_MV_REFRESH_SQL IS
 
  /******************************************************************************/
  /* CREATE DATE - August 2018                                                  */
  /* CREATE USER - Tiago Torres                                                 */
  /* PROJECT     - ADEO                                                         */
  /* DESCRIPTION - Refresh materialized views                                   */
  /******************************************************************************/

  --------------------------------------------------------------------------------
  --Function Name : dashboards_mv_refresh
  --Purpose       : refresh materialized views used in incomplete items dashboard
  --------------------------------------------------------------------------------
  FUNCTION DASHBOARD_INCOMPLETE_ITEMS_MV(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN BOOLEAN;

  --------------------------------------------------------------------------------
  --Function Name : dashboards_mv_refresh
  --Purpose       : refresh materialized views used in items price change dashboard
  --------------------------------------------------------------------------------
  FUNCTION DASHBOARD_ITEMS_PRICE_CHG_MV(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN BOOLEAN;

END XXADEO_MV_REFRESH_SQL;
/
