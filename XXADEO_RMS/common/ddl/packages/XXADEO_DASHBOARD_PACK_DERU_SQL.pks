create or replace package XXADEO_DASHBOARD_PACK_DERU_SQL is

  -- Author  : TAT
  -- Created : 18/12/2018
  -- Purpose : RR148 - Pack Items and Derived Unit Items - Dashboard

  GP_UDA_TYPE_LV VARCHAR2(6) := 'LV';
  GP_UDA_TYPE_DT VARCHAR2(6) := 'DT';
  GP_UDA_TYPE_FF VARCHAR2(6) := 'FF';
  --
  GP_CFA_TEXT   VARCHAR2(10) := 'VARCHAR2';
  GP_CFA_NUMBER VARCHAR2(10) := 'NUMBER';
  GP_CFA_DATE   VARCHAR2(10) := 'DATE';
  --
  GP_CFA_TYPE_CB VARCHAR2(6) := 'CB';
  GP_CFA_TYPE_DT VARCHAR2(6) := 'DT';
  GP_CFA_TYPE_LI VARCHAR2(6) := 'LI';
  GP_CFA_TYPE_RG VARCHAR2(6) := 'RG';
  GP_CFA_TYPE_TI VARCHAR2(6) := 'TI';

  ---------------------------------------------------------------------------------
  -- Name: PACK_DERUNIT_ITEM_SEARCH
  -- Purpose: Function to return pack and derived unit items in the
  --          Dashboard, based on the search criteria defined by the user.
  ---------------------------------------------------------------------------------
  FUNCTION PACK_DERUNIT_ITEM_SEARCH(I_item_type       IN VARCHAR2,
                                    I_bu              IN area_tl.area%TYPE DEFAULT NULL,
                                    I_group           IN groups.group_no%TYPE DEFAULT NULL,
                                    I_dept_list       IN VARCHAR2 DEFAULT NULL,
                                    I_class_list      IN CLOB DEFAULT NULL,
                                    I_subclass_list   IN CLOB DEFAULT NULL,
                                    I_supp_purch_site IN NUMBER DEFAULT NULL,
                                    I_manufacturer    IN NUMBER DEFAULT NULL,
                                    I_item_list       IN VARCHAR2 DEFAULT NULL,
                                    I_item            IN item_master.item%TYPE DEFAULT NULL,
                                    I_item_desc       IN item_master.item_desc%TYPE DEFAULT NULL,
                                    I_gtin            IN NUMBER DEFAULT NULL,
                                    I_block_po_reason IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa1_id      IN NUMBER DEFAULT NULL,
                                    I_udacfa1_value   IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa1_type    IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa1_flag    IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa2_id      IN NUMBER DEFAULT NULL,
                                    I_udacfa2_value   IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa2_type    IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa2_flag    IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa3_id      IN NUMBER DEFAULT NULL,
                                    I_udacfa3_value   IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa3_type    IN VARCHAR2 DEFAULT NULL,
                                    I_udacfa3_flag    IN VARCHAR2 DEFAULT NULL,
                                    I_execute_query   IN VARCHAR2 DEFAULT 'Y')
    RETURN XXADEO_PACK_DERU_RESULT_TBL
    PIPELINED;

  ---------------------------------------------------------------------------------
  -- Name: PACK_DERUNIT_ITM_DETAIL_SEARCH
  -- Purpose: Function to return component and derived unit items level in the
  --          Dashboard, based on the pack/derived unit selected by the user.
  ---------------------------------------------------------------------------------
  FUNCTION PACK_DERUNIT_ITM_DETAIL_SEARCH(I_item_type     IN VARCHAR2,
                                          I_bu            IN area_tl.area%TYPE DEFAULT NULL,
                                          I_agg_item      IN item_master.item%TYPE DEFAULT NULL,
                                          I_execute_query IN VARCHAR2 DEFAULT 'Y')
    RETURN XXADEO_PACK_DERU_RESULT_TBL
    PIPELINED;

  ---------------------------------------------------------------------------------
  -- Name: PACK_DERUNIT_SUPP_SEARCH
  -- Purpose: Function to return suppliers in the Dashboard, based on the
  --          pack/derived unit selected by the user.
  ---------------------------------------------------------------------------------
  FUNCTION PACK_DERUNIT_SUPP_SEARCH(I_item_type     IN VARCHAR2,
                                    I_bu            IN area_tl.area%TYPE,
                                    I_agg_item      IN item_master.item%TYPE,
                                    I_execute_query IN VARCHAR2 DEFAULT 'Y')
    RETURN XXADEO_PACK_DERU_SUPS_TBL
    PIPELINED;
  --
end XXADEO_DASHBOARD_PACK_DERU_SQL;
/