CREATE OR REPLACE PACKAGE BODY XXADEO_MERCHHIER_RDF_DOM_SQL AS
----------------------------------------------------------------------------------------------

  TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
  LP_errors_tab     errors_tab_typ;
  TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
  LP_s9t_errors_tab s9t_errors_tab_typ;

  TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
  LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

  LP_primary_lang                    LANG.LANG%TYPE;
  LP_user                            SVCPROV_CONTEXT.USER_NAME%TYPE   := GET_USER;
  LP_chunk_id                        SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE  := 1;

  L_process_id                       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE;
  L_file_id                          SVC_PROCESS_TRACKER.FILE_ID%TYPE;
  L_date_format                      NLS_DATABASE_PARAMETERS.VALUE%TYPE;
  L_xxadeo_wksht_column_idx_tbl      XXADEO_WKSHT_COLUMN_IDX_TBL;
----------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(O_error_message IN OUT LOGGER_LOGS.TEXT%TYPE,
                          I_issues_rt     IN     S9T_ERRORS%ROWTYPE) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_issues_rt.file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := TEMPLATE_KEY;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_issues_rt.wksht_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_issues_rt.column_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_issues_rt.row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := I_issues_rt.error_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := LP_user;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := LP_user;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
  --
  Lp_errors_tab.extend();
  Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
  Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
  Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
  Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
  Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
  Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
  Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
  Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;
  --
END WRITE_ERROR;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_RDF_DOMAINS(I_file_id   IN   NUMBER ) IS
BEGIN
  --
  insert into TABLE (select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id    = I_file_id
                        and ss.sheet_name = RDF_DOMAINS_SHEET)
                select s9t_row(s9t_cells(XXADEO_MERCHHIER_RDF_DOM_SQL.action_mod,
                                         d.domain_id,
                                         d.domain_desc),rownum+1)                  
                  from domain d;
  --
END POPULATE_RDF_DOMAINS;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_RDF_DOMAINS_DEPT(I_file_id   IN   NUMBER ) IS
BEGIN
  --
  insert into TABLE (select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id    = I_file_id
                        and ss.sheet_name = RDF_DOMAINS_DEPT_SHEET)
                select s9t_row(s9t_cells((case
                                           when dd.load_sales_ind is null then
                                             XXADEO_MERCHHIER_RDF_DOM_SQL.action_new
                                           else
                                             XXADEO_MERCHHIER_RDF_DOM_SQL.action_mod
                                           end),
                                           g.group_no,
                                           g.group_name,
                                           d.dept,
                                           nvl((select dtl.dept_name from deps_tl dtl where dtl.dept = d.dept and dtl.lang = get_user_lang),d.dept_name),
                                           dd.domain_id,
                                           dd.load_sales_ind),rownum+1)
                  from v_deps         d,
                       v_groups       g,
                       domain_dept   dd
                 where d.group_no      = g.group_no
                   and d.dept          = dd.dept(+);
  --
END POPULATE_RDF_DOMAINS_DEPT;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_RDF_DOMAINS_CLASS(I_file_id   IN   NUMBER ) IS
BEGIN
  --
  insert into TABLE (select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id    = I_file_id
                        and ss.sheet_name = RDF_DOMAINS_CLASS_SHEET)
                select s9t_row(s9t_cells((case
                                           when dc.load_sales_ind is null then
                                             XXADEO_MERCHHIER_RDF_DOM_SQL.action_new
                                           else
                                             XXADEO_MERCHHIER_RDF_DOM_SQL.action_mod
                                           end),
                                          g.group_no,
                                          g.group_name,
                                          d.dept,
                                          nvl((select dtl.dept_name from deps_tl dtl where dtl.dept = d.dept and dtl.lang = get_user_lang),d.dept_name),
                                          c.class,
                                          nvl((select ctl.class_name from class_tl ctl where ctl.class = c.class and ctl.dept = c.dept and ctl.lang = get_user_lang), c.class_name),
                                          dc.domain_id,
                                          dc.load_sales_ind),rownum+1)
                  from v_deps         d,
                       v_groups       g,
                       v_class        c,
                       domain_class  dc
                 where d.group_no      = g.group_no
                   and d.dept          = c.dept
                   and c.dept          = dc.dept(+)
                   and c.class         = dc.class(+);
  --
END POPULATE_RDF_DOMAINS_CLASS;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_RDF_DOMAINS_SUBCLASS(I_file_id   IN   NUMBER ) IS
BEGIN
  --
  insert into TABLE (select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id    = I_file_id
                        and ss.sheet_name = RDF_DOMAINS_SC_SHEET)
                select s9t_row(s9t_cells((case
                                           when ds.load_sales_ind is null then
                                             XXADEO_MERCHHIER_RDF_DOM_SQL.action_new
                                           else
                                             XXADEO_MERCHHIER_RDF_DOM_SQL.action_mod
                                           end),
                                          g.group_no,
                                          g.group_name,
                                          d.dept,
                                          nvl((select dtl.dept_name from deps_tl dtl where dtl.dept = d.dept and dtl.lang = get_user_lang),d.dept_name),
                                          c.class,
                                          nvl((select ctl.class_name from class_tl ctl where ctl.class = c.class and ctl.dept = c.dept and ctl.lang = get_user_lang), c.class_name),
                                          s.subclass,
                                          nvl((select stl.sub_name from subclass_tl stl where stl.subclass = s.subclass and stl.class = s.class and stl.dept = s.dept and stl.lang = get_user_lang), s.sub_name),
                                          ds.domain_id,
                                          ds.load_sales_ind),rownum+1)
                  from v_deps            d,
                       v_groups          g,
                       v_class           c,
                       v_subclass        s,
                       domain_subclass  ds
                 where d.group_no      = g.group_no
                   and d.dept          = c.dept
                   and c.class         = s.class
                   and d.dept          = s.dept 
                   and c.dept          = s.dept
                   and s.dept          = ds.dept(+)
                   and s.class         = ds.class(+)
                   and s.subclass      = ds.subclass(+);
  --
END POPULATE_RDF_DOMAINS_SUBCLASS;
----------------------------------------------------------------------------------------------
FUNCTION SETUP_RDF_DOMAINS(O_error_message IN OUT  LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_program              VARCHAR2(64) := 'XXADEO_MERCHHIER_RDF_DOM_SQL.SETUP_RDF_DOMAINS';
  --
  --
BEGIN
  --
  SAVEPOINT INIT_CREATE_RDF_DOMAINS;
  --
  -- create department status
  --
  BEGIN
    --
    merge into domain target
      using (select rds.domain_id,
                    rds.domain_name
               from xxadeo_rdf_domains_stg rds
              where process_id = L_process_id
                and action    in (GP_upload_create,GP_upload_update)) source
      on (target.domain_id = source.domain_id)
    when matched then
      update set target.domain_desc = source.domain_name
    when not matched then
      insert
        (domain_id,
         domain_desc)
       values
         (source.domain_id,
          source.domain_name);
    --
  END;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END SETUP_RDF_DOMAINS;
----------------------------------------------------------------------------------------------
FUNCTION SETUP_RDF_DOMAINS_DEPT(O_error_message IN OUT  LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_program              VARCHAR2(64) := 'XXADEO_MERCHHIER_RDF_DOM_SQL.SETUP_RDF_DOMAINS_DEPT';
  --
  cursor C_get_create_values is
    select rdd.dept,
           rdd.domain_id,
           rdd.load_sales_ind,
           rdd.row_seq
      from xxadeo_rdf_domains_dept_stg rdd
     where process_id = L_process_id
       and action     = GP_upload_create;
  --
  cursor C_get_update_values is 
    select rdd.dept,
           rdd.domain_id,
           rdd.load_sales_ind,
           rdd.row_seq
      from xxadeo_rdf_domains_dept_stg rdd
     where process_id = L_process_id
       and action     = GP_upload_update;
  --
BEGIN
  --
  SAVEPOINT INIT_CREATE_DEPT_STATUS;
  --
  -- create department status
  --
  BEGIN
    --
    for rec in C_get_create_values loop
      --
      begin
        --
        insert into domain_dept
          (dept,
           domain_id,
           load_sales_ind)
         values
           (rec.dept,
            rec.domain_id,
            rec.load_sales_ind);
        --
      exception
        --
        when DUP_VAL_ON_INDEX then
          --
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      RDF_DOMAINS_DEPT_SHEET,
                      rec.row_seq,
                      'INSERTING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$DOM_DEPT_EXISTS);  
          --
        when others then
          --
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      RDF_DOMAINS_DEPT_SHEET,
                      rec.row_seq,
                      'INSERTING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$COULD_NOT_INSERT_REC);  
            --
      end;
      --
    end loop;
    --
    for rec in C_get_update_values loop
      --
      begin
        --
        update domain_dept
           set load_sales_ind = rec.load_sales_ind,
               domain_id      = rec.domain_id
         where dept = rec.dept;
        --
        --
        if SQL%ROWCOUNT = 0 then
          --
           WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      RDF_DOMAINS_DEPT_SHEET,
                      rec.row_seq,
                      'UPDATING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$REC_NOT_EXISTS);  
        --
        end if;
        --
      exception
        --
        when others then
          --
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      RDF_DOMAINS_DEPT_SHEET,
                      rec.row_seq,
                      'UPDATING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$COULD_NOT_UPDATE_REC); 
      end;
      --
    end loop;
    --
  end;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END SETUP_RDF_DOMAINS_DEPT;
----------------------------------------------------------------------------------------------
FUNCTION SETUP_RDF_DOMAINS_CLASS(O_error_message IN OUT  LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_program              VARCHAR2(64) := 'XXADEO_MERCHHIER_RDF_DOM_SQL.SETUP_RDF_DOMAINS_CLASS';
  --
  cursor C_get_create_values is
    select rdc.dept,
           rdc.class,
           rdc.domain_id,
           rdc.load_sales_ind,
           rdc.row_seq
      from xxadeo_rdf_domains_class_stg rdc
     where process_id = L_process_id
       and action     = GP_upload_create;
  --
  cursor C_get_update_values is
    select rdc.dept,
           rdc.class,
           rdc.domain_id,
           rdc.load_sales_ind,
           rdc.row_seq
      from xxadeo_rdf_domains_class_stg rdc
     where process_id = L_process_id
       and action     = GP_upload_update;
  --
BEGIN
  --
  SAVEPOINT INIT_CREATE_RDF_DOMAINS_CLASS;
  --
  -- create class status
  --
  BEGIN
    --
    for rec in C_get_create_values loop
      --
      begin
        --
        insert into domain_class
          (dept,
           class,
           domain_id,
           load_sales_ind)
         values
           (rec.dept,
            rec.class,
            rec.domain_id,
            rec.load_sales_ind);
        --
      exception
        --
        when DUP_VAL_ON_INDEX then
          --
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      RDF_DOMAINS_CLASS_SHEET,
                      rec.row_seq,
                      'INSERTING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$DOM_CLASS_EXISTS);  
          --
        when others then
          --
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      RDF_DOMAINS_CLASS_SHEET,
                      rec.row_seq,
                      'INSERTING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$COULD_NOT_INSERT_REC);  
            --
      end;
      --
    end loop;
    --
    for rec in C_get_update_values loop
      --
      begin
        --
        update domain_class
           set load_sales_ind = rec.load_sales_ind,
               domain_id      = rec.domain_id
         where dept  = rec.dept
           and class = rec.class;
        --
        --
        if SQL%ROWCOUNT = 0 then
          --
           WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      RDF_DOMAINS_CLASS_SHEET,
                      rec.row_seq,
                      'UPDATING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$REC_NOT_EXISTS);  
        --
        end if;
        --
      exception
        --
        when others then
          --
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      RDF_DOMAINS_CLASS_SHEET,
                      rec.row_seq,
                      'UPDATING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$COULD_NOT_UPDATE_REC); 
      end;
      --
    end loop;
    --
  --
  END;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END SETUP_RDF_DOMAINS_CLASS;
----------------------------------------------------------------------------------------------
FUNCTION SETUP_RDF_DOMAINS_SUBCLASS(O_error_message IN OUT  LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_program              VARCHAR2(64) := 'XXADEO_MERCHHIER_SQL.SETUP_RDF_DOMAINS_SUBCLASS';
  --
  cursor C_get_create_values is 
    select dept,
           class,
           subclass,
           domain_id,
           load_sales_ind,
           row_seq
      from xxadeo_rdf_domains_sclass_stg dt
     where process_id = L_process_id
       and action     = GP_upload_create;
  --
  cursor C_get_update_values is
    select dept,
           class,
           subclass,
           domain_id,
           load_sales_ind,
           row_seq
      from xxadeo_rdf_domains_sclass_stg dt
     where process_id = L_process_id
       and action     = GP_upload_update;
BEGIN
  --
  SAVEPOINT INIT_CREATE_RDF_DOMAINS_SCLASS;
  --
  -- create rdf domain subclass
  --
  BEGIN
    --
    for rec in C_get_create_values loop
      --
      begin
        --
        insert into domain_subclass
          (dept,
           class,
           subclass,
           domain_id,
           load_sales_ind)
         values
           (rec.dept,
            rec.class,
            rec.subclass,
            rec.domain_id,
            rec.load_sales_ind);
        --
      exception
        --
        when DUP_VAL_ON_INDEX then
          --
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      RDF_DOMAINS_SC_SHEET,
                      rec.row_seq,
                      'INSERTING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$DOM_SCLASS_EXISTS);  
          --
        when others then
          --
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      RDF_DOMAINS_SC_SHEET,
                      rec.row_seq,
                      'INSERTING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$COULD_NOT_INSERT_REC);  
            --
      end;
      --
    end loop;
    --
    for rec in C_get_update_values loop
      --
      begin
        --
        update domain_subclass
           set load_sales_ind = rec.load_sales_ind,
               domain_id      = rec.domain_id
         where dept     = rec.dept
           and class    = rec.class
           and subclass = rec.subclass;
        --
        --
        if SQL%ROWCOUNT = 0 then
          --
           WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      RDF_DOMAINS_SC_SHEET,
                      rec.row_seq,
                      'UPDATING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$REC_NOT_EXISTS);  
        --
        end if;
        --
      exception
        --
        when others then
          --
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      RDF_DOMAINS_SC_SHEET,
                      rec.row_seq,
                      'UPDATING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$COULD_NOT_UPDATE_REC); 
      end;
      --
    end loop;
    --
  --
  END;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END SETUP_RDF_DOMAINS_SUBCLASS;
----------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(64) := 'XXADEO_MERCHHIER_RDF_DOM_SQL.CREATE_S9T';
  --
  L_file           s9t_file;
  L_s9t_sheet_tab  S9T_SHEET_TAB;
  L_s9t_row_tab    S9T_ROW_TAB;
  --
BEGIN
  --
  LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
  L_s9t_sheet_tab := S9T_SHEET_TAB();
  L_s9t_row_tab   := S9T_ROW_TAB();
  --
  -- get template sheets and columns
  --
  if XXADEO_CUSTOM_UPLOAD_SQL.GET_TEMPLATE_S9T_SHEETS(O_error_message   => O_error_message,
                                                      IO_s9t_sheet_tab  => L_s9t_sheet_tab,
                                                      I_template_key    => TEMPLATE_KEY) = FALSE then
    --
    RETURN FALSE;
    --
  end if;
  --
  XXADEO_CUSTOM_UPLOAD_SQL.INIT_S9T(O_error_message  => O_error_message,
                                    O_file_id        => O_file_id,
                                    I_template_key   => TEMPLATE_KEY,
                                    I_s9t_sheet_tab  => L_s9t_sheet_tab);
  --
  if S9T_PKG.POPULATE_LISTS(O_error_message,
                            O_file_id,
                            template_category,
                            template_key) = FALSE   then
     RETURN FALSE;
  end if;
  --
  if I_template_only_ind = 'N' then
    --
    POPULATE_RDF_DOMAINS(O_file_id);
    POPULATE_RDF_DOMAINS_DEPT(O_file_id);
    POPULATE_RDF_DOMAINS_CLASS(O_file_id);
    POPULATE_RDF_DOMAINS_SUBCLASS(O_file_id);
    COMMIT;
    --
  end if;

  S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
  S9T_PKG.APPLY_TEMPLATE(O_file_id,
                         template_key);
  L_file := S9T_FILE(O_file_id);

  if S9T_PKG.CODE2DESC(O_error_message,
                       template_category,
                       L_file) = FALSE then
    --
    RETURN FALSE;
    --
  end if;
  --
  S9T_PKG.SAVE_OBJ(L_file);
  S9T_PKG.UPDATE_ODS(L_file);
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END CREATE_S9T;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DOMAIN (O_error_message          IN OUT LOGGER_LOGS.TEXT%TYPE,
                          I_domain_id              IN     XXADEO_RDF_DOMAINS_STG.DOMAIN_ID%TYPE,
                          I_process_id             IN     NUMBER)
RETURN BOOLEAN IS
  --
  L_program     VARCHAR2(64) := 'XXADEO_MERCHHIER_RDF_DOM_SQL.VALIDATE_DOMAIN';
  L_chk_domain  VARCHAR2(1)  := 'N';
  --
  cursor C_chk_domain is
     select 'Y'
       from XXADEO_RDF_DOMAINS_STG
      where domain_id = I_domain_id
        and process_id = I_process_id;
  --
BEGIN
  --
  open C_chk_domain;
  fetch C_chk_domain into L_chk_domain;
  close C_chk_domain;
  --
  if L_chk_domain = 'N' then
    --
    if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_DOMAIN(O_error_message,                                                    
                                                      I_domain_id) then
      --
      return FALSE;
      --
    end if;
    --
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    if C_chk_domain%isopen then
      close C_chk_domain;
    end if;
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
  --
END VALIDATE_DOMAIN;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RDF_DOMAINS (O_error_message          IN OUT LOGGER_LOGS.TEXT%TYPE,
                               IO_error                 IN OUT BOOLEAN,
                               I_xxadeo_rdf_domains_stg IN     XXADEO_RDF_DOMAINS_STG%ROWTYPE,
                               I_row_seq                IN     SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE)
RETURN BOOLEAN IS
  --
  L_error     BOOLEAN := IO_error;
  --
BEGIN
  --
  if not I_xxadeo_rdf_domains_stg.action = GP_upload_create then
  --
    if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_DOMAIN(O_error_message,
                                                      I_xxadeo_rdf_domains_stg.domain_id) then
      --
      WRITE_ERROR(L_process_id,
                  svc_admin_upld_er_seq.nextval,
                  LP_chunk_id,
                  RDF_DOMAINS_SHEET,
                  I_row_seq,
                  RDF_DOMAINS$DOMAIN_ID,
                  XXADEO_GLOBAL_VARS_SQL.VAL$INV_DOMAIN); 
      --
      L_error := TRUE;
      --
    end if;
    --
  end if;
  --
  If not IO_ERROR then
    IO_ERROR := L_error;
  end if;
  --
  RETURN TRUE;
  --
END VALIDATE_RDF_DOMAINS;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RDF_DOM_DEPT_ASSOC(O_error_message                 IN OUT LOGGER_LOGS.TEXT%TYPE,
                                     IO_error                        IN OUT BOOLEAN,
                                     I_xxadeo_rdf_dom_dept_stg       IN     XXADEO_RDF_DOMAINS_DEPT_STG%ROWTYPE,
                                     I_row_seq                       IN     SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE)
RETURN BOOLEAN IS
  --
  L_error     BOOLEAN := IO_error;
  --
BEGIN
  --
  if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER(O_error_message,
                                                        I_xxadeo_rdf_dom_dept_stg.dept) then
    --
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                RDF_DOMAINS_DEPT_SHEET,
                I_row_seq,
                RDF_DOMAINS_DEPT$DEPT,
                XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEPT); 
    --
    L_error := TRUE;
    --
  end if;
  --
  if not VALIDATE_DOMAIN(O_error_message,
                         I_xxadeo_rdf_dom_dept_stg.domain_id,
                         L_process_id) then
    --
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                RDF_DOMAINS_DEPT_SHEET,
                I_row_seq,
                RDF_DOMAINS_DEPT$DOMAIN_ID,
                XXADEO_GLOBAL_VARS_SQL.VAL$INV_DOMAIN); 
    --
    L_error := TRUE;
    --
  end if;
  --
  If not IO_ERROR then
    IO_ERROR := L_error;
  end if;
  --
  RETURN TRUE;
  --
END VALIDATE_RDF_DOM_DEPT_ASSOC;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RDF_DOM_CLASS_ASSOC(O_error_message                  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                      IO_error                         IN OUT BOOLEAN,
                                      I_xxadeo_rdf_dom_class_stg       IN     XXADEO_RDF_DOMAINS_CLASS_STG%ROWTYPE,
                                      I_row_seq                        IN     SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE)
RETURN BOOLEAN IS
  --
  L_error     BOOLEAN := IO_error;
  --
BEGIN
  --
  if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER(O_error_message,
                                                        I_xxadeo_rdf_dom_class_stg.dept,
                                                        I_xxadeo_rdf_dom_class_stg.class) then
    --
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                RDF_DOMAINS_CLASS_SHEET,
                I_row_seq,
                RDF_DOMAINS_CLASS$CLASS,
                XXADEO_GLOBAL_VARS_SQL.VAL$INV_CLASS); 
    --
    L_error := TRUE;
    --
  end if;
  --
  if not VALIDATE_DOMAIN(O_error_message,
                         I_xxadeo_rdf_dom_class_stg.domain_id,
                         L_process_id) then
    --
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                RDF_DOMAINS_CLASS_SHEET,
                I_row_seq,
                RDF_DOMAINS_CLASS$DOMAIN_ID,
                XXADEO_GLOBAL_VARS_SQL.VAL$INV_DOMAIN); 
    --
    L_error := TRUE;
    --
  end if;
  --
  If not IO_ERROR then
    IO_ERROR := L_error;
  end if;
  --
  RETURN TRUE;
  --
END VALIDATE_RDF_DOM_CLASS_ASSOC;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RDF_DOM_SCLASS_ASSOC(O_error_message                  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                       IO_error                         IN OUT BOOLEAN,
                                       I_xxadeo_rdf_dom_sclass_stg      IN     XXADEO_RDF_DOMAINS_SCLASS_STG%ROWTYPE,
                                       I_row_seq                        IN     SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE)
RETURN BOOLEAN IS
  --
  L_error     BOOLEAN := IO_error;
  --
BEGIN
  --
  if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER(O_error_message,
                                                        I_xxadeo_rdf_dom_sclass_stg.dept,
                                                        I_xxadeo_rdf_dom_sclass_stg.class,
                                                        I_xxadeo_rdf_dom_sclass_stg.subclass) then
    --
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                RDF_DOMAINS_SC_SHEET,
                I_row_seq,
                RDF_DOMAINS_SC$SUBCLASS,
                XXADEO_GLOBAL_VARS_SQL.VAL$INV_SUBCLASS); 
    --
    L_error := TRUE;
    --
  end if;
  --
  if not VALIDATE_DOMAIN(O_error_message,
                         I_xxadeo_rdf_dom_sclass_stg.domain_id,
                         L_process_id) then
    --
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                RDF_DOMAINS_SC_SHEET,
                I_row_seq,
                RDF_DOMAINS_SC$DOMAIN_ID,
                XXADEO_GLOBAL_VARS_SQL.VAL$INV_DOMAIN); 
    --
    L_error := TRUE;
    --
  end if;
  --
  If not IO_ERROR then
    IO_ERROR := L_error;
  end if;
  --
  RETURN TRUE;
  --
END VALIDATE_RDF_DOM_SCLASS_ASSOC;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_RDF_DOMAINS(O_error_message   IN OUT  LOGGER_LOGS.TEXT%TYPE) IS
  --
  L_program                       VARCHAR2(64) := 'XXADEO_MERCHHIER_RDF_DOM_SQL.PROCESS_S9T_RDF_DOMAINS';
  --
  TYPE XXADEO_RDF_DOMAINS_TYPE IS TABLE OF XXADEO_RDF_DOMAINS_STG%ROWTYPE;
  L_xxadeo_rdf_domains_tbl     XXADEO_RDF_DOMAINS_TYPE := XXADEO_RDF_DOMAINS_TYPE();
  L_temp_rec                   XXADEO_RDF_DOMAINS_STG%ROWTYPE;
  L_issues_rt                  S9T_ERRORS%ROWTYPE;
  --
  L_error                      BOOLEAN:=FALSE;
  L_default_rec                XXADEO_RDF_DOMAINS_STG%ROWTYPE;
  L_row_seq                    SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE;
  L_error_code                 NUMBER;
  L_error_msg                  RTK_ERRORS.RTK_TEXT%TYPE;
  --
  DML_ERRORS    EXCEPTION;
  PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
  --
  cursor C_mandatory_ind is
    select domain_id_mi,
           domain_name_mi,
           1 as dummy
      from (select column_key,
                   required_visual_ind
              from s9t_tmpl_cols_def
             where template_key   = TEMPLATE_KEY
               and wksht_key      = 'RDF_DOMAINS'
            ) pivot (max(required_visual_ind) as mi
                     for (column_key) in ('DOMAIN_ID'         as domain_id,
                                          'DOMAIN_NAME'       as domain_name,
                                           null as dummy));
  L_mi_rec C_mandatory_ind%ROWTYPE;
  --
  CURSOR C_get_date_format IS
    select value
      from nls_database_parameters
     where parameter = 'NLS_DATE_FORMAT';
  --
BEGIN
  --
  OPEN C_get_date_format;
  FETCH C_get_date_format INTO L_date_format;
  CLOSE C_get_date_format;
  --
  -- get default values
  --
  for rec in (select domain_id_dv,
                     domain_name_dv,
                     null as dummy
               from (select column_key,
                            default_value
                       from s9t_tmpl_cols_def
                      where template_key  = template_key
                        and wksht_key     = 'RDF_DOMAINS'
                     ) PIVOT (MAX(default_value) AS dv
                              FOR (column_key) IN ('DOMAIN_ID'         as domain_id,
                                                   'DOMAIN_NAME'       as domain_name,
                                                   NULL AS dummy))) loop
    --
    -- domain_id default
    --
    BEGIN
      --
      L_default_rec.domain_id := rec.domain_id_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_Id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SHEET;
        L_issues_rt.column_Key        := RDF_DOMAINS$DOMAIN_ID;
        L_issues_rt.error_Key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- domain_name default
    --
    BEGIN
      --
      L_default_rec.domain_name := rec.domain_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS$DOMAIN_NAME;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
  end loop;
  --
  -- get mandatory indicators
  --
  open C_mandatory_ind;
  fetch C_mandatory_ind into L_mi_rec;
  close C_mandatory_ind;
  --
  -- validate datatypes and business rules
  --
  for rec IN (select r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS$ACTION))                as ACTION,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS$DOMAIN_ID))             as DOMAIN_ID,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS$DOMAIN_NAME))           as DOMAIN_NAME,
                     r.get_row_seq()                                                                                                      as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = L_file_id
                 and ss.sheet_name = SHEET_NAME_TRANS(RDF_DOMAINS_SHEET)) loop
    --
    L_temp_rec                      := null;
    L_temp_rec.process_id           := L_process_id;
    L_temp_rec.create_id            := LP_user;
    L_temp_rec.last_update_id       := LP_user;
    L_temp_rec.create_datetime      := sysdate;
    L_temp_rec.last_update_datetime := sysdate;
    L_error                         := FALSE;
    L_row_seq                       := rec.row_seq;
    --
    LP_chunk_id                     := 1;
    --
    -- validate data types
    --
    -- action
    --
    BEGIN
      --
      if rec.action is null then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      else
        --
        if rec.action = GP_upload_create or
           rec.action = GP_upload_update then
          --
          L_temp_rec.action := rec.action;
          --
        else
        --
          L_issues_rt                   := null;
          L_issues_rt.file_id           := L_file_id;
          L_issues_rt.wksht_key         := RDF_DOMAINS_SHEET;
          L_issues_rt.column_key        := RDF_DOMAINS$ACTION;
          L_issues_rt.row_seq           := rec.row_seq;
          L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
          --
          WRITE_S9T_ERROR(O_error_message => O_error_message,
                          I_issues_rt     => L_issues_rt);
          --
          L_error := TRUE;
          --
        end if;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- domain_id
    --
    BEGIN
      --
      if L_mi_rec.domain_id_mi   = 'Y'   and
         L_default_rec.domain_id is null and
         rec.domain_id is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS$DOMAIN_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.domain_id := rec.domain_id;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS$DOMAIN_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- domain_name
    --
    BEGIN
      --
      if L_mi_rec.domain_name_mi   = 'Y'   and
         L_default_rec.domain_name is null and
         rec.domain_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS$DOMAIN_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.domain_name := rec.domain_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS$DOMAIN_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- validate business rules
    --
    if not L_error then
      --
      if not VALIDATE_RDF_DOMAINS (O_error_message,
                                   L_error,
                                   L_temp_rec,
                                   L_row_seq) then
        --
        RETURN;
        --
      end if;
      --
    end if;
    --
    -- if line does not have error, insert in table to process
    --
    if NOT L_error then
      --
      L_temp_rec.row_seq   := rec.row_seq;
      --
      L_xxadeo_rdf_domains_tbl.extend();
      L_xxadeo_rdf_domains_tbl(L_xxadeo_rdf_domains_tbl.count()) := L_temp_rec;
      --
    end if;
    --
  --
  end loop;
  --
  -- insert into stagging
  --
  BEGIN
    --
    forall i IN 1..L_xxadeo_rdf_domains_tbl.count SAVE EXCEPTIONS
      insert into xxadeo_rdf_domains_stg
        (process_id,
         action,
         domain_id,
         domain_name,
         create_id,
         create_datetime,
         last_update_id,
         last_update_datetime,
         row_seq)
      values
        (L_xxadeo_rdf_domains_tbl(i).process_id,
         L_xxadeo_rdf_domains_tbl(i).action,
         L_xxadeo_rdf_domains_tbl(i).domain_id,
         L_xxadeo_rdf_domains_tbl(i).domain_name,
         L_xxadeo_rdf_domains_tbl(i).create_id,
         L_xxadeo_rdf_domains_tbl(i).create_datetime,
         L_xxadeo_rdf_domains_tbl(i).last_update_id,
         L_xxadeo_rdf_domains_tbl(i).last_update_datetime,
         L_xxadeo_rdf_domains_tbl(i).row_seq);
    --
  EXCEPTION
    --
    when DML_ERRORS then
      --
      for i in 1..sql%bulk_exceptions.COUNT loop
        --
        L_error_code:=sql%bulk_exceptions(i).error_code;
        --
        if L_error_code=1 then
          --
          L_error_code:=NULL;
          L_error_msg := SQL_LIB.CREATE_MSG(XXADEO_GLOBAL_VARS_SQL.VAL$DUP_REC_EXISTS_S9T);
          --
        end if;
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SHEET;
        L_issues_rt.row_seq           := sql%bulk_exceptions(i).error_index;
        L_issues_rt.error_key         := sqlerrm(-sql%bulk_exceptions(i).error_code);
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      end loop;
      --
    --
  END;
  --
END PROCESS_S9T_RDF_DOMAINS;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_RDF_DOMAINS_DEPT(O_error_message   IN OUT  LOGGER_LOGS.TEXT%TYPE) IS
  --
  L_program                       VARCHAR2(64) := 'XXADEO_MERCHHIER_RDF_DOM_SQL.PROCESS_S9T_RDF_DOMAINS_DEPT';
  --
  TYPE XXADEO_RDF_DOMAINS_DEPT_TYPE IS TABLE OF XXADEO_RDF_DOMAINS_DEPT_STG%ROWTYPE;
  L_xxadeo_rdf_domains_dept_tbl     XXADEO_RDF_DOMAINS_DEPT_TYPE := XXADEO_RDF_DOMAINS_DEPT_TYPE();
  L_temp_rec                        XXADEO_RDF_DOMAINS_DEPT_STG%ROWTYPE;
  L_issues_rt                       S9T_ERRORS%ROWTYPE;
  --
  L_error                           BOOLEAN:=FALSE;
  L_default_rec                     XXADEO_RDF_DOMAINS_DEPT_STG%ROWTYPE;
  L_row_seq                         SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE;
  L_error_code                      NUMBER;
  L_error_msg                       RTK_ERRORS.RTK_TEXT%TYPE;
  --
  DML_ERRORS    EXCEPTION;
  PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
  --
  cursor C_mandatory_ind is
    select group_id_mi,
           group_name_mi,
           dept_mi,
           dept_name_mi,
           domain_id_mi,
           load_sales_ind_mi,
           1 as dummy
      from (select column_key,
                   required_visual_ind
              from s9t_tmpl_cols_def
             where template_key   = TEMPLATE_KEY
               and wksht_key      = 'RDF_DOMAINS_BY_DEPT'
            ) pivot (max(required_visual_ind) as mi
                     for (column_key) in ('GROUP_ID'         as group_id,
                                          'GROUP_NAME'       as group_name,
                                          'DEPT'             as dept,
                                          'DEPT_NAME'        as dept_name,
                                          'DOMAIN_ID'        as domain_id,
                                          'LOAD_SALES_IND'   as load_sales_ind,
                                           null as dummy)); 
  L_mi_rec C_mandatory_ind%ROWTYPE;
  --
  cursor C_get_date_format IS
    select value
      from nls_database_parameters
     where parameter = 'NLS_DATE_FORMAT';
  --
BEGIN
  --
  OPEN C_get_date_format;
  FETCH C_get_date_format INTO L_date_format;
  CLOSE C_get_date_format;
  --
  -- get default values
  --
  for rec in (select group_id_dv,
                     group_name_dv,
                     dept_dv,
                     dept_name_dv,
                     domain_id_dv,
                     load_sales_ind_dv,
                     null as dummy
               from (select column_key,
                            default_value
                       from s9t_tmpl_cols_def
                      where template_key  = template_key
                        and wksht_key     = 'RDF_DOMAINS_BY_DEPT'
                     ) PIVOT (MAX(default_value) AS dv
                              FOR (column_key) IN ('GROUP_ID'         as group_id,
                                                   'GROUP_NAME'       as group_name,
                                                   'DEPT'             as dept,
                                                   'DEPT_NAME'        as dept_name,
                                                   'DOMAIN_ID'        as domain_id,
                                                   'LOAD_SALES_IND'   as load_sales_ind,
                                                   NULL AS dummy))) loop
    --
    -- group_id default
    --
    BEGIN
      --
      L_default_rec.group_id := rec.group_id_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_Id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_Key        := RDF_DOMAINS_DEPT$GROUP_ID;
        L_issues_rt.error_Key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- group_name default
    --
    BEGIN
      --
      L_default_rec.group_name := rec.group_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$GROUP_NAME;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT default
    --
    BEGIN
      --
      L_default_rec.dept := rec.dept_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_DEPT$DEPT;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT_NAME default
    --
    BEGIN
      --
      L_default_rec.dept_name := rec.dept_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_DEPT$DEPT_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Domain_id default
    --
    BEGIN
      --
      L_default_rec.domain_id := rec.domain_id_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_DEPT$DOMAIN_ID;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    --
    -- Load sales indicator default
    --
    BEGIN
      --
      L_default_rec.load_sales_ind := rec.load_sales_ind_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_DEPT$LOAD_SLS_IND;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
  end loop;
  --
  -- get mandatory indicators
  --
  open C_mandatory_ind;
  fetch C_mandatory_ind into L_mi_rec;
  close C_mandatory_ind;
  --
  -- validate datatypes and business rules
  --
  for rec IN (select r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_DEPT_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_DEPT$ACTION))               as ACTION,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_DEPT_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_DEPT$GROUP_ID))             as GROUP_ID,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_DEPT_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_DEPT$GROUP_NAME))           as GROUP_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_DEPT_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_DEPT$DEPT))                 as DEPT,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_DEPT_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_DEPT$DEPT_NAME))            as DEPT_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_DEPT_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_DEPT$DOMAIN_ID))            as DOMAIN_ID,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_DEPT_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_DEPT$LOAD_SLS_IND))         as LOAD_SALES_IND,
                     r.get_row_seq()                                                                                                          as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = L_file_id
                 and ss.sheet_name = SHEET_NAME_TRANS(RDF_DOMAINS_DEPT_SHEET)) loop
    -- 
    L_temp_rec                      := null;
    L_temp_rec.process_id           := L_process_id;
    L_temp_rec.xxadeo_process_id    := xxadeo_upl_process_id_seq.nextval;
    L_temp_rec.create_id            := LP_user;
    L_temp_rec.last_update_id       := LP_user;
    L_temp_rec.create_datetime      := sysdate;
    L_temp_rec.last_update_datetime := sysdate;
    L_error                         := FALSE;
    L_row_seq                       := rec.row_seq;
    --
    LP_chunk_id                     := 1;
    --
    -- validate data types
    --
    if rec.domain_id is null and rec.load_sales_ind is null then 
      --
      continue;
      --
    end if;
    --
    -- action
    --
    BEGIN
      --
      if rec.action is null then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      else
        --
        if rec.action = GP_upload_create or
           rec.action = GP_upload_update then
          --
          L_temp_rec.action := rec.action;
          --
        else
        --
          L_issues_rt                   := null;
          L_issues_rt.file_id           := L_file_id;
          L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
          L_issues_rt.column_key        := RDF_DOMAINS_DEPT$ACTION;
          L_issues_rt.row_seq           := rec.row_seq;
          L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
          --
          WRITE_S9T_ERROR(O_error_message => O_error_message,
                          I_issues_rt     => L_issues_rt);
          --
          L_error := TRUE;
          --
        end if;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- group_id
    --
    BEGIN
      --
      if L_mi_rec.group_id_mi   = 'Y'   and
         L_default_rec.group_id is null and
         rec.group_id is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_id := rec.group_id;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- group_name
    --
    BEGIN
      --
      if L_mi_rec.group_name_mi   = 'Y'   and
         L_default_rec.group_name is null and
         rec.group_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_name := rec.group_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT
    --
    BEGIN
      --
      if L_mi_rec.dept_mi   = 'Y'   and
         L_default_rec.dept is null and
         rec.dept is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept := rec.dept;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT_NAME
    --
    BEGIN
      --
      if L_mi_rec.dept_name_mi   = 'Y'        and
         L_default_rec.dept_name is null and
         rec.dept_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept_name := rec.dept_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DOMAIN ID
    --
    BEGIN
      --
      if L_mi_rec.domain_id_mi   = 'Y'   and
         L_default_rec.domain_id is null and
         rec.domain_id is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$DOMAIN_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.domain_id := rec.domain_id;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$DOMAIN_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- LOAD SALES INDICATOR
    --
    BEGIN
      --
      if L_mi_rec.load_sales_ind_mi   = 'Y'   and
         L_default_rec.load_sales_ind is null and
         rec.load_sales_ind is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$LOAD_SLS_IND;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.load_sales_ind := rec.load_sales_ind;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_DEPT$LOAD_SLS_IND;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- validate business rules
    --
    if not L_error then
      --
      if not VALIDATE_RDF_DOM_DEPT_ASSOC (O_error_message,
                                          L_error,
                                          L_temp_rec,
                                          L_row_seq) then
        --
        RETURN;
        --
      end if;
      --
    end if;
    --
    -- if line does not have error, insert in table to process
    --
    if NOT L_error then
      --
      L_temp_rec.row_seq   := rec.row_seq;
      --
      L_xxadeo_rdf_domains_dept_tbl.extend();
      L_xxadeo_rdf_domains_dept_tbl(L_xxadeo_rdf_domains_dept_tbl.count()) := L_temp_rec;
      --
    end if;
    --
  --
  end loop;
  --
  -- insert into stagging
  --
  BEGIN
    --
    forall i IN 1..L_xxadeo_rdf_domains_dept_tbl.count SAVE EXCEPTIONS
      insert into xxadeo_rdf_domains_dept_stg
        (process_id,
         xxadeo_process_id,
         action,
         group_id,
         group_name,
         dept,
         dept_name,
         domain_id,
         load_sales_ind,
         create_id,
         create_datetime,
         last_update_id,
         last_update_datetime,
         row_seq)
      values
        (L_xxadeo_rdf_domains_dept_tbl(i).process_id,
         L_xxadeo_rdf_domains_dept_tbl(i).xxadeo_process_id,
         L_xxadeo_rdf_domains_dept_tbl(i).action,
         L_xxadeo_rdf_domains_dept_tbl(i).group_id,
         L_xxadeo_rdf_domains_dept_tbl(i).group_name,
         L_xxadeo_rdf_domains_dept_tbl(i).dept,
         L_xxadeo_rdf_domains_dept_tbl(i).dept_name,
         L_xxadeo_rdf_domains_dept_tbl(i).domain_id,
         L_xxadeo_rdf_domains_dept_tbl(i).load_sales_ind,
         L_xxadeo_rdf_domains_dept_tbl(i).create_id,
         L_xxadeo_rdf_domains_dept_tbl(i).create_datetime,
         L_xxadeo_rdf_domains_dept_tbl(i).last_update_id,
         L_xxadeo_rdf_domains_dept_tbl(i).last_update_datetime,
         L_xxadeo_rdf_domains_dept_tbl(i).row_seq);
    --
  EXCEPTION
    --
    when DML_ERRORS then
      --
      for i in 1..sql%bulk_exceptions.COUNT loop
        --
        L_error_code:=sql%bulk_exceptions(i).error_code;
        --
        if L_error_code=1 then
          --
          L_error_code:=NULL;
          L_error_msg := SQL_LIB.CREATE_MSG(XXADEO_GLOBAL_VARS_SQL.VAL$DUP_REC_EXISTS_S9T);
          --
        end if;
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_DEPT_SHEET;
        L_issues_rt.row_seq           := sql%bulk_exceptions(i).error_index;
        L_issues_rt.error_key         := sqlerrm(-sql%bulk_exceptions(i).error_code);
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      end loop;
      --
    --
  END;
  --
END PROCESS_S9T_RDF_DOMAINS_DEPT;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_RDF_DOMAINS_CLASS(O_error_message   IN OUT  LOGGER_LOGS.TEXT%TYPE) IS
  --
  L_program                       VARCHAR2(64) := 'XXADEO_MERCHHIER_RDF_DOM_SQL.PROCESS_S9T_RDF_DOMAINS_CLASS';
  --
  TYPE XXADEO_RDF_DOMAINS_CLASS_TYPE IS TABLE OF XXADEO_RDF_DOMAINS_CLASS_STG%ROWTYPE;
  L_xxadeo_rdf_domains_class_tbl     XXADEO_RDF_DOMAINS_CLASS_TYPE := XXADEO_RDF_DOMAINS_CLASS_TYPE();
  L_temp_rec                         XXADEO_RDF_DOMAINS_CLASS_STG%ROWTYPE;
  L_issues_rt                        S9T_ERRORS%ROWTYPE;
  --
  L_error                            BOOLEAN:=FALSE;
  L_default_rec                      XXADEO_RDF_DOMAINS_CLASS_STG%ROWTYPE;
  L_row_seq                          SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE;
  L_error_code                       NUMBER;
  L_error_msg                        RTK_ERRORS.RTK_TEXT%TYPE;
  --
  DML_ERRORS    EXCEPTION;
  PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
  --
  cursor C_mandatory_ind is
    select group_id_mi,
           group_name_mi,
           dept_mi,
           dept_name_mi,
           class_mi,
           class_name_mi,
           domain_id_mi,
           load_sales_ind_mi,
           1 as dummy
      from (select column_key,
                   required_visual_ind
              from s9t_tmpl_cols_def
             where template_key   = TEMPLATE_KEY
               and wksht_key      = 'RDF_DOMAINS_BY_CLASS'
            ) pivot (max(required_visual_ind) as mi
                     for (column_key) in ('GROUP_ID'          as group_id,
                                          'GROUP_NAME'        as group_name,
                                          'DEPT'              as dept,
                                          'DEPT_NAME'         as dept_name,
                                          'CLASS'             as class,
                                          'CLASS_NAME'        as class_name,
                                          'DOMAIN_ID'         as domain_id,
                                          'LOAD_SALES_IND'    as load_sales_ind, 
                                           null as dummy));
  L_mi_rec C_mandatory_ind%ROWTYPE;
  --
  CURSOR C_get_date_format IS
    select value
      from nls_database_parameters
     where parameter = 'NLS_DATE_FORMAT';
  --
BEGIN
  --
  open C_get_date_format;
  fetch C_get_date_format into L_date_format;
  close C_get_date_format;
  --
  -- get default values
  --
  for rec in (select group_id_dv,
                     group_name_dv,
                     dept_dv,
                     dept_name_dv,
                     class_dv,
                     class_name_dv,
                     domain_id_dv,
                     load_sales_ind_dv,
                     null as dummy
               from (select column_key,
                            default_value
                       from s9t_tmpl_cols_def
                      where template_key  = template_key
                        and wksht_key     = 'RDF_DOMAINS_BY_CLASS'
                     ) PIVOT (MAX(default_value) AS dv
                              FOR (column_key) IN ('GROUP_ID'          as group_id,
                                                   'GROUP_NAME'        as group_name,
                                                   'DEPT'              as dept,
                                                   'DEPT_NAME'         as dept_name,
                                                   'CLASS'             as class,
                                                   'CLASS_NAME'        as class_name,
                                                   'DOMAIN_ID'         as domain_id,
                                                   'LOAD_SALES_IND'    as load_sales_ind,
                                                   NULL AS dummy))) loop
    --
    -- group_id default
    --
    BEGIN
      --
      L_default_rec.group_id := rec.group_id_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_Id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_Key        := RDF_DOMAINS_CLASS$GROUP_ID;
        L_issues_rt.error_Key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- group_name default
    --
    BEGIN
      --
      L_default_rec.group_name := rec.group_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$GROUP_NAME;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT default
    --
    BEGIN
      --
      L_default_rec.dept := rec.dept_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_CLASS$DEPT;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT_NAME default
    --
    BEGIN
      --
      L_default_rec.dept_name := rec.dept_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_CLASS$DEPT_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Class default
    --
    BEGIN
      --
      L_default_rec.class := rec.class_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_CLASS$CLASS;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Class name default
    --
    BEGIN
      --
      L_default_rec.class_name := rec.class_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_CLASS$CLASS_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Domain_id default
    --
    BEGIN
      --
      L_default_rec.domain_id := rec.domain_id_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_CLASS$DOMAIN_ID;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Load sales indicator default
    --
    BEGIN
      --
      L_default_rec.load_sales_ind := rec.load_sales_ind_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_CLASS$LOAD_SLS_IND;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
  end loop;
  --
  -- get mandatory indicators
  --
  open C_mandatory_ind;
  fetch C_mandatory_ind into L_mi_rec;
  close C_mandatory_ind;
  --
  -- validate datatypes and business rules
  --
  for rec IN (select r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_CLASS_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_CLASS$ACTION))                as ACTION,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_CLASS_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_CLASS$GROUP_ID))             as GROUP_ID,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_CLASS_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_CLASS$GROUP_NAME))           as GROUP_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_CLASS_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_CLASS$DEPT))                 as DEPT,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_CLASS_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_CLASS$DEPT_NAME))            as DEPT_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_CLASS_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_CLASS$CLASS))                as CLASS,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_CLASS_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_CLASS$CLASS_NAME))           as CLASS_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_CLASS_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_CLASS$DOMAIN_ID))            as DOMAIN_ID,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_CLASS_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_CLASS$LOAD_SLS_IND))         as LOAD_SALES_IND,
                     r.get_row_seq()                                                                                                           as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = L_file_id
                 and ss.sheet_name = SHEET_NAME_TRANS(RDF_DOMAINS_CLASS_SHEET)) loop
    --
    L_temp_rec                      := null;
    L_temp_rec.process_id           := L_process_id;
    L_temp_rec.xxadeo_process_id    := xxadeo_upl_process_id_seq.nextval;
    L_temp_rec.create_id            := LP_user;
    L_temp_rec.last_update_id       := LP_user;
    L_temp_rec.create_datetime      := sysdate;
    L_temp_rec.last_update_datetime := sysdate;
    L_error                         := FALSE;
    L_row_seq                       := rec.row_seq;
    --
    LP_chunk_id                     := 1;
    --
    -- validate data types
    --
    if rec.domain_id is null and rec.load_sales_ind is null then 
      --
      continue;
      --
    end if;
    --
    -- action
    --
    BEGIN
      --
      if rec.action is null then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      else
        --
        if rec.action = GP_upload_create or
           rec.action = GP_upload_update then
          --
          L_temp_rec.action := rec.action;
          --
        else
        --
          L_issues_rt                   := null;
          L_issues_rt.file_id           := L_file_id;
          L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
          L_issues_rt.column_key        := RDF_DOMAINS_CLASS$ACTION;
          L_issues_rt.row_seq           := rec.row_seq;
          L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
          --
          WRITE_S9T_ERROR(O_error_message => O_error_message,
                          I_issues_rt     => L_issues_rt);
          --
          L_error := TRUE;
          --
        end if;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- group_id
    --
    BEGIN
      --
      if L_mi_rec.group_id_mi   = 'Y'   and
         L_default_rec.group_id is null and
         rec.group_id is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_id := rec.group_id;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- group_name
    --
    BEGIN
      --
      if L_mi_rec.group_name_mi   = 'Y'   and
         L_default_rec.group_name is null and
         rec.group_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_name := rec.group_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT
    --
    BEGIN
      --
      if L_mi_rec.dept_mi   = 'Y'   and
         L_default_rec.dept is null and
         rec.dept is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept := rec.dept;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT_NAME
    --
    BEGIN
      --
      if L_mi_rec.dept_name_mi   = 'Y'        and
         L_default_rec.dept_name is null and
         rec.dept_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept_name := rec.dept_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- CLASS
    --
    BEGIN
      --
      if L_mi_rec.class_mi   = 'Y'   and
         L_default_rec.class is null and
         rec.class is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$CLASS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.class := rec.class;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$CLASS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- CLASS NAME
    --
    BEGIN
      --
      if L_mi_rec.class_name_mi   = 'Y'   and
         L_default_rec.class_name is null and
         rec.class_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$CLASS_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.class_name := rec.class_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$CLASS_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DOMAIN ID
    --
    BEGIN
      --
      if L_mi_rec.domain_id_mi   = 'Y'   and
         L_default_rec.domain_id is null and
         rec.domain_id is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$DOMAIN_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.domain_id := rec.domain_id;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$DOMAIN_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- LOAD SALES INDICATOR
    --
    BEGIN
      --
      if L_mi_rec.load_sales_ind_mi   = 'Y'   and
         L_default_rec.load_sales_ind is null and
         rec.load_sales_ind is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$LOAD_SLS_IND;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.load_sales_ind := rec.load_sales_ind;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_CLASS$LOAD_SLS_IND;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- validate business rules
    --
    if not L_error then
      --
      if not VALIDATE_RDF_DOM_CLASS_ASSOC (O_error_message,
                                           L_error,
                                           L_temp_rec,
                                           L_row_seq) then
        --
        RETURN;
        --
      end if;
      --
    end if;
    --
    -- if line does not have error, insert in table to process
    --
    if NOT L_error then
      --
      L_temp_rec.row_seq   := rec.row_seq;
      --
      L_xxadeo_rdf_domains_class_tbl.extend();
      L_xxadeo_rdf_domains_class_tbl(L_xxadeo_rdf_domains_class_tbl.count()) := L_temp_rec;
      --
    end if;
    --
  --
  end loop;
  --
  -- insert into stagging
  --
  BEGIN
    --
    forall i IN 1..L_xxadeo_rdf_domains_class_tbl.count SAVE EXCEPTIONS
      insert into xxadeo_rdf_domains_class_stg
        (process_id,
         xxadeo_process_id,
         action,
         group_id,
         group_name,
         dept,
         dept_name,
         class,
         class_name,
         domain_id,
         load_sales_ind,
         create_id,
         create_datetime,
         last_update_id,
         last_update_datetime,
         row_seq)
      values
        (L_xxadeo_rdf_domains_class_tbl(i).process_id,
         L_xxadeo_rdf_domains_class_tbl(i).xxadeo_process_id,
         L_xxadeo_rdf_domains_class_tbl(i).action,
         L_xxadeo_rdf_domains_class_tbl(i).group_id,
         L_xxadeo_rdf_domains_class_tbl(i).group_name,
         L_xxadeo_rdf_domains_class_tbl(i).dept,
         L_xxadeo_rdf_domains_class_tbl(i).dept_name,
         L_xxadeo_rdf_domains_class_tbl(i).class,
         L_xxadeo_rdf_domains_class_tbl(i).class_name,
         L_xxadeo_rdf_domains_class_tbl(i).domain_id,
         L_xxadeo_rdf_domains_class_tbl(i).load_sales_ind,
         L_xxadeo_rdf_domains_class_tbl(i).create_id,
         L_xxadeo_rdf_domains_class_tbl(i).create_datetime,
         L_xxadeo_rdf_domains_class_tbl(i).last_update_id,
         L_xxadeo_rdf_domains_class_tbl(i).last_update_datetime,
         L_xxadeo_rdf_domains_class_tbl(i).row_seq);
    --
  EXCEPTION
    --
    when DML_ERRORS then
      --
      for i in 1..sql%bulk_exceptions.COUNT loop
        --
        L_error_code:=sql%bulk_exceptions(i).error_code;
        --
        if L_error_code=1 then
          --
          L_error_code:=NULL;
          L_error_msg := SQL_LIB.CREATE_MSG(XXADEO_GLOBAL_VARS_SQL.VAL$DUP_REC_EXISTS_S9T);
          --
        end if;
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_CLASS_SHEET;
        L_issues_rt.row_seq           := sql%bulk_exceptions(i).error_index;
        L_issues_rt.error_key         := sqlerrm(-sql%bulk_exceptions(i).error_code);
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      end loop;
      --
    --
  END;
  --
END PROCESS_S9T_RDF_DOMAINS_CLASS;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_RDF_DOMAINS_SCLASS(O_error_message   IN OUT  LOGGER_LOGS.TEXT%TYPE) IS
  --
  L_program                       VARCHAR2(64) := 'XXADEO_MERCHHIER_RDF_DOM_SQL.PROCESS_S9T_RDF_DOMAINS_SCLASS';
  --
  TYPE XXADEO_RDF_DOMAINS_SCLASS_TYPE IS TABLE OF XXADEO_RDF_DOMAINS_SCLASS_STG%ROWTYPE;
  L_xxadeo_rdf_domains_sc_tbl         XXADEO_RDF_DOMAINS_SCLASS_TYPE := XXADEO_RDF_DOMAINS_SCLASS_TYPE();
  L_temp_rec                          XXADEO_RDF_DOMAINS_SCLASS_STG%ROWTYPE;
  L_issues_rt                         S9T_ERRORS%ROWTYPE;
  --
  L_error                             BOOLEAN:=FALSE;
  L_default_rec                       XXADEO_RDF_DOMAINS_SCLASS_STG%ROWTYPE;
  L_row_seq                           SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE;
  L_error_code                        NUMBER;
  L_error_msg                         RTK_ERRORS.RTK_TEXT%TYPE;
  --
  DML_ERRORS    EXCEPTION;
  PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
  --
  cursor C_mandatory_ind is
    select group_id_mi,
           group_name_mi,
           dept_mi,
           dept_name_mi,
           class_mi,
           class_name_mi,
           subclass_mi,
           subclass_name_mi,
           domain_id_mi,
           load_sales_ind_mi,
           1 as dummy
      from (select column_key,
                   required_visual_ind
              from s9t_tmpl_cols_def
             where template_key   = TEMPLATE_KEY
               and wksht_key      = 'RDF_DOMAINS_BY_SUBCLASS'
            ) pivot (max(required_visual_ind) as mi
                     for (column_key) in ('GROUP_ID'             as group_id,
                                          'GROUP_NAME'           as group_name,
                                          'DEPT'                 as dept,
                                          'DEPT_NAME'            as dept_name,
                                          'CLASS'                as class,
                                          'CLASS_NAME'           as class_name,
                                          'SUBCLASS'             as subclass,
                                          'SUBCLASS_NAME'        as subclass_name,
                                          'DOMAIN_ID'            as domain_id,
                                          'LOAD_SALES_IND'       as load_sales_ind,
                                           null as dummy));
  L_mi_rec C_mandatory_ind%ROWTYPE;
  --
  CURSOR C_get_date_format IS
    select value
      from nls_database_parameters
     where parameter = 'NLS_DATE_FORMAT';
  --
BEGIN
  --
  OPEN C_get_date_format;
  FETCH C_get_date_format INTO L_date_format;
  CLOSE C_get_date_format;
  --
  -- get default values
  --
  for rec in (select group_id_dv,
                     group_name_dv,
                     dept_dv,
                     dept_name_dv,
                     class_dv,
                     class_name_dv,
                     subclass_dv,
                     subclass_name_dv,
                     domain_id_dv,
                     load_sales_ind_dv,
                     null as dummy
               from (select column_key,
                            default_value
                       from s9t_tmpl_cols_def
                      where template_key  = template_key
                        and wksht_key     = 'RDF_DOMAINS_BY_SUBCLASS'
                     ) PIVOT (MAX(default_value) AS dv
                              FOR (column_key) IN ('GROUP_ID'             as group_id,
                                                   'GROUP_NAME'           as group_name,
                                                   'DEPT'                 as dept,
                                                   'DEPT_NAME'            as dept_name,
                                                   'CLASS'                as class,
                                                   'CLASS_NAME'           as class_name,
                                                   'SUBCLASS'             as subclass,
                                                   'SUBCLASS_NAME'        as subclass_name,
                                                   'DOMAIN_ID'            as domain_id,
                                                   'LOAD_SALES_IND'       as load_sales_ind,
                                                   NULL AS dummy))) loop
    --
    -- group_id default
    --
    BEGIN
      --
      L_default_rec.group_id := rec.group_id_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_Id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_Key        := RDF_DOMAINS_SC$GROUP_ID;
        L_issues_rt.error_Key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- group_name default
    --
    BEGIN
      --
      L_default_rec.group_name := rec.group_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$GROUP_NAME;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT default
    --
    BEGIN
      --
      L_default_rec.dept := rec.dept_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_SC$DEPT;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT_NAME default
    --
    BEGIN
      --
      L_default_rec.dept_name := rec.dept_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_SC$DEPT_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Class default
    --
    BEGIN
      --
      L_default_rec.class := rec.class_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_SC$CLASS;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Class name default
    --
    BEGIN
      --
      L_default_rec.class_name := rec.class_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_SC$CLASS_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- SUBCLASS default
    --
    BEGIN
      --
      L_default_rec.subclass := rec.subclass_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_SC$SUBCLASS;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- SUBCLASS NAME default
    --
    BEGIN
      --
      L_default_rec.subclass_name := rec.subclass_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_SC$SUBCLASS_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- domain_id default
    --
    BEGIN
      --
      L_default_rec.domain_id := rec.domain_id_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_SC$DOMAIN_ID;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Load sales indicator default
    --
    BEGIN
      --
      L_default_rec.load_sales_ind := rec.load_sales_ind_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key   := RDF_DOMAINS_SC$LOAD_SLS_IND;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    --
  end loop;
  --
  -- get mandatory indicators
  --
  open C_mandatory_ind;
  fetch C_mandatory_ind into L_mi_rec;
  close C_mandatory_ind;
  --
  -- validate datatypes and business rules
  --
  for rec IN (select r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SC_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_SC$ACTION))               as ACTION,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SC_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_SC$GROUP_ID))             as GROUP_ID,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SC_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_SC$GROUP_NAME))           as GROUP_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SC_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_SC$DEPT))                 as DEPT,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SC_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_SC$DEPT_NAME))            as DEPT_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SC_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_SC$CLASS))                as CLASS,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SC_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_SC$CLASS_NAME))           as CLASS_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SC_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_SC$SUBCLASS))             as SUBCLASS,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SC_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_SC$SUBCLASS_NAME))        as SUBCLASS_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SC_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_SC$DOMAIN_ID))            as DOMAIN_ID,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => RDF_DOMAINS_SC_SHEET,
                                                                     I_column_key                  => RDF_DOMAINS_SC$LOAD_SLS_IND))         as LOAD_SALES_IND,
                     r.get_row_seq()                                                                                                        as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = L_file_id
                 and ss.sheet_name = SHEET_NAME_TRANS(RDF_DOMAINS_SC_SHEET)) loop
    --
    L_temp_rec                      := null;
    L_temp_rec.process_id           := L_process_id;
    L_temp_rec.xxadeo_process_id    := xxadeo_upl_process_id_seq.nextval;
    L_temp_rec.create_id            := LP_user;
    L_temp_rec.last_update_id       := LP_user;
    L_temp_rec.create_datetime      := sysdate;
    L_temp_rec.last_update_datetime := sysdate;
    L_error                         := FALSE;
    L_row_seq                       := rec.row_seq;
    --
    LP_chunk_id                     := 1;
    --
    -- validate data types
    --
    if rec.domain_id is null and rec.load_sales_ind is null then 
      --
      continue;
      --
    end if;
    --
    -- action
    --
    BEGIN
      --
      if rec.action is null then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      else
        --
        if rec.action = GP_upload_create or
           rec.action = GP_upload_update then
          --
          L_temp_rec.action := rec.action;
          --
        else
        --
          L_issues_rt                   := null;
          L_issues_rt.file_id           := L_file_id;
          L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
          L_issues_rt.column_key        := RDF_DOMAINS_SC$ACTION;
          L_issues_rt.row_seq           := rec.row_seq;
          L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
          --
          WRITE_S9T_ERROR(O_error_message => O_error_message,
                          I_issues_rt     => L_issues_rt);
          --
          L_error := TRUE;
          --
        end if;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- group_id
    --
    BEGIN
      --
      if L_mi_rec.group_id_mi   = 'Y'   and
         L_default_rec.group_id is null and
         rec.group_id is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_id := rec.group_id;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- group_name
    --
    BEGIN
      --
      if L_mi_rec.group_name_mi   = 'Y'   and
         L_default_rec.group_name is null and
         rec.group_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_name := rec.group_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT
    --
    BEGIN
      --
      if L_mi_rec.dept_mi   = 'Y'   and
         L_default_rec.dept is null and
         rec.dept is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept := rec.dept;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT_NAME
    --
    BEGIN
      --
      if L_mi_rec.dept_name_mi   = 'Y'        and
         L_default_rec.dept_name is null and
         rec.dept_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept_name := rec.dept_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- CLASS
    --
    BEGIN
      --
      if L_mi_rec.class_mi   = 'Y'   and
         L_default_rec.class is null and
         rec.class is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$CLASS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.class := rec.class;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$CLASS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- CLASS NAME
    --
    BEGIN
      --
      if L_mi_rec.class_name_mi   = 'Y'   and
         L_default_rec.class_name is null and
         rec.class_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$CLASS_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.class_name := rec.class_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$CLASS_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- SUBCLASS
    --
    BEGIN
      --
      if L_mi_rec.subclass_mi   = 'Y'   and
         L_default_rec.subclass is null and
         rec.subclass is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$SUBCLASS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.subclass := rec.subclass;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$SUBCLASS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- SUBCLASS NAME
    --
    BEGIN
      --
      if L_mi_rec.subclass_name_mi   = 'Y'   and
         L_default_rec.subclass_name is null and
         rec.subclass_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$SUBCLASS_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.subclass_name := rec.subclass_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$SUBCLASS_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DOMAIN_ID
    --
    BEGIN
      --
      if L_mi_rec.domain_id_mi   = 'Y'   and
         L_default_rec.domain_id is null and
         rec.domain_id is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$DOMAIN_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.domain_id := rec.domain_id;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$DOMAIN_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- LOAD SALES INDICATOR
    --
    BEGIN
      --
      if L_mi_rec.load_sales_ind_mi   = 'Y'   and
         L_default_rec.load_sales_ind is null and
         rec.load_sales_ind is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$LOAD_SLS_IND;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.load_sales_ind := rec.load_sales_ind;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.column_key        := RDF_DOMAINS_SC$LOAD_SLS_IND;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- validate business rules
    --
    if not L_error then
      --
      if not VALIDATE_RDF_DOM_SCLASS_ASSOC (O_error_message,
                                            L_error,
                                            L_temp_rec,
                                            L_row_seq) then
        --
        RETURN;
        --
      end if;
      --
    end if;
    --
    -- if line does not have error, insert in table to process
    --
    if NOT L_error then
      --
      L_temp_rec.row_seq   := rec.row_seq;
      --
      L_xxadeo_rdf_domains_sc_tbl.extend();
      L_xxadeo_rdf_domains_sc_tbl(L_xxadeo_rdf_domains_sc_tbl.count()) := L_temp_rec;
      --
    end if;
    --
  --
  end loop;
  --
  -- insert into stagging
  --
  BEGIN
    --
    forall i IN 1..L_xxadeo_rdf_domains_sc_tbl.count SAVE EXCEPTIONS
      insert into xxadeo_rdf_domains_sclass_stg
        (process_id,
         xxadeo_process_id,
         action,
         group_id,
         group_name,
         dept,
         dept_name,
         class,
         class_name,
         subclass,
         subclass_name,
         domain_id,
         load_sales_ind,
         create_id,
         create_datetime,
         last_update_id,
         last_update_datetime,
         row_seq)
      values
        (L_xxadeo_rdf_domains_sc_tbl(i).process_id,
         L_xxadeo_rdf_domains_sc_tbl(i).xxadeo_process_id, 
         L_xxadeo_rdf_domains_sc_tbl(i).action,
         L_xxadeo_rdf_domains_sc_tbl(i).group_id,
         L_xxadeo_rdf_domains_sc_tbl(i).group_name,
         L_xxadeo_rdf_domains_sc_tbl(i).dept,
         L_xxadeo_rdf_domains_sc_tbl(i).dept_name,
         L_xxadeo_rdf_domains_sc_tbl(i).class,
         L_xxadeo_rdf_domains_sc_tbl(i).class_name,
         L_xxadeo_rdf_domains_sc_tbl(i).subclass,
         L_xxadeo_rdf_domains_sc_tbl(i).subclass_name,
         L_xxadeo_rdf_domains_sc_tbl(i).domain_id,
         L_xxadeo_rdf_domains_sc_tbl(i).load_sales_ind,
         L_xxadeo_rdf_domains_sc_tbl(i).create_id,
         L_xxadeo_rdf_domains_sc_tbl(i).create_datetime,
         L_xxadeo_rdf_domains_sc_tbl(i).last_update_id,
         L_xxadeo_rdf_domains_sc_tbl(i).last_update_datetime,
         L_xxadeo_rdf_domains_sc_tbl(i).row_seq);
    --
  EXCEPTION
    --
    when DML_ERRORS then
      --
      for i in 1..sql%bulk_exceptions.COUNT loop
        --
        L_error_code:=sql%bulk_exceptions(i).error_code;
        --
        if L_error_code=1 then
          --
          L_error_code:=NULL;
          L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T');
          --
        end if;
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := RDF_DOMAINS_SC_SHEET;
        L_issues_rt.row_seq           := sql%bulk_exceptions(i).error_index;
        L_issues_rt.error_key         := sqlerrm(-sql%bulk_exceptions(i).error_code);
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      end loop;
      --
    --
  END;
  --
END PROCESS_S9T_RDF_DOMAINS_SCLASS;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
  --
  L_program          VARCHAR2(64) := 'XXADEO_MERCHHIER_RDF_DOM_SQL.PROCESS_S9T';
  --
  L_file             S9T_FILE;
  L_sheets           S9T_PKG.names_map_typ;
  L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
  INVALID_FORMAT     EXCEPTION;
  PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
  MAX_CHAR           EXCEPTION;
  PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
  L_issues_rt        S9T_ERRORS%ROWTYPE;
  L_error            BOOLEAN := FALSE;
  --
BEGIN
  --
  COMMIT;
  S9T_PKG.ODS2OBJ(I_file_id);
  COMMIT;
  --
  L_file_id    := I_file_id;
  L_process_id :=  I_process_id;
  --
  L_file := s9t_file(I_file_id,TRUE);
  LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
  if S9T_PKG.CODE2DESC(O_error_message,
                       template_category,
                       L_file,
                       TRUE) = FALSE then
     return FALSE;
  end if;
  --
  S9T_PKG.SAVE_OBJ(L_file);
  --
  LP_errors_tab   := NEW errors_tab_typ();
  --
  if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
    --
    L_issues_rt                   := null;
    L_issues_rt.file_id           := L_file_id;
    L_issues_rt.column_key        := NULL;
    L_issues_rt.error_type        := NULL;
    L_issues_rt.error_key         := 'S9T_INVALID_TEMPLATE';
    --
    WRITE_S9T_ERROR(O_error_message => O_error_message,
                    I_issues_rt     => L_issues_rt);
  else
    --
    sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                            L_file.user_lang);
    --
    -- load template column indexex object
    --
    L_xxadeo_wksht_column_idx_tbl := XXADEO_WKSHT_COLUMN_IDX_TBL();
    --
    if XXADEO_CUSTOM_UPLOAD_SQL.GET_TEMPLATE_COLUMN_INDEXES(O_error_message                => O_error_message,
                                                            IO_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                            I_template_key                 => TEMPLATE_KEY) = FALSE then
      --
      COMMIT;
      RETURN FALSE;
      --
    end if;
    --
    PROCESS_S9T_RDF_DOMAINS(O_error_message => O_error_message);
    PROCESS_S9T_RDF_DOMAINS_DEPT(O_error_message => O_error_message);
    PROCESS_S9T_RDF_DOMAINS_CLASS(O_error_message => O_error_message);
    PROCESS_S9T_RDF_DOMAINS_SCLASS(O_error_message => O_error_message);
    --
  end if;
  --
  for rec in (select *
                from xxadeo_rdf_domains_dept_stg stg
               where stg.process_id = L_process_id) loop
    --
    L_error := FALSE;
    --
    if rec.action = GP_upload_create then
      --
      if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_DOMAIN_ASSOC(O_error_message => O_error_message,
                                                              I_process_id    => rec.xxadeo_process_id,
                                                              I_domain_id     => rec.domain_id,
                                                              I_dept          => rec.dept) then
        --
        WRITE_ERROR(L_process_id,
                    svc_admin_upld_er_seq.nextval,
                    LP_chunk_id,
                    RDF_DOMAINS_DEPT_SHEET,
                    rec.row_seq,
                    RDF_DOMAINS_DEPT$DEPT,
                    XXADEO_GLOBAL_VARS_SQL.VAL$DOMAIN_MERCHHIER_EXISTS);
        --
        L_error := TRUE;
        --
      end if;
      --
    end if;
    --
    if L_error = TRUE then
      --
      delete 
        from xxadeo_rdf_domains_dept_stg stg
       where stg.xxadeo_process_id = rec.xxadeo_process_id;
      
      --
    end if;
    --
    L_error := FALSE;
    --
  end loop;
  --
  for rec in (select *
                from xxadeo_rdf_domains_class_stg stg
               where stg.process_id = L_process_id) loop
    --
    L_error := FALSE;
    --
    if rec.action = GP_upload_create then
      --
      if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_DOMAIN_ASSOC(O_error_message => O_error_message,
                                                              I_process_id    => rec.xxadeo_process_id,
                                                              I_domain_id     => rec.domain_id,
                                                              I_dept          => rec.dept,
                                                              I_class         => rec.class) then
        WRITE_ERROR(L_process_id,
                    svc_admin_upld_er_seq.nextval,
                    LP_chunk_id,
                    RDF_DOMAINS_CLASS_SHEET,
                    rec.row_seq,
                    RDF_DOMAINS_CLASS$CLASS,
                    XXADEO_GLOBAL_VARS_SQL.VAL$DOMAIN_MERCHHIER_EXISTS); 
        --
        L_error := TRUE;
        --
      end if;
      --
    end if;
    --
    if L_error = TRUE then
      --
      delete 
        from xxadeo_rdf_domains_class_stg stg
       where stg.xxadeo_process_id = rec.xxadeo_process_id;
      --
    end if;
    --
    L_error := FALSE;
    --
  end loop;
  --
  for rec in (select *
                from xxadeo_rdf_domains_sclass_stg stg
               where stg.process_id = L_process_id) loop
    --
    L_error := FALSE;
    --
    if rec.action = GP_upload_create then
      --
      if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_DOMAIN_ASSOC(O_error_message => O_error_message,
                                                              I_process_id    => rec.xxadeo_process_id,
                                                              I_domain_id     => rec.domain_id,
                                                              I_dept          => rec.dept,
                                                              I_class         => rec.class,
                                                              I_subclass      => rec.subclass) then
        WRITE_ERROR(L_process_id,
                    svc_admin_upld_er_seq.nextval,
                    LP_chunk_id,
                    RDF_DOMAINS_SC_SHEET,
                    rec.row_seq,
                    RDF_DOMAINS_SC$SUBCLASS,
                    XXADEO_GLOBAL_VARS_SQL.VAL$DOMAIN_MERCHHIER_EXISTS); 
        --
        L_error := TRUE;
        --
      end if;
      --
    end if;
    --
    if L_error = TRUE then
      --
      delete 
        from xxadeo_rdf_domains_sclass_stg stg
       where stg.xxadeo_process_id = rec.xxadeo_process_id;
      
      --
    end if;
    --
    L_error := FALSE;
    --
  end loop;
  --
  O_error_count := LP_s9t_errors_tab.COUNT() + LP_errors_tab.COUNT();
  --
  FORALL i IN 1..LP_s9t_errors_tab.COUNT
     insert into s9t_errors
          values LP_s9t_errors_tab(i);
  LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
  --
  FORALL i IN 1..LP_errors_tab.COUNT
    insert into svc_admin_upld_er
         values LP_errors_tab(i);
  LP_errors_tab := NEW errors_tab_typ();
  --Update process$status in svc_process_tracker
  if O_error_count = 0 then
     L_process_status := 'PS';
  else
     L_process_status := 'PE';
  end if;
  --
  update svc_process_tracker
     set status     = L_process_status,
         file_id    = I_file_id
   where process_id = I_process_id;
  --
  COMMIT;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when INVALID_FORMAT then
    --
    ROLLBACK;
    --
    O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                          NULL,
                                          NULL,
                                          NULL);
    LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
    --
    L_issues_rt                   := null;
    L_issues_rt.file_id           := L_file_id;
    L_issues_rt.column_key        := NULL;
    L_issues_rt.error_type        := NULL;
    L_issues_rt.error_key         := 'INV_FILE_FORMAT';
    --
    WRITE_S9T_ERROR(O_error_message => O_error_message,
                    I_issues_rt     => L_issues_rt);
    --
    O_error_count := LP_s9t_errors_tab.count();
    forall i IN 1..O_error_count
       insert into s9t_errors
            values LP_s9t_errors_tab(i);

    update svc_process_tracker
       set status       = 'PE',
           file_id      = I_file_id
     where process_id   = I_process_id;
     --
     COMMIT;
     --
     RETURN FALSE;
     --
  when MAX_CHAR then
    --
    ROLLBACK;
    --
    O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
    Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
    L_issues_rt                   := null;
    L_issues_rt.file_id           := L_file_id;
    L_issues_rt.column_key        := NULL;
    L_issues_rt.error_type        := NULL;
    L_issues_rt.error_key         := 'EXCEEDS_4000_CHAR';
    --
    write_s9t_error(O_error_message => O_error_message,
                    I_issues_rt     => L_issues_rt);
    --
    O_error_count := Lp_s9t_errors_tab.count();
    --
    forall i IN 1..O_error_count
      --
      insert into s9t_errors values Lp_s9t_errors_tab (i);
      --
      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      --
      COMMIT;
      --
    RETURN FALSE;
    --
  when OTHERS then
    --
    ROLLBACK;
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END PROCESS_S9T;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS
BEGIN
  --
  delete from xxadeo_rdf_domains_stg
   where process_id = I_process_id;
  --
  delete from xxadeo_rdf_domains_dept_stg
   where process_id = I_process_id;
  --
  delete from xxadeo_rdf_domains_class_stg
   where process_id = I_process_id;
  --
  delete from xxadeo_rdf_domains_sclass_stg
   where process_id = I_process_id; 
  --
END;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
  --
  L_program          VARCHAR2(64)                    :='XXADEO_MERCHHIER_RDF_DOM_SQL.PROCESS';
  --
  L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
  L_err_count        VARCHAR2(1);
  L_warn_count       VARCHAR2(1);
  L_errors_count     NUMBER;
  --
  SQL_ERROR          EXCEPTION;
  --
  cursor C_GET_ERR_COUNT is
     select 'x'
       from svc_admin_upld_er
      where process_id = I_process_id
        and error_type = 'E';

  cursor C_GET_WARN_COUNT is
     select 'x'
       from svc_admin_upld_er
      where process_id = I_process_id
        and error_type = 'W';
  --
BEGIN
  --
  LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
  LP_chunk_id     := I_chunk_id;
  --
  LP_s9t_errors_tab := s9t_errors_tab_typ();
  --
  if SETUP_RDF_DOMAINS(O_error_message   => O_error_message) = FALSE then
    --
    COMMIT;
    --
    RAISE SQL_ERROR;
    --
  end if;
  --
  COMMIT;
  --
  if SETUP_RDF_DOMAINS_DEPT(O_error_message   => O_error_message) = FALSE then
    --
    COMMIT;
    --
    RAISE SQL_ERROR;
    --
  end if;
  --
  COMMIT;
  --
  --
  if SETUP_RDF_DOMAINS_CLASS(O_error_message   => O_error_message) = FALSE then
    --
    COMMIT;
    --
    RAISE SQL_ERROR;
    --
  end if;
  --
  if SETUP_RDF_DOMAINS_SUBCLASS(O_error_message   => O_error_message) = FALSE then
    --
    COMMIT;
    --
    RAISE SQL_ERROR;
    --
  end if;
  --
  COMMIT;
  --
  L_errors_count := L_errors_count + LP_s9t_errors_tab.count();
  --
  O_error_count := LP_errors_tab.COUNT();
  --
  FORALL i IN 1..O_error_count
     --
     insert into svc_admin_upld_er
          values LP_errors_tab(i);
     --
  LP_errors_tab := NEW errors_tab_typ();
  --
  open  c_get_err_count;
  fetch c_get_err_count into L_err_count;
  close c_get_err_count;
  --
  open  c_get_warn_count;
  fetch c_get_warn_count into L_warn_count;
  close c_get_warn_count;
  --
  if L_err_count is NOT NULL then
    --
    L_process_status := 'PE';
    --
  elsif L_warn_count is NOT NULL then
    --
    L_process_status := 'PW';
    --
  else
    --
    L_process_status := 'PS';
    --
  end if;
  --
  update svc_process_tracker
     set status = (CASE
                    when status = 'PE' then
                     'PE'
                    else
                     L_process_status
                  END),
         action_date = SYSDATE
   where process_id = I_process_id;
  --
  CLEAR_STAGING_DATA(I_process_id);
  --
  COMMIT;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when SQL_ERROR then
    --
    update svc_process_tracker
       set status = 'PE',
           action_date = SYSDATE
     where process_id = I_process_id;
    --
    CLEAR_STAGING_DATA(I_process_id);
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
  --
  when OTHERS then
    --
    CLEAR_STAGING_DATA(I_process_id);
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END PROCESS;
----------------------------------------------------------------------------------------------
END XXADEO_MERCHHIER_RDF_DOM_SQL;
/
