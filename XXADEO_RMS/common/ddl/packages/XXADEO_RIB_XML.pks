create or replace package XXADEO_RIB_XML is
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_RIB_XML.pks
* Description:   Package to add namespaces to xml and create XML elements
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/

  function getXMLNamespaceURL return varchar2;
  
  function addElement(parentElem in xmldom.DOMElement, name in varchar2)
    return xmldom.DOMElement;
    
  procedure addElement(parentElem in xmldom.DOMElement,
                       name       in varchar2,
                       value      in varchar2);

  procedure addElement(parentElem in xmldom.DOMElement,
                       name       in varchar2,
                       value      in number);
                       
  procedure addElement(parentElem in xmldom.DOMElement,
                           name       in varchar2,
                           value      in date);

  
end XXADEO_RIB_XML;
/