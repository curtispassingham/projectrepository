CREATE OR REPLACE PACKAGE BODY XXADEO_ITEM_LIFE_CYCLE_SQL AS

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-09-26 - Jorge Agra - Reset to IS-INIT suppliers in IS-STOP with stop  */
/*                           date null                                        */
/* 2018-09-27 - Jorge Agra - BUG#492 Missing check for CFG_ITEM orderable_ind */
/******************************************************************************/

--------------------------------------------------------------------------------
-- Private Constants
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- Private Types
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- Private Globals
--------------------------------------------------------------------------------

-- Are globals initialized?
GV_globals_initialized              boolean       := FALSE;
GV_init_error_msg                   varchar2(500) := null;

-- today for RMS
GV_rms_now                          date          := get_vdate;

GV_bulk_size                        NUMBER        := 100;
GV_user_id                          VARCHAR2(30)  := NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'),  USER);
--GV_user_id                          VARCHAR2(30)  := 'RB107';

GV_time                             NUMBER := dbms_utility.get_time();

--------------------------------------------------------------------------------
-- Forward declarations of private functions
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--Function Name : INITIALIZE_GLOBALS
--Purpose       : TODO
--------------------------------------------------------------------------------
FUNCTION initialize_globals
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : RESET_STOP_TO_INIT_IS
--Purpose       : Reset item/supplier links to init
--------------------------------------------------------------------------------
FUNCTION RESET_STOP_TO_INIT_IS(O_error_message  IN OUT  VARCHAR2)
RETURN BOOLEAN ;
--------------------------------------------------------------------------------
--Function Name : UPDATE_ITEM_STATUS
--Purpose       : TODO
--------------------------------------------------------------------------------
FUNCTION update_item_status(O_error_message IN OUT  VARCHAR2,
                            I_pid           IN      number default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : UPDATE_CONDITIONS
--Purpose       : TODO
--------------------------------------------------------------------------------
FUNCTION update_conditions(O_error_message  IN OUT  VARCHAR2,
                           I_pid            IN      number default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : EXEC
--Purpose       : TODO
--------------------------------------------------------------------------------
FUNCTION execute_cond( O_error_message IN OUT  VARCHAR2,
                       O_rule_changes  OUT     number,
                       O_val_status    OUT     xxadeo_lfcy_item_cond.val_status%TYPE,
                       I_package       IN      xxadeo_system_rules.package_name%TYPE,
                       I_func          IN      xxadeo_system_rules.function_name%TYPE,
                       I_pid           IN      number,
                       I_param1        IN      xxadeo_system_rules_detail.value_1%TYPE,
                       I_param2        IN      xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------
--Function Name : initialize_work_is
--Purpose       : TODO
--------------------------------------------------------------------------------
FUNCTION INITIALIZE_WORK_IS(  O_error_message     IN OUT  VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------
--Function Name : initialize_work_ib
--Purpose       : TODO
--------------------------------------------------------------------------------
FUNCTION INITIALIZE_WORK_IB(O_error_message     IN OUT  VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : MERGE_UDAS_IB
--Purpose       : Creates or updates lifecycle udas for IB rows
--                considering the state in XXADE_LFCY_WORK
--------------------------------------------------------------------------------
FUNCTION merge_udas_ib( O_error_message   IN OUT  VARCHAR2,
                        I_pid             IN      number default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : MERGE_CFAS_IS
--Purpose       : Creates or updates lifecycle cfass for IS rows
--                considering the state in XXADE_LFCY_WORK
--------------------------------------------------------------------------------
FUNCTION merge_cfas_is( O_error_message   IN OUT  VARCHAR2,
                        I_pid             IN      number default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : FILL_IB_DATES
--Purpose       : Set CFA's dates of state transition
--                IB-INIT, IB-ASI, IB-DEL
--------------------------------------------------------------------------------
FUNCTION fill_ib_dates( O_error_message   IN OUT  VARCHAR2,
                        I_pid             IN      number default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : DISC_IB_LOC
--Purpose       : Set ITEM_LOC.STATUS to C for DISC items
--------------------------------------------------------------------------------
FUNCTION disc_ib_loc( O_error_message   IN OUT  VARCHAR2,
                      I_pid             IN      number default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : DBG
--Purpose       : write to DEBUG_MSG table
--------------------------------------------------------------------------------
procedure DBG(I_program varchar2, I_message varchar2)
is
  ---
  L_time      number;
  ---
BEGIN
  ---
  L_time := dbms_utility.get_time();
  ---
  dbms_application_info.set_client_info(I_message);
  ---
  DBG_SQL.MSG(I_program, I_message || ' / dT(ms)=' || to_char(L_time - GV_time));
  ---
  GV_time := L_time;
  ---
end;

--------------------------------------------------------------------------------
-- Public functions implementation
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
FUNCTION PRE_PROCESS(O_error_message    IN OUT  VARCHAR2)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.PRE_PROCESS';
  ---
BEGIN
  if initialize_globals = FALSE then
    O_error_message := GV_init_error_msg;
    return FALSE;
  end if;
  ---
  -- cleanup process execution status - dangerous if middle of proc
  ---
  DBG(L_program, 'PRE: Process cleanup');
  ---
  if process_cleanup(O_error_message) = FALSE then
    return FALSE;
  end if;
  ---
  -- fills working table for item/supplier lifecycle processing
  ---
  DBG(L_program, 'PRE: INITIALIZE_WORK_IS');
  ---
  if initialize_work_is(O_error_message) = FALSE then
    return FALSE;
  end if;
  ---
  -- fills working table for item/bu lifecycle processing
  ---
  DBG(L_program, 'PRE: INITIALIZE_WORK_IB');
  ---
  if initialize_work_ib(O_error_message) = FALSE then
    return FALSE;
  end if;
  ---
  -- insert state uda for item/bu's with a subscription (uda)
  -- and without lifecycle state (uda)
  ---
  /*
  if merge_udas_ib(O_error_message) = FALSE then
      return FALSE;
  end if;
  ---
  -- insert/update state cfa for entries in item_suppliers without one
  -- No row for cfa group or column for storing state is null in row
  ---
  if merge_cfas_is(O_error_message) = FALSE then
      return FALSE;
  end if;
  */
  ---
  -- check items with all conditions met (val_status = Y) and updates
  -- lifecycle uda (ib) or cfa (is) with new status
  ---
  DBG(L_program, 'PRE: UPDATE_ITEM_STATUS');
  ---
  if update_item_status(O_error_message) = FALSE then
    return FALSE;
  end if;
  ---
  -- insert conditions related to current state and remove non related
  ---
  DBG(L_program, 'PRE: UPDATE_CONDITIONS');
  ---
  if update_conditions(O_error_message) = FALSE then
    return FALSE;
  end if;
  ---
  -- updates work (ib and is) with next conditions to check
  -- this will allow bulk processiong of conditions in
  -- validation functions
  ---
  /*
  DBG(L_program, 'PRE: PREPARE_EXECUTION');
  ---
  if prepare_execution(O_error_message) = FALSE then
    return FALSE;
  end if;
  ---
  */
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END PRE_PROCESS;
--------------------------------------------------------------------------------
FUNCTION POST_PROCESS(O_error_message  IN OUT VARCHAR2,
                      I_pid            IN     number)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.POST_PROCESS';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'POST_PROCESS');
  ---
  DBG(L_program, 'Start');
  ---
  -- check items with all conditions met (val_status = Y) and updates
  -- lifecycle uda (ib) or cfa (is) with new status
  ---
  DBG(L_program, 'PRE: UPDATE_ITEM_STATUS');
  ---
  if update_item_status(O_error_message,
                        I_pid) = FALSE then
    return FALSE;
  end if;
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'POST_PROCESS');
  ---
  -- insert conditions related to current state and remove non related
  ---
  DBG(L_program, 'PRE: UPDATE_CONDITIONS');
  ---
  if update_conditions(O_error_message,
                       I_pid) = FALSE then
    return FALSE;
  end if;
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'POST_PROCESS');
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END POST_PROCESS;
--------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message  IN OUT  VARCHAR2,
                 I_item           IN      item_master.item%TYPE default null,
                 I_entity_type    IN      xxadeo_lfcy_work.lfcy_entity_type%type default null,
                 I_entity         IN      xxadeo_lfcy_work.lfcy_entity%TYPE default null,
                 I_cleanup        IN      BOOLEAN default TRUE,
                 I_loop           IN      BOOLEAN default TRUE)
RETURN BOOLEAN IS
  ---
  L_program     VARCHAR2(100) := CONST$PackageName || '.PROCESS';
  L_pid         number := 0;
  L_return      BOOLEAN;
  L_single_item boolean;
  ---
BEGIN
  ---
  DBG(L_program, 'Start');
  ---
  dbms_application_info.set_module(CONST$PackageName, 'PROCESS');
  ---
  if initialize_globals = FALSE then
    O_error_message := GV_init_error_msg;
    return FALSE;
  end if;
  ---
  if I_item is not null and (I_entity_type is null or I_entity is null) then
    O_error_message := 'Missing parameters (Entity Type or Entity)';
    return FALSE;
  end if;
  ---
  if I_item is not null then
    L_single_item := true;
  else
    L_single_item := false;
  end if;
  ---
  if L_single_item then
    DBG(L_program, 'Single Item');
  else
    DBG(L_program, 'All Items');
  end if;
  ---
  select xxadeo_lfcy_process_seq.nextval
    into L_pid
    from dual;
  ---
  if I_item is not null then
    ---
    update xxadeo_lfcy_work
       set process_id = L_pid
     where item in (
          select im.item
            from item_master im
           where (item = I_item
              or  item_parent = I_item
              or  item_grandparent = I_item))
       and lfcy_entity = I_entity
       and lfcy_entity_type = I_entity_type
       and process_id is null
       ;
       ---
      DBG(L_program, 'SINGLE ITEM: UPDATE WORK. #ROWCOUNT=' || to_char(SQL%ROWCOUNT));
    ---
    update xxadeo_lfcy_item_cond
       set process_id = L_pid
     where item in (
          select im.item
            from item_master im
           where (item = I_item
              or  item_parent = I_item
              or  item_grandparent = I_item))
       and lfcy_entity = I_entity
       and lfcy_entity_type = I_entity_type
       and process_id is null
       and nvl(next_val_dt,get_vdate-1) <= GV_rms_now;
       ---
      DBG(L_program, 'SINGLE ITEM: UPDATE COND. #ROWCOUNT=' || to_char(SQL%ROWCOUNT));
  else
    update xxadeo_lfcy_work
       set process_id = L_pid
     where process_id is null;
    ---
    DBG(L_program, 'UPDATE WORK=' || to_char(SQL%ROWCOUNT));
    ---
    update xxadeo_lfcy_item_cond
       set process_id = L_pid
     where process_id is null
       and nvl(next_val_dt,get_vdate-1) <= GV_rms_now;
    ---
    DBG(L_program, 'UPDATE ITEM COND=' || to_char(SQL%ROWCOUNT));
    ---
  end if;
  ---
  DBG(L_program, 'PRE: PROCESS_EXEC');
  ---
  if process_exec( O_error_message,
                   L_pid,
                   I_loop,
                   L_single_item) = FALSE then
    L_return := FALSE;
  end if;
  ---
  DBG(L_program, 'POS: PROCESS_EXEC');
  ---
  dbms_application_info.set_module(CONST$PackageName, 'PROCESS');
  ---
  if I_cleanup = TRUE then
    ---
    update xxadeo_lfcy_work
       set process_id = null
     where process_id = L_pid;
    ---
    DBG(L_program, 'POS: CLEANUP WORK=' || to_char(SQL%ROWCOUNT));
    ---
    update xxadeo_lfcy_item_cond
       set process_id = null
     where process_id = L_pid;
    ---
    DBG(L_program, 'POS: CLEANUP ITEM COND=' || to_char(SQL%ROWCOUNT));
    ---
  end if;
  ---
  return L_return;
  ---
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END PROCESS;
--------------------------------------------------------------------------------
FUNCTION PROCESS_DEPTS(O_error_message  IN OUT  VARCHAR2,
                       I_num_threads    IN      varchar2 default null,
                       I_thread_val     IN      varchar2 default null,
                       I_dept           IN      item_master.dept%TYPE default 0,
                       I_cleanup        IN      BOOLEAN default TRUE,
                       I_loop           IN      BOOLEAN default TRUE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.PROCESS_DEPTS';
  L_pid       number := 0;
  L_return    BOOLEAN;
  ---
  cursor C_depts is
    select driver_value dept
      from v_restart_dept
     where driver_name = 'DEPT'
       and num_threads = to_number(I_num_threads)
       and thread_val = to_number(I_thread_val)
       and driver_value > nvl(I_dept,0)
     order by driver_value;
  ---
  L_rows_work number := 0;
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName, 'PROCESS_DEPTS');
  ---
  DBG(L_program, 'START: vdate=' || to_char(GV_rms_now,'YYYY-MM-DD HH24:MI:SS') || ' ,I_num_threads=' || to_char(nvl(I_num_threads,0)) || ', I_thread_val=' || to_char(nvl(I_thread_val,0)) || ', I_dept=' || to_char(nvl(I_dept,0)));
  ---
  if initialize_globals = FALSE then
    O_error_message := GV_init_error_msg;
    return FALSE;
  end if;
  ---
  --for row in c_depts loop
    ---
    L_rows_work := 0;
    ---
    select xxadeo_lfcy_process_seq.nextval
      into L_pid
      from dual;
    ---
    dbms_application_info.set_module(CONST$PackageName || '#' || L_pid, 'PROCESS_DEPTS');
    dbms_application_info.set_client_info('UPDATE WORK PID');
    ---
    --DBG(L_program, 'LOOP: pid=' || to_char(L_pid) || ', dept=' || to_char(row.dept));
    ---
    update xxadeo_lfcy_work
       set process_id = L_pid
           ,lfcy_status_new = null
     where process_id is null
       and item_dept in (
                          select driver_value
                            from v_restart_dept
                           where driver_name = 'DEPT'
                             and num_threads = to_number(I_num_threads)
                             and thread_val = to_number(I_thread_val)
                             and driver_value > nvl(I_dept,0)
        )
       ;
    ---
    L_rows_work := SQL%ROWCOUNT;
    DBG(L_program, 'POS: UPD PID WORK. #ROWS=' || to_char(SQL%ROWCOUNT));
    ---
    dbms_application_info.set_client_info('UPDATE COND PID');
    ---
    update xxadeo_lfcy_item_cond
       set  process_id = L_pid
     where process_id is null
       and item in (select item from xxadeo_lfcy_work where process_id = L_pid);
    ---
    DBG(L_program, 'POS: UPD ITEM COND. #ROWS=' || to_char(SQL%ROWCOUNT));
    ---
    if process_exec( O_error_message,
                     L_pid,
                     I_loop,
                     false) = FALSE then
      return false;
    end if;
    ---
    dbms_application_info.set_module(CONST$PackageName || '#' || L_pid, 'PROCESS_DEPTS');
    dbms_application_info.set_client_info('PROCESS_EXEC DONE');
    ---
    --DBG(L_program, 'POS: PROCESS_EXEC. DEPT=' || to_char(row.dept));
    ---
    if I_cleanup = TRUE then
      ---
      if process_cleanup(O_error_message, L_pid) = FALSE then
        return FALSE;
      end if;
    end if;
    ---
    commit;
    ---
  --end loop;
  ---
  return L_return;
  ---
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CLEANUP(O_error_message  IN OUT  VARCHAR2, I_pid number default null)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.PROCESS_CLEANUP';
  ---
BEGIN
  if initialize_globals = FALSE then
    O_error_message := GV_init_error_msg;
    return FALSE;
  end if;
  ---
  update xxadeo_lfcy_work
     set process_id = null
   where (i_pid is null or I_pid = process_id);
  ---
  DBG(L_program, 'POS: CLEAN WORK. PID=' || to_char(I_pid) || ', #ROWS=' || to_char(SQL%ROWCOUNT));
  ---
  update xxadeo_lfcy_item_cond
     set process_id = null
   where (i_pid is null or I_pid = process_id);
  ---
  DBG(L_program, 'POS: CLEAN CONDs. PID=' || to_char(I_pid) || ', #ROWS=' || to_char(SQL%ROWCOUNT));
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END PROCESS_CLEANUP;
--------------------------------------------------------------------------------
FUNCTION PROCESS_EXEC(O_error_message IN OUT  VARCHAR2,
                      I_pid           IN      number,
                      I_loop          IN      BOOLEAN,
                      I_single_item   IN      BOOLEAN)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.PROCESS_EXEC';
  ---
  L_error_message   VARCHAR2(500);
  L_result          xxadeo_lfcy_item_cond.val_status%TYPE;
  L_item            xxadeo_lfcy_item_cond.item%TYPE;
  L_skip_item       boolean;
  L_foo             number(1,0);
  L_single_item     varchar2(1) := 'N';
  ---
  cursor C_rules(IC_single_item varchar2) is
    select lc_s.lfcy_status
          , lc_s.lfcy_status_seq
          , rh.rule_id
          , rh.call_seq_no
          , rh.active_ind
          , rh.package_name
          , rh.function_name
          , rdp.value_1           p1
          , rdp.value_2           p2
          , lc_s.lfcy_entity_type
    from xxadeo_lfcy_cfg_status lc_s
         ,xxadeo_system_rules rh
         ,xxadeo_system_rules_detail rd1
         ,xxadeo_system_rules_detail rdp
         ,xxadeo_system_rules_detail rdsi
    where 1=1
      and rh.active_ind = 'Y'
      and rh.rule_id = rd1.rule_id
      and rd1.parameter = 'LFCY_STATUS'
      and rd1.value_1 = lc_s.lfcy_status
      and rh.rule_id = rdp.rule_id (+)
      and rdp.parameter (+)= 'PARAMETER'
      and rh.rule_id = rdsi.rule_id
      and rdsi.parameter = 'SINGLE_ITEM_EXEC'
      and (rdsi.value_1 = 'Y' or IC_single_item = 'N')
    order by lc_s.lfcy_status_seq, rh.call_seq_no
    ;
  ---
  L_loop number;
  L_total_changes number := 0;
  L_rule_changes  number := 0;
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'PROCESS_EXEC');
  ---
  DBG(L_program, 'Start');
  ---
  if initialize_globals = FALSE then
    O_error_message := GV_init_error_msg;
    return FALSE;
  end if;
  ---
  L_loop := 0;
  ---
  -- Reset process control indicators
  -- rule validation procs should filter by them
  -- Rules with next_val_dt in future are not considered (from previous preparation)
  ---
  /*
  update xxadeo_lfcy_item_cond
     set changed_ind = 'N'
         ,process_ind = 'Y'
         ,val_status = null
         ,error_message = null
   where process_id = I_pid
     and nvl(val_status,'N') <> 'Y';
  ---
  DBG(L_program, 'UPDATE ITEM COND: #ROWS=' || to_char(SQL%ROWCOUNT));
  */
  ---
  if I_single_item = TRUE then
    L_single_item := 'Y';
  else
    L_single_item := 'N';
  end if;
  ---
  loop
    ---
    dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'PROCESS_EXEC');
    dbms_application_info.set_client_info('OUTER LOOP');
    ---
    DBG(L_program, 'OUTER LOOP start');
    ---
    --DBMS_MVIEW.REFRESH('XXADEO_MV_LFCY_STATUS_NEXT', method => '?', atomic_refresh => FALSE, out_of_place => TRUE);
    --DBG(L_program, 'POS: Refresh XXADEO_MV_LFCY_STATUS_NEXT');
    ---
    L_total_changes := 0;
    ---
    for rec in C_rules(L_single_item) loop
      ---
      dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'PROCESS_EXEC');
      dbms_application_info.set_client_info('INNER LOOP: ' || rec.rule_id);
      ---
      DBG(L_program, 'INNER LOOP => PID: ' || I_pid || ', RULE=' || rec.rule_id);
      ---
      L_rule_changes := 0;
      ---
      O_error_message := null;
      L_result := null;
      L_rule_changes := 0;
      ---
      if not execute_cond(O_error_message,
                      L_rule_changes,
                      L_result,
                      rec.package_name,
                      rec.function_name,
                      I_pid,
                      rec.p1,
                      rec.p2) then
        ---
        -- if error on bulk execution mark all affected
        -- rules, with status not valid or null, with status E
        ---
        DBG(L_program, 'EXECUTE_COND returned FALSE: L_result=' || L_result || ', error_message=' || O_error_message);
        L_result := 'E';
        ---
      end if;
      ---
      dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'PROCESS_EXEC');
      dbms_application_info.set_client_info('EXECUTE_COND DONE');
      ---
      if L_result is null then
        L_result := 'N';
      end if;
      ---
      if L_rule_changes > 0 then
        DBG(L_program, '#Changes=' || to_char(L_rule_changes));
        L_total_changes := L_total_changes + L_rule_changes;
      end if;
      ---
      if L_result in ('E','N') then
        ---
        dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'PROCESS_EXEC');
        dbms_application_info.set_client_info('MERGE VAL_STATUS E|N');
        ---
        merge /*+ MONITOR */ into xxadeo_lfcy_item_cond t0
          using (
            select  vw.item
                    ,vw.lfcy_entity
                    ,vw.lfcy_cond_id
                    ,vw.lfcy_entity_type
              from xxadeo_v_lfcy_work  vw
            where vw.process_id = I_pid
              and vw.lfcy_cond_id = rec.rule_id
              and vw.lfcy_entity_type = rec.lfcy_entity_type
              --and vw.lfcy_status_next = rec.lfcy_status
              and nvl(vw.val_status,'X') <> 'Y'
              and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
            ) src
          on (
            t0.item = src.item
            and t0.lfcy_entity = src.lfcy_entity
            and t0.lfcy_entity_type = src.lfcy_entity_type
            and t0.lfcy_cond_id = src.lfcy_cond_id)
          when matched then
            update
              set val_status = L_result
                  ,last_val_dt = sysdate
                  ,last_update_datetime = sysdate
                  ,error_message = O_error_message
        ;
        ---
        DBG(L_program, 'MERGE OF NOT CHANGED OR ERR CONDs: #ROWS=' || to_char(SQL%ROWCOUNT));
        ---
      end if;
      ---
    end loop;
    ---
    if I_loop = FALSE then
      exit;
    end if;
    ---
    if L_total_changes > 0 then
      DBG(L_program, 'There are changes.');
    else
      DBG(L_program, 'NO changes. Exiting outer loop.');
      exit;
    end if;
    ---
    L_loop := L_loop + 1;
    ---
    if L_loop > 10 then
      O_error_message := 'Looping too much...';
      return FALSE;
    end if;
    ---
    -- there are changes => prepare changed items/conds for reprocess
    ---
    DBG(L_program, 'PRE: POST_PROCESS');
    ---
    if post_process(O_error_message,
                    I_pid) = FALSE then
      return FALSE;
    end if;
    ---
    DBG(L_program, 'POS: POST_PROCESS');
    ---
  end loop;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    dbms_application_info.set_module(CONST$PackageName, 'PROCESS_EXEC');
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END PROCESS_EXEC;
--------------------------------------------------------------------------------
-- Private functions implementation
--------------------------------------------------------------------------------
FUNCTION INITIALIZE_WORK_IS(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(200) := CONST$PackageName || '.INITIALIZE_WORK_IS';
  ---
  L_sql       VARCHAR2(10000);
  ---
BEGIN
  ---
  DBG(L_program, 'Start');
  ---
  L_sql := '
    merge /*+ MONITOR */ into xxadeo_lfcy_work t0
      using (
        select isup.item
              , isup.supplier lfcy_entity
              , ''IS'' lfcy_entity_type
              , im.dept
              , im.class
              , im.pack_ind
              , im.orderable_ind
              , im.inventory_ind
              , im.sellable_ind
              , im.forecast_ind
              , uil_typ.uda_value       item_type
              , uil_subtyp.uda_value    item_subtype
              , iscfa.' || GV_CFACOL_LnkStaItSup || ' item_entity_status
              , lc_cfg_s.lfcy_status
              , lc_cfg_s.lfcy_status_next
              , decode(nvl(lc_cfg_s.lfcy_status,''X''),''X'',lc_cfg_s_new.lfcy_status,null) lfcy_status_new
              , sysdate
        from item_supplier            isup
             , item_master            im
             , uda_item_lov           uil_typ
             , xxadeo_mom_dvm         xmd_cfa
             , cfa_attrib             cfaa
             , item_supplier_cfa_ext  iscfa
             , xxadeo_mom_dvm         xmd_typ
             , uda_item_lov           uil_subtyp
             , xxadeo_mom_dvm         xmd_subtyp
             , XXADEO_LFCY_CFG_STATUS lc_cfg_s
             , XXADEO_LFCY_CFG_STATUS lc_cfg_s_new
        where 1=1
          and isup.item             = im.item
          ---
          and im.item_level         <= im.tran_level
          and im.status             = ''A''
          ---
          and xmd_typ.parameter     = ''ITEM_TYPE''
          and xmd_typ.func_area     = ''UDA''
          and nvl(xmd_typ.bu,-1)    = -1
          and uil_typ.item          (+) = im.item
          and uil_typ.uda_id        (+) = xmd_typ.value_1
          and xmd_subtyp.parameter  = ''ITEM_SUBTYPE''
          and xmd_subtyp.func_area  = ''UDA''
          and nvl(xmd_subtyp.bu,-1) = -1
          and uil_subtyp.item       (+)= im.item
          and uil_subtyp.uda_id     (+)= xmd_subtyp.value_1
          and xmd_cfa.parameter     = ''' || CONST$CFAKEY_LnkStaItSup || '''
          and nvl(xmd_cfa.bu,-1)    = -1
          and cfaa.attrib_id        = xmd_cfa.value_1
          and iscfa.group_id        (+)= cfaa.group_id
          and iscfa.item                          (+)= isup.item
          and iscfa.supplier                      (+)= isup.supplier
          and lc_cfg_s.lfcy_status_udacfa_value   (+)= iscfa.' || GV_CFACOL_LnkStaItSup || '
          and lc_cfg_s.lfcy_entity_type           (+)= ''IS''
          and lc_cfg_s_new.lfcy_init_ind          = ''Y''
          and lc_cfg_s_new.lfcy_entity_type       = ''IS''
        ) src
      on (
        t0.item = src.item
        and t0.lfcy_entity = src.lfcy_entity
        and t0.lfcy_entity_type = ''IS''
      )
      when matched then
        update
          set item_dept = src.dept
              ,item_class = src.class
              ,pack_ind = src.pack_ind
              ,orderable_ind = src.orderable_ind
              ,inventory_ind = src.inventory_ind
              ,sellable_ind = src.sellable_ind
              ,forecast_ind = src.forecast_ind
              ,item_type = src.item_type
              ,item_subtype = src.item_subtype
              ,item_entity_status = src.item_entity_status
              ,lfcy_status = src.lfcy_status
              ,lfcy_status_new = src.lfcy_status_new
              ,last_update_datetime = sysdate
      when not matched then
        insert ( item,
                 lfcy_entity,
                 lfcy_entity_type,
                 item_dept,
                 item_class,
                 pack_ind,
                 orderable_ind,
                 inventory_ind,
                 sellable_ind,
                 forecast_ind,
                 item_type,
                 item_subtype,
                 item_entity_status,
                 lfcy_status,
                 lfcy_status_new,
                 last_update_datetime)
        values (
            src.item,
            src.lfcy_entity,
            ''IS'',
            src.dept,
            src.class,
            src.pack_ind,
            src.orderable_ind,
            src.inventory_ind,
            src.sellable_ind,
            src.forecast_ind,
            src.item_type,
            src.item_subtype,
            src.item_entity_status,
            src.lfcy_status,
            src.lfcy_status_new,
            sysdate)
  ';
  ---
  execute immediate L_sql;
  ---
  DBG(L_program, 'POS: New item/suppliers, ROWS=' || to_char(SQL%ROWCOUNT));
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
       ---
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));

       ---
       return FALSE;
END;
--------------------------------------------------------------------------------
FUNCTION INITIALIZE_WORK_IB(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.INITIALIZE_WORK_IB';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName, 'INITIALIZE_WORK_IB');
  ---
  merge /*+ MONITOR */ into xxadeo_lfcy_work t0
    using (
    select im.item
          , uil_bus.uda_value lfcy_entity
          , 'IB' lfcy_entity_type
          , im.dept
          , im.class
          , im.pack_ind
          , im.orderable_ind
          , im.inventory_ind
          , im.sellable_ind
          , im.forecast_ind
          , uil_typ.uda_value       item_type
          , uil_subtyp.uda_value    item_subtype
          , uil_sta.uda_value       item_entity_status
          , lc_cfg_s.lfcy_status
          , decode(nvl(lc_cfg_s.lfcy_status,'X'),'X',lc_cfg_s_new.lfcy_status,null) lfcy_status_new
          , sysdate
    from item_master im
         , uda_item_lov uil_bus
         , xxadeo_mom_dvm xmd_bus
         , uda_item_lov uil_typ
         , xxadeo_mom_dvm xmd_typ
         , uda_item_lov uil_subtyp
         , xxadeo_mom_dvm xmd_subtyp
         , uda_item_lov uil_sta
         , xxadeo_mom_dvm xmd_sta
         , XXADEO_LFCY_CFG_STATUS lc_cfg_s
         , XXADEO_LFCY_CFG_STATUS lc_cfg_s_new
    where 1=1
      ---
      and im.item_level         <= im.tran_level
      and im.status             = 'A'
      ---
      and xmd_bus.parameter     = 'BU_SUBSCRIPTION'
      and xmd_bus.func_area     = 'UDA'
      and nvl(xmd_bus.bu,-1)    = -1
      and uil_bus.item          = im.item
      and uil_bus.uda_id        = xmd_bus.value_1
      ---
      and xmd_typ.parameter     = 'ITEM_TYPE'
      and xmd_typ.func_area     = 'UDA'
      and nvl(xmd_typ.bu,-1)    = -1
      and uil_typ.item          (+) = im.item
      and uil_typ.uda_id        (+) = xmd_typ.value_1
      ---
      and xmd_subtyp.parameter  = 'ITEM_SUBTYPE'
      and xmd_subtyp.func_area  = 'UDA'
      and nvl(xmd_subtyp.bu,-1) = -1
      and uil_subtyp.item       (+)= im.item
      and uil_subtyp.uda_id     (+)= xmd_subtyp.value_1
      ---
      and xmd_sta.parameter     = 'LIFECYCLE_STATUS'
      and xmd_sta.func_area     = 'UDA'
      and xmd_sta.bu            = uil_bus.uda_value
      --- Must have already a LFCY UDA
      and uil_sta.item          = im.item
      and uil_sta.uda_id        = xmd_sta.value_1
      ---
      and lc_cfg_s.lfcy_status_udacfa_value (+)= uil_sta.uda_value
      and lc_cfg_s.lfcy_entity_type         (+)= 'IB'
      ---
      and lc_cfg_s_new.lfcy_init_ind        = 'Y'
      and lc_cfg_s_new.lfcy_entity_type     = 'IB'
    ) src
  on (
    t0.item = src.item
    and t0.lfcy_entity = src.lfcy_entity
    and t0.lfcy_entity_type = 'IB'
  )
  when matched then
    update
      set item_dept = src.dept
          ,item_class = src.class
          ,pack_ind = src.pack_ind
          ,orderable_ind = src.orderable_ind
          ,inventory_ind = src.inventory_ind
          ,sellable_ind = src.sellable_ind
          ,forecast_ind = src.forecast_ind
          ,item_type = src.item_type
          ,item_subtype = src.item_subtype
          ,item_entity_status = src.item_entity_status
          ,lfcy_status = src.lfcy_status
          ,lfcy_status_new = src.lfcy_status_new
  when not matched then
    insert ( item,
             lfcy_entity,
             lfcy_entity_type,
             item_dept,
             item_class,
             pack_ind,
             orderable_ind,
             inventory_ind,
             sellable_ind,
             forecast_ind,
             item_type,
             item_subtype,
             item_entity_status,
             lfcy_status,
             lfcy_status_new,
             last_update_datetime)
    values (
        src.item,
        src.lfcy_entity,
        'IB',
        src.dept,
        src.class,
        src.pack_ind,
        src.orderable_ind,
        src.inventory_ind,
        src.sellable_ind,
        src.forecast_ind,
        src.item_type,
        src.item_subtype,
        src.item_entity_status,
        src.lfcy_status,
        src.lfcy_status_new,
        sysdate)
  ;
  ---
  DBG(L_program, 'POS: Merge Work IB, ROWS=' || to_char(SQL%ROWCOUNT));
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
       ---
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
       ---
       return FALSE;
END;
--------------------------------------------------------------------------------
FUNCTION update_item_status(O_error_message IN OUT VARCHAR2,
                            I_pid           IN      number default null)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.UPDATE_ITEM_STATUS';
  ---
  L_foo       varchar2(100);
  L_foo_1     varchar2(100);
  L_foo_2     varchar2(100);
  L_foo_3     varchar2(100);
  ---
  cursor C_cfg_status is
    select lfcy_status
      from xxadeo_lfcy_cfg_status
     order by lfcy_status_seq desc;
  ---
BEGIN
  ---
  DBG(L_program, 'Start: PID=' || I_pid);
  /*
  ---
  -- reset is link status to init if needed
  ---
  DBG(L_program, 'Reset SUPP links to INIT');
  ---
  if not RESET_STOP_TO_INIT_IS(O_error_message) then
    return false;
  end if;
  */
  ---
  -- udpdate work table lfcy_status_new for items with all conds valid
  -- status new MUST be reset at start of processing/pre-processing
  -- loop status cfg, ordered by seq desc, in order to set item status
  -- to the one with highest seq if two or more status can be set
  -- i.e. have all rules valid
  ---
  for rs in c_cfg_status loop
    merge /*+ MONITOR */ into xxadeo_lfcy_work t0
      using (
         select sn.item,
                sn.lfcy_entity_type,
                sn.lfcy_entity,
                sn.lfcy_status
           from xxadeo_v_lfcy_status_next sn
          where (I_pid is null or sn.process_id = I_pid)
            and lfcy_status = rs.lfcy_status
            and yes = nrules
            and no = 0
        ) src
      on (
        t0.item                 = src.item
        and t0.lfcy_entity_type = src.lfcy_entity_type
        and t0.lfcy_entity      = src.lfcy_entity
      )
      when matched then
        update
           set lfcy_status            = null,
               --lfcy_status_next       = null,
               lfcy_status_new        = src.lfcy_status,
               last_update_datetime   = sysdate
         where lfcy_status_new is null
      ;
    ---
    DBG(L_program, 'POS: STATUS=' || rs.lfcy_status || ', ROWS=' || to_char(SQL%ROWCOUNT));
    ---
  end loop;
  ---
  -- Fill IB-INIT, IB-ASI and IB-DEL dates
  ---
  DBG(L_program, 'PRE: FILL_IB_DATES');
  ---
  if fill_ib_dates(O_error_message,
                   I_pid) = FALSE then
    return false;
  end if;
  ---
  -- propagate disc status to item_loc
  ---
  DBG(L_program, 'PRE: DISC_IB_LOC');
  ---
  if disc_ib_loc(O_error_message,
                 I_pid) = FALSE then
    return false;
  end if;
  ---
  -- update item/bu uda value for status
  ---
  DBG(L_program, 'PRE: MERGE_UDAS_IB');
  ---
  if merge_udas_ib(O_error_message
                    ,I_pid) = FALSE then
    return FALSE;
  end if;
  ---
  -- update item/supplier cfa value for status
  ---
  DBG(L_program, 'PRE: MERGE_CFAS_IS');
  ---
  if merge_cfas_is(O_error_message
                    ,I_pid) = FALSE then
    return FALSE;
  end if;
  ---
  -- prepare work table for changed items
  --
  DBG(L_program, 'PRE: Prepare work table for changed items');
  ---
  merge /*+ MONITOR */ into xxadeo_lfcy_work t0
    using (
           select w.item,
                  w.lfcy_entity_type,
                  w.lfcy_entity,
                  lc_cfg_s.lfcy_status_udacfa_value,
                  lc_cfg_s.lfcy_status,
                  lc_cfg_s.lfcy_status_next
               from xxadeo_lfcy_work        w
                  , xxadeo_lfcy_cfg_status  lc_cfg_s
            where (I_pid is null or w.process_id = I_pid)
              and w.lfcy_status_new is not null
              and w.lfcy_status is null
              and lc_cfg_s.lfcy_status = w.lfcy_status_new
          ) src
    on (t0.item                 = src.item
        and t0.lfcy_entity_type = src.lfcy_entity_type
        and t0.lfcy_entity      = src.lfcy_entity
        )
    when matched then
      update
         set t0.lfcy_status            = src.lfcy_status,
             t0.lfcy_status_new        = null,
             t0.last_update_datetime   = sysdate,
             t0.item_entity_status     = src.lfcy_status_udacfa_value
    ;
  ---
  DBG(L_program, 'POS: Prepare work table for changed items, ROWS=' || to_char(SQL%ROWCOUNT));
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    ---
    return FALSE;
END;
--------------------------------------------------------------------------------
FUNCTION merge_udas_ib( O_error_message IN OUT  VARCHAR2,
                        I_pid           IN      number)
RETURN BOOLEAN IS
    L_program   VARCHAR2(100) := CONST$PackageName || '.MERGE_UDAS_IB';
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'MERGE_UDAS_IB');
  ---
  dbms_application_info.set_client_info('MERGE UDA_ITEM_LOV');
  ---
  merge /*+ MONITOR */ into uda_item_lov t0
    using (select w.item,
                  w.lfcy_entity,
                  lc_cfg_s.lfcy_status_udacfa_value uda_value,
                  xmd.value_1                       uda_id
             from xxadeo_lfcy_work          w
                  , xxadeo_mom_dvm          xmd
                  , xxadeo_lfcy_cfg_status  lc_cfg_s
            where (I_pid is null or w.process_id = I_pid)
              and xmd.func_area = 'UDA'
              and xmd.parameter = 'LIFECYCLE_STATUS'
              and xmd.bu = w.lfcy_entity
              and w.lfcy_status is null
              and lc_cfg_s.lfcy_status = w.lfcy_status_new
              and lc_cfg_s.lfcy_entity_type = 'IB'
          ) src
    on (t0.item         = src.item
        and t0.uda_id   = src.uda_id)
    when matched then
      update
         set uda_value            = src.uda_value,
             last_update_id       = GV_user_id,
             last_update_datetime = sysdate
    when not matched then
      insert
        (item,
         uda_id,
         uda_value,
         create_datetime,
         create_id,
         last_update_datetime,
         last_update_id
         )
      values
        (src.item,
         src.uda_id,
         src.uda_value,
         sysdate,
         GV_user_id,
         sysdate,
         GV_user_id);
  ---
  ---
  -- Update status of inserted/updated items for conditions
  ---
  merge into xxadeo_lfcy_work t0
    using (select w.item,
                  w.lfcy_entity,
                  w.lfcy_status_new,
                  lc_cfg_s.lfcy_status_next,
                  lc_cfg_s.lfcy_status_udacfa_value
             from xxadeo_lfcy_work          w
                  , xxadeo_lfcy_cfg_status  lc_cfg_s
            where (I_pid is null or w.process_id = I_pid)
              and w.lfcy_status is null
              and w.lfcy_entity_type = 'IB'
              --and w.lfcy_status_new = 'IB-INIT'
              and lc_cfg_s.lfcy_status = w.lfcy_status_new
          ) src
    on (
      t0.item             = src.item
      and t0.lfcy_entity  = src.lfcy_entity)
    when matched then
      update
         set t0.lfcy_status           = src.lfcy_status_new,
             t0.lfcy_status_new       = null,
             t0.item_entity_status    = src.lfcy_status_udacfa_value,
             t0.last_update_datetime  = sysdate;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END merge_udas_ib;
--------------------------------------------------------------------------------
FUNCTION merge_cfas_is(O_error_message  IN OUT  VARCHAR2,
                       I_pid            IN      NUMBER)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.MERGE_CFAS_IS';
  ---
  L_sql_merge varchar2(2000) := '
    merge into item_supplier_cfa_ext t0
      using (select item,
                    lfcy_entity,
                    cfaa.group_id,
                    lc_cfg_s.lfcy_status_udacfa_value cfa_value
               from xxadeo_lfcy_work          w
                    ,xxadeo_mom_dvm           xmd
                    ,xxadeo_lfcy_cfg_status   lc_cfg_s
                    ,cfa_attrib               cfaa
              where (:1 is null or w.process_id = :2)
                and w.lfcy_status is null
                and w.lfcy_entity_type = ''IS''
                and xmd.parameter = ''' || CONST$CFAKEY_LnkStaItSup || '''
                and xmd.func_area = ''CFA''
                and nvl(xmd.bu,-1) = -1
                and lc_cfg_s.lfcy_status = w.lfcy_status_new
                and lc_cfg_s.lfcy_entity_type = ''IS''
                and cfaa.attrib_id = xmd.value_1
            ) src
      on (t0.item           = src.item
          and t0.supplier   = src.lfcy_entity
          and t0.group_id   = src.group_id)
      when matched then
        update
           set t0.' || GV_CFACOL_LnkStaItSup || ' = src.cfa_value,
               t0.varchar2_10         = ''' || GV_user_id || '''
      when not matched then
        insert
          (item,
           supplier,
           group_id,
           ' || GV_CFACOL_LnkStaItSup || ',
           varchar2_10)
        values
          (src.item,
           src.lfcy_entity,
           src.group_id,
           '''|| GV_CFAVAL_LnkStaItSup$Ini ||''',
           ''' || GV_user_id || ''')';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'MERGE_CFAS_IS');
  ---
  dbms_application_info.set_client_info('MERGE ITEM_SUPPLIER_CFA_EXT');
  ---
  execute immediate L_sql_merge using I_pid, I_pid;
  ---
  -- Update status of inserted/updated items for conditions
  ---
  dbms_application_info.set_client_info('MERGE WORK');
  ---
  merge into xxadeo_lfcy_work t0
    using (select w.item,
                  w.lfcy_entity,
                  w.lfcy_entity_type,
                  w.lfcy_status_new,
                  lc_cfg_s.lfcy_status_next,
                  lc_cfg_s.lfcy_status_udacfa_value
             from xxadeo_lfcy_work w
                  , xxadeo_lfcy_cfg_status lc_cfg_s
            where (I_pid is null or w.process_id = I_pid)
              and w.lfcy_status is null
              and w.lfcy_entity_type = 'IS'
              and lc_cfg_s.lfcy_status = w.lfcy_status_new
          ) src
    on (t0.item                 = src.item
        and t0.lfcy_entity      = src.lfcy_entity
        and t0.lfcy_entity_type = src.lfcy_entity_type)
    when matched then
      update
         set t0.lfcy_status           = src.lfcy_status_new,
             t0.lfcy_status_new       = null,
             t0.item_entity_status    = src.lfcy_status_udacfa_value,
             t0.last_update_datetime  = sysdate;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    ---
    return FALSE;
END merge_cfas_is;
--------------------------------------------------------------------------------
FUNCTION RESET_STOP_TO_INIT_IS(O_error_message  IN OUT  VARCHAR2)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.RESET_STOP_TO_INIT_IS';
  ---
  L_sql_merge varchar2(2000) := '
    merge into item_supplier_cfa_ext t0
      using (
        select w.item,
              w.lfcy_entity,
              cfaa.group_id,
              ''' || GV_CFAVAL_LnkStaItSup$Act || ''' cfa_value
         from xxadeo_lfcy_work          w
              ,xxadeo_mom_dvm           xmd
              ,xxadeo_lfcy_cfg_status   lc_cfg_s
              ,cfa_attrib               cfaa
              ,xxadeo_mom_dvm           xmd_ltdt
              ,cfa_attrib               cfaa_ltdt
              ,item_supplier_cfa_ext    iscfa
        where 1=1
          and w.lfcy_status = ''IS-STOP''
          and w.lfcy_entity_type = ''IS''
          and xmd.parameter = ''' || CONST$CFAKEY_LnkStaItSup || '''
          and xmd.func_area = ''CFA''
          and nvl(xmd.bu,-1) = -1
          and lc_cfg_s.lfcy_status = w.lfcy_status
          and lc_cfg_s.lfcy_entity_type = ''IS''
          and cfaa.attrib_id = xmd.value_1
          ---
          and xmd_ltdt.parameter = ''' || CONST$CFAKEY_LnkTermDtItemSupp || '''
          and nvl(xmd_ltdt.bu,-1) = -1
          and cfaa_ltdt.attrib_id = xmd_ltdt.value_1
          and iscfa.item = w.item
          and iscfa.supplier = w.lfcy_entity
          and iscfa.group_id = cfaa_ltdt.group_id
          and nvl(iscfa.' || GV_CFACOL_LnkTermDtItemSupp|| ',get_vdate+2) > get_vdate+1
            ) src
      on (t0.item           = src.item
          and t0.supplier   = src.lfcy_entity
          and t0.group_id   = src.group_id)
      when matched then
        update
           set t0.' || GV_CFACOL_LnkStaItSup || ' = src.cfa_value'
    ;
  ---
BEGIN
  ---
  execute immediate L_sql_merge;
  ---
  -- Update status of inserted/updated items for conditions
  ---
  merge into xxadeo_lfcy_work t0
    using (select w.item,
                  w.lfcy_entity,
                  w.lfcy_entity_type,
                  w.lfcy_status_new,
                  lc_cfg_s.lfcy_status_next,
                  lc_cfg_s.lfcy_status_udacfa_value
             from xxadeo_lfcy_work w
                  , xxadeo_lfcy_cfg_status lc_cfg_s
            where 1=1
              and w.lfcy_status is null
              and w.lfcy_entity_type = 'IS'
              and lc_cfg_s.lfcy_status = w.lfcy_status_new
          ) src
    on (t0.item                 = src.item
        and t0.lfcy_entity      = src.lfcy_entity
        and t0.lfcy_entity_type = src.lfcy_entity_type)
    when matched then
      update
         set t0.lfcy_status           = src.lfcy_status_new,
             t0.lfcy_status_new       = null,
             t0.item_entity_status    = src.lfcy_status_udacfa_value,
             t0.last_update_datetime  = sysdate;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    ---
    return FALSE;
END RESET_STOP_TO_INIT_IS;
--------------------------------------------------------------------------------
FUNCTION fill_ib_dates(O_error_message  IN OUT  VARCHAR2,
                       I_pid            IN      NUMBER)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.FILL_IB_DATES';
  ---
  L_sql_init varchar2(2000) := '
    merge into item_master_cfa_ext t0
      using (select item
                    ,lfcy_entity
                    ,cfaa.group_id
               from xxadeo_lfcy_work          w
                    ,xxadeo_mom_dvm           xmd
                    ,cfa_attrib               cfaa
              where (:1 is null or w.process_id = :2)
                and w.lfcy_status is null
                and w.lfcy_entity_type = ''IB''
                and w.lfcy_status_new = ''IB-INIT''
                and xmd.parameter = ''' || CONST$CFAKEY_AbonStartDate || '''
                and xmd.func_area = ''CFA''
                and xmd.bu = w.lfcy_entity
                and cfaa.attrib_id = xmd.value_1
            ) src
      on (t0.item           = src.item
          and t0.group_id   = src.group_id)
      when matched then
        update
           set t0.' || GV_CFACOL_AbonStartDate || ' = get_vdate + 1
      when not matched then
        insert
          (item,
           group_id,
           ' || GV_CFACOL_AbonStartDate || ')
        values
          (src.item,
           src.group_id,
           get_vdate + 1)';
  ---
  L_sql_asi varchar2(2000) := '
    merge into item_master_cfa_ext t0
      using (select item
                    ,lfcy_entity
                    ,cfaa.group_id
               from xxadeo_lfcy_work          w
                    ,xxadeo_mom_dvm           xmd
                    ,cfa_attrib               cfaa
              where (:1 is null or w.process_id = :2)
                and w.lfcy_status is null
                and w.lfcy_entity_type = ''IB''
                and w.lfcy_status_new = ''IB-ASI''
                and xmd.parameter = ''' || CONST$CFAKEY_ActifStatusDate || '''
                and xmd.func_area = ''CFA''
                and xmd.bu = w.lfcy_entity
                and cfaa.attrib_id = xmd.value_1
            ) src
      on (t0.item           = src.item
          and t0.group_id   = src.group_id)
      when matched then
        update
           set t0.' || GV_CFACOL_ActifStatusDate || ' = get_vdate + 1
      when not matched then
        insert
          (item,
           group_id,
           ' || GV_CFACOL_ActifStatusDate || ')
        values
          (src.item,
           src.group_id,
           get_vdate + 1)';
  ---
  L_sql_del varchar2(2000) := '
    merge into item_master_cfa_ext t0
      using (select item
                    ,lfcy_entity
                    ,cfaa.group_id
               from xxadeo_lfcy_work          w
                    ,xxadeo_mom_dvm           xmd
                    ,cfa_attrib               cfaa
              where (:1 is null or w.process_id = :2)
                and w.lfcy_status is null
                and w.lfcy_entity_type = ''IB''
                and w.lfcy_status_new = ''IB-DEL''
                and xmd.parameter = ''' || CONST$CFAKEY_AbonEndDate || '''
                and xmd.func_area = ''CFA''
                and xmd.bu = w.lfcy_entity
                and cfaa.attrib_id = xmd.value_1
            ) src
      on (t0.item           = src.item
          and t0.group_id   = src.group_id)
      when matched then
        update
           set t0.' || GV_CFACOL_AbonEndDate || ' = get_vdate + 1
      when not matched then
        insert
          (item,
           group_id,
           ' || GV_CFACOL_AbonEndDate || ')
        values
          (src.item,
           src.group_id,
           get_vdate + 1)';
  ---
BEGIN
  --- Filled by STEP!?
  -- execute immediate L_sql_init using I_pid, I_pid;
  ---
  execute immediate L_sql_asi using I_pid, I_pid;
  --- Filled by STEP!?
  -- execute immediate L_sql_del using I_pid, I_pid;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    ---
    return FALSE;
END;
--------------------------------------------------------------------------------
FUNCTION disc_ib_loc(O_error_message  IN OUT  VARCHAR2,
                     I_pid            IN      NUMBER)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.DISC_IB_LOC';
  ---
  cursor C_disc is
    select item
           ,lfcy_entity bu
      from xxadeo_lfcy_work          w
     where (I_pid is null or w.process_id = I_pid)
       and w.lfcy_status is null
       and w.lfcy_entity_type = 'IB'
       and w.lfcy_status_new = 'IB-DISC';
  ---
  cursor C_loc(I_item IN item_loc.item%TYPE,
               I_bu   IN xxadeo_bu_ou.bu%TYPE) is
    select  il.item,
            im.status item_status,
            im.item_level,
            im.tran_level,
            il.loc,
            il.loc_type,
            il.primary_supp,
            il.primary_cntry,
            il.status,
            il.local_item_desc,
            il.local_short_desc,
            il.primary_variant,
            il.unit_retail,
            il.ti,
            il.hi,
            il.store_ord_mult,
            il.daily_waste_pct,
            il.taxable_ind,
            il.meas_of_each,
            il.meas_of_price,
            il.uom_of_price,
            il.selling_unit_retail,
            il.selling_uom,
            il.primary_cost_pack,
            'Y' process_children,
            il.receive_as_type,
            il.ranged_ind,
            il.inbound_handling_days,
            il.store_price_ind,
            il.source_method,
            il.source_wh,
            il.multi_units,
            il.multi_unit_retail,
            il.multi_selling_uom,
            il.uin_type,
            il.uin_label,
            il.capture_time,
            il.ext_uin_ind,
            il.costing_loc,
            il.costing_loc_type,
            vlo.org_unit_id
    from item_loc il
         ,item_master im
         ,v_loc_ou vlo
         ,xxadeo_bu_ou xbo
   where im.item = I_item
     and il.item = im.item
     and vlo.location = il.loc
     and xbo.bu = I_bu
     and xbo.ou = vlo.org_unit_id
     and il.status in ('A')
  ;
  ---
  L_row_count number := 0;
  ---
BEGIN
  ---
  for rec in C_disc loop
    ---
    L_row_count := L_row_count + 1;
    ---
    for ilrec in C_loc(rec.item,rec.bu) loop
      if item_loc_sql.status_change_valid(O_error_message,
                                          ilrec.item,
                                          ilrec.loc,
                                          ilrec.loc_type,
                                          ilrec.status,
                                          'C') = FALSE then
        -- TODO: UPDATE _work status to ERR????
        continue;
      end if;
      ---
      ilrec.status := 'C';
      ---
      if item_loc_sql.update_item_loc(O_error_message,
                                      ilrec.item,
                                      ilrec.item_status, --            IN     ITEM_MASTER.STATUS%TYPE,
                                      ilrec.item_level, --             IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                                      ilrec.tran_level, --             IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                                      ilrec.loc, --                    IN     ITEM_LOC.LOC%TYPE,
                                      ilrec.loc_type, --               IN     ITEM_LOC.LOC_TYPE%TYPE,
                                      ilrec.primary_supp, --           IN     ITEM_LOC.PRIMARY_SUPP%TYPE,
                                      ilrec.primary_cntry, --          IN     ITEM_LOC.PRIMARY_CNTRY%TYPE,
                                      ilrec.status, --                 IN     ITEM_LOC.STATUS%TYPE,
                                      ilrec.local_item_desc, --        IN     ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
                                      ilrec.local_short_desc, --       IN     ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
                                      ilrec.primary_variant, --        IN     ITEM_LOC.PRIMARY_VARIANT%TYPE,
                                      ilrec.unit_retail, --            IN     ITEM_LOC.UNIT_RETAIL%TYPE,
                                      ilrec.ti, --                     IN     ITEM_LOC.TI%TYPE,
                                      ilrec.hi, --                     IN     ITEM_LOC.HI%TYPE,
                                      ilrec.store_ord_mult, --         IN     ITEM_LOC.STORE_ORD_MULT%TYPE,
                                      ilrec.daily_waste_pct, --        IN     ITEM_LOC.DAILY_WASTE_PCT%TYPE,
                                      ilrec.taxable_ind, --            IN     ITEM_LOC.TAXABLE_IND%TYPE,
                                      ilrec.meas_of_each, --           IN     ITEM_LOC.MEAS_OF_EACH%TYPE,
                                      ilrec.meas_of_price, --          IN     ITEM_LOC.MEAS_OF_PRICE%TYPE,
                                      ilrec.uom_of_price, --           IN     ITEM_LOC.UOM_OF_PRICE%TYPE,
                                      ilrec.selling_unit_retail, --    IN     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                      ilrec.selling_uom, --            IN     ITEM_LOC.SELLING_UOM%TYPE,
                                      ilrec.primary_cost_pack, --      IN     ITEM_LOC.PRIMARY_COST_PACK%TYPE,
                                      ilrec.process_children, --       IN     VARCHAR2,
                                      ilrec.receive_as_type, --        IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                                      ilrec.ranged_ind, --             IN     ITEM_LOC.RANGED_IND%TYPE,
                                      ilrec.inbound_handling_days, --  IN     ITEM_LOC.INBOUND_HANDLING_DAYS%TYPE,
                                      ilrec.store_price_ind, --        IN     ITEM_LOC.STORE_PRICE_IND%TYPE,
                                      ilrec.source_method, --          IN     ITEM_LOC.SOURCE_METHOD%TYPE,
                                      ilrec.source_wh, --              IN     ITEM_LOC.SOURCE_WH%TYPE,
                                      ilrec.multi_units, --            IN     ITEM_LOC.MULTI_UNITS%TYPE,
                                      ilrec.multi_unit_retail, --      IN     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                      ilrec.multi_selling_uom, --      IN     ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                      ilrec.uin_type, --               IN     ITEM_LOC.UIN_TYPE%TYPE,
                                      ilrec.uin_label, --              IN     ITEM_lOC.UIN_LABEL%TYPE,
                                      ilrec.capture_time, --           IN     ITEM_LOC.CAPTURE_TIME%TYPE,
                                      ilrec.ext_uin_ind, --            IN     ITEM_LOC.EXT_UIN_IND%TYPE,
                                      ilrec.costing_loc, --            IN     ITEM_LOC.COSTING_LOC%TYPE DEFAULT NULL,
                                      ilrec.costing_loc_type --       IN     ITEM_LOC.COSTING_LOC_TYPE%TYPE DEFAULT NULL
                                      ) = FALSE then
        return false;
      end if;
    end loop;
  end loop;
  ---
  DBG(L_program, 'POS: Update item_loc. ROWS=' || to_char(L_row_count));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    ---
    return FALSE;
END;
--------------------------------------------------------------------------------
FUNCTION update_conditions(O_error_message  IN OUT  VARCHAR2,
                           I_pid            IN      number default null)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.UPDATE_CONDITIONS';
  ---
BEGIN
  ---
  update xxadeo_lfcy_item_cond
     set delete_ind = 'Y'
   where (item, lfcy_entity, lfcy_entity_type) in (
    select item, lfcy_entity, lfcy_entity_type
    from xxadeo_lfcy_work
    where (I_pid is null or process_id = I_pid)
    );
  ---
  DBG(L_program, 'POS: Mark for deletion. ROWS=' || to_char(SQL%ROWCOUNT));
  ---
  merge into xxadeo_lfcy_item_cond t0
  using (
          select w.item
                , w.lfcy_entity
                , w.lfcy_entity_type
                , rh.rule_id
          from  xxadeo_lfcy_work              w
                , xxadeo_lfcy_cfg_status_next sn
                , xxadeo_system_rules         rh
                , xxadeo_system_rules_detail  rd
          where (I_pid is null or w.process_id = I_pid)
            and sn.lfcy_status    = w.lfcy_status
            and rd.parameter      = 'LFCY_STATUS'
            and rd.value_1        = sn.lfcy_status_next
            and rd.rule_id        = rh.rule_id
            and rh.active_ind     = 'Y'
            and exists (select 1
                        from xxadeo_lfcy_cfg_item lc_cfg_i
                        where lc_cfg_i.lfcy_cond_id = rh.rule_id
                        and active = 'Y'
                        and (lc_cfg_i.item_type is null or lc_cfg_i.item_type = to_char(w.item_type))
                        and (lc_cfg_i.item_subtype is null or lc_cfg_i.item_subtype = to_char(w.item_subtype))
                        and (lc_cfg_i.pack_ind is null or lc_cfg_i.pack_ind = w.pack_ind)
                        and (lc_cfg_i.orderable_ind is null or lc_cfg_i.orderable_ind = w.orderable_ind)
                        and (lc_cfg_i.inventory_ind is null or lc_cfg_i.inventory_ind = w.inventory_ind)
                        and (lc_cfg_i.sellable_ind is null or lc_cfg_i.sellable_ind = w.sellable_ind)
                        and (lc_cfg_i.forecast_ind is null or lc_cfg_i.forecast_ind = w.forecast_ind)
                        )
        ) src
  on (
      src.item                  = t0.item
      and src.lfcy_entity       = t0.lfcy_entity
      and src.lfcy_entity_type  = t0.lfcy_entity_type
      and src.rule_id           = t0.lfcy_cond_id
      )
  when matched then
    update
       set delete_ind = 'N'
  when not matched then
    insert (item
            ,lfcy_entity
            ,lfcy_entity_type
            ,lfcy_cond_id
            ,val_status
            ,next_val_dt
            ,last_val_dt
            ,delete_ind
            ,last_update_datetime
            ,process_id)
      values (src.item
              ,src.lfcy_entity
              ,src.lfcy_entity_type
              ,src.rule_id
              ,null
              ,null
              ,null
              ,'N'
              ,sysdate
              ,I_pid)
  ;
  ---
  DBG(L_program, 'POS: Merge. ROWS=' || to_char(SQL%ROWCOUNT));
  ---
  delete xxadeo_lfcy_item_cond
   where (item, lfcy_entity, lfcy_entity_type) in (
    select item, lfcy_entity, lfcy_entity_type
    from xxadeo_lfcy_work
    where (I_pid is null or process_id = I_pid)
    )
    and delete_ind = 'Y';
  ---
  DBG(L_program, 'POS: Delete. ROWS=' || to_char(SQL%ROWCOUNT));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    ---
    return FALSE;
END;
--------------------------------------------------------------------------------
FUNCTION EXECUTE_COND(O_error_message  IN OUT  VARCHAR2,
                       O_rule_changes  OUT     number,
                       O_val_status    OUT     xxadeo_lfcy_item_cond.val_status%TYPE,
                       I_package       IN      xxadeo_system_rules.package_name%TYPE,
                       I_func          IN      xxadeo_system_rules.function_name%TYPE,
                       I_pid           IN      number,
                       I_param1        IN      xxadeo_system_rules_detail.value_1%TYPE,
                       I_param2        IN      xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.EXECUTE_COND';
  ---
  plsql_block       VARCHAR2(500);
  L_return          VARCHAR2(1);
  L_result          NUMBER;
  L_error_message   VARCHAR2(500);
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'EXECUTE_COND');
  ---
  DBG(L_program, 'Begin, PKG=' || I_package || ', FN=' || I_func);
  ---
  -- Must return varchar T/F (TRUE,FALSE) since exec immediate cant handle booleans
  ---
  plsql_block := 'DECLARE L_return boolean; L_txt_return varchar2(1); '
    || ' BEGIN '
    || '  L_return := ' || I_package || '.' || I_func || '(:o_error_message,:o_rule_changes,:o_val_status,:I_pid,:I_param1,:I_param2); '
    || '  if L_return = FALSE then '
    || '    L_txt_return := ''F''; '
    || '  elsif L_return = TRUE then '
    || '    L_txt_return := ''T''; '
    || '  else '
    || '    L_txt_return := ''E''; '
    || '  end if; '
    || '  :o_return := L_txt_return; '
    || 'END;';
  ---
  DBG(L_program, 'PLSQL=' || plsql_block);
  ---
  dbms_application_info.set_client_info('EXECUTE IMMEDIATE');
  ---
  execute immediate plsql_block
    using in out  O_error_message,
          out     O_rule_changes,
          out     O_val_status,
           in     I_pid,
           in     I_param1,
           in     I_param2,
          out     L_return
  ;
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'EXECUTE_COND');
  dbms_application_info.set_client_info('PREPARE RETURN');
  ---
  DBG(L_program, 'POS: execute immediate. CHANGES=' || to_char(O_rule_changes) || ', err=' || O_error_message || ', L_return=' || L_return);
  ---
  if L_return = 'T' then
    return TRUE;
  elsif L_return = 'F' then
    O_val_status := 'E';
    if O_error_message is null then
      O_error_message := 'Null error_message';
    end if;
    return FALSE;
  else
    O_val_status := 'E';
    if O_error_message is null then
      O_error_message := 'Invalid return + Null error_message';
    else
      O_error_message := 'Invalid return. error_message=' || O_error_message;
    end if;
    return FALSE;
  end if;
  ---
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    ---
    O_val_status := 'E';
    DBG(L_program, 'Exception: ' || O_error_message);
    ---
    return FALSE;
END;
--------------------------------------------------------------------------------
FUNCTION FILL_CFA_COL(O_error_message IN OUT  VARCHAR2,
                      O_col           OUT     cfa_attrib.view_col_name%TYPE,
                      I_parameter     IN      xxadeo_mom_dvm.parameter%TYPE)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.FILL_CFA_COL:' || I_parameter;
  ---
BEGIN
  ---
  -- group by => must have same column name for all BU's
  -- Exception is raised when that is not true
  ---
  select storage_col_name
    into O_col
    from cfa_attrib cfaa
         ,xxadeo_mom_dvm xmd
   where xmd.parameter = I_parameter
     and xmd.value_1 = cfaa.attrib_id
   group by storage_col_name
  ;
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    ---
    return FALSE;
END;
--------------------------------------------------------------------------------
FUNCTION FILL_CFA_COL_AND_GRP(O_error_message IN OUT  VARCHAR2,
                              O_col           OUT     cfa_attrib.view_col_name%TYPE,
                              O_grp           OUT     cfa_attrib.group_id%TYPE,
                              I_parameter     IN      xxadeo_mom_dvm.parameter%TYPE)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.FILL_CFA_COL_AND_GRP:' || I_parameter;
  ---
BEGIN
  ---
  -- group by => must have same column name for all BU's
  -- Exception is raised when that is not true
  ---
  select storage_col_name, group_id
    into O_col, O_grp
    from cfa_attrib cfaa
         ,xxadeo_mom_dvm xmd
   where xmd.parameter = I_parameter
     and xmd.value_1 = cfaa.attrib_id
   group by storage_col_name, group_id
  ;
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    ---
    return FALSE;
END;
--------------------------------------------------------------------------------
-- Validate XXADEO_MOM_DVM for single value
-- UDAs/CFAs without BU
--------------------------------------------------------------------------------
FUNCTION VALIDATE_MOM_DVM_NULLBU(O_error_message IN OUT  VARCHAR2,
                                 I_parameter     IN      varchar2,
                                 I_func_area     IN      varchar2)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.VALIDATE_MOM_DVM:' || I_parameter;
  ---
  L_bu        xxadeo_mom_dvm.bu%TYPE;
  ---
BEGIN
  ---
  select bu
    into L_bu
    from xxadeo_mom_dvm xmd
   where xmd.parameter = I_parameter
     and nvl(bu,-1) = -1
     and func_area = I_func_area;
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    ---
    return FALSE;
END;
--------------------------------------------------------------------------------
-- Validate XXADEO_MOM_DVM for single value
-- UDAs/CFAs with BU
--------------------------------------------------------------------------------
FUNCTION VALIDATE_MOM_DVM_WITHBU(O_error_message IN OUT  VARCHAR2,
                                 I_parameter     IN      varchar2,
                                 I_func_area     IN      varchar2)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.VALIDATE_MOM_DVM_WITHBU:' || I_parameter;
  ---
  L_foo        varchar2(1);
  ---
BEGIN
  ---
  select 'X'
    into L_foo
    from xxadeo_mom_dvm xmd
   where xmd.parameter = I_parameter
     and nvl(bu,-1) <> -1
     and func_area = I_func_area
   group by xmd.func_area, xmd.parameter, xmd.bu
  having count(1) > 1;
  ---
  if sql%notfound then
    return TRUE;
  end if;
  ---
  return false;
  ---
EXCEPTION
  when NO_DATA_FOUND then
    return true;
  when TOO_MANY_ROWS then
    return false;
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    ---
    return FALSE;
END;
--------------------------------------------------------------------------------
FUNCTION initialize_globals
RETURN BOOLEAN
is
  ---
  L_program       VARCHAR2(100) := CONST$PackageName || '.INITIALIZE_GLOBALS';
  ---
  L_error_message VARCHAR2(1000);
  ---
BEGIN
  if GV_globals_initialized = TRUE then
    return TRUE;
  end if;
  ---
  -- TODO: use CODE_HEADE/DETAIL for these values????
  ---
  GV_UDAVAL_LFCY_status$init := 1;
  GV_UDAVAL_LFCY_status$asi  := 2;
  GV_UDAVAL_LFCY_status$acom := 3;
  GV_UDAVAL_LFCY_status$disc := 4;
  ---
  GV_CFAVAL_LnkStaItSup$Ini   := '1';
  GV_CFAVAL_LnkStaItSup$Act   := '2';
  ---
  -- CFA LINK_STATUS_ITEM_SUPP
  ---
  GV_CFACOL_LnkStaItSup       := 'VARCHAR2_1';
  ---
  if FILL_CFA_COL(L_error_message,
                  GV_CFACOL_LnkStaItSup,
                  CONST$CFAKEY_LnkStaItSup) = FALSE then
    raise_application_error(CONST$ERR_FILLCFACOL, CONST$CFAKEY_LnkStaItSup || '@' || L_error_message);
  end if;
  --
  if GV_CFACOL_LnkStaItSup not like 'VARCHAR2%' then
    raise_application_error(-20002, 'Wrong type: ' || CONST$CFAKEY_LnkStaItSup || ' => ' || GV_CFACOL_LnkStaItSup);
  end if;
  ---
  -- CFA LMxx_ACTIF_DDATE_ITEM
  ---
  GV_CFACOL_ActvDDt := 'DATE_24';
  ---
  if FILL_CFA_COL(L_error_message,
                  GV_CFACOL_ActvDDt,
                  CONST$CFAKEY_ActvDDt) = FALSE then
    raise_application_error(CONST$ERR_FILLCFACOL, CONST$CFAKEY_ActvDDt || '@' || L_error_message);
  end if;
  ---
  -- CFA SIGNED_K_IND_SUPP
  ---
  GV_CFACOL_ContrctSignSupp := 'VARCHAR2_1';
  ---
  if FILL_CFA_COL(L_error_message,
                  GV_CFACOL_ContrctSignSupp,
                  CONST$CFAKEY_ContrctSignSupp) = FALSE then
    raise_application_error(CONST$ERR_FILLCFACOL, CONST$CFAKEY_ContrctSignSupp || '@' || L_error_message);
  end if;
  ---
  -- CFA STATUS_QC_COMP_ITEM_SUPP
  ---
  GV_CFACOL_StatusQCSupp := 'VARCHAR2_1';
  ---
  if FILL_CFA_COL(L_error_message,
                  GV_CFACOL_StatusQCSupp,
                  CONST$CFAKEY_StatusQCSupp) = FALSE then
    raise_application_error(CONST$ERR_FILLCFACOL, CONST$CFAKEY_StatusQCSupp || '@' || L_error_message);
  end if;
  ---
  -- CFA DEPARTMENT_SUPP
  ---
  GV_CFACOL_DepartmentSupp := 'NUMBER_11';
  ---
  if FILL_CFA_COL(L_error_message,
                  GV_CFACOL_DepartmentSupp,
                  CONST$CFAKEY_DepartmentSupp) = FALSE then
    raise_application_error(CONST$ERR_FILLCFACOL, CONST$CFAKEY_DepartmentSupp || '@' || L_error_message);
  end if;
  ---
  -- CFA ECOTAX_ELC_COMP
  ---
  GV_CFACOL_EcotaxElcComp := 'NUMBER_11';
  ---
  if FILL_CFA_COL_AND_GRP(L_error_message,
                          GV_CFACOL_EcotaxElcComp,
                          GV_CFAGRP_EcotaxElcComp,
                          CONST$CFAKEY_EcotaxElcComp) = FALSE then
    raise_application_error(CONST$ERR_FILLCFACOL, CONST$CFAKEY_EcotaxElcComp || '@' || L_error_message);
  end if;
  ---
  if GV_CFACOL_EcotaxElcComp not like 'NUMBER_%' then
    raise_application_error(-20002, 'Wrong type: ' || CONST$CFAKEY_EcotaxElcComp || ' => ' || GV_CFACOL_EcotaxElcComp);
  end if;
  ---
  -- CFA LINK_TERM_DT_ITEM_SUPP
  ---
  GV_CFACOL_LnkTermDtItemSupp := 'DATE_22';
  ---
  if FILL_CFA_COL(L_error_message,
                  GV_CFACOL_LnkTermDtItemSupp,
                  CONST$CFAKEY_LnkTermDtItemSupp) = FALSE then
    raise_application_error(CONST$ERR_FILLCFACOL, CONST$CFAKEY_LnkTermDtItemSupp || '@' || L_error_message);
  end if;
  ---
  if GV_CFACOL_LnkTermDtItemSupp not like 'DATE_%' then
    raise_application_error(-20002, 'Wrong type: ' || CONST$CFAKEY_LnkTermDtItemSupp || ' => ' || CONST$CFAKEY_LnkTermDtItemSupp);
  end if;
  ---
  -- CFA LMxx_ABON_START_DATE_ITEM
  ---
  GV_CFACOL_AbonStartDate := 'DATE_21';
  ---
  if FILL_CFA_COL(L_error_message,
                  GV_CFACOL_AbonStartDate ,
                  CONST$CFAKEY_AbonStartDate ) = FALSE then
    raise_application_error(CONST$ERR_FILLCFACOL, CONST$CFAKEY_AbonStartDate  || '@' || L_error_message);
  end if;
  ---
  -- CFA LMxx_ABON_END_DATE_ITEM
  ---
  GV_CFACOL_AbonEndDate := 'DATE_23';
  ---
  if FILL_CFA_COL(L_error_message,
                  GV_CFACOL_AbonEndDate ,
                  CONST$CFAKEY_AbonEndDate ) = FALSE then
    raise_application_error(CONST$ERR_FILLCFACOL, CONST$CFAKEY_AbonEndDate  || '@' || L_error_message);
  end if;
  ---
  -- CFA LMxx_ACTIF_STATUS_DATE_ITEM
  ---
  GV_CFACOL_ActifStatusDate := 'DATE_22';
  ---
  if FILL_CFA_COL(L_error_message,
                  GV_CFACOL_ActifStatusDate ,
                  CONST$CFAKEY_ActifStatusDate ) = FALSE then
    raise_application_error(CONST$ERR_FILLCFACOL, CONST$CFAKEY_ActifStatusDate  || '@' || L_error_message);
  end if;
  ---
  -- Validate XXADEO_MOM_DVM mappings
  -- UDAs/CFAs with no bu should be unique on FUNC_AREA/PARAMETER
  -- UDAs/CFAs with bu should be unique on FUNC_AREA/PARAMETER/BU
  -- This should be enforced by data convertion but we check it here, just in case.
  ---
  if VALIDATE_MOM_DVM_WITHBU(L_error_message,
                             CONST$UDAKEY_BASACompleteRef ,
                             'UDA') = FALSE then
    raise_application_error(CONST$ERR_MOMDVMKEYBU, CONST$UDAKEY_BASACompleteRef || '@' || L_error_message);
  end if;
  ---
  if VALIDATE_MOM_DVM_WITHBU(L_error_message,
                             CONST$UDAKEY_SalesEndDate,
                             'UDA') = FALSE then
    raise_application_error(CONST$ERR_MOMDVMKEYBU, CONST$UDAKEY_SalesEndDate  || '@' || L_error_message);
  end if;
  ---
  if VALIDATE_MOM_DVM_WITHBU(L_error_message,
                             CONST$UDAKEY_SalesStartDate,
                             'UDA') = FALSE then
    raise_application_error(CONST$ERR_MOMDVMKEYBU, CONST$UDAKEY_SalesStartDate  || '@' || L_error_message);
  end if;
  ---
  if VALIDATE_MOM_DVM_NULLBU(L_error_message,
                             CONST$UDAKEY_ItemType,
                             'UDA') = FALSE then
    raise_application_error(CONST$ERR_MOMDVMKEYBU, CONST$UDAKEY_ItemType  || '@' || L_error_message);
  end if;
  ---
  if VALIDATE_MOM_DVM_NULLBU(L_error_message,
                             CONST$UDAKEY_ItemSubType,
                             'UDA') = FALSE then
    raise_application_error(CONST$ERR_MOMDVMKEYBU, CONST$UDAKEY_ItemSubType  || '@' || L_error_message);
  end if;
  ---
  if VALIDATE_MOM_DVM_NULLBU(L_error_message,
                             CONST$CFAKEY_LnkStaItSup,
                             'CFA') = FALSE then
    raise_application_error(CONST$ERR_MOMDVMKEYBU, CONST$CFAKEY_LnkStaItSup  || '@' || L_error_message);
  end if;
  ---
  if VALIDATE_MOM_DVM_NULLBU(L_error_message,
                             CONST$CFAKEY_EcotaxElcComp,
                             'CFA') = FALSE then
    raise_application_error(CONST$ERR_MOMDVMKEYBU, CONST$CFAKEY_EcotaxElcComp  || '@' || L_error_message);
  end if;
  ---
  GV_globals_initialized := TRUE;
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
    ---
    GV_init_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
    ---
    return FALSE;
end initialize_globals;

begin
  ---
  declare
    O_tbl DBG_SQL.TBL_DBG_OBJ;
  begin
    ---
    DBG_SQL.INIT(O_tbl);
    ---
  exception when others
    then null;
  end;
  ---
  if initialize_globals = FALSE then
    null;
  end if;
  ---
END XXADEO_ITEM_LIFE_CYCLE_SQL;
/
