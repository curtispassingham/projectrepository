create or replace package body XXADEO_RIB_XML is
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_RIB_XML.pkb
* Description:   Package to add namespaces to xml and create XML elements
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/

  XMLNS_NS4     constant varchar2(20) := 'xmlns:ns4';
  XMLNS_NS6     constant varchar2(20) := 'xmlns:ns6';
  NS4           constant varchar2(20) := 'ns4:';
  NS6           constant varchar2(20) := 'ns6:';
  EMPTY_ELEMENT constant varchar2(20) := '*EMPTY_ELEMENT*';


  function addElementPrivate(parentElem in xmldom.DOMElement,
                             name       in varchar2,
                             value      in varchar2) return xmldom.DOMElement;
                             

  -------------------------------------------------------------------------------------------------------
  -- Name: getXMLNamespaceURL
  -- Description: function return custom url
  -------------------------------------------------------------------------------------------------------
  function getXMLNamespaceURL return varchar2 is
  
    module varchar2(60) := 'xxadeo_rib_xml.getXMLNamespaceURL';
    url    varchar2(400);
  
  begin
    url := 'http://www.oracle.com/retail/integration/custom/bo';
  
    -- Return the URL.
    return url;
  
  exception
    when OTHERS then
      dbms_output.put_line('ERROR in ' || module || ': ' || sqlerrm);
      raise_application_error(-20001, sqlerrm);
  end getXMLNamespaceURL;

  
  -------------------------------------------------------------------------------------------------------
  -- Name:addElement
  -- Description: invoke addElement when value is a VARCHAR2
  -------------------------------------------------------------------------------------------------------
  procedure addElement(parentElem in xmldom.DOMElement,
                       name       in varchar2,
                       value      in varchar2) is
  
    module varchar2(60) := 'xxadeo_rib_xml.addElement(1)';
    tmp    xmldom.DOMElement;
  
  begin
  
    tmp := addElementPrivate(parentElem, name, value);
  
  exception
    when OTHERS then
      dbms_output.put_line('ERROR in ' || module || ': ' || sqlerrm);
      raise_application_error(-20001, sqlerrm);
  end addElement;

 
  -------------------------------------------------------------------------------------------------------
  -- Name:addElement
  -- Description: invoke addElement when value is a NUMBER
  -------------------------------------------------------------------------------------------------------
  procedure addElement(parentElem in xmldom.DOMElement,
                       name       in varchar2,
                       value      in number) is
  
    module    varchar2(60) := 'xxadeo_rib_xml.addElement(2)';
    tmp       xmldom.DOMElement;
    charValue varchar2(64) := NULL;
  
  begin
  
    if value is NOT NULL then
      charValue := TO_CHAR(value, 'TM', 'NLS_NUMERIC_CHARACTERS = ''. '' ');
      tmp       := addElementPrivate(parentElem, name, charValue);
    end if;
  
  exception
    when OTHERS then
      dbms_output.put_line('ERROR in ' || module || ': ' || sqlerrm);
      raise_application_error(-20001, sqlerrm);
  end addElement;

  
  -------------------------------------------------------------------------------------------------------
  -- Name:addElement
  -- Description: invoke addElement when not have value
  -------------------------------------------------------------------------------------------------------
  function addElement(parentElem in xmldom.DOMElement, name in varchar2)
    return xmldom.DOMElement is
  
    module varchar2(60) := 'xxadeo_rib_xml.addElement(3)';
  
  begin
  
    return addElementPrivate(parentElem, name, EMPTY_ELEMENT);
  
  exception
    when OTHERS then
      dbms_output.put_line('ERROR in ' || module || ': ' || sqlerrm);
      raise_application_error(-20001, sqlerrm);
  end addElement;


  -------------------------------------------------------------------------------------------------------
  -- Name:addElement
  -- Description: invoke addElement when values is a date
  -------------------------------------------------------------------------------------------------------
  procedure addElement(parentElem in xmldom.DOMElement,
                       name       in varchar2,
                       value      in date) is
  
    module    varchar2(60) := 'xxadeo_rib_xml.addElement';
    charValue varchar2(64) := NULL;
    tmp       xmldom.DOMElement;
  begin
    if value is not null then
  charValue:= to_char(value, 'YYYY-MM-DD') || 'T' ||
                   to_char(value, 'HH24:MI:SS') ||'.000Z';
  tmp := addElementPrivate(parentElem, name, charValue);
  
  end if;
  exception
    when OTHERS then
      dbms_output.put_line('ERROR in ' || module || ': ' || sqlerrm);
      raise_application_error(-20001, sqlerrm);
  end addElement;
  
  
  -------------------------------------------------------------------------------------------------------
  -- Name:addElementPrivate
  -- Description: create XML element and add namespaces and prefix
  -------------------------------------------------------------------------------------------------------
  function addElementPrivate(parentElem in xmldom.DOMElement,
                             name       in varchar2,
                             value      in varchar2) return xmldom.DOMElement is
  
    module     varchar2(60) := 'rib_xml.addElementPrivate';
    parentNode xmldom.DOMNode;
    parentName varchar2(100);
    ownerdoc   xmldom.DOMDocument;
    newElem    xmldom.DOMElement;
    newNode    xmldom.DOMNode;
    textNode   xmldom.DOMNode;
    lName      varchar2(100);
    attr       xmldom.DOMAttr;
    namespace  varchar2(20);
  
  begin
    if value is not null then
      -- Get the owner of the document and the name of the parent node.
      parentNode := xmldom.makeNode(parentElem);
      parentName := xmldom.getNodeName(parentNode);
      ownerdoc   := xmldom.getOwnerDocument(parentNode);
    
      if parentName = 'VendorBU' then
        lName := NS4 || name;
      elsif parentName = 'CustFlexAttriVo' or name = 'CustFlexAttriVo' then
        lName := NS6 || name;
      elsif parentName = 'ExtOfVendorHdrDesc' or
            name = 'ExtOfVendorHdrDesc' or parentName = 'create_date' then
        lName := NS4 || name;
      end if;
    
      -- Create a new element within this document and name it lName.
      -- Then create a node from the new element and append it to the parent node.
      newElem := xmldom.createElement(ownerdoc, lName);
      newNode := xmldom.appendChild(parentNode, xmldom.makeNode(newElem));
    
      if parentName = 'ExtOfVendorHdrDesc' then
       if name = 'CustFlexAttriVo' then
          namespace := XMLNS_NS6;
          attr      := xmldom.createAttribute(ownerdoc, namespace);
          xmldom.setValue(attr,
                          getXMLNamespaceURL || '/ExtOf' || name || '/' || 'v1');
          attr := xmldom.setAttributeNode(newElem, attr);
        end if;
      end if;
    
      if value != EMPTY_ELEMENT then
        textNode := xmldom.appendChild(newNode,
                                       xmldom.makeNode(xmldom.createTextNode(ownerdoc,
                                                                             value)));
      end if;
    
    end if;
    return newElem;
  
  exception
    when OTHERS then
      dbms_output.put_line('ERROR in ' || module || ': ' || sqlerrm);
      raise_application_error(-20001, sqlerrm);
  end addElementPrivate;

end XXADEO_RIB_XML;
/