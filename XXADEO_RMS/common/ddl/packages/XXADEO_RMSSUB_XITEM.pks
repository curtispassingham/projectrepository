create or replace PACKAGE XXADEO_RMSSUB_XITEM AUTHID CURRENT_USER AS
  ----------------------------------------------------------------------------
  --PUBLIC VARIABLES
  ----------------------------------------------------------------------------
  ITEM_ADEO   CONSTANT VARCHAR2(30) := 'xitemadeo';
  ITEM_ADD    CONSTANT VARCHAR2(30) := 'xitemcre';
  ITEM_UPD    CONSTANT VARCHAR2(30) := 'xitemmod';
  ITEM_DEL    CONSTANT VARCHAR2(30) := 'xitemdel';
  ISUP_ADD    CONSTANT VARCHAR2(30) := 'xitemsupcre';
  ISUP_UPD    CONSTANT VARCHAR2(30) := 'xitemsupmod';
  ISUP_DEL    CONSTANT VARCHAR2(30) := 'xitemsupdel';
  ISC_ADD     CONSTANT VARCHAR2(30) := 'xitemsupctycre';
  ISC_UPD     CONSTANT VARCHAR2(30) := 'xitemsupctymod';
  ISC_DEL     CONSTANT VARCHAR2(30) := 'xitemsupctydel';
  ISMC_ADD    CONSTANT VARCHAR2(30) := 'xiscmfrcre';
  ISMC_UPD    CONSTANT VARCHAR2(30) := 'xiscmfrmod';
  ISMC_DEL    CONSTANT VARCHAR2(30) := 'xiscmfrdel';
  IVAT_ADD    CONSTANT VARCHAR2(30) := 'xitemvatcre';
  IVAT_DEL    CONSTANT VARCHAR2(30) := 'xitemvatdel';
  ISCD_ADD    CONSTANT VARCHAR2(30) := 'xiscdimcre';
  ISCD_UPD    CONSTANT VARCHAR2(30) := 'xiscdimmod';
  ISCD_DEL    CONSTANT VARCHAR2(30) := 'xiscdimdel';
  ITEMUDA_ADD CONSTANT VARCHAR2(15) := 'xitemudacre';
  ITEMUDA_DEL CONSTANT VARCHAR2(15) := 'xitemudadel';
  IMAGE_ADD   CONSTANT VARCHAR2(30) := 'xitemimagecre';
  IMAGE_UPD   CONSTANT VARCHAR2(30) := 'xitemimagemod';
  IMAGE_DEL   CONSTANT VARCHAR2(30) := 'xitemimagedel';
  IMTL_ADD    CONSTANT VARCHAR2(30) := 'xitemtlcre';
  IMTL_UPD    CONSTANT VARCHAR2(30) := 'xitemtlmod';
  IMTL_DEL    CONSTANT VARCHAR2(30) := 'xitemtldel';
  IITL_ADD    CONSTANT VARCHAR2(30) := 'xitemimagetlcre';
  IITL_UPD    CONSTANT VARCHAR2(30) := 'xitemimagetlmod';
  IITL_DEL    CONSTANT VARCHAR2(30) := 'xitemimagetldel';

  ----------------------------------------------------------------------------
  PROCEDURE CONSUME(O_status_code   IN OUT VARCHAR2,
                    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_message       IN RIB_OBJECT,
                    I_message_type  IN VARCHAR2);
  ----------------------------------------------------------------------------

END XXADEO_RMSSUB_XITEM;
/