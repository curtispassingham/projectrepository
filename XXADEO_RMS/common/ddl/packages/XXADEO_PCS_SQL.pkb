CREATE OR REPLACE PACKAGE BODY XXADEO_PCS_SQL AS
  --
  LP_vdate           DATE        := GET_VDATE();
  LP_active_status   VARCHAR2(1) := 'A';
  LP_inactive_status VARCHAR2(1) := 'I';
  --
---------------------------------------------------------------------------------
-- Name: PCS_LIFE_CYCLE()
-- Purpose: This function is called for update records status to inactive when
--          effective_date is smaller than vdate + 1 and there are another PCS
--          for that same combination of the Item/Supplier/Store/Location or
--          Item/Supplier/Bu with an effective date greater than the previous
--          record and smaller or equal to vdate.
---------------------------------------------------------------------------------
FUNCTION PCS_LIFE_CYCLE(O_error_message OUT LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_program VARCHAR2(75) := 'XXADEO_PCS_SQL.PCS_LIFE_CYCLE';
  --
  cursor C_get_inactive_status_by_loc is
    select item,
           store
      from xxadeo_location_pcs pcs
     where status                = LP_active_status
       and trunc(effective_date) = LP_vdate +1
       for update nowait;
  --
  cursor C_get_inactive_status_by_area is
    select item,
           supplier,
           bu
      from xxadeo_area_pcs pcs
     where status                = LP_active_status
       and trunc(effective_date) = LP_vdate +1
       and exists                  (select 1
                                      from xxadeo_location_pcs xlp,
                                           rpm_zone_location rzl,
                                           xxadeo_rpm_bu_price_zones rbpz
                                     where xlp.status   = LP_active_status
                                       and xlp.store    = rzl.location
                                       and rzl.zone_id  = rbpz.zone_id
                                       and xlp.item     = pcs.item
                                       and xlp.supplier = pcs.supplier
                                       and rbpz.bu_id   = pcs.bu)             
       for update nowait;
  --
  cursor C_get_inactive_supp_area is
    select item,
           supplier,
           bu
      from xxadeo_area_pcs pcs
     where status                = LP_active_status
       and not exists              (select 1
                                      from xxadeo_location_pcs xlp,
                                           rpm_zone_location rzl,
                                           xxadeo_rpm_bu_price_zones rbpz
                                     where xlp.status   = LP_active_status
                                       and xlp.store    = rzl.location
                                       and rzl.zone_id  = rbpz.zone_id
                                       and xlp.item     = pcs.item
                                       and xlp.supplier = pcs.supplier
                                       and rbpz.bu_id   = pcs.bu)
     for update nowait;
  --
  TYPE INACTIVE_STATUS_BY_LOC_TBL IS TABLE OF C_get_inactive_status_by_loc%ROWTYPE;
  L_inactive_status_by_loc        INACTIVE_STATUS_BY_LOC_TBL;
  --
  TYPE INACTIVE_STATUS_BY_AREA_TBL IS TABLE OF C_get_inactive_status_by_area%ROWTYPE;
  L_inactive_status_by_area        INACTIVE_STATUS_BY_AREA_TBL;
  --
  L_inactive_supp_area             INACTIVE_STATUS_BY_AREA_TBL;
  --
BEGIN
  --
  open C_get_inactive_status_by_loc;
  fetch C_get_inactive_status_by_loc bulk collect into L_inactive_status_by_loc;
  close C_get_inactive_status_by_loc;
  --
  open C_get_inactive_status_by_area;
  fetch C_get_inactive_status_by_area bulk collect into L_inactive_status_by_area;
  close C_get_inactive_status_by_area;
  --
  open C_get_inactive_supp_area;
  fetch C_get_inactive_supp_area bulk collect into L_inactive_supp_area;
  close C_get_inactive_supp_area;
  --
  if L_inactive_status_by_loc.count > 0 and L_inactive_status_by_loc is NOT NULL then
    --
    forall rec in  L_inactive_status_by_loc.FIRST..L_inactive_status_by_loc.last
      update xxadeo_location_pcs
         set status = LP_inactive_status
       where item                  = L_inactive_status_by_loc(rec).item
         and store                 = L_inactive_status_by_loc(rec).store
         and trunc(effective_date) < LP_vdate + 1;
    --
  end if;
  --
  if L_inactive_status_by_area.count > 0 and L_inactive_status_by_area is NOT NULL then
    --
    forall rec in  L_inactive_status_by_area.FIRST..L_inactive_status_by_area.last
      update xxadeo_area_pcs
         set status = LP_inactive_status
       where item                  = L_inactive_status_by_area(rec).item
         and supplier              = L_inactive_status_by_area(rec).supplier
         and bu                    = L_inactive_status_by_area(rec).bu
         and trunc(effective_date) < LP_vdate +1;
    --
  end if;
  --
  if L_inactive_supp_area.count > 0 and L_inactive_supp_area is NOT NULL then
    --
    forall rec in L_inactive_supp_area.FIRST..L_inactive_supp_area.LAST
      update xxadeo_area_pcs
         set status = LP_inactive_status
       where item                  = L_inactive_supp_area(rec).item
         and supplier              = L_inactive_supp_area(rec).supplier
         and bu                    = L_inactive_supp_area(rec).bu
         and status                = LP_active_status;
    --
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END PCS_LIFE_CYCLE;
---------------------------------------------------------------------------------
-- Name: PURGE_PCS()
-- Purpose: This function will purge all records with status = I and
--          effective_date < VDate - I_purge_days
---------------------------------------------------------------------------------
FUNCTION PURGE_PCS(O_error_message OUT LOGGER_LOGS.TEXT%TYPE,
                   I_purge_days    IN  NUMBER)
RETURN BOOLEAN IS
  --
  L_program VARCHAR2(75) := 'XXADEO_PCS_SQL.PURGE_PCS';
  --
begin
  --
  if I_purge_days > 0 then
    --
    delete
      from xxadeo_location_pcs
     where status                = LP_inactive_status
       and trunc(effective_date) < LP_vdate - I_purge_days;
    --
    delete
      from xxadeo_area_pcs
     where status                = LP_inactive_status
       and trunc(effective_date) < LP_vdate - I_purge_days;
    --
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when others then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END PURGE_PCS;
--------------------------------------------------------------------------------
END XXADEO_PCS_SQL;
/
