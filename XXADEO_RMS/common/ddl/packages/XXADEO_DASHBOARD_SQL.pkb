create or replace package body XXADEO_DASHBOARD_SQL is

----------------------------------------------------------------------------------
  FUNCTION PC_VARIATION_N4W(I_bu                   IN  XXADEO_MV_DASHBOARD_PRC_CHG_VR.BU%TYPE DEFAULT NULL,
                            I_dept                 IN  XXADEO_MV_DASHBOARD_PRC_CHG_VR.DEPT_ID%TYPE DEFAULT NULL,
                            I_subdept_list         IN  VARCHAR2 DEFAULT NULL,
                            I_type_list            IN  VARCHAR2 DEFAULT NULL,
                            I_subtype_list         IN  VARCHAR2 DEFAULT NULL,
                            I_reason               IN  XXADEO_MV_DASHBOARD_PRC_CHG_VR.REASON_CODE%TYPE DEFAULT NULL,
                            I_effective_date_from  IN  XXADEO_MV_DASHBOARD_PRC_CHG_VR.EFFECTIVE_DATE%TYPE DEFAULT NULL,
                            I_effective_date_to    IN  XXADEO_MV_DASHBOARD_PRC_CHG_VR.EFFECTIVE_DATE%TYPE DEFAULT NULL)
  RETURN XXADEO_PCVARIATION_N4W_TBL
  PIPELINED AS
    --
    L_program               VARCHAR2(64) := 'XXADEO_DASHBOARD_SQL.PC_VARIATION_N4W';
    L_error_message         VARCHAR2(4000);
    --
    L_subdept_list          list_type := list_type();
    L_type_list             list_type := list_type();
    L_subtype_list          list_type := list_type();
    --
    L_PCVariationTBL        XXADEO_PCVARIATION_N4W_TBL;
    L_string_query          VARCHAR2(20000);
    L_string_query_aux      VARCHAR2(20000);
    L_sys_refcur            SYS_REFCURSOR;
    --
  BEGIN
    --
    EXECUTE IMMEDIATE ('ALTER SESSION SET NLS_DATE_FORMAT = ''DD-MM-YYYY''');
    --
    L_string_query := q'{with t_binds as (select :1 db_bu,
                                                 :2 db_dept,
                                                 :3 db_subdept,
                                                 :4 db_type,
                                                 :5 db_subtype,
                                                 :6 db_reason,
                                                 :7 db_effective_date_from,
                                                 :8 db_effective_date_to
                                            from dual)
                         select new XXADEO_PCVARIATION_N4W_OBJ(xDate, total, col_order)
                           from ( select to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 7),'dd/MM/yyyy'),'dd/MM/yyyy') xDate, 0 as total, '1' col_order                       
                                    from dual
                                  union all
                                  select to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 14),'dd/MM/yyyy'),'dd/MM/yyyy') xDate, 0 as total, '2' col_order                       
                                    from dual
                                  union all
                                  select to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 21),'dd/MM/yyyy'),'dd/MM/yyyy') xDate, 0 as total, '3' col_order                       
                                    from dual
                                  union all
                                  select to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 28),'dd/MM/yyyy'),'dd/MM/yyyy') xDate, 0 as total, '4' col_order                       
                                    from dual
                                  union all
                                  select xDate, count(1) as total, col_order
                                  from (
                                    select case
                                             when prc_chg.effective_date between TRUNC(get_vdate, 'IW') + 7 and TRUNC(get_vdate, 'IW') + 13 then
                                               to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 7),'dd/MM/yyyy'),'dd/MM/yyyy')
                                             when prc_chg.effective_date between TRUNC(get_vdate, 'IW') + 14 and TRUNC(get_vdate, 'IW') + 20 then
                                               to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 14),'dd/MM/yyyy'),'dd/MM/yyyy')
                                             when prc_chg.effective_date between TRUNC(get_vdate, 'IW') + 21 and TRUNC(get_vdate, 'IW') + 27 then
                                               to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 21),'dd/MM/yyyy'),'dd/MM/yyyy')
                                             when prc_chg.effective_date between TRUNC(get_vdate, 'IW') + 28 and TRUNC(get_vdate, 'IW') + 34 then
                                               to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 28),'dd/MM/yyyy'),'dd/MM/yyyy')
                                           end as xDate,
                                           1 as total,
                                           case when prc_chg.effective_date between TRUNC(get_vdate, 'IW') + 7 and TRUNC(get_vdate, 'IW') + 13 then
                                             '1'
                                                when prc_chg.effective_date between TRUNC(get_vdate, 'IW') + 14 and TRUNC(get_vdate, 'IW') + 20 then
                                             '2'
                                                when prc_chg.effective_date between TRUNC(get_vdate, 'IW') + 21 and TRUNC(get_vdate, 'IW') + 27 then
                                             '3'
                                                when effective_date between TRUNC(get_vdate, 'IW') + 28 and TRUNC(get_vdate, 'IW') + 34 then
                                             '4'
                                           end as col_order
                                      from XXADEO_MV_DASHBOARD_PRC_CHG_VR prc_chg,
                                           XXADEO_V_AREA_BU abu,
                                           v_item_master vim,
                                           t_binds b
                                     where prc_chg.bu = abu.area
                                       and prc_chg.effective_date between TRUNC(get_vdate, 'IW') + 7 and TRUNC(get_vdate, 'IW') + 34
                                       and vim.item = prc_chg.item}';

    -- BU
    if I_bu is not null then

      L_string_query := L_string_query || q'{ 
                                       and (prc_chg.bu = b.db_bu)}';
      --
    end if;

    -- Dept
    if I_dept is not null then

      L_string_query := L_string_query || q'{ 
                                       and (prc_chg.dept_id = b.db_dept)}';
      --
    end if;

    -- Subdept
    if I_subdept_list is not null then

      select t.*
        bulk collect
        into L_subdept_list
        from table(convert_comma_list(I_list => I_subdept_list)) t;

      L_string_query := L_string_query || q'{ 
                                       and (prc_chg.subdept_id in (select column_value from table(b.db_subdept)))}';
      --
    end if;

    -- Type
    if I_type_list is not null then

      select t.*
        bulk collect
        into L_type_list
        from table(convert_comma_list(I_list => I_type_list)) t;

      L_string_query := L_string_query || q'{ 
                                       and (prc_chg.type in (select column_value from table(b.db_type)))}';
      --
    end if;

    -- Subtype
    if I_subtype_list is not null then

      select t.*
        bulk collect
        into L_subtype_list
        from table(convert_comma_list(I_list => I_subtype_list)) t;

      L_string_query := L_string_query || q'{ 
                                       and (prc_chg.subtype in (select column_value from table(b.db_subtype)))}';
      --
    end if;

    -- Reason
    if I_reason is not null then

      L_string_query := L_string_query || q'{ 
                                       and (prc_chg.reason_code = b.db_reason)}';
      --
    end if;

    -- Effective Date From
    if I_effective_date_from is not null then

      L_string_query := L_string_query || q'{ 
                                       and (prc_chg.effective_date >= nvl(b.db_effective_date_from, prc_chg.effective_date))}';
      --
    end if;
    
    -- Effective Date To
    if I_effective_date_to is not null then

      L_string_query := L_string_query || q'{ 
                                       and (prc_chg.effective_date <= nvl(b.db_effective_date_to, prc_chg.effective_date))}';
      --
    end if;
    --
    L_string_query_aux := ')
                          group by xDate, col_order)
                     order by col_order';
    --
    L_string_query := L_string_query || L_string_query_aux;
    --
    dbms_output.put_line(L_string_query);
    --
    -- bulk query to table type
    --
    open L_sys_refcur for L_string_query
      using I_bu, I_dept, L_subdept_list, L_type_list, L_subtype_list, I_reason, I_effective_date_from, I_effective_date_to;
    loop
      --
      fetch L_sys_refcur bulk collect
        into L_PCVariationTBL;
      exit when L_PCVariationTBL.count = 0;
      --
      -- pipe data form collection
      --
      for i in 1 .. L_PCVariationTBL.count loop
        --
        pipe row(XXADEO_PCVARIATION_N4W_OBJ(L_PCVariationTBL(i).xdate,
                                            L_PCVariationTBL(i).total,
                                            L_PCVariationTBL(i).col_order));
      end loop;

      --
    end loop;
    --
    close L_sys_refcur;
    --
    return;
    --
  EXCEPTION
    --
    WHEN NO_DATA_NEEDED THEN
      --
      RETURN;
      --
    WHEN OTHERS THEN
      --
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      --
      dbms_output.put_line(L_error_message);
      --
      RETURN;
      --
    --
  END PC_VARIATION_N4W;
  
----------------------------------------------------------------------------------
  FUNCTION PC_AMOUNT_N4W(I_bu                   IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.BU%TYPE DEFAULT NULL,
                         I_dept                 IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.DEPT_ID%TYPE DEFAULT NULL,
                         I_subdept_list         IN  VARCHAR2 DEFAULT NULL,
                         I_type_list            IN  VARCHAR2 DEFAULT NULL,
                         I_subtype_list         IN  VARCHAR2 DEFAULT NULL,
                         I_reason               IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.REASON_CODE%TYPE DEFAULT NULL,
                         I_effective_date_from  IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.EFFECTIVE_DATE%TYPE DEFAULT NULL,
                         I_effective_date_to    IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.EFFECTIVE_DATE%TYPE DEFAULT NULL)
  RETURN XXADEO_PCAMOUNT_N4W_TBL
  PIPELINED AS
    --
    L_program               VARCHAR2(64) := 'XXADEO_DASHBOARD_SQL.PC_AMOUNT_N4W';
    L_error_message         VARCHAR2(4000);
    --
    L_subdept_list          list_type := list_type();
    L_type_list             list_type := list_type();
    L_subtype_list          list_type := list_type();
    --
    L_PCAmountTBL           XXADEO_PCAMOUNT_N4W_TBL;
    L_string_query          VARCHAR2(20000);
    L_string_query_aux      VARCHAR2(20000);
    L_sys_refcur            SYS_REFCURSOR;
    --
  BEGIN
    --
    EXECUTE IMMEDIATE ('ALTER SESSION SET NLS_DATE_FORMAT = ''DD-MM-YYYY''');
    --
    L_string_query := q'{with t_binds as (select :1 db_bu,
                                                 :2 db_dept,
                                                 :3 db_subdept,
                                                 :4 db_type,
                                                 :5 db_subtype,
                                                 :6 db_reason,
                                                 :7 db_effective_date_from,
                                                 :8 db_effective_date_to
                                            from dual)
                          select new XXADEO_PCAMOUNT_N4W_OBJ(xDate, total, col_order)
                           from ( select to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 7),'dd/MM/yyyy'),'dd/MM/yyyy') xDate, 0 as total, '1' col_order                       
                                    from dual
                                  union all
                                  select to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 14),'dd/MM/yyyy'),'dd/MM/yyyy') xDate, 0 as total, '2' col_order                       
                                    from dual
                                  union all
                                  select to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 21),'dd/MM/yyyy'),'dd/MM/yyyy') xDate, 0 as total, '3' col_order                       
                                    from dual
                                  union all
                                  select to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 28),'dd/MM/yyyy'),'dd/MM/yyyy') xDate, 0 as total, '4' col_order                       
                                    from dual
                                  union all
                                  select xDate, count(1) as total, col_order
                                  from (
                                    select case
                                             when effective_date between TRUNC(get_vdate, 'IW') + 7 and TRUNC(get_vdate, 'IW') + 13 then
                                               to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 7),'dd/MM/yyyy'),'dd/MM/yyyy')
                                             when effective_date between TRUNC(get_vdate, 'IW') + 14 and TRUNC(get_vdate, 'IW') + 20 then
                                               to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 14),'dd/MM/yyyy'),'dd/MM/yyyy')
                                             when effective_date between TRUNC(get_vdate, 'IW') + 21 and TRUNC(get_vdate, 'IW') + 27 then
                                               to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 21),'dd/MM/yyyy'),'dd/MM/yyyy')
                                             when effective_date between TRUNC(get_vdate, 'IW') + 28 and TRUNC(get_vdate, 'IW') + 34 then
                                               to_date(TO_CHAR((TRUNC(get_vdate, 'IW') + 28),'dd/MM/yyyy'),'dd/MM/yyyy')
                                           end as xDate,
                                           1 as total,
                                           case when effective_date between TRUNC(get_vdate, 'IW') + 7 and TRUNC(get_vdate, 'IW') + 13 then
                                             '1'
                                                when effective_date between TRUNC(get_vdate, 'IW') + 14 and TRUNC(get_vdate, 'IW') + 20 then
                                             '2'
                                                when effective_date between TRUNC(get_vdate, 'IW') + 21 and TRUNC(get_vdate, 'IW') + 27 then
                                             '3'
                                                when effective_date between TRUNC(get_vdate, 'IW') + 28 and TRUNC(get_vdate, 'IW') + 34 then
                                             '4'
                                           end as col_order
                                     from (select prc_chg.item, prc_chg.bu, prc_chg.subdept_id, prc_chg.effective_date
                                             from XXADEO_MV_DASHBOARD_PRC_CHG_NR prc_chg,
                                                  XXADEO_V_AREA_BU abu,  
                                                  v_item_master vim,
                                                  t_binds b
                                             where prc_chg.bu = abu.area               
                                               and prc_chg.effective_date between TRUNC(get_vdate, 'IW') + 7 and TRUNC(get_vdate, 'IW') + 34
                                              and vim.item = prc_chg.item}';

    -- BU
    if I_bu is not null then

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.bu = b.db_bu)}';
      --
    end if;

    -- Dept
    if I_dept is not null then

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.dept_id = b.db_dept)}';
      --
    end if;

    -- Subdept
    if I_subdept_list is not null then

      select t.*
        bulk collect
        into L_subdept_list
        from table(convert_comma_list(I_list => I_subdept_list)) t;

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.subdept_id in (select column_value from table(b.db_subdept)))}';
      --
    end if;

    -- Type
    if I_type_list is not null then

      select t.*
        bulk collect
        into L_type_list
        from table(convert_comma_list(I_list => I_type_list)) t;

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.type in (select column_value from table(b.db_type)))}';
      --
    end if;

    -- Subtype
    if I_subtype_list is not null then

      select t.*
        bulk collect
        into L_subtype_list
        from table(convert_comma_list(I_list => I_subtype_list)) t;

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.subtype in (select column_value from table(b.db_subtype)))}';
      --
    end if;

    -- Reason
    if I_reason is not null then

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.reason_code = b.db_reason)}';
      --
    end if;

    -- Effective Date From
    if I_effective_date_from is not null then

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.effective_date >= nvl(b.db_effective_date_from, prc_chg.effective_date))}';
      --
    end if;
    
    -- Effective Date To
    if I_effective_date_to is not null then

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.effective_date <= nvl(b.db_effective_date_to, prc_chg.effective_date))}';
      --
    end if;
    --
    L_string_query_aux := '
                           group by prc_chg.item, prc_chg.bu, prc_chg.subdept_id, prc_chg.effective_date))
                          group by xDate, col_order)
                     order by col_order';
    --
    L_string_query := L_string_query || L_string_query_aux;
    --
    dbms_output.put_line(L_string_query);
    -- bulk query to table type
    --
    open L_sys_refcur for L_string_query
      using I_bu, I_dept, L_subdept_list, L_type_list, L_subtype_list, I_reason, I_effective_date_from, I_effective_date_to;
    loop
      --
      fetch L_sys_refcur bulk collect
        into L_PCAmountTBL;
      exit when L_PCAmountTBL.count = 0;
      --
      -- pipe data form collection
      --
      for i in 1 .. L_PCAmountTBL.count loop
        --
        pipe row(XXADEO_PCAMOUNT_N4W_OBJ(L_PCAmountTBL(i).xdate,
                                         L_PCAmountTBL(i).total,
                                         L_PCAmountTBL(i).col_order));
      end loop;

      --
    end loop;
    --
    close L_sys_refcur;
    --
    return;
    --
  EXCEPTION
    --
    WHEN NO_DATA_NEEDED THEN
      --
      RETURN;
      --
    WHEN OTHERS THEN
      --
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      --
      dbms_output.put_line(L_error_message);
      --
      RETURN;
      --
    --
  END PC_AMOUNT_N4W;

----------------------------------------------------------------------------------
 /* FUNCTION DEALS_N4W(I_bu                   IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.BU%TYPE DEFAULT NULL,
                     I_dept                 IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.DEPT_ID%TYPE DEFAULT NULL,
                     I_subdept_list         IN  VARCHAR2 DEFAULT NULL,
                     I_type_list            IN  VARCHAR2 DEFAULT NULL,
                     I_subtype_list         IN  VARCHAR2 DEFAULT NULL,
                     I_reason               IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.REASON_CODE%TYPE DEFAULT NULL,
                     I_effective_date_from  IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.EFFECTIVE_DATE%TYPE DEFAULT NULL,
                     I_effective_date_to    IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.EFFECTIVE_DATE%TYPE DEFAULT NULL)
  RETURN XXADEO_PCAMOUNT_N4W_TBL
  PIPELINED AS
    --
    L_program               VARCHAR2(64) := 'XXADEO_DASHBOARD_SQL.XXADEO_PCAMOUNT_N4W';
    L_error_message         VARCHAR2(4000);
    --
    L_subdept_list          list_type := list_type();
    L_type_list             list_type := list_type();
    L_subtype_list          list_type := list_type();
    --
    L_PCAmountTBL           XXADEO_PCAMOUNT_N4W_TBL;
    L_string_query          VARCHAR2(20000);
    L_string_query_aux      VARCHAR2(20000);
    L_sys_refcur            SYS_REFCURSOR;
    --
  BEGIN
    --
    EXECUTE IMMEDIATE ('ALTER SESSION SET NLS_DATE_FORMAT = ''DD-MM-YYYY''');
    --
    L_string_query := q'{with t_binds as (select :1 db_bu,
                                                 :2 db_dept,
                                                 :3 db_subdept,
                                                 :4 db_type,
                                                 :5 db_subtype,
                                                 :6 db_reason,
                                                 :7 db_effective_date_from,
                                                 :8 db_effective_date_to
                                            from dual),
                              table_orders as (select xvdh.deal_id, xvdh.status, xvdh.active_date
                                                 from XXADEO_V_DEAL_DASHBOARD xvdh,
                                                      t_binds b
                                                where 1= 1}';

    -- BU
    if I_bu is not null then

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.bu = b.db_bu)}';
      --
    end if;

    -- Dept
    if I_dept is not null then

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.dept_id = b.db_dept)}';
      --
    end if;

    -- Subdept
    if I_subdept_list is not null then

      select t.*
        bulk collect
        into L_subdept_list
        from table(convert_comma_list(I_list => I_subdept_list)) t;

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.subdept_id in (select column_value from table(b.db_subdept)))}';
      --
    end if;

    -- Type
    if I_type_list is not null then

      select t.*
        bulk collect
        into L_type_list
        from table(convert_comma_list(I_list => I_type_list)) t;

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.type in (select column_value from table(b.db_type)))}';
      --
    end if;

    -- Subtype
    if I_subtype_list is not null then

      select t.*
        bulk collect
        into L_subtype_list
        from table(convert_comma_list(I_list => I_subtype_list)) t;

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.subtype in (select column_value from table(b.db_subtype)))}';
      --
    end if;

    -- Reason
    if I_reason is not null then

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.reason_code = b.db_reason)}';
      --
    end if;

    -- Effective Date From
    if I_effective_date_from is not null then

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.effective_date >= nvl(b.db_effective_date_from, prc_chg.effective_date))}';
      --
    end if;
    
    -- Effective Date To
    if I_effective_date_to is not null then

      L_string_query := L_string_query ||
                        q'{ and (prc_chg.effective_date <= nvl(b.db_effective_date_to, prc_chg.effective_date))}';
      --
    end if;
    --
    L_string_query_aux := '
                           group by xvdh.deal_id, xvdh.status, xvdh.active_date)
      select new XXADEO_PCAMOUNT_N4W_OBJ(xDate,
                                         total,
                                         col_order)
        from (
      select to_date(TO_CHAR((TRUNC(get_vdate, ''IW'') + 7),''dd/MM/yyyy''),''dd/MM/yyyy'') xDate, 0 as total, ''1'' col_order                     
        from dual 
      union all
      select to_date(TO_CHAR((TRUNC(get_vdate, ''IW'') + 7),''dd/MM/yyyy''),''dd/MM/yyyy'') xDate, count(1) total, ''1'' col_order
        from table_orders
       where effective_date between TRUNC(get_vdate, ''IW'') + 7 and TRUNC(get_vdate, ''IW'') + 13
      union all
      select to_date(TO_CHAR((TRUNC(get_vdate, ''IW'') + 14),''dd/MM/yyyy''),''dd/MM/yyyy'') xDate, 0 total, ''2'' col_order                   
        from dual
      union all
      select to_date(TO_CHAR((TRUNC(get_vdate, ''IW'') + 14),''dd/MM/yyyy''),''dd/MM/yyyy'') xDate, count(1) total, ''2'' col_order
        from table_orders 
       where effective_date between TRUNC(get_vdate, ''IW'') + 14 and TRUNC(get_vdate, ''IW'') + 20
      union all
      select to_date(TO_CHAR((TRUNC(get_vdate, ''IW'') + 21),''dd/MM/yyyy''),''dd/MM/yyyy'') xDate, 0 total, ''3'' col_order    
        from dual
      union all
      select to_date(TO_CHAR((TRUNC(get_vdate, ''IW'') + 21),''dd/MM/yyyy''),''dd/MM/yyyy'') xDate, count(1) total, ''3'' col_order
        from table_orders 
       where effective_date between TRUNC(get_vdate, ''IW'') + 21 and TRUNC(get_vdate, ''IW'') + 27
      union all
      select to_date(TO_CHAR((TRUNC(get_vdate, ''IW'') + 28),''dd/MM/yyyy''),''dd/MM/yyyy'') xDate, 0 total, ''4'' col_order    
        from dual
      union all
      select to_date(TO_CHAR((TRUNC(get_vdate, ''IW'') + 28),''dd/MM/yyyy''),''dd/MM/yyyy'') xDate, count(1) total, ''4'' col_order
        from table_orders
       where effective_date between TRUNC(get_vdate, ''IW'') + 28 and TRUNC(get_vdate, ''IW'') + 34)';
    --
    L_string_query := L_string_query || L_string_query_aux;
    --
    -- bulk query to table type
    --
    open L_sys_refcur for L_string_query
      using I_bu, I_dept, L_subdept_list, L_type_list, L_subtype_list, I_reason, I_effective_date_from, I_effective_date_to;
    loop
      --
      fetch L_sys_refcur bulk collect
        into L_PCAmountTBL;
      exit when L_PCAmountTBL.count = 0;
      --
      -- pipe data form collection
      --
      for i in 1 .. L_PCAmountTBL.count loop
        --
        pipe row(XXADEO_PCAMOUNT_N4W_OBJ(L_PCAmountTBL(i).xdate,
                                         L_PCAmountTBL(i).total,
                                         L_PCAmountTBL(i).col_order));
      end loop;

      --
    end loop;
    --
    close L_sys_refcur;
    --
    return;
    --
  EXCEPTION
    --
    WHEN NO_DATA_NEEDED THEN
      --
      RETURN;
      --
    WHEN OTHERS THEN
      --
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      --
      dbms_output.put_line(L_error_message);
      --
      RETURN;
      --
    --
  END DEALS_N4W;*/
  
----------------------------------------------------------------------------------
END XXADEO_DASHBOARD_SQL;
/