CREATE OR REPLACE PACKAGE BODY "FILTER_POLICY_SQL" AS
--------------------------------------------------------------------------
/* Note: All Logger.Log calls are commented. To enable it uncomment it and localise the same */
--------------------------------------------------------------------------------
-- SET_USER_FILTER()
--------------------------------------------------------------------------------
FUNCTION SET_USER_FILTER(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;

   L_application_user_id  SEC_USER.APPLICATION_USER_ID%TYPE;
   L_database_user_id     SEC_USER.DATABASE_USER_ID%TYPE;
   L_valid_user           VARCHAR2(1) := 'N';
   L_group_association    VARCHAR2(1) := 'N';

   -- Retreive 'APP_USER_ID' from 'RETAIL_CTX' to support connection pooling in Java applications (e.g. ReSA ADF).
   -- In a Java application, the login user is the application user defined in LDAP. Its credential should be used
   -- for security filtering, not the database login user. Upon logging into a Java application, the application id
   -- is set in database context 'RETAIL_CTX' with an attribute name of 'APP_USER_ID' and the attribute value of the
   -- login user id. Retrieve the value here for filtering.
   -- If the application user is not defined on the RETAIL_CTX for APP_USER_ID, then this package is called from RMS forms.
   -- Retrieve the database login id from 'USERENV' for data filtering.
   -- For Async processing in which the application user submits a request, the CLIENT_INFO will be set by package processiing
   -- the async request with the application user id.
   ---
   CURSOR C_LOGIN_USER IS
      SELECT NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'),SYS_CONTEXT('USERENV', 'CLIENT_INFO')),
             NVL(SYS_CONTEXT('USERENV', 'CLIENT_INFO'), SYS_CONTEXT('USERENV', 'SESSION_USER'))
        FROM DUAL;

   -- First check if the logged in user (i.e. either the application_user_id or the database_user_id)
   -- is a valid user on SEC_USER. If not, they are not a super-user, and security must be applied,
   -- even though the user will not have visibilty to any data.
   CURSOR C_FIND_USER IS
      SELECT 'Y'
        FROM SEC_USER su
       WHERE (su.application_user_id = L_application_user_id
          OR L_application_user_id is NULL
         AND su.database_user_id = L_database_user_id)
         AND ROWNUM = 1;
   ---
   -- The C_FIND_GROUP cursor is used to catch the scenario where a user is not
   -- associated to any group.  If this is the case, they are not a super-user,
   -- and security must be applied, even though the user will not have visibilty
   -- to any data.
   ---
   CURSOR C_FIND_GROUP IS
      SELECT 'Y'
        FROM SEC_USER_GROUP sug, SEC_USER su
       WHERE sug.user_seq = su.user_seq
         AND (su.application_user_id = L_application_user_id
          OR L_application_user_id is NULL
         AND su.database_user_id = L_database_user_id)
         AND ROWNUM = 1;

   -- When a user is associated to a group that is not associated to any
   -- hierarchy, the C_FILTER_USER_ORG cursor returns no rows.  This cursor
   -- also returns no rows if the user is not associated to any group, however
   -- if the C_FIND_GROUP cursor is fetched before this cursor, we can ensure
   -- that the C_FILTER_USER_ORG cursor is never executed in this second scenario.
   -- That being said, if the C_FILTER_USER_ORG cursor returns no rows (indicating
   -- the first scenario mentioned in this comment), then the user is associated
   -- to a group that is not associated to a hierarchy, and is therefore a super-
   -- user.
   CURSOR C_FILTER_USER_ORG IS
      SELECT 'Y'
        FROM SEC_USER_GROUP sug, SEC_USER su
       WHERE sug.user_seq = su.user_seq
         AND (su.application_user_id = L_application_user_id
          OR L_application_user_id is NULL
         AND su.database_user_id = L_database_user_id)
         AND (EXISTS(SELECT 'x'
                       FROM FILTER_GROUP_ORG fgo
                      WHERE fgo.sec_group_id = sug.group_id
                        AND ROWNUM = 1)
           OR EXISTS(SELECT 'x'
                       FROM SEC_GROUP_LOC_MATRIX sglm
                      WHERE sglm.group_id = sug.group_id
                        AND ROWNUM = 1))
         AND ROWNUM = 1;

   -- When a user is associated to a group that is not associated to any
   -- hierarchy, the C_FILTER_USER_MERCH cursor returns no rows.  This cursor
   -- also returns no rows if the user is not associated to any group, however
   -- if the C_FIND_GROUP cursor is fetched before this cursor, we can ensure
   -- that the C_FILTER_USER_MERCH cursor is never executed in this second scenario.
   -- That being said, if the C_FILTER_USER_MERCH cursor returns no rows (indicating
   -- the first scenario mentioned in this comment), then the user is associated
   -- to a group that is not associated to a hierarchy, and is therefore a super-
   -- user.
   CURSOR C_FILTER_USER_MERCH IS
      SELECT 'Y'
        FROM SEC_USER_GROUP sug, SEC_USER su
       WHERE sug.user_seq = su.user_seq
         AND (su.application_user_id = L_application_user_id
          OR L_application_user_id is NULL
         AND su.database_user_id = L_database_user_id)
         AND EXISTS(SELECT 'x'
                      FROM FILTER_GROUP_MERCH fgm
                     WHERE fgm.sec_group_id = sug.group_id
                       AND ROWNUM = 1)
         AND ROWNUM = 1;

BEGIN

   --logger.log('Call FILTER_POLICY_SQL.SET_USER_FILTER');

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            GP_system_options) = FALSE then
      return FALSE;
   end if;

   --logger.log('System data_level_security_ind is: '||GP_system_options.data_level_security_ind);

   if GP_system_options.data_level_security_ind = 'N' then
      GP_filter_org   := 'N';
      GP_filter_merch := 'N';
   else
      ---
      OPEN C_LOGIN_USER;
      FETCH C_LOGIN_USER into L_application_user_id, L_database_user_id;
      CLOSE C_LOGIN_USER;

      --logger.log('L_application_user_id is: ' || L_application_user_id);
      --logger.log('L_database_user_id is: ' || L_database_user_id);
      ---
      OPEN C_FIND_USER;
      FETCH C_FIND_USER into L_valid_user;
      CLOSE C_FIND_USER;

      -- if the user is not a valid user define on SEC_USER, they must have filtering applied.
      if L_valid_user = 'N' then
         --logger.log('Not a valid security user.');
         GP_filter_org := 'Y';
         GP_filter_merch := 'Y';
      else
         ---
         OPEN C_FIND_GROUP;
         FETCH C_FIND_GROUP into L_group_association;
         CLOSE C_FIND_GROUP;

         -- if the user is not associated to any group, they must have filtering applied.
         if L_group_association = 'N' then
            --logger.log('User is NOT associated with any security group.');
            GP_filter_org := 'Y';
            GP_filter_merch := 'Y';
         else
            ---
            --logger.log('User is associated with at least one security group. Check for super user ...');

            OPEN C_FILTER_USER_ORG;
            FETCH C_FILTER_USER_ORG INTO GP_filter_org;

            if C_FILTER_USER_ORG%NOTFOUND then
               GP_filter_org := 'N';
            end if;

            CLOSE C_FILTER_USER_ORG;
            ---
            OPEN C_FILTER_USER_MERCH;
            FETCH C_FILTER_USER_MERCH INTO GP_filter_merch;

            if C_FILTER_USER_MERCH%NOTFOUND then
               GP_filter_merch := 'N';
            end if;

            CLOSE C_FILTER_USER_MERCH;
         end if;
      end if;
   end if;
   ---

   --logger.log('Value of GP_filter_org after SET_USER_FILTER: '||GP_filter_org);
   --logger.log('Value of GP_filter_merch after SET_USER_FILTER: '||GP_filter_merch);

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_Lib.CREATE_MSG('PACKAGE_ERROR',
                                            'FILTER_POLICY_SQL.SET_USER_FILTER',
                                            SQLERRM,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END SET_USER_FILTER;

----------------------------------------------------------------------------------
FUNCTION V_Mrt_Item_S(d1 IN  VARCHAR2,
                                                    d2 IN VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   if ( GP_filter_merch = 'N' ) then
      return NULL;
   else

      L_predicate := 'exists (select ''X'' ' ||
                                  '  from v_item_master vi ' ||
                                  ' where vi.item = v_mrt_item.item ' ||
                                 ' and rownum = 1)';
   end if;

   return L_predicate;

END V_Mrt_Item_S;

--------------------------------------------------------------------------------

FUNCTION V_Mrt_Item_Loc_S(d1 IN VARCHAR2,
                                                              d2 IN VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   if ( GP_filter_org = 'N' ) then
      return NULL;
   else
      L_predicate := 'exists (select ''X'' ' ||
                                   '  from v_wh vw ' ||
                                   ' where vw.wh = v_mrt_item_loc.location ' ||
                                   ' and v_mrt_item_loc.loc_type = ''W'' ' ||
                                   ' and rownum = 1)' ||
                                   'or exists (select ''X'' ' ||
                                   '  from v_store vs ' ||
                                   ' where vs.store = v_mrt_item_loc.location ' ||
                                   ' and v_mrt_item_loc.loc_type = ''S'' ' ||
                                   ' and rownum = 1)';
   end if;

   return L_predicate;

END V_Mrt_Item_Loc_S;

--------------------------------------------------------------------------------
-- ASSEMBLY()
--------------------------------------------------------------------------------
FUNCTION ASSEMBLY(I_org_null IN VARCHAR2,
                  I_org_exists IN VARCHAR2,
                  I_merch_null IN VARCHAR2,
                  I_merch_exists IN VARCHAR2)
RETURN STRING IS

   L_product VARCHAR2(15000);

BEGIN

   IF(GP_filter_org = 'Y' AND GP_filter_merch = 'N' ) THEN
      L_product := '(' || I_org_null || ' or ' || I_org_exists || ')';
   ELSIF(GP_filter_org = 'N' AND GP_filter_merch = 'Y' ) THEN
      L_product := '(' || I_merch_null || ' or ' || I_merch_exists ||')';
   ELSE
      L_product := '((' || I_org_null || ' and ' || I_merch_null || ') ' ||
                   'or (' || I_org_exists || ' and ' || I_merch_null || ') '||
                   'or (' || I_merch_exists || ' and ' || I_org_null || ') '||
                   'or (' || I_org_exists || ' and ' || I_merch_exists || '))';
   END IF;
   ---
   RETURN L_product;

END ASSEMBLY;

--------------------------------------------------------------------------------
-- V_AREA_S()
--------------------------------------------------------------------------------
FUNCTION V_AREA_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_org = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                     'from v_district vd ' ||
                     'where vd.area = v_area.area ' ||
                     'and rownum = 1)';
   END IF;
   ---
   RETURN L_predicate;
END V_AREA_S;

--------------------------------------------------------------------------------
-- V_CHAIN_S()
--------------------------------------------------------------------------------
FUNCTION V_CHAIN_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_org = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                             '  from v_district vd ' ||
                             ' where vd.chain = v_chain.chain ' ||
                             '   and rownum = 1)';
   END IF;
   ---
   RETURN L_predicate;

END V_CHAIN_S;

--------------------------------------------------------------------------------
-- V_DEPS_S()
--------------------------------------------------------------------------------
FUNCTION V_DEPS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_merch = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select /*+ index(su) index(sug, sec_user_group_i1 ) */ ''X'' ' ||
                     '          from sec_user_group sug, sec_user su, filter_group_merch fgm, class c, subclass s ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (( SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NOT NULL and su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''))) ' ||
                     '            or (SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER'')))) ' ||
                     '           and sug.group_id = fgm.sec_group_id ' ||
                     '           and v_deps.division = DECODE(fgm.filter_merch_level, ''D'', fgm.filter_merch_id, v_deps.division) ' ||
                     '           and v_deps.group_no = DECODE(fgm.filter_merch_level, ''G'', fgm.filter_merch_id, v_deps.group_no) ' ||
                     '           and v_deps.dept     = DECODE(fgm.filter_merch_level, ''P'', fgm.filter_merch_id, v_deps.dept) ' ||
                     '           and c.class         = DECODE(fgm.filter_merch_level, ''C'', fgm.filter_merch_id_class, c.class) ' ||
                     '           and c.dept          = DECODE(fgm.filter_merch_level, ''C'', fgm.filter_merch_id, c.dept) ' ||
                     '           and s.subclass      = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_subclass, s.subclass) ' ||
                     '           and s.class         = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_class, s.class) ' ||
                     '           and s.dept          = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id, s.dept) ' ||
                     '           and s.dept  = c.dept ' ||
                     '           and s.class = c.class ' ||
                     '           and c.dept  = v_deps.dept ' ||
                     '           and rownum  = 1)';
   END IF;
   ---
   RETURN L_predicate;

END V_DEPS_S;

--------------------------------------------------------------------------------
-- V_CLASS_S()
--------------------------------------------------------------------------------
FUNCTION V_CLASS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_merch = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                     '          from filter_group_merch fgm, sec_user_group sug, sec_user su, v_deps vdp, subclass s ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id  = fgm.sec_group_id ' ||
                     '           and vdp.division  = DECODE(fgm.filter_merch_level, ''D'', fgm.filter_merch_id, vdp.division) ' ||
                     '           and vdp.group_no  = DECODE(fgm.filter_merch_level, ''G'', fgm.filter_merch_id, vdp.group_no) ' ||
                     '           and vdp.dept      = DECODE(fgm.filter_merch_level, ''P'', fgm.filter_merch_id, vdp.dept) ' ||
                     '           and v_class.class = DECODE(fgm.filter_merch_level, ''C'', fgm.filter_merch_id_class, v_class.class) ' ||
                     '           and v_class.dept  = DECODE(fgm.filter_merch_level, ''C'', fgm.filter_merch_id, v_class.dept) ' ||
                     '           and s.subclass    = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_subclass, s.subclass) ' ||
                     '           and s.class       = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_class, s.class) ' ||
                     '           and s.dept        = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id, s.dept) ' ||
                     '           and vdp.dept = v_class.dept ' ||
                     '           and s.dept   = v_class.dept ' ||
                     '           and s.class  = v_class.class ' ||
                     '           and rownum   = 1)';

   END IF;
   ---
   RETURN L_predicate;

END V_CLASS_S;

--------------------------------------------------------------------------------
-- V_SUBCLASS_S()
--------------------------------------------------------------------------------
FUNCTION V_SUBCLASS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_merch = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                     '          from filter_group_merch fgm, sec_user_group sug, sec_user su, v_deps vdp, class c ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id = fgm.sec_group_id ' ||
                     '           and vdp.division     = DECODE(fgm.filter_merch_level, ''D'', fgm.filter_merch_id, vdp.division) ' ||
                     '           and vdp.group_no     = DECODE(fgm.filter_merch_level, ''G'', fgm.filter_merch_id, vdp.group_no) ' ||
                     '           and vdp.dept         = DECODE(fgm.filter_merch_level, ''P'', fgm.filter_merch_id, vdp.dept) ' ||
                     '           and c.class          = DECODE(fgm.filter_merch_level, ''C'', fgm.filter_merch_id_class, c.class) ' ||
                     '           and c.dept           = DECODE(fgm.filter_merch_level, ''C'', fgm.filter_merch_id, c.dept) ' ||
                     '           and v_subclass.subclass = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_subclass, v_subclass.subclass) ' ||
                     '           and v_subclass.class    = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_class, v_subclass.class) ' ||
                     '           and v_subclass.dept     = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id, v_subclass.dept) ' ||
                     '           and vdp.dept = c.dept ' ||
                     '           and c.class  = v_subclass.class ' ||
                     '           and c.dept   = v_subclass.dept ' ||
                     '           and rownum   = 1)';
   END IF;
   ---
   RETURN L_predicate;

END V_SUBCLASS_S;

--------------------------------------------------------------------------------
-- V_DIFF_GROUP_HEAD_S()
--------------------------------------------------------------------------------
FUNCTION V_DIFF_GROUP_HEAD_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(15000);
   L_err_msg            VARCHAR2(250);

   L_org_null           VARCHAR2(50);
   L_merch_null         VARCHAR2(50);
   L_org_exists         VARCHAR2(2000);
   L_merch_exists       VARCHAR2(2000);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF(GP_filter_org = 'N' AND GP_filter_merch = 'N') THEN
      RETURN NULL;
   END IF;

   L_org_null := 'v_diff_group_head.filter_org_id is NULL';

   L_merch_null := 'v_diff_group_head.filter_merch_id is NULL';

   L_org_exists := 'exists (select ''X'' ' ||
                   '          from sec_user_group sug, sec_user su, filter_group_org fgo, area ara, region reg, district dis ' ||
                   '         where sug.user_seq = su.user_seq ' ||
                   '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                   '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                   '           and sug.group_id = fgo.sec_group_id ' ||
                   '           and ara.area = reg.area ' ||
                   '           and reg.region = dis.region ' ||
                   '           and fgo.filter_org_level in (''C'', ''A'', ''R'', ''D'') ' ||
                   '           and ara.chain    = decode(fgo.filter_org_level, ''C'', fgo.filter_org_id, ara.chain) ' ||
                   '           and ara.area     = decode(fgo.filter_org_level, ''A'', fgo.filter_org_id, ara.area) ' ||
                   '           and reg.region   = decode(fgo.filter_org_level, ''R'', fgo.filter_org_id, reg.region) ' ||
                   '           and dis.district = decode(fgo.filter_org_level, ''D'', fgo.filter_org_id, dis.district) ';

   IF(GP_system_options.diff_group_org_level_code = 'C') THEN
      L_org_exists := L_org_exists || 'and ara.chain = v_diff_group_head.filter_org_id ';
   ELSIF(GP_system_options.diff_group_org_level_code = 'A') THEN
      L_org_exists := L_org_exists || 'and ara.area = v_diff_group_head.filter_org_id ';
   ELSIF(GP_system_options.diff_group_org_level_code = 'R') THEN
      L_org_exists := L_org_exists || 'and reg.region = v_diff_group_head.filter_org_id ';
   ELSIF(GP_system_options.diff_group_org_level_code = 'D') THEN
      L_org_exists := L_org_exists || 'and dis.district = v_diff_group_head.filter_org_id ';
   END IF;
   ---
   L_org_exists := L_org_exists || 'and rownum = 1)';
   ---

   L_merch_exists := 'exists (select ''X'' ' ||
                     '          from sec_user_group sug, sec_user su, filter_group_merch fgm, groups gps, deps dps, class cls, subclass sub ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id = fgm.sec_group_id ' ||
                     '           and gps.group_no = dps.group_no ' ||
                     '           and dps.dept     = cls.dept ' ||
                     '           and dps.dept     = sub.dept ' ||
                     '           and cls.class    = sub.class '||
                     '           and gps.division = decode(fgm.filter_merch_level, ''D'', fgm.filter_merch_id, gps.division) ' ||
                     '           and gps.group_no = decode(fgm.filter_merch_level, ''G'', fgm.filter_merch_id, gps.group_no) ' ||
                     '           and dps.dept     = decode(fgm.filter_merch_level, ''P'', fgm.filter_merch_id, dps.dept) ' ||
                     '           and cls.class    = decode(fgm.filter_merch_level, ''C'', fgm.filter_merch_id_class, cls.class) ' ||
                     '           and cls.dept     = decode(fgm.filter_merch_level, ''C'', fgm.filter_merch_id, cls.dept) ' ||
                     '           and sub.subclass = decode(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_subclass, sub.subclass) ' ||
                     '           and sub.class    = decode(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_class, sub.class) ' ||
                     '           and sub.dept     = decode(fgm.filter_merch_level, ''S'', fgm.filter_merch_id, sub.dept) ';

   IF(GP_system_options.diff_group_merch_level_code = 'D') THEN
      L_merch_exists := L_merch_exists || 'and gps.division = v_diff_group_head.filter_merch_id ';
   ELSIF(GP_system_options.diff_group_merch_level_code = 'G') THEN
      L_merch_exists := L_merch_exists || 'and gps.group_no = v_diff_group_head.filter_merch_id ';
   ELSIF(GP_system_options.diff_group_merch_level_code = 'P') THEN
      L_merch_exists := L_merch_exists || 'and dps.dept = v_diff_group_head.filter_merch_id ';
   ELSIF(GP_system_options.diff_group_merch_level_code = 'C') THEN
      L_merch_exists := L_merch_exists || 'and cls.class = v_diff_group_head.filter_merch_id_class ' ||
                                          'and cls.dept  = v_diff_group_head.filter_merch_id ';
   ELSIF(GP_system_options.diff_group_merch_level_code = 'S') THEN
      L_merch_exists := L_merch_exists || 'and sub.subclass = v_diff_group_head.filter_merch_id_subclass ' ||
                                          'and sub.class    = v_diff_group_head.filter_merch_id_class ' ||
                                          'and sub.dept     = v_diff_group_head.filter_merch_id ';
   END IF;
   ---
   L_merch_exists := L_merch_exists || 'and rownum = 1)';
   ---

   L_predicate := ASSEMBLY(L_org_null, L_org_exists, L_merch_null, L_merch_exists);
   RETURN L_predicate;

END V_DIFF_GROUP_HEAD_S;

--------------------------------------------------------------------------------
-- V_DISTRICT_S()
--------------------------------------------------------------------------------
FUNCTION V_DISTRICT_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_org = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                     '          from filter_group_org fgo, sec_user_group sug, sec_user su ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id = fgo.sec_group_id ' ||
                     '           and fgo.filter_org_level in (''C'', ''A'', ''R'', ''D'') ' ||
                     '           and v_district.chain    = DECODE(fgo.filter_org_level, ''C'', fgo.filter_org_id, v_district.chain) ' ||
                     '           and v_district.area     = DECODE(fgo.filter_org_level, ''A'', fgo.filter_org_id, v_district.area) ' ||
                     '           and v_district.region   = DECODE(fgo.filter_org_level, ''R'', fgo.filter_org_id, v_district.region) ' ||
                     '           and v_district.district = DECODE(fgo.filter_org_level, ''D'', fgo.filter_org_id, v_district.district) ' ||
                     '           and rownum = 1 ' ||
                     '           union all ' ||
                     '        (select ''X'' ' ||
                     '          from filter_group_org fgo, sec_user_group sug, sec_user su ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id = fgo.sec_group_id ' ||
                     '           and fgo.filter_org_level=''O'' ' ||
                     '           and exists(select ''x'' from v_store v_store where v_store.district = v_district.district)))';
   END IF;
   ---
   RETURN L_predicate;

END V_DISTRICT_S;

--------------------------------------------------------------------------------
-- V_DIVISION_S()
--------------------------------------------------------------------------------
FUNCTION V_DIVISION_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_merch = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                             '  from v_deps vd ' ||
                             ' where vd.division = v_division.division ' ||
                             ' and rownum = 1)';
   END IF;
   ---
   RETURN L_predicate;

END V_DIVISION_S;

--------------------------------------------------------------------------------
-- V_EXTERNAL_FINISHER_S()
--------------------------------------------------------------------------------

FUNCTION V_EXTERNAL_FINISHER_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_org = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                     '          from filter_group_org fgo, sec_user_group sug, sec_user su ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id = fgo.sec_group_id ' ||
                     '           and fgo.filter_org_level in (''E'',''O'') ' ||
                     '           and v_external_finisher.finisher_id  = DECODE(fgo.filter_org_level, ''E'', fgo.filter_org_id, v_external_finisher.finisher_id) ' ||
                     '           and NVL(v_external_finisher.org_unit_id,-1) = DECODE(fgo.filter_org_level, ''O'', fgo.filter_org_id, NVL(v_external_finisher.org_unit_id,-1)) ' ||
                     '           and rownum = 1)';
   END IF;

   ---
   RETURN L_predicate;

END V_EXTERNAL_FINISHER_S;

--------------------------------------------------------------------------------
-- V_INTERNAL_FINISHER_S()
--------------------------------------------------------------------------------

FUNCTION V_INTERNAL_FINISHER_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_org = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                     '          from filter_group_org fgo, sec_user_group sug, sec_user su ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id = fgo.sec_group_id ' ||
                     '           and fgo.filter_org_level in (''I'',''O'') ' ||
                     '           and v_internal_finisher.finisher_id  = DECODE(fgo.filter_org_level, ''I'', fgo.filter_org_id, v_internal_finisher.finisher_id) ' ||
                     '           and NVL(v_internal_finisher.org_unit_id,-1) = DECODE(fgo.filter_org_level, ''O'', fgo.filter_org_id, NVL(v_internal_finisher.org_unit_id,-1)) ' ||
                     '           and rownum = 1)';
   END IF;

   ---
   RETURN L_predicate;

END V_INTERNAL_FINISHER_S;

--------------------------------------------------------------------------------
-- V_GROUPS_S()
--------------------------------------------------------------------------------

FUNCTION V_GROUPS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_merch = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                             '  from  v_deps vd ' ||
                             ' where vd.group_no = v_groups.group_no ' ||
                             '   and rownum = 1)';
   END IF;

   ---
   RETURN L_predicate;

END V_GROUPS_S;

--------------------------------------------------------------------------------
-- XXADEO_V_ITEM_MASTER_S()
--------------------------------------------------------------------------------
FUNCTION XXADEO_V_ITEM_MASTER_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(32767);
   L_err_msg            VARCHAR2(250);

BEGIN
  --
  if SET_USER_FILTER(L_err_msg) = FALSE then
    return NULL;
  END IF;
  --
  -- if not exist item filter, to merchandise structure level and organizational structure level
  -- then return a value of NULL from the function
  --
  IF GP_filter_merch = 'N' and GP_FILTER_ORG = 'N' THEN
    RETURN NULL;
  ELSE
    IF GP_FILTER_ORG = 'Y' and GP_filter_merch = 'Y' then
       --
      L_predicate :=
         'exists (select /*+ LEADING(su, sug) index(fgm FILTER_GROUP_MERCH_I1) index(su) index(sug, sec_user_group_i1) */ ''X'' ' ||
         '          from sec_user_group sug, sec_user su, filter_group_merch fgm ' ||
         '         where sug.user_seq = su.user_seq ' ||
         '           and (( SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NOT NULL and su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''))) ' ||
         '            or (SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER'')))) ' ||
         '           and sug.group_id = fgm.sec_group_id ' ||
         '           and v_item_master.division = DECODE(fgm.filter_merch_level, ''D'', fgm.filter_merch_id, v_item_master.division) ' ||
         '           and v_item_master.group_no = DECODE(fgm.filter_merch_level, ''G'', fgm.filter_merch_id, v_item_master.group_no) ' ||
         '           and v_item_master.dept     = DECODE(fgm.filter_merch_level, ''P'', fgm.filter_merch_id, v_item_master.dept) ' ||
         '           and v_item_master.class    = DECODE(fgm.filter_merch_level, ''C'', fgm.filter_merch_id_class, v_item_master.class) ' ||
         '           and v_item_master.dept     = DECODE(fgm.filter_merch_level, ''C'', fgm.filter_merch_id, v_item_master.dept) ' ||
         '           and v_item_master.subclass = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_subclass, v_item_master.subclass) ' ||
         '           and v_item_master.class    = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_class, v_item_master.class) ' ||
         '           and v_item_master.dept     = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id, v_item_master.dept) ' ||
         '           and rownum = 1 )' ||
         ' and exists( '||
         '        select /*+ LEADING(su2, sug2) index(fgm FILTER_GROUP_MERCH_I1) index(su2) index(sug2, sec_user_group_i1) */ ''X'' ' ||
         '          FROM uda_item_lov uil, area a, filter_group_org fgo, sec_user_group sug2, sec_group sg2, sec_user su2, code_head ch, code_detail cd '||
         '         where sug2.user_seq = su2.user_seq ' ||
         '           and (( SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NOT NULL and su2.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''))) ' ||
         '            or (SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su2.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER'')))) ' ||
         '           and  sug2.group_id = fgo.sec_group_id       '||
         '           and  cd.code_type = ch.code_type            '||
         '           and  ch.code_type = ''BUID''                '||
         '           and  uil.uda_value = fgo.filter_org_id      '||
         '           and  uil.uda_id = cd.code                   '||
         '           and  su2.user_seq = sug2.user_seq           '||
         '           and  sug2.group_id = sg2.group_id           '||
         '           and  nvl(v_item_master.item_parent,v_item_master.item) = uil.item          '||
         '           and  rownum = 1) ';

    ELSIF GP_FILTER_ORG = 'N' and GP_filter_merch = 'Y' THEN
      --
      L_predicate :=
         'exists (select /*+ LEADING(su, sug) index(fgm FILTER_GROUP_MERCH_I1) index(su) index(sug, sec_user_group_i1) */ ''X'' ' ||
         '          from sec_user_group sug, sec_user su, filter_group_merch fgm ' ||
         '         where sug.user_seq = su.user_seq ' ||
         '           and (( SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NOT NULL and su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''))) ' ||
         '            or (SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER'')))) ' ||
         '           and sug.group_id = fgm.sec_group_id ' ||
         '           and v_item_master.division = DECODE(fgm.filter_merch_level, ''D'', fgm.filter_merch_id, v_item_master.division) ' ||
         '           and v_item_master.group_no = DECODE(fgm.filter_merch_level, ''G'', fgm.filter_merch_id, v_item_master.group_no) ' ||
         '           and v_item_master.dept     = DECODE(fgm.filter_merch_level, ''P'', fgm.filter_merch_id, v_item_master.dept) ' ||
         '           and v_item_master.class    = DECODE(fgm.filter_merch_level, ''C'', fgm.filter_merch_id_class, v_item_master.class) ' ||
         '           and v_item_master.dept     = DECODE(fgm.filter_merch_level, ''C'', fgm.filter_merch_id, v_item_master.dept) ' ||
         '           and v_item_master.subclass = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_subclass, v_item_master.subclass) ' ||
         '           and v_item_master.class    = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_class, v_item_master.class) ' ||
         '           and v_item_master.dept     = DECODE(fgm.filter_merch_level, ''S'', fgm.filter_merch_id, v_item_master.dept) ' ||
         '           and rownum = 1 )';
      --
    ELSIF GP_FILTER_ORG = 'Y' and GP_filter_merch = 'N' then
          L_predicate :=
         'exists (select /*+ LEADING(su2, sug2) index(fgm FILTER_GROUP_MERCH_I1) index(su2) index(sug2, sec_user_group_i1) */ ''X'' ' ||
         '          FROM uda_item_lov uil, area a, filter_group_org fgo, sec_user_group sug2, sec_group sg2, sec_user su2, code_head ch, code_detail cd '||
         '         where sug2.user_seq = su2.user_seq ' ||
         '           and (( SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NOT NULL and su2.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''))) ' ||
         '            or (SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su2.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER'')))) ' ||
         '           and  sug2.group_id = fgo.sec_group_id       '||
         '           and  cd.code_type = ch.code_type            '||
         '           and  ch.code_type = ''BUID''                '||
         '           and  uil.uda_value = fgo.filter_org_id      '||
         '           and  uil.uda_id = cd.code                   '||
         '           and  su2.user_seq = sug2.user_seq           '||
         '           and  sug2.group_id = sg2.group_id           '||
         '           and  nvl(v_item_master.item_parent,v_item_master.item) = uil.item          '||
         '           and  rownum = 1) ';

    ELSE
      --
      return NULL;
      --
    END IF;
  END IF;
  ---
  --logger.log('L_predicate returned from V_ITEM_MASTER_S: '||L_predicate);
  RETURN L_predicate;

END XXADEO_V_ITEM_MASTER_S;

--------------------------------------------------------------------------------
-- V_LOC_LIST_HEAD_S()
--------------------------------------------------------------------------------
FUNCTION V_LOC_LIST_HEAD_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

   L_org_null           VARCHAR2(50);
   L_org_exists         VARCHAR2(2000);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   L_org_null := 'v_loc_list_head.filter_org_id is NULL';

   L_org_exists := 'exists (select ''X'' ' ||
                   '          from sec_user_group sug, sec_user su, filter_group_org fgo, area ara, region reg, district dis ' ||
                   '         where sug.user_seq = su.user_seq ' ||
                   '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                   '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                   '           and sug.group_id = fgo.sec_group_id ' ||
                   '           and ara.area = reg.area ' ||
                   '           and reg.region = dis.region ' ||
                   '           and fgo.filter_org_level in (''C'', ''A'', ''R'', ''D'') ' ||
                   '           and ara.chain    = decode(fgo.filter_org_level, ''C'', fgo.filter_org_id, ara.chain) ' ||
                   '           and ara.area     = decode(fgo.filter_org_level, ''A'', fgo.filter_org_id, ara.area) ' ||
                   '           and reg.region   = decode(fgo.filter_org_level, ''R'', fgo.filter_org_id, reg.region) ' ||
                   '           and dis.district = decode(fgo.filter_org_level, ''D'', fgo.filter_org_id, dis.district) ';





   IF(GP_system_options.loc_list_org_level_code = 'C') THEN
      L_org_exists := L_org_exists || 'and ara.chain = v_loc_list_head.filter_org_id ';
   ELSIF(GP_system_options.loc_list_org_level_code = 'A') THEN
      L_org_exists := L_org_exists || 'and ara.area = v_loc_list_head.filter_org_id ';
   ELSIF(GP_system_options.loc_list_org_level_code = 'R') THEN
      L_org_exists := L_org_exists || 'and reg.region = v_loc_list_head.filter_org_id ';
   ELSIF(GP_system_options.loc_list_org_level_code = 'D') THEN
      L_org_exists := L_org_exists || 'and dis.district = v_loc_list_head.filter_org_id ';
   END IF;
   ---
   L_org_exists := L_org_exists || 'and rownum = 1)';
   ---

   IF(GP_filter_org = 'N') THEN
      RETURN NULL;
   ELSE
      L_predicate := '(' || L_org_null || ' or ' || L_org_exists || ')';
   END IF;
   ---
   RETURN L_predicate;

END V_LOC_LIST_HEAD_S;

--------------------------------------------------------------------------------
-- V_LOC_TRAITS_S()
--------------------------------------------------------------------------------
FUNCTION V_LOC_TRAITS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

   L_org_null           VARCHAR2(50);
   L_org_exists         VARCHAR2(2000);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   L_org_null := 'v_loc_traits.filter_org_id is NULL';

   L_org_exists := 'exists (select ''X'' ' ||
                   '          from sec_user_group sug, sec_user su, filter_group_org fgo, area ara, region reg, district dis ' ||
                   '         where sug.user_seq = su.user_seq ' ||
                   '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                   '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                   '           and sug.group_id = fgo.sec_group_id ' ||
                   '           and ara.area = reg.area ' ||
                   '           and reg.region = dis.region ' ||
                   '           and fgo.filter_org_level in (''C'', ''A'', ''R'', ''D'') ' ||
                   '           and ara.chain    = decode(fgo.filter_org_level, ''C'', fgo.filter_org_id, ara.chain) ' ||
                   '           and ara.area     = decode(fgo.filter_org_level, ''A'', fgo.filter_org_id, ara.area) ' ||
                   '           and reg.region   = decode(fgo.filter_org_level, ''R'', fgo.filter_org_id, reg.region) ' ||
                   '           and dis.district = decode(fgo.filter_org_level, ''D'', fgo.filter_org_id, dis.district) ';

   IF(GP_system_options.loc_trait_org_level_code = 'C') THEN
      L_org_exists := L_org_exists || 'and ara.chain = v_loc_traits.filter_org_id ';
   ELSIF(GP_system_options.loc_trait_org_level_code = 'A') THEN
      L_org_exists := L_org_exists || 'and ara.area = v_loc_traits.filter_org_id ';
   ELSIF(GP_system_options.loc_trait_org_level_code = 'R') THEN
      L_org_exists := L_org_exists || 'and reg.region = v_loc_traits.filter_org_id ';
   ELSIF(GP_system_options.loc_trait_org_level_code = 'D') THEN
      L_org_exists := L_org_exists || 'and dis.district = v_loc_traits.filter_org_id ';
   END IF;
   ---
   L_org_exists := L_org_exists || 'and rownum = 1)';
   ---

   IF(GP_filter_org = 'N') THEN
      RETURN NULL;
   ELSE
      L_predicate := '(' || L_org_null || ' or ' || L_org_exists || ')';
   END IF;
   ---
   RETURN L_predicate;

END V_LOC_TRAITS_S;

--------------------------------------------------------------------------------
-- V_REGION_S()
--------------------------------------------------------------------------------
FUNCTION V_REGION_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_org = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                     'from v_district vd ' ||
                     'where vd.region = v_region.region ' ||
                     'and rownum = 1)';
   END IF;
   ---
   RETURN L_predicate;

END V_REGION_S;

--------------------------------------------------------------------------------
-- V_SEASONS_S()
--------------------------------------------------------------------------------

FUNCTION V_SEASONS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(15000);
   L_err_msg            VARCHAR2(250);

   L_org_null           VARCHAR2(50);
   L_merch_null         VARCHAR2(50);
   L_org_exists         VARCHAR2(2000);
   L_merch_exists       VARCHAR2(2000);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF(GP_filter_org = 'N' AND GP_filter_merch = 'N') THEN
      RETURN NULL;
   END IF;

   L_org_null := 'v_seasons.filter_org_id is NULL';

   L_merch_null := 'v_seasons.filter_merch_id is NULL';

   L_org_exists := 'exists (select ''X'' ' ||
                   '          from sec_user_group sug, sec_user su, filter_group_org fgo, area ara, region reg, district dis ' ||
                   '         where sug.user_seq = su.user_seq ' ||
                   '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                   '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                   '           and sug.group_id = fgo.sec_group_id ' ||
                   '            and ara.area = reg.area ' ||
                   '            and reg.region = dis.region ' ||
                   '            and fgo.filter_org_level in (''C'', ''A'', ''R'', ''D'') ' ||
                   '            and ara.chain    = decode(fgo.filter_org_level, ''C'', fgo.filter_org_id, ara.chain) ' ||
                   '            and ara.area     = decode(fgo.filter_org_level, ''A'', fgo.filter_org_id, ara.area) ' ||
                   '            and reg.region   = decode(fgo.filter_org_level, ''R'', fgo.filter_org_id, reg.region) ' ||
                   '            and dis.district = decode(fgo.filter_org_level, ''D'', fgo.filter_org_id, dis.district) ';

   IF(GP_system_options.season_org_level_code = 'C') THEN
      L_org_exists := L_org_exists || 'and ara.chain = v_seasons.filter_org_id ';
   ELSIF(GP_system_options.season_org_level_code = 'A') THEN
      L_org_exists := L_org_exists || 'and ara.area = v_seasons.filter_org_id ';
   ELSIF(GP_system_options.season_org_level_code = 'R') THEN
      L_org_exists := L_org_exists || 'and reg.region = v_seasons.filter_org_id ';
   ELSIF(GP_system_options.season_org_level_code = 'D') THEN
      L_org_exists := L_org_exists || 'and dis.district = v_seasons.filter_org_id ';
   END IF;
   ---
   L_org_exists := L_org_exists || 'and rownum = 1)';
   ---

   L_merch_exists := 'exists (select ''X'' ' ||
                     '          from sec_user_group sug, sec_user su, filter_group_merch fgm, groups gps, deps dps, class cls, subclass sub ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id = fgm.sec_group_id ' ||
                     '           and gps.group_no = dps.group_no ' ||
                     '           and dps.dept     = cls.dept ' ||
                     '           and dps.dept     = sub.dept ' ||
                     '           and cls.class    = sub.class '||
                     '           and gps.division = decode(fgm.filter_merch_level, ''D'', fgm.filter_merch_id, gps.division) ' ||
                     '           and gps.group_no = decode(fgm.filter_merch_level, ''G'', fgm.filter_merch_id, gps.group_no) ' ||
                     '           and dps.dept     = decode(fgm.filter_merch_level, ''P'', fgm.filter_merch_id, dps.dept) ' ||
                     '           and cls.class    = decode(fgm.filter_merch_level, ''C'', fgm.filter_merch_id_class, cls.class) ' ||
                     '           and cls.dept     = decode(fgm.filter_merch_level, ''C'', fgm.filter_merch_id, cls.dept) ' ||
                     '           and sub.subclass = decode(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_subclass, sub.subclass) ' ||
                     '           and sub.class    = decode(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_class, sub.class) ' ||
                     '           and sub.dept     = decode(fgm.filter_merch_level, ''S'', fgm.filter_merch_id, sub.dept) ';

   IF(GP_system_options.season_merch_level_code = 'D') THEN
      L_merch_exists := L_merch_exists || 'and gps.division = v_seasons.filter_merch_id ';
   ELSIF(GP_system_options.season_merch_level_code = 'G') THEN
      L_merch_exists := L_merch_exists || 'and gps.group_no = v_seasons.filter_merch_id ';
   ELSIF(GP_system_options.season_merch_level_code = 'P') THEN
      L_merch_exists := L_merch_exists || 'and dps.dept = v_seasons.filter_merch_id ';
   ELSIF(GP_system_options.season_merch_level_code = 'C') THEN
      L_merch_exists := L_merch_exists || 'and cls.class = v_seasons.filter_merch_id_class ' ||
                                          'and cls.dept = v_seasons.filter_merch_id ';
   ELSIF(GP_system_options.season_merch_level_code = 'S') THEN
      L_merch_exists := L_merch_exists || 'and sub.subclass = v_seasons.filter_merch_id_subclass ' ||
                                          'and sub.class = v_seasons.filter_merch_id_class ' ||
                                          'and sub.dept = v_seasons.filter_merch_id ';
   END IF;
   ---
   L_merch_exists := L_merch_exists || 'and rownum = 1)';
   ---

   L_predicate := ASSEMBLY(L_org_null, L_org_exists, L_merch_null, L_merch_exists);

   RETURN L_predicate;

END V_SEASONS_S;

--------------------------------------------------------------------------------
-- V_SKULIST_HEAD_S()
--------------------------------------------------------------------------------

FUNCTION V_SKULIST_HEAD_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(15000);
   L_err_msg            VARCHAR2(250);

   L_org_null           VARCHAR2(50);
   L_merch_null         VARCHAR2(120);
   L_org_exists         VARCHAR2(2000);
   L_merch_exists       VARCHAR2(2000);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF(GP_filter_org = 'N' AND GP_filter_merch = 'N') THEN
      RETURN NULL;
   END IF;

   L_org_null := 'v_skulist_head.filter_org_id is NULL';

   L_merch_null := 'not exists (select ''X'' ' ||
                   '   from skulist_dept sld ' ||
                   '  where sld.skulist = v_skulist_head.skulist ' ||
                   '    and rownum = 1) ';

   L_org_exists := 'exists (select ''X'' ' ||
                   '          from sec_user_group sug, sec_user su, filter_group_org fgo, area ara, region reg, district dis ' ||
                   '         where sug.user_seq = su.user_seq ' ||
                   '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                   '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                   '           and sug.group_id = fgo.sec_group_id ' ||
                   '           and ara.area = reg.area ' ||
                   '           and reg.region = dis.region ' ||
                   '           and fgo.filter_org_level in (''C'', ''A'', ''R'', ''D'') ' ||
                   '           and ara.chain    = decode(fgo.filter_org_level, ''C'', fgo.filter_org_id, ara.chain) ' ||
                   '           and ara.area     = decode(fgo.filter_org_level, ''A'', fgo.filter_org_id, ara.area) ' ||
                   '           and reg.region   = decode(fgo.filter_org_level, ''R'', fgo.filter_org_id, reg.region) ' ||
                   '           and dis.district = decode(fgo.filter_org_level, ''D'', fgo.filter_org_id, dis.district) ';

   IF(GP_system_options.skulist_org_level_code = 'C') THEN
      L_org_exists := L_org_exists || 'and ara.chain = v_skulist_head.filter_org_id ';
   ELSIF(GP_system_options.skulist_org_level_code = 'A') THEN
      L_org_exists := L_org_exists || 'and ara.area = v_skulist_head.filter_org_id ';
   ELSIF(GP_system_options.skulist_org_level_code = 'R') THEN
      L_org_exists := L_org_exists || 'and reg.region = v_skulist_head.filter_org_id ';
   ELSIF(GP_system_options.skulist_org_level_code = 'D') THEN
      L_org_exists := L_org_exists || 'and dis.district = v_skulist_head.filter_org_id ';
   END IF;
   ---
   L_org_exists := L_org_exists || 'and rownum = 1)';
   ---

   L_merch_exists := 'v_skulist_head.skulist in ' ||
                     ' (select distinct sld1.skulist ' ||
                     '    from skulist_dept sld1 ' ||
                     ' minus ' ||
                     '  select distinct sld2.skulist ' ||
                     '    from skulist_detail sld2 ' ||
                     '   where not exists (select ''X'' ' ||
                                         '   from v_item_master vi ' ||
                                         '  where vi.item = sld2.item ' ||
                                         '    and rownum = 1) ) ';
   ---

   L_predicate := ASSEMBLY(L_org_null, L_org_exists, L_merch_null, L_merch_exists);

   RETURN L_predicate;

END V_SKULIST_HEAD_S;

--------------------------------------------------------------------------------
-- V_STORE_S()
--------------------------------------------------------------------------------
FUNCTION V_STORE_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   --logger.log('Calling V_STORE_S ...');

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_org = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                     '          from filter_group_org fgo, sec_user_group sug, sec_user su ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id = fgo.sec_group_id ' ||
                     '           and fgo.filter_org_level in (''C'', ''A'', ''R'', ''D'', ''O'') ' ||
                     '           and v_store.chain = DECODE(fgo.filter_org_level, ''C'', fgo.filter_org_id, v_store.chain) ' ||
                     '           and v_store.area = DECODE(fgo.filter_org_level, ''A'', fgo.filter_org_id, v_store.area) ' ||
                     '           and v_store.region = DECODE(fgo.filter_org_level, ''R'', fgo.filter_org_id, v_store.region) ' ||
                     '           and v_store.district = DECODE(fgo.filter_org_level, ''D'', fgo.filter_org_id, v_store.district) ' ||
                     '           and NVL(v_store.org_unit_id,-1) = DECODE(fgo.filter_org_level, ''O'', fgo.filter_org_id, NVL(v_store.org_unit_id,-1)) ' ||
                     '           and rownum = 1)';

   END IF;
   --logger.log('L_predicate returned from V_STORE_S: '||L_predicate);
   ---
   RETURN L_predicate;

END V_STORE_S;

--------------------------------------------------------------------------------
-- V_TICKET_TYPE_HEAD_S()
--------------------------------------------------------------------------------

FUNCTION V_TICKET_TYPE_HEAD_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(15000);
   L_err_msg            VARCHAR2(250);

   L_org_null           VARCHAR2(50);
   L_merch_null         VARCHAR2(50);
   L_org_exists         VARCHAR2(2000);
   L_merch_exists       VARCHAR2(2000);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF(GP_filter_org = 'N' AND GP_filter_merch = 'N') THEN
      RETURN NULL;
   END IF;

   L_org_null := 'v_ticket_type_head.filter_org_id is NULL';

   L_merch_null := 'v_ticket_type_head.filter_merch_id is NULL';

   L_org_exists := 'exists (select ''X'' ' ||
                   '          from sec_user_group sug, sec_user su, filter_group_org fgo, area ara, region reg, district dis ' ||
                   '         where sug.user_seq = su.user_seq ' ||
                   '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                   '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                   '           and sug.group_id = fgo.sec_group_id ' ||
                   '           and ara.area = reg.area ' ||
                   '           and reg.region = dis.region ' ||
                   '           and fgo.filter_org_level in (''C'', ''A'', ''R'', ''D'') ' ||
                   '           and ara.chain    = decode(fgo.filter_org_level, ''C'', fgo.filter_org_id, ara.chain) ' ||
                   '           and ara.area     = decode(fgo.filter_org_level, ''A'', fgo.filter_org_id, ara.area) ' ||
                   '           and reg.region   = decode(fgo.filter_org_level, ''R'', fgo.filter_org_id, reg.region) ' ||
                   '           and dis.district = decode(fgo.filter_org_level, ''D'', fgo.filter_org_id, dis.district) ';

   IF(GP_system_options.ticket_type_org_level_code = 'C') THEN
      L_org_exists := L_org_exists || 'and ara.chain = v_ticket_type_head.filter_org_id ';
   ELSIF(GP_system_options.ticket_type_org_level_code = 'A') THEN
      L_org_exists := L_org_exists || 'and ara.area = v_ticket_type_head.filter_org_id ';
   ELSIF(GP_system_options.ticket_type_org_level_code = 'R') THEN
      L_org_exists := L_org_exists || 'and reg.region = v_ticket_type_head.filter_org_id ';
   ELSIF(GP_system_options.ticket_type_org_level_code = 'D') THEN
      L_org_exists := L_org_exists || 'and dis.district = v_ticket_type_head.filter_org_id ';
   END IF;
   ---
   L_org_exists := L_org_exists || 'and rownum = 1)';
   ---

   L_merch_exists := 'exists (select ''X'' ' ||
                     '          from sec_user_group sug, sec_user su, filter_group_merch fgm, groups gps, deps dps, class cls, subclass sub ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id = fgm.sec_group_id ' ||
                     '           and gps.group_no = dps.group_no ' ||
                     '           and dps.dept     = cls.dept ' ||
                     '           and dps.dept     = sub.dept ' ||
                     '           and cls.class    = sub.class '||
                     '           and gps.division = decode(fgm.filter_merch_level, ''D'', fgm.filter_merch_id, gps.division) ' ||
                     '           and gps.group_no = decode(fgm.filter_merch_level, ''G'', fgm.filter_merch_id, gps.group_no) ' ||
                     '           and dps.dept     = decode(fgm.filter_merch_level, ''P'', fgm.filter_merch_id, dps.dept) ' ||
                     '           and cls.class    = decode(fgm.filter_merch_level, ''C'', fgm.filter_merch_id_class, cls.class) ' ||
                     '           and cls.dept     = decode(fgm.filter_merch_level, ''C'', fgm.filter_merch_id, cls.dept) ' ||
                     '           and sub.subclass = decode(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_subclass, sub.subclass) ' ||
                     '           and sub.class    = decode(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_class, sub.class) ' ||
                     '           and sub.dept     = decode(fgm.filter_merch_level, ''S'', fgm.filter_merch_id, sub.dept) ';

   IF(GP_system_options.ticket_type_merch_level_code = 'D') THEN
      L_merch_exists := L_merch_exists || 'and gps.division = v_ticket_type_head.filter_merch_id ';
   ELSIF(GP_system_options.ticket_type_merch_level_code = 'G') THEN
      L_merch_exists := L_merch_exists || 'and gps.group_no = v_ticket_type_head.filter_merch_id ';
   ELSIF(GP_system_options.ticket_type_merch_level_code = 'P') THEN
      L_merch_exists := L_merch_exists || 'and dps.dept = v_ticket_type_head.filter_merch_id ';
   ELSIF(GP_system_options.ticket_type_merch_level_code = 'C') THEN
      L_merch_exists := L_merch_exists || 'and cls.class = v_ticket_type_head.filter_merch_id_class ' ||
                                          'and cls.dept  = v_ticket_type_head.filter_merch_id ';
   ELSIF(GP_system_options.ticket_type_merch_level_code = 'S') THEN
      L_merch_exists := L_merch_exists || 'and sub.subclass = v_ticket_type_head.filter_merch_id_subclass ' ||
                                          'and sub.class    = v_ticket_type_head.filter_merch_id_class ' ||
                                          'and sub.dept     = v_ticket_type_head.filter_merch_id ';
   END IF;
   ---
   L_merch_exists := L_merch_exists || 'and rownum = 1)';
   ---

   L_predicate := ASSEMBLY(L_org_null, L_org_exists, L_merch_null, L_merch_exists);

   RETURN L_predicate;

END V_TICKET_TYPE_HEAD_S;

--------------------------------------------------------------------------------
-- V_TRANSFER_FROM_STORE_S()
--------------------------------------------------------------------------------
FUNCTION V_TRANSFER_FROM_STORE_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF ( GP_filter_org = 'N' ) THEN
      RETURN NULL;
   ELSE
      L_predicate := 'store in (select sh.store ' ||
                     '            from sec_group_loc_matrix sglm, sec_user_group sug, sec_user su, store_hierarchy sh ' ||
                     '           where sug.user_seq = su.user_seq ' ||
                     '             and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '              or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '             and sglm.group_id = sug.group_id ' ||
                     '             and sglm.column_code = ''LTXFRM'' ' ||
                     '             and sh.region = sglm.region ' ||
                     '             and sh.district = NVL(sglm.district, sh.district)' ||
                     '             and sh.store = NVL(sglm.store, sh.store))';
   END IF;

   RETURN L_predicate;

END V_TRANSFER_FROM_STORE_S;

--------------------------------------------------------------------------------
-- V_TRANSFER_FROM_WH_S()
--------------------------------------------------------------------------------
FUNCTION V_TRANSFER_FROM_WH_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF ( GP_filter_org = 'N' ) THEN
      RETURN NULL;
   ELSE
      L_predicate := 'wh in (select sglm.wh ' ||
                     '         from sec_group_loc_matrix sglm, sec_user_group sug, sec_user su ' ||
                     '        where sug.user_seq = su.user_seq ' ||
                     '          and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '           or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '          and sglm.group_id = sug.group_id ' ||
                     '          and sglm.column_code = ''LTXFRM'' ' ||
                     '          and sglm.wh is not null ' ||
                     ' UNION ALL ' ||
                     ' select distinct wh.physical_wh ' ||
                     '   from sec_group_loc_matrix sglm,sec_user_group sug,sec_user su, wh '||
                     '  where sug.user_seq = su.user_seq ' ||
                '    and su.database_user_id = SYS_CONTEXT(''USERENV'', ''SESSION_USER'') ' ||
                     '    and sglm.group_id = sug.group_id ' ||
                     '    and sglm.column_code = ''LTXFRM'' ' ||
                     '    and sglm.wh = wh.wh) ';
   END IF;

   RETURN L_predicate;

END V_TRANSFER_FROM_WH_S;

--------------------------------------------------------------------------------
-- V_TRANSFER_TO_STORE_S()
--------------------------------------------------------------------------------
FUNCTION V_TRANSFER_TO_STORE_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF ( GP_filter_org = 'N' ) THEN
      RETURN NULL;
   ELSE
      L_predicate := 'store in (select sh.store ' ||
                     '            from sec_group_loc_matrix sglm, sec_user_group sug, sec_user su, store_hierarchy sh ' ||
                     '           where sug.user_seq = su.user_seq ' ||
                     '             and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '              or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '             and sglm.group_id = sug.group_id ' ||
                     '             and sglm.column_code = ''LTXFTO'' ' ||
                     '             and sh.region = sglm.region ' ||
                     '             and sh.district = NVL(sglm.district, sh.district) ' ||
                     '             and sh.store = NVL(sglm.store, sh.store)) ';
   END IF;

   RETURN L_predicate;

END V_TRANSFER_TO_STORE_S;

--------------------------------------------------------------------------------
-- V_TRANSFER_TO_WH_S()
--------------------------------------------------------------------------------
FUNCTION V_TRANSFER_TO_WH_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF ( GP_filter_org = 'N' ) THEN
      RETURN NULL;
   ELSE
      L_predicate := ' wh in (select sglm.wh ' ||
                     '          from sec_group_loc_matrix sglm, sec_user_group sug, sec_user su ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sglm.group_id = sug.group_id ' ||
                     '           and sglm.column_code = ''LTXFTO'' ' ||
                     '           and sglm.wh is not NULL ' ||
                     ' UNION ALL ' ||
                     ' select distinct wh.physical_wh ' ||
                     '   from sec_group_loc_matrix sglm, sec_user_group sug,sec_user su, wh '||
                     '  where sug.user_seq = su.user_seq ' ||
                '    and su.database_user_id = SYS_CONTEXT(''USERENV'', ''SESSION_USER'') ' ||
                     '    and sglm.group_id = sug.group_id ' ||
                     '    and sglm.column_code = ''LTXFTO'' ' ||
                     '    and sglm.wh = wh.wh) ';
   END IF;

   RETURN L_predicate;

END V_TRANSFER_TO_WH_S;

--------------------------------------------------------------------------------
-- V_TSF_ENTITY_S()
--------------------------------------------------------------------------------

FUNCTION V_TSF_ENTITY_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_org = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                     '          from filter_group_org fgo, sec_user_group sug, sec_user su ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id = fgo.sec_group_id ' ||
                     '           and fgo.filter_org_level = ''T'' ' ||
                     '           and fgo.filter_org_id = v_tsf_entity.tsf_entity_id ' ||
                     '           and rownum = 1)';
   END IF;

   ---
   RETURN L_predicate;

END V_TSF_ENTITY_S;
--------------------------------------------------------------------------------
-- Name: V_CFA_S()
-- Purpose: This function will enforce user select access to the
--          V_CFA view. If _filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
-------------------------------------------------------------------------------
FUNCTION V_CFA_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(15000);
   L_err_msg            VARCHAR2(250);

   L_org_null           VARCHAR2(50);
   L_org_exists         VARCHAR2(2000);
   L_context_1          VARCHAR2(100);
   l_CONTEXT_2          VARCHAR2(100);


BEGIN
   --
   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF(GP_filter_org = 'N') THEN
      RETURN NULL;
   ELSE
   --
   -- if cfa_attrib_group_set exists into filter_group_org table and entity exist into code_detail table,
   -- then execute where clause
   --
   L_predicate := 'exists (select ''X'' '||
                  '          from cfa_ext_entity ext, code_detail cds '||
                  '         where ext.ext_entity_id  = cfa_attrib_group_set.ext_entity_id '||
                  '           and cds.code != ext.ext_entity_id '||
                  '           and cds.code_type = '''||XXADEO_GLOBAL_VARS_SQL.CFA_SECURITY_ENTITY||''''||
                  '        union '||
                  '        select ''X'' '||
                  '          from  cfa_ext_entity ext, cfa_attrib_group cg, code_detail cds '||
                  '         where ext.ext_entity_id = cfa_attrib_group_set.ext_entity_id '||
                  '           and cg.group_set_id = cfa_attrib_group_set.group_set_id '||
                  '           and cds.code = ext.ext_entity_id '||
                  '           and cds.code_type = '''||XXADEO_GLOBAL_VARS_SQL.CFA_SECURITY_ENTITY||''''||
                  '           and not exists '||
                  '                          (select ''X'' '||
                  '                             from sec_group sg '||
                  '                            where sg.group_id = cg.group_set_id '||
                  '                              and rownum = 1) '||
                  '        union select ''X''   ' ||
                  '          from sec_user su,                  '||
                  '               sec_user_group sug,           '||
                  '               sec_group sg,                 '||
                  '               filter_group_org fgo          '||
                  '         where su.user_seq = sug.user_seq and                   '||
                  '               (( SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NOT NULL and su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''))) or' ||
                  '               (SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER'')))) and' ||
                  '               sug.group_id = sg.group_id   and   '||
                  '               sug.group_id = fgo.sec_group_id          and   ' ||
                  '               fgo.sec_group_id = cfa_attrib_group_set.group_set_id   and ' ||
                  '               rownum = 1 )';
   END IF;
   --LOGGER.LOG('L_predicate: '||L_predicate);
   RETURN L_predicate;
END V_CFA_S;
--------------------------------------------------------------------------------
-- Name: V_UPLOAD_CFA_S()
-- Purpose: This function will enforce user select access to the
--          v_s9t_tmpl_wksht_comp view. If _filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
-------------------------------------------------------------------------------
FUNCTION V_UPLOAD_CFA_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(15000);
   L_err_msg            VARCHAR2(250);

   L_org_null           VARCHAR2(50);
   L_org_exists         VARCHAR2(2000);


BEGIN
   --
   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;
   --
   IF(GP_filter_org = 'N') THEN
      RETURN NULL;
   ELSE
   --
   -- if worksheet name in template is a cfa, then execute where clause
   --
   L_predicate := 'exists (select ''X'' '||
                  '          from xxadeo_v_cfa_attrib_group_set cags '||
                  '          where cags.group_set_view_name = v_s9t_tmpl_wksht_comp.wksht_key '||
                  '            and rownum = 1 '||
                  '        union '||
                  '        select ''X'' '||
                  '          from s9t_tmpl_wksht_def c '||
                  '         where c.wksht_key = (case '||
                                               '   when v_s9t_tmpl_wksht_comp.wksht_key in '||
                                               '        (select cags.group_set_view_name '||
                                               '           from xxadeo_v_cfa_attrib_group_set cags) then '||
                                               '        v_s9t_tmpl_wksht_comp.wksht_key '||
                                               '   when v_s9t_tmpl_wksht_comp.wksht_key like ''V\_%'' ESCAPE ''\'' and '||
                                               '        v_s9t_tmpl_wksht_comp.wksht_key not in (select cags.group_set_view_name '||
                                               '                                                  from xxadeo_v_cfa_attrib_group_set cags) '||
                                               '     then null '||
                                               '   else '||
                                               '     v_s9t_tmpl_wksht_comp.wksht_key '||
                                               ' end) '||
                            ' and rownum = 1)';



   END IF;
   --LOGGER.LOG('L_predicate: '||L_predicate);
   RETURN L_predicate;
END V_UPLOAD_CFA_S;
--------------------------------------------------------------------------------
-- Name: V_S9T_TMPL_WKSHT_DEF_S()
-- Purpose: This function will enforce user select access to the
--          v_s9t_tmpl_wksht_def view. If _filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
-------------------------------------------------------------------------------
FUNCTION V_S9T_TMPL_WKSHT_DEF_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(15000);
   L_err_msg            VARCHAR2(250);

   L_org_null           VARCHAR2(50);
   L_org_exists         VARCHAR2(2000);


BEGIN
   --
   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;
   --
   IF(GP_filter_org = 'N') THEN
      RETURN NULL;
   ELSE
   --
   -- if worksheet name in template is a cfa, then execute where clause
   --
   L_predicate := 'exists (select ''X'' '||
                  '          from xxadeo_v_cfa_attrib_group_set cags '||
                  '          where cags.group_set_view_name = v_s9t_tmpl_wksht_def.wksht_key '||
                  '            and rownum = 1 '||
                  '        union '||
                  '        select ''X'' '||
                  '          from s9t_tmpl_wksht_def c '||
                  '         where c.wksht_key = (case '||
                                               '   when v_s9t_tmpl_wksht_def.wksht_key in '||
                                               '        (select cags.group_set_view_name '||
                                               '           from xxadeo_v_cfa_attrib_group_set cags) then '||
                                               '        v_s9t_tmpl_wksht_def.wksht_key '||
                                               '   when v_s9t_tmpl_wksht_def.wksht_key like ''V\_%'' ESCAPE ''\'' and '||
                                               '        v_s9t_tmpl_wksht_def.wksht_key not in (select cags.group_set_view_name '||
                                               '                                                 from xxadeo_v_cfa_attrib_group_set cags) '||
                                               '     then null '||
                                               '   else '||
                                               '     v_s9t_tmpl_wksht_def.wksht_key '||
                                               ' end) '||
                            ' and rownum = 1)';



   END IF;
   --LOGGER.LOG('L_predicate: '||L_predicate);
   RETURN L_predicate;
END V_S9T_TMPL_WKSHT_DEF_S;
--------------------------------------------------------------------------------
-- Name: V_S9T_LIST_VALS_S()
-- Purpose: This function will enforce user select access to the
--          s9t_list_vals table. If _filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
-------------------------------------------------------------------------------
FUNCTION V_S9T_LIST_VALS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(15000);
   L_err_msg            VARCHAR2(250);

   L_org_null           VARCHAR2(50);
   L_org_exists         VARCHAR2(2000);


BEGIN
   --
   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;
   --
   IF(GP_filter_org = 'N') THEN
      RETURN NULL;
   ELSE
   --
   -- if worksheet name in template is a cfa, then execute where clause
   --
   L_predicate := 'exists (select ''X'' '||
                  '          from xxadeo_v_cfa_attrib_group_set cags '||
                  '          where cags.group_set_view_name = s9t_list_vals.sheet_name '||
                  '            and rownum = 1 '||
                  '        union '||
                  '        select ''X'' '||
                  '          from s9t_tmpl_wksht_def c '||
                  '         where c.wksht_key = (case '||
                                               '   when s9t_list_vals.sheet_name in '||
                                               '        (select cags.group_set_view_name '||
                                               '           from xxadeo_v_cfa_attrib_group_set cags) then '||
                                               '        s9t_list_vals.sheet_name '||
                                               '   when s9t_list_vals.sheet_name like ''V\_%'' ESCAPE ''\'' and '||
                                               '        s9t_list_vals.sheet_name not in (select cags.group_set_view_name '||
                                               '                                          from xxadeo_v_cfa_attrib_group_set cags) '||
                                               '     then null '||
                                               '   else '||
                                               '     s9t_list_vals.sheet_name '||
                                               ' end) '||
                            ' and rownum = 1)';



   END IF;
   --LOGGER.LOG('L_predicate: '||L_predicate);
   RETURN L_predicate;
END V_S9T_LIST_VALS_S;
--------------------------------------------------------------------------------
-- V_UDA_S()
--------------------------------------------------------------------------------

FUNCTION V_UDA_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(15000);
   L_err_msg            VARCHAR2(250);

   L_org_null           VARCHAR2(50);
   L_merch_null         VARCHAR2(50);
   L_org_exists         VARCHAR2(2000);
   L_merch_exists       VARCHAR2(2000);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF(GP_filter_org = 'N' AND GP_filter_merch = 'N') THEN
      RETURN NULL;
   END IF;

   L_org_null := 'v_uda.filter_org_id is NULL';

   L_merch_null := 'v_uda.filter_merch_id is NULL';

   L_org_exists := 'exists (select ''X'' ' ||
                   '          from sec_user_group sug, sec_user su, filter_group_org fgo, area ara, region reg, district dis ' ||
                   '         where sug.user_seq = su.user_seq ' ||
                   '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                   '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                   '           and sug.group_id = fgo.sec_group_id ' ||
                   '           and ara.area = reg.area ' ||
                   '           and reg.region = dis.region ' ||
                   '           and fgo.filter_org_level in (''C'', ''A'', ''R'', ''D'') ' ||
                   '           and ara.chain    = decode(fgo.filter_org_level, ''C'', fgo.filter_org_id, ara.chain) ' ||
                   '           and ara.area     = decode(fgo.filter_org_level, ''A'', fgo.filter_org_id, ara.area) ' ||
                   '           and reg.region   = decode(fgo.filter_org_level, ''R'', fgo.filter_org_id, reg.region) ' ||
                   '           and dis.district = decode(fgo.filter_org_level, ''D'', fgo.filter_org_id, dis.district) ';

   IF(GP_system_options.uda_org_level_code = 'C') THEN
      L_org_exists := L_org_exists || 'and ara.chain = v_uda.filter_org_id ';
   ELSIF(GP_system_options.uda_org_level_code = 'A') THEN
      L_org_exists := L_org_exists || 'and ara.area = v_uda.filter_org_id ';
   ELSIF(GP_system_options.uda_org_level_code = 'R') THEN
      L_org_exists := L_org_exists || 'and reg.region = v_uda.filter_org_id ';
   ELSIF(GP_system_options.uda_org_level_code = 'D') THEN
      L_org_exists := L_org_exists || 'and dis.district = v_uda.filter_org_id ';
   END IF;
   ---
   L_org_exists := L_org_exists || 'and rownum = 1)';
   ---
   L_merch_exists := 'exists (select ''X'' ' ||
                     '          from sec_user_group sug, sec_user su, filter_group_merch fgm, groups gps, deps dps, class cls, subclass sub ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id = fgm.sec_group_id ' ||
                     '           and gps.group_no = dps.group_no ' ||
                     '           and dps.dept     = cls.dept ' ||
                     '           and cls.dept     = sub.dept ' ||
                     '           and cls.class    = sub.class ' ||
                     '           and gps.division = decode(fgm.filter_merch_level, ''D'', fgm.filter_merch_id, gps.division) ' ||
                     '           and gps.group_no = decode(fgm.filter_merch_level, ''G'', fgm.filter_merch_id, gps.group_no) ' ||
                     '           and dps.dept     = decode(fgm.filter_merch_level, ''P'', fgm.filter_merch_id, dps.dept) ' ||
                     '           and cls.class    = decode(fgm.filter_merch_level, ''C'', fgm.filter_merch_id_class, cls.class) ' ||
                     '           and cls.dept     = decode(fgm.filter_merch_level, ''C'', fgm.filter_merch_id, cls.dept) ' ||
                     '           and sub.subclass = decode(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_subclass, sub.subclass) ' ||
                     '           and sub.class    = decode(fgm.filter_merch_level, ''S'', fgm.filter_merch_id_class, sub.class) ' ||
                     '           and sub.dept     = decode(fgm.filter_merch_level, ''S'', fgm.filter_merch_id, sub.dept) ';

   IF(GP_system_options.uda_merch_level_code = 'D') THEN
      L_merch_exists := L_merch_exists || 'and gps.division = v_uda.filter_merch_id ';
   ELSIF(GP_system_options.uda_merch_level_code = 'G') THEN
      L_merch_exists := L_merch_exists || 'and gps.group_no = v_uda.filter_merch_id ';
   ELSIF(GP_system_options.uda_merch_level_code = 'P') THEN
      L_merch_exists := L_merch_exists || 'and dps.dept = v_uda.filter_merch_id ';
   ELSIF(GP_system_options.uda_merch_level_code = 'C') THEN
      L_merch_exists := L_merch_exists || 'and cls.class = v_uda.filter_merch_id_class ' ||
                                          'and cls.dept  = v_uda.filter_merch_id ';
   ELSIF(GP_system_options.uda_merch_level_code = 'S') THEN
      L_merch_exists := L_merch_exists || 'and sub.subclass = v_uda.filter_merch_id_subclass ' ||
                                          'and sub.class    = v_uda.filter_merch_id_class ' ||
                                          'and sub.dept     = v_uda.filter_merch_id ';
   END IF;
   ---
   L_merch_exists := L_merch_exists || 'and rownum = 1)';
   ---

   L_predicate := ASSEMBLY(L_org_null, L_org_exists, L_merch_null, L_merch_exists);

   RETURN L_predicate;

END V_UDA_S;

--------------------------------------------------------------------------------
-- V_WH_S()
--------------------------------------------------------------------------------

FUNCTION V_WH_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            VARCHAR2(250);

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   IF GP_filter_org = 'N' THEN
      RETURN NULL;
   ELSE
      L_predicate := 'exists (select ''X'' ' ||
                     '          from filter_group_org fgo, sec_user_group sug, sec_user su ' ||
                     '         where sug.user_seq = su.user_seq ' ||
                     '           and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                     '            or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                     '           and sug.group_id = fgo.sec_group_id ' ||
                     '           and fgo.filter_org_level in  (''W'',''O'') ' ||
                     '           and v_wh.wh  = DECODE(fgo.filter_org_level, ''W'', fgo.filter_org_id, v_wh.wh ) ' ||
                     '           and NVL(v_wh.org_unit_id,-1) = DECODE(fgo.filter_org_level, ''O'', fgo.filter_org_id, NVL(v_wh.org_unit_id,-1)) ' ||
                     '           and rownum = 1)';
   END IF;

   ---
   RETURN L_predicate;

END V_WH_S;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
-- Name:    V_SUPS_S
-- Purpose: This function will enforce user select access to the
--          V_SUPS view. If _filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_SUPS_S(d1 IN VARCHAR2,
                  d2 IN VARCHAR2)
RETURN VARCHAR2 IS

   L_predicate          VARCHAR2(2000);
   L_err_msg            RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   if SET_USER_FILTER(L_err_msg) = FALSE then
      return NULL;
   END IF;

   if GP_filter_org = 'N'
   or GP_system_options.org_unit_ind = 'N' then
      return NULL;
   else
      L_predicate := 'exists (select ''X'' '||
                               'from filter_group_org fgo, '||
                                    'sec_user_group sug, '||
                                    'sec_user su, ' ||
                                    'sups s, '||
                                    'partner_org_unit pou '||
                              'where s.supplier = v_sups.supplier '||
                               ' and pou.partner = s.supplier '||
                               ' and pou.partner_type = ''U'' '||
                               ' and sug.user_seq = su.user_seq ' ||
                               ' and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                               ' or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                               ' and sug.group_id = fgo.sec_group_id  '||
                               ' and fgo.filter_org_level = ''O'' '||
                               ' and pou.org_unit_id = fgo.filter_org_id '||
                               ' and rownum = 1 '||
                              'union all '||
                             'select ''X'' '||
                               'from sec_user_group sug, '||
                                    'sec_user su, ' ||
                                    'sups s '||
                              'where (s.supplier = v_sups.supplier and s.supplier_parent is NULL) '||
                               ' and sug.user_seq = su.user_seq ' ||
                               ' and (su.application_user_id = NVL(SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID''), SYS_CONTEXT(''USERENV'', ''CLIENT_INFO'')) ' ||
                                ' or SYS_CONTEXT(''RETAIL_CTX'', ''APP_USER_ID'') is NULL and su.database_user_id = NVL(SYS_CONTEXT(''USERENV'', ''CLIENT_INFO''), SYS_CONTEXT(''USERENV'', ''SESSION_USER''))) ' ||
                               ' and rownum = 1 '||
                              'union all '||
                             'select ''X'' '||
                               'from (select org_unit_id from v_store '||
                                     'union '||
                                     'select org_unit_id from v_wh '||
                                     'union '||
                                     'select org_unit_id from v_external_finisher '||
                                     'union '||
                                     'select org_unit_id from v_internal_finisher) loc, '||
                                     'sups s, '||
                                     'partner_org_unit pou '||
                               'where (s.supplier = v_sups.supplier or s.supplier_parent = v_sups.supplier) '||
                                 'and pou.partner = s.supplier '||
                                 'and pou.partner_type in (''S'',''U'') '||
                                 'and pou.org_unit_id = loc.org_unit_id '||
                                 'and rownum = 1) ';
   end if;
   ---
   return L_predicate;

END V_SUPS_S;
--------------------------------------------------------------------------------------

END FILTER_POLICY_SQL;
/
