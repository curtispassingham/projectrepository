CREATE OR REPLACE PACKAGE XXADEO_ITEM_ASSORTMENT_SQL AUTHID CURRENT_USER AS

/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Jorge Agra                                                   */
/* UPDATE USER - Elsa Barros, Pedro Miranda                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package for RB55a - Assortment ITEM Level and ITEM_LOC Level */
/******************************************************************************/

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-07-12 - Jorge Agra - Separation of validation to PREPROCESS_ALL       */
/* 2018-07-19 - Jorge Agra - BUG#145 VALIDATE_SD: SALES_END_DATE MUST be in   */
/*                           future.                                          */
/*                           To ease debug, make validate functions public    */
/*                           Include DBG_SQL.MSG                              */
/* 2018-07-20 - Jorge Agra - BUG#153 pre-process all records                  */
/* 2018-07-26 - Jorge Agra - BUG#163 ITEM_LOC CFA fetch problem corrected     */
/* 2018-08-30 - Jorge Agra - BUG#347 Comparison of effective date with =.     */
/*                           Should be <=                                     */
/* 2018-09-13 - Jorge Agra - BUG#397+398 effective date for RS and AM must be */
/*                           in the future                                    */
/* 2018-09-13 - Jorge Agra - BUG#399 record_type of RS not validated          */
/* 2018-09-14 - Jorge Agra - BUG#401/403/404/406 Calc real availability not   */
/*                           considering rows with ready_for_export <> 'N'    */
/* 2018-09-14 - Jorge Agra - BUG#405 Missing validations for RMS_STATUS and   */
/*                           ASSORTMENT_TYPE                                  */
/* 2018-09-17 - Jorge Agra - BUG#416 Invalid number in GET_ILOC_CFAS          */
/* 2018-09-28 - Jorge Agra - ORACR 00327 Items are ranged in preproc if valid */
/*                         - BUG#500 O1C/Items/RB55a - Regular assortment -   */
/*                           End of assortment 2 (Permout)                    */
/* 2018-10-28 - Jorge Agra - BUG#617 Record 'D' in range size integrated      */
/* 2018-11-19 - Jorge Agra - BUG#676 delete uda of sales end date when null   */
/* 2018-11-19 - Jorge Agra - BUG#678 handle of dup vals in stg assortment mode*/
/* 2018-11-21 - Jorge Agra - BUG#692 sales start date can be in past          */
/* 2018-11-28 - Jorge Agra - Group by in PROCESS_RS                           */
/* 2018-12-12 - Jorge Agra - BUG#738 UDA date create on GTIN/EAN level        */
/* 2019-01-18 - Pedro Miranda - CR00390 Code 48                               */
/******************************************************************************/


GP_trace          boolean := false;
GP_trace_what     varchar2(500) := '';

--------------------------------------------------------------------------------
--Function Name : VALIDATE_xx
--Purpose       : Validate each one of the staging tables, markink records with
--                Y - if conditions for integration are met
--                E - if some error occurs
--------------------------------------------------------------------------------
FUNCTION VALIDATE_LOC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN        NUMBER default null,
                     I_dept             IN        ITEM_MASTER.DEPT%TYPE default null)
return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SD(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN        NUMBER default null,
                     I_dept             IN        ITEM_MASTER.DEPT%TYPE default null)
return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_AM(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN        NUMBER default null,
                     I_dept             IN        ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_RS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN        NUMBER default null,
                     I_dept             IN        ITEM_MASTER.DEPT%TYPE default null)
return BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : PREPROCESS_ALL
--Purpose       : Validate all staging tables
--------------------------------------------------------------------------------
FUNCTION PREPROCESS_ALL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_bulk_size        IN     NUMBER default null,
                        I_dept             IN      ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : PROCESS_ALL
--Purpose       : Main process.
--                  Execute ITEM Level assortments (UDAs):
--                  - Sales dates (start and end)
--                  - Assortment mode
--                  - Range size
--                  And ITEM_LOC level assortments (CFAs)
--                  - Visibility
--                  - Visibility date
--                  - Availability
--                  - Availability Date
--------------------------------------------------------------------------------
FUNCTION PROCESS_ALL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN     NUMBER default null,
                     I_dept             IN      ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : PROCESS_SD
--Purpose       : Execute ITEM Level assortments (UDAs):
--                  - Sales dates (start and end)
--------------------------------------------------------------------------------
FUNCTION PROCESS_SD(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_bulk_size        IN     NUMBER default null,
                    I_dept             IN      ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : PROCESS_AM
--Purpose       : Execute ITEM Level assortments (UDAs):
--                  - Assortment Mode
--------------------------------------------------------------------------------
FUNCTION PROCESS_AM(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN     NUMBER default null,
                     I_dept             IN      ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : PROCESS_RS
--Purpose       : Execute ITEM Level assortments (UDAs):
--                  - Range Size
--------------------------------------------------------------------------------
FUNCTION PROCESS_RS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN     NUMBER default null,
                     I_dept             IN      ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : PROCESS_LOC
--Purpose       : Execute ITEM Loc level ranging and assortments (CFAs):
--                  - Visibility
--                  - Visibility date
--                  - Availability
--                  - Availability Date
--------------------------------------------------------------------------------
FUNCTION PROCESS_LOC(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN     NUMBER default null,
                     I_dept             IN      ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : PURGE
--Purpose       : Purge processed and error (if I_purge_errors = Y)
--                records from interface tables
--------------------------------------------------------------------------------
FUNCTION PURGE(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_date              IN      DATE DEFAULT get_vdate,
                            I_purge_errors      IN      VARCHAR2 DEFAULT 'N',
                            I_retention_days    IN      NUMBER DEFAULT 7
                            )
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END XXADEO_ITEM_ASSORTMENT_SQL;



/


