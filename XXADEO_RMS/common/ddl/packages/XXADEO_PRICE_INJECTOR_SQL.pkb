CREATE OR REPLACE PACKAGE BODY XXADEO_PRICE_INJECTOR_SQL AS

L_default_request_size NUMBER := 25000;
LP_custom_options      XXADEO_RPM_CUSTOM_OPTIONS%ROWTYPE;
LP_vdate               DATE   := get_vdate;
LP_new_status       CONSTANT VARCHAR2(1) := 'N';
LP_worksheet_status CONSTANT VARCHAR2(1) := 'W';
LP_approved_status  CONSTANT VARCHAR2(1) := 'A';
LP_error_status     CONSTANT VARCHAR2(1) := 'E';
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_PRICE_CHANGE_STATUS(O_error_message         OUT VARCHAR2,
                                   O_status                OUT XXADEO_RPM_STAGE_PRICE_CHANGE.STATUS%TYPE,
                                   O_price_change_id       OUT XXADEO_RPM_STAGE_PRICE_CHANGE.PRICE_CHANGE_ID%TYPE,
                                   IO_rpm_price_dtl_rec IN OUT RPM_PRICE_DTL_REC)
RETURN BOOLEAN IS
  --
  L_program VARCHAR2(255) := 'XXADEO_PRICE_INJECTOR_SQL.CHECK_PRICE_CHANGE_STATUS';
  --
  cursor C_get_price_from_rpm is
    select price_change_id,
           decode(rpc.state, RPM_CONSTANTS.PC_APPROVED_STATE_CODE, 'A',
                             RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE, 'W') status
      from rpm_price_change rpc
     where rpc.item               = IO_rpm_price_dtl_rec.item
       and rpc.reason_code        = IO_rpm_price_dtl_rec.reason_code
       and nvl(rpc.location,-999) = nvl(IO_rpm_price_dtl_rec.location,-999)
       and nvl(rpc.zone_id,-999)  = nvl(IO_rpm_price_dtl_rec.zone_id,-999)
       and rpc.effective_date     = IO_rpm_price_dtl_rec.effective_date;
  --
BEGIN
  --
  open C_get_price_from_rpm;
  fetch C_get_price_from_rpm into O_price_change_id,
                                  O_status;
    --
    if C_get_price_from_rpm%NOTFOUND then
      --
      O_price_change_id := null;
      O_status          := 'N';
      --
    end if;
    --
  close C_get_price_from_rpm;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    if C_get_price_from_rpm%ISOPEN then
      --
      close C_get_price_from_rpm;
      --
    end if;
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END CHECK_PRICE_CHANGE_STATUS;
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_PRICE_DEPENDENCY(O_error_message         OUT VARCHAR2,
                                I_stage_price_change_id IN  XXADEO_RPM_STAGE_PRICE_CHANGE.STAGE_PRICE_CHANGE_ID%TYPE,
                                I_xxadeo_process_id     IN  XXADEO_RPM_STAGE_PRICE_CHANGE.XXADEO_PROCESS_ID%TYPE)
RETURN BOOLEAN IS
  --
  L_program VARCHAR2(255) := 'XXADEO_PRICE_INJECTOR_SQL.CHECK_PRICE_DEPENDENCY';
  --
  cursor C_get_price_dependency is
    select xrspc.price_change_id,
           xpr.request_process_id,
           xpr.process_order,
           xpr.pe_process_id
      from xxadeo_price_request xpr,
           xxadeo_rpm_stage_price_change xrspc
     where xpr.dependency_type = 0
       and xpr.dependency is not null
       and xrspc.stage_price_change_id = I_stage_price_change_id
       and xrspc.xxadeo_process_id     = I_xxadeo_process_id
       and xrspc.stage_price_change_id = xpr.dependency;
  --
BEGIN
  --
  for rec in C_get_price_dependency loop
    --
    update xxadeo_price_request xpr
       set xpr.dependency = rec.price_change_id,
           xpr.dependency_type = 0
     where xpr.request_process_id = rec.request_process_id
       and xpr.process_order      = rec.process_order
       and xpr.pe_process_id      = rec.pe_process_id;
    --
  end loop;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END CHECK_PRICE_DEPENDENCY;
-----------------------------------------------------------------------------------------------
FUNCTION GET_PRICE_CHANGE_EVENTS(O_error_message      IN OUT        LOGGER_LOGS.TEXT%TYPE,
                                 O_price_events_tbl      OUT NOCOPY XXADEO_PRICE_EVENTS_TBL,
                                 I_price_event_type   IN            XXADEO_PRICE_REQUEST.TYPE%TYPE)
RETURN BOOLEAN IS
  --
  L_program VARCHAR2(255) := 'XXADEO_PRICE_INJECTOR_SQL.GET_PRICE_CHANGE_EVENTS';
  --
  cursor C_get_emergency_pc is
    select new XXADEO_PRICE_EVENTS_OBJ(tbl.xxadeo_process_id,
                                       tbl.stage_price_change_id,
                                       tbl.reason_code,
                                       tbl.item,
                                       tbl.diff_id,
                                       tbl.zone_id,
                                       tbl.location,
                                       tbl.zone_node_type,
                                       tbl.link_code,
                                       tbl.effective_date,
                                       tbl.change_type,
                                       tbl.change_amount,
                                       tbl.change_currency,
                                       tbl.change_percent,
                                       tbl.change_selling_uom,
                                       tbl.null_multi_ind,
                                       tbl.multi_units,
                                       tbl.multi_unit_retail,
                                       tbl.multi_selling_uom,
                                       tbl.price_guide_id,
                                       tbl.ignore_constraints,
                                       tbl.auto_approve_ind,
                                       tbl.status,
                                       tbl.error_message,
                                       tbl.process_id,
                                       tbl.price_change_id,
                                       tbl.price_change_display_id,
                                       tbl.skulist,
                                       tbl.thread_num,
                                       tbl.exclusion_created,
                                       tbl.vendor_funded_ind,
                                       tbl.funding_type,
                                       tbl.funding_amount,
                                       tbl.funding_amount_currency,
                                       tbl.funding_percent,
                                       tbl.deal_id,
                                       tbl.deal_detail_id,
                                       tbl.zone_group_id,
                                       tbl.stage_cust_attr_id,
                                       tbl.cust_attr_id,
                                       tbl.processed_date,
                                       tbl.create_id,
                                       tbl.create_datetime,
                                       tbl.line_item_loc_accumulator)
      from (select p.*,
                   null line_item_loc_accumulator
              from (select *
                      from xxadeo_rpm_stage_price_change
                     where status in ('N','U','D'))p
            order by item,
                     zone_id,
                     location,
                     case
                       when trunc(p.effective_date) <= trunc(get_vdate +1) then
                         p.effective_date
                       else
                         null
                       end asc) tbl
     where trunc(tbl.effective_date) <= trunc(get_vdate +1);
  --
  cursor C_get_regular_pc is
    select new XXADEO_PRICE_EVENTS_OBJ(tbl.xxadeo_process_id,
                                       tbl.stage_price_change_id,
                                       tbl.reason_code,
                                       tbl.item,
                                       tbl.diff_id,
                                       tbl.zone_id,
                                       tbl.location,
                                       tbl.zone_node_type,
                                       tbl.link_code,
                                       tbl.effective_date,
                                       tbl.change_type,
                                       tbl.change_amount,
                                       tbl.change_currency,
                                       tbl.change_percent,
                                       tbl.change_selling_uom,
                                       tbl.null_multi_ind,
                                       tbl.multi_units,
                                       tbl.multi_unit_retail,
                                       tbl.multi_selling_uom,
                                       tbl.price_guide_id,
                                       tbl.ignore_constraints,
                                       tbl.auto_approve_ind,
                                       tbl.status,
                                       tbl.error_message,
                                       tbl.process_id,
                                       tbl.price_change_id,
                                       tbl.price_change_display_id,
                                       tbl.skulist,
                                       tbl.thread_num,
                                       tbl.exclusion_created,
                                       tbl.vendor_funded_ind,
                                       tbl.funding_type,
                                       tbl.funding_amount,
                                       tbl.funding_amount_currency,
                                       tbl.funding_percent,
                                       tbl.deal_id,
                                       tbl.deal_detail_id,
                                       tbl.zone_group_id,
                                       tbl.stage_cust_attr_id,
                                       tbl.cust_attr_id,
                                       tbl.processed_date,
                                       tbl.create_id,
                                       tbl.create_datetime,
                                       tbl.line_item_loc_accumulator)
      from (select p.*,
                   null line_item_loc_accumulator
              from (select *
                      from xxadeo_rpm_stage_price_change
                     where status in ('N','U','D'))p
            order by item,
                     zone_id,
                     location,
                     case
                       when trunc(p.effective_date) <= trunc(get_vdate +1) then
                         p.effective_date
                       else
                         null
                       end asc) tbl
     where trunc(tbl.effective_date) > trunc(get_vdate);
  --
  cursor C_get_price_events is
    select new XXADEO_PRICE_EVENTS_OBJ(tbl.xxadeo_process_id,
                                       tbl.stage_price_change_id,
                                       tbl.reason_code,
                                       tbl.item,
                                       tbl.diff_id,
                                       tbl.zone_id,
                                       tbl.location,
                                       tbl.zone_node_type,
                                       tbl.link_code,
                                       tbl.effective_date,
                                       tbl.change_type,
                                       tbl.change_amount,
                                       tbl.change_currency,
                                       tbl.change_percent,
                                       tbl.change_selling_uom,
                                       tbl.null_multi_ind,
                                       tbl.multi_units,
                                       tbl.multi_unit_retail,
                                       tbl.multi_selling_uom,
                                       tbl.price_guide_id,
                                       tbl.ignore_constraints,
                                       tbl.auto_approve_ind,
                                       tbl.status,
                                       tbl.error_message,
                                       tbl.process_id,
                                       tbl.price_change_id,
                                       tbl.price_change_display_id,
                                       tbl.skulist,
                                       tbl.thread_num,
                                       tbl.exclusion_created,
                                       tbl.vendor_funded_ind,
                                       tbl.funding_type,
                                       tbl.funding_amount,
                                       tbl.funding_amount_currency,
                                       tbl.funding_percent,
                                       tbl.deal_id,
                                       tbl.deal_detail_id,
                                       tbl.zone_group_id,
                                       tbl.stage_cust_attr_id,
                                       tbl.cust_attr_id,
                                       tbl.processed_date,
                                       tbl.create_id,
                                       tbl.create_datetime,
                                       tbl.line_item_loc_accumulator)
      from (select p.*,
                   null line_item_loc_accumulator
              from (select *
                      from xxadeo_rpm_stage_price_change
                     where status in ('N','U','D'))p
            order by item,
                     zone_id,
                     location,
                     case
                       when trunc(p.effective_date) <= trunc(get_vdate +1) then
                         p.effective_date
                       else
                         null
                       end asc) tbl;
  --
begin
  --
  if I_price_event_type = GP_emergency_pc then
    --
    open C_get_emergency_pc;
    fetch C_get_emergency_pc bulk collect into O_price_events_tbl;
    close C_get_emergency_pc;
    --
  elsif I_price_event_type = GP_regular_pc then
    --
    open C_get_regular_pc;
    fetch C_get_regular_pc bulk collect into O_price_events_tbl;
    close C_get_regular_pc;
    --
  else
    --
    open C_get_price_events;
    fetch C_get_price_events bulk collect into O_price_events_tbl;
    close C_get_price_events;
    --
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END GET_PRICE_CHANGE_EVENTS;
------------------------------------------------------------------------------------------------
FUNCTION GET_PROCESSING_REQUESTS(O_error_message            IN OUT        LOGGER_LOGS.TEXT%TYPE,
                                 O_xxadeo_price_request_tbl    OUT NOCOPY XXADEO_PRICE_REQUEST_TBL,
                                 O_request_type                OUT        XXADEO_PRICE_REQUEST.TYPE%TYPE,
                                 O_request_action              OUT        XXADEO_PRICE_REQUEST.ACTION%TYPE)
RETURN BOOLEAN IS
PRAGMA AUTONOMOUS_TRANSACTION;
  --
  L_program     VARCHAR2(255) := 'XXADEO_PRICE_INJECTOR_SQL.GET_PROCESSING_REQUESTS';
  --
  NO_REQUESTS   EXCEPTION;
  --
  L_lock        VARCHAR(1);
  L_pc_rejected XXADEO_PC_REJECTED_TBL := new XXADEO_PC_REJECTED_TBL();
  --
  cursor C_get_pc_rejected is
    select new XXADEO_PC_REJECTED_OBJ(process_id                 => process_id,
                                      price_change_id            => price_change_id,
                                      price_change_display_id    => price_change_display_id,
                                      change_type                => change_type,
                                      change_amount              => change_amount,
                                      change_currency            => change_currency,
                                      change_percent             => change_percent,
                                      selling_uom                => selling_uom,
                                      multi_units                => multi_units,
                                      multi_unit_retail          => multi_unit_retail,
                                      multi_unit_retail_currency => multi_unit_retail_currency,
                                      multi_selling_uom          => multi_selling_uom,
                                      effective_date             => effective_date,
                                      item                       => item,
                                      dept                       => dept,
                                      class                      => class,
                                      subclass                   => subclass,
                                      location                   => location,
                                      zone_id                    => zone_id,
                                      zone_node_type             => zone_node_type,
                                      error_message              => error_message,
                                      action                     => action,
                                      status                     => status,
                                      user_id                    => user_id,
                                      create_datetime            => create_datetime,
                                      source                     => source)
      from (select xrspc.xxadeo_process_id process_id,
                   xrspc.stage_price_change_id price_change_id,
                   xrspc.price_change_display_id,
                   xrspc.change_type,
                   xrspc.change_amount,
                   xrspc.change_currency,
                   xrspc.change_percent,
                   xrspc.change_selling_uom selling_uom,
                   xrspc.multi_units,
                   xrspc.multi_unit_retail,
                   null multi_unit_retail_currency,
                   xrspc.multi_selling_uom,
                   xrspc.effective_date,
                   xrspc.item,
                   i.dept,
                   i.class,
                   i.subclass,
                   nvl(xrspc.location,xrspc.zone_id) location,
                   xrspc.zone_id zone_id,
                   xrspc.zone_node_type,
                   xrspc.error_message,
                   tbl.action,
                   xrspc.status,
                   xrspc.create_id user_id,
                   xrspc.create_datetime,
                   'STG' source
              from xxadeo_rpm_stage_price_change xrspc,
                   xxadeo_price_request tbl,
                   item_master i
             where xrspc.status           in ('R','E')
               and tbl.action             <> 'PE'
               and i.item(+)               = xrspc.item
               and xrspc.xxadeo_process_id = tbl.request_process_id
               and tbl.status             in ('R','E')
               and exists (select code_id
                             from rpm_codes rc
                            where rc.code_id = xrspc.reason_code
                              and exists (select 1
                                            from code_detail cd
                                           where code_type    = XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_TYPE
                                             and code        in (XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_EM_ST_PORTAL, XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_ST_PORTAL)
                                             and cd.code_desc = rc.code)));
  --
  -- get requests to process
  --
  cursor C_get_requests is
    with first_event as (select *
                           from (select *
                                   from xxadeo_price_request xpr
                                  where xpr.status in ('W','A')
                                 order by pe_process_id asc, process_order asc, request_process_id asc)
                          where rownum = 1)
    select new XXADEO_PRICE_REQUEST_OBJ(request_process_id,
                                        type,
                                        action,
                                        process_order,
                                        dependency,
                                        pe_process_id)
      from (select request_process_id,
                   type,
                   status action,
                   process_order,
                   dependency,
                   pe_process_id,
                   sum(request_size) over (order by pe_process_id asc, process_order asc, request_process_id asc) line_accumulator
              from (select request_process_id,
                           decode(type,GP_regular_pc,'PC',GP_emergency_pc,'PC','PC') type,
                           status,
                           request_size,
                           process_order,
                           dependency,
                           pe_process_id
                      from first_event
                    union all
                    select pr.request_process_id,
                           decode(pr.type,GP_regular_pc,'PC',GP_emergency_pc,'PC','PC') type,
                           pr.status,
                           pr.request_size,
                           pr.process_order,
                           pr.dependency,
                           pr.pe_process_id
                      from xxadeo_price_request pr,
                           first_event fe
                     where pr.status              in ('W','A')
                       and pr.dependency          is null
                       and pr.request_process_id != fe.request_process_id
                       and pr.type                = fe.type
                       and pr.status              = fe.status)
            order by pe_process_id asc, process_order asc, request_process_id asc)
     where line_accumulator <= nvl(LP_custom_options.pc_process_max_request_size,L_default_request_size);
  --
  cursor C_lock_requests is
    select 'Y'
      from xxadeo_price_request xpr
     where exists (select 1
                     from table(O_xxadeo_price_request_tbl) tbl
                    where xpr.request_process_id = tbl.request_process_id) for update nowait;
  --
BEGIN
  --
  open C_get_pc_rejected;
  fetch C_get_pc_rejected bulk collect into L_pc_rejected;
  close C_get_pc_rejected;
  --
  for i in 1..L_pc_rejected.count loop
    --
    -- sent notification of conflict error for the Store Portal
    --
    if XXADEO_NOTIFICATIONS_SQL.CREATE_RAF_NOTIFICATION(O_error_message           => O_error_message,
                                                        I_application_code        => 'StorePortalApp',
                                                        I_notification_type       => 'Price Change Rejected',
                                                        I_notification_desc       => 'Price change id ' ||L_pc_rejected(i).price_change_display_id || ' rejected for the Item '||L_pc_rejected(i).item||', location '||L_pc_rejected(i).location||', loc_type '||L_pc_rejected(i).zone_node_type||': '|| L_pc_rejected(i).error_message || '.',
                                                        I_notification_context    => 'title=List of rejected price changes (Conflict Checking)|url=/WEB-INF/com/adeo/merchandising/storeportal/view/price/rejected/flow/RejectedPriceChangeFlow.xml#RejectedPriceChangeFlow', --NOTIFICATION_CONTEXT,
                                                        I_user                    => L_pc_rejected(i).user_id,
                                                        I_errors_count            => 1) = FALSE then

      --
      RETURN FALSE;
      --
    end if;
    --
    update xxadeo_price_request
       set action = 'PE'
     where request_process_id = L_pc_rejected(i).process_id
       and status            in ('R','E')
       and action            <> 'PE';
    --
  end loop;
  --
  --
  -- get requests to process
  --
  open C_get_requests;
  fetch C_get_requests bulk collect into O_xxadeo_price_request_tbl;
  close C_get_requests;
  --
  -- get requests type and action
  --
  if O_xxadeo_price_request_tbl.count > 0 then
    --
    O_request_type   := O_xxadeo_price_request_tbl(1).type;
    O_request_action := O_xxadeo_price_request_tbl(1).action;
    --
    -- lock requests
    --
    open C_lock_requests;
    fetch C_lock_requests into L_lock;
    close C_lock_requests;
    --
    -- mark requests as processing
    --
    update xxadeo_price_request xpr
       set status = 'P'
     where exists (select 1
                     from table(O_xxadeo_price_request_tbl) tbl
                    where xpr.request_process_id = tbl.request_process_id
                      and xpr.process_order      = tbl.process_order
                      and xpr.pe_process_id      = tbl.pe_process_id);
    --
  end if;
  --
  COMMIT;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when NO_REQUESTS then
    --
    O_error_message := 'There are no requests to Price changes processing.';
    --
    RETURN FALSE;
    --
  when OTHERS then
    --
    if C_get_requests%ISOPEN then
      --
      close C_get_requests;
      --
    end if;
    --
    if C_lock_requests%ISOPEN then
      --
      close C_lock_requests;
      --
    end if;
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END GET_PROCESSING_REQUESTS;
------------------------------------------------------------------------------------------------
FUNCTION INJECT_PRICE_CHANGES(O_error_message            IN OUT  LOGGER_LOGS.TEXT%TYPE,
                              I_xxadeo_price_request_tbl IN      XXADEO_PRICE_REQUEST_TBL,
                              I_price_event_action       IN      XXADEO_PRICE_REQUEST.ACTION%TYPE)
RETURN BOOLEAN IS
  --
  L_program                   VARCHAR2(255) := 'XXADEO_PRICE_INJECTOR_SQL.INJECT_PRICE_CHANGES';
  --
  L_stage_pc_id               XXADEO_RPM_STAGE_PRICE_CHANGE.STAGE_PRICE_CHANGE_ID%TYPE;
  L_process_id                XXADEO_RPM_STAGE_PRICE_CHANGE.XXADEO_PROCESS_ID%TYPE;
  L_dependency                XXADEO_PRICE_REQUEST.DEPENDENCY%TYPE;
  L_process_order             XXADEO_PRICE_REQUEST.PROCESS_ORDER%TYPE;
  L_pe_process_id             XXADEO_PRICE_REQUEST.PE_PROCESS_ID%TYPE;
  --
  cursor C_check_pc_id is
    select xrpcs.xxadeo_process_id,
           xrpcs.stage_price_change_id,
           xrpcs.price_change_id ,
           tbl.pe_process_id,
           tbl.process_order
      from xxadeo_rpm_stage_price_change xrpcs,
           table(I_xxadeo_price_request_tbl) tbl
     where xrpcs.price_change_id  is null
       and xrpcs.xxadeo_process_id = tbl.request_process_id
       and xrpcs.status = 'P'
    order by tbl.process_order asc;
  --
  cursor C_check_rpm_pc_id is
    select xrpcs.xxadeo_process_id,
           xrpcs.stage_price_change_id,
           xrpcs.price_change_id ,
           tbl.pe_process_id,
           tbl.process_order
      from xxadeo_rpm_stage_price_change xrpcs,
           table(I_xxadeo_price_request_tbl) tbl
     where xrpcs.price_change_id  is not null
       and xrpcs.xxadeo_process_id = tbl.request_process_id
       and xrpcs.status = 'P'
    order by tbl.process_order asc;
  --
  cursor C_check_dependency is
    select (case
             when xpr.dependency_type = 1 then
              (select price_change_id
                 from xxadeo_rpm_stage_price_change tbl
                where tbl.stage_price_change_id = xpr.dependency)
             else
              xpr.dependency
           end) price_change_id
      from xxadeo_rpm_stage_price_change xrspc,
           xxadeo_price_request xpr
     where xrspc.xxadeo_process_id     = xpr.request_process_id
       and xpr.dependency             is not null
       and xrspc.xxadeo_process_id     = L_process_id
       and xrspc.stage_price_change_id = L_stage_pc_id
       and xpr.pe_process_id           = L_pe_process_id
       and xpr.process_order           = L_process_order;
  --
  --
BEGIN
  --
  if I_price_event_action = 'W' then
    --
    for rpm_pc in C_check_rpm_pc_id loop
      --
      for stg_pc in (select xrpcs.xxadeo_process_id,
                            xrpcs.stage_price_change_id,
                            xrpcs.price_change_id,
                            case
                               when lower(pc.state) = 'pricechange.state.approved' then 'A'
                               when lower(pc.state) = 'pricechange.state.worksheet' then 'W'
                               else null
                            end status,
                            xrpcs.change_amount,
                            xrpcs.change_currency,
                            xrpcs.effective_date,
                            xrpcs.create_id,
                            xrpcs.create_datetime
                        from xxadeo_rpm_stage_price_change xrpcs,
                             rpm_price_change pc,
                             table(I_xxadeo_price_request_tbl) tbl
                       where xrpcs.price_change_id       = pc.price_change_id
                         and xrpcs.xxadeo_process_id     = tbl.request_process_id
                         and tbl.process_order           = rpm_pc.process_order
                         and xrpcs.xxadeo_process_id     = rpm_pc.xxadeo_process_id
                         and xrpcs.stage_price_change_id = rpm_pc.stage_price_change_id) loop
        --
        update xxadeo_rpm_stage_price_change xrpcs
           set xrpcs.status = stg_pc.status
         where xrpcs.xxadeo_process_id     = stg_pc.xxadeo_process_id
           and xrpcs.stage_price_change_id = stg_pc.stage_price_change_id
           and xrpcs.price_change_id       = stg_pc.price_change_id;
        --
        update rpm_price_change rpc
           set rpc.change_amount   = stg_pc.change_amount,
               rpc.effective_date  = stg_pc.effective_date
         where rpc.price_change_id = stg_pc.price_change_id
           and rpc.state           = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE;
        --
      end loop;
      --
    end loop;
    --
    for pc in C_check_pc_id loop
      --
      L_dependency    := NULL;
      --
      update xxadeo_rpm_stage_price_change xrpcs
         set xrpcs.price_change_id         = rpm_price_change_seq.nextval,
             xrpcs.price_change_display_id = rpm_price_change_display_seq.nextval,
             xrpcs.status                  = 'W'
       where xrpcs.xxadeo_process_id       = pc.xxadeo_process_id
         and xrpcs.stage_price_change_id   = pc.stage_price_change_id;
      --
      L_process_id    := pc.xxadeo_process_id;
      L_stage_pc_id   := pc.stage_price_change_id;
      L_pe_process_id := pc.pe_process_id;
      L_process_order := pc.process_order;
      --
      if check_price_dependency(O_error_message         => O_error_message,
                                I_stage_price_change_id => L_stage_pc_id,
                                I_xxadeo_process_id     => L_process_id) = FALSE then
        --
        RETURN FALSE;
        --
      end if;
      --
      open C_check_dependency;
      fetch C_check_dependency into L_dependency;
      close C_check_dependency;
      --
      update xxadeo_price_request xpr
         set xpr.dependency         = L_dependency
       where xpr.request_process_id = pc.xxadeo_process_id
         and xpr.process_order      = pc.process_order
         and xpr.pe_process_id      = pc.pe_process_id;
      --

      insert into rpm_price_change
        (select price_change_id,
                price_change_display_id,
                'pricechange.state.worksheet',
                reason_code,
                (case
                  when tbl.dependency is NULL then
                    NULL
                  else
                    (select dependency
                       from xxadeo_price_request xpr
                      where xpr.request_process_id = tbl.request_process_id
                        and xpr.process_order      = tbl.process_order
                        and xpr.pe_process_id      = tbl.pe_process_id)
                end) exception_parent,
                item,
                diff_id,
                zone_id,
                location,
                zone_node_type,
                link_code,
                effective_date,
                change_type,
                change_amount,
                case
                   when change_type IN (RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE,
                                        RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE) then
                        locs.currency_code
                   else
                        NULL
                end,--change_currency,
                change_percent,
                change_selling_uom,
                null_multi_ind,
                null,
                null,
                null,
                multi_selling_uom,
                price_guide_id,
                vendor_funded_ind,
                funding_type,
                funding_amount,
                case
                   when funding_type = RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE then
                        locs.currency_code
                   else
                        NULL
                end funding_amount_currency,--funding_amount_currency,
                funding_percent,
                deal_id,
                deal_detail_id,
                sysdate,
                nvl(xrspc.create_id, get_user),
                null,
                nvl(xrspc.create_id,get_user),
                null,
                ignore_constraints,
                null,
                skulist,
                0,
                null,
                cust_attr_id
           from xxadeo_rpm_stage_price_change xrspc,
                table(I_xxadeo_price_request_tbl) tbl,
                (select store loc,
                        currency_code,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE loc_type
                   from store
                 union all
                 select wh loc,
                        currency_code,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE loc_type
                   from wh
                 union all
                 select zone_id loc,
                        currency_code,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE loc_type
                   from rpm_zone) locs
          where xrspc.xxadeo_process_id      = tbl.request_process_id
            and xrspc.zone_node_type         = locs.loc_type
            and nvl(xrspc.location, zone_id) = locs.loc
            and xrspc.xxadeo_process_id      = pc.xxadeo_process_id
            and xrspc.stage_price_change_id  = pc.stage_price_change_id
            and tbl.process_order            = pc.process_order);
      --
    end loop;
    --
  else
    --
    for i in 1..I_xxadeo_price_request_tbl.Last loop
      --
      for stg_pc in (select xrpcs.xxadeo_process_id,
                            xrpcs.stage_price_change_id,
                            xrpcs.price_change_id,
                            case
                               when lower(pc.state) = 'pricechange.state.approved' then 'A'
                               when lower(pc.state) = 'pricechange.state.worksheet' then 'W'
                               else null
                            end status,
                            xrpcs.change_amount,
                            xrpcs.change_currency,
                            xrpcs.effective_date,
                            xrpcs.create_id,
                            xrpcs.create_datetime
                        from xxadeo_rpm_stage_price_change xrpcs,
                             rpm_price_change pc
                       where xrpcs.price_change_id   = pc.price_change_id
                         and xrpcs.xxadeo_process_id = I_xxadeo_price_request_tbl(i).request_process_id) loop
        --
        update xxadeo_rpm_stage_price_change xrpcs
           set xrpcs.status = stg_pc.status
         where xrpcs.xxadeo_process_id     = stg_pc.xxadeo_process_id
           and xrpcs.price_change_id       = stg_pc.price_change_id
           and xrpcs.stage_price_change_id = stg_pc.stage_price_change_id;
        --
        update rpm_price_change rpc
           set rpc.change_amount   = stg_pc.change_amount,
               rpc.effective_date  = stg_pc.effective_date
         where rpc.price_change_id = stg_pc.price_change_id
           and rpc.state           = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE;
        --
      end loop;
      --
    end loop;
    --
  end if;
  --
  delete
    from rpm_stage_price_change rspc
   where exists (select 1
                   from table(I_xxadeo_price_request_tbl) tbl,
                        xxadeo_rpm_stage_price_change     xrspc
                     where tbl.request_process_id      = xrspc.xxadeo_process_id
                       and xrspc.stage_price_change_id = rspc.stage_price_change_id);

  --
  insert
    into rpm_stage_price_change
    (select stage_price_change_id,
            reason_code,
            item,
            diff_id,
            zone_id,
            location,
            zone_node_type,
            link_code,
            effective_date,
            change_type,
            change_amount,
            change_currency,
            change_percent,
            change_selling_uom,
            null_multi_ind,
            multi_units,
            multi_unit_retail,
            multi_selling_uom,
            price_guide_id,
            ignore_constraints,
            auto_approve_ind,
            status,
            null,
            process_id,
            price_change_id,
            price_change_display_id,
            skulist,
            thread_num,
            exclusion_created,
            vendor_funded_ind,
            funding_type,
            funding_amount,
            funding_amount_currency,
            funding_percent,
            deal_id,
            deal_detail_id,
            zone_group_id,
            stage_cust_attr_id,
            cust_attr_id
       from xxadeo_rpm_stage_price_change xrspc
      where exists (select 1
                      from table(I_xxadeo_price_request_tbl) tbl
                     where xrspc.xxadeo_process_id = tbl.request_process_id));
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END INJECT_PRICE_CHANGES;
------------------------------------------------------------------------------------------------
FUNCTION RUN_BATCH_PRE_INJECTOR(O_error_message       IN OUT LOGGER_LOGS.TEXT%TYPE,
                                IO_price_event_type   IN OUT XXADEO_PRICE_REQUEST.TYPE%TYPE,
                                IO_price_event_action IN OUT XXADEO_PRICE_REQUEST.ACTION%TYPE)
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(255) := 'XXADEO_PRICE_INJECTOR_SQL.RUN_BATCH_PRE_INJECTOR';
  --
  INVALID_ACTION_TYPE EXCEPTION;
  NO_PRICE_CHANGE     EXCEPTION;
  --
  L_request_type              XXADEO_PRICE_REQUEST.TYPE%TYPE;
  L_request_action            XXADEO_PRICE_REQUEST.ACTION%TYPE;
  L_xxadeo_price_request_tbl  XXADEO_PRICE_REQUEST_TBL;
  L_xxadeo_price_events_tbl   XXADEO_PRICE_EVENTS_TBL := NEW XXADEO_PRICE_EVENTS_TBL();
  L_con_check_err_id          NUMBER(15) := NULL;
  L_request_size              XXADEO_PRICE_REQUEST.REQUEST_SIZE%TYPE := NULL;
  --
  cursor C_get_custom_options is
    select *
      from xxadeo_rpm_custom_options;
  --
  cursor C_get_price_in_process is
    select xrspc.xxadeo_process_id,
           xrspc.item,
           xrspc.zone_id
      from xxadeo_rpm_stage_price_change xrspc
     where xrspc.status = 'P'
       and not exists (select 1
                         from xxadeo_price_request xpr
                        where xpr.request_process_id = xrspc.xxadeo_process_id
                          and xpr.status             = 'P'
                          and xpr.request_size       is not null);
  --
  cursor C_update_price_request(V_process_id XXADEO_RPM_STAGE_PRICE_CHANGE.XXADEO_PROCESS_ID%TYPE,
                                V_item       XXADEO_RPM_STAGE_PRICE_CHANGE.ITEM%TYPE,
                                V_zone_id    XXADEO_RPM_STAGE_PRICE_CHANGE.ZONE_ID%TYPE) is
    select ((select case
                      when item_level = tran_level then
                         1
                      when item_level < tran_level then
                       (select count(1)
                          from item_master
                         where item_parent = V_item)
                    end count_items
               from (select item_level,
                            tran_level
                       from item_master
                      where item        = V_item
                        and item_level <= tran_level)) * (select case
                                                                   when V_zone_id is null then
                                                                     1
                                                                   else
                                                                     (select decode(count(1),0,1,count(1))
                                                                        from rpm_zone_location
                                                                       where zone_id = V_zone_id)
                                                                   end count_locations
                                                            from dual)) request_size
              from xxadeo_price_request xpr
            where xpr.request_process_id = V_process_id for update nowait;
  --
BEGIN
  --
  LP_custom_options := NULL;
  --
  open C_get_custom_options;
  fetch C_get_custom_options into LP_custom_options;
  close C_get_custom_options;
  --
  if IO_price_event_type IN (GP_emergency_pc,GP_regular_pc, 'PC') then
    --
    if GET_PRICE_CHANGE_EVENTS(O_error_message      => O_error_message,
                               O_price_events_tbl   => L_xxadeo_price_events_tbl,
                               I_price_event_type   => IO_price_event_type) = FALSE then
      --
      RETURN FALSE;
      --
    end if;
    --
  elsif IO_price_event_type = 'SP' then
    --
    -- to do
    RETURN TRUE;
    --
  elsif IO_price_event_type = 'CL' then
    --
    -- to do
    RETURN TRUE;
    --
  end if;
  --
  if L_xxadeo_price_events_tbl.count > 0 or L_xxadeo_price_events_tbl is NULL then
    --
    if XXADEO_PRICE_CHANGE_SQL.RUN_PROCESS_PRICE_CHANGE(O_error_message            => O_error_message,
                                                        I_xxadeo_price_events_tbl => L_xxadeo_price_events_tbl) = FALSE then
      --
      RETURN FALSE;
      --
    END IF;
    --
    --
    for rec in C_get_price_in_process loop
      --
      L_request_size := NULL;
      --
      open C_update_price_request(rec.xxadeo_process_id,
                                  rec.item,
                                  rec.zone_id);
      fetch C_update_price_request into L_request_size;
      close C_update_price_request;
      --
      update xxadeo_price_request rprs
         set request_size       = L_request_size
       where request_process_id = rec.xxadeo_process_id
         and exists (select 1
                       from xxadeo_price_request xpr
                      where xpr.request_process_id = rprs.request_process_id
                        and xpr.process_order      = rprs.process_order
                        and xpr.pe_process_id      = rprs.pe_process_id
                        and xpr.status             = 'P');
      --
      commit;
      --
    end loop;
    --
  end if;
  --
  if GET_PROCESSING_REQUESTS(O_error_message            => O_error_message,
                             O_xxadeo_price_request_tbl => L_xxadeo_price_request_tbl,
                             O_request_type             => L_request_type,
                             O_request_action           => L_request_action) = FALSE then
    --
    RETURN FALSE;
    --
  end if;
  --
  if L_request_type is not null and L_request_action is not null then
    --
    if L_request_type in (GP_emergency_pc,GP_regular_pc,'PC') then
      --
      IO_price_event_type   := 'PC';
      IO_price_event_action := L_request_action;
      --
    else
      --
      IO_price_event_type   := L_request_type;
      IO_price_event_action := L_request_action;
      --
    end if;
    --
  else
    --
    -- delete when price change is processed with errors in staging area and
    -- price change id not exist into rpm area
    --
    delete xxadeo_price_request xpr
     where ((status in ('P','E','R')
       and action = 'PE')
        or (status = 'E'
       and action = 'R'))
       and not exists (select 1
                         from xxadeo_rpm_stage_price_change xrspc
                        where xrspc.xxadeo_process_id = xpr.request_process_id
                          and (xrspc.price_change_id is not null or
                               exists (select 1
                                         from rpm_price_change rpc
                                        where rpc.price_change_id = xrspc.price_change_id)));
    --
    commit;
    --
    if L_xxadeo_price_request_tbl.count <= 0 then
      --
      raise NO_PRICE_CHANGE;
      --
    else
      --
      raise INVALID_ACTION_TYPE;
      --
    end if;
    --
  end if;
  --
  --
  --
  if L_xxadeo_price_request_tbl.count > 0 then
    --
    if INJECT_PRICE_CHANGES(O_error_message            => O_error_message,
                            I_xxadeo_price_request_tbl => L_xxadeo_price_request_tbl,
                            I_price_event_action       => IO_price_event_action) = FALSE then
      --
      RETURN FALSE;
      --
    end if;
    --
  else
    --
    raise NO_PRICE_CHANGE;
    --
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when NO_PRICE_CHANGE then
    --
    O_error_message := 'There are no Price changes.';
    --
    RETURN FALSE;
  --
  when INVALID_ACTION_TYPE then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          'Event Type '||to_char(IO_price_event_type)||' or Action Type '||to_char(IO_price_event_action)||' is invalid',
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END RUN_BATCH_PRE_INJECTOR;
------------------------------------------------------------------------------------------------
FUNCTION RUN_BATCH_POST_INJECTOR(O_error_message      IN OUT LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(255) := 'XXADEO_PRICE_INJECTOR_SQL.RUN_BATCH_POST_INJECTOR';
  --
  L_request_type              XXADEO_PRICE_REQUEST.TYPE%TYPE;
  L_request_action            XXADEO_PRICE_REQUEST.ACTION%TYPE;
  L_xxadeo_price_request_tbl  XXADEO_PRICE_REQUEST_TBL;
  L_bulk_cc_pe_ids            OBJ_NUMERIC_ID_TABLE;
  L_con_check_err_id          NUMBER := NULL;
  L_cc_error_tbl              CONFLICT_CHECK_ERROR_TBL;
  --
  cursor C_get_request_type_action is
    select type,
           action
      from xxadeo_price_request
     where status = 'P'
    group by type,
             action;
  --
  cursor C_get_request is
    select new xxadeo_price_request_obj(request_process_id => request_process_id,
                                        type               => type,
                                        action             => action,
                                        process_order      => process_order,
                                        dependency         => dependency,
                                        pe_process_id      => pe_process_id)
      from xxadeo_price_request
     where status = 'P'
       and type   = L_request_type
       and action = L_request_action;
  --
  cursor C_get_price_change_by_stg is
    select xrspc.stage_price_change_id,
           xrspc.price_change_id
      from xxadeo_rpm_stage_price_change xrspc
     where exists (select 1
                     from table(L_xxadeo_price_request_tbl) tbl
                    where xrspc.xxadeo_process_id = tbl.request_process_id);
  --
  cursor C_get_pc_rejected is
    select new XXADEO_PC_REJECTED_OBJ(process_id                 => process_id,
                                      price_change_id            => price_change_id,
                                      price_change_display_id    => price_change_display_id,
                                      change_type                => change_type,
                                      change_amount              => change_amount,
                                      change_currency            => change_currency,
                                      change_percent             => change_percent,
                                      selling_uom                => selling_uom,
                                      multi_units                => multi_units,
                                      multi_unit_retail          => multi_unit_retail,
                                      multi_unit_retail_currency => multi_unit_retail_currency,
                                      multi_selling_uom          => multi_selling_uom,
                                      effective_date             => effective_date,
                                      item                       => item,
                                      dept                       => dept,
                                      class                      => class,
                                      subclass                   => subclass,
                                      location                   => location,
                                      zone_id                    => zone_id,
                                      zone_node_type             => zone_node_type,
                                      error_message              => error_message,
                                      action                     => action,
                                      status                     => status,
                                      user_id                    => user_id,
                                      create_datetime            => create_datetime,
                                      source                     => source)
      from (select xrspc.xxadeo_process_id process_id,
                   xrspc.stage_price_change_id price_change_id,
                   xrspc.price_change_display_id,
                   xrspc.change_type,
                   xrspc.change_amount,
                   xrspc.change_currency,
                   xrspc.change_percent,
                   xrspc.change_selling_uom selling_uom,
                   xrspc.multi_units,
                   xrspc.multi_unit_retail,
                   null multi_unit_retail_currency,
                   xrspc.multi_selling_uom,
                   xrspc.effective_date,
                   xrspc.item,
                   i.dept,
                   i.class,
                   i.subclass,
                   nvl(xrspc.location,xrspc.zone_id) location,
                   xrspc.zone_id zone_id,
                   xrspc.zone_node_type,
                   xrspc.error_message,
                   xpr.action,
                   xrspc.status,
                   xrspc.create_id user_id,
                   xrspc.create_datetime,
                   'STG' source
              from xxadeo_rpm_stage_price_change xrspc,
                   xxadeo_price_request          xpr,
                   item_master i
             where xrspc.status          in ('R','E')
               and i.item(+)              = xrspc.item
               and xpr.request_process_id = xrspc.xxadeo_process_id
               and xpr.status             = 'P'
               and exists (select code_id
                             from rpm_codes rc
                            where rc.code_id = xrspc.reason_code
                              and exists (select 1
                                            from code_detail cd
                                           where code_type    = XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_TYPE
                                             and code        in (XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_EM_ST_PORTAL, XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_ST_PORTAL)
                                             and cd.code_desc = rc.code))
            union all
            select xrspc.xxadeo_process_id process_id,
                   rspc.price_change_id,
                   rspc.price_change_display_id,
                   rspc.change_type,
                   rspc.change_amount,
                   rspc.change_currency,
                   rspc.change_percent,
                   rspc.change_selling_uom selling_uom,
                   rpc.multi_units,
                   rpc.multi_unit_retail,
                   rpc.multi_unit_retail_currency,
                   rpc.multi_selling_uom,
                   rspc.effective_date,
                   rspc.item,
                   i.dept,
                   i.class,
                   i.subclass,
                   nvl(xrspc.location,xrspc.zone_id) location,
                   xrspc.zone_id zone_id,
                   rspc.zone_node_type,
                   xrspc.error_message,
                   xpr.action,
                   rspc.status,
                   xrspc.create_id user_id,
                   xrspc.create_datetime,
                   (case 
                      when xrspc.reason_code in (select code_id
                                                   from rpm_codes rc
                                                  where exists (select 1
                                                                  from code_detail cd
                                                                 where code_type    = XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_TYPE
                                                                   and code        in (XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_EM_ST_PORTAL, XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_ST_PORTAL)
                                                                   and cd.code_desc = rc.code)) then 
                        'SP'
                      else
                        'RPM'
                   end) source
              from xxadeo_rpm_stage_price_change xrspc,
                   xxadeo_price_request          xpr,
                   rpm_stage_price_change        rspc,
                   rpm_price_change              rpc,
                   item_master i
             where xrspc.xxadeo_process_id    = xpr.request_process_id
               and rspc.stage_price_change_id = xrspc.stage_price_change_id
               and rspc.price_change_id       = rpc.price_change_id
               and i.item (+)                 = xrspc.item
               and rspc.price_change_id      is not null
               and xpr.action                in ('R','D','PE')
               and xpr.status                 = 'P'
               and rspc.status               in ('W','A'));

  --
  cursor C_price_conflict is
    with rpm_pc as
     (select rpc.price_change_id,
             rpc.effective_date,
             rpc.change_type,
             case rpc.change_type
               when RPM_CONSTANTS.RETAIL_PERCENT_OFF_VALUE then
                rpc.change_percent
               else
                rpc.change_amount
             end change_amount_percent
        from rpm_price_change rpc)
    select rspc.price_change_id,
           rspc.price_change_display_id,
           rspc.item,
           nvl(rspc.location, rspc.zone_id) location,
           rspc.zone_node_type,
           rspc.change_amount,
           rspc.effective_date,
           case rcce.ref_class
             when RPM_CONSTANTS.PC_REF_CLASS then
              RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
             when RPM_CONSTANTS.COMP_DETAIL_REF_CLASS then
              RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION
             when RPM_CONSTANTS.CL_REF_CLASS then
              RPM_CONSTANTS.PE_TYPE_CLEARANCE
             when RPM_CONSTANTS.CR_REF_CLASS then
              RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET
           end event_type,
           rcce.ref_display_id price_event_id,
           rcce.effective_date start_date,
           null end_date,
           (select trp.change_type
              from rpm_pc trp
             where trp.price_change_id = rcce.ref_id) change_type,
           (select trp.change_amount_percent
              from rpm_pc trp
             where trp.price_change_id = rcce.ref_id) change_amount_percent,
           rcce.message_key,
           xrspc.create_id user_id
      from rpm_stage_price_change        rspc,
           xxadeo_rpm_stage_price_change xrspc,
           xxadeo_price_request          xpr,
           rpm_con_check_err             rcce
     where xrspc.price_change_id   = rspc.price_change_id
       and xrspc.xxadeo_process_id = xpr.request_process_id
       and xpr.status              = 'P'
       and xpr.action              not in ('R')
       and rspc.status             = 'W'
       and rspc.error_message     is NOT NULL
       and rcce.ref_class          = RPM_CONSTANTS.PC_REF_CLASS
       and rcce.ref_id             = rspc.price_change_id
       and exists (select code_id
                     from rpm_codes rc
                    where rc.code_id = xrspc.reason_code
                      and exists (select 1
                                    from code_detail cd
                                   where code_type    = XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_TYPE
                                     and code        in (XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_EM_ST_PORTAL, XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_ST_PORTAL)
                                     and cd.code_desc = rc.code));
  --
  TYPE PRICE_CHANGE_REC IS TABLE OF C_get_price_change_by_stg%ROWTYPE;
  L_price_change_ids    PRICE_CHANGE_REC;
  --
  L_pc_rejected        XXADEO_PC_REJECTED_TBL := new XXADEO_PC_REJECTED_TBL();
  --
BEGIN
  --
  open C_get_pc_rejected;
  fetch C_get_pc_rejected bulk collect into L_pc_rejected;
  close C_get_pc_rejected;
  --
  for i in 1..L_pc_rejected.count loop
    --
    -- when rpm price change rejected by custom validation, inserts conflict error into rpm tables
    --
    if L_pc_rejected(i).source in ('RPM','SP') then
      --
      if POPULATE_RPM_CC_ERROR_TABLE(O_cc_error_tbl            => L_cc_error_tbl,
                                     IO_pc_rejected            => L_pc_rejected,
                                     I_price_change_event_type => RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE) = FALSE then
        --
        RETURN FALSE;
        --
      end if;
      --
    end if;
    --
    -- sent notification of conflict error for the Store Portal
    --
    if (L_pc_rejected(i).action <> 'PE' or L_pc_rejected(i).action is NULL) and L_pc_rejected(i).source in ('SP','STG') then
      --
      if XXADEO_NOTIFICATIONS_SQL.CREATE_RAF_NOTIFICATION(O_error_message           => O_error_message,
                                                          I_application_code        => 'StorePortalApp',
                                                          I_notification_type       => 'Price Change Rejected',
                                                          I_notification_desc       => 'Price change id ' ||L_pc_rejected(i).price_change_display_id || ' rejected for the Item '||L_pc_rejected(i).item||', location '||L_pc_rejected(i).location||', loc_type '||L_pc_rejected(i).zone_node_type||': '|| L_pc_rejected(i).error_message || '.',
                                                          I_notification_context    => 'title=List of rejected price changes (Conflict Checking)|url=/WEB-INF/com/adeo/merchandising/storeportal/view/price/rejected/flow/RejectedPriceChangeFlow.xml#RejectedPriceChangeFlow', --NOTIFICATION_CONTEXT,
                                                          I_user                    => L_pc_rejected(i).user_id,
                                                          I_errors_count            => 1) = FALSE then

        --
        RETURN FALSE;
        --
      end if;
      --
    end if;
    --
  end loop;
  --
  -- delete when price change is processed with errors in staging area
  --
  delete xxadeo_price_request xpr
   where (status in ('P','E','R')
     and  action = 'PE') 
      or (status = 'E'
     and  action = 'R');
  --
  for rec in C_price_conflict loop
    --
    -- sent notification of conflict error for the Store Portal
    --
    if XXADEO_NOTIFICATIONS_SQL.CREATE_RAF_NOTIFICATION(O_error_message           => O_error_message,
                                                        I_application_code        => 'StorePortalApp',
                                                        I_notification_type       => 'Price Change Rejected',
                                                        I_notification_desc       => 'Price change id ' ||rec.price_change_display_id || ' with conflict errors: ' || rec.message_key || '. Item '||rec.item||', location '||rec.location||', loc_type '||rec.zone_node_type||'.',
                                                        I_notification_context    => 'title=List of rejected price changes (Conflict Checking)|url=/WEB-INF/com/adeo/merchandising/storeportal/view/price/rejected/flow/RejectedPriceChangeFlow.xml#RejectedPriceChangeFlow', --NOTIFICATION_CONTEXT,
                                                        I_user                    => rec.user_id,
                                                        I_errors_count            => 1) = FALSE then

      --
      RETURN FALSE;
      --
    end if;
    --
  end loop;
  --
  for rec_type in C_get_request_type_action loop
    --
    L_request_type    := rec_type.type;
    L_request_action  := rec_type.action;
    --
    open C_get_request;
    fetch C_get_request bulk collect into L_xxadeo_price_request_tbl;
    close C_get_request;
    --
    if L_xxadeo_price_request_tbl is not null and L_xxadeo_price_request_tbl.count> 0 then
      --
      open C_get_price_change_by_stg;
      fetch C_get_price_change_by_stg bulk collect into L_price_change_ids;
      close C_get_price_change_by_stg;
      --
      -- delete price_changes from rpm staging area
      --
      forall i in L_price_change_ids.first..L_price_change_ids.last
        delete rpm_stage_price_change rspc
         where rspc.stage_price_change_id = L_price_change_ids(i).stage_price_change_id;
      --
      -- delete price_change from xxadeo stagging area
      --
      delete xxadeo_rpm_stage_price_change xrspc
       where exists (select 1
                       from table(L_xxadeo_price_request_tbl) tbl
                      where xrspc.xxadeo_process_id = tbl.request_process_id);
      --
      delete xxadeo_price_request xpr
       where exists (select 1
                       from table(L_xxadeo_price_request_tbl) tbl
                      where xpr.request_process_id = tbl.request_process_id);
      --
    end if;
    --
    -- delete when price change is processed with errors in staging area
    --
    delete xxadeo_price_request xpr
     where status in ('P','E','R')
       and action in ('PE','R','D');
    --
  end loop;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END RUN_BATCH_POST_INJECTOR;
------------------------------------------------------------------------------------------------
FUNCTION PRICE_PURGE_PROCESS(O_error_message OUT LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_program VARCHAR2(75) := 'XXADEO_PRICE_INJECTOR_SQL.PURGE_PRICE_PROCESS';
  --
BEGIN
  --
  for V_price_id in (select xpr.*
                       from xxadeo_price_request xpr,
                            xxadeo_rpm_stage_price_change xrpcs
                      where xpr.request_process_id = xrpcs.xxadeo_process_id
                        and xrpcs.create_datetime <= sysdate - (select xrpo.processed_pc_retention_days
                                                                  from xxadeo_rpm_custom_options xrpo
                                                                 where rownum = 1)
                         or xpr.status             = 'P') loop
    --
    delete from xxadeo_rpm_stage_price_change rsp
     where rsp.process_id = V_price_id.request_process_id;
    --
    delete from xxadeo_price_request xpr
     where xpr.request_process_id = V_price_id.request_process_id;
    --
  end loop;
  --
  for V_price_id in (select xpr.*
                       from xxadeo_price_request xpr,
                            xxadeo_rpm_stage_price_change xrpcs
                      where xpr.request_process_id = xrpcs.xxadeo_process_id
                        and xrpcs.create_datetime <= sysdate - (select xrpo.error_pc_retention_days
                                                                  from xxadeo_rpm_custom_options xrpo
                                                                 where rownum = 1)
                         or xpr.status             = 'E') loop
    --
    delete from xxadeo_rpm_stage_price_change rsp
     where rsp.process_id = V_price_id.request_process_id;
    --
    delete from xxadeo_price_request xpr
     where xpr.request_process_id = V_price_id.request_process_id;
    --
  end loop;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END PRICE_PURGE_PROCESS;
------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RPM_CC_ERROR_TABLE(O_cc_error_tbl               OUT CONFLICT_CHECK_ERROR_TBL,
                                     IO_pc_rejected            IN OUT XXADEO_PC_REJECTED_TBL,
                                     I_price_change_event_type IN     VARCHAR2)
RETURN BOOLEAN IS
  --
  L_program VARCHAR2(75) := 'XXADEO_PRICE_INJECTOR_SQL.POPULATE_RPM_CC_ERROR_TABLE';
  --
  L_con_check_err_id       NUMBER(15);
  --
BEGIN
  --
  if IO_pc_rejected is NOT NULL and IO_pc_rejected.COUNT > 0 then
    --
    if I_price_change_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then
      --
      for i in (select *
                  from table(cast(IO_pc_rejected AS xxadeo_pc_rejected_tbl))cc
                 where cc.source = 'RPM') loop
        --
        L_con_check_err_id := RPM_CON_CHECK_ERR_SEQ.NEXTVAL;
        --
        insert into rpm_con_check_err (con_check_err_id,
                                       ref_class,
                                       ref_id,
                                       ref_display_id,
                                       item,
                                       location,
                                       change_type,
                                       change_amount,
                                       change_currency,
                                       change_selling_uom,
                                       effective_date,
                                       error_date,
                                       message_key)
             values(L_con_check_err_id,
                    RPM_CONSTANTS.PC_REF_CLASS,
                    i.price_change_id,
                    i.price_change_display_id,
                    i.item,
                    i.location,
                    i.change_type,
                    i.change_amount,
                    i.change_currency,
                    i.selling_uom,
                    i.effective_date,
                    i.create_datetime,
                    i.error_message);

        insert into rpm_con_check_err_detail (con_check_err_detail_id,
                                              con_check_err_id,
                                              ref_class,
                                              ref_id,
                                              ref_display_id,
                                              effective_date,
                                              selling_retail,
                                              selling_retail_currency,
                                              selling_uom,
                                              multi_units,
                                              multi_unit_retail,
                                              multi_unit_retail_currency,
                                              multi_unit_uom)
        values(RPM_CON_CHECK_ERR_DETAIL_SEQ.NEXTVAL,
               L_con_check_err_id,
               RPM_CONSTANTS.PC_REF_CLASS,
               i.price_change_id,
               i.price_change_display_id,
               i.effective_date,
               i.change_amount,
               i.change_currency,
               i.selling_uom,
               i.multi_units,
               i.multi_unit_retail,
               i.multi_unit_retail_currency,
               null);
        --
        insert into rpm_conflict_check_result(conflict_check_result_id,
                                              event_type,
                                              result,
                                              results_date,
                                              user_id,
                                              event_id,
                                              promo_comp_id,
                                              promo_comp_detail_id,
                                              merch_type,
                                              dept,
                                              class,
                                              subclass,
                                              zone_id,
                                              error_string)
        values(RPM_CONFLICT_CHECK_RESULT_SEQ.NEXTVAL,
               1,--PRICE_CHANGE_RESULT
               1,
               GET_VDATE,
               'PRE-INJECTOR',
               i.price_change_id,
               NULL,
               NULL,
               NULL,
               i.dept,
               i.class,
               i.subclass,
               i.zone_id,
               NULL);
      --
      end loop;
      --
    end if;
    --
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
    --
    RETURN FALSE;
    --
END POPULATE_RPM_CC_ERROR_TABLE;
------------------------------------------------------------------------------------------------
END XXADEO_PRICE_INJECTOR_SQL;
/
