CREATE OR REPLACE PACKAGE XXADEO_CUSTOM_RULES_SUPP_SQL
AS
 
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package of Custom Rules Supplier for RB129                   */
/******************************************************************************/

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-07-24 - Jorge Agra - BUG#169 - RG5 Query for OU validation fixed      */
/*                           BUG#160 - RG1 will trigger for any status        */
/*                                     was hardcoded to only trigger on       */
/*                                     activate                               */
/* 2018-10-11 - Jorge Agra - Changes after RMS patch changing activation /    */
/*                           deactivation behaviour                           */
/* 2018-10-22 - Filipa Neves - Removed RG6 and RG7 implemented on        /    */
/*                           XXADEO_SUPP_CUST_RULES_CFA_SQL                   */
/******************************************************************************/

--------------------------------------------------------------------------------
GP_SUPP_STATUS_ACTIVATE   VARCHAR2(30) := 'SUPPLIER_ACTIVATE_UI';
GP_SUPP_STATUS_DEACTIVATE VARCHAR2(30) := 'SUPPLIER_DEACTIVATE_UI';

--------------------------------------------------------------------------------
FUNCTION RUN_CUSTOM_RULES_SUP_RG1(O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_parameters     IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION RUN_CUSTOM_RULES_SUP_RG2(O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_parameters     IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION RUN_CUSTOM_RULES_SUP_RG3(O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_parameters     IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION RUN_CUSTOM_RULES_SUP_RG4(O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_parameters     IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION RUN_CUSTOM_RULES_SUP_RG5(O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_parameters     IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION RUN_CUSTOM_RULES_SUPPLIER(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                                   I_parameters      IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
END XXADEO_CUSTOM_RULES_SUPP_SQL;
/