create or replace package XXADEO_ITEM_EXPENSES is

  -- Author  : LILIANA.FERREIRA
  -- Created : 18-Sep-18 14:17:38
  -- Purpose : 

  PROCEDURE PROCESS_RECORDS(O_status_code   OUT VARCHAR2,
                            O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE);

  PROCEDURE ENRICH_AND_VALIDATE;

end XXADEO_ITEM_EXPENSES;
/