--------------------------------------------------------
--  DDL for Package XXADEO_SUPPLIER_SQL
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE PACKAGE XXADEO_SUPPLIER_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
--- Function Name : PERSIST
--- Purpose       : Inserts records into SUPPLIER,SUPPLIER SITE,PARTNER_ORG_UNIT and ADDR tables
---                 by calling appropriate PRIVATE functions in the same package.
-------------------------------------------------------------------------------------------
FUNCTION PERSIST (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_supplier_record   IN OUT   SUPP_REC,
                  I_supCFAs           IN OUT   XXADEO_CFA_DETAILS_SUP_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
END XXADEO_SUPPLIER_SQL;

/