create or replace package XXADEO_CFAS_UTILS is
  /*---------------------------------------------------------------------*/
  /*
  * Object Name:   XXADEO_CFAS_UTILS.pks
  * Description:   This package is responsible for necessary operations with CFAS
  * Version:       1.0
  * Author:        Liliana Soraia Ferreira
  * Creation Date: 23/05/2018
  * Last Modified: 13/12/2018
  * History: 
  *               1.0 - Initial version
  *               1.1  - private function validate_cfa to public
  */
  /*------------------------------------------------------------------------*/

  PROCEDURE SET_CFAS(I_Entity        IN VARCHAR2,
                     I_primarykey    IN VARCHAR2,
                     I_message       IN XXADEO_CFA_DETAILS_TBL,
                     O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_message IN OUT VARCHAR2);

  PROCEDURE DELETE_CFAS(I_Entity        IN VARCHAR2,
                        I_primarykey    IN VARCHAR2,
                        I_cfas          IN XXADEO_CFA_DETAILS_TBL,
                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_message IN OUT VARCHAR2);

  PROCEDURE GET_CFAS_EXIST(I_Entity        IN VARCHAR2,
                           I_primarykey    IN VARCHAR2,
                           O_list_cfas     OUT XXADEO_CFA_DETAILS_TBL,
                           O_status_code   IN OUT VARCHAR2,
                           O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE);

  PROCEDURE GET_ALL_CFAS(I_Entity        IN VARCHAR2,
                         O_CFAs          OUT XXADEO_CFA_DETAILS_TBL,
                         O_status_code   IN OUT VARCHAR2,
                         O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE);

  PROCEDURE INICIALIZE_CFAS_TO_VIEW(I_cfas          IN XXADEO_CFA_DETAILS_TBL,
                                    I_primarykey    IN VARCHAR2,
                                    I_entity        IN VARCHAR2,
                                    O_status_code   IN OUT VARCHAR2,
                                    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE);

  FUNCTION GET_CFA_VALUE_NUM(I_entity      IN VARCHAR2,
                             I_primary_key IN VARCHAR2,
                             I_cfa_id      IN NUMBER) RETURN NUMBER;

  FUNCTION GET_CFA_VALUE_VARCHAR(I_entity      IN VARCHAR2,
                                 I_primary_key IN VARCHAR2,
                                 I_cfa_id      IN NUMBER) RETURN VARCHAR2;

  FUNCTION GET_CFA_VALUE_DATE(I_entity      IN VARCHAR2,
                              I_primary_key IN VARCHAR2,
                              I_cfa_id      IN NUMBER) RETURN DATE;

  Function VALIDATE_CFA(I_attrib_id IN INTEGER, I_value IN VARCHAR2)
    return BOOLEAN;

end XXADEO_CFAS_UTILS;
/