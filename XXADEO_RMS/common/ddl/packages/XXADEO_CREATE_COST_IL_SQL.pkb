CREATE OR REPLACE PACKAGE BODY XXADEO_CREATE_COST_IL_SQL AS
--------------------------------------------------------------------------------

LP_template_key              SVC_PROCESS_TRACKER.TEMPLATE_KEY%TYPE;
LP_process_source            SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE;
LP_ext_source                CONSTANT SVC_PROCESS_TRACKER.TEMPLATE_KEY%TYPE := 'COST_CHANGE';
--
TYPE ERRORS_TAB_TYP IS TABLE OF CORESVC_COSTCHG_ERR%ROWTYPE;
LP_errors_tab ERRORS_TAB_TYP;
--

TYPE item_locs_rec IS RECORD(item               svc_cost_susp_sup_detail.item%TYPE,
                             supplier           svc_cost_susp_sup_detail.supplier%TYPE,
                             origin_country_id  svc_cost_susp_sup_detail.origin_country_id%TYPE,
                             loc_type           svc_cost_susp_sup_detail_loc.loc_type%TYPE,
                             location           svc_cost_susp_sup_detail_loc.loc%TYPE,
                             pack_type          item_master.pack_type%TYPE,
                             unit_cost          svc_cost_susp_sup_detail.unit_cost%TYPE,
                             row_seq            svc_cost_susp_sup_detail.row_seq%TYPE);                            

   TYPE item_locs_tbl IS TABLE OF item_locs_rec INDEX BY BINARY_INTEGER;   
--------------------------------------------------------------------------------
/*Description: Logs validation errors in the coresvc_costchg_err table.       */ 
--------------------------------------------------------------------------------
FUNCTION WRITE_ERROR(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_process_id    IN      CORESVC_COSTCHG_ERR.PROCESS_ID%TYPE,
                     I_error_seq     IN      CORESVC_COSTCHG_ERR.ERROR_SEQ%TYPE,
                     I_chunk_id      IN      CORESVC_COSTCHG_ERR.CHUNK_ID%TYPE,
                     I_table_name    IN      CORESVC_COSTCHG_ERR.TABLE_NAME%TYPE,
                     I_row_seq       IN      CORESVC_COSTCHG_ERR.ROW_SEQ%TYPE,
                     I_column_name   IN      CORESVC_COSTCHG_ERR.COLUMN_NAME%TYPE,
                     I_error_msg     IN      CORESVC_COSTCHG_ERR.ERROR_MSG%TYPE,
                     I_item          IN      CORESVC_COSTCHG_ERR.ITEM%TYPE DEFAULT NULL,
                     I_supplier      IN      CORESVC_COSTCHG_ERR.SUPPLIER%TYPE DEFAULT NULL,
                     I_country_id    IN      CORESVC_COSTCHG_ERR.COUNTRY_ID%TYPE DEFAULT NULL,
                     I_error_type    IN      CORESVC_COSTCHG_ERR.ERROR_TYPE%TYPE DEFAULT ERROR)
RETURN BOOLEAN IS
   
  L_program         VARCHAR2(60) := 'XXADEO_CREATE_COST_IL_SQL.WRITE_ERROR';
  --
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  --
BEGIN
  -- Control should return as soon as first error is encountered
  if LP_template_key = LP_ext_source and LP_process_source = 'S9T' and I_error_type <> WARNING then
     L_error_message := SQL_LIB.PARSE_MSG(I_error_msg);
     O_error_message := L_error_message;
     return FALSE;
  end if;
  LP_errors_tab.EXTEND();
  LP_errors_tab(LP_errors_tab.count()).process_id  := I_process_id;
  LP_errors_tab(LP_errors_tab.count()).error_seq   := I_error_seq;
  LP_errors_tab(LP_errors_tab.count()).chunk_id    := I_chunk_id;
  LP_errors_tab(LP_errors_tab.count()).table_name  := I_table_name;
  LP_errors_tab(LP_errors_tab.count()).row_seq     := I_row_seq;
  LP_errors_tab(LP_errors_tab.count()).column_name := I_column_name;
  LP_errors_tab(LP_errors_tab.count()).error_msg   := I_error_msg;
  LP_errors_tab(LP_errors_tab.count()).item        := I_item;
  LP_errors_tab(LP_errors_tab.count()).supplier    := I_supplier;
  LP_errors_tab(LP_errors_tab.COUNT()).country_id  := I_country_id;
  LP_errors_tab(LP_errors_tab.count()).error_type  := I_error_type;
  --
  return TRUE;
  -- 
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END WRITE_ERROR;
--------------------------------------------------------------------------------
/*Description: This function creates item-location relation if not already    */
/*             exists.                                                        */
--------------------------------------------------------------------------------
FUNCTION CREATE_ITEMLOC(O_error_message   IN OUT   LOGGER_LOGS.TEXT%TYPE,
                        I_process_id      IN       NUMBER,
                        I_cost_rec        IN       ITEM_LOCS_TBL)
RETURN BOOLEAN IS

  L_program           VARCHAR2(50) := 'XXADEO_CREATE_COST_IL_SQL.CREATE_ITEMLOC';
  
  L_table             VARCHAR2(30) := 'SVC_COST_SUSP_SUP_DETAIL_LOC';
  L_input_rec         NEW_ITEM_LOC_SQL.NIL_INPUT_RECORD;
  L_input             NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;
  L_item_status       ITEM_MASTER.STATUS%TYPE;
  L_valid_item_sup    VARCHAR2(1);
  L_status            VARCHAR2(1) := NULL;
  L_ranged_ind        VARCHAR2(1);
  L_primary_supp      SUPS.SUPPLIER%TYPE;
  L_primary_cntry     COUNTRY.COUNTRY_ID%TYPE;
  L_error             BOOLEAN := FALSE;
  L_exists            BOOLEAN := FALSE;
  L_exist_sup_parent  VARCHAR2(1) := 'N';
  L_loc_type          item_loc.loc_type%TYPE;
  L_store_type        item_loc.loc_type%TYPE;
  L_franchise_loc_ind VARCHAR2(1) := 'N';
  --
  cursor C_loc_type(v_Location item_loc.loc%TYPE) is
    select loc_type
      from(
           select store loc, 'S'  loc_type
             from store
            where store = v_Location
          union
           select wh loc, 'W' loc_type
             from wh
            where wh = v_Location
                  union
           select to_number(partner_id) loc, 'E' loc_type
             from partner
            where partner_id = v_Location
              and partner_type = 'E')
     where loc = v_Location;
  --
  cursor C_check_supp_parent(V_supplier SUPS.SUPPLIER%TYPE) is
    select 'Y'                  
      from sups sups
     where sups.supplier_parent = V_supplier
       and rownum = 1;             
  --
BEGIN
  --
  LP_errors_tab := NEW errors_tab_typ();
  --
  if I_cost_rec is null or I_cost_rec.count = 0 then
    --
    return TRUE;
    --
  end if;
  --
  for i in I_cost_rec.FIRST..I_cost_rec.LAST LOOP
    --
    L_exists            := FALSE;
    L_exist_sup_parent  := 'N';
    L_franchise_loc_ind := 'N';
    L_error             := FALSE;
    --
    if I_cost_rec(i).loc_type IS NULL then
      --
      open  C_LOC_TYPE(I_cost_rec(i).location);
      fetch C_LOC_TYPE into L_loc_type;
      close C_LOC_TYPE;
      --
     else
       --
       L_loc_type   := I_cost_rec(i).loc_type;
       --
     end if;
    --
    if L_loc_type = 'S' then
      --
      select store_type
        into L_store_type
        from store
       where store = I_cost_rec(i).location;
      --
      if L_store_type = 'F' then
        --
        L_franchise_loc_ind := 'Y';
        --
      end if;
      --
      if L_franchise_loc_ind = 'Y' then
        --
        if NOT WRITE_ERROR(O_error_message,
                           I_process_id,
                           CORESVC_COSTCHG_ESEQ.NEXTVAL,
                           1,
                           L_table,
                           I_cost_rec(i).row_seq,
                           NULL,
                           'XXADEO_INVALID_LOC',
                           I_cost_rec(i).item,
                           I_cost_rec(i).supplier,
                           I_cost_rec(i).origin_country_id,
                           ERROR) then
           return FALSE;
        end if;
        L_error := TRUE;
        continue;
      end if;
      --
    end if;
    --
    -- Checks if the item is valid.
    --
    if XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_ITEM(O_error_message => O_error_message,
                                                O_item_status   => L_item_status,
                                                I_item          => I_cost_rec(i).item) = FALSE then
       --
       return FALSE;
       --
    end if;
    --
    if L_item_status is NULL or L_item_status <> 'A' then
        if NOT WRITE_ERROR(O_error_message,
                           I_process_id,
                           CORESVC_COSTCHG_ESEQ.NEXTVAL,
                           1,
                           L_table,
                           I_cost_rec(i).row_seq,
                           NULL,
                           'XXADEO_INVALID_ITEM',
                           I_cost_rec(i).item,
                           I_cost_rec(i).supplier,
                           I_cost_rec(i).origin_country_id,
                           ERROR) then
           return FALSE;
        end if;
        L_error := TRUE;
        continue;
     end if;
    --
    -- Checks if item/sup relation exist
    --
    if XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_ITEM_SUPPLIER(O_error_message   => O_error_message,
                                                         I_item            => I_cost_rec(i).item,
                                                         I_supplier        => I_cost_rec(i).supplier,
                                                         O_valid_item_sup  => L_valid_item_sup) = FALSE then
       --
       return FALSE;
       --
    end if;
    --
    if L_valid_item_sup is NULL then
        if NOT WRITE_ERROR(O_error_message,
                           I_process_id,
                           CORESVC_COSTCHG_ESEQ.NEXTVAL,
                           1,
                           L_table,
                           I_cost_rec(i).row_seq,
                           NULL,
                           'XXADEO_INV_ITEM_SUP',
                           I_cost_rec(i).item,
                           I_cost_rec(i).supplier,
                           I_cost_rec(i).origin_country_id,
                           ERROR) then
           return FALSE;
        end if;
        L_error := TRUE;
        continue;
     end if;
    --
    -- Checks if the supplier given is a Parent supplier
    --
    open C_check_supp_parent(I_cost_rec(i).supplier);
    fetch C_check_supp_parent into L_exist_sup_parent;
      --
      if C_check_supp_parent%NOTFOUND then
        --
        L_exist_sup_parent := 'N';
        --
      end if;
      --
    close C_check_supp_parent;
    --
    if L_exist_sup_parent = 'Y' then
        if NOT WRITE_ERROR(O_error_message,
                           I_process_id,
                           CORESVC_COSTCHG_ESEQ.NEXTVAL,
                           1,
                           L_table,
                           I_cost_rec(i).row_seq,
                           NULL,
                           'XXADEO_INV_ITEM_SUP',
                           I_cost_rec(i).item,
                           I_cost_rec(i).supplier,
                           I_cost_rec(i).origin_country_id,
                           ERROR) then
           return FALSE;
        end if;
        L_error := TRUE;
        continue;
     end if;
    --
    -- checks if item_supp_country exists for the item
    --
     if SUPP_ITEM_SQL.ITEM_SUPP_COUNTRY_EXISTS(O_error_message,
                                               L_exists,
                                               I_cost_rec(i).item,
                                               I_cost_rec(i).supplier,
                                               I_cost_rec(i).origin_country_id) = FALSE then
        --
        return FALSE;
        --
     end if;
    --
    if L_exists = FALSE then
       if NOT WRITE_ERROR(O_error_message,
                          I_process_id,
                          CORESVC_COSTCHG_ESEQ.NEXTVAL,
                          1,
                          L_table,
                          I_cost_rec(i).row_seq,
                          NULL,
                          'XXADEO_INV_ISUP_COUNTRY',
                          I_cost_rec(i).item,
                          I_cost_rec(i).supplier,
                          I_cost_rec(i).origin_country_id,
                          ERROR) then
          return FALSE;
       end if;
       L_error := TRUE;
       continue;
       -- 
    end if;
    --
    -- checks if item/location relations exist 
    --
    if XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_ITEM_RANGE(O_error_message     => O_error_message,
                                                      O_status            => L_status,
                                                      O_ranged_ind        => L_ranged_ind,
                                                      I_item              => I_cost_rec(i).item,
                                                      I_item_parent       => NULL,
                                                      I_loc               => I_cost_rec(i).location,
                                                      I_loc_type          => I_cost_rec(i).loc_type) = FALSE then
       --
       return FALSE;
       --
    end if;
    --
    -- get the item's primary supplier and primary country, which will be used as
    -- the new item_loc's primary supplier and primary country.
    if not ITEM_SUPP_COUNTRY_SQL.GET_PRIM_SUPP_CNTRY(O_error_message,
                                                     L_primary_supp,
                                                     L_primary_cntry,
                                                     I_cost_rec(i).item) then
       return FALSE;
    end if;
    --
    if L_error = FALSE then
      --
      if L_status is NULL then
         --
       L_input_rec.item                    := I_cost_rec(i).item;
       L_input_rec.item_parent             := NULL;
       L_input_rec.item_grandparent        := NULL;
       L_input_rec.item_desc               := NULL;
       L_input_rec.item_short_desc         := NULL;
       L_input_rec.dept                    := NULL;
       L_input_rec.item_class              := NULL;
       L_input_rec.subclass                := NULL;
       L_input_rec.item_level              := NULL;
       L_input_rec.tran_level              := NULL;
       L_input_rec.item_status             := NULL;
       L_input_rec.waste_type              := NULL;
       L_input_rec.sellable_ind            := NULL;
       L_input_rec.orderable_ind           := NULL;
       L_input_rec.pack_ind                := NULL;
       L_input_rec.pack_type               := NULL;
       L_input_rec.diff_1                  := NULL;
       L_input_rec.diff_2                  := NULL;
       L_input_rec.diff_3                  := NULL;
       L_input_rec.diff_4                  := NULL;
       L_input_rec.loc                     := I_cost_rec(i).location;
       L_input_rec.loc_type                := I_cost_rec(i).loc_type;
       L_input_rec.daily_waste_pct         := NULL;
       L_input_rec.unit_cost_loc           := NULL;
       L_input_rec.unit_retail_loc         := NULL;
       L_input_rec.selling_retail_loc      := NULL;
       L_input_rec.selling_uom             := NULL;
       L_input_rec.multi_units             := NULL;
       L_input_rec.multi_unit_retail       := NULL;
       L_input_rec.multi_selling_uom       := NULL;
       L_input_rec.item_loc_status         := 'A';
       L_input_rec.taxable_ind             := NULL;
       L_input_rec.ti                      := NULL;
       L_input_rec.hi                      := NULL;
       L_input_rec.store_ord_mult          := NULL;
       L_input_rec.meas_of_each            := NULL;
       L_input_rec.meas_of_price           := NULL;
       L_input_rec.uom_of_price            := NULL;
       L_input_rec.primary_variant         := NULL;
       L_input_rec.primary_supp            := L_primary_supp;
       L_input_rec.primary_cntry           := L_primary_cntry;
       L_input_rec.local_item_desc         := NULL;
       L_input_rec.local_short_desc        := NULL;
       L_input_rec.primary_cost_pack       := NULL;
       L_input_rec.receive_as_type         := NULL;
       L_input_rec.store_price_ind         := NULL;
       L_input_rec.uin_type                := NULL;
       L_input_rec.uin_label               := NULL;
       L_input_rec.capture_time            := NULL;
       L_input_rec.ext_uin_ind             := NULL;
       L_input_rec.source_method           := NULL;
       L_input_rec.source_wh               := NULL;
       L_input_rec.inbound_handling_days   := NULL;
       L_input_rec.currency_code           := NULL;
       L_input_rec.like_store              := NULL;
       L_input_rec.default_to_children_ind := NULL;
       L_input_rec.class_vat_ind           := NULL;
       L_input_rec.hier_level              := NULL;
       L_input_rec.hier_num_value          := NULL;
       L_input_rec.hier_char_value         := NULL;
       L_input_rec.ranged_ind              := 'Y';
       L_input_rec.costing_loc             := NULL;
       L_input_rec.costing_loc_type        := NULL;
        --
        L_input(L_input.count) := L_input_rec;
      --
      elsif L_ranged_ind ='N' then
        --
        update item_loc il
           set ranged_ind = 'Y'
         where il.item = I_cost_rec(i).item
           and il.loc  = I_cost_rec(i).location;
        --
      end if;
      --
    end if;
    --
  end loop;
  --
  if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message,
                                   L_input) = FALSE then
     return FALSE;
  end if;
  --
  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_ITEMLOC;
--------------------------------------------------------------------------------
--PUBLIC FUNCTIONS
FUNCTION PROCESS(O_error_message   IN OUT   LOGGER_LOGS.TEXT%TYPE,
                 I_process_id      IN       NUMBER,
                 I_template_key    IN       SVC_PROCESS_TRACKER.TEMPLATE_KEY%TYPE)
RETURN BOOLEAN IS
  L_program       VARCHAR2(60) := 'XXADEO_CREATE_COST_IL_SQL.PROCESS';
  --
  L_err_count     NUMBER;
  L_item_locs_tbl item_locs_tbl;   
  --
  cursor C_COST_DETAILS IS
     with locs as (select wh location,
                          physical_wh
                     from wh wh
                   union all
                   select store location,
                          null physical_wh
                     from store st)
     select scssdl.item,
            scssdl.supplier,
            scssdl.origin_country_id,
            scssdl.loc_type, --scssd.loc_type,
            locs.location, --scssd.location,
            im.pack_type,
            scssdl.unit_cost,
            scssdl.row_seq
       from svc_cost_susp_sup_detail_loc scssdl,
            svc_process_tracker          spt,
            item_master                  im,
            locs                         locs
      where scssdl.action                 = ACTION_NEW
        and scssdl.process$status         = PS_NEW
        and scssdl.process_id             = I_process_id
        and scssdl.item                   = im.item
        and scssdl.process_id             = spt.process_id
        and scssdl.loc                    = nvl(locs.physical_wh,locs.location)
        and nvl(locs.physical_wh,'-999') <> locs.location
        and spt.template_key              = I_template_key
        and scssdl.loc_type              in ('S', 'W');
  --
BEGIN
   --
   LP_errors_tab := NEW errors_tab_typ();
   --
   open C_COST_DETAILS;
   fetch C_COST_DETAILS bulk collect into L_item_locs_tbl;
   close C_COST_DETAILS;
   --
   if CREATE_ITEMLOC(O_error_message => O_error_message,
                     I_process_id    => I_process_id,
                     I_cost_rec      => L_item_locs_tbl) = FALSE then
      --
      return FALSE;
      --
   end if;
   --
   L_err_count   := LP_errors_tab.count();

   FORALL i IN 1..L_err_count
      insert into coresvc_costchg_err values LP_errors_tab(i);

   LP_errors_tab := NEW errors_tab_typ();

   return TRUE;
   --
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS;
--------------------------------------------------------------------------------
END XXADEO_CREATE_COST_IL_SQL;
/
