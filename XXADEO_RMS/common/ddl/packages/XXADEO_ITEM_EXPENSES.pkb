create or replace package body XXADEO_ITEM_EXPENSES is

  FUNCTION GET_NEXT_EXP_SEQ(I_item     IN ITEM_EXP_HEAD.ITEM%TYPE,
                            I_supplier IN ITEM_EXP_HEAD.SUPPLIER%TYPE)
    return number;

  PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                          IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cause          IN VARCHAR2,
                          I_program        IN VARCHAR2);

  -------------------------------------------------------------------------------------------------------
  -- PRIVATE FUNCTION
  -- GET  next value sequence of expense;

  -------------------------------------------------------------------------------------------------------
  FUNCTION GET_NEXT_EXP_SEQ(I_item     IN ITEM_EXP_HEAD.ITEM%TYPE,
                            I_supplier IN ITEM_EXP_HEAD.SUPPLIER%TYPE)
    return number is
  
    L_seq_no NUMBER;
  BEGIN
  
    select nvl(max(item_exp_seq), 0)
      into L_seq_no
      from item_exp_head
     where item = I_item
       and supplier = I_supplier;
  
    return L_seq_no + 1;
  
  END GET_NEXT_EXP_SEQ;

  -------------------------------------------------------------------------------------------------------
  -- PUBLIC PROCEDURE
  -------------------------------------------------------------------------------------------------------
  PROCEDURE PROCESS_RECORDS(I_dept          IN ITEM_MASTER.DEPT%TYPE,
                            O_status_code   OUT VARCHAR2,
                            O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE) IS
  
    CURSOR get_records IS
      select item,
             supplier,
             item_exp_type,
             origin_country_id,
             zone_id,
             lading_port,
             discharge_port,
             zone_group_id,
             comp_id,
             comp_rate,
             comp_currency,
             per_count,
             per_count_uom,
             nom_flag_4,
             last_update_datetime,
             last_update_id,
             update_type,
             pub_status,
             seq_no
        from xxadeo_stg_item_exp_in
       where pub_status = 'R'
         and item in (select item from item_master where dept = I_dept)
       order by seq_no asc;
  
    L_item_expense        xxadeo_stg_item_exp_in%ROWTYPE;
    L_exist_item_exp_head number;
    L_display_order       ELC_COMP.DISPLAY_ORDER%TYPE;
    L_result              BOOLEAN;
    L_count_detail        NUMBER;
    L_item_exp_seq        NUMBER;
  
  BEGIN
  
    O_status_code := 'S';
  
    for L_item_expense in get_records LOOP
      -- Creation expense
      if L_item_Expense.update_type = 'C' then
        -- item_Exp_head
        select count(*)
          into L_exist_item_exp_head
          from item_exp_head
         where item = L_item_expense.Item
           and supplier = L_item_expense.supplier
           and item_exp_type = L_item_expense.Item_Exp_Type
           and ((origin_country_id = L_item_expense.Origin_Country_Id and
               lading_port = L_item_expense.Lading_Port and
               discharge_port = L_item_expense.Discharge_Port) or
               (zone_id = L_item_expense.Zone_Id and
               discharge_port = L_item_expense.Discharge_Port));
      
        -- insert in item_Exp_head
      
        if L_exist_item_exp_head = 0 then
        
          L_item_exp_seq := GET_NEXT_EXP_SEQ(L_item_expense.item,
                                             L_item_expense.supplier);
        
          insert into item_exp_head
            (item,
             supplier,
             item_exp_type,
             item_exp_seq,
             origin_country_id,
             zone_id,
             lading_port,
             discharge_port,
             zone_group_id,
             base_exp_ind,
             create_datetime,
             last_update_datetime,
             last_update_id,
             create_id)
          values
            (L_item_expense.item,
             L_item_expense.supplier,
             L_item_expense.item_exp_type,
             L_item_exp_seq,
             L_item_expense.origin_country_id,
             L_item_expense.zone_id,
             L_item_expense.lading_port,
             L_item_expense.discharge_port,
             L_item_expense.zone_group_id,
             'N',
             L_item_expense.last_update_datetime,
             L_item_expense.last_update_datetime,
             L_item_expense.Last_Update_Id,
             L_item_expense.Last_Update_Id);
        
          -- exist in item_exp_head
        else
          select item_exp_seq
            into L_item_exp_seq
            from item_exp_head
           where item = L_item_expense.Item
             and supplier = L_item_expense.supplier
             and item_exp_type = L_item_expense.Item_Exp_Type
             and ((discharge_port = L_item_expense.Discharge_Port and
                 zone_id = L_item_expense.Zone_Id) or
                 (origin_country_id = L_item_expense.Origin_Country_Id and
                 lading_port = L_item_expense.Lading_Port and
                 discharge_port = L_item_expense.Discharge_Port));
        
        end if;
      
        Select display_order
          into L_display_order
          from elc_comp
         where comp_id = L_item_expense.Comp_Id;
      
        -- validate if exist in item_exp_detail
        -- verify if detail exist
        select count(*)
          into L_count_detail
          from item_exp_detail
         where item = L_item_expense.item
           and supplier = L_item_expense.supplier
           and item_exp_type = L_item_expense.Item_Exp_Type
           and item_exp_seq = L_item_exp_seq
           and comp_id = L_item_expense.comp_id;
      
        -- if detail not exist
        if L_count_detail = 0 then
          -- insert in item_exp_detail
          insert into item_exp_detail
            (item,
             supplier,
             item_exp_type,
             item_exp_seq,
             comp_id,
             comp_rate,
             comp_currency,
             per_count,
             per_count_uom,
             nom_flag_1,
             nom_flag_2,
             nom_flag_3,
             nom_flag_4,
             nom_flag_5,
             display_order,
             est_exp_value,
             create_datetime,
             last_update_datetime,
             create_id,
             last_update_id)
          values
            (L_item_expense.Item,
             L_item_expense.supplier,
             L_item_expense.Item_Exp_Type,
             L_item_exp_seq,
             L_item_expense.comp_id,
             L_item_expense.comp_rate,
             L_item_expense.comp_currency,
             L_item_expense.per_count,
             L_item_expense.per_count_uom,
             'N',
             'N',
             'N',
             L_item_expense.nom_flag_4,
             'N',
             L_display_order,
             0.0,
             L_item_expense.last_update_datetime,
             L_item_expense.last_update_datetime,
             L_item_expense.Last_Update_Id,
             L_item_expense.Last_Update_Id);
        
          L_result := item_expense_sql.insert_always_expenses(o_error_message     => o_error_message,
                                                              i_item              => L_item_expense.Item,
                                                              i_supplier          => L_item_expense.supplier,
                                                              i_item_exp_type     => L_item_expense.item_exp_type,
                                                              i_item_exp_seq      => L_item_exp_seq,
                                                              i_origin_country_id => L_item_expense.Origin_Country_Id);
        
          if L_result = FALSE then
            update xxadeo_stg_item_exp_in
               set pub_status           = 'E',
                   error_msg            = o_error_message,
                   last_update_datetime = sysdate,
                   last_update_id       = get_user
             where seq_no = L_item_expense.seq_no;
          
          else
          
            Update xxadeo_stg_item_exp_in
               set pub_status           = 'P',
                   last_update_datetime = sysdate,
                   last_update_id       = get_user
             where seq_no = L_item_expense.seq_no;
          
          end if;
        
          continue;
        
        else
          -- update record in staging with error
          update xxadeo_stg_item_exp_in
             set pub_status = 'E',
                 error_msg  = 'Item Expense detail already exists.'
           where item = L_item_expense.Item
             and supplier = L_item_expense.supplier
             and item_exp_type = L_item_expense.Item_Exp_Type
             and ((origin_country_id = L_item_expense.Origin_Country_Id and
                 lading_port = L_item_expense.Lading_Port and
                 discharge_port = L_item_expense.Discharge_Port) or
                 (zone_id = L_item_expense.Zone_Id and
                 discharge_port = L_item_expense.Discharge_Port));
        
          continue;
        end if;
      
        --update expense
      elsif L_item_expense.update_type = 'U' then
        -- verify if head exist and get display_order
        Select nvl(item_exp_seq, -1)
          into L_item_exp_seq
          from item_exp_head
         where item = L_item_expense.Item
           and supplier = L_item_expense.supplier
           and item_exp_type = L_item_expense.Item_Exp_Type
           and ((origin_country_id = L_item_expense.Origin_Country_Id and
               lading_port = L_item_expense.Lading_Port and
               discharge_port = L_item_expense.Discharge_Port) or
               (zone_id = L_item_expense.Zone_Id and
               discharge_port = L_item_expense.Discharge_Port));
      
        -- if head not exist
        if L_item_exp_seq = -1 then
        
          -- update record in staging with error
          update xxadeo_stg_item_exp_in
             set pub_status = 'E',
                 error_msg  = 'Item Expense Head not exist.'
           where seq_no = L_item_expense.seq_no;
        
          continue;
        end if;
      
        -- verify if detail exist
        select count(*)
          into L_count_detail
          from item_exp_detail
         where item = L_item_expense.item
           and supplier = L_item_expense.supplier
           and item_exp_type = L_item_expense.Item_Exp_Type
           and item_exp_seq = L_item_exp_seq
           and comp_id = L_item_expense.comp_id;
      
        -- if detail not exist
        if L_count_detail = 0 then
        
          -- update record in staging with error
          update xxadeo_stg_item_exp_in
             set pub_status = 'E',
                 error_msg  = 'Item Expense Detail not exist.'
           where seq_no = L_item_expense.seq_no;
        end if;
      
        UPDATE item_exp_detail
           set comp_rate     = L_item_expense.comp_rate,
               per_count     = L_item_expense.Per_Count,
               per_count_uom = L_item_expense.Per_Count_Uom,
               comp_currency = L_item_expense.Comp_Currency,
               nom_flag_4    = L_item_expense.Nom_Flag_4
         where item = L_item_expense.item
           and supplier = L_item_expense.supplier
           and item_exp_type = L_item_expense.Item_Exp_Type
           and item_exp_seq = L_item_exp_seq
           and comp_id = L_item_expense.comp_id;
      
        L_result := item_expense_sql.insert_always_expenses(o_error_message     => o_error_message,
                                                            i_item              => L_item_expense.Item,
                                                            i_supplier          => L_item_expense.supplier,
                                                            i_item_exp_type     => L_item_expense.item_exp_type,
                                                            i_item_exp_seq      => L_item_exp_seq,
                                                            i_origin_country_id => L_item_expense.Origin_Country_Id);
      
        if L_result = FALSE then
          update xxadeo_stg_item_exp_in
             set pub_status           = 'E',
                 error_msg            = o_error_message,
                 last_update_datetime = sysdate,
                 last_update_id       = get_user
           where seq_no = L_item_expense.seq_no;
        
        else
        
          Update xxadeo_stg_item_exp_in
             set pub_status           = 'P',
                 last_update_datetime = sysdate,
                 last_update_id       = get_user
           where seq_no = L_item_expense.seq_no;
        
        end if;
      
      elsif L_item_expense.update_type = 'D' then
        -- verify if head exist and get display_order
        Select nvl(item_exp_seq, -1)
          into L_item_exp_seq
          from item_exp_head
         where item = L_item_expense.Item
           and supplier = L_item_expense.supplier
           and item_exp_type = L_item_expense.Item_Exp_Type
           and ((origin_country_id = L_item_expense.Origin_Country_Id and
               lading_port = L_item_expense.Lading_Port and
               discharge_port = L_item_expense.Discharge_Port) or
               (zone_id = L_item_expense.Zone_Id and
               discharge_port = L_item_expense.Discharge_Port));
      
        -- if head not exist
        if L_display_order = -1 then
        
          -- update record in staging with error
          update xxadeo_stg_item_exp_in
             set pub_status = 'E',
                 error_msg  = 'Item Expense Head not exist.'
           where seq_no = L_item_Expense.seq_no;
        
        end if;
      
        -- verify if detail exist
        select count(*)
          into L_count_detail
          from item_exp_detail
         where item = L_item_expense.item
           and supplier = L_item_expense.supplier
           and item_exp_type = L_item_expense.Item_Exp_Type
           and item_exp_seq = L_item_exp_seq
           and comp_id = L_item_expense.comp_id;
      
        -- if detail not exist
        if L_count_detail = 0 then
        
          -- update record in staging with error
          update xxadeo_stg_item_exp_in
             set pub_status = 'E',
                 error_msg  = 'Item Expense Detail not exist.'
           where seq_no = L_item_Expense.seq_no;
        
        end if;
      
        delete from item_Exp_detail
         where item = L_item_expense.item
           and supplier = L_item_expense.supplier
           and item_exp_type = L_item_expense.Item_Exp_Type
           and item_exp_seq = L_item_exp_seq
           and comp_id = L_item_expense.comp_id;
      
        -- count records != total
        select count(*)
          into L_count_detail
          from item_exp_detail
         where item = L_item_expense.item
           and supplier = L_item_expense.supplier
           and item_exp_type = L_item_expense.Item_Exp_Type
           and item_exp_seq = L_item_exp_seq
           and comp_id not in
               (select comp_id
                  from elc_comp
                 where always_default_ind = 'Y'
                   and expense_type = L_item_expense.Item_Exp_Type);
      
        -- not exist others records 
        if L_count_detail = 0 then
          delete from item_Exp_detail
           where item = L_item_expense.item
             and supplier = L_item_expense.supplier
             and item_exp_type = L_item_expense.Item_Exp_Type
             and item_Exp_seq = L_item_exp_seq;
        
          delete from item_exp_head
           where item = L_item_expense.item
             and supplier = L_item_expense.supplier
             and item_exp_type = L_item_expense.Item_Exp_Type
             and item_exp_seq = L_item_exp_seq;
        
          Update xxadeo_stg_item_exp_in
             set pub_status           = 'P',
                 last_update_datetime = sysdate,
                 last_update_id       = get_user
           where seq_no = L_item_expense.seq_no;
        
        else
        
          L_result := item_expense_sql.insert_always_expenses(o_error_message     => o_error_message,
                                                              i_item              => L_item_expense.Item,
                                                              i_supplier          => L_item_expense.supplier,
                                                              i_item_exp_type     => L_item_expense.item_exp_type,
                                                              i_item_exp_seq      => L_item_exp_seq,
                                                              i_origin_country_id => L_item_expense.Origin_Country_Id);
        
          if L_result = FALSE then
            update xxadeo_stg_item_exp_in
               set pub_status           = 'E',
                   error_msg            = o_error_message,
                   last_update_datetime = sysdate,
                   last_update_id       = get_user
             where seq_no = L_item_expense.seq_no;
          
          else
          
            Update xxadeo_stg_item_exp_in
               set pub_status           = 'P',
                   last_update_datetime = sysdate,
                   last_update_id       = get_user
             where seq_no = L_item_expense.seq_no;
          
          end if;
        
          null;
        end if;
      
      end if;
    
    END LOOP;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    'XXADEO_ITEM_EXPENSES.PROCESS_RECORDS');
    
  END PROCESS_RECORDS;

  -------------------------------------------------------------------------------------------------------
  -- PUBLIC PROCEDURE
  -- ENRICH_AND_VALIDATE
  -------------------------------------------------------------------------------------------------------

  PROCEDURE ENRICH_AND_VALIDATE IS
  
    L_zone_group_id     COST_ZONE.ZONE_GROUP_ID%TYPE;
    L_origin_country_id OUTLOC.OUTLOC_COUNTRY_ID%TYPE;
  
    O_status_code         VARCHAR2(400);
    O_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
    L_count_item_supplier NUMBER;
    L_count_zone          NUMBER;
    L_discharge_port      NUMBER;
    L_lading_port         NUMBER;
    L_currency            NUMBER;
  
  BEGIN
  
    for r in (SELECT * from xxadeo_stg_item_exp_in where pub_status = 'N') loop
    
      --validate item - supplier relationship
      select count(*)
        into L_count_item_supplier
        from item_supplier
       where item = r.item
         and supplier = r.supplier;
    
      if L_count_item_supplier = 0 then
        update xxadeo_stg_item_exp_in
           set error_msg            = 'Not exist item/supplier relationship',
               pub_status           = 'E',
               last_update_datetime = sysdate,
               last_update_id       = get_user
         where item = r.item
           and supplier = r.supplier;
      
        CONTINUE;
      end if;
    
      -- validate currency
      select count(*)
        into L_currency
        from elc_comp
       where comp_id = r.comp_id
         and comp_currency = r.comp_currency;
    
      if L_currency = 0 then
        update xxadeo_stg_item_exp_in
           set error_msg            = 'Currency is not valid.',
               pub_status           = 'E',
               last_update_datetime = sysdate,
               last_update_id       = get_user
         where item = r.item
           and supplier = r.supplier
           and comp_id = r.comp_id
           and comp_currency = r.comp_currency;
        CONTINUE;
      end if;
    
      --enrich
    
      if r.Item_Exp_Type = 'Z' then
        -- validate zone
        Select count(*)
          into L_count_zone
          from cost_zone
         where zone_id = r.zone_id;
      
        if L_count_zone = 0 then
          update xxadeo_stg_item_exp_in
             set error_msg            = 'Zone not exist',
                 pub_status           = 'E',
                 last_update_datetime = sysdate,
                 last_update_id       = get_user
           where item = r.item
             and supplier = r.supplier
             and zone_id = r.zone_id;
        
          CONTINUE;
        end if;
        -- enrich zone_group_id
        Select zone_group_id
          into L_zone_group_id
          from cost_zone
         where zone_id = r.zone_id
           and rownum = 1;
      
        UPDATE xxadeo_stg_item_exp_in
           set zone_group_id = L_zone_group_id
         where zone_id = r.zone_id
           and item = r.item
           and supplier = r.supplier;
      
        -- validate discharge_port
        Select count(*)
          into L_discharge_port
          from outloc
         where outloc_type = 'DP'
           and outloc_id = r.discharge_port;
      
        if L_discharge_port = 0 then
          update xxadeo_stg_item_exp_in
             set error_msg            = 'Discharge port not exist.',
                 pub_status           = 'E',
                 last_update_datetime = sysdate,
                 last_update_id       = get_user
           where item = r.item
             and supplier = r.supplier
             and zone_id = r.zone_id
             and discharge_port = r.discharge_port;
        
          CONTINUE;
        end if;
      
      elsif r.item_exp_type = 'C' then
      
        -- validate discharge_port
        Select count(*)
          into L_discharge_port
          from outloc
         where outloc_type = 'DP'
           and outloc_id = r.discharge_port;
      
        if L_discharge_port = 0 then
          update xxadeo_stg_item_exp_in
             set error_msg            = 'Discharge port not exist.',
                 pub_status           = 'E',
                 last_update_datetime = sysdate,
                 last_update_id       = get_user
           where item = r.item
             and supplier = r.supplier
             and zone_id = r.zone_id
             and discharge_port = r.discharge_port;
        
          CONTINUE;
        end if;
      
        -- validate lading_port
        Select count(*)
          into L_lading_port
          from outloc
         where outloc_type = 'LP'
           and outloc_id = r.lading_port;
      
        if L_lading_port = 0 then
          update xxadeo_stg_item_exp_in
             set error_msg            = 'Lading port not exist.',
                 pub_status           = 'E',
                 last_update_datetime = sysdate,
                 last_update_id       = get_user
           where item = r.item
             and supplier = r.supplier
             and zone_id = r.zone_id
             and lading_port = r.lading_port;
        
          CONTINUE;
        end if;
      
        -- enrich origin_country_id
        select origin_country_id
          into L_origin_country_id
          from item_supp_country
         where item = r.item
           and supplier = r.supplier
           and primary_country_ind = 'Y';
      
        update xxadeo_stg_item_exp_in
           set origin_country_id = L_origin_country_id
         where lading_port = r.lading_port
           and item = r.item
           and supplier = r.supplier
           and item_exp_type = r.item_Exp_type;
      
      end if;
    
    end loop;
  
    UPDATE xxadeo_stg_item_exp_in
       set pub_status = 'R'
     where pub_status = 'N';
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    'XXADEO_ITEM_EXPENSES.ENRICH_AND_VALIDATE');
    
  END ENRICH_AND_VALIDATE;

  -------------------------------------------------------------------------------------------------------
  -- PRIVATE PROCEDURE
  -------------------------------------------------------------------------------------------------------
  PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                          IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cause          IN VARCHAR2,
                          I_program        IN VARCHAR2) IS
  
    L_program VARCHAR2(50) := 'XXADEO_ITEM_EXPENSES.HANDLE_ERRORS';
  
  BEGIN
  
    API_LIBRARY.HANDLE_ERRORS(O_status_code,
                              IO_error_message,
                              I_cause,
                              I_program);
  EXCEPTION
    when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
    
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
    
  END HANDLE_ERRORS;

end XXADEO_ITEM_EXPENSES;
/