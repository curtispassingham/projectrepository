CREATE OR REPLACE PACKAGE BODY XXADEO_XITEM_UTILS_PCK AS
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_XITEM_UTILS_PCK.pkb
* Description:   This package contains tools that help in the 
*				 implementation of the publisher package XXADEO_RMSSUB_XITEM.
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
  -------------------------------------------------------------------------------------------------------
  -- PRIVATE PROCEDURE
  -------------------------------------------------------------------------------------------------------
  PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                          IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cause          IN VARCHAR2,
                          I_program        IN VARCHAR2);

 
  -------------------------------------------------------------------------------------------------------
  -- Name: GENERATE_BARCODE
  -- Description: This function is responsible for generate a Barcode identifier according rules defined 
  -------------------------------------------------------------------------------------------------------
  FUNCTION GENERATE_BARCODE RETURN NUMBER IS
  
    L_num_char    VARCHAR2(13);
    L_sum         NUMBER(20) := 0;
    L_multiple    NUMBER(1) := 0;
    L_check_digit NUMBER;
    L_prefix      VARCHAR(10);
  
  BEGIN
    select value_1
      into L_prefix
      from xxadeo_mom_dvm
     where func_area = 'XITEM'
       and parameter = 'PREFIX_EAN';
  
    select L_prefix ||
           lpad(XXADEO_BARCODE_SEQ.nextval, 12 - length(L_prefix), 0)
      into L_num_char
      from dual;
  
    IF LENGTH(L_num_char) != 12 THEN
      raise_application_error(-20001,
                              'Account basic cannot be more than 12 digits long');
    END IF;
  
    FOR I IN 1 .. 12 LOOP
      IF mod(i, 2) = 0 THEN
        L_multiple := 3;
      ELSE
        L_multiple := 1;
      END IF;
      L_sum := L_sum + SUBSTR(L_num_char, i, 1) * L_multiple;
    END LOOP;
  
    L_check_digit := 10 - Mod(L_sum, 10);
    if L_check_digit = 10 then
      L_check_digit := 0;
    end if;
  
    L_num_char := L_num_char || to_char(L_check_digit);
  
    RETURN to_number(L_num_char);
  
  END GENERATE_BARCODE;
  
  
  -------------------------------------------------------------------------------------------------------
  -- Name: VERIFY_SEQUENCING
  -- Description: This procedure is responsible to verify if message is a create or update 
  -- O_code = 0  new item
  -- O_code = 1 update item for I_bu 
  -- O_code = 2 new bu related for this item
  -------------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_SEQUENCING(I_identifier    IN VARCHAR2,
                              I_bu            IN VARCHAR2,
                              I_extract_date  IN date,
                              O_code          OUT NUMBER,
                              O_status_code   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error_message OUT varchar2) is
  
    L_program     VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.VERIFY_SEQUENCING';
    L_exists      number;
    L_last_update date;
    L_new_bu      number;
  
  BEGIN
    O_status_code := 'S';
    O_code        := 0;
  
    select count(*)
      into L_exists
      from xxadeo_sequencing_ctrl cl
     where cl.id = I_identifier;
    --and cl.id in (select item from item_master);
  
    if L_exists != 0 then
      select count(*)
        into L_new_bu
        from xxadeo_sequencing_ctrl cl
       where cl.bu = I_bu
         and cl.id = I_identifier;
    
      if L_new_bu = 0 then
        Insert into XXADEO_SEQUENCING_CTRL
          (id, bu, last_update)
        values
          (I_identifier, I_bu, I_extract_date);
        O_code := 2;
      else
        select cast(last_update as date)
          into L_last_update
          from XXADEO_SEQUENCING_CTRL
         where id = I_identifier
           and bu = I_bu;
        O_code := 1;
      
        if L_last_update > I_extract_date then
        
          O_code          := -1;
          O_error_message := 'Message was Ignored';
          O_status_code   := 'E';
          raise PROGRAM_ERROR;
        
        else
          update XXADEO_SEQUENCING_CTRL set last_update = I_extract_date;
        
        end if;
      end if;
    
    else
      Insert into XXADEO_SEQUENCING_CTRL
        (id, bu, last_update)
      values
        (I_identifier, I_bu, I_extract_date);
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
      null;
    
  end verify_sequencing;
  

   -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_DEL
  -- Description: This procedure is responsible for create a XItemRef message to delete an item (barcode)
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_DEL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                O_status_code   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error_message OUT VARCHAR2) IS
  
    L_program      VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MESSAGE_ITEM_DEL';
    L_message      "RIB_XItemRef_REC";
    L_message_type VARCHAR2(30);
  
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.ITEM_DEL;
    L_message      := "RIB_XItemRef_REC"(0, --rib oid
                                         I_item, --ITEM-VARCHAR2(25)
                                         NULL, --HIER_LEVEL-VARCHAR2(2)
                                         NULL, --XITEMCTRYREF_TBL-RIB_XItemCtryRef_TBL()
                                         NULL, --XITEMSUPREF_TBL-RIB_XItemSupRef_TBL()
                                         NULL, --XITEMVATREF_TBL-RIB_XItemVATRef_TBL()
                                         NULL, --XITEMIMAGEREF_TBL-RIB_XItemImageRef_TBL()
                                         NULL, --XITEMSEASONREF_TBL-RIB_XItemSeasonRef_TBL()
                                         NULL, --XITEMUDAREF_TBL-RIB_XItemUDARef_TBL()
                                         NULL, --XITEMBOMREF_TBL-RIB_XItemBOMRef_TBL()
                                         NULL, --SYSTEM_IND-VARCHAR2(1)
                                         NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                         NULL); --LANGOFXITEMREF_TBL-RIB_LangOfXItemRef_TBL()
  
    XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                O_error_message,
                                L_message,
                                L_message_type);
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_ITEM_DEL;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_CRE 
  -- Description: Responsible for create a XItemDesc(base) message to create an item
  --              This procedure have logic to set some fields null according base messages
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_CRE(I_message          IN "RIB_XItemDesc_REC",
                                I_item_level       IN NUMBER,
                                I_type             IN ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                                I_supplier_primary IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                I_item_id          IN ITEM_MASTER.ITEM%TYPE,
                                I_item_parent      IN ITEM_MASTER.ITEM%TYPE,
                                I_item_grandparent IN ITEM_MASTER.ITEM%TYPE,
                                O_status_code      OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error_message    OUT VARCHAR2) IS
  
    L_program           VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_CRE';
    L_message_sup_tbl   "RIB_XItemSupDesc_TBL";
    L_message_sup_rec   "RIB_XItemSupDesc_REC";
    L_message_type      VARCHAR2(30);
    L_message_uda_rec   "RIB_XItemUDADtl_REC";
    L_message_uda_tbl   "RIB_XItemUDADtl_TBL";
    L_message_image_tbl "RIB_XItemImage_TBL";
    L_message_image_rec "RIB_XItemImage_REC";
    L_message           "RIB_XItemDesc_REC";
    L_vat_item_rec      "RIB_XItemVATDesc_REC";
    L_vat_item_tbl      "RIB_XItemVATDesc_TBL";
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.ITEM_ADD;
    
    for i in 1 .. I_message.XITEMVATDESC_TBL.count loop
      L_vat_item_rec := "RIB_XItemVATDesc_REC"(0, --rib oid
                                               I_message.XITEMVATDESC_TBL(i)
                                               .vat_type, --VAT_TYPE-VARCHAR2(1)
                                               I_message.XITEMVATDESC_TBL(i)
                                               .vat_region, --VAT_REGION-NUMBER(6)
                                               I_message.XITEMVATDESC_TBL(i)
                                               .vat_code, --VAT_CODE-VARCHAR2(6)
                                               get_vdate(), --ACTIVE_DATE-DATE()
                                               'N'); --REVERSE_VAT_IND-VARCHAR2(1)
    
      if L_vat_item_tbl is null then
        L_vat_item_tbl := "RIB_XItemVATDesc_TBL"();
      end if;
    
      L_vat_item_tbl.extend();
      L_vat_item_tbl(L_vat_item_tbl.LAST) := L_vat_item_rec;
    
    end loop;
  
    if (I_message.item_level = 1 and I_message.pack_ind = 'Y' and
       I_message.sellable_ind = 'Y' and I_message.orderable_ind = 'N' and
       I_message.inventory_ind = 'Y') 
       or (I_message.item_level = 1 and I_message.pack_ind = 'N' and
       I_message.sellable_ind = 'Y' and I_message.orderable_ind = 'N' and
       I_message.inventory_ind = 'N') 
       then
      null;
    
    else
      if I_message.XITEMSUPDESC_TBL is null or
         I_message.XITEMSUPDESC_TBL.count = 0 then
        O_status_code   := 'E';
        O_error_message := 'Item Supplier is required!';
        raise PROGRAM_ERROR;
      
      end if;
    end if;
  
    L_message_sup_tbl := "RIB_XItemSupDesc_TBL"();
  
    if I_message.XITEMSUPDESC_TBL is not null and
       I_message.XITEMSUPDESC_TBL.count > 0 then
      for i in 1 .. I_message.XITEMSUPDESC_TBL.count loop
        if I_supplier_primary is null and i = 1 then
          L_message_sup_rec := "RIB_XItemSupDesc_REC"(0, --rib oid
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .SUPPLIER, --SUPPLIER-VARCHAR2(10)
                                                      'Y', --PRIMARY_SUPP_IND-VARCHAR2(3)
                                                      I_message.XITEMSUPDESC_TBL(i).vpn, --VPN-VARCHAR2(30)
                                                      NULL, --SUPP_LABEL-VARCHAR2(15)
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .XITEMSUPCTYDESC_TBL, --XITEMSUPCTYDESC_TBL-RIB_XItemSupCtyDesc_TBL()
                                                      NULL, --CONSIGNMENT_RATE-NUMBER(12)
                                                      NULL, --SUPP_DISCONTINUE_DATE-DATE()
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .direct_ship_ind, --DIRECT_SHIP_IND-VARCHAR2(1)
                                                      NULL, --PALLET_NAME-VARCHAR2(6)
                                                      NULL, --CASE_NAME-VARCHAR2(6)
                                                      NULL, --INNER_NAME-VARCHAR2(6)
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .XITMSUPCTYMFRDESC_TBL, --XITMSUPCTYMFRDESC_TBL-RIB_XItmSupCtyMfrDesc_TBL()
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .primary_case_size, --PRIMARY_CASE_SIZE-VARCHAR2(6)
                                                      NULL, --SUPP_DIFF_1-VARCHAR2(120)
                                                      NULL, --SUPP_DIFF_2-VARCHAR2(120)
                                                      NULL, --SUPP_DIFF_3-VARCHAR2(120)
                                                      NULL, --SUPP_DIFF_4-VARCHAR2(120)
                                                      NULL, --CONCESSION_RATE-NUMBER(12)
                                                      NULL); --LANGOFXITEMSUPDESC_TBL-RIB_LangOfXItemSupDesc_TBL()
        
        elsif I_supplier_primary is not null and
              I_supplier_primary = I_message.XITEMSUPDESC_TBL(i).supplier then
          L_message_sup_rec := "RIB_XItemSupDesc_REC"(0, --rib oid
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .SUPPLIER, --SUPPLIER-VARCHAR2(10)
                                                      'Y', --PRIMARY_SUPP_IND-VARCHAR2(3)
                                                      upper(I_message.XITEMSUPDESC_TBL(i).vpn), --VPN-VARCHAR2(30)
                                                      NULL, --SUPP_LABEL-VARCHAR2(15)
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .XITEMSUPCTYDESC_TBL, --XITEMSUPCTYDESC_TBL-RIB_XItemSupCtyDesc_TBL()
                                                      NULL, --CONSIGNMENT_RATE-NUMBER(12)
                                                      NULL, --SUPP_DISCONTINUE_DATE-DATE()
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .direct_ship_ind, --DIRECT_SHIP_IND-VARCHAR2(1)
                                                      NULL, --PALLET_NAME-VARCHAR2(6)
                                                      NULL, --CASE_NAME-VARCHAR2(6)
                                                      NULL, --INNER_NAME-VARCHAR2(6)
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .XITMSUPCTYMFRDESC_TBL, --XITMSUPCTYMFRDESC_TBL-RIB_XItmSupCtyMfrDesc_TBL()
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .primary_case_size, --PRIMARY_CASE_SIZE-VARCHAR2(6)
                                                      NULL, --SUPP_DIFF_1-VARCHAR2(120)
                                                      NULL, --SUPP_DIFF_2-VARCHAR2(120)
                                                      NULL, --SUPP_DIFF_3-VARCHAR2(120)
                                                      NULL, --SUPP_DIFF_4-VARCHAR2(120)
                                                      NULL, --CONCESSION_RATE-NUMBER(12)
                                                      NULL); --LANGOFXITEMSUPDESC_TBL-RIB_LangOfXItemSupDesc_TBL()
        
        else
        
          L_message_sup_rec := "RIB_XItemSupDesc_REC"(0, --rib oid
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .SUPPLIER, --SUPPLIER-VARCHAR2(10)
                                                      'N', --PRIMARY_SUPP_IND-VARCHAR2(3)
                                                      upper(I_message.XITEMSUPDESC_TBL(i).vpn), --VPN-VARCHAR2(30)
                                                      NULL, --SUPP_LABEL-VARCHAR2(15)
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .XITEMSUPCTYDESC_TBL, --XITEMSUPCTYDESC_TBL-RIB_XItemSupCtyDesc_TBL()
                                                      NULL, --CONSIGNMENT_RATE-NUMBER(12)
                                                      NULL, --SUPP_DISCONTINUE_DATE-DATE()
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .direct_ship_ind, --DIRECT_SHIP_IND-VARCHAR2(1)
                                                      NULL, --PALLET_NAME-VARCHAR2(6)
                                                      NULL, --CASE_NAME-VARCHAR2(6)
                                                      NULL, --INNER_NAME-VARCHAR2(6)
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .XITMSUPCTYMFRDESC_TBL, --XITMSUPCTYMFRDESC_TBL-RIB_XItmSupCtyMfrDesc_TBL()
                                                      I_message.XITEMSUPDESC_TBL(i)
                                                      .primary_case_size, --PRIMARY_CASE_SIZE-VARCHAR2(6)
                                                      NULL, --SUPP_DIFF_1-VARCHAR2(120)
                                                      NULL, --SUPP_DIFF_2-VARCHAR2(120)
                                                      NULL, --SUPP_DIFF_3-VARCHAR2(120)
                                                      NULL, --SUPP_DIFF_4-VARCHAR2(120)
                                                      NULL, --CONCESSION_RATE-NUMBER(12)
                                                      NULL); --LANGOFXITEMSUPDESC_TBL-RIB_LangOfXItemSupDesc_TBL()
        
        end if;
        L_message_sup_tbl.extend();
        L_message_sup_tbl(L_message_sup_tbl.last) := L_message_sup_rec;
      end loop;
    
    end if;
  
    if I_message.tran_level < I_item_level then
       if (I_message.item_level = 1 and I_message.pack_ind = 'Y' and
          I_message.pack_type ='B') then
         
           L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                         I_item_id, --ITEM-VARCHAR2 (25)
                                         I_item_parent, --ITEM_PARENT-VARCHAR2(25)
                                         I_item_grandparent, --ITEM_GRANDPARENT-VARCHAR2(25)
                                         I_message.pack_ind, --PACK_IND-VARCHAR2(1)
                                         I_item_level, --ITEM_LEVEL-NUMBER(1)
                                         I_message.tran_level, --TRAN_LEVEL-NUMBER(1)
                                         I_message.diff_1, --DIFF_1-VARCHAR2(10)
                                         I_message.diff_2, --DIFF_2-VARCHAR2(10)
                                         I_message.diff_3, --DIFF_3-VARCHAR2(10)
                                         I_message.diff_4, --DIFF_4-VARCHAR2(10)
                                         I_message.dept, --DEPT-NUMBER(4)
                                         I_message.class, --CLASS-NUMBER(4)
                                         I_message.subclass, --SUBCLASS-NUMBER(4)
                                         I_message.item_desc, --ITEM_DESC-VARCHAR2(250)
                                         I_message.iscloc_hier_level, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                         I_message.izp_hier_level, --IZP_HIER_LEVEL-VARCHAR2(2)
                                         I_message.short_desc, --SHORT_DESC-VARCHAR2(120)
                                         null, --COST_ZONE_GROUP_ID-NUMBER(4)
                                         I_message.standard_uom, --STANDARD_UOM-VARCHAR2(4)
                                         I_message.store_ord_mult, --STORE_ORD_MULT-VARCHAR2(1)
                                         I_message.forecast_ind, --FORECAST_IND-VARCHAR2(1)
                                         I_message.simple_pack_ind, --SIMPLE_PACK_IND-VARCHAR2(1)
                                         I_message.CONTAINS_INNER_IND, --CONTAINS_INNER_IND-VARCHAR2(1)
                                         I_message.sellable_ind, --SELLABLE_IND-VARCHAR2(1)
                                         I_message.orderable_ind, --ORDERABLE_IND-VARCHAR2(1)
                                         null, --PACK_TYPE-VARCHAR2(1)
                                         null, --ORDER_AS_TYPE-VARCHAR2(1)
                                         I_message.comments, --COMMENTS-VARCHAR2(2000)
                                         I_message.create_datetime, --CREATE_DATETIME-DATE()
                                         I_message.XITEMCTRYDESC_TBL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                         L_message_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()--- preencher isto
                                         NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                         L_vat_item_tbl, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                         I_message.XIZPDESC_TBL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                         NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                         I_message.XITEMSEASON_TBL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                         NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                         I_message.status, --STATUS-VARCHAR2(1)
                                         I_message.uom_conv_factor, --UOM_CONV_FACTOR-NUMBER(20)
                                         I_message.package_size, --PACKAGE_SIZE-NUMBER(12)
                                         I_message.HANDLING_TEMP, --HANDLING_TEMP-VARCHAR2(6)
                                         I_message.HANDLING_SENSITIVITY, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                         I_message.MFG_REC_RETAIL, --MFG_REC_RETAIL-NUMBER(20)
                                         I_message.WASTE_TYPE, --WASTE_TYPE-VARCHAR2(6)
                                         I_message.WASTE_PCT, --WASTE_PCT-NUMBER(12)
                                         I_type, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                         I_message.CATCH_WEIGHT_IND, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                         I_message.const_dimen_ind, --CONST_DIMEN_IND-VARCHAR2(1)
                                         I_message.gift_wrap_ind, --GIFT_WRAP_IND-VARCHAR2(1)
                                         I_message.SHIP_ALONE_IND, --SHIP_ALONE_IND-VARCHAR2(1)
                                         I_message.EXT_SOURCE_SYSTEM, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                         I_message.SIZE_GROUP1, --SIZE_GROUP1-NUMBER(4)
                                         I_message.SIZE_GROUP2, --SIZE_GROUP2-NUMBER(4)
                                         I_message.SIZE1, --SIZE1-VARCHAR2(6)
                                         I_message.SIZE2, --SIZE2-VARCHAR2(6)
                                         I_message.COLOR, --COLOR-VARCHAR2(24)
                                         I_message.SYSTEM_IND, --SYSTEM_IND-VARCHAR2(1)
                                         I_message.UPC_SUPPLEMENT, --UPC_SUPPLEMENT-NUMBER(5)
                                         I_message.UPC_TYPE, --UPC_TYPE-VARCHAR2(5)
                                         I_message.PRIMARY_UPC_IND, --PRIMARY_UPC_IND-VARCHAR2(1)
                                         I_message.PRIMARY_REPL_IND, --PRIMARY_REPL_IND-VARCHAR2(1)
                                         I_message.ITEM_AGGREGATE_IND, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_1_AGGREGATE_IND, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_2_AGGREGATE_IND, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_3_AGGREGATE_IND, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_4_AGGREGATE_IND, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.perishable_ind, --PERISHABLE_IND-VARCHAR2(1)
                                         I_message.NOTIONAL_PACK_IND, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                         I_message.SOH_INQUIRY_AT_PACK_IND, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                         null, --AIP_CASE_TYPE-VARCHAR2(6)
                                         I_message.ORDER_TYPE, --ORDER_TYPE-VARCHAR2(6)
                                         I_message.SALE_TYPE, --SALE_TYPE-VARCHAR2(6)
                                         I_message.CATCH_WEIGHT_UOM, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                         I_message.deposit_item_type, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                         I_message.inventory_ind, --INVENTORY_IND-VARCHAR2(1)
                                         I_message.ITEM_XFORM_IND, --ITEM_XFORM_IND-VARCHAR2(1)
                                         I_message.container_item, --CONTAINER_ITEM-VARCHAR2(25)
                                         I_message.package_uom, --PACKAGE_UOM-VARCHAR2(4)
                                         I_message.format_id, --FORMAT_ID-VARCHAR2(1)
                                         I_message.prefix, --PREFIX-NUMBER(2)
                                         I_message.brand, --BRAND-VARCHAR2(120)
                                         I_message.product_classification, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                         I_message.item_desc_secondary, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                         I_message.desc_up, --DESC_UP-VARCHAR2(250)
                                         I_message.merchandise_ind, --MERCHANDISE_IND-VARCHAR2(1)
                                         I_message.ORIGINAL_RETAIL, --ORIGINAL_RETAIL-NUMBER(20)
                                         I_message.RETAIL_LABEL_TYPE, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                         I_message.RETAIL_LABEL_VALUE, --RETAIL_LABEL_VALUE-NUMBER(20)
                                         I_message.DEFAULT_WASTE_PCT, --DEFAULT_WASTE_PCT-NUMBER(12)
                                         I_message.ITEM_SERVICE_LEVEL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                         I_message.CHECK_UDA_IND, --CHECK_UDA_IND-VARCHAR2(1)
                                         I_message.DEPOSIT_IN_PRICE_PER_UOM, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                         I_message.attempt_rms_load, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                         I_message.LocOfXItemDesc,
                                         I_message.LangOfXItemDesc_TBL);
      
      
      elsif (I_message.tran_level = 1  and
         (I_message.orderable_ind = 'N' or I_message.pack_ind ='Y') ) 
          then
        L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                         I_item_id, --ITEM-VARCHAR2 (25)
                                         I_item_parent, --ITEM_PARENT-VARCHAR2(25)
                                         I_item_grandparent, --ITEM_GRANDPARENT-VARCHAR2(25)
                                         I_message.pack_ind, --PACK_IND-VARCHAR2(1)
                                         I_item_level, --ITEM_LEVEL-NUMBER(1)
                                         I_message.tran_level, --TRAN_LEVEL-NUMBER(1)
                                         I_message.diff_1, --DIFF_1-VARCHAR2(10)
                                         I_message.diff_2, --DIFF_2-VARCHAR2(10)
                                         I_message.diff_3, --DIFF_3-VARCHAR2(10)
                                         I_message.diff_4, --DIFF_4-VARCHAR2(10)
                                         I_message.dept, --DEPT-NUMBER(4)
                                         I_message.class, --CLASS-NUMBER(4)
                                         I_message.subclass, --SUBCLASS-NUMBER(4)
                                         I_message.item_desc, --ITEM_DESC-VARCHAR2(250)
                                         I_message.iscloc_hier_level, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                         I_message.izp_hier_level, --IZP_HIER_LEVEL-VARCHAR2(2)
                                         I_message.short_desc, --SHORT_DESC-VARCHAR2(120)
                                         I_message.cost_zone_group_id, --COST_ZONE_GROUP_ID-NUMBER(4)
                                         I_message.standard_uom, --STANDARD_UOM-VARCHAR2(4)
                                         I_message.store_ord_mult, --STORE_ORD_MULT-VARCHAR2(1)
                                         I_message.forecast_ind, --FORECAST_IND-VARCHAR2(1)
                                         I_message.simple_pack_ind, --SIMPLE_PACK_IND-VARCHAR2(1)
                                         I_message.CONTAINS_INNER_IND, --CONTAINS_INNER_IND-VARCHAR2(1)
                                         I_message.sellable_ind, --SELLABLE_IND-VARCHAR2(1)
                                         I_message.orderable_ind, --ORDERABLE_IND-VARCHAR2(1)
                                         I_message.pack_type, --PACK_TYPE-VARCHAR2(1)
                                         I_message.order_as_type, --ORDER_AS_TYPE-VARCHAR2(1)
                                         I_message.comments, --COMMENTS-VARCHAR2(2000)
                                         I_message.create_datetime, --CREATE_DATETIME-DATE()
                                         I_message.XITEMCTRYDESC_TBL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                         L_message_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()--- preencher isto
                                         NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                         L_vat_item_tbl, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                         I_message.XIZPDESC_TBL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                         NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                         I_message.XITEMSEASON_TBL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                         NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                         I_message.status, --STATUS-VARCHAR2(1)
                                         I_message.uom_conv_factor, --UOM_CONV_FACTOR-NUMBER(20)
                                         I_message.package_size, --PACKAGE_SIZE-NUMBER(12)
                                         I_message.HANDLING_TEMP, --HANDLING_TEMP-VARCHAR2(6)
                                         I_message.HANDLING_SENSITIVITY, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                         I_message.MFG_REC_RETAIL, --MFG_REC_RETAIL-NUMBER(20)
                                         I_message.WASTE_TYPE, --WASTE_TYPE-VARCHAR2(6)
                                         I_message.WASTE_PCT, --WASTE_PCT-NUMBER(12)
                                         I_type, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                         I_message.CATCH_WEIGHT_IND, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                         I_message.const_dimen_ind, --CONST_DIMEN_IND-VARCHAR2(1)
                                         I_message.gift_wrap_ind, --GIFT_WRAP_IND-VARCHAR2(1)
                                         I_message.SHIP_ALONE_IND, --SHIP_ALONE_IND-VARCHAR2(1)
                                         I_message.EXT_SOURCE_SYSTEM, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                         I_message.SIZE_GROUP1, --SIZE_GROUP1-NUMBER(4)
                                         I_message.SIZE_GROUP2, --SIZE_GROUP2-NUMBER(4)
                                         I_message.SIZE1, --SIZE1-VARCHAR2(6)
                                         I_message.SIZE2, --SIZE2-VARCHAR2(6)
                                         I_message.COLOR, --COLOR-VARCHAR2(24)
                                         I_message.SYSTEM_IND, --SYSTEM_IND-VARCHAR2(1)
                                         I_message.UPC_SUPPLEMENT, --UPC_SUPPLEMENT-NUMBER(5)
                                         I_message.UPC_TYPE, --UPC_TYPE-VARCHAR2(5)
                                         I_message.PRIMARY_UPC_IND, --PRIMARY_UPC_IND-VARCHAR2(1)
                                         I_message.PRIMARY_REPL_IND, --PRIMARY_REPL_IND-VARCHAR2(1)
                                         I_message.ITEM_AGGREGATE_IND, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_1_AGGREGATE_IND, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_2_AGGREGATE_IND, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_3_AGGREGATE_IND, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_4_AGGREGATE_IND, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.perishable_ind, --PERISHABLE_IND-VARCHAR2(1)
                                         I_message.NOTIONAL_PACK_IND, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                         I_message.SOH_INQUIRY_AT_PACK_IND, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                         null, --AIP_CASE_TYPE-VARCHAR2(6)
                                         I_message.ORDER_TYPE, --ORDER_TYPE-VARCHAR2(6)
                                         I_message.SALE_TYPE, --SALE_TYPE-VARCHAR2(6)
                                         I_message.CATCH_WEIGHT_UOM, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                         I_message.deposit_item_type, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                         I_message.inventory_ind, --INVENTORY_IND-VARCHAR2(1)
                                         I_message.ITEM_XFORM_IND, --ITEM_XFORM_IND-VARCHAR2(1)
                                         I_message.container_item, --CONTAINER_ITEM-VARCHAR2(25)
                                         I_message.package_uom, --PACKAGE_UOM-VARCHAR2(4)
                                         I_message.format_id, --FORMAT_ID-VARCHAR2(1)
                                         I_message.prefix, --PREFIX-NUMBER(2)
                                         I_message.brand, --BRAND-VARCHAR2(120)
                                         I_message.product_classification, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                         I_message.item_desc_secondary, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                         I_message.desc_up, --DESC_UP-VARCHAR2(250)
                                         I_message.merchandise_ind, --MERCHANDISE_IND-VARCHAR2(1)
                                         I_message.ORIGINAL_RETAIL, --ORIGINAL_RETAIL-NUMBER(20)
                                         I_message.RETAIL_LABEL_TYPE, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                         I_message.RETAIL_LABEL_VALUE, --RETAIL_LABEL_VALUE-NUMBER(20)
                                         I_message.DEFAULT_WASTE_PCT, --DEFAULT_WASTE_PCT-NUMBER(12)
                                         I_message.ITEM_SERVICE_LEVEL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                         I_message.CHECK_UDA_IND, --CHECK_UDA_IND-VARCHAR2(1)
                                         I_message.DEPOSIT_IN_PRICE_PER_UOM, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                         I_message.attempt_rms_load, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                         I_message.LocOfXItemDesc,
                                         I_message.LangOfXItemDesc_TBL);
      
             
         
         else
      
        L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                         I_item_id, --ITEM-VARCHAR2 (25)
                                         I_item_parent, --ITEM_PARENT-VARCHAR2(25)
                                         I_item_grandparent, --ITEM_GRANDPARENT-VARCHAR2(25)
                                         I_message.pack_ind, --PACK_IND-VARCHAR2(1)
                                         I_item_level, --ITEM_LEVEL-NUMBER(1)
                                         I_message.tran_level, --TRAN_LEVEL-NUMBER(1)
                                         I_message.diff_1, --DIFF_1-VARCHAR2(10)
                                         I_message.diff_2, --DIFF_2-VARCHAR2(10)
                                         I_message.diff_3, --DIFF_3-VARCHAR2(10)
                                         I_message.diff_4, --DIFF_4-VARCHAR2(10)
                                         I_message.dept, --DEPT-NUMBER(4)
                                         I_message.class, --CLASS-NUMBER(4)
                                         I_message.subclass, --SUBCLASS-NUMBER(4)
                                         I_message.item_desc, --ITEM_DESC-VARCHAR2(250)
                                         I_message.iscloc_hier_level, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                         I_message.izp_hier_level, --IZP_HIER_LEVEL-VARCHAR2(2)
                                         I_message.short_desc, --SHORT_DESC-VARCHAR2(120)
                                         I_message.cost_zone_group_id, --COST_ZONE_GROUP_ID-NUMBER(4)
                                         I_message.standard_uom, --STANDARD_UOM-VARCHAR2(4)
                                         I_message.store_ord_mult, --STORE_ORD_MULT-VARCHAR2(1)
                                         I_message.forecast_ind, --FORECAST_IND-VARCHAR2(1)
                                         I_message.simple_pack_ind, --SIMPLE_PACK_IND-VARCHAR2(1)
                                         I_message.CONTAINS_INNER_IND, --CONTAINS_INNER_IND-VARCHAR2(1)
                                         I_message.sellable_ind, --SELLABLE_IND-VARCHAR2(1)
                                         I_message.orderable_ind, --ORDERABLE_IND-VARCHAR2(1)
                                         I_message.pack_type, --PACK_TYPE-VARCHAR2(1)
                                         I_message.order_as_type, --ORDER_AS_TYPE-VARCHAR2(1)
                                         I_message.comments, --COMMENTS-VARCHAR2(2000)
                                         I_message.create_datetime, --CREATE_DATETIME-DATE()
                                         I_message.XITEMCTRYDESC_TBL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                         L_message_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()--- preencher isto
                                         NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                         L_vat_item_tbl, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                         I_message.XIZPDESC_TBL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                         NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                         I_message.XITEMSEASON_TBL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                         NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                         I_message.status, --STATUS-VARCHAR2(1)
                                         I_message.uom_conv_factor, --UOM_CONV_FACTOR-NUMBER(20)
                                         I_message.package_size, --PACKAGE_SIZE-NUMBER(12)
                                         I_message.HANDLING_TEMP, --HANDLING_TEMP-VARCHAR2(6)
                                         I_message.HANDLING_SENSITIVITY, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                         I_message.MFG_REC_RETAIL, --MFG_REC_RETAIL-NUMBER(20)
                                         I_message.WASTE_TYPE, --WASTE_TYPE-VARCHAR2(6)
                                         I_message.WASTE_PCT, --WASTE_PCT-NUMBER(12)
                                         I_type, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                         I_message.CATCH_WEIGHT_IND, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                         I_message.const_dimen_ind, --CONST_DIMEN_IND-VARCHAR2(1)
                                         I_message.gift_wrap_ind, --GIFT_WRAP_IND-VARCHAR2(1)
                                         I_message.SHIP_ALONE_IND, --SHIP_ALONE_IND-VARCHAR2(1)
                                         I_message.EXT_SOURCE_SYSTEM, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                         I_message.SIZE_GROUP1, --SIZE_GROUP1-NUMBER(4)
                                         I_message.SIZE_GROUP2, --SIZE_GROUP2-NUMBER(4)
                                         I_message.SIZE1, --SIZE1-VARCHAR2(6)
                                         I_message.SIZE2, --SIZE2-VARCHAR2(6)
                                         I_message.COLOR, --COLOR-VARCHAR2(24)
                                         I_message.SYSTEM_IND, --SYSTEM_IND-VARCHAR2(1)
                                         I_message.UPC_SUPPLEMENT, --UPC_SUPPLEMENT-NUMBER(5)
                                         I_message.UPC_TYPE, --UPC_TYPE-VARCHAR2(5)
                                         I_message.PRIMARY_UPC_IND, --PRIMARY_UPC_IND-VARCHAR2(1)
                                         I_message.PRIMARY_REPL_IND, --PRIMARY_REPL_IND-VARCHAR2(1)
                                         I_message.ITEM_AGGREGATE_IND, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_1_AGGREGATE_IND, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_2_AGGREGATE_IND, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_3_AGGREGATE_IND, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_4_AGGREGATE_IND, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.perishable_ind, --PERISHABLE_IND-VARCHAR2(1)
                                         I_message.NOTIONAL_PACK_IND, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                         I_message.SOH_INQUIRY_AT_PACK_IND, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                         I_message.aip_case_type, --AIP_CASE_TYPE-VARCHAR2(6)
                                         I_message.ORDER_TYPE, --ORDER_TYPE-VARCHAR2(6)
                                         I_message.SALE_TYPE, --SALE_TYPE-VARCHAR2(6)
                                         I_message.CATCH_WEIGHT_UOM, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                         I_message.deposit_item_type, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                         I_message.inventory_ind, --INVENTORY_IND-VARCHAR2(1)
                                         I_message.ITEM_XFORM_IND, --ITEM_XFORM_IND-VARCHAR2(1)
                                         I_message.container_item, --CONTAINER_ITEM-VARCHAR2(25)
                                         I_message.package_uom, --PACKAGE_UOM-VARCHAR2(4)
                                         I_message.format_id, --FORMAT_ID-VARCHAR2(1)
                                         I_message.prefix, --PREFIX-NUMBER(2)
                                         I_message.brand, --BRAND-VARCHAR2(120)
                                         I_message.product_classification, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                         I_message.item_desc_secondary, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                         I_message.desc_up, --DESC_UP-VARCHAR2(250)
                                         I_message.merchandise_ind, --MERCHANDISE_IND-VARCHAR2(1)
                                         I_message.ORIGINAL_RETAIL, --ORIGINAL_RETAIL-NUMBER(20)
                                         I_message.RETAIL_LABEL_TYPE, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                         I_message.RETAIL_LABEL_VALUE, --RETAIL_LABEL_VALUE-NUMBER(20)
                                         I_message.DEFAULT_WASTE_PCT, --DEFAULT_WASTE_PCT-NUMBER(12)
                                         I_message.ITEM_SERVICE_LEVEL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                         I_message.CHECK_UDA_IND, --CHECK_UDA_IND-VARCHAR2(1)
                                         I_message.DEPOSIT_IN_PRICE_PER_UOM, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                         I_message.attempt_rms_load, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                         I_message.LocOfXItemDesc,
                                         I_message.LangOfXItemDesc_TBL);
      end if;
    
    else
      if I_message.XITEMIMAGE_TBL is not null and
         I_message.XITEMIMAGE_TBL.count() > 0 then
        L_message_image_tbl := "RIB_XItemImage_TBL"();
        for i in 1 .. I_message.XITEMIMAGE_TBL.count() loop
          if i = 1 then
            L_message_image_rec := "RIB_XItemImage_REC"(0, --rib oid
                                                        I_message.XITEMIMAGE_TBL(i)
                                                        .image_name, --IMAGE_NAME-VARCHAR2(120)
                                                        I_message.XITEMIMAGE_TBL(i)
                                                        .image_addr, --IMAGE_ADDR-VARCHAR2(255)
                                                        I_message.XITEMIMAGE_TBL(i)
                                                        .image_desc, --IMAGE_DESC-VARCHAR2(40)
                                                        NULL, --CREATE_DATETIME-DATE()
                                                        NULL, --LAST_UPDATE_DATETIME-DATE()
                                                        NULL, --LAST_UPDATE_ID-VARCHAR2(30)
                                                        null,
                                                        I_message.XITEMIMAGE_TBL(i)
                                                        .image_type, --IMAGE_TYPE-VARCHAR2(6)
                                                        'Y', --PRIMARY_IND-VARCHAR2(1)
                                                        i, --DISPLAY_PRIORITY-NUMBER(4)
                                                        I_message.XITEMIMAGE_TBL(i)
                                                        .langofxitemimage_tbl); --LANGOFXITEMIMAGE_TBL-RIB_LangOfXItemImage_TBL()
          
          else
            L_message_image_rec := "RIB_XItemImage_REC"(0, --rib oid
                                                        I_message.XITEMIMAGE_TBL(i)
                                                        .image_name, --IMAGE_NAME-VARCHAR2(120)
                                                        I_message.XITEMIMAGE_TBL(i)
                                                        .image_addr, --IMAGE_ADDR-VARCHAR2(255)
                                                        I_message.XITEMIMAGE_TBL(i)
                                                        .image_desc, --IMAGE_DESC-VARCHAR2(40)
                                                        NULL, --CREATE_DATETIME-DATE()
                                                        NULL, --LAST_UPDATE_DATETIME-DATE()
                                                        NULL, --LAST_UPDATE_ID-VARCHAR2(30)
                                                        null,
                                                        I_message.XITEMIMAGE_TBL(i)
                                                        .image_type, --IMAGE_TYPE-VARCHAR2(6)
                                                        'N', --PRIMARY_IND-VARCHAR2(1)
                                                        i, --DISPLAY_PRIORITY-NUMBER(4)
                                                        I_message.XITEMIMAGE_TBL(i)
                                                        .langofxitemimage_tbl); --LANGOFXITEMIMAGE_TBL-RIB_LangOfXItemImage_TBL()
          end if;
          L_message_image_tbl.extend();
          L_message_image_tbl(L_message_image_tbl.last) := L_message_image_rec;
        end loop;
      
      end if;
    
      if I_message.XITEMUDADTL_TBL is not null and
         I_message.XITEMUDADTL_TBL.count() > 0 then
        for i in 1 .. I_message.XITEMUDADTL_TBL.count() loop
          L_message_uda_rec := "RIB_XItemUDADtl_REC"(0, --rib oid
                                                     I_message.XITEMUDADTL_TBL(i)
                                                     .uda_id, --UDA_ID-NUMBER(5)
                                                     I_message.XITEMUDADTL_TBL(i)
                                                     .display_type, --DISPLAY_TYPE-VARCHAR2(2)
                                                     I_message.XITEMUDADTL_TBL(i)
                                                     .uda_date, --UDA_DATE-DATE()
                                                     I_message.XITEMUDADTL_TBL(i)
                                                     .uda_value, --UDA_VALUE-VARCHAR2(30)
                                                     I_message.XITEMUDADTL_TBL(i)
                                                     .uda_text, --UDA_TEXT-VARCHAR2(250)
                                                     I_message.XITEMUDADTL_TBL(i)
                                                     .create_datetime, --CREATE_DATETIME-DATE()
                                                     I_message.XITEMUDADTL_TBL(i)
                                                     .last_update_datetime, --LAST_UPDATE_DATETIME-DATE()
                                                     I_message.XITEMUDADTL_TBL(i)
                                                     .last_update_id); --LAST_UPDATE_ID-VARCHAR2(30)
        
          if L_message_uda_tbl is null then
            L_message_uda_tbl := "RIB_XItemUDADtl_TBL"();
          end if;
          L_message_uda_tbl.extend();
          L_message_uda_tbl(L_message_uda_tbl.last) := L_message_uda_rec;
        
        end loop;
      
      end if;
    
      for val in (Select to_number(value_1) as uda_id,
                         to_number(value_2) as uda_value
                    from XXADEO_MOM_DVM
                   where bu = to_number(I_message.EXTOFXITEMDESC_TBL(1).bu)
                     and func_area = 'UDA'
                     and parameter = 'LIFECYCLE_STATUS') loop
      
        L_message_uda_rec := "RIB_XItemUDADtl_REC"(0, --rib oid
                                                   val.uda_id, --UDA_ID-NUMBER(5)
                                                   'LV', --DISPLAY_TYPE-VARCHAR2(2)
                                                   NULL, --UDA_DATE-DATE()
                                                   val.uda_value, --UDA_VALUE-VARCHAR2(30)
                                                   NULL, --UDA_TEXT-VARCHAR2(250)
                                                   NULL, --CREATE_DATETIME-DATE()
                                                   NULL, --LAST_UPDATE_DATETIME-DATE()
                                                   NULL); --LAST_UPDATE_ID-VARCHAR2(30)
      
        if L_message_uda_tbl is null then
          L_message_uda_tbl := "RIB_XItemUDADtl_TBL"();
        end if;
        L_message_uda_tbl.extend();
        L_message_uda_tbl(L_message_uda_tbl.last) := L_message_uda_rec;
      end loop;
    
      if (I_message.item_level = 1 and I_message.pack_ind = 'Y' and
         I_message.sellable_ind = 'Y' and I_message.orderable_ind = 'N' and
         I_message.inventory_ind = 'Y'  and I_message.pack_type ='B')
         then
      
        L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                         I_item_id, --ITEM-VARCHAR2(25)
                                         I_item_parent, --ITEM_PARENT-VARCHAR2(25)
                                         I_item_grandparent, --ITEM_GRANDPARENT-VARCHAR2(25)
                                         I_message.pack_ind, --PACK_IND-VARCHAR2(1)
                                         I_item_level, --ITEM_LEVEL-NUMBER(1)
                                         I_message.tran_level, --TRAN_LEVEL-NUMBER(1)
                                         I_message.diff_1, --DIFF_1-VARCHAR2(10)
                                         I_message.diff_2, --DIFF_2-VARCHAR2(10)
                                         I_message.diff_3, --DIFF_3-VARCHAR2(10)
                                         I_message.diff_4, --DIFF_4-VARCHAR2(10)
                                         I_message.dept, --DEPT-NUMBER(4)
                                         I_message.class, --CLASS-NUMBER(4)
                                         I_message.subclass, --SUBCLASS-NUMBER(4)
                                         I_message.item_desc, --ITEM_DESC-VARCHAR2(250)
                                         I_message.iscloc_hier_level, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                         I_message.izp_hier_level, --IZP_HIER_LEVEL-VARCHAR2(2)
                                         I_message.short_desc, --SHORT_DESC-VARCHAR2(120)
                                         NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                         I_message.standard_uom, --STANDARD_UOM-VARCHAR2(4)
                                         I_message.store_ord_mult, --STORE_ORD_MULT-VARCHAR2(1)
                                         I_message.forecast_ind, --FORECAST_IND-VARCHAR2(1)
                                         I_message.simple_pack_ind, --SIMPLE_PACK_IND-VARCHAR2(1)
                                         I_message.contains_inner_ind, --CONTAINS_INNER_IND-VARCHAR2(1)
                                         I_message.sellable_ind, --SELLABLE_IND-VARCHAR2(1)
                                         I_message.orderable_ind, --ORDERABLE_IND-VARCHAR2(1)
                                         null, --PACK_TYPE-VARCHAR2(1)
                                         null, --ORDER_AS_TYPE-VARCHAR2(1)
                                         I_message.comments, --COMMENTS-VARCHAR2(2000)
                                         I_message.create_datetime, --CREATE_DATETIME-DATE()
                                         I_message.XITEMCTRYDESC_TBL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                         L_message_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()--- preencher isto
                                         I_message.XITEMBOMDESC_TBL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                         L_vat_item_tbl, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                         I_message.XIZPDESC_TBL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                         L_message_uda_tbl, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                         I_message.XITEMSEASON_TBL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                         L_message_image_tbl, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                         I_message.status, --STATUS-VARCHAR2(1)
                                         I_message.uom_conv_factor, --UOM_CONV_FACTOR-NUMBER(20)
                                         I_message.package_size, --PACKAGE_SIZE-NUMBER(12)
                                         I_message.HANDLING_TEMP, --HANDLING_TEMP-VARCHAR2(6)
                                         I_message.HANDLING_SENSITIVITY, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                         I_message.MFG_REC_RETAIL, --MFG_REC_RETAIL-NUMBER(20)
                                         I_message.WASTE_TYPE, --WASTE_TYPE-VARCHAR2(6)
                                         I_message.WASTE_PCT, --WASTE_PCT-NUMBER(12)
                                         I_type, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                         I_message.CATCH_WEIGHT_IND, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                         I_message.const_dimen_ind, --CONST_DIMEN_IND-VARCHAR2(1)
                                         I_message.gift_wrap_ind, --GIFT_WRAP_IND-VARCHAR2(1)
                                         I_message.SHIP_ALONE_IND, --SHIP_ALONE_IND-VARCHAR2(1)
                                         I_message.EXT_SOURCE_SYSTEM, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                         I_message.SIZE_GROUP1, --SIZE_GROUP1-NUMBER(4)
                                         I_message.SIZE_GROUP2, --SIZE_GROUP2-NUMBER(4)
                                         I_message.SIZE1, --SIZE1-VARCHAR2(6)
                                         I_message.SIZE2, --SIZE2-VARCHAR2(6)
                                         I_message.COLOR, --COLOR-VARCHAR2(24)
                                         I_message.SYSTEM_IND, --SYSTEM_IND-VARCHAR2(1)
                                         I_message.UPC_SUPPLEMENT, --UPC_SUPPLEMENT-NUMBER(5)
                                         I_message.UPC_TYPE, --UPC_TYPE-VARCHAR2(5)
                                         I_message.PRIMARY_UPC_IND, --PRIMARY_UPC_IND-VARCHAR2(1)
                                         I_message.PRIMARY_REPL_IND, --PRIMARY_REPL_IND-VARCHAR2(1)
                                         I_message.ITEM_AGGREGATE_IND, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_1_AGGREGATE_IND, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_2_AGGREGATE_IND, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_3_AGGREGATE_IND, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.DIFF_4_AGGREGATE_IND, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                         I_message.perishable_ind, --PERISHABLE_IND-VARCHAR2(1)
                                         I_message.NOTIONAL_PACK_IND, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                         I_message.SOH_INQUIRY_AT_PACK_IND, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                         null, --AIP_CASE_TYPE-VARCHAR2(6)
                                         I_message.ORDER_TYPE, --ORDER_TYPE-VARCHAR2(6)
                                         I_message.SALE_TYPE, --SALE_TYPE-VARCHAR2(6)
                                         I_message.CATCH_WEIGHT_UOM, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                         I_message.deposit_item_type, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                         I_message.inventory_ind, --INVENTORY_IND-VARCHAR2(1)
                                         I_message.ITEM_XFORM_IND, --ITEM_XFORM_IND-VARCHAR2(1)
                                         I_message.container_item, --CONTAINER_ITEM-VARCHAR2(25)
                                         I_message.package_uom, --PACKAGE_UOM-VARCHAR2(4)
                                         I_message.format_id, --FORMAT_ID-VARCHAR2(1)
                                         I_message.prefix, --PREFIX-NUMBER(2)
                                         I_message.brand, --BRAND-VARCHAR2(120)
                                         I_message.product_classification, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                         I_message.item_desc_secondary, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                         I_message.desc_up, --DESC_UP-VARCHAR2(250)
                                         I_message.merchandise_ind, --MERCHANDISE_IND-VARCHAR2(1)
                                         I_message.ORIGINAL_RETAIL, --ORIGINAL_RETAIL-NUMBER(20)
                                         I_message.RETAIL_LABEL_TYPE, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                         I_message.RETAIL_LABEL_VALUE, --RETAIL_LABEL_VALUE-NUMBER(20)
                                         I_message.DEFAULT_WASTE_PCT, --DEFAULT_WASTE_PCT-NUMBER(12)
                                         I_message.ITEM_SERVICE_LEVEL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                         I_message.CHECK_UDA_IND, --CHECK_UDA_IND-VARCHAR2(1)
                                         I_message.DEPOSIT_IN_PRICE_PER_UOM, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                         I_message.attempT_rms_load, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                         I_message.LocOfXItemDesc,
                                         I_message.LangOfXItemDesc_TBL);
      
      elsif (I_message.tran_level =1 and 
           (I_message.orderable_ind = 'N' or I_message.pack_ind ='Y'))  then
        
          L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                           I_item_id, --ITEM-VARCHAR2(25)
                                           I_item_parent, --ITEM_PARENT-VARCHAR2(25)
                                           I_item_grandparent, --ITEM_GRANDPARENT-VARCHAR2(25)
                                           I_message.pack_ind, --PACK_IND-VARCHAR2(1)
                                           I_item_level, --ITEM_LEVEL-NUMBER(1)
                                           I_message.tran_level, --TRAN_LEVEL-NUMBER(1)
                                           I_message.diff_1, --DIFF_1-VARCHAR2(10)
                                           I_message.diff_2, --DIFF_2-VARCHAR2(10)
                                           I_message.diff_3, --DIFF_3-VARCHAR2(10)
                                           I_message.diff_4, --DIFF_4-VARCHAR2(10)
                                           I_message.dept, --DEPT-NUMBER(4)
                                           I_message.class, --CLASS-NUMBER(4)
                                           I_message.subclass, --SUBCLASS-NUMBER(4)
                                           I_message.item_desc, --ITEM_DESC-VARCHAR2(250)
                                           I_message.iscloc_hier_level, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                           I_message.izp_hier_level, --IZP_HIER_LEVEL-VARCHAR2(2)
                                           I_message.short_desc, --SHORT_DESC-VARCHAR2(120)
                                           I_message.cost_zone_group_id, --COST_ZONE_GROUP_ID-NUMBER(4)
                                           I_message.standard_uom, --STANDARD_UOM-VARCHAR2(4)
                                           I_message.store_ord_mult, --STORE_ORD_MULT-VARCHAR2(1)
                                           I_message.forecast_ind, --FORECAST_IND-VARCHAR2(1)
                                           I_message.simple_pack_ind, --SIMPLE_PACK_IND-VARCHAR2(1)
                                           I_message.contains_inner_ind, --CONTAINS_INNER_IND-VARCHAR2(1)
                                           I_message.sellable_ind, --SELLABLE_IND-VARCHAR2(1)
                                           I_message.orderable_ind, --ORDERABLE_IND-VARCHAR2(1)
                                           I_message.pack_type, --PACK_TYPE-VARCHAR2(1)
                                           I_message.order_as_type, --ORDER_AS_TYPE-VARCHAR2(1)
                                           I_message.comments, --COMMENTS-VARCHAR2(2000)
                                           I_message.create_datetime, --CREATE_DATETIME-DATE()
                                           I_message.XITEMCTRYDESC_TBL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                           L_message_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()--- preencher isto
                                           I_message.XITEMBOMDESC_TBL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                           L_vat_item_tbl, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                           I_message.XIZPDESC_TBL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                           L_message_uda_tbl, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                           I_message.XITEMSEASON_TBL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                           L_message_image_tbl, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                           I_message.status, --STATUS-VARCHAR2(1)
                                           I_message.uom_conv_factor, --UOM_CONV_FACTOR-NUMBER(20)
                                           I_message.package_size, --PACKAGE_SIZE-NUMBER(12)
                                           I_message.HANDLING_TEMP, --HANDLING_TEMP-VARCHAR2(6)
                                           I_message.HANDLING_SENSITIVITY, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                           I_message.MFG_REC_RETAIL, --MFG_REC_RETAIL-NUMBER(20)
                                           I_message.WASTE_TYPE, --WASTE_TYPE-VARCHAR2(6)
                                           I_message.WASTE_PCT, --WASTE_PCT-NUMBER(12)
                                           I_type, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                           I_message.CATCH_WEIGHT_IND, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                           I_message.const_dimen_ind, --CONST_DIMEN_IND-VARCHAR2(1)
                                           I_message.gift_wrap_ind, --GIFT_WRAP_IND-VARCHAR2(1)
                                           I_message.SHIP_ALONE_IND, --SHIP_ALONE_IND-VARCHAR2(1)
                                           I_message.EXT_SOURCE_SYSTEM, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                           I_message.SIZE_GROUP1, --SIZE_GROUP1-NUMBER(4)
                                           I_message.SIZE_GROUP2, --SIZE_GROUP2-NUMBER(4)
                                           I_message.SIZE1, --SIZE1-VARCHAR2(6)
                                           I_message.SIZE2, --SIZE2-VARCHAR2(6)
                                           I_message.COLOR, --COLOR-VARCHAR2(24)
                                           I_message.SYSTEM_IND, --SYSTEM_IND-VARCHAR2(1)
                                           I_message.UPC_SUPPLEMENT, --UPC_SUPPLEMENT-NUMBER(5)
                                           I_message.UPC_TYPE, --UPC_TYPE-VARCHAR2(5)
                                           I_message.PRIMARY_UPC_IND, --PRIMARY_UPC_IND-VARCHAR2(1)
                                           I_message.PRIMARY_REPL_IND, --PRIMARY_REPL_IND-VARCHAR2(1)
                                           I_message.ITEM_AGGREGATE_IND, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.DIFF_1_AGGREGATE_IND, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.DIFF_2_AGGREGATE_IND, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.DIFF_3_AGGREGATE_IND, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.DIFF_4_AGGREGATE_IND, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.perishable_ind, --PERISHABLE_IND-VARCHAR2(1)
                                           I_message.NOTIONAL_PACK_IND, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                           I_message.SOH_INQUIRY_AT_PACK_IND, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                           NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                           I_message.ORDER_TYPE, --ORDER_TYPE-VARCHAR2(6)
                                           I_message.SALE_TYPE, --SALE_TYPE-VARCHAR2(6)
                                           I_message.CATCH_WEIGHT_UOM, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                           I_message.deposit_item_type, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                           I_message.inventory_ind, --INVENTORY_IND-VARCHAR2(1)
                                           I_message.ITEM_XFORM_IND, --ITEM_XFORM_IND-VARCHAR2(1)
                                           I_message.container_item, --CONTAINER_ITEM-VARCHAR2(25)
                                           I_message.package_uom, --PACKAGE_UOM-VARCHAR2(4)
                                           I_message.format_id, --FORMAT_ID-VARCHAR2(1)
                                           I_message.prefix, --PREFIX-NUMBER(2)
                                           I_message.brand, --BRAND-VARCHAR2(120)
                                           I_message.product_classification, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                           I_message.item_desc_secondary, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                           I_message.desc_up, --DESC_UP-VARCHAR2(250)
                                           I_message.merchandise_ind, --MERCHANDISE_IND-VARCHAR2(1)
                                           I_message.ORIGINAL_RETAIL, --ORIGINAL_RETAIL-NUMBER(20)
                                           I_message.RETAIL_LABEL_TYPE, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                           I_message.RETAIL_LABEL_VALUE, --RETAIL_LABEL_VALUE-NUMBER(20)
                                           I_message.DEFAULT_WASTE_PCT, --DEFAULT_WASTE_PCT-NUMBER(12)
                                           I_message.ITEM_SERVICE_LEVEL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                           I_message.CHECK_UDA_IND, --CHECK_UDA_IND-VARCHAR2(1)
                                           I_message.DEPOSIT_IN_PRICE_PER_UOM, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                           I_message.attempT_rms_load, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                           I_message.LocOfXItemDesc,
                                           I_message.LangOfXItemDesc_TBL);
       
       
        
        elsif I_message.tran_level = 2 and I_item_level = 2 then
          L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                           I_item_id, --ITEM-VARCHAR2(25)
                                           I_item_parent, --ITEM_PARENT-VARCHAR2(25)
                                           I_item_grandparent, --ITEM_GRANDPARENT-VARCHAR2(25)
                                           I_message.pack_ind, --PACK_IND-VARCHAR2(1)
                                           I_item_level, --ITEM_LEVEL-NUMBER(1)
                                           I_message.tran_level, --TRAN_LEVEL-NUMBER(1)
                                           I_message.diff_1, --DIFF_1-VARCHAR2(10)
                                           I_message.diff_2, --DIFF_2-VARCHAR2(10)
                                           I_message.diff_3, --DIFF_3-VARCHAR2(10)
                                           I_message.diff_4, --DIFF_4-VARCHAR2(10)
                                           I_message.dept, --DEPT-NUMBER(4)
                                           I_message.class, --CLASS-NUMBER(4)
                                           I_message.subclass, --SUBCLASS-NUMBER(4)
                                           I_message.item_desc, --ITEM_DESC-VARCHAR2(250)
                                           I_message.iscloc_hier_level, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                           I_message.izp_hier_level, --IZP_HIER_LEVEL-VARCHAR2(2)
                                           I_message.short_desc, --SHORT_DESC-VARCHAR2(120)
                                           I_message.cost_zone_group_id, --COST_ZONE_GROUP_ID-NUMBER(4)
                                           I_message.standard_uom, --STANDARD_UOM-VARCHAR2(4)
                                           I_message.store_ord_mult, --STORE_ORD_MULT-VARCHAR2(1)
                                           I_message.forecast_ind, --FORECAST_IND-VARCHAR2(1)
                                           I_message.simple_pack_ind, --SIMPLE_PACK_IND-VARCHAR2(1)
                                           I_message.contains_inner_ind, --CONTAINS_INNER_IND-VARCHAR2(1)
                                           I_message.sellable_ind, --SELLABLE_IND-VARCHAR2(1)
                                           I_message.orderable_ind, --ORDERABLE_IND-VARCHAR2(1)
                                           I_message.pack_type, --PACK_TYPE-VARCHAR2(1)
                                           I_message.order_as_type, --ORDER_AS_TYPE-VARCHAR2(1)
                                           I_message.comments, --COMMENTS-VARCHAR2(2000)
                                           I_message.create_datetime, --CREATE_DATETIME-DATE()
                                           I_message.XITEMCTRYDESC_TBL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                           L_message_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()--- preencher isto
                                           I_message.XITEMBOMDESC_TBL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                           L_vat_item_tbl, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                           I_message.XIZPDESC_TBL,
                                           null, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                           --L_message_uda_tbl, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                           I_message.XITEMSEASON_TBL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                           NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                           I_message.status, --STATUS-VARCHAR2(1)
                                           I_message.uom_conv_factor, --UOM_CONV_FACTOR-NUMBER(20)
                                           I_message.package_size, --PACKAGE_SIZE-NUMBER(12)
                                           I_message.HANDLING_TEMP, --HANDLING_TEMP-VARCHAR2(6)
                                           I_message.HANDLING_SENSITIVITY, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                           I_message.MFG_REC_RETAIL, --MFG_REC_RETAIL-NUMBER(20)
                                           I_message.WASTE_TYPE, --WASTE_TYPE-VARCHAR2(6)
                                           I_message.WASTE_PCT, --WASTE_PCT-NUMBER(12)
                                           I_type, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                           I_message.CATCH_WEIGHT_IND, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                           I_message.const_dimen_ind, --CONST_DIMEN_IND-VARCHAR2(1)
                                           I_message.gift_wrap_ind, --GIFT_WRAP_IND-VARCHAR2(1)
                                           I_message.SHIP_ALONE_IND, --SHIP_ALONE_IND-VARCHAR2(1)
                                           I_message.EXT_SOURCE_SYSTEM, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                           I_message.SIZE_GROUP1, --SIZE_GROUP1-NUMBER(4)
                                           I_message.SIZE_GROUP2, --SIZE_GROUP2-NUMBER(4)
                                           I_message.SIZE1, --SIZE1-VARCHAR2(6)
                                           I_message.SIZE2, --SIZE2-VARCHAR2(6)
                                           I_message.COLOR, --COLOR-VARCHAR2(24)
                                           I_message.SYSTEM_IND, --SYSTEM_IND-VARCHAR2(1)
                                           I_message.UPC_SUPPLEMENT, --UPC_SUPPLEMENT-NUMBER(5)
                                           I_message.UPC_TYPE, --UPC_TYPE-VARCHAR2(5)
                                           I_message.PRIMARY_UPC_IND, --PRIMARY_UPC_IND-VARCHAR2(1)
                                           I_message.PRIMARY_REPL_IND, --PRIMARY_REPL_IND-VARCHAR2(1)
                                           I_message.ITEM_AGGREGATE_IND, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.DIFF_1_AGGREGATE_IND, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.DIFF_2_AGGREGATE_IND, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.DIFF_3_AGGREGATE_IND, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.DIFF_4_AGGREGATE_IND, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.perishable_ind, --PERISHABLE_IND-VARCHAR2(1)
                                           I_message.NOTIONAL_PACK_IND, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                           I_message.SOH_INQUIRY_AT_PACK_IND, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                           I_message.aip_case_type, --AIP_CASE_TYPE-VARCHAR2(6)
                                           I_message.ORDER_TYPE, --ORDER_TYPE-VARCHAR2(6)
                                           I_message.SALE_TYPE, --SALE_TYPE-VARCHAR2(6)
                                           I_message.CATCH_WEIGHT_UOM, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                           I_message.deposit_item_type, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                           I_message.inventory_ind, --INVENTORY_IND-VARCHAR2(1)
                                           I_message.ITEM_XFORM_IND, --ITEM_XFORM_IND-VARCHAR2(1)
                                           I_message.container_item, --CONTAINER_ITEM-VARCHAR2(25)
                                           I_message.package_uom, --PACKAGE_UOM-VARCHAR2(4)
                                           I_message.format_id, --FORMAT_ID-VARCHAR2(1)
                                           I_message.prefix, --PREFIX-NUMBER(2)
                                           I_message.brand, --BRAND-VARCHAR2(120)
                                           I_message.product_classification, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                           I_message.item_desc_secondary, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                           I_message.desc_up, --DESC_UP-VARCHAR2(250)
                                           I_message.merchandise_ind, --MERCHANDISE_IND-VARCHAR2(1)
                                           I_message.ORIGINAL_RETAIL, --ORIGINAL_RETAIL-NUMBER(20)
                                           I_message.RETAIL_LABEL_TYPE, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                           I_message.RETAIL_LABEL_VALUE, --RETAIL_LABEL_VALUE-NUMBER(20)
                                           I_message.DEFAULT_WASTE_PCT, --DEFAULT_WASTE_PCT-NUMBER(12)
                                           I_message.ITEM_SERVICE_LEVEL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                           I_message.CHECK_UDA_IND, --CHECK_UDA_IND-VARCHAR2(1)
                                           I_message.DEPOSIT_IN_PRICE_PER_UOM, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                           I_message.attempT_rms_load, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                           I_message.LocOfXItemDesc,
                                           I_message.LangOfXItemDesc_TBL);
        else
        
          L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                           I_item_id, --ITEM-VARCHAR2(25)
                                           I_item_parent, --ITEM_PARENT-VARCHAR2(25)
                                           I_item_grandparent, --ITEM_GRANDPARENT-VARCHAR2(25)
                                           I_message.pack_ind, --PACK_IND-VARCHAR2(1)
                                           I_item_level, --ITEM_LEVEL-NUMBER(1)
                                           I_message.tran_level, --TRAN_LEVEL-NUMBER(1)
                                           I_message.diff_1, --DIFF_1-VARCHAR2(10)
                                           I_message.diff_2, --DIFF_2-VARCHAR2(10)
                                           I_message.diff_3, --DIFF_3-VARCHAR2(10)
                                           I_message.diff_4, --DIFF_4-VARCHAR2(10)
                                           I_message.dept, --DEPT-NUMBER(4)
                                           I_message.class, --CLASS-NUMBER(4)
                                           I_message.subclass, --SUBCLASS-NUMBER(4)
                                           I_message.item_desc, --ITEM_DESC-VARCHAR2(250)
                                           I_message.iscloc_hier_level, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                           I_message.izp_hier_level, --IZP_HIER_LEVEL-VARCHAR2(2)
                                           I_message.short_desc, --SHORT_DESC-VARCHAR2(120)
                                           I_message.cost_zone_group_id, --COST_ZONE_GROUP_ID-NUMBER(4)
                                           I_message.standard_uom, --STANDARD_UOM-VARCHAR2(4)
                                           I_message.store_ord_mult, --STORE_ORD_MULT-VARCHAR2(1)
                                           I_message.forecast_ind, --FORECAST_IND-VARCHAR2(1)
                                           I_message.simple_pack_ind, --SIMPLE_PACK_IND-VARCHAR2(1)
                                           I_message.contains_inner_ind, --CONTAINS_INNER_IND-VARCHAR2(1)
                                           I_message.sellable_ind, --SELLABLE_IND-VARCHAR2(1)
                                           I_message.orderable_ind, --ORDERABLE_IND-VARCHAR2(1)
                                           I_message.pack_type, --PACK_TYPE-VARCHAR2(1)
                                           I_message.order_as_type, --ORDER_AS_TYPE-VARCHAR2(1)
                                           I_message.comments, --COMMENTS-VARCHAR2(2000)
                                           I_message.create_datetime, --CREATE_DATETIME-DATE()
                                           I_message.XITEMCTRYDESC_TBL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                           L_message_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()--- preencher isto
                                           I_message.XITEMBOMDESC_TBL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                           L_vat_item_tbl, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                           I_message.XIZPDESC_TBL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                           L_message_uda_tbl, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                           I_message.XITEMSEASON_TBL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                           L_message_image_tbl, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                           I_message.status, --STATUS-VARCHAR2(1)
                                           I_message.uom_conv_factor, --UOM_CONV_FACTOR-NUMBER(20)
                                           I_message.package_size, --PACKAGE_SIZE-NUMBER(12)
                                           I_message.HANDLING_TEMP, --HANDLING_TEMP-VARCHAR2(6)
                                           I_message.HANDLING_SENSITIVITY, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                           I_message.MFG_REC_RETAIL, --MFG_REC_RETAIL-NUMBER(20)
                                           I_message.WASTE_TYPE, --WASTE_TYPE-VARCHAR2(6)
                                           I_message.WASTE_PCT, --WASTE_PCT-NUMBER(12)
                                           I_type, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                           I_message.CATCH_WEIGHT_IND, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                           I_message.const_dimen_ind, --CONST_DIMEN_IND-VARCHAR2(1)
                                           I_message.gift_wrap_ind, --GIFT_WRAP_IND-VARCHAR2(1)
                                           I_message.SHIP_ALONE_IND, --SHIP_ALONE_IND-VARCHAR2(1)
                                           I_message.EXT_SOURCE_SYSTEM, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                           I_message.SIZE_GROUP1, --SIZE_GROUP1-NUMBER(4)
                                           I_message.SIZE_GROUP2, --SIZE_GROUP2-NUMBER(4)
                                           I_message.SIZE1, --SIZE1-VARCHAR2(6)
                                           I_message.SIZE2, --SIZE2-VARCHAR2(6)
                                           I_message.COLOR, --COLOR-VARCHAR2(24)
                                           I_message.SYSTEM_IND, --SYSTEM_IND-VARCHAR2(1)
                                           I_message.UPC_SUPPLEMENT, --UPC_SUPPLEMENT-NUMBER(5)
                                           I_message.UPC_TYPE, --UPC_TYPE-VARCHAR2(5)
                                           I_message.PRIMARY_UPC_IND, --PRIMARY_UPC_IND-VARCHAR2(1)
                                           I_message.PRIMARY_REPL_IND, --PRIMARY_REPL_IND-VARCHAR2(1)
                                           I_message.ITEM_AGGREGATE_IND, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.DIFF_1_AGGREGATE_IND, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.DIFF_2_AGGREGATE_IND, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.DIFF_3_AGGREGATE_IND, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.DIFF_4_AGGREGATE_IND, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                           I_message.perishable_ind, --PERISHABLE_IND-VARCHAR2(1)
                                           I_message.NOTIONAL_PACK_IND, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                           I_message.SOH_INQUIRY_AT_PACK_IND, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                           I_message.aip_case_type, --AIP_CASE_TYPE-VARCHAR2(6)
                                           I_message.ORDER_TYPE, --ORDER_TYPE-VARCHAR2(6)
                                           I_message.SALE_TYPE, --SALE_TYPE-VARCHAR2(6)
                                           I_message.CATCH_WEIGHT_UOM, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                           I_message.deposit_item_type, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                           I_message.inventory_ind, --INVENTORY_IND-VARCHAR2(1)
                                           I_message.ITEM_XFORM_IND, --ITEM_XFORM_IND-VARCHAR2(1)
                                           I_message.container_item, --CONTAINER_ITEM-VARCHAR2(25)
                                           I_message.package_uom, --PACKAGE_UOM-VARCHAR2(4)
                                           I_message.format_id, --FORMAT_ID-VARCHAR2(1)
                                           I_message.prefix, --PREFIX-NUMBER(2)
                                           I_message.brand, --BRAND-VARCHAR2(120)
                                           I_message.product_classification, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                           I_message.item_desc_secondary, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                           I_message.desc_up, --DESC_UP-VARCHAR2(250)
                                           I_message.merchandise_ind, --MERCHANDISE_IND-VARCHAR2(1)
                                           I_message.ORIGINAL_RETAIL, --ORIGINAL_RETAIL-NUMBER(20)
                                           I_message.RETAIL_LABEL_TYPE, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                           I_message.RETAIL_LABEL_VALUE, --RETAIL_LABEL_VALUE-NUMBER(20)
                                           I_message.DEFAULT_WASTE_PCT, --DEFAULT_WASTE_PCT-NUMBER(12)
                                           I_message.ITEM_SERVICE_LEVEL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                           I_message.CHECK_UDA_IND, --CHECK_UDA_IND-VARCHAR2(1)
                                           I_message.DEPOSIT_IN_PRICE_PER_UOM, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                           I_message.attempT_rms_load, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                           I_message.LocOfXItemDesc,
                                           I_message.LangOfXItemDesc_TBL);
        
        end if;
      
      end if;
    
    XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                O_error_message,
                                L_message,
                                L_message_type);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_ITEM_CRE;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_UDA_CRE
  -- Description: Responsible to create XItemDesc for create message to new UDAs
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_UDA_CRE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                               I_uda_lov       IN XXADEO_UDA_LOV_TBL,
                               I_uda_ff        IN XXADEO_UDA_FF_TBL,
                               I_uda_date      IN XXADEO_UDA_DATE_TBL,
                               O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error_message IN OUT VARCHAR2) IS
  
    L_message_type VARCHAR2(30);
    L_message_uda  "RIB_XItemUDADtl_REC";
    L_message_tbl  "RIB_XItemUDADtl_TBL";
    L_message      "RIB_XItemDesc_REC";
    L_program      VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_CRE';
  
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.ITEMUDA_ADD;
    if I_uda_lov is not null then
      for i in 1 .. I_uda_lov.count loop
        L_message_uda := "RIB_XItemUDADtl_REC"(0, --rib oid
                                               I_uda_lov(i).uda_id, --UDA_ID-NUMBER(5)
                                               'LV', --DISPLAY_TYPE-VARCHAR2(2)
                                               NULL, --UDA_DATE-DATE()
                                               I_uda_lov(i).uda_value, --UDA_VALUE-VARCHAR2(30)
                                               NULL, --UDA_TEXT-VARCHAR2(250)
                                               I_uda_lov(i).create_datetime, --CREATE_DATETIME-DATE()
                                               I_uda_lov(i)
                                               .last_update_datetime, --LAST_UPDATE_DATETIME-DATE()
                                               I_uda_lov(i).last_update_id); --LAST_UPDATE_ID-VARCHAR2(30)
      
        if L_message_tbl is null then
          L_message_tbl := "RIB_XItemUDADtl_TBL"();
        end if;
        L_message_tbl.extend();
        L_message_tbl(L_message_tbl.LAST) := L_message_uda;
      end loop;
    
    elsif I_uda_ff is not null then
      for i in 1 .. I_uda_ff.count loop
        L_message_uda := "RIB_XItemUDADtl_REC"(0, --rib oid
                                               I_uda_ff(i).uda_id, --UDA_ID-NUMBER(5)
                                               'FF', --DISPLAY_TYPE-VARCHAR2(2)
                                               NULL, --UDA_DATE-DATE()
                                               NULL, --UDA_VALUE-VARCHAR2(30)
                                               I_uda_ff(i).uda_text, --UDA_TEXT-VARCHAR2(250)
                                               I_uda_ff(i).create_datetime, --CREATE_DATETIME-DATE()
                                               I_uda_ff(i)
                                               .last_update_datetime, --LAST_UPDATE_DATETIME-DATE()
                                               I_uda_ff(i).last_update_id); --LAST_UPDATE_ID-VARCHAR2(30)
      
        if L_message_tbl is null then
          L_message_tbl := "RIB_XItemUDADtl_TBL"();
        end if;
        L_message_tbl.extend();
        L_message_tbl(L_message_tbl.LAST) := L_message_uda;
      end loop;
    
    elsif I_uda_date is not null then
      for i in 1 .. I_uda_date.count loop
        L_message_uda := "RIB_XItemUDADtl_REC"(0, --rib oid
                                               I_uda_date(i).uda_id, --UDA_ID-NUMBER(5)
                                               'DT', --DISPLAY_TYPE-VARCHAR2(2)
                                               I_uda_date(i).uda_date, --UDA_DATE-DATE()
                                               NULL, --UDA_VALUE-VARCHAR2(30)
                                               NULL, --UDA_TEXT-VARCHAR2(250)
                                               I_uda_date(i).create_datetime, --CREATE_DATETIME-DATE()
                                               I_uda_date(i)
                                               .last_update_datetime, --LAST_UPDATE_DATETIME-DATE()
                                               I_uda_date(i).last_update_id); --LAST_UPDATE_ID-VARCHAR2(30)
      
        if L_message_tbl is null then
          L_message_tbl := "RIB_XItemUDADtl_TBL"();
        end if;
        L_message_tbl.extend();
        L_message_tbl(L_message_tbl.LAST) := L_message_uda;
      end loop;
    end if;
  
    L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                     I_item, --ITEM-VARCHAR2(25)
                                     NULL, --ITEM_PARENT-VARCHAR2(25)
                                     NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                     NULL, --PACK_IND-VARCHAR2(1)
                                     NULL, --ITEM_LEVEL-NUMBER(1)
                                     NULL, --TRAN_LEVEL-NUMBER(1)
                                     NULL, --DIFF_1-VARCHAR2(10)
                                     NULL, --DIFF_2-VARCHAR2(10)
                                     NULL, --DIFF_3-VARCHAR2(10)
                                     NULL, --DIFF_4-VARCHAR2(10)
                                     NULL, --DEPT-NUMBER(4)
                                     NULL, --CLASS-NUMBER(4)
                                     NULL, --SUBCLASS-NUMBER(4)
                                     NULL, --ITEM_DESC-VARCHAR2(250)
                                     NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                     NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                     NULL, --SHORT_DESC-VARCHAR2(120)
                                     NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                     NULL, --STANDARD_UOM-VARCHAR2(4)
                                     NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                     NULL, --FORECAST_IND-VARCHAR2(1)
                                     NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                     NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                     NULL, --SELLABLE_IND-VARCHAR2(1)
                                     NULL, --ORDERABLE_IND-VARCHAR2(1)
                                     NULL, --PACK_TYPE-VARCHAR2(1)
                                     NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                     NULL, --COMMENTS-VARCHAR2(2000)
                                     NULL, --CREATE_DATETIME-DATE()
                                     NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                     NULL, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                     NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                     NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                     NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                     L_message_tbl, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                     NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                     NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                     NULL, --STATUS-VARCHAR2(1)
                                     NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                     NULL, --PACKAGE_SIZE-NUMBER(12)
                                     NULL, --HANDLING_TEMP-VARCHAR2(6)
                                     NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                     NULL, --MFG_REC_RETAIL-NUMBER(20)
                                     NULL, --WASTE_TYPE-VARCHAR2(6)
                                     NULL, --WASTE_PCT-NUMBER(12)
                                     NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                     NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                     NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                     NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                     NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                     NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                     NULL, --SIZE_GROUP1-NUMBER(4)
                                     NULL, --SIZE_GROUP2-NUMBER(4)
                                     NULL, --SIZE1-VARCHAR2(6)
                                     NULL, --SIZE2-VARCHAR2(6)
                                     NULL, --COLOR-VARCHAR2(24)
                                     NULL, --SYSTEM_IND-VARCHAR2(1)
                                     NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                     NULL, --UPC_TYPE-VARCHAR2(5)
                                     NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                     NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                     NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --PERISHABLE_IND-VARCHAR2(1)
                                     NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                     NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                     NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                     NULL, --ORDER_TYPE-VARCHAR2(6)
                                     NULL, --SALE_TYPE-VARCHAR2(6)
                                     NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                     NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                     NULL, --INVENTORY_IND-VARCHAR2(1)
                                     NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                     NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                     NULL, --PACKAGE_UOM-VARCHAR2(4)
                                     NULL, --FORMAT_ID-VARCHAR2(1)
                                     NULL, --PREFIX-NUMBER(2)
                                     NULL, --BRAND-VARCHAR2(120)
                                     NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                     NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                     NULL, --DESC_UP-VARCHAR2(250)
                                     NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                     NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                     NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                     NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                     NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                     NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                     NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                     NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                     NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                     NULL); --LANGOFXITEMDESC_TBL-RIB_LangOfXItemDesc_TBL()
  
    XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                O_error_message,
                                L_message,
                                L_message_type);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_UDA_CRE;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_UDA_DEL
  -- Description: Responsible for create XItemRef for delete a list of UDAs
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_UDA_DEL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                               I_uda_lov       IN XXADEO_UDA_LOV_TBL,
                               I_uda_ff        IN XXADEO_UDA_FF_TBL,
                               I_uda_date      IN XXADEO_UDA_DATE_TBL,
                               O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error_message IN OUT VARCHAR2) IS
  
    L_message_type VARCHAR2(30);
    L_message_uda  "RIB_XItemUDARef_REC";
    L_message_tbl  "RIB_XItemUDARef_TBL";
    L_message      "RIB_XItemRef_REC";
    L_uda_text     UDA_ITEM_FF.UDA_TEXT%TYPE;
    L_uda_date     UDA_ITEM_DATE.UDA_DATE%TYPE;
    L_program      VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_DEL';
  
  BEGIN
    L_message_type := XXADEO_RMSSUB_XITEM.ITEMUDA_DEL;
  
    if I_uda_lov is not null then
      for i in 1 .. I_uda_lov.count loop
        L_message_tbl := null;
      
        L_message_uda := "RIB_XItemUDARef_REC"(0, --rib oid
                                               I_uda_lov(i).uda_id, --UDA_ID-NUMBER(5)
                                               'LV', --DISPLAY_TYPE-VARCHAR2(2)
                                               I_uda_lov(i).uda_value);
        if L_message_tbl is null then
          L_message_tbl := "RIB_XItemUDARef_TBL"();
        end if;
        L_message_tbl.extend();
        L_message_tbl(L_message_tbl.LAST) := L_message_uda;
      
        L_message := "RIB_XItemRef_REC"(0, --rib oid
                                        I_item, --ITEM-VARCHAR2(25)
                                        NULL, --HIER_LEVEL-VARCHAR2(2)
                                        NULL, --XITEMCTRYREF_TBL-RIB_XItemCtryRef_TBL()
                                        NULL, --XITEMSUPREF_TBL-RIB_XItemSupRef_TBL()
                                        NULL, --XITEMVATREF_TBL-RIB_XItemVATRef_TBL()
                                        NULL, --XITEMIMAGEREF_TBL-RIB_XItemImageRef_TBL()
                                        NULL, --XITEMSEASONREF_TBL-RIB_XItemSeasonRef_TBL()
                                        L_message_tbl, --XITEMUDAREF_TBL-RIB_XItemUDARef_TBL()
                                        NULL, --XITEMBOMREF_TBL-RIB_XItemBOMRef_TBL()
                                        NULL, --SYSTEM_IND-VARCHAR2(1)
                                        NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                        NULL);
      
        XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                    O_error_message,
                                    L_message,
                                    L_message_type);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end loop;
    elsif I_uda_ff is not null then
      for i in 1 .. I_uda_ff.count loop
        Select uda_text
          into L_uda_text
          from uda_item_ff
         where item = I_item
           and uda_id = I_uda_ff(i).uda_id;
        L_message_uda := "RIB_XItemUDARef_REC"(0, --rib oid
                                               I_uda_ff(i).uda_id, --UDA_ID-NUMBER(5)
                                               'FF', --DISPLAY_TYPE-VARCHAR2(2)
                                               L_uda_text);
        if L_message_tbl is null then
          L_message_tbl := "RIB_XItemUDARef_TBL"();
        end if;
        L_message_tbl.extend();
        L_message_tbl(L_message_tbl.LAST) := L_message_uda;
      end loop;
    
      L_message := "RIB_XItemRef_REC"(0, --rib oid
                                      I_item, --ITEM-VARCHAR2(25)
                                      NULL, --HIER_LEVEL-VARCHAR2(2)
                                      NULL, --XITEMCTRYREF_TBL-RIB_XItemCtryRef_TBL()
                                      NULL, --XITEMSUPREF_TBL-RIB_XItemSupRef_TBL()
                                      NULL, --XITEMVATREF_TBL-RIB_XItemVATRef_TBL()
                                      NULL, --XITEMIMAGEREF_TBL-RIB_XItemImageRef_TBL()
                                      NULL, --XITEMSEASONREF_TBL-RIB_XItemSeasonRef_TBL()
                                      L_message_tbl, --XITEMUDAREF_TBL-RIB_XItemUDARef_TBL()
                                      NULL, --XITEMBOMREF_TBL-RIB_XItemBOMRef_TBL()
                                      NULL, --SYSTEM_IND-VARCHAR2(1)
                                      NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                      NULL);
    
      XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                  O_error_message,
                                  L_message,
                                  L_message_type);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    elsif I_uda_date is not null then
      for i in 1 .. I_uda_date.count loop
        Select uda_date
          into L_uda_date
          from uda_item_date
         where item = I_item
           and uda_id = I_uda_date(i).uda_id;
        L_message_uda := "RIB_XItemUDARef_REC"(0, --rib oid
                                               I_uda_date(i).uda_id, --UDA_ID-NUMBER(5)
                                               'DT', --DISPLAY_TYPE-VARCHAR2(2)
                                               to_char(L_uda_date,
                                                       'YYYY-MM-DD'));
        if L_message_tbl is null then
          L_message_tbl := "RIB_XItemUDARef_TBL"();
        end if;
        L_message_tbl.extend();
        L_message_tbl(L_message_tbl.LAST) := L_message_uda;
      end loop;
    
      L_message := "RIB_XItemRef_REC"(0, --rib oid
                                      I_item, --ITEM-VARCHAR2(25)
                                      NULL, --HIER_LEVEL-VARCHAR2(2)
                                      NULL, --XITEMCTRYREF_TBL-RIB_XItemCtryRef_TBL()
                                      NULL, --XITEMSUPREF_TBL-RIB_XItemSupRef_TBL()
                                      NULL, --XITEMVATREF_TBL-RIB_XItemVATRef_TBL()
                                      NULL, --XITEMIMAGEREF_TBL-RIB_XItemImageRef_TBL()
                                      NULL, --XITEMSEASONREF_TBL-RIB_XItemSeasonRef_TBL()
                                      L_message_tbl, --XITEMUDAREF_TBL-RIB_XItemUDARef_TBL()
                                      NULL, --XITEMBOMREF_TBL-RIB_XItemBOMRef_TBL()
                                      NULL, --SYSTEM_IND-VARCHAR2(1)
                                      NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                      NULL);
    
      XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                  O_error_message,
                                  L_message,
                                  L_message_type);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_UDA_DEL;

  
   -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_VAT_ITEM_CRE
  -- Description: Responsible for create a XItemDesc to create VAT Item
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_VAT_ITEM_CRE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                    I_message       IN XXADEO_VAT_ITEM_TBL,
                                    O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error_message IN OUT VARCHAR2) IS
  
    L_message_type VARCHAR2(30);
    L_message      "RIB_XItemDesc_REC";
    L_message_rec  "RIB_XItemVATDesc_REC";
    L_message_tbl  "RIB_XItemVATDesc_TBL";
    L_program      VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_VAT_ITEM_CRE';
  
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.IVAT_ADD;
  
    if I_message is not null and I_message.count > 0 then
      for i in 1 .. I_message.count loop
        L_message_rec := "RIB_XItemVATDesc_REC"(0, --rib oid
                                                I_message(i).vat_type, --VAT_TYPE-VARCHAR2(1)
                                                I_message(i).vat_region, --VAT_REGION-NUMBER(6)
                                                I_message(i).vat_code, --VAT_CODE-VARCHAR2(6)
                                                get_vdate(), --ACTIVE_DATE-DATE()
                                                'N'); --REVERSE_VAT_IND-VARCHAR2(1)
        if L_message_tbl is null then
          L_message_tbl := "RIB_XItemVATDesc_TBL"();
        end if;
        L_message_tbl.extend();
        L_message_tbl(L_message_tbl.last) := L_message_rec;
      end loop;
    
      if L_message_tbl is not null and L_message_tbl.count > 0 then
        L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                         I_item, --ITEM-VARCHAR2(25)
                                         NULL, --ITEM_PARENT-VARCHAR2(25)
                                         NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                         NULL, --PACK_IND-VARCHAR2(1)
                                         NULL, --ITEM_LEVEL-NUMBER(1)
                                         NULL, --TRAN_LEVEL-NUMBER(1)
                                         NULL, --DIFF_1-VARCHAR2(10)
                                         NULL, --DIFF_2-VARCHAR2(10)
                                         NULL, --DIFF_3-VARCHAR2(10)
                                         NULL, --DIFF_4-VARCHAR2(10)
                                         NULL, --DEPT-NUMBER(4)
                                         NULL, --CLASS-NUMBER(4)
                                         NULL, --SUBCLASS-NUMBER(4)
                                         NULL, --ITEM_DESC-VARCHAR2(250)
                                         NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                         NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                         NULL, --SHORT_DESC-VARCHAR2(120)
                                         NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                         NULL, --STANDARD_UOM-VARCHAR2(4)
                                         NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                         NULL, --FORECAST_IND-VARCHAR2(1)
                                         NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                         NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                         NULL, --SELLABLE_IND-VARCHAR2(1)
                                         NULL, --ORDERABLE_IND-VARCHAR2(1)
                                         NULL, --PACK_TYPE-VARCHAR2(1)
                                         NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                         NULL, --COMMENTS-VARCHAR2(2000)
                                         NULL, --CREATE_DATETIME-DATE()
                                         NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                         NULL, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                         NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                         L_message_tbl, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                         NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                         NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                         NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                         NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                         NULL, --STATUS-VARCHAR2(1)
                                         NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                         NULL, --PACKAGE_SIZE-NUMBER(12)
                                         NULL, --HANDLING_TEMP-VARCHAR2(6)
                                         NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                         NULL, --MFG_REC_RETAIL-NUMBER(20)
                                         NULL, --WASTE_TYPE-VARCHAR2(6)
                                         NULL, --WASTE_PCT-NUMBER(12)
                                         NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                         NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                         NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                         NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                         NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                         NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                         NULL, --SIZE_GROUP1-NUMBER(4)
                                         NULL, --SIZE_GROUP2-NUMBER(4)
                                         NULL, --SIZE1-VARCHAR2(6)
                                         NULL, --SIZE2-VARCHAR2(6)
                                         NULL, --COLOR-VARCHAR2(24)
                                         NULL, --SYSTEM_IND-VARCHAR2(1)
                                         NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                         NULL, --UPC_TYPE-VARCHAR2(5)
                                         NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                         NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                         NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                         NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                         NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                         NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                         NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                         NULL, --PERISHABLE_IND-VARCHAR2(1)
                                         NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                         NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                         NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                         NULL, --ORDER_TYPE-VARCHAR2(6)
                                         NULL, --SALE_TYPE-VARCHAR2(6)
                                         NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                         NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                         NULL, --INVENTORY_IND-VARCHAR2(1)
                                         NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                         NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                         NULL, --PACKAGE_UOM-VARCHAR2(4)
                                         NULL, --FORMAT_ID-VARCHAR2(1)
                                         NULL, --PREFIX-NUMBER(2)
                                         NULL, --BRAND-VARCHAR2(120)
                                         NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                         NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                         NULL, --DESC_UP-VARCHAR2(250)
                                         NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                         NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                         NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                         NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                         NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                         NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                         NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                         NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                         NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                         NULL);
        XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                    O_error_message,
                                    L_message,
                                    L_message_type);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_VAT_ITEM_CRE;

  
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_IMAGE_CRE
  -- Description: Responsible for create a XItemDesc for create Item Image
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_IMAGE_CRE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                      I_item_image    IN XXADEO_ITEM_IMAGE_TBL,
                                      I_message       IN "RIB_XItemImage_TBL",
                                      O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_error_message IN OUT VARCHAR2) IS
  
    L_message_type      VARCHAR2(30);
    L_message           "RIB_XItemDesc_REC";
    L_message_image_rec "RIB_XItemImage_REC";
    L_message_tbl       "RIB_XItemImage_TBL";
    L_priority          NUMBER;
    L_count             NUMBER;
    L_program           VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_IMAGE_CRE';
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.IMAGE_ADD;
  
    for i in 1 .. I_item_image.count loop
    
      for j in 1 .. I_message.count loop
        if I_item_image(i).image_name = I_message(j).image_name and I_item_image(i)
           .image_desc = I_message(j).image_desc and I_item_image(i)
           .image_addr = I_message(j).image_addr then
        
          select count(*), nvl(max(display_priority), 0)
            into L_count, L_priority
            from item_image
           where item = I_item;
        
          if L_count > 0 then
          
            L_message_image_rec := "RIB_XItemImage_REC"(0, --rib oid
                                                        I_message(j)
                                                        .image_name, --IMAGE_NAME-VARCHAR2(120)
                                                        I_message(j)
                                                        .image_addr, --IMAGE_ADDR-VARCHAR2(255)
                                                        I_message(j)
                                                        .image_desc, --IMAGE_DESC-VARCHAR2(40)
                                                        NULL, --CREATE_DATETIME-DATE()
                                                        NULL, --LAST_UPDATE_DATETIME-DATE()
                                                        NULL, --LAST_UPDATE_ID-VARCHAR2(30)
                                                        null,
                                                        I_message(j)
                                                        .image_type, --IMAGE_TYPE-VARCHAR2(6)
                                                        'N', --PRIMARY_IND-VARCHAR2(1)
                                                        L_priority + 1, --DISPLAY_PRIORITY-NUMBER(4)
                                                        I_message(j)
                                                        .langofxitemimage_tbl); --LANGOFXITEMIMAGE_TBL-RIB_LangOfXItemImage_TBL()
          
          else
            L_message_image_rec := "RIB_XItemImage_REC"(0, --rib oid
                                                        I_message(j)
                                                        .image_name, --IMAGE_NAME-VARCHAR2(120)
                                                        I_message(j)
                                                        .image_addr, --IMAGE_ADDR-VARCHAR2(255)
                                                        I_message(j)
                                                        .image_desc, --IMAGE_DESC-VARCHAR2(40)
                                                        NULL, --CREATE_DATETIME-DATE()
                                                        NULL, --LAST_UPDATE_DATETIME-DATE()
                                                        NULL, --LAST_UPDATE_ID-VARCHAR2(30)
                                                        null,
                                                        I_message(j)
                                                        .image_type, --IMAGE_TYPE-VARCHAR2(6)
                                                        'Y', --PRIMARY_IND-VARCHAR2(1)
                                                        L_priority + 1, --DISPLAY_PRIORITY-NUMBER(4)
                                                        I_message(j)
                                                        .langofxitemimage_tbl); --LANGOFXITEMIMAGE_TBL-RIB_LangOfXItemImage_TBL()
          end if;
        
          if L_message_tbl is null then
            L_message_tbl := "RIB_XItemImage_TBL"();
          end if;
          L_message_tbl.extend();
          L_message_tbl(L_message_tbl.LAST) := L_message_image_rec;
        end if;
      end loop;
    
    end loop;
    if L_message_tbl is not null and L_message_tbl.count > 0 then
    
      L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                       I_item, --ITEM-VARCHAR2(25)
                                       NULL, --ITEM_PARENT-VARCHAR2(25)
                                       NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                       NULL, --PACK_IND-VARCHAR2(1)
                                       NULL, --ITEM_LEVEL-NUMBER(1)
                                       NULL, --TRAN_LEVEL-NUMBER(1)
                                       NULL, --DIFF_1-VARCHAR2(10)
                                       NULL, --DIFF_2-VARCHAR2(10)
                                       NULL, --DIFF_3-VARCHAR2(10)
                                       NULL, --DIFF_4-VARCHAR2(10)
                                       NULL, --DEPT-NUMBER(4)
                                       NULL, --CLASS-NUMBER(4)
                                       NULL, --SUBCLASS-NUMBER(4)
                                       NULL, --ITEM_DESC-VARCHAR2(250)
                                       NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --SHORT_DESC-VARCHAR2(120)
                                       NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                       NULL, --STANDARD_UOM-VARCHAR2(4)
                                       NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                       NULL, --FORECAST_IND-VARCHAR2(1)
                                       NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                       NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                       NULL, --SELLABLE_IND-VARCHAR2(1)
                                       NULL, --ORDERABLE_IND-VARCHAR2(1)
                                       NULL, --PACK_TYPE-VARCHAR2(1)
                                       NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                       NULL, --COMMENTS-VARCHAR2(2000)
                                       NULL, --CREATE_DATETIME-DATE()
                                       NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                       NULL, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                       NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                       NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                       NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                       NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                       NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                       L_message_tbl, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                       NULL, --STATUS-VARCHAR2(1)
                                       NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                       NULL, --PACKAGE_SIZE-NUMBER(12)
                                       NULL, --HANDLING_TEMP-VARCHAR2(6)
                                       NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                       NULL, --MFG_REC_RETAIL-NUMBER(20)
                                       NULL, --WASTE_TYPE-VARCHAR2(6)
                                       NULL, --WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                       NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                       NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                       NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                       NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                       NULL, --SIZE_GROUP1-NUMBER(4)
                                       NULL, --SIZE_GROUP2-NUMBER(4)
                                       NULL, --SIZE1-VARCHAR2(6)
                                       NULL, --SIZE2-VARCHAR2(6)
                                       NULL, --COLOR-VARCHAR2(24)
                                       NULL, --SYSTEM_IND-VARCHAR2(1)
                                       NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                       NULL, --UPC_TYPE-VARCHAR2(5)
                                       NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                       NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                       NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --PERISHABLE_IND-VARCHAR2(1)
                                       NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                       NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                       NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                       NULL, --ORDER_TYPE-VARCHAR2(6)
                                       NULL, --SALE_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                       NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                       NULL, --INVENTORY_IND-VARCHAR2(1)
                                       NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                       NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                       NULL, --PACKAGE_UOM-VARCHAR2(4)
                                       NULL, --FORMAT_ID-VARCHAR2(1)
                                       NULL, --PREFIX-NUMBER(2)
                                       NULL, --BRAND-VARCHAR2(120)
                                       NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                       NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                       NULL, --DESC_UP-VARCHAR2(250)
                                       NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                       NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                       NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                       NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                       NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                       NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                       NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                       NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                       NULL);
    
      XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                  O_error_message,
                                  L_message,
                                  L_message_type);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END CREATE_MSG_ITEM_IMAGE_CRE;
  
  
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_IMAGE_MOD
  -- Description: Responsible to create XItemDesc for update a list of item images according 
  --              information managed by STEP
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_IMAGE_MOD(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                      I_item_image    IN XXADEO_ITEM_IMAGE_TBL,
                                      O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_error_message IN OUT VARCHAR2) IS
    L_message_type      VARCHAR2(30);
    L_message           "RIB_XItemDesc_REC";
    L_message_image_rec "RIB_XItemImage_REC";
    L_message_tbl       "RIB_XItemImage_TBL";
    L_row               ITEM_IMAGE%ROWTYPE;
  
    L_program VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_IMAGE_MOD';
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.IMAGE_UPD;
  
    if I_item_image is not null and I_item_image.count > 0 then
    
      for i in 1 .. I_item_image.count loop
        select *
          into L_row
          from item_image
         where item = I_item
           and image_name = I_item_image(i).image_name;
        if L_row.image_addr != I_item_image(i).image_addr or
           L_row.image_desc != I_item_image(i).image_desc then
        
          L_message_image_rec := "RIB_XItemImage_REC"(0, --rib oid
                                                      I_item_image          (i)
                                                      .image_name, --IMAGE_NAME-VARCHAR2(120)
                                                      I_item_image          (i)
                                                      .image_addr, --IMAGE_ADDR-VARCHAR2(255)
                                                      I_item_image          (i)
                                                      .image_desc, --IMAGE_DESC-VARCHAR2(40)
                                                      NULL, --CREATE_DATETIME-DATE()
                                                      NULL, --LAST_UPDATE_DATETIME-DATE()
                                                      NULL, --LAST_UPDATE_ID-VARCHAR2(30)
                                                      NULL,
                                                      L_row.image_type, --IMAGE_TYPE-VARCHAR2(6)
                                                      L_row.Primary_Ind, --PRIMARY_IND-VARCHAR2(1)
                                                      L_row.display_priority, --DISPLAY_PRIORITY-NUMBER(4)
                                                      NULL);
        
          if L_message_tbl is null then
            L_message_tbl := "RIB_XItemImage_TBL"();
          end if;
          L_message_tbl.extend();
          L_message_tbl(L_message_tbl.LAST) := L_message_image_rec;
        end if;
      
      end loop;
      if L_message_tbl is not null and L_message_tbl.count > 0 then
        L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                         I_item, --ITEM-VARCHAR2(25)
                                         NULL, --ITEM_PARENT-VARCHAR2(25)
                                         NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                         NULL, --PACK_IND-VARCHAR2(1)
                                         NULL, --ITEM_LEVEL-NUMBER(1)
                                         NULL, --TRAN_LEVEL-NUMBER(1)
                                         NULL, --DIFF_1-VARCHAR2(10)
                                         NULL, --DIFF_2-VARCHAR2(10)
                                         NULL, --DIFF_3-VARCHAR2(10)
                                         NULL, --DIFF_4-VARCHAR2(10)
                                         NULL, --DEPT-NUMBER(4)
                                         NULL, --CLASS-NUMBER(4)
                                         NULL, --SUBCLASS-NUMBER(4)
                                         NULL, --ITEM_DESC-VARCHAR2(250)
                                         NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                         NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                         NULL, --SHORT_DESC-VARCHAR2(120)
                                         NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                         NULL, --STANDARD_UOM-VARCHAR2(4)
                                         NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                         NULL, --FORECAST_IND-VARCHAR2(1)
                                         NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                         NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                         NULL, --SELLABLE_IND-VARCHAR2(1)
                                         NULL, --ORDERABLE_IND-VARCHAR2(1)
                                         NULL, --PACK_TYPE-VARCHAR2(1)
                                         NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                         NULL, --COMMENTS-VARCHAR2(2000)
                                         NULL, --CREATE_DATETIME-DATE()
                                         NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                         NULL, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                         NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                         NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                         NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                         NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                         NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                         L_message_tbl, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                         NULL, --STATUS-VARCHAR2(1)
                                         NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                         NULL, --PACKAGE_SIZE-NUMBER(12)
                                         NULL, --HANDLING_TEMP-VARCHAR2(6)
                                         NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                         NULL, --MFG_REC_RETAIL-NUMBER(20)
                                         NULL, --WASTE_TYPE-VARCHAR2(6)
                                         NULL, --WASTE_PCT-NUMBER(12)
                                         NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                         NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                         NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                         NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                         NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                         NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                         NULL, --SIZE_GROUP1-NUMBER(4)
                                         NULL, --SIZE_GROUP2-NUMBER(4)
                                         NULL, --SIZE1-VARCHAR2(6)
                                         NULL, --SIZE2-VARCHAR2(6)
                                         NULL, --COLOR-VARCHAR2(24)
                                         NULL, --SYSTEM_IND-VARCHAR2(1)
                                         NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                         NULL, --UPC_TYPE-VARCHAR2(5)
                                         NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                         NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                         NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                         NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                         NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                         NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                         NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                         NULL, --PERISHABLE_IND-VARCHAR2(1)
                                         NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                         NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                         NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                         NULL, --ORDER_TYPE-VARCHAR2(6)
                                         NULL, --SALE_TYPE-VARCHAR2(6)
                                         NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                         NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                         NULL, --INVENTORY_IND-VARCHAR2(1)
                                         NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                         NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                         NULL, --PACKAGE_UOM-VARCHAR2(4)
                                         NULL, --FORMAT_ID-VARCHAR2(1)
                                         NULL, --PREFIX-NUMBER(2)
                                         NULL, --BRAND-VARCHAR2(120)
                                         NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                         NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                         NULL, --DESC_UP-VARCHAR2(250)
                                         NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                         NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                         NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                         NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                         NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                         NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                         NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                         NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                         NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                         NULL);
      
        XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                    O_error_message,
                                    L_message,
                                    L_message_type);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      end if;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END CREATE_MSG_ITEM_IMAGE_MOD;

  
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_IMAGE_DEL
  -- Description: Responsible for create a XItemRef to delete Item Images
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_IMAGE_DEL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                      I_item_image    IN XXADEO_ITEM_IMAGE_TBL,
                                      O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_error_message IN OUT VARCHAR2) IS
    L_message_type      VARCHAR2(30);
    L_message           "RIB_XItemRef_REC";
    L_message_image_rec "RIB_XItemImageRef_REC";
    L_message_tbl       "RIB_XItemImageRef_TBL";
    L_program           VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_ITEM_IMAGE_DEL';
  
  BEGIN
    L_message_type := XXADEO_RMSSUB_XITEM.IMAGE_DEL;
  
    if I_item_image is not null and I_item_image.count > 0 then
      for i in 1 .. I_item_image.count loop
        L_message_image_rec := "RIB_XItemImageRef_REC"(0,
                                                       I_item_image(i)
                                                       .image_name);
      
        if L_message_tbl is null then
          L_message_tbl := "RIB_XItemImageRef_TBL"();
        end if;
        L_message_tbl.extend();
        L_message_tbl(L_message_tbl.last) := L_message_image_rec;
      end loop;
      if L_message_tbl is not null and L_message_tbl.count > 0 then
        L_message := "RIB_XItemRef_REC"(0, --rib oid
                                        I_item, --ITEM-VARCHAR2(25)
                                        NULL, --HIER_LEVEL-VARCHAR2(2)
                                        NULL, --XITEMCTRYREF_TBL-RIB_XItemCtryRef_TBL()
                                        NULL, --XITEMSUPREF_TBL-RIB_XItemSupRef_TBL()
                                        NULL, --XITEMVATREF_TBL-RIB_XItemVATRef_TBL()
                                        L_message_tbl, --XITEMIMAGEREF_TBL-RIB_XItemImageRef_TBL()
                                        NULL, --XITEMSEASONREF_TBL-RIB_XItemSeasonRef_TBL()
                                        NULL, --XITEMUDAREF_TBL-RIB_XItemUDARef_TBL()
                                        NULL, --XITEMBOMREF_TBL-RIB_XItemBOMRef_TBL()
                                        NULL, --SYSTEM_IND-VARCHAR2(1)
                                        NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                        NULL); --LANGOFXITEMREF_TBL-RIB_LangOfXItemRef_TBL()
      
        XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                    O_error_message,
                                    L_message,
                                    L_message_type);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END CREATE_MSG_ITEM_IMAGE_DEL;

  
   -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_SUP_CRE
  -- Description: Responsible to create XItemDesc to create suppliers
  -- 	          if Item have childrens create new message for childrens (excluding Barcodes)
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_SUP_CRE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                    I_message       IN "RIB_XItemSupDesc_REC",
                                    O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error_message IN OUT VARCHAR2) IS
  
    L_message_type    VARCHAR2(30);
    L_message         "RIB_XItemDesc_REC";
    L_message_tbl     "RIB_XItemSupDesc_TBL";
    L_message_sup_rec "RIB_XItemSupDesc_REC";
    L_program         VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_CRE';
    L_tran_level      ITEM_MASTER.TRAN_LEVEL%TYPE;
    L_item_level      ITEM_MASTER.ITEM_LEVEL%TYPE;
  
  BEGIN
    L_message_type := XXADEO_RMSSUB_XITEM.ISUP_ADD;
    L_message_tbl  := "RIB_XItemSupDesc_TBL"();
  
    select tran_level, item_level
      into L_tran_level, L_item_level
      from item_master
     where item = I_item;
  
    L_message_sup_rec := "RIB_XItemSupDesc_REC"(0, --rib oid
                                                I_message.SUPPLIER, --SUPPLIER-VARCHAR2(10)
                                                'N', --PRIMARY_SUPP_IND-VARCHAR2(3)
                                                I_message.vpn, --VPN-VARCHAR2(30)
                                                NULL, --SUPP_LABEL-VARCHAR2(15)
                                                I_message.XItemSupCtyDesc_TBL, --XITEMSUPCTYDESC_TBL-RIB_XItemSupCtyDesc_TBL()
                                                NULL, --CONSIGNMENT_RATE-NUMBER(12)
                                                NULL, --SUPP_DISCONTINUE_DATE-DATE()
                                                I_message.direct_ship_ind, --DIRECT_SHIP_IND-VARCHAR2(1)
                                                NULL, --PALLET_NAME-VARCHAR2(6)
                                                NULL, --CASE_NAME-VARCHAR2(6)
                                                NULL, --INNER_NAME-VARCHAR2(6)
                                                I_message.XItmSupCtyMfrDesc_TBL, --XITMSUPCTYMFRDESC_TBL-RIB_XItmSupCtyMfrDesc_TBL()
                                                I_message.primary_case_size, --PRIMARY_CASE_SIZE-VARCHAR2(6)
                                                NULL, --SUPP_DIFF_1-VARCHAR2(120)
                                                NULL, --SUPP_DIFF_2-VARCHAR2(120)
                                                NULL, --SUPP_DIFF_3-VARCHAR2(120)
                                                NULL, --SUPP_DIFF_4-VARCHAR2(120)
                                                NULL, --CONCESSION_RATE-NUMBER(12)
                                                NULL); --LANGOFXITEMSUPDESC_TBL-RIB_LangOfXItemSupDesc_TBL()
  
    L_message_tbl.extend();
    L_message_tbl(L_message_tbl.last) := L_message_sup_rec;
  
    L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                     I_item, --ITEM-VARCHAR2(25)
                                     NULL, --ITEM_PARENT-VARCHAR2(25)
                                     NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                     NULL, --PACK_IND-VARCHAR2(1)
                                     NULL, --ITEM_LEVEL-NUMBER(1)
                                     NULL, --TRAN_LEVEL-NUMBER(1)
                                     NULL, --DIFF_1-VARCHAR2(10)
                                     NULL, --DIFF_2-VARCHAR2(10)
                                     NULL, --DIFF_3-VARCHAR2(10)
                                     NULL, --DIFF_4-VARCHAR2(10)
                                     NULL, --DEPT-NUMBER(4)
                                     NULL, --CLASS-NUMBER(4)
                                     NULL, --SUBCLASS-NUMBER(4)
                                     NULL, --ITEM_DESC-VARCHAR2(250)
                                     NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                     NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                     NULL, --SHORT_DESC-VARCHAR2(120)
                                     NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                     NULL, --STANDARD_UOM-VARCHAR2(4)
                                     NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                     NULL, --FORECAST_IND-VARCHAR2(1)
                                     NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                     NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                     NULL, --SELLABLE_IND-VARCHAR2(1)
                                     NULL, --ORDERABLE_IND-VARCHAR2(1)
                                     NULL, --PACK_TYPE-VARCHAR2(1)
                                     NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                     NULL, --COMMENTS-VARCHAR2(2000)
                                     NULL, --CREATE_DATETIME-DATE()
                                     NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                     L_message_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                     NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                     NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                     NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                     NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                     NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                     NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                     NULL, --STATUS-VARCHAR2(1)
                                     NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                     NULL, --PACKAGE_SIZE-NUMBER(12)
                                     NULL, --HANDLING_TEMP-VARCHAR2(6)
                                     NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                     NULL, --MFG_REC_RETAIL-NUMBER(20)
                                     NULL, --WASTE_TYPE-VARCHAR2(6)
                                     NULL, --WASTE_PCT-NUMBER(12)
                                     NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                     NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                     NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                     NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                     NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                     NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                     NULL, --SIZE_GROUP1-NUMBER(4)
                                     NULL, --SIZE_GROUP2-NUMBER(4)
                                     NULL, --SIZE1-VARCHAR2(6)
                                     NULL, --SIZE2-VARCHAR2(6)
                                     NULL, --COLOR-VARCHAR2(24)
                                     NULL, --SYSTEM_IND-VARCHAR2(1)
                                     NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                     NULL, --UPC_TYPE-VARCHAR2(5)
                                     NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                     NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                     NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --PERISHABLE_IND-VARCHAR2(1)
                                     NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                     NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                     NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                     NULL, --ORDER_TYPE-VARCHAR2(6)
                                     NULL, --SALE_TYPE-VARCHAR2(6)
                                     NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                     NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                     NULL, --INVENTORY_IND-VARCHAR2(1)
                                     NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                     NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                     NULL, --PACKAGE_UOM-VARCHAR2(4)
                                     NULL, --FORMAT_ID-VARCHAR2(1)
                                     NULL, --PREFIX-NUMBER(2)
                                     NULL, --BRAND-VARCHAR2(120)
                                     NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                     NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                     NULL, --DESC_UP-VARCHAR2(250)
                                     NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                     NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                     NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                     NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                     NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                     NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                     NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                     NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                     NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                     NULL); --LANGOFXITEMDESC_TBL-RIB_LangOfXItemDesc_TBL()
  
    XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                O_error_message,
                                L_message,
                                L_message_type);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    if L_item_level <= L_tran_level then
      if I_message.XITEMSUPCTYDESC_TBL is not null and
         I_message.XITEMSUPCTYDESC_TBL.count > 0 then
        L_message_type := XXADEO_RMSSUB_XITEM.ISC_ADD;
      
        XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                    O_error_message,
                                    L_message,
                                    L_message_type);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end if;
      if I_message.XITMSUPCTYMFRDESC_TBL is not null and
         I_message.XITMSUPCTYMFRDESC_TBL.count > 0 then
      
        L_message_type := XXADEO_RMSSUB_XITEM.ISMC_ADD;
      
        XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                    O_error_message,
                                    L_message,
                                    L_message_type);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_ITEM_SUP_CRE;

 
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_SUP_MOD 
  -- Description: Responsible for create XItemDesc to update Item SUPPLIER
  --              with information managed by STEP 
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_SUP_MOD(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                    I_message       IN XXADEO_ITEM_SUP_REC,
                                    I_primary_ind   IN ITEM_SUPPLIER.PRIMARY_SUPP_IND%TYPE,
                                    O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error_message IN OUT VARCHAR2) IS
  
    L_message_rec  "RIB_XItemSupDesc_REC";
    L_message_tbl  "RIB_XItemSupDesc_TBL";
    L_message      "RIB_XItemDesc_REC";
    L_message_type VARCHAR2(30);
    L_row          ITEM_SUPPLIER%ROWTYPE;
    L_program      VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_MOD';
  
  BEGIN
    SELECT *
      into L_row
      from item_supplier
     where item = I_item
       and supplier = I_message.supplier;
  
    L_message_type := XXADEO_RMSSUB_XITEM.ISUP_UPD;
  
    if I_primary_ind is null then
    
      L_message_rec := "RIB_XItemSupDesc_REC"(0, --rib oid
                                              I_message.supplier, --SUPPLIER-VARCHAR2(10)
                                              L_row.primary_supp_ind, --PRIMARY_SUPP_IND-VARCHAR2(3)
                                              I_message.vpn, --VPN-VARCHAR2(30)
                                              NULL, --SUPP_LABEL-VARCHAR2(15)
                                              NULL, --XITEMSUPCTYDESC_TBL-RIB_XItemSupCtyDesc_TBL()
                                              NULL, --CONSIGNMENT_RATE-NUMBER(12)
                                              NULL, --SUPP_DISCONTINUE_DATE-DATE()
                                              L_row.Direct_Ship_Ind, --DIRECT_SHIP_IND-VARCHAR2(1)
                                              L_row.pallet_name, --PALLET_NAME-VARCHAR2(6)
                                              L_row.case_name, --CASE_NAME-VARCHAR2(6)
                                              L_row.inner_name, --INNER_NAME-VARCHAR2(6)
                                              NULL, --XITMSUPCTYMFRDESC_TBL-RIB_XItmSupCtyMfrDesc_TBL()
                                              L_row.primary_case_size, --PRIMARY_CASE_SIZE-VARCHAR2(6)
                                              NULL, --SUPP_DIFF_1-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_2-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_3-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_4-VARCHAR2(120)
                                              NULL, --CONCESSION_RATE-NUMBER(12)
                                              NULL); --LANGOFXITEMSUPDESC_TBL-RIB_LangOfXItemSupDesc_TBL()
    
      L_message_tbl := "RIB_XItemSupDesc_TBL"();
      L_message_tbl.extend();
      L_message_tbl(L_message_tbl.last) := L_message_rec;
    
    else
      L_message_rec := "RIB_XItemSupDesc_REC"(0, --rib oid
                                              I_message.supplier, --SUPPLIER-VARCHAR2(10)
                                              I_primary_ind, --PRIMARY_SUPP_IND-VARCHAR2(3)
                                              I_message.vpn, --VPN-VARCHAR2(30)
                                              NULL, --SUPP_LABEL-VARCHAR2(15)
                                              NULL, --XITEMSUPCTYDESC_TBL-RIB_XItemSupCtyDesc_TBL()
                                              NULL, --CONSIGNMENT_RATE-NUMBER(12)
                                              NULL, --SUPP_DISCONTINUE_DATE-DATE()
                                              L_row.Direct_Ship_Ind, --DIRECT_SHIP_IND-VARCHAR2(1)
                                              L_row.pallet_name, --PALLET_NAME-VARCHAR2(6)
                                              L_row.case_name, --CASE_NAME-VARCHAR2(6)
                                              L_row.inner_name, --INNER_NAME-VARCHAR2(6)
                                              NULL, --XITMSUPCTYMFRDESC_TBL-RIB_XItmSupCtyMfrDesc_TBL()
                                              L_row.primary_case_size, --PRIMARY_CASE_SIZE-VARCHAR2(6)
                                              NULL, --SUPP_DIFF_1-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_2-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_3-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_4-VARCHAR2(120)
                                              NULL, --CONCESSION_RATE-NUMBER(12)
                                              NULL); --LANGOFXITEMSUPDESC_TBL-RIB_LangOfXItemSupDesc_TBL()
    
      L_message_tbl := "RIB_XItemSupDesc_TBL"();
      L_message_tbl.extend();
      L_message_tbl(L_message_tbl.last) := L_message_rec;
    
    end if;
  
    for i in (select item
                from item_master
               where item = I_item
                  or item_parent = I_item
                  or item_grandparent = I_item) loop
    
      L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                       i.item, --ITEM-VARCHAR2(25)
                                       NULL, --ITEM_PARENT-VARCHAR2(25)
                                       NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                       NULL, --PACK_IND-VARCHAR2(1)
                                       NULL, --ITEM_LEVEL-NUMBER(1)
                                       NULL, --TRAN_LEVEL-NUMBER(1)
                                       NULL, --DIFF_1-VARCHAR2(10)
                                       NULL, --DIFF_2-VARCHAR2(10)
                                       NULL, --DIFF_3-VARCHAR2(10)
                                       NULL, --DIFF_4-VARCHAR2(10)
                                       NULL, --DEPT-NUMBER(4)
                                       NULL, --CLASS-NUMBER(4)
                                       NULL, --SUBCLASS-NUMBER(4)
                                       NULL, --ITEM_DESC-VARCHAR2(250)
                                       NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --SHORT_DESC-VARCHAR2(120)
                                       NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                       NULL, --STANDARD_UOM-VARCHAR2(4)
                                       NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                       NULL, --FORECAST_IND-VARCHAR2(1)
                                       NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                       NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                       NULL, --SELLABLE_IND-VARCHAR2(1)
                                       NULL, --ORDERABLE_IND-VARCHAR2(1)
                                       NULL, --PACK_TYPE-VARCHAR2(1)
                                       NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                       NULL, --COMMENTS-VARCHAR2(2000)
                                       NULL, --CREATE_DATETIME-DATE()
                                       NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                       L_message_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                       NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                       NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                       NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                       NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                       NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                       NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                       NULL, --STATUS-VARCHAR2(1)
                                       NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                       NULL, --PACKAGE_SIZE-NUMBER(12)
                                       NULL, --HANDLING_TEMP-VARCHAR2(6)
                                       NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                       NULL, --MFG_REC_RETAIL-NUMBER(20)
                                       NULL, --WASTE_TYPE-VARCHAR2(6)
                                       NULL, --WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                       NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                       NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                       NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                       NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                       NULL, --SIZE_GROUP1-NUMBER(4)
                                       NULL, --SIZE_GROUP2-NUMBER(4)
                                       NULL, --SIZE1-VARCHAR2(6)
                                       NULL, --SIZE2-VARCHAR2(6)
                                       NULL, --COLOR-VARCHAR2(24)
                                       NULL, --SYSTEM_IND-VARCHAR2(1)
                                       NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                       NULL, --UPC_TYPE-VARCHAR2(5)
                                       NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                       NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                       NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --PERISHABLE_IND-VARCHAR2(1)
                                       NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                       NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                       NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                       NULL, --ORDER_TYPE-VARCHAR2(6)
                                       NULL, --SALE_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                       NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                       NULL, --INVENTORY_IND-VARCHAR2(1)
                                       NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                       NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                       NULL, --PACKAGE_UOM-VARCHAR2(4)
                                       NULL, --FORMAT_ID-VARCHAR2(1)
                                       NULL, --PREFIX-NUMBER(2)
                                       NULL, --BRAND-VARCHAR2(120)
                                       NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                       NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                       NULL, --DESC_UP-VARCHAR2(250)
                                       NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                       NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                       NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                       NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                       NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                       NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                       NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                       NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                       NULL); --LANGOFXITEMDESC_TBL-RIB_LangOfXItemDesc_TBL()
    
      XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                  O_error_message,
                                  L_message,
                                  L_message_type);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end loop;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_ITEM_SUP_MOD;

  
   -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_SUP_CTY_CRE
  -- Description: Responsible for create XItemDesc to create Item/Supplier/Country relationship
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_SUP_CTY_CRE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                        I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_message       IN "RIB_XItemSupCtyDesc_REC",
                                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error_message IN OUT VARCHAR2) IS
  
    L_program         VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_CTY_CRE';
    L_message_sup     "RIB_XItemSupDesc_REC";
    L_message         "RIB_XItemDesc_REC";
    L_message_type    VARCHAR2(30);
    L_message_sup_tbl "RIB_XItemSupDesc_TBL";
    L_message_cty_tbl "RIB_XItemSupCtyDesc_TBL";
    L_message_cty_rec "RIB_XItemSupCtyDesc_REC";
    L_exist           number;
    L_item_level      ITEM_MASTER.ITEM_LEVEL%TYPE;
    L_tran_level      ITEM_MASTER.TRAN_LEVEL%TYPE;
  
  BEGIN
    L_message_type := XXADEO_RMSSUB_XITEM.ISC_ADD;
  
    select count(*)
      into L_exist
      from item_supp_country
     where item = I_item
       and supplier = I_supplier
       and primary_country_ind = 'Y';
  
    if L_exist = 1 then
      L_message_cty_rec := "RIB_XItemSupCtyDesc_REC"(0, --rib oid
                                                     I_message.origin_country_id, --ORIGIN_COUNTRY_ID-VARCHAR2(3)
                                                     'N', --PRIMARY_COUNTRY_IND-VARCHAR2(1)
                                                     I_message.unit_cost, --UNIT_COST-NUMBER(20)
                                                     NULL, --XITEMCOSTDESC_TBL-RIB_XItemCostDesc_TBL()
                                                     NULL, --XISCLOCDESC_TBL-RIB_XISCLocDesc_TBL()
                                                     I_message.lead_time, --LEAD_TIME-NUMBER(4)
                                                     NULL, --PICKUP_LEAD_TIME-NUMBER(4)
                                                     NULL, --MIN_ORDER_QTY-NUMBER(12)
                                                     NULL, --MAX_ORDER_QTY-NUMBER(12)
                                                     NULL, --SUPP_HIER_LVL_1-VARCHAR2(10)
                                                     NULL, --SUPP_HIER_LVL_2-VARCHAR2(10)
                                                     NULL, --SUPP_HIER_LVL_3-VARCHAR2(10)
                                                     I_message.default_uop, --DEFAULT_UOP-VARCHAR2(6)
                                                     I_message.supp_pack_size, --SUPP_PACK_SIZE-NUMBER(12)
                                                     I_message.inner_pack_size, --INNER_PACK_SIZE-NUMBER(12)
                                                     I_message.ti, --TI-NUMBER(12)
                                                     I_message.hi, --HI-NUMBER(12)
                                                     I_message.xiscdimdesc_tbl, --XISCDIMDESC_TBL-RIB_XISCDimDesc_TBL()
                                                     I_message.cost_uom, --COST_UOM-VARCHAR2(4)
                                                     NULL, --TOLERANCE_TYPE-VARCHAR2(6)
                                                     NULL, --MIN_TOLERANCE-NUMBER(12)
                                                     NULL, --MAX_TOLERANCE-NUMBER(12)
                                                     NULL, --SUPP_HIER_TYPE_1-VARCHAR2(6)
                                                     NULL, --SUPP_HIER_TYPE_2-VARCHAR2(6)
                                                     NULL, --SUPP_HIER_TYPE_3-VARCHAR2(6)
                                                     NULL, --ROUND_LVL-VARCHAR2(6)
                                                     NULL, --ROUND_TO_INNER_PCT-NUMBER(12)
                                                     NULL, --ROUND_TO_CASE_PCT-NUMBER(12)
                                                     NULL, --ROUND_TO_LAYER_PCT-NUMBER(12)
                                                     NULL, --ROUND_TO_PALLET_PCT-NUMBER(12)
                                                     NULL); --PACKING_METHOD-VARCHAR2(6)
    
      L_message_cty_tbl := "RIB_XItemSupCtyDesc_TBL"();
      L_message_cty_tbl.extend();
      L_message_cty_tbl(L_message_cty_tbl.last) := L_message_cty_rec;
    else
      L_message_cty_tbl := "RIB_XItemSupCtyDesc_TBL"();
      L_message_cty_tbl.extend();
      L_message_cty_tbl(L_message_cty_tbl.last) := I_message;
    
    end if;
  
    L_message_sup := "RIB_XItemSupDesc_REC"(0, --rib oid
                                            I_supplier, --SUPPLIER-VARCHAR2(10)
                                            NULL, --PRIMARY_SUPP_IND-VARCHAR2(3)
                                            NULL, --VPN-VARCHAR2(30)
                                            NULL, --SUPP_LABEL-VARCHAR2(15)
                                            L_message_cty_tbl, --XITEMSUPCTYDESC_TBL-RIB_XItemSupCtyDesc_TBL()
                                            NULL, --CONSIGNMENT_RATE-NUMBER(12)
                                            NULL, --SUPP_DISCONTINUE_DATE-DATE()
                                            NULL, --DIRECT_SHIP_IND-VARCHAR2(1)
                                            NULL, --PALLET_NAME-VARCHAR2(6)
                                            NULL, --CASE_NAME-VARCHAR2(6)
                                            NULL, --INNER_NAME-VARCHAR2(6)
                                            NULL, --XITMSUPCTYMFRDESC_TBL-RIB_XItmSupCtyMfrDesc_TBL()
                                            NULL, --PRIMARY_CASE_SIZE-VARCHAR2(6)
                                            NULL, --SUPP_DIFF_1-VARCHAR2(120)
                                            NULL, --SUPP_DIFF_2-VARCHAR2(120)
                                            NULL, --SUPP_DIFF_3-VARCHAR2(120)
                                            NULL, --SUPP_DIFF_4-VARCHAR2(120)
                                            NULL, --CONCESSION_RATE-NUMBER(12)
                                            NULL); --LANGOFXITEMSUPDESC_TBL-RIB_LangOfXItemSupDesc_TBL()
  
    L_message_sup_tbl := "RIB_XItemSupDesc_TBL"();
    L_message_sup_tbl.extend();
    L_message_sup_tbl(L_message_sup_tbl.last) := L_message_sup;
  
    L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                     I_item, --ITEM-VARCHAR2(25)
                                     NULL, --ITEM_PARENT-VARCHAR2(25)
                                     NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                     NULL, --PACK_IND-VARCHAR2(1)
                                     NULL, --ITEM_LEVEL-NUMBER(1)
                                     NULL, --TRAN_LEVEL-NUMBER(1)
                                     NULL, --DIFF_1-VARCHAR2(10)
                                     NULL, --DIFF_2-VARCHAR2(10)
                                     NULL, --DIFF_3-VARCHAR2(10)
                                     NULL, --DIFF_4-VARCHAR2(10)
                                     NULL, --DEPT-NUMBER(4)
                                     NULL, --CLASS-NUMBER(4)
                                     NULL, --SUBCLASS-NUMBER(4)
                                     NULL, --ITEM_DESC-VARCHAR2(250)
                                     NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                     NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                     NULL, --SHORT_DESC-VARCHAR2(120)
                                     NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                     NULL, --STANDARD_UOM-VARCHAR2(4)
                                     NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                     NULL, --FORECAST_IND-VARCHAR2(1)
                                     NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                     NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                     NULL, --SELLABLE_IND-VARCHAR2(1)
                                     NULL, --ORDERABLE_IND-VARCHAR2(1)
                                     NULL, --PACK_TYPE-VARCHAR2(1)
                                     NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                     NULL, --COMMENTS-VARCHAR2(2000)
                                     NULL, --CREATE_DATETIME-DATE()
                                     NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                     L_message_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                     NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                     NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                     NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                     NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                     NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                     NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                     NULL, --STATUS-VARCHAR2(1)
                                     NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                     NULL, --PACKAGE_SIZE-NUMBER(12)
                                     NULL, --HANDLING_TEMP-VARCHAR2(6)
                                     NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                     NULL, --MFG_REC_RETAIL-NUMBER(20)
                                     NULL, --WASTE_TYPE-VARCHAR2(6)
                                     NULL, --WASTE_PCT-NUMBER(12)
                                     NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                     NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                     NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                     NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                     NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                     NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                     NULL, --SIZE_GROUP1-NUMBER(4)
                                     NULL, --SIZE_GROUP2-NUMBER(4)
                                     NULL, --SIZE1-VARCHAR2(6)
                                     NULL, --SIZE2-VARCHAR2(6)
                                     NULL, --COLOR-VARCHAR2(24)
                                     NULL, --SYSTEM_IND-VARCHAR2(1)
                                     NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                     NULL, --UPC_TYPE-VARCHAR2(5)
                                     NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                     NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                     NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --PERISHABLE_IND-VARCHAR2(1)
                                     NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                     NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                     NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                     NULL, --ORDER_TYPE-VARCHAR2(6)
                                     NULL, --SALE_TYPE-VARCHAR2(6)
                                     NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                     NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                     NULL, --INVENTORY_IND-VARCHAR2(1)
                                     NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                     NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                     NULL, --PACKAGE_UOM-VARCHAR2(4)
                                     NULL, --FORMAT_ID-VARCHAR2(1)
                                     NULL, --PREFIX-NUMBER(2)
                                     NULL, --BRAND-VARCHAR2(120)
                                     NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                     NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                     NULL, --DESC_UP-VARCHAR2(250)
                                     NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                     NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                     NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                     NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                     NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                     NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                     NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                     NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                     NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                     NULL);
    XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                O_error_message,
                                L_message,
                                L_message_type);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    select item_level, tran_level
      into L_item_level, L_tran_level
      from item_master
     where item = I_item;
  
    if L_tran_level > L_item_level then
    
      if I_message.XISCDIMDESC_TBL is not null and
         I_message.XISCDIMDESC_TBL.count > 0 then
      
        L_message_type := XXADEO_RMSSUB_XITEM.ISMC_ADD;
      
        XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                    O_error_message,
                                    L_message,
                                    L_message_type);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      end if;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_ITEM_SUP_CTY_CRE;

  
   -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_SUP_CTY_MOD
  -- Description: Responsible for create XItemDesc to update item/supplier/country relationship
  --              with information managed by STEP
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_SUP_CTY_MOD(I_item           IN ITEM_MASTER.ITEM%TYPE,
                                        I_supplier       IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_message        IN XXADEO_ITEM_SUP_CTY_TBL,
                                        I_change_primary IN NUMBER,
                                        O_status_code    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error_message  IN OUT VARCHAR2) IS
  
    L_msg_cty_rec  "RIB_XItemSupCtyDesc_REC";
    L_msg_cty_tbl  "RIB_XItemSupCtyDesc_TBL";
    L_message_type VARCHAR2(30);
    L_msg_sup_rec  "RIB_XItemSupDesc_REC";
    L_msg_sup_tbl  "RIB_XItemSupDesc_TBL";
    L_message      "RIB_XItemDesc_REC";
    L_row          ITEM_SUPP_COUNTRY%ROWTYPE;
    L_program      VARCHAR2(55) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_CTY_MOD';
  
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.ISC_UPD;
  
    for i in 1 .. I_message.count() loop
      Select *
        into L_row
        from item_supp_country
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_message(i).origin_country_id;
    
      if L_row.cost_uom != I_message(i).cost_uom or
         L_row.default_uop != I_message(i).default_uop or
         L_row.supp_pack_size != I_message(i).supp_pack_size or
         L_row.inner_pack_size != I_message(i).inner_pack_size or
         L_row.TI != I_message(i).TI or L_row.HI != I_message(i).HI or
         I_change_primary = 1 then
      
        L_msg_cty_rec := "RIB_XItemSupCtyDesc_REC"(0, --rib oid
                                                   I_message                (i)
                                                   .origin_country_id, --ORIGIN_COUNTRY_ID-VARCHAR2(3)
                                                   I_message                (i)
                                                   .PRIMARY_IND, --PRIMARY_COUNTRY_IND-VARCHAR2(1)
                                                   L_row.unit_cost, --UNIT_COST-NUMBER(20)
                                                   NULL, --XITEMCOSTDESC_TBL-RIB_XItemCostDesc_TBL()
                                                   NULL, --XISCLOCDESC_TBL-RIB_XISCLocDesc_TBL()
                                                   NULL, --LEAD_TIME-NUMBER(4)
                                                   NULL, --PICKUP_LEAD_TIME-NUMBER(4)
                                                   NULL, --MIN_ORDER_QTY-NUMBER(12)
                                                   NULL, --MAX_ORDER_QTY-NUMBER(12)
                                                   NULL, --SUPP_HIER_LVL_1-VARCHAR2(10)
                                                   NULL, --SUPP_HIER_LVL_2-VARCHAR2(10)
                                                   NULL, --SUPP_HIER_LVL_3-VARCHAR2(10)
                                                   I_message                (i)
                                                   .default_uop, --DEFAULT_UOP-VARCHAR2(6)
                                                   I_message                (i)
                                                   .supp_pack_size, --SUPP_PACK_SIZE-NUMBER(12)
                                                   I_message                (i)
                                                   .inner_pack_size, --INNER_PACK_SIZE-NUMBER(12)
                                                   I_message                (i).TI, --TI-NUMBER(12)
                                                   I_message                (i).HI, --HI-NUMBER(12)
                                                   NULL, --XISCDIMDESC_TBL-RIB_XISCDimDesc_TBL()
                                                   I_message                (i)
                                                   .cost_uom, --COST_UOM-VARCHAR2(4)
                                                   NULL, --TOLERANCE_TYPE-VARCHAR2(6)
                                                   NULL, --MIN_TOLERANCE-NUMBER(12)
                                                   NULL, --MAX_TOLERANCE-NUMBER(12)
                                                   NULL, --SUPP_HIER_TYPE_1-VARCHAR2(6)
                                                   NULL, --SUPP_HIER_TYPE_2-VARCHAR2(6)
                                                   NULL, --SUPP_HIER_TYPE_3-VARCHAR2(6)
                                                   L_row.round_lvl, --ROUND_LVL-VARCHAR2(6)
                                                   L_row.round_to_inner_pct, --ROUND_TO_INNER_PCT-NUMBER(12)
                                                   L_row.round_to_case_pct, --ROUND_TO_CASE_PCT-NUMBER(12)
                                                   L_row.round_to_layer_pct, --ROUND_TO_LAYER_PCT-NUMBER(12)
                                                   L_row.round_to_pallet_pct, --ROUND_TO_PALLET_PCT-NUMBER(12)
                                                   NULL); --PACKING_METHOD-VARCHAR2(6)
      
        if L_msg_cty_tbl is null then
          L_msg_cty_tbl := "RIB_XItemSupCtyDesc_TBL"();
        end if;
        L_msg_cty_tbl.extend();
        L_msg_cty_tbl(L_msg_cty_tbl.last) := L_msg_cty_rec;
      end if;
    
    end loop;
  
    if L_msg_cty_tbl is not null and L_msg_cty_tbl.count > 0 then
    
      L_msg_sup_rec := "RIB_XItemSupDesc_REC"(0, --rib oid
                                              I_supplier, --SUPPLIER-VARCHAR2(10)
                                              NULL, --PRIMARY_SUPP_IND-VARCHAR2(3)
                                              NULL, --VPN-VARCHAR2(30)
                                              NULL, --SUPP_LABEL-VARCHAR2(15)
                                              L_msg_cty_tbl, --XITEMSUPCTYDESC_TBL-RIB_XItemSupCtyDesc_TBL()
                                              NULL, --CONSIGNMENT_RATE-NUMBER(12)
                                              NULL, --SUPP_DISCONTINUE_DATE-DATE()
                                              NULL, --DIRECT_SHIP_IND-VARCHAR2(1)
                                              NULL, --PALLET_NAME-VARCHAR2(6)
                                              NULL, --CASE_NAME-VARCHAR2(6)
                                              NULL, --INNER_NAME-VARCHAR2(6)
                                              NULL, --XITMSUPCTYMFRDESC_TBL-RIB_XItmSupCtyMfrDesc_TBL()
                                              NULL, --PRIMARY_CASE_SIZE-VARCHAR2(6)
                                              NULL, --SUPP_DIFF_1-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_2-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_3-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_4-VARCHAR2(120)
                                              NULL, --CONCESSION_RATE-NUMBER(12)
                                              NULL); --LANGOFXITEMSUPDESC_TBL-RIB_LangOfXItemSupDesc_TBL()
    
      L_msg_sup_tbl := "RIB_XItemSupDesc_TBL"();
      L_msg_sup_tbl.extend();
      L_msg_sup_tbl(L_msg_sup_tbl.last) := L_msg_sup_rec;
    
      L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                       I_item, --ITEM-VARCHAR2(25)
                                       NULL, --ITEM_PARENT-VARCHAR2(25)
                                       NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                       NULL, --PACK_IND-VARCHAR2(1)
                                       NULL, --ITEM_LEVEL-NUMBER(1)
                                       NULL, --TRAN_LEVEL-NUMBER(1)
                                       NULL, --DIFF_1-VARCHAR2(10)
                                       NULL, --DIFF_2-VARCHAR2(10)
                                       NULL, --DIFF_3-VARCHAR2(10)
                                       NULL, --DIFF_4-VARCHAR2(10)
                                       NULL, --DEPT-NUMBER(4)
                                       NULL, --CLASS-NUMBER(4)
                                       NULL, --SUBCLASS-NUMBER(4)
                                       NULL, --ITEM_DESC-VARCHAR2(250)
                                       NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --SHORT_DESC-VARCHAR2(120)
                                       NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                       NULL, --STANDARD_UOM-VARCHAR2(4)
                                       NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                       NULL, --FORECAST_IND-VARCHAR2(1)
                                       NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                       NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                       NULL, --SELLABLE_IND-VARCHAR2(1)
                                       NULL, --ORDERABLE_IND-VARCHAR2(1)
                                       NULL, --PACK_TYPE-VARCHAR2(1)
                                       NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                       NULL, --COMMENTS-VARCHAR2(2000)
                                       NULL, --CREATE_DATETIME-DATE()
                                       NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                       L_msg_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                       NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                       NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                       NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                       NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                       NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                       NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                       NULL, --STATUS-VARCHAR2(1)
                                       NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                       NULL, --PACKAGE_SIZE-NUMBER(12)
                                       NULL, --HANDLING_TEMP-VARCHAR2(6)
                                       NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                       NULL, --MFG_REC_RETAIL-NUMBER(20)
                                       NULL, --WASTE_TYPE-VARCHAR2(6)
                                       NULL, --WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                       NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                       NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                       NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                       NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                       NULL, --SIZE_GROUP1-NUMBER(4)
                                       NULL, --SIZE_GROUP2-NUMBER(4)
                                       NULL, --SIZE1-VARCHAR2(6)
                                       NULL, --SIZE2-VARCHAR2(6)
                                       NULL, --COLOR-VARCHAR2(24)
                                       NULL, --SYSTEM_IND-VARCHAR2(1)
                                       NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                       NULL, --UPC_TYPE-VARCHAR2(5)
                                       NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                       NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                       NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --PERISHABLE_IND-VARCHAR2(1)
                                       NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                       NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                       NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                       NULL, --ORDER_TYPE-VARCHAR2(6)
                                       NULL, --SALE_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                       NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                       NULL, --INVENTORY_IND-VARCHAR2(1)
                                       NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                       NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                       NULL, --PACKAGE_UOM-VARCHAR2(4)
                                       NULL, --FORMAT_ID-VARCHAR2(1)
                                       NULL, --PREFIX-NUMBER(2)
                                       NULL, --BRAND-VARCHAR2(120)
                                       NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                       NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                       NULL, --DESC_UP-VARCHAR2(250)
                                       NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                       NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                       NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                       NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                       NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                       NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                       NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                       NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                       NULL); --LANGOFXITEMDESC_TBL-RIB_LangOfXItemDesc_TBL()
    
      XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                  O_error_message,
                                  L_message,
                                  L_message_type);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END CREATE_MSG_ITEM_SUP_CTY_MOD;

  
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_SUP_CTY_DEL 
  -- Description: Responsible for create XItemRef to delete item/supplier/country relationship
  --              if item/supplier/country relationship is Primary, first change other 
  --              relationship to be primary and after delete a relationship
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_SUP_CTY_DEL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                        I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_message       IN XXADEO_ITEM_SUP_CTY_TBL,
                                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error_message IN OUT VARCHAR2) IS
  
    L_message_cty      "RIB_XItemSupCtyRef_REC";
    L_message_cty_tbl  "RIB_XItemSupCtyRef_TBL";
    L_message_sup      "RIB_XItemSupRef_REC";
    L_message_sup_tbl  "RIB_XItemSupRef_TBL";
    L_message          "RIB_XItemRef_REC";
    L_message_type     VARCHAR2(30);
    L_program          VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_CTY_DEL';
    L_row              ITEM_SUPP_COUNTRY%ROWTYPE;
    L_primary          ITEM_SUPP_COUNTRY.PRIMARY_COUNTRY_IND%TYPE;
    L_ITEM_SUP_CTY_REC XXADEO_ITEM_SUP_CTY_REC;
    L_ITEM_SUP_CTY_TBL XXADEO_ITEM_SUP_CTY_TBL;
  
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.ISC_DEL;
  
    for i in 1 .. I_message.count loop
      select primary_country_ind
        into L_primary
        from item_supp_country
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_message(i).origin_country_id;
    
      if L_primary = 'Y' then
        select *
          into L_row
          from item_supp_country
         where item = I_item
           and supplier = I_supplier
           and primary_country_ind = 'N'
           and rownum = 1;
      
        L_item_sup_cty_rec := XXADEO_ITEM_SUP_CTY_REC(L_row.origin_country_id,
                                                      'Y',
                                                      L_row.default_uop,
                                                      L_row.supp_pack_size,
                                                      L_row.inner_pack_size,
                                                      L_row.ti,
                                                      L_row.hi,
                                                      L_row.cost_uom);
      
        L_item_sup_cty_tbl := XXADEO_ITEM_SUP_CTY_TBL();
        L_item_sup_cty_tbl.extend();
        L_item_sup_cty_tbl(L_item_sup_cty_tbl.last) := L_item_sup_cty_rec;
        L_item_sup_cty_rec := XXADEO_ITEM_SUP_CTY_REC(I_message(i)
                                                      .origin_country_id,
                                                      'N',
                                                      I_message(i)
                                                      .default_uop,
                                                      I_message(i)
                                                      .supp_pack_size,
                                                      I_message(i)
                                                      .inner_pack_size,
                                                      I_message(i).ti,
                                                      I_message(i).hi,
                                                      I_message(i).cost_uom);
        L_item_sup_cty_tbl.extend();
        L_item_sup_cty_tbl(L_item_sup_cty_tbl.last) := L_item_sup_cty_rec;
      
        CREATE_MSG_ITEM_SUP_CTY_MOD(I_item,
                                    I_supplier,
                                    L_item_sup_cty_tbl,
                                    1,
                                    O_status_code,
                                    O_error_message);
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end if;
      L_message_cty := "RIB_XItemSupCtyRef_REC"(0, --rib oid
                                                I_message(i)
                                                .origin_country_id, --ORIGIN_COUNTRY_ID-VARCHAR2(3)
                                                NULL, --XISCLOCREF_TBL-RIB_XISCLocRef_TBL()
                                                NULL, --XITEMCOSTREF_TBL-RIB_XItemCostRef_TBL()
                                                NULL); --XISCDIMREF_TBL-RIB_XISCDimRef_TBL()
    
      if L_message_cty_tbl is null then
        L_message_cty_tbl := "RIB_XItemSupCtyRef_TBL"();
      end if;
      L_message_cty_tbl.extend();
      L_message_cty_tbl(L_message_cty_tbl.last) := L_message_cty;
    
    end loop;
  
    L_message_sup := "RIB_XItemSupRef_REC"(0, --rib oid
                                           I_supplier, --SUPPLIER-VARCHAR2(10)
                                           'Y', --DELETE_CHILDREN_IND-VARCHAR2(1)
                                           L_message_cty_tbl, --XITEMSUPCTYREF_TBL-RIB_XItemSupCtyRef_TBL()
                                           NULL, --XITEMSUPCTYMFRREF_TBL-RIB_XItemSupCtyMfrRef_TBL()
                                           NULL); --LANGOFXITEMSUPREF_TBL-RIB_LangOfXItemSupRef_TBL()
  
    L_message_sup_tbl := "RIB_XItemSupRef_TBL"();
    L_message_sup_tbl.extend();
    L_message_sup_tbl(L_message_sup_tbl.LAST) := L_message_sup;
  
    L_message := "RIB_XItemRef_REC"(0, --rib oid
                                    I_item, --ITEM-VARCHAR2(25)
                                    NULL, --HIER_LEVEL-VARCHAR2(2)
                                    NULL, --XITEMCTRYREF_TBL-RIB_XItemCtryRef_TBL()
                                    L_message_sup_tbl, --XITEMSUPREF_TBL-RIB_XItemSupRef_TBL()
                                    NULL, --XITEMVATREF_TBL-RIB_XItemVATRef_TBL()
                                    NULL, --XITEMIMAGEREF_TBL-RIB_XItemImageRef_TBL()
                                    NULL, --XITEMSEASONREF_TBL-RIB_XItemSeasonRef_TBL()
                                    NULL, --XITEMUDAREF_TBL-RIB_XItemUDARef_TBL()
                                    NULL, --XITEMBOMREF_TBL-RIB_XItemBOMRef_TBL()
                                    NULL, --SYSTEM_IND-VARCHAR2(1)
                                    NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                    NULL); --LANGOFXITEMREF_TBL-RIB_LangOfXItemRef_TBL()
  
    XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                O_error_message,
                                L_message,
                                L_message_type);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_ITEM_SUP_CTY_DEL;
  

   -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ISUP_MANU_CTY_MOD
  -- Description: Responsible for update Item/Supplier/Manufacturer Country realtionship
  --              with information managed by STEP
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ISUP_MANU_CTY_MOD(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                         I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                         I_message       IN XXADEO_ITEM_SUP_CTY_MFR_TBL,
                                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_error_message IN OUT VARCHAR2) IS
  
    L_message_manu     "RIB_XItmSupCtyMfrDesc_REC";
    L_message_manu_tbl "RIB_XItmSupCtyMfrDesc_TBL";
    L_message_sup      "RIB_XItemSupDesc_REC";
    L_message_sup_tbl  "RIB_XItemSupDesc_TBL";
    L_message          "RIB_XItemDesc_REC";
    L_message_type     VARCHAR2(30);
    L_program          VARCHAR2(55) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ISUP_MANU_CTY_CRE';
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.ISMC_UPD;
    for i in 1 .. I_message.count loop
    
      L_message_manu := "RIB_XItmSupCtyMfrDesc_REC"(0, --rib oid
                                                    I_message(i)
                                                    .manu_country_id, --MANUFACTURER_CTRY_ID-VARCHAR2(23)
                                                    I_message(i)
                                                    .primary_manu_country_id); --PRIMARY_MANUFACTURER_CTRY_IND-VARCHAR2(1)
    
      if L_message_manu_tbl is null then
        L_message_manu_tbl := "RIB_XItmSupCtyMfrDesc_TBL"();
      end if;
      L_message_manu_tbl.extend();
      L_message_manu_tbl(L_message_manu_tbl.LAST) := L_message_manu;
    end loop;
  
    if L_message_manu_tbl is not null and L_message_manu_tbl.count > 0 then
      L_message_sup := "RIB_XItemSupDesc_REC"(0, --rib oid
                                              I_supplier, --SUPPLIER-VARCHAR2(10)
                                              NULL, --PRIMARY_SUPP_IND-VARCHAR2(3)
                                              NULL, --VPN-VARCHAR2(30)
                                              NULL, --SUPP_LABEL-VARCHAR2(15)
                                              NULL, --XITEMSUPCTYDESC_TBL-RIB_XItemSupCtyDesc_TBL()
                                              NULL, --CONSIGNMENT_RATE-NUMBER(12)
                                              NULL, --SUPP_DISCONTINUE_DATE-DATE()
                                              NULL, --DIRECT_SHIP_IND-VARCHAR2(1)
                                              NULL, --PALLET_NAME-VARCHAR2(6)
                                              NULL, --CASE_NAME-VARCHAR2(6)
                                              NULL, --INNER_NAME-VARCHAR2(6)
                                              L_message_manu_tbl, --XITMSUPCTYMFRDESC_TBL-RIB_XItmSupCtyMfrDesc_TBL()
                                              NULL, --PRIMARY_CASE_SIZE-VARCHAR2(6)
                                              NULL, --SUPP_DIFF_1-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_2-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_3-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_4-VARCHAR2(120)
                                              NULL, --CONCESSION_RATE-NUMBER(12)
                                              NULL);
    
      L_message_sup_tbl := "RIB_XItemSupDesc_TBL"();
      L_message_sup_tbl.extend();
      L_message_sup_tbl(L_message_sup_tbl.LAST) := L_message_sup;
    
      L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                       I_item, --ITEM-VARCHAR2(25)
                                       NULL, --ITEM_PARENT-VARCHAR2(25)
                                       NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                       NULL, --PACK_IND-VARCHAR2(1)
                                       NULL, --ITEM_LEVEL-NUMBER(1)
                                       NULL, --TRAN_LEVEL-NUMBER(1)
                                       NULL, --DIFF_1-VARCHAR2(10)
                                       NULL, --DIFF_2-VARCHAR2(10)
                                       NULL, --DIFF_3-VARCHAR2(10)
                                       NULL, --DIFF_4-VARCHAR2(10)
                                       NULL, --DEPT-NUMBER(4)
                                       NULL, --CLASS-NUMBER(4)
                                       NULL, --SUBCLASS-NUMBER(4)
                                       NULL, --ITEM_DESC-VARCHAR2(250)
                                       NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --SHORT_DESC-VARCHAR2(120)
                                       NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                       NULL, --STANDARD_UOM-VARCHAR2(4)
                                       NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                       NULL, --FORECAST_IND-VARCHAR2(1)
                                       NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                       NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                       NULL, --SELLABLE_IND-VARCHAR2(1)
                                       NULL, --ORDERABLE_IND-VARCHAR2(1)
                                       NULL, --PACK_TYPE-VARCHAR2(1)
                                       NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                       NULL, --COMMENTS-VARCHAR2(2000)
                                       NULL, --CREATE_DATETIME-DATE()
                                       NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                       L_message_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                       NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                       NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                       NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                       NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                       NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                       NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                       NULL, --STATUS-VARCHAR2(1)
                                       NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                       NULL, --PACKAGE_SIZE-NUMBER(12)
                                       NULL, --HANDLING_TEMP-VARCHAR2(6)
                                       NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                       NULL, --MFG_REC_RETAIL-NUMBER(20)
                                       NULL, --WASTE_TYPE-VARCHAR2(6)
                                       NULL, --WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                       NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                       NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                       NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                       NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                       NULL, --SIZE_GROUP1-NUMBER(4)
                                       NULL, --SIZE_GROUP2-NUMBER(4)
                                       NULL, --SIZE1-VARCHAR2(6)
                                       NULL, --SIZE2-VARCHAR2(6)
                                       NULL, --COLOR-VARCHAR2(24)
                                       NULL, --SYSTEM_IND-VARCHAR2(1)
                                       NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                       NULL, --UPC_TYPE-VARCHAR2(5)
                                       NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                       NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                       NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --PERISHABLE_IND-VARCHAR2(1)
                                       NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                       NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                       NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                       NULL, --ORDER_TYPE-VARCHAR2(6)
                                       NULL, --SALE_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                       NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                       NULL, --INVENTORY_IND-VARCHAR2(1)
                                       NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                       NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                       NULL, --PACKAGE_UOM-VARCHAR2(4)
                                       NULL, --FORMAT_ID-VARCHAR2(1)
                                       NULL, --PREFIX-NUMBER(2)
                                       NULL, --BRAND-VARCHAR2(120)
                                       NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                       NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                       NULL, --DESC_UP-VARCHAR2(250)
                                       NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                       NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                       NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                       NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                       NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                       NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                       NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                       NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                       NULL); --LANGOFXITEMDESC_TBL-RIB_LangOfXItemDesc_TBL()
    
      XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                  O_error_message,
                                  L_message,
                                  L_message_type);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_ISUP_MANU_CTY_MOD;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ISUP_MANU_CTY_DEL
  -- Description: Responsible for create XItemRef to delete Item/Supplier/Manufacturer Country relationship
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ISUP_MANU_CTY_DEL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                         I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                         I_message       IN XXADEO_ITEM_SUP_CTY_MFR_TBL,
                                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_error_message IN OUT VARCHAR2) IS
  
    L_message_manu          "RIB_XItemSupCtyMfrRef_REC";
    L_message_manu_tbl      "RIB_XItemSupCtyMfrRef_TBL";
    L_message_sup           "RIB_XItemSupRef_REC";
    L_message_sup_tbl       "RIB_XItemSupRef_TBL";
    L_message               "RIB_XItemRef_REC";
    L_message_type          VARCHAR2(30);
    L_program               VARCHAR2(55) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ISUP_MANU_CTY_DEL';
    L_item_sup_cty_manu_rec XXADEO_ITEM_SUP_CTY_MFR_REC;
    L_item_sup_cty_manu_tbl XXADEO_ITEM_SUP_CTY_MFR_TBL;
    L_country_id            ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE;
    L_primary               ITEM_SUPP_MANU_COUNTRY.PRIMARY_MANU_CTRY_IND%TYPE;
    L_exist                 NUMBER;
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.ISMC_DEL;
    for i in 1 .. I_message.count loop
      select primary_manu_ctry_ind
        into L_primary
        from item_supp_manu_country
       where item = I_item
         and supplier = I_supplier
         and manu_country_id = I_message(i).manu_country_id;
    
      if L_primary = 'Y' then
        select count(*)
          into L_exist
          from item_supp_manu_country
         where item = I_item
           and supplier = I_supplier
           and manu_country_id != I_message(i).manu_country_id
           and primary_manu_ctry_ind = 'N';
      
        if L_exist > 0 then
        
          select manu_country_id
            into L_country_id
            from item_supp_manu_country
           where item = I_item
             and supplier = I_supplier
             and primary_manu_ctry_ind = 'N'
             and rownum = 1;
        
          L_item_sup_cty_manu_rec := XXADEO_ITEM_SUP_CTY_MFR_REC(L_country_id,
                                                                 'Y');
          L_item_sup_cty_manu_tbl := XXADEO_ITEM_SUP_CTY_MFR_TBL();
          L_item_sup_cty_manu_tbl.extend();
          L_item_sup_cty_manu_tbl(L_item_sup_cty_manu_tbl.last) := L_item_sup_cty_manu_rec;
          L_item_sup_cty_manu_rec := XXADEO_ITEM_SUP_CTY_MFR_REC(I_message(i)
                                                                 .manu_country_id,
                                                                 'N');
          L_item_sup_cty_manu_tbl.extend();
          L_item_sup_cty_manu_tbl(L_item_sup_cty_manu_tbl.last) := L_item_sup_cty_manu_rec;
        
          CREATE_MSG_ISUP_MANU_CTY_MOD(I_item,
                                       I_supplier,
                                       L_item_sup_cty_manu_tbl,
                                       O_status_code,
                                       O_error_message);
        
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        end if;
      end if;
    
      L_message_manu := "RIB_XItemSupCtyMfrRef_REC"(0, --rib oid
                                                    I_message(i)
                                                    .manu_country_id); --MANUFACTURER_CTRY_ID-VARCHAR2(23)
    
      if L_message_manu_tbl is null then
        L_message_manu_tbl := "RIB_XItemSupCtyMfrRef_TBL"();
      end if;
      L_message_manu_tbl.extend();
      L_message_manu_tbl(L_message_manu_tbl.LAST) := L_message_manu;
    end loop;
  
    if L_message_manu_tbl is not null and L_message_manu_tbl.count > 0 then
      L_message_sup := "RIB_XItemSupRef_REC"(0, --rib oid
                                             I_supplier, --SUPPLIER-VARCHAR2(10)
                                             NULL, --DELETE_CHILDREN_IND-VARCHAR2(1)
                                             NULL, --XITEMSUPCTYREF_TBL-RIB_XItemSupCtyRef_TBL()
                                             L_message_manu_tbl, --XITEMSUPCTYMFRREF_TBL-RIB_XItemSupCtyMfrRef_TBL()
                                             NULL); --LANGOFXITEMSUPREF_TBL-RIB_LangOfXItemSupRef_TBL()
    
      L_message_sup_tbl := "RIB_XItemSupRef_TBL"();
      L_message_sup_tbl.extend();
      L_message_sup_tbl(L_message_sup_tbl.LAST) := L_message_sup;
    
      L_message := "RIB_XItemRef_REC"(0, --rib oid
                                      I_item, --ITEM-VARCHAR2(25)
                                      NULL, --HIER_LEVEL-VARCHAR2(2)
                                      NULL, --XITEMCTRYREF_TBL-RIB_XItemCtryRef_TBL()
                                      L_message_sup_tbl, --XITEMSUPREF_TBL-RIB_XItemSupRef_TBL()
                                      NULL, --XITEMVATREF_TBL-RIB_XItemVATRef_TBL()
                                      NULL, --XITEMIMAGEREF_TBL-RIB_XItemImageRef_TBL()
                                      NULL, --XITEMSEASONREF_TBL-RIB_XItemSeasonRef_TBL()
                                      NULL, --XITEMUDAREF_TBL-RIB_XItemUDARef_TBL()
                                      NULL, --XITEMBOMREF_TBL-RIB_XItemBOMRef_TBL()
                                      NULL, --SYSTEM_IND-VARCHAR2(1)
                                      NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                      NULL); --LANGOFXITEMREF_TBL-RIB_LangOfXItemRef_TBL()
    
      XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                  O_error_message,
                                  L_message,
                                  L_message_type);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_ISUP_MANU_CTY_DEL;

 
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ISUP_MANU_CTY_CRE
  -- Description: Responsible to create XItemDesc for new Item/Supplier/Manufacturer Country relationship
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ISUP_MANU_CTY_CRE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                         I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                         I_message       IN XXADEO_ITEM_SUP_CTY_MFR_TBL,
                                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_error_message IN OUT VARCHAR2) IS
  
    L_message_manu     "RIB_XItmSupCtyMfrDesc_REC";
    L_message_manu_tbl "RIB_XItmSupCtyMfrDesc_TBL";
    L_message_sup      "RIB_XItemSupDesc_REC";
    L_message_sup_tbl  "RIB_XItemSupDesc_TBL";
    L_message          "RIB_XItemDesc_REC";
    L_exist            NUMBER;
    L_message_type     VARCHAR2(30);
    L_program          VARCHAR2(55) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ISUP_MANU_CTY_CRE';
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.ISMC_ADD;
    for i in 1 .. I_message.count loop
    
      select count(*)
        into L_exist
        from item_supp_manu_country
       where item = I_item
         and supplier = I_supplier
         and primary_manu_ctry_ind = 'Y';
    
      if L_exist > 0 then
        L_message_manu := "RIB_XItmSupCtyMfrDesc_REC"(0, --rib oid
                                                      I_message(i)
                                                      .manu_country_id, --MANUFACTURER_CTRY_ID-VARCHAR2(23)
                                                      'N'); --PRIMARY_MANUFACTURER_CTRY_IND-VARCHAR2(1)
      
      else
        L_message_manu := "RIB_XItmSupCtyMfrDesc_REC"(0, --rib oid
                                                      I_message(i)
                                                      .manu_country_id, --MANUFACTURER_CTRY_ID-VARCHAR2(23)
                                                      'Y'); --PRIMARY_MANUFACTURER_CTRY_IND-VARCHAR2(1)
      end if;
    
      if L_message_manu_tbl is null then
        L_message_manu_tbl := "RIB_XItmSupCtyMfrDesc_TBL"();
      end if;
      L_message_manu_tbl.extend();
      L_message_manu_tbl(L_message_manu_tbl.LAST) := L_message_manu;
    end loop;
  
    if L_message_manu_tbl is not null and L_message_manu_tbl.count > 0 then
      L_message_sup := "RIB_XItemSupDesc_REC"(0, --rib oid
                                              I_supplier, --SUPPLIER-VARCHAR2(10)
                                              NULL, --PRIMARY_SUPP_IND-VARCHAR2(3)
                                              NULL, --VPN-VARCHAR2(30)
                                              NULL, --SUPP_LABEL-VARCHAR2(15)
                                              NULL, --XITEMSUPCTYDESC_TBL-RIB_XItemSupCtyDesc_TBL()
                                              NULL, --CONSIGNMENT_RATE-NUMBER(12)
                                              NULL, --SUPP_DISCONTINUE_DATE-DATE()
                                              NULL, --DIRECT_SHIP_IND-VARCHAR2(1)
                                              NULL, --PALLET_NAME-VARCHAR2(6)
                                              NULL, --CASE_NAME-VARCHAR2(6)
                                              NULL, --INNER_NAME-VARCHAR2(6)
                                              L_message_manu_tbl, --XITMSUPCTYMFRDESC_TBL-RIB_XItmSupCtyMfrDesc_TBL()
                                              NULL, --PRIMARY_CASE_SIZE-VARCHAR2(6)
                                              NULL, --SUPP_DIFF_1-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_2-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_3-VARCHAR2(120)
                                              NULL, --SUPP_DIFF_4-VARCHAR2(120)
                                              NULL, --CONCESSION_RATE-NUMBER(12)
                                              NULL);
    
      L_message_sup_tbl := "RIB_XItemSupDesc_TBL"();
      L_message_sup_tbl.extend();
      L_message_sup_tbl(L_message_sup_tbl.LAST) := L_message_sup;
    
      L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                       I_item, --ITEM-VARCHAR2(25)
                                       NULL, --ITEM_PARENT-VARCHAR2(25)
                                       NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                       NULL, --PACK_IND-VARCHAR2(1)
                                       NULL, --ITEM_LEVEL-NUMBER(1)
                                       NULL, --TRAN_LEVEL-NUMBER(1)
                                       NULL, --DIFF_1-VARCHAR2(10)
                                       NULL, --DIFF_2-VARCHAR2(10)
                                       NULL, --DIFF_3-VARCHAR2(10)
                                       NULL, --DIFF_4-VARCHAR2(10)
                                       NULL, --DEPT-NUMBER(4)
                                       NULL, --CLASS-NUMBER(4)
                                       NULL, --SUBCLASS-NUMBER(4)
                                       NULL, --ITEM_DESC-VARCHAR2(250)
                                       NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --SHORT_DESC-VARCHAR2(120)
                                       NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                       NULL, --STANDARD_UOM-VARCHAR2(4)
                                       NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                       NULL, --FORECAST_IND-VARCHAR2(1)
                                       NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                       NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                       NULL, --SELLABLE_IND-VARCHAR2(1)
                                       NULL, --ORDERABLE_IND-VARCHAR2(1)
                                       NULL, --PACK_TYPE-VARCHAR2(1)
                                       NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                       NULL, --COMMENTS-VARCHAR2(2000)
                                       NULL, --CREATE_DATETIME-DATE()
                                       NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                       L_message_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                       NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                       NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                       NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                       NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                       NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                       NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                       NULL, --STATUS-VARCHAR2(1)
                                       NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                       NULL, --PACKAGE_SIZE-NUMBER(12)
                                       NULL, --HANDLING_TEMP-VARCHAR2(6)
                                       NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                       NULL, --MFG_REC_RETAIL-NUMBER(20)
                                       NULL, --WASTE_TYPE-VARCHAR2(6)
                                       NULL, --WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                       NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                       NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                       NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                       NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                       NULL, --SIZE_GROUP1-NUMBER(4)
                                       NULL, --SIZE_GROUP2-NUMBER(4)
                                       NULL, --SIZE1-VARCHAR2(6)
                                       NULL, --SIZE2-VARCHAR2(6)
                                       NULL, --COLOR-VARCHAR2(24)
                                       NULL, --SYSTEM_IND-VARCHAR2(1)
                                       NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                       NULL, --UPC_TYPE-VARCHAR2(5)
                                       NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                       NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                       NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --PERISHABLE_IND-VARCHAR2(1)
                                       NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                       NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                       NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                       NULL, --ORDER_TYPE-VARCHAR2(6)
                                       NULL, --SALE_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                       NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                       NULL, --INVENTORY_IND-VARCHAR2(1)
                                       NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                       NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                       NULL, --PACKAGE_UOM-VARCHAR2(4)
                                       NULL, --FORMAT_ID-VARCHAR2(1)
                                       NULL, --PREFIX-NUMBER(2)
                                       NULL, --BRAND-VARCHAR2(120)
                                       NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                       NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                       NULL, --DESC_UP-VARCHAR2(250)
                                       NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                       NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                       NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                       NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                       NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                       NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                       NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                       NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                       NULL); --LANGOFXITEMDESC_TBL-RIB_LangOfXItemDesc_TBL()
    
      XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                  O_error_message,
                                  L_message,
                                  L_message_type);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END CREATE_MSG_ISUP_MANU_CTY_CRE;


   -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ISUP_CTY_DIM_CRE 
  -- Description: Responsible for create a XItemDesc message to create Item/Supplier/Country/Dimensions
  --              relationship
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ISUP_CTY_DIM_CRE(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                        I_supplier          IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_origin_country_id IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                        I_isup_cty_dim_add  IN "RIB_XISCDimDesc_REC",
                                        O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error_message     IN OUT VARCHAR2) IS
  
    L_message_type VARCHAR2(30);
    L_message      "RIB_XItemDesc_REC";
    L_msg_dim_tbl  "RIB_XISCDimDesc_TBL";
    L_msg_cty_rec  "RIB_XItemSupCtyDesc_REC";
    L_msg_cty_tbl  "RIB_XItemSupCtyDesc_TBL";
    L_msg_sup_rec  "RIB_XItemSupDesc_REC";
    L_msg_sup_tbl  "RIB_XItemSupDesc_TBL";
    L_program      VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ISUP_CTY_DIM_CRE';
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.ISCD_ADD;
  
    L_msg_dim_tbl := "RIB_XISCDimDesc_TBL"();
  
    L_msg_dim_tbl.extend();
    L_msg_dim_tbl(L_msg_dim_tbl.last) := I_isup_cty_dim_add;
  
    L_msg_cty_rec := "RIB_XItemSupCtyDesc_REC"(0, --rib oid
                                               I_origin_country_id, --ORIGIN_COUNTRY_ID-VARCHAR2(3)
                                               NULL, --PRIMARY_COUNTRY_IND-VARCHAR2(1)
                                               NULL, --UNIT_COST-NUMBER(20)
                                               NULL, --XITEMCOSTDESC_TBL-RIB_XItemCostDesc_TBL()
                                               NULL, --XISCLOCDESC_TBL-RIB_XISCLocDesc_TBL()
                                               NULL, --LEAD_TIME-NUMBER(4)
                                               NULL, --PICKUP_LEAD_TIME-NUMBER(4)
                                               NULL, --MIN_ORDER_QTY-NUMBER(12)
                                               NULL, --MAX_ORDER_QTY-NUMBER(12)
                                               NULL, --SUPP_HIER_LVL_1-VARCHAR2(10)
                                               NULL, --SUPP_HIER_LVL_2-VARCHAR2(10)
                                               NULL, --SUPP_HIER_LVL_3-VARCHAR2(10)
                                               NULL, --DEFAULT_UOP-VARCHAR2(6)
                                               NULL, --SUPP_PACK_SIZE-NUMBER(12)
                                               NULL, --INNER_PACK_SIZE-NUMBER(12)
                                               NULL, --TI-NUMBER(12)
                                               NULL, --HI-NUMBER(12)
                                               L_msg_dim_tbl, --XISCDIMDESC_TBL-RIB_XISCDimDesc_TBL()
                                               NULL, --COST_UOM-VARCHAR2(4)
                                               NULL, --TOLERANCE_TYPE-VARCHAR2(6)
                                               NULL, --MIN_TOLERANCE-NUMBER(12)
                                               NULL, --MAX_TOLERANCE-NUMBER(12)
                                               NULL, --SUPP_HIER_TYPE_1-VARCHAR2(6)
                                               NULL, --SUPP_HIER_TYPE_2-VARCHAR2(6)
                                               NULL, --SUPP_HIER_TYPE_3-VARCHAR2(6)
                                               NULL, --ROUND_LVL-VARCHAR2(6)
                                               NULL, --ROUND_TO_INNER_PCT-NUMBER(12)
                                               NULL, --ROUND_TO_CASE_PCT-NUMBER(12)
                                               NULL, --ROUND_TO_LAYER_PCT-NUMBER(12)
                                               NULL, --ROUND_TO_PALLET_PCT-NUMBER(12)
                                               NULL); --PACKING_METHOD-VARCHAR2(6)
  
    L_msg_cty_tbl := "RIB_XItemSupCtyDesc_TBL"();
    L_msg_cty_tbl.extend();
    L_msg_cty_tbl(L_msg_cty_tbl.last) := L_msg_cty_rec;
  
    L_msg_sup_rec := "RIB_XItemSupDesc_REC"(0, --rib oid
                                            I_supplier, --SUPPLIER-VARCHAR2(10)
                                            NULL, --PRIMARY_SUPP_IND-VARCHAR2(3)
                                            NULL, --VPN-VARCHAR2(30)
                                            NULL, --SUPP_LABEL-VARCHAR2(15)
                                            L_msg_cty_tbl, --XITEMSUPCTYDESC_TBL-RIB_XItemSupCtyDesc_TBL()
                                            NULL, --CONSIGNMENT_RATE-NUMBER(12)
                                            NULL, --SUPP_DISCONTINUE_DATE-DATE()
                                            NULL, --DIRECT_SHIP_IND-VARCHAR2(1)
                                            NULL, --PALLET_NAME-VARCHAR2(6)
                                            NULL, --CASE_NAME-VARCHAR2(6)
                                            NULL, --INNER_NAME-VARCHAR2(6)
                                            NULL, --XITMSUPCTYMFRDESC_TBL-RIB_XItmSupCtyMfrDesc_TBL()
                                            NULL, --PRIMARY_CASE_SIZE-VARCHAR2(6)
                                            NULL, --SUPP_DIFF_1-VARCHAR2(120)
                                            NULL, --SUPP_DIFF_2-VARCHAR2(120)
                                            NULL, --SUPP_DIFF_3-VARCHAR2(120)
                                            NULL, --SUPP_DIFF_4-VARCHAR2(120)
                                            NULL, --CONCESSION_RATE-NUMBER(12)
                                            NULL); --LANGOFXITEMSUPDESC_TBL-RIB_LangOfXItemSupDesc_TBL()
  
    L_msg_sup_tbl := "RIB_XItemSupDesc_TBL"();
    L_msg_sup_tbl.extend();
    L_msg_sup_tbl(L_msg_sup_tbl.last) := L_msg_sup_rec;
  
    L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                     I_item, --ITEM-VARCHAR2(25)
                                     NULL, --ITEM_PARENT-VARCHAR2(25)
                                     NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                     NULL, --PACK_IND-VARCHAR2(1)
                                     NULL, --ITEM_LEVEL-NUMBER(1)
                                     NULL, --TRAN_LEVEL-NUMBER(1)
                                     NULL, --DIFF_1-VARCHAR2(10)
                                     NULL, --DIFF_2-VARCHAR2(10)
                                     NULL, --DIFF_3-VARCHAR2(10)
                                     NULL, --DIFF_4-VARCHAR2(10)
                                     NULL, --DEPT-NUMBER(4)
                                     NULL, --CLASS-NUMBER(4)
                                     NULL, --SUBCLASS-NUMBER(4)
                                     NULL, --ITEM_DESC-VARCHAR2(250)
                                     NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                     NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                     NULL, --SHORT_DESC-VARCHAR2(120)
                                     NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                     NULL, --STANDARD_UOM-VARCHAR2(4)
                                     NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                     NULL, --FORECAST_IND-VARCHAR2(1)
                                     NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                     NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                     NULL, --SELLABLE_IND-VARCHAR2(1)
                                     NULL, --ORDERABLE_IND-VARCHAR2(1)
                                     NULL, --PACK_TYPE-VARCHAR2(1)
                                     NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                     NULL, --COMMENTS-VARCHAR2(2000)
                                     NULL, --CREATE_DATETIME-DATE()
                                     NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                     L_msg_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                     NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                     NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                     NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                     NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                     NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                     NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                     NULL, --STATUS-VARCHAR2(1)
                                     NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                     NULL, --PACKAGE_SIZE-NUMBER(12)
                                     NULL, --HANDLING_TEMP-VARCHAR2(6)
                                     NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                     NULL, --MFG_REC_RETAIL-NUMBER(20)
                                     NULL, --WASTE_TYPE-VARCHAR2(6)
                                     NULL, --WASTE_PCT-NUMBER(12)
                                     NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                     NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                     NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                     NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                     NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                     NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                     NULL, --SIZE_GROUP1-NUMBER(4)
                                     NULL, --SIZE_GROUP2-NUMBER(4)
                                     NULL, --SIZE1-VARCHAR2(6)
                                     NULL, --SIZE2-VARCHAR2(6)
                                     NULL, --COLOR-VARCHAR2(24)
                                     NULL, --SYSTEM_IND-VARCHAR2(1)
                                     NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                     NULL, --UPC_TYPE-VARCHAR2(5)
                                     NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                     NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                     NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --PERISHABLE_IND-VARCHAR2(1)
                                     NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                     NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                     NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                     NULL, --ORDER_TYPE-VARCHAR2(6)
                                     NULL, --SALE_TYPE-VARCHAR2(6)
                                     NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                     NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                     NULL, --INVENTORY_IND-VARCHAR2(1)
                                     NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                     NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                     NULL, --PACKAGE_UOM-VARCHAR2(4)
                                     NULL, --FORMAT_ID-VARCHAR2(1)
                                     NULL, --PREFIX-NUMBER(2)
                                     NULL, --BRAND-VARCHAR2(120)
                                     NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                     NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                     NULL, --DESC_UP-VARCHAR2(250)
                                     NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                     NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                     NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                     NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                     NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                     NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                     NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                     NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                     NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                     NULL); --LANGOFXITEMDESC_TBL-RIB_LangOfXItemDesc_TBL()
  
    XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                O_error_message,
                                L_message,
                                L_message_type);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_ISUP_CTY_DIM_CRE;

  
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ISUP_CTY_DIM_DEL
  -- Description: Responsible for create XItemRef to delete item/supplier/country/dimensions
  --              relationship
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ISUP_CTY_DIM_DEL(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                        I_supplier          IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_origin_country_id IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                        I_isup_cty_dim_del  IN xxadeo_item_sup_cty_dim_tbl,
                                        O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error_message     IN OUT VARCHAR2) IS
    L_message_type VARCHAR2(30);
    L_message      "RIB_XItemRef_REC";
    L_msg_dim_rec  "RIB_XISCDimRef_REC";
    L_msg_dim_tbl  "RIB_XISCDimRef_TBL";
    L_msg_sup_rec  "RIB_XItemSupRef_REC";
    L_msg_sup_tbl  "RIB_XItemSupRef_TBL";
    L_msg_cty_rec  "RIB_XItemSupCtyRef_REC";
    L_msg_cty_tbl  "RIB_XItemSupCtyRef_TBL";
    L_program      VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ISUP_CTY_DIM_DEL';
  
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.ISCD_DEL;
    for i in 1 .. I_isup_cty_dim_del.count loop
      L_msg_dim_rec := "RIB_XISCDimRef_REC"(0,
                                            I_isup_cty_dim_del(i).dim_object);
    
      if L_msg_dim_tbl is null then
        L_msg_dim_tbl := "RIB_XISCDimRef_TBL"();
      end if;
      L_msg_dim_tbl.extend();
      L_msg_dim_tbl(L_msg_dim_tbl.last) := L_msg_dim_rec;
    end loop;
  
    L_msg_cty_rec := "RIB_XItemSupCtyRef_REC"(0, --rib oid
                                              I_origin_country_id, --ORIGIN_COUNTRY_ID-VARCHAR2(3)
                                              NULL, --XISCLOCREF_TBL-RIB_XISCLocRef_TBL()
                                              NULL, --XITEMCOSTREF_TBL-RIB_XItemCostRef_TBL()
                                              L_msg_dim_tbl); --XISCDIMREF_TBL-RIB_XISCDimRef_TBL()
  
    L_msg_cty_tbl := "RIB_XItemSupCtyRef_TBL"();
    L_msg_cty_tbl.extend();
    L_msg_cty_tbl(L_msg_cty_tbl.last) := L_msg_cty_rec;
  
    L_msg_sup_rec := "RIB_XItemSupRef_REC"(0, --rib oid
                                           I_supplier, --SUPPLIER-VARCHAR2(10)
                                           NULL, --DELETE_CHILDREN_IND-VARCHAR2(1)
                                           L_msg_cty_tbl, --XITEMSUPCTYREF_TBL-RIB_XItemSupCtyRef_TBL()
                                           NULL, --XITEMSUPCTYMFRREF_TBL-RIB_XItemSupCtyMfrRef_TBL()
                                           NULL); --LANGOFXITEMSUPREF_TBL-RIB_LangOfXItemSupRef_TBL()
  
    L_msg_sup_tbl := "RIB_XItemSupRef_TBL"();
    L_msg_sup_tbl.extend();
    L_msg_sup_tbl(L_msg_sup_tbl.LAST) := L_msg_sup_rec;
  
    L_message := "RIB_XItemRef_REC"(0, --rib oid
                                    I_item, --ITEM-VARCHAR2(25)
                                    NULL, --HIER_LEVEL-VARCHAR2(2)
                                    NULL, --XITEMCTRYREF_TBL-RIB_XItemCtryRef_TBL()
                                    L_msg_sup_tbl, --XITEMSUPREF_TBL-RIB_XItemSupRef_TBL()
                                    NULL, --XITEMVATREF_TBL-RIB_XItemVATRef_TBL()
                                    NULL, --XITEMIMAGEREF_TBL-RIB_XItemImageRef_TBL()
                                    NULL, --XITEMSEASONREF_TBL-RIB_XItemSeasonRef_TBL()
                                    NULL, --XITEMUDAREF_TBL-RIB_XItemUDARef_TBL()
                                    NULL, --XITEMBOMREF_TBL-RIB_XItemBOMRef_TBL()
                                    NULL, --SYSTEM_IND-VARCHAR2(1)
                                    NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                    NULL); --LANGOFXITEMREF_TBL-RIB_LangOfXItemRef_TBL()
  
    XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                O_error_message,
                                L_message,
                                L_message_type);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_ISUP_CTY_DIM_DEL;

 
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ISUP_CTY_DIM_MOD
  -- Description: Responsible for create XItemDesc to update item/supplier/country/dimensions
  --              relationship with information managed by STEP
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ISUP_CTY_DIM_MOD(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                        I_supplier          IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_origin_country_id IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                        I_isup_cty_dim_mod  IN "RIB_XISCDimDesc_REC",
                                        O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error_message     IN OUT VARCHAR2) IS
  
    L_message_type VARCHAR2(30);
    L_message      "RIB_XItemDesc_REC";
    L_msg_dim_tbl  "RIB_XISCDimDesc_TBL";
    L_msg_sup_rec  "RIB_XItemSupDesc_REC";
    L_msg_sup_tbl  "RIB_XItemSupDesc_TBL";
    L_msg_cty_rec  "RIB_XItemSupCtyDesc_REC";
    L_msg_cty_tbl  "RIB_XItemSupCtyDesc_TBL";
    L_program      VARCHAR2(55) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ISUP_CTY_DIM_MOD';
  
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.ISCD_UPD;
  
    L_msg_dim_tbl := "RIB_XISCDimDesc_TBL"();
    L_msg_dim_tbl.extend();
    L_msg_dim_tbl(L_msg_dim_tbl.last) := I_isup_cty_dim_mod;
  
    L_msg_cty_rec := "RIB_XItemSupCtyDesc_REC"(0, --rib oid
                                               I_origin_country_id, --ORIGIN_COUNTRY_ID-VARCHAR2(3)
                                               NULL, --PRIMARY_COUNTRY_IND-VARCHAR2(1)
                                               NULL, --UNIT_COST-NUMBER(20)
                                               NULL, --XITEMCOSTDESC_TBL-RIB_XItemCostDesc_TBL()
                                               NULL, --XISCLOCDESC_TBL-RIB_XISCLocDesc_TBL()
                                               NULL, --LEAD_TIME-NUMBER(4)
                                               NULL, --PICKUP_LEAD_TIME-NUMBER(4)
                                               NULL, --MIN_ORDER_QTY-NUMBER(12)
                                               NULL, --MAX_ORDER_QTY-NUMBER(12)
                                               NULL, --SUPP_HIER_LVL_1-VARCHAR2(10)
                                               NULL, --SUPP_HIER_LVL_2-VARCHAR2(10)
                                               NULL, --SUPP_HIER_LVL_3-VARCHAR2(10)
                                               NULL, --DEFAULT_UOP-VARCHAR2(6)
                                               NULL, --SUPP_PACK_SIZE-NUMBER(12)
                                               NULL, --INNER_PACK_SIZE-NUMBER(12)
                                               NULL, --TI-NUMBER(12)
                                               NULL, --HI-NUMBER(12)
                                               L_msg_dim_tbl, --XISCDIMDESC_TBL-RIB_XISCDimDesc_TBL()
                                               NULL, --COST_UOM-VARCHAR2(4)
                                               NULL, --TOLERANCE_TYPE-VARCHAR2(6)
                                               NULL, --MIN_TOLERANCE-NUMBER(12)
                                               NULL, --MAX_TOLERANCE-NUMBER(12)
                                               NULL, --SUPP_HIER_TYPE_1-VARCHAR2(6)
                                               NULL, --SUPP_HIER_TYPE_2-VARCHAR2(6)
                                               NULL, --SUPP_HIER_TYPE_3-VARCHAR2(6)
                                               NULL, --ROUND_LVL-VARCHAR2(6)
                                               NULL, --ROUND_TO_INNER_PCT-NUMBER(12)
                                               NULL, --ROUND_TO_CASE_PCT-NUMBER(12)
                                               NULL, --ROUND_TO_LAYER_PCT-NUMBER(12)
                                               NULL, --ROUND_TO_PALLET_PCT-NUMBER(12)
                                               NULL); --PACKING_METHOD-VARCHAR2(6)
  
    L_msg_cty_tbl := "RIB_XItemSupCtyDesc_TBL"();
    L_msg_cty_tbl.extend();
    L_msg_cty_tbl(L_msg_cty_tbl.last) := L_msg_cty_rec;
  
    L_msg_sup_rec := "RIB_XItemSupDesc_REC"(0, --rib oid
                                            I_supplier, --SUPPLIER-VARCHAR2(10)
                                            NULL, --PRIMARY_SUPP_IND-VARCHAR2(3)
                                            NULL, --VPN-VARCHAR2(30)
                                            NULL, --SUPP_LABEL-VARCHAR2(15)
                                            L_msg_cty_tbl, --XITEMSUPCTYDESC_TBL-RIB_XItemSupCtyDesc_TBL()
                                            NULL, --CONSIGNMENT_RATE-NUMBER(12)
                                            NULL, --SUPP_DISCONTINUE_DATE-DATE()
                                            NULL, --DIRECT_SHIP_IND-VARCHAR2(1)
                                            NULL, --PALLET_NAME-VARCHAR2(6)
                                            NULL, --CASE_NAME-VARCHAR2(6)
                                            NULL, --INNER_NAME-VARCHAR2(6)
                                            NULL, --XITMSUPCTYMFRDESC_TBL-RIB_XItmSupCtyMfrDesc_TBL()
                                            NULL, --PRIMARY_CASE_SIZE-VARCHAR2(6)
                                            NULL, --SUPP_DIFF_1-VARCHAR2(120)
                                            NULL, --SUPP_DIFF_2-VARCHAR2(120)
                                            NULL, --SUPP_DIFF_3-VARCHAR2(120)
                                            NULL, --SUPP_DIFF_4-VARCHAR2(120)
                                            NULL, --CONCESSION_RATE-NUMBER(12)
                                            NULL); --LANGOFXITEMSUPDESC_TBL-RIB_LangOfXItemSupDesc_TBL()
  
    L_msg_sup_tbl := "RIB_XItemSupDesc_TBL"();
    L_msg_sup_tbl.extend();
    L_msg_sup_tbl(L_msg_sup_tbl.last) := L_msg_sup_rec;
  
    L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                     I_item, --ITEM-VARCHAR2(25)
                                     NULL, --ITEM_PARENT-VARCHAR2(25)
                                     NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                     NULL, --PACK_IND-VARCHAR2(1)
                                     NULL, --ITEM_LEVEL-NUMBER(1)
                                     NULL, --TRAN_LEVEL-NUMBER(1)
                                     NULL, --DIFF_1-VARCHAR2(10)
                                     NULL, --DIFF_2-VARCHAR2(10)
                                     NULL, --DIFF_3-VARCHAR2(10)
                                     NULL, --DIFF_4-VARCHAR2(10)
                                     NULL, --DEPT-NUMBER(4)
                                     NULL, --CLASS-NUMBER(4)
                                     NULL, --SUBCLASS-NUMBER(4)
                                     NULL, --ITEM_DESC-VARCHAR2(250)
                                     NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                     NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                     NULL, --SHORT_DESC-VARCHAR2(120)
                                     NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                     NULL, --STANDARD_UOM-VARCHAR2(4)
                                     NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                     NULL, --FORECAST_IND-VARCHAR2(1)
                                     NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                     NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                     NULL, --SELLABLE_IND-VARCHAR2(1)
                                     NULL, --ORDERABLE_IND-VARCHAR2(1)
                                     NULL, --PACK_TYPE-VARCHAR2(1)
                                     NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                     NULL, --COMMENTS-VARCHAR2(2000)
                                     NULL, --CREATE_DATETIME-DATE()
                                     NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                     L_msg_sup_tbl, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                     NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                     NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                     NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                     NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                     NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                     NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                     NULL, --STATUS-VARCHAR2(1)
                                     NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                     NULL, --PACKAGE_SIZE-NUMBER(12)
                                     NULL, --HANDLING_TEMP-VARCHAR2(6)
                                     NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                     NULL, --MFG_REC_RETAIL-NUMBER(20)
                                     NULL, --WASTE_TYPE-VARCHAR2(6)
                                     NULL, --WASTE_PCT-NUMBER(12)
                                     NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                     NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                     NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                     NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                     NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                     NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                     NULL, --SIZE_GROUP1-NUMBER(4)
                                     NULL, --SIZE_GROUP2-NUMBER(4)
                                     NULL, --SIZE1-VARCHAR2(6)
                                     NULL, --SIZE2-VARCHAR2(6)
                                     NULL, --COLOR-VARCHAR2(24)
                                     NULL, --SYSTEM_IND-VARCHAR2(1)
                                     NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                     NULL, --UPC_TYPE-VARCHAR2(5)
                                     NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                     NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                     NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --PERISHABLE_IND-VARCHAR2(1)
                                     NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                     NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                     NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                     NULL, --ORDER_TYPE-VARCHAR2(6)
                                     NULL, --SALE_TYPE-VARCHAR2(6)
                                     NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                     NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                     NULL, --INVENTORY_IND-VARCHAR2(1)
                                     NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                     NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                     NULL, --PACKAGE_UOM-VARCHAR2(4)
                                     NULL, --FORMAT_ID-VARCHAR2(1)
                                     NULL, --PREFIX-NUMBER(2)
                                     NULL, --BRAND-VARCHAR2(120)
                                     NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                     NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                     NULL, --DESC_UP-VARCHAR2(250)
                                     NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                     NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                     NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                     NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                     NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                     NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                     NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                     NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                     NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                     NULL); --LANGOFXITEMDESC_TBL-RIB_LangOfXItemDesc_TBL()
  
    XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                O_error_message,
                                L_message,
                                L_message_type);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_ISUP_CTY_DIM_MOD;

  
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_IMAGE_TL_CRE
  -- Description: Responsible for create XItemDesc to new Item Image Translations
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_IMAGE_TL_CRE(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                         I_image_name        IN ITEM_IMAGE.IMAGE_NAME%TYPE,
                                         I_item_image_tl_add IN XXADEO_ITEM_IMAGE_TL_TBL,
                                         O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_error_message     IN OUT VARCHAR2) IS
  
    L_message_type VARCHAR2(30);
    L_message      "RIB_XItemDesc_REC";
    L_msg_iitl_rec "RIB_LangOfXItemImage_REC";
    L_msg_iitl_tbl "RIB_LangOfXItemImage_TBL";
    L_msg_ii_rec   "RIB_XItemImage_REC";
    L_msg_ii_tbl   "RIB_XItemImage_TBL";
    L_program      VARCHAR2(60) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_IMAGE_TL_CRE';
  BEGIN
    L_message_type := XXADEO_RMSSUB_XITEM.IITL_ADD;
  
    for i in 1 .. I_item_image_tl_add.count() loop
      L_msg_iitl_rec := "RIB_LangOfXItemImage_REC"(0,
                                                   I_item_image_tl_add(i).lang,
                                                   I_item_image_tl_add(i)
                                                   .image_desc);
    
      if L_msg_iitl_tbl is null then
        L_msg_iitl_tbl := "RIB_LangOfXItemImage_TBL"();
      end if;
      L_msg_iitl_tbl.extend();
      L_msg_iitl_tbl(L_msg_iitl_tbl.last) := L_msg_iitl_rec;
    end loop;
  
    if L_msg_iitl_tbl is not null and L_msg_iitl_tbl.count > 0 then
      L_msg_ii_rec := "RIB_XItemImage_REC"(0, --rib oid
                                           I_image_name, --IMAGE_NAME-VARCHAR2(120)
                                           NULL, --IMAGE_ADDR-VARCHAR2(255)
                                           NULL, --IMAGE_DESC-VARCHAR2(40)
                                           NULL, --CREATE_DATETIME-DATE()
                                           NULL, --LAST_UPDATE_DATETIME-DATE()
                                           NULL, --LAST_UPDATE_ID-VARCHAR2(30)
                                           null,
                                           NULL, --IMAGE_TYPE-VARCHAR2(6)
                                           NULL, --PRIMARY_IND-VARCHAR2(1)
                                           NULL, --DISPLAY_PRIORITY-NUMBER(4)
                                           L_msg_iitl_tbl); --LANGOFXITEMIMAGE_TBL-RIB_LangOfXItemImage_TBL()
    
      L_msg_ii_tbl := "RIB_XItemImage_TBL"();
      L_msg_ii_tbl.extend();
      L_msg_ii_tbl(L_msg_ii_tbl.last) := L_msg_ii_rec;
    
      L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                       I_item, --ITEM-VARCHAR2(25)
                                       NULL, --ITEM_PARENT-VARCHAR2(25)
                                       NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                       NULL, --PACK_IND-VARCHAR2(1)
                                       NULL, --ITEM_LEVEL-NUMBER(1)
                                       NULL, --TRAN_LEVEL-NUMBER(1)
                                       NULL, --DIFF_1-VARCHAR2(10)
                                       NULL, --DIFF_2-VARCHAR2(10)
                                       NULL, --DIFF_3-VARCHAR2(10)
                                       NULL, --DIFF_4-VARCHAR2(10)
                                       NULL, --DEPT-NUMBER(4)
                                       NULL, --CLASS-NUMBER(4)
                                       NULL, --SUBCLASS-NUMBER(4)
                                       NULL, --ITEM_DESC-VARCHAR2(250)
                                       NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --SHORT_DESC-VARCHAR2(120)
                                       NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                       NULL, --STANDARD_UOM-VARCHAR2(4)
                                       NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                       NULL, --FORECAST_IND-VARCHAR2(1)
                                       NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                       NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                       NULL, --SELLABLE_IND-VARCHAR2(1)
                                       NULL, --ORDERABLE_IND-VARCHAR2(1)
                                       NULL, --PACK_TYPE-VARCHAR2(1)
                                       NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                       NULL, --COMMENTS-VARCHAR2(2000)
                                       NULL, --CREATE_DATETIME-DATE()
                                       NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                       NULL, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                       NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                       NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                       NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                       NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                       NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                       L_msg_ii_tbl, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                       NULL, --STATUS-VARCHAR2(1)
                                       NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                       NULL, --PACKAGE_SIZE-NUMBER(12)
                                       NULL, --HANDLING_TEMP-VARCHAR2(6)
                                       NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                       NULL, --MFG_REC_RETAIL-NUMBER(20)
                                       NULL, --WASTE_TYPE-VARCHAR2(6)
                                       NULL, --WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                       NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                       NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                       NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                       NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                       NULL, --SIZE_GROUP1-NUMBER(4)
                                       NULL, --SIZE_GROUP2-NUMBER(4)
                                       NULL, --SIZE1-VARCHAR2(6)
                                       NULL, --SIZE2-VARCHAR2(6)
                                       NULL, --COLOR-VARCHAR2(24)
                                       NULL, --SYSTEM_IND-VARCHAR2(1)
                                       NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                       NULL, --UPC_TYPE-VARCHAR2(5)
                                       NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                       NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                       NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --PERISHABLE_IND-VARCHAR2(1)
                                       NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                       NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                       NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                       NULL, --ORDER_TYPE-VARCHAR2(6)
                                       NULL, --SALE_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                       NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                       NULL, --INVENTORY_IND-VARCHAR2(1)
                                       NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                       NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                       NULL, --PACKAGE_UOM-VARCHAR2(4)
                                       NULL, --FORMAT_ID-VARCHAR2(1)
                                       NULL, --PREFIX-NUMBER(2)
                                       NULL, --BRAND-VARCHAR2(120)
                                       NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                       NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                       NULL, --DESC_UP-VARCHAR2(250)
                                       NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                       NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                       NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                       NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                       NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                       NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                       NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                       NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                       NULL,
                                       NULL); --LANGOFXITEMDESC_TBL-RIB_LangOfXItemDesc_TBL()
    
      XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                  O_error_message,
                                  L_message,
                                  L_message_type);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_ITEM_IMAGE_TL_CRE;

  
   -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_IMAGE_TL_DEL 
  -- Description: Responsible for create XItemRef to delete item/image/translations relationship
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_IMAGE_TL_DEL(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                         I_image_name        IN ITEM_IMAGE.IMAGE_NAME%TYPE,
                                         I_item_image_tl_del IN XXADEO_ITEM_IMAGE_TL_TBL,
                                         O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_error_message     IN OUT VARCHAR2) IS
  
    L_message_type VARCHAR2(30);
    L_message      "RIB_XItemRef_REC";
    L_msg_iitl_rec "RIB_LangOfXItemImageRef_REC";
    L_msg_iitl_tbl "RIB_LangOfXItemImageRef_TBL";
    L_msg_ii_rec   "RIB_XItemImageRef_REC";
    L_msg_ii_tbl   "RIB_XItemImageRef_TBL";
    L_program      VARCHAR2(60) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_IMAGE_TL_DEL';
  BEGIN
    L_message_type := XXADEO_RMSSUB_XITEM.IITL_DEL;
  
    for i in 1 .. I_item_image_tl_del.count() loop
      L_msg_iitl_rec := "RIB_LangOfXItemImageRef_REC"(0,
                                                      I_item_image_tl_del(i).lang);
    
      if L_msg_iitl_tbl is null then
        L_msg_iitl_tbl := "RIB_LangOfXItemImageRef_TBL"();
      end if;
      L_msg_iitl_tbl.extend();
      L_msg_iitl_tbl(L_msg_iitl_tbl.last) := L_msg_iitl_rec;
    end loop;
    L_msg_ii_rec := "RIB_XItemImageRef_REC"(0, --rib oid
                                            I_image_name, --IMAGE_NAME-VARCHAR2(120)
                                            L_msg_iitl_tbl); --LANGOFXITEMIMAGEREF_TBL-RIB_LangOfXItemImageRef_TBL()
  
    L_msg_ii_tbl := "RIB_XItemImageRef_TBL"();
    L_msg_ii_tbl.extend();
    L_msg_ii_tbl(L_msg_ii_tbl.last) := L_msg_ii_rec;
  
    L_message := "RIB_XItemRef_REC"(0, --rib oid
                                    I_item, --ITEM-VARCHAR2(25)
                                    NULL, --HIER_LEVEL-VARCHAR2(2)
                                    NULL, --XITEMCTRYREF_TBL-RIB_XItemCtryRef_TBL()
                                    NULL, --XITEMSUPREF_TBL-RIB_XItemSupRef_TBL()
                                    NULL, --XITEMVATREF_TBL-RIB_XItemVATRef_TBL()
                                    L_msg_ii_tbl, --XITEMIMAGEREF_TBL-RIB_XItemImageRef_TBL()
                                    NULL, --XITEMSEASONREF_TBL-RIB_XItemSeasonRef_TBL()
                                    NULL, --XITEMUDAREF_TBL-RIB_XItemUDARef_TBL()
                                    NULL, --XITEMBOMREF_TBL-RIB_XItemBOMRef_TBL()
                                    NULL, --SYSTEM_IND-VARCHAR2(1)
                                    NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                    NULL); --LANGOFXITEMREF_TBL-RIB_LangOfXItemRef_TBL()
  
    XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                O_error_message,
                                L_message,
                                L_message_type);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END CREATE_MSG_ITEM_IMAGE_TL_DEL;

  
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_ITEM_IMAGE_TL_MOD
  -- Description: Responsible for create XItemDesc to update item/image/translations relationship
  --              with information managed by STEP
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_ITEM_IMAGE_TL_MOD(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                         I_image_name        IN ITEM_IMAGE.IMAGE_NAME%TYPE,
                                         I_item_image_tl_mod IN XXADEO_ITEM_IMAGE_TL_TBL,
                                         O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_error_message     IN OUT VARCHAR2) IS
  
    L_message_type VARCHAR2(30);
    L_message      "RIB_XItemDesc_REC";
    L_msg_iitl_rec "RIB_LangOfXItemImage_REC";
    L_msg_iitl_tbl "RIB_LangOfXItemImage_TBL";
    L_msg_ii_rec   "RIB_XItemImage_REC";
    L_msg_ii_tbl   "RIB_XItemImage_TBL";
    L_row          ITEM_IMAGE_TL%ROWTYPE;
  
    L_program VARCHAR2(60) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_IMAGE_TL_MOD';
  BEGIN
    L_message_type := XXADEO_RMSSUB_XITEM.IITL_UPD;
  
    for i in 1 .. I_item_image_tl_mod.count() loop
      select *
        into L_row
        from item_image_tl
       where image_name = I_image_name
         and lang = I_item_image_tl_mod(i).lang
         and item = I_item;
    
      if L_row.image_desc != I_item_image_tl_mod(i).image_desc then
      
        L_msg_iitl_rec := "RIB_LangOfXItemImage_REC"(0,
                                                     I_item_image_tl_mod(i).lang,
                                                     I_item_image_tl_mod(i)
                                                     .image_desc);
      
        if L_msg_iitl_tbl is null then
          L_msg_iitl_tbl := "RIB_LangOfXItemImage_TBL"();
        end if;
        L_msg_iitl_tbl.extend();
        L_msg_iitl_tbl(L_msg_iitl_tbl.last) := L_msg_iitl_rec;
      
      end if;
    end loop;
  
    if L_msg_iitl_tbl is not null and L_msg_iitl_tbl.count > 0 then
      L_msg_ii_rec := "RIB_XItemImage_REC"(0, --rib oid
                                           I_image_name, --IMAGE_NAME-VARCHAR2(120)
                                           NULL, --IMAGE_ADDR-VARCHAR2(255)
                                           NULL, --IMAGE_DESC-VARCHAR2(40)
                                           NULL, --CREATE_DATETIME-DATE()
                                           NULL, --LAST_UPDATE_DATETIME-DATE()
                                           NULL, --LAST_UPDATE_ID-VARCHAR2(30)
                                           null,
                                           NULL, --IMAGE_TYPE-VARCHAR2(6)
                                           NULL, --PRIMARY_IND-VARCHAR2(1)
                                           NULL, --DISPLAY_PRIORITY-NUMBER(4)
                                           L_msg_iitl_tbl); --LANGOFXITEMIMAGE_TBL-RIB_LangOfXItemImage_TBL()
    
      L_msg_ii_tbl := "RIB_XItemImage_TBL"();
      L_msg_ii_tbl.extend();
      L_msg_ii_tbl(L_msg_ii_tbl.last) := L_msg_ii_rec;
    
      L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                       I_item, --ITEM-VARCHAR2(25)
                                       NULL, --ITEM_PARENT-VARCHAR2(25)
                                       NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                       NULL, --PACK_IND-VARCHAR2(1)
                                       NULL, --ITEM_LEVEL-NUMBER(1)
                                       NULL, --TRAN_LEVEL-NUMBER(1)
                                       NULL, --DIFF_1-VARCHAR2(10)
                                       NULL, --DIFF_2-VARCHAR2(10)
                                       NULL, --DIFF_3-VARCHAR2(10)
                                       NULL, --DIFF_4-VARCHAR2(10)
                                       NULL, --DEPT-NUMBER(4)
                                       NULL, --CLASS-NUMBER(4)
                                       NULL, --SUBCLASS-NUMBER(4)
                                       NULL, --ITEM_DESC-VARCHAR2(250)
                                       NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --SHORT_DESC-VARCHAR2(120)
                                       NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                       NULL, --STANDARD_UOM-VARCHAR2(4)
                                       NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                       NULL, --FORECAST_IND-VARCHAR2(1)
                                       NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                       NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                       NULL, --SELLABLE_IND-VARCHAR2(1)
                                       NULL, --ORDERABLE_IND-VARCHAR2(1)
                                       NULL, --PACK_TYPE-VARCHAR2(1)
                                       NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                       NULL, --COMMENTS-VARCHAR2(2000)
                                       NULL, --CREATE_DATETIME-DATE()
                                       NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                       NULL, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                       NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                       NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                       NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                       NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                       NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                       L_msg_ii_tbl, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                       NULL, --STATUS-VARCHAR2(1)
                                       NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                       NULL, --PACKAGE_SIZE-NUMBER(12)
                                       NULL, --HANDLING_TEMP-VARCHAR2(6)
                                       NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                       NULL, --MFG_REC_RETAIL-NUMBER(20)
                                       NULL, --WASTE_TYPE-VARCHAR2(6)
                                       NULL, --WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                       NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                       NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                       NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                       NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                       NULL, --SIZE_GROUP1-NUMBER(4)
                                       NULL, --SIZE_GROUP2-NUMBER(4)
                                       NULL, --SIZE1-VARCHAR2(6)
                                       NULL, --SIZE2-VARCHAR2(6)
                                       NULL, --COLOR-VARCHAR2(24)
                                       NULL, --SYSTEM_IND-VARCHAR2(1)
                                       NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                       NULL, --UPC_TYPE-VARCHAR2(5)
                                       NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                       NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                       NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --PERISHABLE_IND-VARCHAR2(1)
                                       NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                       NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                       NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                       NULL, --ORDER_TYPE-VARCHAR2(6)
                                       NULL, --SALE_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                       NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                       NULL, --INVENTORY_IND-VARCHAR2(1)
                                       NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                       NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                       NULL, --PACKAGE_UOM-VARCHAR2(4)
                                       NULL, --FORMAT_ID-VARCHAR2(1)
                                       NULL, --PREFIX-NUMBER(2)
                                       NULL, --BRAND-VARCHAR2(120)
                                       NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                       NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                       NULL, --DESC_UP-VARCHAR2(250)
                                       NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                       NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                       NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                       NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                       NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                       NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                       NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                       NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                       NULL,
                                       NULL); --LANGOFXITEMDESC_TBL-RIB_LangOfXItemDesc_TBL()
    
      XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                  O_error_message,
                                  L_message,
                                  L_message_type);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END CREATE_MSG_ITEM_IMAGE_TL_MOD;

 
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_IM_TL_CRE
  -- Description:  Responsible for create XItemDesc to create item/translations relationship
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_IM_TL_CRE(I_item               IN ITEM_MASTER.ITEM%TYPE,
                                 I_item_master_tl_add IN XXADEO_ITEM_MASTER_TL_TBL,
                                 O_status_code        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error_message      IN OUT VARCHAR2) IS
  
    L_message_type VARCHAR2(30);
    L_message      "RIB_XItemDesc_REC";
    L_msg_imtl_rec "RIB_LangOfXItemDesc_REC";
    L_msg_imtl_tbl "RIB_LangOfXItemDesc_TBL";
    L_program      VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_IM_TL_CRE';
  
  BEGIN
    L_message_type := XXADEO_RMSSUB_XITEM.IMTL_ADD;
  
    for i in 1 .. I_item_master_tl_add.count loop
      L_msg_imtl_rec := "RIB_LangOfXItemDesc_REC"(0, --rib oid
                                                  I_item_master_tl_add(i).lang, --LANG-NUMBER(6)
                                                  I_item_master_tl_add(i)
                                                  .short_desc, --SHORT_DESC-VARCHAR2(120)
                                                  I_item_master_tl_add(i)
                                                  .item_desc, --ITEM_DESC-VARCHAR2(250)
                                                  I_item_master_tl_add(i)
                                                  .item_desc_secondary); --ITEM_DESC_SECONDARY-VARCHAR2(250)
    
      if L_msg_imtl_tbl is null then
        L_msg_imtl_tbl := "RIB_LangOfXItemDesc_TBL"();
      end if;
      L_msg_imtl_tbl.extend();
      L_msg_imtl_tbl(L_msg_imtl_tbl.last) := L_msg_imtl_rec;
    end loop;
  
    L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                     I_item, --ITEM-VARCHAR2(25)
                                     NULL, --ITEM_PARENT-VARCHAR2(25)
                                     NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                     NULL, --PACK_IND-VARCHAR2(1)
                                     NULL, --ITEM_LEVEL-NUMBER(1)
                                     NULL, --TRAN_LEVEL-NUMBER(1)
                                     NULL, --DIFF_1-VARCHAR2(10)
                                     NULL, --DIFF_2-VARCHAR2(10)
                                     NULL, --DIFF_3-VARCHAR2(10)
                                     NULL, --DIFF_4-VARCHAR2(10)
                                     NULL, --DEPT-NUMBER(4)
                                     NULL, --CLASS-NUMBER(4)
                                     NULL, --SUBCLASS-NUMBER(4)
                                     NULL, --ITEM_DESC-VARCHAR2(250)
                                     NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                     NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                     NULL, --SHORT_DESC-VARCHAR2(120)
                                     NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                     NULL, --STANDARD_UOM-VARCHAR2(4)
                                     NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                     NULL, --FORECAST_IND-VARCHAR2(1)
                                     NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                     NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                     NULL, --SELLABLE_IND-VARCHAR2(1)
                                     NULL, --ORDERABLE_IND-VARCHAR2(1)
                                     NULL, --PACK_TYPE-VARCHAR2(1)
                                     NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                     NULL, --COMMENTS-VARCHAR2(2000)
                                     NULL, --CREATE_DATETIME-DATE()
                                     NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                     NULL, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                     NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                     NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                     NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                     NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                     NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                     NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                     NULL, --STATUS-VARCHAR2(1)
                                     NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                     NULL, --PACKAGE_SIZE-NUMBER(12)
                                     NULL, --HANDLING_TEMP-VARCHAR2(6)
                                     NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                     NULL, --MFG_REC_RETAIL-NUMBER(20)
                                     NULL, --WASTE_TYPE-VARCHAR2(6)
                                     NULL, --WASTE_PCT-NUMBER(12)
                                     NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                     NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                     NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                     NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                     NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                     NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                     NULL, --SIZE_GROUP1-NUMBER(4)
                                     NULL, --SIZE_GROUP2-NUMBER(4)
                                     NULL, --SIZE1-VARCHAR2(6)
                                     NULL, --SIZE2-VARCHAR2(6)
                                     NULL, --COLOR-VARCHAR2(24)
                                     NULL, --SYSTEM_IND-VARCHAR2(1)
                                     NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                     NULL, --UPC_TYPE-VARCHAR2(5)
                                     NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                     NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                     NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                     NULL, --PERISHABLE_IND-VARCHAR2(1)
                                     NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                     NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                     NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                     NULL, --ORDER_TYPE-VARCHAR2(6)
                                     NULL, --SALE_TYPE-VARCHAR2(6)
                                     NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                     NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                     NULL, --INVENTORY_IND-VARCHAR2(1)
                                     NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                     NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                     NULL, --PACKAGE_UOM-VARCHAR2(4)
                                     NULL, --FORMAT_ID-VARCHAR2(1)
                                     NULL, --PREFIX-NUMBER(2)
                                     NULL, --BRAND-VARCHAR2(120)
                                     NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                     NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                     NULL, --DESC_UP-VARCHAR2(250)
                                     NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                     NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                     NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                     NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                     NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                     NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                     NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                     NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                     NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                     null,
                                     L_msg_imtl_tbl); --LANGOFXITEMDESC_TBL-RIB_LangOfXItemDesc_TBL()
  
    XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                O_error_message,
                                L_message,
                                L_message_type);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_IM_TL_CRE;

  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_IM_TL_DEL
  -- Description: Responsible to create XItemRef for delete Item/Translations relationship
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_IM_TL_DEL(I_item               IN ITEM_MASTER.ITEM%TYPE,
                                 I_item_master_tl_del IN XXADEO_ITEM_MASTER_TL_TBL,
                                 O_status_code        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error_message      IN OUT VARCHAR2) IS
  
    L_message_type VARCHAR2(30);
    L_message      "RIB_XItemRef_REC";
    L_msg_imtl_rec "RIB_LangOfXItemRef_REC";
    L_msg_imtl_tbl "RIB_LangOfXItemRef_TBL";
    L_program      VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_IM_TL_DEL';
  BEGIN
  
    L_message_type := XXADEO_RMSSUB_XITEM.IMTL_DEL;
    for i in 1 .. I_item_master_tl_del.count loop
      L_msg_imtl_rec := "RIB_LangOfXItemRef_REC"(0,
                                                 I_item_master_tl_del(i).lang);
    
      if L_msg_imtl_tbl is null then
        L_msg_imtl_tbl := "RIB_LangOfXItemRef_TBL"();
      end if;
      L_msg_imtl_tbl.extend();
      L_msg_imtl_tbl(L_msg_imtl_tbl.last) := L_msg_imtl_rec;
    
    end loop;
  
    L_message := "RIB_XItemRef_REC"(0, --rib oid
                                    I_item, --ITEM-VARCHAR2(25)
                                    NULL, --HIER_LEVEL-VARCHAR2(2)
                                    NULL, --XITEMCTRYREF_TBL-RIB_XItemCtryRef_TBL()
                                    NULL, --XITEMSUPREF_TBL-RIB_XItemSupRef_TBL()
                                    NULL, --XITEMVATREF_TBL-RIB_XItemVATRef_TBL()
                                    NULL, --XITEMIMAGEREF_TBL-RIB_XItemImageRef_TBL()
                                    NULL, --XITEMSEASONREF_TBL-RIB_XItemSeasonRef_TBL()
                                    NULL, --XITEMUDAREF_TBL-RIB_XItemUDARef_TBL()
                                    NULL, --XITEMBOMREF_TBL-RIB_XItemBOMRef_TBL()
                                    NULL, --SYSTEM_IND-VARCHAR2(1)
                                    NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                    L_msg_imtl_tbl); --LANGOFXITEMREF_TBL-RIB_LangOfXItemRef_TBL()
  
    XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                O_error_message,
                                L_message,
                                L_message_type);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_IM_TL_DEL;

  
  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_MSG_IM_TL_MOD
  -- Description:Responsible to create XItemDesc for update Item/Translations relationship
  --             with information managed by STEP
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_MSG_IM_TL_MOD(I_item               IN ITEM_MASTER.ITEM%TYPE,
                                 I_item_master_tl_mod IN XXADEO_ITEM_MASTER_TL_TBL,
                                 O_status_code        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error_message      IN OUT VARCHAR2) is
    L_message_type        VARCHAR2(30);
    L_message             "RIB_XItemDesc_REC";
    L_msg_imtl_rec        "RIB_LangOfXItemDesc_REC";
    L_msg_imtl_tbl        "RIB_LangOfXItemDesc_TBL";
    L_program             VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.CREATE_MSG_IM_TL_MOD';
    L_short_desc          ITEM_MASTER.SHORT_DESC%TYPE;
    L_item_desc           ITEM_MASTER.ITEM_DESC%TYPE;
    L_item_desc_secondary ITEM_MASTER.ITEM_DESC_SECONDARY%TYPE;
  BEGIN
    L_message_type := XXADEO_RMSSUB_XITEM.IMTL_UPD;
  
    for i in 1 .. I_item_master_tl_mod.count loop
      select short_desc, item_desc, item_desc_secondary
        into L_short_desc, L_item_desc, L_item_desc_secondary
        from item_master_tl
       where item = I_item
         and lang = I_item_master_tl_mod(i).lang;
    
      if L_short_desc != I_item_master_tl_mod(i).short_desc or
         L_item_desc != I_item_master_tl_mod(i).item_desc or
         L_item_desc_secondary != I_item_master_tl_mod(i)
        .item_desc_secondary then
      
        L_msg_imtl_rec := "RIB_LangOfXItemDesc_REC"(0, --rib oid
                                                    I_item_master_tl_mod(i).lang, --LANG-NUMBER(6)
                                                    I_item_master_tl_mod(i)
                                                    .short_desc, --SHORT_DESC-VARCHAR2(120)
                                                    I_item_master_tl_mod(i)
                                                    .item_desc, --ITEM_DESC-VARCHAR2(250)
                                                    I_item_master_tl_mod(i)
                                                    .item_desc_secondary); --ITEM_DESC_SECONDARY-VARCHAR2(250)
      
        if L_msg_imtl_tbl is null then
          L_msg_imtl_tbl := "RIB_LangOfXItemDesc_TBL"();
        end if;
      
        L_msg_imtl_tbl.extend();
        L_msg_imtl_tbl(L_msg_imtl_tbl.last) := L_msg_imtl_rec;
      end if;
    
    end loop;
  
    if L_msg_imtl_tbl is not null and L_msg_imtl_tbl.count > 0 then
      L_message := "RIB_XItemDesc_REC"(0, --rib oid
                                       I_item, --ITEM-VARCHAR2(25)
                                       NULL, --ITEM_PARENT-VARCHAR2(25)
                                       NULL, --ITEM_GRANDPARENT-VARCHAR2(25)
                                       NULL, --PACK_IND-VARCHAR2(1)
                                       NULL, --ITEM_LEVEL-NUMBER(1)
                                       NULL, --TRAN_LEVEL-NUMBER(1)
                                       NULL, --DIFF_1-VARCHAR2(10)
                                       NULL, --DIFF_2-VARCHAR2(10)
                                       NULL, --DIFF_3-VARCHAR2(10)
                                       NULL, --DIFF_4-VARCHAR2(10)
                                       NULL, --DEPT-NUMBER(4)
                                       NULL, --CLASS-NUMBER(4)
                                       NULL, --SUBCLASS-NUMBER(4)
                                       NULL, --ITEM_DESC-VARCHAR2(250)
                                       NULL, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --IZP_HIER_LEVEL-VARCHAR2(2)
                                       NULL, --SHORT_DESC-VARCHAR2(120)
                                       NULL, --COST_ZONE_GROUP_ID-NUMBER(4)
                                       NULL, --STANDARD_UOM-VARCHAR2(4)
                                       NULL, --STORE_ORD_MULT-VARCHAR2(1)
                                       NULL, --FORECAST_IND-VARCHAR2(1)
                                       NULL, --SIMPLE_PACK_IND-VARCHAR2(1)
                                       NULL, --CONTAINS_INNER_IND-VARCHAR2(1)
                                       NULL, --SELLABLE_IND-VARCHAR2(1)
                                       NULL, --ORDERABLE_IND-VARCHAR2(1)
                                       NULL, --PACK_TYPE-VARCHAR2(1)
                                       NULL, --ORDER_AS_TYPE-VARCHAR2(1)
                                       NULL, --COMMENTS-VARCHAR2(2000)
                                       NULL, --CREATE_DATETIME-DATE()
                                       NULL, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                       NULL, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()
                                       NULL, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                       NULL, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                       NULL, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                       NULL, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                       NULL, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                       NULL, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                       NULL, --STATUS-VARCHAR2(1)
                                       NULL, --UOM_CONV_FACTOR-NUMBER(20)
                                       NULL, --PACKAGE_SIZE-NUMBER(12)
                                       NULL, --HANDLING_TEMP-VARCHAR2(6)
                                       NULL, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                       NULL, --MFG_REC_RETAIL-NUMBER(20)
                                       NULL, --WASTE_TYPE-VARCHAR2(6)
                                       NULL, --WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                       NULL, --CONST_DIMEN_IND-VARCHAR2(1)
                                       NULL, --GIFT_WRAP_IND-VARCHAR2(1)
                                       NULL, --SHIP_ALONE_IND-VARCHAR2(1)
                                       NULL, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                       NULL, --SIZE_GROUP1-NUMBER(4)
                                       NULL, --SIZE_GROUP2-NUMBER(4)
                                       NULL, --SIZE1-VARCHAR2(6)
                                       NULL, --SIZE2-VARCHAR2(6)
                                       NULL, --COLOR-VARCHAR2(24)
                                       NULL, --SYSTEM_IND-VARCHAR2(1)
                                       NULL, --UPC_SUPPLEMENT-NUMBER(5)
                                       NULL, --UPC_TYPE-VARCHAR2(5)
                                       NULL, --PRIMARY_UPC_IND-VARCHAR2(1)
                                       NULL, --PRIMARY_REPL_IND-VARCHAR2(1)
                                       NULL, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                       NULL, --PERISHABLE_IND-VARCHAR2(1)
                                       NULL, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                       NULL, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                       NULL, --AIP_CASE_TYPE-VARCHAR2(6)
                                       NULL, --ORDER_TYPE-VARCHAR2(6)
                                       NULL, --SALE_TYPE-VARCHAR2(6)
                                       NULL, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                       NULL, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                       NULL, --INVENTORY_IND-VARCHAR2(1)
                                       NULL, --ITEM_XFORM_IND-VARCHAR2(1)
                                       NULL, --CONTAINER_ITEM-VARCHAR2(25)
                                       NULL, --PACKAGE_UOM-VARCHAR2(4)
                                       NULL, --FORMAT_ID-VARCHAR2(1)
                                       NULL, --PREFIX-NUMBER(2)
                                       NULL, --BRAND-VARCHAR2(120)
                                       NULL, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                       NULL, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                       NULL, --DESC_UP-VARCHAR2(250)
                                       NULL, --MERCHANDISE_IND-VARCHAR2(1)
                                       NULL, --ORIGINAL_RETAIL-NUMBER(20)
                                       NULL, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                       NULL, --RETAIL_LABEL_VALUE-NUMBER(20)
                                       NULL, --DEFAULT_WASTE_PCT-NUMBER(12)
                                       NULL, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                       NULL, --CHECK_UDA_IND-VARCHAR2(1)
                                       NULL, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                       NULL, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                       NULL,
                                       L_msg_imtl_tbl); --LANGOFXITEMDESC_TBL-RIB_LangOfXItemDesc_TBL()
      XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                  O_error_message,
                                  L_message,
                                  L_message_type);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_MSG_IM_TL_MOD;
  
  
  -------------------------------------------------------------------------------------------------------
  -- Name: VALIDATE_CFAS_TO_ITEM_SUP
  -- Description: Responsible for verify if CFA 472 will be changed
  --		PREVIOUS				NEW STATUS
  --		462 	472  	 		462   	472  
  --		N		Null			Y		Qualité
  --		N		Qualité			Y		Qualité
  --		N		Non commandable	Y		Qualité
  --		N		Null			N		Null
  --		N		Qualité			N		Qualité
  --		N		Non commandable	N		Non commandable
  --		Y		Null			Y		Null
  --		Y		Qualité			Y		Qualité
  --		Y		Non commandable	Y		Non commandable
  --		Y		Null			N		Null
  --		Y		Qualité			N		Null
  --		Y		Non commandable	N		Non commandable            
  -------------------------------------------------------------------------------------------------------
  PROCEDURE VALIDATE_CFAS_TO_ITEM_SUP(I_primary_key       IN VARCHAR2,
                                      I_value_cfa_quality IN VARCHAR2,
                                      O_cfa               OUT XXADEO_CFA_DETAILS) is
  
    L_cfa_blk_po_id   CFA_ATTRIB.ATTRIB_ID%TYPE;
    L_cfa_blk_qlty_id CFA_ATTRIB.ATTRIB_ID%TYPE;
    L_cfa_po          VARCHAR2(6);
    L_cfa_qlty        VARCHAR2(6);
  
  BEGIN
    select value_1
      into L_cfa_blk_po_id
      from xxadeo_mom_dvm
     where func_area = 'CFA'
       and parameter = 'BLOCKING_PO_ITEM_SUPP'; -- cfa 472
  
    select value_1
      into L_cfa_blk_qlty_id
      from xxadeo_mom_dvm
     where func_area = 'CFA'
       and parameter = 'BLK_QUALITY_ITEM_SUPP'; -- cfa 462
  
    L_cfa_po := XXADEO_CFAS_UTILS.GET_CFA_VALUE_VARCHAR('ITEM_SUPPLIER',
                                      I_primary_key,
                                      L_cfa_blk_po_id);
  
    L_cfa_qlty := XXADEO_CFAS_UTILS.GET_CFA_VALUE_VARCHAR('ITEM_SUPPLIER',
                                        I_primary_key,
                                        L_cfa_blk_qlty_id);
    if L_cfa_qlty is null then
    
      null;
    
    elsif I_value_cfa_quality = L_cfa_qlty then
    
      null;
    
    elsif I_value_cfa_quality = 'Y' then
      if L_cfa_po is null or L_cfa_po != '2' then
        O_cfa := XXADEO_CFA_DETAILS(L_cfa_blk_po_id,
                                    'BLOCKING_PO_ITEM_SUPP',
                                    NULL,
                                    NULL,
                                    null,
                                    '2',
                                    null);
      
      end if;
    elsif I_value_cfa_quality = 'N' then
      if L_cfa_po = '2' then
        O_cfa := XXADEO_CFA_DETAILS(L_cfa_blk_po_id,
                                    'BLOCKING_PO_ITEM_SUPP',
                                    NULL,
                                    NULL,
                                    null,
                                    null,
                                    null);
      end if;
    end if;
  
  END VALIDATE_CFAS_TO_ITEM_SUP;

  -------------------------------------------------------------------------------------------------------
  -- Name: HANDLE_ERRORS
  -- Description: This procedure is responsible to register any errors if occurred
  -------------------------------------------------------------------------------------------------------
  PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                          IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cause          IN VARCHAR2,
                          I_program        IN VARCHAR2) IS
  
    L_program VARCHAR2(50) := 'XXADEO_XITEM_UTILS_PCK.HANDLE_ERRORS';
  
  BEGIN
  
    API_LIBRARY.HANDLE_ERRORS(O_status_code,
                              IO_error_message,
                              I_cause,
                              I_program);
  EXCEPTION
    when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
    
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
    
  END HANDLE_ERRORS;

END XXADEO_XITEM_UTILS_PCK;
/