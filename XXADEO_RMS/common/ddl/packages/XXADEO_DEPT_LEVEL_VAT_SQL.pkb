CREATE OR REPLACE PACKAGE BODY XXADEO_DEPT_LEVEL_VAT_SQL AS
----------------------------------------------------------------------------------------------

  TYPE errors_tab_typ     IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
  TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
  LP_errors_tab     errors_tab_typ;
  LP_s9t_errors_tab s9t_errors_tab_typ;
   
  TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
  LP_bulk_fetch_limit                CONSTANT NUMBER(12) := 1000;

  LP_primary_lang                    LANG.LANG%TYPE;
  LP_user                            SVCPROV_CONTEXT.USER_NAME%TYPE   := GET_USER;
  LP_chunk_id                        SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE  := 1;
   
  L_process_id                       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE;
  L_file_id                          SVC_PROCESS_TRACKER.FILE_ID%TYPE;
  L_date_format                      NLS_DATABASE_PARAMETERS.VALUE%TYPE;
  L_xxadeo_wksht_column_idx_tbl      XXADEO_WKSHT_COLUMN_IDX_TBL;
  
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(O_error_message IN OUT LOGGER_LOGS.TEXT%TYPE,
                          I_issues_rt     IN     S9T_ERRORS%ROWTYPE) IS
BEGIN
  --
  LP_s9t_errors_tab.EXTEND();
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_issues_rt.file_id;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := TEMPLATE_KEY;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_issues_rt.wksht_key;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_issues_rt.column_key;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_issues_rt.row_seq;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := I_issues_rt.error_key;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := LP_user;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := LP_user;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
  --
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_txt_1         IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE DEFAULT NULL,
                      I_txt_2         IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE DEFAULT NULL,
                      I_txt_3         IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE DEFAULT NULL,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
  --
  Lp_errors_tab.extend();
  Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
  Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
  Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
  Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
  Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
  Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
  Lp_errors_tab(Lp_errors_tab.count()).error_msg   := SQL_LIB.GET_MESSAGE_TEXT(I_error_msg,
                                                                               I_txt_1,
                                                                               I_txt_2,
                                                                               I_txt_3);
  Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;
  --
END WRITE_ERROR;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_DEPT_LEVEL_VAT(I_file_id   IN   NUMBER ) IS
BEGIN
  --
  insert into TABLE (select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id = I_file_id
                        and ss.sheet_name = DEPT_LEVEL_VAT_sheet )
                select s9t_row(s9t_cells((case
                                           when vd.vat_type is null or vd.vat_code is null then
                                             XXADEO_DEPT_LEVEL_VAT_SQL.action_new
                                           else
                                             XXADEO_DEPT_LEVEL_VAT_SQL.action_mod
                                           end),
                                          vd.vat_region,
                                          g.group_no,
                                          g.group_name,
                                          d.dept,
                                          d.dept_name,
                                          vd.vat_type,
                                          vd.vat_code),rownum+1)  
                from v_deps    d,
                     vat_deps vd, 
                     v_groups  g
               where d.dept     = vd.dept(+)
                 and d.group_no = g.group_no;
  --
END POPULATE_DEPT_LEVEL_VAT;
----------------------------------------------------------------------------------------------
FUNCTION SETUP_DEPT_LEVEL_VAT(O_error_message IN OUT  LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_program              VARCHAR2(64) := 'XXADEO_DEPT_LEVEL_VAT_SQL.SETUP_DEPT_LEVEL_VAT';
  --
  cursor C_get_create_values is 
    select vat_region,
           dept,
           vat_type,
           vat_code,
           'N' reverse_vat_ind,
           create_id create_id,
           create_datetime create_datetime,
           row_seq
      from xxadeo_dept_level_vat_stg
     where process_id = L_process_id
       and action     = GP_upload_create;
  --
  cursor C_get_update_values is 
    select vat_region,
           dept,
           vat_type,
           vat_code,
           'N' reverse_vat_ind,
           create_id create_id,
           create_datetime create_datetime,
           row_seq
      from xxadeo_dept_level_vat_stg
     where process_id = L_process_id
       and action     = GP_upload_update;
  --
BEGIN
  --
  SAVEPOINT INIT_CREATE_DEPT;
  --
  -- create VAT by Department
  --
  BEGIN
    --
    for rec in C_get_create_values loop
      --
      begin
        --
        insert into vat_deps
          (vat_region,
           dept,
           vat_type,
           vat_code,
           reverse_vat_ind,
           create_id,
           create_datetime)
         values
           (rec.vat_region,
            rec.dept,
            rec.vat_type,
            rec.vat_code,
            rec.reverse_vat_ind,
            rec.create_id,
            rec.create_datetime);
        --
      exception
        --
        when DUP_VAL_ON_INDEX then
          --
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      DEPT_LEVEL_VAT_SHEET,
                      rec.row_seq,
                      'INSERTING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$VAT_DEPT_EXISTS);  
          --
        when others then
          --
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      DEPT_LEVEL_VAT_SHEET,
                      rec.row_seq,
                      'INSERTING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$COULD_NOT_INSERT_REC);  
            --
      end;
      --
    end loop;
    --
    for rec in C_get_update_values loop
      --
      begin
        --
        update vat_deps
           set vat_code = rec.vat_code
         where vat_region = rec.vat_region 
           and dept       = rec.dept       
           and vat_type   = rec.vat_type;
        --
        --
        if SQL%ROWCOUNT = 0 then
          --
           WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      DEPT_LEVEL_VAT_SHEET,
                      rec.row_seq,
                      'UPDATING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$REC_NOT_EXISTS);  
        --
        end if;
        --
      exception
        --
        when others then
          --
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      DEPT_LEVEL_VAT_SHEET,
                      rec.row_seq,
                      'UPDATING',
                      XXADEO_GLOBAL_VARS_SQL.VAL$COULD_NOT_UPDATE_REC); 
      end;
      --
    end loop;
    --
  --
  END;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
  --
END SETUP_DEPT_LEVEL_VAT;
----------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
  --
  L_program   VARCHAR2(64) := 'XXADEO_DEPT_LEVEL_VAT_SQL.CREATE_S9T';
  --
  L_file      s9t_file;
  L_s9t_sheet_tab  S9T_SHEET_TAB;
  --
BEGIN
  --
  L_s9t_sheet_tab := S9T_SHEET_TAB();
  --
  -- get template sheets and columns
  --
  if XXADEO_CUSTOM_UPLOAD_SQL.GET_TEMPLATE_S9T_SHEETS(O_error_message   => O_error_message,
                                                      IO_s9t_sheet_tab  => L_s9t_sheet_tab,
                                                      I_template_key    => TEMPLATE_KEY) = FALSE then
    --
    RETURN FALSE;
    --
  end if;
  --
   XXADEO_CUSTOM_UPLOAD_SQL.INIT_S9T(O_error_message  => O_error_message,
                                     O_file_id        => O_file_id,
                                     I_template_key   => TEMPLATE_KEY,
                                     I_s9t_sheet_tab  => L_s9t_sheet_tab);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE   then
      return FALSE;
   end if;
   --
   if I_template_only_ind = 'N' then
      POPULATE_DEPT_LEVEL_VAT(O_file_id);
      COMMIT;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;

EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
   --
   return FALSE;
   --
END CREATE_S9T;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DEPT_LEVEL_VAT (O_error_message             IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  IO_error                    IN OUT BOOLEAN,
                                  I_xxadeo_dept_level_vat_stg IN     XXADEO_DEPT_LEVEL_VAT_STG%ROWTYPE,
                                  I_row_seq                   IN     SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE)
RETURN BOOLEAN IS
  --
  L_error     BOOLEAN := IO_error;
  --
BEGIN
  --
  if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER(O_error_message,
                                                        I_xxadeo_dept_level_vat_stg.dept) then
    --
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                DEPT_LEVEL_VAT_SHEET,
                I_row_seq,
                DEPT_LEVEL_VAT$DEPT,
                nvl(O_error_message,XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEPT)); 
    --
    L_error := TRUE;
    --
  end if;
  --
  if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_VAT_REGION(O_error_message,
                                                        I_xxadeo_dept_level_vat_stg.vat_region) then
    --
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                DEPT_LEVEL_VAT_SHEET,
                I_row_seq,
                DEPT_LEVEL_VAT$VAT_REGION,
                nvl(O_error_message,XXADEO_GLOBAL_VARS_SQL.VAL$INV_VAT_REGION)); 
    --    
    L_error := TRUE;
    --
  end if;
  --
  if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_VAT_CODE(O_error_message,
                                                      I_xxadeo_dept_level_vat_stg.vat_code) then
    --
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                DEPT_LEVEL_VAT_SHEET,
                I_row_seq,
                DEPT_LEVEL_VAT$VAT_CODE,
                nvl(O_error_message,XXADEO_GLOBAL_VARS_SQL.VAL$VAT_NO_ACTIVE_RATE)); 
    --    
    L_error := TRUE;
    --
  end if;
  --
  if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_VAT_TYPE(O_error_message,
                                                      I_xxadeo_dept_level_vat_stg.vat_type) then
    --
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                DEPT_LEVEL_VAT_SHEET,
                I_row_seq,
                DEPT_LEVEL_VAT$VAT_TYPE,
                nvl(O_error_message,XXADEO_GLOBAL_VARS_SQL.VAL$INV_VAT_TYPE)); 
    --
    L_error := TRUE;
    --
  end if;
  --
  if I_xxadeo_dept_level_vat_stg.action = action_new then
    --
    if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_VAT_DEPT_REG_TYPE(O_error_message,
                                                                 I_xxadeo_dept_level_vat_stg.dept,
                                                                 I_xxadeo_dept_level_vat_stg.vat_region,
                                                                 I_xxadeo_dept_level_vat_stg.vat_type) then
      --
      WRITE_ERROR(L_process_id,
                  svc_admin_upld_er_seq.nextval,
                  LP_chunk_id,
                  DEPT_LEVEL_VAT_SHEET,
                  I_row_seq,
                  DEPT_LEVEL_VAT$DEPT,
                  nvl(O_error_message,XXADEO_GLOBAL_VARS_SQL.VAL$VAT_DEPT_REG_TYPE)); 
      --    
      L_error := TRUE;
      --
    end if;
    --
  end if;
  --
  If not IO_ERROR then
    IO_ERROR := L_error;
  end if;
  --
  return TRUE;
  --
END;     
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_DEPT_LEVEL_VAT(O_error_message   IN OUT  LOGGER_LOGS.TEXT%TYPE) IS
  --
  L_program                       VARCHAR2(64) := 'XXADEO_DEPT_LEVEL_VAT_SQL.PROCESS_S9T_DEPT_LEVEL_VAT';
  --
  TYPE XXADEO_DEPT_LEVEL_VAT_TYPE IS TABLE OF XXADEO_DEPT_LEVEL_VAT_STG%ROWTYPE;
  L_xxadeo_dept_level_vat_tbl     XXADEO_DEPT_LEVEL_VAT_TYPE := XXADEO_DEPT_LEVEL_VAT_TYPE();
  L_temp_rec                      XXADEO_DEPT_LEVEL_VAT_STG%ROWTYPE;
  L_issues_rt                     S9T_ERRORS%ROWTYPE;
  --
  L_error                         BOOLEAN:=FALSE;
  L_default_rec                   XXADEO_DEPT_LEVEL_VAT_STG%ROWTYPE;
  L_row_seq                       SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE;
  L_error_code                    NUMBER;
  L_error_msg                     RTK_ERRORS.RTK_TEXT%TYPE;
  --
  DML_ERRORS    EXCEPTION;
  PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
  --
  cursor C_mandatory_ind is
    select vat_region_mi,
           group_id_mi,
           group_name_mi,
           dept_mi,
           dept_name_mi,
           vat_type_mi,
           vat_code_mi,
           1 as dummy
      from (select column_key,
                   required_visual_ind
              from s9t_tmpl_cols_def
             where template_key   = TEMPLATE_KEY
               and wksht_key      = 'DEPT_LEVEL_VAT'
            ) pivot (max(required_visual_ind) as mi
                     for (column_key) in ('VAT_REGION' as vat_region,
                                          'GROUP_ID'   as group_id,
                                          'GROUP_NAME' as group_name,
                                          'DEPT'       as dept,
                                          'DEPT_NAME'  as dept_name,
                                          'VAT_TYPE'   as vat_type,
                                          'VAT_CODE'   as vat_code,
                                           null as dummy));
  L_mi_rec C_mandatory_ind%ROWTYPE;
  --
  CURSOR C_get_date_format IS
    select value
      from nls_database_parameters
     where parameter = 'NLS_DATE_FORMAT';
  --
BEGIN
  --
  OPEN C_get_date_format;
  FETCH C_get_date_format INTO L_date_format;
  CLOSE C_get_date_format;
  --
  -- get default values
  --
  for rec in (select vat_region_dv,
                     group_id_dv,
                     group_name_dv,
                     dept_dv,
                     dept_name_dv,
                     vat_type_dv,
                     vat_code_dv,
                     null as dummy
               from (select column_key,
                            default_value
                       from s9t_tmpl_cols_def
                      where template_key  = template_key
                        and wksht_key     = 'DEPT_LEVEL_VAT'
                     ) PIVOT (MAX(default_value) AS dv
                              FOR (column_key) IN ('VAT_REGION' as vat_region,
                                                   'GROUP_ID'   as group_id,
                                                   'GROUP_NAME' as group_name,
                                                   'DEPT'       as dept,
                                                   'DEPT_NAME'  as dept_name,
                                                   'VAT_TYPE'   as vat_type,
                                                   'VAT_CODE'   as vat_code,
                                                   NULL AS dummy))) loop
    --
    -- vat_region default
    --
    BEGIN
      --
      L_default_rec.vat_region := rec.vat_region_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$VAT_REGION;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- group_id default
    --
    BEGIN
      --
      L_default_rec.group_id := rec.group_id_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_Id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_Key        := DEPT_LEVEL_VAT$GROUP_ID;
        L_issues_rt.error_Key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- group_name default
    --
    BEGIN
      --
      L_default_rec.group_name := rec.group_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$GROUP_NAME;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT default
    --
    BEGIN
      --
      L_default_rec.dept := rec.dept_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_process_id;
        L_issues_rt.wksht_key    := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key   := DEPT_LEVEL_VAT$DEPT;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT_NAME default
    --
    BEGIN
      --
      L_default_rec.dept_name := rec.dept_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_process_id;
        L_issues_rt.wksht_key    := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key   := DEPT_LEVEL_VAT$DEPT_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- VAT_TYPE default
    --
    BEGIN
      --
      L_default_rec.vat_type := rec.vat_type_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_process_id;
        L_issues_rt.wksht_key    := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key   := DEPT_LEVEL_VAT$VAT_TYPE;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- VAT_CODE default
    --
    BEGIN
      --
      L_default_rec.vat_code := rec.vat_code_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_process_id;
        L_issues_rt.wksht_key    := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key   := DEPT_LEVEL_VAT$VAT_CODE;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
  end loop;
  --
  -- get mandatory indicators
  --
  open C_mandatory_ind;
  fetch C_mandatory_ind into L_mi_rec;
  close C_mandatory_ind;
  --
  -- validate datatypes and business rules
  --
  for rec IN (select r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPT_LEVEL_VAT_SHEET,
                                                                     I_column_key                  => DEPT_LEVEL_VAT$ACTION))               as ACTION,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPT_LEVEL_VAT_SHEET,
                                                                     I_column_key                  => DEPT_LEVEL_VAT$VAT_REGION))           as VAT_REGION,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPT_LEVEL_VAT_SHEET,
                                                                     I_column_key                  => DEPT_LEVEL_VAT$GROUP_ID))             as GROUP_ID,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPT_LEVEL_VAT_SHEET,
                                                                     I_column_key                  => DEPT_LEVEL_VAT$GROUP_NAME))           as GROUP_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPT_LEVEL_VAT_SHEET,
                                                                     I_column_key                  => DEPT_LEVEL_VAT$DEPT))                 as DEPT,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPT_LEVEL_VAT_SHEET,
                                                                     I_column_key                  => DEPT_LEVEL_VAT$DEPT_NAME))            as DEPT_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPT_LEVEL_VAT_SHEET,
                                                                     I_column_key                  => DEPT_LEVEL_VAT$VAT_TYPE))             as VAT_TYPE,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPT_LEVEL_VAT_SHEET,
                                                                     I_column_key                  => DEPT_LEVEL_VAT$VAT_CODE))             as VAT_CODE,
                     r.get_row_seq()                                                                                                        as row_seq                                                                                             
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = L_file_id
                 and ss.sheet_name = SHEET_NAME_TRANS(DEPT_LEVEL_VAT_SHEET)) loop
    --
    L_temp_rec                      := null;
    L_temp_rec.process_id           := L_process_id;
    L_temp_rec.create_id            := LP_user;
    L_temp_rec.last_update_id       := LP_user;
    L_temp_rec.create_datetime      := sysdate;
    L_temp_rec.last_update_datetime := sysdate;
    L_error                         := FALSE;
    L_row_seq                       := rec.row_seq;
    --
    LP_chunk_id                     := 1;
    --
    -- validate data types
    --
    if rec.vat_type is null and rec.vat_code is null then
      --
      continue;
      --
    end if;
    --
    -- action
    --
    BEGIN
      --
      if rec.action is null then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      else
        --
        if rec.action = GP_upload_create or
           rec.action = GP_upload_update then
          --
          L_temp_rec.action := rec.action;
          --
        else
        --
          L_issues_rt                   := null;
          L_issues_rt.file_id           := L_file_id;
          L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
          L_issues_rt.column_key        := DEPT_LEVEL_VAT$ACTION;
          L_issues_rt.row_seq           := rec.row_seq;
          L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
          --
          WRITE_S9T_ERROR(O_error_message => O_error_message,
                          I_issues_rt     => L_issues_rt);
          --
          L_error := TRUE;
          --
        end if;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- vat_region
    --
    BEGIN
      --
      if L_mi_rec.vat_region_mi   = 'Y'   and
         L_default_rec.vat_region is null and
         rec.vat_region is null     then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$VAT_REGION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.vat_region := rec.vat_region;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$VAT_REGION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- group_id
    --
    BEGIN
      --
      if L_mi_rec.group_id_mi   = 'Y'   and
         L_default_rec.group_id is null and
         rec.group_id is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_id := rec.group_id;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- group_name
    --
    BEGIN
      --
      if L_mi_rec.group_name_mi   = 'Y'   and
         L_default_rec.group_name is null and
         rec.group_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_name := rec.group_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT
    --
    BEGIN
      --
      if L_mi_rec.dept_mi   = 'Y'   and
         L_default_rec.dept is null and
         rec.dept is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept := rec.dept;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT_NAME
    --
    BEGIN
      --
      if L_mi_rec.dept_name_mi   = 'Y'        and
         L_default_rec.dept_name is null and
         rec.dept_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept_name := rec.dept_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- VAT_TYPE
    --
    BEGIN
      --
      if L_mi_rec.vat_type_mi   = 'Y'   and
         L_default_rec.vat_type is null and
         rec.vat_type is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$VAT_TYPE;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.vat_type := rec.vat_type;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$VAT_TYPE;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- VAT_CODE
    --
    BEGIN
      --
      if L_mi_rec.vat_code_mi   = 'Y'   and
         L_default_rec.vat_code is null and
         rec.vat_code is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$VAT_CODE;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.vat_code := rec.vat_code;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.column_key        := DEPT_LEVEL_VAT$VAT_CODE;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- validate business rules
    --
    if not L_error then
      --
      if not VALIDATE_DEPT_LEVEL_VAT(O_error_message,
                                     L_error,
                                     L_temp_rec,
                                     L_row_seq) then
        --
        RETURN;
        --
      end if;
      --
    end if;
    --
    -- if line does not have error, insert in table to process
    --
    if NOT L_error then
      --
      L_temp_rec.row_seq   := rec.row_seq;
      --
      L_xxadeo_dept_level_vat_tbl.extend();
      L_xxadeo_dept_level_vat_tbl(L_xxadeo_dept_level_vat_tbl.count()) := L_temp_rec;
      --
    end if;
    --
  --
  end loop;
  --
  -- insert into stagging
  --
  BEGIN
    --
    forall i IN 1..L_xxadeo_dept_level_vat_tbl.count SAVE EXCEPTIONS
      insert into xxadeo_dept_level_vat_stg
        (process_id,
         action,
         vat_region,
         group_id,
         group_name,
         dept,
         dept_name,
         vat_type,
         vat_code,
         create_id,
         create_datetime,
         last_update_id,
         last_update_datetime,
         row_seq)
      values
        (L_xxadeo_dept_level_vat_tbl(i).process_id,
         L_xxadeo_dept_level_vat_tbl(i).action,
         L_xxadeo_dept_level_vat_tbl(i).vat_region,
         L_xxadeo_dept_level_vat_tbl(i).group_id,
         L_xxadeo_dept_level_vat_tbl(i).group_name,
         L_xxadeo_dept_level_vat_tbl(i).dept,
         L_xxadeo_dept_level_vat_tbl(i).dept_name,
         L_xxadeo_dept_level_vat_tbl(i).vat_type,
         L_xxadeo_dept_level_vat_tbl(i).vat_code,
         L_xxadeo_dept_level_vat_tbl(i).create_id,
         L_xxadeo_dept_level_vat_tbl(i).create_datetime,
         L_xxadeo_dept_level_vat_tbl(i).last_update_id,
         L_xxadeo_dept_level_vat_tbl(i).last_update_datetime,
         L_xxadeo_dept_level_vat_tbl(i).row_seq);
    --
  EXCEPTION
    --
    when DML_ERRORS then
      --
      for i in 1..sql%bulk_exceptions.COUNT loop
        --
        L_error_code:=sql%bulk_exceptions(i).error_code;
        --
        if L_error_code=1 then
          --
          L_error_code:=NULL;
          L_error_msg := SQL_LIB.CREATE_MSG(XXADEO_GLOBAL_VARS_SQL.VAL$DUP_REC_EXISTS_S9T);
          --
        end if;
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPT_LEVEL_VAT_SHEET;
        L_issues_rt.row_seq           := sql%bulk_exceptions(i).error_index;
        L_issues_rt.error_key         := sqlerrm(-sql%bulk_exceptions(i).error_code);
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      end loop;
      --
    --
  END;
  --
END PROCESS_S9T_DEPT_LEVEL_VAT;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
  --
  L_program          VARCHAR2(64) := 'XXADEO_DEPT_LEVEL_VAT.PROCESS_S9T';
  --
  L_file             S9T_FILE;
  L_sheets           S9T_PKG.names_map_typ;
  L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
  INVALID_FORMAT     EXCEPTION;
  PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
  MAX_CHAR           EXCEPTION;
  PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
  L_issues_rt        S9T_ERRORS%ROWTYPE;
  --
BEGIN
  --
  COMMIT;
  S9T_PKG.ODS2OBJ(I_file_id);
  COMMIT;
  --
  L_file_id    := I_file_id;
  L_process_id :=  I_process_id;
  --
  L_file := s9t_file(I_file_id,TRUE);
  LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
  if S9T_PKG.CODE2DESC(O_error_message,
                       template_category,
                       L_file,
                       TRUE) = FALSE then
     return FALSE;
  end if;
  --
  S9T_PKG.SAVE_OBJ(L_file);
  --
  LP_errors_tab   := NEW errors_tab_typ();
  --
  if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
    --
    L_issues_rt                   := null;
    L_issues_rt.file_id           := L_file_id;
    L_issues_rt.column_key        := NULL;
    L_issues_rt.error_type        := NULL;
    L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$S9T_INVALID_TEMPLATE;
    --
    WRITE_S9T_ERROR(O_error_message => O_error_message,
                    I_issues_rt     => L_issues_rt);
  else
    sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                            L_file.user_lang);
    --
    -- load template column indexex object
    --
    L_xxadeo_wksht_column_idx_tbl := XXADEO_WKSHT_COLUMN_IDX_TBL();
    --
    if XXADEO_CUSTOM_UPLOAD_SQL.GET_TEMPLATE_COLUMN_INDEXES(O_error_message                => O_error_message,
                                                            IO_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                            I_template_key                 => TEMPLATE_KEY) = FALSE then
      --
      COMMIT;
      RETURN FALSE;
      --
    end if;
    --
    PROCESS_S9T_DEPT_LEVEL_VAT(O_error_message => O_error_message);
    --
  end if;
  --
  O_error_count := LP_s9t_errors_tab.COUNT() + LP_errors_tab.COUNT();
  --
  FORALL i IN 1..LP_s9t_errors_tab.COUNT
     insert into s9t_errors
          values LP_s9t_errors_tab(i);
  LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
  --
  FORALL i IN 1..LP_errors_tab.COUNT
    insert into svc_admin_upld_er
         values LP_errors_tab(i);
  LP_errors_tab := NEW errors_tab_typ();
  --Update process$status in svc_process_tracker
  if O_error_count = 0 then
     L_process_status := 'PS';
  else
     L_process_status := 'PE';
  end if;
  --
  update svc_process_tracker
     set status     = L_process_status,
         file_id    = I_file_id
   where process_id = I_process_id;
  --
  COMMIT;
  --
  return TRUE;
  --
EXCEPTION
  --
  when INVALID_FORMAT then
    --
    rollback;
    O_error_message := SQL_LIB.CREATE_MSG(XXADEO_GLOBAL_VARS_SQL.VAL$INV_FILE_FORMAT);
    LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
    --
    L_issues_rt                   := null;
    L_issues_rt.file_id           := L_file_id;
    L_issues_rt.column_key        := NULL;
    L_issues_rt.error_type        := NULL;
    L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_FILE_FORMAT;
    --
    WRITE_S9T_ERROR(O_error_message => O_error_message,
                   I_issues_rt     => L_issues_rt);
    --
    O_error_count := LP_s9t_errors_tab.count();
    forall i IN 1..O_error_count
       insert into s9t_errors
            values LP_s9t_errors_tab(i);

    update svc_process_tracker
       set status       = 'PE',
           file_id      = I_file_id
     where process_id   = I_process_id;
    COMMIT;
    --
    return FALSE;
    --
  when MAX_CHAR then
    --
    ROLLBACK;
    O_error_message := SQL_LIB.CREATE_MSG(XXADEO_GLOBAL_VARS_SQL.VAL$EXCEEDS_4000_CHAR);
    Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
    L_issues_rt                   := null;
    L_issues_rt.file_id           := L_file_id;
    L_issues_rt.column_key        := NULL;
    L_issues_rt.error_type        := NULL;
    L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$EXCEEDS_4000_CHAR;
    write_s9t_error(O_error_message => O_error_message,
                    I_issues_rt     => L_issues_rt);
    O_error_count := Lp_s9t_errors_tab.count();
    forall i IN 1..O_error_count
      --
      insert into s9t_errors values Lp_s9t_errors_tab (i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
    --
    RETURN FALSE;
    --
  when OTHERS then
    --
    ROLLBACK;
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END PROCESS_S9T;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS
BEGIN
  --
  delete from xxadeo_dept_level_vat_stg
   where process_id = I_process_id;
  --
END;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
  L_program          VARCHAR2(64)                    :='XXADEO_DEPT_LEVEL_VAT.PROCESS';
  L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
  L_err_count        VARCHAR2(1);
  L_warn_count       VARCHAR2(1);
  L_errors_count     NUMBER;
  --
  SQL_ERROR EXCEPTION;
  --
  cursor C_GET_ERR_COUNT is
     select 'x'
       from svc_admin_upld_er
      where process_id = I_process_id
        and error_type = 'E';

  cursor C_GET_WARN_COUNT is
     select 'x'
       from svc_admin_upld_er
      where process_id = I_process_id
        and error_type = 'W';

BEGIN
  --
  LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
  LP_chunk_id     := I_chunk_id;
  LP_errors_tab   := NEW errors_tab_typ();
  --
  LP_s9t_errors_tab := s9t_errors_tab_typ();
  --
  if SETUP_DEPT_LEVEL_VAT(O_error_message   => O_error_message) = FALSE then
    --
    COMMIT;
    RAISE SQL_ERROR;
    --
  end if;
  --
  COMMIT;
  --
  L_errors_count := L_errors_count + LP_s9t_errors_tab.count();
   --
   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
  --
  when SQL_ERROR then
    --
    update svc_process_tracker
       set status = 'PE',
           action_date = SYSDATE
     where process_id = I_process_id;
    --
    CLEAR_STAGING_DATA(I_process_id);
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
  --
  when OTHERS then
    --
    CLEAR_STAGING_DATA(I_process_id);
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    return FALSE;
    --
END PROCESS;
----------------------------------------------------------------------------------------------
END XXADEO_DEPT_LEVEL_VAT_SQL;
/
