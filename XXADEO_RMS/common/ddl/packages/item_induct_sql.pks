CREATE OR REPLACE PACKAGE ITEM_INDUCT_SQL AUTHID CURRENT_USER
AS
   template_key                   VARCHAR2(255) :='CORESVC_ITEM';
   file_name                      VARCHAR2(255);
   add_error_4_process            SVC_PROCESS_TRACKER.PROCESS_ID%TYPE;
   user_lang                      NUMBER(6)     := 1;
   action_column                  VARCHAR2(255) := 'ACTION';
   ICD_sheet                      VARCHAR2(255) := 'ITEM_COST_DETAIL';
   ICD$Action                     NUMBER        :=1;
   ICD$ITEM                       NUMBER        :=2;
   ICD$SUPPLIER                   NUMBER        :=3;
   ICD$ORIGIN_COUNTRY_ID          NUMBER        :=4;
   ICD$DELIVERY_COUNTRY_ID        NUMBER        :=5;
   ICD$COND_TYPE                  NUMBER        :=6;
   ICD$COND_VALUE                 NUMBER        :=7;
   ICD$APPLIED_ON                 NUMBER        :=8;
   ICD$COMP_RATE                  NUMBER        :=9;
   ICD$CALCULATION_BASIS          NUMBER        :=10;
   ICD$RECOVERABLE_AMOUNT         NUMBER        :=11;
   ICD$MODIFIED_TAXABLE_BASE      NUMBER        :=12;
   ICH_sheet                      VARCHAR2(255) := 'ITEM_COST_HEAD';
   ICH$Action                     NUMBER        :=1;
   ICH$ITEM                       NUMBER        :=2;
   ICH$SUPPLIER                   NUMBER        :=3;
   ICH$ORIGIN_COUNTRY_ID          NUMBER        :=4;
   ICH$DELIVERY_COUNTRY_ID        NUMBER        :=5;
   ICH$PRIM_DLVY_CTRY_IND         NUMBER        :=6;
   ICH$NIC_STATIC_IND             NUMBER        :=7;
   ICH$BASE_COST                  NUMBER        :=8;
   ICH$NEGOTIATED_ITEM_COST       NUMBER        :=9;
   ICH$EXTENDED_BASE_COST         NUMBER        :=10;
   ICH$INCLUSIVE_COST             NUMBER        :=11;
   IC_sheet                       VARCHAR2(255) := 'ITEM_COUNTRY';
   IC$Action                      NUMBER        :=1;
   IC$ITEM                        NUMBER        :=2;
   IC$COUNTRY_ID                  NUMBER        :=3;
   IC_L10_sheet                   VARCHAR2(255) := 'ITEM_COUNTRY_L10N_EXT';
   IC_L10$Action                  NUMBER        :=1;
   IC_L10$ITEM                    NUMBER        :=2;
   IC_L10$COUNTRY_ID              NUMBER        :=3;
   IC_L10$L10N_COUNTRY_ID         NUMBER        :=4;
   IC_L10$GROUP_ID                NUMBER        :=5;
   IC_L10$VARCHAR2_1              NUMBER        :=6;
   IC_L10$VARCHAR2_2              NUMBER        :=7;
   IC_L10$VARCHAR2_3              NUMBER        :=8;
   IC_L10$VARCHAR2_4              NUMBER        :=9;
   IC_L10$VARCHAR2_5              NUMBER        :=10;
   IC_L10$VARCHAR2_6              NUMBER        :=11;
   IC_L10$VARCHAR2_7              NUMBER        :=12;
   IC_L10$VARCHAR2_8              NUMBER        :=13;
   IC_L10$VARCHAR2_9              NUMBER        :=14;
   IC_L10$VARCHAR2_10             NUMBER        :=15;
   IC_L10$NUMBER_11               NUMBER        :=16;
   IC_L10$NUMBER_12               NUMBER        :=17;
   IC_L10$NUMBER_13               NUMBER        :=18;
   IC_L10$NUMBER_14               NUMBER        :=19;
   IC_L10$NUMBER_15               NUMBER        :=20;
   IC_L10$NUMBER_16               NUMBER        :=21;
   IC_L10$NUMBER_17               NUMBER        :=22;
   IC_L10$NUMBER_18               NUMBER        :=23;
   IC_L10$NUMBER_19               NUMBER        :=24;
   IC_L10$NUMBER_20               NUMBER        :=25;
   IC_L10$DATE_21                 NUMBER        :=26;
   IC_L10$DATE_22                 NUMBER        :=27;
   IM_sheet                       VARCHAR2(255) := 'ITEM_MASTER';
   IM$Action                      NUMBER        :=1;
   IM$ITEM                        NUMBER        :=2;
   IM$ITEM_NUMBER_TYPE            NUMBER        :=3;
   IM$FORMAT_ID                   NUMBER        :=4;
   IM$PREFIX                      NUMBER        :=5;
   IM$ITEM_PARENT                 NUMBER        :=6;
   IM$ITEM_GRANDPARENT            NUMBER        :=7;
   IM$PACK_IND                    NUMBER        :=8;
   IM$ITEM_LEVEL                  NUMBER        :=9;
   IM$TRAN_LEVEL                  NUMBER        :=10;
   IM$ITEM_AGGREGATE_IND          NUMBER        :=11;
   IM$DIFF_1                      NUMBER        :=12;
   IM$DIFF_1_AGGREGATE_IND        NUMBER        :=13;
   IM$DIFF_2                      NUMBER        :=14;
   IM$DIFF_2_AGGREGATE_IND        NUMBER        :=15;
   IM$DIFF_3                      NUMBER        :=16;
   IM$DIFF_3_AGGREGATE_IND        NUMBER        :=17;
   IM$DIFF_4                      NUMBER        :=18;
   IM$DIFF_4_AGGREGATE_IND        NUMBER        :=19;
   IM$DEPT                        NUMBER        :=20;
   IM$CLASS                       NUMBER        :=21;
   IM$SUBCLASS                    NUMBER        :=22;
   IM$STATUS                      NUMBER        :=23;
   IM$ITEM_DESC                   NUMBER        :=24;
   IM$ITEM_DESC_SECONDARY         NUMBER        :=25;
   IM$SHORT_DESC                  NUMBER        :=26;
   IM$DESC_UP                     NUMBER        :=27;
   IM$PRIMARY_REF_ITEM_IND        NUMBER        :=28;
   IM$COST_ZONE_GROUP_ID          NUMBER        :=29;
   IM$STANDARD_UOM                NUMBER        :=30;
   IM$UOM_CONV_FACTOR             NUMBER        :=31;
   IM$PACKAGE_SIZE                NUMBER        :=32;
   IM$PACKAGE_UOM                 NUMBER        :=33;
   IM$MERCHANDISE_IND             NUMBER        :=34;
   IM$STORE_ORD_MULT              NUMBER        :=35;
   IM$FORECAST_IND                NUMBER        :=36;
   IM$ORIGINAL_RETAIL             NUMBER        :=37;
   IM$MFG_REC_RETAIL              NUMBER        :=38;
   IM$RETAIL_LABEL_TYPE           NUMBER        :=39;
   IM$RETAIL_LABEL_VALUE          NUMBER        :=40;
   IM$HANDLING_TEMP               NUMBER        :=41;
   IM$HANDLING_SENSITIVITY        NUMBER        :=42;
   IM$CATCH_WEIGHT_IND            NUMBER        :=43;
   IM$WASTE_TYPE                  NUMBER        :=44;
   IM$WASTE_PCT                   NUMBER        :=45;
   IM$DEFAULT_WASTE_PCT           NUMBER        :=46;
   IM$CONST_DIMEN_IND             NUMBER        :=47;
   IM$SIMPLE_PACK_IND             NUMBER        :=48;
   IM$CONTAINS_INNER_IND          NUMBER        :=49;
   IM$SELLABLE_IND                NUMBER        :=50;
   IM$ORDERABLE_IND               NUMBER        :=51;
   IM$PACK_TYPE                   NUMBER        :=52;
   IM$ORDER_AS_TYPE               NUMBER        :=53;
   IM$COMMENTS                    NUMBER        :=54;
   IM$ITEM_SERVICE_LEVEL          NUMBER        :=55;
   IM$GIFT_WRAP_IND               NUMBER        :=56;
   IM$SHIP_ALONE_IND              NUMBER        :=57;
   IM$CHECK_UDA_IND               NUMBER        :=61;
   IM$ITEM_XFORM_IND              NUMBER        :=62;
   IM$INVENTORY_IND               NUMBER        :=63;
   IM$ORDER_TYPE                  NUMBER        :=64;
   IM$SALE_TYPE                   NUMBER        :=65;
   IM$DEPOSIT_ITEM_TYPE           NUMBER        :=66;
   IM$CONTAINER_ITEM              NUMBER        :=67;
   IM$DEPOSIT_IN_PRICE_PER_UOM    NUMBER        :=68;
   IM$AIP_CASE_TYPE               NUMBER        :=69;
   IM$CATCH_WEIGHT_TYPE           NUMBER        :=70;
   IM$PERISHABLE_IND              NUMBER        :=71;
   IM$NOTIONAL_PACK_IND           NUMBER        :=72;
   IM$SOH_INQUIRY_AT_PACK_IND     NUMBER        :=73;
   IM$CATCH_WEIGHT_UOM            NUMBER        :=74;
   IM$PRODUCT_CLASSIFICATION      NUMBER        :=75;
   IM$BRAND_NAME                  NUMBER        :=76;
   IM$PRE_RESERVED_IND            NUMBER        :=77;
   IM$NEXT_UPD_ID                 VARCHAR2(30)  := 78;
   IM$ORIG_REF_NO                 VARCHAR2(25)  := 79;
   IMTL_sheet                    VARCHAR2(255) := 'ITEM_MASTER_TL';
   IMTL$Action                   NUMBER        :=1;
   IMTL$LANG                     NUMBER        :=2;
   IMTL$ITEM                     NUMBER        :=3;
   IMTL$ITEM_DESC                NUMBER        :=4;
   IMTL$ITEM_DESC_SECONDARY      NUMBER        :=5;
   IMTL$SHORT_DESC               NUMBER        :=6;
   IM_CFA_sheet                   VARCHAR2(255) := 'ITEM_MASTER_CFA_EXT';
   IM_CFA$Action                  NUMBER        :=1;
   IM_CFA$ITEM                    NUMBER        :=2;
   IM_CFA$GROUP_ID                NUMBER        :=3;
   IM_CFA$VARCHAR2_1              NUMBER        :=4;
   IM_CFA$VARCHAR2_2              NUMBER        :=5;
   IM_CFA$VARCHAR2_3              NUMBER        :=6;
   IM_CFA$VARCHAR2_4              NUMBER        :=7;
   IM_CFA$VARCHAR2_5              NUMBER        :=8;
   IM_CFA$VARCHAR2_6              NUMBER        :=9;
   IM_CFA$VARCHAR2_7              NUMBER        :=10;
   IM_CFA$VARCHAR2_8              NUMBER        :=11;
   IM_CFA$VARCHAR2_9              NUMBER        :=12;
   IM_CFA$VARCHAR2_10             NUMBER        :=13;
   IM_CFA$NUMBER_11               NUMBER        :=14;
   IM_CFA$NUMBER_12               NUMBER        :=15;
   IM_CFA$NUMBER_13               NUMBER        :=16;
   IM_CFA$NUMBER_14               NUMBER        :=17;
   IM_CFA$NUMBER_15               NUMBER        :=18;
   IM_CFA$NUMBER_16               NUMBER        :=19;
   IM_CFA$NUMBER_17               NUMBER        :=20;
   IM_CFA$NUMBER_18               NUMBER        :=21;
   IM_CFA$NUMBER_19               NUMBER        :=22;
   IM_CFA$NUMBER_20               NUMBER        :=23;
   IM_CFA$DATE_21                 NUMBER        :=24;
   IM_CFA$DATE_22                 NUMBER        :=25;
   IM_CFA$DATE_23                 NUMBER        :=26;
   IM_CFA$DATE_24                 NUMBER        :=27;
   IM_CFA$DATE_25                 NUMBER        :=28;
   IS_sheet                       VARCHAR2(255) := 'ITEM_SUPPLIER';
   IS$Action                      NUMBER        :=1;
   IS$ITEM                        NUMBER        :=2;
   IS$SUPPLIER                    NUMBER        :=3;
   IS$PRIMARY_SUPP_IND            NUMBER        :=4;
   IS$VPN                         NUMBER        :=5;
   IS$SUPP_LABEL                  NUMBER        :=6;
   IS$CONSIGNMENT_RATE            NUMBER        :=7;
   IS$SUPP_DIFF_1                 NUMBER        :=8;
   IS$SUPP_DIFF_2                 NUMBER        :=9;
   IS$SUPP_DIFF_3                 NUMBER        :=10;
   IS$SUPP_DIFF_4                 NUMBER        :=11;
   IS$PALLET_NAME                 NUMBER        :=12;
   IS$CASE_NAME                   NUMBER        :=13;
   IS$INNER_NAME                  NUMBER        :=14;
   IS$SUPP_DISCONTINUE_DATE       NUMBER        :=15;
   IS$DIRECT_SHIP_IND             NUMBER        :=16;
   IS$CONCESSION_RATE             NUMBER        :=20;
   IS$PRIMARY_CASE_SIZE           NUMBER        :=21;
   ISTL_sheet                    VARCHAR2(255) := 'ITEM_SUPPLIER_TL';
   ISTL$Action                   NUMBER        :=1;
   ISTL$LANG                     NUMBER        :=2;
   ISTL$ITEM                     NUMBER        :=3;
   ISTL$SUPPLIER                 NUMBER        :=4;
   ISTL$SUPP_LABEL               NUMBER        :=5;
   ISTL$SUPP_DIFF_1              NUMBER        :=6;
   ISTL$SUPP_DIFF_2              NUMBER        :=7;
   ISTL$SUPP_DIFF_3              NUMBER        :=8;
   ISTL$SUPP_DIFF_4              NUMBER        :=9;
   IS_CFA_sheet                   VARCHAR2(255) := 'ITEM_SUPPLIER_CFA_EXT';
   IS_CFA$Action                  NUMBER        :=1;
   IS_CFA$ITEM                    NUMBER        :=2;
   IS_CFA$SUPPLIER                NUMBER        :=3;
   IS_CFA$GROUP_ID                NUMBER        :=4;
   IS_CFA$VARCHAR2_1              NUMBER        :=5;
   IS_CFA$VARCHAR2_2              NUMBER        :=6;
   IS_CFA$VARCHAR2_3              NUMBER        :=7;
   IS_CFA$VARCHAR2_4              NUMBER        :=8;
   IS_CFA$VARCHAR2_5              NUMBER        :=9;
   IS_CFA$VARCHAR2_6              NUMBER        :=10;
   IS_CFA$VARCHAR2_7              NUMBER        :=11;
   IS_CFA$VARCHAR2_8              NUMBER        :=12;
   IS_CFA$VARCHAR2_9              NUMBER        :=13;
   IS_CFA$VARCHAR2_10             NUMBER        :=14;
   IS_CFA$NUMBER_11               NUMBER        :=15;
   IS_CFA$NUMBER_12               NUMBER        :=16;
   IS_CFA$NUMBER_13               NUMBER        :=17;
   IS_CFA$NUMBER_14               NUMBER        :=18;
   IS_CFA$NUMBER_15               NUMBER        :=19;
   IS_CFA$NUMBER_16               NUMBER        :=20;
   IS_CFA$NUMBER_17               NUMBER        :=21;
   IS_CFA$NUMBER_18               NUMBER        :=22;
   IS_CFA$NUMBER_19               NUMBER        :=23;
   IS_CFA$NUMBER_20               NUMBER        :=24;
   IS_CFA$DATE_21                 NUMBER        :=25;
   IS_CFA$DATE_22                 NUMBER        :=26;
   IS_CFA$DATE_23                 NUMBER        :=27;
   IS_CFA$DATE_24                 NUMBER        :=28;
   IS_CFA$DATE_25                 NUMBER        :=29;
   ISC_sheet                      VARCHAR2(255) := 'ITEM_SUPP_COUNTRY';
   ISC$Action                     NUMBER        :=1;
   ISC$ITEM                       NUMBER        :=2;
   ISC$SUPPLIER                   NUMBER        :=3;
   ISC$ORIGIN_COUNTRY_ID          NUMBER        :=4;
   ISC$UNIT_COST                  NUMBER        :=5;
   ISC$LEAD_TIME                  NUMBER        :=6;
   ISC$PICKUP_LEAD_TIME           NUMBER        :=7;
   ISC$SUPP_PACK_SIZE             NUMBER        :=8;
   ISC$INNER_PACK_SIZE            NUMBER        :=9;
   ISC$ROUND_LVL                  NUMBER        :=10;
   ISC$ROUND_TO_INNER_PCT         NUMBER        :=11;
   ISC$ROUND_TO_CASE_PCT          NUMBER        :=12;
   ISC$ROUND_TO_LAYER_PCT         NUMBER        :=13;
   ISC$ROUND_TO_PALLET_PCT        NUMBER        :=14;
   ISC$MIN_ORDER_QTY              NUMBER        :=15;
   ISC$MAX_ORDER_QTY              NUMBER        :=16;
   ISC$PACKING_METHOD             NUMBER        :=17;
   ISC$PRIMARY_SUPP_IND           NUMBER        :=18;
   ISC$PRIMARY_COUNTRY_IND        NUMBER        :=19;
   ISC$DEFAULT_UOP                NUMBER        :=20;
   ISC$TI                         NUMBER        :=21;
   ISC$HI                         NUMBER        :=22;
   ISC$SUPP_HIER_TYPE_1           NUMBER        :=23;
   ISC$SUPP_HIER_LVL_1            NUMBER        :=24;
   ISC$SUPP_HIER_TYPE_2           NUMBER        :=25;
   ISC$SUPP_HIER_LVL_2            NUMBER        :=26;
   ISC$SUPP_HIER_TYPE_3           NUMBER        :=27;
   ISC$SUPP_HIER_LVL_3            NUMBER        :=28;
   ISC$COST_UOM                   NUMBER        :=32;
   ISC$TOLERANCE_TYPE             NUMBER        :=33;
   ISC$MAX_TOLERANCE              NUMBER        :=34;
   ISC$MIN_TOLERANCE              NUMBER        :=35;
   ISC$NEGOTIATED_ITEM_COST       NUMBER        :=36;
   ISC$EXTENDED_BASE_COST         NUMBER        :=37;
   ISC$INCLUSIVE_COST             NUMBER        :=38;
   ISC$BASE_COST                  NUMBER        :=39;

   ISCL_sheet                      VARCHAR2(255) := 'ITEM_SUPP_COUNTRY_LOC';
   ISCL$Action                     NUMBER        :=1;
   ISCL$ITEM                       NUMBER        :=2;
   ISCL$SUPPLIER                   NUMBER        :=3;
   ISCL$ORIGIN_COUNTRY_ID          NUMBER        :=4;
   ISCL$LOC                        NUMBER        :=5;
   ISCL$LOC_TYPE                   NUMBER        :=6;
   ISCL$PRIMARY_LOC_IND            NUMBER        :=7;
   ISCL$UNIT_COST                  NUMBER        :=8;
   ISCL$ROUND_LVL                  NUMBER        :=9;
   ISCL$ROUND_TO_INNER_PCT         NUMBER        :=10;
   ISCL$ROUND_TO_CASE_PCT          NUMBER        :=11;
   ISCL$ROUND_TO_LAYER_PCT         NUMBER        :=12;
   ISCL$ROUND_TO_PALLET_PCT        NUMBER        :=13;
   ISCL$SUPP_HIER_TYPE_1           NUMBER        :=14;
   ISCL$SUPP_HIER_LVL_1            NUMBER        :=15;
   ISCL$SUPP_HIER_TYPE_2           NUMBER        :=16;
   ISCL$SUPP_HIER_LVL_2            NUMBER        :=17;
   ISCL$SUPP_HIER_TYPE_3           NUMBER        :=18;
   ISCL$SUPP_HIER_LVL_3            NUMBER        :=19;
   ISCL$PICKUP_LEAD_TIME           NUMBER        :=20;
   ISCL$NEGOTIATED_ITEM_COST       NUMBER        :=21;
   ISCL$EXTENDED_BASE_COST         NUMBER        :=22;
   ISCL$INCLUSIVE_COST             NUMBER        :=23;
   ISCL$BASE_COST                  NUMBER        :=24;

   ISC_CFA_sheet                  VARCHAR2(255) := 'ITEM_SUPP_COUNTRY_CFA_EXT';
   ISC_CFA$Action                 NUMBER        :=1;
   ISC_CFA$ITEM                   NUMBER        :=2;
   ISC_CFA$SUPPLIER               NUMBER        :=3;
   ISC_CFA$ORIGIN_COUNTRY_ID      NUMBER        :=4;
   ISC_CFA$GROUP_ID               NUMBER        :=5;
   ISC_CFA$VARCHAR2_1             NUMBER        :=6;
   ISC_CFA$VARCHAR2_2             NUMBER        :=7;
   ISC_CFA$VARCHAR2_3             NUMBER        :=8;
   ISC_CFA$VARCHAR2_4             NUMBER        :=9;
   ISC_CFA$VARCHAR2_5             NUMBER        :=10;
   ISC_CFA$VARCHAR2_6             NUMBER        :=11;
   ISC_CFA$VARCHAR2_7             NUMBER        :=12;
   ISC_CFA$VARCHAR2_8             NUMBER        :=13;
   ISC_CFA$VARCHAR2_9             NUMBER        :=14;
   ISC_CFA$VARCHAR2_10            NUMBER        :=15;
   ISC_CFA$NUMBER_11              NUMBER        :=16;
   ISC_CFA$NUMBER_12              NUMBER        :=17;
   ISC_CFA$NUMBER_13              NUMBER        :=18;
   ISC_CFA$NUMBER_14              NUMBER        :=19;
   ISC_CFA$NUMBER_15              NUMBER        :=20;
   ISC_CFA$NUMBER_16              NUMBER        :=21;
   ISC_CFA$NUMBER_17              NUMBER        :=22;
   ISC_CFA$NUMBER_18              NUMBER        :=23;
   ISC_CFA$NUMBER_19              NUMBER        :=24;
   ISC_CFA$NUMBER_20              NUMBER        :=25;
   ISC_CFA$DATE_21                NUMBER        :=26;
   ISC_CFA$DATE_22                NUMBER        :=27;
   ISC_CFA$DATE_23                NUMBER        :=28;
   ISC_CFA$DATE_24                NUMBER        :=29;
   ISC_CFA$DATE_25                NUMBER        :=30;
   ISCD_sheet                     VARCHAR2(255) := 'ITEM_SUPP_COUNTRY_DIM';
   ISCD$Action                    NUMBER        :=1;
   ISCD$ITEM                      NUMBER        :=2;
   ISCD$SUPPLIER                  NUMBER        :=3;
   ISCD$ORIGIN_COUNTRY            NUMBER        :=4;
   ISCD$DIM_OBJECT                NUMBER        :=5;
   ISCD$PRESENTATION_METHOD       NUMBER        :=6;
   ISCD$LENGTH                    NUMBER        :=7;
   ISCD$WIDTH                     NUMBER        :=8;
   ISCD$HEIGHT                    NUMBER        :=9;
   ISCD$LWH_UOM                   NUMBER        :=10;
   ISCD$WEIGHT                    NUMBER        :=11;
   ISCD$NET_WEIGHT                NUMBER        :=12;
   ISCD$WEIGHT_UOM                NUMBER        :=13;
   ISCD$LIQUID_VOLUME             NUMBER        :=14;
   ISCD$LIQUID_VOLUME_UOM         NUMBER        :=15;
   ISCD$STAT_CUBE                 NUMBER        :=16;
   ISCD$TARE_WEIGHT               NUMBER        :=17;
   ISCD$TARE_TYPE                 NUMBER        :=18;
   ISMC_sheet                     VARCHAR2(255) := 'ITEM_SUPP_MANU_COUNTRY';
   ISMC$Action                    NUMBER        :=1;
   ISMC$ITEM                      NUMBER        :=2;
   ISMC$SUPPLIER                  NUMBER        :=3;
   ISMC$MANU_COUNTRY_ID           NUMBER        :=4;
   ISMC$PRIMARY_MANU_CTRY_IND     NUMBER        :=5;
   ISU_sheet                      VARCHAR2(255) := 'ITEM_SUPP_UOM';
   ISU$Action                     NUMBER        :=1;
   ISU$ITEM                       NUMBER        :=2;
   ISU$SUPPLIER                   NUMBER        :=3;
   ISU$UOM                        NUMBER        :=4;
   ISU$VALUE                      NUMBER        :=5;
   IXD_sheet                      VARCHAR2(255) := 'ITEM_XFORM_DETAIL';
   IXD$Action                     NUMBER        :=1;
   IXD$HEAD_ITEM                  NUMBER        :=2;
   IXD$DETAIL_ITEM                NUMBER        :=3;
   IXD$ITEM_QUANTITY_PCT          NUMBER        :=4;
   IXD$YIELD_FROM_HEAD_ITEM_PCT   NUMBER        :=5;
   IXH_sheet                      VARCHAR2(255) := 'ITEM_XFORM_HEAD';
   IXH$Action                     NUMBER        :=1;
   IXH$ITEM_XFORM_HEAD_ID         NUMBER        :=2;
   IXH$HEAD_ITEM                  NUMBER        :=3;
   IXH$ITEM_XFORM_TYPE            NUMBER        :=4;
   IXH$ITEM_XFORM_DESC            NUMBER        :=5;
   IXH$PRODUCTION_LOSS_PCT        NUMBER        :=6;
   IXH$COMMENTS_DESC              NUMBER        :=7;
   IXHTL_sheet                    VARCHAR2(255) := 'ITEM_XFORM_HEAD_TL';
   IXHTL$Action                   NUMBER        :=1;
   IXHTL$LANG                     NUMBER        :=2;
   IXHTL$HEAD_ITEM                NUMBER        :=3;
   IXHTL$ITEM_XFORM_HEAD_ID       NUMBER        :=4;
   IXHTL$ITEM_XFORM_DESC          NUMBER        :=5;
   PI_sheet                       VARCHAR2(255) := 'PACKITEM';
   PI$Action                      NUMBER        :=1;
   PI$PACK_NO                     NUMBER        :=2;
   PI$SEQ_NO                      NUMBER        :=3;
   PI$ITEM                        NUMBER        :=4;
   PI$ITEM_PARENT                 NUMBER        :=5;
   PI$PACK_TMPL_ID                NUMBER        :=6;
   PI$PACK_QTY                    NUMBER        :=7;
   IZP_sheet                      VARCHAR2(255) := 'RPM_ITEM_ZONE_PRICE';
   IZP$Action                     NUMBER        :=1;
   IZP$ITEM_ZONE_PRICE_ID         NUMBER        :=2;
   IZP$ITEM                       NUMBER        :=3;
   IZP$ZONE_ID                    NUMBER        :=4;
   IZP$STANDARD_RETAIL            NUMBER        :=5;
   IZP$STANDARD_RETAIL_CURRENCY   NUMBER        :=6;
   IZP$STANDARD_UOM               NUMBER        :=7;
   IZP$SELLING_RETAIL             NUMBER        :=8;
   IZP$SELLING_RETAIL_CURRENCY    NUMBER        :=9;
   IZP$SELLING_UOM                NUMBER        :=10;
   IZP$MULTI_UNITS                NUMBER        :=11;
   IZP$MULTI_UNIT_RETAIL          NUMBER        :=12;
   IZP$MULTI_UNIT_RETAIL_CURRENCY NUMBER        :=13;
   IZP$MULTI_SELLING_UOM          NUMBER        :=14;
   IZP$LOCK_VERSION               NUMBER        :=15;
   UID_sheet                      VARCHAR2(255) := 'UDA_ITEM_DATE';
   UID$Action                     NUMBER        :=1;
   UID$ITEM                       NUMBER        :=2;
   UID$UDA_ID                     NUMBER        :=3;
   UID$UDA_DATE                   NUMBER        :=4;
   UIF_sheet                      VARCHAR2(255) := 'UDA_ITEM_FF';
   UIF$Action                     NUMBER        :=1;
   UIF$ITEM                       NUMBER        :=2;
   UIF$UDA_ID                     NUMBER        :=3;
   UIF$UDA_TEXT                   NUMBER        :=4;
   UIL_sheet                      VARCHAR2(255) := 'UDA_ITEM_LOV';
   UIL$Action                     NUMBER        :=1;
   UIL$ITEM                       NUMBER        :=2;
   UIL$UDA_ID                     NUMBER        :=3;
   UIL$UDA_VALUE                  NUMBER        :=4;
   VI_sheet                       VARCHAR2(255) := 'VAT_ITEM';
   VI$Action                      NUMBER        :=1;
   VI$ITEM                        NUMBER        :=2;
   VI$VAT_REGION                  NUMBER        :=3;
   VI$ACTIVE_DATE                 NUMBER        :=4;
   VI$VAT_TYPE                    NUMBER        :=5;
   VI$VAT_CODE                    NUMBER        :=6;
   VI$VAT_RATE                    NUMBER        :=7;
   VI$REVERSE_VAT_IND             NUMBER        :=8;
   CSH_sheet                      VARCHAR2(255) := 'COST_SUSP_SUP_HEAD';
   CSH$Action                     NUMBER        :=1;
   CSH$APPROVAL_ID                NUMBER        :=11;
   CSH$APPROVAL_DATE              NUMBER        :=10;
   CSH$COST_CHANGE_ORIGIN         NUMBER        :=7;
   CSH$STATUS                     NUMBER        :=6;
   CSH$ACTIVE_DATE                NUMBER        :=5;
   CSH$REASON                     NUMBER        :=4;
   CSH$COST_CHANGE_DESC           NUMBER        :=3;
   CSH$COST_CHANGE                NUMBER        :=2;
   CSH_CFA_sheet                   VARCHAR2(255) := 'COST_SUSP_SUP_HEAD_CFA_EXT';
   CSH_CFA$Action                  NUMBER        :=1;
   CSH_CFA$COST_CHANGE             NUMBER        :=2;
   CSH_CFA$GROUP_ID                NUMBER        :=3;
   CSH_CFA$VARCHAR2_1              NUMBER        :=4;
   CSH_CFA$VARCHAR2_2              NUMBER        :=5;
   CSH_CFA$VARCHAR2_3              NUMBER        :=6;
   CSH_CFA$VARCHAR2_4              NUMBER        :=7;
   CSH_CFA$VARCHAR2_5              NUMBER        :=8;
   CSH_CFA$VARCHAR2_6              NUMBER        :=9;
   CSH_CFA$VARCHAR2_7              NUMBER        :=10;
   CSH_CFA$VARCHAR2_8              NUMBER        :=11;
   CSH_CFA$VARCHAR2_9              NUMBER        :=12;
   CSH_CFA$VARCHAR2_10             NUMBER        :=13;
   CSH_CFA$NUMBER_11               NUMBER        :=14;
   CSH_CFA$NUMBER_12               NUMBER        :=15;
   CSH_CFA$NUMBER_13               NUMBER        :=16;
   CSH_CFA$NUMBER_14               NUMBER        :=17;
   CSH_CFA$NUMBER_15               NUMBER        :=18;
   CSH_CFA$NUMBER_16               NUMBER        :=19;
   CSH_CFA$NUMBER_17               NUMBER        :=20;
   CSH_CFA$NUMBER_18               NUMBER        :=21;
   CSH_CFA$NUMBER_19               NUMBER        :=22;
   CSH_CFA$NUMBER_20               NUMBER        :=23;
   CSH_CFA$DATE_21                 NUMBER        :=24;
   CSH_CFA$DATE_22                 NUMBER        :=25;
   CSH_CFA$DATE_23                 NUMBER        :=26;
   CSH_CFA$DATE_24                 NUMBER        :=27;
   CSH_CFA$DATE_25                 NUMBER        :=28;
   CSDL_sheet                     VARCHAR2(255) := 'COST_SUSP_SUP_DETAIL_LOC';
   CSDL$Action                    NUMBER        :=1;
   CSDL$BRACKET_VALUE1            NUMBER        :=8;
   CSDL$LOC                       NUMBER        :=7;
   CSDL$LOC_TYPE                  NUMBER        :=6;
   CSDL$ITEM                      NUMBER        :=5;
   CSDL$ORIGIN_COUNTRY_ID         NUMBER        :=4;
   CSDL$SUPPLIER                  NUMBER        :=3;
   CSDL$COST_CHANGE               NUMBER        :=2;
   CSDL$DELIVERY_COUNTRY_ID       NUMBER        :=18;
   CSDL$SUP_DEPT_SEQ_NO           NUMBER        :=17;
   CSDL$DEPT                      NUMBER        :=16;
   CSDL$DEFAULT_BRACKET_IND       NUMBER        :=15;
   CSDL$RECALC_ORD_IND            NUMBER        :=14;
   CSDL$COST_CHANGE_VALUE         NUMBER        :=13;
   CSDL$COST_CHANGE_TYPE          NUMBER        :=12;
   CSDL$UNIT_COST                 NUMBER        :=11;
   CSDL$BRACKET_VALUE2            NUMBER        :=10;
   CSDL$BRACKET_UOM1              NUMBER        :=9;
   CSD_sheet                      VARCHAR2(255) := 'COST_SUSP_SUP_DETAIL';
   CSD$Action                     NUMBER        :=1;
   CSD$DELIVERY_COUNTRY_ID        NUMBER        :=16;
   CSD$SUP_DEPT_SEQ_NO            NUMBER        :=15;
   CSD$DEPT                       NUMBER        :=14;
   CSD$DEFAULT_BRACKET_IND        NUMBER        :=13;
   CSD$RECALC_ORD_IND             NUMBER        :=12;
   CSD$COST_CHANGE_VALUE          NUMBER        :=11;
   CSD$COST_CHANGE_TYPE           NUMBER        :=10;
   CSD$UNIT_COST                  NUMBER        :=9;
   CSD$BRACKET_VALUE2             NUMBER        :=8;
   CSD$BRACKET_UOM1               NUMBER        :=7;
   CSD$BRACKET_VALUE1             NUMBER        :=6;
   CSD$ITEM                       NUMBER        :=5;
   CSD$ORIGIN_COUNTRY_ID          NUMBER        :=4;
   CSD$SUPPLIER                   NUMBER        :=3;
   CSD$COST_CHANGE                NUMBER        :=2;
   IIM_sheet                      VARCHAR2(255) := 'ITEM_IMAGE';
   IIM$Action                     NUMBER        :=1;
   IIM$ITEM                       NUMBER        :=2;
   IIM$IMAGE_NAME                 NUMBER        :=3;
   IIM$IMAGE_ADDR                 NUMBER        :=4;
   IIM$IMAGE_DESC                 NUMBER        :=5;
   IIM$IMAGE_TYPE                 NUMBER        :=6;
   IIM$PRIMARY_IND                NUMBER        :=7;
   IIM$DISPLAY_PRIORITY           NUMBER        :=8;
   IIMTL_sheet                    VARCHAR2(255) := 'ITEM_IMAGE_TL';
   IIMTL$Action                   NUMBER        :=1;
   IIMTL$LANG                     NUMBER        :=2;
   IIMTL$ITEM                     NUMBER        :=3;
   IIMTL$IMAGE_NAME               NUMBER        :=4;
   IIMTL$IMAGE_DESC               NUMBER        :=5;
   ISE_sheet                      VARCHAR2(255) := 'ITEM_SEASONS';
   ISE$Action                     NUMBER        :=1;
   ISE$ITEM                       NUMBER        :=2;
   ISE$SEASON_ID                  NUMBER        :=3;
   ISE$PHASE_ID                   NUMBER        :=4;
   ISE$ITEM_SEASON_SEQ_NO         NUMBER        :=5;
   ISE$DIFF_ID                    NUMBER        :=6;

Type name_trans
IS
  TABLE OF VARCHAR2(255) INDEX BY VARCHAR2(255);
  sheet_name_trans s9t_pkg.trans_map_typ;
  FUNCTION create_s9t(
      O_error_message     IN OUT rtk_errors.rtk_text%type,
      O_file_id           IN OUT s9t_folder.file_id%type,
      I_process_id        IN NUMBER,
      I_template_only_ind IN CHAR DEFAULT 'N')
    RETURN BOOLEAN;
  FUNCTION get_new_action(
      I_old_action IN VARCHAR2,
      I_new_action IN VARCHAR2)
    RETURN VARCHAR2;
  FUNCTION get_prc_dest(
      I_process_id svc_process_tracker.process_id%type)
    RETURN VARCHAR2;
  FUNCTION process_s9t(
      O_error_message IN OUT rtk_errors.rtk_text%type ,
      I_file_id       IN s9t_folder.file_id%type,
      I_process_id    IN NUMBER,
      O_error_count OUT NUMBER)
    RETURN BOOLEAN;
  FUNCTION create_svc_s9t(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      O_file_id       IN OUT s9t_folder.file_id%type,
      I_process_id    IN NUMBER)
    RETURN BOOLEAN;
  FUNCTION apply_tmpl_defaults(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_file_id       IN NUMBER,
      I_process_id    IN NUMBER)
    RETURN BOOLEAN;
  FUNCTION search_items(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_sch_crit_rec  IN item_induct_sch_crit_typ,
      I_source        IN VARCHAR2,
      I_process_id    IN NUMBER,
      O_item_count OUT NUMBER)
    RETURN BOOLEAN;
  FUNCTION init_process(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_process_id    IN svc_process_tracker.process_id%type,
      I_process_desc  IN svc_process_tracker.process_desc%type,
      I_template_key  IN svc_process_tracker.template_key%type,
      I_action_type   IN svc_process_tracker.action_type%type,
      I_source        IN svc_process_tracker.process_source%type,
      I_destination   IN svc_process_tracker.process_destination%type,
      I_rms_async_id  IN svc_process_tracker.rms_async_id%type,
      I_file_id       IN svc_process_tracker.file_id%type,
      I_file_path     IN svc_process_tracker.file_path%type)
    RETURN BOOLEAN;
  FUNCTION get_item_type(
      I_sellable_ind       IN VARCHAR2,
      I_orderable_ind      IN VARCHAR2,
      I_inventory_ind      IN VARCHAR2,
      I_simple_pack_ind    IN VARCHAR2,
      I_pack_ind           IN VARCHAR2,
      I_dept_purchase_type IN NUMBER,
      I_deposit_item_type  IN VARCHAR2,
      I_item_xform_ind     IN VARCHAR2 )
    RETURN VARCHAR2;
  FUNCTION create_file(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      O_file_id OUT svc_process_tracker.file_id%type,
      I_process_id    IN svc_process_tracker.process_id%type,
      I_action        IN VARCHAR2,
      I_source        IN VARCHAR2,
      I_template      IN VARCHAR2,
      I_file_path     IN VARCHAR2,
      I_err_4_process IN svc_process_tracker.process_id%type DEFAULT NULL)
    RETURN BOOLEAN;
  FUNCTION delete_search_items(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_process_id    IN NUMBER)
    RETURN BOOLEAN;
  FUNCTION populate_lists(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_file_id       IN NUMBER,
      I_template_key  IN VARCHAR2 DEFAULT NULL)
    RETURN BOOLEAN;
  FUNCTION get_process_desc(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_process_id    IN svc_process_tracker.process_id%type,
      O_process_desc OUT svc_process_tracker.process_desc%type,
      O_found OUT BOOLEAN)
    RETURN BOOLEAN;
  FUNCTION delete_processes(
      O_error_message  IN OUT rtk_errors.rtk_text%type,
      I_process_id_tab IN num_tab)
    RETURN BOOLEAN;
  FUNCTION EXEC_ASYNC(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_rms_async_id  IN rms_async_status.rms_async_id%type)
    RETURN BOOLEAN;
  FUNCTION get_config(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      O_rec OUT item_induct_config%rowtype)
    RETURN BOOLEAN;
  FUNCTION add_error_sheet(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_file_id       IN s9t_folder.file_id%type,
      I_process_id    IN svc_process_tracker.process_id%type)
    RETURN BOOLEAN;
  FUNCTION process_items(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_process_id    IN svc_process_tracker.process_id%type)
    RETURN BOOLEAN;
  FUNCTION chunk_data(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_process_id    IN svc_process_tracker.process_id%type)
    RETURN BOOLEAN;
  FUNCTION get_error_display(
      I_key   IN VARCHAR2,
      I_txt_1 IN VARCHAR2 DEFAULT NULL,
      I_txt_2 IN VARCHAR2 DEFAULT NULL,
      I_txt_3 IN VARCHAR2 DEFAULT NULL)
    RETURN VARCHAR2;
  FUNCTION NEXT_UPDATE_ID_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists           IN OUT   BOOLEAN,
                                 I_next_update_id   IN       SVC_ITEM_MASTER.NEXT_UPD_ID%TYPE)
    RETURN BOOLEAN;
  FUNCTION LAST_UPDATE_ID_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists           IN OUT   BOOLEAN,
                                 I_last_update_id   IN       SVC_ITEM_MASTER.LAST_UPD_ID%TYPE)
    RETURN BOOLEAN;
  FUNCTION UPDATE_SVC_PROCESS_TRACKER(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_process_id       IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                                      I_status           IN       SVC_PROCESS_TRACKER.STATUS%TYPE,
                                      I_process_src      IN       SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
                                      I_process_dest     IN       SVC_PROCESS_TRACKER.PROCESS_DESTINATION%TYPE,
                                      I_action_date      IN       SVC_PROCESS_TRACKER.ACTION_DATE%TYPE,
                                      I_rms_async_id     IN       SVC_PROCESS_TRACKER.RMS_ASYNC_ID%TYPE)
    RETURN BOOLEAN;
  FUNCTION UPDATE_SVC_PROCESS_TRACKER(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_process_id       IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                                      I_status           IN       SVC_PROCESS_TRACKER.STATUS%TYPE,
                                      I_process_src      IN       SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
                                      I_process_dest     IN       SVC_PROCESS_TRACKER.PROCESS_DESTINATION%TYPE,
                                      I_action_date      IN       SVC_PROCESS_TRACKER.ACTION_DATE%TYPE,
                                      I_rms_async_id     IN       SVC_PROCESS_TRACKER.RMS_ASYNC_ID%TYPE,
                                      I_new_process_id   IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
    RETURN BOOLEAN;
  FUNCTION VALIDATE_COST_CHANGE(
      O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      O_COST_CHANGE_DESC IN OUT COST_SUSP_SUP_HEAD.COST_CHANGE_DESC%TYPE,
      I_source           IN SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
      I_COST_CHANGE      IN COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
    RETURN BOOLEAN;
  FUNCTION IS_COST_CHANGE_TEMPLATE(
      I_template IN S9T_TEMPLATE.TEMPLATE_KEY%TYPE)
    RETURN BOOLEAN;
  FUNCTION SEARCH_COST_CHANGES(
      O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      O_count                 IN OUT NUMBER,
      I_sch_crit_rec IN cost_chg_sch_crit_typ,
      I_source                IN SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
      I_process_id            IN SVC_PROCESS_TRACKER.PROCESS_ID%TYPE )
    RETURN BOOLEAN;
  FUNCTION delete_search_ccs(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_process_id    IN NUMBER)
    RETURN BOOLEAN;
  FUNCTION process_cost_changes(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_process_id    IN svc_process_tracker.process_id%type)
    RETURN BOOLEAN;
  FUNCTION GET_ITEM_COL(I_table IN VARCHAR2)
    RETURN VARCHAR2;
    FUNCTION get_sheet_name_trans(
    I_sheet_name IN VARCHAR)
  RETURN VARCHAR2;
  FUNCTION SEARCH_COST_CHANGES_WRP(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_count	    IN OUT NUMBER,
    I_sch_crit_rec  IN cost_chg_sch_crit_typ,
    I_source	    IN SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
    I_process_id    IN SVC_PROCESS_TRACKER.PROCESS_ID%TYPE )
  RETURN NUMBER;
  FUNCTION SEARCH_ITEMS_WRP(
      O_error_message IN OUT rtk_errors.rtk_text%type,
      I_sch_crit_rec  IN item_induct_sch_crit_typ,
      I_source        IN VARCHAR2,
      I_process_id    IN NUMBER,
      O_item_count OUT NUMBER)
    RETURN NUMBER;
  FUNCTION DELETE_PROCESSES_WRP(
      O_error_message  IN OUT rtk_errors.rtk_text%type,
      I_process_id_tab IN num_tab)
    RETURN NUMBER;
END item_induct_sql;
/
