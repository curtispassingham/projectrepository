create or replace package XXADEO_GLOBAL_VARS_SQL is

--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/*               Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package with global variables for ADEO                       */
/******************************************************************************/
--------------------------------------------------------------------------------
  GP_upload_create                VARCHAR2(6)    := 'NEW';
  GP_upload_update                VARCHAR2(6)    := 'MOD';
  --
  -- Validations
  VAL$VAT_DEPT_REG_TYPE           VARCHAR2(255)  := 'VAT_DEPT_REG_TYPE';
  VAL$INV_VAT_REGION              VARCHAR2(255)  := 'INV_VAT_REGION';
  VAL$VAT_NO_ACTIVE_RATE          VARCHAR2(255)  := 'VAT_NO_ACTIVE_RATE';
  VAL$INV_VAT_TYPE                VARCHAR2(255)  := 'INV_VAT_TYPE';
  VAL$INV_VAT_CODE                VARCHAR2(255)  := 'INV_VAT_CODE';
  VAL$FIELD_TOO_LONG              VARCHAR2(255)  := 'FIELD_TOO_LONG';
  VAL$LANG_EXIST                  VARCHAR2(255)  := 'LANG_EXIST';
  VAL$ITEM_STATUS                 VARCHAR2(255)  := 'ITEM_STATUS';
  VAL$INV_STORE                   VARCHAR2(255)  := 'INV_STORE';
  VAL$INV_ZONE                    VARCHAR2(255)  := 'INV_ZONE';
  VAL$INV_REASON_CODE             VARCHAR2(255)  := 'INV_REASON_CODE';
  VAL$INV_ITEM                    VARCHAR2(255)  := 'INV_ITEM';
  VAL$INV_LANG                    VARCHAR2(255)  := 'INV_LANG';
  VAL$INV_DEPT                    VARCHAR2(255)  := 'INV_DEPT';
  VAL$INV_CLASS                   VARCHAR2(255)  := 'INV_CLASS';
  VAL$INV_SUBCLASS                VARCHAR2(255)  := 'INV_SUBCLASS';
  VAL$INV_DEFAULT                 VARCHAR2(255)  := 'INV_DEFAULT';
  VAL$XXADEO_INV_CLASS            VARCHAR2(255)  := 'XXADEO_INV_CLASS';
  VAL$INV_FILE_FORMAT             VARCHAR2(255)  := 'INV_FILE_FORMAT';
  VAL$INV_GROUP_ID                VARCHAR2(255)  := 'INV_GROUP_ID';
  VAL$INV_EFFECTIVE_DATE          VARCHAR2(255)  := 'INV_EFFECTIVE_DATE';
  VAL$S9T_INVALID_TEMPLATE        VARCHAR2(255)  := 'S9T_INVALID_TEMPLATE';
  VAL$EXCEEDS_4000_CHAR           VARCHAR2(255)  := 'EXCEEDS_4000_CHAR';
  VAL$DUP_REC_EXISTS_S9T          VARCHAR2(255)  := 'DUP_REC_EXISTS_S9T';
  VAL$COULD_NOT_INSERT_REC        VARCHAR2(255)  := 'COULD_NOT_INSERT_REC';
  VAL$COULD_NOT_UPDATE_REC        VARCHAR2(255)  := 'COULD_NOT_UPDATE_REC';
  VAL$INV_DOMAIN                  VARCHAR2(255)  := 'INV_DOMAIN';
  VAL$REC_NOT_EXISTS              VARCHAR2(255)  := 'XXADEO_REC_NOT_EXISTS';
  VAL$VAT_DEPT_EXISTS             VARCHAR2(255)  := 'XXADEO_VAT_DEPT_EXISTS';
  VAL$DEPT_TL_EXISTS              VARCHAR2(255)  := 'XXADEO_DEPT_TL_EXISTS';
  VAL$CLASS_TL_EXISTS             VARCHAR2(255)  := 'XXADEO_CLASS_TL_EXISTS';
  VAL$SUBCLASS_TL_EXISTS          VARCHAR2(255)  := 'XXADEO_SUBCLASS_TL_EXISTS';
  VAL$DOMAIN_EXISTS               VARCHAR2(255)  := 'DOMAIN_EXISTS';
  VAL$DOMAIN_MERCHHIER_EXISTS     VARCHAR2(255)  := 'DOMAIN_MERCHHIER_EXISTS';
  VAL$DOM_DEPT_EXISTS             VARCHAR2(255)  := 'DOMAIN_DEPT_EXISTS';
  VAL$DOM_CLASS_EXISTS            VARCHAR2(255)  := 'DOMAIN_CLASS_EXISTS';
  VAL$DOM_SCLASS_EXISTS           VARCHAR2(255)  := 'DOMAIN_SUBCLASS_EXISTS';
  VAL$ACT_CLASS_NOT_DEPT          VARCHAR2(255)  := 'ACT_CLASS_NOT_DEPT';
  VAL$ACT_SCLASS_NOT_CLASS        VARCHAR2(255)  := 'ACT_SCLASS_NOT_CLASS';
  VAL$DEACT_DEPT_ACT_CLASS        VARCHAR2(255)  := 'DEACT_DEPT_ACT_CLASS';
  VAL$DEACT_CLASS_ACT_SCLASS      VARCHAR2(255)  := 'DEACT_CLASS_ACT_SCLASS';
  VAL$DEACT_SCLASS_WITH_ITEM      VARCHAR2(255)  := 'DEACT_SCLASS_WITH_ITEM';
  VAL$AMOUNT_CANNOT_NEG           VARCHAR2(255)  := 'AMOUNT_CANNOT_NEG';
  VAL$PRICE_CHANGE_EVENT          VARCHAR2(255)  := 'PRICE_CHANGE_EVENT';
  --
  VAL$XXADEO_MAND_VALUE           VARCHAR2(255)  := 'XXADEO_MAND_VALUE';
  VAL$XXADEO_HAS_ROWS             VARCHAR2(255)  := 'XXADEO_HAS_ROWS';
  VAL$XXADEO_INV_DT_TEXT          VARCHAR2(255)  := 'XXADEO_INV_DT_TEXT';
  VAL$XXADEO_INV_DT_NUMBER        VARCHAR2(255)  := 'XXADEO_INV_DT_NUMBER';
  VAL$XXADEO_INV_ACTION_STATUS    VARCHAR2(255)  := 'XXADEO_INV_ACTION_STATUS';
  VAL$XXADEO_INV_ZONE_NODE_TYPE   VARCHAR2(255)  := 'XXADEO_INV_ZONE_NODE_TYPE';
  --
  -- RMS entity on which will be applied the CFA security.
  --
  CFA_SECURITY_ENTITY             VARCHAR2(10)   := 'CFAS';
  --
  -- RPM
  -- Record Status
  RS_NEW                          CONSTANT VARCHAR2(1) := 'N';
  RS_UPDATE                       CONSTANT VARCHAR2(1) := 'U';
  RS_DELETE                       CONSTANT VARCHAR2(1) := 'D';
  RS_ERROR                        CONSTANT VARCHAR2(1) := 'E';
  RS_PROCESSED                    CONSTANT VARCHAR2(1) := 'P';
  --

--------------------------------------------------------------------------------
end XXADEO_GLOBAL_VARS_SQL;
/
