CREATE OR REPLACE PACKAGE XXADEO_CREATE_COST_IL_SQL AS
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package "XXADEO_CREATE_COST_ITEM_LOC_SQL" for RB121          */
/******************************************************************************/
--------------------------------------------------------------------------------

ERROR              CONSTANT CORESVC_PO_ERR.ERROR_TYPE%TYPE := 'E';
WARNING            CONSTANT CORESVC_PO_ERR.ERROR_TYPE%TYPE := 'W';
PS_NEW             CONSTANT VARCHAR2(1) := 'N';
ACTION_NEW         CONSTANT VARCHAR2(25)  :=  'NEW';
ACTION_MOD         CONSTANT VARCHAR2(25)  :=  'MOD';
ACTION_DEL         CONSTANT VARCHAR2(25)  :=  'DEL';

--------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   LOGGER_LOGS.TEXT%TYPE,
                 I_process_id      IN       NUMBER,
                 I_template_key    IN       SVC_PROCESS_TRACKER.TEMPLATE_KEY%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END XXADEO_CREATE_COST_IL_SQL;
/
