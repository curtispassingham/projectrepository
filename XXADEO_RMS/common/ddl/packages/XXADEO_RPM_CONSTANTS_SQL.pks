CREATE OR REPLACE PACKAGE XXADEO_RPM_CONSTANTS_SQL is

--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - June 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/*               Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package with global variables for ADEO                       */
/******************************************************************************/
--------------------------------------------------------------------------------
  --
  -- RTK_ERRORS for custom valitions of the RB34a
  --
  VAL$PC_ERROR_VALIDATION       VARCHAR2(250) := 'XXADEO_PC_ERR_VALIDATION';
  VAL$PC_NOT_FOUND              VARCHAR2(250) := 'XXADEO_PC_NOT_FOUND';
  VAL$PC_DUPLICATED             VARCHAR2(250) := 'XXADEO_PC_DUPLICATED';
  VAL$PC_STORE_AMOUNT_HIGH      VARCHAR2(250) := 'xxadeo_pc_st_amount_high';
  VAL$PC_STORE_PORTAL           VARCHAR2(250) := 'xxadeo_pc_sp_blocked';
  --
  ZONE_NODE_TYPE_GROUP_ZONE     CONSTANT NUMBER := 3;
  --
  STATUS_W       VARCHAR2(1)    := 'W'; -- To be Approved
  STATUS_A       VARCHAR2(1)    := 'A'; -- To be Disapproved
  STATUS_E       VARCHAR2(1)    := 'E'; -- Error
  STATUS_P       VARCHAR2(1)    := 'P'; -- Processed
  STATUS_R       VARCHAR2(1)    := 'R'; -- Rejected
  --
  ACTION_N       VARCHAR2(1)    := 'N'; -- New
  ACTION_U       VARCHAR2(1)    := 'U'; -- Update
  ACTION_D       VARCHAR2(1)    := 'D'; -- Delete
  ACTION_R       VARCHAR2(1)    := 'R'; -- Rejected
  --
  TYPE_PC        VARCHAR2(2)    := 'PC';
  --
  REASON_CODE_TYPE              VARCHAR2(4)  := 'RSCD';
  REASON_CODE_EM_ST_PORTAL      VARCHAR2(40) := 'EMSTPO';
  REASON_CODE_RPO               VARCHAR2(40) := 'RERPO';
  REASON_CODE_ST_PORTAL         VARCHAR2(40) := 'RESTPO';
  REASON_CODE_SIMTAR            VARCHAR2(40) := 'RESIMT';
  --
  CFA_RETAIL_PRICE_BLOCKING     VARCHAR2(30) := 'RTL_BLK_ITEM';
  --
--------------------------------------------------------------------------------
end XXADEO_RPM_CONSTANTS_SQL; 
/
