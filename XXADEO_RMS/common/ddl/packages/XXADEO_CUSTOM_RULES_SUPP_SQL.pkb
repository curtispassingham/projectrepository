CREATE OR REPLACE PACKAGE BODY XXADEO_CUSTOM_RULES_SUPP_SQL
AS
 
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package of Custom Rules Supplier for RB129                   */
/******************************************************************************/

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-07-24 - Jorge Agra - BUG#169 - RG5 Query for OU validation fixed      */
/*                           BUG#160 - RG1 will trigger for any status        */
/*                                     was hardcoded to only trigger on       */
/*                                     activate                               */
/* 2018-10-11 - Jorge Agra - Changes after RMS patch changing activation /    */
/*                           deactivation behaviour                           */
/* 2018-10-25 - Jorge Agra - Disable, in code, execution of rules RG6&RG7,    */
/*                           moving to package XXADEO_SUPP_CUST_RULES_CFA_SQL */              
/******************************************************************************/

--------------------------------------------------------------------------------
  LP_msg_rules1   RTK_ERRORS_TL.RTK_KEY%TYPE    := 'XXADEO_SUP_CHG_STATUS_RG1';
  LP_msg_rules2   RTK_ERRORS_TL.RTK_KEY%TYPE    := 'XXADEO_SUP_CHG_STATUS_RG2';
  LP_msg_rules3   RTK_ERRORS_TL.RTK_KEY%TYPE    := 'XXADEO_SUP_CHG_STATUS_RG3';
  LP_msg_rules4   RTK_ERRORS_TL.RTK_KEY%TYPE    := 'XXADEO_SUP_CHG_STATUS_RG4';
  LP_msg_rules5   RTK_ERRORS_TL.RTK_KEY%TYPE    := 'XXADEO_SUP_CHG_STATUS_RG5';
  --
  LP_module          ADDR.MODULE%TYPE           := 'SUPP';
  LP_addr_type_04    ADDR.ADDR_TYPE%TYPE        := '04';
  LP_addr_type_06    ADDR.ADDR_TYPE%TYPE        := '06';
  --
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Cannot create supplier parent in active status in RMS
--------------------------------------------------------------------------------
FUNCTION RUN_CUSTOM_RULES_SUP_RG1(O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_parameters     IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(64) := 'XXADEO_CUSTOM_RULES_SUPP_SQL.RUN_CUSTOM_RULES_SUP_RG1';
  --
  L_return         VARCHAR2(1)  := 'N';
  L_rtk_text       RTK_ERRORS_TL.RTK_TEXT%TYPE := NULL;
  --
  cursor C_chk_supplier_parent is
    select 'Y'
      from sups
     where supplier = I_parameters.supplier
       and supplier_parent is null;
  --
BEGIN
  --
  DBG_SQL.MSG(L_program,
              'Begin: ' || L_program || ', function_key=' || I_parameters.function_key);
  --
  if I_parameters.function_key = GP_SUPP_STATUS_DEACTIVATE then
    return TRUE;
  end if;
  ---
  -- check is a parent supplier
  ---
  L_return := 'N';
  ---
  open C_chk_supplier_parent;
  fetch C_chk_supplier_parent into L_return;
  ---
  if C_chk_supplier_parent%NOTFOUND then
    DBG_SQL.MSG(L_program, 'Supplier=' || I_parameters.supplier || ' => Not found');
  end if;
  ---
  close C_chk_supplier_parent;
  --
  if L_return = 'Y' then
    --
    L_rtk_text := XXADEO_GET_RTK_TEXT_TL(O_error_message,
                                         LP_msg_rules1);
    --
    if O_error_message is null then
      --
      O_error_message := L_rtk_text;
      --
    end if;
    --
    RETURN FALSE;
    --
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  WHEN OTHERS THEN
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
  --
END RUN_CUSTOM_RULES_SUP_RG1;
--------------------------------------------------------------------------------
FUNCTION RUN_CUSTOM_RULES_SUP_RG2(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_parameters      IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(64) := 'XXADEO_CUSTOM_RULES_SUPP_SQL.RUN_CUSTOM_RULES_SUP_RG2';
  --
  L_return         VARCHAR2(1)  := 'N';
  L_rtk_text       RTK_ERRORS_TL.RTK_TEXT%TYPE := NULL;
  --
  cursor C_chk_supplier is
    select 'Y'
      from sups
     where supplier = I_parameters.supplier
       and supplier_parent is null;
  --
BEGIN
  --
  DBG_SQL.MSG(L_program,
              'Begin: ' || L_program || ', function_key=' || I_parameters.function_key);
  --
  L_return := 'N';
  ---
  open C_chk_supplier;
  fetch C_chk_supplier into L_return;
  close C_chk_supplier;
  --
  if L_return = 'Y' then
    L_rtk_text := XXADEO_GET_RTK_TEXT_TL(O_error_message,
                                         LP_msg_rules2);
    --
    if O_error_message is null then
      O_error_message := L_rtk_text;
    end if;
    --
    RETURN FALSE;
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  WHEN OTHERS THEN
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
  --
END RUN_CUSTOM_RULES_SUP_RG2;

--------------------------------------------------------------------------------
FUNCTION RUN_CUSTOM_RULES_SUP_RG3(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_parameters      IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(64) := 'XXADEO_CUSTOM_RULES_SUPP_SQL.RUN_CUSTOM_RULES_SUP_RG3';
  --
  L_return         VARCHAR2(1)  := 'N';
  L_rtk_text       RTK_ERRORS_TL.RTK_TEXT%TYPE := NULL;
  --
  cursor C_chk_supplier is
    select 'Y'
      from sups s,
           addr a
     where s.supplier = I_parameters.supplier
       and to_char(s.supplier) = a.key_value_1
       and a.addr_type = LP_addr_type_06
       and a.module = LP_module
       and s.supplier_parent is not null;
  --
BEGIN
  --
  DBG_SQL.MSG(L_program,
              'Begin: ' || L_program || ', function_key=' || I_parameters.function_key);
  --
  L_return := 'N';
  --
  open C_chk_supplier;
  fetch C_chk_supplier into L_return;
  close C_chk_supplier;
  --
  if L_return = 'Y' then
    L_rtk_text := XXADEO_GET_RTK_TEXT_TL(O_error_message,
                                         LP_msg_rules3);
    --
    if O_error_message is null then
      O_error_message := L_rtk_text;
    end if;
    --
    RETURN FALSE;
  end if;
  --
  RETURN TRUE;  
  --
EXCEPTION
  --
  WHEN OTHERS THEN
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
  --
END RUN_CUSTOM_RULES_SUP_RG3;

--------------------------------------------------------------------------------
FUNCTION RUN_CUSTOM_RULES_SUP_RG4(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_parameters      IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(64) := 'XXADEO_CUSTOM_RULES_SUPP_SQL.RUN_CUSTOM_RULES_SUP_RG4';
  --
  L_return         VARCHAR2(1)  := 'N';
  L_rtk_text       RTK_ERRORS_TL.RTK_TEXT%TYPE := NULL;
  --
  cursor C_chk_supplier_pay is
    select 'Y'
      from sups s,
           addr a
     where s.supplier = I_parameters.supplier
       and to_char(s.supplier) = a.key_value_1
       and a.addr_type in (LP_addr_type_06) 
       and a.module = LP_module
       and s.supplier_parent is not null;
  ---
  cursor C_chk_supplier_ord is
    select 'Y'
      from sups s,
           addr a
     where s.supplier = I_parameters.supplier
       and to_char(s.supplier) = a.key_value_1
       and a.addr_type in (LP_addr_type_04) 
       and a.module = LP_module
       and s.supplier_parent is not null;
  --
  cursor C_chk_supplier_site is
    select 'Y'
      from sups s
     where s.supplier_parent is not null;
  --
BEGIN
  --
  DBG_SQL.MSG(L_program,
              'Begin: ' || L_program || ', function_key=' || I_parameters.function_key);
  ---
  if I_parameters.function_key <> GP_SUPP_STATUS_ACTIVATE then
    return false;
  end if;
  ---
  -- if is not a site, ignore
  ---
  L_return := 'N';
  ---
  open C_chk_supplier_site;
  fetch C_chk_supplier_site into L_return;
  close C_chk_supplier_site;
  ---
  if L_return <> 'Y' then
    return true;
  end if;
  ---
  -- if is a payment site, ignore (it should never happen as rule RG3 should fail!!!)
  ---
  L_return := 'N';
  ---
  open C_chk_supplier_pay;
  fetch C_chk_supplier_pay into L_return;
  close C_chk_supplier_pay;
  ---
  if L_return = 'Y' then
    return true;
  end if;
  ---
  -- if no addr 04, fail
  ---
  L_return := 'N';
  ---
  open C_chk_supplier_ord;
  fetch C_chk_supplier_ord into L_return;
  close C_chk_supplier_ord;
  ---
  if L_return <> 'Y' then
    
    L_rtk_text := XXADEO_GET_RTK_TEXT_TL(O_error_message,
                                         LP_msg_rules4);
    --
    if O_error_message is null then
      --
      O_error_message := L_rtk_text;
      --
    end if;
    --
    RETURN FALSE;
  end if;
  ---
  RETURN TRUE;
  --
EXCEPTION
  --
  WHEN OTHERS THEN
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
  --
END RUN_CUSTOM_RULES_SUP_RG4;

--------------------------------------------------------------------------------
FUNCTION RUN_CUSTOM_RULES_SUP_RG5(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_parameters      IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program          VARCHAR2(64) := 'XXADEO_CUSTOM_RULES_SUPP_SQL.RUN_CUSTOM_RULES_SUP_RG5';
  --
  L_return           VARCHAR2(1)  := 'N';
  L_org_unit_id      PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE;
  L_rtk_text         RTK_ERRORS_TL.RTK_TEXT%TYPE := NULL;
  --
  cursor C_chk_supplier is
    select 'Y'
      from sups s,
           addr a
     where s.supplier = I_parameters.supplier
       and to_char(s.supplier) = a.key_value_1
       and a.addr_type = LP_addr_type_04
       and a.module = LP_module
       and s.supplier_parent is not null;
  ---     
  cursor C_get_org_unit_id is
    select pou.org_unit_id
      from partner_org_unit pou
     where pou.partner = I_parameters.supplier;
  --
  cursor C_chk_payment_sites (I_org_unit_id partner_org_unit.org_unit_id%type) is
    select 'Y'
      from  sups              s_ord,
            sups              s_pay,
            addr              a,
            partner_org_unit  pou
     where s_ord.supplier           = I_parameters.supplier
       and s_pay.supplier           <> I_parameters.supplier
       and s_pay.supplier_parent    = s_ord.supplier_parent
       and s_pay.supplier_parent    is not null
       and s_ord.supplier_parent    is not null
       --
       and to_char(s_pay.supplier)  = a.key_value_1
       and a.addr_type              = LP_addr_type_06
       and a.module                 = LP_module
       --
       and pou.partner              = s_pay.supplier
       and pou.org_unit_id          = I_org_unit_id;
  --
BEGIN
  --
  DBG_SQL.MSG(L_program,
              'Begin: supp=' ||  to_char(I_parameters.supplier) || ', function_key=' || I_parameters.function_key);
 ---
  -- if not activating, ignore
  ---
  if I_parameters.function_key <> GP_SUPP_STATUS_ACTIVATE then
    return true;
  end if;
  ---
  -- only validate if is an order supplier addr 04
  ---
  L_return := 'N';
  --
  open C_chk_supplier;
  fetch C_chk_supplier into L_return;
  close C_chk_supplier;
  ---
  if L_return <> 'Y' then
    return true;
  end if;
  ---
  -- for each org unit of order site...
  ---
  open C_get_org_unit_id;
  loop
    fetch C_get_org_unit_id into L_org_unit_id;
    exit when C_get_org_unit_id%NOTFOUND;
    ---
    -- must exist a pay supplier site of the same parent supplier
    ---
    L_return := 'N';
    ---
    DBG_SQL.MSG(L_program, 'ORG UNIT=' || L_org_unit_id);
    ---
    open C_chk_payment_sites(L_org_unit_id);
    fetch C_chk_payment_sites into L_return;
    close C_chk_payment_sites;
    ---
    if L_return = 'N' then
      ---
      L_rtk_text := XXADEO_GET_RTK_TEXT_TL(O_error_message,
                                           LP_msg_rules5);
      --
      if O_error_message is null then
        O_error_message := L_rtk_text;
      end if;
      --
      close C_get_org_unit_id;
      RETURN FALSE;
    --  
    end if;
    ---
  --    
  end loop;
  close C_get_org_unit_id;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  WHEN OTHERS THEN
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    if C_chk_payment_sites%isopen then
      close C_chk_payment_sites;
    end if;
    
    if C_get_org_unit_id%isopen then
      close C_get_org_unit_id;
    end if;
    --
    RETURN FALSE;
  --
END RUN_CUSTOM_RULES_SUP_RG5;

--------------------------------------------------------------------------------
FUNCTION RUN_CUSTOM_RULES_SUPPLIER(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                                   I_parameters      IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(64) := 'XXADEO_CUSTOM_RULES_SUPP_SQL.RUN_CUSTOM_RULES_SUPPLIER';
  --
  L_execute_string VARCHAR2(2000);
  L_return         VARCHAR2(1);
  --
  cursor C_get_custom_rules is
    select decode(asr.package_name,
                  null,
                  asr.function_name,
                  asr.package_name||'.'||asr.function_name) function_name
      from xxadeo_system_rules asr
     where asr.active_ind = 'Y'
       and asr.func_area = 'RG'
       and asr.rule_id = I_parameters.function_key
    order by asr.call_seq_no asc;
  --
  cursor C_chk_supplier is
    select 'Y'
      from sups
     where supplier = I_parameters.supplier;
  --
  --
BEGIN
  --
  open C_chk_supplier;
  FETCH C_chk_supplier INTO L_return;
  CLOSE C_chk_supplier;
  --
  DBG_SQL.MSG(L_program,
              '1 Begin: ' || L_program || ', L_return= ' || L_return);
  --
  DBG_SQL.MSG(L_program,
              '1 Begin: ' || L_program || ', DOC_TYPE= ' || I_parameters.DOC_TYPE||', DOC_ID'||I_parameters.DOC_ID||
                ', SOURCE_ENTITY'||I_parameters.SOURCE_ENTITY||', SOURCE_TYPE'||I_parameters.SOURCE_TYPE||', SOURCE_ID'||I_parameters.SOURCE_ID||
                ', DEST_ENTITY'||I_parameters.DEST_ENTITY||', DEST_TYPE'||I_parameters.DEST_TYPE||', DEST_ID'||I_parameters.DEST_ID||', SUPPLIER'||I_parameters.SUPPLIER||
                ', PARTNER'||I_parameters.PARTNER||', PARTNER_TYPE'||I_parameters.PARTNER_TYPE);
  --
  for i in C_get_custom_rules loop
    --
    L_execute_string := 'begin if '||i.function_name
                        ||' (:O_error_message, :I_parameters) = FALSE then :L_return := ''N''; end if; end;';
    --
    execute immediate L_execute_string using IN OUT O_error_message,
                                             IN OUT I_parameters,
                                             OUT    L_return;
    --
    if L_return = 'N' then
      --
      RETURN FALSE;
      --
    end if;
    --
  end loop;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  WHEN OTHERS THEN
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
  --
END RUN_CUSTOM_RULES_SUPPLIER;

--------------------------------------------------------------------------------
begin
  ---
  declare
    O_tbl DBG_SQL.TBL_DBG_OBJ;
  begin
    ---
    DBG_SQL.INIT(O_tbl);
    ---
  exception when others
    then null;
  end;
  ---  
END XXADEO_CUSTOM_RULES_SUPP_SQL;
/
