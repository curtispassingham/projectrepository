CREATE OR REPLACE PACKAGE BODY XXADEO_MERCHHIER_ATTRIB_SQL AS    
----------------------------------------------------------------------------------------------

  TYPE errors_tab_typ     IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
  TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
  TYPE ROW_SEQ_TAB        IS TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;

  LP_errors_tab                      errors_tab_typ;
  LP_s9t_errors_tab                  s9t_errors_tab_typ;
  LP_bulk_fetch_limit                CONSTANT NUMBER(12) := 1000;

  LP_primary_lang                    LANG.LANG%TYPE;
  LP_user                            SVCPROV_CONTEXT.USER_NAME%TYPE   := GET_USER;
  LP_chunk_id                        SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE  := 1;
  LP_cfa_attrib_dept                 CFA_ATTRIB.ATTRIB_ID%TYPE;
  LP_cfa_attrib_class                CFA_ATTRIB.ATTRIB_ID%TYPE;
  LP_cfa_attrib_subclass             CFA_ATTRIB.ATTRIB_ID%TYPE;

  L_process_id                       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE;
  L_file_id                          SVC_PROCESS_TRACKER.FILE_ID%TYPE;
  L_date_format                      NLS_DATABASE_PARAMETERS.VALUE%TYPE;
  L_xxadeo_wksht_column_idx_tbl      XXADEO_WKSHT_COLUMN_IDX_TBL;

----------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(O_error_message IN OUT LOGGER_LOGS.TEXT%TYPE,
                          I_issues_rt     IN     S9T_ERRORS%ROWTYPE) IS
BEGIN
  --
  LP_s9t_errors_tab.EXTEND();
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_issues_rt.file_id;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := TEMPLATE_KEY;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_issues_rt.wksht_key;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_issues_rt.column_key;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_issues_rt.row_seq;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := I_issues_rt.error_key;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := LP_user;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := LP_user;
  LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
  --
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_txt_1         IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE DEFAULT NULL,
                      I_txt_2         IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE DEFAULT NULL,
                      I_txt_3         IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE DEFAULT NULL,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
  --
  Lp_errors_tab.extend();
  Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
  Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
  Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
  Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
  Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
  Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
  Lp_errors_tab(Lp_errors_tab.count()).error_msg   := SQL_LIB.GET_MESSAGE_TEXT(I_error_msg,
                                                                               I_txt_1,
                                                                               I_txt_2,
                                                                               I_txt_3);
  Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;
  --
END WRITE_ERROR;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_DEPT_STATUS(I_file_id   IN   NUMBER ) IS
BEGIN
  --
  insert into TABLE (select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id    = I_file_id
                        and ss.sheet_name = DEPARTMENT_STATUS_SHEET )
                select s9t_row(s9t_cells((case
                                           when dce.cfa_value is null then
                                             XXADEO_MERCHHIER_ATTRIB_SQL.action_new
                                           else
                                             XXADEO_MERCHHIER_ATTRIB_SQL.action_mod
                                           end),
                                          g.group_no,
                                          g.group_name,
                                          d.dept,
                                          nvl((select dtl.dept_name from deps_tl dtl where dtl.dept = d.dept and dtl.lang = get_user_lang),d.dept_name),
                                          dce.cfa_value),rownum+1)
                  from v_deps         d,
                       v_groups       g,
                       table(XXADEO_CFA_SQL.SEARCH_CFA_BY_ATTRIB(I_attrib_id           => LP_cfa_attrib_dept,
                                                                 I_xxadeo_cfa_key_val_tbl  => null)) dce
                 where d.group_no      = g.group_no
                   and d.dept          = dce.key_col_1(+)
                   and exists            (select 1
                                            from cfa_attrib cfa,
                                                 cfa_attrib_group cfag,
                                                 cfa_attrib_group_set cags
                                           where cfa.attrib_id     = LP_cfa_attrib_dept
                                             and cfa.group_id      = cfag.group_id
                                             and cfag.group_set_id = cags.group_set_id);
  --
END POPULATE_DEPT_STATUS;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_CLASS_STATUS(I_file_id   IN   NUMBER ) IS
BEGIN
  --
  insert into TABLE (select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id    = I_file_id
                        and ss.sheet_name = CLASS_STATUS_SHEET)
                select s9t_row(s9t_cells((case
                                           when cce.cfa_value is null then
                                             XXADEO_MERCHHIER_ATTRIB_SQL.action_new
                                           else
                                             XXADEO_MERCHHIER_ATTRIB_SQL.action_mod
                                           end),
                                          g.group_no,
                                          g.group_name,
                                          d.dept,
                                          nvl((select dtl.dept_name from deps_tl dtl where dtl.dept = d.dept and dtl.lang = get_user_lang),d.dept_name),
                                          c.class,
                                          nvl((select ctl.class_name from class_tl ctl where ctl.class = c.class and ctl.dept = c.dept and ctl.lang = get_user_lang), c.class_name),
                                          cce.cfa_value),rownum+1)
                  from v_deps          d,
                       v_groups        g,
                       v_class         c,
                       table(XXADEO_CFA_SQL.SEARCH_CFA_BY_ATTRIB(I_attrib_id               => LP_cfa_attrib_class,
                                                                 I_xxadeo_cfa_key_val_tbl  => null)) cce
                 where d.group_no      = g.group_no
                   and d.dept          = c.dept
                   and c.dept          = cce.key_col_1(+)
                   and c.class         = cce.key_col_2(+)
                   and exists            (select 1
                                            from cfa_attrib cfa,
                                                 cfa_attrib_group cfag,
                                                 cfa_attrib_group_set cags
                                           where cfa.attrib_id     = LP_cfa_attrib_class
                                             and cfa.group_id      = cfag.group_id
                                             and cfag.group_set_id = cags.group_set_id);
  --
END POPULATE_CLASS_STATUS;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SUBCLASS_STATUS(I_file_id   IN   NUMBER ) IS
BEGIN
  --
  insert into TABLE (select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id    = I_file_id
                        and ss.sheet_name = SCLASS_STATUS_SHEET)
                select s9t_row(s9t_cells((case
                                           when sce.cfa_value is null then
                                             XXADEO_MERCHHIER_ATTRIB_SQL.action_new
                                           else
                                             XXADEO_MERCHHIER_ATTRIB_SQL.action_mod
                                           end),
                                          g.group_no,
                                          g.group_name,
                                          d.dept,
                                          nvl((select dtl.dept_name from deps_tl dtl where dtl.dept = d.dept and dtl.lang = get_user_lang),d.dept_name),
                                          c.class,
                                          nvl((select ctl.class_name from class_tl ctl where ctl.class = c.class and ctl.dept = c.dept and ctl.lang = get_user_lang), c.class_name),
                                          s.subclass,
                                          nvl((select stl.sub_name from subclass_tl stl where stl.subclass = s.subclass and stl.class = s.class and stl.dept = s.dept and stl.lang = get_user_lang), s.sub_name),
                                          sce.cfa_value),rownum+1)
                  from v_deps             d,
                       v_groups           g,
                       v_class            c,
                       v_subclass         s,
                       table(XXADEO_CFA_SQL.SEARCH_CFA_BY_ATTRIB(I_attrib_id               => LP_cfa_attrib_subclass,
                                                                 I_xxadeo_cfa_key_val_tbl  => null)) sce
                 where d.group_no      = g.group_no
                   and d.dept          = c.dept
                   and c.dept          = s.dept
                   and c.class         = s.class
                   and s.dept          = sce.key_col_1(+)
                   and s.class         = sce.key_col_2(+)
                   and s.subclass      = sce.key_col_3(+)
                   and exists            (select 1
                                            from cfa_attrib cfa,
                                                 cfa_attrib_group cfag,
                                                 cfa_attrib_group_set cags
                                           where cfa.attrib_id     = LP_cfa_attrib_subclass
                                             and cfa.group_id      = cfag.group_id
                                             and cfag.group_set_id = cags.group_set_id);
  --
END POPULATE_SUBCLASS_STATUS;
----------------------------------------------------------------------------------------------
FUNCTION SETUP_DEPT_STATUS(O_error_message IN OUT  LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_program                VARCHAR2(64)       := 'XXADEO_MERCHHIER_SQL.SETUP_DEPT_STATUS';
  --
  L_cfa_attrib_tbl          XXADEO_CFA_ATTRIB_TBL  := XXADEO_CFA_ATTRIB_TBL();
  L_xxadeo_cfa_key_val_tbl  XXADEO_CFA_KEY_VAL_TBL := XXADEO_CFA_KEY_VAL_TBL();
  --
  cursor C_get_cfa_attrib is
    select value_1 attrib_id
      from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                    I_parameter => DEPARTMENT_STATUS_SHEET));
  --
  cursor C_get_stg_dept_status is
    select new XXADEO_CFA_KEY_VAL_OBJ(key_col_1            => 'DEPT',
                                      key_value_1          => dept,
                                      attrib_varchar_value => dept_status)
      from xxadeo_dept_status_stg dt
     where process_id   = L_process_id
       and action      in (GP_upload_create,GP_upload_update);
  --
BEGIN
  --
  SAVEPOINT INIT_CREATE_DEPT_STATUS;
  --
  -- create department status
  --
  BEGIN
    --
    --
    LP_cfa_attrib_dept   := NULL;
    --
    open C_get_cfa_attrib;
    fetch C_get_cfa_attrib into LP_cfa_attrib_dept;
    close C_get_cfa_attrib;
    --
    open C_get_stg_dept_status;
    fetch C_get_stg_dept_status bulk collect into L_xxadeo_cfa_key_val_tbl;
    --
      if L_xxadeo_cfa_key_val_tbl.count = 0 then
        --
        goto end_process_dept;
        --
      else
        --
        L_cfa_attrib_tbl.extend();
        L_cfa_attrib_tbl(L_cfa_attrib_tbl.count) := XXADEO_CFA_ATTRIB_OBJ(attrib_id            => LP_cfa_attrib_dept,
                                                                          cfa_key_val_tbl      => L_xxadeo_cfa_key_val_tbl);
        --
        if XXADEO_CFA_SQL.SET_CFA_BY_ATTRIB(O_error_message   => O_error_message,
                                            IO_cfa_attrib_tbl => L_cfa_attrib_tbl) = FALSE then
          --
          RETURN FALSE;
          --
        end if;
        --
      end if;
      --
    close C_get_stg_dept_status;
    --
  END;
  --
  <<end_process_dept>>
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    if C_get_stg_dept_status%ISOPEN then
      --
      close C_get_stg_dept_status;
      --
    end if;
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END SETUP_DEPT_STATUS;
----------------------------------------------------------------------------------------------
FUNCTION SETUP_CLASS_STATUS(O_error_message IN OUT  LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_program             VARCHAR2(64)       := 'XXADEO_MERCHHIER_SQL.SETUP_CLASS_STATUS';
  --
  L_cfa_attrib_tbl          XXADEO_CFA_ATTRIB_TBL  := XXADEO_CFA_ATTRIB_TBL();
  L_xxadeo_cfa_key_val_tbl  XXADEO_CFA_KEY_VAL_TBL := XXADEO_CFA_KEY_VAL_TBL();
  --
  cursor C_get_cfa_attrib is
    select value_1 attrib_id
      from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                    I_parameter => CLASS_STATUS_SHEET));
  --
  cursor C_get_stg_class_status is
    select new XXADEO_CFA_KEY_VAL_OBJ(key_col_1            => 'DEPT',
                                      key_col_2            => 'CLASS',
                                      key_value_1          => dept,
                                      key_value_2          => class,
                                      attrib_varchar_value => class_status)
      from xxadeo_class_status_stg dt
     where process_id   = L_process_id
       and action      in (GP_upload_create,GP_upload_update);
  --
BEGIN
  --
  SAVEPOINT INIT_CREATE_CLASS_STATUS;
  --
  -- create class status
  --
  BEGIN
    --
    LP_cfa_attrib_class  := NULL;
    --
    open C_get_cfa_attrib;
    fetch C_get_cfa_attrib into LP_cfa_attrib_class;
    close C_get_cfa_attrib;
    --
    open C_get_stg_class_status;
    fetch C_get_stg_class_status bulk collect into L_xxadeo_cfa_key_val_tbl;
      --
      if L_xxadeo_cfa_key_val_tbl.count = 0 then
        --
        goto end_process_class;
        --
      else
        --
        L_cfa_attrib_tbl.extend();
        L_cfa_attrib_tbl(L_cfa_attrib_tbl.count) := XXADEO_CFA_ATTRIB_OBJ(attrib_id            => LP_cfa_attrib_class,
                                                                          cfa_key_val_tbl      => L_xxadeo_cfa_key_val_tbl);
        --
        if XXADEO_CFA_SQL.SET_CFA_BY_ATTRIB(O_error_message  => O_error_message,
                                            IO_cfa_attrib_tbl => L_cfa_attrib_tbl) = FALSE then
          --
          RETURN FALSE;
          --
        end if;
        --
      end if;
      --
    close C_get_stg_class_status;
    --
  --
  END;
  --
  <<end_process_class>>
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    if C_get_stg_class_status%ISOPEN then
      --
      close C_get_stg_class_status;
      --
    end if;
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END SETUP_CLASS_STATUS;
----------------------------------------------------------------------------------------------
FUNCTION SETUP_SUBCLASS_STATUS(O_error_message IN OUT  LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_program             VARCHAR2(64) := 'XXADEO_MERCHHIER_SQL.SETUP_SUBCLASS_STATUS';
  --
  L_cfa_attrib_tbl          XXADEO_CFA_ATTRIB_TBL  := XXADEO_CFA_ATTRIB_TBL();
  L_xxadeo_cfa_key_val_tbl  XXADEO_CFA_KEY_VAL_TBL := XXADEO_CFA_KEY_VAL_TBL();
  --
  cursor C_get_cfa_attrib is
    select value_1 attrib_id
      from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                    I_parameter => SCLASS_STATUS_SHEET));
  --
  cursor C_get_stg_subclass_status is
    select new XXADEO_CFA_KEY_VAL_OBJ(key_col_1            => 'DEPT',
                                      key_col_2            => 'CLASS',
                                      key_col_3            => 'SUBCLASS',
                                      key_value_1          => dept,
                                      key_value_2          => class,
                                      key_value_3          => subclass,
                                      attrib_varchar_value => subclass_status)
      from xxadeo_subclass_status_stg dt
     where process_id       = L_process_id
       and action          in (GP_upload_create,GP_upload_update);
  --
BEGIN
  --
  SAVEPOINT INIT_CREATE_SUBCLASS_STATUS;
  --
  -- create subclass status
  --
  BEGIN
    --
    --
    LP_cfa_attrib_subclass := NULL;
    --
    open C_get_cfa_attrib;
    fetch C_get_cfa_attrib into LP_cfa_attrib_subclass;
    close C_get_cfa_attrib;
    --
    open C_get_stg_subclass_status;
    fetch C_get_stg_subclass_status bulk collect into L_xxadeo_cfa_key_val_tbl;
      --
      if L_xxadeo_cfa_key_val_tbl.count = 0 then
        --
        goto end_process_subclass;
        --
      else
        --
        L_cfa_attrib_tbl.extend();
        L_cfa_attrib_tbl(L_cfa_attrib_tbl.count) := XXADEO_CFA_ATTRIB_OBJ(attrib_id            => LP_cfa_attrib_subclass,
                                                                          cfa_key_val_tbl      => L_xxadeo_cfa_key_val_tbl);
        --
        if XXADEO_CFA_SQL.SET_CFA_BY_ATTRIB(O_error_message  => O_error_message,
                                            IO_cfa_attrib_tbl => L_cfa_attrib_tbl) = FALSE then
          --
          RETURN FALSE;
          --
        end if;
        --
      end if;
      --
    close C_get_stg_subclass_status;
    --
  END;
  --
  <<end_process_subclass>>
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    if C_get_stg_subclass_status%ISOPEN then
      --
      close C_get_stg_subclass_status;
      --
    end if;
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END SETUP_SUBCLASS_STATUS;
----------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(64) := 'XXADEO_MERCHHIER_ATTRIB_SQL.CREATE_S9T';
  --
  L_file           s9t_file;
  L_s9t_sheet_tab  S9T_SHEET_TAB;
  --
BEGIN
  --
  LP_primary_lang        := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
  L_s9t_sheet_tab        := S9T_SHEET_TAB();
  --
  -- get template sheets and columns
  --
  if XXADEO_CUSTOM_UPLOAD_SQL.GET_TEMPLATE_S9T_SHEETS(O_error_message   => O_error_message,
                                                      IO_s9t_sheet_tab  => L_s9t_sheet_tab,
                                                      I_template_key    => TEMPLATE_KEY) = FALSE then
    --
    RETURN FALSE;
    --
  end if;
  --
  XXADEO_CUSTOM_UPLOAD_SQL.INIT_S9T(O_error_message  => O_error_message,
                                    O_file_id        => O_file_id,
                                    I_template_key   => TEMPLATE_KEY,
                                    I_s9t_sheet_tab  => L_s9t_sheet_tab);
  --
  if S9T_PKG.POPULATE_LISTS(O_error_message,
                            O_file_id,
                            template_category,
                            template_key) = FALSE   then
     RETURN FALSE;
  end if;
  --
  if I_template_only_ind = 'N' then
    --
    LP_cfa_attrib_dept     := NULL;
    LP_cfa_attrib_class    := NULL;
    LP_cfa_attrib_subclass := NULL;
    --
    select value_1 attrib_id
      into LP_cfa_attrib_dept
      from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                    I_parameter => DEPARTMENT_STATUS_SHEET));
    --
    POPULATE_DEPT_STATUS(O_file_id);
    --
    select value_1 attrib_id
      into LP_cfa_attrib_class
      from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                    I_parameter => CLASS_STATUS_SHEET));
    --
    POPULATE_CLASS_STATUS(O_file_id);
    --
    select value_1 attrib_id
      into LP_cfa_attrib_subclass
      from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                    I_parameter => SCLASS_STATUS_SHEET));
    --
    POPULATE_SUBCLASS_STATUS(O_file_id);
    COMMIT;
    --
  end if;

  S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
  S9T_PKG.APPLY_TEMPLATE(O_file_id,
                         template_key);
  L_file := S9T_FILE(O_file_id);

  if S9T_PKG.CODE2DESC(O_error_message,
                       template_category,
                       L_file) = FALSE then
    --
    RETURN FALSE;
    --
  end if;
  --
  S9T_PKG.SAVE_OBJ(L_file);
  S9T_PKG.UPDATE_ODS(L_file);
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END CREATE_S9T;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DEPT_STATUS (O_error_message             IN OUT LOGGER_LOGS.TEXT%TYPE,
                               IO_error                    IN OUT BOOLEAN,
                               I_xxadeo_dept_status_stg    IN     XXADEO_DEPT_STATUS_STG%ROWTYPE,
                               I_row_seq                   IN     SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE)
RETURN BOOLEAN IS
  --
  L_error     BOOLEAN := IO_error;
  --
BEGIN
  --
  if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER(O_error_message,
                                                        I_xxadeo_dept_status_stg.dept) then
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                DEPARTMENT_STATUS_SHEET,
                I_row_seq,
                DEPARTMENT_STATUS$DEPT,
                XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEPT);
    --
    L_error := TRUE;
    --
  end if;
  --
  If not IO_ERROR then
    IO_ERROR := L_error;
  end if;
  --
  RETURN TRUE;
  --
END VALIDATE_DEPT_STATUS;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CLASS_STATUS (O_error_message             IN OUT LOGGER_LOGS.TEXT%TYPE,
                                IO_error                    IN OUT BOOLEAN,
                                I_xxadeo_class_status_stg   IN     XXADEO_CLASS_STATUS_STG%ROWTYPE,
                                I_row_seq                   IN     SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE)
RETURN BOOLEAN IS
  --
  L_error     BOOLEAN := IO_error;
  --
BEGIN
  --
  if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER(O_error_message,
                                                        I_xxadeo_class_status_stg.dept,
                                                        I_xxadeo_class_status_stg.class) then
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                CLASS_STATUS_SHEET,
                I_row_seq,
                CLASS_STATUS$CLASS,
                XXADEO_GLOBAL_VARS_SQL.VAL$INV_CLASS);
    --
    L_error := TRUE;
    --
  end if;
  --
  If not IO_ERROR then
    IO_ERROR := L_error;
  end if;
  --
  RETURN TRUE;
  --
END VALIDATE_CLASS_STATUS;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_SUBCLASS_STATUS (O_error_message              IN OUT LOGGER_LOGS.TEXT%TYPE,
                                   IO_error                     IN OUT BOOLEAN,
                                   I_xxadeo_sclass_status_stg   IN     XXADEO_SUBCLASS_STATUS_STG%ROWTYPE,
                                   I_row_seq                    IN     SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE)
RETURN BOOLEAN IS
  --
  L_error     BOOLEAN := IO_error;
  --
BEGIN
  --
  if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER(O_error_message,
                                                        I_xxadeo_sclass_status_stg.dept,
                                                        I_xxadeo_sclass_status_stg.class,
                                                        I_xxadeo_sclass_status_stg.subclass) then
    WRITE_ERROR(L_process_id,
                svc_admin_upld_er_seq.nextval,
                LP_chunk_id,
                SCLASS_STATUS_SHEET,
                I_row_seq,
                SCLASS_STATUS$SUBCLASS,
                XXADEO_GLOBAL_VARS_SQL.VAL$INV_SUBCLASS);
    --
    L_error := TRUE;
    --
  end if;
  --
  If not IO_ERROR then
    IO_ERROR := L_error;
  end if;
  --
  RETURN TRUE;
  --
END VALIDATE_SUBCLASS_STATUS;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_DEPT_STATUS(O_error_message   IN OUT  LOGGER_LOGS.TEXT%type) IS
  --
  L_program                       VARCHAR2(64) := 'XXADEO_MERCHHIER_ATTRIB_SQL.PROCESS_S9T_DEPT_STATUS';
  --
  TYPE XXADEO_DEPT_STATUS_TYPE IS TABLE OF XXADEO_DEPT_STATUS_STG%ROWTYPE;
  L_xxadeo_dept_status_tbl     XXADEO_DEPT_STATUS_TYPE := XXADEO_DEPT_STATUS_TYPE();
  L_temp_rec                   XXADEO_DEPT_STATUS_STG%ROWTYPE;
  L_issues_rt                  S9T_ERRORS%ROWTYPE;
  --
  L_error                      BOOLEAN:=FALSE;
  L_default_rec                XXADEO_DEPT_STATUS_STG%ROWTYPE;
  L_row_seq                    SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE;
  L_error_code                 NUMBER;
  L_error_msg                  RTK_ERRORS.RTK_TEXT%TYPE;
  --
  DML_ERRORS    EXCEPTION;
  PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
  --
  cursor C_mandatory_ind is
    select group_id_mi,
           group_name_mi,
           dept_mi,
           dept_name_mi,
           dept_status_mi,
           1 as dummy
      from (select column_key,
                   required_visual_ind
              from s9t_tmpl_cols_def
             where template_key   = TEMPLATE_KEY
               and wksht_key      = 'DEPARTMENT_STATUS'
            ) pivot (max(required_visual_ind) as mi
                     for (column_key) in ('GROUP_ID'         as group_id,
                                          'GROUP_NAME'       as group_name,
                                          'DEPT'             as dept,
                                          'DEPT_NAME'        as dept_name,
                                          'DEPT_STATUS'      as dept_status,
                                           null as dummy));
  L_mi_rec C_mandatory_ind%ROWTYPE;
  --
  CURSOR C_get_date_format IS
    select value
      from nls_database_parameters
     where parameter = 'NLS_DATE_FORMAT';
  --
BEGIN
  --
  OPEN C_get_date_format;
  FETCH C_get_date_format INTO L_date_format;
  CLOSE C_get_date_format;
  --
  -- get default values
  --
  for rec in (select group_id_dv,
                     group_name_dv,
                     dept_dv,
                     dept_name_dv,
                     dept_status_dv,
                     null as dummy
               from (select column_key,
                            default_value
                       from s9t_tmpl_cols_def
                      where template_key  = TEMPLATE_KEY
                        and wksht_key     = 'DEPARTMENT_STATUS'
                     ) PIVOT (MAX(default_value) AS dv
                              FOR (column_key) IN ('GROUP_ID'         as group_id,
                                                   'GROUP_NAME'       as group_name,
                                                   'DEPT'             as dept,
                                                   'DEPT_NAME'        as dept_name,
                                                   'DEPT_STATUS'      as dept_status,
                                                   NULL AS dummy))) loop
    --
    -- group_id default
    --
    BEGIN
      --
      L_default_rec.group_id := rec.group_id_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_Id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_Key        := DEPARTMENT_STATUS$GROUP_ID;
        L_issues_rt.error_Key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- group_name default
    --
    BEGIN
      --
      L_default_rec.group_name := rec.group_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$GROUP_NAME;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT default
    --
    BEGIN
      --
      L_default_rec.dept := rec.dept_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key   := DEPARTMENT_STATUS$DEPT;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT_NAME default
    --
    BEGIN
      --
      L_default_rec.dept_name := rec.dept_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key   := DEPARTMENT_STATUS$DEPT_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Department Status default
    --
    BEGIN
      --
      L_default_rec.dept_status := rec.dept_status_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key   := DEPARTMENT_STATUS$DEPT_STATUS;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
  end loop;
  --
  -- get mandatory indicators
  --
  open C_mandatory_ind;
  fetch C_mandatory_ind into L_mi_rec;
  close C_mandatory_ind;
  --
  -- validate datatypes and business rules
  --
  for rec IN (select r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPARTMENT_STATUS_SHEET,
                                                                     I_column_key                  => DEPARTMENT_STATUS$ACTION))                as ACTION,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPARTMENT_STATUS_SHEET,
                                                                     I_column_key                  => DEPARTMENT_STATUS$GROUP_ID))              as GROUP_ID,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPARTMENT_STATUS_SHEET,
                                                                     I_column_key                  => DEPARTMENT_STATUS$GROUP_NAME))            as GROUP_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPARTMENT_STATUS_SHEET,
                                                                     I_column_key                  => DEPARTMENT_STATUS$DEPT))                  as DEPT,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPARTMENT_STATUS_SHEET,
                                                                     I_column_key                  => DEPARTMENT_STATUS$DEPT_NAME))             as DEPT_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => DEPARTMENT_STATUS_SHEET,
                                                                     I_column_key                  => DEPARTMENT_STATUS$DEPT_STATUS))           as DEPT_STATUS,
                     r.get_row_seq()                                                                                                            as ROW_SEQ
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = L_file_id
                 and ss.sheet_name = SHEET_NAME_TRANS(DEPARTMENT_STATUS_SHEET)) loop
    --
    L_temp_rec                      := null;
    L_temp_rec.process_id           := L_process_id;
    L_temp_rec.xxadeo_process_id    := xxadeo_upl_process_id_seq.nextval;
    L_temp_rec.create_id            := LP_user;
    L_temp_rec.last_update_id       := LP_user;
    L_temp_rec.create_datetime      := sysdate;
    L_temp_rec.last_update_datetime := sysdate;
    L_error                         := FALSE;
    L_row_seq                       := rec.row_seq;
    --
    LP_chunk_id                     := 1;
    --
    -- validate data types
    --
    if rec.dept_status is null then
      --
      continue;
      --
    end if;
    --
    -- action
    --
    BEGIN
      --
      if rec.action is null then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      else
        --
        if rec.action = GP_upload_create or
           rec.action = GP_upload_update then
          --
          L_temp_rec.action := rec.action;
          --
        else
        --
          L_issues_rt                   := null;
          L_issues_rt.file_id           := L_file_id;
          L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
          L_issues_rt.column_key        := DEPARTMENT_STATUS$ACTION;
          L_issues_rt.row_seq           := rec.row_seq;
          L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
          --
          WRITE_S9T_ERROR(O_error_message => O_error_message,
                          I_issues_rt     => L_issues_rt);
          --
          L_error := TRUE;
          --
        end if;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- Action and status
    --
    if rec.action = GP_upload_create and rec.dept_status = XXADEO_ATTR_INACTIVE then
      --
      L_issues_rt                   := null;
      L_issues_rt.file_id           := L_file_id;
      L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
      L_issues_rt.column_key        := DEPARTMENT_STATUS$DEPT_STATUS;
      L_issues_rt.row_seq           := rec.row_seq;
      L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_ACTION_STATUS;
      --
      WRITE_S9T_ERROR(O_error_message => O_error_message,
                      I_issues_rt     => L_issues_rt);
      --
    elsif rec.action = GP_upload_update and rec.dept_status = XXADEO_ATTR_IN_PREPARATION then
      --
      L_issues_rt                   := null;
      L_issues_rt.file_id           := L_file_id;
      L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
      L_issues_rt.column_key        := DEPARTMENT_STATUS$DEPT_STATUS;
      L_issues_rt.row_seq           := rec.row_seq;
      L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_ACTION_STATUS;
      --
      WRITE_S9T_ERROR(O_error_message => O_error_message,
                      I_issues_rt     => L_issues_rt);
      --
    end if;
    --
    -- group_id
    --
    BEGIN
      --
      if L_mi_rec.group_id_mi   = 'Y'   and
         L_default_rec.group_id is null and
         rec.group_id is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_id := rec.group_id;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- group_name
    --
    BEGIN
      --
      if L_mi_rec.group_name_mi   = 'Y'   and
         L_default_rec.group_name is null and
         rec.group_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_name := rec.group_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT
    --
    BEGIN
      --
      if L_mi_rec.dept_mi   = 'Y'   and
         L_default_rec.dept is null and
         rec.dept is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept := rec.dept;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT_NAME
    --
    BEGIN
      --
      if L_mi_rec.dept_name_mi   = 'Y'        and
         L_default_rec.dept_name is null and
         rec.dept_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept_name := rec.dept_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPARTMENT STATUS
    --
    BEGIN
      --
      if L_mi_rec.dept_status_mi   = 'Y'   and
         L_default_rec.dept_status is null and
         rec.dept_status is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$DEPT_STATUS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept_status := rec.dept_status;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.column_key        := DEPARTMENT_STATUS$DEPT_STATUS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- validate business rules
    --
    if not L_error then
      --
      --
      if not VALIDATE_DEPT_STATUS (O_error_message,
                                   L_error,
                                   L_temp_rec,
                                   L_row_seq) then
        --
        RETURN;
        --
      end if;
      --
    end if;
    --
    -- if line does not have error, insert in table to process
    -- 
    if NOT L_error then
      --
      L_temp_rec.row_seq   := rec.row_seq;
      --
      L_xxadeo_dept_status_tbl.extend();
      L_xxadeo_dept_status_tbl(L_xxadeo_dept_status_tbl.count()) := L_temp_rec;
      --  
    end if;
    --
  --
  end loop;
  --
  -- insert into stagging
  --
  BEGIN
    --
    forall i IN 1..L_xxadeo_dept_status_tbl.count SAVE EXCEPTIONS
      insert into xxadeo_dept_status_stg
        (process_id,
         xxadeo_process_id,
         action,
         group_id,
         group_name,
         dept,
         dept_name,
         dept_status,
         create_id,
         create_datetime,
         last_update_id,
         last_update_datetime,
         row_seq)
      values
        (L_xxadeo_dept_status_tbl(i).process_id,
         L_xxadeo_dept_status_tbl(i).xxadeo_process_id,
         L_xxadeo_dept_status_tbl(i).action,
         L_xxadeo_dept_status_tbl(i).group_id,
         L_xxadeo_dept_status_tbl(i).group_name,
         L_xxadeo_dept_status_tbl(i).dept,
         L_xxadeo_dept_status_tbl(i).dept_name,
         L_xxadeo_dept_status_tbl(i).dept_status,
         L_xxadeo_dept_status_tbl(i).create_id,
         L_xxadeo_dept_status_tbl(i).create_datetime,
         L_xxadeo_dept_status_tbl(i).last_update_id,
         L_xxadeo_dept_status_tbl(i).last_update_datetime,
         L_xxadeo_dept_status_tbl(i).row_seq);
    --
  EXCEPTION
    --
    when DML_ERRORS then
      --
      for i in 1..sql%bulk_exceptions.COUNT loop
        --
        L_error_code:=sql%bulk_exceptions(i).error_code;
        --
        if L_error_code=1 then
          --
          L_error_code:=NULL;
          L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T');
          --
        end if;
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := DEPARTMENT_STATUS_SHEET;
        L_issues_rt.row_seq           := sql%bulk_exceptions(i).error_index;
        L_issues_rt.error_key         := sqlerrm(-sql%bulk_exceptions(i).error_code);
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      end loop;
      --
    --
  END;
  --
END PROCESS_S9T_DEPT_STATUS;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CLASS_STATUS(O_error_message   IN OUT  LOGGER_LOGS.TEXT%TYPE)IS
  --
  L_program                       VARCHAR2(64) := 'XXADEO_MERCHHIER_ATTRIB_SQL.PROCESS_S9T_CLASS_STATUS';
  --
  TYPE XXADEO_CLASS_STATUS_TYPE IS TABLE OF XXADEO_CLASS_STATUS_STG%ROWTYPE;
  L_xxadeo_class_status_tbl     XXADEO_CLASS_STATUS_TYPE := XXADEO_CLASS_STATUS_TYPE();
  L_temp_rec                    XXADEO_CLASS_STATUS_STG%ROWTYPE;
  L_issues_rt                   S9T_ERRORS%ROWTYPE;
  --
  L_error                       BOOLEAN:=FALSE;
  L_default_rec                 XXADEO_CLASS_STATUS_STG%ROWTYPE;
  L_row_seq                     SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE;
  L_error_code                  NUMBER;
  L_error_msg                   RTK_ERRORS.RTK_TEXT%TYPE;
  --
  DML_ERRORS    EXCEPTION;
  PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
  --
  cursor C_mandatory_ind is
    select group_id_mi,
           group_name_mi,
           dept_mi,
           dept_name_mi,
           class_mi,
           class_name_mi,
           class_status_mi,
           1 as dummy
      from (select column_key,
                   required_visual_ind
              from s9t_tmpl_cols_def
             where template_key   = TEMPLATE_KEY
               and wksht_key      = CLASS_STATUS_SHEET
            ) pivot (max(required_visual_ind) as mi
                     for (column_key) in ('GROUP_ID'          as group_id,
                                          'GROUP_NAME'        as group_name,
                                          'DEPT'              as dept,
                                          'DEPT_NAME'         as dept_name,
                                          'CLASS'             as class,
                                          'CLASS_NAME'        as class_name,
                                          'CLASS_STATUS'      as class_status,
                                           null as dummy));
  L_mi_rec C_mandatory_ind%ROWTYPE;
  --
  CURSOR C_get_date_format IS
    select value
      from nls_database_parameters
     where parameter = 'NLS_DATE_FORMAT';
  --
BEGIN
  --
  open C_get_date_format;
  fetch C_get_date_format into L_date_format;
  close C_get_date_format;
  --
  -- get default values
  --
  for rec in (select group_id_dv,
                     group_name_dv,
                     dept_dv,
                     dept_name_dv,
                     class_dv,
                     class_name_dv,
                     class_status_dv,
                     null as dummy
               from (select column_key,
                            default_value
                       from s9t_tmpl_cols_def
                      where template_key  = template_key
                        and wksht_key     = CLASS_STATUS_SHEET
                     ) PIVOT (MAX(default_value) AS dv
                              FOR (column_key) IN ('GROUP_ID'          as group_id,
                                                   'GROUP_NAME'        as group_name,
                                                   'DEPT'              as dept,
                                                   'DEPT_NAME'         as dept_name,
                                                   'CLASS'             as class,
                                                   'CLASS_NAME'        as class_name,
                                                   'CLASS_STATUS'      as CLASS_STATUS,
                                                   NULL AS dummy))) loop
    --
    -- group_id default
    --
    BEGIN
      --
      L_default_rec.group_id := rec.group_id_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_Id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_Key        := CLASS_STATUS$GROUP_ID;
        L_issues_rt.error_Key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- group_name default
    --
    BEGIN
      --
      L_default_rec.group_name := rec.group_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$GROUP_NAME;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT default
    --
    BEGIN
      --
      L_default_rec.dept := rec.dept_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := CLASS_STATUS_SHEET;
        L_issues_rt.column_key   := CLASS_STATUS$DEPT;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT_NAME default
    --
    BEGIN
      --
      L_default_rec.dept_name := rec.dept_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := CLASS_STATUS_SHEET;
        L_issues_rt.column_key   := CLASS_STATUS$DEPT_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Class default
    --
    BEGIN
      --
      L_default_rec.class := rec.class_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := CLASS_STATUS_SHEET;
        L_issues_rt.column_key   := CLASS_STATUS$CLASS;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Class name default
    --
    BEGIN
      --
      L_default_rec.class_name := rec.class_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := CLASS_STATUS_SHEET;
        L_issues_rt.column_key   := CLASS_STATUS$CLASS_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Class Status default
    --
    BEGIN
      --
      L_default_rec.class_status := rec.class_status_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := CLASS_STATUS_SHEET;
        L_issues_rt.column_key   := CLASS_STATUS$CLASS_STATUS;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
  end loop;
  --
  -- get mandatory indicators
  --
  open C_mandatory_ind;
  fetch C_mandatory_ind into L_mi_rec;
  close C_mandatory_ind;
  --
  -- validate datatypes and business rules
  --
  for rec IN (select r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => CLASS_STATUS_SHEET,
                                                                     I_column_key                  => CLASS_STATUS$ACTION))                as ACTION,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => CLASS_STATUS_SHEET,
                                                                     I_column_key                  => CLASS_STATUS$GROUP_ID))              as GROUP_ID,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => CLASS_STATUS_SHEET,
                                                                     I_column_key                  => CLASS_STATUS$GROUP_NAME))            as GROUP_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => CLASS_STATUS_SHEET,
                                                                     I_column_key                  => CLASS_STATUS$DEPT))                  as DEPT,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => CLASS_STATUS_SHEET,
                                                                     I_column_key                  => CLASS_STATUS$DEPT_NAME))             as DEPT_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => CLASS_STATUS_SHEET,
                                                                     I_column_key                  => CLASS_STATUS$CLASS))                 as CLASS,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => CLASS_STATUS_SHEET,
                                                                     I_column_key                  => CLASS_STATUS$CLASS_NAME))            as CLASS_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => CLASS_STATUS_SHEET,
                                                                     I_column_key                  => CLASS_STATUS$CLASS_STATUS))          as CLASS_STATUS,
                     r.get_row_seq()                                                                                                       as ROW_SEQ
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = L_file_id
                 and ss.sheet_name = SHEET_NAME_TRANS(CLASS_STATUS_SHEET)) loop
    --
    L_temp_rec                      := null;
    L_temp_rec.process_id           := L_process_id;
    L_temp_rec.xxadeo_process_id    := xxadeo_upl_process_id_seq.nextval;
    L_temp_rec.create_id            := LP_user;
    L_temp_rec.last_update_id       := LP_user;
    L_temp_rec.create_datetime      := sysdate;
    L_temp_rec.last_update_datetime := sysdate;
    L_error                         := FALSE;
    L_row_seq                       := rec.row_seq;
    --
    LP_chunk_id                     := 1;
    --
    -- validate data types
    --
    if rec.class_status is null then
      --
      continue;
      --
    end if;
    --
    -- action
    --
    BEGIN
      --
      if rec.action is null then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      else
        --
        if rec.action = GP_upload_create or
           rec.action = GP_upload_update then
          --
          L_temp_rec.action := rec.action;
          --
        else
        --
          L_issues_rt                   := null;
          L_issues_rt.file_id           := L_file_id;
          L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
          L_issues_rt.column_key        := CLASS_STATUS$ACTION;
          L_issues_rt.row_seq           := rec.row_seq;
          L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
          --
          WRITE_S9T_ERROR(O_error_message => O_error_message,
                          I_issues_rt     => L_issues_rt);
          --
          L_error := TRUE;
          --
        end if;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- Action and status
    --
    if rec.action = GP_upload_create and rec.class_status = XXADEO_ATTR_INACTIVE then
      --
      L_issues_rt                   := null;
      L_issues_rt.file_id           := L_file_id;
      L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
      L_issues_rt.column_key        := CLASS_STATUS$CLASS_STATUS;
      L_issues_rt.row_seq           := rec.row_seq;
      L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_ACTION_STATUS;
      --
      WRITE_S9T_ERROR(O_error_message => O_error_message,
                      I_issues_rt     => L_issues_rt);
      --
    elsif rec.action = GP_upload_update and rec.class_status = XXADEO_ATTR_IN_PREPARATION then
      --
      L_issues_rt                   := null;
      L_issues_rt.file_id           := L_file_id;
      L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
      L_issues_rt.column_key        := CLASS_STATUS$CLASS_STATUS;
      L_issues_rt.row_seq           := rec.row_seq;
      L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_ACTION_STATUS;
      --
      WRITE_S9T_ERROR(O_error_message => O_error_message,
                      I_issues_rt     => L_issues_rt);
      --
    end if;
    --
    --
    -- group_id
    --
    BEGIN
      --
      if L_mi_rec.group_id_mi   = 'Y'   and
         L_default_rec.group_id is null and
         rec.group_id is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_id := rec.group_id;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- group_name
    --
    BEGIN
      --
      if L_mi_rec.group_name_mi   = 'Y'   and
         L_default_rec.group_name is null and
         rec.group_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_name := rec.group_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT
    --
    BEGIN
      --
      if L_mi_rec.dept_mi   = 'Y'   and
         L_default_rec.dept is null and
         rec.dept is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept := rec.dept;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT_NAME
    --
    BEGIN
      --
      if L_mi_rec.dept_name_mi   = 'Y'        and
         L_default_rec.dept_name is null and
         rec.dept_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept_name := rec.dept_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- CLASS
    --
    BEGIN
      --
      if L_mi_rec.class_mi   = 'Y'   and
         L_default_rec.class is null and
         rec.class is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$CLASS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.class := rec.class;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$CLASS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- CLASS NAME
    --
    BEGIN
      --
      if L_mi_rec.class_name_mi   = 'Y'   and
         L_default_rec.class_name is null and
         rec.class_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$CLASS_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.class_name := rec.class_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$CLASS_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- CLASS STATUS
    --
    BEGIN
      --
      if L_mi_rec.class_status_mi   = 'Y'   and
         L_default_rec.class_status is null and
         rec.class_status is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$CLASS_STATUS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.class_status := rec.class_status;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.column_key        := CLASS_STATUS$CLASS_STATUS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- validate business rules
    --
    if not L_error then
      --
      if not VALIDATE_CLASS_STATUS (O_error_message,
                                    L_error,
                                    L_temp_rec,
                                    L_row_seq) then
        --
        RETURN;
        --
      end if;
      --
    end if;
    --
    -- if line does not have error, insert in table to process
    --
    if NOT L_error then
      --
      L_temp_rec.row_seq   := rec.row_seq;
      --
      L_xxadeo_class_status_tbl.extend();
      L_xxadeo_class_status_tbl(L_xxadeo_class_status_tbl.count()) := L_temp_rec;
      --
    end if;
    --
  --
  end loop;
  --
  -- insert into stagging
  --
  BEGIN
    --
    forall i IN 1..L_xxadeo_class_status_tbl.count SAVE EXCEPTIONS
      insert into xxadeo_class_status_stg
        (process_id,
         xxadeo_process_id,
         action,
         group_id,
         group_name,
         dept,
         dept_name,
         class,
         class_name,
         class_status,
         create_id,
         create_datetime,
         last_update_id,
         last_update_datetime,
         row_seq)
      values
        (L_xxadeo_class_status_tbl(i).process_id,
         L_xxadeo_class_status_tbl(i).xxadeo_process_id,
         L_xxadeo_class_status_tbl(i).action,
         L_xxadeo_class_status_tbl(i).group_id,
         L_xxadeo_class_status_tbl(i).group_name,
         L_xxadeo_class_status_tbl(i).dept,
         L_xxadeo_class_status_tbl(i).dept_name,
         L_xxadeo_class_status_tbl(i).class,
         L_xxadeo_class_status_tbl(i).class_name,
         L_xxadeo_class_status_tbl(i).class_status,
         L_xxadeo_class_status_tbl(i).create_id,
         L_xxadeo_class_status_tbl(i).create_datetime,
         L_xxadeo_class_status_tbl(i).last_update_id,
         L_xxadeo_class_status_tbl(i).last_update_datetime,
         L_xxadeo_class_status_tbl(i).row_seq);
    --
  EXCEPTION
    --
    when DML_ERRORS then
      --
      for i in 1..sql%bulk_exceptions.COUNT loop
        --
        L_error_code:=sql%bulk_exceptions(i).error_code;
        --
        if L_error_code=1 then
          --
          L_error_code:=NULL;
          L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T');
          --
        end if;
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := CLASS_STATUS_SHEET;
        L_issues_rt.row_seq           := sql%bulk_exceptions(i).error_index;
        L_issues_rt.error_key         := sqlerrm(-sql%bulk_exceptions(i).error_code);
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      end loop;
      --
    --
  END;
  --
END PROCESS_S9T_CLASS_STATUS;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SUBCLASS_STATUS(O_error_message   IN OUT  LOGGER_LOGS.TEXT%type) IS
  --
  L_program                       VARCHAR2(64) := 'XXADEO_MERCHHIER_ATTRIB_SQL.PROCESS_S9T_SUBCLASS_STATUS';
  --
  TYPE XXADEO_SUBCLASS_STATUS_TYPE IS TABLE OF XXADEO_SUBCLASS_STATUS_STG%ROWTYPE;
  L_xxadeo_subclass_status_tbl     XXADEO_SUBCLASS_STATUS_TYPE := XXADEO_SUBCLASS_STATUS_TYPE();
  L_temp_rec                       XXADEO_SUBCLASS_STATUS_STG%ROWTYPE;
  L_issues_rt                      S9T_ERRORS%ROWTYPE;
  --
  L_error                          BOOLEAN:=FALSE;
  L_default_rec                    XXADEO_SUBCLASS_STATUS_STG%ROWTYPE;
  L_row_seq                        SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE;
  L_error_code                     NUMBER;
  L_error_msg                      RTK_ERRORS.RTK_TEXT%TYPE;
  --
  DML_ERRORS    EXCEPTION;
  PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
  --
  cursor C_mandatory_ind is
    select group_id_mi,
           group_name_mi,
           dept_mi,
           dept_name_mi,
           class_mi,
           class_name_mi,
           subclass_mi,
           subclass_name_mi,
           subclass_status_mi, 
           1 as dummy
      from (select column_key,
                   required_visual_ind
              from s9t_tmpl_cols_def
             where template_key   = TEMPLATE_KEY
               and wksht_key      = SCLASS_STATUS_SHEET
            ) pivot (max(required_visual_ind) as mi
                     for (column_key) in ('GROUP_ID'             as group_id,
                                          'GROUP_NAME'           as group_name,
                                          'DEPT'                 as dept,
                                          'DEPT_NAME'            as dept_name,
                                          'CLASS'                as class,
                                          'CLASS_NAME'           as class_name,
                                          'SUBCLASS'             as subclass,
                                          'SUBCLASS_NAME'        as subclass_name,
                                          'SUBCLASS_STATUS'      as subclass_status,
                                           null as dummy));
  L_mi_rec C_mandatory_ind%ROWTYPE;
  --
  CURSOR C_get_date_format IS
    select value
      from nls_database_parameters
     where parameter = 'NLS_DATE_FORMAT';
  --
BEGIN
  --
  OPEN C_get_date_format;
  FETCH C_get_date_format INTO L_date_format;
  CLOSE C_get_date_format;
  --
  -- get default values
  --
  for rec in (select group_id_dv,
                     group_name_dv,
                     dept_dv,
                     dept_name_dv,
                     class_dv,
                     class_name_dv,
                     subclass_dv,
                     subclass_name_dv,
                     subclass_status_dv,
                     null as dummy
               from (select column_key,
                            default_value
                       from s9t_tmpl_cols_def
                      where template_key  = template_key
                        and wksht_key     = SCLASS_STATUS_SHEET
                     ) PIVOT (MAX(default_value) AS dv
                              FOR (column_key) IN ('GROUP_ID'             as group_id,
                                                   'GROUP_NAME'           as group_name,
                                                   'DEPT'                 as dept,
                                                   'DEPT_NAME'            as dept_name,
                                                   'CLASS'                as class,
                                                   'CLASS_NAME'           as class_name,
                                                   'SUBCLASS'             as subclass,
                                                   'SUBCLASS_NAME'        as subclass_name,
                                                   'SUBCLASS_STATUS'      as subclass_status,
                                                   NULL AS dummy))) loop
    --
    -- group_id default
    --
    BEGIN
      --
      L_default_rec.group_id := rec.group_id_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_Id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_Key        := SCLASS_STATUS$GROUP_ID;
        L_issues_rt.error_Key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- group_name default
    --
    BEGIN
      --
      L_default_rec.group_name := rec.group_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$GROUP_NAME;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT default
    --
    BEGIN
      --
      L_default_rec.dept := rec.dept_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key   := SCLASS_STATUS$DEPT;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- DEPT_NAME default
    --
    BEGIN
      --
      L_default_rec.dept_name := rec.dept_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key   := SCLASS_STATUS$DEPT_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Class default
    --
    BEGIN
      --
      L_default_rec.class := rec.class_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key   := SCLASS_STATUS$CLASS;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- Class name default
    --
    BEGIN
      --
      L_default_rec.class_name := rec.class_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key   := SCLASS_STATUS$CLASS_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- SUBCLASS default
    --
    BEGIN
      --
      L_default_rec.subclass := rec.subclass_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key   := SCLASS_STATUS$SUBCLASS;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- SUBCLASS NAME default
    --
    BEGIN
      --
      L_default_rec.subclass_name := rec.subclass_name_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key   := SCLASS_STATUS$SUBCLASS_NAME;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    -- SUBCLASS STATUS default
    --
    BEGIN
      --
      L_default_rec.subclass_status := rec.subclass_status_dv;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt              := null;
        L_issues_rt.file_id      := L_file_id;
        L_issues_rt.wksht_key    := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key   := SCLASS_STATUS$SUBCLASS_STATUS;
        L_issues_rt.error_key    := XXADEO_GLOBAL_VARS_SQL.VAL$INV_DEFAULT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
    END;
    --
    --
  end loop;
  --
  -- get mandatory indicators
  --
  open C_mandatory_ind;
  fetch C_mandatory_ind into L_mi_rec;
  close C_mandatory_ind;
  --
  -- validate datatypes and business rules
  --
  for rec IN (select r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => SCLASS_STATUS_SHEET,
                                                                     I_column_key                  => SCLASS_STATUS$ACTION))                as ACTION,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => SCLASS_STATUS_SHEET,
                                                                     I_column_key                  => SCLASS_STATUS$GROUP_ID))              as GROUP_ID,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => SCLASS_STATUS_SHEET,
                                                                     I_column_key                  => SCLASS_STATUS$GROUP_NAME))            as GROUP_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => SCLASS_STATUS_SHEET,
                                                                     I_column_key                  => SCLASS_STATUS$DEPT))                  as DEPT,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => SCLASS_STATUS_SHEET,
                                                                     I_column_key                  => SCLASS_STATUS$DEPT_NAME))             as DEPT_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => SCLASS_STATUS_SHEET,
                                                                     I_column_key                  => SCLASS_STATUS$CLASS))                 as CLASS,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => SCLASS_STATUS_SHEET,
                                                                     I_column_key                  => SCLASS_STATUS$CLASS_NAME))            as CLASS_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => SCLASS_STATUS_SHEET,
                                                                     I_column_key                  => SCLASS_STATUS$SUBCLASS))              as SUBCLASS,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => SCLASS_STATUS_SHEET,
                                                                     I_column_key                  => SCLASS_STATUS$SUBCLASS_NAME))         as SUBCLASS_NAME,
                     r.get_cell(XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                                     I_wksht_key                   => SCLASS_STATUS_SHEET,
                                                                     I_column_key                  => SCLASS_STATUS$SUBCLASS_STATUS))       as SUBCLASS_STATUS,
                     r.get_row_seq()                                                                                                        as ROW_SEQ
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = L_file_id
                 and ss.sheet_name = SHEET_NAME_TRANS(SCLASS_STATUS_SHEET)) loop
    --
    L_temp_rec                      := null;
    L_temp_rec.process_id           := L_process_id;
    L_temp_rec.xxadeo_process_id    := xxadeo_upl_process_id_seq.nextval;
    L_temp_rec.create_id            := LP_user;
    L_temp_rec.last_update_id       := LP_user;
    L_temp_rec.create_datetime      := sysdate;
    L_temp_rec.last_update_datetime := sysdate;
    L_error                         := FALSE;
    L_row_seq                       := rec.row_seq;
    --
    LP_chunk_id                     := 1;
    --
    -- validate data types
    --
    if rec.subclass_status is null then
      --
      continue;
      --
    end if;
    --
    -- action
    --
    BEGIN
      --
      if rec.action is null then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      else
        --
        if rec.action = GP_upload_create or
           rec.action = GP_upload_update then
          --
          L_temp_rec.action := rec.action;
          --
        else
        --
          L_issues_rt                   := null;
          L_issues_rt.file_id           := L_file_id;
          L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
          L_issues_rt.column_key        := SCLASS_STATUS$ACTION;
          L_issues_rt.row_seq           := rec.row_seq;
          L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
          --
          WRITE_S9T_ERROR(O_error_message => O_error_message,
                          I_issues_rt     => L_issues_rt);
          --
          L_error := TRUE;
          --
        end if;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$ACTION;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- Action and status
    --
    if rec.action = GP_upload_create and rec.subclass_status = XXADEO_ATTR_INACTIVE then
      --
      L_issues_rt                   := null;
      L_issues_rt.file_id           := L_file_id;
      L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
      L_issues_rt.column_key        := SCLASS_STATUS$SUBCLASS_STATUS;
      L_issues_rt.row_seq           := rec.row_seq;
      L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_ACTION_STATUS;
      --
      WRITE_S9T_ERROR(O_error_message => O_error_message,
                      I_issues_rt     => L_issues_rt);
      --
    elsif rec.action = GP_upload_update and rec.subclass_status = XXADEO_ATTR_IN_PREPARATION then
      --
      L_issues_rt                   := null;
      L_issues_rt.file_id           := L_file_id;
      L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
      L_issues_rt.column_key        := SCLASS_STATUS$SUBCLASS_STATUS;
      L_issues_rt.row_seq           := rec.row_seq;
      L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_ACTION_STATUS;
      --
      WRITE_S9T_ERROR(O_error_message => O_error_message,
                      I_issues_rt     => L_issues_rt);
      --
    end if;
    --
    -- group_id
    --
    BEGIN
      --
      if L_mi_rec.group_id_mi   = 'Y'   and
         L_default_rec.group_id is null and
         rec.group_id is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_id := rec.group_id;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$GROUP_ID;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- group_name
    --
    BEGIN
      --
      if L_mi_rec.group_name_mi   = 'Y'   and
         L_default_rec.group_name is null and
         rec.group_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.group_name := rec.group_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$GROUP_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT
    --
    BEGIN
      --
      if L_mi_rec.dept_mi   = 'Y'   and
         L_default_rec.dept is null and
         rec.dept is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept := rec.dept;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$DEPT;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- DEPT_NAME
    --
    BEGIN
      --
      if L_mi_rec.dept_name_mi   = 'Y'        and
         L_default_rec.dept_name is null and
         rec.dept_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.dept_name := rec.dept_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$DEPT_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- CLASS
    --
    BEGIN
      --
      if L_mi_rec.class_mi   = 'Y'   and
         L_default_rec.class is null and
         rec.class is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$CLASS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.class := rec.class;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$CLASS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- CLASS NAME
    --
    BEGIN
      --
      if L_mi_rec.class_name_mi   = 'Y'   and
         L_default_rec.class_name is null and
         rec.class_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$CLASS_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.class_name := rec.class_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$CLASS_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- SUBCLASS
    --
    BEGIN
      --
      if L_mi_rec.subclass_mi   = 'Y'   and
         L_default_rec.subclass is null and
         rec.subclass is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$SUBCLASS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.subclass := rec.subclass;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$SUBCLASS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_NUMBER;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- SUBCLASS NAME
    --
    BEGIN
      --
      if L_mi_rec.subclass_name_mi   = 'Y'   and
         L_default_rec.subclass_name is null and
         rec.subclass_name is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$SUBCLASS_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.subclass_name := rec.subclass_name;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$SUBCLASS_NAME;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- SUBCLASS STATUS
    --
    BEGIN
      --
      if L_mi_rec.subclass_status_mi   = 'Y'   and
         L_default_rec.subclass_status is null and
         rec.subclass_status is null           then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$SUBCLASS_STATUS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_MAND_VALUE;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      else
        --
        L_temp_rec.subclass_status := rec.subclass_status;
        --
      end if;
      --
    EXCEPTION
      --
      when OTHERS then
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.column_key        := SCLASS_STATUS$SUBCLASS_STATUS;
        L_issues_rt.row_seq           := rec.row_seq;
        L_issues_rt.error_key         := XXADEO_GLOBAL_VARS_SQL.VAL$XXADEO_INV_DT_TEXT;
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
        L_error := TRUE;
        --
      --
    END;
    --
    -- validate business rules
    --
    if not L_error then
      --
      --
      if not VALIDATE_SUBCLASS_STATUS (O_error_message,
                                       L_error,
                                       L_temp_rec,
                                       L_row_seq) then
        --
        RETURN;
        --
      end if;
      --
    end if;
    --
    -- if line does not have error, insert in table to process
    --
    if NOT L_error then
      --
      L_temp_rec.row_seq   := rec.row_seq;
      --
      L_xxadeo_subclass_status_tbl.extend();
      L_xxadeo_subclass_status_tbl(L_xxadeo_subclass_status_tbl.count()) := L_temp_rec;
      --
    end if;
    --
  --
  end loop;
  --
  -- insert into stagging
  --
  BEGIN
    --
    forall i IN 1..L_xxadeo_subclass_status_tbl.count SAVE EXCEPTIONS
      insert into xxadeo_subclass_status_stg
        (process_id,
         xxadeo_process_id,
         action,
         group_id,
         group_name,
         dept,
         dept_name,
         class,
         class_name,
         subclass,
         subclass_name,
         subclass_status,
         create_id,
         create_datetime,
         last_update_id,
         last_update_datetime,
         row_seq)
      values
        (L_xxadeo_subclass_status_tbl(i).process_id,
         L_xxadeo_subclass_status_tbl(i).xxadeo_process_id,
         L_xxadeo_subclass_status_tbl(i).action,
         L_xxadeo_subclass_status_tbl(i).group_id,
         L_xxadeo_subclass_status_tbl(i).group_name,
         L_xxadeo_subclass_status_tbl(i).dept,
         L_xxadeo_subclass_status_tbl(i).dept_name,
         L_xxadeo_subclass_status_tbl(i).class,
         L_xxadeo_subclass_status_tbl(i).class_name,
         L_xxadeo_subclass_status_tbl(i).subclass,
         L_xxadeo_subclass_status_tbl(i).subclass_name,
         L_xxadeo_subclass_status_tbl(i).subclass_status,
         L_xxadeo_subclass_status_tbl(i).create_id,
         L_xxadeo_subclass_status_tbl(i).create_datetime,
         L_xxadeo_subclass_status_tbl(i).last_update_id,
         L_xxadeo_subclass_status_tbl(i).last_update_datetime,
         L_xxadeo_subclass_status_tbl(i).row_seq);
    --
  EXCEPTION
    --
    when DML_ERRORS then
      --
      for i in 1..sql%bulk_exceptions.COUNT loop
        --
        L_error_code:=sql%bulk_exceptions(i).error_code;
        --
        if L_error_code=1 then
          --
          L_error_code:=NULL;
          L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T');
          --
        end if;
        --
        L_issues_rt                   := null;
        L_issues_rt.file_id           := L_file_id;
        L_issues_rt.wksht_key         := SCLASS_STATUS_SHEET;
        L_issues_rt.row_seq           := sql%bulk_exceptions(i).error_index;
        L_issues_rt.error_key         := sqlerrm(-sql%bulk_exceptions(i).error_code);
        --
        WRITE_S9T_ERROR(O_error_message => O_error_message,
                        I_issues_rt     => L_issues_rt);
        --
      end loop;
      --
    --
  END;
  --
END PROCESS_S9T_SUBCLASS_STATUS;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
  --
  L_program          VARCHAR2(64) := 'XXADEO_MERCHHIER_ATTRIB_SQL.PROCESS_S9T';
  --
  L_file             S9T_FILE;
  L_sheets           S9T_PKG.names_map_typ;
  L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
  INVALID_FORMAT     EXCEPTION;
  PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
  MAX_CHAR           EXCEPTION;
  PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
  L_issues_rt        S9T_ERRORS%ROWTYPE;
  L_error            BOOLEAN:=FALSE;
  --
  cursor C_get_status_active is
    select code
      from code_detail
     where code_type = XXADEO_ATTR_STATUS_CODE
       and upper(code_desc) = XXADEO_ACTIVE;
  --
  cursor C_get_status_inactive is
    select code
      from code_detail
     where code_type = XXADEO_ATTR_STATUS_CODE
       and upper(code_desc) = XXADEO_INACTIVE;
  --
BEGIN
  --
  COMMIT;
  S9T_PKG.ODS2OBJ(I_file_id);
  COMMIT;
  --
  L_file_id    := I_file_id;
  L_process_id :=  I_process_id;
  --
  L_file := s9t_file(I_file_id,TRUE);
  LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
  if S9T_PKG.CODE2DESC(O_error_message,
                       template_category,
                       L_file,
                       TRUE) = FALSE then
     return FALSE;
  end if;
  --
  S9T_PKG.SAVE_OBJ(L_file);
  --
  LP_errors_tab   := NEW errors_tab_typ();
  --
  if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
    --
    L_issues_rt                   := null;
    L_issues_rt.file_id           := L_file_id;
    L_issues_rt.column_key        := NULL;
    L_issues_rt.error_type        := NULL;
    L_issues_rt.error_key         := 'S9T_INVALID_TEMPLATE';
    --
    WRITE_S9T_ERROR(O_error_message => O_error_message,
                    I_issues_rt     => L_issues_rt);
  else
    --
    sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                            L_file.user_lang);
    --
    -- load template column indexex object
    --
    L_xxadeo_wksht_column_idx_tbl := XXADEO_WKSHT_COLUMN_IDX_TBL();
    --
    if XXADEO_CUSTOM_UPLOAD_SQL.GET_TEMPLATE_COLUMN_INDEXES(O_error_message                => O_error_message,
                                                            IO_xxadeo_wksht_column_idx_tbl => L_xxadeo_wksht_column_idx_tbl,
                                                            I_template_key                 => TEMPLATE_KEY) = FALSE then
      --
      COMMIT;
      RETURN FALSE;
      --
    end if;
    --
    OPEN C_get_status_active;
    FETCH C_get_status_active INTO XXADEO_ATTR_ACTIVE;
    CLOSE C_get_status_active;
    --
    OPEN C_get_status_inactive;
    FETCH C_get_status_inactive INTO XXADEO_ATTR_INACTIVE;
    CLOSE C_get_status_inactive;
    --
    PROCESS_S9T_DEPT_STATUS(O_error_message => O_error_message);
    PROCESS_S9T_CLASS_STATUS(O_error_message => O_error_message);
    PROCESS_S9T_SUBCLASS_STATUS(O_error_message => O_error_message);    
    --
    -- ATIVATE CLASS
    --
    for rec in (select *
                  from xxadeo_class_status_stg
                 where process_id = L_process_id) loop
      --
      L_error := FALSE;
      --
      if upper(trim(rec.class_status)) = XXADEO_ATTR_ACTIVE then
        --
        if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER_ACT(O_error_message,
                                                                  rec.xxadeo_process_id,
                                                                  rec.class_status,
                                                                  rec.dept) then
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      CLASS_STATUS_SHEET,
                      rec.row_seq,
                      CLASS_STATUS$CLASS,
                      XXADEO_GLOBAL_VARS_SQL.VAL$ACT_CLASS_NOT_DEPT);
          --
          L_error := TRUE;
          --
        end if;
        --
      end if;
      --
      if L_error = TRUE then
        --
        delete
          from xxadeo_class_status_stg
         where process_id = rec.process_id
           and xxadeo_process_id = rec.xxadeo_process_id;
        --
      end if;
      --
      L_error := FALSE;
      --
    end loop;
    --
    -- ACTIVATE SUBCLASS
    --
    for rec in (select *
                  from xxadeo_subclass_status_stg
                 where process_id = L_process_id) loop
      --
      L_error := FALSE;
      --
      if upper(trim(rec.subclass_status)) = XXADEO_ATTR_ACTIVE then
        --
        if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER_ACT(O_error_message,
                                                                  rec.xxadeo_process_id,
                                                                  rec.subclass_status,
                                                                  rec.dept,
                                                                  rec.class) then
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id, 
                      SCLASS_STATUS_SHEET,
                      rec.row_seq,
                      SCLASS_STATUS$SUBCLASS,
                      XXADEO_GLOBAL_VARS_SQL.VAL$ACT_SCLASS_NOT_CLASS);
          --
          L_error := TRUE; 
          --
        end if;     
        --
      end if;
      --
      if L_error = TRUE then
        --
        delete
          from xxadeo_subclass_status_stg
         where process_id = rec.process_id
           and xxadeo_process_id = rec.xxadeo_process_id;
        --
      end if;
      --
      L_error := FALSE;
      --
    end loop;
    --
    -- INACTIVATE SUBCLASS
    --
    for rec in (select *
                  from xxadeo_subclass_status_stg
                 where process_id = L_process_id) loop
      --
      L_error := FALSE;
      --
      if upper(trim(rec.subclass_status)) = XXADEO_ATTR_INACTIVE then
        --
        if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER_DEACT(O_error_message,
                                                                    rec.xxadeo_process_id,
                                                                    rec.subclass_status,
                                                                    rec.dept,
                                                                    rec.class,
                                                                    rec.subclass) then
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      SCLASS_STATUS_SHEET,
                      rec.row_seq,
                      SCLASS_STATUS$SUBCLASS,
                      XXADEO_GLOBAL_VARS_SQL.VAL$DEACT_SCLASS_WITH_ITEM);
          --
          L_error := TRUE;
          --
        end if;
        --
      end if;  
      --
      if L_error = TRUE then
        --
        delete
          from xxadeo_subclass_status_stg
         where process_id = rec.process_id
           and xxadeo_process_id = rec.xxadeo_process_id;
        --
      end if;
      --
      L_error := FALSE;
      --
    end loop;
    --
    -- INATIVATE CLASS
    -- 
    for rec in (select *
                  from xxadeo_class_status_stg
                 where process_id = L_process_id) loop
      --
      L_error := FALSE;
      --
      if upper(trim(rec.class_status)) = XXADEO_ATTR_INACTIVE then
        --
        if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER_DEACT(O_error_message,
                                                                    rec.xxadeo_process_id,
                                                                    rec.class_status,
                                                                    rec.dept,
                                                                    rec.class) then
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      CLASS_STATUS_SHEET,
                      rec.row_seq,
                      CLASS_STATUS$CLASS,
                      XXADEO_GLOBAL_VARS_SQL.VAL$DEACT_CLASS_ACT_SCLASS);
          --
          L_error := TRUE;
          --
        end if;
        --
      end if;
      --
      if L_error = TRUE then
        --
        delete
          from xxadeo_class_status_stg
         where process_id = rec.process_id
           and xxadeo_process_id = rec.xxadeo_process_id;
        --
      end if;
      --
      L_error := FALSE;
      --
    end loop;
    --
    -- DEACTIVATE DEPT
    --
    for rec in (select *
                  from xxadeo_dept_status_stg
                 where process_id = L_process_id) loop
      --
      L_error := FALSE;
      --
      if upper(trim(rec.dept_status)) = XXADEO_ATTR_INACTIVE then
        --
        if not XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER_DEACT(O_error_message,
                                                                    rec.xxadeo_process_id,
                                                                    rec.dept_status,
                                                                    rec.dept) then
          WRITE_ERROR(L_process_id,
                      svc_admin_upld_er_seq.nextval,
                      LP_chunk_id,
                      DEPARTMENT_STATUS_SHEET,
                      rec.row_seq,
                      DEPARTMENT_STATUS$DEPT,
                      XXADEO_GLOBAL_VARS_SQL.VAL$DEACT_DEPT_ACT_CLASS);
          --
          L_error := TRUE;
          --
        end if;
        --
      end if;
      --
      if L_error = TRUE then
        --
        delete
          from xxadeo_dept_status_stg
         where process_id = rec.process_id
           and xxadeo_process_id = rec.xxadeo_process_id;
        --
      end if;
      --
      L_error := FALSE; 
      --
    end loop;
    --
    L_error := FALSE;
    --
  end if;
  --
  O_error_count := LP_s9t_errors_tab.COUNT() + LP_errors_tab.COUNT();
  --
  FORALL i IN 1..LP_s9t_errors_tab.COUNT
     insert into s9t_errors
          values LP_s9t_errors_tab(i);
  LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
  --
  FORALL i IN 1..LP_errors_tab.COUNT
    insert into svc_admin_upld_er
         values LP_errors_tab(i);
  LP_errors_tab := NEW errors_tab_typ();
  --
  --Update process$status in svc_process_tracker
  --
  if O_error_count = 0 then
     L_process_status := 'PS';
  else
     L_process_status := 'PE';
  end if;

  update svc_process_tracker
     set status     = L_process_status,
         file_id    = I_file_id
   where process_id = I_process_id;
  --
  COMMIT;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when INVALID_FORMAT then
    --
    ROLLBACK;
    O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                          NULL,
                                          NULL,
                                          NULL);
    LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
    --
    L_issues_rt                   := null;
    L_issues_rt.file_id           := L_file_id;
    L_issues_rt.column_key        := NULL;
    L_issues_rt.error_type        := NULL;
    L_issues_rt.error_key         := 'INV_FILE_FORMAT';
    --
    WRITE_S9T_ERROR(O_error_message => O_error_message,
                    I_issues_rt     => L_issues_rt);
    --
    O_error_count := LP_s9t_errors_tab.count();
    forall i IN 1..O_error_count
       insert into s9t_errors
            values LP_s9t_errors_tab(i);

    update svc_process_tracker
       set status       = 'PE',
           file_id      = I_file_id
     where process_id   = I_process_id;
     --
     COMMIT;
     --
     RETURN FALSE;
     --
  when MAX_CHAR then
    --
    ROLLBACK;
    O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
    Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
    L_issues_rt                   := null;
    L_issues_rt.file_id           := L_file_id;
    L_issues_rt.column_key        := NULL;
    L_issues_rt.error_type        := NULL;
    L_issues_rt.error_key         := 'EXCEEDS_4000_CHAR';
    --
    write_s9t_error(O_error_message => O_error_message,
                    I_issues_rt     => L_issues_rt);
    --
    O_error_count := Lp_s9t_errors_tab.count();
    --
    forall i IN 1..O_error_count
      --
      insert into s9t_errors values Lp_s9t_errors_tab (i);
      --
      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      --
      COMMIT;
      --
    RETURN FALSE;
    --
  when OTHERS then
    --
    ROLLBACK;
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END PROCESS_S9T;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS
BEGIN
  --
  delete from xxadeo_dept_status_stg
   where process_id = I_process_id;
  --
  delete from xxadeo_class_status_stg
   where process_id = I_process_id;
  --
  delete from xxadeo_subclass_status_stg
   where process_id = I_process_id;
  --
END;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
  --
  L_program          VARCHAR2(64)                    :='XXADEO_MERCHHIER_ATTRIB_SQL.PROCESS';
  --
  L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
  L_err_count        VARCHAR2(1);
  L_warn_count       VARCHAR2(1);
  L_errors_count     NUMBER;
  --
  SQL_ERROR          EXCEPTION;
  --
  cursor C_GET_ERR_COUNT is
     select 'x'
       from svc_admin_upld_er
      where process_id = I_process_id
        and error_type = 'E';

  cursor C_GET_WARN_COUNT is
     select 'x'
       from svc_admin_upld_er
      where process_id = I_process_id
        and error_type = 'W';
  --
BEGIN
  --
  LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
  LP_errors_tab   := NEW errors_tab_typ();
  LP_chunk_id     := I_chunk_id;
  L_process_id    := I_process_id;
  --
  LP_s9t_errors_tab := s9t_errors_tab_typ();
  --
  if SETUP_DEPT_STATUS(O_error_message   => O_error_message) = FALSE then
    --
    COMMIT;
    RAISE SQL_ERROR;
    --
  end if;
  --
  COMMIT;
  --
  if SETUP_CLASS_STATUS(O_error_message   => O_error_message) = FALSE then
    --
    COMMIT;
    RAISE SQL_ERROR;
    --
  end if;
  --
  COMMIT;
  --
  --
  if SETUP_SUBCLASS_STATUS(O_error_message   => O_error_message) = FALSE then
    --
    COMMIT;
    RAISE SQL_ERROR;
    --
  end if;
  --
  COMMIT;
  --
  L_errors_count := L_errors_count + LP_s9t_errors_tab.count();
  --
  O_error_count := LP_errors_tab.COUNT();
  --
  FORALL i IN 1..O_error_count
     --
     insert into svc_admin_upld_er
          values LP_errors_tab(i);
     --
  LP_errors_tab := NEW errors_tab_typ();
  --
  open  c_get_err_count;
  fetch c_get_err_count into L_err_count;
  close c_get_err_count;
  --
  open  c_get_warn_count;
  fetch c_get_warn_count into L_warn_count;
  close c_get_warn_count;
  --
  if L_err_count is NOT NULL then
    --
    L_process_status := 'PE';
    --
  elsif L_warn_count is NOT NULL then
    --
    L_process_status := 'PW';
    --
  else
    --
    L_process_status := 'PS';
    --
  end if;
  --
  update svc_process_tracker
     set status = (CASE
                    when status = 'PE' then
                     'PE'
                    else
                     L_process_status
                  END),
         action_date = SYSDATE
   where process_id = I_process_id;
  --
  CLEAR_STAGING_DATA(I_process_id);
  --
  COMMIT;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when SQL_ERROR then
    --
    update svc_process_tracker
       set status = 'PE',
           action_date = SYSDATE
     where process_id = I_process_id;
    --
    CLEAR_STAGING_DATA(I_process_id);
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --   
  --
  when OTHERS then
    --
    CLEAR_STAGING_DATA(I_process_id);
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END PROCESS;
----------------------------------------------------------------------------------------------
END XXADEO_MERCHHIER_ATTRIB_SQL;
/
