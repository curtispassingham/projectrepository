CREATE OR REPLACE package XXADEO_SUPP_CUST_RULES_CFA_SQL is

  -- Author  : Paulo Mamede / Tiago Torres
  -- Created : 24/05/2018
  -- Purpose : Custom Rules for Supplier CFA RB135

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-07-19 - Jorge Agra - BUG#120 Invalid error message                    */
/*                           DBG_SQL.MSG                                      */
/* 2018-07-23 - Jorge Agra - BUG#153 New err message for dept/type validation */
/*                           when both fail                                   */
/* 2018-10-25 - Filipa Neves - Added function check_all_validation to be call */
/*                             on cfa_entity level                            */
/* 2018-12-03 - Jorge Agra - BUG#695 single message for required fields when  */
/*                           shipping cost billing is checked                 */
/******************************************************************************/

  GP_LAST_SUPPLIER_SITE_SUPP            VARCHAR2(255) := 'LAST_SUPPLIER_SITE_SUPP';
  GP_DEPARTMENT_SUPP                    VARCHAR2(255) := 'DEPARTMENT_SUPP';
  GP_SUPPLIER_TYPE_SUPP                 VARCHAR2(255) := 'SUPPLIER_TYPE_SUPP';
  GP_SUPPLIER                           VARCHAR2(255) := 'SUPPLIER';
  GP_SHIPPING_COST_BILLING_SUPP         VARCHAR2(255) := 'SHIPPING_COST_BILLING_SUPP';
  GP_ERRKEY_SHIP_COST_BILL_SUPP         VARCHAR2(255) := 'CFA_SHIP_COST_BILL_SUPP';
  GP_UNIT_FREE_PORT_SUPP                VARCHAR2(255) := 'UNIT_FREE_PORT_SUPP';
  GP_VALUE_FREE_PORT_SUPP               VARCHAR2(255) := 'VALUE_FREE_PORT_SUPP';
  GP_FREIGHT_CHARGE_SUPP                VARCHAR2(255) := 'FREIGHT_CHARGE_SUPP';
  GP_GLN_CODE_SUPP                      VARCHAR2(255) := 'GLN_CODE_SUPP';
  GP_GLN_CODE_SUPP_TYPE                 VARCHAR2(255) := 'EAN13';

  --called at cfa attribute level
  FUNCTION VALID_LAST_SUPPLIER_SITE_SUPP(O_error_message IN OUT VARCHAR2)
    RETURN BOOLEAN;

  FUNCTION SUPPLIER_IC_MANDATORY_FIELDS(O_error_message IN OUT VARCHAR2,
                                        I_supplier_id   IN SUPS.SUPPLIER%TYPE)
    RETURN BOOLEAN;
  --called at cfa group set level
  FUNCTION VALIDATE_CFA_ATTRIB_GRP_SET(O_error_message IN OUT VARCHAR2)
    RETURN BOOLEAN;

  --called at cfa attribute level
  FUNCTION CHECK_BARCODE_FORMAT(O_error_message IN OUT LOGGER_LOGS.TEXT%TYPE)
    RETURN BOOLEAN;
  
  --called at cfa entity level
  FUNCTION CHECK_ALL_VALIDATIONS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN BOOLEAN;
    
end XXADEO_SUPP_CUST_RULES_CFA_SQL;
/


