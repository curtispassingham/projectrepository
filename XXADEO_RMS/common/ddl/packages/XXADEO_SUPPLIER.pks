--------------------------------------------------------
--  DDL for Package XXADEO_SUPPLIER
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE PACKAGE XXADEO_SUPPLIER AUTHID CURRENT_USER AS
-------------------------------------------------------------------------
-- TYPE SUPP_REF_TBL IS TABLE OF "RIB_SupplierColRef_REC" INDEX BY BINARY_INTEGER;
--
LP_sup_add    VARCHAR2(15) := 'suppadd';
LP_sup_upd    VARCHAR2(15) := 'suppmod';
-------------------------------------------------------------------------
-- Function Name: CONSUME
-- Purpose      : This function will validate the input supplier record and
--                populate the local tables to be used for processing.
--                Also this populates output ref_object, which will be sent back to
--                AIA/XAU for Xref key and Retail key mapping.
--                Calls RMSAIASUB_SUPPLIER_SQL.PERSIST to populate RMS tables.
-------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code        IN OUT   VARCHAR2,
                  O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_outputobject       IN OUT   "XXADEO_SupplierColRef_REC",
                  I_inputobject        IN       "XXADEO_SupplierColDesc_REC",
                  I_inputobject_type   IN       VARCHAR2);
-------------------------------------------------------------------------
END XXADEO_SUPPLIER;

/