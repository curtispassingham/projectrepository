CREATE OR REPLACE PACKAGE BODY XXADEO_ITEM_LFCY_VAL_IS_SQL AS

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-08-20 - Jorge Agra - Removal of IS-ACT-08 code                        */
/* 2018-09-20 - Jorge Agra - BUG#438 IS-ACT-06 check item group and not dept  */
/*                           BUG#440 IS-ACT-04 check ITEM_SUPP_COUNTRY_DIM    */
/*                                   for at least a row with DIM_OBJECT = CA  */
/*                                   and not supp_pack_size > 0 on            */
/*                                   item_supp_country                        */
/* 2018-10-02 - Jorge Agra - include validation of next_val_dt in all rules   */
/* 2018-11-05 - Jorge Agra - Rule IS-INIT-01 to control STOP=>INIT change     */
/* 2018-11-28 - Jorge Agra - FUNC_AREA = 'CFA'|'UDA' in all XXADEO_MOM_DVM    */
/*                           where conditions                                 */
/******************************************************************************/

--------------------------------------------------------------------------------
-- Constants
--------------------------------------------------------------------------------

CONST$PackageName             CONSTANT VARCHAR2(30) := 'XXADEO_ITEM_LFCY_VAL_IS_SQL';

--------------------------------------------------------------------------------
-- Types
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Globals
--------------------------------------------------------------------------------

-- today for RMS
G_rms_now date := get_vdate;
GV_user_id                          VARCHAR2(30)  := NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'),  USER);
--GV_user_id                          VARCHAR2(30)  := 'RB107';

GV_time                             NUMBER := dbms_utility.get_time();
GV_fake_exec                        boolean := false;

--------------------------------------------------------------------------------
--Function Name : DBG
--Purpose       : write to DEBUG_MSG table
--------------------------------------------------------------------------------
procedure DBG(I_program varchar2, I_message varchar2)
is
  ---
  L_time      number;
  ---
BEGIN
  ---
  L_time := dbms_utility.get_time();
  ---
  DBG_SQL.MSG(I_program, I_message || ' / dT(ms)=' || to_char(L_time - GV_time));
  ---
  GV_time := L_time;
  ---
end;

--------------------------------------------------------------------------------
--Function Name : DBG_COND
--Purpose       : write to DEBUG_MSG count of records to validate
--------------------------------------------------------------------------------
procedure DBG_COND(I_program varchar2, I_pid number, I_status varchar2, I_status_next varchar2, I_cond varchar2, I_entity_type varchar2)
is
  ---
  L_count     number := 0;
  ---
  L_text  varchar2(4000);
  --
BEGIN
  ---
  -- To debug, this specific procedure DBG conf must be enabled
  -- And... the program that called this should also have debug enabled
  -- Note: some parameters might be redundant...
  ---
  if DBG_SQL.GP_DBG_OBJ.count <= 0 then
    return;
  end if;
  if not DBG_SQL.GP_DBG_OBJ.exists('XXADEO_ITEM_LFCY_VAL_IS_SQL.DBG_COND') then
    return;
  end if;
  if not DBG_SQL.GP_DBG_OBJ.exists(I_program) then
    return;
  end if;
  ---
  L_text := 'DBG_COND: PID=' || to_char(I_pid) || ',STATUS=' || I_status || ', COND=' || I_cond || ', ENTITY_TYPE=' || I_entity_type;
  ---
  ---
  -- count and log the number of rows that should be affected by this condition (I_cond) evaluation
  ---
  select count(1)
    into L_count
    from xxadeo_v_lfcy_work vw
  where vw.process_id = I_pid
    and vw.lfcy_cond_id = I_cond
    and vw.lfcy_entity_type = I_entity_type
    --and vw.lfcy_status = I_status
    and nvl(vw.val_status,'X') <> 'Y'
    and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
  ;
  ---
  DBG_SQL.MSG(I_program, L_text || ', #ROWS=' || to_char(L_count));
  ---
end;
--------------------------------------------------------------------------------
--Function Name : DBG_LFCY
--Purpose       : write to DEBUG_MSG table WORK and ITEM COND records
--------------------------------------------------------------------------------
procedure DBG_ITEM(I_program varchar2, I_pid number)
is
  ---
  L_item          xxadeo_lfcy_work.item%type;
  L_entity        xxadeo_lfcy_work.item%type;
  L_entity_type   xxadeo_lfcy_work.item%type;
  ---
  L_work xxadeo_lfcy_work%rowtype;
  L_cond xxadeo_lfcy_item_cond%rowtype;
  ---
  L_text  varchar2(4000);
  --
BEGIN
  ---
  if not nvl(xxadeo_item_life_cycle_sql.G_debug_item_ind,FALSE) then
    return;
  end if;
  ---
  L_item := xxadeo_item_life_cycle_sql.G_debug_item;
  L_entity := xxadeo_item_life_cycle_sql.G_debug_entity;
  L_entity_type := xxadeo_item_life_cycle_sql.G_debug_entity_type;
  ---
  L_text := 'DBG_ITEM: PID=' || to_char(I_pid) || ',ITEM=' || L_item || ', ENTITY_TYPE=' || L_entity_type || ', ENTITY=' || L_entity;
  ---
  L_text := L_text || chr(10) || 'WORK';
  for r in (select * from xxadeo_lfcy_work where item = L_item and lfcy_entity = L_entity and lfcy_entity_type = L_entity_type) loop
    L_text := L_text || chr(10) || 'STATUS=' || r.lfcy_status || ', PROCESS_ID=' || to_char(r.process_id);
  end loop;
  ---
  L_text := L_text || chr(10) || 'CONDs';
  for r in (select * from xxadeo_lfcy_item_cond where item = L_item and lfcy_entity = L_entity and lfcy_entity_type = L_entity_type) loop
    L_text := L_text || chr(10) || 'COND=' || r.lfcy_cond_id || ', VAL_STATUS=' || r.val_status || ', PROCESS_ID=' || to_char(r.process_id);
  end loop;
  ---
  DBG_SQL.MSG(I_program, L_text);
  ---
end;

--------------------------------------------------------------------------------
-- IS-INIT-01
-- LINK_TERM_DT_ITEM_SUPP >= vdate or null
--------------------------------------------------------------------------------
FUNCTION IS_INIT_01( O_error_message   IN OUT  varchar2,
                     O_rule_changes    OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IS_INIT_01';
  ---
  L_sql                 varchar2(3000) := '
    merge into xxadeo_lfcy_item_cond t0
      using (
        select  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id            rule_id
          from xxadeo_v_lfcy_work           vw
               ,xxadeo_mom_dvm              xmd
               ,cfa_attrib                  cfaa
               ,item_supplier_cfa_ext       iscfa
        where vw.process_id = :1
          and vw.lfcy_cond_id = ''IS-INIT-01''
          and vw.lfcy_entity_type = ''IS''
          and nvl(vw.val_status,''X'') <> ''Y''
          and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
          and xmd.parameter = ''' || xxadeo_item_life_cycle_sql.CONST$CFAKEY_LnkTermDtItemSupp || '''
          and nvl(xmd.bu,-1) = -1
          and xmd.func_area = ''CFA''
          and cfaa.attrib_id = xmd.value_1
          and iscfa.item = vw.item
          and iscfa.supplier = vw.lfcy_entity
          and iscfa.group_id = cfaa.group_id
          and nvl(iscfa.' || xxadeo_item_life_cycle_sql.GV_CFACOL_LnkTermDtItemSupp || ',get_vdate+1) >= get_vdate
        ) src
      on (
        t0.item = src.item
        and t0.lfcy_entity = src.lfcy_entity
        and t0.lfcy_entity_type = src.lfcy_entity_type
        and t0.lfcy_cond_id = src.rule_id
      )
      when matched then
        update
          set val_status = ''Y''
              ,error_message = null
              ,last_val_dt = sysdate
              ,last_update_datetime = sysdate
  ';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IS_STOP_01');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  execute immediate L_sql using I_pid;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IS_INIT_01;
--------------------------------------------------------------------------------
-- IS-ACT-01
-- The supplier must be the Active Purchase Site (commercial site)
--------------------------------------------------------------------------------
FUNCTION IS_ACT_01( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IS_ACT_01';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IS_ACT_01');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  DBG_COND(L_program, I_pid, 'IS-INIT', 'IS-ACT', 'IS-ACT-01', 'IS');
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
      select  vw.item
              ,vw.lfcy_entity
              ,vw.lfcy_entity_type
              ,vw.lfcy_cond_id            rule_id
        from xxadeo_v_lfcy_work           vw
             ,sups                        s
      where vw.process_id = I_pid
        and vw.lfcy_cond_id = 'IS-ACT-01'
        and vw.lfcy_entity_type = 'IS'
        --and vw.lfcy_status_next = 'IS-ACT'
        and nvl(vw.val_status,'X') <> 'Y'
        and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
        and s.supplier = vw.lfcy_entity
        and s.sup_status = 'A'
        and exists (select 1
                      from addr
                     where addr.key_value_1 = to_char(vw.lfcy_entity)
                       and addr.addr_type = xxadeo_item_life_cycle_sql.GV_VAL_PurchasingSuppSite
                    )
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'Y'
            ,last_val_dt = sysdate
            ,error_message = null
            ,last_update_datetime = sysdate;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  O_result := null;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    DBG(L_program, 'Exception: ' || O_error_message);
    ---
    return FALSE;
END IS_ACT_01;
--------------------------------------------------------------------------------
-- IS-ACT-02
-- Presumption of Quality Certification Received
--------------------------------------------------------------------------------
FUNCTION IS_ACT_02( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IS_ACT_02';
  L_count     number := 0;
  ---
  L_sql                 varchar2(4000) := '
    merge into xxadeo_lfcy_item_cond t0
      using (
        select  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id            rule_id
          from xxadeo_v_lfcy_work           vw
        where vw.process_id = :1
          and vw.lfcy_cond_id = ''IS-ACT-02''
          and vw.lfcy_entity_type = ''IS''
          --and vw.lfcy_status_next = ''IS-ACT''
          and nvl(vw.val_status,''X'') <> ''Y''
          and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
          and exists (select 1
                        from sups                         s04
                             ,sups                        s06
                             ,addr
                             ,partner_org_unit            pou04
                             ,partner_org_unit            pou06
                             ,xxadeo_bu_ou                xbo04
                             ,xxadeo_bu_ou                xbo06
                             ,xxadeo_mom_dvm              xmd
                             ,cfa_attrib                  cfaa
                             ,sups_cfa_ext                scfa
                       where 1=1
                         and s04.supplier = vw.lfcy_entity
                         and s06.supplier_parent = s04.supplier_parent
                         and s04.supplier <> s06.supplier
                         ---
                         and addr.key_value_1 = to_char(s06.supplier)
                         and addr.module = ''SUPP''
                         and addr.addr_type = ''' || xxadeo_item_life_cycle_sql.GV_VAL_PaymentSuppSite || '''
                         ---
                         and pou04.partner = s04.supplier
                         and pou06.partner = s06.supplier
                         ---
                         and xbo04.ou = pou04.org_unit_id
                         and xbo06.ou = pou06.org_unit_id
                         ---
                         and xbo04.bu = xbo06.bu
                         ---
                         and xmd.parameter = ''' || xxadeo_item_life_cycle_sql.CONST$CFAKEY_ContrctSignSupp || '''
                         and nvl(xmd.bu,-1) = -1
                         and xmd.func_area = ''CFA''
                         and cfaa.attrib_id = xmd.value_1
                         and scfa.supplier = s06.supplier
                         and scfa.group_id = cfaa.group_id
                         and nvl(scfa.' || xxadeo_item_life_cycle_sql.GV_CFACOL_ContrctSignSupp || ', ''N'') = ''Y''
                      )
          group by  vw.item
                    ,vw.lfcy_entity
                    ,vw.lfcy_entity_type
                    ,vw.lfcy_cond_id
        ) src
      on (
        t0.item = src.item
        and t0.lfcy_entity = src.lfcy_entity
        and t0.lfcy_entity_type = src.lfcy_entity_type
        and t0.lfcy_cond_id = src.rule_id
      )
      when matched then
        update
          set val_status = ''Y''
              ,error_message = null
              ,last_val_dt = sysdate
              ,last_update_datetime = sysdate
  ';

BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IS_ACT_02');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG(L_program, 'IS-ACT-02: Start. I_pid=' || to_char(I_pid));
  ---
  DBG_ITEM(L_program, I_pid);
  DBG_COND(L_program, I_pid, 'IS-INIT', 'IS-ACT', 'IS-ACT-02', 'IS');
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  execute immediate L_sql using I_pid;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IS_ACT_02;
--------------------------------------------------------------------------------
-- IS-ACT-03
--------------------------------------------------------------------------------
FUNCTION IS_ACT_03( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IS_ACT_03';
  ---
  L_sql                 varchar2(3000) := '
    merge into xxadeo_lfcy_item_cond t0
      using (
        select  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id            rule_id
          from xxadeo_v_lfcy_work           vw
               ,xxadeo_mom_dvm              xmd
               ,cfa_attrib                  cfaa
               ,item_supplier_cfa_ext       iscfa
        where vw.process_id = :1
          and vw.lfcy_cond_id = ''IS-ACT-03''
          and vw.lfcy_entity_type = ''IS''
          --and vw.lfcy_status_next = ''IS-ACT''
          and nvl(vw.val_status,''X'') <> ''Y''
          and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
          and xmd.parameter = ''' || xxadeo_item_life_cycle_sql.CONST$CFAKEY_StatusQCSupp || '''
          and nvl(xmd.bu,-1) = -1
          and xmd.func_area = ''CFA''
          and cfaa.attrib_id = xmd.value_1
          and iscfa.item = vw.item
          and iscfa.supplier = vw.lfcy_entity
          and iscfa.group_id = cfaa.group_id
          and nvl(iscfa.' || xxadeo_item_life_cycle_sql.GV_CFACOL_StatusQCSupp || ', ''0'') in '
            || xxadeo_item_life_cycle_sql.GV_CFAVAL_StatusQCSuppPre || '
          and exists (select 1
                        from addr
                       where addr.key_value_1 = to_char(vw.lfcy_entity)
                         and addr.addr_type = ''' || xxadeo_item_life_cycle_sql.GV_VAL_PurchasingSuppSite || '''
                      )
        ) src
      on (
        t0.item = src.item
        and t0.lfcy_entity = src.lfcy_entity
        and t0.lfcy_entity_type = src.lfcy_entity_type
        and t0.lfcy_cond_id = src.rule_id
      )
      when matched then
        update
          set val_status = ''Y''
              ,error_message = null
              ,last_val_dt = sysdate
              ,last_update_datetime = sysdate
  ';

BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IS_ACT_03');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  execute immediate L_sql using I_pid;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IS_ACT_03;
--------------------------------------------------------------------------------
-- ...
--------------------------------------------------------------------------------
FUNCTION IS_ACT_04( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IS_ACT_04';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IS_ACT_04');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
      select  vw.item
              ,vw.lfcy_entity
              ,vw.lfcy_entity_type
              ,vw.lfcy_cond_id          rule_id
       from xxadeo_v_lfcy_work          vw
      where vw.process_id = I_pid
        and vw.lfcy_cond_id = 'IS-ACT-04'
        and vw.lfcy_entity_type = 'IS'
        --and vw.lfcy_status_next = 'IS-ACT'
        and nvl(vw.val_status,'X') <> 'Y'
        and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
        and exists (select 1
                      from item_supp_country_dim iccd
                     where iccd.item = vw.item
                       and iccd.supplier = vw.lfcy_entity
                       and iccd.dim_object = 'CA'
                    )
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'Y'
            ,error_message = null
            ,last_val_dt = sysdate
            ,last_update_datetime = sysdate;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  O_result := null;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IS_ACT_04;
--------------------------------------------------------------------------------
-- ...
--------------------------------------------------------------------------------
FUNCTION IS_ACT_05( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IS_ACT_05';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IS_ACT_05');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
      select  vw.item
              ,vw.lfcy_entity
              ,vw.lfcy_entity_type
              ,vw.lfcy_cond_id          rule_id
       from xxadeo_v_lfcy_work          vw
      where vw.process_id = I_pid
        and vw.lfcy_cond_id = 'IS-ACT-05'
        and vw.lfcy_entity_type = 'IS'
        --and vw.lfcy_status_next = 'IS-ACT'
        and nvl(vw.val_status,'X') <> 'Y'
        and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
        and exists (select 1
                      from item_supp_country icc
                     where icc.item = vw.item
                       and icc.supplier = vw.lfcy_entity
                       and icc.unit_cost > 0
                    )
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'Y'
            ,error_message = null
            ,last_val_dt = sysdate
            ,last_update_datetime = sysdate;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  O_result := null;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IS_ACT_05;
--------------------------------------------------------------------------------
-- IS-ACT-06
--------------------------------------------------------------------------------
FUNCTION IS_ACT_06( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IS_ACT_06';
  ---
  L_sql                 varchar2(3000) := '
    merge into xxadeo_lfcy_item_cond t0
    using (
        select  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id            rule_id
          from xxadeo_v_lfcy_work           vw
               ,xxadeo_mom_dvm              xmd
               ,cfa_attrib                  cfaa
               ,sups_cfa_ext                scfa
               ,deps                        d
               ,item_master                 im
        where vw.process_id = :1
          and vw.lfcy_cond_id = ''IS-ACT-06''
          and vw.lfcy_entity_type = ''IS''
          --and vw.lfcy_status_next = ''IS-ACT''
          and nvl(vw.val_status,''X'') <> ''Y''
          and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
          and xmd.parameter = ''' || xxadeo_item_life_cycle_sql.CONST$CFAKEY_DepartmentSupp || '''
          and nvl(xmd.bu,-1) = -1
          and xmd.func_area = ''CFA''
          and cfaa.attrib_id = xmd.value_1
          and scfa.supplier = vw.lfcy_entity
          and scfa.group_id = cfaa.group_id
          and im.item = vw.item
          and d.dept = im.dept
          and scfa.' || xxadeo_item_life_cycle_sql.GV_CFACOL_DepartmentSupp || ' = d.group_no
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = ''Y''
            ,error_message = null
            ,last_val_dt = sysdate
            ,last_update_datetime = sysdate
    ';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IS_ACT_06');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  execute immediate L_sql using I_pid;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IS_ACT_06;
--------------------------------------------------------------------------------
-- IS-ACT-07
-- tem-Supplier-Country must be aligned with the Supplier Country based on
-- the country in the supplier PO address
--------------------------------------------------------------------------------
FUNCTION IS_ACT_07( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IS_ACT_07';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IS_ACT_07');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
      select  vw.item
              ,vw.lfcy_entity
              ,vw.lfcy_entity_type
              ,vw.lfcy_cond_id            rule_id
        from xxadeo_v_lfcy_work           vw
             ,item_supp_country           icc
             ,addr
      where vw.process_id = I_pid
        and vw.lfcy_cond_id = 'IS-ACT-07'
        and vw.lfcy_entity_type = 'IS'
        --and vw.lfcy_status_next = 'IS-ACT'
        and nvl(vw.val_status,'X') <> 'Y'
        and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
        and icc.item = vw.item
        and icc.supplier = vw.lfcy_entity
        and addr.key_value_1 = to_char(vw.lfcy_entity)
        and addr.addr_type = xxadeo_item_life_cycle_sql.GV_VAL_PurchasingSuppSite
        and addr.country_id = icc.origin_country_id
      group by vw.item
              ,vw.lfcy_entity
              ,vw.lfcy_entity_type
              ,vw.lfcy_cond_id
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'Y'
            ,error_message = null
            ,last_val_dt = sysdate
            ,last_update_datetime = sysdate;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  O_result := null;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IS_ACT_07;
--------------------------------------------------------------------------------
-- IS-ACT_08
-- LINK_TERM_DT_ITEM_SUPP is null or > vdate
--------------------------------------------------------------------------------
FUNCTION IS_ACT_08( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IS_ACT_08';
  ---
  L_sql                 varchar2(3000) := '
    merge into xxadeo_lfcy_item_cond t0
      using (
        select  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id            rule_id
          from xxadeo_v_lfcy_work           vw
               ,xxadeo_mom_dvm              xmd
               ,cfa_attrib                  cfaa
               ,item_supplier_cfa_ext       iscfa
        where vw.process_id = :1
          and vw.lfcy_cond_id = ''IS-ACT-08''
          and vw.lfcy_entity_type = ''IS''
          --and vw.lfcy_status_next = ''IS-ACT''
          and nvl(vw.val_status,''X'') <> ''Y''
          and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
          and xmd.parameter = ''' || xxadeo_item_life_cycle_sql.CONST$CFAKEY_LnkTermDtItemSupp || '''
          and nvl(xmd.bu,-1) = -1
          and xmd.func_area = ''CFA''
          and cfaa.attrib_id = xmd.value_1
          and iscfa.item = vw.item
          and iscfa.supplier = vw.lfcy_entity
          and iscfa.group_id = cfaa.group_id
          and nvl(iscfa.' || xxadeo_item_life_cycle_sql.GV_CFACOL_LnkTermDtItemSupp || ',get_vdate+1) >= get_vdate
        ) src
      on (
        t0.item = src.item
        and t0.lfcy_entity = src.lfcy_entity
        and t0.lfcy_entity_type = src.lfcy_entity_type
        and t0.lfcy_cond_id = src.rule_id
      )
      when matched then
        update
          set val_status = ''Y''
              ,error_message = null
              ,last_val_dt = sysdate
              ,last_update_datetime = sysdate
  ';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IS_ACT_08');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  execute immediate L_sql using I_pid;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IS_ACT_08;
--------------------------------------------------------------------------------
-- IS-STOP-01
-- LINK_TERM_DT_ITEM_SUPP <= vdate
--------------------------------------------------------------------------------
FUNCTION IS_STOP_01( O_error_message   IN OUT  varchar2,
                     O_rule_changes    OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IS_STOP_01';
  ---
  L_sql                 varchar2(3000) := '
    merge into xxadeo_lfcy_item_cond t0
      using (
        select  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id            rule_id
          from xxadeo_v_lfcy_work           vw
               ,xxadeo_mom_dvm              xmd
               ,cfa_attrib                  cfaa
               ,item_supplier_cfa_ext       iscfa
        where vw.process_id = :1
          and vw.lfcy_cond_id = ''IS-STOP-01''
          and vw.lfcy_entity_type = ''IS''
          --and vw.lfcy_status_next = ''IS-STOP''
          and nvl(vw.val_status,''X'') <> ''Y''
          and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
          and xmd.parameter = ''' || xxadeo_item_life_cycle_sql.CONST$CFAKEY_LnkTermDtItemSupp || '''
          and nvl(xmd.bu,-1) = -1
          and xmd.func_area = ''CFA''
          and cfaa.attrib_id = xmd.value_1
          and iscfa.item = vw.item
          and iscfa.supplier = vw.lfcy_entity
          and iscfa.group_id = cfaa.group_id
          and nvl(iscfa.' || xxadeo_item_life_cycle_sql.GV_CFACOL_LnkTermDtItemSupp || ',get_vdate+1) <= get_vdate
        ) src
      on (
        t0.item = src.item
        and t0.lfcy_entity = src.lfcy_entity
        and t0.lfcy_entity_type = src.lfcy_entity_type
        and t0.lfcy_cond_id = src.rule_id
      )
      when matched then
        update
          set val_status = ''Y''
              ,error_message = null
              ,last_val_dt = sysdate
              ,last_update_datetime = sysdate
  ';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IS_STOP_01');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  execute immediate L_sql using I_pid;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IS_STOP_01;

END XXADEO_ITEM_LFCY_VAL_IS_SQL;

/
