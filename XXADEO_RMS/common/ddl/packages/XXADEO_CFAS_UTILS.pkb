create or replace package body XXADEO_CFAS_UTILS is

  PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                          IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cause          IN VARCHAR2,
                          I_program        IN VARCHAR2);

  -------------------------------------------------------------------------------------------------------
  -- Name: VALIDATE_CFA
  -- Description: verify if value received is valid when CFA is a List of Values or CheckBox
  -------------------------------------------------------------------------------------------------------  

  Function VALIDATE_CFA(I_attrib_id IN INTEGER, I_value IN VARCHAR2)
    return BOOLEAN is
  
    L_ui_widget cfa_attrib.ui_widget%Type;
    L_code_type cfa_Attrib.code_type%TYPE;
    L_exist     NUMBER;
  BEGIN
    select ui_widget, code_type
      into L_ui_widget, L_code_type
      from cfa_attrib
     where attrib_id = I_attrib_id;
  
    if L_ui_widget = 'LI' then
      Select count(*)
        into L_exist
        from code_detail
       where code_type = L_code_type
         and code = I_value;
    
      if L_exist = 0 then
        return false;
      end if;
      return true;
    
    elsif L_ui_widget = 'CB' then
      if I_value not in ('N', 'Y') then
        return false;
      end if;
    
    end if;
    return true;
  END;

  -------------------------------------------------------------------------------------------------------
  -- Name:GET_CFAS_EXIST
  -- Description: This procedure is responsible to get all cfas where value is not null
  -------------------------------------------------------------------------------------------------------
  PROCEDURE GET_CFAS_EXIST(I_Entity        IN VARCHAR2,
                           I_primarykey    IN VARCHAR2,
                           O_LIST_CFAS     OUT XXADEO_CFA_DETAILS_TBL,
                           O_status_code   IN OUT VARCHAR2,
                           O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE) is
  
    CURSOR C_key(l_base_rms_table varchar2) is
      select key_col, data_type, custom_ext_table
        from cfa_ext_entity_key key, cfa_ext_entity ent
       where key.base_rms_table = l_base_rms_table
         and ent.base_rms_table = l_base_rms_table
       order by key_number;
  
    type key_rec is record(
      col        varchar2(30),
      data_type  varchar2(30),
      table_name varchar2(30));
  
    type key_tbl is table of key_rec;
  
    L_program               VARCHAR2(50) := 'XXADEO_CFAS_UTILS.GET_CFAS_EXIST';
    V_key_tbl               key_tbl;
    L_cfas_tbl              XXADEO_CFA_DETAILS_TBL;
    V_response              xxadeo_cfa_details_tbl;
    L_primary_key           varchar2(4000);
    L_key                   varchar2(100);
    L_count                 number;
    L_clause_where          varchar2(4000);
    L_sql                   varchar2(4000);
    L_value                 varchar2(100);
    L_exist                 number;
    L_rec                   xxadeo_cfa_details;
    L_two_pipes_encountered number;
  
  BEGIN
    O_status_code := 'S';
  
    l_primary_key := I_primarykey || '|';
    select INSTR(l_primary_key, '||', 1)
      into l_two_pipes_encountered
      from dual;
  
    if l_two_pipes_encountered > 0 then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    
    end if;
    select REGEXP_COUNT(l_primary_key, '\|') into L_count from dual;
  
    OPEN C_key(I_Entity);
    FETCH C_key BULK COLLECT
      INTO V_key_tbl;
    CLOSE C_key;
  
    if V_key_tbl is null or V_key_tbl.count != L_count then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    end if;
  
    L_clause_where := '';
    for i in 1 .. L_count loop
      select SUBSTR(l_primary_key, 1, INSTR(l_primary_key, '|', 1) - 1),
             SUBSTR(l_primary_key,
                    INSTR(l_primary_key, '|', 1) + 1,
                    length(l_primary_key))
        into L_key, L_primary_key
        from dual;
      if V_key_tbl(i)
       .data_type = 'VARCHAR2' or V_key_tbl(i).data_type = 'DATE' then
        L_clause_where := L_clause_where || V_key_tbl(i).col || '= ''' ||
                          L_key || ''' AND ';
      else
        L_clause_where := L_clause_where || V_key_tbl(i).col || '= ' ||
                          L_key || ' AND ';
      end if;
    end loop;
  
    GET_ALL_CFAS(I_entity, L_cfas_tbl, O_status_code, O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    if L_cfas_tbl is not null and L_cfas_tbl.count > 0 then
      V_response := XXADEO_CFA_DETAILS_TBL();
      for i in 1 .. L_cfas_tbl.count loop
      
        L_sql := 'Select count(*) from ' || V_key_tbl(1).table_name ||
                 ' where ' || L_clause_where || ' group_id = ' || L_cfas_tbl(i)
                .cfa_group;
        EXECUTE IMMEDIATE L_sql
          into L_exist;
      
        if L_exist > 0 then
        
          L_sql := 'Select nvl(TO_CHAR( ' || L_cfas_tbl(i).cfa_col ||
                   '),''|'') from ' || V_key_tbl(1).table_name || ' where ' ||
                   L_clause_where || ' group_id = ' || L_cfas_tbl(i)
                  .cfa_group;
        
          EXECUTE IMMEDIATE L_sql
            into L_value;
        
          if L_value != '|' then
            L_rec := L_cfas_tbl(i);
          
            if L_cfas_tbl(i).CFA_DATA_TYPE like '%VARCHAR%' OR L_cfas_tbl(i)
               .CFA_DATA_TYPE like 'NUMBER' then
              L_rec.value := L_value;
            elsif L_cfas_tbl(i).CFA_DATA_TYPE like '%DATE%' then
              L_rec.value_date := L_value;
            end if;
            V_response.extend();
            V_response(V_response.last) := L_rec;
          end if;
        end if;
      
      end loop;
      O_LIST_CFAS := V_response;
    
    end if;
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END GET_CFAS_EXIST;

  -------------------------------------------------------------------------------------------------------
  -- Name: GET_ALL_CFAS
  -- Description: This procedure is responsible to get all cfas related with one entity
  -------------------------------------------------------------------------------------------------------
  PROCEDURE GET_ALL_CFAS(I_Entity        IN VARCHAR2,
                         O_CFAS          OUT XXADEO_CFA_DETAILS_TBL,
                         O_status_code   IN OUT VARCHAR2,
                         O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE) is
  
    CURSOR C_cfas is
      select XXADEO_CFA_DETAILS(attrib_id,
                                cfa_name,
                                cfa_group,
                                cfa_col,
                                cfa_data_type,
                                value,
                                value_date)
        from (select a.attrib_id,
                     a.view_col_name    as cfa_name,
                     g.group_id         as cfa_group,
                     a.storage_col_name as cfa_col,
                     a.data_type        as cfa_data_type,
                     null               as value,
                     null               as value_date
                from cfa_attrib a
                join cfa_attrib_group g
                  on a.group_id = g.group_id
                join cfa_attrib_group_set s
                  on g.group_set_id = s.group_set_id
                join cfa_ext_entity e
                  on s.ext_entity_id = e.ext_entity_id
               where e.base_rms_table = I_Entity
               order by a.attrib_id);
  
    L_program  VARCHAR2(50) := 'XXADEO_CFAS_UTILS.GET_ALL_CFAS';
    V_cfas_tbl XXADEO_CFA_DETAILS_TBL;
  
  BEGIN
  
    OPEN C_cfas;
    FETCH C_cfas BULK COLLECT
      INTO V_cfas_tbl;
    CLOSE C_cfas;
  
    O_CFAS := V_cfas_tbl;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END GET_ALL_CFAS;

  -------------------------------------------------------------------------------------------------------
  -- Name: DELETE_CFAS
  -- Description: Responsible to "delete" one list of cfa values for one entity 
  --              "delete" = update this field to null
  ------------------------------------------------------------------------------------------------------- 
  PROCEDURE DELETE_CFAS(I_Entity        IN VARCHAR2,
                        I_primarykey    IN VARCHAR2,
                        I_cfas          IN XXADEO_CFA_DETAILS_TBL,
                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_message IN OUT VARCHAR2) IS
  
    L_table                 varchar2(30);
    L_primary_key           varchar2(100);
    L_key                   varchar2(100);
    l_two_pipes_encountered number;
    L_count                 number;
    L_clause_where          varchar2(400);
    L_sql                   varchar2(4000);
    L_program               VARCHAR2(50) := 'XXADEO_CFAS_UTILS.DELETE_CFAS';
  
    CURSOR C_key(l_base_rms_table varchar2) is
      select key_col, data_type
        from cfa_ext_entity_key
       where base_rms_table = l_base_rms_table
       order by key_number;
  
    Type key_rec is record(
      col       varchar2(30),
      data_type varchar2(30));
  
    Type key_tbl is table of key_rec;
  
    V_key_tbl key_tbl;
  
  BEGIN
    O_status_code := 'S';
    l_primary_key := I_primarykey || '|';
    select INSTR(l_primary_key, '||', 1)
      into l_two_pipes_encountered
      from dual;
  
    if l_two_pipes_encountered > 0 then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    end if;
    select REGEXP_COUNT(l_primary_key, '\|') into L_count from dual;
  
    OPEN C_key(I_Entity);
    FETCH C_key BULK COLLECT
      INTO V_key_tbl;
    CLOSE C_key;
  
    if V_key_tbl is null or V_key_tbl.count != L_count then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    end if;
  
    L_clause_where := '';
    for i in 1 .. L_count loop
      select SUBSTR(l_primary_key, 1, INSTR(l_primary_key, '|', 1) - 1),
             SUBSTR(l_primary_key,
                    INSTR(l_primary_key, '|', 1) + 1,
                    length(l_primary_key))
        into L_key, L_primary_key
        from dual;
      if V_key_tbl(i)
       .data_type = 'VARCHAR2' or V_key_tbl(i).data_type = 'DATE' then
        L_clause_where := L_clause_where || V_key_tbl(i).col || '= ''' ||
                          L_key || ''' AND ';
      else
        L_clause_where := L_clause_where || V_key_tbl(i).col || '= ' ||
                          L_key || ' AND ';
      end if;
    end loop;
  
    select custom_ext_table
      into L_table
      from cfa_ext_entity
     where base_rms_table = I_entity;
  
    for i in 1 .. I_cfas.count loop
    
      L_sql := 'UPDATE ' || L_table || ' SET ' || I_cfas(i).cfa_col ||
               '= NULL WHERE ';
      L_sql := L_sql || L_clause_where || ' group_id=' || I_cfas(i)
              .cfa_group;
    
      EXECUTE IMMEDIATE L_sql;
    
    end loop;
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END DELETE_CFAS;

  -------------------------------------------------------------------------------------------------------
  -- Name: SET_CFAS
  -- Description: Insert and/or update list of CFAS received for one entity
  -------------------------------------------------------------------------------------------------------
  PROCEDURE SET_CFAS(I_Entity        IN VARCHAR2,
                     I_primarykey    IN VARCHAR2,
                     I_message       IN XXADEO_CFA_DETAILS_TBL,
                     O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_message IN OUT VARCHAR2) IS
  
    L_program   VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.SET_CFAS';
    L_sql       VARCHAR2(4000);
    L_sql_exist VARCHAR2(4000);
    L_exist     NUMBER;
    l_wrong_key EXCEPTION;
  
    l_cfa_name              cfa_attrib.view_col_name%TYPE;
    l_custom_ext_table      cfa_ext_entity.custom_ext_table%TYPE;
    l_storage_col_name      cfa_attrib.view_col_name%TYPE;
    l_group_id              cfa_attrib_group.group_id%TYPE;
    l_group_name            cfa_attrib_group.group_view_name%TYPE;
    L_primary_key           varchar2(4000);
    L_count                 number;
    L_key                   varchar2(100);
    L_sql_insert_column     varchar2(4000);
    L_sql_insert_value      varchar2(4000);
    L_sql_update            varchar2(4000);
    L_two_pipes_encountered number;
  
    L_message xxadeo_cfa_details_tbl;
  
    CURSOR C_key(l_base_rms_table varchar2) is
      select key_col, data_type
        from cfa_ext_entity_key
       where base_rms_table = l_base_rms_table
       order by key_number;
  
    Type key_rec is record(
      col       varchar2(30),
      data_type varchar2(30));
  
    Type key_tbl is table of key_rec;
  
    V_key_tbl key_tbl;
  
  BEGIN
    O_status_code := 'S';
    l_primary_key := I_primarykey || '|';
    L_message     := I_message;
  
    --check if received Primary Key has null values
    select INSTR(l_primary_key, '||', 1)
      into l_two_pipes_encountered
      from dual;
  
    if l_two_pipes_encountered > 0 then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    end if;
    select REGEXP_COUNT(l_primary_key, '\|') into L_count from dual;
  
    OPEN C_key(I_entity);
    FETCH C_key BULK COLLECT
      INTO V_key_tbl;
    CLOSE C_key;
  
    if V_key_tbl is null or V_key_tbl.count != L_count then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    end if;
    L_sql_insert_column := '';
    L_sql_insert_value  := '';
    L_sql_update        := '';
    for i in 1 .. L_count loop
      L_sql_insert_column := L_sql_insert_column || V_key_tbl(i).col || ', ';
      select SUBSTR(l_primary_key, 1, INSTR(l_primary_key, '|', 1) - 1),
             SUBSTR(l_primary_key,
                    INSTR(l_primary_key, '|', 1) + 1,
                    length(l_primary_key))
        into L_key, L_primary_key
        from dual;
    
      L_sql_insert_value := L_sql_insert_value || ' ''' || L_key || ''', ';
      L_sql_update       := L_sql_update || V_key_tbl(i).col || '= ''' ||
                            L_key || ''' AND ';
    
    end loop;
  
    for i in 1 .. L_message.count loop
      L_sql := '';
    
      select e.custom_ext_table,
             a.storage_col_name,
             a.view_col_name,
             g.group_id,
             g.group_view_name
        into l_custom_ext_table,
             l_storage_col_name,
             l_cfa_name,
             l_group_id,
             l_group_name
        from cfa_attrib a
        join cfa_attrib_group g
          on a.group_id = g.group_id
        join cfa_attrib_group_set s
          on g.group_set_id = s.group_set_id
        join cfa_ext_entity e
          on s.ext_entity_id = e.ext_entity_id
       where attrib_id = L_message(i).cfa_id;
    
      if L_message(i).value is not null then
      
        if not VALIDATE_CFA(L_message(i).cfa_id, L_message(i).value) then
          O_status_code   := 'E';
          O_error_message := 'Value inserted for CFA ' || L_message(i)
                            .cfa_id || ' is not valid.';
          raise PROGRAM_ERROR;
        end if;
      
      end if;
    
      L_sql_exist := 'select count(*)
           from ' || l_custom_ext_table || '
           where ' || L_sql_update || '
                 group_id = ' || L_group_id;
    
      EXECUTE IMMEDIATE L_sql_exist
        into L_exist;
    
      if L_exist = 0 then
        if L_storage_col_name like '%DATE%' then
          L_sql := 'insert into ' || l_custom_ext_table || ' ( ' ||
                   L_sql_insert_column || ' group_id,' ||
                   l_storage_col_name || ') values ( ' ||
                   L_sql_insert_value || L_group_id || ', ''' || L_message(i)
                  .value_date || ''' )';
        else
          L_sql := 'insert into ' || l_custom_ext_table || ' ( ' ||
                   L_sql_insert_column || ' group_id,' ||
                   l_storage_col_name || ') values ( ' ||
                   L_sql_insert_value || L_group_id || ', ''' || L_message(i)
                  .value || ''' )';
        end if;
      
      elsif L_exist = 1 then
        if L_storage_col_name like '%DATE%' then
          L_sql := 'update ' || l_custom_ext_table || '
                   set ' || l_storage_col_name || '= ''' || L_message(i)
                  .value_date || ''' where ' || L_sql_update ||
                   ' group_id=' || L_group_id || '';
        else
          L_sql := 'update ' || l_custom_ext_table || '
                   set ' || l_storage_col_name || '= ''' || L_message(i)
                  .value || ''' where ' || L_sql_update || ' group_id=' ||
                   L_group_id || '';
        end if;
      end if;
    
      EXECUTE IMMEDIATE L_sql;
    
    end loop;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END SET_CFAS;

  -------------------------------------------------------------------------------------------------------
  -- Name: INITIALIZE_CFAS_TO_VIEW
  -- Description: responsible to initialize CFAS that are in same view of list received
  -------------------------------------------------------------------------------------------------------
  PROCEDURE INICIALIZE_CFAS_TO_VIEW(I_cfas          IN XXADEO_CFA_DETAILS_TBL,
                                    I_primarykey    IN VARCHAR2,
                                    I_entity        IN VARCHAR2,
                                    O_status_code   IN OUT VARCHAR2,
                                    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE) IS
  
    L_string_ids            VARCHAR2(1000) := '';
    L_query                 VARCHAR2(2000) := '';
    l_primary_key           VARCHAr2(1000);
    L_two_pipes_encountered number;
    L_key                   VARCHAR2(100);
    L_exist                 NUMBER;
    L_sql_where             VARCHAr2(1000);
    L_verify_exist          VARCHAR2(2000);
    L_update_sql            VARCHAR2(2000);
  
    CURSOR C_key(l_base_rms_table varchar2) is
      select key_col, data_type
        from cfa_ext_entity_key
       where base_rms_table = l_base_rms_table
       order by key_number;
  
    type groups_rec is record(
      group_id  number(10),
      table_cfa varchar2(30));
  
    type t_groups_tbl is table of groups_rec;
  
    Type key_rec is record(
      col       varchar2(30),
      data_type varchar2(30));
  
    Type key_tbl is table of key_rec;
  
    V_key_tbl           key_tbl;
    L_groups_tbl        t_groups_tbl;
    L_sql_insert_column VARCHAR2(4000);
    L_sql_insert_value  VARCHAR2(4000);
    L_sql_insert        VARCHAR2(4000);
    L_count             NUMBER;
  BEGIN
  
    l_primary_key := I_primarykey || '|';
    select INSTR(l_primary_key, '||', 1)
      into l_two_pipes_encountered
      from dual;
  
    if l_two_pipes_encountered > 0 then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    
    end if;
  
    select REGEXP_COUNT(l_primary_key, '\|') into L_count from dual;
  
    L_string_ids := '';
    for i in 1 .. I_cfas.count loop
      if i = I_cfas.count then
        L_string_ids := L_string_ids || I_cfas(i).cfa_id;
      else
        L_string_ids := L_string_ids || I_cfas(i).cfa_id || ', ';
      end if;
    
    end loop;
  
    L_query := 'select cag.group_id, custom_ext_table
  from cfa_attrib_group cag
  join cfa_attrib_group_set cags
    on cags.group_set_id = cag.group_set_id
  join cfa_ext_entity cee
    on cee.ext_entity_id = cags.ext_entity_id
 where cag.group_set_id in
       (select cag.group_set_id
          from cfa_attrib_group cag
          join cfa_attrib_group_set cags
            on cag.group_set_id = cags.group_set_id
          join cfa_attrib ca
            on ca.group_id = cag.group_id
         where attrib_id in (' || L_string_ids || '))';
  
    Execute immediate L_query BULK COLLECT
      INTO L_groups_tbl;
  
    OPEN C_key(I_entity);
    FETCH C_key BULK COLLECT
      INTO V_key_tbl;
    CLOSE C_key;
    L_sql_insert_column := '';
    L_sql_where         := '';
    for i in 1 .. L_count loop
      L_sql_insert_column := L_sql_insert_column || V_key_tbl(i).col || ', ';
      select SUBSTR(l_primary_key, 1, INSTR(l_primary_key, '|', 1) - 1),
             SUBSTR(l_primary_key,
                    INSTR(l_primary_key, '|', 1) + 1,
                    length(l_primary_key))
        into L_key, L_primary_key
        from dual;
    
      L_sql_insert_value := L_sql_insert_value || ' ''' || L_key || ''', ';
      L_sql_where        := L_sql_where || V_key_tbl(i).col || ' = ''' ||
                            L_key || ''' AND ';
    
    end loop;
  
    for i in 1 .. L_groups_tbl.count loop
    
      if V_key_tbl is null or V_key_tbl.count != L_count then
        O_status_code   := 'E';
        O_error_message := 'Primary Key has no value on last position';
        raise PROGRAM_ERROR;
      end if;
    
      L_verify_exist := 'Select count(*)
      from ' || L_groups_tbl(i).table_cfa ||
                        ' where ' || L_sql_where || ' group_id = ' || L_groups_tbl(i)
                       .group_id;
    
      execute immediate L_verify_exist
        into L_exist;
    
      if L_exist = 0 then
      
        L_sql_insert := 'INSERT INTO ' || L_groups_tbl(i).table_cfa || '(' ||
                        L_sql_insert_column || ' group_id ) values (';
        L_sql_insert := L_sql_insert || L_sql_insert_value || L_groups_tbl(i)
                       .group_id || ')';
      
        execute immediate L_sql_insert;
      
        for col in (select storage_col_name
                      from cfa_attrib
                     where ui_widget = 'CB'
                       and group_id = L_groups_tbl(i).group_id) loop
        
          L_update_sql := 'UPDATE ' || L_groups_tbl(i).table_cfa || ' SET ';
          L_update_sql := L_update_sql || col.storage_col_name ||
                          ' = ''N'' ';
        
          L_update_sql := L_update_sql || ' WHERE ' || L_sql_where ||
                          ' group_id = ' || L_groups_tbl(i).group_id;
        
          execute immediate L_update_sql;
        end loop;
      
      end if;
    
    end loop;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    'XXADEO_CFAS_UTILS.INICIALIZE_CFAS_TO_VIEW');
    
  END INICIALIZE_CFAS_TO_VIEW;

  -------------------------------------------------------------------------------------------------------
  -- Name:GET_CFA_VALUE_NUM
  -- Description: Get value of CFA if value is a number
  -------------------------------------------------------------------------------------------------------
  FUNCTION GET_CFA_VALUE_NUM(I_entity      IN VARCHAR2,
                             I_primary_key IN VARCHAR2,
                             I_cfa_id      IN NUMBER) RETURN NUMBER IS
  
    CURSOR C_key(l_base_rms_table varchar2) is
      select key_col, data_type, custom_ext_table
        from cfa_ext_entity_key key, cfa_ext_entity ent
       where key.base_rms_table = l_base_rms_table
         and ent.base_rms_table = l_base_rms_table
       order by key_number;
  
    type key_rec is record(
      col        varchar2(30),
      data_type  varchar2(30),
      table_name varchar2(30));
  
    type key_tbl is table of key_rec;
  
    L_PROGRAM VARCHAR2(50) := 'XXADEO_CFAS_UTILS.GET_CFA_VALUE_NUM';
  
    V_key_tbl               key_tbl;
    L_primary_key           varchar2(4000);
    L_two_pipes_encountered number;
    L_column                VARCHAR2(30);
    L_data_type             VARCHAR2(30);
    L_sql                   varchar2(4000);
    L_value                 NUMBER;
    L_clause_where          VARCHAR2(4000);
    L_count                 NUMBER;
    L_key                   varchar2(100);
    L_group_id              NUMBER;
    L_exist                 NUMBER;
    O_status_code           VARCHAR2(1);
    O_error_message         VARCHAR2(4000);
  BEGIN
  
    l_primary_key := I_primary_key || '|';
    select INSTR(l_primary_key, '||', 1)
      into l_two_pipes_encountered
      from dual;
  
    if l_two_pipes_encountered > 0 then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    end if;
    select REGEXP_COUNT(l_primary_key, '\|') into L_count from dual;
  
    OPEN C_key(I_Entity);
    FETCH C_key BULK COLLECT
      INTO V_key_tbl;
    CLOSE C_key;
  
    if V_key_tbl is null or V_key_tbl.count != L_count then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    end if;
  
    L_clause_where := '';
    for i in 1 .. L_count loop
      select SUBSTR(l_primary_key, 1, INSTR(l_primary_key, '|', 1) - 1),
             SUBSTR(l_primary_key,
                    INSTR(l_primary_key, '|', 1) + 1,
                    length(l_primary_key))
        into L_key, L_primary_key
        from dual;
      if V_key_tbl(i)
       .data_type = 'VARCHAR2' or V_key_tbl(i).data_type = 'DATE' then
        L_clause_where := L_clause_where || V_key_tbl(i).col || '= ''' ||
                          L_key || ''' AND ';
      else
        L_clause_where := L_clause_where || V_key_tbl(i).col || '= ' ||
                          L_key || ' AND ';
      end if;
    end loop;
  
    SELECT storage_col_name, data_type, group_id
      into L_column, L_data_type, L_group_id
      from cfa_attrib
     where attrib_id = I_cfa_id;
  
    if L_data_type like '%VARCHAR%' or L_data_type like '%DATE%' then
      O_status_code   := 'E';
      O_error_message := 'Data type is not valid.';
      raise PROGRAM_ERROR;
    end if;
  
    L_sql := 'SELECT count(' || L_column || ') from ' || V_key_tbl(1)
            .table_name || ' WHERE ' || L_clause_where || ' GROUP_ID = ' ||
             L_group_id;
    EXECUTE IMMEDIATE L_sql
      into L_exist;
    if L_exist > 0 then
    
      L_sql := 'SELECT ' || L_column || ' from ' || V_key_tbl(1).table_name ||
               ' WHERE ';
      L_sql := L_sql || L_clause_where || ' GROUP_ID = ' || L_group_id;
    
      EXECUTE IMMEDIATE L_sql
        into L_value;
    
      return L_value;
    else
      return null;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END GET_CFA_VALUE_NUM;

  -------------------------------------------------------------------------------------------------------
  -- Name:GET_CFA_VALUE_VARCHAR
  -- Description: Get value of CFA if value is a varchar2
  -------------------------------------------------------------------------------------------------------
  FUNCTION GET_CFA_VALUE_VARCHAR(I_entity      IN VARCHAR2,
                                 I_primary_key IN VARCHAR2,
                                 I_cfa_id      IN NUMBER) RETURN VARCHAR2 IS
  
    CURSOR C_key(l_base_rms_table varchar2) is
      select key_col, data_type, custom_ext_table
        from cfa_ext_entity_key key, cfa_ext_entity ent
       where key.base_rms_table = l_base_rms_table
         and ent.base_rms_table = l_base_rms_table
       order by key_number;
  
    type key_rec is record(
      col        varchar2(30),
      data_type  varchar2(30),
      table_name varchar2(30));
  
    type key_tbl is table of key_rec;
  
    L_PROGRAM               VARCHAR2(50) := 'XXADEO_CFAS_UTILS.GET_CFA_VALUE_VARCHAR';
    V_key_tbl               key_tbl;
    L_primary_key           varchar2(4000);
    L_two_pipes_encountered number;
    L_column                VARCHAR2(30);
    L_data_type             VARCHAR2(30);
    L_sql                   varchar2(4000);
    L_value                 VARCHAR2(100);
    L_clause_where          VARCHAR2(4000);
    L_count                 NUMBER;
    L_key                   varchar2(100);
    L_group_id              NUMBER;
    L_exist                 NUMBER;
    O_status_code           VARCHAR2(1);
    O_error_message         VARCHAR2(4000);
  BEGIN
  
    l_primary_key := I_primary_key || '|';
    select INSTR(l_primary_key, '||', 1)
      into l_two_pipes_encountered
      from dual;
  
    if l_two_pipes_encountered > 0 then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    end if;
    select REGEXP_COUNT(l_primary_key, '\|') into L_count from dual;
  
    OPEN C_key(I_Entity);
    FETCH C_key BULK COLLECT
      INTO V_key_tbl;
    CLOSE C_key;
  
    if V_key_tbl is null or V_key_tbl.count != L_count then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    end if;
  
    L_clause_where := '';
    for i in 1 .. L_count loop
      select SUBSTR(l_primary_key, 1, INSTR(l_primary_key, '|', 1) - 1),
             SUBSTR(l_primary_key,
                    INSTR(l_primary_key, '|', 1) + 1,
                    length(l_primary_key))
        into L_key, L_primary_key
        from dual;
      if V_key_tbl(i)
       .data_type = 'VARCHAR2' or V_key_tbl(i).data_type = 'DATE' then
      
        L_clause_where := L_clause_where || V_key_tbl(i).col || '= ''' ||
                          L_key || ''' AND ';
      else
      
        L_clause_where := L_clause_where || V_key_tbl(i).col || '= ' ||
                          L_key || ' AND ';
      end if;
    end loop;
  
    SELECT storage_col_name, data_type, group_id
      into L_column, L_data_type, L_group_id
      from cfa_attrib
     where attrib_id = I_cfa_id;
  
    if L_data_type like '%NUMBER%' or L_data_type like '%DATE%' then
      O_status_code   := 'E';
      O_error_message := 'Data type is not valid.';
      raise PROGRAM_ERROR;
    end if;
  
    L_sql := 'SELECT count(' || L_column || ') from ' || V_key_tbl(1)
            .table_name || ' WHERE ' || L_clause_where || ' GROUP_ID = ' ||
             L_group_id;
    EXECUTE IMMEDIATE L_sql
      into L_exist;
    if L_exist > 0 then
    
      L_sql := 'SELECT ' || L_column || ' from ' || V_key_tbl(1).table_name ||
               ' WHERE ';
      L_sql := L_sql || L_clause_where || ' GROUP_ID = ' || L_group_id;
    
      EXECUTE IMMEDIATE L_sql
        into L_value;
    
      return L_value;
    else
      return null;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END GET_CFA_VALUE_VARCHAR;

  -------------------------------------------------------------------------------------------------------
  -- Name:GET_CFA_VALUE_DATE
  -- Description: Get value of CFA if value is a date
  -------------------------------------------------------------------------------------------------------
  FUNCTION GET_CFA_VALUE_DATE(I_entity      IN VARCHAR2,
                              I_primary_key IN VARCHAR2,
                              I_cfa_id      IN NUMBER) RETURN DATE IS
  
    CURSOR C_key(l_base_rms_table varchar2) is
      select key_col, data_type, custom_ext_table
        from cfa_ext_entity_key key, cfa_ext_entity ent
       where key.base_rms_table = l_base_rms_table
         and ent.base_rms_table = l_base_rms_table
       order by key_number;
  
    type key_rec is record(
      col        varchar2(30),
      data_type  varchar2(30),
      table_name varchar2(30));
  
    type key_tbl is table of key_rec;
  
    L_PROGRAM VARCHAR2(50) := 'XXADEO_CFAS_UTILS.GET_CFA_VALUE_DATE';
  
    V_key_tbl               key_tbl;
    L_primary_key           varchar2(4000);
    L_two_pipes_encountered number;
    L_column                VARCHAR2(30);
    L_data_type             VARCHAR2(30);
    L_sql                   varchar2(4000);
    L_value                 DATE;
    L_clause_where          VARCHAR2(4000);
    L_count                 NUMBER;
    L_key                   varchar2(100);
    L_group_id              NUMBER;
    L_exist                 NUMBER;
    O_status_code           VARCHAR2(1);
    O_error_message         VARCHAR2(4000);
  
  BEGIN
  
    l_primary_key := I_primary_key || '|';
    select INSTR(l_primary_key, '||', 1)
      into l_two_pipes_encountered
      from dual;
  
    if l_two_pipes_encountered > 0 then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    end if;
    select REGEXP_COUNT(l_primary_key, '\|') into L_count from dual;
  
    OPEN C_key(I_Entity);
    FETCH C_key BULK COLLECT
      INTO V_key_tbl;
    CLOSE C_key;
  
    if V_key_tbl is null or V_key_tbl.count != L_count then
      O_status_code   := 'E';
      O_error_message := 'Primary Key has no value on last position';
      raise PROGRAM_ERROR;
    end if;
  
    L_clause_where := '';
    for i in 1 .. L_count loop
      select SUBSTR(l_primary_key, 1, INSTR(l_primary_key, '|', 1) - 1),
             SUBSTR(l_primary_key,
                    INSTR(l_primary_key, '|', 1) + 1,
                    length(l_primary_key))
        into L_key, L_primary_key
        from dual;
      if V_key_tbl(i)
       .data_type = 'VARCHAR2' or V_key_tbl(i).data_type = 'DATE' then
      
        L_clause_where := L_clause_where || V_key_tbl(i).col || '= ''' ||
                          L_key || ''' AND ';
      else
      
        L_clause_where := L_clause_where || V_key_tbl(i).col || '= ' ||
                          L_key || ' AND ';
      end if;
    end loop;
  
    SELECT storage_col_name, data_type, group_id
      into L_column, L_data_type, L_group_id
      from cfa_attrib
     where attrib_id = I_cfa_id;
  
    if L_data_type like '%VARCHAR%' or L_data_type like '%NUMBER%' then
      O_status_code   := 'E';
      O_error_message := 'Data type is not valid.';
      raise PROGRAM_ERROR;
    end if;
  
    L_sql := 'SELECT count(' || L_column || ') from ' || V_key_tbl(1)
            .table_name || ' WHERE ' || L_clause_where || ' GROUP_ID = ' ||
             L_group_id;
    EXECUTE IMMEDIATE L_sql
      into L_exist;
    if L_exist > 0 then
    
      L_sql := 'SELECT ' || L_column || ' from ' || V_key_tbl(1).table_name ||
               ' WHERE ';
      L_sql := L_sql || L_clause_where || ' GROUP_ID = ' || L_group_id;
    
      EXECUTE IMMEDIATE L_sql
        into L_value;
    
      return L_value;
    else
      return null;
    end if;
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END GET_CFA_VALUE_DATE;

  -------------------------------------------------------------------------------------------------------
  -- Name: HANDLE_ERRORS
  -- Description: This procedure is responsible to register any errors if occurred
  -------------------------------------------------------------------------------------------------------
  PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                          IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cause          IN VARCHAR2,
                          I_program        IN VARCHAR2) IS
  
    L_program VARCHAR2(50) := 'XXADEO_CFAS_UTLS.HANDLE_ERRORS';
  
  BEGIN
  
    API_LIBRARY.HANDLE_ERRORS(O_status_code,
                              IO_error_message,
                              I_cause,
                              I_program);
  EXCEPTION
    when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
    
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
    
  END HANDLE_ERRORS;

end XXADEO_CFAS_UTILS;
/