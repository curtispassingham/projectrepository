CREATE OR REPLACE PACKAGE XXADEO_XITEM_UTILS_PCK AS
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_XITEM_UTILS_PCK.pks
* Description:   This package contains tools that help in the 
*				 implementation of the publisher package XXADEO_RMSSUB_XITEM.
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/

  FUNCTION GENERATE_BARCODE RETURN NUMBER;

  PROCEDURE VERIFY_SEQUENCING(I_identifier    IN VARCHAR2,
                              I_bu            IN VARCHAR2,
                              I_extract_date  IN date,
                              O_code          OUT NUMBER,
                              O_status_code   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error_message OUT varchar2);

  PROCEDURE CREATE_MSG_ITEM_DEL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                O_status_code   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error_message OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ITEM_CRE(I_message          IN "RIB_XItemDesc_REC",
                                I_item_level       IN NUMBER,
                                I_type             IN ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                                I_supplier_primary IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                I_item_id          IN ITEM_MASTER.ITEM%TYPE,
                                I_item_parent      IN ITEM_MASTER.ITEM%TYPE,
                                I_item_grandparent IN ITEM_MASTER.ITEM%TYPE,
                                O_status_code      OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error_message    OUT VARCHAR2);

  PROCEDURE CREATE_MSG_UDA_CRE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                               I_uda_lov       IN XXADEO_UDA_LOV_TBL,
                               I_uda_ff        IN XXADEO_UDA_FF_TBL,
                               I_uda_date      IN XXADEO_UDA_DATE_TBL,
                               O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_UDA_DEL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                               I_uda_lov       IN XXADEO_UDA_LOV_TBL,
                               I_uda_ff        IN XXADEO_UDA_FF_TBL,
                               I_uda_date      IN XXADEO_UDA_DATE_TBL,
                               O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ITEM_IMAGE_CRE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                      I_item_image    IN XXADEO_ITEM_IMAGE_TBL,
                                      I_message       IN "RIB_XItemImage_TBL",
                                      O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ITEM_IMAGE_MOD(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                      I_item_image    IN XXADEO_ITEM_IMAGE_TBL,
                                      O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ITEM_IMAGE_DEL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                      I_item_image    IN XXADEO_ITEM_IMAGE_TBL,
                                      O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_VAT_ITEM_CRE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                    I_message       IN XXADEO_VAT_ITEM_TBL,
                                    O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ITEM_SUP_CRE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                    I_message       IN "RIB_XItemSupDesc_REC",
                                    O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ITEM_SUP_MOD(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                    I_message       IN XXADEO_ITEM_SUP_REC,
                                    I_primary_ind   IN ITEM_SUPPLIER.PRIMARY_SUPP_IND%TYPE,
                                    O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ITEM_SUP_CTY_CRE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                        I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_message       IN "RIB_XItemSupCtyDesc_REC",
                                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ITEM_SUP_CTY_MOD(I_item           IN ITEM_MASTER.ITEM%TYPE,
                                        I_supplier       IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_message        IN XXADEO_ITEM_SUP_CTY_TBL,
                                        I_change_primary IN NUMBER,
                                        O_status_code    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error_message  IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ITEM_SUP_CTY_DEL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                        I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_message       IN XXADEO_ITEM_SUP_CTY_TBL,
                                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ISUP_MANU_CTY_DEL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                         I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                         I_message       IN XXADEO_ITEM_SUP_CTY_MFR_TBL,
                                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ISUP_MANU_CTY_CRE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                         I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                         I_message       IN XXADEO_ITEM_SUP_CTY_MFR_TBL,
                                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ISUP_CTY_DIM_CRE(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                        I_supplier          IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_origin_country_id IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                        I_isup_cty_dim_add  IN "RIB_XISCDimDesc_REC",
                                        O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error_message     IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ISUP_CTY_DIM_DEL(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                        I_supplier          IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_origin_country_id IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                        I_isup_cty_dim_del  IN xxadeo_item_sup_cty_dim_tbl,
                                        O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error_message     IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ISUP_CTY_DIM_MOD(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                        I_supplier          IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_origin_country_id IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                        I_isup_cty_dim_mod  IN "RIB_XISCDimDesc_REC",
                                        O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error_message     IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_IM_TL_CRE(I_item               IN ITEM_MASTER.ITEM%TYPE,
                                 I_item_master_tl_add IN XXADEO_ITEM_MASTER_TL_TBL,
                                 O_status_code        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error_message      IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_IM_TL_DEL(I_item               IN ITEM_MASTER.ITEM%TYPE,
                                 I_item_master_tl_del IN XXADEO_ITEM_MASTER_TL_TBL,
                                 O_status_code        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error_message      IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_IM_TL_MOD(I_item               IN ITEM_MASTER.ITEM%TYPE,
                                 I_item_master_tl_mod IN XXADEO_ITEM_MASTER_TL_TBL,
                                 O_status_code        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error_message      IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ITEM_IMAGE_TL_CRE(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                         I_image_name        IN ITEM_IMAGE.IMAGE_NAME%TYPE,
                                         I_item_image_tl_add IN XXADEO_ITEM_IMAGE_TL_TBL,
                                         O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_error_message     IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ITEM_IMAGE_TL_DEL(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                         I_image_name        IN ITEM_IMAGE.IMAGE_NAME%TYPE,
                                         I_item_image_tl_del IN XXADEO_ITEM_IMAGE_TL_TBL,
                                         O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_error_message     IN OUT VARCHAR2);

  PROCEDURE CREATE_MSG_ITEM_IMAGE_TL_MOD(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                         I_image_name        IN ITEM_IMAGE.IMAGE_NAME%TYPE,
                                         I_item_image_tl_mod IN XXADEO_ITEM_IMAGE_TL_TBL,
                                         O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_error_message     IN OUT VARCHAR2);
                                         
 PROCEDURE VALIDATE_CFAS_TO_ITEM_SUP(I_primary_key       IN VARCHAR2,
                                      I_value_cfa_quality IN VARCHAR2,
                                      O_cfa               OUT XXADEO_CFA_DETAILS);


END XXADEO_XITEM_UTILS_PCK;
/