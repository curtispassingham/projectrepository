CREATE OR REPLACE PACKAGE BODY XXADEO_MV_REFRESH_SQL IS
 
  /******************************************************************************/
  /* CREATE DATE - August 2018                                                  */
  /* CREATE USER - Tiago Torres                                                 */
  /* PROJECT     - ADEO                                                         */
  /* DESCRIPTION - Refresh materialized views                                   */
  /******************************************************************************/

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------
  FUNCTION DASHBOARD_INCOMPLETE_ITEMS_MV(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN BOOLEAN IS
  
    --
    L_program VARCHAR2(100) := 'XXADEO_MV_REFRESH_SQL.DASHBOARD_INCOMPLETE_ITEMS_MV';
    --
  
    cursor C_get_dashboard_inc_items_mv is
      select mv_name
        from xxadeo_m_view_refresh_cfg
       where upper(mv_scope) = 'DASHBOARDS_MV'
         and upper(mv_execution) = 'XXADEO_INCOMPLETE_ITEMS';
  
  BEGIN
  
    for db_mv in C_get_dashboard_inc_items_mv loop
      execute immediate 'BEGIN DBMS_MVIEW.REFRESH(''' || db_mv.mv_name || ''');END;';
    end loop;

    --
    return TRUE;
    --
  EXCEPTION
    --
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      --
      return FALSE;
      --
  
  END DASHBOARD_INCOMPLETE_ITEMS_MV;
  
  

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------  
  FUNCTION DASHBOARD_ITEMS_PRICE_CHG_MV(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN BOOLEAN IS
  
    --
    L_program VARCHAR2(100) := 'XXADEO_MV_REFRESH_SQL.DASHBOARD_ITEMS_PRICE_CHG_MV';
    --
  
    cursor C_get_dashboard_inc_items_mv is
      select mv_name
        from xxadeo_m_view_refresh_cfg
       where upper(mv_scope) = 'DASHBOARDS_MV'
         and upper(mv_execution) = 'XXADEO_ITEMS_PRICE_CHG';
  
  BEGIN
  
    for db_mv in C_get_dashboard_inc_items_mv loop
      execute immediate 'BEGIN DBMS_MVIEW.REFRESH(''' || db_mv.mv_name || ''');END;';
    end loop;
  
    --
    return TRUE;
    --
  EXCEPTION
    --
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      --
      return FALSE;
      --
  
  END DASHBOARD_ITEMS_PRICE_CHG_MV;
  
  
END XXADEO_MV_REFRESH_SQL;
/
