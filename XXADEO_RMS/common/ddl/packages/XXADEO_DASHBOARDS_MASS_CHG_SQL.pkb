CREATE OR REPLACE PACKAGE BODY XXADEO_DASHBOARDS_MASS_CHG_SQL IS

  /******************************************************************************/
  /* CREATE DATE - August 2018                                                  */
  /* CREATE USER - Tiago Torres                                                 */
  /* PROJECT     - ADEO                                                         */
  /* DESCRIPTION - Item list to be used in static report                        */
  /******************************************************************************/

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------
  FUNCTION XXADEO_MASS_CHG_ITEM_PROCESS(I_item_list     IN XXADEO_ITEM_MASS_CHANGE_TBL := XXADEO_ITEM_MASS_CHANGE_TBL(),
                                       O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN NUMBER IS
  
    --
    L_program VARCHAR2(100) := 'XXADEO_DASHBOARDS_MASS_CHG_SQL.XXADEO_MASS_CHG_ITEM_PROCESS';
    --
    L_process_id      xxadeo_dashboards_item_list.process_id%TYPE;
    L_exists          NUMBER;
    process_id_exists BOOLEAN;
    --
  
    cursor C_check_process_id is
      select 1
        from xxadeo_dashboards_item_list dmc
       where dmc.process_id = L_process_id;
  
  BEGIN
  
    --set process id
    
    --
    process_id_exists := TRUE;
    --
    
    while process_id_exists loop
      L_process_id := xxadeo_dashboard_item_list_seq.nextval();
    
      open C_check_process_id;
      fetch C_check_process_id
        into L_exists;
    
      if C_check_process_id%NOTFOUND then
        --
        close C_check_process_id;
        process_id_exists := FALSE;
        --
      else
        --
        close C_check_process_id;
        --
      end if;
    end loop;
  
    --insert items with the respective process id
    
    for i in 1 .. I_item_list.Count loop
      --
      insert into XXADEO_DASHBOARDS_ITEM_LIST
        (process_id, bu, item_id, create_id, create_datetime)
      values
        (L_process_id, I_item_list(i).bu, I_item_list(i).item_id, NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')), sysdate);
      --
    end loop;
    commit;
    --
    return L_process_id;
    --
  
  EXCEPTION
    --
    when OTHERS then
      rollback;
      --
      if C_check_process_id%ISOPEN then
        close C_check_process_id;
      end if;
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      --
      return -1;
      --
  
  END XXADEO_MASS_CHG_ITEM_PROCESS;

  --------------------------------------------------------------
  --**********************************************************--
  --------------------------------------------------------------
 
  FUNCTION XXADEO_ITEM_LIST_PURGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN BOOLEAN IS
  
    --
    L_program VARCHAR2(100) := 'XXADEO_DASHBOARDS_MASS_CHG_SQL.XXADEO_ITEM_LIST_PURGE';
    --

  
  BEGIN
  
  
    --delete items
    
    delete from XXADEO_DASHBOARDS_ITEM_LIST;
    commit;
    --
    return true;
    --
  
  EXCEPTION
    --
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      --
      return false;
      --
  
  END XXADEO_ITEM_LIST_PURGE;

END XXADEO_DASHBOARDS_MASS_CHG_SQL;
/