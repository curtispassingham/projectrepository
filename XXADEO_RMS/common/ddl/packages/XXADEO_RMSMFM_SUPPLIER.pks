CREATE OR REPLACE PACKAGE XXADEO_RMSMFM_SUPPLIER AS
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_RMSMFM_SUPPLIER.pks
* Description:   Package that will be used by RIB to publish 
*                supplier information.
*                
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/

  FAMILY CONSTANT RIB_SETTINGS.FAMILY%TYPE := 'Vendor';

  VEND_DESC_MSG     CONSTANT VARCHAR2(15) := 'VendorDesc';
  VEND_HDR_DESC_MSG CONSTANT VARCHAR2(15) := 'VendorHdrDesc';
  VEND_ADDR_DESC_MSG CONSTANT VARCHAR2(15) := 'VendorAddrDesc';
  VEND_OU_DESC_MSG CONSTANT VARCHAR2(15) := 'VendorOUDesc';
  
  VENDOR_BU_MSG CONSTANT VARCHAR2(15) := 'VendorBU';
  CFA_MSG CONSTANT VARCHAR2(20) := 'CustFlexAttriVo';
  EXTOF_MSG CONSTANT VARCHAR2(20) := 'ExtOfVendorHdrDesc';

  --------------------------------------------------------
  VEND_ADD  CONSTANT VARCHAR2(15) := 'VendorCre';
  
  -- new message type
  VEND_ADEO CONSTANT VARCHAR2(15) := 'VendorModADEO';

  --------------------------------------------------------
  PROCEDURE ADDTOQ(I_message_type  IN VARCHAR2,
                   I_supplier      IN sups.supplier%TYPE,
                   I_addr_seq_no   IN addr.seq_no%TYPE,
                   I_addr_type     IN addr.addr_type%TYPE,
                   I_ret_allow_ind IN VARCHAR2,
                   I_org_unit_id   IN VARCHAR2,
                   I_message       IN CLOB,
                   O_status        OUT VARCHAR2,
                   O_text          OUT VARCHAR2);
  ----------------------------------------------------------
  PROCEDURE GETNXT(O_status_code  OUT VARCHAR2,
                   O_error_msg    OUT VARCHAR2,
                   O_message_type OUT VARCHAR2,
                   O_message      OUT CLOB,
                   O_supplier     OUT sups.supplier%TYPE,
                   O_addr_seq_no  OUT addr.seq_no%TYPE,
                   O_addr_type    OUT addr.addr_type%TYPE);
  ---------------------------------------------------------
END XXADEO_RMSMFM_SUPPLIER;
/
