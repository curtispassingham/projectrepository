create or replace package body XXADEO_INC_ITEM_SEARCH_SQL is

  FUNCTION XXADEO_INC_ITEM_SEARCH(I_cond_flag       IN VARCHAR2,
                                  I_bu              IN area_tl.area%TYPE DEFAULT NULL,
                                  I_group           IN groups.group_no%TYPE DEFAULT NULL,
                                  I_dept_list       IN VARCHAR2 DEFAULT NULL,
                                  I_class_list      IN VARCHAR2 DEFAULT NULL,
                                  I_subclass_list   IN VARCHAR2 DEFAULT NULL,
                                  I_supp_purch_site IN NUMBER DEFAULT NULL,
                                  I_manufacturer    IN NUMBER DEFAULT NULL,
                                  I_item_list       IN VARCHAR2 DEFAULT NULL,
                                  I_item            IN item_master.item%TYPE DEFAULT NULL,
                                  I_item_desc       IN item_master.item_desc%TYPE DEFAULT NULL,
                                  I_gtin            IN NUMBER DEFAULT NULL,
                                  I_udacfa1_id      IN NUMBER DEFAULT NULL,
                                  I_udacfa1_value   IN VARCHAR2 DEFAULT NULL,
                                  I_udacfa1_type    IN VARCHAR2 DEFAULT NULL,
                                  I_udacfa1_flag    IN VARCHAR2 DEFAULT NULL,
                                  I_udacfa2_id      IN NUMBER DEFAULT NULL,
                                  I_udacfa2_value   IN VARCHAR2 DEFAULT NULL,
                                  I_udacfa2_type    IN VARCHAR2 DEFAULT NULL,
                                  I_udacfa2_flag    IN VARCHAR2 DEFAULT NULL,
                                  I_udacfa3_id      IN NUMBER DEFAULT NULL,
                                  I_udacfa3_value   IN VARCHAR2 DEFAULT NULL,
                                  I_udacfa3_type    IN VARCHAR2 DEFAULT NULL,
                                  I_udacfa3_flag    IN VARCHAR2 DEFAULT NULL,
                                  I_execute_query   IN  VARCHAR2 DEFAULT 'Y')
    RETURN XXADEO_INC_ITEM_RESULT_TBL
    PIPELINED AS
  
    L_program       VARCHAR2(64) := 'XXADEO_INC_ITEM_SEARCH_SQL.XXADEO_INC_ITEM_SEARCH';
    L_error_message VARCHAR2(2000);
    --
    L_IncompleteItemSearchTbl XXADEO_INC_ITEM_RESULT_TBL;
    L_string_query            VARCHAR2(20000);
    L_sys_refcur              SYS_REFCURSOR;
    --
    L_dept_list     list_type := list_type();
    L_class_list    list_type := list_type();
    L_subclass_list list_type := list_type();
    L_item_list     list_type := list_type();
    L_udacfa1_list  list_type := list_type();
    L_udacfa2_list  list_type := list_type();
    L_udacfa3_list  list_type := list_type();
    L_cfa_type      cfa_attrib.data_type%TYPE;
    --
    L_date1_1       DATE;
    L_date1_2       DATE;
    L_date2_1       DATE;
    L_date2_2       DATE;
    L_date3_1       DATE;
    L_date3_2       DATE;
    --
    
    --cursor
    
    --get CFA data type
    cursor C_get_CFA_data_type(L_attrib_id cfa_attrib.attrib_id%TYPE) is
      select data_type from cfa_attrib where attrib_id = L_attrib_id;
  
  BEGIN
    --
    if I_execute_query = 'N' then
      --
      return;
      --
    end if;
    --
    EXECUTE IMMEDIATE ('ALTER SESSION SET NLS_DATE_FORMAT = ''DD-MM-YYYY''');
    --
  
    L_string_query := q'{with t_binds as
                            (select :1  db_cond_flag,
                                    :2  db_bu,
                                    :3  db_group,
                                    :4  db_dept,
                                    :5  db_class,
                                    :6  db_subclass,
                                    :7  db_supp_purch_site,
                                    :8  db_manufacturer,
                                    :9  db_item_list,
                                    :10 db_item,
                                    :11 db_item_desc,
                                    :12 db_gtin,
                                    :13 db_udacfa1_id,
                                    :14 db_udacfa1_value,  
                                    :15 db_udacfa2_id,   
                                    :16 db_udacfa2_value,    
                                    :17 db_udacfa3_id,     
                                    :18 db_udacfa3_value,
                                    :19 db_date1_1,
                                    :20 db_date1_2,
                                    :21 db_date2_1,
                                    :22 db_date2_2,
                                    :23 db_date3_1,
                                    :24 db_date3_2
                               from dual)
                              select new XXADEO_INC_ITEM_RESULT_OBJ(i.BU,
                                                                    i.DEPT_ID,
                                                                    i.DEPT_NAME, 
                                                                    i.SUBDEPT_ID, 
                                                                    i.SUBDEPT_NAME, 
                                                                    i.TYPE_ID, TYPE_NAME, 
                                                                    i.SUBTYPE_ID, 
                                                                    i.SUBTYPE_NAME, 
                                                                    i.ITEM, 
                                                                    i.ITEM_DESC, 
                                                                    i.SUPP_IND,
                                                                    i.CLASSMENT_BU,
                                                                    i.SUBTYPOLOGY, 
                                                                    i.LFCY_ENTITY, 
                                                                    i.LFCY_STATUS, 
                                                                    i.LFCY_ENTITY_TYPE, 
                                                                    i.ACTIVE_COMMERCE_DATE, 
                                                                    i.COND_FLAG,
                                                                    im.SELLABLE_IND,
                                                                    im.ORDERABLE_IND,
                                                                    im.STATUS,
                                                                    i.COMPLETEDPERCENT, 
                                                                    i.COND_IB_ASI_01, 
                                                                    i.COND_IB_ASI_02, 
                                                                    i.COND_IB_ASI_03, 
                                                                    i.COND_IB_ASI_04, 
                                                                    i.COND_IB_ASI_05,
                                                                    i.COND_IB_ACOM_01, 
                                                                    i.COND_IB_ACOM_02, 
                                                                    i.COND_IB_ACOM_03,
                                                                    i.COND_IB_ACOM_04, 
                                                                    i.COND_IS_ACT_01, 
                                                                    i.COND_IS_ACT_02, 
                                                                    i.COND_IS_ACT_03, 
                                                                    i.COND_IS_ACT_04, 
                                                                    i.COND_IS_ACT_05, 
                                                                    i.COND_IS_ACT_06, 
                                                                    i.COND_IS_ACT_07)
                                 from xxadeo_mv_incomplete_items i,
                                      v_item_master im, 
                                      t_binds b
                                      where im.item = i.item
                                        and exists (select 1 from xxadeo_v_area_bu ab where i.bu = ab.area)
                                      }';
  
    -- BU
    if I_bu is not null then

      L_string_query := L_string_query ||
                        q'{ and (i.bu = b.db_bu)}';
      --                  
    end if;
    
    
    --Condition flag
    if I_cond_flag is not null then
      
      L_string_query := L_string_query ||
                        q'{ and (i.COND_FLAG = b.db_cond_flag)}';
      --
    end if;
    
    
    --department
    if I_group is not null then
      
      L_string_query := L_string_query ||
                        q'{ and (i.dept_id = b.db_group) }';
      --
    end if;
  
  
    --subdepartment
    if I_dept_list is not null then
      
      select t.*
        bulk collect
        into L_dept_list
        from table(convert_comma_list(I_list => I_dept_list)) t;
      --
      L_string_query := L_string_query ||
                        q'{ and i.subdept_id in (select column_value
                                                                        from table(b.db_dept)) }';
      --
    end if;
  
  
    --class
    if I_class_list is not null then
      
      select t.*
        bulk collect
        into L_class_list
        from table(convert_comma_list(I_list => I_class_list)) t;
      --
      L_string_query := L_string_query ||
                        q'{ and i.type_id in (select column_value
                                                                      from table(b.db_class)) }';
      --
    end if;
  
  
    --subclass
    if I_subclass_list is not null then
      
      select t.*
        bulk collect
        into L_subclass_list
        from table(convert_comma_list(I_list => I_subclass_list)) t;
      --
      L_string_query := L_string_query ||
                        q'{ and i.subtype_id in (select column_value
                                                                      from table(b.db_subclass)) }';
      --
    end if;
    
    
    --supp_purch_site
  
    if I_supp_purch_site is not null then
      
      L_string_query := L_string_query ||
                        q'{ and exists (select 1 from item_supplier isup 
                                         where i.item = isup.item 
                                           and isup.supplier = b.db_supp_purch_site) }';
      --
    
    end if;
  
    --manufacturer
  
    if I_manufacturer is not null then
      
      L_string_query := L_string_query ||
                        q'{ and exists ( select 1 
                                                            from v_item_supp_country isc
                                                           where to_number(isc.supp_hier_lvl_1) = b.db_manufacturer
                                                             and i.item = isc.item) }';
    
    end if;
    
    
    --item
    if I_item is not null then
      
      L_string_query := L_string_query || q'{ and exists (select 1
                                                               from v_item_master imaster 
                                                              where imaster.item = to_char(b.db_item)
                                                                and i.item = imaster.item)}';
      --
    end if;
  
    --item list
    if I_item_list is not null then
      
      select t.item
        bulk collect
        into L_item_list
        from skulist_detail t
       where t.skulist = I_item_list;
       
      --
      L_string_query := L_string_query ||
                        q'{ and i.item in (select column_value
                                                                      from table(b.db_item_list)) }';
      --
    end if;
  
    -- Item_Description 
    if I_item_desc is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and upper(i.item_desc) like upper('%}' || I_item_desc || q'{%') }';
      --
    end if;
  
    --gtin
    if I_gtin is not null then
       
      L_string_query := L_string_query || q'{ and (i.item in (  select nvl(im2.item_grandparent,im2.item_parent) 
                                                                  from item_master im2
                                                                 where im2.item = b.db_gtin
                                                                   and im2.item_level > im2.tran_level
                                                              ))}';
      --
    end if;
    
    
    
    
        
        --item attr 1
  
    if I_udacfa1_id is not null then
      --
      if I_udacfa1_flag = 'UDA' then
        --
        if I_udacfa1_type = GP_UDA_TYPE_LV then
        
          select t.*
            bulk collect
            into L_udacfa1_list
            from table(convert_comma_list(I_list => I_udacfa1_value)) t;
            
          --
          L_string_query := L_string_query ||    
                            q'{ and exists ( select 1 
                                                              from uda_item_lov uda_lov
                                                             where uda_lov.uda_id = b.db_udacfa1_id
                                                               and uda_lov.uda_value in (select column_value
                                                                        from table(b.db_udacfa1_value))
                                                               and i.item = uda_lov.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_lov.uda_id
                                                                              and func_area = 'UDA'))}';
        --
        elsif I_udacfa1_type = GP_UDA_TYPE_DT then
      
        
        select t.*
              bulk collect
              into L_udacfa1_list
              from table(convert_comma_list(I_list => I_udacfa1_value)) t;
          --   
          L_date1_1 := trunc(TO_DATE(TO_CHAR(L_udacfa1_list(1)),'YYYY-MM-DD'));
          L_date1_2 := trunc(TO_DATE(TO_CHAR(L_udacfa1_list(2)),'YYYY-MM-DD'));
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1 
                                                              from uda_item_date uda_date
                                                             where uda_date.uda_id = b.db_udacfa1_id
                                                                and trunc(uda_date.uda_date) between trunc(b.db_date1_1) and trunc(b.db_date1_2)
                                                               and i.item = uda_date.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_date.uda_id
                                                                              and func_area = 'UDA'))}';
        --
        elsif I_udacfa1_type = GP_UDA_TYPE_FF then
          
          L_udacfa1_list.Delete;
          L_udacfa1_list.extend();
          L_udacfa1_list(1) := I_udacfa1_value;
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1 
                                                              from uda_item_ff uda_ff
                                                             where uda_ff.uda_id = b.db_udacfa1_id
                                                               and upper(uda_ff.uda_text) in (select upper(column_value)
                                                                        from table(b.db_udacfa1_value))
                                                               and i.item = uda_ff.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_ff.uda_id
                                                                              and func_area = 'UDA'))}';
        
        end if;
      --
      elsif I_udacfa1_flag = 'CFA' then
        --
        open C_get_CFA_data_type(I_udacfa1_id);
        fetch C_get_CFA_data_type
          into L_cfa_type;
        close C_get_CFA_data_type;
        --
        if L_cfa_type = GP_CFA_TEXT then
          --
          if I_udacfa1_type in (GP_CFA_TYPE_LI, GP_CFA_TYPE_RG) then
          
            select t.*
              bulk collect
              into L_udacfa1_list
              from table(convert_comma_list(I_list => I_udacfa1_value)) t;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (varchar2_1,
                                                                                                              varchar2_2,
                                                                                                              varchar2_3,
                                                                                                              varchar2_4,
                                                                                                              varchar2_5,
                                                                                                              varchar2_6,
                                                                                                              varchar2_7,
                                                                                                              varchar2_8,
                                                                                                              varchar2_9,
                                                                                                              varchar2_10))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa1_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa1_value)))}';
          --
          elsif I_udacfa1_type in (GP_CFA_TYPE_CB, GP_CFA_TYPE_TI) then
          
            L_udacfa1_list.Delete;
            L_udacfa1_list.extend();
            L_udacfa1_list(1) := I_udacfa1_value;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (varchar2_1,
                                                                                                              varchar2_2,
                                                                                                              varchar2_3,
                                                                                                              varchar2_4,
                                                                                                              varchar2_5,
                                                                                                              varchar2_6,
                                                                                                              varchar2_7,
                                                                                                              varchar2_8,
                                                                                                              varchar2_9,
                                                                                                              varchar2_10))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa1_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa1_value)))}';
          
          end if;
        --
        elsif L_cfa_type = GP_CFA_NUMBER then
        
          if I_udacfa1_type in (GP_CFA_TYPE_LI, GP_CFA_TYPE_RG) then
          
            select t.*
              bulk collect
              into L_udacfa1_list
              from table(convert_comma_list(I_list => I_udacfa1_value)) t;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (number_11,
                                                                                                              number_12,
                                                                                                              number_13,
                                                                                                              number_14,
                                                                                                              number_15,
                                                                                                              number_16,
                                                                                                              number_17,
                                                                                                              number_18,
                                                                                                              number_19,
                                                                                                              number_20))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa1_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and cfa_value in (select column_value
                                                                        from table(b.db_udacfa1_value)))}';
          --
          elsif I_udacfa1_type in (GP_CFA_TYPE_TI) then
          
            L_udacfa1_list.Delete;
            L_udacfa1_list.extend();
            L_udacfa1_list(1) := I_udacfa1_value;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (number_11,
                                                                                                              number_12,
                                                                                                              number_13,
                                                                                                              number_14,
                                                                                                              number_15,
                                                                                                              number_16,
                                                                                                              number_17,
                                                                                                              number_18,
                                                                                                              number_19,
                                                                                                              number_20))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa1_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa1_value)))}';
          
          end if;
        --
        elsif L_cfa_type = GP_CFA_DATE then
        
          
          select t.*
              bulk collect
              into L_udacfa1_list
              from table(convert_comma_list(I_list => I_udacfa1_value)) t;
          --  
          L_date1_1 := trunc(TO_DATE(TO_CHAR(L_udacfa1_list(1)),'YYYY-MM-DD'));
          L_date1_2 := trunc(TO_DATE(TO_CHAR(L_udacfa1_list(2)),'YYYY-MM-DD'));
          --       
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (date_21,
                                                                                                              date_22,
                                                                                                              date_23,
                                                                                                              date_24,
                                                                                                              date_25))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa1_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and trunc(cfa_value) between trunc(b.db_date1_1) and trunc(b.db_date1_2))}';
        --
        end if;
      --
      end if;
    --
    end if;
  
  
  
    --item attr 2
  
    if I_udacfa2_id is not null then
      --
      if I_udacfa2_flag = 'UDA' then
        --      
        if I_udacfa2_type = GP_UDA_TYPE_LV then
        
          select t.*
            bulk collect
            into L_udacfa2_list
            from table(convert_comma_list(I_list => I_udacfa2_value)) t;
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1 
                                                            from uda_item_lov uda_lov
                                                           where uda_lov.uda_id = b.db_udacfa2_id
                                                             and uda_lov.uda_value in (select column_value
                                                                      from table(b.db_udacfa2_value))
                                                             and i.item = uda_lov.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_lov.uda_id
                                                                              and func_area = 'UDA'))}';
        --
        elsif I_udacfa2_type = GP_UDA_TYPE_DT then
          
        
        
          select t.*
            bulk collect
            into L_udacfa2_list
            from table(convert_comma_list(I_list => I_udacfa2_value)) t;
          --
          L_date2_1 := trunc(TO_DATE(TO_CHAR(L_udacfa2_list(1)),'YYYY-MM-DD'));
          L_date2_2 := trunc(TO_DATE(TO_CHAR(L_udacfa2_list(2)),'YYYY-MM-DD'));
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1 
                                                              from uda_item_date uda_date
                                                             where uda_date.uda_id = b.db_udacfa2_id
                                                                and trunc(uda_date.uda_date) between trunc(b.db_date2_1) and trunc(b.db_date2_2)
                                                               and i.item = uda_date.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_date.uda_id
                                                                              and func_area = 'UDA'))}';
        --
        elsif I_udacfa2_type = GP_UDA_TYPE_FF then
          
          L_udacfa2_list.Delete;
          L_udacfa2_list.extend();
          L_udacfa2_list(1) := I_udacfa2_value;
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1 
                                                              from uda_item_ff uda_ff
                                                             where uda_ff.uda_id = b.db_udacfa2_id
                                                               and upper(uda_ff.uda_text) in (select upper(column_value)
                                                                        from table(b.db_udacfa2_value))
                                                               and i.item = uda_ff.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_ff.uda_id
                                                                              and func_area = 'UDA'))}';
        
        end if;
      --
      elsif I_udacfa2_flag = 'CFA' then
        --
        open C_get_CFA_data_type(I_udacfa2_id);
        fetch C_get_CFA_data_type
          into L_cfa_type;
        close C_get_CFA_data_type;
        --
        if L_cfa_type = GP_CFA_TEXT then
        
          if I_udacfa2_type in (GP_CFA_TYPE_LI, GP_CFA_TYPE_RG) then
          
            select t.*
              bulk collect
              into L_udacfa2_list
              from table(convert_comma_list(I_list => I_udacfa2_value)) t;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (varchar2_1,
                                                                                                              varchar2_2,
                                                                                                              varchar2_3,
                                                                                                              varchar2_4,
                                                                                                              varchar2_5,
                                                                                                              varchar2_6,
                                                                                                              varchar2_7,
                                                                                                              varchar2_8,
                                                                                                              varchar2_9,
                                                                                                              varchar2_10))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa2_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa2_value)))}';
          --
          elsif I_udacfa2_type in (GP_CFA_TYPE_CB, GP_CFA_TYPE_TI) then
          
            L_udacfa2_list.Delete;
            L_udacfa2_list.extend();
            L_udacfa2_list(1) := I_udacfa2_value;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (varchar2_1,
                                                                                                              varchar2_2,
                                                                                                              varchar2_3,
                                                                                                              varchar2_4,
                                                                                                              varchar2_5,
                                                                                                              varchar2_6,
                                                                                                              varchar2_7,
                                                                                                              varchar2_8,
                                                                                                              varchar2_9,
                                                                                                              varchar2_10))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa2_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa2_value)))}';
          
          end if;
        --
        elsif L_cfa_type = GP_CFA_NUMBER then
        
          if I_udacfa2_type in (GP_CFA_TYPE_LI, GP_CFA_TYPE_RG) then
          
            select t.*
              bulk collect
              into L_udacfa2_list
              from table(convert_comma_list(I_list => I_udacfa2_value)) t;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (number_11,
                                                                                                              number_12,
                                                                                                              number_13,
                                                                                                              number_14,
                                                                                                              number_15,
                                                                                                              number_16,
                                                                                                              number_17,
                                                                                                              number_18,
                                                                                                              number_19,
                                                                                                              number_20))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa2_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and cfa_value in (select column_value
                                                                        from table(b.db_udacfa2_value)))}';
          --
          elsif I_udacfa2_type in (GP_CFA_TYPE_TI) then
          
            L_udacfa2_list.Delete;
            L_udacfa2_list.extend();
            L_udacfa2_list(1) := I_udacfa2_value;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (number_11,
                                                                                                              number_12,
                                                                                                              number_13,
                                                                                                              number_14,
                                                                                                              number_15,
                                                                                                              number_16,
                                                                                                              number_17,
                                                                                                              number_18,
                                                                                                              number_19,
                                                                                                              number_20))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa2_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa2_value)))}';
          
          end if;
        --
        elsif L_cfa_type = GP_CFA_DATE then
        
         select t.*
              bulk collect
              into L_udacfa2_list
              from table(convert_comma_list(I_list => I_udacfa2_value)) t;
          --  
          L_date2_1 := trunc(TO_DATE(TO_CHAR(L_udacfa2_list(1)),'YYYY-MM-DD'));
          L_date2_2 := trunc(TO_DATE(TO_CHAR(L_udacfa2_list(2)),'YYYY-MM-DD'));
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (date_21,
                                                                                                              date_22,
                                                                                                              date_23,
                                                                                                              date_24,
                                                                                                              date_25))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa2_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and trunc(cfa_value) between trunc(b.db_date2_1) and trunc(b.db_date2_2))}';
        --
        end if;
      --
      end if;
    --
    end if;
  
  
    --item attr 3
  
    if I_udacfa3_id is not null then
      
      if I_udacfa3_flag = 'UDA' then
              
        if I_udacfa3_type = GP_UDA_TYPE_LV then
        
          select t.*
            bulk collect
            into L_udacfa3_list
            from table(convert_comma_list(I_list => I_udacfa3_value)) t;
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1 
                                                            from uda_item_lov uda_lov
                                                           where uda_lov.uda_id = b.db_udacfa3_id
                                                             and uda_lov.uda_value in (select column_value
                                                                      from table(b.db_udacfa3_value))
                                                             and i.item = uda_lov.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_lov.uda_id
                                                                              and func_area = 'UDA'))}';
        --
        elsif I_udacfa3_type = GP_UDA_TYPE_DT then
          
           select t.*
              bulk collect
              into L_udacfa3_list
              from table(convert_comma_list(I_list => I_udacfa3_value)) t; 
          --   
          L_date3_1 := trunc(TO_DATE(TO_CHAR(L_udacfa3_list(1)),'YYYY-MM-DD'));
          L_date3_2 := trunc(TO_DATE(TO_CHAR(L_udacfa3_list(2)),'YYYY-MM-DD'));
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1 
                                                              from uda_item_date uda_date
                                                             where uda_date.uda_id = b.db_udacfa3_id
                                                                and trunc(uda_date.uda_date) between trunc(b.db_date3_1) and trunc(b.db_date3_2)
                                                               and i.item = uda_date.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_date.uda_id
                                                                              and func_area = 'UDA'))}';
        
        --
        elsif I_udacfa3_type = GP_UDA_TYPE_FF then
          
          L_udacfa3_list.Delete;
          L_udacfa3_list.extend();
          L_udacfa3_list(1) := I_udacfa3_value;
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1 
                                                              from uda_item_ff uda_ff
                                                             where uda_ff.uda_id = b.db_udacfa3_id
                                                               and upper(uda_ff.uda_text) in (select upper(column_value)
                                                                        from table(b.db_udacfa3_value))
                                                               and i.item = uda_ff.item
                                                               and exists (select 1
                                                                             from xxadeo_mom_dvm
                                                                            where (bu = i.bu or bu is null or bu = -1)
                                                                              and value_1 = uda_ff.uda_id
                                                                              and func_area = 'UDA'))}';
          
        end if;
      --
      elsif I_udacfa3_flag = 'CFA' then
      
        open C_get_CFA_data_type(I_udacfa3_id);
        fetch C_get_CFA_data_type
          into L_cfa_type;
        close C_get_CFA_data_type;
        --
        if L_cfa_type = GP_CFA_TEXT then
        
          if I_udacfa3_type in (GP_CFA_TYPE_LI, GP_CFA_TYPE_RG) then
          
            select t.*
              bulk collect
              into L_udacfa3_list
              from table(convert_comma_list(I_list => I_udacfa3_value)) t;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (varchar2_1,
                                                                                                              varchar2_2,
                                                                                                              varchar2_3,
                                                                                                              varchar2_4,
                                                                                                              varchar2_5,
                                                                                                              varchar2_6,
                                                                                                              varchar2_7,
                                                                                                              varchar2_8,
                                                                                                              varchar2_9,
                                                                                                              varchar2_10))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa3_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa3_value)))}';
          
          elsif I_udacfa3_type in (GP_CFA_TYPE_CB, GP_CFA_TYPE_TI) then
          
            L_udacfa3_list.Delete;
            L_udacfa3_list.extend();
            L_udacfa3_list(1) := I_udacfa3_value;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (varchar2_1,
                                                                                                              varchar2_2,
                                                                                                              varchar2_3,
                                                                                                              varchar2_4,
                                                                                                              varchar2_5,
                                                                                                              varchar2_6,
                                                                                                              varchar2_7,
                                                                                                              varchar2_8,
                                                                                                              varchar2_9,
                                                                                                              varchar2_10))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa3_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa3_value)))}';
          
          end if;
        --
        elsif L_cfa_type = GP_CFA_NUMBER then
        
          if I_udacfa3_type in (GP_CFA_TYPE_LI, GP_CFA_TYPE_RG) then
          
            select t.*
              bulk collect
              into L_udacfa3_list
              from table(convert_comma_list(I_list => I_udacfa3_value)) t;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (number_11,
                                                                                                              number_12,
                                                                                                              number_13,
                                                                                                              number_14,
                                                                                                              number_15,
                                                                                                              number_16,
                                                                                                              number_17,
                                                                                                              number_18,
                                                                                                              number_19,
                                                                                                              number_20))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa3_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and cfa_value in (select column_value
                                                                        from table(b.db_udacfa3_value)))}';
          --
          elsif I_udacfa3_type in (GP_CFA_TYPE_TI) then
          
            L_udacfa3_list.Delete;
            L_udacfa3_list.extend();
            L_udacfa3_list(1) := I_udacfa3_value;
            --
            L_string_query := L_string_query ||
                              q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (number_11,
                                                                                                              number_12,
                                                                                                              number_13,
                                                                                                              number_14,
                                                                                                              number_15,
                                                                                                              number_16,
                                                                                                              number_17,
                                                                                                              number_18,
                                                                                                              number_19,
                                                                                                              number_20))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa3_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and upper(cfa_value) in (select upper(column_value)
                                                                        from table(b.db_udacfa3_value)))}';
          
          end if;
        --
        elsif L_cfa_type = GP_CFA_DATE then
        
          select t.*
              bulk collect
              into L_udacfa3_list
              from table(convert_comma_list(I_list => I_udacfa3_value)) t;
          --   
          L_date3_1 := trunc(TO_DATE(TO_CHAR(L_udacfa3_list(1)),'YYYY-MM-DD'));
          L_date3_2 := trunc(TO_DATE(TO_CHAR(L_udacfa3_list(2)),'YYYY-MM-DD'));
          --
          L_string_query := L_string_query ||
                            q'{ and exists ( select 1
                                                               from (item_master_cfa_ext unpivot
                                                                                         (cfa_value for cfa_attribute in
                                                                                                             (date_21,
                                                                                                              date_22,
                                                                                                              date_23,
                                                                                                              date_24,
                                                                                                              date_25))) aux,
                                                                cfa_attrib cfa
                                                              where cfa.group_id = aux.group_id
                                                                and upper(cfa.storage_col_name) = upper(aux.cfa_attribute)
                                                                and aux.item = i.item
                                                                and cfa.attrib_id = b.db_udacfa3_id
                                                                and exists (select 1
                                                                              from xxadeo_mom_dvm
                                                                             where (bu = i.bu or bu is null or bu = -1)
                                                                               and value_1 = cfa.attrib_id
                                                                               and func_area = 'CFA')
                                                                and trunc(cfa_value) between trunc(b.db_date3_1) and trunc(b.db_date3_2))}';        
        --
        end if;
      --
      end if;
    --
    end if;
 
  
    --
    -- bulk query to table type
    --
    open L_sys_refcur for L_string_query
      using I_cond_flag, I_bu, I_group, L_dept_list, L_class_list, L_subclass_list, I_supp_purch_site, I_manufacturer, L_item_list, I_item, I_item_desc, I_gtin, I_udacfa1_id, L_udacfa1_list, I_udacfa2_id, L_udacfa2_list, I_udacfa3_id, L_udacfa3_list, L_date1_1, L_date1_2, L_date2_1, L_date2_2, L_date3_1, L_date3_2;
    loop
      --
      fetch L_sys_refcur bulk collect
        into L_IncompleteItemSearchTbl;
      exit when L_IncompleteItemSearchTbl.count = 0;
      --
      -- pipe data form collection
      --
      for i in 1 .. L_IncompleteItemSearchTbl.count loop
        --
        pipe row(XXADEO_INC_ITEM_RESULT_OBJ(L_IncompleteItemSearchTbl(i).BU,
                                            L_IncompleteItemSearchTbl(i).DEPT_ID,
                                            L_IncompleteItemSearchTbl(i).DEPT_NAME, 
                                            L_IncompleteItemSearchTbl(i).SUBDEPT_ID, 
                                            L_IncompleteItemSearchTbl(i).SUBDEPT_NAME, 
                                            L_IncompleteItemSearchTbl(i).TYPE_ID, 
                                            L_IncompleteItemSearchTbl(i).TYPE_NAME, 
                                            L_IncompleteItemSearchTbl(i).SUBTYPE_ID, 
                                            L_IncompleteItemSearchTbl(i).SUBTYPE_NAME, 
                                            L_IncompleteItemSearchTbl(i).ITEM, 
                                            L_IncompleteItemSearchTbl(i).ITEM_DESC, 
                                            L_IncompleteItemSearchTbl(i).SUPP_IND,
                                            L_IncompleteItemSearchTbl(i).CLASSMENT_BU,
                                            L_IncompleteItemSearchTbl(i).SUBTYPOLOGY, 
                                            L_IncompleteItemSearchTbl(i).LFCY_ENTITY, 
                                            L_IncompleteItemSearchTbl(i).LFCY_STATUS, 
                                            L_IncompleteItemSearchTbl(i).LFCY_ENTITY_TYPE, 
                                            L_IncompleteItemSearchTbl(i).ACTIVE_COMMERCE_DATE,
                                            L_IncompleteItemSearchTbl(i).COND_FLAG,
                                            L_IncompleteItemSearchTbl(i).SELLABLE_IND,
                                            L_IncompleteItemSearchTbl(i).ORDERABLE_IND,
                                            L_IncompleteItemSearchTbl(i).STATUS,
                                            L_IncompleteItemSearchTbl(i).COMPLETEDPERCENT, 
                                            L_IncompleteItemSearchTbl(i).COND_IB_ASI_01, 
                                            L_IncompleteItemSearchTbl(i).COND_IB_ASI_02, 
                                            L_IncompleteItemSearchTbl(i).COND_IB_ASI_03, 
                                            L_IncompleteItemSearchTbl(i).COND_IB_ASI_04,
                                            L_IncompleteItemSearchTbl(i).COND_IB_ASI_05, 
                                            L_IncompleteItemSearchTbl(i).COND_IB_ACOM_01, 
                                            L_IncompleteItemSearchTbl(i).COND_IB_ACOM_02, 
                                            L_IncompleteItemSearchTbl(i).COND_IB_ACOM_03, 
                                            L_IncompleteItemSearchTbl(i).COND_IB_ACOM_04,
                                            L_IncompleteItemSearchTbl(i).COND_IS_ACT_01, 
                                            L_IncompleteItemSearchTbl(i).COND_IS_ACT_02, 
                                            L_IncompleteItemSearchTbl(i).COND_IS_ACT_03, 
                                            L_IncompleteItemSearchTbl(i).COND_IS_ACT_04, 
                                            L_IncompleteItemSearchTbl(i).COND_IS_ACT_05, 
                                            L_IncompleteItemSearchTbl(i).COND_IS_ACT_06, 
                                            L_IncompleteItemSearchTbl(i).COND_IS_ACT_07));
      end loop;
    
      --
    end loop;
    --
    close L_sys_refcur;
    --
    return;
    --
  EXCEPTION
    --
    WHEN NO_DATA_NEEDED THEN
      --
      RETURN;
      --
    WHEN OTHERS THEN
      --
      if L_sys_refcur%ISOPEN then
        close L_sys_refcur;
      end if;
      --
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      --
      dbms_output.put_line(L_error_message);
      --  
      RETURN;
      --
    --
  END XXADEO_INC_ITEM_SEARCH;

end XXADEO_INC_ITEM_SEARCH_SQL;
/