CREATE OR REPLACE PACKAGE XXADEO_DEPT_LEVEL_VAT_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/*               Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package "XXADEO_DEPT_LEVEL_VAT"                              */
/******************************************************************************/
--------------------------------------------------------------------------------

  template_key                    CONSTANT VARCHAR2(255):='XXADEO_DEPT_LEVEL_VAT';
  action_new                      VARCHAR2(25)          :='NEW';
  action_mod                      VARCHAR2(25)          :='MOD';
  action_del                      VARCHAR2(25)          :='DEL';

  SHEET_NAME_TRANS                S9T_PKG.TRANS_MAP_TYP;

  GP_upload_create                VARCHAR2(6)           := 'NEW';
  GP_upload_update                VARCHAR2(6)           := 'MOD';
  
  TABLE$VAT_DEPS                  VARCHAR2(255)         := 'VAT_DEPS';

  DEPT_LEVEL_VAT_SHEET            VARCHAR2(255)         := 'DEPT_LEVEL_VAT';
  DEPT_LEVEL_VAT$ACTION           VARCHAR2(255)         := 'ACTION';
  DEPT_LEVEL_VAT$VAT_REGION       VARCHAR2(255)         := 'VAT_REGION';
  DEPT_LEVEL_VAT$GROUP_ID         VARCHAR2(255)         := 'GROUP_ID';
  DEPT_LEVEL_VAT$GROUP_NAME       VARCHAR2(255)         := 'GROUP_NAME';
  DEPT_LEVEL_VAT$DEPT             VARCHAR2(255)         := 'DEPT';
  DEPT_LEVEL_VAT$DEPT_NAME        VARCHAR2(255)         := 'DEPT_NAME';
  DEPT_LEVEL_VAT$VAT_TYPE         VARCHAR2(255)         := 'VAT_TYPE';
  DEPT_LEVEL_VAT$VAT_CODE         VARCHAR2(255)         := 'VAT_CODE';

  action_column                   VARCHAR2(255)         :='ACTION';
  template_category               CODE_DETAIL.CODE%TYPE :='RMSADM';
-------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind IN       CHAR DEFAULT 'N')
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
 FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
 RETURN BOOLEAN;
-------------------------------------------------------------------------------
END XXADEO_DEPT_LEVEL_VAT_SQL;
/
