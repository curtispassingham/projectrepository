create or replace package body XXADEO_ITEMLOC_IN is

  PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                          IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cause          IN VARCHAR2,
                          I_program        IN VARCHAR2);

  --------------------------------------------------------------------------------------------------------
  -- Name: ENRICH_VALIDATE
  -- Description: Procedure responsible to validate information and enrich the information if required
  --------------------------------------------------------------------------------------------------------
  PROCEDURE ENRICH_VALIDATE(O_error_message OUT VARCHAR2,
                            O_status_code   OUT VARCHAR2) is
  
    CURSOR c_get_rows_core is
      Select item, loc, primary_sup, src_method
        from xxadeo_stg_itemloc_in
       where pub_status = 'N'
         and op_type = 'CORE';
  
    CURSOR c_get_rows_attr is
      Select item, loc, attrib_1, attrib_2, attrib_3, attrib_4, attrib_5
        from xxadeo_stg_itemloc_in
       where pub_status = 'N'
         and op_type = 'ATTR';
  
    CURSOR c_get_cfas_id is
      SELECT parameter, to_number(value_1) as cfa_id
        from xxadeo_mom_dvm
       where func_area = 'ITEM_LOC_CFA'
         and parameter like 'ATTRIB_%'
       order by parameter asc;
  
    CURSOR C_Location(I_location number) is
      select count(1)
        from (select store         location                     
                from store
              union
              select wh            location
                from wh
              union
              select TO_NUMBER(partner_id) location
                from partner
               where partner_type = 'E') loc
       where loc.location = I_location;
  
    L_row_core          c_get_rows_core%ROWTYPE;
    L_row_attr          c_get_rows_attr%ROWTYPE;
    L_count             number;
    L_error_message     VARCHAR2(400);
    L_message_type      VARCHAR2(15);
    L_origin_country_id ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
    L_store_ord_mult    ITEM_LOC.STORE_ORD_MULT%TYPE := NULL;
    L_dsd_ind           SUPS.DSD_IND%TYPE;
  
    type t_cfas is record(
      parameter varchar2(30),
      cfa_id    number);
  
    type t_cfas_tbl is table of t_cfas;
  
    L_cfas t_cfas_tbl;
  
  BEGIN
  
    O_error_message := 'S';
    open c_get_rows_core;
    LOOP
      FETCH c_get_rows_core
        INTO L_row_core;
    
      EXIT WHEN c_get_rows_core%notfound;
    
      -- validate if location is valid
      open c_location(L_row_core.loc);
      fetch c_location
        into L_count;
      close c_location;
    
      if L_count = 0 then
        L_error_message := 'Location is not valid.';
      
        update xxadeo_stg_itemloc_in
           set error_message        = L_error_message,
               pub_status           = 'E',
               last_update_datetime = sysdate
         where item = L_row_core.item
           and primary_sup = L_row_core.primary_sup
           and loc = L_row_core.loc
           and pub_status = 'N';
      
        Continue;
      end if;
    
      -- validate if item/ supplier combination exist
      select count(1)
        into L_count
        from item_supplier
       where item = L_row_core.item
         and supplier = L_row_core.primary_sup;
    
      if L_count = 0 then
      
        L_error_message := 'Item and Supplier combination not exist.';
      
        update xxadeo_stg_itemloc_in
           set error_message        = L_error_message,
               pub_status           = 'E',
               last_update_datetime = sysdate
         where item = L_row_core.item
           and primary_sup = L_row_core.primary_sup
           and loc = L_row_core.loc
           and pub_status = 'N';
      
        Continue;
      end if;
    
      -- define message_type
      select count(1)
        into L_count
        from item_loc
       where item = L_row_core.item
         and loc = L_row_core.loc;
    
      if L_count = 0 then
        L_message_type := 'xitemloccre';
      else
        L_message_type := 'xitemlocmod';
      
        select store_ord_mult
          into l_store_ord_mult
          from item_loc
         where item = L_row_core.item
           and loc = L_row_core.loc;
      
      end if;
    
      -- get origin_country_id
      select origin_country_id
        into L_origin_country_id
        from item_supp_country
       where item = L_row_core.item
         and supplier = L_row_core.primary_sup
         and primary_country_ind = 'Y';
    
      if L_row_core.src_method = 'S' then
        select dsd_ind
          into L_dsd_ind
          from sups
         where supplier = L_row_core.primary_sup;
      
        if l_dsd_ind = 'N' then
          L_error_message := 'Supplier not supports direct store delivery.';
        
          update xxadeo_stg_itemloc_in
             set error_message        = L_error_message,
                 pub_status           = 'E',
                 last_update_datetime = sysdate
           where item = L_row_core.item
             and primary_sup = L_row_core.primary_sup
             and loc = L_row_core.loc
             and pub_status = 'N';
        
          Continue;
        end if;
      
      end if;
    
      UPDATE xxadeo_stg_itemloc_in
         set message_type         = L_message_type,
             origin_country_id    = L_origin_country_id,
             store_ord_mult       = L_store_ord_mult,
             last_update_datetime = sysdate,
             pub_status           = 'R'
       where item = L_row_core.item
         and primary_sup = L_row_core.primary_sup
         and loc = L_row_core.loc
         and pub_status = 'N';
    
    end loop;
    close c_get_rows_core;
  
    -- attrib
    open c_get_cfas_id;
    FETCH c_get_cfas_id BULK COLLECT
      INTO L_cfas;
    close c_get_cfas_id;
  
    if L_cfas is not null and L_cfas.count > 0 then
      open c_get_rows_attr;
      LOOP
        FETCH c_get_rows_attr
          INTO L_row_attr;
      
        EXIT WHEN c_get_rows_attr%notfound;
      
        for i in 1 .. L_cfas.count loop
          if L_cfas(i).parameter = 'ATTRIB_1' then
          
            if XXADEO_CFAS_UTILS.VALIDATE_CFA(L_cfas(i).cfa_id,
                                              l_row_attr.attrib_1) = FALSE then
              L_error_message := 'Value for cfa ' || L_cfas(i).cfa_id ||
                                 'is not valid. (ATTRIB_1)';
            
              update xxadeo_stg_itemloc_in
                 set error_message        = L_error_message,
                     pub_status           = 'E',
                     last_update_datetime = sysdate
               where item = L_row_attr.item
                 and loc = L_row_attr.loc
                 and pub_status = 'N';
            
              Continue;
            end if;
          
          elsif L_cfas(i).parameter = 'ATTRIB_2' then
            if XXADEO_CFAS_UTILS.VALIDATE_CFA(L_cfas(i).cfa_id,
                                              l_row_attr.attrib_2) = FALSE then
              L_error_message := 'Value for cfa ' || L_cfas(i).cfa_id ||
                                 'is not valid (ATTRIB_2)';
            
              update xxadeo_stg_itemloc_in
                 set error_message        = L_error_message,
                     pub_status           = 'E',
                     last_update_datetime = sysdate
               where item = L_row_attr.item
                 and loc = L_row_attr.loc
                 and pub_status = 'N';
            
              Continue;
            end if;
          elsif L_cfas(i).parameter = 'ATTRIB_3' then
            if XXADEO_CFAS_UTILS.VALIDATE_CFA(L_cfas(i).cfa_id,
                                              l_row_attr.attrib_3) = FALSE then
              L_error_message := 'Value for cfa ' || L_cfas(i).cfa_id ||
                                 'is not valid (ATTRIB_3)';
            
              update xxadeo_stg_itemloc_in
                 set error_message        = L_error_message,
                     pub_status           = 'E',
                     last_update_datetime = sysdate
               where item = L_row_attr.item
                 and loc = L_row_attr.loc
                 and pub_status = 'N';
            
              Continue;
            end if;
          elsif L_cfas(i).parameter = 'ATTRIB_4' then
            if XXADEO_CFAS_UTILS.VALIDATE_CFA(L_cfas(i).cfa_id,
                                              l_row_attr.attrib_4) = FALSE then
              L_error_message := 'Value for cfa ' || L_cfas(i).cfa_id ||
                                 'is not valid(ATTRIB_4)';
            
              update xxadeo_stg_itemloc_in
                 set error_message        = L_error_message,
                     pub_status           = 'E',
                     last_update_datetime = sysdate
               where item = L_row_attr.item
                 and loc = L_row_attr.loc
                 and pub_status = 'N';
            
              Continue;
            end if;
          elsif L_cfas(i).parameter = 'ATTRIB_5' then
            if XXADEO_CFAS_UTILS.VALIDATE_CFA(L_cfas(i).cfa_id,
                                              l_row_attr.attrib_5) = FALSE then
              L_error_message := 'Value for cfa ' || L_cfas(i).cfa_id ||
                                 'is not valid (ATTRIB_5)';
            
              update xxadeo_stg_itemloc_in
                 set error_message        = L_error_message,
                     pub_status           = 'E',
                     last_update_datetime = sysdate
               where item = L_row_attr.item
                 and loc = L_row_attr.loc
                 and pub_status = 'N';
            
              Continue;
            end if;
          
          end if;
        
          UPDATE xxadeo_stg_itemloc_in
             set last_update_datetime = sysdate, pub_status = 'R'
           where item = L_row_attr.item
             and loc = L_row_attr.loc
             and pub_status = 'N';
        
        end loop;
      
      end loop;
      close c_get_rows_attr;
    
    else
      update xxadeo_stg_itemloc_in
         set error_message        = 'Not exist values in XXADEO_MOM_DVM.',
             pub_status           = 'E',
             last_update_datetime = sysdate
       where pub_status = 'N'
         and op_type = 'ATTR';
    
    end if;
  
  EXCEPTION
  
    WHEN OTHERS THEN
      O_error_message := 'E';
      HANDLE_ERRORS(O_error_message,
                    L_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    'XXADEO_ITEMLOC_IN.PROCESS');
    
  end ENRICH_VALIDATE;

  -------------------------------------------------------------------------------------------------------
  -- Name: PROCESS
  -- Description: Procedure responsible for process records. 
  --              if record type = 'CORE' upate item_loc table using rmssub_xitemloc
  --              if record type = 'ATTR' update item_loc_cfa_ext using package xxadeo_cfs_utils
  ------------------------------------------------------------------------------------------------------- 
  PROCEDURE PROCESS(I_DEPT          NUMBER,
                    O_error_message OUT VARCHAR2,
                    O_status_code   OUT VARCHAR2) IS
  
    CURSOR c_get_rows_core is
      Select stg.item,
             loc,
             loc_type,
             primary_sup,
             origin_country_id,
             src_method,
             source_wh,
             stg.store_ord_mult,
             message_type
        from xxadeo_stg_itemloc_in stg, item_master im
       where pub_status = 'R'
         and op_type = 'CORE'
         and stg.item = im.item
         and im.dept = I_dept;
  
    CURSOR c_get_rows_attr is
      Select stg.item,
             loc,
             loc_type,
             attrib_1,
             attrib_2,
             attrib_3,
             attrib_4,
             attrib_5
        from xxadeo_stg_itemloc_in stg, item_master im
       where pub_status = 'R'
         and op_type = 'ATTR'
         and stg.item = im.item
         and im.dept = I_dept;
  
    CURSOR c_get_cfas_id is
      SELECT parameter, to_number(value_1) as cfa_id
        from xxadeo_mom_dvm
       where func_area = 'ITEM_LOC_CFA'
         and parameter like 'ATTRIB_%'
       order by parameter asc;
  
    type t_cfas is record(
      parameter varchar2(30),
      cfa_id    number);
  
    type t_cfas_tbl is table of t_cfas;
  
    L_cfas t_cfas_tbl;
  
    L_row_core c_get_rows_core%ROWTYPE;
    L_row_attr c_get_rows_attr%ROWTYPE;
  
    L_status ITEM_LOC.STATUS%TYPE;
  
    L_XItemLocDtl_REC  "RIB_XItemLocDtl_REC";
    L_XItemLocDtl_TBL  "RIB_XItemLocDtl_TBL";
    L_XItemLocDesc_REC "RIB_XItemLocDesc_REC";
    L_error_message    varchar2(300);
  
    L_cfas_tbl XXADEO_CFA_DETAILS_TBL;
    L_cfas_rec XXADEO_CFA_DETAILS;
  
    L_status_code RTK_ERRORS.RTK_TEXT%TYPE := 'S';
  
  BEGIN
  
    -- type = core
    open c_get_rows_core;
    LOOP
      FETCH c_get_rows_core
        INTO L_row_core;
    
      EXIT WHEN c_get_rows_core%notfound;
    
      if L_row_core.message_type = 'xitemloccre' then
        if L_row_core.src_method = 'S' then
        
          L_XItemLocDtl_REC := "RIB_XItemLocDtl_REC"(0, --rib oid
                                                     L_row_core.loc, --HIER_VALUE-NUMBER(10)
                                                     L_row_core.primary_sup, --PRIMARY_SUPP-NUMBER(10)
                                                     L_row_core.origin_country_id, --PRIMARY_CNTRY-VARCHAR2(3)
                                                     NULL, --LOCAL_ITEM_DESC-VARCHAR2(250)
                                                     'A', --STATUS-VARCHAR2(1)
                                                     Null, --STORE_ORD_MULT-VARCHAR2(1)
                                                     NULL, --RECEIVE_AS_TYPE-VARCHAR2(1)
                                                     NULL, --TAXABLE_IND-VARCHAR2(1)
                                                     NULL, --TI-NUMBER(12)
                                                     NULL, --HI-NUMBER(12)
                                                     NULL, --DAILY_WASTE_PCT-NUMBER(12)
                                                     NULL, --LOCAL_SHORT_DESC-VARCHAR2(120)
                                                     NULL, --XITEMLOCTRT-RIB_XItemLocTrt_REC()
                                                     NULL, --UIN_TYPE-VARCHAR2(6)
                                                     NULL, --UIN_LABEL-VARCHAR2(6)
                                                     NULL, --CAPTURE_TIME-VARCHAR2(6)
                                                     NULL, --EXT_UIN_IND-VARCHAR2(1)
                                                     NULL, --SOURCE_METHOD-VARCHAR2(1)
                                                     NULL); --SOURCE_WH-NUMBER(10)
        
        else
          L_XItemLocDtl_REC := "RIB_XItemLocDtl_REC"(0, --rib oid
                                                     L_row_core.loc, --HIER_VALUE-NUMBER(10)
                                                     L_row_core.primary_sup, --PRIMARY_SUPP-NUMBER(10)
                                                     L_row_core.origin_country_id, --PRIMARY_CNTRY-VARCHAR2(3)
                                                     NULL, --LOCAL_ITEM_DESC-VARCHAR2(250)
                                                     'A', --STATUS-VARCHAR2(1)
                                                     Null, --STORE_ORD_MULT-VARCHAR2(1)
                                                     NULL, --RECEIVE_AS_TYPE-VARCHAR2(1)
                                                     NULL, --TAXABLE_IND-VARCHAR2(1)
                                                     NULL, --TI-NUMBER(12)
                                                     NULL, --HI-NUMBER(12)
                                                     NULL, --DAILY_WASTE_PCT-NUMBER(12)
                                                     NULL, --LOCAL_SHORT_DESC-VARCHAR2(120)
                                                     NULL, --XITEMLOCTRT-RIB_XItemLocTrt_REC()
                                                     NULL, --UIN_TYPE-VARCHAR2(6)
                                                     NULL, --UIN_LABEL-VARCHAR2(6)
                                                     NULL, --CAPTURE_TIME-VARCHAR2(6)
                                                     NULL, --EXT_UIN_IND-VARCHAR2(1)
                                                     L_row_core.src_method, --SOURCE_METHOD-VARCHAR2(1)
                                                     L_row_core.source_wh); --SOURCE_WH-NUMBER(10)
        
        end if;
      
      elsif L_row_core.message_type = 'xitemlocmod' then
        select status
          into l_status
          from item_loc
         where item = L_row_core.item
           and loc = L_row_core.loc;
      
        if L_row_core.src_method = 'S' then
        
          L_XItemLocDtl_REC := "RIB_XItemLocDtl_REC"(0, --rib oid
                                                     L_row_core.loc, --HIER_VALUE-NUMBER(10)
                                                     L_row_core.primary_sup, --PRIMARY_SUPP-NUMBER(10)
                                                     L_row_core.origin_country_id, --PRIMARY_CNTRY-VARCHAR2(3)
                                                     NULL, --LOCAL_ITEM_DESC-VARCHAR2(250)
                                                     l_status, --STATUS-VARCHAR2(1)
                                                     L_row_core.store_ord_mult, --STORE_ORD_MULT-VARCHAR2(1)
                                                     NULL, --RECEIVE_AS_TYPE-VARCHAR2(1)
                                                     NULL, --TAXABLE_IND-VARCHAR2(1)
                                                     NULL, --TI-NUMBER(12)
                                                     NULL, --HI-NUMBER(12)
                                                     NULL, --DAILY_WASTE_PCT-NUMBER(12)
                                                     NULL, --LOCAL_SHORT_DESC-VARCHAR2(120)
                                                     NULL, --XITEMLOCTRT-RIB_XItemLocTrt_REC()
                                                     NULL, --UIN_TYPE-VARCHAR2(6)
                                                     NULL, --UIN_LABEL-VARCHAR2(6)
                                                     NULL, --CAPTURE_TIME-VARCHAR2(6)
                                                     NULL, --EXT_UIN_IND-VARCHAR2(1)
                                                     NULL, --SOURCE_METHOD-VARCHAR2(1)
                                                     NULL); --SOURCE_WH-NUMBER(10)
        
        else
          L_XItemLocDtl_REC := "RIB_XItemLocDtl_REC"(0, --rib oid
                                                     L_row_core.loc, --HIER_VALUE-NUMBER(10)
                                                     L_row_core.primary_sup, --PRIMARY_SUPP-NUMBER(10)
                                                     L_row_core.origin_country_id, --PRIMARY_CNTRY-VARCHAR2(3)
                                                     NULL, --LOCAL_ITEM_DESC-VARCHAR2(250)
                                                     l_status, --STATUS-VARCHAR2(1)
                                                     L_row_core.store_ord_mult, --STORE_ORD_MULT-VARCHAR2(1)
                                                     NULL, --RECEIVE_AS_TYPE-VARCHAR2(1)
                                                     NULL, --TAXABLE_IND-VARCHAR2(1)
                                                     NULL, --TI-NUMBER(12)
                                                     NULL, --HI-NUMBER(12)
                                                     NULL, --DAILY_WASTE_PCT-NUMBER(12)
                                                     NULL, --LOCAL_SHORT_DESC-VARCHAR2(120)
                                                     NULL, --XITEMLOCTRT-RIB_XItemLocTrt_REC()
                                                     NULL, --UIN_TYPE-VARCHAR2(6)
                                                     NULL, --UIN_LABEL-VARCHAR2(6)
                                                     NULL, --CAPTURE_TIME-VARCHAR2(6)
                                                     NULL, --EXT_UIN_IND-VARCHAR2(1)
                                                     L_row_core.src_method, --SOURCE_METHOD-VARCHAR2(1)
                                                     L_row_core.source_wh); --SOURCE_WH-NUMBER(10)
        
        end if;
      
      end if;
    
      L_XItemLocDtl_TBL := "RIB_XItemLocDtl_TBL"();
      L_XItemLocDtl_TBL.extend();
      L_XItemLocDtl_TBL(L_XItemLocDtl_TBL.LAST) := L_XItemLocDtl_REC;
    
      L_XItemLocDesc_REC := "RIB_XItemLocDesc_REC"(0, --rib oid
                                                   L_row_core.item, --ITEM-VARCHAR2(25)
                                                   L_row_core.loc_type, --HIER_LEVEL-VARCHAR2(2)
                                                   L_XItemLocDtl_TBL); --XITEMLOCDTL_TBL-RIB_XItemLocDtl_TBL()
    
      RMSSUB_XITEMLOC.consume(L_status_code,
                              L_error_message,
                              L_XItemLocDesc_REC,
                              l_row_core.message_type);
    
      if L_status_code = 'E' then
      
        update xxadeo_stg_itemloc_in
           set error_message        = L_error_message,
               pub_status           = 'E',
               last_update_datetime = sysdate
         where item = l_row_core.item
           and primary_sup = l_row_core.primary_sup
           and loc = l_row_core.loc;
        CONTINUE;
      
      else
        update xxadeo_stg_itemloc_in
           set pub_status = 'P', last_update_datetime = sysdate
         where item = l_row_core.item
           and primary_sup = l_row_core.primary_sup
           and loc = l_row_core.loc
           and pub_status = 'R'
           and op_type = 'CORE';
      end if;
    
    END LOOP;
  
    close c_get_rows_core;
  
    -- type = attr
  
    open c_get_cfas_id;
    FETCH c_get_cfas_id BULK COLLECT
      INTO L_cfas;
    close c_get_cfas_id;
  
    if L_cfas is not null and L_cfas.count() > 0 then
      open c_get_rows_attr;
      LOOP
        FETCH c_get_rows_attr
          INTO L_row_attr;
      
        EXIT WHEN c_get_rows_attr%notfound;
      
        L_cfas_tbl := null;
      
        for i in 1 .. L_cfas.count() loop
          if L_cfas(i).parameter = 'ATTRIB_1' then
            L_cfas_rec := XXADEO_CFA_DETAILS(L_cfas(i).Cfa_Id,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             l_row_attr.attrib_1,
                                             null);
          
          elsif L_cfas(i).parameter = 'ATTRIB_2' then
            L_cfas_rec := XXADEO_CFA_DETAILS(L_cfas(i).Cfa_Id,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             l_row_attr.attrib_2,
                                             null);
          elsif L_cfas(i).parameter = 'ATTRIB_3' then
            L_cfas_rec := XXADEO_CFA_DETAILS(L_cfas(i).Cfa_Id,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             l_row_attr.attrib_3,
                                             null);
          elsif L_cfas(i).parameter = 'ATTRIB_4' then
            L_cfas_rec := XXADEO_CFA_DETAILS(L_cfas(i).Cfa_Id,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             l_row_attr.attrib_4,
                                             null);
          elsif L_cfas(i).parameter = 'ATTRIB_5' then
            L_cfas_rec := XXADEO_CFA_DETAILS(L_cfas(i).Cfa_Id,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             l_row_attr.attrib_5,
                                             null);
          end if;
        
          if L_cfas_tbl is null then
            L_cfas_tbl := XXADEO_CFA_DETAILS_TBL();
          end if;
          L_cfas_tbl.extend();
          L_cfas_tbl(L_cfas_tbl.last) := L_cfas_rec;
        
        end loop;
      
        if L_cfas_tbl is not null and L_cfas_tbl.count > 0 then
          XXADEO_CFAS_UTILS.INICIALIZE_CFAS_TO_VIEW(L_cfas_tbl,
                                                    l_row_attr.item || '|' ||
                                                    l_row_attr.loc,
                                                    'ITEM_LOC',
                                                    L_status_code,
                                                    L_error_message);
        
          if L_status_code = 'E' then
          
            update xxadeo_stg_itemloc_in
               set error_message        = L_error_message,
                   pub_status           = 'E',
                   last_update_datetime = sysdate
             where item = l_row_attr.item
               and loc = l_row_attr.loc;
            CONTINUE;
          
          end if;
        
          XXADEO_CFAS_UTILS.SET_CFAS('ITEM_LOC',
                                     l_row_attr.item || '|' ||
                                     l_row_attr.loc,
                                     L_cfas_tbl,
                                     L_status_code,
                                     L_error_message);
        
          if L_status_code = 'E' then
          
            update xxadeo_stg_itemloc_in
               set error_message        = L_error_message,
                   pub_status           = 'E',
                   last_update_datetime = sysdate
             where item = l_row_attr.item
               and loc = l_row_attr.loc;
          
            CONTINUE;
          else
          
            update xxadeo_stg_itemloc_in
               set pub_status = 'P', last_update_datetime = sysdate
             where item = l_row_attr.item
               and loc = l_row_attr.loc
               and pub_status = 'R'
               and op_type = 'ATTR';
          end if;
        end if;
      
      END LOOP;
    
      close c_get_rows_attr;
    
    end if;
  
  EXCEPTION
  
    WHEN OTHERS THEN
      O_error_message := 'E';
      HANDLE_ERRORS(L_status_code,
                    L_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    'XXADEO_ITEMLOC_IN.PROCESS');
    
  END PROCESS;

  -------------------------------------------------------------------------------------------------------
  -- Name: PURGE
  -- Description: Procedure responsible for delete processed records. 
  ------------------------------------------------------------------------------------------------------- 
  PROCEDURE PURGE(O_error_message OUT VARCHAR2, O_status_code OUT VARCHAR2) IS
  
  BEGIN
  
    delete from xxadeo_stg_itemloc_in where pub_status = 'P';
  
  END PURGE;

  -------------------------------------------------------------------------------------------------------
  -- Name: HANDLE_ERRORS
  -- Description: Procedure responsible for process errors if any occurs.
  ------------------------------------------------------------------------------------------------------- 

  PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                          IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cause          IN VARCHAR2,
                          I_program        IN VARCHAR2) IS
  
    L_program VARCHAR2(50) := 'XXADEO_ITEMLOC_IN.HANDLE_ERRORS';
  
  BEGIN
  
    API_LIBRARY.HANDLE_ERRORS(O_status_code,
                              IO_error_message,
                              I_cause,
                              I_program);
  EXCEPTION
    when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
    
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
    
  END HANDLE_ERRORS;

END XXADEO_ITEMLOC_IN;
/