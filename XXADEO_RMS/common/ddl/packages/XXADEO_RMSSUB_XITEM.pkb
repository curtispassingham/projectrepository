CREATE OR REPLACE PACKAGE BODY XXADEO_RMSSUB_XITEM AS
  /*---------------------------------------------------------------------*/
  /*
  * Object Name:   XXADEO_RMSSUB_XITEM.pks
  * Description:   Wrapper Package that will be used by RIB to consume 
  *                item information.
  *                
  * Version:       1.0
  * Author:        Liliana Soraia Ferreira
  * Creation Date: 23/05/2018
  * Last Modified: 23/05/2018
  * History:
  *               1.0 - Initial version
  */
  /*------------------------------------------------------------------------*/
  -------------------------------------------------------------------------------
  -- CHANGE TAG  - #001
  -- CHANGE DATE - 21/01/2019
  -- CHANGE USER - Tiago Torres
  -- PROJECT     - ADEO
  -- DESCRIPTION - CR391
-------------------------------------------------------------------------------


  -------------------------------------------------------------------------------------------------------
  -- PRIVATE PROCEDURES
  -------------------------------------------------------------------------------------------------------
  PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                          IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cause          IN VARCHAR2,
                          I_program        IN VARCHAR2);

  PROCEDURE POPULATE_SKUS(I_message       IN "RIB_XItemDesc_REC",
                          O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_error_message IN OUT VARCHAR2);

  PROCEDURE VERIFY_UDAS(I_message       IN "RIB_XItemUDADtl_TBL",
                        I_bu            IN number,
                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_message IN OUT VARCHAR2);

  PROCEDURE VERIFY_CFAS(I_message       IN "RIB_CustFlexAttriVo_TBL",
                        I_entity        IN VARCHAR2,
                        I_bu            IN number,
                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_message IN OUT VARCHAR2);

  PROCEDURE INSERT_EXTOF(I_message       IN "RIB_ExtOfXItemDesc_REC",
                         I_item          IN ITEM_MASTER.ITEM%TYPE,
                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error_message IN OUT VARCHAR2);

  PROCEDURE POPULATE_STYLE(I_message       IN "RIB_XItemDesc_REC",
                           O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_error_message IN OUT VARCHAR2);

  PROCEDURE UPDATE_SKUS(I_message       IN "RIB_XItemDesc_REC",
                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_message IN OUT VARCHAR2);

  PROCEDURE UPDATE_STYLE(I_message       IN "RIB_XItemDesc_REC",
                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_ITEM_MASTER(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                I_message       IN "RIB_XItemDesc_REC",
                                O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_BARCODES(I_message       IN "RIB_XItemDesc_REC",
                             O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_ITEM_TL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                            I_message       IN "RIB_LangOfXItemDesc_TBL",
                            O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_ITEM_IMAGE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                               I_message       IN "RIB_XItemImage_TBL",
                               O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_ITEM_IMAGE_TL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                  I_image_name    IN ITEM_IMAGE.IMAGE_NAME%TYPE,
                                  I_message       IN "RIB_LangOfXItemImage_TBL",
                                  O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_UDA_LOV(I_item          IN ITEM_MASTER.ITEM%TYPE,
                            I_bu            IN NUMBER,
                            I_udas_msg      IN "RIB_XItemUDADtl_TBL",
                            O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_UDA_FF(I_item          IN ITEM_MASTER.ITEM%TYPE,
                           I_bu            IN NUMBER,
                           I_udas_msg      IN "RIB_XItemUDADtl_TBL",
                           O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_UDA_DATE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                             I_bu            IN NUMBER,
                             I_udas_msg      IN "RIB_XItemUDADtl_TBL",
                             O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_UDAS(I_item          IN ITEM_MASTER.ITEM%TYPE,
                         I_bu            IN NUMBER,
                         I_message       IN "RIB_XItemUDADtl_TBL",
                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_VAT_ITEM(I_item          IN ITEM_MASTER.ITEM%TYPE,
                             I_message       IN "RIB_XItemVATDesc_TBL",
                             O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_SUP(I_item          IN ITEM_MASTER.ITEM%TYPE,
                        I_message       IN "RIB_XItemSupDesc_TBL",
                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_SUP_BARCODE(I_barcode_id    IN ITEM_MASTER.ITEM%TYPE,
                                I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_ITEM_SUP_CTY(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                 I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                 I_message       IN "RIB_XItemSupCtyDesc_TBL",
                                 O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_ITEM_SUP_CTY_MFR(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                     I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                     I_message       IN "RIB_XItmSupCtyMfrDesc_TBL",
                                     O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_ITEM_SUP_CTY_DIM(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                     I_supplier          IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                     I_origin_country_id IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                     I_message           IN "RIB_XISCDimDesc_TBL",
                                     O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_error_message     IN OUT VARCHAR2);

  PROCEDURE COMPARE_RELATED_ITEM(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                 I_message       IN "RIB_ExtOfReltdItem_TBL",
                                 O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error_message IN OUT VARCHAR2);

  PROCEDURE POPULATE_RELATED_ITEM(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                  I_message       IN "RIB_ExtOfReltdItem_TBL",
                                  O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error_message IN OUT VARCHAR2);

  PROCEDURE COMPARE_CFAS(I_entity        IN VARCHAR2,
                         I_identifier    IN VARCHAR2,
                         I_bu            IN NUMBER,
                         I_CFAS          IN "RIB_CustFlexAttriVo_TBL",
                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error_message IN OUT VARCHAR2);

  PROCEDURE CREATE_UDA_BU(I_item          IN ITEM_MASTER.ITEM%TYPE,
                          I_bu            IN VARCHAR2,
                          O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_error_message IN OUT VARCHAR2);

  -------------------------------------------------------------------------------------------------------
  -- Name: VERIFY_CFAS 
  -- Description: Verify if CFAS received are in scope of XItem
  -------------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_CFAS(I_message       IN "RIB_CustFlexAttriVo_TBL",
                        I_entity        IN VARCHAR2,
                        I_bu            IN number,
                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_message IN OUT VARCHAR2) IS
  
    CURSOR C_validate_cfas_item IS
      select id
        from TABLE(CAST(I_message as "RIB_CustFlexAttriVo_TBL")) msg
       where id not in (select to_number(value_1)
                          from xxadeo_mom_dvm
                         where func_area = 'XITEM_CFAS'
                           and parameter = 'CFAS'
                           and bu in (-1, I_bu));
  
    CURSOR C_validate_cfas_item_sup IS
      select id
        from TABLE(CAST(I_message as "RIB_CustFlexAttriVo_TBL")) msg
       where id not in (select to_number(value_1)
                          from xxadeo_mom_dvm
                         where func_area = 'XITEM_SUP_CFAS'
                           and parameter = 'CFAS'
                           and bu in (-1, I_bu));
  
    Type cfas_tbl is table of number;
    V_cfas_tbl cfas_tbl;
    L_ids_cfas VARCHAR2(400);
    L_program  VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.VERIFY_CFAS';
  BEGIN
  
    if I_entity like 'ITEM_MASTER' then
      OPEN C_validate_cfas_item;
      FETCH C_validate_cfas_item BULK COLLECT
        INTO V_cfas_tbl;
      CLOSE C_validate_cfas_item;
    
    elsif I_entity like 'ITEM_SUP' then
      OPEN C_validate_cfas_item_sup;
      FETCH C_validate_cfas_item_sup BULK COLLECT
        INTO V_cfas_tbl;
      CLOSE C_validate_cfas_item_sup;
    
    end if;
  
    if V_cfas_tbl is not null and V_cfas_tbl.count > 0 then
      L_ids_cfas := '( ';
      for i in 1 .. V_cfas_tbl.count loop
        if i = V_cfas_tbl.count then
          L_ids_cfas := L_ids_cfas || V_cfas_tbl(i) || ')';
        else
          L_ids_cfas := L_ids_cfas || V_cfas_tbl(i) || ', ';
        end if;
      end loop;
      if I_entity like 'ITEM_MASTER' then
        O_status_code   := 'E';
        O_error_message := 'CFAs ' || L_ids_cfas ||
                           ' for the Item are not valid for BU ' || I_bu || '.';
      
      else
        O_status_code   := 'E';
        O_error_message := 'CFAs ' || L_ids_cfas ||
                           ' for the Item/Supplier relationship are not valid for BU ' || I_bu || '.';
      
      end if;
    
      raise PROGRAM_ERROR;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END VERIFY_CFAS;

  -------------------------------------------------------------------------------------------------------
  -- Name: VERIFY_UDAS
  -- Description: Verify if UDAS received are in Scope
  -------------------------------------------------------------------------------------------------------
  PROCEDURE VERIFY_UDAS(I_message       IN "RIB_XItemUDADtl_TBL",
                        I_bu            IN number,
                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_message IN OUT VARCHAR2) IS
  
    CURSOR C_validate_udas IS
      select uda_id
        from TABLE(CAST(I_message as "RIB_XItemUDADtl_TBL")) msg
       where uda_id not in (select to_number(value_1)
                              from xxadeo_mom_dvm
                             where func_area = 'XITEM_UDAS'
                               and parameter = 'UDA'
                               and bu in (-1, I_bu));
  
    Type udas_tbl is table of number;
    V_udas_tbl   udas_tbl;
    L_string_ids VARCHAR2(4000);
  
    L_program VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.VERIFY_UDAS';
  BEGIN
    if I_message is not null then
      OPEN C_validate_udas;
      FETCH C_validate_udas BULK COLLECT
        INTO V_udas_tbl;
      CLOSE C_validate_udas;
    
      L_string_ids := '(';
      if V_udas_tbl is not null and V_udas_tbl.count > 0 then
        for i in 1 .. V_udas_tbl.count loop
          if i = V_udas_tbl.count then
            L_string_ids := L_string_ids || V_udas_tbl(i) || ')';
          else
            L_string_ids := L_string_ids || V_udas_tbl(i) || ',';
          end if;
        end loop;
      
        O_status_code   := 'E';
        O_error_message := 'UDAs ' || L_string_ids ||
                           ' received are not valid for BU ' || I_bu || '.';
        raise PROGRAM_ERROR;
      end if;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END VERIFY_UDAS;

  -------------------------------------------------------------------------------------------------------
  -- Name: CREATE_UDA_BU 
  -- Description: Create lifecycle_status UDA and Abbonnement Start date CFA
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CREATE_UDA_BU(I_item          IN ITEM_MASTER.ITEM%TYPE,
                          I_bu            IN VARCHAR2,
                          O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_error_message IN OUT VARCHAR2) is
  
    L_uda_lov_tbl XXADEO_UDA_LOV_TBL;
    L_uda_lov_rec XXADEO_UDA_LOV_REC;
    L_cfas_rec    XXADEO_CFA_DETAILS;
    L_cfas_tbl    XXADEO_CFA_DETAILS_TBL;
  
    L_program VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.CREATE_UDA_BU';
  
  BEGIN
    for udas in (Select to_number(value_1) as uda_id,
                        to_number(value_2) as uda_value
                   from XXADEO_MOM_DVM
                  where bu = to_number(I_bu)
                    and func_area = 'UDA'
                    and parameter = 'LIFECYCLE_STATUS') loop
    
      L_uda_lov_rec := XXADEO_UDA_LOV_REC(udas.uda_id,
                                          udas.uda_value,
                                          null,
                                          null,
                                          null);
    
      if L_uda_lov_tbl is null then
        L_uda_lov_tbl := XXADEO_UDA_LOV_TBL();
      end if;
    
      L_uda_lov_tbl.extend();
      L_uda_lov_tbl(L_uda_lov_tbl.last) := L_uda_lov_rec;
    end loop;
  
    XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_CRE(I_item,
                                              L_uda_lov_tbl,
                                              NULL,
                                              NULL,
                                              O_status_code,
                                              O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    for cfa in (Select to_number(value_1) as cfa_id
                  from XXADEO_MOM_DVM
                 where bu = to_number(I_bu)
                   and func_area = 'CFA'
                   and parameter = 'ABON_START_DATE_ITEM') loop
      L_cfas_rec := XXADEO_CFA_DETAILS(cfa.cfa_id,
                                       'ABON_START_DATE_ITEM',
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       sysdate);
    
      if L_cfas_tbl is null then
        L_cfas_tbl := XXADEO_CFA_DETAILS_TBL();
      end if;
      L_cfas_tbl.extend();
      L_cfas_tbl(L_cfas_tbl.last) := L_cfas_rec;
    end loop;
  
    XXADEO_CFAS_UTILS.SET_CFAS('ITEM_MASTER',
                               I_item,
                               L_cfas_tbl,
                               O_status_code,
                               O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CREATE_UDA_BU;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_SUP
  -- Description: Compare Suppliers Received with suppliers existing in RMS 
  --              Not possible delete Suppliers
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_SUP(I_item          IN ITEM_MASTER.ITEM%TYPE,
                        I_message       IN "RIB_XItemSupDesc_TBL",
                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_message IN OUT VARCHAR2) IS
  
    CURSOR C_item_supplier_upd IS
      Select XXADEO_ITEM_SUP_REC(supplier, vpn)
        from (select supplier, vpn
                from TABLE(CAST(I_message as "RIB_XItemSupDesc_TBL")) msg
               where supplier in (select supplier
                                    from item_supplier
                                   where supplier = msg.supplier
                                     and item = I_item));
  
    CURSOR C_item_supplier_add IS
      Select XXADEO_ITEM_SUP_REC(supplier, vpn)
        from (select supplier, vpn
                from TABLE(CAST(I_message as "RIB_XItemSupDesc_TBL"))
               where supplier not in
                     (select supplier from item_supplier where item = I_item));
  
    V_item_supplier_add XXADEO_ITEM_SUP_TBL;
    V_item_supplier_mod XXADEO_ITEM_SUP_TBL;
    L_vpn               ITEM_SUPPLIER.VPN%TYPE;
    L_program           VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_SUP';
    L_item_level        ITEM_MASTER.ITEM_LEVEL%TYPE;
    L_tran_level        ITEM_MASTER.TRAN_LEVEL%TYPE;
  
  BEGIN
    OPEN C_item_supplier_add;
    FETCH C_item_supplier_add BULK COLLECT
      INTO V_item_supplier_add;
    CLOSE C_item_supplier_add;
    OPEN C_item_supplier_upd;
    FETCH C_item_supplier_upd BULK COLLECT
      INTO V_item_supplier_mod;
    CLOSE C_item_supplier_upd;
  
    if V_item_supplier_add is not null and V_item_supplier_add.count > 0 then
      for i in 1 .. V_item_supplier_add.count loop
        for j in 1 .. I_message.count loop
          if I_message(j).supplier = V_item_supplier_add(i).supplier then
            XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_CRE(I_item,
                                                           I_message(j),
                                                           O_status_code,
                                                           O_error_message);
            if O_status_code = 'E' then
              raise PROGRAM_ERROR;
            end if;
          end if;
        end loop;
      end loop;
    
    end if;
  
    if V_item_supplier_mod is not null and V_item_supplier_mod.count > 0 then
      for i in 1 .. V_item_supplier_mod.count loop
        select vpn
          into L_vpn
          from item_supplier
         where supplier = V_item_supplier_mod(i).supplier
           and item = I_item;
        if (L_vpn != V_item_supplier_mod(i).vpn) or
           (L_vpn is null and V_item_supplier_mod(i).vpn is not null) or
           (L_vpn is not null and V_item_supplier_mod(i).vpn is null) then
          XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_MOD(I_item,
                                                         V_item_supplier_mod(i),
                                                         null,
                                                         O_status_code,
                                                         O_error_message);
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        end if;
      
        select tran_level, item_level
          into L_tran_level, L_item_level
          from item_master
         where item = I_item;
      
        if L_tran_level >= L_item_level then
        
          for j in 1 .. I_message.count loop
            if V_item_supplier_mod(i).supplier = I_message(j).supplier then
              COMPARE_ITEM_SUP_CTY(I_item,
                                   I_message      (j).supplier,
                                   I_message      (j).XItemSupCtyDesc_TBL,
                                   O_status_code,
                                   O_error_message);
              if O_status_code = 'E' then
                raise PROGRAM_ERROR;
              end if;
              COMPARE_ITEM_SUP_CTY_MFR(I_item,
                                       I_message      (j).supplier,
                                       I_message      (j)
                                       .XItmSupCtyMfrDesc_TBL,
                                       O_status_code,
                                       O_error_message);
              if O_status_code = 'E' then
                raise PROGRAM_ERROR;
              end if;
            
            end if;
          end loop;
        
        end if;
      end loop;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_SUP;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_SUP_BARCODE 
  -- Description: Compare suppliers to Barcodes (when barcode have item_level=2 and tran_level =1) 
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_SUP_BARCODE(I_barcode_id    IN ITEM_MASTER.ITEM%TYPE,
                                I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error_message IN OUT VARCHAR2) IS
  
    L_supplier ITEM_SUPPLIER.SUPPLIER%TYPE;
    L_row      ITEM_SUPPLIER%ROWTYPE;
    L_row_aux  ITEM_SUPPLIER%ROWTYPE;
    L_vpn     ITEM_SUPPLIER.VPN%TYPE;
    L_sup_rec XXADEO_ITEM_SUP_REC;
    L_program VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_SUP_BARCODE';
  
  BEGIN
  
    if I_barcode_id is not null and I_supplier is not null then
      select *
        into L_row
        from item_supplier
       where item = I_barcode_id
         and supplier = I_supplier;
    
      if L_row.primary_supp_ind = 'N' then
      
        select supplier, vpn
          into L_supplier, L_vpn
          from item_supplier
         where item = I_barcode_id
           and primary_supp_ind = 'Y';
      
        L_sup_rec := XXADEO_ITEM_SUP_REC(L_row.supplier, L_row.vpn);
      
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_MOD(I_barcode_id,
                                                       L_sup_rec,
                                                       'Y',
                                                       O_status_code,
                                                       O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
        L_sup_rec := XXADEO_ITEM_SUP_REC(L_supplier, L_vpn);
      
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_MOD(I_barcode_id,
                                                       L_sup_rec,
                                                       'N',
                                                       O_status_code,
                                                       O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      end if;
    
       else
      
      select *
        into L_row
        from item_supplier
       where item in (select item_parent
                        from item_master
                       where item = I_barcode_id)
         and primary_supp_ind = 'Y';
      
      select *
        into L_row_aux
        from item_supplier
       where item = I_barcode_id
         and primary_supp_ind = 'Y';
      
      if L_row.supplier != L_row_aux.supplier then
      
        L_sup_rec := XXADEO_ITEM_SUP_REC(L_row.supplier, L_row.vpn);
      
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_MOD(I_barcode_id,
                                                       L_sup_rec,
                                                       'Y',
                                                       O_status_code,
                                                       O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
        L_sup_rec := XXADEO_ITEM_SUP_REC(L_row_aux.supplier, L_row_aux.vpn);
      
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_MOD(I_barcode_id,
                                                       L_sup_rec,
                                                       'N',
                                                       O_status_code,
                                                       O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_SUP_BARCODE;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_ITEM_SUP_CTY 
  -- Description: Compare Item/Supplier/Country relationship received with Item/Supplier/Country 
  --              relationship existing in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_ITEM_SUP_CTY(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                 I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                 I_message       IN "RIB_XItemSupCtyDesc_TBL",
                                 O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error_message IN OUT VARCHAR2) IS
  
    CURSOR C_item_sup_cty_upd IS
      Select XXADEO_ITEM_SUP_CTY_REC(origin_country_id,
                                     primary_ind,
                                     default_uop,
                                     supp_pack_size,
                                     inner_pack_size,
                                     ti,
                                     hi,
                                     cost_uom)
        from (select origin_country_id,
                     primary_country_ind as primary_ind,
                     default_uop,
                     supp_pack_size,
                     inner_pack_size,
                     ti,
                     hi,
                     cost_uom
                from TABLE(CAST(I_message as "RIB_XItemSupCtyDesc_TBL")) msg
               where origin_country_id in
                     (select origin_country_id
                        from item_supp_country
                       where supplier = I_supplier
                         and item = I_item));
  
    CURSOR C_item_sup_cty_del IS
      Select XXADEO_ITEM_SUP_CTY_REC(origin_country_id,
                                     primary_ind,
                                     default_uop,
                                     supp_pack_size,
                                     inner_pack_size,
                                     ti,
                                     hi,
                                     cost_uom)
        from (select origin_country_id,
                     primary_country_ind as primary_ind,
                     default_uop,
                     supp_pack_size,
                     inner_pack_size,
                     ti,
                     hi,
                     cost_uom
                from item_supp_country
               where item = I_item
                 and supplier = I_supplier
                 and origin_country_id not in
                     (select origin_country_id
                        from TABLE(CAST(I_message as
                                        "RIB_XItemSupCtyDesc_TBL"))));
  
    V_item_sup_country_mod XXADEO_ITEM_SUP_CTY_TBL;
    V_item_sup_country_del XXADEO_ITEM_SUP_CTY_TBL;
    L_program              VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_ITEM_SUP_CTY';
  
  BEGIN
  
    OPEN C_item_sup_cty_upd;
    FETCH C_item_sup_cty_upd BULK COLLECT
      INTO V_item_sup_country_mod;
    CLOSE C_item_sup_cty_upd;
    OPEN C_item_sup_cty_del;
    FETCH C_item_sup_cty_del BULK COLLECT
      INTO V_item_sup_country_del;
    CLOSE C_item_sup_cty_del;
  
    for itemSupCty in (select origin_country_id, rownum
                         from TABLE(CAST(I_message as
                                         "RIB_XItemSupCtyDesc_TBL"))
                        where origin_country_id not in
                              (select origin_country_id
                                 from item_supp_country
                                where item = I_item
                                  and supplier = I_supplier)) loop
    
      if itemSupCty.origin_country_id is not null then
      
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_CTY_CRE(I_item,
                                                           I_supplier,
                                                           I_message(itemSupCty.rownum),
                                                           O_status_code,
                                                           O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      end if;
    end loop;
  
    if V_item_sup_country_mod is not null and
       v_item_sup_country_mod.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_CTY_MOD(I_item,
                                                         I_supplier,
                                                         V_item_sup_country_mod,
                                                         0,
                                                         O_status_code,
                                                         O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
    if V_item_sup_country_del is not null and
       v_item_sup_country_del.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_SUP_CTY_DEL(I_item,
                                                         I_supplier,
                                                         V_item_sup_country_del,
                                                         O_status_code,
                                                         O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
  
    for j in 1 .. I_message.count loop
      COMPARE_ITEM_SUP_CTY_DIM(I_item,
                               I_supplier,
                               I_message      (j).origin_country_id,
                               I_message      (j).XISCDIMDESC_TBL,
                               O_status_code,
                               O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end loop;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_ITEM_SUP_CTY;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_ITEM_SUP_CTY_MFR
  -- Description: Compare Item/Supplier/Manufactuter Country relationships received with
  --              relationships exising in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_ITEM_SUP_CTY_MFR(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                     I_supplier      IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                     I_message       IN "RIB_XItmSupCtyMfrDesc_TBL",
                                     O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_error_message IN OUT VARCHAR2) IS
  
    CURSOR C_isup_cty_mfr_del IS
      select XXADEO_ITEM_SUP_CTY_MFR_REC(manu_country_id,
                                         primary_manu_country_ind)
        from (select manu_country_id,
                     primary_manu_ctry_ind as primary_manu_country_ind
                from item_supp_manu_country
               where item = I_item
                 and supplier = I_supplier
                 and manu_country_id not in
                     (select manufacturer_ctry_id
                        from TABLE(CAST(I_message as
                                        "RIB_XItmSupCtyMfrDesc_TBL"))));
  
    CURSOR C_isup_cty_mfr_add IS
      select XXADEO_ITEM_SUP_CTY_MFR_REC(manu_country_id,
                                         primary_manu_country_ind)
        from (
              
              select manufacturer_ctry_id          as manu_country_id,
                      primary_manufacturer_ctry_ind as primary_manu_country_ind
                from TABLE(CAST(I_message as "RIB_XItmSupCtyMfrDesc_TBL"))
               where manufacturer_ctry_id not in
                     (select manu_country_id
                        from item_supp_manu_country
                       where item = I_item
                         and supplier = I_supplier));
  
    V_isup_cty_mfr_del XXADEO_ITEM_SUP_CTY_MFR_TBL;
    V_isup_cty_mfr_add XXADEO_ITEM_SUP_CTY_MFR_TBL;
    L_program          VARCHAR2(55) := 'XXADEO_RMSSUB_XITEM.COMPARE_ITEM_SUP_CTY_MFR';
  
  BEGIN
    OPEN C_isup_cty_mfr_del;
    FETCH C_isup_cty_mfr_del BULK COLLECT
      INTO V_isup_cty_mfr_del;
    CLOSE C_isup_cty_mfr_del;
  
    OPEN C_isup_cty_mfr_add;
    FETCH C_isup_cty_mfr_add BULK COLLECT
      INTO V_isup_cty_mfr_add;
    CLOSE C_isup_cty_mfr_add;
  
    if V_isup_cty_mfr_add is not null and V_isup_cty_mfr_add.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ISUP_MANU_CTY_CRE(I_item,
                                                          I_supplier,
                                                          V_isup_cty_mfr_add,
                                                          O_status_code,
                                                          O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
  
    if V_isup_cty_mfr_del is not null and V_isup_cty_mfr_del.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ISUP_MANU_CTY_DEL(I_item,
                                                          I_supplier,
                                                          V_isup_cty_mfr_del,
                                                          O_status_code,
                                                          O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END COMPARE_ITEM_SUP_CTY_MFR;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_ITEM_SUP_CTY_DIM 
  -- Description: Compare item/Supplier/Country/Dimensions relationships received with
  --        item/Supplier/Country/Dimensions relationships existing in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_ITEM_SUP_CTY_DIM(I_item              IN ITEM_MASTER.ITEM%TYPE,
                                     I_supplier          IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                                     I_origin_country_id IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                     I_message           IN "RIB_XISCDimDesc_TBL",
                                     O_status_code       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_error_message     IN OUT VARCHAR2) IS
  
    CURSOR C_isup_cty_dim_upd IS
      Select XXADEO_ITEM_SUP_CTY_DIM_REC(dim_object,
                                         lwh_uom,
                                         length,
                                         width,
                                         height,
                                         stat_cube,
                                         weight_uom,
                                         weight,
                                         net_weight)
        from (select dim_object,
                     lwh_uom,
                     length,
                     width,
                     dim_height as height,
                     stat_cube,
                     weight_uom,
                     weight,
                     net_weight
                from TABLE(CAST(I_message as "RIB_XISCDimDesc_TBL")) msg
               where dim_object in
                     (select dim_object
                        from item_supp_country_dim
                       where dim_object = msg.dim_object
                         and item = I_item
                         and supplier = I_supplier
                         and origin_country = I_origin_country_id));
  
    CURSOR C_isup_cty_dim_del IS
      Select XXADEO_ITEM_SUP_CTY_DIM_REC(dim_object,
                                         lwh_uom,
                                         length,
                                         width,
                                         height,
                                         stat_cube,
                                         weight_uom,
                                         weight,
                                         net_weight)
        from (select dim_object,
                     lwh_uom,
                     length,
                     width,
                     height,
                     stat_cube,
                     weight_uom,
                     weight,
                     net_weight
                from item_supp_country_dim
               where item = I_item
                 and supplier = I_supplier
                 and origin_country = I_origin_country_id
                 and dim_object not in
                     (select dim_object
                        from TABLE(CAST(I_message as "RIB_XISCDimDesc_TBL"))));
  
    CURSOR C_isup_cty_dim_add IS
      Select XXADEO_ITEM_SUP_CTY_DIM_REC(dim_object,
                                         lwh_uom,
                                         length,
                                         width,
                                         height,
                                         stat_cube,
                                         weight_uom,
                                         weight,
                                         net_weight)
        from (select dim_object,
                     lwh_uom,
                     length,
                     width,
                     dim_height as height,
                     stat_cube,
                     weight_uom,
                     weight,
                     net_weight
                from TABLE(CAST(I_message as "RIB_XISCDimDesc_TBL"))
               where dim_object not in
                     (select dim_object
                        from item_supp_country_dim
                       where item = I_item
                         and supplier = I_supplier
                         and origin_country = I_origin_country_id));
  
    V_isup_cty_dim_add XXADEO_ITEM_SUP_CTY_DIM_TBL;
    V_isup_cty_dim_mod XXADEO_ITEM_SUP_CTY_DIM_TBL;
    V_isup_cty_dim_del XXADEO_ITEM_SUP_CTY_DIM_TBL;
    L_row              ITEM_SUPP_COUNTRY_DIM%ROWTYPE;
  
    L_program VARCHAR2(55) := 'XXADEO_RMSSUB_XITEM.COMPARE_ITEM_SUP_CTY_DIM';
  
  BEGIN
    OPEN C_isup_cty_dim_add;
    FETCH C_isup_cty_dim_add BULK COLLECT
      INTO V_isup_cty_dim_add;
    CLOSE C_isup_cty_dim_add;
  
    OPEN C_isup_cty_dim_upd;
    FETCH C_isup_cty_dim_upd BULK COLLECT
      INTO V_isup_cty_dim_mod;
    CLOSE C_isup_cty_dim_upd;
  
    OPEN C_isup_cty_dim_del;
    FETCH C_isup_cty_dim_del BULK COLLECT
      INTO V_isup_cty_dim_del;
    CLOSE C_isup_cty_dim_del;
  
    if V_isup_cty_dim_add is not null and V_isup_cty_dim_add.count > 0 then
      for i in 1 .. V_isup_cty_dim_add.count loop
        for j in 1 .. I_message.count loop
          if I_message(j).dim_object = V_isup_cty_dim_add(i).dim_object then
            XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ISUP_CTY_DIM_CRE(I_item,
                                                               I_supplier,
                                                               I_origin_country_id,
                                                               I_message(j),
                                                               O_status_code,
                                                               O_error_message);
          
            if O_status_code = 'E' then
              raise PROGRAM_ERROR;
            end if;
          end if;
        
        end loop;
      end loop;
    
    end if;
    if V_isup_cty_dim_del is not null and V_isup_cty_dim_del.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ISUP_CTY_DIM_DEL(I_item,
                                                         I_supplier,
                                                         I_origin_country_id,
                                                         V_isup_cty_dim_del,
                                                         O_status_code,
                                                         O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
    if V_isup_cty_dim_mod is not null and V_isup_cty_dim_mod.count > 0 then
    
      for i in 1 .. V_isup_cty_dim_mod.count loop
      
        select *
          into L_row
          from item_supp_country_dim
         where item = I_item
           and supplier = I_supplier
           and origin_country = I_origin_country_id
           and dim_object = V_isup_cty_dim_mod(i).dim_object;
      
        if L_row.length != V_isup_cty_dim_mod(i).length or
           L_row.lwh_uom != V_isup_cty_dim_mod(i).lwh_uom or
           L_row.width != V_isup_cty_dim_mod(i).width or
           L_row.height != V_isup_cty_dim_mod(i).height or
           L_row.stat_cube != V_isup_cty_dim_mod(i).stat_cube or
           L_row.weight_uom != V_isup_cty_dim_mod(i).weight_uom or
           L_row.weight != V_isup_cty_dim_mod(i).weight or
           L_row.net_weight != V_isup_cty_dim_mod(i).net_weight then
          for j in 1 .. I_message.count loop
            if I_message(j).dim_object = V_isup_cty_dim_mod(i).dim_object then
              XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ISUP_CTY_DIM_MOD(I_item,
                                                                 I_supplier,
                                                                 I_origin_country_id,
                                                                 I_message(j),
                                                                 O_status_code,
                                                                 O_error_message);
              if O_status_code = 'E' then
                raise PROGRAM_ERROR;
              end if;
            end if;
          end loop;
        end if;
      end loop;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_ITEM_SUP_CTY_DIM;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_UDAS
  -- Description: Compare UDAS according type (LOV, FF, DATE)
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_UDAS(I_item          IN ITEM_MASTER.ITEM%TYPE,
                         I_bu            IN NUMBER,
                         I_message       IN "RIB_XItemUDADtl_TBL",
                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error_message IN OUT VARCHAR2) IS
  
    L_program VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_UDAS';
  
  BEGIN
  
    for i in (select item
                from item_master
               where (item = I_item or item_parent = I_item or
                     item_grandparent = I_item)
                 and item_level <= tran_level) loop
    
      COMPARE_UDA_LOV(i.item,
                      I_bu,
                      I_message,
                      O_status_code,
                      O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
      COMPARE_UDA_FF(i.item,
                     I_bu,
                     I_message,
                     O_status_code,
                     O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
      COMPARE_UDA_DATE(i.item,
                       I_bu,
                       I_message,
                       O_status_code,
                       O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end loop;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_UDAS;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_CFAS
  -- Description: Compare CFAS received with CFAS existing in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_CFAS(I_entity        IN VARCHAR2,
                         I_identifier    IN VARCHAR2,
                         I_bu            IN NUMBER,
                         I_CFAS          IN "RIB_CustFlexAttriVo_TBL",
                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error_message IN OUT VARCHAR2) IS
  
    L_cfas_received_tbl XXADEO_CFA_DETAILS_TBL;
    L_cfa_rec           XXADEO_CFA_DETAILS;
    L_group_id          cfa_attrib.group_id%type;
    L_storage_col_name  cfa_attrib.storage_col_name%type;
    L_data_type         cfa_attrib.data_type%type;
    L_cfa_blk_qlt_id    CFA_ATTRIB.ATTRIB_ID%TYPE;
  
    Cursor C_delete_cfas_item_master is
      Select XXADEO_CFA_DETAILS(id,
                                name,
                                group_id,
                                storage_col_name,
                                data_type,
                                null,
                                null)
        from (Select to_number(value_1) as id,
                     value_2 as name,
                     cfa.group_id as group_id,
                     cfa.storage_col_name,
                     cfa.data_type
                from xxadeo_mom_dvm, cfa_attrib cfa
               where func_area = 'XITEM_CFAS'
                 and parameter = 'CFAS'
                 and bu in (-1, I_bu)
                 and cfa.attrib_id = to_number(value_1)
                 and to_number(value_1) not in
                     (select id
                        from TABLE(CAST(I_CFAS as "RIB_CustFlexAttriVo_TBL"))));
  
    Cursor C_delete_cfas_item_supplier is
      Select XXADEO_CFA_DETAILS(id,
                                name,
                                group_id,
                                storage_col_name,
                                data_type,
                                null,
                                null)
        from (Select to_number(value_1) as id,
                     value_2 as name,
                     cfa.group_id as group_id,
                     cfa.storage_col_name,
                     cfa.data_type
                from xxadeo_mom_dvm, cfa_attrib cfa
               where func_area = 'XITEM_SUP_CFAS'
                 and parameter = 'CFAS'
                 and cfa.attrib_id = to_number(value_1)
                 and bu in (-1, I_bu)
                 and to_number(value_1) not in
                     (select id
                        from TABLE(CAST(I_CFAS as "RIB_CustFlexAttriVo_TBL"))));
  
    V_cfas    XXADEO_CFA_DETAILS_TBL;
    L_program VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_CFAS';
    L_new_cfa XXADEO_CFA_DETAILS;
  
  BEGIN
  
    L_cfas_received_tbl := XXADEO_CFA_DETAILS_TBL();
  
    if I_CFAS is not null and I_CFAS.count > 0 then
    
      for i in 1 .. I_CFAS.count loop
        select group_id, storage_col_name, data_type
          into L_group_id, L_storage_col_name, L_data_type
          from cfa_attrib
         where attrib_id = I_cfas(i).id;
      
        L_cfa_rec := XXADEO_CFA_DETAILS(I_cfas            (i).id,
                                        I_cfas            (i).name,
                                        L_group_id,
                                        L_storage_col_name,
                                        L_data_type,
                                        null,
                                        null);
        if L_data_type like '%DATE%' then
          L_cfa_rec.value_date := I_cfas(i).value_date;
        else
          L_cfa_rec.value := I_cfas(i).value;
        end if;
        L_cfas_received_tbl.extend();
        L_cfas_received_tbl(L_cfas_received_tbl.last) := L_cfa_rec;
      end loop;
    
      if I_entity like 'ITEM_SUPPLIER' then
        select value_1
          into L_cfa_blk_qlt_id
          from xxadeo_mom_dvm
         where func_area = 'CFA'
           and parameter = 'BLK_QUALITY_ITEM_SUPP'; -- cfa 462
      
        for i in 1 .. L_cfas_received_tbl.count loop
          if L_cfas_received_tbl(i).cfa_id = L_cfa_blk_qlt_id then
            XXADEO_XITEM_UTILS_PCK.VALIDATE_CFAS_TO_ITEM_SUP(I_identifier,
                                                             L_cfas_received_tbl(i)
                                                             .value,
                                                             L_new_cfa);
            if L_new_cfa is not null then
              L_cfas_received_tbl.extend();
              L_cfas_received_tbl(L_cfas_received_tbl.last) := L_new_cfa;
            end if;
          end if;
        
        end loop;
      end if;
    
      XXADEO_CFAS_UTILS.INICIALIZE_CFAS_TO_VIEW(L_cfas_received_tbl,
                                                I_identifier,
                                                I_entity,
                                                O_status_code,
                                                O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
      XXADEO_CFAS_UTILS.SET_CFAS(I_entity,
                                 I_identifier,
                                 L_cfas_received_tbl,
                                 O_status_code,
                                 O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
    if I_entity like 'ITEM_MASTER' then
      OPEN C_delete_cfas_item_master;
      FETCH C_delete_cfas_item_master BULK COLLECT
        INTO V_cfas;
      CLOSE C_delete_cfas_item_master;
    elsif I_entity like 'ITEM_SUPPLIER' then
      OPEN C_delete_cfas_item_supplier;
      FETCH C_delete_cfas_item_supplier BULK COLLECT
        INTO V_cfas;
      CLOSE C_delete_cfas_item_supplier;
    end if;
  
    if V_cfas is not null and V_cfas.count > 0 then
      XXADEO_CFAS_UTILS.DELETE_CFAS(I_entity,
                                    I_identifier,
                                    V_cfas,
                                    O_status_code,
                                    O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_CFAS;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_VAT_ITEM
  -- Description: Compare VAT item Received with VAT item existing in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_VAT_ITEM(I_item          IN ITEM_MASTER.ITEM%TYPE,
                             I_message       IN "RIB_XItemVATDesc_TBL",
                             O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error_message IN OUT VARCHAR2) IS
  
    CURSOR C_vat_item_add IS
      Select XXADEO_VAT_ITEM_REC(vat_region,
                                 active_date,
                                 vat_type,
                                 vat_code,
                                 vat_rate)
        from (select vat_region,
                     active_date,
                     vat_type,
                     vat_code,
                     NULL as vat_rate
                from TABLE(CAST(I_message as "RIB_XItemVATDesc_TBL"))
               where vat_region not in
                     (select vat_region from vat_item where item = I_item));
  
    V_vat_item_add XXADEO_VAT_ITEM_TBL;
    L_program      VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_VAT_ITEM';
  
  BEGIN
    OPEN C_vat_item_add;
    FETCH C_vat_item_add BULK COLLECT
      INTO V_vat_item_add;
    CLOSE C_vat_item_add;
  
    for i in (select item
                from item_master
               where item = I_item
                  or item_parent = I_item
                  or item_grandparent = I_item
                 and item_level <= tran_level) loop
    
      if V_vat_item_add is not null and V_vat_item_add.count > 0 then
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_VAT_ITEM_CRE(i.item,
                                                       V_vat_item_add,
                                                       O_status_code,
                                                       O_error_message);
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      end if;
    
    end loop;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_VAT_ITEM;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_UDA_LOV
  -- Description: Compare UDAS LOV received with UDAs LOV existing in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_UDA_LOV(I_item          IN ITEM_MASTER.ITEM%TYPE,
                            I_bu            IN NUMBER,
                            I_udas_msg      IN "RIB_XItemUDADtl_TBL",
                            O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error_message IN OUT VARCHAR2) IS
  
    CURSOR C_udas_lov_upd IS
      Select XXADEO_UDA_LOV_REC(uda_id,
                                uda_value,
                                create_datetime,
                                last_update_datetime,
                                last_update_id)
        from (select uda_id,
                     to_number(uda_value) as uda_value,
                     create_datetime,
                     last_update_datetime,
                     last_update_id
                from TABLE(CAST(I_udas_msg as "RIB_XItemUDADtl_TBL")) msg
               where uda_id in (select uil.uda_id
                                  from uda_item_lov uil, uda
                                 where uil.uda_id = msg.uda_id
                                   and uda.uda_id = uil.uda_id
                                   and uda.single_value_ind = 'Y'
                                   and item = I_item
                                   and msg.display_type = 'LV')
                 and uda_id in (select to_number(value_1)
                                  from xxadeo_mom_dvm
                                 where func_area = 'XITEM_UDAS'
                                   and parameter = 'UDA'
                                   and bu in (-1, I_bu)));
  
    CURSOR C_udas_lov_del IS
      Select XXADEO_UDA_LOV_REC(uda_id,
                                uda_value,
                                create_datetime,
                                last_update_datetime,
                                last_update_id)
        from (select uil.uda_id,
                     TO_NUMBER(uil.uda_value) as uda_value,
                     uil.create_datetime,
                     uil.last_update_datetime,
                     uil.last_update_id
                from uda_item_lov uil, uda
               where item = I_item
                 and uda.uda_id = uil.uda_id
                 and uda.single_value_ind = 'Y'
                 and uil.uda_id not in
                     (select uda_id
                        from TABLE(CAST(I_udas_msg as "RIB_XItemUDADtl_TBL"))
                       where display_type = 'LV')
                 and uil.uda_id not in
                     (select to_number(value_1)
                        from xxadeo_mom_dvm
                       where func_area = 'UDA')
                 and uil.uda_id in (select to_number(value_1)
                                      from xxadeo_mom_dvm
                                     where func_area = 'XITEM_UDAS'
                                       and parameter = 'UDA'
                                       and bu in (-1, I_bu)));
  
    CURSOR C_udas_lov_add IS
      Select XXADEO_UDA_LOV_REC(uda_id,
                                uda_value,
                                create_datetime,
                                last_update_datetime,
                                last_update_id)
        from (select uda_id,
                     to_number(uda_value) as uda_value,
                     create_datetime,
                     last_update_datetime,
                     last_update_id
                from TABLE(CAST(I_udas_msg as "RIB_XItemUDADtl_TBL"))
               where uda_id not in
                     (select uil.uda_id
                        from uda_item_lov uil, uda
                       where item = I_item
                         and uda.uda_id = uil.uda_id
                         and single_value_ind = 'Y')
                 and display_type = 'LV'
                 and uda_id in (select to_number(value_1)
                                  from xxadeo_mom_dvm
                                 where func_area = 'XITEM_UDAS'
                                   and parameter = 'UDA'
                                   and bu in (-1, I_bu))
                 and uda_id not in (select to_number(value_1)
                                      from xxadeo_mom_dvm
                                     where func_area = 'UDA'));
  
    V_uda_lov_cre        XXADEO_UDA_LOV_TBL;
    V_uda_lov_mod        XXADEO_UDA_LOV_TBL;
    V_uda_lov_del        XXADEO_UDA_LOV_TBL;
    L_uda_lov_rec        XXADEO_UDA_LOV_REC;
    V_uda_lov_mod_final  XXADEO_UDA_LOV_TBL;
    V_uda_lov_mod_delete XXADEO_UDA_LOV_TBL;
  
    L_program     VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_UDA_LOV';
    L_exist       NUMBER;
    L_uda_bu_id   NUMBER;
    L_delete      NUMBER;
    L_uda_subtype NUMBER;
    L_uda_type    NUMBER;
    L_uda_value   NUMBER;
  
  BEGIN
  
    OPEN C_udas_lov_add;
    FETCH C_udas_lov_add BULK COLLECT
      INTO V_uda_lov_cre;
    CLOSE C_udas_lov_add;
    OPEN C_udas_lov_upd;
    FETCH C_udas_lov_upd BULK COLLECT
      INTO V_uda_lov_mod;
    CLOSE C_udas_lov_upd;
    OPEN C_udas_lov_del;
    FETCH C_udas_lov_del BULK COLLECT
      INTO V_uda_lov_del;
    CLOSE C_udas_lov_del;
  
    for uda in (select uda_id,
                       to_number(uda_value) as uda_value,
                       create_datetime,
                       last_update_datetime,
                       last_update_id
                  from TABLE(CAST(I_udas_msg as "RIB_XItemUDADtl_TBL"))
                 where display_type = 'LV'
                   and uda_id in
                       (Select to_number(value_1)
                          from xxadeo_mom_dvm
                         where func_area = 'UDA'
                           and parameter = 'BU_SUBSCRIPTION')) loop
    
      if uda.uda_value = I_bu then
      
        Select count(*)
          into L_exist
          from uda_item_lov
         where uda_id = uda.uda_id
           and uda_value = I_bu
           and item = I_item;
      
        if L_exist = 0 then
          if v_uda_lov_cre is null then
            v_uda_lov_cre := XXADEO_UDA_LOV_TBL();
          end if;
          L_uda_lov_rec := XXADEO_UDA_LOV_REC(uda.uda_id,
                                              I_bu,
                                              uda.create_datetime,
                                              uda.last_update_datetime,
                                              uda.last_update_id);
        
          v_uda_lov_cre.extend();
          v_uda_lov_cre(v_uda_lov_cre.last) := L_uda_lov_rec;
        end if;
      end if;
    
    end loop;
  
    select count(*)
      into L_exist
      from uda_item_lov
     where uda_id = (Select to_number(value_1)
                       from xxadeo_mom_dvm
                      where func_area = 'UDA'
                        and parameter = 'BU_SUBSCRIPTION')
       and uda_value = I_bu
       and item = I_item;
  
    if L_exist = 1 then
      Select to_number(value_1)
        into L_uda_bu_id
        from xxadeo_mom_dvm
       where func_area = 'UDA'
         and parameter = 'BU_SUBSCRIPTION';
    
      select count(*)
        into L_delete
        from TABLE(CAST(I_udas_msg as "RIB_XItemUDADtl_TBL"))
       where display_type = 'LV'
         and uda_id in (Select to_number(value_1)
                          from xxadeo_mom_dvm
                         where func_area = 'UDA'
                           and parameter = 'BU_SUBSCRIPTION')
         and uda_id in
             (select uda_id from uda where single_value_ind = 'N')
         and uda_value = I_bu;
    
      if L_delete = 0 then
        if v_uda_lov_del is null then
          v_uda_lov_del := XXADEO_UDA_LOV_TBL();
        end if;
      
        L_uda_lov_rec := XXADEO_UDA_LOV_REC(L_uda_bu_id,
                                            I_bu,
                                            NULL,
                                            NULL,
                                            NULL);
      
        v_uda_lov_del.extend();
        v_uda_lov_del(v_uda_lov_del.last) := L_uda_lov_rec;
      
      end if;
    
    end if;
  
    if v_uda_lov_cre is not null and v_uda_lov_cre.count > 0 then
    
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_CRE(I_item,
                                                v_uda_lov_cre,
                                                NULL,
                                                NULL,
                                                O_status_code,
                                                O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
  
    select to_number(value_1)
      into L_uda_subtype
      from xxadeo_mom_dvm
     where func_area = 'UDA'
       and parameter = 'ITEM_SUBTYPE';
  
    select to_number(value_1)
      into L_uda_type
      from xxadeo_mom_dvm
     where func_area = 'UDA'
       and parameter = 'ITEM_TYPE';
  
    if v_uda_lov_del is not null and v_uda_lov_del.count > 0 then
      for i in 1 .. v_uda_lov_del.count loop
        if v_uda_lov_del(i)
         .uda_id = L_uda_type or v_uda_lov_del(i).uda_id = L_uda_subtype then
          O_error_message := 'Cannot delete UDA, uda_id( ' || v_uda_lov_del(i)
                            .uda_id || ').';
          O_status_code   := 'E';
          raise PROGRAM_ERROR;
        end if;
      end loop;
    
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_DEL(I_item,
                                                v_uda_lov_del,
                                                NULL,
                                                NULL,
                                                O_status_code,
                                                O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
    if v_uda_lov_mod is not null and v_uda_lov_mod.count > 0 then
      for i in 1 .. v_uda_lov_mod.count loop
        if v_uda_lov_mod(i).uda_id = L_uda_type then
          Select uda_value
            into L_uda_value
            from uda_item_lov
           where item = I_item
             and uda_id = L_uda_type;
        
          if L_uda_value != v_uda_lov_mod(i).uda_value then
            O_error_message := 'Cannot modify UDA, uda_id(' || v_uda_lov_mod(i)
                              .uda_id || ').';
            O_status_code   := 'E';
            raise PROGRAM_ERROR;
          end if;
        
        elsif v_uda_lov_mod(i).uda_id = L_uda_subtype then
          Select uda_value
            into L_uda_value
            from uda_item_lov
           where item = I_item
             and uda_id = L_uda_subtype;
        
          if L_uda_value != v_uda_lov_mod(i).uda_value then
            O_error_message := 'Cannot modify UDA, uda_id(' || v_uda_lov_mod(i)
                              .uda_id || ').';
            O_status_code   := 'E';
            raise PROGRAM_ERROR;
          end if;
        else
          Select uda_value
            into L_uda_value
            from uda_item_lov
           where item = I_item
             and uda_id = v_uda_lov_mod(i).uda_id;
        
          if L_uda_value != v_uda_lov_mod(i).uda_value then
            if V_uda_lov_mod_final is null then
              V_uda_lov_mod_final := XXADEO_UDA_LOV_TBL();
            end if;
            V_uda_lov_mod_final.extend();
            V_uda_lov_mod_final(V_uda_lov_mod_final.last) := v_uda_lov_mod(i);
            if V_uda_lov_mod_delete is null then
              V_uda_lov_mod_delete := XXADEO_UDA_LOV_TBL();
            end if;
            V_uda_lov_mod_delete.extend();
            V_uda_lov_mod_delete(V_uda_lov_mod_delete.last) := v_uda_lov_mod(i);
            V_uda_lov_mod_delete(V_uda_lov_mod_delete.last).uda_value := L_uda_value;
          
          end if;
        
        end if;
      end loop;
    
      if V_uda_lov_mod_final is not null and V_uda_lov_mod_final.count > 0 then
      
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_DEL(I_item,
                                                  V_uda_lov_mod_delete,
                                                  NULL,
                                                  NULL,
                                                  O_status_code,
                                                  O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_CRE(I_item,
                                                  V_uda_lov_mod_final,
                                                  NULL,
                                                  NULL,
                                                  O_status_code,
                                                  O_error_message);
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end if;
    end if;
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_UDA_LOV;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_UDA_FF
  -- Description: Compare UDAs FF received with UDAs FF existing in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_UDA_FF(I_item          IN ITEM_MASTER.ITEM%TYPE,
                           I_bu            IN NUMBER,
                           I_udas_msg      IN "RIB_XItemUDADtl_TBL",
                           O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_error_message IN OUT VARCHAR2) IS
  
    CURSOR C_udas_ff_upd IS
      Select XXADEO_UDA_FF_REC(uda_id,
                               uda_text,
                               create_datetime,
                               last_update_datetime,
                               last_update_id)
        from (select uda_id,
                     uda_text,
                     create_datetime,
                     last_update_datetime,
                     last_update_id
                from TABLE(CAST(I_udas_msg as "RIB_XItemUDADtl_TBL")) msg
               where uda_id in (select uda_id
                                  from uda_item_ff
                                 where uda_id = msg.uda_id
                                   and item = I_item
                                   and msg.display_type = 'FF')
                 and uda_id in (select to_number(value_1)
                                  from xxadeo_mom_dvm
                                 where func_area = 'XITEM_UDAS'
                                   and parameter = 'UDA'
                                   and bu in (-1, I_bu)));
  
    CURSOR C_udas_ff_del IS
      Select XXADEO_UDA_FF_REC(uda_id,
                               uda_text,
                               create_datetime,
                               last_update_datetime,
                               last_update_id)
        from (select uda_id,
                     uda_text,
                     create_datetime,
                     last_update_datetime,
                     last_update_id
                from uda_item_ff
               where item = I_item
                 and uda_id not in
                     (select uda_id
                        from TABLE(CAST(I_udas_msg as "RIB_XItemUDADtl_TBL"))
                       where display_type = 'FF')
                 and uda_id in (select to_number(value_1)
                                  from xxadeo_mom_dvm
                                 where func_area = 'XITEM_UDAS'
                                   and parameter = 'UDA'
                                   and bu in (-1, I_bu)));
  
    CURSOR C_udas_ff_add IS
      Select XXADEO_UDA_FF_REC(uda_id,
                               uda_text,
                               create_datetime,
                               last_update_datetime,
                               last_update_id)
        from (select uda_id,
                     uda_text,
                     create_datetime,
                     last_update_datetime,
                     last_update_id
                from TABLE(CAST(I_udas_msg as "RIB_XItemUDADtl_TBL"))
               where uda_id not in
                     (select uda_id from uda_item_ff where item = I_item)
                 and display_type = 'FF'
                 and uda_id in (select to_number(value_1)
                                  from xxadeo_mom_dvm
                                 where func_area = 'XITEM_UDAS'
                                   and parameter = 'UDA'
                                   and bu in (-1, I_bu)));
  
    V_uda_ff_cre XXADEO_UDA_FF_TBL;
    V_uda_ff_mod XXADEO_UDA_FF_TBL;
    V_uda_ff_del XXADEO_UDA_FF_TBL;
    L_program    VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_UDA_FF';
    L_uda_text   UDA_ITEM_FF.UDA_TEXT%TYPE;
  BEGIN
  
    OPEN C_udas_ff_add;
    FETCH C_udas_ff_add BULK COLLECT
      INTO V_uda_ff_cre;
    CLOSE C_udas_ff_add;
    OPEN C_udas_ff_upd;
    FETCH C_udas_ff_upd BULK COLLECT
      INTO V_uda_ff_mod;
    CLOSE C_udas_ff_upd;
    OPEN C_udas_ff_del;
    FETCH C_udas_ff_del BULK COLLECT
      INTO V_uda_ff_del;
    CLOSE C_udas_ff_del;
  
    if v_uda_ff_cre is not null and v_uda_ff_cre.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_CRE(I_item,
                                                NULL,
                                                v_uda_ff_cre,
                                                NULL,
                                                O_status_code,
                                                O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
    if v_uda_ff_del is not null and v_uda_ff_del.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_DEL(I_item,
                                                NULL,
                                                v_uda_ff_del,
                                                NULL,
                                                O_status_code,
                                                O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
    if v_uda_ff_mod is not null and v_uda_ff_mod.count > 0 then
      for i in 1 .. v_uda_ff_mod.count loop
        select uda_text
          into L_uda_text
          from uda_item_ff
         where item = I_item
           and uda_id = v_uda_ff_mod(i).uda_id;
        if L_uda_text != v_uda_ff_mod(i).uda_text then
          XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_DEL(I_item,
                                                    NULL,
                                                    v_uda_ff_mod,
                                                    NULL,
                                                    O_status_code,
                                                    O_error_message);
        
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        
          XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_CRE(I_item,
                                                    NULL,
                                                    v_uda_ff_mod,
                                                    NULL,
                                                    O_status_code,
                                                    O_error_message);
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        end if;
      
      end loop;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_UDA_FF;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_UDA_DATE
  -- Description: Compare UDAs DATE received with UDAs DATE existing in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_UDA_DATE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                             I_bu            IN NUMBER,
                             I_udas_msg      IN "RIB_XItemUDADtl_TBL",
                             O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error_message IN OUT VARCHAR2) IS
  
    CURSOR C_udas_date_upd IS
      Select XXADEO_UDA_DATE_REC(uda_id,
                                 uda_date,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id)
        from (select uda_id,
                     uda_date,
                     create_datetime,
                     last_update_datetime,
                     last_update_id
                from TABLE(CAST(I_udas_msg as "RIB_XItemUDADtl_TBL")) msg
               where uda_id in (select uda_id
                                  from uda_item_date
                                 where uda_id = msg.uda_id
                                   and item = I_item
                                   and msg.display_type = 'DT')
                 and uda_id in (select to_number(value_1)
                                  from xxadeo_mom_dvm
                                 where func_area = 'XITEM_UDAS'
                                   and parameter = 'UDA'
                                   and bu in (-1, I_bu)));
  
    CURSOR C_udas_date_del IS
      Select XXADEO_UDA_DATE_REC(uda_id,
                                 uda_date,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id)
        from (select uda_id,
                     uda_date,
                     create_datetime,
                     last_update_datetime,
                     last_update_id
                from uda_item_date
               where item = I_item
                 and uda_id not in
                     (select uda_id
                        from TABLE(CAST(I_udas_msg as "RIB_XItemUDADtl_TBL"))
                       where display_type = 'DT')
                 and uda_id in (select to_number(value_1)
                                  from xxadeo_mom_dvm
                                 where func_area = 'XITEM_UDAS'
                                   and parameter = 'UDA'
                                   and bu in (-1, I_bu)));
  
    CURSOR C_udas_date_add IS
      Select XXADEO_UDA_DATE_REC(uda_id,
                                 uda_date,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id)
        from (select uda_id,
                     uda_date,
                     create_datetime,
                     last_update_datetime,
                     last_update_id
                from TABLE(CAST(I_udas_msg as "RIB_XItemUDADtl_TBL"))
               where uda_id not in
                     (select uda_id from uda_item_date where item = I_item)
                 and display_type = 'DT'
                 and uda_id in (select to_number(value_1)
                                  from xxadeo_mom_dvm
                                 where func_area = 'XITEM_UDAS'
                                   and parameter = 'UDA'
                                   and bu in (-1, I_bu)));
  
    V_uda_date_cre XXADEO_UDA_DATE_TBL;
    V_uda_date_mod XXADEO_UDA_DATE_TBL;
    V_uda_date_del XXADEO_UDA_DATE_TBL;
    L_program      VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_UDA_DATE';
    L_uda_date     UDA_ITEM_DATE.UDA_DATE%TYPE;
  BEGIN
  
    OPEN C_udas_date_add;
    FETCH C_udas_date_add BULK COLLECT
      INTO V_uda_date_cre;
    CLOSE C_udas_date_add;
    OPEN C_udas_date_upd;
    FETCH C_udas_date_upd BULK COLLECT
      INTO V_uda_date_mod;
    CLOSE C_udas_date_upd;
    OPEN C_udas_date_del;
    FETCH C_udas_date_del BULK COLLECT
      INTO V_uda_date_del;
    CLOSE C_udas_date_del;
  
    if v_uda_date_cre is not null and v_uda_date_cre.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_CRE(I_item,
                                                NULL,
                                                NULL,
                                                v_uda_date_cre,
                                                O_status_code,
                                                O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
    if v_uda_date_del is not null and v_uda_date_del.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_DEL(I_item,
                                                NULL,
                                                NULL,
                                                v_uda_date_del,
                                                O_status_code,
                                                O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
    if v_uda_date_mod is not null and v_uda_date_mod.count > 0 then
      for i in 1 .. v_uda_date_mod.count loop
        select uda_date
          into L_uda_date
          from uda_item_date
         where item = I_item
           and uda_id = v_uda_date_mod(i).uda_id;
        if L_uda_date != v_uda_date_mod(i).uda_date then
          XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_DEL(I_item,
                                                    NULL,
                                                    NULL,
                                                    v_uda_date_mod,
                                                    O_status_code,
                                                    O_error_message);
        
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        
          XXADEO_XITEM_UTILS_PCK.CREATE_MSG_UDA_CRE(I_item,
                                                    NULL,
                                                    NULL,
                                                    v_uda_date_mod,
                                                    O_status_code,
                                                    O_error_message);
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        end if;
      
      end loop;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_UDA_DATE;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_ITEM_TL
  -- Description: Compare Translations received with translations existing in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_ITEM_TL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                            I_message       IN "RIB_LangOfXItemDesc_TBL",
                            O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error_message IN OUT VARCHAR2) IS
  
    CURSOR C_item_master_tl_upd(L_item varchar2) IS
      select XXADEO_ITEM_MASTER_TL_REC(lang,
                                       item_desc,
                                       item_desc_secondary,
                                       short_desc)
        from (select lang, item_desc, item_desc_secondary, short_desc
                from TABLE(CAST(I_message as "RIB_LangOfXItemDesc_TBL")) msg
               where lang in
                     (select lang from item_master_tl where item = L_item));
  
    CURSOR C_item_master_tl_del(L_item varchar2) IS
      select XXADEO_ITEM_MASTER_TL_REC(lang,
                                       item_desc,
                                       item_desc_secondary,
                                       short_desc)
        from (select lang, item_desc, item_desc_secondary, short_desc
                from item_master_tl
               where item = L_item
                 and lang not in
                     (select lang
                        from TABLE(CAST(I_message as
                                        "RIB_LangOfXItemDesc_TBL"))));
  
    CURSOR C_item_master_tl_add(L_item varchar2) IS
      select XXADEO_ITEM_MASTER_TL_REC(lang,
                                       item_desc,
                                       item_desc_secondary,
                                       short_desc)
        from (select lang, item_desc, item_desc_secondary, short_desc
                from TABLE(CAST(I_message as "RIB_LangOfXItemDesc_TBL"))
               where lang not in
                     (select lang from item_master_tl where item = L_item));
  
    V_item_master_tl_add XXADEO_ITEM_MASTER_TL_TBL;
    V_item_master_tl_mod XXADEO_ITEM_MASTER_TL_TBL;
    V_item_master_tl_del XXADEO_ITEM_MASTER_TL_TBL;
    L_program            VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_ITEM_TL';
  
  BEGIN
  
    for j in (select item
                from item_master
               where item = I_item
                  or item_parent = I_item
                  or item_grandparent = I_item) loop
    
      OPEN C_item_master_tl_add(j.item);
      FETCH C_item_master_tl_add BULK COLLECT
        INTO V_item_master_tl_add;
      CLOSE C_item_master_tl_add;
      OPEN C_item_master_tl_del(j.item);
      FETCH C_item_master_tl_del BULK COLLECT
        INTO V_item_master_tl_del;
      CLOSE C_item_master_tl_del;
      OPEN C_item_master_tl_upd(j.item);
      FETCH C_item_master_tl_upd BULK COLLECT
        INTO V_item_master_tl_mod;
      CLOSE C_item_master_tl_upd;
    
      if V_item_master_tl_add is not null and
         V_item_master_tl_add.count > 0 then
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_IM_TL_CRE(j.item,
                                                    V_item_master_tl_add,
                                                    O_status_code,
                                                    O_error_message);
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      end if;
      if V_item_master_tl_mod is not null and
         V_item_master_tl_mod.count > 0 then
      
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_IM_TL_MOD(j.item,
                                                    V_item_master_tl_mod,
                                                    O_status_code,
                                                    O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end if;
      if V_item_master_tl_del is not null and
         V_item_master_tl_del.count > 0 then
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_IM_TL_DEL(j.item,
                                                    V_item_master_tl_del,
                                                    O_status_code,
                                                    O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      end if;
    
    end loop;
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_ITEM_TL;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_ITEM_IMAGE
  -- Description: Compare Item/Image relationship received with Item/Image relationship existing in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_ITEM_IMAGE(I_item          IN ITEM_MASTER.ITEM%TYPE,
                               I_message       IN "RIB_XItemImage_TBL",
                               O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error_message IN OUT VARCHAR2) IS
  
    CURSOR C_item_image_upd IS
      select XXADEO_ITEM_IMAGE_REC(image_name, image_addr, image_desc)
        from (
              
              select image_name, image_addr, image_desc
                from TABLE(CAST(I_message as "RIB_XItemImage_TBL")) msg
               where image_name in
                     (select image_name from item_image where item = I_item));
  
    CURSOR C_item_image_del IS
      select XXADEO_ITEM_IMAGE_REC(image_name, image_addr, image_desc)
        from (select image_name, image_addr, image_desc
                from item_image
               where item = I_item
                 and image_name not in
                     (select image_name
                        from TABLE(CAST(I_message as "RIB_XItemImage_TBL"))));
  
    CURSOR C_item_image_add IS
      select XXADEO_ITEM_IMAGE_REC(image_name, image_addr, image_desc)
        from (select image_name, image_addr, image_desc
                from TABLE(CAST(I_message as "RIB_XItemImage_TBL"))
               where image_name not in
                     (select image_name from item_image where item = I_item));
  
    V_item_image_cre XXADEO_ITEM_IMAGE_TBL;
    V_item_image_mod XXADEO_ITEM_IMAGE_TBL;
    V_item_image_del XXADEO_ITEM_IMAGE_TBL;
    L_program        VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMP_ITEM_IMAGE';
  
  BEGIN
  
    OPEN C_item_image_add;
    FETCH C_item_image_add BULK COLLECT
      INTO V_item_image_cre;
    CLOSE C_item_image_add;
    OPEN C_item_image_upd;
    FETCH C_item_image_upd BULK COLLECT
      INTO V_item_image_mod;
    CLOSE C_item_image_upd;
    OPEN C_item_image_del;
    FETCH C_item_image_del BULK COLLECT
      INTO V_item_image_del;
    CLOSE C_item_image_del;
  
    if v_item_image_cre is not null and v_item_image_cre.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_IMAGE_CRE(I_item,
                                                       v_item_image_cre,
                                                       I_message,
                                                       O_status_code,
                                                       O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
  
    if v_item_image_mod is not null and v_item_image_mod.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_IMAGE_MOD(I_item,
                                                       v_item_image_mod,
                                                       O_status_code,
                                                       O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
    if v_item_image_del is not null and v_item_image_del.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_IMAGE_DEL(I_item,
                                                       v_item_image_del,
                                                       O_status_code,
                                                       O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
  
    for i in 1 .. I_message.count() loop
      if I_message(i).LANGOFXITEMIMAGE_TBL is not null and I_message(i)
         .LANGOFXITEMIMAGE_TBL.count() > 0 then
        COMPARE_ITEM_IMAGE_TL(I_item,
                              I_message      (i).image_name,
                              I_message      (i).LANGOFXITEMIMAGE_TBL,
                              O_status_code,
                              O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      end if;
    end loop;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_ITEM_IMAGE;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_ITEM_IMAGE_TL
  -- Description: Compare Item/Image/Translations relationships received with 
  --        Item/Image/Translations relationships existing in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_ITEM_IMAGE_TL(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                  I_image_name    IN ITEM_IMAGE.IMAGE_NAME%TYPE,
                                  I_message       IN "RIB_LangOfXItemImage_TBL",
                                  O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error_message IN OUT VARCHAR2) IS
  
    CURSOR C_item_image_tl_upd IS
      select XXADEO_ITEM_IMAGE_TL_REC(lang, image_desc)
        from (select lang, image_desc
                from TABLE(CAST(I_message as "RIB_LangOfXItemImage_TBL")) msg
               where lang in (select lang
                                from item_image_tl
                               where item = I_item
                                 and image_name = I_image_name));
  
    CURSOR C_item_image_tl_del IS
      select XXADEO_ITEM_IMAGE_TL_REC(lang, image_desc)
        from (select lang, image_desc
                from item_image_tl
               where item = I_item
                 and image_name = I_image_name
                 and lang not in
                     (select lang
                        from TABLE(CAST(I_message as
                                        "RIB_LangOfXItemImage_TBL"))));
  
    CURSOR C_item_image_tl_add IS
      select XXADEO_ITEM_IMAGE_TL_REC(lang, image_desc)
        from (select lang, image_desc
                from TABLE(CAST(I_message as "RIB_LangOfXItemImage_TBL"))
               where lang not in
                     (select lang
                        from item_image_tl
                       where item = I_item
                         and image_name = I_image_name));
  
    V_item_image_tl_add XXADEO_ITEM_IMAGE_TL_TBL;
    V_item_image_tl_mod XXADEO_ITEM_IMAGE_TL_TBL;
    V_item_image_tl_del XXADEO_ITEM_IMAGE_TL_TBL;
    L_program           VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_ITEM_TL';
  
  BEGIN
    OPEN C_item_image_tl_add;
    FETCH C_item_image_tl_add BULK COLLECT
      INTO V_item_image_tl_add;
    CLOSE C_item_image_tl_add;
    OPEN C_item_image_tl_del;
    FETCH C_item_image_tl_del BULK COLLECT
      INTO V_item_image_tl_del;
    CLOSE C_item_image_tl_del;
    OPEN C_item_image_tl_upd;
    FETCH C_item_image_tl_upd BULK COLLECT
      INTO V_item_image_tl_mod;
    CLOSE C_item_image_tl_upd;
  
    if V_item_image_tl_add is not null and V_item_image_tl_add.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_IMAGE_TL_CRE(I_item,
                                                          I_image_name,
                                                          V_item_image_tl_add,
                                                          O_status_code,
                                                          O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
    if V_item_image_tl_mod is not null and V_item_image_tl_mod.count > 0 then
    
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_IMAGE_TL_MOD(I_item,
                                                          I_image_name,
                                                          V_item_image_tl_mod,
                                                          O_status_code,
                                                          O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
    if V_item_image_tl_del is not null and V_item_image_tl_del.count > 0 then
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_IMAGE_TL_DEL(I_item,
                                                          I_image_name,
                                                          V_item_image_tl_del,
                                                          O_status_code,
                                                          O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_ITEM_IMAGE_TL;

  -------------------------------------------------------------------------------------------------------
  -- Name: UPDATE_SKUS 
  -- Description: Responsible to invoke procedures to compare all information received 
  --              when tran_level = 1
  -------------------------------------------------------------------------------------------------------
  PROCEDURE UPDATE_SKUS(I_message       IN "RIB_XItemDesc_REC",
                        O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_message IN OUT VARCHAR2) IS
  
    L_program VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.UPDATE_SKUS';
  BEGIN
    -- compare item_master
    COMPARE_ITEM_MASTER(I_message.item,
                        I_message,
                        O_status_code,
                        O_error_message);
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    -- translations item descriptions
    COMPARE_ITEM_TL(I_message.item,
                    I_message.LANGOFXITEMDESC_TBL,
                    O_status_code,
                    O_error_message);
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    -- UDAS
    COMPARE_UDAS(I_message.item,
                 I_message.ExtOfXItemDesc_TBL(1).bu,
                 I_message.XItemUDADtl_TBL,
                 O_status_code,
                 O_error_message);
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
    -- Item_image and tls
    COMPARE_ITEM_IMAGE(I_message.item,
                       I_message.XItemImage_TBL,
                       O_status_code,
                       O_error_message);
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
    -- vat_item
    COMPARE_VAT_ITEM(I_message.item,
                     I_message.XItemVatDesc_TBL,
                     O_status_code,
                     O_error_message);
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
    --item_supplier
  
    COMPARE_SUP(I_message.item,
                I_message.XItemSupDesc_TBL,
                O_status_code,
                O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
    -- barcodes
    if I_message.ExtOfXItemDesc_TBL(1).ExtOfBarcodeDesc_TBL is not null and I_message.ExtOfXItemDesc_TBL(1)
       .ExtOfBarcodeDesc_TBL.count > 0 then
    
      COMPARE_BARCODES(I_message, O_status_code, O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
    -- related_items
    if I_message.ExtOfXItemDesc_TBL(1).ExtOfReltdItem_TBL is not null and I_message.ExtOfXItemDesc_TBL(1)
       .ExtOfReltdItem_TBL.count > 0 then
      COMPARE_RELATED_ITEM(I_message.item,
                           I_message.ExtOfXItemDesc_TBL(1)
                           .ExtOfReltdItem_TBL,
                           O_status_code,
                           O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
    -- item cfas
    if I_message.ExtOfXItemDesc_TBL(1).CustFlexAttriVo_TBL is not null and I_message.ExtOfXItemDesc_TBL(1)
       .CustFlexAttriVo_TBL.count > 0 then
      COMPARE_CFAS('ITEM_MASTER',
                   I_message.item,
                   I_message.ExtOfXItemDesc_TBL(1).bu,
                   I_message.ExtOfXItemDesc_TBL(1).CustFlexAttriVo_TBL,
                   O_status_code,
                   O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
    -- item supplier cfas
    if I_message.ExtOfXItemDesc_TBL(1).ExtOfXItemSupDesc_TBL is not null and I_message.ExtOfXItemDesc_TBL(1)
       .ExtOfXItemSupDesc_TBL.count > 0 then
      for i in 1 .. I_message.ExtOfXItemDesc_TBL(1)
                    .ExtOfXItemSupDesc_TBL.count loop
        COMPARE_CFAS('ITEM_SUPPLIER',
                     I_message.item || '|' || I_message.ExtOfXItemDesc_TBL(1).ExtOfXItemSupDesc_TBL(i)
                     .supplier,
                     I_message.ExtOfXItemDesc_TBL(1).bu,
                     I_message.ExtOfXItemDesc_TBL(1).ExtOfXItemSupDesc_TBL(i)
                     .CustFlexAttriVo_TBL,
                     O_status_code,
                     O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end loop;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END UPDATE_SKUS;

  -------------------------------------------------------------------------------------------------------
  -- Name: UPDATE_STYLE
  -- Description:Responsible to invoke procedures to compare all information received 
  --              when tran_level = 2
  -------------------------------------------------------------------------------------------------------
  PROCEDURE UPDATE_STYLE(I_message       IN "RIB_XItemDesc_REC",
                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error_message IN OUT VARCHAR2) IS
  
    L_program VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.UPDATE_STYLE';
  
  BEGIN
  
    -- compare item_master to style
    COMPARE_ITEM_MASTER(I_message.item,
                        I_message,
                        O_status_code,
                        O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    -- compare item_master to sku
  
    for sku in (select item
                  from item_master
                 where item_parent = I_message.item) loop
    
      COMPARE_ITEM_MASTER(sku.item,
                          I_message,
                          O_status_code,
                          O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
      COMPARE_SUP(sku.item,
                  I_message.XItemSupDesc_TBL,
                  O_status_code,
                  O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
      if I_message.ExtOfXItemDesc_TBL(1).CustFlexAttriVo_TBL is not null and I_message.ExtOfXItemDesc_TBL(1)
         .CustFlexAttriVo_TBL.count > 0 then
        COMPARE_CFAS('ITEM_MASTER',
                     sku.item,
                     I_message.ExtOfXItemDesc_TBL(1).bu,
                     I_message.ExtOfXItemDesc_TBL(1).CustFlexAttriVo_TBL,
                     O_status_code,
                     O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      end if;
    
      -- item supplier cfas
      if I_message.ExtOfXItemDesc_TBL(1).ExtOfXItemSupDesc_TBL is not null and I_message.ExtOfXItemDesc_TBL(1)
         .ExtOfXItemSupDesc_TBL.count > 0 then
        for i in 1 .. I_message.ExtOfXItemDesc_TBL(1)
                      .ExtOfXItemSupDesc_TBL.count loop
          COMPARE_CFAS('ITEM_SUPPLIER',
                       sku.item || '|' || I_message.ExtOfXItemDesc_TBL(1).ExtOfXItemSupDesc_TBL(i)
                       .supplier,
                       I_message.ExtOfXItemDesc_TBL(1).bu,
                       I_message.ExtOfXItemDesc_TBL(1).ExtOfXItemSupDesc_TBL(i)
                       .CustFlexAttriVo_TBL,
                       O_status_code,
                       O_error_message);
        
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        
        end loop;
      end if;
    
      --#001 BEG
      /*for barcode in (select item
                        from item_master
                       where item_parent = sku.item) loop
        COMPARE_ITEM_MASTER(barcode.item,
                            I_message,
                            O_status_code,
                            O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
        COMPARE_SUP(barcode.item,
                    I_message.XItemSupDesc_TBL,
                    O_status_code,
                    O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end loop;*/
      --#001 END
    
    end loop;
    -- compare item_master to barcode
  
    COMPARE_ITEM_TL(I_message.item,
                    I_message.LANGOFXITEMDESC_TBL,
                    O_status_code,
                    O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    COMPARE_UDAS(I_message.item,
                 I_message.ExtOfXItemDesc_TBL(1).bu,
                 I_message.XItemUDADtl_TBL,
                 O_status_code,
                 O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    COMPARE_ITEM_IMAGE(I_message.item,
                       I_message.XItemImage_TBL,
                       O_status_code,
                       O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    COMPARE_VAT_ITEM(I_message.item,
                     I_message.XItemVatDesc_TBL,
                     O_status_code,
                     O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    COMPARE_SUP(I_message.item,
                I_message.XItemSupDesc_TBL,
                O_status_code,
                O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    if I_message.ExtOfXItemDesc_TBL(1).CustFlexAttriVo_TBL is not null and I_message.ExtOfXItemDesc_TBL(1)
       .CustFlexAttriVo_TBL.count > 0 then
      COMPARE_CFAS('ITEM_MASTER',
                   I_message.item,
                   I_message.ExtOfXItemDesc_TBL(1).bu,
                   I_message.ExtOfXItemDesc_TBL(1).CustFlexAttriVo_TBL,
                   O_status_code,
                   O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
  
    -- item supplier cfas
    if I_message.ExtOfXItemDesc_TBL(1).ExtOfXItemSupDesc_TBL is not null and I_message.ExtOfXItemDesc_TBL(1)
       .ExtOfXItemSupDesc_TBL.count > 0 then
      for i in 1 .. I_message.ExtOfXItemDesc_TBL(1)
                    .ExtOfXItemSupDesc_TBL.count loop
        COMPARE_CFAS('ITEM_SUPPLIER',
                     I_message.item || '|' || I_message.ExtOfXItemDesc_TBL(1).ExtOfXItemSupDesc_TBL(i)
                     .supplier,
                     I_message.ExtOfXItemDesc_TBL(1).bu,
                     I_message.ExtOfXItemDesc_TBL(1).ExtOfXItemSupDesc_TBL(i)
                     .CustFlexAttriVo_TBL,
                     O_status_code,
                     O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end loop;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END UPDATE_STYLE;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_ITEM_MASTER 
  -- Description: Compare Item received with item existing in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_ITEM_MASTER(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                I_message       IN "RIB_XItemDesc_REC",
                                O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error_message IN OUT VARCHAR2) IS
  
    L_message_type        VARCHAR2(30) := XXADEO_RMSSUB_XITEM.ITEM_UPD;
    L_message             "RIB_XItemDesc_REC";
    L_program             VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_ITEM_MASTER';
    L_store_ord_mult      ITEM_MASTER.STORE_ORD_MULT%TYPE;
    L_aip_case_type       ITEM_MASTER.AIP_CASE_TYPE%TYPE;
    L_short_desc          ITEM_MASTER.SHORT_DESC%TYPE;
    L_item_desc           ITEM_MASTER.ITEM_DESC%TYPE;
    L_item_desc_secondary ITEM_MASTER.ITEM_DESC_SECONDARY%TYPE;
    L_standard_uom        ITEM_MASTER.STANDARD_UOM%TYPE;
  
  BEGIN
  
    SELECT short_desc,
           item_desc,
           item_desc_secondary,
           standard_uom,
           store_ord_mult,
           aip_case_type
      into L_short_desc,
           L_item_desc,
           L_item_desc_secondary,
           L_standard_uom,
           L_store_ord_mult,
           L_aip_case_type
      FROM ITEM_MASTER
     where item = I_item;
  
    if L_short_desc != I_message.short_desc or
       L_item_desc != I_message.item_desc or
       L_item_desc_secondary != I_message.item_desc_secondary or
       L_standard_uom != I_message.standard_uom then
    
      L_message := RMS."RIB_XItemDesc_REC"(0, --rib oid
                                           I_item, --ITEM-VARCHAR2(25)
                                           null, --ITEM_PARENT-VARCHAR2(25)
                                           null, --ITEM_GRANDPARENT-VARCHAR2(25)
                                           null, --PACK_IND-VARCHAR2(1)
                                           null, --ITEM_LEVEL-NUMBER(1)
                                           null, --TRAN_LEVEL-NUMBER(1)
                                           null, --DIFF_1-VARCHAR2(10)
                                           null, --DIFF_2-VARCHAR2(10)
                                           null, --DIFF_3-VARCHAR2(10)
                                           null, --DIFF_4-VARCHAR2(10)
                                           null, --DEPT-NUMBER(4)
                                           null, --CLASS-NUMBER(4)
                                           null, --SUBCLASS-NUMBER(4)
                                           I_message.item_desc, --ITEM_DESC-VARCHAR2(250)
                                           null, --ISCLOC_HIER_LEVEL-VARCHAR2(2)
                                           null, --IZP_HIER_LEVEL-VARCHAR2(2)
                                           I_message.short_desc, --SHORT_DESC-VARCHAR2(120)
                                           null, --COST_ZONE_GROUP_ID-NUMBER(4)
                                           I_message.standard_uom, --STANDARD_UOM-VARCHAR2(4)
                                           L_store_ord_mult, --STORE_ORD_MULT-VARCHAR2(1)
                                           null, --FORECAST_IND-VARCHAR2(1)
                                           null, --SIMPLE_PACK_IND-VARCHAR2(1)
                                           null, --CONTAINS_INNER_IND-VARCHAR2(1)
                                           null, --SELLABLE_IND-VARCHAR2(1)
                                           null, --ORDERABLE_IND-VARCHAR2(1)
                                           null, --PACK_TYPE-VARCHAR2(1)
                                           null, --ORDER_AS_TYPE-VARCHAR2(1)
                                           null, --COMMENTS-VARCHAR2(2000)
                                           null, --CREATE_DATETIME-DATE()
                                           null, --XITEMCTRYDESC_TBL-RIB_XItemCtryDesc_TBL()
                                           null, --XITEMSUPDESC_TBL-RIB_XItemSupDesc_TBL()--- preencher isto
                                           null, --XITEMBOMDESC_TBL-RIB_XItemBOMDesc_TBL()
                                           null, --XITEMVATDESC_TBL-RIB_XItemVATDesc_TBL()
                                           null, --XIZPDESC_TBL-RIB_XIZPDesc_TBL()
                                           null, --XITEMUDADTL_TBL-RIB_XItemUDADtl_TBL()
                                           null, --XITEMSEASON_TBL-RIB_XItemSeason_TBL()
                                           null, --XITEMIMAGE_TBL-RIB_XItemImage_TBL()
                                           null, --STATUS-VARCHAR2(1)
                                           null, --UOM_CONV_FACTOR-NUMBER(20)
                                           null, --PACKAGE_SIZE-NUMBER(12)
                                           null, --HANDLING_TEMP-VARCHAR2(6)
                                           null, --HANDLING_SENSITIVITY-VARCHAR2(6)
                                           null, --MFG_REC_RETAIL-NUMBER(20)
                                           null, --WASTE_TYPE-VARCHAR2(6)
                                           null, --WASTE_PCT-NUMBER(12)
                                           null, --ITEM_NUMBER_TYPE-VARCHAR2(6)
                                           null, --CATCH_WEIGHT_IND-VARCHAR2(1)
                                           null, --CONST_DIMEN_IND-VARCHAR2(1)
                                           null, --GIFT_WRAP_IND-VARCHAR2(1)
                                           null, --SHIP_ALONE_IND-VARCHAR2(1)
                                           null, --EXT_SOURCE_SYSTEM-VARCHAR2(6)
                                           null, --SIZE_GROUP1-NUMBER(4)
                                           null, --SIZE_GROUP2-NUMBER(4)
                                           null, --SIZE1-VARCHAR2(6)
                                           null, --SIZE2-VARCHAR2(6)
                                           null, --COLOR-VARCHAR2(24)
                                           null, --SYSTEM_IND-VARCHAR2(1)
                                           null, --UPC_SUPPLEMENT-NUMBER(5)
                                           null, --UPC_TYPE-VARCHAR2(5)
                                           null, --PRIMARY_UPC_IND-VARCHAR2(1)
                                           null, --PRIMARY_REPL_IND-VARCHAR2(1)
                                           null, --ITEM_AGGREGATE_IND-VARCHAR2(1)
                                           null, --DIFF_1_AGGREGATE_IND-VARCHAR2(1)
                                           null, --DIFF_2_AGGREGATE_IND-VARCHAR2(1)
                                           null, --DIFF_3_AGGREGATE_IND-VARCHAR2(1)
                                           null, --DIFF_4_AGGREGATE_IND-VARCHAR2(1)
                                           null, --PERISHABLE_IND-VARCHAR2(1)
                                           null, --NOTIONAL_PACK_IND-VARCHAR2(1)
                                           null, --SOH_INQUIRY_AT_PACK_IND-VARCHAR2(1)
                                           L_aip_case_type, --AIP_CASE_TYPE-VARCHAR2(6)
                                           null, --ORDER_TYPE-VARCHAR2(6)
                                           null, --SALE_TYPE-VARCHAR2(6)
                                           null, --CATCH_WEIGHT_UOM-VARCHAR2(4)
                                           null, --DEPOSIT_ITEM_TYPE-VARCHAR2(6)
                                           null, --INVENTORY_IND-VARCHAR2(1)
                                           null, --ITEM_XFORM_IND-VARCHAR2(1)
                                           null, --CONTAINER_ITEM-VARCHAR2(25)
                                           null, --PACKAGE_UOM-VARCHAR2(4)
                                           null, --FORMAT_ID-VARCHAR2(1)
                                           null, --PREFIX-NUMBER(2)
                                           null, --BRAND-VARCHAR2(120)
                                           null, --PRODUCT_CLASSIFICATION-VARCHAR2(6)
                                           I_message.item_desc_secondary, --ITEM_DESC_SECONDARY-VARCHAR2(250)
                                           null, --DESC_UP-VARCHAR2(250)
                                           null, --MERCHANDISE_IND-VARCHAR2(1)
                                           null, --ORIGINAL_RETAIL-NUMBER(20)
                                           null, --RETAIL_LABEL_TYPE-VARCHAR2(6)
                                           null, --RETAIL_LABEL_VALUE-NUMBER(20)
                                           null, --DEFAULT_WASTE_PCT-NUMBER(12)
                                           null, --ITEM_SERVICE_LEVEL-VARCHAR2(6)
                                           null, --CHECK_UDA_IND-VARCHAR2(1)
                                           null, --DEPOSIT_IN_PRICE_PER_UOM-VARCHAR2(6)
                                           null, --ATTEMPT_RMS_LOAD-VARCHAR2(6)
                                           null,
                                           null);
    
      XXADEO_RMSSUB_XITEM.CONSUME(O_status_code,
                                  O_error_message,
                                  L_message,
                                  L_message_type);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_ITEM_MASTER;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_BARCODES
  -- Description: Compare Barcodes received with barcodes existing in RMS
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_BARCODES(I_message       IN "RIB_XItemDesc_REC",
                             O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error_message IN OUT VARCHAR2) IS
  
    L_program VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_BARCODES';
  
    type barcode is record(
      barcode_id ITEM_MASTER.ITEM%TYPE,
      supplier   ITEM_SUPPLIER.SUPPLIER%TYPE,
      type       ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE);
  
    rec barcode;
  
    Cursor C_barcodes_upd is
      select barcode_id, supplier, type
        from TABLE(CAST(I_message.ExtOfXItemDesc_TBL(1).ExtOfBarcodeDesc_TBL as
                         "RIB_ExtOfBarcodeDesc_TBL"))
       where barcode_id in
             (select item
                from item_master
               where item_parent = I_message.item);
  
    Cursor C_barcodes_cre is
      select barcode_id, supplier, type
        from TABLE(CAST(I_message.ExtOfXItemDesc_TBL(1).ExtOfBarcodeDesc_TBL as
                         "RIB_ExtOfBarcodeDesc_TBL"))
       where barcode_id not in
             (select item
                from item_master
               where item_parent = I_message.item);
  
    CURSOR C_barcodes_del is
      select item as barcode_id, null as supplier, null as type
        from item_master
       where item_parent = I_message.item
         and item not in
             (select barcode_id as item
                from TABLE(CAST(I_message.ExtOfXItemDesc_TBL(1)
                                .ExtOfBarcodeDesc_TBL as
                                 "RIB_ExtOfBarcodeDesc_TBL")));
  
  BEGIN
  
    -- create barcodes exist in message and not exist in item_master
    for rec in c_barcodes_cre loop
    
      if rec.barcode_id is not null then
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_CRE(I_message,
                                                   2,
                                                   rec.type,
                                                   rec.supplier,
                                                   rec.barcode_id,
                                                   I_message.item,
                                                   null,
                                                   O_status_code,
                                                   O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end if;
    end loop;
  
    -- update barcodes in item_master where exist in message
    for rec in c_barcodes_upd loop
    
      COMPARE_ITEM_MASTER(rec.barcode_id,
                          I_message,
                          O_status_code,
                          O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
      -- compare supplier to barcode existing
      COMPARE_SUP_BARCODE(rec.barcode_id,
                          rec.supplier,
                          O_status_code,
                          O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end loop;
  
    -- delete barcodes where not exist in message 
    for rec in C_barcodes_del LOOP
    
      XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_DEL(rec.barcode_id,
                                                 O_status_code,
                                                 O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    
    end loop;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END COMPARE_BARCODES;

  -------------------------------------------------------------------------------------------------------
  -- Name: COMPARE_RELATED_ITEM
  -- Description: Compare Related Items received with related item existing in RMS
  --              In XItemDesc isn't possible create/update/delete Related Items
  -------------------------------------------------------------------------------------------------------
  PROCEDURE COMPARE_RELATED_ITEM(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                 I_message       IN "RIB_ExtOfReltdItem_TBL",
                                 O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error_message IN OUT VARCHAR2) IS
  
    L_exist           number;
    L_relationship_id RELATED_ITEM_HEAD.RELATIONSHIP_ID%TYPE;
    L_program         VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.COMPARE_RELATED_ITEM';
  
  BEGIN
    for id in (select relationship_id
                 from related_item_head hd
                where relationship_name not in
                      (select relationship_name
                         from TABLE(CAST(I_message as
                                         "RIB_ExtOfReltdItem_TBL")) msg
                        where hd.relationship_type = msg.relationship_type)
                  and item = I_item) loop
    
      delete from related_item_detail
       where relationship_id = id.relationship_id;
    
      delete from related_item_head hd
       where relationship_id = id.relationship_id;
    
    end loop;
  
    for i in 1 .. I_message.count loop
      select count(*)
        into L_exist
        from related_item_head
       where relationship_name = I_message(i).relationship_name
         and relationship_type = I_message(i).relationship_type
         and item = I_item;
    
      if L_exist = 0 then
        insert into related_item_head
          (relationship_id,
           item,
           relationship_name,
           relationship_type,
           mandatory_ind)
        values
          (RELATIONSHIP_ID_SEQ.nextval,
           I_item,
           I_message                  (i).relationship_name,
           I_message                  (i).relationship_type,
           I_message                  (i).mandatory_ind);
      
        select relationship_id
          into L_relationship_id
          from related_item_head
         where relationship_name = I_message(i).relationship_name
           and relationship_type = I_message(i).relationship_type
           and item = I_item;
      
        insert into related_item_detail
          (relationship_id, related_item, priority, start_date, end_date)
        values
          (L_relationship_id,
           I_message        (i).related_item,
           I_message        (i).priority,
           I_message        (i).start_date,
           I_message        (i).end_date);
      else
        update related_item_head
           set mandatory_ind = I_message(i).mandatory_ind
         where item = I_item
           and relationship_name = I_message(i).relationship_name
           and relationship_type = I_message(i).relationship_type;
      
        select relationship_id
          into L_relationship_id
          from related_item_head
         where relationship_name = I_message(i).relationship_name
           and relationship_type = I_message(i).relationship_type
           and item = I_item;
      
        update related_item_detail
           set related_item = I_message(i).related_item,
               start_date   = I_message(i).start_date,
               end_date     = I_message(i).end_date,
               priority     = I_message(i).priority
         where relationship_id = L_relationship_id;
      end if;
    
    end loop;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
  END COMPARE_RELATED_ITEM;

  -------------------------------------------------------------------------------------------------------
  -- Name: POPULATE_RELATED_ITEM
  -- Description: Create Related Items received
  -------------------------------------------------------------------------------------------------------
  PROCEDURE POPULATE_RELATED_ITEM(I_item          IN ITEM_MASTER.ITEM%TYPE,
                                  I_message       IN "RIB_ExtOfReltdItem_TBL",
                                  O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error_message IN OUT VARCHAR2) IS
  
    L_relationship_id RELATED_ITEM_HEAD.RELATIONSHIP_ID%TYPE;
    L_program         VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.POPULATE_RELATED_ITEM';
  BEGIN
  
    for i in 1 .. I_message.count loop
      Select RELATIONSHIP_ID_SEQ.nextval into L_relationship_id from dual;
    
      insert into related_item_head
        (relationship_id,
         item,
         relationship_name,
         relationship_type,
         mandatory_ind)
      values
        (L_relationship_id,
         I_item,
         I_message        (i).relationship_name,
         I_message        (i).relationship_type,
         I_message        (i).mandatory_ind);
    
      insert into related_item_detail
        (relationship_id, related_item, priority, start_date, end_date)
      values
        (L_relationship_id,
         I_message        (i).related_item,
         I_message        (i).priority,
         I_message        (i).start_date,
         I_message        (i).end_date);
    
    end loop;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END POPULATE_RELATED_ITEM;

  -------------------------------------------------------------------------------------------------------
  -- Name: POPULATE_SKUS
  -- Description: Create Item when tran_level = 1
  -------------------------------------------------------------------------------------------------------
  PROCEDURE POPULATE_SKUS(I_message       IN "RIB_XItemDesc_REC",
                          O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_error_message IN OUT VARCHAR2) IS
  
    L_program VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.POPULATE_SKUS';
  
  BEGIN
  -- create sku
    XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_CRE(I_message,
                                               I_message.item_level,
                                               I_message.item_number_type,
                                               null,
                                               I_message.item,
                                               I_message.item_parent,
                                               I_message.item_grandparent,
                                               O_status_code,
                                               O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  -- create barcodes
    if I_message.ExtOfXItemDesc_TBL(1).ExtOfBarcodeDesc_TBL is not null and I_message.ExtOfXItemDesc_TBL(1)
       .ExtOfBarcodeDesc_TBL.count > 0 then
      for i in 1 .. I_message.ExtOfXItemDesc_TBL(1)
                    .ExtOfBarcodeDesc_TBL.count loop
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_CRE(I_message,
                                                   2,
                                                   'EAN13',
                                                   I_message.ExtOfXItemDesc_TBL(1).ExtOfBarcodeDesc_TBL(i)
                                                   .supplier,
                                                   I_message.ExtOfXItemDesc_TBL(1).ExtOfBarcodeDesc_TBL(i)
                                                   .barcode_id,
                                                   I_message.item,
                                                   null,
                                                   O_status_code,
                                                   O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end loop;
    
    end if;
    
    -- extension
    INSERT_EXTOF(I_message.ExtOfXItemDesc_TBL(1),
                 I_message.item,
                 O_status_code,
                 O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END POPULATE_SKUS;

  -------------------------------------------------------------------------------------------------------
  -- Name: POPULATE_STYLE
  -- Description: Create Items when tran_level =2 
  --              Number of SKUs is defined in XXADEO_MOM_DVM
  -------------------------------------------------------------------------------------------------------
  PROCEDURE POPULATE_STYLE(I_message       IN "RIB_XItemDesc_REC",
                           O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_error_message IN OUT VARCHAR2) IS
    L_program VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.POPULATE_STYLE';
  
    L_number_skus    NUMBER;
    L_sku_identifier VARCHAR2(25);
    L_barcode_id VARCHAR2(25);
    
  BEGIN
    
    -- create style  
    XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_CRE(I_message,
                                               I_message.item_level,
                                               I_message.item_number_type,
                                               null,
                                               I_message.item,
                                               I_message.item_parent,
                                               I_message.item_grandparent,
                                               O_status_code,
                                               O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
  -- extension for style
    INSERT_EXTOF(I_message.ExtOfXItemDesc_TBL(1),
                 I_message.item,
                 O_status_code,
                 O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    Select to_number(value_1)
      into L_number_skus
      from xxadeo_mom_dvm
     where func_area = 'XITEM'
       and parameter = 'NUMBER_SKUS'
       and bu = -1;
       
       
  -- number of skus
    if L_number_skus > 0 then
      --#001 BEG
      /*for i in (SELECT CHR(divided + 65) || CHR(remainder + 65) as sufix
                  FROM (SELECT 0 AS initial_val, 0 AS divided, 0 AS remainder
                          FROM dual
                        UNION
                        SELECT LEVEL AS initial_val,
                               TRUNC(LEVEL / 26) AS divided,
                               MOD(LEVEL, 26) AS remainder
                          FROM dual
                        CONNECT BY LEVEL < L_number_skus)
                 ORDER BY initial_val) loop*/
       for i in 1 .. L_number_skus loop
       --#001 END
      
        -- sku
        --#001 BEG
        /*L_sku_identifier := I_message.item || i.sufix;
      
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_CRE(I_message,
                                                   2,
                                                   'MANL',
                                                   null,
                                                   L_sku_identifier,
                                                   I_message.item,
                                                   I_message.item_parent,
                                                   O_status_code,
                                                   O_error_message);*/
        L_barcode_id :=  XXADEO_XITEM_UTILS_PCK.generate_barcode();
        
        L_sku_identifier := L_barcode_id;
        
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_CRE(I_message,
                                                   2,
                                                   'EAN13',
                                                   null,
                                                   L_sku_identifier,
                                                   I_message.item,
                                                   I_message.item_parent,
                                                   O_status_code,
                                                   O_error_message);
        --#001 END
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
        INSERT_EXTOF(I_message.ExtOfXItemDesc_TBL(1),
                     L_sku_identifier,
                     O_status_code,
                     O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
        --#001 BEG  
        --barcode
        /*L_barcode_id :=  XXADEO_XITEM_UTILS_PCK.generate_barcode();
                
        XXADEO_XITEM_UTILS_PCK.CREATE_MSG_ITEM_CRE(I_message,
                                                   3,
                                                   'EAN13',
                                                   null,
                                                  L_barcode_id,
                                                   L_sku_identifier,
                                                   I_message.item,
                                                   O_status_code,
                                                   O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;*/
        --#001 END
      end loop;
    
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END POPULATE_STYLE;

  -------------------------------------------------------------------------------------------------------
  -- Name: INSERT_EXTOF
  -- Description: Insert CFAS at Item Level, Item/Supplier Level and initialize views
  --              Insert Related items
  -------------------------------------------------------------------------------------------------------
  PROCEDURE INSERT_EXTOF(I_message       IN "RIB_ExtOfXItemDesc_REC",
                         I_item          IN ITEM_MASTER.ITEM%TYPE,
                         O_status_code   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error_message IN OUT VARCHAR2) IS
  
    L_cfas_tbl XXADEO_CFA_DETAILS_TBL;
    L_cfas_rec XXADEO_CFA_DETAILS;
  
    L_program VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.INSERT_CFAS';
  
  BEGIN
    -- CFAS to item_master 
    if I_message.CustFlexAttriVo_TBL is not null and
       I_message.CustFlexAttriVo_TBL.count > 0 then
      for i in 1 .. I_message.CustFlexAttriVo_TBL.count loop
        L_cfas_rec := XXADEO_CFA_DETAILS(I_message.CustFlexAttriVo_TBL(i).id,
                                         I_message.CustFlexAttriVo_TBL(i).name,
                                         NULL,
                                         NULL,
                                         NULL,
                                         I_message.CustFlexAttriVo_TBL(i)
                                         .value,
                                         I_message.CustFlexAttriVo_TBL(i)
                                         .value_date);
      
        if L_cfas_tbl is null then
          L_cfas_tbl := XXADEO_CFA_DETAILS_TBL();
        end if;
        L_cfas_tbl.extend();
        L_cfas_tbl(L_cfas_tbl.last) := L_cfas_rec;
      
      end loop;
    
    end if;
    for cfa in (Select to_number(value_1) as cfa_id
                  from XXADEO_MOM_DVM
                 where bu = to_number(I_message.bu)
                   and func_area = 'CFA'
                   and parameter = 'ABON_START_DATE_ITEM') loop
      L_cfas_rec := XXADEO_CFA_DETAILS(cfa.cfa_id,
                                       'ABON_START_DATE_ITEM',
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       sysdate);
    
      if L_cfas_tbl is null then
        L_cfas_tbl := XXADEO_CFA_DETAILS_TBL();
      end if;
      L_cfas_tbl.extend();
      L_cfas_tbl(L_cfas_tbl.last) := L_cfas_rec;
    end loop;
  
    XXADEO_CFAS_UTILS.INICIALIZE_CFAS_TO_VIEW(L_cfas_tbl,
                                              I_item,
                                              'ITEM_MASTER',
                                              O_status_code,
                                              O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    XXADEO_CFAS_UTILS.SET_CFAS('ITEM_MASTER',
                               I_item,
                               L_cfas_tbl,
                               O_status_code,
                               O_error_message);
  
    if O_status_code = 'E' then
      raise PROGRAM_ERROR;
    end if;
  
    if I_message.ExtOfReltdItem_TBL is not null and
       I_message.ExtOfReltdItem_TBL.count > 0 then
       
      POPULATE_RELATED_ITEM(I_item,
                            I_message.ExtOfReltdItem_TBL,
                            O_status_code,
                            O_error_message);
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
  
    if I_message.ExtOfXItemSupDesc_TBL is not null and
       I_message.ExtOfXItemSupDesc_TBL.count > 0 then
    
      for i in 1 .. I_message.ExtOfXItemSupDesc_TBL.count loop
        L_cfas_tbl := null;
        if I_message.ExtOfXItemSupDesc_TBL(i)
         .CustFlexAttriVo_TBL is not null and I_message.ExtOfXItemSupDesc_TBL(i)
           .CustFlexAttriVo_TBL.count > 0 then
        
          for j in 1 .. I_message.ExtOfXItemSupDesc_TBL(i)
                        .CustFlexAttriVo_TBL.count loop
          
            L_cfas_rec := XXADEO_CFA_DETAILS(I_message.ExtOfXItemSupDesc_TBL(i).CustFlexAttriVo_TBL(j).id,
                                             I_message.ExtOfXItemSupDesc_TBL(i).CustFlexAttriVo_TBL(j).name,
                                             NULL,
                                             NULL,
                                             null,
                                             I_message.ExtOfXItemSupDesc_TBL(i).CustFlexAttriVo_TBL(j)
                                             .value,
                                             I_message.ExtOfXItemSupDesc_TBL(i).CustFlexAttriVo_TBL(j)
                                             .value_date);
          
            if L_cfas_tbl is null then
              L_cfas_tbl := XXADEO_CFA_DETAILS_TBL();
            end if;
            L_cfas_tbl.extend();
            L_cfas_tbl(L_cfas_tbl.last) := L_cfas_rec;
          
          end loop;
        
          XXADEO_CFAS_UTILS.INICIALIZE_CFAS_TO_VIEW(L_cfas_tbl,
                                                    I_item || '|' || I_message.ExtOfXItemSupDesc_TBL(i)
                                                    .supplier,
                                                    'ITEM_SUPPLIER',
                                                    O_status_code,
                                                    O_error_message);
        
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        
          XXADEO_CFAS_UTILS.SET_CFAS('ITEM_SUPPLIER',
                                     I_item || '|' || I_message.ExtOfXItemSupDesc_TBL(i)
                                     .supplier,
                                     L_cfas_tbl,
                                     O_status_code,
                                     O_error_message);
        
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        
        end if;
      
      end loop;
    end if;
  
  EXCEPTION
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END INSERT_EXTOF;

  -------------------------------------------------------------------------------------------------------
  -- Name: CONSUME 
  -- Description: This procedure is called by the Rib subscriber adapter to process the 
  --              message data into the target system.
  -------------------------------------------------------------------------------------------------------
  PROCEDURE CONSUME(O_status_code   IN OUT VARCHAR2,
                    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_message       IN RIB_OBJECT,
                    I_message_type  IN VARCHAR2) IS
  
    PROGRAM_ERROR EXCEPTION;
  
    L_program                VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.CONSUME';
    L_message_type           VARCHAR2(30) := LOWER(I_message_type);
    L_message                "RIB_XItemDesc_REC";
    L_message_base           RIB_OBJECT;
    O_code                   NUMBER;
    L_uda_type               NUMBER;
    L_uda_subtype            NUMBER;
    L_uda_BU                 NUMBER;
    L_count_typologie_uda    NUMBER := 0;
    L_count_subtypologie_uda NUMBER := 0;
    L_count_bu_uda           NUMBER := 0;
    L_count_bu_equal         NUMBER := 0;
  
  BEGIN
    O_STATUS_CODE := API_CODES.SUCCESS;
  
    -- perform common api initialization tasks
    if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
    end if;
  
    if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('XXADEO_RMSSUB_INV_MESSAGE',
                                            NULL,
                                            NULL,
                                            NULL);
      raise PROGRAM_ERROR;
    end if;
    if UPPER(L_message_type) = UPPER(ITEM_ADEO) then
    
      L_message := treat(I_MESSAGE AS "RIB_XItemDesc_REC");
    
      if L_message is NULL then
      
        O_error_message := SQL_LIB.CREATE_MSG('XXADEO_RMSSUB_INV_MESSAGE_TYPE',
                                              'No Message',
                                              NULL,
                                              NULL);
        raise PROGRAM_ERROR;
      end if;
    
      if L_message.ExtOfXItemDesc_TBL is null then
        O_error_message := SQL_LIB.CREATE_MSG('XXADEO_RMSSUB_INV_MESSAGE',
                                              NULL,
                                              NULL,
                                              NULL);
      
        raise PROGRAM_ERROR;
      
      end if;
    
      XXADEO_XITEM_UTILS_PCK.VERIFY_SEQUENCING(L_message.item, -- identifier,
                                               L_message.ExtOfXItemDesc_TBL(1).bu, --BU,
                                               L_message.ExtOfXItemDesc_TBL(1)
                                               .extract_date, --extract_date,
                                               O_code,
                                               O_status_code,
                                               O_error_message);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
      if L_message.XITEMUDADTL_TBL is not null and
         L_message.XITEMUDADTL_TBL.count > 0 then
      
        VERIFY_UDAS(L_message.XITEMUDADTL_TBL,
                    L_message.ExtOfXItemDesc_TBL(1).bu,
                    O_status_code,
                    O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      end if;
      if L_message.ExtOfXItemDesc_TBL(1).CustFlexAttriVo_TBL is not null and L_message.ExtOfXItemDesc_TBL(1)
         .CustFlexAttriVo_TBL.count > 0 then
      
        VERIFY_CFAS(L_message.ExtOfXItemDesc_TBL(1).CustFlexAttriVo_TBL,
                    'ITEM_MASTER',
                    L_message.ExtOfXItemDesc_TBL(1).bu,
                    O_status_code,
                    O_error_message);
      
        if O_status_code = 'E' then
          raise PROGRAM_ERROR;
        end if;
      
      end if;
    
      if L_message.ExtOfXItemDesc_TBL(1).ExtOfXItemSupDesc_TBL is not null and L_message.ExtOfXItemDesc_TBL(1)
         .ExtOfXItemSupDesc_TBL.count > 0 then
      
        for i in 1 .. L_message.ExtOfXItemDesc_TBL(1)
                      .ExtOfXItemSupDesc_TBL.count loop
        
          VERIFY_CFAS(L_message.ExtOfXItemDesc_TBL(1).ExtOfXItemSupDesc_TBL(i)
                      .CustFlexAttriVo_TBL,
                      'ITEM_SUP',
                      L_message.ExtOfXItemDesc_TBL(1).bu,
                      O_status_code,
                      O_error_message);
        
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        
        end loop;
      end if;
    
      if O_code = 0 then
        -- insert
        Select to_number(value_1)
          into L_uda_type
          from xxadeo_mom_dvm
         where func_area = 'UDA'
           and parameter = 'ITEM_TYPE';
      
        Select to_number(value_1)
          into L_uda_subtype
          from xxadeo_mom_dvm
         where func_area = 'UDA'
           and parameter = 'ITEM_SUBTYPE';
      
        Select to_number(value_1)
          into L_uda_bu
          from xxadeo_mom_dvm
         where func_area = 'UDA'
           and parameter = 'BU_SUBSCRIPTION';
      
        if L_message.XITEMUDADTL_TBL is not null and
           L_message.XITEMUDADTL_TBL.count > 0 then
          for i in 1 .. L_message.XITEMUDADTL_TBL.count loop
            if L_message.XITEMUDADTL_TBL(i).uda_id = L_uda_type then
              L_count_typologie_uda := L_count_typologie_uda + 1;
            elsif L_message.XITEMUDADTL_TBL(i).uda_id = L_uda_subtype then
              L_count_subtypologie_uda := L_count_subtypologie_uda + 1;
            elsif L_message.XITEMUDADTL_TBL(i).uda_id = L_uda_bu then
              L_count_bu_uda := L_count_bu_uda + 1;
              if L_message.XITEMUDADTL_TBL(i)
               .uda_value = L_message.ExtOfXItemDesc_TBL(1).bu then
                L_count_bu_equal := L_count_bu_equal + 1;
              end if;
            end if;
          end loop;
        
          if L_count_typologie_uda != 1 then
            O_status_code   := 'E';
            O_error_message := 'UDA "Typologie" is  required.';
            raise PROGRAM_ERROR;
          elsif L_count_subtypologie_uda != 1 then
            O_status_code   := 'E';
            O_error_message := 'UDA "Sous-typologie" is required.';
            raise PROGRAM_ERROR;
          elsif L_count_bu_uda > 1 then
            O_status_code   := 'E';
            O_error_message := 'Item should only have 1 BU subscription on creation.';
            raise PROGRAM_ERROR;
          elsif L_count_bu_uda = 0 then
            O_status_code   := 'E';
            O_error_message := 'UDA "Abonnement BU" is required!';
            raise PROGRAM_ERROR;
          elsif L_count_bu_equal = 0 then
            O_status_code   := 'E';
            O_error_message := 'The Abonnement BU should be equal to BU in extension message.';
            raise PROGRAM_ERROR;
          end if;
        
        else
          O_status_code   := 'E';
          O_error_message := 'UDAs are required.';
          raise PROGRAM_ERROR;
        end if;
      
        if L_message.tran_level = 1 and L_message.item_level = 1 then
          --sku
        
          POPULATE_SKUS(L_message, O_status_code, O_error_message);
        
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        
        elsif L_message.tran_level = 2 and L_message.item_level = 1 then
          -- style
          POPULATE_STYLE(L_message, O_status_code, O_error_message);
        
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        
        end if;
      
      elsif O_code >= 1 then
        -- update
        if O_code = 2 then
          --validate uda_value of abbonementBU is equals to extension
          Select to_number(value_1)
            into L_uda_bu
            from xxadeo_mom_dvm
           where func_area = 'UDA'
             and parameter = 'BU_SUBSCRIPTION';
        
          if L_message.XITEMUDADTL_TBL is not null and
             L_message.XITEMUDADTL_TBL.count > 0 then
            for i in 1 .. L_message.XITEMUDADTL_TBL.count loop
              if L_message.XITEMUDADTL_TBL(i).uda_id = L_uda_bu then
                if L_message.XITEMUDADTL_TBL(i)
                 .uda_value = L_message.ExtOfXItemDesc_TBL(1).bu then
                  L_count_bu_equal := L_count_bu_equal + 1;
                end if;
              end if;
            end loop;
          
            if L_count_bu_equal = 0 then
              O_status_code   := 'E';
              O_error_message := 'The Abonnement BU should be equal to BU in extension message.';
              raise PROGRAM_ERROR;
            end if;
          
          end if;
        end if;
      
        if L_message.tran_level = 1 and L_message.item_level = 1 then
        
          UPDATE_SKUS(L_message, O_status_code, O_error_message);
        
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        
          if O_code = 2 then
            -- new bu
            -- add uda
            CREATE_UDA_BU(L_message.item,
                          L_message.ExtOfXItemDesc_TBL(1).bu,
                          O_status_code,
                          O_error_message);
          
            if O_status_code = 'E' then
              raise PROGRAM_ERROR;
            end if;
          end if;
        else
        
          UPDATE_STYLE(L_message, O_status_code, O_error_message);
        
          if O_status_code = 'E' then
            raise PROGRAM_ERROR;
          end if;
        
          if O_code = 2 then
            -- new bu
            -- add uda
            CREATE_UDA_BU(L_message.item,
                          L_message.ExtOfXItemDesc_TBL(1).bu,
                          O_status_code,
                          O_error_message);
          
            if O_status_code = 'E' then
              raise PROGRAM_ERROR;
            end if;
          end if;
        
        end if;
      
      end if;
    
    else
    
      if I_message_type not in (ITEM_DEL,
                                ISUP_DEL,
                                ISC_DEL,
                                ISMC_DEL,
                                IVAT_DEL,
                                ISCD_DEL,
                                ITEMUDA_DEL,
                                IMAGE_DEL,
                                IMTL_DEL,
                                IITL_DEL) then
      
        L_message_base := treat(I_MESSAGE AS "RIB_XItemDesc_REC");
      
      else
        L_message_base := I_message;
      end if;
    
      RMSSUB_XITEM.CONSUME(O_status_code,
                           O_error_message,
                           L_message_base,
                           I_message_type);
    
      if O_status_code = 'E' then
        raise PROGRAM_ERROR;
      end if;
    end if;
    return;
  
  EXCEPTION
  
    when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
    
  END CONSUME;

  -------------------------------------------------------------------------------------------------------
  -- Name: HANDLE_ERRORS
  -- Description: This procedure is responsible to register any errors if occurred
  -------------------------------------------------------------------------------------------------------
  PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                          IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cause          IN VARCHAR2,
                          I_program        IN VARCHAR2) IS
  
    L_program VARCHAR2(50) := 'XXADEO_RMSSUB_XITEM.HANDLE_ERRORS';
  
  BEGIN
  
    API_LIBRARY.HANDLE_ERRORS(O_status_code,
                              IO_error_message,
                              I_cause,
                              I_program);
  EXCEPTION
    when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
    
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
    
  END HANDLE_ERRORS;

-------------------------------------------------------------------------------------------------
END XXADEO_RMSSUB_XITEM;
/