CREATE OR REPLACE PACKAGE BODY XXADEO_PCS_UTILS_SQL AS
  --
  LP_bu_zone_node_type XXADEO_RPM_BU_PRICE_ZONES.BU_ZONE_TYPE%TYPE := 'NATZ';
  --
-----------------------------------------------------------------------------------------------------------
-- Name: GET_PV()
-- Purpose: This function returns the retail price by item/location or item/bu.
-----------------------------------------------------------------------------------------------------------
FUNCTION get_pv(O_error_message  OUT VARCHAR2,
                O_pv             OUT ITEM_LOC.REGULAR_UNIT_RETAIL%TYPE,
                I_item           IN  ITEM_LOC.ITEM%TYPE,
                I_location       IN  XXADEO_LOCATION_PCS.STORE%TYPE default null,
                I_loc_type       IN  RPM_PRICE_CHANGE.ZONE_NODE_TYPE%TYPE default null,
                I_bu             IN  XXADEO_AREA_PCS.BU%TYPE default null)
RETURN BOOLEAN AS
  --
  L_program  VARCHAR2(64) := 'XXADEO_PCS_UTILS_SQL.GET_PV';
  --
  -- The retail price used is the Store Retail price in the standard unit 
  -- of measure for the item/location combination (PV to MSV magasin)
  --
  cursor C_get_pv_store is
    select il.regular_unit_retail
      from item_loc il
     where il.item = I_item
       and il.loc  = I_location;
  --
  -- The retail price used is the Retail price of the zone to which the 
  -- store is associeted from rpm_zone_location (PV to MSV Zone Magasin)
  --
  cursor C_get_pv_zone_by_zone is
    select rizp.standard_retail
      from rpm_item_zone_price rizp
     where rizp.item    = I_item
       and rizp.zone_id = I_location;
  --
  -- The retail price used is the Nation Zone Retail Price of the BU 
  -- (PV to MSV CA, MSV Zone CA, MSV CA by Supplier)
  --
  cursor C_get_pv_zone_by_bu is
    select rizp.standard_retail
      from rpm_item_zone_price rizp,
           xxadeo_rpm_bu_price_zones rbpz
     where rizp.item         = I_item
       and rizp.zone_id      = rbpz.zone_id
       and rbpz.bu_id        = I_bu
       and rbpz.bu_zone_type = LP_bu_zone_node_type;
  --
BEGIN
  --
  if I_loc_type = 0 then
    --
    open C_get_pv_store;
    fetch C_get_pv_store into O_pv;
    close C_get_pv_store;
    --
  elsif I_loc_type = 1 then
    --
    open C_get_pv_zone_by_zone;
    fetch C_get_pv_zone_by_zone into O_pv;
    close C_get_pv_zone_by_zone;
    --
  elsif I_bu is NOT NULL then
    --
    open C_get_pv_zone_by_bu;
    fetch C_get_pv_zone_by_bu into O_pv;
    close C_get_pv_zone_by_bu;
    --
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  WHEN OTHERS THEN
    --
    if C_get_pv_store%ISOPEN then
      --
      close C_get_pv_store;
      --
    end if;
    --
    if C_get_pv_zone_by_zone%ISOPEN then
      --
      close C_get_pv_zone_by_zone;
      --
    end if;
    --
    if C_get_pv_zone_by_bu%ISOPEN then
      --
      close C_get_pv_zone_by_bu;
      --
    end if;
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END get_pv;
-----------------------------------------------------------------------------------------------------------
-- Name: CALCULATE_MARGIN()
-- Purpose: This function is responsible per perform margin calculation from input parameters and return
--          your result in percentage and/or amount.
-----------------------------------------------------------------------------------------------------------
FUNCTION calculate_margin(O_error_message     OUT VARCHAR2,
                          O_margin_percent    OUT NUMBER,
                          O_margin_amount     OUT NUMBER,
                          I_pcs            IN     XXADEO_LOCATION_PCS.PCS%TYPE,
                          I_item           IN     RPM_PRICE_CHANGE.ITEM%TYPE,
                          I_location       IN     RPM_PRICE_CHANGE.LOCATION%TYPE default null,
                          I_loc_type       IN     RPM_PRICE_CHANGE.ZONE_NODE_TYPE%TYPE default null,
                          I_bu             IN     AREA.AREA%TYPE default null)
RETURN BOOLEAN AS
  --
  L_program  VARCHAR2(64) := 'XXADEO_PCS_UTILS_SQL.CALCULATE_MARGIN';
  --
  L_vat_rate VAT_ITEM.VAT_RATE%TYPE := NULL;
  L_pv       ITEM_LOC.REGULAR_UNIT_RETAIL%TYPE := NULL;
  --
  -- The VAT removed is the VAT of the Region of the Store
  --
  cursor C_get_vat_rate_store is
    select nvl(vvi.vat_rate, vvcr.vat_rate)
      from v_store          vs,
           vat_item         vvi,
           vat_code_rates   vvcr
     where vs.store          = I_location
       and vvi.item          = I_item
       and vs.vat_region     = vvi.vat_region
       and vvcr.vat_code     = vvi.vat_code;
  --
  -- The VAT removed is the VAT of the VAT Region of the current Store because all stores 
  -- have the same VAT within the same Zone.
  --
  cursor C_get_vat_rate_zone is
    select NVL(vvi.vat_rate, vvcr.vat_rate)
      from rpm_zone_location rzl,
           v_store           vs,
           vat_item          vvi,
           vat_code_rates    vvcr
     where rzl.zone_id       = I_location
       and rzl.location      = vs.store
       and vs.vat_region     = vvi.vat_region
       and vvi.item          = I_item
       and vvcr.vat_code     = vvi.vat_code;
  --
  -- The VAT removed is the MAX(VAT) of all VAT regions of the BU
  --
  cursor C_get_vat_rate_area is
    select max(nvl(vvi.vat_rate, vvcr.vat_rate))
      from v_store          vs,
           vat_item         vvi,
           vat_code_rates   vvcr
     where vs.area          = I_bu
       and vvi.item         = I_item
       and vvi.vat_region   = vs.vat_region
       and vvi.vat_code     = vvcr.vat_code;
  --
  -- The VAT removed is the MAX (VAT) of all VAT Regions of the BU of the Nation Zone.
  --
  cursor C_get_vat_rate_zone_bu is
    with zone_bu as (select rbpz.zone_id
                       from xxadeo_rpm_bu_price_zones rbpz
                      where rbpz.bu_zone_type <> LP_bu_zone_node_type
                        and rbpz.bu_id         = I_bu)
    select max(NVL(vvi.vat_rate, vvcr.vat_rate))
      from zone_bu           zb,
           rpm_zone_location rzl,
           v_store           vs,
           vat_item          vvi,
           vat_code_rates    vvcr
     where rzl.zone_id       = zb.zone_id
       and rzl.location      = vs.store
       and vs.vat_region     = vvi.vat_region
       and vvi.item          = I_item
       and vvcr.vat_code     = vvi.vat_code;
  --
BEGIN
  --
  -- if PCS is 0 then the result of both margins is 0
  --
  if I_pcs = 0 then
    O_margin_percent := 0;
    O_margin_amount  := 0;
    RETURN TRUE;
  end if;
  --
  -- if loc_type is 0 - margin calculation is at the store level (MSV Magasin)
  -- elsif loc_type is 1 and BU is NULL - margin calculation is at the zone level for a store (MSV Zone Magasin)
  -- elsif loc_type is 1 and BU is NOT NULL - margin calculation is at the zone level for a BU (MSV Zone CA)
  -- elsif BU is NOT NULL - margin calculation is at the BU level
  --
  if I_loc_type = 0 then
    --
    if get_pv(O_error_message  => O_error_message,
              O_pv             => L_pv,
              I_item           => I_item,
              I_location       => I_location,
              I_loc_type       => I_loc_type) = FALSE then
      --
      RETURN FALSE;
      --
    end if;
    --
    open C_get_vat_rate_store;
    fetch C_get_vat_rate_store into L_vat_rate;
    close C_get_vat_rate_store;
    --
  elsif I_loc_type = 1 and I_bu is NULL then
    --
    if get_pv(O_error_message  => O_error_message,
              O_pv             => L_pv,
              I_item           => I_item,
              I_location       => I_location,
              I_loc_type       => I_loc_type) = FALSE then
      --
      RETURN FALSE;
      --
    end if;
    --
    open C_get_vat_rate_zone;
    fetch C_get_vat_rate_zone into L_vat_rate;
    close C_get_vat_rate_zone;
    --
  elsif I_loc_type = 1 and I_bu is NOT NULL then
    --
    if get_pv(O_error_message  => O_error_message,
              O_pv             => L_pv,
              I_item           => I_item,
              I_location       => I_location,
              I_loc_type       => I_loc_type) = FALSE then
      --
      RETURN FALSE;
      --
    end if;
    --
    open C_get_vat_rate_zone;
    fetch C_get_vat_rate_zone into L_vat_rate;
      --
      if C_get_vat_rate_zone%NOTFOUND then
        --
        open C_get_vat_rate_zone_bu;
        fetch C_get_vat_rate_zone_bu into L_vat_rate;
        close C_get_vat_rate_zone_bu;
        --
      end if;
      --
    close C_get_vat_rate_zone;
    --
  elsif I_bu is NOT NULL then
    --
    if get_pv(O_error_message  => O_error_message,
              O_pv             => L_pv,
              I_item           => I_item,
              I_bu             => I_bu) = FALSE then
      --
      RETURN FALSE;
      --
    end if;
    --
    open C_get_vat_rate_area;
    fetch C_get_vat_rate_area into L_vat_rate;
    close C_get_vat_rate_area;
    --
  end if;
  --
  if L_vat_rate is NULL then
    --
    O_error_message := 'Cannot find VAT rate.';
    RETURN FALSE;
    --
  end if;
  --
  if L_pv  is NULL then
    --
    O_error_message := 'Cannot find PV.';
    RETURN FALSE;
    --
  end if;
  --
  -- Return percentage value
  --
  O_margin_percent := ROUND(((L_pv/((L_vat_rate / 100) + 1)) - I_pcs)/ (L_pv/((L_vat_rate / 100) + 1)),4);
  --
  -- Return amount value
  --
  O_margin_amount  := ROUND(((L_pv/((L_vat_rate / 100) + 1)) - I_pcs),2);
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  WHEN OTHERS THEN
    --
    if C_get_vat_rate_store%ISOPEN then
      --
      close C_get_vat_rate_store;
      --
    end if;
    --
    if C_get_vat_rate_zone%ISOPEN then
      --
      close C_get_vat_rate_zone;
      --
    end if;
    --
    if C_get_vat_rate_area%ISOPEN then
      --
      close C_get_vat_rate_area;
      --
    end if;
    --
    if C_get_vat_rate_zone_bu%ISOPEN then
      --
      close C_get_vat_rate_zone_bu;
      --
    end if;
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END calculate_margin;
-----------------------------------------------------------------------------------------------------------
-- Name: RETURN_MARGIN()
-- Purpose: This function is responsible for returning the margin calculated from input parameters.
-----------------------------------------------------------------------------------------------------------
FUNCTION return_margin(I_pcs            IN     XXADEO_LOCATION_PCS.PCS%TYPE,
                       I_item           IN     RPM_PRICE_CHANGE.ITEM%TYPE,
                       I_location       IN     RPM_PRICE_CHANGE.LOCATION%TYPE default null,
                       I_loc_type       IN     RPM_PRICE_CHANGE.ZONE_NODE_TYPE%TYPE default null,
                       I_bu             IN     AREA.AREA%TYPE default null,
                       I_margin_type    IN     VARCHAR2 default null)
RETURN NUMBER AS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PCS_UTILS_SQL.RETURN_MARGIN';
  --
  L_error_message  VARCHAR2(225);
  L_margin_percent NUMBER(20,4);
  L_margin_amount  NUMBER(20,4);
  --
BEGIN
  --
  IF calculate_margin(O_error_message  => L_error_message,
                      O_margin_percent => L_margin_percent,
                      O_margin_amount  => L_margin_amount,
                      I_pcs            => I_pcs,
                      I_item           => I_item,
                      I_location       => I_location,
                      I_loc_type       => I_loc_type,
                      I_bu             => I_bu) = FALSE THEN
    --
    RETURN 0;
    --
  END IF;
  --
  -- if I_margin_type input parameter is '%' should be returned the margin value
  -- in percentage else margin value should bu returned in amount 
  --
  if I_margin_type = '%' then
    --
    RETURN L_margin_percent;
    --
  else
    --
    RETURN L_margin_amount;
    --
  end if;
  --
EXCEPTION
  --
  WHEN OTHERS THEN
    --
    L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN 0;
    --
END return_margin;
-----------------------------------------------------------------------------------------------------------
END XXADEO_PCS_UTILS_SQL;
/
