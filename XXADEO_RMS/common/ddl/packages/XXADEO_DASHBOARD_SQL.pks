create or replace package XXADEO_DASHBOARD_SQL is

/********************************************************************************/
/* CREATE DATE - September 2018                                                 */
/* CREATE USER - Felipe Villa                                                   */
/* PROJECT     - ADEO                                                           */
/* DESCRIPTION - Package "XXADEO_DASHBOARD_SQL" for Dashboard                   */
/********************************************************************************/

----------------------------------------------------------------------------------
  FUNCTION PC_VARIATION_N4W(I_bu                   IN  XXADEO_MV_DASHBOARD_PRC_CHG_VR.BU%TYPE DEFAULT NULL,
                            I_dept                 IN  XXADEO_MV_DASHBOARD_PRC_CHG_VR.DEPT_ID%TYPE DEFAULT NULL,
                            I_subdept_list         IN  VARCHAR2 DEFAULT NULL,
                            I_type_list            IN  VARCHAR2 DEFAULT NULL,
                            I_subtype_list         IN  VARCHAR2 DEFAULT NULL,
                            I_reason               IN  XXADEO_MV_DASHBOARD_PRC_CHG_VR.REASON_CODE%TYPE DEFAULT NULL,
                            I_effective_date_from  IN  XXADEO_MV_DASHBOARD_PRC_CHG_VR.EFFECTIVE_DATE%TYPE DEFAULT NULL,
                            I_effective_date_to    IN  XXADEO_MV_DASHBOARD_PRC_CHG_VR.EFFECTIVE_DATE%TYPE DEFAULT NULL)
  RETURN XXADEO_PCVARIATION_N4W_TBL
  PIPELINED;

----------------------------------------------------------------------------------
  FUNCTION PC_AMOUNT_N4W(I_bu                   IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.BU%TYPE DEFAULT NULL,
                         I_dept                 IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.DEPT_ID%TYPE DEFAULT NULL,
                         I_subdept_list         IN  VARCHAR2 DEFAULT NULL,
                         I_type_list            IN  VARCHAR2 DEFAULT NULL,
                         I_subtype_list         IN  VARCHAR2 DEFAULT NULL,
                         I_reason               IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.REASON_CODE%TYPE DEFAULT NULL,
                         I_effective_date_from  IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.EFFECTIVE_DATE%TYPE DEFAULT NULL,
                         I_effective_date_to    IN  XXADEO_MV_DASHBOARD_PRC_CHG_NR.EFFECTIVE_DATE%TYPE DEFAULT NULL)
  RETURN XXADEO_PCAMOUNT_N4W_TBL
  PIPELINED;

----------------------------------------------------------------------------------
end XXADEO_DASHBOARD_SQL;
/
