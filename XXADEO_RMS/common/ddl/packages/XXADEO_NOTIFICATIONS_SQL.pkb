CREATE OR REPLACE PACKAGE BODY XXADEO_NOTIFICATIONS_SQL AS
--------------------------------------------------------------------------------
FUNCTION CREATE_RAF_NOTIFICATION(O_error_message        OUT VARCHAR2,
                                 I_application_code     IN  RAF_NOTIFICATION.APPLICATION_CODE%TYPE,
                                 I_notification_type    IN  RAF_NOTIFICATION_TYPE_B.NOTIFICATION_TYPE_CODE%TYPE,
                                 I_notification_desc    IN  RAF_NOTIFICATION.NOTIFICATION_DESC%TYPE,
                                 I_notification_context IN  VARCHAR2,
                                 I_user                 IN  RAF_NOTIFICATION_RECIPIENTS.RECIPIENT_ID%TYPE DEFAULT GET_USER,
                                 I_errors_count         IN  NUMBER)
RETURN BOOLEAN IS
  --
  L_program           VARCHAR2(64) := 'XXADEO_NOTIFICATIONS_SQL.CREATE_RAF_NOTIFICATION';
  L_notification_id   RAF_NOTIFICATION.RAF_NOTIFICATION_ID%TYPE;
  L_user              VARCHAR2(64) := I_USER;
  L_notification_type RAF_NOTIFICATION.NOTIFICATION_TYPE%TYPE;
  L_schema_name       SEC_USER.DATABASE_USER_ID%TYPE;
  vSQL                VARCHAR2(255) := null;
  vSQL2               VARCHAR2(255) := null;
  --
BEGIN
  --
  -- If application code is Store Portal, notification is sent to Store Portal Schema
  --
  if I_application_code = 'StorePortalApp' then
    --
    L_schema_name := 'XXADEO_SP';
    --
  else
    --
    select sys_context('userenv','current_schema')
      into L_schema_name
      from dual;
    --
  end if;
  --
  vSQL := 'select rntb.notification_type
             from '||L_schema_name||'.raf_notification_type_b rntb
            where upper(rntb.notification_type_code) = upper('''||I_notification_type||''')';
  --
  execute immediate vSQL
     into L_notification_type;
  --
  vSQL2 := 'select '||L_schema_name||'.raf_notification_seq.nextval
              from dual';
  --
  execute immediate vSQL2
     into L_notification_id;
  --
  execute immediate 'insert into '||L_schema_name||'.raf_notification
                       (raf_notification_id,
                        application_code,
                        notification_type,
                        notification_desc,
                        notification_active,
                        created_by,
                        create_date,
                        last_updated_by,
                        last_update_date,
                        object_version_number,
                        severity,
                        launchable)
                     values
                       ('''||L_notification_id||''',
                        '''||I_application_code||''',
                        '''||L_notification_type||''',
                        substr('''||I_notification_desc||''', 1, 200),
                        ''U'',
                        '''||L_user||''',
                        sysdate,
                        '''||L_user||''',
                        sysdate,
                        ''1'',
                        ''3'',
                        case
                          when '''||I_errors_count||''' = 0 then
                            ''N''
                          else
                            ''Y''
                        end)';
  --
  if I_notification_context is not null then
    --
    execute immediate 'insert into '||L_schema_name||'.raf_notification_context
                         (raf_notification_id,
                          notification_context,
                          source)
                       values
                         ('''||L_notification_id||''',
                          '''||I_notification_context||''',
                          ''TEXT'')';
    --
  end if;
  --
  execute immediate 'insert into '||L_schema_name||'.raf_notification_recipients
                       (raf_notification_id,
                        recipient_id)
                     values
                       ('''||L_notification_id||''',
                        '''||L_user||''')';
  --
  execute immediate 'insert into '||L_schema_name||'.raf_notification_status
                       (raf_notification_id,
                        status,
                        user_id,
                        created_by,
                        create_date,
                        last_updated_by,
                        last_update_date,
                        object_version_number)
                     values
                       ('''||L_notification_id||''',
                        ''U'',
                        '''||L_user||''',
                        '''||L_user||''',
                        sysdate,
                        '''||L_user||''',
                        sysdate,
                        ''1'')';
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END CREATE_RAF_NOTIFICATION;
--------------------------------------------------------------------------------
END XXADEO_NOTIFICATIONS_SQL;
/
