CREATE OR REPLACE PACKAGE BODY XXADEO_CUSTOM_UPLOAD_SQL AS
----------------------------------------------------------------------------------------------
FUNCTION GET_TEMPLATE_S9T_SHEETS(O_error_message  IN OUT        LOGGER_LOGS.TEXT%TYPE,
                                 IO_s9t_sheet_tab IN OUT NOCOPY S9T_SHEET_TAB,
                                 I_template_key   IN            S9T_TEMPLATE.TEMPLATE_KEY%TYPE)
RETURN BOOLEAN IS
  --
  L_program      VARCHAR2(64):='XXADEO_CUSTOM_UPLOAD_SQL.GET_TEMPLATE_S9T_SHEETS';
  L_s9t_cells    S9T_CELLS;
  NO_CONFIG      EXCEPTION;
  --
  cursor C_get_template_sheets is
    select wksht_key sheet_name
      from s9t_tmpl_wksht_def
     where template_key = I_template_key
     order by seq_no asc;
  --
  cursor C_get_template_sheets_cols(I_sheet_name IN S9T_TMPL_COLS_DEF.WKSHT_KEY%TYPE) is
    select column_key
      from xxadeo_s9t_tmpl_cols_def
     where template_key = I_template_key
       and wksht_key    = I_sheet_name
    order by column_index asc;
  --
BEGIN
  --
  for trec in C_get_template_sheets loop
    --
    L_s9t_cells := S9T_CELLS();
    --
    for crec in C_get_template_sheets_cols(I_sheet_name => trec.sheet_name) loop
      --
      L_s9t_cells.extend();
      L_s9t_cells(L_s9t_cells.count) := crec.column_key;
      --
    end loop;
    --
    if L_s9t_cells.count > 0  then
      --
      IO_s9t_sheet_tab.extend();
      IO_s9t_sheet_tab(IO_s9t_sheet_tab.count) := S9T_SHEET(trec.sheet_name,
                                                            L_s9t_cells,
                                                            null,
                                                            null);
      --
    end if;
    --
  end loop;
  --
  if IO_s9t_sheet_tab.count = 0 then
    --
    RAISE NO_CONFIG;
    --
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when NO_CONFIG then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          'Template columns not configured on the table XXADEO_S9T_TMPL_COLS_DEF',
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END GET_TEMPLATE_S9T_SHEETS;
----------------------------------------------------------------------------------------------
FUNCTION GET_TEMPLATE_COLUMN_INDEXES(O_error_message                IN OUT        LOGGER_LOGS.TEXT%TYPE,
                                     IO_xxadeo_wksht_column_idx_tbl IN OUT NOCOPY xxadeo_WKSHT_COLUMN_IDX_TBL,
                                     I_template_key                 IN            S9T_TEMPLATE.TEMPLATE_KEY%TYPE)
RETURN BOOLEAN IS
  --
  L_program    VARCHAR2(64):='XXADEO_CUSTOM_UPLOAD_SQL.GET_TEMPLATE_COLUMN_INDEXES';
  NO_CONFIG    EXCEPTION;
  --
  cursor C_get_template_sheets_cols is
    select new XXADEO_WKSHT_COLUMN_IDX_OBJ(template_key,
                                           wksht_key,
                                           column_key,
                                           column_index)
      from xxadeo_s9t_tmpl_cols_def s
     where template_key = I_template_key
     order by column_index asc;
  --
BEGIN
  --
  open C_get_template_sheets_cols;
  fetch C_get_template_sheets_cols bulk collect into IO_xxadeo_wksht_column_idx_tbl;
  close C_get_template_sheets_cols;
  --
  if IO_xxadeo_wksht_column_idx_tbl.count = 0 then
    --
    RAISE NO_CONFIG;
    --
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when NO_CONFIG then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          'Worksheet not configured on the table XXADEO_S9T_TMPL_COLS_DEF',
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
    --
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END GET_TEMPLATE_COLUMN_INDEXES;
----------------------------------------------------------------------------------------------
FUNCTION GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl IN XXADEO_WKSHT_COLUMN_IDX_TBL,
                     I_wksht_key                   IN XXADEO_S9T_TMPL_COLS_DEF.WKSHT_KEY%TYPE,
                     I_column_key                  IN XXADEO_S9T_TMPL_COLS_DEF.COLUMN_KEY%TYPE)
RETURN NUMBER IS
  --
  L_program       VARCHAR2(64):='XXADEO_CUSTOM_UPLOAD_SQL.GET_COL_IDX';
  L_error_message LOGGER_LOGS.TEXT%TYPE;
  L_column_index  XXADEO_S9T_TMPL_COLS_DEF.COLUMN_KEY%TYPE;
  --
BEGIN
  --
  select column_index
    into L_column_index
    from table(I_xxadeo_wksht_column_idx_tbl)
   where wksht_key  = I_wksht_key
     and column_key = I_column_key;
  --
  RETURN L_column_index;
  --
EXCEPTION
  --
  when OTHERS then
    --
    L_error_message := SQL_LIB.CREATE_MSG(I_key   => 'PACKAGE_ERROR',
                                          I_txt_1 => 'ERROR_GET_COL_IDX',
                                          I_txt_2 => L_program,
                                          I_txt_3 => TO_CHAR(SQLCODE));
    --
    RETURN 0;
    --
  --
END GET_COL_IDX;
----------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                   O_file_id        IN OUT NUMBER,
                   I_template_key   IN     S9T_TEMPLATE.TEMPLATE_KEY%TYPE,
                   I_s9t_sheet_tab  IN     S9T_SHEET_TAB) IS
  --
  L_program   VARCHAR2(255):='XXADEO_CUSTOM_UPLOAD_SQL.INIT_S9T';
  --
  L_file      S9T_FILE;
  L_file_name S9T_FOLDER.FILE_NAME%TYPE;
  --
BEGIN
  --
  L_file              := NEW s9t_file();
  O_file_id           := s9t_folder_seq.nextval;
  L_file.file_id      := O_file_id;
  L_file_name         := I_template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
  L_file.file_name    := L_file_name;
  L_file.template_key := I_template_key;
  L_file.user_lang    := GET_USER_LANG;

  for i in 1..I_s9t_sheet_tab.count loop
    --
    L_file.add_sheet(I_s9t_sheet_tab(i).sheet_name);
    L_file.sheets(L_file.get_sheet_index(I_s9t_sheet_tab(i).sheet_name)).column_headers := I_s9t_sheet_tab(i).column_headers;
    --
  end loop;
  --
  S9T_PKG.SAVE_OBJ(L_file);
  --
END INIT_S9T;
----------------------------------------------------------------------------------------------
END XXADEO_CUSTOM_UPLOAD_SQL;
/
