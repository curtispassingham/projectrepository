CREATE OR REPLACE PACKAGE BODY XXADEO_ITEM_LFCY_VAL_IB_SQL AS

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-09-04 - Jorge Agra - BUG#365 IB-DISC_01 Compare with vdate+1,         */
/*                           not vdate. Next val date must be set to:         */
/*                           end date - 1                                     */
/* 2018-09-06 - Jorge Agra - Bew rule IB-ASI-05 (ref complete BASA)           */
/* 2018-09-20 - Jorge Agra - BUG#443 IB-ACOM_02 Only check sales start date   */
/*                                              not its value                 */
/* 2018-09-21 - Jorge Agra - BUG#450 IB-ACOM_031                              */
/* 2018-10-02 - Jorge Agra - BUG#520 IB-ASI-03 consider ASI and ACOM for      */
/*                           components                                       */
/* 2018-10-02 - Jorge Agra - include validation of next_val_dt in all rules   */
/* 2018-10-19 - Jorge Agra - BUG#555 change to disc from init, asi and acom   */
/*                           BUG#??? execution of rules with same seqence     */
/* 2018-11-06 - Jorge Agra - IB_DISC_02 should also check STOP DATE           */
/* 2018-11-19 - Jorge Agra - IB_DISC_02 group by to avoid merge unstable      */
/*                           results on duplicate rows                        */
/* 2018-11-26 - Jorge Agra - ORACR_00257 Use CFA ECOTAX_ELC_COMP instead of   */
/*                           EXP_CATEGORY to identify ecotaxes in IB-ASI-02   */
/******************************************************************************/

--------------------------------------------------------------------------------
-- Constants
--------------------------------------------------------------------------------

CONST$PackageName             CONSTANT VARCHAR2(30) := 'XXADEO_ITEM_LFCY_VAL_IB_SQL';

--------------------------------------------------------------------------------
-- Types
--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
-- Globals
--------------------------------------------------------------------------------

-- today for RMS
G_rms_now date := get_vdate;
GV_user_id                          VARCHAR2(30)  := NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'),  USER);
--GV_user_id                          VARCHAR2(30)  := 'RB107';

GV_fake_exec                        boolean := false;

--------------------------------------------------------------------------------
-- Retail price should be different from 0 (Retail price is different from 0
-- for the National Retail Zone of the BU)
--------------------------------------------------------------------------------
FUNCTION IB_ASI_01( O_error_message   IN OUT   varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IB_ASI_01';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_ASI_01');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    xxadeo_item_life_cycle_sql.DBG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
      select  vw.item
              ,vw.lfcy_entity
              ,vw.lfcy_entity_type
              ,vw.lfcy_cond_id            rule_id
              ,pzbu.zone_id
              ,zp.standard_retail
        from xxadeo_v_lfcy_work           vw
             ,xxadeo_rpm_bu_price_zones   pzbu
             ,rpm_item_zone_price         zp
      where vw.process_id = I_pid
        and vw.lfcy_cond_id = 'IB-ASI-01'
        and vw.lfcy_entity_type = 'IB'
        --and vw.lfcy_status_next = 'IB-ASI'
        and nvl(vw.val_status,'X') <> 'Y'
        and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
        and pzbu.bu_id = vw.lfcy_entity
        and pzbu.bu_zone_type = 'NATZ'
        and zp.item = vw.item
        and zp.zone_id = pzbu.zone_id
        and zp.standard_retail > 0
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'Y',
            last_val_dt = sysdate,
            error_message = null,
            last_update_datetime = sysdate;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                        L_program,
                                            TO_CHAR(SQLCODE));
    ---
  return FALSE;
END IB_ASI_01;
--------------------------------------------------------------------------------
-- IB-ASI-02
-- If ?Ecopart? is applicable to the item (e.g. D3E), there is at least one
-- cost component associated with the item corresponding to the Ecopart programm
-- (D3E cost line).
-- Ensure that for a D3E eligible product (ecopart is applicable), there is the
-- corresponding D3E cost line (ecopart cost component) provided.
--------------------------------------------------------------------------------
FUNCTION IB_ASI_02( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program       VARCHAR2(100) := CONST$PackageName || '.IB_ASI_02';
  ---
  L_cfa_ecop_bu               xxadeo_mom_dvm.bu%type;
  L_cfa_ecop_group_id         cfa_attrib.group_id%type;
  L_cfa_ecop_attrib_id        cfa_attrib.attrib_id%type;
  L_cfa_ecop_storage_col_name cfa_attrib.storage_col_name%type;
  ---
  L_sql  varchar2(4000) := null;
  ---
  cursor C_ecop_cfas is
    select xmd.parameter
           ,xmd.bu
           ,cfaa.attrib_id
           ,cfaa.group_id
           ,cfaa.storage_col_name
      from xxadeo_mom_dvm xmd,
           cfa_attrib     cfaa
     where to_char(cfaa.attrib_id) = xmd.value_1
       and xmd.func_area = 'CFA_ECOP';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_ASI_02');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Start. Set val_status=T');
  ---
  -- Set val_status=T of all non valid conditions
  ---
  update xxadeo_lfcy_item_cond
     set val_status = 'T'
         ,error_message = null
         ,last_update_datetime = sysdate
   where process_id = I_pid
     and lfcy_cond_id = 'IB-ASI-02'
     and lfcy_entity_type = 'IB'
     and nvl(val_status,'X') <> 'Y'
     and nvl(next_val_dt,get_vdate-1) <= get_vdate
  ;
  ---
  -- loop all ecop cfas and check (for all items/bus beeing checked => IS-ASI-02, ...):
  --   1) has this ecop cfa = Y
  --   2) does not have valid elc_comp
  --  => then set val_status 'N'
  ---
  for rec_ecop_cfa in c_ecop_cfas loop
    ---
    xxadeo_item_life_cycle_sql.DBG(L_program, 'CFA LOOP => CFA=' || rec_ecop_cfa.parameter);
    ---
    L_cfa_ecop_bu := rec_ecop_cfa.bu;
    L_cfa_ecop_group_id := rec_ecop_cfa.group_id;
    L_cfa_ecop_attrib_id := rec_ecop_cfa.attrib_id;
    L_cfa_ecop_storage_col_name := rec_ecop_cfa.storage_col_name;
    ---
    L_sql := '
      merge into xxadeo_lfcy_item_cond t0
        using (
          select  vw.item
                  ,vw.lfcy_entity
                  ,vw.lfcy_entity_type
                  ,vw.lfcy_cond_id rule_id
            from xxadeo_v_lfcy_work vw
          where vw.process_id = ' || to_char(I_pid) || '
            and vw.lfcy_cond_id = ''IB-ASI-02''
            and vw.lfcy_entity_type = ''IB''
            -- checking only specific rows
            and nvl(vw.val_status,''X'') = ''T''
            and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
            and lfcy_entity = to_char(' || to_char(L_cfa_ecop_bu) || ')
            --- check eco CFA
            and exists (
              select 1
                from item_master_cfa_ext imce
               where imce.item = vw.item
                 and imce.group_id = ' || to_char(L_cfa_ecop_group_id) || '
                 and imce.' || L_cfa_ecop_storage_col_name || '
                  = ''Y''
            )
            --- check elc_comp CFA
            and not exists (
              select 1
                from item_exp_head      ieh
                     ,item_exp_detail   ied
                     ,elc_comp          ec
                     ,partner_org_unit  pou
                     ,xxadeo_bu_ou      xbo
                     ,cfa_attrib        cfaa
                     ,elc_comp_cfa_ext  ecce
               where 1=1
                 ---
                 and ieh.item          = vw.item
                 and ied.item          = ieh.item
                 and ied.supplier      = ieh.supplier
                 and ied.item_exp_type = ieh.item_exp_type
                 and ied.item_exp_seq  = ieh.item_exp_seq
                 ---
                 and pou.partner       = ied.supplier
                 ---
                 and xbo.ou            = pou.org_unit_id
                 and xbo.bu            = vw.lfcy_entity
                 ---
                 and ec.comp_id        = ied.comp_id
                 ---
                 and ecce.comp_id      = ec.comp_id
                 and ecce.group_id     = ' || to_char(xxadeo_item_life_cycle_sql.GV_CFAGRP_EcotaxElcComp) || '
                 and nvl(ecce.' || xxadeo_item_life_cycle_sql.GV_CFACOL_EcotaxElcComp || ',0)
                  = ' || to_char(L_cfa_ecop_attrib_id) || '
            )
          ) src
      on (
        t0.item                 = src.item
        and t0.lfcy_entity      = src.lfcy_entity
        and t0.lfcy_entity_type = src.lfcy_entity_type
        and t0.lfcy_cond_id     = src.rule_id
        )
      when matched then
        update
           set t0.val_status              = ''N''
               ,t0.last_val_dt            = sysdate
               ,t0.error_message          = null
               ,t0.last_update_datetime   = sysdate
       '
    ;
    execute immediate L_sql;
  end loop;
  ---
  -- all rows beeing processed that did not change to N are valid
  ---
  update xxadeo_lfcy_item_cond
     set val_status = 'Y'
         ,last_val_dt = sysdate
         ,error_message = null
         ,last_update_datetime = sysdate
   where process_id = I_pid
     and lfcy_cond_id = 'IB-ASI-02'
     and val_status = 'T'
  ;
  ---
  O_rule_changes := sql%rowcount;
  ---
  return true;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IB_ASI_02;
--------------------------------------------------------------------------------
-- All components of the pack are in Active for Internal Service status.
--------------------------------------------------------------------------------
FUNCTION IB_ASI_03( O_error_message IN OUT  varchar2,
                    O_rule_changes     OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IB_ASI_03';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_ASI_03');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Start');
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
      select  vw.item
              ,vw.lfcy_entity
              ,vw.lfcy_entity_type
              ,vw.lfcy_cond_id            rule_id
        from xxadeo_v_lfcy_work           vw
      where vw.process_id = I_pid
        and vw.lfcy_cond_id = 'IB-ASI-03'
        and vw.lfcy_entity_type = 'IB'
        --and vw.lfcy_status_next = 'IB-ASI'
        and nvl(vw.val_status,'X') <> 'Y'
        and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
        and not exists (
          select 1
            from item_master        im,
                 packitem           pi,
                 uda_item_lov       uil,
                 xxadeo_mom_dvm     xmd
           where pi.pack_no = vw.item
             and im.item = pi.item
             and uil.item = im.item
             and xmd.func_area = 'UDA'
             and xmd.parameter = 'LIFECYCLE_STATUS'
             and xmd.bu = vw.lfcy_entity
             and uil.uda_id = xmd.value_1
             and nvl(uil.uda_value,0) not in (
              xxadeo_item_life_cycle_sql.GV_UDAVAL_LFCY_status$asi
              ,xxadeo_item_life_cycle_sql.GV_UDAVAL_LFCY_status$acom
              )
          )
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'Y',
            error_message = null,
            last_val_dt = sysdate,
            last_update_datetime = sysdate;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IB_ASI_03;
--------------------------------------------------------------------------------
-- IB-ASI-04 - For orderable items, at least one active supplier must exist.
--------------------------------------------------------------------------------
FUNCTION IB_ASI_04( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IB_ASI_04';
  ---
  L_sql       varchar2(5000) := '
      merge into xxadeo_lfcy_item_cond t0
        using (
          select  vw.item
                  ,vw.lfcy_entity
                  ,vw.lfcy_entity_type
                  ,vw.lfcy_cond_id      rule_id
            from xxadeo_v_lfcy_work     vw
          where vw.process_id = :1
            and vw.lfcy_cond_id = ''IB-ASI-04''
            and vw.lfcy_entity_type = ''IB''
            --and vw.lfcy_status_next = ''IB-ASI''
            and nvl(vw.val_status,''X'') <> ''Y''
            and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
            and exists (
              select 1
                from item_supplier          isup
                     ,xxadeo_mom_dvm        xmd
                     ,cfa_attrib            cfaa
                     ,item_supplier_cfa_ext isce
                     ,partner_org_unit      pou
                     ,xxadeo_bu_ou          xbo
               where 1=1
                 and isup.item = vw.item
                 and xmd.parameter = ''LINK_STATUS_ITEM_SUPP''
                 and xmd.func_area = ''CFA''
                 and nvl(xmd.bu,-1) = -1
                 and cfaa.attrib_id = xmd.value_1
                 and isce.item = isup.item
                 and isce.supplier = isup.supplier
                 and isce.group_id = cfaa.group_id
                 and isce.' || xxadeo_item_life_cycle_sql.GV_CFACOL_LnkStaItSup
                  || ' = ''' || xxadeo_item_life_cycle_sql.GV_CFAVAL_LnkStaItSup$Act || '''
                 and pou.partner = isup.supplier
                 and xbo.ou = pou.org_unit_id
                 and xbo.bu = vw.lfcy_entity
              )
            ) src
        on (
          t0.item                 = src.item
          and t0.lfcy_entity      = src.lfcy_entity
          and t0.lfcy_entity_type = src.lfcy_entity_type
          and t0.lfcy_cond_id     = src.rule_id
          )
        when matched then
          update
             set t0.val_status              = ''Y''
                 ,t0.last_val_dt            = sysdate
                 ,t0.error_message          = null
                 ,t0.last_update_datetime   = sysdate
      ';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_ASI_01');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  execute immediate L_sql
    using I_pid;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  O_result := null;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IB_ASI_04;
--------------------------------------------------------------------------------
-- IB-ASI-05 - BASA Complete referencing
--------------------------------------------------------------------------------
FUNCTION IB_ASI_05( O_error_message   IN OUT   varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_rule      VARCHAR2(30) := 'IB-ASI-05';
  L_program   VARCHAR2(100) := CONST$PackageName || '.IB_ASI_05';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_ASI_01');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
        select  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id            rule_id
          from xxadeo_v_lfcy_work           vw
               ,uda_item_lov                uil
               ,xxadeo_mom_dvm              xmd
          where vw.process_id = I_pid
            and vw.lfcy_cond_id = 'IB-ASI-05'
            and vw.lfcy_entity_type = 'IB'
            --and vw.lfcy_status_next = 'IB-ASI'
            and nvl(vw.val_status,'X') <> 'Y'
            and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
            and uil.item = vw.item
            and xmd.func_area = 'UDA'
            and xmd.parameter = xxadeo_item_life_cycle_sql.CONST$UDAKEY_BASACompleteRef
            and xmd.bu = vw.lfcy_entity
            and uil.uda_id = xmd.value_1
            and nvl(uil.uda_value,xxadeo_item_life_cycle_sql.GV_CFAVAL_BasaCompleteRefNo) = xxadeo_item_life_cycle_sql.GV_CFAVAL_BasaCompleteRefYes
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'Y',
            error_message = null,
            last_val_dt = sysdate,
            last_update_datetime = sysdate;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IB_ASI_05;
--------------------------------------------------------------------------------
-- IB-ACOM-01
-- Packs
-- All components of the pack must be in the Active for Commerce status
--------------------------------------------------------------------------------
FUNCTION IB_ACOM_01( O_error_message IN OUT  varchar2,
                     O_rule_changes     OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IB_ACOM_01';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_ACOM_01');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  merge /*+ MONITOR */ into xxadeo_lfcy_item_cond t0
    using (
        select  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id rule_id
          from xxadeo_v_lfcy_work vw
          where vw.process_id = I_pid
            and vw.lfcy_cond_id = 'IB-ACOM-01'
            and vw.lfcy_entity_type = 'IB'
            --and vw.lfcy_status_next = 'IB-ACOM'
            and nvl(vw.val_status,'X') <> 'Y'
            and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
            and not exists (select 1
                      from item_master        im,
                           packitem           pi,
                           uda_item_lov       uil,
                           xxadeo_mom_dvm     xmd
                     where pi.pack_no = vw.item
                       and im.item = pi.item
                       and uil.item = im.item
                       and xmd.func_area = 'UDA'
                       and xmd.parameter = 'LIFECYCLE_STATUS'
                       and xmd.bu = vw.lfcy_entity
                       and uil.uda_id = xmd.value_1
                       and nvl(uil.uda_value,0) <> xxadeo_item_life_cycle_sql.GV_UDAVAL_LFCY_status$acom
                    )
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'Y',
            error_message = null,
            last_val_dt = sysdate,
            last_update_datetime = sysdate;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IB_ACOM_01;
--------------------------------------------------------------------------------
-- IB-ACOM-02: Sales Start Date must be populated
--------------------------------------------------------------------------------
FUNCTION IB_ACOM_02( O_error_message   IN OUT   varchar2,
                     O_rule_changes     OUT      number,
                     O_result           OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid              IN       number,
                     I_param1           IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2           IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  --
  L_program             VARCHAR2(100) := CONST$PackageName || '.IB_ACOM_02';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_ACOM_02');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
        select  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id rule_id
          from xxadeo_v_lfcy_work           vw
               ,uda_item_date               uidt
               ,xxadeo_mom_dvm              xmd
          where vw.process_id = I_pid
            and vw.lfcy_cond_id = 'IB-ACOM-02'
            and vw.lfcy_entity_type = 'IB'
            --and vw.lfcy_status_next = 'IB-ACOM'
            and nvl(vw.val_status,'X') <> 'Y'
            and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
            and uidt.item = vw.item
            and xmd.func_area = 'UDA'
            and xmd.parameter = 'SALES_START_DATE'
            and xmd.bu = vw.lfcy_entity
            and uidt.uda_id = xmd.value_1
            and uidt.UDA_DATE is not null
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'Y',
            error_message = null,
            last_val_dt = sysdate,
            last_update_datetime = sysdate;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IB_ACOM_02;
--------------------------------------------------------------------------------
-- IB_ACOM_03: The Active for commerce Target Date must be defined (the user
-- will be alerted through the dashboard otherwise).
-- The item will go to Actif Commerce when this date has been reached.
--------------------------------------------------------------------------------
FUNCTION IB_ACOM_03( O_error_message IN OUT   varchar2,
                     O_rule_changes     OUT      number,
                     O_result        OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid           IN       number,
                     I_param1        IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2        IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  --
  L_program             VARCHAR2(100) := CONST$PackageName || '.IB_ACOM_03';
  ---
  L_sql                 varchar2(3000) := '
    merge into xxadeo_lfcy_item_cond t0
      using (
        select  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id rule_id
          from xxadeo_v_lfcy_work vw
               ,xxadeo_mom_dvm              xmd
               ,cfa_attrib                  cfaa
               ,item_master_cfa_ext         imce
          where vw.process_id = :1
            and vw.lfcy_cond_id = ''IB-ACOM-03''
            and vw.lfcy_entity_type = ''IB''
            --and vw.lfcy_status_next = ''IB-ACOM''
            and nvl(vw.val_status,''X'') <> ''Y''
            and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
            and xmd.parameter = ''' || xxadeo_item_life_cycle_sql.CONST$CFAKEY_ActvDDt || '''
            and xmd.func_area = ''CFA''
            and xmd.bu = vw.lfcy_entity
            and cfaa.attrib_id = xmd.value_1
            and imce.group_id = cfaa.group_id
            and imce.' || xxadeo_item_life_cycle_sql.GV_CFACOL_ActvDDt || ' is not null
            and imce.item = vw.item
        ) src
      on (
        t0.item = src.item
        and t0.lfcy_entity = src.lfcy_entity
        and t0.lfcy_entity_type = src.lfcy_entity_type
        and t0.lfcy_cond_id = src.rule_id
      )
      when matched then
        update
          set val_status = ''Y''
              ,error_message = null
              ,last_val_dt = sysdate
              ,last_update_datetime = sysdate
  ';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_ACOM_03');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  execute immediate L_sql using I_pid;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IB_ACOM_03;
--------------------------------------------------------------------------------
-- IB_ACOM_031: For sellable items the Active for commerce Target Date must be
-- equal or less tommorow. Set next_val_dt if in future
--------------------------------------------------------------------------------
FUNCTION IB_ACOM_031( O_error_message IN OUT   varchar2,
                      O_rule_changes     OUT      number,
                      O_result        OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                      I_pid           IN       number,
                      I_param1        IN       xxadeo_system_rules_detail.value_1%TYPE,
                      I_param2        IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  --
  L_program             VARCHAR2(100) := CONST$PackageName || '.IB_ACOM_031';
  ---
  L_sql                 varchar2(3000) := '
    merge into xxadeo_lfcy_item_cond t0
      using (
        select  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id rule_id
          from xxadeo_v_lfcy_work vw
               ,xxadeo_mom_dvm              xmd
               ,cfa_attrib                  cfaa
               ,item_master_cfa_ext         imce
          where vw.process_id = :1
            and vw.lfcy_cond_id = ''IB-ACOM-031''
            and vw.lfcy_entity_type = ''IB''
            and nvl(vw.val_status,''X'') <> ''Y''
            and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
            and xmd.parameter = ''' || xxadeo_item_life_cycle_sql.CONST$CFAKEY_ActvDDt || '''
            and xmd.bu = vw.lfcy_entity
            and xmd.func_area = ''CFA''
            and cfaa.attrib_id = xmd.value_1
            and imce.group_id = cfaa.group_id
            and imce.' || xxadeo_item_life_cycle_sql.GV_CFACOL_ActvDDt || ' is not null
            and imce.' || xxadeo_item_life_cycle_sql.GV_CFACOL_ActvDDt || ' <= get_vdate+1
            and imce.item = vw.item

        ) src
      on (
        t0.item = src.item
        and t0.lfcy_entity = src.lfcy_entity
        and t0.lfcy_entity_type = src.lfcy_entity_type
        and t0.lfcy_cond_id = src.rule_id
      )
      when matched then
        update
          set val_status = ''Y''
              ,error_message = null
              ,last_val_dt = sysdate
              ,last_update_datetime = sysdate
  ';
  ---
  L_sql_dt               varchar2(3000) := '
    merge into xxadeo_lfcy_item_cond t0
      using (
        select  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id rule_id
                ,cfaa.attrib_id
                ,cfaa.group_id
                ,cfaa.storage_col_name
                ,imce.' || xxadeo_item_life_cycle_sql.GV_CFACOL_ActvDDt || ' next_val_dt
          from xxadeo_v_lfcy_work vw
               ,xxadeo_mom_dvm              xmd
               ,cfa_attrib                  cfaa
               ,item_master_cfa_ext         imce
          where vw.process_id = :1
            and vw.lfcy_cond_id = ''IB-ACOM-031''
            and vw.lfcy_entity_type = ''IB''
            --and vw.lfcy_status_next = ''IB-ACOM''
            and nvl(vw.val_status,''X'') not in (''Y'',''E'')
            and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
            and xmd.parameter = ''' || xxadeo_item_life_cycle_sql.CONST$CFAKEY_ActvDDt || '''
            and xmd.bu = vw.lfcy_entity
            and xmd.func_area = ''CFA''
            and cfaa.attrib_id = xmd.value_1
            and imce.group_id = cfaa.group_id
            and imce.' || xxadeo_item_life_cycle_sql.GV_CFACOL_ActvDDt || ' is not null
            and imce.' || xxadeo_item_life_cycle_sql.GV_CFACOL_ActvDDt || ' > get_vdate+1
            and imce.item = vw.item
        ) src
      on (
        t0.item = src.item
        and t0.lfcy_entity = src.lfcy_entity
        and t0.lfcy_entity_type = src.lfcy_entity_type
        and t0.lfcy_cond_id = src.rule_id
      )
      when matched then
        update
          set val_status = ''N''
              ,error_message = null
              ,last_val_dt = sysdate
              ,next_val_dt = src.next_val_dt
              ,last_update_datetime = sysdate
  ';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_ACOM_031');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Start');
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  -- validate rule
  ---
  execute immediate L_sql using I_pid;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  dbms_application_info.set_client_info('MERGE NEXT_VAL_DT');
  ---
  -- change next val date
  ---
  execute immediate L_sql_dt using I_pid;
  ---
  xxadeo_item_life_cycle_sql.DBG(L_program, 'After set next_val_dt. #ROWCOUNT=' || to_char(SQL%ROWCOUNT));
  xxadeo_item_life_cycle_sql.DBG(L_program, 'After set next_val_dt. SQL=' || L_sql_dt);
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IB_ACOM_031;
--------------------------------------------------------------------------------
-- IB_ACOM_04: Quality certification must be received for all the active
--             item-supplier combinations
--------------------------------------------------------------------------------
FUNCTION IB_ACOM_04( O_error_message IN OUT   varchar2,
                     O_rule_changes     OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  --
  L_program   VARCHAR2(100) := CONST$PackageName || '.IB_ACOM_04';
  ---
  L_sql       varchar2(6000) := '';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_ACOM_04');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  --
  L_sql := '
      merge into xxadeo_lfcy_item_cond t0
        using (
          select  vw.item
                  ,vw.lfcy_entity
                  ,vw.lfcy_entity_type
                  ,vw.lfcy_cond_id rule_id
            from xxadeo_v_lfcy_work vw
          where vw.process_id = :1
            and vw.lfcy_cond_id = ''IB-ACOM-04''
            and vw.lfcy_entity_type = ''IB''
            --and vw.lfcy_status_next = ''IB-ACOM''
            and nvl(vw.val_status,''X'') <> ''Y''
            and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
            -- must exist at least one active item/supp for the bu
            and exists (
              select 1
                from item_supplier          isup_1
                     ,xxadeo_mom_dvm        xmd_1
                     ,cfa_attrib            cfaa_1
                     ,item_supplier_cfa_ext isce_1
                     ,partner_org_unit      pou_1
                     ,xxadeo_bu_ou          xbo_1
               where 1=1
                 and isup_1.item = vw.item
                 and xmd_1.parameter = ''' || xxadeo_item_life_cycle_sql.CONST$CFAKEY_LnkStaItSup || '''
                 and nvl(xmd_1.bu,-1) = -1
                 and xmd_1.func_area = ''CFA''
                 and cfaa_1.attrib_id = xmd_1.value_1
                 and isce_1.item = isup_1.item
                 and isce_1.supplier = isup_1.supplier
                 and isce_1.group_id = cfaa_1.group_id
                 and isce_1.' || xxadeo_item_life_cycle_sql.GV_CFACOL_LnkStaItSup
                  || ' = ''' || xxadeo_item_life_cycle_sql.GV_CFAVAL_LnkStaItSup$Act || '''
                 and pou_1.partner = isup_1.supplier
                 and xbo_1.ou = pou_1.org_unit_id
                 and xbo_1.bu = vw.lfcy_entity
              )
            -- all active item/supp must have valid QC
            and not exists (
              select 1
                from item_supplier          isup_2
                     ,xxadeo_mom_dvm        xmd_2
                     ,cfa_attrib            cfaa_2
                     ,item_supplier_cfa_ext isce_2
                     ,partner_org_unit      pou_2
                     ,xxadeo_bu_ou          xbo_2
               where 1=1
                 and isup_2.item = vw.item
                 and xmd_2.parameter = ''' || xxadeo_item_life_cycle_sql.CONST$CFAKEY_LnkStaItSup || '''
                 and nvl(xmd_2.bu,-1) = -1
                 and xmd_2.func_area = ''CFA''
                 and cfaa_2.attrib_id = xmd_2.value_1
                 and isce_2.item = isup_2.item
                 and isce_2.supplier = isup_2.supplier
                 and isce_2.group_id = cfaa_2.group_id
                 and isce_2.' || xxadeo_item_life_cycle_sql.GV_CFACOL_LnkStaItSup
                  || ' = ''' || xxadeo_item_life_cycle_sql.GV_CFAVAL_LnkStaItSup$Act || '''
                 and pou_2.partner = isup_2.supplier
                 and xbo_2.ou = pou_2.org_unit_id
                 and xbo_2.bu = vw.lfcy_entity
                 -- this item/sup has no QC CFA line or QC has no valid values
                 and not exists (
                    select 1
                      from xxadeo_mom_dvm         xmd_3
                           ,cfa_attrib            cfaa_3
                           ,item_supplier_cfa_ext isce_3
                    where isce_3.item     = isup_2.item
                      and isce_3.supplier = isup_2.supplier
                      and xmd_3.parameter = ''' || xxadeo_item_life_cycle_sql.CONST$CFAKEY_StatusQCSupp || '''
                      and nvl(xmd_3.bu,-1) = -1
                      and xmd_3.func_area = ''CFA''
                      and cfaa_3.attrib_id = xmd_3.value_1
                      and isce_3.group_id = cfaa_3.group_id
                      and isce_3.' ||  xxadeo_item_life_cycle_sql.GV_CFACOL_StatusQC
                        || ' in ' || xxadeo_item_life_cycle_sql.GV_CFAVAL_StatusQCSuppCert || '
                )
              )
            ) src
        on
        (
          t0.item                 = src.item
          and t0.lfcy_entity      = src.lfcy_entity
          and t0.lfcy_entity_type = src.lfcy_entity_type
          and t0.lfcy_cond_id     = src.rule_id
          )
        when matched then
          update
             set t0.val_status              = ''Y''
                 ,t0.last_val_dt            = sysdate
                 ,t0.error_message          = null
                 ,t0.last_update_datetime   = sysdate
      ';
  ---
  xxadeo_item_life_cycle_sql.DBG(L_program, 'SQL=' || L_sql);
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  execute immediate L_sql using I_pid;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
  --
EXCEPTION
  when OTHERS then
    DBG_SQL.MSG(L_program, 'Error: ' || SQLERRM);
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IB_ACOM_04;
--------------------------------------------------------------------------------
-- IB-DISC-01 - The Sales Stop Date has been reached.
--------------------------------------------------------------------------------
FUNCTION IB_DISC_01( O_error_message  IN OUT   varchar2,
                     O_rule_changes     OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  --
  L_program             VARCHAR2(100) := CONST$PackageName || '.IB_DISC_01';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_DISC_01');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
          select  vw.item
                  ,vw.lfcy_entity
                  ,vw.lfcy_entity_type
                  ,vw.lfcy_cond_id            rule_id
            from xxadeo_v_lfcy_work           vw
                 ,uda_item_date               uidt
                 ,xxadeo_mom_dvm              xmd
          where vw.process_id = I_pid
            and vw.lfcy_cond_id = 'IB-DISC-01'
            and vw.lfcy_entity_type = 'IB'
            --and vw.lfcy_status_next = 'IB-DISC'
            and nvl(vw.val_status,'X') <> 'Y'
            and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
            and uidt.item = vw.item
            and xmd.func_area = 'UDA'
            and xmd.parameter = 'SALES_END_DATE'
            and xmd.bu = vw.lfcy_entity
            and uidt.uda_id = xmd.value_1
            and nvl(uidt.uda_date,sysdate+2) <= get_vdate+1
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'Y',
            error_message = null,
            last_val_dt = sysdate,
            last_update_datetime = sysdate;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  dbms_application_info.set_client_info('MERGE NEXT_VAL_DT');
  ---
  -- update next_val_dt
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
          select  vw.item
                  ,vw.lfcy_entity
                  ,vw.lfcy_entity_type
                  ,vw.lfcy_cond_id            rule_id
                  ,uidt.uda_date
            from xxadeo_v_lfcy_work           vw
                 ,uda_item_date               uidt
                 ,xxadeo_mom_dvm              xmd
          where vw.process_id = I_pid
            and vw.lfcy_cond_id = 'IB-DISC-01'
            and vw.lfcy_entity_type = 'IB'
            --and vw.lfcy_status_next = 'IB-DISC'
            and nvl(vw.val_status,'X') <> 'Y'
            and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
            and uidt.item = vw.item
            and xmd.func_area = 'UDA'
            and xmd.parameter = 'SALES_END_DATE'
            and xmd.bu = vw.lfcy_entity
            and uidt.uda_id = xmd.value_1
            and uidt.uda_date is not null
            and uidt.uda_date > get_vdate+1
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'N',
            error_message = null,
            last_val_dt = sysdate,
            next_val_dt = src.uda_date - 1,
            last_update_datetime = sysdate;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IB_DISC_01;
--------------------------------------------------------------------------------
-- IB_DISC_02 - At least one of the pack components is in the
-- Discontinued Status OR pack stop date reached
--------------------------------------------------------------------------------
FUNCTION IB_DISC_02(O_error_message  IN OUT  varchar2,
                    O_rule_changes   OUT      number,
                    O_result         OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid            IN       number,
                    I_param1         IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2         IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IB_DISC_02';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_DISC_02');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
      select  vw.item
              ,vw.lfcy_entity
              ,vw.lfcy_entity_type
              ,vw.lfcy_cond_id            rule_id
        from xxadeo_v_lfcy_work           vw
             ,uda_item_date               uidt
             ,xxadeo_mom_dvm              xmd
      where vw.process_id = I_pid
        and vw.lfcy_cond_id = 'IB-DISC-02'
        and vw.lfcy_entity_type = 'IB'
        and nvl(vw.val_status,'X') <> 'Y'
        and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
        and uidt.item = vw.item
        and xmd.func_area (+) = 'UDA'
        and xmd.parameter (+) = 'SALES_END_DATE'
        and xmd.bu (+) = vw.lfcy_entity
        and uidt.uda_id = xmd.value_1 (+)
        and uidt.uda_date is not null
        and (
          nvl(uidt.uda_date,sysdate+2) <= get_vdate+1
          OR
          exists (select 1
                    from item_master        im,
                         packitem           pi,
                         uda_item_lov       uil,
                         xxadeo_mom_dvm     xmd
                   where pi.pack_no = vw.item
                     and im.item = pi.item
                     and uil.item = im.item
                     and xmd.func_area = 'UDA'
                     and xmd.parameter = 'LIFECYCLE_STATUS'
                     and xmd.bu = vw.lfcy_entity
                     and uil.uda_id = xmd.value_1
                     and nvl(uil.uda_value,0) = xxadeo_item_life_cycle_sql.GV_UDAVAL_LFCY_status$disc
                  )
            )
      group by vw.item
               ,vw.lfcy_entity
               ,vw.lfcy_entity_type
               ,vw.lfcy_cond_id
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'Y',
            error_message = null,
            last_val_dt = sysdate,
            last_update_datetime = sysdate;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  dbms_application_info.set_client_info('MERGE NEXT_VAL_DT');
  ---
  -- For non validated conditions, set next_val_dt = max(next_val_dt) of pack components
  ---
    merge into xxadeo_lfcy_item_cond t0
    using (
      select  vw.item
              ,vw.lfcy_entity
              ,vw.lfcy_entity_type
              ,vw.lfcy_cond_id              rule_id
              ,min(cpi.next_val_dt)         next_val_dt
        from xxadeo_v_lfcy_work             vw
             ,packitem                      pi
             ,xxadeo_lfcy_item_cond         cpi
      where vw.process_id = I_pid
        and vw.lfcy_cond_id = 'IB-DISC-02'
        and vw.lfcy_entity_type = 'IB'
        --and vw.lfcy_status_next = 'IB-DISC'
        and nvl(vw.val_status,'X') <> 'Y'
        and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
        --and vw.pack_ind = 'Y'
        and pi.pack_no = vw.item
        and cpi.item = pi.item
        and cpi.lfcy_entity_type = 'IB'
        and cpi.lfcy_entity = vw.lfcy_entity
        and cpi.lfcy_cond_id = 'IB-DISC-01'
        and nvl(cpi.val_status,'X') <> 'Y'
      group by  vw.item
                ,vw.lfcy_entity
                ,vw.lfcy_entity_type
                ,vw.lfcy_cond_id
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'N',
            error_message = null,
            last_val_dt = sysdate,
            next_val_dt = greatest(nvl(src.next_val_dt,get_vdate),nvl(next_val_dt,get_vdate)),
            last_update_datetime = sysdate;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
    ---
END IB_DISC_02;
--------------------------------------------------------------------------------
-- IB-DEL-01
-- No stock movements, transfers, etc
-- and item created date <= min(I_param1,get_vdate)
--------------------------------------------------------------------------------
FUNCTION IB_DEL_01( O_error_message    IN OUT  varchar2,
                    O_rule_changes     OUT      number,
                    O_result           OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid              IN       number,
                    I_param1           IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2           IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IB_DEL_01';
  ---
  L_diff_months number(3,0) := 24;
  ---
  cursor C_items is
    select  vw.item
            ,vw.lfcy_entity
            ,vw.lfcy_cond_id          rule_id
            ,im.create_datetime
            --,c.val_status
            --,c.last_update_datetime
            --,c.last_val_dt
    from xxadeo_v_lfcy_work           vw
         ,item_master                 im
    where vw.process_id = I_pid
      and vw.lfcy_cond_id = 'IB-DISC-02'
      and vw.lfcy_entity_type = 'IB'
      --and vw.lfcy_status_next = 'IB-DISC'
      and nvl(vw.val_status,'X') <> 'Y'
      and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
      and im.item = vw.item
  ;
  ---
  L_foo_int   number;
  L_min_dt    date;
  L_ord_dt    date;
  ---
  L_tsf_dt            date := null;
  L_tsf_create_date   date := null;
  L_tsf_approval_date date := null;
  L_tsf_delivery_date date := null;
  L_tsf_close_date    date := null;
  ---
  L_inv_adj_dt        date := null;
  ---
  L_ship_min_dt       date := null;
  L_ship_date         date := null;
  L_ship_rcv_date     date := null;
  ---
  L_tran_dt           date := null;
  ---
  L_error_message     xxadeo_lfcy_item_cond.error_message%TYPE;
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_DEL_01');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  -- Param1 is number of months to consider for deletion
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  if I_param1 is not null then
    L_diff_months := to_number(I_param1);
    ---
    if L_diff_months <= 0 then
      O_error_message := 'Invalid value for parameter';
      return FALSE;
    end if;
    ---
  end if;
  ---
  -- Calc min date to consider rule valid
  ---
  L_min_dt := add_months(get_vdate, -L_diff_months);
  ---
  dbms_application_info.set_client_info('MERGE ITEM_MASTER CREATE DT');
  ---
  -- First set next_val_dt
  -- check item_master creation date and set cond next_val_dt + diff
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
      select  vw.item
              ,vw.lfcy_entity
              ,vw.lfcy_entity_type
              ,vw.lfcy_cond_id          rule_id
              ,im.create_datetime
      from xxadeo_v_lfcy_work           vw
           ,item_master                 im
      where vw.process_id = I_pid
        and vw.lfcy_cond_id = 'IB-DEL-01'
        and vw.lfcy_entity_type = 'IB'
        --and vw.lfcy_status_next = 'IB-DEL'
        and nvl(vw.val_status,'X') <> 'Y'
        and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
        and im.item = vw.item
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set
            next_val_dt = add_months(src.create_datetime,L_diff_months),
            error_message = null,
            val_status = 'N',
            last_update_datetime = sysdate;
  ---
  -- check items that can be evaluated now
  ---
  for row in c_items loop
    begin
      -- Check stock on hand
      L_foo_int := 0;
      ---
      dbms_application_info.set_client_info('ITEM_LOC_SOH');
      ---
      select count(1)
        into L_foo_int
        from item_loc_soh soh
       where soh.item = row.item
         and soh.stock_on_hand > 0;
      ---
      if L_foo_int > 0 then
        update xxadeo_lfcy_item_cond
          set val_status = 'N'
              , last_val_dt = sysdate
              , last_update_datetime = sysdate
          where item = row.item
            and lfcy_entity_type = 'IB'
            and lfcy_entity = row.lfcy_entity
            and lfcy_cond_id = 'IB-DEL-01';
        continue;
      end if;
      ---
      -- Check open PO (purchase orders) => ORDHEAD / ORDLOC
      -- and set next_val_dt if one exists
      ---
      L_ord_dt := null;
      ---
      dbms_application_info.set_client_info('ORDHEAD');
      ---
      select max(oh.last_update_datetime)
        into L_ord_dt
        from ordhead oh
             ,ordloc ol
       where ol.item = row.item
         and oh.order_no = ol.order_no;
      ---
      if L_ord_dt is not null and L_ord_dt > L_min_dt then
        ---
        dbms_application_info.set_client_info('ORDERS => UPDATE VAL_STATUS=N');
        ---
        update xxadeo_lfcy_item_cond
          set val_status = 'N'
              , error_message = null
              , last_val_dt = sysdate
              , last_update_datetime = sysdate
              , next_val_dt = greatest(next_val_dt,add_months(L_ord_dt,L_diff_months))
          where item = row.item
            and lfcy_entity_type = 'IB'
            and lfcy_entity = row.lfcy_entity
            and lfcy_cond_id = 'IB-DEL-01';
        ---
        continue;
        ---
      end if;
      ---
      -- Check open TSF (transfer) => TSFHEAD / TSFDETAIL
      -- and set next_val_dt if one exists
      ---
      L_tsf_create_date := null;
      L_tsf_approval_date := null;
      L_tsf_delivery_date := null;
      L_tsf_close_date := null;
      ---
      dbms_application_info.set_client_info('TSFHEAD');
      ---
      select max(th.create_date)
             ,max(th.approval_date)
             ,max(th.delivery_date)
             ,max(th.close_date)
        into L_tsf_create_date
             ,L_tsf_approval_date
             ,L_tsf_delivery_date
             ,L_tsf_close_date
        from tsfhead th
             ,tsfdetail td
       where td.item = row.item
         and th.tsf_no = td.tsf_no;
      ---
      L_tsf_dt := greatest(nvl(L_tsf_create_date,L_min_dt)
                          ,nvl(L_tsf_approval_date,L_min_dt)
                          ,nvl(L_tsf_delivery_date,L_min_dt)
                          ,nvl(L_tsf_close_date,L_min_dt));
      ---
      if L_tsf_dt is not null and L_tsf_dt > L_min_dt then
        ---
        dbms_application_info.set_client_info('TRANSFERS => UPDATE VAL_STATUS=N');
        ---
        update xxadeo_lfcy_item_cond
          set val_status = 'N'
              , error_message = null
              , last_val_dt = sysdate
              , last_update_datetime = sysdate
              , next_val_dt = greatest(next_val_dt,add_months(L_tsf_dt,L_diff_months))
          where item = row.item
            and lfcy_entity_type = 'IB'
            and lfcy_entity = row.lfcy_entity
            and lfcy_cond_id = 'IB-DEL-01';
        ---
        continue;
        ---
      end if;
      ---
      -- Check open shipments => SHIPMENT/SHIPSKU
      -- and set next_val_dt if one exists
      ---
      L_ship_date := null;
      L_ship_rcv_date := null;
      ---
      dbms_application_info.set_client_info('SHIPMENT');
      ---
      select max(sh.ship_date)
             ,max(sh.receive_date)
        into L_ship_date
             ,L_ship_rcv_date
        from shipment sh
             ,shipsku ssku
       where ssku.item = row.item
         and sh.shipment = ssku.shipment;
      ---
      L_ship_min_dt := greatest(nvl(L_ship_date,L_min_dt)
                                ,nvl(L_ship_rcv_date,L_min_dt));
      ---
      if L_ship_min_dt is not null and L_ship_min_dt > L_min_dt then
        ---
        dbms_application_info.set_client_info('SHIPMENT => UPDATE VAL_STATUS=N');
        ---
        update xxadeo_lfcy_item_cond
          set val_status = 'N'
              , error_message = null
              , last_val_dt = sysdate
              , last_update_datetime = sysdate
              , next_val_dt = greatest(next_val_dt,add_months(L_ship_min_dt,L_diff_months))
          where item = row.item
            and lfcy_entity_type = 'IB'
            and lfcy_entity = row.lfcy_entity
            and lfcy_cond_id = 'IB-DEL-01';
        ---
        continue;
        ---
      end if;
      ---
      -- Check inventory adjustments => INV_ADJ
      -- and set next_val_dt if one exists
      ---
      L_inv_adj_dt := null;
      ---
      dbms_application_info.set_client_info('INV_ADJ');
      ---
      select max(adj_date)
        into L_inv_adj_dt
        from inv_adj
       where inv_adj.item = row.item;
      ---
      if L_inv_adj_dt is not null and L_inv_adj_dt > L_min_dt then
        ---
        dbms_application_info.set_client_info('INV_ADJ => UPDATE VAL_STATUS=N');
        ---
        update xxadeo_lfcy_item_cond
          set val_status = 'N'
              , error_message = null
              , last_val_dt = sysdate
              , last_update_datetime = sysdate
              , next_val_dt = greatest(next_val_dt,add_months(L_inv_adj_dt,L_diff_months))
          where item = row.item
            and lfcy_entity_type = 'IB'
            and lfcy_entity = row.lfcy_entity
            and lfcy_cond_id = 'IB-DEL-01';
        ---
        continue;
        ---
      end if;
      ---
      -- Check sales and returns => TRAN_DATA_HISTORY (per day)
      -- and set next_val_dt if one exists
      ---
      L_tran_dt := null;
      ---
      dbms_application_info.set_client_info('TRAN_DATA_HISTORY');
      ---
      select max(tdh.tran_date)
        into L_tran_dt
        from tran_data_history tdh
       where tdh.item = row.item;
      ---
      if L_tran_dt is not null and L_tran_dt > L_min_dt then
        ---
        dbms_application_info.set_client_info('TRANS_DATA_HISTORY => UPDATE VAL_STATUS=N');
        ---
        update xxadeo_lfcy_item_cond
          set val_status = 'N'
              , error_message = null
              , last_val_dt = sysdate
              , last_update_datetime = sysdate
              , next_val_dt = greatest(next_val_dt,add_months(L_tran_dt,L_diff_months))
          where item = row.item
            and lfcy_entity_type = 'IB'
            and lfcy_entity = row.lfcy_entity
            and lfcy_cond_id = 'IB-DEL-01';
        ---
        continue;
        ---
      end if;
    exception when others then
      ---
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      dbms_application_info.set_client_info('EXCEPTION => UPDATE VAL_STATUS=E');
      ---
      update xxadeo_lfcy_item_cond
        set val_status = 'E'
            , last_val_dt = sysdate
            , last_update_datetime = sysdate
            , error_message = L_error_message
          where item = row.item
            and lfcy_entity_type = 'IB'
            and lfcy_entity = row.lfcy_entity
            and lfcy_cond_id = 'IB-DEL-01';
      ---
    end;
    ---
  end loop;
  ---
  ---
  --O_rule_changes := SQL%ROWCOUNT;
  --xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  O_result := null;
  ---
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IB_DEL_01;
--------------------------------------------------------------------------------
-- IB_DEL_02 -
-- If item is a pack and at least one component is in Deleted (Supprim?) Status
-- make entire pack Deleted.
--------------------------------------------------------------------------------
FUNCTION IB_DEL_02( O_error_message IN OUT  varchar2,
                    O_rule_changes     OUT      number,
                    O_result         OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid            IN       number,
                    I_param1         IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2         IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN IS
  ---
  L_program   VARCHAR2(100) := CONST$PackageName || '.IB_DEL_02';
  ---
BEGIN
  ---
  dbms_application_info.set_module(CONST$PackageName || '#' || I_pid, 'IB_DEL_02');
  ---
  O_rule_changes := 0;
  ---
  if GV_fake_exec then
    DBG_SQL.MSG(L_program, 'Begin fake. PID=' || to_char(I_pid));
    return true;
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Begin. PID=' || to_char(I_pid));
  ---
  dbms_application_info.set_client_info('MERGE');
  ---
  merge into xxadeo_lfcy_item_cond t0
    using (
      select  vw.item
              ,vw.lfcy_entity
              ,vw.lfcy_entity_type
              ,vw.lfcy_cond_id            rule_id
        from xxadeo_v_lfcy_work           vw
      where vw.process_id = I_pid
        and vw.lfcy_cond_id = 'IB-DEL-02'
        and vw.lfcy_entity_type = 'IB'
        --and vw.lfcy_status_next = 'IB-DEL'
        and nvl(vw.val_status,'X') <> 'Y'
        and nvl(vw.next_val_dt,get_vdate-1) <= get_vdate
        and exists (select 1
                      from item_master        im,
                           packitem           pi,
                           uda_item_lov       uil,
                           xxadeo_mom_dvm     xmd
                     where pi.pack_no = vw.item
                       and im.item = pi.item
                       and uil.item = im.item
                       and xmd.func_area = 'UDA'
                       and xmd.parameter = 'LIFECYCLE_STATUS'
                       and xmd.bu = vw.lfcy_entity
                       and uil.uda_id = xmd.value_1
                       and nvl(uil.uda_value,0) = xxadeo_item_life_cycle_sql.GV_UDAVAL_LFCY_status$del
                    )
      ) src
    on (
      t0.item = src.item
      and t0.lfcy_entity = src.lfcy_entity
      and t0.lfcy_entity_type = src.lfcy_entity_type
      and t0.lfcy_cond_id = src.rule_id
    )
    when matched then
      update
        set val_status = 'Y',
            error_message = null,
            last_val_dt = sysdate,
            last_update_datetime = sysdate;
  ---
  O_rule_changes := SQL%ROWCOUNT;
  xxadeo_item_life_cycle_sql.DBG(L_program, 'Merge: #ROWCOUNT=' || to_char(O_rule_changes));
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
END IB_DEL_02;

begin
  ---
  declare
    O_tbl DBG_SQL.TBL_DBG_OBJ;
  begin
    ---
    DBG_SQL.INIT(O_tbl);
    ---
  exception when others
    then null;
  end;
  ---
  null;
END XXADEO_ITEM_LFCY_VAL_IB_SQL;
/
