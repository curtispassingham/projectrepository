CREATE OR REPLACE PACKAGE BODY XXADEO_ITEM_ASSORTMENT_SQL AS

/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Jorge Agra                                                   */
/* UPDATE USER - Elsa Barros, Pedro Miranda                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package for RB55a - Assortment ITEM Level and ITEM_LOC Level */
/******************************************************************************/

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-07-12 - Jorge Agra - Separation of validation to PREPROCESS_ALL       */
/* 2018-07-19 - Jorge Agra - BUG#145 VALIDATE_SD: SALES_END_DATE MUST be in   */
/*                           future.                                          */
/*                           To ease debug, make validate functions public    */
/*                           Include DBG_SQL.MSG                              */
/* 2018-07-20 - Jorge Agra - BUG#153 pre-process all records                  */
/* 2018-07-26 - Jorge Agra - BUG#163 ITEM_LOC CFA fetch problem corrected     */
/* 2018-08-30 - Jorge Agra - BUG#347 Comparison of effective date with =.     */
/*                           Should be <=                                     */
/* 2018-09-13 - Jorge Agra - BUG#397+398 effective date for RS and AM must be */
/*                           in the future                                    */
/* 2018-09-13 - Jorge Agra - BUG#399 record_type of RS not validated          */
/* 2018-09-14 - Jorge Agra - BUG#401/403/404/406 Calc real availability not   */
/*                           considering rows with ready_for_export <> 'N'    */
/* 2018-09-14 - Jorge Agra - BUG#405 Missing validations for RMS_STATUS and   */
/*                           ASSORTMENT_TYPE                                  */
/* 2018-09-17 - Jorge Agra - BUG#416 Invalid number in GET_ILOC_CFAS          */
/* 2018-09-28 - Jorge Agra - ORACR 00327 Items are ranged in preproc if valid */
/*                         - BUG#500 O1C/Items/RB55a - Regular assortment -   */
/*                           End of assortment 2 (Permout)                    */
/* 2018-10-28 - Jorge Agra - BUG#617 Record 'D' in range size integrated      */
/* 2018-11-19 - Jorge Agra - BUG#676 delete uda of sales end date when null   */
/* 2018-11-19 - Jorge Agra - BUG#678 handle of dup vals in stg assortment mode*/
/* 2018-11-21 - Jorge Agra - BUG#692 sales start date can be in past          */
/* 2018-11-28 - Jorge Agra - Group by in PROCESS_RS                           */
/* 2018-12-12 - Jorge Agra - BUG#738 UDA date create on GTIN/EAN level        */
/* 2019-01-18 - Pedro Miranda - CR00390 Code 48                               */
/******************************************************************************/

--------------------------------------------------------------------------------
--   TYPES
--------------------------------------------------------------------------------
TYPE item_loc_assortment_rec IS RECORD( item                        ITEM_LOC.item%TYPE,
                                        loc_type                    ITEM_LOC.loc_type%TYPE,
                                        loc                         ITEM_LOC.loc%TYPE,
                                        assortment_type             XXADEO_ITEM_LOC_ASSORTMENT.ASSORTMENT_TYPE%TYPE,
                                        effective_date              DATE,
                                        availability                XXADEO_ITEM_LOC_ASSORTMENT.AVAILABILITY%TYPE,
                                        visibility                  XXADEO_ITEM_LOC_ASSORTMENT.VISIBILITY%TYPE,
                                        real_availability           XXADEO_ITEM_LOC_ASSORTMENT.REAL_AVAILABILITY%TYPE,
                                        hier_level                  XXADEO_ITEM_LOC_ASSORTMENT.HIER_LEVEL%TYPE,
                                        hier_value                  XXADEO_ITEM_LOC_ASSORTMENT.HIER_VALUE%TYPE,
                                        record_type                 XXADEO_ITEM_LOC_ASSORTMENT.RECORD_TYPE%TYPE,
                                        error_message               XXADEO_ITEM_LOC_ASSORTMENT.ERROR_MESSAGE%TYPE,
                                        ready_for_export            XXADEO_ITEM_LOC_ASSORTMENT.READY_FOR_EXPORT%TYPE,
                                        EXTRACTED_TO_RMS_DATETIME   XXADEO_ITEM_LOC_ASSORTMENT.EXTRACTED_TO_RMS_DATETIME%TYPE,
                                        ---
                                        VISIBILITY_DATE_ITEM_LOC    DATE,
                                        AVAILABILITY_DATE_ITEM_LOC  DATE,
                                        VISIBILITY_ITEM_LOC         XXADEO_ITEM_LOC_ASSORTMENT.VISIBILITY%TYPE,
                                        AVAILABILITY_ITEM_LOC       XXADEO_ITEM_LOC_ASSORTMENT.AVAILABILITY%TYPE,
                                        ---
                                        NEW_VISIBILITY_DATE         DATE,
                                        NEW_VISIBILITY              XXADEO_ITEM_LOC_ASSORTMENT.VISIBILITY%TYPE,
                                        NEW_AVAILABILITY_DATE       DATE,
                                        NEW_AVAILABILITY            XXADEO_ITEM_LOC_ASSORTMENT.AVAILABILITY%TYPE
                                       );

TYPE item_loc_assortment_tbl IS TABLE OF item_loc_assortment_rec INDEX BY BINARY_INTEGER;
TYPE UDACFAID_BY_BU_TBL is TABLE of NUMBER index by PLS_INTEGER;

LP_mom_dvm_rec      XXADEO_MOM_DVM_TBL;

--------------------------------------------------------------------------------
--   GLOBAL VARIABLES
--------------------------------------------------------------------------------
LP_system_options       SYSTEM_OPTIONS%ROWTYPE;
LP_date                 DATE := get_vdate;
LP_user_id              VARCHAR2(30)            := NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'),  USER);

Gb_range_in_preproc     boolean                 := TRUE;

--GV_UDA_sd               UDACFAID_BY_BU_TBL;
--GV_UDA_ed               UDACFAID_BY_BU_TBL;
--GV_UDA_am               UDACFAID_BY_BU_TBL;
--GV_UDA_rs               UDACFAID_BY_BU_TBL;

GV_globals_initialized        boolean := FALSE;

GP_sales_start_date           VARCHAR2(30) := 'SALES_START_DATE';
GP_sales_end_date             VARCHAR2(30) := 'SALES_END_DATE';
GP_assortment_mode            VARCHAR2(30) := 'ASSORTMENT_MODE';
GP_range_size                 VARCHAR2(30) := 'RANGE_SIZE';
GP_availability_item_loc      VARCHAR2(30) := 'AVAILABILITY_ITEM_LOC';
GP_availability_date_item_loc VARCHAR2(30) := 'AVAILABILITY_DATE_ITEM_LOC';
GP_visibility_item_loc        VARCHAR2(30) := 'VISIBILITY_ITEM_LOC';
GP_visibility_date_item_loc   VARCHAR2(30) := 'VISIBILITY_DATE_ITEM_LOC';

GV_CFACOL_Availability        VARCHAR2(30) := 'VARCHAR2_x';
GV_CFACOL_AvailabilityDate    VARCHAR2(30) := 'DATE_xx';
GV_CFACOL_Visibility          VARCHAR2(30) := 'VARCHAR2_y';
GV_CFACOL_VisibilityDate      VARCHAR2(30) := 'DATE_yy';

GV_CFAGRP_Availability        cfa_attrib.group_id%TYPE := 0;
GV_CFAGRP_AvailabilityDate    cfa_attrib.group_id%TYPE := 0;
GV_CFAGRP_Visibility          cfa_attrib.group_id%TYPE := 0;
GV_CFAGRP_VisibilityDate      cfa_attrib.group_id%TYPE := 0;

GV_CFAKEY_Availability        VARCHAR2(30) := 'AVAILABILITY_ITEM_LOC';
GV_CFAKEY_AvailabilityDate    VARCHAR2(30) := 'AVAILABILITY_DATE_ITEM_LOC';
GV_CFAKEY_Visibility          VARCHAR2(30) := 'VISIBILITY_ITEM_LOC';
GV_CFAKEY_VisibilityDate      VARCHAR2(30) := 'VISIBILITY_DATE_ITEM_LOC';


--------------------------------------------------------------------------------
--   PRIVATE  FUNCTIONS
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
FUNCTION get_iloc_cfas( O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_v           OUT     varchar2,
                        O_a           OUT     varchar2,
                        O_vd          OUT     date,
                        O_ad          OUT     date,
                        I_item        IN      item_loc_cfa_ext.item%TYPE,
                        I_loc         IN      item_loc_cfa_ext.loc%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION INITIALIZE_GLOBALS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION MERGE_ASSORTMENT_CFAS_LOC(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_input_rec        IN      item_loc_assortment_rec)
return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PERMOUT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 I_item             IN      item_loc.item%type,
                 I_loc              IN      item_loc.loc%type,
                 I_loc_type         IN      item_loc.loc_type%type
                 )
return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION RANGE_ITEM_LOC(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item            IN      item_loc.item%type,
                        I_loc             IN      item_loc.loc%type,
                        I_loc_type        IN      item_loc.loc_type%type
                        )
return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION CALC_REAL_AVAILABILITY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_rec              IN OUT  item_loc_assortment_rec)
return BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION CALC_VISIBILITY(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_rec             IN OUT  item_loc_assortment_rec)
return BOOLEAN;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--   PUBLIC  FUNCTIONS
--------------------------------------------------------------------------------

FUNCTION PREPROCESS_ALL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_bulk_size        IN     NUMBER default null,
                        I_dept             IN      ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN IS
  --
  L_program   VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.PREPROCESS_ALL';

  L_nil_process_id    NIL_INPUT_WORKING.PROCESS_ID%TYPE;
  L_owner             VARCHAR2(50) := NULL;
  L_called_name       VARCHAR2(50) := NULL;
  L_line              NUMBER       := NULL;
  L_caller_type       VARCHAR2(50) := NULL;
  --
BEGIN
  ---
  DBG_SQL.MSG(L_program, 'Start: LP_date=' || to_char(LP_date,'YYYY-MM-DD'));
  ---
  if VALIDATE_SD(O_error_message) = FALSE then
    ---
    return FALSE;
    ---
  end if;
  ---
  if VALIDATE_AM(O_error_message, I_bulk_size, I_dept) = FALSE then
    --
    return FALSE;
    --
  end if;
  --
  if VALIDATE_RS(O_error_message) = FALSE then
    --
    return FALSE;
    --
  end if;
  ---
  if VALIDATE_LOC(O_error_message) = FALSE then
    --
    return FALSE;
    --
  end if;
  ---
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
    --
END PREPROCESS_ALL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_ALL(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size          IN      NUMBER default null,
                     I_dept               IN      ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN IS
  --
  L_program   VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_ALL';

  L_nil_process_id    NIL_INPUT_WORKING.PROCESS_ID%TYPE;
  L_owner             VARCHAR2(50) := NULL;
  L_called_name       VARCHAR2(50) := NULL;
  L_line              NUMBER       := NULL;
  L_caller_type       VARCHAR2(50) := NULL;
  --
BEGIN
  --
  DBG_SQL.MSG(L_program, 'Start: LP_date=' || to_char(LP_date,'YYYY-MM-DD HH24:MI'));
  --
  if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                           LP_system_options) = FALSE then
    --
    return FALSE;
    --
  end if;
  --
  SYS.OWA_UTIL.who_called_me(L_owner,
                             L_called_name,
                             L_line,
                             L_caller_type);
  --
  if PROCESS_SD(O_error_message) = FALSE then
    --
    return FALSE;
    --
  end if;
  --
  if PROCESS_AM(O_error_message, I_bulk_size, I_dept) = FALSE then
    --
    return FALSE;
    --
  end if;
  --
  if PROCESS_RS(O_error_message, I_bulk_size, I_dept) = FALSE then
    --
    return FALSE;
    --
  end if;
  --
  if PROCESS_LOC(O_error_message) = FALSE then
    --
    return FALSE;
    --
  end if;
  --
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
    --
END PROCESS_ALL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_SD(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN        NUMBER default null,
                     I_dept             IN        ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN IS
  --
  L_program     varchar2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_SD';
  L_process_id  number;
  --
BEGIN
  ---
  dbms_application_info.set_module('XXADEO_ITEM_ASSORTMENT_SQL', 'PROCESS_SD');
  --dbms_application_info.set_client_info('');
  ---
  DBG_SQL.MSG(L_program, 'Start. LP_date=' || to_char(LP_date,'YYYY-MM-DD'));
  ---
  -- Cria/atualiza UDA para start date
  ---
  dbms_application_info.set_action(action_name => 'MERGE START DATE');
  ---
  merge into UDA_ITEM_DATE t0
  using (select im.item,
                xcfg.value_1 uda_id,
                xsd.sales_start_date
           from xxadeo_item_sales_dates xsd,
                item_master             im,
                table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                         I_parameter => GP_sales_start_date)) xcfg
          where xsd.ready_for_export  = 'Y'
            and (im.item = xsd.item or im.item_parent = xsd.item or
                im.item_grandparent   = xsd.item)
            and xsd.business_unit_id  = xcfg.bu
            and im.item_level         <= im.tran_level
            --and im.dept              = nvl(I_dept,im.dept)
            --and rownum               <= I_bulk_size
        ) src
  on (t0.uda_id = src.uda_id
  and t0.item   = src.item)
  when matched then
    update
       set t0.uda_date             = src.sales_start_date,
           t0.last_update_datetime = sysdate,
           t0.last_update_id       = LP_user_id
  when not matched then
    insert
      (item,
       uda_id,
       uda_date,
       create_id,
       last_update_id,
       create_datetime,
       last_update_datetime)
    values
      (src.item,
       src.uda_id,
       src.sales_start_date,
       LP_user_id,
       LP_user_id,
       sysdate,
       sysdate);
  ---
  -- Cria/atualiza UDA para end date
  ---
  dbms_application_info.set_action(action_name => 'MERGE END DATE');
  ---
  merge into uda_item_date t0
  using (select im.item,
                xcfg.value_1 uda_id,
                xsd.sales_end_date
           from xxadeo_item_sales_dates xsd,
                item_master             im,
                table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                         I_parameter => GP_sales_end_date)) xcfg
          where xsd.ready_for_export = 'Y'
            and xsd.sales_end_date is not null
            and (im.item = xsd.item or im.item_parent = xsd.item or
                im.item_grandparent = xsd.item)
            and xsd.business_unit_id = xcfg.bu
            and im.item_level         <= im.tran_level
            --and im.dept              = nvl(I_dept,im.dept)
            --and rownum               <= I_bulk_size
        ) src
  on (t0.uda_id = src.uda_id
  and t0.item   = src.item)
  when matched then
    update
       set t0.uda_date             = src.sales_end_date,
           t0.last_update_datetime = sysdate,
           t0.last_update_id       = LP_user_id
  when not matched then
    insert
      (item,
       uda_id,
       uda_date,
       create_id,
       last_update_id,
       create_datetime,
       last_update_datetime)
    values
      (src.item,
       src.uda_id,
       src.sales_end_date,
       LP_user_id,
       LP_user_id,
       sysdate,
       sysdate);
  ---
  -- Apaga UDA para end date null
  ---
  dbms_application_info.set_action(action_name => 'DEL END DATE');
  ---
  delete uda_item_date
  where (item,uda_id) in
         (select im.item,
                xcfg.value_1 uda_id
           from xxadeo_item_sales_dates xsd,
                item_master             im,
                table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                         I_parameter => GP_sales_end_date)) xcfg
          where xsd.ready_for_export = 'Y'
            and xsd.sales_end_date is null
            and (im.item = xsd.item or im.item_parent = xsd.item or
                im.item_grandparent = xsd.item)
            and im.item_level <= im.tran_level
            and xsd.business_unit_id = xcfg.bu
          )
    ;
  ---
  update xxadeo_item_sales_dates xd
      set xd.ready_for_export          = 'S',
          xd.extracted_to_rms_datetime = sysdate,
          xd.last_update_datetime      = sysdate
    where xd.ready_for_export = 'Y';
  ---
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END PROCESS_SD;
--------------------------------------------------------------------------------
FUNCTION PROCESS_AM(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_bulk_size           IN      NUMBER default null,
                    I_dept                IN      ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN IS
  --
  L_program   VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_AM';
  --
BEGIN
  ---
  LP_mom_dvm_rec := NULL;
  ---
  -- Cria/atualiza UDA para assortment mode
  ---
  merge into uda_item_lov t0
  using (select im.item,
                xcfg.value_1 uda_id,
                xam.assortment_mode
           from xxadeo_item_assortment_mode xam,
                item_master                 im,
                table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                         I_parameter => GP_assortment_mode)) xcfg
          where xam.ready_for_export = 'Y'
            and xam.effective_date  <= lp_date + 1
            and xam.record_type      = 'C'
            and (im.item = xam.item or im.item_parent = xam.item or
                im.item_grandparent  = xam.item)
            and xam.business_unit_id = xcfg.bu
            and im.item_level         <= im.tran_level
            --and im.dept              = nvl(I_dept,im.dept)
            --and rownum               <= I_bulk_size
          group by im.item,
                   xcfg.value_1,
                   xam.assortment_mode
          ) src
  on (t0.uda_id = src.uda_id
  and t0.item   = src.item)
  when matched then
    update
       set t0.uda_value            = src.assortment_mode,
           t0.last_update_datetime = sysdate,
           t0.last_update_id       = LP_user_id
  when not matched then
    insert
      (item,
       uda_id,
       uda_value,
       create_id,
       last_update_id,
       create_datetime,
       last_update_datetime)
    values
      (src.item,
       src.uda_id,
       src.assortment_mode,
       LP_user_id,
       LP_user_id,
       sysdate,
       sysdate);
  ---
  -- Elimina os UDAs assortment mode com record type = 'D'
  ---
  delete uda_item_lov t0
   where (item, uda_id) IN
         (select im.item,
                 xcfg.value_1 uda_id
            from xxadeo_item_assortment_mode xam,
                 item_master                 im,
                 table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                         I_parameter => GP_assortment_mode)) xcfg
           where xam.ready_for_export = 'Y'
             and 1=0 -- TODO: ? mesmo para apagar???
             and xam.record_type      = 'D'
             and (im.item = xam.item or im.item_parent = xam.item or
                 im.item_grandparent  = xam.item)
             and xam.business_unit_id = xcfg.bu);
  ---
  --- TODO: bulk e dept filter
  ---
  update xxadeo_item_assortment_mode xd
     set xd.ready_for_export          = 'S',
         xd.extracted_to_rms_datetime = lp_date,
         xd.last_update_datetime      = sysdate
   where xd.ready_for_export = 'Y'
     and xd.effective_date  <= lp_date + 1;
  ---
  return TRUE;

EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END PROCESS_AM;
--------------------------------------------------------------------------------
FUNCTION PROCESS_RS(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN        NUMBER default null,
                     I_dept             IN        ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN IS
  --
  L_program   VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_RS';
  --
BEGIN
  --
  DBG_SQL.MSG(L_program, 'Start: LP_date=' || to_char(LP_date,'YYYY-MM-DD'));
  --
  merge into uda_item_lov t0
  using (select im.item,
                xcfg.value_1 uda_id,
                xrs.range_size
           from xxadeo_item_range_size xrs,
                table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                         I_parameter => GP_range_size))  xcfg,
                item_master im
          where xrs.business_unit_id = xcfg.bu
            and xrs.record_type = 'C'
            and (xrs.item = im.item or xrs.item = im.item_parent or
                 xrs.item = im.item_grandparent)
            and xrs.ready_for_export = 'Y'
            and XRS.EFFECTIVE_DATE  <= LP_Date + 1
            and im.item_level         <= im.tran_level
            --and im.dept              = nvl(I_dept,im.dept)
            --and rownum               <= I_bulk_size
          group by im.item,
                   xcfg.value_1,
                   xrs.range_size
        ) src
  on (t0.uda_id = src.uda_id
  and t0.item   = src.item)
  when matched then
    update
       set t0.uda_value            = src.range_size,
           t0.last_update_datetime = sysdate,
           t0.last_update_id       = LP_user_id
  when not matched then
    insert
      (item,
       uda_id,
       uda_value,
       create_id,
       last_update_id,
       create_datetime,
       last_update_datetime)
    values
      (src.item,
       src.uda_id,
       src.range_size,
       LP_user_id,
       LP_user_id,
       sysdate,
       sysdate);
  ---
  -- Elimina os UDAs range size se record type = 'D'
  ---
  delete uda_item_lov t0
   where (item, uda_id) in
         (select xrs.item, xcfg.value_1 uda_id
            from xxadeo_item_range_size xrs,
                 table(xxadeo_get_mom_dvm(I_func_area => 'UDA',
                                         I_parameter  => GP_range_size))xcfg,
                 item_master            im
           where xrs.ready_for_export = 'Y'
             and XRS.EFFECTIVE_DATE  <= LP_Date + 1
             and xrs.record_type = 'D'
             and 1 = 0 -- todo: ? mesmo para apagar????
             and xrs.business_unit_id = xcfg.bu
             and (xrs.item = im.item or xrs.item = im.item_parent or
                 xrs.item = im.item_grandparent));
  ---
  --- TODO: bulk e dept filter
  ---
  update xxadeo_item_range_size xd
     set xd.ready_for_export          = 'S',
         xd.extracted_to_rms_datetime = get_vdate,
         xd.last_update_datetime      = sysdate
   where xd.ready_for_export = 'Y'
     and xd.EFFECTIVE_DATE  <= LP_Date + 1;
  ---
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END PROCESS_RS;
--------------------------------------------------------------------------------
FUNCTION PROCESS_LOC(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN     NUMBER default null,
                     I_dept             IN      ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN IS
  --
  L_program           VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_LOC';
  --
  L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
  --
  L_input_rec         NEW_ITEM_LOC_SQL.NIL_INPUT_RECORD;
  L_input             NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;
  L_primary_supp      SUPS.SUPPLIER%TYPE;
  L_primary_cntry     COUNTRY.COUNTRY_ID%TYPE;
  L_status            VARCHAR2(1) := NULL;
  L_ranged_ind        VARCHAR2(1);
  L_rec               item_loc_assortment_rec;
  ---
  cursor C_cur_loc is
    select xloc.item,
           xloc.hier_level,
           xloc.hier_value,
           xloc.effective_date,
           xloc.record_type,
           xloc.availability,
           xloc.real_availability,
           xloc.visibility,
           xloc.assortment_type,
           xloc.ready_for_export
      from xxadeo_item_loc_assortment xloc,
           item_master im
     where xloc.ready_for_export = 'Y'
       and xloc.effective_date   <= LP_date + 1
       and im.item = xloc.item
       and (I_dept is null or im.dept = I_dept)
       --and rownum < I_bulk_size
    for update of xloc.ready_for_export;
    ---
    L_count number := 0;
BEGIN
  --
  DBG_SQL.MSG(L_program, 'Start LP_date=' || to_char(LP_date,'YYYY-MM-DD'));
  --
  if initialize_globals(O_error_message) = FALSE then
    --
    return FALSE;
    --
  end if;
  --
  for rec_loc in C_cur_loc loop
    --
    DBG_SQL.MSG(L_program, 'Loop: ITEM=' || to_char(rec_loc.item) || ', LOC=' || rec_loc.hier_value);
    DBG_SQL.MSG(L_program, 'Loop: READY_FOR_EXPORT=' || rec_loc.ready_for_export);
    --
    L_rec.item                          := rec_loc.item;
    L_rec.hier_value                    := rec_loc.hier_value;
    L_rec.hier_level                    := 'S';
    L_rec.effective_date                := rec_loc.effective_date;
    L_rec.availability                  := rec_loc.availability;
    L_rec.real_availability             := rec_loc.real_availability;
    L_rec.visibility                    := rec_loc.visibility;
    L_rec.assortment_type               := rec_loc.assortment_type;
    L_rec.record_type                   := rec_loc.record_type;
    L_rec.ready_for_export              := rec_loc.ready_for_export;
    ---
    if L_rec.ready_for_export = 'Y' and not get_iloc_cfas(L_error_message,
                                                          L_rec.availability_item_loc,
                                                          L_rec.visibility_item_loc,
                                                          L_rec.availability_date_item_loc,
                                                          L_rec.visibility_date_item_loc,
                                                          L_rec.item,
                                                          L_rec.hier_value)
    then
      ---
      DBG_SQL.MSG(L_program, 'Error in GET_ILOC_CFAS');
      ---
      L_rec.ready_for_export := 'E';
      L_rec.error_message    := L_error_message;
      ---
    end if;
    ---
    if not Gb_range_in_preproc then
      if L_error_message is null and not XXADEO_ITEM_LOC_WRP( L_error_message,
                                                         rec_loc.item,
                                                         rec_loc.hier_value,
                                                         rec_loc.hier_level
                                                         ) then
        ---
        L_error_message := SQL_LIB.CREATE_MSG('RANGING_ERROR',
                                              L_error_message,
                                              L_program,
                                              TO_CHAR(SQLCODE));
        ---
        L_rec.ready_for_export := 'E';
        L_rec.error_message    := L_error_message;
        ---
      end if;
    end if;
    ---
    if L_rec.ready_for_export = 'Y' then
      ---
      DBG_SQL.MSG(L_program, 'Before INTEGRATE_ITEM_LOC');
      ---
      if MERGE_ASSORTMENT_CFAS_LOC(O_error_message,
                                   L_rec) = FALSE then
        ---
        DBG_SQL.MSG(L_program, 'Error in MERGE_ASSORTMENT_CFAS_LOC');
        ---
        L_rec.ready_for_export := 'E';
        L_rec.error_message    := O_error_message;
        --
      else
        ---
        DBG_SQL.MSG(L_program, 'MERGE_ASSORTMENT_CFAS_LOC done');
        ---
        L_rec.ready_for_export          := 'S';
        L_rec.extracted_to_rms_datetime := sysdate;
        --
      end if;
    end if;
    ---
    if L_rec.ready_for_export = 'S' and (L_rec.availability = 'C' or L_rec.visibility = 'C') then
      ---
      if PERMOUT(O_error_message,
                 rec_loc.item,
                 rec_loc.hier_value,
                 rec_loc.hier_level
                 ) = FALSE then
        ---
        DBG_SQL.MSG(L_program, 'Error in PERMOUT');
        ---
        L_rec.ready_for_export := 'E';
        L_rec.error_message    := O_error_message;
        ---
      end if;
    end if;
    ---
    DBG_SQL.MSG(L_program, 'Before UPDATE: READY_FOR_EXPORT=' || L_rec.ready_for_export);
    ---
    update xxadeo_item_loc_assortment
       set extracted_to_rms_datetime = L_rec.extracted_to_rms_datetime,
           ready_for_export          = L_rec.ready_for_export,
           error_message             = L_rec.error_message
     where current of C_cur_loc;
  end loop;
  ---
  return TRUE;
  ---
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
    --
END PROCESS_LOC;

--------------------------------------------------------------------------------
--   PRIVATE  FUNCTIONS
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
FUNCTION INTEGRATE_ITEM_LOC_old(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_input_rec       IN      item_loc_assortment_rec)
RETURN BOOLEAN IS
  --
  L_program              VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.INTEGRATE_ITEM_LOC';
  --
  L_input_rec               NEW_ITEM_LOC_SQL.NIL_INPUT_RECORD;
  L_input                   NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;
  L_primary_supp            SUPS.SUPPLIER%TYPE;
  L_primary_cntry           COUNTRY.COUNTRY_ID%TYPE;
  L_status                  VARCHAR2(1) := NULL;
  L_ranged_ind              VARCHAR2(1);
  L_cfa_attrib_tbl          XXADEO_CFA_ATTRIB_TBL  := XXADEO_CFA_ATTRIB_TBL();
  L_xxadeo_cfa_key_val_tbl  XXADEO_CFA_KEY_VAL_TBL := XXADEO_CFA_KEY_VAL_TBL();
  --
BEGIN
  --
  -- get the item's primary supplier and primary country, which will be used as
  -- the new item_loc's primary supplier and primary country.
  --
  if not ITEM_SUPP_COUNTRY_SQL.GET_PRIM_SUPP_CNTRY(O_error_message,
                                                   L_primary_supp,
                                                   L_primary_cntry,
                                                   I_input_rec.item) then
    -- ERROR??
    return FALSE;
    --
  end if;
  ---
  -- Create item_loc
  ---
  L_input_rec.item                    := I_input_rec.item;
  L_input_rec.item_parent             := NULL;
  L_input_rec.item_grandparent        := NULL;
  L_input_rec.item_desc               := NULL;
  L_input_rec.item_short_desc         := NULL;
  L_input_rec.dept                    := NULL;
  L_input_rec.item_class              := NULL;
  L_input_rec.subclass                := NULL;
  L_input_rec.item_level              := NULL;
  L_input_rec.tran_level              := NULL;
  L_input_rec.item_status             := NULL;
  L_input_rec.waste_type              := NULL;
  L_input_rec.sellable_ind            := NULL;
  L_input_rec.orderable_ind           := NULL;
  L_input_rec.pack_ind                := NULL;
  L_input_rec.pack_type               := NULL;
  L_input_rec.diff_1                  := NULL;
  L_input_rec.diff_2                  := NULL;
  L_input_rec.diff_3                  := NULL;
  L_input_rec.diff_4                  := NULL;
  L_input_rec.loc                     := I_input_rec.hier_value;
  L_input_rec.loc_type                := I_input_rec.hier_level;
  L_input_rec.daily_waste_pct         := NULL;
  L_input_rec.unit_cost_loc           := NULL;
  L_input_rec.unit_retail_loc         := NULL;
  L_input_rec.selling_retail_loc      := NULL;
  L_input_rec.selling_uom             := NULL;
  L_input_rec.multi_units             := NULL;
  L_input_rec.multi_unit_retail       := NULL;
  L_input_rec.multi_selling_uom       := NULL;
  L_input_rec.item_loc_status         := 'A';
  L_input_rec.taxable_ind             := NULL;
  L_input_rec.ti                      := NULL;
  L_input_rec.hi                      := NULL;
  L_input_rec.store_ord_mult          := NULL;
  L_input_rec.meas_of_each            := NULL;
  L_input_rec.meas_of_price           := NULL;
  L_input_rec.uom_of_price            := NULL;
  L_input_rec.primary_variant         := NULL;
  L_input_rec.primary_supp            := L_primary_supp;
  L_input_rec.primary_cntry           := L_primary_cntry;
  L_input_rec.local_item_desc         := NULL;
  L_input_rec.local_short_desc        := NULL;
  L_input_rec.primary_cost_pack       := NULL;
  L_input_rec.receive_as_type         := NULL;
  L_input_rec.store_price_ind         := NULL;
  L_input_rec.uin_type                := NULL;
  L_input_rec.uin_label               := NULL;
  L_input_rec.capture_time            := NULL;
  L_input_rec.ext_uin_ind             := NULL;
  L_input_rec.source_method           := NULL;
  L_input_rec.source_wh               := NULL;
  L_input_rec.inbound_handling_days   := NULL;
  L_input_rec.currency_code           := NULL;
  L_input_rec.like_store              := NULL;
  L_input_rec.default_to_children_ind := 'Y';
  L_input_rec.class_vat_ind           := NULL;
  L_input_rec.hier_level              := I_input_rec.hier_level;
  L_input_rec.hier_num_value          := I_input_rec.hier_value;
  L_input_rec.hier_char_value         := NULL;
  L_input_rec.ranged_ind              := 'N';
  L_input_rec.costing_loc             := NULL;
  L_input_rec.costing_loc_type        := NULL;
  ---
  L_input(0) := L_input_rec;
  ---
  declare
    --
    O_error_message_nil RTK_ERRORS.RTK_TEXT%TYPE;
    --
  begin
    --
    if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message_nil, L_input) = FALSE then
      --
      return FALSE;
      --
    end if;
    --
  end;
  --
  -- Create/Update CFA's for ITEM and children From NEW_ITEM_LOC the children are already ranged
  --
  -- set cfa
  --
  for rec in (select im.item,
                     il.loc
                from item_master      im,
                     item_loc         il
               where (im.item             = i_input_rec.item or
                      im.item_parent      = i_input_rec.item or
                      im.item_grandparent = i_input_rec.item)
                 and il.item              = im.item
                 and il.loc               = i_input_rec.hier_value) loop
    --
    for i in (select null value_date,
                     i_input_rec.new_availability value_varchar,
                     ca.data_type data_type,
                     md.*
                from cfa_attrib ca,
                     table(XXADEO_GET_MOM_DVM(I_func_area     => 'CFA',
                                              I_parameter     => GP_availability_item_loc))md
               where ca.attrib_id = md.value_1
              union all
              select i_input_rec.effective_date value_date,
                     null value_varchar,
                     ca.data_type data_type,
                     md.*
                from cfa_attrib ca,
                     table(XXADEO_GET_MOM_DVM(I_func_area     => 'CFA',
                                              I_parameter     => GP_availability_date_item_loc))md
               where ca.attrib_id = md.value_1
              union all
              select null value_date,
                     i_input_rec.new_visibility value_varchar,
                     ca.data_type data_type,
                     md.*
                from cfa_attrib ca,
                     table(XXADEO_GET_MOM_DVM(I_func_area     => 'CFA',
                                              I_parameter     => GP_visibility_item_loc))md
               where ca.attrib_id = md.value_1
              union all
              select i_input_rec.new_visibility_date value_date,
                     null value_varchar,
                     ca.data_type data_type,
                     md.*
                from cfa_attrib ca,
                     table(XXADEO_GET_MOM_DVM(I_func_area     => 'CFA',
                                              I_parameter     => GP_visibility_date_item_loc))md
               where ca.attrib_id = md.value_1) loop
      --
      L_xxadeo_cfa_key_val_tbl := XXADEO_CFA_KEY_VAL_TBL();
      L_cfa_attrib_tbl         := XXADEO_CFA_ATTRIB_TBL();
      --
      if i.data_type = 'VARCHAR2' then
        --
      L_xxadeo_cfa_key_val_tbl := XXADEO_CFA_KEY_VAL_TBL();
      L_cfa_attrib_tbl         := XXADEO_CFA_ATTRIB_TBL();
      --
        L_xxadeo_cfa_key_val_tbl.extend();
        L_xxadeo_cfa_key_val_tbl(L_xxadeo_cfa_key_val_tbl.count) := new XXADEO_CFA_KEY_VAL_OBJ(key_col_1            => 'ITEM',
                                                                                               key_col_2            => 'LOC',
                                                                                               key_value_1          => rec.item,
                                                                                               key_value_2          => rec.loc,-- 494  AVAILABILITY_DATE_ITEM_LOC  DATE_22
                                                                                               attrib_varchar_value => i.value_varchar,
                                                                                               attrib_number_value  => null,
                                                                                               attrib_date_value    => null);-- 492 AVAILABILITY_ITEM_LOC VARCHAR2_2
        --
        L_cfa_attrib_tbl.extend();
        L_cfa_attrib_tbl(L_cfa_attrib_tbl.count) := XXADEO_CFA_ATTRIB_OBJ(attrib_id            => i.value_1,
                                                                          cfa_key_val_tbl      => L_xxadeo_cfa_key_val_tbl);
        --
      elsif i.data_type = 'DATE' then
        --
      L_xxadeo_cfa_key_val_tbl := XXADEO_CFA_KEY_VAL_TBL();
      L_cfa_attrib_tbl         := XXADEO_CFA_ATTRIB_TBL();
      --
        L_xxadeo_cfa_key_val_tbl.extend();
        L_xxadeo_cfa_key_val_tbl(L_xxadeo_cfa_key_val_tbl.count) := new XXADEO_CFA_KEY_VAL_OBJ(key_col_1            => 'ITEM',
                                                                                               key_col_2            => 'LOC',
                                                                                               key_value_1          => rec.item,
                                                                                               key_value_2          => rec.loc,
                                                                                               attrib_date_value    => i.value_date, -- 492 AVAILABILITY_ITEM_LOC VARCHAR2_2
                                                                                               attrib_varchar_value => null,
                                                                                               attrib_number_value  => null);-- 492 AVAILABILITY_ITEM_LOC VARCHAR2_2
        --
        L_cfa_attrib_tbl.extend();
        L_cfa_attrib_tbl(L_cfa_attrib_tbl.count) := XXADEO_CFA_ATTRIB_OBJ(attrib_id            => i.value_1,
                                                                          cfa_key_val_tbl      => L_xxadeo_cfa_key_val_tbl);
        --
      end if;
      --
      if L_cfa_attrib_tbl.count <> 1 then
        raise_application_error(-20000, 'erro');
      end if;
      --
      if XXADEO_CFA_SQL.SET_CFA_BY_ATTRIB(O_error_message   => O_error_message,
                                          IO_cfa_attrib_tbl => L_cfa_attrib_tbl) = FALSE then
        --
        RETURN FALSE;
        --
      end if;
      --
    end loop;
    --
  end loop;
  --
  /*
  if NS_CFA_SQL.SET_CFA_BY_ATTRIB(O_error_message   => O_error_message,
                                  IO_cfa_attrib_tbl => L_cfa_attrib_tbl) = FALSE then
    --
    RETURN FALSE;
    --
  end if;
  */
  --
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END INTEGRATE_ITEM_LOC_old;
--------------------------------------------------------------------------------
FUNCTION merge_cfa( O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item            IN      item_loc_cfa_ext.item%TYPE,
                    I_loc             IN      item_loc_cfa_ext.loc%TYPE,
                    I_value_type      IN      varchar2,
                    I_value_date      IN      date default null,
                    I_value_varchar2  IN      varchar2 default null,
                    I_group_id        IN      item_loc_cfa_ext.group_id%TYPE,
                    I_col_name        IN      varchar2)
return BOOLEAN is
  ---
  L_program   VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.MERGE_CFA';
  ---
  L_sql_merge varchar2(2000) := '';
  ---
BEGIN
  ---
  DBG_SQL.MSG(L_program, 'Begin');
  ---
  L_sql_merge := '
    merge into item_loc_cfa_ext t0
      using (select item,
                    loc
               from item_loc iloc
              where iloc.item = ''' || I_item || '''
                and iloc.loc = ''' || I_loc || '''
            ) src
      on (t0.item           = src.item
          and t0.loc        = src.loc
          and t0.group_id   = ' || to_char(I_group_id) ||')
      when matched then
        update
           set t0.' || I_col_name || ' = :1
      when not matched then
        insert
          (item,
           loc,
           group_id,
           ' || I_col_name || ')
        values
          (src.item,
           src.loc,
           ' || to_char(I_group_id) || ',
           :2
           )';
  DBG_SQL.MSG(L_program, L_sql_merge);
  ---
  if I_value_type ='DATE' then
    execute immediate L_sql_merge using I_value_date, I_value_date;
  elsif I_value_type ='VARCHAR2' then
    execute immediate L_sql_merge using I_value_varchar2, I_value_varchar2;
  else
    O_error_message := 'Invalid value type: ' || I_value_type;
    return false;
  end if;
  ---
  return true;
  ---
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
end;
--------------------------------------------------------------------------------
FUNCTION get_iloc_cfas( O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_v           OUT     varchar2,
                        O_a           OUT     varchar2,
                        O_vd          OUT     date,
                        O_ad          OUT     date,
                        I_item        IN      item_loc_cfa_ext.item%TYPE,
                        I_loc         IN      item_loc_cfa_ext.loc%TYPE)
return BOOLEAN is
  ---
  L_program   VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.GET_ILOCS_CFA';
  ---
  L_sql varchar2(2000) := '
             select ' || GV_CFACOL_Availability || ' availability,
                    ' || GV_CFACOL_Visibility || ' visibility,
                    ' || GV_CFACOL_AvailabilityDate || ' availability_date,
                    ' || GV_CFACOL_VisibilityDate || ' visibility_date
               from item_loc_cfa_ext ilce
              where ilce.item = ''' || I_item || '''
                and ilce.loc = ''' || I_loc || '''
                and ilce.group_id = ' || GV_CFAGRP_Availability;
  ---
BEGIN
  ---
  DBG_SQL.MSG(L_program, L_sql);
  ---
  execute immediate L_sql into O_a, O_v, O_ad, O_vd;
  ---
  return true;
  ---
EXCEPTION
  --
  when NO_DATA_FOUND then
    O_a := null;
    O_v := null;
    O_ad := null;
    O_vd := null;
    return TRUE;

  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
end;
--------------------------------------------------------------------------------
FUNCTION MERGE_ASSORTMENT_CFAS_LOC(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_input_rec       IN      item_loc_assortment_rec)
RETURN BOOLEAN IS
  --
  L_program                 VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.MERGE_ASSORTMENT_CFAS_LOC';
  --
  cursor C_item_loc is
    select im.item,
           il.loc
      from item_master      im,
           item_loc         il
     where (im.item             = i_input_rec.item or
            im.item_parent      = i_input_rec.item or
            im.item_grandparent = i_input_rec.item)
       and il.item              = im.item
       and il.loc               = i_input_rec.hier_value;
  ---
  L_visibility_date date;
  L_availability_date date;
  ---
BEGIN
  ---
  DBG_SQL.MSG(L_program, 'ITEM=' || to_char(I_input_rec.item) || ', LOC=' || I_input_rec.hier_value);
  ---
  for rec in C_item_loc loop
    ---
    DBG_SQL.MSG(L_program, 'Loop: ITEM=' || rec.item || ', LOC=' || rec.loc);
    ---
    if merge_cfa( O_error_message,
                  rec.item,
                  rec.loc,
                  'VARCHAR2',
                  null,
                  i_input_rec.real_availability,
                  GV_CFAGRP_Availability,
                  GV_CFACOL_Availability) = false then
      return false;
    end if;
    ---
    L_availability_date := i_input_rec.effective_date;
    ---
    if i_input_rec.real_availability is  null then
      L_availability_date := null;
    end if;
    ---
    if merge_cfa( O_error_message,
                  rec.item,
                  rec.loc,
                  'DATE',
                  L_availability_date,
                  null,
                  GV_CFAGRP_AvailabilityDate,
                  GV_CFACOL_AvailabilityDate) = false then
      return false;
    end if;
    ---
    if merge_cfa( O_error_message,
                  rec.item,
                  rec.loc,
                  'VARCHAR2',
                  null,
                  i_input_rec.visibility,
                  GV_CFAGRP_Visibility,
                  GV_CFACOL_Visibility) = false then
      return false;
    end if;
    ---
    L_visibility_date := i_input_rec.effective_date;
    ---
    if nvl(i_input_rec.visibility,'C') = 'C' then
      L_visibility_date := null;
    end if;
    ---
    if merge_cfa( O_error_message,
                  rec.item,
                  rec.loc,
                  'DATE',
                  L_visibility_date,
                  null,
                  GV_CFAGRP_VisibilityDate,
                  GV_CFACOL_VisibilityDate) = false then
      return false;
    end if;
    ---
  end loop;
  ---
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END MERGE_ASSORTMENT_CFAS_LOC;
--------------------------------------------------------------------------------
FUNCTION PERMOUT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 I_item            IN      item_loc.item%type,
                 I_loc             IN      item_loc.loc%type,
                 I_loc_type        IN      item_loc.loc_type%type
                 )
RETURN BOOLEAN IS
  --
  L_program                 VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.PERMOUT';
  --
  L_item_loc_rec            ITEM_LOC%ROWTYPE;
  L_item_rec                ITEM_MASTER%ROWTYPE;
  --
BEGIN
  ---
  DBG_SQL.MSG(L_program, 'ITEM=' || to_char(I_item) || ', LOC=' || I_loc);
  ---
  if not ITEM_LOC_SQL.GET_ITEM_LOC(O_error_message,
                                   L_item_loc_rec,
                                   I_item,
                                   I_loc
                                   ) then
    return FALSE;
    --
  end if;
  ---
  DBG_SQL.MSG(L_program, 'ITEM_LOC.STATUS=' || L_item_loc_rec.status);
  ---
  if not ITEM_LOC_SQL.STATUS_CHANGE_VALID(O_error_message,
                                          I_item,
                                          I_loc,
                                          I_loc_type,
                                          L_item_loc_rec.status,
                                          'C'
                                          ) then
    ---
    DBG_SQL.MSG(L_program, 'Invalid status change');
    ---
    return FALSE;
    --
  end if;
  ---
  DBG_SQL.MSG(L_program, 'Valid status change');
  ---
  select status, item_level, tran_level
  into L_item_rec.status, L_item_rec.item_level, L_item_rec.tran_level
  from item_master
  where item = I_item;
  ---
  DBG_SQL.MSG(L_program, 'ITEM.STATUS=' || L_item_rec.status);
  ---
  if not ITEM_LOC_SQL.UPDATE_ITEM_LOC( O_error_message,
                                       I_item,
                                       L_item_rec.status,
                                       L_item_rec.item_level,
                                       L_item_rec.tran_level,
                                       I_loc,
                                       I_loc_type,
                                       L_item_loc_rec.primary_supp,
                                       L_item_loc_rec.primary_cntry,
                                       'C',
                                       L_item_loc_rec.local_item_desc,
                                       L_item_loc_rec.local_short_desc,
                                       L_item_loc_rec.primary_variant,
                                       L_item_loc_rec.unit_retail,
                                       L_item_loc_rec.ti,
                                       L_item_loc_rec.hi,
                                       L_item_loc_rec.store_ord_mult,
                                       L_item_loc_rec.daily_waste_pct,
                                       L_item_loc_rec.taxable_ind,
                                       L_item_loc_rec.meas_of_each,
                                       L_item_loc_rec.meas_of_price,
                                       L_item_loc_rec.uom_of_price,
                                       L_item_loc_rec.selling_unit_retail,
                                       L_item_loc_rec.selling_uom,
                                       L_item_loc_rec.primary_cost_pack,
                                       'Y',
                                       L_item_loc_rec.receive_as_type,
                                       L_item_loc_rec.ranged_ind,
                                       L_item_loc_rec.inbound_handling_days,
                                       L_item_loc_rec.store_price_ind,
                                       L_item_loc_rec.source_method,
                                       L_item_loc_rec.source_wh,
                                       L_item_loc_rec.uin_type,
                                       L_item_loc_rec.uin_label,
                                       L_item_loc_rec.capture_time,
                                       L_item_loc_rec.ext_uin_ind,
                                       L_item_loc_rec.costing_loc,
                                       L_item_loc_rec.costing_loc_type
                                      ) then
    return FALSE;
    --
  end if;
  ---
  DBG_SQL.MSG(L_program, 'ITEM_LOC status changed');
  ---
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END PERMOUT;
--------------------------------------------------------------------------------
FUNCTION RANGE_ITEM_LOC(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item            IN      item_loc.item%type,
                        I_loc             IN      item_loc.loc%type,
                        I_loc_type        IN      item_loc.loc_type%type
                        )
RETURN BOOLEAN IS
  --
  L_program                 VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.RANGE_ITEM_LOC';
  --
  L_input_rec               NEW_ITEM_LOC_SQL.NIL_INPUT_RECORD;
  L_input                   NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;
  L_primary_supp            SUPS.SUPPLIER%TYPE;
  L_primary_cntry           COUNTRY.COUNTRY_ID%TYPE;
  L_status                  VARCHAR2(1) := NULL;
  L_ranged_ind              VARCHAR2(1);
  L_cfa_attrib_tbl          XXADEO_CFA_ATTRIB_TBL  := XXADEO_CFA_ATTRIB_TBL();
  L_xxadeo_cfa_key_val_tbl  XXADEO_CFA_KEY_VAL_TBL := XXADEO_CFA_KEY_VAL_TBL();
  --
BEGIN
  --
  -- get the item's primary supplier and primary country, which will be used as
  -- the new item_loc's primary supplier and primary country.
  --
  DBG_SQL.MSG(L_program, 'ITEM=' || to_char(I_item) || ', LOC=' || I_loc);

  if not ITEM_SUPP_COUNTRY_SQL.GET_PRIM_SUPP_CNTRY(O_error_message,
                                                   L_primary_supp,
                                                   L_primary_cntry,
                                                   I_item) then
    -- ERROR??
    return FALSE;
    --
  end if;
  ---
  DBG_SQL.MSG(L_program, 'PRIM SUP==' || L_primary_supp || ', PRIM SUPP COUNTRY=' || L_primary_cntry);
  ---
  -- Create item_loc
  ---
  L_input_rec.item                    := I_item;
  L_input_rec.item_parent             := NULL;
  L_input_rec.item_grandparent        := NULL;
  L_input_rec.item_desc               := NULL;
  L_input_rec.item_short_desc         := NULL;
  L_input_rec.dept                    := NULL;
  L_input_rec.item_class              := NULL;
  L_input_rec.subclass                := NULL;
  L_input_rec.item_level              := NULL;
  L_input_rec.tran_level              := NULL;
  L_input_rec.item_status             := NULL;
  L_input_rec.waste_type              := NULL;
  L_input_rec.sellable_ind            := NULL;
  L_input_rec.orderable_ind           := NULL;
  L_input_rec.pack_ind                := NULL;
  L_input_rec.pack_type               := NULL;
  L_input_rec.diff_1                  := NULL;
  L_input_rec.diff_2                  := NULL;
  L_input_rec.diff_3                  := NULL;
  L_input_rec.diff_4                  := NULL;
  L_input_rec.loc                     := I_loc;
  L_input_rec.loc_type                := I_loc_type;
  L_input_rec.daily_waste_pct         := NULL;
  L_input_rec.unit_cost_loc           := NULL;
  L_input_rec.unit_retail_loc         := NULL;
  L_input_rec.selling_retail_loc      := NULL;
  L_input_rec.selling_uom             := NULL;
  L_input_rec.multi_units             := NULL;
  L_input_rec.multi_unit_retail       := NULL;
  L_input_rec.multi_selling_uom       := NULL;
  L_input_rec.item_loc_status         := 'A';
  L_input_rec.taxable_ind             := NULL;
  L_input_rec.ti                      := NULL;
  L_input_rec.hi                      := NULL;
  L_input_rec.store_ord_mult          := NULL;
  L_input_rec.meas_of_each            := NULL;
  L_input_rec.meas_of_price           := NULL;
  L_input_rec.uom_of_price            := NULL;
  L_input_rec.primary_variant         := NULL;
  L_input_rec.primary_supp            := L_primary_supp;
  L_input_rec.primary_cntry           := L_primary_cntry;
  L_input_rec.local_item_desc         := NULL;
  L_input_rec.local_short_desc        := NULL;
  L_input_rec.primary_cost_pack       := NULL;
  L_input_rec.receive_as_type         := NULL;
  L_input_rec.store_price_ind         := NULL;
  L_input_rec.uin_type                := NULL;
  L_input_rec.uin_label               := NULL;
  L_input_rec.capture_time            := NULL;
  L_input_rec.ext_uin_ind             := NULL;
  L_input_rec.source_method           := NULL;
  L_input_rec.source_wh               := NULL;
  L_input_rec.inbound_handling_days   := NULL;
  L_input_rec.currency_code           := NULL;
  L_input_rec.like_store              := NULL;
  L_input_rec.default_to_children_ind := 'Y';
  L_input_rec.class_vat_ind           := NULL;
  L_input_rec.hier_level              := I_loc_type;
  L_input_rec.hier_num_value          := I_loc;
  L_input_rec.hier_char_value         := NULL;
  L_input_rec.ranged_ind              := 'N';
  L_input_rec.costing_loc             := NULL;
  L_input_rec.costing_loc_type        := NULL;
  ---
  L_input(0) := L_input_rec;
  ---
  DBG_SQL.MSG(L_program, 'PRE: NEW_ITEM_LOC_SQL.NEW_ITEM_LOC');
  ---
  O_error_message := null;
  ---
  if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message, L_input) = FALSE then
    --
    DBG_SQL.MSG(L_program, 'RANGING Error: ' || O_error_message);
    return FALSE;
    --
  end if;
  ---
  DBG_SQL.MSG(L_program, 'After NEW_ITEM_LOC');
  ---
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END RANGE_ITEM_LOC;
--------------------------------------------------------------------------------
FUNCTION PREPROC_CALC_REAL_AVAILABILITY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_real_availability OUT     varchar2,
                                        I_item              IN      item_master.item%type,
                                        I_loc               IN      ITEM_LOC.LOC%TYPE,
                                        I_effective_date    IN      date,
                                        I_assortment_type   IN      varchar2,
                                        I_record_type       IN      varchar2,
                                        I_availability      IN      varchar2)
RETURN BOOLEAN IS
  --
  L_program           VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.PREPROC_CALC_REAL_AVAILABILITY';
  ---
  L_prev_availability XXADEO_ITEM_LOC_ASSORTMENT.AVAILABILITY%TYPE := null;
  ---
  cursor C_cur_prev_avail(V_type XXADEO_ITEM_LOC_ASSORTMENT.AVAILABILITY%TYPE,
                          V_item ITEM_LOC.ITEM%TYPE,
                          V_loc ITEM_LOC.LOC%TYPE) is
    select availability
      from xxadeo_item_loc_assortment
     where assortment_type = V_type
       and item            = v_item
       and hier_value      = v_loc
       and effective_date <= I_effective_date
       and record_type     = 'C'
     order by effective_date desc;
BEGIN
  --
  DBG_SQL.MSG(L_program, 'ITEM=' || I_item || ', Loc=' || to_char(I_loc));
  --
  if I_record_type = 'D' then
    --
    O_real_availability     := null;
    --
    return TRUE;
    --
  end if;
  ---
  if I_availability = '1' then
    --
    O_real_availability := '1';
    --
    DBG_SQL.MSG(L_program, 'DBG: Avail=1');
    --
  else
    -- fetch L_prev_availability
    if I_assortment_type = 'R' then
      --
      open C_cur_prev_avail('P',
                            I_item,
                            I_loc);
      fetch C_cur_prev_avail INTO L_prev_availability;
      close C_cur_prev_avail;
      --
    else
      --
      open C_cur_prev_avail('R',
                            I_item,
                            I_loc);
      fetch C_cur_prev_avail INTO L_prev_availability;
      close C_cur_prev_avail;
      --
    end if;
    ---
    DBG_SQL.MSG(L_program, 'DBG: PrvAvail=' || L_prev_availability || ', aType=[' || I_assortment_type || ']');
    ---
    if I_availability = '2' then
      --
      if nvl(L_prev_availability,'X') = '1' then
        --
        O_real_availability := L_prev_availability;
        --
      else
        --
        O_real_availability := '2';
        --
      end if;
      --
    else
      --
      if nvl(I_availability,'X') = 'T' then
        --
        if L_prev_availability is null then
          --
          O_real_availability := 'T';
          --
        else
          --
          O_real_availability := L_prev_availability;
          --
        end if;
        --
      else
        --
        O_real_availability := L_prev_availability;
        --
      end if;
      --
    end if;
    --
  end if;
  ---
  DBG_SQL.MSG(L_program, 'RealAv=' || O_real_availability);
  ---
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
     if C_cur_prev_avail%ISOPEN then
       --
       close C_cur_prev_avail;
       --
     end if;
     --
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
     --
     return FALSE;
     --
END PREPROC_CALC_REAL_AVAILABILITY;
--------------------------------------------------------------------------------
FUNCTION CALC_REAL_AVAILABILITY(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_rec            IN OUT  item_loc_assortment_rec)
RETURN BOOLEAN IS
  --
  L_program           VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.CALC_REAL_AVAILABILITY';
  ---
  L_prev_availability XXADEO_ITEM_LOC_ASSORTMENT.AVAILABILITY%TYPE := null;
  ---
  cursor C_cur_prev_avail(V_type XXADEO_ITEM_LOC_ASSORTMENT.AVAILABILITY%TYPE,
                          V_item ITEM_LOC.ITEM%TYPE,
                          V_loc ITEM_LOC.LOC%TYPE) is
    select availability
      from xxadeo_item_loc_assortment
     where assortment_type = V_type
       and item            = v_item
       and hier_value      = v_loc
       and effective_date <= LP_date + 1
       and record_type     = 'C'
     order by effective_date desc;
BEGIN
  --
  if IO_rec.record_type = 'D' then
    --
    IO_rec.real_availability     := null;
    IO_rec.new_availability      := null;
    IO_rec.new_availability_date := IO_rec.effective_date;
    --
    return TRUE;
    --
  end if;
  ---
  if IO_rec.availability = '1' then
    --
    IO_rec.real_availability := '1';
    --
    if GP_trace = true then
      IO_rec.error_message     := 'DBG: Avail=1';
    end if;
    --
  else
    -- fetch L_prev_availability
    if IO_rec.assortment_type = 'R' then
      --
      open C_cur_prev_avail('P',
                            IO_rec.item,
                            IO_rec.hier_value);
      fetch C_cur_prev_avail INTO L_prev_availability;
      close C_cur_prev_avail;
      --
    else
      --
      open C_cur_prev_avail('R',
                            IO_rec.item,
                            IO_rec.hier_value);
      fetch C_cur_prev_avail INTO L_prev_availability;
      close C_cur_prev_avail;
      --
    end if;
    ---
    if GP_trace = true then
      IO_rec.error_message := 'DBG: PrvAvail=' || L_prev_availability || ', aType=[' || IO_rec.assortment_type || ']';
    end if;
    ---
    if IO_rec.availability = '2' then
      --
      if nvl(L_prev_availability,'X') = '1' then
        --
        IO_rec.real_availability := L_prev_availability;
        --
      else
        --
        IO_rec.real_availability := '2';
        --
      end if;
      --
    else
      --
      if nvl(IO_rec.availability,'X') = 'T' then
        --
        if L_prev_availability is null then
          --
          IO_rec.real_availability := 'T';
          --
        else
          --
          IO_rec.real_availability := L_prev_availability;
          --
        end if;
        --
      else
        --
        IO_rec.real_availability := L_prev_availability;
        --
      end if;
      --
    end if;
    --
  end if;
  ---
  if GP_trace = true then
    if GP_trace_what like '%CALC_RA:01%' then
      logger.log_info('RAv=' || IO_rec.real_availability, 'CALC_RA:01');
      logger.log_info('AvItem=' || IO_rec.availability_item_loc, 'CALC_RA:01');
    end if;
  end if;
  ---
  if IO_rec.real_availability is NOT NULL and IO_rec.real_availability <> nvl(IO_rec.availability_item_loc,'X') then
    --
    IO_rec.new_availability      := IO_rec.real_availability;
    IO_rec.new_availability_date := IO_rec.effective_date;
    --
  else
    --
    IO_rec.new_availability      := IO_rec.availability_item_loc;
    IO_rec.new_availability_date := IO_rec.availability_date_item_loc;
    --
  end if;
  --
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
     if C_cur_prev_avail%ISOPEN then
       --
       close C_cur_prev_avail;
       --
     end if;
     --
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
     --
     return FALSE;
     --
END CALC_REAL_AVAILABILITY;
--------------------------------------------------------------------------------
FUNCTION CALC_VISIBILITY(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_rec             IN OUT  item_loc_assortment_rec)
RETURN BOOLEAN IS
  --
  L_program           VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.CALC_VISIBILITY';
  --
BEGIN
  --
  if IO_rec.visibility is not null and IO_rec.visibility <> nvl(IO_rec.visibility_item_loc,'X') then
    --
    IO_rec.new_visibility      := IO_rec.visibility;
    IO_rec.new_visibility_date := IO_rec.effective_date;
    --
    if IO_rec.new_visibility = 'C' then
      IO_rec.new_visibility      := null;
      IO_rec.new_visibility_date := null;
    end  if;
    --
  else
    --
    IO_rec.new_visibility      := IO_rec.visibility_item_loc;
    IO_rec.new_visibility_date := IO_rec.visibility_date_item_loc;
  end if;
  --
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END CALC_VISIBILITY;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SD(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN        NUMBER default null,
                     I_dept             IN        ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN IS
  --
  L_program           VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.VALIDATE_SD';
  --
  L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
  L_uda_value         UDA_VALUES.UDA_VALUE%TYPE;
  L_dlyprg_exists     BOOLEAN := FALSE;

  --
  cursor C_cur_sd is
    select im.item,
           im.status,
           xum_sd_s.bu                as bu_s,
           xum_sd_e.bu                as bu_e,
           uda_s.uda_id               as uda_id_s,
           uda_e.uda_id               as uda_id_e,
           xsd.sales_start_date,
           xsd.sales_end_date,
           area.area,
           uda_v_sdt.uda_date         as sales_start_date_0
      from xxadeo_item_sales_dates xsd,
           item_master             im,
           xxadeo_mom_dvm          xum_sd_s,
           uda                     uda_s,
           xxadeo_mom_dvm          xum_sd_e,
           uda                     uda_e,
           uda_item_date           uda_dt,
           area,
           uda_item_date           uda_v_sdt
     where xsd.ready_for_export  = 'N'
       and xsd.item              = im.item(+)
       and xsd.business_unit_id  = xum_sd_s.bu(+)
       and xum_sd_s.parameter(+) = GP_sales_start_date
       and xum_sd_s.value_1      = uda_s.uda_id(+)
       and xsd.business_unit_id  = xum_sd_e.bu(+)
       and xum_sd_e.parameter(+) = GP_sales_end_date
       and xum_sd_e.value_1      = uda_e.uda_id(+)
       and xsd.business_unit_id  = area.area (+)
       ---
       and uda_v_sdt.item (+)      = xsd.item
       and uda_v_sdt.uda_id (+)    = uda_s.uda_id
       ---
       and (
            im.dept              is null
            or im.dept           = nvl(I_dept,im.dept)
          )
       --and rownum               <= I_bulk_size
       ---
       for update of xsd.error_message, xsd.ready_for_export, xsd.last_update_datetime;
  ---
  subtype TYP_C_cur_sd is C_cur_sd%rowtype;
  L_row TYP_C_cur_sd;
  ---
BEGIN
  --
  DBG_SQL.MSG(L_program, 'Start: LP_date=' || to_char(LP_date,'YYYY-MM-DD'));
  --
  --loop
    --
    for rec_sd IN C_cur_sd loop
      --
      L_error_message := NULL;
      --
      begin
        if  L_error_message is NULL and rec_sd.item is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                 'ITEM',
                                                 L_program,
                                                 NULL);
          --
        end if;
        --
        if  L_error_message is NULL and nvl(rec_sd.status,'X') <> 'A' then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                 'ITEM.STATUS',
                                                 L_program,
                                                 NULL);
          --
        end if;
        --
        if  L_error_message is NULL and rec_sd.area is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'BU not AREA',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if  L_error_message is NULL and rec_sd.bu_s is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'BU/UDA MAP START_DATE',
                                                L_program,
                                                NULL);
          --
        end if;
        ---
        if L_error_message is NULL and rec_sd.bu_e is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'BU/UDA MAP END_DATE',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if L_error_message is NULL and rec_sd.sales_start_date is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'SALES_START_DATE',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if  L_error_message is NULL and rec_sd.sales_end_date is NOT NULL and rec_sd.sales_start_date >= rec_sd.sales_end_date then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_DATES',
                                                'START>=END',
                                                L_program,
                                                NULL);
          --
        end if;
        ---
        if  L_error_message is NULL and rec_sd.sales_end_date is NOT NULL and rec_sd.sales_end_date <= LP_date then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_DATES',
                                                'END<=NOW',
                                                L_program,
                                                NULL);
          --
        end if;
        ---
        -- if sales start in past
        ---
        if  L_error_message is NULL and rec_sd.sales_start_date <= LP_date then
          ---  sales end date null and creating sales start date => error
          if rec_sd.sales_end_date is null and rec_sd.sales_start_date_0 is null then
            L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                  'NEW START<VDATE+END=null',
                                                  L_program,
                                                  NULL);
          end if;
          --
        end if;
        --
        if  L_error_message is NULL and REC_SD.UDA_ID_S is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'UDA_ID (START_DATE)',
                                                L_program,
                                                NULL);
        end if;
        --
        if  L_error_message is NULL and rec_sd.uda_id_e is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'UDA_ID (END_DATE)',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        L_dlyprg_exists := FALSE;
        if L_error_message is null and DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                         L_dlyprg_exists,
                                                         rec_sd.item,
                                                         'ITEM_MASTER') = FALSE then
          return FALSE;
        end if;
        --
        if L_dlyprg_exists = TRUE then
          L_error_message := SQL_LIB.CREATE_MSG('SVC_ITEM_DLY_PRG',
                                                'ITEM',
                                                 L_program,
                                                 NULL);
        end if;
      exception when others then
        L_error_message := SQL_LIB.CREATE_MSG('ROW_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
      end ;
      --
      if L_error_message is NOT NULL then
        --
        update xxadeo_item_sales_dates
           set error_message        = l_error_message,
               ready_for_export     = 'E',
               last_update_datetime = sysdate
         where current of C_cur_sd;
         --
      else
          update xxadeo_item_sales_dates
             set ready_for_export     = 'Y',
                 last_update_datetime = sysdate,
                 error_message = null
           where current of C_cur_sd;
      end if;
      --
    end loop;
    ---
    commit;
    ---
    /*
    open C_cur_sd;
    fetch C_Cur_sd into L_row;
    if C_cur_sd%notfound then
      close C_cur_sd;
      exit;
    else
      close C_cur_sd;
    end if;
    */
  --end loop;
  --
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    ---
    if C_cur_sd%isopen then
      --
      close C_cur_sd;
      --
    end if;
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END VALIDATE_SD;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_AM(O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN        NUMBER default null,
                     I_dept             IN        ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN IS
  --
  L_program           VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.VALIDATE_AM';
  L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
  L_uda_id            UDA.UDA_ID%TYPE;
  L_uda_value         UDA_VALUES.UDA_VALUE%TYPE;
  L_dlyprg_exists     BOOLEAN := FALSE;
  --
  cursor C_cur_am is
    select im.item,
           im.status,
           um_am.bu,
           xam.assortment_mode,
           xam.effective_date,
           xam.record_type,
           um_am.value_1                as uda_id,
           im.item_level,
           im.tran_level,
           area.area
      from xxadeo_item_assortment_mode  xam,
           item_master                  im,
           xxadeo_mom_dvm               um_am,
           area
     where xam.ready_for_export     = 'N'
       and xam.item                 = im.item(+)
       and xam.business_unit_id     = um_am.bu(+)
       and um_am.parameter(+)       = GP_assortment_mode
       and um_am.func_area(+)       = 'UDA'
       and xam.business_unit_id     = area.area (+)
       ---
       and (im.dept                 is null
            or im.dept              = nvl(I_dept,im.dept)
            )
       --and rownum                   <= I_bulk_size
       ---
       for update of xam.error_message, xam.ready_for_export, xam.last_update_datetime;
  --
  cursor C_cur_uda_val(V_uda_id UDA_VALUES.UDA_ID%TYPE, V_uda_value UDA_VALUES.UDA_VALUE%TYPE) is
    select uda.uda_id,
           uda_values.uda_value
      from uda,
           uda_values
     where uda.uda_id               = V_uda_id
       and uda.uda_id               = uda_values.uda_id (+)
       and uda_values.uda_value (+) = V_uda_value;
  ---
  subtype TYP_C_cur_am is C_cur_am%rowtype;
  L_row TYP_C_cur_am;
  ---
BEGIN
  --
  --loop
    --
    L_error_message := NULL;
    --
    for rec_am IN C_cur_am loop
      --
      L_error_message := NULL;
      --
      begin
        if  L_error_message is NULL and rec_am.item is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'ITEM',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if  L_error_message is NULL and nvl(rec_am.status,'-') <> 'A' then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'ITEM.STATUS',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if  L_error_message is NULL and rec_am.area is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'BU not AREA',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if  L_error_message is NULL and rec_am.bu is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'BU/UDA MAP',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if  L_error_message is NULL and rec_am.effective_date <= LP_date then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'EFFECTIVE_DATE',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if  L_error_message is NULL and nvl(rec_am.record_type,'X') not in ('C','D') then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'RECORD_TYPE',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        L_dlyprg_exists := FALSE;
        if L_error_message is null and DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                         L_dlyprg_exists,
                                                         rec_am.item,
                                                         'ITEM_MASTER') = FALSE then
          return FALSE;
        end if;
        --
        if L_dlyprg_exists = TRUE then
          L_error_message := SQL_LIB.CREATE_MSG('SVC_ITEM_DLY_PRG',
                                                'ITEM',
                                                 L_program,
                                                 NULL);
        end if;
        ---
        L_uda_id    := null;
        L_uda_value := null;
        if  L_error_message is NULL then
          --
          open c_cur_uda_val(rec_am.uda_id,
                             rec_am.assortment_mode);
          fetch C_cur_uda_val into L_uda_id, L_uda_value;
          close C_cur_uda_val;
          --
          if L_uda_id is NULL then
            L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                  'BU map to UDA_ID',
                                                  L_program,
                                                  NULL);
          end if;
          --
          if L_uda_id is not NULL and L_uda_value is null then
            L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                  'ASSORTMENT_MODE/UDA_VALUE',
                                                  L_program,
                                                  NULL);
          end if;
          --
        end if;
      exception when others then
        L_error_message := SQL_LIB.CREATE_MSG('ROW_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
      end ;
      --
      if L_error_message is NOT NULL then
        --
        update xxadeo_item_assortment_mode
           set error_message        = L_error_message,
               ready_for_export     = 'E',
               last_update_datetime = sysdate
         where current of C_cur_am;
         --
      else
        --
        update xxadeo_item_assortment_mode
           set ready_for_export     = 'Y',
               last_update_datetime = sysdate
         where current of C_cur_am;
        --
      end if;
      --
    end loop;
    ---
    commit;
    ---
    /*
    open C_cur_am;
    fetch C_Cur_am into L_row;
    if C_cur_am%notfound then
      close C_cur_am;
      exit;
    else
      close C_cur_am;
    end if;
    */
  --end loop;
  --
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    if C_cur_uda_val%ISOPEN then
      --
      close C_cur_uda_val;
      --
    end if;
    ---
    if C_cur_am%isopen then
      --
      close C_cur_am;
      --
    end if;
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END VALIDATE_AM;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_RS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN        NUMBER default null,
                     I_dept             IN        ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN IS
  --
  L_program           VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.VALIDATE_RS';
  L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
  L_uda_value         UDA_VALUES.UDA_VALUE%TYPE;
  L_dlyprg_exists     BOOLEAN := FALSE;
  ---
  cursor C_cur_rs is
    select im.item,
           im.status,
           um_rs.bu,
           xrs.range_size,
           xrs.effective_date,
           xrs.record_type,
           um_rs.value_1                as uda_id,
           im.item_level,
           im.tran_level
      from xxadeo_item_range_size       xrs,
           item_master                  im,
           xxadeo_mom_dvm               um_rs
     where xrs.ready_for_export = 'N'
       and xrs.item             = im.item(+)
       and xrs.business_unit_id = um_rs.bu(+)
       and um_rs.parameter(+)   = GP_range_size
       and um_rs.func_area(+)   = 'UDA'
       ---
       and (
            im.dept                  is null
            or im.dept = nvl(I_dept,im.dept)
          )
       --and rownum                   <= I_bulk_size
       ---
       for update of xrs.error_message, xrs.ready_for_export, xrs.last_update_datetime;
  ---
  cursor C_cur_uda_val(V_uda_id UDA_VALUES.UDA_ID%TYPE,
                       V_uda_value UDA_VALUES.UDA_VALUE%TYPE) is
    select 1
      from uda_values
     where uda_id    = V_uda_id
       and uda_value = V_uda_value;
  ---
  subtype TYP_C_cur_rs is C_cur_rs%rowtype;
  L_row TYP_C_cur_rs;
  ---
BEGIN
  --
  --loop
    for rec_rs IN C_cur_rs loop
      --
      L_error_message := NULL;
      --
      begin
        if L_error_message is NULL and rec_rs.item is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'ITEM',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if L_error_message is NULL and rec_rs.bu is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'BUSINESS_UNIT_ID',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if  L_error_message is NULL and nvl(rec_rs.status,'-') <> 'A' then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'ITEM.STATUS',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if  L_error_message is NULL and nvl(rec_rs.record_type,'X') not in ('C','D') then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'RECORD_TYPE',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if  L_error_message is NULL and rec_rs.effective_date <= LP_date then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                'EFFECTIVE_DATE',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        L_dlyprg_exists := FALSE;
        if L_error_message is null and DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                         L_dlyprg_exists,
                                                         rec_rs.item,
                                                         'ITEM_MASTER') = FALSE then
          return FALSE;
        end if;
        --
        if L_dlyprg_exists = TRUE then
          L_error_message := SQL_LIB.CREATE_MSG('SVC_ITEM_DLY_PRG',
                                                'ITEM',
                                                 L_program,
                                                 NULL);
        end if;
        --
        L_uda_value := null;
        if  L_error_message is NULL then
          --
          open c_cur_uda_val(rec_rs.uda_id,
                             rec_rs.range_size);
          fetch C_cur_uda_val into L_uda_value;
          close C_cur_uda_val;
          --
          if L_uda_value is NULL then
            L_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                                  GP_range_size,
                                                  L_program,
                                                  NULL);
          end if;
        end if;
      exception when others then
        L_error_message := SQL_LIB.CREATE_MSG('ROW_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
      end ;
      --
      if L_error_message is NOT NULL then
        --
        update xxadeo_item_range_size
           set error_message        = L_error_message,
               ready_for_export     = 'E',
               last_update_datetime = sysdate
         where current of C_cur_rs;
        --
      else
        --
        update xxadeo_item_range_size
           set ready_for_export     = 'Y',
               last_update_datetime = sysdate
         where current of C_cur_rs;
        --
      end if;
      --
    end loop;
    ---
    commit;
    ---
    /*
    open C_cur_rs;
    fetch C_Cur_rs into L_row;
    if C_cur_rs%notfound then
      close C_cur_rs;
      exit;
    else
      close C_cur_rs;
    end if;
    */
  --end loop;
  --
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    if C_cur_uda_val%ISOPEN then
      --
      close C_cur_uda_val;
      --
    end if;
    --
    if C_cur_rs%ISOPEN then
      --
      close C_cur_rs;
      --
    end if;
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END VALIDATE_RS;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_LOC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bulk_size        IN        NUMBER default null,
                     I_dept             IN        ITEM_MASTER.DEPT%TYPE default null)
RETURN BOOLEAN IS
  --
  L_program         VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.VALIDATE_LOC';
  --
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  --
  cursor C_loc is
    select im.item,
           im.status,
           nvl(xloc.hier_level,'S') as hier_level,
           xloc.hier_value,
           xloc.assortment_type,
           xloc.record_type,
           xloc.availability,
           xloc.visibility,
           xloc.effective_date,
           xloc.rms_status,
           xloc.ready_for_export,
           s.store
     from xxadeo_item_loc_assortment xloc,
          item_master                im,
          store                      s
    where xloc.ready_for_export     in ('N','Y')
      and xloc.item                 = im.item (+)
      and xloc.hier_value           = s.store (+)
       ---
       and (im.dept                 is  null
            or im.dept              = nvl(I_dept,im.dept)
            )
       --and rownum                   <= I_bulk_size
       ---
    order by      effective_date
    for update of xloc.error_message,
                  xloc.ready_for_export,
                  xloc.last_update_datetime,
                  xloc.real_availability;
  ---
  subtype TYP_C_loc is C_loc%rowtype;
  L_row TYP_C_loc;
  ---
  L_real_availability   xxadeo_item_loc_assortment.real_availability%type;
  ---
  L_primary_supp            SUPS.SUPPLIER%TYPE;
  L_primary_cntry           COUNTRY.COUNTRY_ID%TYPE;
  ---
BEGIN
  ---
  DBG_SQL.MSG(L_program, 'Start LP_date=' || to_char(LP_date,'YYYY-MM-DD'));
  ---
  --loop
    --
    L_error_message := NULL;
    --
    DBG_SQL.MSG(L_program, 'Inner loop start...');
    ---
    for r_loc in C_loc loop
      --
      DBG_SQL.MSG(L_program, 'For loop: ITEM=' || r_loc.item || ', LOC=' || r_loc.store || ', EFFECTIVE_DATE=' || to_char(r_loc.effective_date,'YYYY-MM-DD HH24:MI:SS'));
      L_error_message := NULL;
      --
      begin
        --
        L_real_availability := null;
        --
        if L_error_message is NULL and r_loc.item is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('ITEM_NOT_FOUND',
                                                'ITEM',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if L_error_message is null and nvl(r_loc.status,'X') <> 'A' then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM_STATUS',
                                                'ITEM_MASTER.STATUS',
                                                L_program,
                                                NULL);
        end if;
        ---
        -- TODO: Check requirements => ONLY stores?????? 3.6.6.2 says YES, Item-Location Entity maybe!?!?
        ---
        if L_error_message is NULL and r_loc.store is NULL then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_VALUE',
                                                'HIER_VALUE',
                                                L_program,
                                                NULL);
          --
        end if;
        ---
        if L_error_message is NULL and r_loc.rms_status not in ('A','C') then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_VALUE',
                                                'RMS_STATUS',
                                                L_program,
                                                NULL);
          --
        end if;
        ---
        if L_error_message is NULL and r_loc.assortment_type not in ('R','P') then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_VALUE',
                                                'ASSORTMENT_TYPE',
                                                L_program,
                                                NULL);
          --
        end if;
        ---
        if L_error_message is NULL and r_loc.hier_level not in ('CH','AR','RE','DI','S','W') then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_HIER_LEVEL',
                                                'HIER_LEVEL',
                                                L_program,
                                                NULL);
          --
        end if;
        --
        if L_error_message is NULL and nvl(r_loc.visibility,'X') not in ('X','P','V','T','C') then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_VALUE',
                                                'VISIBILITY',
                                                L_program,
                                                NULL);
        end if;
        --
        if L_error_message is NULL and nvl(r_loc.availability,'X') not in ('X','1','2','T','C') then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_VALUE',
                                                'AVAILABILITY',
                                                L_program,
                                                NULL);
        end if;
        --
        if L_error_message is NULL and nvl(r_loc.record_type,'X') not in ('X','C','D') then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_VALUE',
                                                'RECORD_TYPE',
                                                L_program,
                                                NULL);
          --
        end if;
        ---
        -- CHECK => EFFECTIVE_DATE must be in future, but if we are recovering from prior failures!?
        ---
        if L_error_message is NULL and r_loc.effective_date <= LP_date then
          --
          L_error_message := SQL_LIB.CREATE_MSG('INVALID_VALUE',
                                                'EFFECTIVE_DATE',
                                                L_program,
                                                NULL);
          --
        end if;

        ---
        -- TODO: CHECK => ITEM tran_level????
        ---


        ---
        -- Calc real availability
        ---
        if L_error_message is NULL and not preproc_calc_real_availability(L_error_message,
                                                                          L_real_availability,
                                                                          r_loc.item,
                                                                          r_loc.hier_value,
                                                                          r_loc.effective_date,
                                                                          r_loc.assortment_type,
                                                                          r_loc.record_type,
                                                                          r_loc.availability)
        then
          --
          L_error_message := SQL_LIB.CREATE_MSG('CALC_REAL_AVAILABILITY_ERROR',
                                                L_error_message,
                                                L_program,
                                                NULL);
          --
        end if;

      exception when others then
        L_error_message := SQL_LIB.CREATE_MSG('ROW_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
      end ;
      ---
      if Gb_range_in_preproc then
        if r_loc.ready_for_export = 'N' and L_error_message is null and not XXADEO_ITEM_LOC_WRP( L_error_message,
                                                           r_loc.item,
                                                           r_loc.hier_value,
                                                           r_loc.hier_level
                                                           ) then
          ---
          L_error_message := SQL_LIB.CREATE_MSG('RANGING_ERROR',
                                                L_error_message,
                                                L_program,
                                                TO_CHAR(SQLCODE));
          ---
        end if;
        ---
        DBG_SQL.MSG(L_program, 'POS: RANGING => L_ERR_MSG=' || nvl(L_error_message,'*null*'));
        ---
      else
        DBG_SQL.MSG(L_program, 'NOT ranging in PREPROC');
      end if;
      ---
      if L_error_message is NOT NULL then
        --
        update xxadeo_item_loc_assortment
           set error_message    = l_error_message,
               ready_for_export = 'E',
               last_update_datetime = sysdate
         where current of C_loc;
        --
      else
        --
        update xxadeo_item_loc_assortment
           set ready_for_export       = 'Y'
               ,error_message         = null
               ,last_update_datetime  = sysdate
               ,real_availability     = L_real_availability
         where current of C_loc;
        --
      end if;
      --
    end loop;
    ---
    --commit;
    ---
    /*
    open C_loc;
    fetch C_loc into L_row;
    if C_loc%notfound then
      close C_loc;
      exit;
    else
      close C_loc;
    end if;
    */
  --end loop;
  ---
  return TRUE;
  --
EXCEPTION
  ---
  when OTHERS then
    --
    if C_loc%isopen then
      close C_loc;
    end if;
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END VALIDATE_LOC;
--------------------------------------------------------------------------------
FUNCTION PURGE( O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                I_date              IN      DATE DEFAULT get_vdate,
                I_purge_errors      IN      VARCHAR2 DEFAULT 'N',
                I_retention_days    IN      NUMBER DEFAULT 7)
RETURN BOOLEAN IS
  --
  L_program         VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.PURGE';
  --
BEGIN
  --
  if I_retention_days < 1 then
    --
    O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARAM_VALUE',
                                          'I_RETENTION_DAYS',
                                          L_program,
                                          NULL);
    return FALSE;
    --
  end if;
  --
  delete from xxadeo_item_sales_dates
   where last_update_datetime < I_date - I_retention_days
     and ((ready_for_export in ('E', 'S') and I_purge_errors = 'Y') or
         (ready_for_export in ('S')));
  --
  delete from xxadeo_item_assortment_mode
   where last_update_datetime < I_date - I_retention_days
     and ((ready_for_export in ('E', 'S') and I_purge_errors = 'Y') or
         (ready_for_export in ('S')));
  ---
  delete from xxadeo_item_range_size
   where last_update_datetime < I_date - I_retention_days
     and ((ready_for_export in ('E', 'S') and I_purge_errors = 'Y') or
         (ready_for_export in ('S')));
  ---
  delete from xxadeo_item_loc_assortment d1
   where effective_date < I_date - I_retention_days
     and ((ready_for_export in ('E', 'S') and I_purge_errors = 'Y') or
         (ready_for_export in ('S')))
     and (d1.record_type = 'D'
          or ready_for_export = 'E'
          or exists
          (select 1
             from xxadeo_item_loc_assortment s1
            where s1.item             = d1.item
              and s1.hier_value       = d1.hier_value
              and s1.effective_date   > d1.effective_date
              and s1.assortment_type  = d1.assortment_type
              and s1.effective_date  <= i_date
              and s1.ready_for_export = 'S'
              and d1.ready_for_export = 'S'
              and s1.record_type      = d1.record_type));
  --
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    return FALSE;
    --
END PURGE;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION FILL_CFA(O_error_message IN OUT  VARCHAR2,
                  O_col           OUT     cfa_attrib.view_col_name%TYPE,
                  O_grp           OUT     cfa_attrib.group_id%TYPE,
                  I_parameter     IN      xxadeo_mom_dvm.parameter%TYPE,
                  I_prefix        IN      varchar2)
RETURN BOOLEAN is
  ---
  L_program   VARCHAR2(200) := 'XXADEO_ITEM_ASSORTMENT_SQL.FILL_CFA:' || I_parameter;
  ---
BEGIN
  ---
  -- group by => must have same column name for all BU's
  -- Exception is raised when that is not true
  ---
  select storage_col_name
         ,group_id
    into O_col
          ,O_grp
    from cfa_attrib cfaa
         ,xxadeo_mom_dvm xmd
   where xmd.parameter = I_parameter
     and xmd.value_1 = to_char(cfaa.attrib_id)
   group by storage_col_name, group_id
  ;
  ---
  if O_col not like I_prefix || '%' then
    O_error_message := 'Wrong column type!?';
    return false;
  end if;
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    ---
    return FALSE;
END;
--------------------------------------------------------------------------------
FUNCTION initialize_globals(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN
is
  ---
  L_program       VARCHAR2(100) := 'XXADEO_ITEM_ASSORTMENT_SQL.INITIALIZE_GLOBALS';
  ---
  L_error_message VARCHAR2(1000);
  ---
BEGIN
  if GV_globals_initialized = TRUE then
    return TRUE;
  end if;
  ---
  if FILL_CFA(L_error_message,
              GV_CFACOL_Availability,
              GV_CFAGRP_Availability,
              GV_CFAKEY_Availability,
              'VARCHAR2') = FALSE then
    return false;
  end if;
  ---
  if FILL_CFA(L_error_message,
              GV_CFACOL_AvailabilityDate,
              GV_CFAGRP_AvailabilityDate,
              GV_CFAKEY_AvailabilityDate,
              'DATE') = FALSE then
    return false;
  end if;
  ---
  if FILL_CFA(L_error_message,
              GV_CFACOL_Visibility,
              GV_CFAGRP_Visibility,
              GV_CFAKEY_Visibility,
              'VARCHAR2') = FALSE then
    return false;
  end if;
  ---
  if FILL_CFA(L_error_message,
              GV_CFACOL_VisibilityDate,
              GV_CFAGRP_VisibilityDate,
              GV_CFAKEY_VisibilityDate,
              'DATE') = FALSE then
    return false;
  end if;
  ---
  if not GV_CFAGRP_VisibilityDate = GV_CFAGRP_Visibility
       and GV_CFAGRP_VisibilityDate = GV_CFAGRP_AvailabilityDate
       and GV_CFAGRP_VisibilityDate = GV_CFAGRP_Availability
  then
    O_error_message := 'CFA group must be the same for Visibility(+Date) and Availability(+Date) attributes';
    return false;
  end if;
  ---
  declare
    O_tbl DBG_SQL.TBL_DBG_OBJ;
  begin
    ---
    DBG_SQL.INIT(O_tbl);
    ---
  exception when others
    then null;
  end;

  ---
  GV_globals_initialized := TRUE;
  ---
  return TRUE;
  ---
EXCEPTION
  when OTHERS then
    ---
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    ---
    return FALSE;
end initialize_globals;


END XXADEO_ITEM_ASSORTMENT_SQL;
/
