CREATE OR REPLACE PACKAGE XXADEO_MERCHHIER_TRANS_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/*               Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package "XXADEO_MERCHHIER_TRANS_SQL"                         */
/******************************************************************************/
--------------------------------------------------------------------------------

  template_key                      CONSTANT VARCHAR2(255):='XXADEO_MERCHANDISE_HIERARCHY_TRANSLATIONS';
  action_new                        VARCHAR2(25)          :='NEW';
  action_mod                        VARCHAR2(25)          :='MOD';
  action_del                        VARCHAR2(25)          :='DEL';

  SHEET_NAME_TRANS                  S9T_PKG.TRANS_MAP_TYP;

  GP_upload_create                  VARCHAR2(6)           := 'CREATE';
  GP_upload_update                  VARCHAR2(6)           := 'UPDATE';
  GP_CODE_TYPE_LANG                 VARCHAR2(4)           := 'ADLG';
  
  TABLE$LANG_TL                     VARCHAR2(255)         := 'LANG_TL';
  TABLE$DEPS_TL                     VARCHAR2(255)         := 'DEPS_TL';
  TABLE$CLASS_TL                    VARCHAR2(255)         := 'CLASS_TL';
  TABLE$SUBCLASS_TL                 VARCHAR2(255)         := 'SUBCLASS_TL';

  LANG$LANG                         VARCHAR2(255)         := 'LANG';
  PARAMETER$LIMIT_LENGTH            NUMBER                := 120;

  DEPARTMENT_TRANS_SHEET            VARCHAR2(255)         := 'DEPARTMENT_TRANSLATIONS';
  DEPARTMENT_TRANS$ACTION           VARCHAR2(255)         := 'ACTION';
  DEPARTMENT_TRANS$LANG             VARCHAR2(255)         := 'LANGUAGE';
  DEPARTMENT_TRANS$GROUP_ID         VARCHAR2(255)         := 'GROUP_ID';
  DEPARTMENT_TRANS$GROUP_NAME       VARCHAR2(255)         := 'GROUP_NAME';
  DEPARTMENT_TRANS$DEPT             VARCHAR2(255)         := 'DEPT';
  DEPARTMENT_TRANS$DEPT_NAME        VARCHAR2(255)         := 'DEPT_NAME';
  DEPARTMENT_TRANS$DEPT_TRANS       VARCHAR2(255)         := 'DEPT_TRANSLATION';

  CLASS_TRANS_SHEET                 VARCHAR2(255)         := 'CLASS_TRANSLATIONS';
  CLASS_TRANS$ACTION                VARCHAR2(255)         := 'ACTION';
  CLASS_TRANS$LANG                  VARCHAR2(255)         := 'LANGUAGE';
  CLASS_TRANS$GROUP_ID              VARCHAR2(255)         := 'GROUP_ID';
  CLASS_TRANS$GROUP_NAME            VARCHAR2(255)         := 'GROUP_NAME';
  CLASS_TRANS$DEPT                  VARCHAR2(255)         := 'DEPT';
  CLASS_TRANS$DEPT_NAME             VARCHAR2(255)         := 'DEPT_NAME';
  CLASS_TRANS$CLASS                 VARCHAR2(255)         := 'CLASS';
  CLASS_TRANS$CLASS_NAME            VARCHAR2(255)         := 'CLASS_NAME';
  CLASS_TRANS$CLASS_TRANS           VARCHAR2(255)         := 'CLASS_TRANSLATION';

  SUBCLASS_TRANS_SHEET              VARCHAR2(255)         := 'SUBCLASS_TRANSLATIONS';
  SUBCLASS_TRANS$ACTION             VARCHAR2(255)         := 'ACTION';
  SUBCLASS_TRANS$LANG               VARCHAR2(255)         := 'LANGUAGE';
  SUBCLASS_TRANS$GROUP_ID           VARCHAR2(255)         := 'GROUP_ID';
  SUBCLASS_TRANS$GROUP_NAME         VARCHAR2(255)         := 'GROUP_NAME';
  SUBCLASS_TRANS$DEPT               VARCHAR2(255)         := 'DEPT';
  SUBCLASS_TRANS$DEPT_NAME          VARCHAR2(255)         := 'DEPT_NAME';
  SUBCLASS_TRANS$CLASS              VARCHAR2(255)         := 'CLASS';
  SUBCLASS_TRANS$CLASS_NAME         VARCHAR2(255)         := 'CLASS_NAME';
  SUBCLASS_TRANS$SUBCLASS           VARCHAR2(255)         := 'SUBCLASS';
  SUBCLASS_TRANS$SUBCLASS_NAME      VARCHAR2(255)         := 'SUBCLASS_NAME';
  SUBCLASS_TRANS$SUBCLASS_TRANS     VARCHAR2(255)         := 'SUBCLASS_TRANSLATION';

  action_column                     VARCHAR2(255)         :='ACTION';
  template_category                 CODE_DETAIL.CODE%TYPE :='RMSADM';
-------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind IN       CHAR DEFAULT 'N')
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
 FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
 RETURN BOOLEAN;
-------------------------------------------------------------------------------
END XXADEO_MERCHHIER_TRANS_SQL;
/
