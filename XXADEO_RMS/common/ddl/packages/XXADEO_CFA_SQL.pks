CREATE OR REPLACE PACKAGE XXADEO_CFA_SQL IS
    
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - MARCH 2018                                                   */
/* CREATE USER - Gustavo Martins                                              */
/* PROJECT     - Dev Center (adjust logs as needed)                           */
/* DESCRIPTION - CFA API for CFA Attrib IDs configued on RMS Code Detail      */
/******************************************************************************/
--------------------------------------------------------------------------------

/******************************************************************************/
/* Get CFA value for attribute id passed by value or by code detail/code      */
/*    - I_xxadeo_cfa_key_val_obj should have the search key column            */
/*      from cfa_ext_entity_key and the value to search                       */
/******************************************************************************/
--------------------------------------------------------------------------------
FUNCTION GET_CFA_BY_ATTRIB(O_error_message                OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_xxadeo_cfa_search_obj        OUT XXADEO_CFA_SEARCH_OBJ,
                           I_attrib_id                    IN  CFA_ATTRIB.ATTRIB_ID%TYPE DEFAULT NULL,
                           I_code_type                    IN  CODE_DETAIL.CODE_TYPE%TYPE DEFAULT NULL,
                           I_code                         IN  CODE_DETAIL.CODE%TYPE DEFAULT NULL,
                           I_xxadeo_cfa_key_val_obj       IN  XXADEO_CFA_KEY_VAL_OBJ)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/******************************************************************************/
/* Bulk Set CFA value                                                         */
/*    - Each IO_cfa_attrib_tbl entry must have the attribute id passed by     */
/*      value or by code detail/code                                          */
/*    - Each entry of I_xxadeo_cfa_key_val_tbl must have the search key           */
/*      column from cfa_ext_entity_key and the value to search                */
/******************************************************************************/
--------------------------------------------------------------------------------
FUNCTION SET_CFA_BY_ATTRIB(O_error_message      OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_cfa_attrib_tbl IN OUT XXADEO_CFA_ATTRIB_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
/******************************************************************************/
/* Queriable method to get cfa values passing the attribute id passed by      */
/*  value or by code detail/code                                              */
/*   - (OPTIONAL) Each entry of I_xxadeo_cfa_key_val_tbl must have the        */
/*     search key column from cfa_ext_entity_key and the value to search      */
/******************************************************************************/
--------------------------------------------------------------------------------
FUNCTION SEARCH_CFA_BY_ATTRIB(I_attrib_id                IN  CFA_ATTRIB.ATTRIB_ID%TYPE DEFAULT NULL,
                              I_code_type                IN  CODE_DETAIL.CODE_TYPE%TYPE DEFAULT NULL,
                              I_code                     IN  CODE_DETAIL.CODE%TYPE DEFAULT NULL,
                              I_xxadeo_cfa_key_val_tbl   IN  XXADEO_CFA_KEY_VAL_TBL DEFAULT NULL)
RETURN XXADEO_CFA_SEARCH_TBL;
---------------------------------------------------------------------------------------


END XXADEO_CFA_SQL;
/
