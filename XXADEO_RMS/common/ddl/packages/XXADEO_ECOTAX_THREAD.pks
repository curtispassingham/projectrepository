--------------------------------------------------------
--  DDL for Package XXADEO_ECOTAX_THREAD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "XXADEO_RMS"."XXADEO_ECOTAX_THREAD" AS 

FUNCTION THREAD_BALANCE(O_error_message     IN OUT      VARCHAR2,
                        I_table_name        IN          VARCHAR2,
                        I_program_name      IN          VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------------------------------------------

PROCEDURE POPULATE(o_status                  OUT         NUMBER,
                  o_message                 OUT         VARCHAR2,
                  I_dept                    IN          VARCHAR2);
                  
--------------------------------------------------------------------------------------------------------------------------------

END XXADEO_ECOTAX_THREAD;

/
