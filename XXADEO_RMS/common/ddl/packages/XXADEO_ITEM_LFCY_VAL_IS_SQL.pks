CREATE OR REPLACE PACKAGE XXADEO_ITEM_LFCY_VAL_IS_SQL AUTHID CURRENT_USER AS

/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Jorge Agra                                                   */
/*                                                                            */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package for RB107/RB118                                      */
/*               Validations for ITEM/SUPPLIER relation                       */
/******************************************************************************/

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-08-20 - Jorge Agra - Removal of IS-ACT-08 code                        */
/* 2018-09-20 - Jorge Agra - BUG#438 IS-ACT-06 check item group and not dept  */
/*                           BUG#440 IS-ACT-04 check ITEM_SUPP_COUNTRY_DIM    */
/*                                   for at least a row with DIM_OBJECT = CA  */
/*                                   and not supp_pack_size > 0 on            */
/*                                   item_supp_country                        */
/* 2018-10-02 - Jorge Agra - include validation of next_val_dt in all rules   */
/* 2018-11-05 - Jorge Agra - Rule IS-INIT-01 to control STOP=>INIT change     */
/******************************************************************************/

/******************************************************************************/
/* Activation (IS-INIT-01)	                                                  */
/* Should be applied on STOP status to change to INIT status if LINK_TERM_DT  */
/* is null or in the future                                                   */
/******************************************************************************/
FUNCTION IS_INIT_01(O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/*  Activation (IS-ACT-01)	                                                  */
/* The supplier must be the Active Purchase Site (commercial site)            */
/******************************************************************************/
FUNCTION IS_ACT_01(O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* IS-ACT-02 	                                                                */
/******************************************************************************/
FUNCTION IS_ACT_02(O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* IS-ACT-03 	                                                                */
/******************************************************************************/
FUNCTION IS_ACT_03(O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* IS-ACT-04 	                                                                */
/******************************************************************************/
FUNCTION IS_ACT_04(O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* IS-ACT-05 	                                                                */
/******************************************************************************/
FUNCTION IS_ACT_05(O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* IS-ACT-06 	                                                                */
/******************************************************************************/
FUNCTION IS_ACT_06(O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* IS-ACT-07 	                                                                */
/******************************************************************************/
FUNCTION IS_ACT_07( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* IS-ACT-08 	                                                                */
/******************************************************************************/
FUNCTION IS_ACT_08( O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                    O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                    I_pid             IN       number,
                    I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                    I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;
/******************************************************************************/
/* IS-STOP-01	                                                                */
/******************************************************************************/
FUNCTION IS_STOP_01(O_error_message   IN OUT  varchar2,
                    O_rule_changes    OUT      number,
                     O_result          OUT      xxadeo_lfcy_item_cond.val_status%TYPE,
                     I_pid             IN       number,
                     I_param1          IN       xxadeo_system_rules_detail.value_1%TYPE,
                     I_param2          IN       xxadeo_system_rules_detail.value_2%TYPE)
RETURN BOOLEAN;

END XXADEO_ITEM_LFCY_VAL_IS_SQL;


/


