CREATE OR REPLACE PACKAGE BODY XXADEO_RMSMFM_ITEMS_BUILD AS
/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_RMSMFM_ITEMS_BUILD.pkb
* Description:   This package contains tools that help 
*				 in the implementation of the publisher 
*				 package XXADEO_RMSMFM_ITEMS.
*
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/

  TYPE item_info is RECORD(
    dept_desc                 DEPS.DEPT_NAME%TYPE,
    class_desc                CLASS.CLASS_NAME%TYPE,
    subclass_desc             SUBCLASS.SUB_NAME%TYPE,
    handling_temp_desc        CODE_DETAIL.CODE_DESC%TYPE,
    handling_sensitivity_desc CODE_DETAIL.CODE_DESC%TYPE,
    waste_type_desc           CODE_DETAIL.CODE_DESC%TYPE,
    retail_label_desc         CODE_DETAIL.CODE_DESC%TYPE,
    diff_1_type               DIFF_IDS.DIFF_TYPE%TYPE,
    diff_2_type               DIFF_IDS.DIFF_TYPE%TYPE,
    diff_3_type               DIFF_IDS.DIFF_TYPE%TYPE,
    diff_4_type               DIFF_IDS.DIFF_TYPE%TYPE,
    unit_retail               ITEM_LOC.UNIT_RETAIL%TYPE);

  
  FUNCTION BUILD_HEADER_OBJECT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message       IN OUT NOCOPY "RIB_ItemHdrDesc_REC",
                               O_record_exists IN OUT BOOLEAN,
                               O_orderable_ind IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                               I_item          IN ITEM_MFQUEUE.ITEM%TYPE)
    RETURN BOOLEAN;

  FUNCTION BUILD_IMAGE_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_message       IN OUT NOCOPY "RIB_ItemImageDesc_TBL",
                              O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                              O_record_exists IN OUT BOOLEAN,
                              I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                              I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;

  FUNCTION BUILD_TICKET_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message       IN OUT NOCOPY "RIB_ItemTcktDesc_TBL",
                               O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                               O_record_exists IN OUT BOOLEAN,
                               I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                               I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                               I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
 
  FUNCTION BUILD_SUPPLIER_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_message       IN OUT NOCOPY "RIB_ItemSupDesc_TBL",
                                 O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                 O_record_exists IN OUT BOOLEAN,
                                 I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                                 I_orderable_ind IN ITEM_MASTER.ORDERABLE_IND%TYPE,
                                 I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                 I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
 
  FUNCTION BUILD_COUNTRY_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_message       IN OUT NOCOPY "RIB_ItemSupCtyDesc_TBL",
                                O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                O_record_exists IN OUT BOOLEAN,
                                I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                                I_orderable_ind IN ITEM_MASTER.ORDERABLE_IND%TYPE,
                                I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
  
  FUNCTION BUILD_DIM_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_message       IN OUT NOCOPY "RIB_ISCDimDesc_TBL",
                            O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                            O_record_exists IN OUT BOOLEAN,
                            I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                            I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                            I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
 
  FUNCTION GET_ITEM_INFO(O_error_message   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_info_rec   OUT ITEM_INFO,
                         I_item_master_rec IN ITEM_MASTER%ROWTYPE)
    RETURN BOOLEAN;
  
  FUNCTION GET_DIMENSION_DESCRIPTIONS(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_dim_object_desc     OUT CODE_DETAIL.CODE_DESC%TYPE,
                                      O_pres_method_desc    OUT CODE_DETAIL.CODE_DESC%TYPE,
                                      I_dimension_object    IN ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE,
                                      I_presentation_method IN ITEM_SUPP_COUNTRY_DIM.PRESENTATION_METHOD%TYPE)
    RETURN BOOLEAN;
  
  FUNCTION BUILD_BOM_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_message       IN OUT NOCOPY "RIB_ItemBOMDesc_TBL",
                            O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                            O_record_exists IN OUT BOOLEAN,
                            I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                            I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                            I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
  
  FUNCTION BUILD_UPC_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_message       IN OUT NOCOPY "RIB_ItemUPCDesc_TBL",
                            O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                            O_record_exists IN OUT BOOLEAN,
                            I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                            I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                            I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
  
  FUNCTION BUILD_UDA_LOV_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_message       IN OUT NOCOPY "RIB_ItemUDALOVDesc_TBL",
                                O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                O_record_exists IN OUT BOOLEAN,
                                I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                                I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
  
  FUNCTION BUILD_UDA_FF_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message       IN OUT NOCOPY "RIB_ItemUDAFFDesc_TBL",
                               O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                               O_record_exists IN OUT BOOLEAN,
                               I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                               I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                               I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
 
  FUNCTION BUILD_UDA_DATE_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_message       IN OUT NOCOPY "RIB_ItemUDADateDesc_TBL",
                                 O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                 O_record_exists IN OUT BOOLEAN,
                                 I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                                 I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                 I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
  
  FUNCTION BUILD_MANU_COUNTRY_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_message       IN OUT NOCOPY "RIB_ItemSupCtyMfrDesc_TBL",
                                     O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                     O_record_exists IN OUT BOOLEAN,
                                     I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                                     I_orderable_ind IN ITEM_MASTER.ORDERABLE_IND%TYPE,
                                     I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                     I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
  
  FUNCTION BUILD_RELATED_ITEMS_HEAD(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_message       IN OUT NOCOPY "RIB_RelatedItemDesc_TBL",
                                    O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                    O_record_exists IN OUT BOOLEAN,
                                    I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                                    I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                    I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
 
  FUNCTION BUILD_RELATED_ITEMS_DETAIL(O_error_msg       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_message         IN OUT NOCOPY "RIB_RelatedItemDtl_TBL",
                                      O_rowids_rec      IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                      O_record_exists   IN OUT BOOLEAN,
                                      I_item            IN ITEM_MFQUEUE.ITEM%TYPE,
                                      I_relationship_id IN ITEM_MFQUEUE.RELATIONSHIP_ID%TYPE,
                                      I_message_type    IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                      I_seq_no          IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN;
 
  -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_MESSAGE
  -- Description: This function is responsible for building 
  --              detail level DESC Oracle Objects
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_MESSAGE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_message        IN OUT NOCOPY RIB_OBJECT,
                         O_rowids_rec     IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                         O_record_exists  IN OUT BOOLEAN,
                         I_message_type   IN VARCHAR2,
                         I_tran_level_ind IN ITEM_PUB_INFO.TRAN_LEVEL_IND%TYPE,
                         I_queue_rec      IN ITEM_MFQUEUE%ROWTYPE)
    RETURN BOOLEAN IS
  
    L_module        VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_MESSAGE';
    L_orderable_ind ITEM_MASTER.ORDERABLE_IND%TYPE := NULL;
  
    L_itemhdrdesc_rec       "RIB_ItemHdrDesc_REC" := NULL;
    L_itemsupdesc_tbl       "RIB_ItemSupDesc_TBL" := NULL;
    L_itemsupctydesc_tbl    "RIB_ItemSupCtyDesc_TBL" := NULL;
    L_iscdimdesc_tbl        "RIB_ISCDimDesc_TBL" := NULL;
    L_itemudalovdesc_tbl    "RIB_ItemUDALOVDesc_TBL" := NULL;
    L_itemudaffdesc_tbl     "RIB_ItemUDAFFDesc_TBL" := NULL;
    L_itemudadatedesc_tbl   "RIB_ItemUDADateDesc_TBL" := NULL;
    L_itemimagedesc_tbl     "RIB_ItemImageDesc_TBL" := NULL;
    L_itemupcdesc_tbl       "RIB_ItemUPCDesc_TBL" := NULL;
    L_itembomdesc_tbl       "RIB_ItemBOMDesc_TBL" := NULL;
    L_itemtcktdesc_tbl      "RIB_ItemTcktDesc_TBL" := NULL;
    L_itemsupctymfrDesc_tbl "RIB_ItemSupCtyMfrDesc_TBL" := NULL;
    L_relateditemdesc_rec   "RIB_RelatedItemDesc_REC" := NULL;
    L_relateditemdesc_tbl   "RIB_RelatedItemDesc_TBL" := "RIB_RelatedItemDesc_TBL"();
    L_relateditemdesc1_tbl  "RIB_RelatedItemDesc_TBL" := "RIB_RelatedItemDesc_TBL"();
  
  BEGIN
    --- initialize O_record_exists to TRUE
    O_record_exists := TRUE;
  
    if I_message_type in
       (XXADEO_RMSMFM_ITEMS.ITEM_ADD, XXADEO_RMSMFM_ITEMS.ITEM_UPD) then
      if BUILD_HEADER_OBJECT(O_error_message,
                             L_itemhdrdesc_rec,
                             O_record_exists,
                             L_orderable_ind,
                             I_queue_rec.item) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_itemhdrdesc_rec;
      elsif not O_record_exists then
        return TRUE;
      end if;
    end if;
  
    if I_message_type in
       (XXADEO_RMSMFM_ITEMS.ISUP_ADD, XXADEO_RMSMFM_ITEMS.ISUP_UPD) or
       (I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD and
       I_tran_level_ind = 'Y') then
    
      if BUILD_SUPPLIER_DETAIL(O_error_message,
                               L_itemsupdesc_tbl,
                               O_rowids_rec,
                               O_record_exists,
                               I_queue_rec.item,
                               L_orderable_ind,
                               I_message_type,
                               I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_itemsupdesc_tbl(1);
      end if;
    
    end if;
  
    if I_message_type in
       (XXADEO_RMSMFM_ITEMS.ISC_ADD, XXADEO_RMSMFM_ITEMS.ISC_UPD) or
       (I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD and
       I_tran_level_ind = 'Y') then
    
      if BUILD_COUNTRY_DETAIL(O_error_message,
                              L_itemsupctydesc_tbl,
                              O_rowids_rec,
                              O_record_exists,
                              I_queue_rec.item,
                              L_orderable_ind,
                              I_message_type,
                              I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_itemsupctydesc_tbl(1);
      end if;
    
    end if;
  
    if I_message_type in
       (XXADEO_RMSMFM_ITEMS.ISMC_ADD, XXADEO_RMSMFM_ITEMS.ISMC_UPD) or
       (I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD and
       I_tran_level_ind = 'Y') then
    
      if BUILD_MANU_COUNTRY_DETAIL(O_error_message,
                                   L_itemsupctymfrDesc_tbl,
                                   O_rowids_rec,
                                   O_record_exists,
                                   I_queue_rec.item,
                                   L_orderable_ind,
                                   I_message_type,
                                   I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_itemsupctymfrDesc_tbl(1);
      end if;
    
    end if;
  
    if I_message_type in
       (XXADEO_RMSMFM_ITEMS.ISCD_ADD, XXADEO_RMSMFM_ITEMS.ISCD_UPD) or
       (I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD and
       I_tran_level_ind = 'Y') then
    
      if BUILD_DIM_DETAIL(O_error_message,
                          L_iscdimdesc_tbl,
                          O_rowids_rec,
                          O_record_exists,
                          I_queue_rec.item,
                          I_message_type,
                          I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_iscdimdesc_tbl(1);
      end if;
    
    end if;
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.UDAL_ADD or
       (I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD and
       I_tran_level_ind = 'Y') then
    
      if BUILD_UDA_LOV_DETAIL(O_error_message,
                              L_itemudalovdesc_tbl,
                              O_rowids_rec,
                              O_record_exists,
                              I_queue_rec.item,
                              I_message_type,
                              I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_itemudalovdesc_tbl(1);
      end if;
    
    end if;
  
    if I_message_type in (XXADEO_RMSMFM_ITEMS.UDAF_ADD) or
       (I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD and
       I_tran_level_ind = 'Y') then
    
      if BUILD_UDA_FF_DETAIL(O_error_message,
                             L_itemudaffdesc_tbl,
                             O_rowids_rec,
                             O_record_exists,
                             I_queue_rec.item,
                             I_message_type,
                             I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_itemudaffdesc_tbl(1);
      end if;
    
    end if;
  
    if I_message_type in (XXADEO_RMSMFM_ITEMS.UDAD_ADD) or
       (I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD and
       I_tran_level_ind = 'Y') then
    
      if BUILD_UDA_DATE_DETAIL(O_error_message,
                               L_itemudadatedesc_tbl,
                               O_rowids_rec,
                               O_record_exists,
                               I_queue_rec.item,
                               I_message_type,
                               I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_itemudadatedesc_tbl(1);
      end if;
    
    end if;
  
    if I_message_type in
       (XXADEO_RMSMFM_ITEMS.IMG_ADD, XXADEO_RMSMFM_ITEMS.IMG_UPD) or
       (I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD and
       I_tran_level_ind = 'Y') then
    
      if BUILD_IMAGE_DETAIL(O_error_message,
                            L_itemimagedesc_tbl,
                            O_rowids_rec,
                            O_record_exists,
                            I_queue_rec.item,
                            I_message_type,
                            I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_itemimagedesc_tbl(1);
      end if;
    
    end if;
  
    if I_message_type in
       (XXADEO_RMSMFM_ITEMS.UPC_ADD, XXADEO_RMSMFM_ITEMS.UPC_UPD) or
       (I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD and
       I_tran_level_ind = 'Y') then
    
      if BUILD_UPC_DETAIL(O_error_message,
                          L_itemupcdesc_tbl,
                          O_rowids_rec,
                          O_record_exists,
                          I_queue_rec.item,
                          I_message_type,
                          I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_itemupcdesc_tbl(1);
      end if;
    
    end if;
  
    if I_message_type in
       (XXADEO_RMSMFM_ITEMS.BOM_ADD, XXADEO_RMSMFM_ITEMS.BOM_UPD) or
       (I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD and
       I_tran_level_ind = 'Y') then
    
      if BUILD_BOM_DETAIL(O_error_message,
                          L_itembomdesc_tbl,
                          O_rowids_rec,
                          O_record_exists,
                          I_queue_rec.item,
                          I_message_type,
                          I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_itembomdesc_tbl(1);
      end if;
    
    end if;
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.TCKT_ADD or
       (I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD and
       I_tran_level_ind = 'Y') then
    
      if BUILD_TICKET_DETAIL(O_error_message,
                             L_itemtcktdesc_tbl,
                             O_rowids_rec,
                             O_record_exists,
                             I_queue_rec.item,
                             I_message_type,
                             I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_itemtcktdesc_tbl(1);
      end if;
    
    end if;
  
    if I_message_type in (XXADEO_RMSMFM_ITEMS.RIH_ADD,
                          XXADEO_RMSMFM_ITEMS.RIH_UPD,
                          XXADEO_RMSMFM_ITEMS.RID_ADD,
                          XXADEO_RMSMFM_ITEMS.RID_UPD) or
       (I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD and
       I_tran_level_ind = 'Y') then
    
      if BUILD_RELATED_ITEMS_HEAD(O_error_message,
                                  L_relateditemdesc_tbl,
                                  O_rowids_rec,
                                  O_record_exists,
                                  I_queue_rec.item,
                                  I_message_type,
                                  I_queue_rec.seq_no) = FALSE then
        return FALSE;
      end if;
    
      if I_message_type != XXADEO_RMSMFM_ITEMS.ITEM_ADD and O_record_exists then
        O_message := L_relateditemdesc_tbl(1);
      end if;
    
    end if;
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
      O_message := "RIB_ItemDesc_REC"(0,
                                      L_itemhdrdesc_rec, ---"RIB_ItemHdrDesc_REC",
                                      L_itemsupdesc_tbl, ---"RIB_ItemSupDesc_TBL",
                                      L_itemsupctydesc_tbl, ---"RIB_ItemSupCtyDesc_TBL",
                                      L_iscdimdesc_tbl, ---"RIB_ISCDimDesc_TBL",
                                      L_itemudalovdesc_tbl, ---"RIB_ItemUDALOVDesc_TBL",
                                      L_itemudaffdesc_tbl, ---"RIB_ItemUDAFFDesc_TBL",
                                      L_itemudadatedesc_tbl, ---"RIB_ItemUDADateDesc_TBL",
                                      L_itemimagedesc_tbl, ---"RIB_ItemImageDesc_TBL",
                                      L_itemupcdesc_tbl, ---"RIB_ItemUPCDesc_TBL",
                                      L_itembomdesc_tbl, ---"RIB_ItemBOMDesc_TBL",
                                      L_itemtcktdesc_tbl, ---"RIB_ItemTcktDesc_TBL"
                                      L_itemsupctymfrDesc_tbl, ---"RIB_ItemSupCtyMfrDesc_TBL"
                                      L_relateditemdesc_tbl); ---"RIB_RelatedItemDesc_TBL"
    
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;
    
  END BUILD_MESSAGE;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_HEADER_OBJECT
  -- Description: This function is responsible to create the ItemHdrDesc message with extension
  --              For create extension is necessary get CFAS and a List of BU related with this item
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_HEADER_OBJECT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message       IN OUT NOCOPY "RIB_ItemHdrDesc_REC",
                               O_record_exists IN OUT BOOLEAN,
                               O_orderable_ind IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                               I_item          IN ITEM_MFQUEUE.ITEM%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_HEADER_OBJECT';
  
    L_im_rec        ITEM_MASTER%ROWTYPE := NULL;
    L_item_info_rec ITEM_INFO := NULL;
    L_purchase_type VARCHAR2(1);
    L_division      GROUPS.DIVISION%TYPE;
    L_uda_id        XXADEO_MOM_DVM.VALUE_1%TYPE;
    L_extof_rec     "RIB_ExtOfItemHdrDesc_REC";
    L_bu_tbl        "RIB_bu_TBL";
    L_ListBU_rec    "RIB_ListOfBU_REC";
    L_cfa_rec       "RIB_CustFlexAttriVo_REC";
    L_cfas_tbl      "RIB_CustFlexAttriVo_TBL";
    L_list_cfas     XXADEO_CFA_DETAILS_TBL;
    O_status_code   VARCHAR2(2);
  
    cursor C_GET_ITEM is
      select * from item_master where item = I_item;
  
    cursor C_GET_PURCHASE_TYPE(I_dept DEPS.DEPT%TYPE) is
      select DECODE(d.purchase_type, 0, 'N', 1, 'S', 2, 'C', NULL)
        from deps d
       where d.dept = I_dept;
  
    cursor C_GET_DIVISION(I_dept DEPS.DEPT%TYPE) is
      select division
        from groups
       where group_no = (select group_no from deps where dept = I_dept);
  
  BEGIN
  
    open C_GET_ITEM;
    fetch C_GET_ITEM
      into L_im_rec;
    close C_GET_ITEM;
  
    if L_im_rec.item is NULL then
      O_record_exists := FALSE;
      return TRUE;
    end if;
  
    if GET_ITEM_INFO(O_error_message, L_item_info_rec, L_im_rec) = FALSE then
      return FALSE;
    end if;
    ---
    open C_GET_PURCHASE_TYPE(L_im_rec.dept);
    fetch C_GET_PURCHASE_TYPE
      into L_purchase_type;
    close C_GET_PURCHASE_TYPE;
    ---
    open C_GET_DIVISION(L_im_rec.dept);
    fetch C_GET_DIVISION
      into L_division;
    close C_GET_DIVISION;
  
    -- get BUs
    select value_1
      into L_uda_id
      from xxadeo_mom_dvm
     where func_area = 'UDA'
       and parameter = 'BU';
  
    for bu in (select uda_value
                 from uda_item_lov
                where item = L_im_rec.item
                  and uda_id = L_uda_id) loop
    
      if L_bu_tbl is null then
        L_bu_tbl := "RIB_bu_TBL"();
      end if;
      L_bu_tbl.extend();
      L_bu_tbl(L_bu_tbl.last) := bu.uda_value;
    end loop;
    L_ListBu_rec := "RIB_ListOfBU_REC"(0, L_bu_tbl);
  
    -- get CFAS
    XXADEO_CFAS_UTILS.GET_CFAS_EXIST('ITEM_MASTER',
                                     L_im_rec.item,
                                     L_list_cfas,
                                     O_status_code,
                                     O_error_message);
  
    if (O_status_code is not null and O_status_code = 'E') then
      raise PROGRAM_ERROR;
    end if;
  
    if L_list_cfas is not null and L_list_cfas.count() > 0 then
    
      for i in 1 .. L_list_cfas.count() loop
        L_cfa_rec := "RIB_CustFlexAttriVo_REC"(0, --RIB oid
                                               L_list_cfas(i).CFA_ID,
                                               L_list_cfas(i).CFA_NAME,
                                               L_list_cfas(i).VALUE,
                                               L_list_cfas(i).VALUE_date);
      
        if L_cfas_tbl is null then
          L_cfas_tbl := "RIB_CustFlexAttriVo_TBL"();
        end if;
        L_cfas_tbl.extend();
        L_cfas_tbl(L_cfas_tbl.last) := L_cfa_rec;
      end loop;
    end if;
    L_ExtOf_rec := "RIB_ExtOfItemHdrDesc_REC"(0, L_cfas_tbl, L_ListBu_rec);
  
    O_message := "RIB_ItemHdrDesc_REC"(0,
                                       L_im_rec.item,
                                       L_im_rec.item_number_type,
                                       L_im_rec.format_id,
                                       L_im_rec.prefix,
                                       L_im_rec.item_parent,
                                       L_im_rec.item_grandparent,
                                       L_im_rec.pack_ind,
                                       L_im_rec.item_level,
                                       L_im_rec.tran_level,
                                       L_im_rec.diff_1,
                                       L_item_info_rec.diff_1_type,
                                       L_im_rec.diff_2,
                                       L_item_info_rec.diff_2_type,
                                       L_im_rec.diff_3,
                                       L_item_info_rec.diff_3_type,
                                       L_im_rec.diff_4,
                                       L_item_info_rec.diff_4_type,
                                       L_im_rec.dept,
                                       L_item_info_rec.dept_desc,
                                       L_im_rec.class,
                                       L_item_info_rec.class_desc,
                                       L_im_rec.subclass,
                                       L_item_info_rec.subclass_desc,
                                       L_im_rec.status,
                                       L_im_rec.item_desc,
                                       L_im_rec.short_desc,
                                       L_im_rec.desc_up,
                                       L_im_rec.primary_ref_item_ind,
                                       L_im_rec.cost_zone_group_id,
                                       L_im_rec.standard_uom,
                                       L_im_rec.uom_conv_factor,
                                       L_im_rec.package_size,
                                       L_im_rec.package_uom,
                                       L_im_rec.merchandise_ind,
                                       L_im_rec.store_ord_mult,
                                       L_im_rec.forecast_ind,
                                       L_im_rec.mfg_rec_retail,
                                       L_im_rec.retail_label_type,
                                       L_item_info_rec.retail_label_desc,
                                       L_im_rec.retail_label_value,
                                       L_im_rec.handling_temp,
                                       L_item_info_rec.handling_temp_desc,
                                       L_im_rec.handling_sensitivity,
                                       L_item_info_rec.handling_sensitivity_desc,
                                       L_im_rec.catch_weight_ind,
                                       L_im_rec.waste_type,
                                       L_item_info_rec.waste_type_desc,
                                       L_im_rec.waste_pct,
                                       L_im_rec.default_waste_pct,
                                       L_im_rec.const_dimen_ind,
                                       L_im_rec.simple_pack_ind,
                                       L_im_rec.contains_inner_ind,
                                       L_im_rec.sellable_ind,
                                       L_im_rec.orderable_ind,
                                       L_im_rec.pack_type,
                                       L_im_rec.order_as_type,
                                       L_im_rec.comments,
                                       L_item_info_rec.unit_retail,
                                       L_im_rec.item_service_level,
                                       L_im_rec.gift_wrap_ind,
                                       L_im_rec.ship_alone_ind,
                                       NULL, --- vendor_style
                                       NULL, --- std_unit_weight
                                       NULL, --- single_price_flag
                                       NULL, --- preticket_flag
                                       NULL, --- planned_residual
                                       NULL, --- sortable
                                       NULL, --- item_master_uda1
                                       NULL, --- item_master_uda2
                                       NULL, --- item_master_uda3
                                       NULL, --- item_master_uda4
                                       NULL, --- item_master_uda5
                                       NULL, --- item_master_uda6
                                       NULL, --- item_master_uda7
                                       NULL, --- item_master_uda8
                                       NULL, --- item_master_uda9
                                       NULL, --- item_master_uda10
                                       NULL, --- item_master_uda11
                                       NULL, --- item_master_uda12
                                       NULL, --- item_master_uda13
                                       NULL, --- item_master_uda14
                                       NULL, --- item_master_uda15
                                       NULL, --- ship_alone
                                       NULL, --- slottable
                                       NULL, --- freight_class
                                       NULL, --- new_item
                                       L_im_rec.brand_name, --- brand
                                       NULL, --- break_case_ups
                                       NULL, --- rigid
                                       NULL, --- fragile
                                       NULL, --- container_type
                                       NULL, --- conveyable_flag
                                       NULL, --- hazard_matl_code
                                       NULL, --- velocity
                                       NULL, --- high_value_ind
                                       NULL, --- ticket_type
                                       NULL, --- color
                                       NULL, --- size1
                                       NULL, --- fit
                                       NULL, --- shade
                                       NULL, --- single_contain_bulk
                                       NULL, --- unit_pick_system_code
                                       NULL, --- roundable_flag
                                       L_im_rec.perishable_ind,
                                       L_division, --- division
                                       NULL, --- vendor_nbr
                                       NULL, --- kitting_wip_code
                                       NULL, --- unit_ticket_qty
                                       NULL, --- item_length
                                       NULL, --- item_width
                                       NULL, --- item_height
                                       NULL, --- item_weight
                                       NULL, --- item_cube
                                       NULL, --- std_unit_factor
                                       NULL, --- std_unit_qty
                                       NULL, --- expiration_days
                                       NULL, --- putaway_by_volume
                                       NULL, --- putaway_plan
                                       NULL, --- item_type
                                       NULL, --- sorter_group
                                       NULL, --- sku_optimization
                                       NULL, --- ext_source_system
                                       L_im_rec.item_xform_ind,
                                       L_im_rec.inventory_ind,
                                       L_im_rec.order_type,
                                       L_im_rec.sale_type,
                                       L_im_rec.deposit_item_type,
                                       L_im_rec.container_item,
                                       L_im_rec.deposit_in_price_per_uom,
                                       NULL, --- transport_ind
                                       L_im_rec.notional_pack_ind,
                                       L_im_rec.soh_inquiry_at_pack_ind,
                                       L_purchase_type,
                                       L_im_rec.product_classification,
                                       L_ExtOf_rec);
  
    O_orderable_ind := L_im_rec.orderable_ind;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;
    
  END BUILD_HEADER_OBJECT;
  
  
  -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_SUPPLIER_DETAIL
  -- Description: this function is responsible to create ItemSupDesc message with extension
  --              for create extension is necessary get CFAS at item/supplier level and
  --              merge BUs defined at item level and supplier level
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_SUPPLIER_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_message       IN OUT NOCOPY "RIB_ItemSupDesc_TBL",
                                 O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                 O_record_exists IN OUT BOOLEAN,
                                 I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                                 I_orderable_ind IN ITEM_MASTER.ORDERABLE_IND%TYPE,
                                 I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                 I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_SUPPLIER_DETAIL';
  
    L_itemsupp_rec      "RIB_ItemSupDesc_REC" := NULL;
    L_details_processed BOOLEAN := FALSE;
    L_purchase_type     DEPS.PURCHASE_TYPE%TYPE := NULL;
    L_extof_rec         "RIB_ExtOfItemSupDesc_REC";
    L_bu_tbl            "RIB_bu_TBL";
    L_ListBU_rec        "RIB_ListOfBU_REC";
    L_cfa_rec           "RIB_CustFlexAttriVo_REC";
    L_cfas_tbl          "RIB_CustFlexAttriVo_TBL";
    L_list_cfas         XXADEO_CFA_DETAILS_TBL;
    L_uda_id            XXADEO_MOM_DVM.Value_1%TYPE;
    O_status_code       VARCHAR2(2);
  
    cursor C_ITEM_PURCHSE_TYPE is
      select d.purchase_type
        from deps d, item_master im
       where d.dept = im.dept
         and im.item = I_item;
  
    cursor C_ITEMSUP_MC is
      select isp.supplier,
             isp.primary_supp_ind,
             isp.vpn,
             isp.supp_label,
             isp.consignment_rate,
             isp.concession_rate,
             isp.supp_diff_1,
             isp.supp_diff_2,
             isp.supp_diff_3,
             isp.supp_diff_4,
             isp.pallet_name,
             isp.case_name,
             isp.inner_name,
             isp.supp_discontinue_date,
             isp.direct_ship_ind,
             NULL                      q_rowid,
             NULL                      q_seq_no
        from item_supplier isp
       where isp.item = I_item
      UNION ALL
      select q.supplier,
             NULL       primary_supp_ind,
             NULL       vpn,
             NULL       supp_label,
             NULL       consignment_rate,
             NULL       concession_rate,
             NULL       supp_diff_1,
             NULL       supp_diff_2,
             NULL       supp_diff_3,
             NULL       supp_diff_4,
             NULL       pallet_name,
             NULL       case_name,
             NULL       inner_name,
             NULL       supp_discontinue_date,
             NULL       direct_ship_ind,
             q.rowid    q_rowid,
             q.seq_no   q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.message_type in
             (XXADEO_RMSMFM_ITEMS.ISUP_ADD,
              XXADEO_RMSMFM_ITEMS.ISUP_UPD,
              XXADEO_RMSMFM_ITEMS.ISUP_DEL);
  
    TYPE ITEMSUP_MC_TBL is TABLE OF C_ITEMSUP_MC%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_itemsup_mc_tbl ITEMSUP_MC_TBL;
  
    cursor C_ITEMSUP_DTL is
      select q.message_type,
             isp.supplier,
             isp.primary_supp_ind,
             isp.vpn,
             isp.supp_label,
             isp.consignment_rate,
             isp.concession_rate,
             isp.supp_diff_1,
             isp.supp_diff_2,
             isp.supp_diff_3,
             isp.supp_diff_4,
             isp.pallet_name,
             isp.case_name,
             isp.inner_name,
             isp.supp_discontinue_date,
             isp.direct_ship_ind,
             q.seq_no                  q_seq_no,
             q.rowid                   q_rowid
        from item_supplier isp, item_mfqueue q
       where isp.item = I_item
         and isp.item = q.item
         and isp.supplier = q.supplier
         and q.seq_no = I_seq_no;
  
    L_itemsup_dtl_rec C_ITEMSUP_DTL%ROWTYPE;
  
  BEGIN
  
    SQL_LIB.SET_MARK('OPEN',
                     'C_ITEM_PURCHSE_TYPE',
                     'ITEM_MASTER',
                     'item: ' || I_item);
    open C_ITEM_PURCHSE_TYPE;
    SQL_LIB.SET_MARK('FETCH',
                     'C_ITEM_PURCHSE_TYPE',
                     'ITEM_MASTER',
                     'item: ' || I_item);
    fetch C_ITEM_PURCHSE_TYPE
      into L_purchase_type;
    SQL_LIB.SET_MARK('CLOSE',
                     'C_ITEM_PURCHSE_TYPE',
                     'ITEM_MASTER',
                     'item: ' || I_item);
    close C_ITEM_PURCHSE_TYPE;
    O_message := "RIB_ItemSupDesc_TBL"();
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_ITEMSUP_MC;
      fetch C_ITEMSUP_MC BULK COLLECT
        INTO L_itemsup_mc_tbl;
      close C_ITEMSUP_MC;
    
      FOR i IN 1 .. L_itemsup_mc_tbl.COUNT LOOP
      
        if L_itemsup_mc_tbl(i).q_rowid is not NULL then
          ---  add rowids to TBLs of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_itemsup_mc_tbl(i)
                                                                              .q_rowid;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_itemsup_mc_tbl(i)
                                                                          .q_seq_no;
        else
        
          -- get BUs
          select value_1
            into L_uda_id
            from xxadeo_mom_dvm
           where func_area = 'UDA'
             and parameter = 'BU';
        
          for bu in (select uda_value
                       from uda_item_lov
                      where item = I_item
                        and uda_id = L_uda_id
                        and uda_value in
                            (select bu
                               from xxadeo_bu_ou,
                                    sups,
                                    partner_org_unit pou,
                                    item_master      im,
                                    item_supplier    its
                              where im.item = I_item
                                and its.supplier = sups.supplier
                                and its.item = im.item
                                and pou.partner = sups.supplier
                                and pou.org_unit_id = ou)) loop
          
            if L_bu_tbl is null then
              L_bu_tbl := "RIB_bu_TBL"();
            end if;
            L_bu_tbl.extend();
            L_bu_tbl(L_bu_tbl.last) := bu.uda_value;
          end loop;
          L_ListBu_rec := "RIB_ListOfBU_REC"(0, L_bu_tbl);
        
          -- get CFAS
          XXADEO_CFAS_UTILS.GET_CFAS_EXIST('ITEM_SUPPLIER',
                                           I_item || '|' || L_itemsup_mc_tbl(i)
                                           .supplier,
                                           L_list_cfas,
                                           O_status_code,
                                           O_error_msg);
        
          if (O_status_code is not null and O_status_code = 'E') then
            raise PROGRAM_ERROR;
          end if;
        
          if L_list_cfas is not null and L_list_cfas.count() > 0 then
          
            for i in 1 .. L_list_cfas.count() loop
              L_cfa_rec := "RIB_CustFlexAttriVo_REC"(0, --RIB oid
                                                     L_list_cfas(i).CFA_ID,
                                                     L_list_cfas(i).CFA_NAME,
                                                     L_list_cfas(i).VALUE,
                                                     L_list_cfas(i)
                                                     .VALUE_date);
            
              if L_cfas_tbl is null then
                L_cfas_tbl := "RIB_CustFlexAttriVo_TBL"();
              end if;
              L_cfas_tbl.extend();
              L_cfas_tbl(L_cfas_tbl.last) := L_cfa_rec;
            end loop;
          end if;
        
          L_ExtOf_rec := "RIB_ExtOfItemSupDesc_REC"(0,
                                                    L_cfas_tbl,
                                                    L_ListBu_rec);
        
          L_itemsupp_rec := "RIB_ItemSupDesc_REC"(0,
                                                  I_item,
                                                  NULL, --- supplier
                                                  NULL, --- primary_supp_ind
                                                  NULL, --- vpn
                                                  NULL, --- supp_label
                                                  NULL, --- consignment_rate
                                                  NULL, --- supp_diff_1
                                                  NULL, --- supp_diff_2
                                                  NULL, --- supp_diff_3
                                                  NULL, --- supp_diff_4
                                                  NULL, --- pallet_name
                                                  NULL, --- case_name
                                                  NULL, --- inner_name
                                                  NULL, --- supp_discontinue_date
                                                  NULL, --- direct_ship_ind
                                                  L_ExtOf_rec); --- extof
        
          O_message := "RIB_ItemSupDesc_TBL"();
        
          L_itemsupp_rec.supplier         := L_itemsup_mc_tbl(i).supplier;
          L_itemsupp_rec.primary_supp_ind := L_itemsup_mc_tbl(i)
                                             .primary_supp_ind;
          L_itemsupp_rec.vpn              := L_itemsup_mc_tbl(i).vpn;
          L_itemsupp_rec.supp_label       := L_itemsup_mc_tbl(i).supp_label;
          if L_purchase_type = 1 then
            L_itemsupp_rec.consignment_rate := L_itemsup_mc_tbl(i)
                                               .consignment_rate;
          elsif L_purchase_type = 2 then
            L_itemsupp_rec.consignment_rate := L_itemsup_mc_tbl(i)
                                               .concession_rate;
          end if;
          L_itemsupp_rec.supp_diff_1           := L_itemsup_mc_tbl(i)
                                                  .supp_diff_1;
          L_itemsupp_rec.supp_diff_2           := L_itemsup_mc_tbl(i)
                                                  .supp_diff_2;
          L_itemsupp_rec.supp_diff_3           := L_itemsup_mc_tbl(i)
                                                  .supp_diff_3;
          L_itemsupp_rec.supp_diff_4           := L_itemsup_mc_tbl(i)
                                                  .supp_diff_4;
          L_itemsupp_rec.pallet_name           := L_itemsup_mc_tbl(i)
                                                  .pallet_name;
          L_itemsupp_rec.case_name             := L_itemsup_mc_tbl(i)
                                                  .case_name;
          L_itemsupp_rec.inner_name            := L_itemsup_mc_tbl(i)
                                                  .inner_name;
          L_itemsupp_rec.supp_discontinue_date := L_itemsup_mc_tbl(i)
                                                  .supp_discontinue_date;
          L_itemsupp_rec.direct_ship_ind       := L_itemsup_mc_tbl(i)
                                                  .direct_ship_ind;
        
          ---
          L_details_processed := TRUE;
          
        O_message.EXTEND();
        O_message(O_message.count) := L_itemsupp_rec;
        end if;
      
      END LOOP;
    
    else
    
      open C_ITEMSUP_DTL;
      fetch C_ITEMSUP_DTL
        INTO L_itemsup_dtl_rec;
      close C_ITEMSUP_DTL;
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
      if L_itemsup_dtl_rec.supplier is NULL then
        O_record_exists := FALSE;
        return TRUE;
      end if;
    
      -- get BUs
      for bu in (select bu
                   from xxadeo_bu_ou, sups, partner_org_unit pou
                  where sups.supplier = L_itemsup_dtl_rec.supplier
                    and pou.partner = sups.supplier
                    and pou.org_unit_id = ou) loop
      
        if L_bu_tbl is null then
          L_bu_tbl := "RIB_bu_TBL"();
        end if;
        L_bu_tbl.extend();
        L_bu_tbl(L_bu_tbl.last) := bu.bu;
      end loop;
      L_ListBu_rec := "RIB_ListOfBU_REC"(0, L_bu_tbl);
    
      -- get CFAS
      XXADEO_CFAS_UTILS.GET_CFAS_EXIST('ITEM_SUPPLIER',
                                       I_item || '|' ||
                                       L_itemsup_dtl_rec.supplier,
                                       L_list_cfas,
                                       O_status_code,
                                       O_error_msg);
    
      if (O_status_code is not null and O_status_code = 'E') then
        raise PROGRAM_ERROR;
      end if;
    
      if L_list_cfas is not null and L_list_cfas.count() > 0 then
      
        for i in 1 .. L_list_cfas.count() loop
          L_cfa_rec := "RIB_CustFlexAttriVo_REC"(0, --RIB oid
                                                 L_list_cfas(i).CFA_ID,
                                                 L_list_cfas(i).CFA_NAME,
                                                 L_list_cfas(i).VALUE,
                                                 L_list_cfas(i).VALUE_date);
        
          if L_cfas_tbl is null then
            L_cfas_tbl := "RIB_CustFlexAttriVo_TBL"();
          end if;
          L_cfas_tbl.extend();
          L_cfas_tbl(L_cfas_tbl.last) := L_cfa_rec;
        end loop;
      end if;
    
      L_ExtOf_rec := "RIB_ExtOfItemSupDesc_REC"(0, L_cfas_tbl, L_ListBu_rec);
    
      L_itemsupp_rec := "RIB_ItemSupDesc_REC"(0,
                                              I_item,
                                              NULL, --- supplier
                                              NULL, --- primary_supp_ind
                                              NULL, --- vpn
                                              NULL, --- supp_label
                                              NULL, --- consignment_rate
                                              NULL, --- supp_diff_1
                                              NULL, --- supp_diff_2
                                              NULL, --- supp_diff_3
                                              NULL, --- supp_diff_4
                                              NULL, --- pallet_name
                                              NULL, --- case_name
                                              NULL, --- inner_name
                                              NULL, --- supp_discontinue_date
                                              NULL, --- direct_ship_ind
                                              L_ExtOf_rec); --- extof
    
      L_itemsupp_rec.supplier         := L_itemsup_dtl_rec.supplier;
      L_itemsupp_rec.primary_supp_ind := L_itemsup_dtl_rec.primary_supp_ind;
      L_itemsupp_rec.vpn              := L_itemsup_dtl_rec.vpn;
      L_itemsupp_rec.supp_label       := L_itemsup_dtl_rec.supp_label;
      if L_purchase_type = 1 then
        L_itemsupp_rec.consignment_rate := L_itemsup_dtl_rec.consignment_rate;
      elsif L_purchase_type = 2 then
        L_itemsupp_rec.consignment_rate := L_itemsup_dtl_rec.concession_rate;
      end if;
      L_itemsupp_rec.supp_diff_1           := L_itemsup_dtl_rec.supp_diff_1;
      L_itemsupp_rec.supp_diff_2           := L_itemsup_dtl_rec.supp_diff_2;
      L_itemsupp_rec.supp_diff_3           := L_itemsup_dtl_rec.supp_diff_3;
      L_itemsupp_rec.supp_diff_4           := L_itemsup_dtl_rec.supp_diff_4;
      L_itemsupp_rec.pallet_name           := L_itemsup_dtl_rec.pallet_name;
      L_itemsupp_rec.case_name             := L_itemsup_dtl_rec.case_name;
      L_itemsupp_rec.inner_name            := L_itemsup_dtl_rec.inner_name;
      L_itemsupp_rec.supp_discontinue_date := L_itemsup_dtl_rec.supp_discontinue_date;
      L_itemsupp_rec.direct_ship_ind       := L_itemsup_dtl_rec.direct_ship_ind;
    
      O_message.EXTEND();
      O_message(O_message.COUNT) := L_itemsupp_rec;
    
      --- add rowid to TBL of rowids that will be deleted
      O_rowids_rec.queue_rowid_tbl.EXTEND;
      O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_itemsup_dtl_rec.q_rowid;
      O_rowids_rec.queue_seq_tbl.EXTEND;
      O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_itemsup_dtl_rec.q_seq_no;
      ---
    
      L_details_processed := TRUE;
    
    end if;
  
    if L_details_processed = FALSE and I_orderable_ind = 'N' then
      --- if there are no supplier records in the Item create message for a non-orderable item,
      --- we need to initialize the supplier table to be atomically NULL
      O_message := NULL;
      ---
    elsif L_details_processed = FALSE then
      -- if no data found in cursor for orderable item, raise error
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_SUPPLIER_DETAIL;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_COUNTRY_DETAIL
  -- Description: This Function is responsible for create ItemSupCtyDesc message with extension
  --              For create extension will be necessary get supp_hier_type_1, supp_hier_lvl_1 fields
  --              of item_supp_country and merge BUs defined at item level and supplier level
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_COUNTRY_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_message       IN OUT NOCOPY "RIB_ItemSupCtyDesc_TBL",
                                O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                O_record_exists IN OUT BOOLEAN,
                                I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                                I_orderable_ind IN ITEM_MASTER.ORDERABLE_IND%TYPE,
                                I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_COUNTRY_DETAIL';
  
    L_itemsupcty_rec    "RIB_ItemSupCtyDesc_REC" := NULL;
    L_details_processed BOOLEAN := FALSE;
  
    cursor C_ISCOUNTRY_MC is
      select isc.supplier,
             isc.origin_country_id,
             isc.primary_supp_ind,
             isc.primary_country_ind,
             isc.unit_cost,
             isc.lead_time,
             isc.pickup_lead_time,
             isc.supp_pack_size,
             isc.inner_pack_size,
             isc.round_lvl,
             isc.min_order_qty,
             isc.max_order_qty,
             isc.packing_method,
             isc.default_uop,
             isc.ti,
             isc.hi,
             isc.cost_uom,
             isc.tolerance_type,
             isc.max_tolerance,
             isc.min_tolerance,
             NULL                    q_rowid,
             NULL                    q_seq_no
        from item_supp_country isc
       where isc.item = I_item
      UNION ALL
      select q.supplier,
             NULL       origin_country_id,
             NULL       primary_supp_ind,
             NULL       primary_country_ind,
             NULL       unit_cost,
             NULL       lead_time,
             NULL       pickup_lead_time,
             NULL       supp_pack_size,
             NULL       inner_pack_size,
             NULL       round_lvl,
             NULL       min_order_qty,
             NULL       max_order_qty,
             NULL       packing_method,
             NULL       default_uop,
             NULL       ti,
             NULL       hi,
             NULL       cost_uom,
             NULL       tolerance_type,
             NULL       max_tolerance,
             NULL       min_tolerance,
             q.rowid    q_rowid,
             q.seq_no   q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.message_type in
             (XXADEO_RMSMFM_ITEMS.ISC_ADD,
              XXADEO_RMSMFM_ITEMS.ISC_UPD,
              XXADEO_RMSMFM_ITEMS.ISC_DEL);
  
    TYPE ISCOUNTRY_MC_TBL is TABLE OF C_ISCOUNTRY_MC%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_iscountry_mc_tbl ISCOUNTRY_MC_TBL;
  
    cursor C_ISCOUNTRY_DTL is
      select q.message_type,
             isc.supplier,
             isc.origin_country_id,
             isc.primary_supp_ind,
             isc.primary_country_ind,
             isc.unit_cost,
             isc.lead_time,
             isc.pickup_lead_time,
             isc.supp_pack_size,
             isc.inner_pack_size,
             isc.round_lvl,
             isc.min_order_qty,
             isc.max_order_qty,
             isc.packing_method,
             isc.default_uop,
             isc.ti,
             isc.hi,
             isc.cost_uom,
             isc.tolerance_type,
             isc.max_tolerance,
             isc.min_tolerance,
             q.seq_no                q_seq_no,
             q.rowid                 q_rowid
        from item_supp_country isc, item_mfqueue q
       where q.item = I_item
         and q.seq_no = I_seq_no
         and isc.item = q.item
         and isc.supplier = q.supplier
         and isc.origin_country_id = q.country_id;
  
    L_iscountry_dtl_rec C_ISCOUNTRY_DTL%ROWTYPE;
    L_extOf             "RIB_ExtOfItemSupCtyDesc_REC";
    L_bu_tbl            "RIB_bu_TBL";
    L_ListBU_rec        "RIB_ListOfBU_REC";
    L_origin            ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
    L_supp_type         ITEM_SUPP_COUNTRY.SUPP_HIER_TYPE_1%TYPE;
    L_supp_lvl          ITEM_SUPP_COUNTRY.SUPP_HIER_lvl_1%TYPE;
    L_uda_id            XXADEO_MOM_DVM.VALUE_1%TYPE;
  BEGIN
  
    O_message := "RIB_ItemSupCtyDesc_TBL"();
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_ISCOUNTRY_MC;
      fetch C_ISCOUNTRY_MC BULK COLLECT
        INTO L_iscountry_mc_tbl;
      close C_ISCOUNTRY_MC;
    
      FOR i IN 1 .. L_iscountry_mc_tbl.COUNT LOOP
      
        if L_iscountry_mc_tbl(i).q_rowid is not NULL then
          ---  add rowids to TBLs of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_iscountry_mc_tbl(i)
                                                                              .q_rowid;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_iscountry_mc_tbl(i)
                                                                          .q_seq_no;
        else
          -- get BUs
          select value_1
            into L_uda_id
            from xxadeo_mom_dvm
           where func_area = 'UDA'
             and parameter = 'BU';
        
          for bu in (select uda_value
                       from uda_item_lov
                      where item = I_item
                        and uda_id = L_uda_id
                        and uda_value in
                            (select bu
                               from xxadeo_bu_ou,
                                    sups,
                                    partner_org_unit pou,
                                    item_master      im,
                                    item_supplier    its
                              where im.item = I_item
                                and its.supplier = sups.supplier
                                and its.item = im.item
                                and pou.partner = sups.supplier
                                and pou.org_unit_id = ou)) loop
          
            if L_bu_tbl is null then
              L_bu_tbl := "RIB_bu_TBL"();
            end if;
            L_bu_tbl.extend();
            L_bu_tbl(L_bu_tbl.last) := bu.uda_value;
          end loop;
          L_ListBu_rec := "RIB_ListOfBU_REC"(0, L_bu_tbl);
        
          select origin_country_id, supp_hier_type_1, supp_hier_lvl_1
            into L_origin, L_supp_type, L_supp_lvl
            from item_supp_country
           where item = I_item
             and supplier = L_iscountry_mc_tbl(i).supplier
             and origin_country_id = L_iscountry_mc_tbl(i)
                .origin_country_id;
        
          L_extOf := "RIB_ExtOfItemSupCtyDesc_REC"(0,
                                                   L_supp_type,
                                                   L_supp_lvl,
                                                   L_ListBu_rec);
        
          L_itemsupcty_rec := "RIB_ItemSupCtyDesc_REC"(0,
                                                       I_item,
                                                       NULL, --- supplier
                                                       NULL, --- origin_country_id
                                                       NULL, --- primary_supp_ind
                                                       NULL, --- primary_country_ind
                                                       NULL, --- unit_cost
                                                       NULL, --- lead_time
                                                       NULL, --- pickup_lead_time
                                                       NULL, --- supp_pack_size
                                                       NULL, --- inner_pack_size
                                                       NULL, --- round_lvl
                                                       NULL, --- min_order_qty
                                                       NULL, --- max_order_qty
                                                       NULL, --- packing_method
                                                       NULL, --- default_uop
                                                       NULL, --- ti
                                                       NULL, --- hi
                                                       NULL, --- cost_uom
                                                       NULL, --- tolerance_type
                                                       NULL, --- max_tolerance
                                                       NULL, --- min_tolerance
                                                       L_extOf); --extof
        
          L_itemsupcty_rec.supplier            := L_iscountry_mc_tbl(i)
                                                  .supplier;
          L_itemsupcty_rec.origin_country_id   := L_iscountry_mc_tbl(i)
                                                  .origin_country_id;
          L_itemsupcty_rec.primary_supp_ind    := L_iscountry_mc_tbl(i)
                                                  .primary_supp_ind;
          L_itemsupcty_rec.primary_country_ind := L_iscountry_mc_tbl(i)
                                                  .primary_country_ind;
          L_itemsupcty_rec.unit_cost           := L_iscountry_mc_tbl(i)
                                                  .unit_cost;
          L_itemsupcty_rec.lead_time           := L_iscountry_mc_tbl(i)
                                                  .lead_time;
          L_itemsupcty_rec.pickup_lead_time    := L_iscountry_mc_tbl(i)
                                                  .pickup_lead_time;
          L_itemsupcty_rec.supp_pack_size      := L_iscountry_mc_tbl(i)
                                                  .supp_pack_size;
          L_itemsupcty_rec.inner_pack_size     := L_iscountry_mc_tbl(i)
                                                  .inner_pack_size;
          L_itemsupcty_rec.round_lvl           := L_iscountry_mc_tbl(i)
                                                  .round_lvl;
          L_itemsupcty_rec.min_order_qty       := L_iscountry_mc_tbl(i)
                                                  .min_order_qty;
          L_itemsupcty_rec.max_order_qty       := L_iscountry_mc_tbl(i)
                                                  .max_order_qty;
          L_itemsupcty_rec.packing_method      := L_iscountry_mc_tbl(i)
                                                  .packing_method;
          L_itemsupcty_rec.default_uop         := L_iscountry_mc_tbl(i)
                                                  .default_uop;
          L_itemsupcty_rec.ti                  := L_iscountry_mc_tbl(i).ti;
          L_itemsupcty_rec.hi                  := L_iscountry_mc_tbl(i).hi;
          L_itemsupcty_rec.cost_uom            := L_iscountry_mc_tbl(i)
                                                  .cost_uom;
          L_itemsupcty_rec.tolerance_type      := L_iscountry_mc_tbl(i)
                                                  .tolerance_type;
          L_itemsupcty_rec.max_tolerance       := L_iscountry_mc_tbl(i)
                                                  .max_tolerance;
          L_itemsupcty_rec.min_tolerance       := L_iscountry_mc_tbl(i)
                                                  .min_tolerance;
        
          O_message.EXTEND();
          O_message(O_message.COUNT) := L_itemsupcty_rec;
          ---
          L_details_processed := TRUE;
        end if;
      
      END LOOP;
    
    else
    
      open C_ISCOUNTRY_DTL;
      fetch C_ISCOUNTRY_DTL
        INTO L_iscountry_dtl_rec;
      close C_ISCOUNTRY_DTL;
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
      if L_iscountry_dtl_rec.supplier is NULL then
        O_record_exists := FALSE;
        return TRUE;
      end if;
    
      -- get BUs
      for bu in (select bu
                   from xxadeo_bu_ou,
                        sups,
                        partner_org_unit  pou,
                        item_supplier     its,
                        item_supp_country isc
                  where its.supplier = sups.supplier
                    and isc.supplier = its.supplier
                    and its.item = isc.item
                    and its.item = I_item
                    and isc.origin_country_id = L_iscountry_dtl_rec
                 .origin_country_id
                    and pou.partner = sups.supplier
                    and pou.org_unit_id = ou) loop
      
        if L_bu_tbl is null then
          L_bu_tbl := "RIB_bu_TBL"();
        end if;
        L_bu_tbl.extend();
        L_bu_tbl(L_bu_tbl.last) := bu.bu;
      end loop;
      L_ListBu_rec := "RIB_ListOfBU_REC"(0, L_bu_tbl);
      -- get supp_hier_type_1 and supp_hier_lvl_1
    
      select origin_country_id, supp_hier_type_1, supp_hier_lvl_1
        into L_origin, L_supp_type, L_supp_lvl
        from item_supp_country
       where item = I_item
         and supplier = L_iscountry_dtl_rec.supplier
         and origin_country_id = L_iscountry_dtl_rec.origin_country_id;
    
      L_extOf := "RIB_ExtOfItemSupCtyDesc_REC"(0,
                                               L_supp_type,
                                               L_supp_lvl,
                                               L_ListBu_rec);
    
      L_itemsupcty_rec := "RIB_ItemSupCtyDesc_REC"(0,
                                                   I_item,
                                                   NULL, --- supplier
                                                   NULL, --- origin_country_id
                                                   NULL, --- primary_supp_ind
                                                   NULL, --- primary_country_ind
                                                   NULL, --- unit_cost
                                                   NULL, --- lead_time
                                                   NULL, --- pickup_lead_time
                                                   NULL, --- supp_pack_size
                                                   NULL, --- inner_pack_size
                                                   NULL, --- round_lvl
                                                   NULL, --- min_order_qty
                                                   NULL, --- max_order_qty
                                                   NULL, --- packing_method
                                                   NULL, --- default_uop
                                                   NULL, --- ti
                                                   NULL, --- hi
                                                   NULL, --- cost_uom
                                                   NULL, --- tolerance_type
                                                   NULL, --- max_tolerance
                                                   NULL, --- min_tolerance
                                                   L_extOf); --extof
    
      L_itemsupcty_rec.supplier            := L_iscountry_dtl_rec.supplier;
      L_itemsupcty_rec.origin_country_id   := L_iscountry_dtl_rec.origin_country_id;
      L_itemsupcty_rec.primary_supp_ind    := L_iscountry_dtl_rec.primary_supp_ind;
      L_itemsupcty_rec.primary_country_ind := L_iscountry_dtl_rec.primary_country_ind;
      L_itemsupcty_rec.unit_cost           := L_iscountry_dtl_rec.unit_cost;
      L_itemsupcty_rec.lead_time           := L_iscountry_dtl_rec.lead_time;
      L_itemsupcty_rec.pickup_lead_time    := L_iscountry_dtl_rec.pickup_lead_time;
      L_itemsupcty_rec.supp_pack_size      := L_iscountry_dtl_rec.supp_pack_size;
      L_itemsupcty_rec.inner_pack_size     := L_iscountry_dtl_rec.inner_pack_size;
      L_itemsupcty_rec.round_lvl           := L_iscountry_dtl_rec.round_lvl;
      L_itemsupcty_rec.min_order_qty       := L_iscountry_dtl_rec.min_order_qty;
      L_itemsupcty_rec.max_order_qty       := L_iscountry_dtl_rec.max_order_qty;
      L_itemsupcty_rec.packing_method      := L_iscountry_dtl_rec.packing_method;
      L_itemsupcty_rec.default_uop         := L_iscountry_dtl_rec.default_uop;
      L_itemsupcty_rec.ti                  := L_iscountry_dtl_rec.ti;
      L_itemsupcty_rec.hi                  := L_iscountry_dtl_rec.hi;
      L_itemsupcty_rec.cost_uom            := L_iscountry_dtl_rec.cost_uom;
      L_itemsupcty_rec.tolerance_type      := L_iscountry_dtl_rec.tolerance_type;
      L_itemsupcty_rec.max_tolerance       := L_iscountry_dtl_rec.max_tolerance;
      L_itemsupcty_rec.min_tolerance       := L_iscountry_dtl_rec.min_tolerance;
    
      O_message.EXTEND();
      O_message(O_message.COUNT) := L_itemsupcty_rec;
    
      --- add rowid to TBL of rowids that will be deleted
      O_rowids_rec.queue_rowid_tbl.EXTEND;
      O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_iscountry_dtl_rec.q_rowid;
      O_rowids_rec.queue_seq_tbl.EXTEND;
      O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_iscountry_dtl_rec.q_seq_no;
      ---
    
      L_details_processed := TRUE;
    
    end if;
  
    if L_details_processed = FALSE and I_orderable_ind = 'N' then
      --- if there are no supp/country records in the Item create message for a non-orderable item,
      --- we need to initialize the supp/country table to be atomically NULL
      O_message := NULL;
      ---
    elsif L_details_processed = FALSE then
      -- if no data found in cursor for orderable item, raise error
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_COUNTRY_DETAIL;
  
  
  -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_DIM_DETAIL
  -- Description: This procedure is responsible to create ISCDimDesc message
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_DIM_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_message       IN OUT NOCOPY "RIB_ISCDimDesc_TBL",
                            O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                            O_record_exists IN OUT BOOLEAN,
                            I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                            I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                            I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_DIM_DETAIL';
  
    L_iscdim_rec        "RIB_ISCDimDesc_REC" := NULL;
    L_details_processed BOOLEAN := FALSE;
  
    cursor C_ISCDIM_MC is
      select iscd.supplier,
             iscd.origin_country,
             iscd.dim_object,
             iscd.presentation_method,
             iscd.length,
             iscd.width,
             iscd.height,
             iscd.lwh_uom,
             iscd.weight,
             iscd.net_weight,
             iscd.weight_uom,
             iscd.liquid_volume,
             iscd.liquid_volume_uom,
             iscd.stat_cube,
             iscd.tare_weight,
             iscd.tare_type,
             NULL                     q_rowid,
             NULL                     q_seq_no
        from item_supp_country_dim iscd
       where iscd.item = I_item
      UNION ALL
      select q.supplier,
             NULL       origin_country,
             NULL       dim_object,
             NULL       presentation_method,
             NULL       length,
             NULL       width,
             NULL       height,
             NULL       lwh_uom,
             NULL       weight,
             NULL       net_weight,
             NULL       weight_uom,
             NULL       liquid_volume,
             NULL       liquid_volume_uom,
             NULL       stat_cube,
             NULL       tare_weight,
             NULL       tare_type,
             q.rowid    q_rowid,
             q.seq_no   q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.message_type in
             (XXADEO_RMSMFM_ITEMS.ISCD_ADD,
              XXADEO_RMSMFM_ITEMS.ISCD_UPD,
              XXADEO_RMSMFM_ITEMS.ISCD_DEL);
  
    TYPE ISCDIM_MC_TBL is TABLE OF C_ISCDIM_MC%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_iscdim_mc_tbl ISCDIM_MC_TBL;
  
    cursor C_ISCDIM_DTL is
      select q.message_type,
             iscd.supplier,
             iscd.origin_country,
             iscd.dim_object,
             iscd.presentation_method,
             iscd.length,
             iscd.width,
             iscd.height,
             iscd.lwh_uom,
             iscd.weight,
             iscd.net_weight,
             iscd.weight_uom,
             iscd.liquid_volume,
             iscd.liquid_volume_uom,
             iscd.stat_cube,
             iscd.tare_weight,
             iscd.tare_type,
             q.seq_no                 q_seq_no,
             q.rowid                  q_rowid
        from item_supp_country_dim iscd, item_mfqueue q
       where q.item = I_item
         and q.seq_no = I_seq_no
         and iscd.item = q.item
         and iscd.supplier = q.supplier
         and iscd.origin_country = q.country_id
         and iscd.dim_object = q.dim_object;
  
    L_iscdim_dtl_rec C_ISCDIM_DTL%ROWTYPE;
  
  BEGIN
  
    L_iscdim_rec := "RIB_ISCDimDesc_REC"(0,
                                         I_item,
                                         NULL, --- supplier
                                         NULL, --- origin_country
                                         NULL, --- dim_object
                                         NULL, --- object_desc
                                         NULL, --- presentation_method
                                         NULL, --- method_desc
                                         NULL, --- length
                                         NULL, --- width
                                         NULL, --- height
                                         NULL, --- lwh_uom
                                         NULL, --- weight
                                         NULL, --- net_weight
                                         NULL, --- weight_uom
                                         NULL, --- liquid_volume
                                         NULL, --- liquid_volume_uom
                                         NULL, --- stat_cube
                                         NULL, --- tare_weight
                                         NULL); --- tare_type
  
    O_message := "RIB_ISCDimDesc_TBL"();
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_ISCDIM_MC;
      fetch C_ISCDIM_MC BULK COLLECT
        INTO L_iscdim_mc_tbl;
      close C_ISCDIM_MC;
    
      FOR i IN 1 .. L_iscdim_mc_tbl.COUNT LOOP
      
        if L_iscdim_mc_tbl(i).q_rowid is not NULL then
          ---  add rowids to TBLs of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_iscdim_mc_tbl(i)
                                                                              .q_rowid;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_iscdim_mc_tbl(i)
                                                                          .q_seq_no;
        else
          L_iscdim_rec.supplier            := L_iscdim_mc_tbl(i).supplier;
          L_iscdim_rec.origin_country      := L_iscdim_mc_tbl(i)
                                              .origin_country;
          L_iscdim_rec.dim_object          := L_iscdim_mc_tbl(i).dim_object;
          L_iscdim_rec.presentation_method := L_iscdim_mc_tbl(i)
                                              .presentation_method;
          L_iscdim_rec.length              := L_iscdim_mc_tbl(i).length;
          L_iscdim_rec.width               := L_iscdim_mc_tbl(i).width;
          L_iscdim_rec.height              := L_iscdim_mc_tbl(i).height;
          L_iscdim_rec.lwh_uom             := L_iscdim_mc_tbl(i).lwh_uom;
          L_iscdim_rec.weight              := L_iscdim_mc_tbl(i).weight;
          L_iscdim_rec.net_weight          := L_iscdim_mc_tbl(i).net_weight;
          L_iscdim_rec.weight_uom          := L_iscdim_mc_tbl(i).weight_uom;
          L_iscdim_rec.liquid_volume       := L_iscdim_mc_tbl(i)
                                              .liquid_volume;
          L_iscdim_rec.liquid_volume_uom   := L_iscdim_mc_tbl(i)
                                              .liquid_volume_uom;
          L_iscdim_rec.stat_cube           := L_iscdim_mc_tbl(i).stat_cube;
          L_iscdim_rec.tare_weight         := L_iscdim_mc_tbl(i).tare_weight;
          L_iscdim_rec.tare_type           := L_iscdim_mc_tbl(i).tare_type;
        
          if GET_DIMENSION_DESCRIPTIONS(O_error_msg,
                                        L_iscdim_rec.object_desc,
                                        L_iscdim_rec.method_desc,
                                        L_iscdim_rec.dim_object,
                                        L_iscdim_rec.presentation_method) =
             FALSE then
            return FALSE;
          end if;
        
          O_message.EXTEND();
          O_message(O_message.COUNT) := L_iscdim_rec;
          ---
          L_details_processed := TRUE;
        end if;
      
      END LOOP;
    
    else
    
      open C_ISCDIM_DTL;
      fetch C_ISCDIM_DTL
        INTO L_iscdim_dtl_rec;
      close C_ISCDIM_DTL;
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
      if L_iscdim_dtl_rec.supplier is NULL then
        O_record_exists := FALSE;
        return TRUE;
      end if;
    
      L_iscdim_rec.supplier            := L_iscdim_dtl_rec.supplier;
      L_iscdim_rec.origin_country      := L_iscdim_dtl_rec.origin_country;
      L_iscdim_rec.dim_object          := L_iscdim_dtl_rec.dim_object;
      L_iscdim_rec.presentation_method := L_iscdim_dtl_rec.presentation_method;
      L_iscdim_rec.length              := L_iscdim_dtl_rec.length;
      L_iscdim_rec.width               := L_iscdim_dtl_rec.width;
      L_iscdim_rec.height              := L_iscdim_dtl_rec.height;
      L_iscdim_rec.lwh_uom             := L_iscdim_dtl_rec.lwh_uom;
      L_iscdim_rec.weight              := L_iscdim_dtl_rec.weight;
      L_iscdim_rec.net_weight          := L_iscdim_dtl_rec.net_weight;
      L_iscdim_rec.weight_uom          := L_iscdim_dtl_rec.weight_uom;
      L_iscdim_rec.liquid_volume       := L_iscdim_dtl_rec.liquid_volume;
      L_iscdim_rec.liquid_volume_uom   := L_iscdim_dtl_rec.liquid_volume_uom;
      L_iscdim_rec.stat_cube           := L_iscdim_dtl_rec.stat_cube;
      L_iscdim_rec.tare_weight         := L_iscdim_dtl_rec.tare_weight;
      L_iscdim_rec.tare_type           := L_iscdim_dtl_rec.tare_type;
    
      if GET_DIMENSION_DESCRIPTIONS(O_error_msg,
                                    L_iscdim_rec.object_desc,
                                    L_iscdim_rec.method_desc,
                                    L_iscdim_rec.dim_object,
                                    L_iscdim_rec.presentation_method) =
         FALSE then
        return FALSE;
      end if;
    
      O_message.EXTEND();
      O_message(O_message.COUNT) := L_iscdim_rec;
    
      --- add rowid to TBL of rowids that will be deleted
      O_rowids_rec.queue_rowid_tbl.EXTEND;
      O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_iscdim_dtl_rec.q_rowid;
      O_rowids_rec.queue_seq_tbl.EXTEND;
      O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_iscdim_dtl_rec.q_seq_no;
      ---
    
      L_details_processed := TRUE;
    
    end if;
  
    -- if no data found in cursor, raise error
    if L_details_processed = FALSE and
       I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
      --- if there are no dimension records in the Item create message,
      --- we need to initialize the dimension table to be atomically NULL
      O_message := NULL;
    elsif L_details_processed = FALSE then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_DIM_DETAIL;
  
  
   -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_UDA_LOV_DETAIL
  -- Description: This function is responsible for create ItemUDALOVDesc message
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_UDA_LOV_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_message       IN OUT NOCOPY "RIB_ItemUDALOVDesc_TBL",
                                O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                O_record_exists IN OUT BOOLEAN,
                                I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                                I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_UDA_LOV_DETAIL';
  
    L_udalov_rec        "RIB_ItemUDALOVDesc_REC" := NULL;
    L_details_processed BOOLEAN := FALSE;
  
    cursor C_UDALOV_MC is
      select uil.uda_id, uil.uda_value, NULL q_rowid, NULL q_seq_no
        from uda_item_lov uil
       where uil.item = I_item
      UNION ALL
      select q.uda_id, NULL uda_value, q.rowid q_rowid, q.seq_no q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.message_type in
             (RMSMFM_ITEMS.UDAL_ADD, RMSMFM_ITEMS.UDAL_DEL);
  
    TYPE UDALOV_MC_TBL is TABLE OF C_UDALOV_MC%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_udalov_mc_tbl UDALOV_MC_TBL;
  
    cursor C_UDALOV_DTL is
      select q.message_type,
             uil.uda_id,
             uil.uda_value,
             q.seq_no,
             q.rowid        q_rowid,
             q.seq_no       q_seq_no
        from uda_item_lov uil, item_mfqueue q
       where q.item = I_item
         and q.seq_no = I_seq_no
         and uil.item = q.item
         and uil.uda_id = q.uda_id
         and uil.uda_value = q.uda_value;
  
    L_udalov_dtl_rec C_UDALOV_DTL%ROWTYPE;
  
  BEGIN
  
    L_udalov_rec := "RIB_ItemUDALOVDesc_REC"(0,
                                             I_item, --- item
                                             NULL, --- uda_id
                                             NULL); --- uda_value
  
    O_message := "RIB_ItemUDALOVDesc_TBL"();
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_UDALOV_MC;
      fetch C_UDALOV_MC BULK COLLECT
        INTO L_udalov_mc_tbl;
      close C_UDALOV_MC;
    
      FOR i IN 1 .. L_udalov_mc_tbl.COUNT LOOP
      
        if L_udalov_mc_tbl(i).q_rowid is not NULL then
          ---  add rowids to TBLs of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_udalov_mc_tbl(i)
                                                                              .q_rowid;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_udalov_mc_tbl(i)
                                                                          .q_seq_no;
        else
          L_udalov_rec.uda_id    := L_udalov_mc_tbl(i).uda_id;
          L_udalov_rec.uda_value := L_udalov_mc_tbl(i).uda_value;
        
          O_message.EXTEND();
          O_message(O_message.COUNT) := L_udalov_rec;
          ---
          L_details_processed := TRUE;
        end if;
      
      END LOOP;
    
    else
    
      open C_UDALOV_DTL;
      fetch C_UDALOV_DTL
        INTO L_udalov_dtl_rec;
      close C_UDALOV_DTL;
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
      if L_udalov_dtl_rec.uda_id is NULL then
        O_record_exists := FALSE;
        return TRUE;
      end if;
    
      L_udalov_rec.uda_id    := L_udalov_dtl_rec.uda_id;
      L_udalov_rec.uda_value := L_udalov_dtl_rec.uda_value;
    
      O_message.EXTEND();
      O_message(O_message.COUNT) := L_udalov_rec;
    
      --- add rowid to TBL of rowids that will be deleted
      O_rowids_rec.queue_rowid_tbl.EXTEND;
      O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_udalov_dtl_rec.q_rowid;
      O_rowids_rec.queue_seq_tbl.EXTEND;
      O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_udalov_dtl_rec.q_seq_no;
      ---
    
      L_details_processed := TRUE;
    
    end if;
  
    -- if no data found in cursor, raise error
    if L_details_processed = FALSE and
       I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
      --- if there are no UDA records in the Item create message,
      --- we need to initialize the UDA table to be atomically NULL
      O_message := NULL;
    elsif L_details_processed = FALSE then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_UDA_LOV_DETAIL;
  
  
   -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_UDA_FF_DETAIL
  -- Description: This function is responsible for create ItemUDAFFDesc message
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_UDA_FF_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message       IN OUT NOCOPY "RIB_ItemUDAFFDesc_TBL",
                               O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                               O_record_exists IN OUT BOOLEAN,
                               I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                               I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                               I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_UDA_FF_DETAIL';
  
    L_udaff_rec         "RIB_ItemUDAFFDesc_REC" := NULL;
    L_details_processed BOOLEAN := FALSE;
  
    cursor C_UDAFF_MC is
      select uif.uda_id, uif.uda_text, NULL q_rowid, NULL q_seq_no
        from uda_item_ff uif
       where uif.item = I_item
      UNION ALL
      select q.uda_id, NULL uda_text, q.rowid q_rowid, q.seq_no q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.message_type in
             (RMSMFM_ITEMS.UDAF_ADD, RMSMFM_ITEMS.UDAF_DEL);
  
    TYPE UDAFF_MC_TBL is TABLE OF C_UDAFF_MC%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_udaff_mc_tbl UDAFF_MC_TBL;
  
    cursor C_UDAFF_DTL is
      select q.message_type,
             uif.uda_id,
             uif.uda_text,
             q.seq_no       q_seq_no,
             q.rowid        q_rowid
        from uda_item_ff uif, item_mfqueue q
       where q.item = I_item
         and q.seq_no = I_seq_no
         and uif.item = q.item
         and uif.uda_id = q.uda_id
         and uif.uda_text = q.uda_text
         and to_char(q.transaction_time_stamp, 'YYYYMMDD HH24:MI:SS') =
             to_char(uif.last_update_datetime, 'YYYYMMDD HH24:MI:SS');
  
    L_udaff_dtl_rec C_UDAFF_DTL%ROWTYPE;
  
  BEGIN
  
    L_udaff_rec := "RIB_ItemUDAFFDesc_REC"(0,
                                           I_item, --- item
                                           NULL, --- uda_id
                                           NULL); --- uda_text
  
    O_message := "RIB_ItemUDAFFDesc_TBL"();
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_UDAFF_MC;
      fetch C_UDAFF_MC BULK COLLECT
        INTO L_udaff_mc_tbl;
      close C_UDAFF_MC;
    
      FOR i IN 1 .. L_udaff_mc_tbl.COUNT LOOP
      
        if L_udaff_mc_tbl(i).q_rowid is not NULL then
          ---  add rowids to TBLs of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_udaff_mc_tbl(i)
                                                                              .q_rowid;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_udaff_mc_tbl(i)
                                                                          .q_seq_no;
        else
          L_udaff_rec.uda_id   := L_udaff_mc_tbl(i).uda_id;
          L_udaff_rec.uda_text := L_udaff_mc_tbl(i).uda_text;
        
          O_message.EXTEND();
          O_message(O_message.COUNT) := L_udaff_rec;
          ---
          L_details_processed := TRUE;
        end if;
      
      END LOOP;
    
    else
    
      open C_UDAFF_DTL;
      fetch C_UDAFF_DTL
        INTO L_udaff_dtl_rec;
      close C_UDAFF_DTL;
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
      if L_udaff_dtl_rec.uda_id is NULL then
        O_record_exists := FALSE;
        return TRUE;
      end if;
    
      L_udaff_rec.uda_id   := L_udaff_dtl_rec.uda_id;
      L_udaff_rec.uda_text := L_udaff_dtl_rec.uda_text;
    
      O_message.EXTEND();
      O_message(O_message.COUNT) := L_udaff_rec;
    
      --- add rowid to TBL of rowids that will be deleted
      O_rowids_rec.queue_rowid_tbl.EXTEND;
      O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_udaff_dtl_rec.q_rowid;
      O_rowids_rec.queue_seq_tbl.EXTEND;
      O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_udaff_dtl_rec.q_seq_no;
      ---
    
      L_details_processed := TRUE;
    
    end if;
  
    -- if no data found in cursor, raise error
    if L_details_processed = FALSE and
       I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
      --- if there are no UDA records in the Item create message,
      --- we need to initialize the UDA table to be atomically NULL
      O_message := NULL;
    elsif L_details_processed = FALSE then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_UDA_FF_DETAIL;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_UDA_DATE_DETAIL
  -- Description: This function is responsible for create ItemUDADATEDesc message
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_UDA_DATE_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_message       IN OUT NOCOPY "RIB_ItemUDADateDesc_TBL",
                                 O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                 O_record_exists IN OUT BOOLEAN,
                                 I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                                 I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                 I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_UDA_DATE_DETAIL';
  
    L_udadate_rec       "RIB_ItemUDADateDesc_REC" := NULL;
    L_details_processed BOOLEAN := FALSE;
  
    cursor C_UDADATE_MC is
      select uit.uda_id, uit.uda_date, NULL q_rowid, NULL q_seq_no
        from uda_item_date uit
       where uit.item = I_item
      UNION ALL
      select q.uda_id, NULL uda_date, q.rowid q_rowid, q.seq_no q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.message_type in
             (XXADEO_RMSMFM_ITEMS.UDAD_ADD, XXADEO_RMSMFM_ITEMS.UDAD_DEL);
  
    TYPE UDADATE_MC_TBL is TABLE OF C_UDADATE_MC%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_udadate_mc_tbl UDADATE_MC_TBL;
  
    cursor C_UDADATE_DTL is
      select q.message_type,
             uit.uda_id,
             uit.uda_date,
             q.seq_no       q_seq_no,
             q.rowid        q_rowid
        from uda_item_date uit, item_mfqueue q
       where q.item = I_item
         and q.seq_no = I_seq_no
         and uit.item = q.item
         and uit.uda_id = q.uda_id
         and uit.uda_date = q.uda_date
         and to_char(q.transaction_time_stamp, 'YYYYMMDD HH24:MI:SS') =
             to_char(uit.last_update_datetime, 'YYYYMMDD HH24:MI:SS');
  
    L_udadate_dtl_rec C_UDADATE_DTL%ROWTYPE;
  
  BEGIN
    L_udadate_rec := "RIB_ItemUDADateDesc_REC"(0,
                                               I_item, --- item
                                               NULL, --- uda_id
                                               NULL); --- uda_date
  
    O_message := "RIB_ItemUDADateDesc_TBL"();
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_UDADATE_MC;
      fetch C_UDADATE_MC BULK COLLECT
        INTO L_udadate_mc_tbl;
      close C_UDADATE_MC;
    
      FOR i IN 1 .. L_udadate_mc_tbl.COUNT LOOP
      
        if L_udadate_mc_tbl(i).q_rowid is not NULL then
          ---  add rowids to TBLs of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_udadate_mc_tbl(i)
                                                                              .q_rowid;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_udadate_mc_tbl(i)
                                                                          .q_seq_no;
        else
          L_udadate_rec.uda_id   := L_udadate_mc_tbl(i).uda_id;
          L_udadate_rec.uda_date := L_udadate_mc_tbl(i).uda_date;
          ---
          O_message.EXTEND();
          O_message(O_message.COUNT) := L_udadate_rec;
          ---
          L_details_processed := TRUE;
        end if;
      
      END LOOP;
    
    else
    
      open C_UDADATE_DTL;
      fetch C_UDADATE_DTL
        INTO L_udadate_dtl_rec;
      close C_UDADATE_DTL;
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
      if L_udadate_dtl_rec.uda_id is NULL then
        O_record_exists := FALSE;
        return TRUE;
      end if;
    
      L_udadate_rec.uda_id   := L_udadate_dtl_rec.uda_id;
      L_udadate_rec.uda_date := L_udadate_dtl_rec.uda_date;
    
      O_message.EXTEND();
      O_message(O_message.COUNT) := L_udadate_rec;
    
      --- add rowid to TBL of rowids that will be deleted
      O_rowids_rec.queue_rowid_tbl.EXTEND;
      O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_udadate_dtl_rec.q_rowid;
      O_rowids_rec.queue_seq_tbl.EXTEND;
      O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_udadate_dtl_rec.q_seq_no;
      ---
    
      L_details_processed := TRUE;
    
    end if;
  
    -- if no data found in cursor, raise error
    if L_details_processed = FALSE and
       I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
      --- if there are no UDA records in the Item create message,
      --- we need to initialize the UDA table to be atomically NULL
      O_message := NULL;
    elsif L_details_processed = FALSE then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_UDA_DATE_DETAIL;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_IMAGE_DETAIL
  -- Description: This function is responsible to create ItemImageDesc message
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_IMAGE_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_message       IN OUT NOCOPY "RIB_ItemImageDesc_TBL",
                              O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                              O_record_exists IN OUT BOOLEAN,
                              I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                              I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module            VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_IMAGE_DETAIL';
    L_image_rec         "RIB_ItemImageDesc_REC" := NULL;
    L_details_processed BOOLEAN := FALSE;
  
    cursor C_IMAGE_MC is
      select iim.image_name,
             iim.image_addr,
             iim.image_type,
             iim.primary_ind,
             iim.display_priority,
             NULL                 q_rowid,
             NULL                 q_seq_no
        from item_image iim
       where iim.item = I_item
      UNION ALL
      select q.image_name,
             NULL         image_addr,
             NULL         image_type,
             NULL         primary_ind,
             NULL         display_priority,
             q.rowid      q_rowid,
             q.seq_no     q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.message_type in
             (XXADEO_RMSMFM_ITEMS.IMG_ADD,
              XXADEO_RMSMFM_ITEMS.IMG_UPD,
              XXADEO_RMSMFM_ITEMS.IMG_DEL);
  
    TYPE IMAGE_MC_TBL is TABLE OF C_IMAGE_MC%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_image_mc_tbl IMAGE_MC_TBL;
  
    cursor C_IMAGE_DTL is
      select q.message_type,
             iim.image_name,
             iim.image_addr,
             iim.image_type,
             iim.primary_ind,
             iim.display_priority,
             q.seq_no             q_seq_no,
             q.rowid              q_rowid
        from item_image iim, item_mfqueue q
       where q.item = I_item
         and q.seq_no = I_seq_no
         and iim.item = q.item
         and iim.image_name = q.image_name;
  
    L_image_dtl_rec C_IMAGE_DTL%ROWTYPE;
  
  BEGIN
  
    L_image_rec := "RIB_ItemImageDesc_REC"(0,
                                           I_item, --- item
                                           NULL, --- image_name
                                           NULL, --- image_addr
                                           NULL, --- image_type
                                           NULL, --- primary_ind
                                           NULL); --- display_priority
  
    O_message := "RIB_ItemImageDesc_TBL"();
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_IMAGE_MC;
      fetch C_IMAGE_MC BULK COLLECT
        INTO L_image_mc_tbl;
      close C_IMAGE_MC;
    
      FOR i IN 1 .. L_image_mc_tbl.COUNT LOOP
      
        if L_image_mc_tbl(i).q_rowid is not NULL then
          ---  add rowids to TBLs of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_image_mc_tbl(i)
                                                                              .q_rowid;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_image_mc_tbl(i)
                                                                          .q_seq_no;
        else
        
          L_image_rec.image_name       := L_image_mc_tbl(i).image_name;
          L_image_rec.image_addr       := L_image_mc_tbl(i).image_addr;
          L_image_rec.image_type       := L_image_mc_tbl(i).image_type;
          L_image_rec.primary_ind      := L_image_mc_tbl(i).primary_ind;
          L_image_rec.display_priority := L_image_mc_tbl(i).display_priority;
        
          O_message.EXTEND();
          O_message(O_message.COUNT) := L_image_rec;
          ---
          L_details_processed := TRUE;
        end if;
      
      END LOOP;
    
    else
    
      open C_IMAGE_DTL;
      fetch C_IMAGE_DTL
        INTO L_image_dtl_rec;
      close C_IMAGE_DTL;
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
      if L_image_dtl_rec.image_name is NULL then
        O_record_exists := FALSE;
        return TRUE;
      end if;
    
      L_image_rec.image_name       := L_image_dtl_rec.image_name;
      L_image_rec.image_addr       := L_image_dtl_rec.image_addr;
      L_image_rec.image_type       := L_image_dtl_rec.image_type;
      L_image_rec.primary_ind      := L_image_dtl_rec.primary_ind;
      L_image_rec.display_priority := L_image_dtl_rec.display_priority;
    
      O_message.EXTEND();
      O_message(O_message.COUNT) := L_image_rec;
    
      --- add rowid to TBL of rowids that will be deleted
      O_rowids_rec.queue_rowid_tbl.EXTEND;
      O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_image_dtl_rec.q_rowid;
      O_rowids_rec.queue_seq_tbl.EXTEND;
      O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_image_dtl_rec.q_seq_no;
      ---
    
      L_details_processed := TRUE;
    
    end if;
  
    -- if no data found in cursor, raise error
    if L_details_processed = FALSE and
       I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
      --- if there are no image records in the Item create message,
      --- we need to initialize the image table to be atomically NULL
      O_message := NULL;
    elsif L_details_processed = FALSE then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_IMAGE_DETAIL;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_UPC_DETAIL
  -- Description: this fucntion is responsible to create ItemUPCDesc messages
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_UPC_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_message       IN OUT NOCOPY "RIB_ItemUPCDesc_TBL",
                            O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                            O_record_exists IN OUT BOOLEAN,
                            I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                            I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                            I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_UPC_DETAIL';
  
    L_itemupc_rec       "RIB_ItemUPCDesc_REC" := NULL;
    L_details_processed BOOLEAN := FALSE;
  
    cursor C_ITEMUPC_MC is
      select im.item,
             im.primary_ref_item_ind,
             im.format_id,
             im.prefix,
             im.item_number_type,
             NULL                    q_rowid,
             NULL                    q_seq_no
        from item_master im
       where im.item_parent = I_item
         and im.status = 'A'
      UNION ALL
      select q.ref_item,
             NULL       primary_ref_item_ind,
             NULL       format_id,
             NULL       prefix,
             NULL       item_number_type,
             q.rowid    q_rowid,
             q.seq_no   q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.message_type in
             (XXADEO_RMSMFM_ITEMS.UPC_ADD,
              XXADEO_RMSMFM_ITEMS.UPC_UPD,
              XXADEO_RMSMFM_ITEMS.UPC_DEL);
  
    TYPE ITEMUPC_MC_TBL is TABLE OF C_ITEMUPC_MC%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_itemupc_mc_tbl ITEMUPC_MC_TBL;
  
    cursor C_ITEMUPC_DTL is
      select q.message_type,
             im.item,
             im.primary_ref_item_ind,
             im.format_id,
             im.prefix,
             im.item_number_type,
             q.seq_no                q_seq_no,
             q.rowid                 q_rowid
        from item_master im, item_mfqueue q
       where q.item = I_item
         and q.seq_no = I_seq_no
         and im.item_parent = q.item
         and im.item = q.ref_item;
  
    L_itemupc_dtl_rec C_ITEMUPC_DTL%ROWTYPE;
  
  BEGIN
  
    L_itemupc_rec := "RIB_ItemUPCDesc_REC"(0,
                                           NULL, --- item_id
                                           I_item, --- item_parent
                                           NULL, --- primary_ref_item_ind
                                           NULL, --- format_id
                                           NULL, --- prefix
                                           NULL); --- item_number_type
  
    O_message := "RIB_ItemUPCDesc_TBL"();
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_ITEMUPC_MC;
      fetch C_ITEMUPC_MC BULK COLLECT
        INTO L_itemupc_mc_tbl;
      close C_ITEMUPC_MC;
    
      FOR i IN 1 .. L_itemupc_mc_tbl.COUNT LOOP
      
        if L_itemupc_mc_tbl(i).q_rowid is not NULL then
          ---  add rowids to TBLs of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_itemupc_mc_tbl(i)
                                                                              .q_rowid;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_itemupc_mc_tbl(i)
                                                                          .q_seq_no;
        else
          L_itemupc_rec.item_id              := L_itemupc_mc_tbl(i).item;
          L_itemupc_rec.primary_ref_item_ind := L_itemupc_mc_tbl(i)
                                                .primary_ref_item_ind;
          L_itemupc_rec.format_id            := L_itemupc_mc_tbl(i)
                                                .format_id;
          L_itemupc_rec.prefix               := L_itemupc_mc_tbl(i).prefix;
          L_itemupc_rec.item_number_type     := L_itemupc_mc_tbl(i)
                                                .item_number_type;
        
          O_message.EXTEND();
          O_message(O_message.COUNT) := L_itemupc_rec;
          ---
          L_details_processed := TRUE;
        end if;
      
      END LOOP;
    
    else
    
      open C_ITEMUPC_DTL;
      fetch C_ITEMUPC_DTL
        INTO L_itemupc_dtl_rec;
      close C_ITEMUPC_DTL;
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
      if L_itemupc_dtl_rec.item is NULL then
        O_record_exists := FALSE;
        return TRUE;
      end if;
    
      L_itemupc_rec.item_id              := L_itemupc_dtl_rec.item;
      L_itemupc_rec.primary_ref_item_ind := L_itemupc_dtl_rec.primary_ref_item_ind;
      L_itemupc_rec.format_id            := L_itemupc_dtl_rec.format_id;
      L_itemupc_rec.prefix               := L_itemupc_dtl_rec.prefix;
      L_itemupc_rec.item_number_type     := L_itemupc_dtl_rec.item_number_type;
    
      O_message.EXTEND();
      O_message(O_message.COUNT) := L_itemupc_rec;
    
      --- add rowid to TBL of rowids that will be deleted
      O_rowids_rec.queue_rowid_tbl.EXTEND;
      O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_itemupc_dtl_rec.q_rowid;
      O_rowids_rec.queue_seq_tbl.EXTEND;
      O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_itemupc_dtl_rec.q_seq_no;
      ---
    
      L_details_processed := TRUE;
    
    end if;
  
    -- if no data found in cursor, raise error
    if L_details_processed = FALSE and
       I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
      --- if there are no UPC records in the Item create message,
      --- we need to initialize the UPC table to be atomically NULL
      O_message := NULL;
    elsif L_details_processed = FALSE then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_UPC_DETAIL;

  
  -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_BOM_DETAIL
  -- Description: This function is responsible for create ItemBOMDesc message
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_BOM_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_message       IN OUT NOCOPY "RIB_ItemBOMDesc_TBL",
                            O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                            O_record_exists IN OUT BOOLEAN,
                            I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                            I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                            I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_BOM_DETAIL';
  
    L_bom_rec           "RIB_ItemBOMDesc_REC" := NULL;
    L_details_processed BOOLEAN := FALSE;
    L_prev_comp_item    ITEM_MFQUEUE.PACK_COMP%TYPE := NULL;
  
    cursor C_BOM_MC is
      select NVL(pks.comp_pack_no, pks.item) item,
             NVL(pks.comp_pack_qty, pks.pack_item_qty) pack_item_qty,
             NULL q_rowid,
             NULL q_seq_no
        from packitem_breakout pks
       where pks.pack_no = I_item
       group by NVL(pks.comp_pack_no, pks.item),
                NVL(pks.comp_pack_qty, pks.pack_item_qty)
      UNION ALL
      select q.pack_comp,
             NULL        pack_item_qty,
             q.rowid     q_rowid,
             q.seq_no    q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.message_type in
             (XXADEO_RMSMFM_ITEMS.BOM_ADD,
              XXADEO_RMSMFM_ITEMS.BOM_UPD,
              XXADEO_RMSMFM_ITEMS.BOM_DEL)
       order by 1;
  
    TYPE BOM_MC_TBL is TABLE OF C_BOM_MC%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_bom_mc_tbl BOM_MC_TBL;
  
    cursor C_BOM_DTL is
      select q.message_type,
             pkdtl.item,
             pkdtl.qty,
             q.seq_no,
             q.rowid        q_rowid,
             q.seq_no       q_seq_no
        from (select pks.pack_no,
                     NVL(pks.comp_pack_no, pks.item) item,
                     NVL(pks.comp_pack_qty, pks.pack_item_qty) qty
                from packitem_breakout pks
               where pks.pack_no = I_item
               group by pks.pack_no,
                        NVL(pks.comp_pack_no, pks.item),
                        NVL(pks.comp_pack_qty, pks.pack_item_qty)) pkdtl,
             item_mfqueue q
       where q.item = I_item
         and q.seq_no = I_seq_no
         and pkdtl.pack_no = q.item
         and pkdtl.item = q.pack_comp;
  
    L_bom_dtl_rec C_BOM_DTL%ROWTYPE;
  
  BEGIN
  
    L_bom_rec := "RIB_ItemBOMDesc_REC"(0,
                                       I_item, --- pack_no
                                       NULL, --- item
                                       NULL); --- pack_qty
  
    O_message := "RIB_ItemBOMDesc_TBL"();
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_BOM_MC;
      fetch C_BOM_MC BULK COLLECT
        INTO L_bom_mc_tbl;
      close C_BOM_MC;
    
      FOR i IN 1 .. L_bom_mc_tbl.COUNT LOOP
      
        if L_bom_mc_tbl(i).q_rowid is not NULL then
          --- add rowid to TBL of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_bom_mc_tbl(i)
                                                                              .q_rowid;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_bom_mc_tbl(i)
                                                                          .q_seq_no;
        else
          if L_bom_mc_tbl(i).item = L_prev_comp_item then
            O_message(O_message.COUNT).pack_qty := O_message(O_message.COUNT)
                                                   .pack_qty + L_bom_mc_tbl(i)
                                                   .pack_item_qty;
          else
            L_bom_rec.item     := L_bom_mc_tbl(i).item;
            L_bom_rec.pack_qty := L_bom_mc_tbl(i).pack_item_qty;
            ---
            O_message.EXTEND();
            O_message(O_message.COUNT) := L_bom_rec;
          end if;
          ---
          L_prev_comp_item    := L_bom_mc_tbl(i).item;
          L_details_processed := TRUE;
        end if;
      
      END LOOP;
    
    else
    
      open C_BOM_DTL;
      fetch C_BOM_DTL
        INTO L_bom_dtl_rec;
      close C_BOM_DTL;
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
      if L_bom_dtl_rec.item is NULL then
        O_record_exists := FALSE;
        return TRUE;
      end if;
    
      L_bom_rec.item     := L_bom_dtl_rec.item;
      L_bom_rec.pack_qty := L_bom_dtl_rec.qty;
      ---
      O_message.EXTEND();
      O_message(O_message.COUNT) := L_bom_rec;
    
      --- add rowid to TBL of rowids that will be deleted
      O_rowids_rec.queue_rowid_tbl.EXTEND;
      O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_bom_dtl_rec.q_rowid;
      O_rowids_rec.queue_seq_tbl.EXTEND;
      O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_bom_dtl_rec.q_seq_no;
      ---
      L_details_processed := TRUE;
    
    end if;
  
    -- if no data found in cursor, raise error
    if L_details_processed = FALSE and
       I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
      --- if there are no dimension records in the Item create message,
      --- we need to initialize the BOM table to be atomically NULL
      O_message := NULL;
    elsif L_details_processed = FALSE then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_BOM_DETAIL;
  
  
   -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_TICKET_DETAIL 
  -- Description: This function is responsible to create ItemTcktDesc message
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_TICKET_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message       IN OUT NOCOPY "RIB_ItemTcktDesc_TBL",
                               O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                               O_record_exists IN OUT BOOLEAN,
                               I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                               I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                               I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_TICKET_DETAIL';
  
    L_ticket_rec        "RIB_ItemTcktDesc_REC" := NULL;
    L_details_processed BOOLEAN := FALSE;
  
    cursor C_TICKET_MC is
      select itk.ticket_type_id, NULL q_rowid, NULL q_seq_no
        from item_ticket itk
       where itk.item = I_item
      UNION ALL
      select q.ticket_type_id, q.rowid q_rowid, q.seq_no q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.message_type in
             (RMSMFM_ITEMS.TCKT_ADD, RMSMFM_ITEMS.TCKT_DEL);
  
    TYPE TICKET_MC_TBL is TABLE OF C_TICKET_MC%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_ticket_mc_tbl TICKET_MC_TBL;
  
    cursor C_TICKET_DTL is
      select q.ticket_type_id, q.rowid q_rowid, q.seq_no q_seq_no
        from item_mfqueue q
       where q.seq_no = I_seq_no;
  
    L_ticket_dtl_rec C_TICKET_DTL%ROWTYPE;
  
  BEGIN
  
    L_ticket_rec := "RIB_ItemTcktDesc_REC"(0,
                                           I_item, --- item
                                           NULL); --- ticket_type_id
  
    O_message := "RIB_ItemTcktDesc_TBL"();
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_TICKET_MC;
      fetch C_TICKET_MC BULK COLLECT
        INTO L_ticket_mc_tbl;
      close C_TICKET_MC;
    
      FOR i IN 1 .. L_ticket_mc_tbl.COUNT LOOP
      
        if L_ticket_mc_tbl(i).q_rowid is not NULL then
          ---  add rowids to TBLs of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_ticket_mc_tbl(i)
                                                                              .q_rowid;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_ticket_mc_tbl(i)
                                                                          .q_seq_no;
        else
          L_ticket_rec.ticket_type_id := L_ticket_mc_tbl(i).ticket_type_id;
          ---
          O_message.EXTEND();
          O_message(O_message.COUNT) := L_ticket_rec;
          ---
          L_details_processed := TRUE;
        end if;
      
      END LOOP;
    
    else
    
      open C_TICKET_DTL;
      fetch C_TICKET_DTL
        INTO L_ticket_dtl_rec;
      close C_TICKET_DTL;
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
      if L_ticket_dtl_rec.ticket_type_id is NULL then
        O_record_exists := FALSE;
        return TRUE;
      end if;
    
      L_ticket_rec.ticket_type_id := L_ticket_dtl_rec.ticket_type_id;
    
      O_message.EXTEND();
      O_message(O_message.COUNT) := L_ticket_rec;
    
      --- add rowid to TBL of rowids that will be deleted
      O_rowids_rec.queue_rowid_tbl.EXTEND;
      O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_ticket_dtl_rec.q_rowid;
      O_rowids_rec.queue_seq_tbl.EXTEND;
      O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_ticket_dtl_rec.q_seq_no;
      ---
      L_details_processed := TRUE;
    
    end if;
  
    -- if no data found in cursor, raise error
    if L_details_processed = FALSE and
       I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
      --- if there are no ticket records in the Item create message,
      --- we need to initialize the ticket table to be atomically NULL
      O_message := NULL;
    elsif L_details_processed = FALSE then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_TICKET_DETAIL;
 
 
  -------------------------------------------------------------------------------------------------------
  -- Name: GET_ITEM_INFO 
  -- Description: This function is responsible to get information about an item
  -------------------------------------------------------------------------------------------------------
  FUNCTION GET_ITEM_INFO(O_error_message   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_info_rec   OUT ITEM_INFO,
                         I_item_master_rec IN ITEM_MASTER%ROWTYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.GET_ITEM_INFO';
  
  BEGIN
  
    -- Get code descriptions
    if I_item_master_rec.retail_label_type is NOT NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'RTLT',
                                    I_item_master_rec.retail_label_type,
                                    O_item_info_rec.retail_label_desc) =
         FALSE then
        return FALSE;
      end if;
    end if;
    ---
    if I_item_master_rec.handling_temp is NOT NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'HTMP',
                                    I_item_master_rec.handling_temp,
                                    O_item_info_rec.handling_temp_desc) =
         FALSE then
        return FALSE;
      end if;
    end if;
    ---
    if I_item_master_rec.handling_sensitivity is NOT NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'HSEN',
                                    I_item_master_rec.handling_sensitivity,
                                    O_item_info_rec.handling_sensitivity_desc) =
         FALSE then
        return FALSE;
      end if;
    end if;
    ---
    if I_item_master_rec.waste_type is NOT NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'WSTG',
                                    I_item_master_rec.waste_type,
                                    O_item_info_rec.waste_type_desc) =
         FALSE then
        return FALSE;
      end if;
    end if;
  
    -- Get merchandise hierarchy descriptions
    if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                I_item_master_rec.dept,
                                O_item_info_rec.dept_desc) = FALSE then
      return FALSE;
    end if;
    ---
    if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                 I_item_master_rec.dept,
                                 I_item_master_rec.class,
                                 O_item_info_rec.class_desc) = FALSE then
      return FALSE;
    end if;
    ---
    if SUBCLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                    I_item_master_rec.dept,
                                    I_item_master_rec.class,
                                    I_item_master_rec.subclass,
                                    O_item_info_rec.subclass_desc) = FALSE then
      return FALSE;
    end if;
  
    if I_item_master_rec.diff_1 is NOT NULL then
      if DIFF_SQL.GET_DIFF_TYPE(O_error_message,
                                O_item_info_rec.diff_1_type,
                                I_item_master_rec.diff_1) = FALSE then
        return FALSE;
      end if;
    end if;
  
    if I_item_master_rec.diff_2 is NOT NULL then
      if DIFF_SQL.GET_DIFF_TYPE(O_error_message,
                                O_item_info_rec.diff_2_type,
                                I_item_master_rec.diff_2) = FALSE then
        return FALSE;
      end if;
    end if;
  
    if I_item_master_rec.diff_3 is NOT NULL then
      if DIFF_SQL.GET_DIFF_TYPE(O_error_message,
                                O_item_info_rec.diff_3_type,
                                I_item_master_rec.diff_3) = FALSE then
        return FALSE;
      end if;
    end if;
  
    if I_item_master_rec.diff_4 is NOT NULL then
      if DIFF_SQL.GET_DIFF_TYPE(O_error_message,
                                O_item_info_rec.diff_4_type,
                                I_item_master_rec.diff_4) = FALSE then
        return FALSE;
      end if;
    end if;
  
    -- only pass a value into the downstream system for unit retail if the item is a sellable item.
    if I_item_master_rec.sellable_ind = 'Y' then
      if ITEM_PRICING_SQL.GET_BASE_RETAIL(O_error_message,
                                          O_item_info_rec.unit_retail,
                                          I_item_master_rec.item) = FALSE then
        return FALSE;
      end if;
    else
      O_item_info_rec.unit_retail := NULL;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            to_char(SQLCODE));
      return FALSE;
    
  END GET_ITEM_INFO;
  
  
   -------------------------------------------------------------------------------------------------------
  -- Name: GET_DIMENSION_DESCRIPTIONS
  -- Description: This function is responsible to get information about dimension of an item
  -------------------------------------------------------------------------------------------------------
  FUNCTION GET_DIMENSION_DESCRIPTIONS(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_dim_object_desc     OUT CODE_DETAIL.CODE_DESC%TYPE,
                                      O_pres_method_desc    OUT CODE_DETAIL.CODE_DESC%TYPE,
                                      I_dimension_object    IN ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE,
                                      I_presentation_method IN ITEM_SUPP_COUNTRY_DIM.PRESENTATION_METHOD%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'RMSMFM_ITEMS_BUILD.GET_DIMENSION_DESCRIPTIONS';
  
  BEGIN
  
    -- Get code descriptions
    if I_dimension_object is NOT NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'DIMO',
                                    I_dimension_object,
                                    O_dim_object_desc) = FALSE then
        return FALSE;
      end if;
    end if;
    ---
    if I_presentation_method is NOT NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'PCKT',
                                    I_presentation_method,
                                    O_pres_method_desc) = FALSE then
        return FALSE;
      end if;
    end if;
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            to_char(SQLCODE));
      return FALSE;
    
  END GET_DIMENSION_DESCRIPTIONS;
  
  
   -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_MANU_COUNTRY_DETAIL
  -- Description: This function is responsible for create ItemSupCtyMfrDesc message
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_MANU_COUNTRY_DETAIL(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_message       IN OUT NOCOPY "RIB_ItemSupCtyMfrDesc_TBL",
                                     O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                     O_record_exists IN OUT BOOLEAN,
                                     I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                                     I_orderable_ind IN ITEM_MASTER.ORDERABLE_IND%TYPE,
                                     I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                     I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_MANU_COUNTRY_DETAIL';
  
    L_itemsupctymfr_rec "RIB_ItemSupCtyMfrDesc_REC" := NULL;
    L_details_processed BOOLEAN := FALSE;
  
    cursor C_ISMC_IN_ITEM is
      select ismc.item,
             ismc.supplier,
             ismc.manu_country_id,
             ismc.primary_manu_ctry_ind,
             NULL                       q_row_id,
             NULL                       q_seq_no
        from item_supp_manu_country ismc
       where ismc.item = I_item
      UNION ALL
      select q.item,
             NULL     supplier,
             NULL     manu_country_id,
             NULL     primary_manu_ctry_ind,
             q.rowid  q_row_id,
             q.seq_no q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.message_type in
             (XXADEO_RMSMFM_ITEMS.ISMC_ADD,
              XXADEO_RMSMFM_ITEMS.ISMC_UPD,
              XXADEO_RMSMFM_ITEMS.ISMC_DEL);
  
    TYPE ISMC_MC_TBL IS TABLE OF C_ISMC_IN_ITEM%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_ismc_mc_tbl ISMC_MC_TBL;
  
    cursor C_ISMC_ISC is
      select ismc.item,
             ismc.supplier,
             ismc.manu_country_id,
             ismc.primary_manu_ctry_ind,
             q.seq_no                   q_seq_no,
             q.rowid                    q_row_id
        from item_supp_manu_country ismc, item_mfqueue q
       where q.item = I_item
         and q.seq_no = I_seq_no
         and ismc.item = q.item
         and ismc.supplier = q.supplier
         and ismc.manu_country_id = q.country_id;
  
    L_ismc_isc_rec C_ISMC_ISC%ROWTYPE;
  
  BEGIN
  
    L_itemsupctymfr_rec := "RIB_ItemSupCtyMfrDesc_REC"(0,
                                                       I_item,
                                                       NULL, -- Supplier
                                                       NULL, -- manufacturer_ctry_id
                                                       NULL); -- primary_manufacturer_ctry_ind
    O_message           := "RIB_ItemSupCtyMfrDesc_TBL"();
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_ISMC_IN_ITEM;
      fetch C_ISMC_IN_ITEM BULK COLLECT
        INTO L_ismc_mc_tbl;
      close C_ISMC_IN_ITEM;
    
      FOR i in 1 .. L_ismc_mc_tbl.COUNT LOOP
      
        if L_ismc_mc_tbl(i).q_row_id IS NOT NULL then
          ---  add rowids to TBLs of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_ismc_mc_tbl(i)
                                                                              .q_row_id;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_ismc_mc_tbl(i)
                                                                          .q_seq_no;
        else
          L_itemsupctymfr_rec.item                          := L_ismc_mc_tbl(i).item;
          L_itemsupctymfr_rec.supplier                      := L_ismc_mc_tbl(i)
                                                               .supplier;
          L_itemsupctymfr_rec.manufacturer_ctry_id          := L_ismc_mc_tbl(i)
                                                               .manu_country_id;
          L_itemsupctymfr_rec.primary_manufacturer_ctry_ind := L_ismc_mc_tbl(i)
                                                               .primary_manu_ctry_ind;
        
          O_message.EXTEND();
          O_message(O_message.COUNT) := L_itemsupctymfr_rec;
          ---
          L_details_processed := TRUE;
        end if;
      
      END LOOP;
    
    else
    
      open C_ISMC_ISC;
      fetch C_ISMC_ISC
        INTO L_ismc_isc_rec;
      close C_ISMC_ISC;
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
      if L_ismc_isc_rec.supplier is NULL then
        O_record_exists := FALSE;
        return TRUE;
      end if;
    
      L_itemsupctymfr_rec.item                          := L_ismc_isc_rec.item;
      L_itemsupctymfr_rec.supplier                      := L_ismc_isc_rec.supplier;
      L_itemsupctymfr_rec.manufacturer_ctry_id          := L_ismc_isc_rec.manu_country_id;
      L_itemsupctymfr_rec.primary_manufacturer_ctry_ind := L_ismc_isc_rec.primary_manu_ctry_ind;
    
      O_message.EXTEND();
      O_message(O_message.COUNT) := L_itemsupctymfr_rec;
    
      --- add rowid to TBL of rowids that will be deleted
      O_rowids_rec.queue_rowid_tbl.EXTEND;
      O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_ismc_isc_rec.q_row_id;
      O_rowids_rec.queue_seq_tbl.EXTEND;
      O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_ismc_isc_rec.q_seq_no;
      ---
    
      L_details_processed := TRUE;
    
    end if;
  
    if L_details_processed = FALSE and I_orderable_ind = 'N' then
      --- if there are no supp/country records in the Item create message for a non-orderable item,
      --- we need to initialize the supp/country table to be atomically NULL
      O_message := NULL;
      ---
    elsif L_details_processed = FALSE then
      -- if no data found in cursor for orderable item, raise error
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_MANU_COUNTRY_DETAIL;
  
  
   -------------------------------------------------------------------------------------------------------
  -- Name: BUILD_RELATED_ITEMS_HEAD
  -- Description: This function is responsible for create RelatedItemDesc message (Head)
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_RELATED_ITEMS_HEAD(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_message       IN OUT NOCOPY "RIB_RelatedItemDesc_TBL",
                                    O_rowids_rec    IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                    O_record_exists IN OUT BOOLEAN,
                                    I_item          IN ITEM_MFQUEUE.ITEM%TYPE,
                                    I_message_type  IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                    I_seq_no        IN ITEM_MFQUEUE.SEQ_NO%TYPE)
  
   RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_RELATED_ITEMS_HEAD';
  
    L_relateditemdesc_rec "RIB_RelatedItemDesc_REC" := NULL;
    L_details_processed   BOOLEAN := FALSE;
    L_relateditemdtl_tbl  "RIB_RelatedItemDtl_TBL" := "RIB_RelatedItemDtl_TBL"();
  
    cursor C_RELATEDITEMDESC_MC is
      select rih.relationship_id,
             rih.relationship_name,
             rih.relationship_type,
             rih.mandatory_ind,
             rih.item,
             NULL                  q_rowid,
             NULL                  q_seq_no
        from related_item_head rih
       where rih.item = I_item
      UNION ALL
      select q.relationship_id,
             NULL              relationship_name,
             NULL              relationship_type,
             NULL              mandatory_ind,
             q.item,
             q.rowid           q_rowid,
             q.seq_no          q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.message_type in (RMSMFM_ITEMS.RIH_ADD,
                                RMSMFM_ITEMS.RIH_UPD,
                                RMSMFM_ITEMS.RIH_DEL);
  
    TYPE RELATEDITEMDESC_MC_TBL is TABLE OF C_RELATEDITEMDESC_MC%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_relateditemdesc_mc_tbl RELATEDITEMDESC_MC_TBL;
  
    cursor C_RELATEDITEMDESC_DTL is
      select rih.relationship_id,
             rih.relationship_name,
             rih.relationship_type,
             rih.mandatory_ind,
             rih.item,
             NULL                  q_rowid,
             NULL                  q_seq_no
        from related_item_head rih, item_mfqueue q
       where q.item = I_item
         and q.seq_no = I_seq_no
         and rih.item = q.item
         and rih.relationship_id = q.relationship_id;
  
    L_relateditemdesc_dtl_rec C_RELATEDITEMDESC_DTL%ROWTYPE;
  
  BEGIN
  
    L_relateditemdesc_rec := "RIB_RelatedItemDesc_REC"(0,
                                                       I_item, --- item
                                                       NULL, --- relationship_id
                                                       NULL, --- relationship_name
                                                       NULL, --- relationship_type
                                                       NULL, --- mandatory_ind
                                                       NULL); --- RelatedItemDtl_TBL;
  
    O_message := "RIB_RelatedItemDesc_TBL"();
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_RELATEDITEMDESC_MC;
      fetch C_RELATEDITEMDESC_MC bulk collect
        into L_relateditemdesc_mc_tbl;
      close C_RELATEDITEMDESC_MC;
    
      FOR i IN 1 .. L_relateditemdesc_mc_tbl.COUNT LOOP
      
        if L_relateditemdesc_mc_tbl(i).q_rowid is NOT NULL then
          ---  add rowids to TBLs of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_relateditemdesc_mc_tbl(i)
                                                                              .q_rowid;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_relateditemdesc_mc_tbl(i)
                                                                          .q_seq_no;
        else
          L_relateditemdesc_rec.relationship_id   := L_relateditemdesc_mc_tbl(i)
                                                     .relationship_id;
          L_relateditemdesc_rec.relationship_name := L_relateditemdesc_mc_tbl(i)
                                                     .relationship_name;
          L_relateditemdesc_rec.relationship_type := L_relateditemdesc_mc_tbl(i)
                                                     .relationship_type;
          L_relateditemdesc_rec.mandatory_ind     := L_relateditemdesc_mc_tbl(i)
                                                     .mandatory_ind;
          L_relateditemdesc_rec.item              := I_item;
        
          if BUILD_RELATED_ITEMS_DETAIL(O_error_msg,
                                        L_relateditemdtl_tbl,
                                        O_rowids_rec,
                                        O_record_exists,
                                        I_item,
                                        L_relateditemdesc_rec.relationship_id,
                                        I_message_type,
                                        I_seq_no) = FALSE then
            return FALSE;
          end if;
          L_relateditemdesc_rec.RelatedItemDtl_tbl := L_relateditemdtl_tbl;
        
          O_message.EXTEND();
          O_message(O_message.COUNT) := L_relateditemdesc_rec;
          L_details_processed := TRUE;
        end if;
      
      END LOOP;
    
    else
    
      open C_RELATEDITEMDESC_DTL;
      fetch C_RELATEDITEMDESC_DTL
        INTO L_relateditemdesc_dtl_rec;
      close C_RELATEDITEMDESC_DTL;
    
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
    
      if L_relateditemdesc_dtl_rec.relationship_id is NULL then
        O_record_exists := FALSE;
        return TRUE;
      end if;
    
      L_relateditemdesc_rec.relationship_id   := L_relateditemdesc_dtl_rec.relationship_id;
      L_relateditemdesc_rec.relationship_name := L_relateditemdesc_dtl_rec.relationship_name;
      L_relateditemdesc_rec.relationship_type := L_relateditemdesc_dtl_rec.relationship_type;
      L_relateditemdesc_rec.mandatory_ind     := L_relateditemdesc_dtl_rec.mandatory_ind;
      L_relateditemdesc_rec.item              := I_item;
    
      if I_message_type in (RMSMFM_ITEMS.RID_ADD, RMSMFM_ITEMS.RID_UPD) then
        if BUILD_RELATED_ITEMS_DETAIL(O_error_msg,
                                      L_relateditemdtl_tbl,
                                      O_rowids_rec,
                                      O_record_exists,
                                      I_item,
                                      L_relateditemdesc_rec.relationship_id,
                                      I_message_type,
                                      I_seq_no) = FALSE then
          return FALSE;
        end if;
        L_relateditemdesc_rec.RelatedItemDtl_TBL := L_relateditemdtl_tbl;
      else
        L_relateditemdesc_rec.RelatedItemDtl_TBL := NULL;
      end if;
    
      O_message.EXTEND();
      O_message(O_message.COUNT) := L_relateditemdesc_rec;
    
      --- add rowid to TBL of rowids that will be deleted
      O_rowids_rec.queue_rowid_tbl.EXTEND;
      O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_relateditemdesc_dtl_rec.q_rowid;
      O_rowids_rec.queue_seq_tbl.EXTEND;
      O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_relateditemdesc_dtl_rec.q_seq_no;
      ---
    
      L_details_processed := TRUE;
    
    end if;
  
    -- if no data found in cursor, raise error
    if L_details_processed = FALSE and
       I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
      --- if there are no relationship records in the Item create message,
      --- we need to initialize the related_item_head table to be atomically NULL
      O_message := NULL;
    elsif L_details_processed = FALSE then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_RELATED_ITEMS_HEAD;
  
  
   -------------- ----------------------------------------------------------------------------------------
  -- Name: BUILD_RELATED_ITEMS_DETAIL
  -- Description: This function is responsible for create RelatedItemDtl message 
  -------------------------------------------------------------------------------------------------------
  FUNCTION BUILD_RELATED_ITEMS_DETAIL(O_error_msg       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_message         IN OUT NOCOPY "RIB_RelatedItemDtl_TBL",
                                      O_rowids_rec      IN OUT NOCOPY XXADEO_RMSMFM_ITEMS.ITEM_ROWID_REC,
                                      O_record_exists   IN OUT BOOLEAN,
                                      I_item            IN ITEM_MFQUEUE.ITEM%TYPE,
                                      I_relationship_id IN ITEM_MFQUEUE.RELATIONSHIP_ID%TYPE,
                                      I_message_type    IN ITEM_MFQUEUE.MESSAGE_TYPE%TYPE,
                                      I_seq_no          IN ITEM_MFQUEUE.SEQ_NO%TYPE)
    RETURN BOOLEAN IS
  
    L_module VARCHAR2(64) := 'XXADEO_RMSMFM_ITEMS_BUILD.BUILD_RELATED_ITEMS_DETAIL';
  
    L_relateditemdtl_rec "RIB_RelatedItemDtl_REC" := NULL;
    L_relateditemdtl_tbl "RIB_RelatedItemDtl_TBL" := "RIB_RelatedItemDtl_TBL"();
    L_details_processed  BOOLEAN := FALSE;
  
    cursor C_RELATEDITEMDTL_MC is
      select rid.relationship_id,
             rid.related_item,
             rid.priority,
             rid.start_date,
             rid.end_date,
             NULL                q_rowid,
             NULL                q_seq_no
        from related_item_head rih, related_item_detail rid
       where rih.item = I_item
         and rih.relationship_id = I_relationship_id
         and rih.relationship_id = rid.relationship_id
      UNION ALL
      select q.relationship_id,
             q.related_item,
             NULL              priority,
             NULL              start_date,
             NULL              end_date,
             q.rowid           q_rowid,
             q.seq_no          q_seq_no
        from item_mfqueue q
       where q.item = I_item
         and q.relationship_id = I_relationship_id
         and q.message_type in (RMSMFM_ITEMS.RID_ADD,
                                RMSMFM_ITEMS.RID_UPD,
                                RMSMFM_ITEMS.RID_DEL);
  
    TYPE RELATEDITEMDTL_MC_TBL is TABLE OF C_RELATEDITEMDTL_MC%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_relateditemdtl_mc_tbl RELATEDITEMDTL_MC_TBL;
  
    cursor C_RELATEDITEMDTL_DTL is
      select rid.relationship_id,
             rid.related_item,
             rid.priority,
             rid.start_date,
             rid.end_date,
             NULL                q_rowid,
             NULL                q_seq_no
        from related_item_detail rid, item_mfqueue q
       where q.item = I_item
         and q.relationship_id = I_relationship_id
         and q.seq_no = I_seq_no
         and rid.related_item = q.related_item
         and rid.relationship_id = q.relationship_id;
  
    TYPE RELATEDITEMDTL_DTL_TBL is TABLE OF C_RELATEDITEMDTL_DTL%ROWTYPE INDEX BY BINARY_INTEGER;
  
    L_relateditemdtl_dtl_tbl RELATEDITEMDTL_DTL_TBL;
  
  BEGIN
    L_relateditemdtl_rec := "RIB_RelatedItemDtl_REC"(0,
                                                     NULL, --- related_item
                                                     NULL, --- priority
                                                     NULL, --- start_date
                                                     NULL); --- end_date
  
    O_message := "RIB_RelatedItemDtl_TBL"();
  
    if I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
    
      open C_RELATEDITEMDTL_MC;
      fetch C_RELATEDITEMDTL_MC BULK COLLECT
        INTO L_relateditemdtl_mc_tbl;
      close C_RELATEDITEMDTL_MC;
    
      FOR i in 1 .. L_relateditemdtl_mc_tbl.COUNT LOOP
      
        L_relateditemdtl_tbl := "RIB_RelatedItemDtl_TBL"();
        if L_relateditemdtl_mc_tbl(i).q_rowid is NOT NULL then
          ---  add rowids to TBLs of rowids that will be deleted
          O_rowids_rec.queue_rowid_tbl.EXTEND;
          O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_relateditemdtl_mc_tbl(i)
                                                                              .q_rowid;
          O_rowids_rec.queue_seq_tbl.EXTEND;
          O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_relateditemdtl_mc_tbl(i)
                                                                          .q_seq_no;
        else
          L_relateditemdtl_rec.related_item   := L_relateditemdtl_mc_tbl(i)
                                                 .related_item;
          L_relateditemdtl_rec.priority       := L_relateditemdtl_mc_tbl(i)
                                                 .priority;
          L_relateditemdtl_rec.effective_date := L_relateditemdtl_mc_tbl(i)
                                                 .start_date;
          L_relateditemdtl_rec.end_date       := L_relateditemdtl_mc_tbl(i)
                                                 .end_date;
        
          O_message.EXTEND();
          O_message(O_message.COUNT) := L_relateditemdtl_rec;
          L_details_processed := TRUE;
        end if;
      
      END LOOP;
    
    else
    
      open C_RELATEDITEMDTL_DTL;
      fetch C_RELATEDITEMDTL_DTL BULK COLLECT
        INTO L_relateditemdtl_dtl_tbl;
      close C_RELATEDITEMDTL_DTL;
    
      ----
      -- If the record has been deleted before the CRE or UPD message gets published,
      -- do not publish any records.  Set O_record_exists to false so that the current
      -- record on the queue gets skipped in the next iteration of the main loop.
      ----
    
      FOR i in 1 .. L_relateditemdtl_dtl_tbl.COUNT LOOP
        if L_relateditemdtl_dtl_tbl(i).relationship_id is NULL then
          O_record_exists := FALSE;
          return TRUE;
        end if;
      
        L_relateditemdtl_rec.related_item   := L_relateditemdtl_dtl_tbl(i)
                                               .related_item;
        L_relateditemdtl_rec.priority       := L_relateditemdtl_dtl_tbl(i)
                                               .priority;
        L_relateditemdtl_rec.effective_date := L_relateditemdtl_dtl_tbl(i)
                                               .start_date;
        L_relateditemdtl_rec.end_date       := L_relateditemdtl_dtl_tbl(i)
                                               .end_date;
        O_message.EXTEND();
        O_message(O_message.COUNT) := L_relateditemdtl_rec;
      
        --- add rowid to TBL of rowids that will be deleted
        O_rowids_rec.queue_rowid_tbl.EXTEND;
        O_rowids_rec.queue_rowid_tbl(O_rowids_rec.queue_rowid_tbl.COUNT) := L_relateditemdtl_dtl_tbl(i)
                                                                            .q_rowid;
        O_rowids_rec.queue_seq_tbl.EXTEND;
        O_rowids_rec.queue_seq_tbl(O_rowids_rec.queue_seq_tbl.COUNT) := L_relateditemdtl_dtl_tbl(i)
                                                                        .q_seq_no;
        ---
      
        L_details_processed := TRUE;
      END LOOP;
    
    end if;
  
    -- if no data found in cursor, raise error
    if L_details_processed = FALSE and
       I_message_type = XXADEO_RMSMFM_ITEMS.ITEM_ADD then
      --- if there are no related item records in the Item create message,
      --- we need to initialize the related_item_detail table to be atomically NULL
      O_message := NULL;
    elsif L_details_processed = FALSE then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ITEM_DETAIL_PUB',
                                        I_item,
                                        L_module,
                                        NULL);
      return FALSE;
    end if;
  
    return TRUE;
  
  EXCEPTION
    when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
  END BUILD_RELATED_ITEMS_DETAIL;
  
  
  
END XXADEO_RMSMFM_ITEMS_BUILD;
/