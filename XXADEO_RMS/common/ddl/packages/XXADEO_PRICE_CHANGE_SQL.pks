CREATE OR REPLACE PACKAGE XXADEO_PRICE_CHANGE_SQL AS   
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package of Price Change Validation for RB34a                 */
/******************************************************************************/
--------------------------------------------------------------------------------
  --
  ZONE_NODE_TYPE_GROUP_ZONE     CONSTANT NUMBER := 3; 
  --
  GP_emergency_type_pc          VARCHAR2(10) := 'PC_E';
  GP_regular_type_pc            VARCHAR2(10) := 'PC_R';
  GP_type_pc                    VARCHAR2(10) := 'PC';
  --
  DEP_TYPE_SOURCE_RPM           NUMBER(6)    := 0;
  DEP_TYPE_SOURCE_XXADEO        NUMBER(6)    := 1;
  --
  TABLE$ITEM_MASTER             VARCHAR2(30) := 'ITEM_MASTER';
  TABLE$STORE                   VARCHAR2(30) := 'STORE';
  TABLE$RPM_ZONE                VARCHAR2(30) := 'RPM_ZONE';
  TABLE$RPM_CODES               VARCHAR2(30) := 'RPM_CODES';
  TABLE$XXADEO_PRICE_REQUEST    VARCHAR2(30) := 'XXADEO_PRICE_REQUEST';
  TABLE$XXADEO_RPM_STG_PC       VARCHAR2(30) := 'XXADEO_RPM_STAGE_PRICE_CHANGE';
  --
  TABLE$VDATE                   VARCHAR2(30) := 'VDATE';
  TABLE$CHANGE_AMOUNT           VARCHAR2(30) := 'CHANGE_AMOUNT';
  --
  ITEM_MASTER$ITEM              VARCHAR2(30) := 'ITEM';
  STORE$STORE                   VARCHAR2(30) := 'STORE';
  RPM_ZONE$ZONE_ID              VARCHAR2(30) := 'ZONE_ID';
  RPM_CODES$CODE_ID             VARCHAR2(30) := 'CODE_ID';
  --
--------------------------------------------------------------------------------
FUNCTION PROCESS_EM_STORE_PORTAL_PC (O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                     I_custom_obj_rec IN     XXADEO_CUSTOM_OBJ_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION PROCESS_EMERGENCY_PC (O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                               I_custom_obj_rec IN     XXADEO_CUSTOM_OBJ_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION PROCESS_RPO_PC (O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                         I_custom_obj_rec IN     XXADEO_CUSTOM_OBJ_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION PROCESS_STORE_PORTAL_PC (O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_custom_obj_rec IN     XXADEO_CUSTOM_OBJ_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION PROCESS_SIMTAR_PC (O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                            I_custom_obj_rec IN     XXADEO_CUSTOM_OBJ_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION RUN_PROCESS_PRICE_CHANGE (O_error_message            IN OUT LOGGER_LOGS.TEXT%TYPE,
                                   I_xxadeo_price_events_tbl  IN     XXADEO_PRICE_EVENTS_TBL)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
END XXADEO_PRICE_CHANGE_SQL;
/
