CREATE OR REPLACE PACKAGE XXADEO_CUSTOM_UPLOAD_SQL AS

--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package "XXADEO_CUSTOM_UPLOAD"                               */
/******************************************************************************/
----------------------------------------------------------------------------------
FUNCTION GET_TEMPLATE_S9T_SHEETS(O_error_message  IN OUT        LOGGER_LOGS.TEXT%TYPE,
                                 IO_s9t_sheet_tab IN OUT NOCOPY S9T_SHEET_TAB,
                                 I_template_key   IN            S9T_TEMPLATE.TEMPLATE_KEY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION GET_TEMPLATE_COLUMN_INDEXES(O_error_message                IN OUT        LOGGER_LOGS.TEXT%TYPE,
                                     IO_xxadeo_wksht_column_idx_tbl IN OUT NOCOPY xxadeo_WKSHT_COLUMN_IDX_TBL,
                                     I_template_key                 IN            S9T_TEMPLATE.TEMPLATE_KEY%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION GET_COL_IDX(I_xxadeo_wksht_column_idx_tbl IN XXADEO_WKSHT_COLUMN_IDX_TBL,
                     I_wksht_key                   IN XXADEO_S9T_TMPL_COLS_DEF.WKSHT_KEY%TYPE,
                     I_column_key                  IN XXADEO_S9T_TMPL_COLS_DEF.COLUMN_KEY%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                   O_file_id        IN OUT NUMBER,
                   I_template_key   IN     S9T_TEMPLATE.TEMPLATE_KEY%TYPE,
                   I_s9t_sheet_tab  IN     S9T_SHEET_TAB);
----------------------------------------------------------------------------------
END XXADEO_CUSTOM_UPLOAD_SQL;
/
