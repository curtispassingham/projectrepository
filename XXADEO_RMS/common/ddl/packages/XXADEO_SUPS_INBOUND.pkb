create or replace PACKAGE BODY XXADEO_SUPS_INBOUND AS
  ------------------------------------------------------------------------------------
  --- Function Name : SUPS_PURGE_HIST
  --- Purpose       : Purges the staging tables from records older than the number of
  ---                 days passed as a parameter, and marked as 'P'rocessed, or the
  ---                 ones marked as 'D'iscard.
  ---
  --- Changes       : 001 - 07/DEC/2018
  --                      - Added logic on PROCESS_RECORDS to create the France national 
  ---                       Org Unit GSB CHAMPAGNE MOSELLE. 
  ---                     - Added the default BU OrgUnits on xxadeo_mom_dvm, functional
  ---                       area = 'SUPS_INBOUND_DEFAULTS' and parameter = 'ORG_UNITS'
  ---                       *Obs: Only FR national OrgUnit exists at this time.
  ---    
  ------------------------------------------------------------------------------------
FUNCTION SUPS_PURGE_HIST(O_error_message IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                           I_days_number   IN            INTEGER)

return BOOLEAN IS

    L_program    VARCHAR2(64) := 'XXADEO_SUPS_INBOUND.SUPS_PURGE_HIST';
    L_today_date DATE         := SYSDATE;
    ---
    RECORD_LOCKED EXCEPTION;
    PRAGMA EXCEPTION_INIT(RECORD_LOCKED, -54);

    CURSOR C_LOCK_REC IS
        SELECT 'x'
          FROM XXADEO_SUPS
         WHERE ((L_today_date - XX_PROCESSING_TS) >= I_days_number
           AND XX_RECORD_STATUS = 'P')
            OR XX_RECORD_STATUS = 'D' for UPDATE nowait;

    CURSOR C_SUPSID_REC IS
        SELECT XX_SUPPLIER_ID,
               XX_SUPPLIER_SITE_ID,
               XX_BATCH_ID
          FROM XXADEO_SUPS
         WHERE ((L_today_date - XX_PROCESSING_TS) >= I_days_number
           AND XX_RECORD_STATUS = 'P')
            OR XX_RECORD_STATUS = 'D';
    --------------------------------------------------------------------------------
BEGIN
    OPEN C_LOCK_REC;
    CLOSE C_LOCK_REC;
    --- Delete records
    for sup IN C_SUPSID_REC
    loop
      DELETE
          FROM XXADEO_SUPS_CFA
         WHERE XX_BATCH_ID  =sup.XX_BATCH_ID
           AND (XX_SUPPLIER_ID=sup.XX_SUPPLIER_ID
            OR XX_SUPPLIER_ID  =sup.XX_SUPPLIER_SITE_ID);

      DELETE
          FROM XXADEO_SUPS_ADDR
         WHERE XX_BATCH_ID =sup.XX_BATCH_ID
           AND XX_SUPPLIER_ID=sup.XX_SUPPLIER_ID;

      DELETE
          FROM XXADEO_SUPSITE_ORG_UNIT
         WHERE XX_BATCH_ID      =sup.XX_BATCH_ID
           AND XX_SUPPLIER_SITE_ID=sup.XX_SUPPLIER_SITE_ID;

      DELETE
          FROM XXADEO_SUPSITE_ADDR
         WHERE XX_BATCH_ID      =sup.XX_BATCH_ID
           AND XX_SUPPLIER_SITE_ID=sup.XX_SUPPLIER_SITE_ID;

      DELETE
          FROM XXADEO_SUPS
         WHERE XX_BATCH_ID     =sup.XX_BATCH_ID
           AND (XX_SUPPLIER_ID   =sup.XX_SUPPLIER_ID
            OR XX_SUPPLIER_SITE_ID=sup.XX_SUPPLIER_SITE_ID);
    end loop;

    COMMIT;

    DELETE
        FROM XXADEO_SUPS_ADDR
       WHERE (XX_BATCH_ID NOT IN (SELECT XX_BATCH_ID FROM XXADEO_SUPS))
          OR (XX_SUPPLIER_ID NOT IN (SELECT XX_SUPPLIER_ID FROM XXADEO_SUPS)
         AND XX_SUPPLIER_ID NOT IN (SELECT XX_SUPPLIER_SITE_ID FROM XXADEO_SUPS));

    DELETE
        FROM XXADEO_SUPS_CFA
       WHERE (XX_BATCH_ID NOT IN (SELECT XX_BATCH_ID FROM XXADEO_SUPS))
          OR (XX_SUPPLIER_ID NOT IN (SELECT XX_SUPPLIER_ID FROM XXADEO_SUPS)
         AND XX_SUPPLIER_ID NOT IN (SELECT XX_SUPPLIER_SITE_ID FROM XXADEO_SUPS));

    DELETE
        FROM XXADEO_SUPSITE_ADDR
       WHERE (XX_BATCH_ID NOT IN (SELECT XX_BATCH_ID FROM XXADEO_SUPS))
          OR (XX_SUPPLIER_SITE_ID NOT IN (SELECT XX_SUPPLIER_ID FROM XXADEO_SUPS)
         AND XX_SUPPLIER_SITE_ID NOT IN (SELECT XX_SUPPLIER_SITE_ID FROM XXADEO_SUPS));

    DELETE
        FROM XXADEO_SUPSITE_ORG_UNIT
       WHERE (XX_BATCH_ID NOT IN (SELECT XX_BATCH_ID FROM XXADEO_SUPS))
          OR (XX_SUPPLIER_SITE_ID NOT IN (SELECT XX_SUPPLIER_ID FROM XXADEO_SUPS)
         AND XX_SUPPLIER_SITE_ID NOT IN (SELECT XX_SUPPLIER_SITE_ID FROM XXADEO_SUPS));

    COMMIT;
    return TRUE;
    --------------------------------------------------------------------------------
EXCEPTION
    WHEN RECORD_LOCKED then
        O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', 'XXADEO_SUPS', NULL, NULL);
        return FALSE;
    WHEN OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
        return FALSE;

END SUPS_PURGE_HIST;

------------------------------------------------------------------------------------
--- Public Function Name  : MARK_DISCARD
--- Purpose               :
---     Marks records that are 'E' and for which a newer version has been loaded to the
---     Staging tables
------------------------------------------------------------------------------------
FUNCTION MARK_DISCARD(O_error_message               IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN IS

    L_program       VARCHAR2(64)  := 'XXADEO_SUPS_INBOUND.MARK_DISCARD';

    CURSOR CHECK_ERROR_RECORDS IS
        SELECT XX_SUPPLIER_ID, XX_SUPPLIER_SITE_ID, XX_BATCH_ID
          FROM XXADEO_SUPS
         WHERE XX_RECORD_STATUS = 'E';

    CURSOR CHECK_NEW_SUPPLIERS(P_supplier_id IN NUMBER, P_batch_id IN NUMBER) IS
        SELECT 'x'
          FROM XXADEO_SUPS
         WHERE XX_SUPPLIER_ID = P_supplier_id
           AND XX_RECORD_STATUS = 'N'
           AND XX_BATCH_ID > P_batch_id;
    CURSOR CHECK_NEW_SUPSITES(P_supplier_id IN NUMBER, P_batch_id IN NUMBER) IS
        SELECT 'x'
          FROM XXADEO_SUPS
         WHERE XX_SUPPLIER_SITE_ID = P_supplier_id
           AND XX_RECORD_STATUS = 'N'
           AND XX_BATCH_ID > P_batch_id;


    TYPE new_supplier_type IS
    TABLE OF CHECK_NEW_SUPPLIERS%ROWTYPE INDEX BY BINARY_INTEGER;
    L_new_suppliers     new_supplier_type;

    TYPE new_supsite_type IS
    TABLE OF CHECK_NEW_SUPSITES%ROWTYPE INDEX BY BINARY_INTEGER;
    L_new_supsites      new_supsite_type;

BEGIN
    FOR errored IN CHECK_ERROR_RECORDS
    LOOP
        IF errored.XX_SUPPLIER_ID IS NOT NULL THEN
            OPEN CHECK_NEW_SUPPLIERS(errored.XX_SUPPLIER_ID,errored.XX_BATCH_ID);
            FETCH CHECK_NEW_SUPPLIERS BULK COLLECT INTO L_new_suppliers;
            CLOSE CHECK_NEW_SUPPLIERS;

            IF L_new_suppliers IS NOT NULL and L_new_suppliers.COUNT>0 THEN
                UPDATE XXADEO_SUPS
                    SET XX_RECORD_STATUS = 'D'
                  WHERE XX_SUPPLIER_ID = errored.XX_SUPPLIER_ID
                    AND XX_BATCH_ID = errored.XX_BATCH_ID;
            END IF;
        ELSIF errored.XX_SUPPLIER_SITE_ID IS NOT NULL THEN
            OPEN CHECK_NEW_SUPSITES(errored.XX_SUPPLIER_SITE_ID,errored.XX_BATCH_ID);
            FETCH CHECK_NEW_SUPSITES BULK COLLECT INTO L_new_supsites;
            CLOSE CHECK_NEW_SUPSITES;

            IF L_new_supsites IS NOT NULL and L_new_supsites.COUNT>0 THEN
                UPDATE XXADEO_SUPS
                    SET XX_RECORD_STATUS = 'D'
                  WHERE XX_SUPPLIER_SITE_ID = errored.XX_SUPPLIER_SITE_ID
                    AND XX_BATCH_ID = errored.XX_BATCH_ID;
            END IF;
        END IF;
    END LOOP;

    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
        return FALSE;
END MARK_DISCARD;
------------------------------------------------------------------------------------
--- Function Name : PROCESS_SUPPLIER
--- Purpose       : Processes the suppliers and supplier sites in the staging tables.
---                 First, creates a supplier object that will be passed to the revisited
---                 supplier API.
---                 The API then proceeds to validation tests. If they are all passed, the records
---                are loaded in the sups tables, updated or inserted, and the result
---                is passed back to this function that marks the records in the staging tables.
---                'P'rocessed for no validation error. Otherwise, they are marked as
---                'E'rror, and the validation error message is put in the column error
---                message.
------------------------------------------------------------------------------------
  FUNCTION PROCESS_RECORDS(O_error_message     IN OUT NOCOPY       RTK_ERRORS.RTK_TEXT%TYPE)

return BOOLEAN IS

    ---- Program variables
    L_program                   VARCHAR2(64)         := 'XXADEO_SUPS_INBOUND.PROCESS_RECORDS';
    O_serviceoperationstatus    "RIB_ServiceOpStatus_REC";
    O_businessobject            "XXADEO_SupplierRef_REC";
    I_serviceoperationcontext   "RIB_ServiceOpContext_REC";
    L1RIB_Principal_REC         "RIB_Principal_REC";
    L1RIB_MessageContext_REC    "RIB_MessageContext_REC";
    L1RIB_Property_TBL          "RIB_Property_TBL";
    L1RIB_Property_REC          "RIB_Property_REC";

    ---- RIB Variables
    L5RIB_Addr_REC              "RIB_Addr_REC";
    L4RIB_SupAttr_REC           "RIB_SupAttr_REC";
    L4RIB_SupSiteOrgUnit_REC    "RIB_SupSiteOrgUnit_REC";
    L4RIB_SupSiteOrgUnit_TBL    "RIB_SupSiteOrgUnit_TBL";
    L5RIB_SupAddr_REC           "XXADEO_SupAddr_REC";
    L4RIB_SupAddr_TBL           "XXADEO_SupAddr_TBL";
    L3RIB_SupAttr_REC           "RIB_SupAttr_REC";
    L2RIB_SupplierDesc_REC      "XXADEO_SupplierDesc_REC";
    L4RIB_SupCFA_REC            "XXADEO_SUP_CFA_REC";
    L3RIB_SupCFA_TBL            "XXADEO_SUP_CFA_TBL" := "XXADEO_SUP_CFA_TBL"();

    ---- Operation variables
    L_Supplier_Id               XXADEO_SUPS.XX_SUPPLIER_ID%TYPE;
    L_Batch_Id                  XXADEO_SUPS.XX_BATCH_ID%TYPE;
    L_Success_count             NUMBER               :=0;
    L_Fail_count                NUMBER               :=0;
    L_Status_code               VARCHAR2(1);
    L_Record_Status             VARCHAR2(1);
    L_Error_Message             RTK_ERRORS.RTK_TEXT%TYPE;
    L_Sup_exist                 VARCHAR2(1) :=NULL;

    ---- Cursors
    CURSOR C_GET_SUPPLIERS IS
        SELECT *
          FROM XXADEO_SUPS
         WHERE XX_RECORD_STATUS = 'N'
      ORDER BY XX_BATCH_ID ASC, XX_LAST_UPDATE_TS ASC;

    CURSOR C_GET_ADDRESS (P_Batch_Id IN NUMBER, P_Supplier_Id IN NUMBER) IS
        SELECT *
          FROM XXADEO_SUPS_ADDR
         WHERE XX_BATCH_ID  = P_Batch_Id
           AND XX_SUPPLIER_ID = P_Supplier_Id;

    CURSOR C_GET_CFA (P_Batch_Id IN NUMBER, P_Supplier_Id IN NUMBER) IS
        SELECT *
          FROM XXADEO_SUPS_CFA
         WHERE XX_BATCH_ID  = P_Batch_Id
           AND XX_SUPPLIER_ID = P_Supplier_Id;

    CURSOR C_GET_ORG_UNIT (P_Batch_Id IN NUMBER, P_Supplier_Site_Id IN NUMBER) IS
        SELECT *
          FROM XXADEO_SUPSITE_ORG_UNIT
         WHERE XX_BATCH_ID       = P_Batch_Id
           AND XX_SUPPLIER_SITE_ID = P_Supplier_Site_Id;

    CURSOR C_GET_SUPSITE_ADDR (P_Batch_Id IN NUMBER, P_Supplier_Site_Id IN NUMBER) IS
        SELECT *
          FROM XXADEO_SUPSITE_ADDR
         WHERE XX_BATCH_ID       = P_Batch_Id
           AND XX_SUPPLIER_SITE_ID = P_Supplier_Site_Id;

    CURSOR C_GET_PARENT_SUPP (P_Supplier_Id IN NUMBER) IS
        SELECT *
          FROM SUPS
          WHERE SUPPLIER = P_Supplier_Id;

    CURSOR C_GET_CFA_ID (P_CFA_NAME IN VARCHAR2) IS
        SELECT ATTRIB_ID
          FROM CFA_ATTRIB
         WHERE VIEW_COL_NAME = P_CFA_NAME;

    CURSOR C_GET_SUPP_EXIST (P_SUPPLIER_ID IN NUMBER) IS
        SELECT SUPPLIER
          FROM SUPS
         WHERE SUPPLIER=P_SUPPLIER_ID
           AND ROWNUM = 1;

   --001 BEGIN
	CURSOR C_GET_DEFAULT_ORG_UNIT (P_Batch_Id IN NUMBER, P_Supplier_Site_Id IN NUMBER) IS
        SELECT DVM.VALUE_1 
          FROM XXADEO_SUPSITE_ORG_UNIT SOU, 
               XXADEO_BU_OU BUOU, 
               XXADEO_MOM_DVM DVM
        WHERE SOU.XX_ORG_UNIT_ID = BUOU.OU
           -- DEFINE THE FILTERING ON TOP OF THE DVM
           AND DVM.BU = BUOU.BU
           AND DVM.FUNC_AREA = 'SUPS_INBOUND_DEFAULTS'
           AND DVM.PARAMETER = 'ORG_UNITS'
           -- ONLY CHECK FOR THE BATCH PROCESS ID RELATED RECORDS AND THE SUPPLIER BEING ANALYSED
           AND SOU.XX_BATCH_ID = P_Batch_Id
           AND SOU.XX_SUPPLIER_SITE_ID = P_Supplier_Site_Id;
    --001 END

    --- Types and record/table variables
    TYPE cfa_id_type IS
        TABLE OF C_GET_CFA_ID%ROWTYPE INDEX BY BINARY_INTEGER;

    L_CFA_ID_TBL  cfa_id_type;

    TYPE suppliers_type IS
        TABLE OF C_GET_SUPPLIERS%ROWTYPE INDEX BY BINARY_INTEGER;

    L_suppliers suppliers_type;
    L_parent C_GET_PARENT_SUPP%ROWTYPE;
    L_exist_cursor NUMBER;
  --------------------------------------------------------------------------------
BEGIN
    ---- Initialization of properties etc
    L1RIB_Property_REC := "RIB_Property_REC"(1,                                            --  rib_oid number
                                              'PROCESSING_SUPPLIER',                       --  name varchar2
                                              'Supplier supsite integration interface');   --  value varchar2

    L1RIB_Property_TBL := "RIB_Property_TBL"();
    L1RIB_Property_TBL.extend();
    L1RIB_Property_TBL(L1RIB_Property_TBL.LAST) := L1RIB_Property_REC;
    L1RIB_MessageContext_REC                    := "RIB_MessageContext_REC" (1,                     --  rib_oid number
                                                                               L1RIB_Property_TBL); --  Property_TBL "RIB_Property_TBL"


    L1RIB_Principal_REC := "RIB_Principal_REC"(1,                         --rib_oid number
                                                'PROCESSING_SUPPLIER');   --name varchar2


    I_serviceoperationcontext := "RIB_ServiceOpContext_REC"(1,                            --rib_oid number
                                                              L1RIB_Principal_REC,        --Principal "RIB_Principal_REC"
                                                              L1RIB_MessageContext_REC);  --MessageContext "RIB_MessageContext_REC"


  --- First, we get all the data from the supplier staging table
    OPEN C_GET_SUPPLIERS;
    FETCH C_GET_SUPPLIERS BULK COLLECT INTO L_suppliers;
    CLOSE C_GET_SUPPLIERS;

    if L_suppliers is NOT NULL then
      --- Loop on all the records that are marked as 'N', and need to be processed
        for i in L_suppliers.FIRST..L_suppliers.LAST
        loop
            L4RIB_SupAddr_TBL        := "XXADEO_SupAddr_TBL"();
            L4RIB_SupSiteOrgUnit_TBL := "RIB_SupSiteOrgUnit_TBL"();
            L3RIB_SupCFA_TBL         := "XXADEO_SUP_CFA_TBL"();
            L_Supplier_Id            := L_suppliers(i).xx_supplier_id;
            L_Batch_Id               := L_suppliers(i).XX_BATCH_ID;
            --- First we take care of suppliers: each iteration creates a supplier object
            if L_Supplier_Id IS NOT NULL then

                OPEN C_GET_SUPP_EXIST(L_Supplier_Id); 
                FETCH C_GET_SUPP_EXIST INTO L_exist_cursor;
                CLOSE C_GET_SUPP_EXIST;
                if L_exist_cursor IS NULL then
                    L_Sup_exist :='N';
                else 
                    L_Sup_exist :='Y';
                end if;


                L_Supplier_Id := L_suppliers(i).XX_SUPPLIER_ID;
                --- Supplier attributes
                L3RIB_SupAttr_REC := "RIB_SupAttr_REC"(1, --rib oid
                                                        L_suppliers(i).XX_SUP_NAME,               --SUP_NAME-VARCHAR2(240)
                                                        L_suppliers(i).XX_SUP_NAME_SECONDARY,     --SUP_NAME_SECONDARY-VARCHAR2(240)
                                                        L_suppliers(i).XX_CONTACT_NAME,           --CONTACT_NAME-VARCHAR2(120)
                                                        L_suppliers(i).XX_CONTACT_PHONE,          --CONTACT_PHONE-VARCHAR2(20)
                                                        L_suppliers(i).XX_CONTACT_FAX,            --CONTACT_FAX-VARCHAR2(20)
                                                        L_suppliers(i).XX_CONTACT_PAGER,          --CONTACT_PAGER-VARCHAR2(20)
                                                        L_suppliers(i).XX_SUP_STATUS,             --SUP_STATUS-VARCHAR2(1)
                                                        L_suppliers(i).XX_QC_IND,                 --QC_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_QC_PCT,                 --QC_PCT-NUMBER(12)
                                                        L_suppliers(i).XX_QC_FREQ,                --QC_FREQ-VARCHAR2(2)
                                                        L_suppliers(i).XX_VC_IND,                 --VC_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_VC_PCT,                 --VC_PCT-NUMBER(12)
                                                        L_suppliers(i).XX_VC_FREQ,                --VC_FREQ-NUMBER(2)
                                                        L_suppliers(i).XX_CURRENCY_CODE,          --CURRENCY_CODE-VARCHAR2(3)
                                                        L_suppliers(i).XX_LANG,                   --LANG-NUMBER(6)
                                                        L_suppliers(i).XX_TERMS,                  --TERMS-VARCHAR2(15)
                                                        L_suppliers(i).XX_FREIGHT_TERMS,          --FREIGHT_TERMS-VARCHAR2(30)
                                                        L_suppliers(i).XX_RET_ALLOW_IND,          --RET_ALLOW_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_RET_AUTH_REQ,           --RET_AUTH_REQ-VARCHAR2(1)
                                                        L_suppliers(i).XX_RET_MIN_DOL_AMT,        --RET_MIN_DOL_AMT-NUMBER(20)
                                                        L_suppliers(i).XX_RET_COURIER,            --RET_COURIER-VARCHAR2(250)
                                                        L_suppliers(i).XX_HANDLING_PCT,           --HANDLING_PCT-NUMBER(12)
                                                        L_suppliers(i).XX_EDI_PO_IND,             --EDI_PO_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_EDI_PO_CHG,             --EDI_PO_CHG-VARCHAR2(1)
                                                        L_suppliers(i).XX_EDI_PO_CONFIRM,         --EDI_PO_CONFIRM-VARCHAR2(1)
                                                        L_suppliers(i).XX_EDI_ASN,                --EDI_ASN-VARCHAR2(1)
                                                        L_suppliers(i).XX_EDI_SALES_RPT_FREQ,     --EDI_SALES_RPT_FREQ-VARCHAR2(1)
                                                        L_suppliers(i).XX_EDI_SUPP_AVAIL_IND,     --EDI_SUPP_AVAILABLE_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_EDI_CONTRACT_IND,       --EDI_CONTRACT_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_EDI_INVC_IND,           --EDI_INVC_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_EDI_CHANNEL_ID,         --EDI_CHANNEL_IND-NUMBER(4)
                                                        L_suppliers(i).XX_COST_CHG_PCT_VAR,       --COST_CHG_PCT_VAR-NUMBER(12)
                                                        L_suppliers(i).XX_COST_CHG_AMT_VAR,       --COST_CHG_AMT_VAR-NUMBER(20)
                                                        L_suppliers(i).XX_REPLEN_APPROVAL_IND,    --REPLEN_APPROVAL_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_SHIP_METHOD,            --SHIP_METHOD-VARCHAR2(6)
                                                        L_suppliers(i).XX_PAYMENT_METHOD,         --PAYMENT_METHOD-VARCHAR2(6)
                                                        L_suppliers(i).XX_CONTACT_TELEX,          --CONTACT_TELEX-VARCHAR2(20)
                                                        L_suppliers(i).XX_CONTACT_EMAIL,          --CONTACT_EMAIL-VARCHAR2(100)
                                                        L_suppliers(i).XX_SETTLEMENT_CODE,        --SETTLEMENT_CODE-VARCHAR2(1)
                                                        L_suppliers(i).XX_PRE_MARK_IND,           --PRE_MARK_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_AUTO_APPR_INVC_IND,     --AUTO_APPR_INVC_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_DBT_MEMO_CODE,          --DBT_MEMO_CODE-VARCHAR2(1)
                                                        L_suppliers(i).XX_FREIGHT_CHARGE_IND,     --FREIGHT_CHARGE_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_AUTO_APPR_MEMO_IND,     --AUTO_APPR_DBT_MEMO_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_PREPAY_INVC_IND,        --PREPAY_INVC_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_BACKORDER_IND,          --BACKORDER_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_VAT_REGION,             --VAT_REGION-NUMBER(4)
                                                        L_suppliers(i).XX_INV_MGMT_LVL,           --INV_MGMT_LVL-VARCHAR2(6)
                                                        L_suppliers(i).XX_SERVICE_PERF_REQ_IND,   --SERVICE_PERF_REQ_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_INVC_PAY_LOC,           --INVC_PAY_LOC-VARCHAR2(6)
                                                        L_suppliers(i).XX_INVC_RECEIVE_LOC,       --INVC_RECEIVE_LOC-VARCHAR2(6)
                                                        L_suppliers(i).XX_ADDINVC_GROSS_NET,      --ADDINVC_GROSS_NET-VARCHAR2(6)
                                                        L_suppliers(i).XX_DELIVERY_POLICY,        --DELIVERY_POLICY-VARCHAR2(6)
                                                        L_suppliers(i).XX_COMMENT_DESC,           --COMMENT_DESC-VARCHAR2(2000)
                                                        L_suppliers(i).XX_DEF_ITEM_LEAD_TIME,     --DEFAULT_ITEM_LEAD_TIME-NUMBER(4)
                                                        L_suppliers(i).XX_DUNS_NUMBER,            --DUNS_NUMBER-VARCHAR2(9)
                                                        L_suppliers(i).XX_DUNS_LOC,               --DUNS_LOC-VARCHAR2(4)
                                                        L_suppliers(i).XX_BRACKET_COSTING_IND,    --BRACKET_COSTING_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_VMI_ORDER_STATUS,       --VMI_ORDER_STATUS-VARCHAR2(6)
                                                        L_suppliers(i).XX_DSD_IND,                --DSD_IND-VARCHAR2(1)
                                                        L_suppliers(i).XX_SCALE_AIP_ORDERS,       --SCALE_AIP_ORDERS-VARCHAR2(1)
                                                        L_suppliers(i).XX_SUP_QTY_LEVEL);         --SUP_QTY_LEVEL-VARCHAR2(6)

            --- Loop on all addresses related to this supplier
                for address IN C_GET_ADDRESS(L_Batch_Id, L_Supplier_Id)
                loop
                    L5RIB_Addr_REC := "RIB_Addr_REC"(1, address.XX_ADDR_TYPE, --ADDR_TYPE-VARCHAR2(2)
                                                        address.XX_PRIMARY_ADDR_IND,                              --PRIMARY_ADDR_IND-VARCHAR2(1)
                                                        address.XX_ADD_1,                                         --ADD_1-VARCHAR2(240)
                                                        address.XX_ADD_2,                                         --ADD_2-VARCHAR2(240)
                                                        address.XX_ADD_3,                                         --ADD_3-VARCHAR2(240)
                                                        address.XX_CITY,                                          --CITY-VARCHAR2(120)
                                                        address.XX_STATE,                                         --STATE-VARCHAR2(3)
                                                        address.XX_COUNTRY_ID,                                    --COUNTRY_ID-VARCHAR2(3)
                                                        address.XX_POST,                                          --POST-VARCHAR2(30)
                                                        address.XX_CONTACT_NAME,                                  --CONTACT_NAME-VARCHAR2(120)
                                                        address.XX_CONTACT_PHONE,                                 --CONTACT_PHONE-VARCHAR2(20)
                                                        address.XX_CONTACT_FAX,                                   --CONTACT_FAX-VARCHAR2(20)
                                                        address.XX_CONTACT_EMAIL,                                 --CONTACT_EMAIL-VARCHAR2(100)
                                                        address.XX_JURISDICTION_CODE);                            --JURISDICTION_CODE-VARCHAR2(10)

                    L5RIB_SupAddr_REC := "XXADEO_SupAddr_REC"(1,              --rib oid
                                                                address.XX_ADDR_XREF_KEY,                                 --ADDR_XREF_KEY-VARCHAR2(32)
                                                                address.XX_ADDR_KEY,                                      --ADDR_KEY-NUMBER(11)
                                                                L5RIB_Addr_REC);                                          --ADDR-RIB_Addr_REC()

                    L4RIB_SupAddr_TBL.extend();
                    L4RIB_SupAddr_TBL(L4RIB_SupAddr_TBL.LAST) := L5RIB_SupAddr_REC; --- Put the record in a table
                end loop;


            --- Loop on all CFAs related to this supplier
                for CFA in C_GET_CFA(L_Batch_Id, L_Supplier_id)
                loop
                  L4RIB_SupCFA_REC := "XXADEO_SUP_CFA_REC"(CFA.XX_NAME,
                                                            CFA.XX_VALUE,
                                                            CFA.XX_VALUE_NUMBER,
                                                            CFA.XX_VALUE_DATE);

                  L3RIB_SupCFA_TBL.extend();
                  L3RIB_SupCFA_TBL(L3RIB_SupCFA_TBL.LAST):= L4RIB_SupCFA_REC;
                end loop;


            --- We create the supplier object, containing supplier attributes, org units and addresses
                L2RIB_SupplierDesc_REC := "XXADEO_SupplierDesc_REC"(1, -- RIB OID
                                                              L_suppliers(i).XX_SUP_XREF_KEY,                        --SUPPLIER_XREF_KEY-VARCHAR2()
                                                              L_suppliers(i).XX_SUPPLIER_ID,                         --SUPPLIER_ID-NUMBER(10)
                                                              L_suppliers(i).XX_SUPSITE_XREF_KEY, L_suppliers(i).XX_SUPPLIER_SITE_ID,
                                                              L3RIB_SupAttr_REC, L4RIB_SupAddr_TBL,
                                                              L4RIB_SupSiteOrgUnit_TBL,
                                                              L3RIB_SupCFA_TBL);


            --- We send that object to the supplier API that is in charge of validations.
                L_Status_code :=XXADEO_SUPS_INBOUND.CONSUME(I_serviceoperationcontext, L_Sup_exist, L2RIB_SupplierDesc_REC, O_serviceoperationstatus, O_businessobject, L_Error_Message);

                --- If the validation is successful, mark record as 'P', otherwise as 'E' and give it the current timestamp
                if L_Status_code   = 'S' then
                    L_Record_Status := 'P';
                    L_success_count := L_success_count + 1;
                else
                    L_Record_Status := 'E';
                    L_fail_count := L_fail_count + 1;
                end if;

                UPDATE XXADEO_SUPS
                    SET XX_RECORD_STATUS   = L_Record_Status,
                        XX_ERROR_MSG       = L_Error_Message,
                        XX_PROCESSING_TS   = systimestamp,
                        XX_LAST_UPDATE_TS  = systimestamp
                  WHERE XX_BATCH_ID        = L_Batch_ID
                    AND XX_SUPPLIER_ID     = L_Supplier_Id;

            else

                ---  We now do the same with supplier sites
                L_supplier_id := L_suppliers(i).XX_SUPPLIER_SITE_ID;
                --- We search the parent of the supplier site (that should already be in RMS)

                OPEN C_GET_SUPP_EXIST(L_Supplier_Id);
                FETCH C_GET_SUPP_EXIST INTO L_exist_cursor;
                CLOSE C_GET_SUPP_EXIST;
                if L_exist_cursor IS NULL then
                    L_Sup_exist :='N';
                else 
                    L_Sup_exist :='Y';
                end if;


                OPEN C_GET_PARENT_SUPP(L_suppliers(i).XX_SUPPLIER_PARENT_ID);
                FETCH C_GET_PARENT_SUPP INTO L_parent;
                CLOSE C_GET_PARENT_SUPP;
                if L_parent.SUPPLIER IS NULL then
                    UPDATE XXADEO_SUPS
                       SET XX_RECORD_STATUS      = 'E',
                           XX_ERROR_MSG          = 'Supplier parent does not exist',
                           XX_PROCESSING_TS      = CURRENT_TIMESTAMP,
                           XX_LAST_UPDATE_TS     = CURRENT_TIMESTAMP
                     WHERE XX_BATCH_ID           = L_Batch_ID
                       AND XX_SUPPLIER_SITE_ID   = L_Supplier_Id;

                    CONTINUE;
                END if;

                --- Take supplier site information
                L4RIB_SupAttr_REC := "RIB_SupAttr_REC"(1,                                 --rib oid
                                                L_suppliers(i).XX_SUP_NAME,               --SUP_NAME-VARCHAR2(240)
                                                L_suppliers(i).XX_SUP_NAME_SECONDARY,     --SUP_NAME_SECONDARY-VARCHAR2(240)
                                                L_suppliers(i).XX_CONTACT_NAME,           --CONTACT_NAME-VARCHAR2(120)
                                                L_suppliers(i).XX_CONTACT_PHONE,          --CONTACT_PHONE-VARCHAR2(20)
                                                L_suppliers(i).XX_CONTACT_FAX,            --CONTACT_FAX-VARCHAR2(20)
                                                L_suppliers(i).XX_CONTACT_PAGER,          --CONTACT_PAGER-VARCHAR2(20)
                                                L_suppliers(i).XX_SUP_STATUS,             --SUP_STATUS-VARCHAR2(1)
                                                L_suppliers(i).XX_QC_IND,                 --QC_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_QC_PCT,                 --QC_PCT-NUMBER(12)
                                                L_suppliers(i).XX_QC_FREQ,                --QC_FREQ-VARCHAR2(2)
                                                L_suppliers(i).XX_VC_IND,                 --VC_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_VC_PCT,                 --VC_PCT-NUMBER(12)
                                                L_suppliers(i).XX_VC_FREQ,                --VC_FREQ-NUMBER(2)
                                                L_suppliers(i).XX_CURRENCY_CODE,          --CURRENCY_CODE-VARCHAR2(3)
                                                L_suppliers(i).XX_LANG,                   --LANG-NUMBER(6)
                                                L_suppliers(i).XX_TERMS,                  --TERMS-VARCHAR2(15)
                                                L_suppliers(i).XX_FREIGHT_TERMS,          --FREIGHT_TERMS-VARCHAR2(30)
                                                L_suppliers(i).XX_RET_ALLOW_IND,          --RET_ALLOW_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_RET_AUTH_REQ,           --RET_AUTH_REQ-VARCHAR2(1)
                                                L_suppliers(i).XX_RET_MIN_DOL_AMT,        --RET_MIN_DOL_AMT-NUMBER(20)
                                                L_suppliers(i).XX_RET_COURIER,            --RET_COURIER-VARCHAR2(250)
                                                L_suppliers(i).XX_HANDLING_PCT,           --HANDLING_PCT-NUMBER(12)
                                                L_suppliers(i).XX_EDI_PO_IND,             --EDI_PO_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_EDI_PO_CHG,             --EDI_PO_CHG-VARCHAR2(1)
                                                L_suppliers(i).XX_EDI_PO_CONFIRM,         --EDI_PO_CONFIRM-VARCHAR2(1)
                                                L_suppliers(i).XX_EDI_ASN,                --EDI_ASN-VARCHAR2(1)
                                                L_suppliers(i).XX_EDI_SALES_RPT_FREQ,     --EDI_SALES_RPT_FREQ-VARCHAR2(1)
                                                L_suppliers(i).XX_EDI_SUPP_AVAIL_IND,     --EDI_SUPP_AVAILABLE_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_EDI_CONTRACT_IND,       --EDI_CONTRACT_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_EDI_INVC_IND,           --EDI_INVC_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_EDI_CHANNEL_ID,         --EDI_CHANNEL_IND-NUMBER(4)
                                                L_suppliers(i).XX_COST_CHG_PCT_VAR,       --COST_CHG_PCT_VAR-NUMBER(12)
                                                L_suppliers(i).XX_COST_CHG_AMT_VAR,       --COST_CHG_AMT_VAR-NUMBER(20)
                                                L_suppliers(i).XX_REPLEN_APPROVAL_IND,    --REPLEN_APPROVAL_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_SHIP_METHOD,            --SHIP_METHOD-VARCHAR2(6)
                                                L_suppliers(i).XX_PAYMENT_METHOD,         --PAYMENT_METHOD-VARCHAR2(6)
                                                L_suppliers(i).XX_CONTACT_TELEX,          --CONTACT_TELEX-VARCHAR2(20)
                                                L_suppliers(i).XX_CONTACT_EMAIL,          --CONTACT_EMAIL-VARCHAR2(100)
                                                L_suppliers(i).XX_SETTLEMENT_CODE,        --SETTLEMENT_CODE-VARCHAR2(1)
                                                L_suppliers(i).XX_PRE_MARK_IND,           --PRE_MARK_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_AUTO_APPR_INVC_IND,     --AUTO_APPR_INVC_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_DBT_MEMO_CODE,          --DBT_MEMO_CODE-VARCHAR2(1)
                                                L_suppliers(i).XX_FREIGHT_CHARGE_IND,     --FREIGHT_CHARGE_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_AUTO_APPR_MEMO_IND,     --AUTO_APPR_DBT_MEMO_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_PREPAY_INVC_IND,        --PREPAY_INVC_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_BACKORDER_IND,          --BACKORDER_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_VAT_REGION,             --VAT_REGION-NUMBER(4)
                                                L_suppliers(i).XX_INV_MGMT_LVL,           --INV_MGMT_LVL-VARCHAR2(6)
                                                L_suppliers(i).XX_SERVICE_PERF_REQ_IND,   --SERVICE_PERF_REQ_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_INVC_PAY_LOC,           --INVC_PAY_LOC-VARCHAR2(6)
                                                L_suppliers(i).XX_INVC_RECEIVE_LOC,       --INVC_RECEIVE_LOC-VARCHAR2(6)
                                                L_suppliers(i).XX_ADDINVC_GROSS_NET,      --ADDINVC_GROSS_NET-VARCHAR2(6)
                                                L_suppliers(i).XX_DELIVERY_POLICY,        --DELIVERY_POLICY-VARCHAR2(6)
                                                L_suppliers(i).XX_COMMENT_DESC,           --COMMENT_DESC-VARCHAR2(2000)
                                                L_suppliers(i).XX_DEF_ITEM_LEAD_TIME,     --DEFAULT_ITEM_LEAD_TIME-NUMBER(4)
                                                L_suppliers(i).XX_DUNS_NUMBER,            --DUNS_NUMBER-VARCHAR2(9)
                                                L_suppliers(i).XX_DUNS_LOC,               --DUNS_LOC-VARCHAR2(4)
                                                L_suppliers(i).XX_BRACKET_COSTING_IND,    --BRACKET_COSTING_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_VMI_ORDER_STATUS,       --VMI_ORDER_STATUS-VARCHAR2(6)
                                                L_suppliers(i).XX_DSD_IND,                --DSD_IND-VARCHAR2(1)
                                                L_suppliers(i).XX_SCALE_AIP_ORDERS,       --SCALE_AIP_ORDERS-VARCHAR2(1)
                                                L_suppliers(i).XX_SUP_QTY_LEVEL);         --SUP_QTY_LEVEL-VARCHAR2(6)


                                      --- Loop on the supplier sites addresses
                for address IN C_GET_SUPSITE_ADDR(L_Batch_Id, L_Supplier_Id)
                loop
                    L5RIB_Addr_REC := "RIB_Addr_REC"(0, address.XX_ADDR_TYPE,                                     --ADDR_TYPE-VARCHAR2(2)
                                                        address.XX_PRIMARY_ADDR_IND,                              --PRIMARY_ADDR_IND-VARCHAR2(1)
                                                        address.XX_ADD_1,                                         --ADD_1-VARCHAR2(240)
                                                        address.XX_ADD_2,                                         --ADD_2-VARCHAR2(240)
                                                        address.XX_ADD_3,                                         --ADD_3-VARCHAR2(240)
                                                        address.XX_CITY,                                          --CITY-VARCHAR2(120)
                                                        address.XX_STATE,                                         --STATE-VARCHAR2(3)
                                                        address.XX_COUNTRY_ID,                                    --COUNTRY_ID-VARCHAR2(3)
                                                        address.XX_POST,                                          --POST-VARCHAR2(30)
                                                        address.XX_CONTACT_NAME,                                  --CONTACT_NAME-VARCHAR2(120)
                                                        address.XX_CONTACT_PHONE,                                 --CONTACT_PHONE-VARCHAR2(20)
                                                        address.XX_CONTACT_FAX,                                   --CONTACT_FAX-VARCHAR2(20)
                                                        address.XX_CONTACT_EMAIL,                                 --CONTACT_EMAIL-VARCHAR2(100)
                                                        address.XX_JURISDICTION_CODE);                            --JURISDICTION_CODE-VARCHAR2(10)

                    L5RIB_SupAddr_REC := "XXADEO_SupAddr_REC"(1,                                                  --rib oid
                                                                address.XX_ADDR_XREF_KEY,                         --ADDR_XREF_KEY-VARCHAR2(32)
                                                                address.XX_ADDR_KEY,                              --ADDR_KEY-NUMBER(11)
                                                                L5RIB_Addr_REC);                                  --ADDR-RIB_Addr_REC()

                    L4RIB_SupAddr_TBL.extend();
                    L4RIB_SupAddr_TBL(L4RIB_SupAddr_TBL.LAST) := L5RIB_SupAddr_REC;
                END loop;

                --- Loop on the supplier sites org units
                for orgunit IN C_GET_ORG_UNIT(L_Batch_Id, L_Supplier_id)
                loop
                    L4RIB_SupSiteOrgUnit_REC := "RIB_SupSiteOrgUnit_REC"(1,                                        --rib oid
                                                                          orgunit.XX_ORG_UNIT_ID,                  --ORG_UNIT_ID-NUMBER(15)
                                                                          orgunit.XX_PRIMARY_PAY_SITE);            --PRIMARY_PAY_SITE-VARCHAR2(1)

                    L4RIB_SupSiteOrgUnit_TBL.extend();
                    L4RIB_SupSiteOrgUnit_TBL(L4RIB_SupSiteOrgUnit_TBL.LAST) := L4RIB_SupSiteOrgUnit_REC;
                END loop;

                --001 BEGIN
				-- Loop on the supplier sites org units for default OU check
				for default_orgunit IN C_GET_DEFAULT_ORG_UNIT (L_Batch_Id, L_Supplier_id)
				loop
                      L4RIB_SupSiteOrgUnit_REC := "RIB_SupSiteOrgUnit_REC"(1,                                   --rib oid
                                                                          default_orgunit.VALUE_1,                  --ORG_UNIT_ID-NUMBER(15)
                                                                          'N');                                    --PRIMARY_PAY_SITE-VARCHAR2(1)

                    L4RIB_SupSiteOrgUnit_TBL.extend();
                    L4RIB_SupSiteOrgUnit_TBL(L4RIB_SupSiteOrgUnit_TBL.LAST) := L4RIB_SupSiteOrgUnit_REC;
                END loop;
				--001 END


                --- Loop on all CFAs related to this supplier
                for CFA IN C_GET_CFA(L_Batch_Id, L_Supplier_id)
                loop
                    L4RIB_SupCFA_REC := "XXADEO_SUP_CFA_REC"(CFA.XX_NAME,
                                                             CFA.XX_VALUE,
                                                             CFA.XX_VALUE_NUMBER,
                                                             CFA.XX_VALUE_DATE);

                    L3RIB_SupCFA_TBL.extend();
                    L3RIB_SupCFA_TBL(L3RIB_SupCFA_TBL.LAST):= L4RIB_SupCFA_REC;
                END loop;


                L2RIB_SupplierDesc_REC := "XXADEO_SupplierDesc_REC"(1,                                           -- RIB OID
                                                                    L_parent.EXTERNAL_REF_ID,                    --SUPPLIER_XREF_KEY-VARCHAR2()
                                                                    L_suppliers(i).XX_SUPPLIER_PARENT_ID,        --SUPPLIER_ID-NUMBER(10)
                                                                    L_suppliers(i).XX_SUPSITE_XREF_KEY,
                                                                    L_suppliers(i).XX_SUPPLIER_SITE_ID,
                                                                    L4RIB_SupAttr_REC,
                                                                    L4RIB_SupAddr_TBL,
                                                                    L4RIB_SupSiteOrgUnit_TBL,
                                                                    L3RIB_SupCFA_TBL);


                L_Status_code := XXADEO_SUPS_INBOUND.CONSUME(I_serviceoperationcontext, L_Sup_exist, L2RIB_SupplierDesc_REC, O_serviceoperationstatus, O_businessobject, L_Error_Message);

                if L_Status_code   = 'S' then
                    L_Record_Status := 'P';
                    L_success_count := L_success_count + 1;
                else
                    L_Record_Status := 'E';
                    L_fail_count := L_fail_count + 1;
                END IF;

                UPDATE XXADEO_SUPS
                    SET XX_RECORD_STATUS    = L_Record_Status,
                        XX_ERROR_MSG          = L_Error_Message,
                        XX_PROCESSING_TS      = systimestamp,
                        XX_LAST_UPDATE_TS     = systimestamp
                  WHERE XX_BATCH_ID       = L_Batch_ID
                    AND XX_SUPPLIER_SITE_ID = L_Supplier_Id;
            END IF;
        END loop;
    END IF;

    COMMIT;
    -- Do not remove, important to retrieve these values in the ksh program
    dbms_output.put_line(L_success_count);
    dbms_output.put_line(L_fail_count);
return TRUE;
--------------------------------------------------------------------------------
EXCEPTION
WHEN OTHERS then
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  return FALSE;
END PROCESS_RECORDS;
------------------------------------------------------------------------------------
--- Function Name : CONSUME
--- Purpose       : Consumes the rib objects previously created and validates them.
---             Basically the same as the one from the SUPPLIERSERVICEPROVIDERIMPL
---                 package, but gives access to the resulting error messages if any.
------------------------------------------------------------------------------------
FUNCTION CONSUME(I_serviceoperationcontext        IN OUT "RIB_ServiceOpContext_REC",
                 I_supexist                       IN VARCHAR2,
                 I_businessobject                 IN "XXADEO_SupplierDesc_REC",
                 O_serviceoperationstatus         OUT "RIB_ServiceOpStatus_REC",
                 O_businessobject                 OUT "XXADEO_SupplierRef_REC",
                 O_Error_Message                  OUT RTK_ERRORS.RTK_TEXT%TYPE)

  return VARCHAR2

IS

  L_program     VARCHAR2(64)                             := 'XXADEO_SUPS_INBOUND.CONSUME';
  L_status_code VARCHAR2(1)                              := NULL;
  L_error_message RTK_ERRORS.RTK_TEXT%TYPE               := NULL;
  L_supplier_collection_rec "XXADEO_SupplierColDesc_REC" := NULL;
  L_supplier_collection_ref "XXADEO_SupplierColRef_REC"  := NULL;
  L_supplier_desc_tbl "XXADEO_SupplierDesc_TBL"          := NULL;
  L_supplier_desc_rec "XXADEO_SupplierDesc_REC"          := NULL;
  L_operation_type  VARCHAR2(7)                          := NULL;
  ------------------------------------------------------------------------------

BEGIN
  --- Populate first Records for "RIB_SupplierColDesc_REC" objects
  if I_businessobject   IS NOT NULL then
    L_supplier_desc_rec := I_businessobject;
    L_supplier_desc_tbl := "XXADEO_SupplierDesc_TBL"();
    L_supplier_desc_tbl.EXTEND;
    L_supplier_desc_tbl(L_supplier_desc_tbl.LAST)    := L_supplier_desc_rec;
    L_supplier_collection_rec := "XXADEO_SupplierColDesc_REC"(0,--- rib_oid
                                                              1,                                                          --- Collection Size
                                                              L_supplier_desc_tbl);                                       --- "RIB_SupplierDesc_TBL"

  END IF;

  if I_supexist = 'Y' then
    L_operation_type := 'suppmod';
  else 
    L_operation_type := 'suppadd';
  end if;


  O_error_message := NULL;
  XXADEO_SUPPLIER.CONSUME (L_status_code, L_error_message, L_supplier_collection_ref, L_supplier_collection_rec, L_operation_type);

  if L_status_code          = 'S' then --- Successful completion
    O_businessobject       := L_supplier_collection_ref.SupplierRef_TBL(1);
  END IF;

  RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus, L_status_code, L_error_message, L_program);
  O_Error_Message := L_error_message;


  return L_status_code;
  --------------------------------------------------------------------------------
EXCEPTION
WHEN OTHERS then
  L_status_code   := API_CODES.UNHANDLED_ERROR;
  L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR', SQLERRM, NULL, TO_CHAR(SQLCODE));
  RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus, L_status_code, L_error_message, L_program);
END CONSUME;
--------------------------------------------------------------------------------
END XXADEO_SUPS_INBOUND;
/
