CREATE OR REPLACE PACKAGE XXADEO_CUSTOM_CONFIG_WRP_SQL AS
  
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - MARCH 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package to wrap and run RMS custom_pkg_config functions      */
/******************************************************************************/
--------------------------------------------------------------------------------
FUNCTION RUN_SUPPLIER_APPROVE_WRP(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  IO_custom_obj_rec IN OUT CUSTOM_OBJ_REC)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION RUN_SYSTEM_RULES_WRP(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                              IO_custom_obj_rec IN OUT XXADEO_CUSTOM_OBJ_REC)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END XXADEO_CUSTOM_CONFIG_WRP_SQL;
/ 
 