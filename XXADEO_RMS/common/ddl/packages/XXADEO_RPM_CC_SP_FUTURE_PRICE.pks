CREATE OR REPLACE PACKAGE XXADEO_RPM_CC_SP_FUTURE_PRICE AS

--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - June 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package "XXADEO_RPM_CC_SP_FUTURE_PRICE" for RB34a            */
/******************************************************************************/
--------------------------------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
END XXADEO_RPM_CC_SP_FUTURE_PRICE;
/
