CREATE OR REPLACE PACKAGE BODY XXADEO_ECOTAX_THREAD AS

  TYPE LIST_OF_THREADS IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;

  FUNCTION THREAD_BALANCE(O_error_message IN OUT VARCHAR2,
                          I_table_name    IN VARCHAR2,
                          I_program_name  IN VARCHAR2) RETURN BOOLEAN IS
  
    TYPE L_cursor IS REF CURSOR;
    L_cursor_thread_id L_cursor;
    L_cursor_lines     L_cursor;
    ---
    L_list_of_threads LIST_OF_THREADS;
    L_num_of_threads  NUMBER;
    L_prev_thread     NUMBER;
    L_weight          NUMBER;
    L_thread_id       NUMBER;
    L_row_id          ROWID;
    L_stmt            VARCHAR2(200);
    L_total_uow       NUMBER := 0;
  
    L_stmt_lines VARCHAR2(2000) := 'select rowid, THREAD, WEIGHT' ||
                                   ' from ' || I_table_name ||
                                   ' where WEIGHT >= 0' ||
                                   ' order by WEIGHT desc';
  
    CURSOR C_num_threads IS
      select NVL(num_threads, 0)
        from restart_control
       where program_name = I_program_name;
  
    L_stmt_get_thread_id VARCHAR2(2000) := 'select THREAD, sum(WEIGHT) WEIGHT' ||
                                           ' from ' || I_table_name ||
                                           ' where THREAD is not null' ||
                                           ' group by THREAD';
  BEGIN
  
    L_list_of_threads.delete();
    L_stmt := 'update ' || I_table_name ||
              ' set THREAD = :b_prev_thread where rowid = :b_row_id';
  
    OPEN C_num_threads;
  
    FETCH C_num_threads
      into L_num_of_threads;
    if c_num_threads%NOTFOUND then
      close C_num_threads;
      O_error_message := 'No parameter found';
      return FALSE;
    end if;
    close C_num_threads;
  
    for i in 1 .. L_num_of_threads loop
      L_list_of_threads(i) := 0;
    end loop;
  
    open L_cursor_thread_id for L_stmt_get_thread_id;
    loop
      fetch L_cursor_thread_id
        into L_thread_id, L_weight;
      exit when L_cursor_thread_id%NOTFOUND;
    
      if L_weight < 0 then
        L_list_of_threads(L_thread_id) := L_weight * -1;
      else
        L_list_of_threads(L_thread_id) := L_weight;
      end if;
    
      l_total_uow := l_total_uow + L_list_of_threads(L_thread_id);
    end loop;
    close L_cursor_thread_id;
  
    open L_cursor_lines for L_stmt_lines;
    loop
      fetch L_cursor_lines
        into L_row_id, L_thread_id, L_weight;
      exit when L_cursor_lines%NOTFOUND;
    
      L_prev_thread := 1;
      for i in 1 .. L_num_of_threads loop
        if L_list_of_threads(i) < L_list_of_threads(L_prev_thread) then
          L_prev_thread := i;
        end if;
      end loop;
    
      L_list_of_threads(L_prev_thread) := NVL(L_list_of_threads(L_prev_thread),
                                              0) + L_weight;
      l_total_uow := L_total_uow + L_weight;
    
      execute immediate L_stmt
        using L_prev_thread, L_row_id;
    
    end loop;
  
    return TRUE;
  
  EXCEPTION
    WHEN OTHERS THEN
      O_error_message := SQLERRM;
      return FALSE;
  END THREAD_BALANCE;

  --------------------------------------------------------------------------------------------------------------------------------

  PROCEDURE POPULATE(o_status  OUT NUMBER,
                     o_message OUT VARCHAR2,
                     I_dept    IN VARCHAR2) IS
  
    CURSOR c_unpub_ecotax IS
      SELECT a.ITEM,
             a.SUPPLIER,
             a.ITEM_EXP_TYPE,
             a.ITEM_EXP_SEQ,
             a.COMP_ID,
             a.ORIGIN_COUNTRY_ID,
             a.LADING_PORT,
             a.EVENT_TYPE,
             a.RECORD_STATUS,
             a.LAST_UPDATE_TS,
             a.SEQ_NO,
             a.STATUS_MESSAGE
        FROM XXADEO_STG_ECOTAX_OUT a, ITEM_MASTER b
       WHERE a.RECORD_STATUS = 'N'
         AND a.ITEM = b.ITEM
         AND b.DEPT = I_dept
       ORDER BY a.seq_no asc;
  
    L_unpub_ecotax        c_unpub_ecotax%rowtype;
    L_last_msg_event_type VARCHAR2(20);
    L_last_seq_no         NUMBER;
    L_next_msg_event_type VARCHAR2(20);
    L_next_seq_no         NUMBER;
    L_update_flag         NUMBER := 0;
    e_line_error exception;
    l_exp_category  ELC_COMP.EXP_CATEGORY%TYPE;
    l_result        VARCHAR2(1) := NULL;
    l_record_status XXADEO_STG_ECOTAX_OUT.RECORD_STATUS%TYPE;
  
    CURSOR c_get_last_ecotax(in_item_id       VARCHAR2,
                             in_supplier_id   NUMBER,
                             in_item_exp_type VARCHAR2,
                             in_item_exp_seq  NUMBER,
                             in_comp_id       VARCHAR2,
                             in_seq           NUMBER) IS
      SELECT stg.seq_no, stg.EVENT_TYPE
        FROM XXADEO_STG_ECOTAX_OUT stg, ITEM_MASTER itm
       WHERE stg.ITEM = itm.ITEM
         AND itm.dept = I_dept
         AND stg.ITEM = in_item_id
         AND stg.SUPPLIER = in_supplier_id
         AND stg.ITEM_EXP_TYPE = in_item_exp_type
         AND stg.ITEM_EXP_SEQ = in_item_exp_seq
         AND stg.COMP_ID = in_comp_id
         AND stg.seq_no = (SELECT max(seq_no)
                             FROM XXADEO_STG_ECOTAX_OUT stgg, item_master im
                            WHERE stgg.ITEM = im.ITEM
                              AND im.dept = I_dept
                              AND stgg.ITEM = in_item_id
                              AND stgg.SUPPLIER = in_supplier_id
                              AND stgg.ITEM_EXP_TYPE = in_item_exp_type
                              AND stgg.ITEM_EXP_SEQ = in_item_exp_seq
                              AND stgg.COMP_ID = in_comp_id)
         AND stg.seq_no > in_seq
         AND stg.RECORD_STATUS = 'N';
  
    CURSOR c_get_next_ecotax(in_item_id       VARCHAR2,
                             in_supplier_id   NUMBER,
                             in_item_exp_type VARCHAR2,
                             in_item_exp_seq  NUMBER,
                             in_comp_id       VARCHAR2,
                             in_seq           NUMBER) IS
      SELECT stg.seq_no, stg.EVENT_TYPE
        FROM XXADEO_STG_ECOTAX_OUT stg, ITEM_MASTER itm
       WHERE stg.ITEM = itm.ITEM
         AND itm.dept = I_dept
         AND stg.ITEM = in_item_id
         AND stg.SUPPLIER = in_supplier_id
         AND stg.ITEM_EXP_TYPE = in_item_exp_type
         AND stg.ITEM_EXP_SEQ = in_item_exp_seq
         AND stg.COMP_ID = in_comp_id
         AND stg.seq_no > in_seq
         AND stg.RECORD_STATUS = 'N'
       ORDER BY stg.seq_no ASC;
  
  BEGIN
  
    OPEN c_unpub_ecotax;
    LOOP
      FETCH c_unpub_ecotax
        INTO L_unpub_ecotax;
      EXIT WHEN c_unpub_ecotax%NOTFOUND;
    
      SELECT XXADEO_CFAS_UTILS.GET_CFA_VALUE_NUM('ELC_COMP',
                                                 L_unpub_ecotax.comp_id,
                                                 (SELECT VALUE_1
                                                    FROM XXADEO_MOM_DVM
                                                   WHERE FUNC_AREA = 'CFA'
                                                     AND PARAMETER =
                                                         'ECOTAX_ELC_COMP'))
       into l_exp_category
        from dual;
    
      l_result := XXADEO_CFAS_UTILS.GET_CFA_VALUE_VARCHAR('ITEM_MASTER',
                                                          L_unpub_ecotax.ITEM,
                                                          l_exp_category);
      -- Update the record status field in case it changed since the opening of the cursor
    
      SELECT RECORD_STATUS
        INTO l_record_status
        FROM XXADEO_STG_ECOTAX_OUT
       WHERE seq_no = L_unpub_ecotax.seq_no;
    
      IF l_record_status like 'N' THEN
        -- Case message event type is 'C'
        IF L_unpub_ecotax.event_type like 'C' then
        
          IF l_result = 'Y' then
            L_last_msg_event_type := NULL;
          
            OPEN c_get_last_ecotax(L_unpub_ecotax.ITEM,
                                   L_unpub_ecotax.SUPPLIER,
                                   L_unpub_ecotax.ITEM_EXP_TYPE,
                                   L_unpub_ecotax.ITEM_EXP_SEQ,
                                   L_unpub_ecotax.COMP_ID,
                                   L_unpub_ecotax.seq_no);
            FETCH c_get_last_ecotax
              into l_last_seq_no, l_last_msg_event_type;
            CLOSE c_get_last_ecotax;
            --Case there are no identical record
            IF l_last_msg_event_type IS NULL THEN
              UPDATE XXADEO_STG_ECOTAX_OUT
                 SET RECORD_STATUS = 'R', LAST_UPDATE_TS = sysdate
               WHERE seq_no = L_unpub_ecotax.seq_no
                 AND ITEM = L_unpub_ecotax.ITEM
                 AND SUPPLIER = L_unpub_ecotax.SUPPLIER
                 AND ITEM_EXP_TYPE = L_unpub_ecotax.ITEM_EXP_TYPE
                 AND ITEM_EXP_SEQ = L_unpub_ecotax.ITEM_EXP_SEQ
                 AND COMP_ID = L_unpub_ecotax.COMP_ID
                 AND RECORD_STATUS = 'N';
              --Case of last message is 'D'
            ELSIF l_last_msg_event_type = 'D' then
              UPDATE XXADEO_STG_ECOTAX_OUT stg
                 SET RECORD_STATUS = 'D', LAST_UPDATE_TS = sysdate
               WHERE stg.seq_no >= L_unpub_ecotax.seq_no
                 AND stg.seq_no <= l_last_seq_no
                 AND ITEM = L_unpub_ecotax.ITEM
                 AND SUPPLIER = L_unpub_ecotax.SUPPLIER
                 AND ITEM_EXP_TYPE = L_unpub_ecotax.ITEM_EXP_TYPE
                 AND ITEM_EXP_SEQ = L_unpub_ecotax.ITEM_EXP_SEQ
                 AND COMP_ID = L_unpub_ecotax.COMP_ID
                 AND RECORD_STATUS = 'N';
            
              --Case of last message is 'C'
            ELSIF l_last_msg_event_type = 'C' then
              UPDATE XXADEO_STG_ECOTAX_OUT stg
                 SET RECORD_STATUS = 'R', LAST_UPDATE_TS = sysdate
               WHERE stg.seq_no = l_last_seq_no
                 AND ITEM = L_unpub_ecotax.ITEM
                 AND SUPPLIER = L_unpub_ecotax.SUPPLIER
                 AND ITEM_EXP_TYPE = L_unpub_ecotax.ITEM_EXP_TYPE
                 AND ITEM_EXP_SEQ = L_unpub_ecotax.ITEM_EXP_SEQ
                 AND COMP_ID = L_unpub_ecotax.COMP_ID
                 AND RECORD_STATUS = 'N';
            
              UPDATE XXADEO_STG_ECOTAX_OUT stg
                 SET RECORD_STATUS = 'D', LAST_UPDATE_TS = sysdate
               WHERE stg.seq_no < l_last_seq_no
                 AND ITEM = L_unpub_ecotax.ITEM
                 AND SUPPLIER = L_unpub_ecotax.SUPPLIER
                 AND ITEM_EXP_TYPE = L_unpub_ecotax.ITEM_EXP_TYPE
                 AND ITEM_EXP_SEQ = L_unpub_ecotax.ITEM_EXP_SEQ
                 AND COMP_ID = L_unpub_ecotax.COMP_ID
                 AND RECORD_STATUS = 'N';
            
              -- Case last message event is 'U'
            ELSIF l_last_msg_event_type = 'U' then
              l_update_flag := 0;
              -- Loop on all the newer records and apply a logic to mark them either D or R.
              OPEN c_get_next_ecotax(L_unpub_ecotax.ITEM,
                                     L_unpub_ecotax.SUPPLIER,
                                     L_unpub_ecotax.ITEM_EXP_TYPE,
                                     L_unpub_ecotax.ITEM_EXP_SEQ,
                                     L_unpub_ecotax.COMP_ID,
                                     L_unpub_ecotax.seq_no);
              LOOP
                FETCH c_get_next_ecotax
                  INTO l_next_seq_no, l_next_msg_event_type;
                EXIT WHEN c_get_next_ecotax%NOTFOUND;
              
                IF l_next_msg_event_type like 'D' THEN
                  UPDATE XXADEO_STG_ECOTAX_OUT stg
                     SET RECORD_STATUS = 'D', LAST_UPDATE_TS = sysdate
                   WHERE stg.seq_no = l_unpub_ecotax.seq_no
                     AND RECORD_STATUS = 'N';
                  l_update_flag := -1;
                  EXIT;
                
                ELSIF l_next_msg_event_type like 'C' THEN
                  UPDATE XXADEO_STG_ECOTAX_OUT stg
                     SET RECORD_STATUS = 'D', LAST_UPDATE_TS = sysdate
                   WHERE stg.seq_no = l_unpub_ecotax.seq_no
                     AND RECORD_STATUS = 'N';
                  l_update_flag := -1;
                  EXIT;
                
                ELSIF l_next_msg_event_type like 'U' THEN
                  UPDATE XXADEO_STG_ECOTAX_OUT stg
                     SET RECORD_STATUS = 'D', LAST_UPDATE_TS = sysdate
                   WHERE stg.seq_no = l_next_seq_no
                     AND RECORD_STATUS = 'N';
                END IF;
              END LOOP;
              CLOSE c_get_next_ecotax;
            
              IF l_update_flag = 0 THEN
                UPDATE XXADEO_STG_ECOTAX_OUT stg
                   SET RECORD_STATUS = 'R', LAST_UPDATE_TS = sysdate
                 WHERE stg.seq_no = L_unpub_ecotax.seq_no
                   AND RECORD_STATUS = 'N';
              END IF;
            END IF;
          ELSE
            UPDATE XXADEO_STG_ECOTAX_OUT
               SET RECORD_STATUS  = 'D',
                   LAST_UPDATE_TS = sysdate,
                   STATUS_MESSAGE = 'Ecotax CFA value is set to No'
             WHERE seq_no = L_unpub_ecotax.seq_no;
          END IF;
        
          -- End of case last message event is 'U'
        END IF;
      
        -- Case message event type is 'U'
        IF l_unpub_ecotax.event_type = 'U' then
          IF l_result = 'Y' then
            L_last_msg_event_type := NULL;
          
            OPEN c_get_last_ecotax(L_unpub_ecotax.ITEM,
                                   L_unpub_ecotax.SUPPLIER,
                                   L_unpub_ecotax.ITEM_EXP_TYPE,
                                   L_unpub_ecotax.ITEM_EXP_SEQ,
                                   L_unpub_ecotax.COMP_ID,
                                   L_unpub_ecotax.seq_no);
            FETCH c_get_last_ecotax
              into l_last_seq_no, l_last_msg_event_type;
            CLOSE c_get_last_ecotax;
          
            IF l_last_msg_event_type IS NULL THEN
              UPDATE XXADEO_STG_ECOTAX_OUT
                 SET RECORD_STATUS = 'R', LAST_UPDATE_TS = sysdate
               WHERE seq_no = L_unpub_ecotax.seq_no
                 AND ITEM = L_unpub_ecotax.ITEM
                 AND SUPPLIER = L_unpub_ecotax.SUPPLIER
                 AND ITEM_EXP_TYPE = L_unpub_ecotax.ITEM_EXP_TYPE
                 AND ITEM_EXP_SEQ = L_unpub_ecotax.ITEM_EXP_SEQ
                 AND COMP_ID = L_unpub_ecotax.COMP_ID
                 AND RECORD_STATUS = 'N';
            
            ELSIF l_last_msg_event_type = 'D' THEN
              UPDATE XXADEO_STG_ECOTAX_OUT
                 SET RECORD_STATUS = 'D', LAST_UPDATE_TS = sysdate
               WHERE seq_no = L_unpub_ecotax.seq_no
                 AND ITEM = L_unpub_ecotax.ITEM
                 AND SUPPLIER = L_unpub_ecotax.SUPPLIER
                 AND ITEM_EXP_TYPE = L_unpub_ecotax.ITEM_EXP_TYPE
                 AND ITEM_EXP_SEQ = L_unpub_ecotax.ITEM_EXP_SEQ
                 AND COMP_ID = L_unpub_ecotax.COMP_ID
                 AND RECORD_STATUS = 'N';
            
            ELSIF l_last_msg_event_type = 'U' THEN
              OPEN c_get_next_ecotax(L_unpub_ecotax.ITEM,
                                     L_unpub_ecotax.SUPPLIER,
                                     L_unpub_ecotax.ITEM_EXP_TYPE,
                                     L_unpub_ecotax.ITEM_EXP_SEQ,
                                     L_unpub_ecotax.COMP_ID,
                                     L_unpub_ecotax.seq_no);
              LOOP
              
                FETCH c_get_next_ecotax
                  INTO l_next_seq_no, l_next_msg_event_type;
                EXIT WHEN c_get_next_ecotax%NOTFOUND;
              
                IF l_next_msg_event_type like 'D' THEN
                  UPDATE XXADEO_STG_ECOTAX_OUT stg
                     SET RECORD_STATUS = 'D', LAST_UPDATE_TS = sysdate
                   WHERE stg.seq_no = l_unpub_ecotax.seq_no
                     AND RECORD_STATUS = 'N';
                  EXIT;
                
                ELSIF l_next_msg_event_type like 'C' THEN
                  UPDATE XXADEO_STG_ECOTAX_OUT stg
                     SET RECORD_STATUS = 'D', LAST_UPDATE_TS = sysdate
                   WHERE stg.seq_no = l_unpub_ecotax.seq_no
                     AND RECORD_STATUS = 'N';
                  EXIT;
                
                ELSIF l_next_msg_event_type like 'U' THEN
                  UPDATE XXADEO_STG_ECOTAX_OUT stg
                     SET RECORD_STATUS = 'D', LAST_UPDATE_TS = sysdate
                   WHERE stg.seq_no = l_unpub_ecotax.seq_no
                     AND RECORD_STATUS = 'N';
                  EXIT;
                END IF;
              
              END LOOP;
              CLOSE c_get_next_ecotax;
            END IF;
          ELSE
            UPDATE XXADEO_STG_ECOTAX_OUT
               SET RECORD_STATUS  = 'D',
                   LAST_UPDATE_TS = sysdate,
                   STATUS_MESSAGE = 'Ecotax CFA value is set to No'
             WHERE seq_no = L_unpub_ecotax.seq_no;
          END IF;
        END IF;
      
        -- Case the record event type is 'D'
        IF l_unpub_ecotax.event_type = 'D' then
          L_last_msg_event_type := NULL;
        
          OPEN c_get_last_ecotax(L_unpub_ecotax.ITEM,
                                 L_unpub_ecotax.SUPPLIER,
                                 L_unpub_ecotax.ITEM_EXP_TYPE,
                                 L_unpub_ecotax.ITEM_EXP_SEQ,
                                 L_unpub_ecotax.COMP_ID,
                                 L_unpub_ecotax.seq_no);
          FETCH c_get_last_ecotax
            into l_last_seq_no, l_last_msg_event_type;
          CLOSE c_get_last_ecotax;
        
          IF l_last_msg_event_type IS NULL THEN
            UPDATE XXADEO_STG_ECOTAX_OUT
               SET RECORD_STATUS = 'R', LAST_UPDATE_TS = sysdate
             WHERE seq_no = L_unpub_ecotax.seq_no
               AND ITEM = L_unpub_ecotax.ITEM
               AND SUPPLIER = L_unpub_ecotax.SUPPLIER
               AND ITEM_EXP_TYPE = L_unpub_ecotax.ITEM_EXP_TYPE
               AND ITEM_EXP_SEQ = L_unpub_ecotax.ITEM_EXP_SEQ
               AND COMP_ID = L_unpub_ecotax.COMP_ID;
          
          ELSIF l_last_msg_event_type = 'C' then
            UPDATE XXADEO_STG_ECOTAX_OUT
               SET RECORD_STATUS  = 'R',
                   EVENT_TYPE     = 'U',
                   LAST_UPDATE_TS = sysdate
             WHERE seq_no = l_last_seq_no
               AND ITEM = L_unpub_ecotax.ITEM
               AND SUPPLIER = L_unpub_ecotax.SUPPLIER
               AND ITEM_EXP_TYPE = L_unpub_ecotax.ITEM_EXP_TYPE
               AND ITEM_EXP_SEQ = L_unpub_ecotax.ITEM_EXP_SEQ
               AND COMP_ID = L_unpub_ecotax.COMP_ID
               AND RECORD_STATUS = 'N';
          
            UPDATE XXADEO_STG_ECOTAX_OUT
               SET RECORD_STATUS = 'D', LAST_UPDATE_TS = sysdate
             WHERE seq_no < l_last_seq_no
               AND ITEM = L_unpub_ecotax.ITEM
               AND SUPPLIER = L_unpub_ecotax.SUPPLIER
               AND ITEM_EXP_TYPE = L_unpub_ecotax.ITEM_EXP_TYPE
               AND ITEM_EXP_SEQ = L_unpub_ecotax.ITEM_EXP_SEQ
               AND COMP_ID = L_unpub_ecotax.COMP_ID
               AND RECORD_STATUS = 'N';
          
          ELSIF l_last_msg_event_type = 'D' then
            UPDATE XXADEO_STG_ECOTAX_OUT
               SET RECORD_STATUS = 'R', LAST_UPDATE_TS = sysdate
             WHERE seq_no = l_last_seq_no
               AND ITEM = L_unpub_ecotax.ITEM
               AND SUPPLIER = L_unpub_ecotax.SUPPLIER
               AND ITEM_EXP_TYPE = L_unpub_ecotax.ITEM_EXP_TYPE
               AND ITEM_EXP_SEQ = L_unpub_ecotax.ITEM_EXP_SEQ
               AND COMP_ID = L_unpub_ecotax.COMP_ID
               AND RECORD_STATUS = 'N';
          
            UPDATE XXADEO_STG_ECOTAX_OUT
               SET RECORD_STATUS = 'D', LAST_UPDATE_TS = sysdate
             WHERE seq_no < l_last_seq_no
               AND ITEM = L_unpub_ecotax.ITEM
               AND SUPPLIER = L_unpub_ecotax.SUPPLIER
               AND ITEM_EXP_TYPE = L_unpub_ecotax.ITEM_EXP_TYPE
               AND ITEM_EXP_SEQ = L_unpub_ecotax.ITEM_EXP_SEQ
               AND COMP_ID = L_unpub_ecotax.COMP_ID
               AND RECORD_STATUS = 'N';
          END IF;
        END IF;
      END IF;
    END LOOP;
  
    CLOSE c_unpub_ecotax;
  
    if o_status is null then
      o_status := 0;
    end if;
  
  EXCEPTION
    WHEN OTHERS THEN
      o_message := sqlerrm;
      o_status  := 1;
  END POPULATE;

END XXADEO_ECOTAX_THREAD;
/