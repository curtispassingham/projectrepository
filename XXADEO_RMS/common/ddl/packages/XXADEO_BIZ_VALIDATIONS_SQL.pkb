CREATE OR REPLACE PACKAGE BODY XXADEO_BIZ_VALIDATIONS_SQL AS

  cursor C_get_cfa_attrib_dept is
    select value_1 attrib_id
      from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                    I_parameter => XXADEO_MERCHHIER_ATTRIB_SQL.DEPARTMENT_STATUS_SHEET));
  --
  cursor C_get_cfa_attrib_class is
    select value_1 attrib_id
      from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                    I_parameter => XXADEO_MERCHHIER_ATTRIB_SQL.CLASS_STATUS_SHEET));
  --
  cursor C_get_cfa_attrib_sclass is
    select value_1 attrib_id
      from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                    I_parameter => XXADEO_MERCHHIER_ATTRIB_SQL.SCLASS_STATUS_SHEET));
  --
  cursor C_get_status_active is
    select code
      from code_detail
     where code_type = XXADEO_MERCHHIER_ATTRIB_SQL.XXADEO_ATTR_STATUS_CODE
       and upper(code_desc) = XXADEO_MERCHHIER_ATTRIB_SQL.XXADEO_ACTIVE;
  --
  cursor C_get_status_inactive is
    select code
      from code_detail
     where code_type = XXADEO_MERCHHIER_ATTRIB_SQL.XXADEO_ATTR_STATUS_CODE
       and upper(code_desc) = XXADEO_MERCHHIER_ATTRIB_SQL.XXADEO_INACTIVE;  
  --                            
--------------------------------------------------------------------------------

FUNCTION VALIDATE_ITEM(O_error_message           IN OUT   LOGGER_LOGS.TEXT%TYPE,
                       O_item_status                OUT   ITEM_MASTER.STATUS%TYPE,
                       I_item                    IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
  --
  L_program   VARCHAR2(64) := 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_ITEM';
  --
  cursor C_check_item_status is 
     select status
       from item_master
      where item = I_item;
  --
BEGIN
  --
  open C_check_item_status;
  fetch C_check_item_status into O_item_status;
  close C_check_item_status;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
  --
END VALIDATE_ITEM;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_SUPPLIER(O_error_message  IN OUT   LOGGER_LOGS.TEXT%TYPE,
                                O_valid_item_sup    OUT   VARCHAR2,
                                I_item           IN       ITEM_MASTER.ITEM%TYPE,
                                I_supplier       IN       ITEM_SUPPLIER.SUPPLIER%TYPE)
RETURN BOOLEAN IS
  --
  L_program   VARCHAR2(64) := 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_ITEM_SUPPLIER';
  --
  cursor C_check_item_supp is
     select 'Y'
       from item_supplier
      where item     = I_item
        and supplier = I_supplier;
  --
BEGIN
  --
  open C_check_item_supp;
  fetch C_check_item_supp into O_valid_item_sup;
    --
    if C_check_item_supp%NOTFOUND then
      --
      O_valid_item_sup := 'N';
      --
    end if;
    --
  close C_check_item_supp;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    RETURN FALSE;
    --
  --
END VALIDATE_ITEM_SUPPLIER;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_RANGE(O_error_message     IN OUT   LOGGER_LOGS.TEXT%TYPE,
                             O_status               OUT   ITEM_LOC.STATUS%TYPE,
                             O_ranged_ind           OUT   ITEM_LOC.RANGED_IND%TYPE,
                             I_item              IN       ITEM_MASTER.ITEM%TYPE,
                             I_item_parent       IN       ITEM_MASTER.ITEM_PARENT%TYPE DEFAULT NULL,
                             I_loc               IN       ITEM_LOC.LOC%TYPE,
                             I_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS
  --
  L_program   VARCHAR2(64) := 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_ITEM_RANGE';
  --
  cursor C_check_itemloc is
     select status,
            ranged_ind
       from item_loc
      where item     = nvl(I_item_parent,I_item)
        and loc      = I_loc
        and loc_type = I_loc_type
        and rownum   = 1;
  --
BEGIN
  --
  open C_check_itemloc;
  fetch C_check_itemloc into O_status,
                             O_ranged_ind;
  close C_check_itemloc;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    RETURN FALSE;
    --
  --
END VALIDATE_ITEM_RANGE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_MERCH_HIER(O_error_message     IN OUT   LOGGER_LOGS.TEXT%TYPE,
                             I_dept              IN       DEPS.DEPT%TYPE DEFAULT NULL,
                             I_class             IN       CLASS.CLASS%TYPE DEFAULT NULL,
                             I_subclass          IN       SUBCLASS.SUBCLASS%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
  --
  L_program   VARCHAR2(64) := 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER';
  L_return    VARCHAR2(1)  := 'N';
  L_context   VARCHAR2(100) := NULL;
  --
  cursor C_check_merchhier is
  select 'Y'
    from dual
   where exists(select 1
                  from v_deps d,
                       v_class c,
                       v_subclass s
                 where d.dept     = c.dept(+)
                   and c.dept     = s.dept(+)
                   and c.class    = s.class(+)
                   and d.dept     = I_dept
                   and (c.class  = nvl(I_class,c.class)
                        or I_class is null)
                   and (s.subclass = nvl(I_subclass,s.subclass)
                        or I_subclass is null));
  --
BEGIN
  --
  -- DEPT/CLASS/SUBCLASS Validation
  open C_check_merchhier;
  fetch C_check_merchhier into L_return;
  --
  if C_check_merchhier%NOTFOUND then
    --
    close C_check_merchhier;
    return FALSE;
    --
  end if;
  --
  close C_check_merchhier;
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    return FALSE;
    --
  --
END VALIDATE_MERCH_HIER;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_LANGUAGE(O_error_message       IN OUT LOGGER_LOGS.TEXT%TYPE,
                           IO_error              IN OUT BOOLEAN,
                           I_lang                IN     LANG.LANG%TYPE)
RETURN BOOLEAN IS
  --
  L_program      VARCHAR2(64):= 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_LANGUAGE';
  L_lang_exists  VARCHAR2(1) := 'N';
  L_lang_valid   VARCHAR2(1) := 'N';
  --
  cursor C_chk_lang_exists is 
     select 'Y'
       from lang
      where lang = I_lang;
  --
  cursor C_chk_valid_lang is 
     select 'Y'
       from code_detail
      where code_type = GP_CODE_TYPE_LANG
        and code = to_char(I_lang);
  --
BEGIN
  --
  if not IO_error then
    IO_error := FALSE;
  end if;
  --
  open C_chk_lang_exists;
  fetch C_chk_lang_exists into L_lang_exists;
  close C_chk_lang_exists;
  --
  if L_lang_exists = 'N' then
    IO_error := TRUE;
    RETURN FALSE;
  end if;
  --
  open C_chk_valid_lang;
  fetch C_chk_valid_lang into L_lang_valid;
  close C_chk_valid_lang;
  --
  if L_lang_valid = 'N' then
    IO_error := TRUE;
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
  --
END VALIDATE_LANGUAGE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_VAT_DEPT_REG_TYPE(O_error_message     IN OUT   LOGGER_LOGS.TEXT%TYPE,
                                    I_dept              IN       VAT_DEPS.DEPT%TYPE,
                                    I_vat_region        IN       VAT_DEPS.VAT_REGION%TYPE,
                                    I_vat_type          IN       VAT_DEPS.VAT_TYPE%TYPE)
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(64) := 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_VAT_REGION';
  L_dept_reg_type  VARCHAR2(1)  := 'N';
  --
  cursor C_chk_dept_reg_type is 
    select 'Y'
      from vat_deps
     where dept = I_dept
       and vat_region = I_vat_region
       and vat_type = I_vat_type;
  --
BEGIN
  --
  open C_chk_dept_reg_type;
  fetch C_chk_dept_reg_type into L_dept_reg_type;
  close C_chk_dept_reg_type;
  --
  if L_dept_reg_type = 'Y' then
    RETURN FALSE;
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
  --
END VALIDATE_VAT_DEPT_REG_TYPE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_VAT_REGION(O_error_message     IN OUT   LOGGER_LOGS.TEXT%TYPE,
                             I_vat_region        IN       VAT_DEPS.VAT_REGION%TYPE)
RETURN BOOLEAN IS
  --
  L_program     VARCHAR2(64) := 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_VAT_REGION';
  L_vat_region  VARCHAR2(1)  := 'N';
  --
  cursor C_chk_vat_region is 
    select 'Y'
      from vat_region
     where vat_region = I_vat_region;
  --
BEGIN
  --
  open C_chk_vat_region;
  fetch C_chk_vat_region into L_vat_region;
  close C_chk_vat_region;
  --
  if L_vat_region = 'N' then
    RETURN FALSE;
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
  --
END VALIDATE_VAT_REGION;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_VAT_CODE(O_error_message       IN OUT   LOGGER_LOGS.TEXT%TYPE,
                           I_vat_code            IN       VAT_CODES.VAT_CODE%TYPE)
RETURN BOOLEAN IS
  --
  L_program   VARCHAR2(64) := 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_VAT_CODE';
  L_vat_code  VARCHAR2(1)  := 'N';
  --
  cursor C_chk_vat_code is 
     select 'Y'
       from vat_codes
      where vat_code = I_vat_code;
  --
BEGIN
  --
  open C_chk_vat_code;
  fetch C_chk_vat_code into L_vat_code;
  close C_chk_vat_code;
  --
  if L_vat_code = 'N' then
    RETURN FALSE;
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
  --
END VALIDATE_VAT_CODE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_VAT_TYPE(O_error_message       IN OUT   LOGGER_LOGS.TEXT%TYPE,
                           I_vat_type            IN       VAT_DEPS.VAT_TYPE%TYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_VAT_TYPE';
  L_chk_vat_type  VARCHAR2(1)  := 'N';
  --
  cursor C_vat_type is
    select 'Y'
      from code_detail
     where code_type = GP_CODE_TYPE_VAT
       and code = I_vat_type;
  --
BEGIN
  --
  -- C = Cost / R = Retail / B = Both
  open C_vat_type;
  fetch C_vat_type into L_chk_vat_type;
  close C_vat_type;

  if L_chk_vat_type = 'N' then
    RETURN FALSE;
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
  --
END VALIDATE_VAT_TYPE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DOMAIN(O_error_message         IN OUT   LOGGER_LOGS.TEXT%TYPE,
                         I_domain_id             IN       DOMAIN.DOMAIN_ID%TYPE)
RETURN BOOLEAN IS
  --
  L_program     VARCHAR2(64) := 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_DOMAIN';
  L_chk_domain  VARCHAR2(1)  := 'N';
  --
  cursor C_chk_domain is
     select 'Y'
       from domain
      where domain_id = I_domain_id;
  --
BEGIN
  --
  open C_chk_domain;
  fetch C_chk_domain into L_chk_domain;
  close C_chk_domain;
  --
  if L_chk_domain = 'N' then
    RETURN FALSE;
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
  --
END VALIDATE_DOMAIN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DOMAIN_ASSOC(O_error_message         IN OUT   LOGGER_LOGS.TEXT%TYPE,
                               I_process_id            IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                               I_domain_id             IN       DOMAIN.DOMAIN_ID%TYPE,
                               I_dept                  IN       DOMAIN_DEPT.DEPT%TYPE DEFAULT NULL,
                               I_class                 IN       DOMAIN_CLASS.CLASS%TYPE DEFAULT NULL,
                               I_subclass              IN       DOMAIN_SUBCLASS.SUBCLASS%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
  --
  L_program     VARCHAR2(64) := 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_DOMAIN_ASSOC';
  --
  L_chk_domain_assoc      VARCHAR2(1)  := 'N';
  L_chk_domain_assoc_stg  VARCHAR2(1)  := 'N';
  --
  cursor C_chk_domain_assoc is
    select 'Y'
       from domain_dept
      where dept     = I_dept
     union all
     select 'Y'
       from domain_class
      where dept     = I_dept
        and class    = nvl(I_class,class)
     union all
     select 'Y'
       from domain_subclass
      where dept     = I_dept
        and class    = nvl(I_class,class)
        and subclass = nvl(I_subclass,subclass);
  --
  cursor C_chk_domain_assoc_stg is
    select 'Y'
       from xxadeo_rdf_domains_dept_stg
      where dept               = I_dept
        and xxadeo_process_id != I_process_id
     union all
     select 'Y'
       from xxadeo_rdf_domains_class_stg
      where dept               = I_dept
        and class              = nvl(I_class,class)
        and xxadeo_process_id != I_process_id
     union all
     select 'Y'
       from xxadeo_rdf_domains_sclass_stg
      where dept               = I_dept
        and class              = nvl(I_class,class)
        and subclass           = nvl(I_subclass,subclass)
        and xxadeo_process_id != I_process_id;
  --
BEGIN
  --
  open C_chk_domain_assoc;
  fetch C_chk_domain_assoc into L_chk_domain_assoc;
  close C_chk_domain_assoc;
  --
  open C_chk_domain_assoc_stg;
  fetch C_chk_domain_assoc_stg into L_chk_domain_assoc_stg;
  close C_chk_domain_assoc_stg;
  --
  if L_chk_domain_assoc = 'Y' or L_chk_domain_assoc_stg = 'Y' then
    RETURN FALSE;
  end if;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
  --
END VALIDATE_DOMAIN_ASSOC;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_MERCH_HIER_ACT(O_error_message   IN OUT   LOGGER_LOGS.TEXT%TYPE,
                                 I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                                 I_status          IN       VARCHAR2,
                                 I_dept            IN       DEPS.DEPT%TYPE DEFAULT NULL,
                                 I_class           IN       CLASS.CLASS%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
  --
  L_program   VARCHAR2(64) := 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER_ACT';
  --
  L_return           VARCHAR2(1);
  L_cfa_attrib_dept  CFA_ATTRIB.ATTRIB_ID%TYPE := NULL;
  L_cfa_attrib_class CFA_ATTRIB.ATTRIB_ID%TYPE := NULL;
  --
  cursor C_check_class_act( I_dept        deps.dept%type ) is
  select 'Y'
    from dual
   where exists( select 1
                   from v_deps vd,
                        table(XXADEO_CFA_SQL.SEARCH_CFA_BY_ATTRIB(I_attrib_id              => L_cfa_attrib_dept,
                                                                  I_xxadeo_cfa_key_val_tbl => CAST(MULTISET
                                                                                                (select new XXADEO_CFA_KEY_VAL_OBJ(key_col_1   => 'DEPT', 
                                                                                                                                   key_value_1 => vd.dept)
                                                                                                   from dual) as XXADEO_CFA_KEY_VAL_TBL))) cfa_tbl
                 
                  where vd.dept = cfa_tbl.key_col_1
                    and vd.dept = I_dept
                    and upper(trim(cfa_tbl.cfa_value)) = I_status
                 union all
                 select 1
                   from xxadeo_dept_status_stg x
                  where (x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_new or
                        x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_mod) 
                    and x.dept_status = I_status
                    and x.dept = I_dept 
                    and x.xxadeo_process_id != I_process_id);
  --
  cursor C_check_subclass_act( I_dept        deps.dept%type,
                               I_class       class.class%type ) is
  select 'Y'
    from dual
   where exists( select 1
                   from v_class vc,
                        table(xxadeo_cfa_sql.SEARCH_CFA_BY_ATTRIB(I_attrib_id              => L_cfa_attrib_class,
                                                                  I_xxadeo_cfa_key_val_tbl => CAST(MULTISET
                                                                                                (select new XXADEO_CFA_KEY_VAL_OBJ(key_col_1   => 'DEPT', 
                                                                                                                                   key_col_2   => 'CLASS',
                                                                                                                                   key_value_1 => dept,
                                                                                                                                   key_value_2 => class)
                                                                                                   from dual) as XXADEO_CFA_KEY_VAL_TBL))) cfa_tbl
                  where vc.dept = cfa_tbl.key_col_1
                    and vc.class = cfa_tbl.key_col_2
                    and vc.dept = I_dept
                    and vc.class = I_class
                    and upper(trim(cfa_tbl.cfa_value)) = I_status
                 union all
                 select 1
                   from xxadeo_class_status_stg x
                  where (x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_new or
                        x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_mod)
                    and x.class_status = I_status
                    and x.dept = I_dept
                    and x.class = I_class
                    and x.xxadeo_process_id != I_process_id);
  --
BEGIN
  --
  open C_get_cfa_attrib_dept;
  fetch C_get_cfa_attrib_dept into L_cfa_attrib_dept;
  close C_get_cfa_attrib_dept;
  --
  open C_get_cfa_attrib_class;
  fetch C_get_cfa_attrib_class into L_cfa_attrib_class;
  close C_get_cfa_attrib_class;
  --
  if I_class is null then
    --
    -- Validate Class Activation
    open C_check_class_act(I_dept);
    fetch C_check_class_act into L_return;
      --
      if C_check_class_act%NOTFOUND then
        --
        close C_check_class_act;
        return FALSE;
        --
      end if;
      --
    close C_check_class_act;
    --
  elsif I_class is not null then
    --
    -- Validate Subclass Activation
    open C_check_subclass_act(I_dept,
                              I_class);
    fetch C_check_subclass_act into L_return;
      --
      --
      if C_check_subclass_act%NOTFOUND then
        --
        close C_check_subclass_act;
        return FALSE;
        --
      end if;
      --
    close C_check_subclass_act;
    --
  end if;
  --
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    return FALSE;
    --
  --
END VALIDATE_MERCH_HIER_ACT;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_MERCH_HIER_DEACT(O_error_message   IN OUT   LOGGER_LOGS.TEXT%TYPE,
                                   I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                                   I_status          IN       VARCHAR2,
                                   I_dept            IN       DEPS.DEPT%TYPE DEFAULT NULL,
                                   I_class           IN       CLASS.CLASS%TYPE DEFAULT NULL,
                                   I_subclass        IN       SUBCLASS.SUBCLASS%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
  --
  L_program                VARCHAR2(64) := 'XXADEO_BIZ_VALIDATIONS_SQL.VALIDATE_MERCH_HIER_DEACT';
  --
  L_return                 VARCHAR2(1)               := 'N';
  L_cfa_attrib_class       CFA_ATTRIB.ATTRIB_ID%TYPE := NULL;
  L_cfa_attrib_sclass      CFA_ATTRIB.ATTRIB_ID%TYPE := NULL;
  L_active_status          VARCHAR2(10)              := NULL;
  L_inactive_status        VARCHAR2(10)              := NULL;
  L_exists_inactive_class  VARCHAR2(1)               := 'N';
  L_exists_inactive_sclass VARCHAR2(1)               := 'N';
  --
  cursor C_check_dept_deact( I_dept        deps.dept%type ) is
  select 'Y'
    from dual
   where exists( select 1
                   from v_class vc,
                        table(XXADEO_CFA_SQL.SEARCH_CFA_BY_ATTRIB(I_attrib_id              => L_cfa_attrib_class,
                                                                  I_xxadeo_cfa_key_val_tbl => CAST(MULTISET
                                                                                                (select new XXADEO_CFA_KEY_VAL_OBJ(key_col_1   => 'DEPT', 
                                                                                                                                   key_col_2   => 'CLASS',
                                                                                                                                   key_value_1 => vc.dept,
                                                                                                                                   key_value_2 => vc.class)
                                                                                                   from dual) as XXADEO_CFA_KEY_VAL_TBL))) cfa_tbl
                  where vc.dept = cfa_tbl.key_col_1
                    and vc.class = cfa_tbl.key_col_2
                    and vc.dept = I_dept
                    and upper(trim(cfa_tbl.cfa_value)) = L_active_status
                    and not exists (select 1
                                      from xxadeo_class_status_stg x
                                     where (x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_new or 
                                           x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_mod)
                                       and x.class_status = L_inactive_status
                                       and x.dept = vc.dept
                                       and x.class = vc.class
                                       and x.xxadeo_process_id != I_process_id)
                    and rownum = 1
                 union all
                 select 1
                   from xxadeo_class_status_stg x
                  where (x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_new or 
                        x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_mod)
                    and x.class_status = L_active_status
                    and x.dept = I_dept
                    and x.xxadeo_process_id != I_process_id
                    and rownum = 1);
  --
  cursor C_check_class_deact( I_dept        deps.dept%type,
                              I_class       class.class%type ) is
  select 'Y'
    from dual
   where exists (select 1
                   from v_subclass vs, 
                        table(XXADEO_CFA_SQL.SEARCH_CFA_BY_ATTRIB(I_attrib_id              => L_cfa_attrib_sclass,
                                                                  I_xxadeo_cfa_key_val_tbl => CAST(MULTISET
                                                                                                (select new XXADEO_CFA_KEY_VAL_OBJ(key_col_1 => 'DEPT', 
                                                                                                                                   key_col_2 => 'CLASS', 
                                                                                                                                   key_col_3 => 'SUBCLASS', 
                                                                                                                                   key_value_1 => vs.dept, 
                                                                                                                                   key_value_2 => vs.class,
                                                                                                                                   key_value_3 => vs.subclass)
                                                                                                   from dual) as XXADEO_CFA_KEY_VAL_TBL))) cfa_tbl
                  where vs.dept = cfa_tbl.key_col_1 
                    and vs.class = cfa_tbl.key_col_2
                    and vs.subclass = cfa_tbl.key_col_3
                    and vs.dept = I_dept 
                    and vs.class = I_class 
                    and upper(trim(cfa_tbl.cfa_value)) = L_active_status
                    and not exists (select 1
                                      from xxadeo_subclass_status_stg x
                                     where (x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_new or
                                           x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_mod) 
                                       and x.subclass_status = L_inactive_status
                                       and x.dept            = vs.dept
                                       and x.class           = vs.class
                                       and x.subclass        = vs.subclass
                                       and x.xxadeo_process_id != I_process_id)
                    and rownum = 1
                 union all
                 select 1
                   from xxadeo_subclass_status_stg x
                  where (x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_new or
                        x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_mod)
                    and x.subclass_status = L_active_status
                    and x.dept = I_dept
                    and x.class = I_class
                    and x.xxadeo_process_id != I_process_id
                    and rownum = 1);
  --
  cursor C_check_dept_deact_stg is 
    select 'Y'
      from dual 
     where exists (select 1
                     from xxadeo_class_status_stg x
                    where (x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_new or 
                          x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_mod)
                      and x.class_status = L_inactive_status
                      and x.dept = I_dept
                      and x.xxadeo_process_id != I_process_id);
  --
  cursor C_check_class_deact_stg is 
    select 'Y'
      from dual 
     where exists (select 1
                     from xxadeo_subclass_status_stg x
                    where (x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_new or
                          x.action = XXADEO_MERCHHIER_ATTRIB_SQL.action_mod) 
                      and x.subclass_status = L_inactive_status
                      and x.dept            = I_dept
                      and x.class           = I_class
                      and x.xxadeo_process_id != I_process_id);
    
  --
  cursor C_check_subclass_deact( I_dept        deps.dept%type,
                                 I_class       class.class%type,
                                 I_subclass    subclass.subclass%type ) is
  select 'Y'
    from dual
   where exists( select 1
                   from item_master i
                  where i.dept     = I_dept
                    and i.class    = I_class
                    and i.subclass = I_subclass
                    and rownum     = 1);
  --
BEGIN
  --
  open C_get_status_active;
  fetch C_get_status_active into L_active_status;
  close C_get_status_active;
  --
  open C_get_status_inactive;
  fetch C_get_status_inactive into L_inactive_status;
  close C_get_status_inactive;
  --
  open C_get_cfa_attrib_class;
  fetch C_get_cfa_attrib_class into L_cfa_attrib_class;
  close C_get_cfa_attrib_class;
  --
  open C_get_cfa_attrib_sclass;
  fetch C_get_cfa_attrib_sclass into L_cfa_attrib_sclass;
  close C_get_cfa_attrib_sclass;
  --
  if I_class is null and I_subclass is null then
    --
    -- Validate Dept Deactivation
    --
    open C_check_dept_deact_stg;
    fetch C_check_dept_deact_stg into L_exists_inactive_class;
    close C_check_dept_deact_stg;
    --
    open C_check_dept_deact(I_dept);
    fetch C_check_dept_deact into L_return;
    close C_check_dept_deact;
    --
    if (L_return = 'Y' and L_exists_inactive_class = 'N') or L_return = 'Y' then 
      --
      return FALSE;
      --
    end if;
    --
  elsif I_class is not null and I_subclass is null then
    --
    -- Validate Class Deactivation
    --
    open C_check_class_deact_stg; 
    fetch C_check_class_deact_stg into L_exists_inactive_sclass;
    close C_check_class_deact_stg;
    --
    open C_check_class_deact(I_dept,
                             I_class);
    fetch C_check_class_deact into L_return;
    close C_check_class_deact;
    --
    if (L_return = 'Y' and L_exists_inactive_sclass = 'N') or L_return = 'Y'  then
      --
      return FALSE;
      --
    end if;
    --
  elsif I_class is not null and I_subclass is not null then
    -- Validate Subclass Deactivation
    open C_check_subclass_deact(I_dept,
                                I_class,
                                I_subclass);
    fetch C_check_subclass_deact into L_return;
      --
      if C_check_subclass_deact%FOUND then
        --
        close C_check_subclass_deact;
        return FALSE;
        --
      end if;
      --
    close C_check_subclass_deact;
    --
  end if;
  --
  return TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    return FALSE;
    --
  --
END VALIDATE_MERCH_HIER_DEACT;
--------------------------------------------------------------------------------
END XXADEO_BIZ_VALIDATIONS_SQL;
/
