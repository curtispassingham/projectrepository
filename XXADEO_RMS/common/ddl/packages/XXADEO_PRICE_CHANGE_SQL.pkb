CREATE OR REPLACE PACKAGE BODY XXADEO_PRICE_CHANGE_SQL AS

--------------------------------------------------------------------------------
  --
  LP_price_event_id_seq  NUMBER := 0;
  --
  LP_FUNC_AREA        CONSTANT VARCHAR2(250) := 'PRICE_CHANGE';
  LP_BU_ZONE_TYPE     XXADEO_RPM_BU_PRICE_ZONES.BU_ZONE_TYPE%TYPE := 'NATZ';
  --
  LP_vdate            DATE := get_vdate;
  LP_worksheet_status CONSTANT VARCHAR2(1) := 'W';
  LP_error_status     CONSTANT VARCHAR2(1) := 'E';
  --
  LP_type_pc          VARCHAR2(10) := GP_type_pc;
  --
  SAVE_N_RETURN       EXCEPTION;
  --
--------------------------------------------------------------------------------
  -- Global Cursor
  cursor C_get_reason_code (I_code  VARCHAR2) is
    select code_id
      from rpm_codes rc
     where exists (select 1
                     from code_detail cd
                    where code_type    = XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_TYPE
                      and code         = I_code
                      and cd.code_desc = rc.code);
  --
--------------------------------------------------------------------------------
FUNCTION RPM_BASE_VALIDATIONS(O_error_message                  OUT VARCHAR2,
                              O_error_count                    OUT NUMBER,
                              I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program                   VARCHAR2(64) := 'XXADEO_PRICE_INJECTOR_SQL.RPM_BASE_VALIDATIONS';
  --
  L_zone_ranging              NUMBER := 0;
  L_open_zone_use             NUMBER := 0;
  L_recognize_wh_as_locations NUMBER := 0;
  L_selling_uom               XXADEO_RPM_STAGE_PRICE_CHANGE.CHANGE_SELLING_UOM%TYPE;
  L_default_standard_uom      XXADEO_RPM_STAGE_PRICE_CHANGE.CHANGE_SELLING_UOM%TYPE;
  --
  cursor C_get_error_count is
    select COUNT(1)
      from xxadeo_rpm_stage_price_change
     where xxadeo_process_id     = I_xxadeo_rpm_stg_price_change.xxadeo_process_id
       and stage_price_change_id = I_xxadeo_rpm_stg_price_change.stage_price_change_id
       and status     = LP_error_status;
  --
  cursor C_get_selling_uom is
    select il.selling_uom
      from item_loc il
     where il.item = I_xxadeo_rpm_stg_price_change.item
       and (il.loc  = I_xxadeo_rpm_stg_price_change.location
        or exists (select 1
                     from rpm_zone_location rzl
                    where rzl.zone_id = I_xxadeo_rpm_stg_price_change.zone_id
                      and rzl.location = il.loc))
       and rownum = 1;
    --
  --
BEGIN
  --
  select zone_ranging,
         open_zone_use,
         recognize_wh_as_locations
    into L_zone_ranging,
         L_open_zone_use,
         L_recognize_wh_as_locations
    from rpm_system_options;
  --
  select s.default_standard_uom
    into L_default_standard_uom
    from system_options s;
  --
  open C_get_selling_uom;
  fetch C_get_selling_uom into L_selling_uom;
  close C_get_selling_uom;
  --
  update xxadeo_rpm_stage_price_change xrspc
     set xrspc.change_selling_uom = nvl(L_selling_uom,L_default_standard_uom),
         xrspc.ignore_constraints = 1,
         xrspc.auto_approve_ind   = 1,
         xrspc.vendor_funded_ind  = 0
   where xrspc.xxadeo_process_id     = I_xxadeo_rpm_stg_price_change.xxadeo_process_id
     and xrspc.stage_price_change_id = I_xxadeo_rpm_stg_price_change.stage_price_change_id;
  --
  -- Base Validations
  --
  merge into xxadeo_rpm_stage_price_change target
  using (select xxadeo_process_id,
                stage_price_change_id,
                case
                  when effective_date < LP_vdate or
                       TO_CHAR(effective_date, 'HH24MISS') != '000000' then
                   'INVALID_EFFECTIVE_DATE'
                  when item is NOT NULL and link_code is NOT NULL then
                   'ITEM_OR_LINK_CODE'
                  when item is NOT NULL and skulist is NOT NULL then
                   'ITEM_OR_ITEMLIST'
                  when skulist is NOT NULL and link_code is NOT NULL then
                   'ITEMLIST_OR_LINK_CODE'
                  when item is NULL and link_code is NULL and skulist is NULL then
                   'NO_ITEM_OR_LINK_CODE_OR_SKULIST'
                  when skulist is NOT NULL and diff_id is NOT NULL then
                   'CANNOT_COMBINE_SKULIST_DIFF_ID'
                  when zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                       NOT EXISTS (select rz.zone_id
                          from rpm_zone rz
                         where rz.zone_id = xrspc.zone_id) then
                   'INVALID_ZONE_ID'
                  when zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                       location is NOT NULL then
                   'LOCATION_OR_ZONE'
                  when zone_node_type != RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                       location is NULL then
                   'INVALID_LOCATION'
                  when zone_node_type != RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                       zone_id is NOT NULL then
                   'LOCATION_OR_ZONE'
                  when change_type NOT IN
                       (RPM_CONSTANTS.RETAIL_NONE,
                        RPM_CONSTANTS.RETAIL_PERCENT_OFF_VALUE,
                        RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE,
                        RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE,
                        RPM_CONSTANTS.RESET_POS,
                        RPM_CONSTANTS.RETAIL_EXCLUDE,
                        RPM_CONSTANTS.RETAIL_AMOUNT_OFF_UOM_VALUE) then
                   'INVALID_CHANGE_TYPE'
                  when change_type = RPM_CONSTANTS.RETAIL_PERCENT_OFF_VALUE and
                       change_percent is NULL then
                   'INVALID_CHANGE_PERCENT'
                  when change_type = RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE and
                       change_amount is NULL then
                   'INVALID_CHANGE_AMOUNT'
                  when change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
                       (change_amount is NULL or change_amount < 0) then
                   'INVALID_CHANGE_AMOUNT_FIXED_PRICE'
                  when change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
                       change_selling_uom is NULL then
                   'CHANGE_SELLING_UOM_REQUIRED_FOR_FIXED_PRICE'
                  when change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
                       price_guide_id is NOT NULL then
                   'PRICE_GUIDE_NOT_ALLOWED_WITH_FIXED_PRICE'
                  when link_code is NOT NULL and
                       change_type != RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then
                   'INVALID_CHANGE_TYPE_FOR_LINK_CODE'
                  when link_code is NOT NULL and change_currency is NULL then
                   'CHANGE_CURRENCY_REQUIRED_FOR_LINK_CODE'
                  when ignore_constraints NOT IN (0, 1) then
                   'INVALID_IGNORE_CONSTRAINTS'
                  when auto_approve_ind is NOT NULL and
                       auto_approve_ind NOT IN (0, 1) then
                   'INVALID_AUTO_APPROVE'
                  when change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE and
                       change_selling_uom is NOT NULL and NOT EXISTS
                   (select 1
                          from uom_class
                         where uom = xrspc.change_selling_uom) then
                   'INVALID_CHANGE_SELLING_UOM'
                  when reason_code is NOT NULL and NOT EXISTS
                   (select 1
                          from rpm_codes
                         where code_type IN (2, 3)
                           and code_id = xrspc.reason_code) then
                   'INVALID_REASON_CODE'
                  when zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                       L_open_zone_use = 0 and NOT EXISTS
                   (select zone_id
                          from rpm_zone rz, rpm_zone_group_type rzgt
                         where rz.zone_id = xrspc.zone_id
                           and rzgt.zone_group_id = rz.zone_group_id
                           and rzgt.type =
                               RPM_CONSTANTS.REGULAR_ZONE_GROUP_TYPE) then
                   'INVALID_ZONE_ID'
                  when zone_node_type =
                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE and
                       L_recognize_wh_as_locations = 0 then
                   'WH_NOT_ALLOWED'
                  when zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE and
                       NOT EXISTS
                   (select 1 from store where store = xrspc.location) then
                   'INVALID_LOCATION'
                  when zone_node_type =
                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE and NOT EXISTS
                   (select 1 from wh where wh = xrspc.location) then
                   'INVALID_LOCATION'
                  when item is NOT NULL and NOT EXISTS
                   (select 1
                          from item_master im
                         where im.item = xrspc.item
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS) then
                   'INVALID_ITEM'
                  when skulist is NOT NULL and NOT EXISTS
                   (select 1
                          from skulist_detail sd
                         where sd.skulist = xrspc.skulist) then
                   'INVALID_SKULIST'
                  when item is NOT NULL and diff_id is NOT NULL and
                       NOT EXISTS
                   (select 1
                          from item_master im
                         where im.item_parent = xrspc.item
                           and im.item_level = im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                           and (im.diff_1 = xrspc.diff_id or
                               im.diff_2 = xrspc.diff_id or
                               im.diff_3 = xrspc.diff_id or
                               im.diff_4 = xrspc.diff_id)) then
                   'INVALID_ITEM_DIFF_ID'
                  when link_code is NOT NULL and NOT EXISTS
                   (select 1
                          from rpm_link_code_attribute rlca
                         where rlca.link_code = xrspc.link_code) then
                   'INVALID_LINK_CODE'
                  when link_code is NOT NULL and NOT EXISTS
                   (select 1
                          from (select store                              loc,
                                       currency_code,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_STORE loc_type
                                  from store
                                union all
                                select wh                                     loc,
                                       currency_code,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE loc_type
                                  from wh
                                union all
                                select zone_id loc,
                                       currency_code,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                  from rpm_zone) locs
                         where xrspc.zone_node_type = locs.loc_type
                           and NVL(xrspc.location, zone_id) = locs.loc
                           and xrspc.change_currency = locs.currency_code) then
                   'CURRENCY_MISMATCH_FOR_LOCATION_LINK_CODE'
                  when link_code is NULL and change_currency is NOT NULL then
                   'CURRENCY_ONLY_ALLOWED_FOR_LINK_CODE'
                  when price_guide_id is NOT NULL and NOT EXISTS
                   (select 1
                          from rpm_price_guide rpg
                         where rpg.price_guide_id = xrspc.price_guide_id) then
                   'INVALID_PRICE_GUIDE'
                  when item is NOT NULL and location is NOT NULL and
                       diff_id is NULL and EXISTS
                   (select 1
                          from rpm_link_code_attribute rlca
                         where rlca.item = xrspc.item
                           and rlca.location = xrspc.location) then
                   'ITEM_LOC_LINK_CODE_EXISTS'
                  when item is NOT NULL and zone_id is NOT NULL and
                       diff_id is NULL and EXISTS
                   (select 1
                          from rpm_link_code_attribute rlcs,
                               rpm_zone_location       rzl
                         where rlcs.item = xrspc.item
                           and rzl.zone_id = xrspc.zone_id
                           and rlcs.location = rzl.location) then
                   'ITEM_LOC_LINK_CODE_EXISTS'
                  when zone_node_type IN
                       (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) and EXISTS
                   (select 1
                          from item_master im
                         where im.item = xrspc.item
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                           and item_level = tran_level) and NOT EXISTS
                   (select 1
                          from item_master im, rpm_item_loc il
                         where im.item = xrspc.item
                           and il.dept = im.dept
                           and il.item = im.item
                           and il.loc = xrspc.location) then
                   'ITEM_LOC_NOT_RANGED'
                  when zone_node_type IN
                       (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) and
                       item is NOT NULL and diff_id is NULL and EXISTS
                   (select 1
                          from item_master im
                         where im.item = xrspc.item
                           and im.item_level = im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS) and
                       NOT EXISTS (select /*+ ORDERED */
                         1
                          from item_master im, rpm_item_loc il
                         where im.item = xrspc.item
                           and il.dept = im.dept
                           and il.item = im.item
                           and il.loc = xrspc.location) then
                   'ITEM_LOC_NOT_RANGED'
                  when L_zone_ranging = 1 and
                       zone_node_type IN
                       (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) and
                       item is NOT NULL and diff_id is NULL and EXISTS
                   (select 1
                          from item_master im
                         where im.item = xrspc.item
                           and im.item_level < im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS) and
                       NOT EXISTS
                   (select 1
                          from item_master im, rpm_item_loc il
                         where im.item_parent = xrspc.item
                           and im.item_level = im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                           and il.dept = im.dept
                           and il.item = im.item
                           and il.loc = xrspc.location) then
                   'ITEM_LOC_NOT_RANGED'
                  when L_zone_ranging = 1 and
                       zone_node_type IN
                       (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) and
                       skulist is NOT NULL and EXISTS
                   (select 1
                          from skulist_detail skd, item_master im
                         where skd.skulist = xrspc.skulist
                           and im.item = skd.item
                           and im.item_level = im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS) and
                       NOT EXISTS (select 1
                          from skulist_detail skd,
                               item_master    im,
                               rpm_item_loc   il
                         where skd.skulist = xrspc.skulist
                           and im.item = skd.item
                           and il.dept = im.dept
                           and il.loc = xrspc.location
                           and il.item = im.item) then
                   'ITEMS_IN_SKULIST_NOT_RANGED_TO_LOC'
                  when L_zone_ranging = 1 and
                       zone_node_type IN
                       (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) and
                       skulist is NOT NULL and EXISTS
                   (select 1
                          from skulist_detail skd, item_master im
                         where skd.skulist = xrspc.skulist
                           and im.item = skd.item
                           and im.item_level < im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS) and
                       NOT EXISTS
                   (select 1
                          from skulist_detail skd,
                               item_master    im,
                               rpm_item_loc   il
                         where skd.skulist = xrspc.skulist
                           and im.item_parent = skd.item
                           and im.item_level = im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                           and il.dept = im.dept
                           and il.item = im.item
                           and il.loc = xrspc.location) then
                   'ITEMS_IN_SKULIST_NOT_RANGED_TO_LOC'
                  when L_zone_ranging = 1 and
                       zone_node_type IN
                       (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) and
                       item is NOT NULL and diff_id is NOT NULL and EXISTS
                   (select 1
                          from item_master im
                         where im.item = xrspc.item
                           and im.item_level < im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS) and
                       NOT EXISTS
                   (select 1
                          from item_master im, rpm_item_loc il
                         where im.item_parent = xrspc.item
                           and im.item_level = im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                           and (im.diff_1 = xrspc.diff_id or
                               im.diff_2 = xrspc.diff_id or
                               im.diff_3 = xrspc.diff_id or
                               im.diff_4 = xrspc.diff_id)
                           and il.dept = im.dept
                           and il.item = im.item
                           and il.loc = xrspc.location) then
                   'ITEM_LOC_NOT_RANGED'
                  when L_zone_ranging = 1 and
                       zone_node_type IN
                       (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) and
                       link_code is NOT NULL and NOT EXISTS
                   (select 1
                          from rpm_link_code_attribute rlca
                         where rlca.link_code = xrspc.link_code
                           and rlca.location = xrspc.location) then
                   'LINK_CODE_NOT_RANGED'
                  when L_zone_ranging = 1 and
                       zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                       item is NOT NULL and diff_id is NULL and EXISTS
                   (select 1
                          from item_master im
                         where im.item = xrspc.item
                           and im.item_level = im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS) and
                       NOT EXISTS (select 1
                          from item_master       im,
                               rpm_zone_location rzl,
                               rpm_item_loc      il
                         where im.item = xrspc.item
                           and rzl.zone_id = xrspc.zone_id
                           and il.loc = rzl.location
                           and il.dept = im.dept
                           and il.item = im.item) then
                   'ITEM_LOC_NOT_RANGED'
                  when L_zone_ranging = 1 and
                       zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                       item is NOT NULL and diff_id is NULL and EXISTS
                   (select 1
                          from item_master im
                         where im.item = xrspc.item
                           and im.item_level < im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS) and
                       NOT EXISTS
                   (select 1
                          from item_master       im,
                               rpm_zone_location rzl,
                               rpm_item_loc      il
                         where im.item_parent = xrspc.item
                           and im.item_level = im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                           and rzl.zone_id = xrspc.zone_id
                           and il.dept = im.dept
                           and il.item = im.item
                           and il.loc = rzl.location) then
                   'ITEM_LOC_NOT_RANGED'
                  when L_zone_ranging = 1 and
                       zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                       skulist is NOT NULL and EXISTS
                   (select 1
                          from skulist_detail skd, item_master im
                         where skd.skulist = xrspc.skulist
                           and im.item = skd.item
                           and im.item_level = im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS) and
                       NOT EXISTS
                   (select 1
                          from skulist_detail    skd,
                               item_master       im,
                               rpm_item_loc      il,
                               rpm_zone_location rzl
                         where skd.skulist = xrspc.skulist
                           and im.item = skd.item
                           and im.item_level = im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                           and il.dept = im.dept
                           and il.item = im.item
                           and rzl.zone_id = xrspc.zone_id
                           and il.loc = rzl.location) then
                   'ITEMS_IN_SKULIST_NOT_RANGED_TO_LOC'
                  when L_zone_ranging = 1 and
                       zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                       skulist is NOT NULL and EXISTS
                   (select 1
                          from skulist_detail skd, item_master im
                         where skd.skulist = xrspc.skulist
                           and im.item = skd.item
                           and im.item_level < im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS) and
                       NOT EXISTS
                   (select 1
                          from skulist_detail    skd,
                               item_master       im,
                               rpm_item_loc      il,
                               rpm_zone_location rzl
                         where skd.skulist = xrspc.skulist
                           and im.item_parent = skd.item
                           and im.item_level = im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                           and il.dept = im.dept
                           and il.item = im.item
                           and rzl.zone_id = xrspc.zone_id
                           and il.loc = rzl.location) then
                   'ITEMS_IN_SKULIST_NOT_RANGED_TO_LOC'
                  when L_zone_ranging = 1 and
                       zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                       item is NOT NULL and diff_id is NOT NULL and
                       NOT EXISTS
                   (select 1
                          from item_master       im,
                               rpm_item_loc      il,
                               rpm_zone_location rzl
                         where im.item_parent = xrspc.item
                           and im.item_level = im.tran_level
                           and im.sellable_ind =
                               RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                           and (im.diff_1 = xrspc.diff_id or
                               im.diff_2 = xrspc.diff_id or
                               im.diff_3 = xrspc.diff_id or
                               im.diff_4 = xrspc.diff_id)
                           and il.dept = im.dept
                           and il.item = im.item
                           and rzl.zone_id = xrspc.zone_id
                           and il.loc = rzl.location) then
                   'ITEM_LOC_NOT_RANGED'
                  when L_zone_ranging = 1 and
                       zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                       link_code is NOT NULL and NOT EXISTS
                   (select 1
                          from rpm_link_code_attribute rlca,
                               rpm_zone_location       rzl
                         where rlca.link_code = xrspc.link_code
                           and rzl.zone_id = xrspc.zone_id
                           and rlca.location = rzl.location) then
                   'LINK_CODE_NOT_RANGED'
                  when price_guide_id is NOT NULL and NOT EXISTS
                   (select 1
                          from rpm_price_guide rpg
                         where rpg.corp_ind = 1
                           and rpg.price_guide_id = xrspc.price_guide_id
                        union all
                        select 1
                          from rpm_price_guide rpg, rpm_price_guide_dept rpgd
                         where rpg.corp_ind = 0
                           and rpg.price_guide_id = xrspc.price_guide_id
                           and rpg.price_guide_id = rpgd.price_guide_id
                           and rpgd.dept =
                               (select dept
                                  from item_master
                                 where item = xrspc.item)) then
                   'INVALID_PRICE_GUIDE_DEPT'
                  when price_guide_id is NOT NULL and
                       ((zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE and
                       NOT EXISTS
                        (select 1
                            from rpm_price_guide rpg, store s
                           where rpg.price_guide_id = xrspc.price_guide_id
                             and s.store = xrspc.location
                             and rpg.currency_code = s.currency_code)) or
                       (zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                       NOT EXISTS
                        (select 1
                            from rpm_price_guide rpg, rpm_zone rz
                           where rpg.price_guide_id = xrspc.price_guide_id
                             and rz.zone_id = xrspc.zone_id
                             and rpg.currency_code = rz.currency_code)) or
                       (zone_node_type =
                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE and
                       NOT EXISTS
                        (select 1
                            from rpm_price_guide rpg, wh
                           where rpg.price_guide_id = xrspc.price_guide_id
                             and wh.wh = xrspc.location
                             and rpg.currency_code = wh.currency_code))) then
                   'INVALID_PRICE_GUIDE_LOC_CURRENCY'
                  when vendor_funded_ind = 1 and
                       (diff_id is NOT NULL or skulist is NOT NULL or EXISTS
                        (select 1
                           from item_master im
                          where im.item = xrspc.item
                            and im.item_level < im.tran_level
                            and im.sellable_ind =
                                RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            and im.status = RPM_CONSTANTS.APPROVED_ITEM_STATUS)) then
                   'ONLY_TRAN_ITEM_IS_ALLOWED_FOR_VENDOR_FUNDED'
                  when vendor_funded_ind = 1 and NOT EXISTS
                   (select 1
                          from rpm_deal_head h, deal_detail d
                         where h.deal_id = xrspc.deal_id
                           and d.deal_id = xrspc.deal_id
                           and d.deal_detail_id = xrspc.deal_detail_id
                           and h.deal_id = d.deal_id
                           and h.status = 'A'
                           and h.billing_type = 'VFM'
                           and h.active_date <= xrspc.effective_date
                           and h.close_date >= xrspc.effective_date) then
                   'INVALID_DEAL'
                  when stage_cust_attr_id is NOT NULL and NOT EXISTS
                   (select 1
                          from rpm_stage_pc_cust_attr rspca
                         where rspca.stage_cust_attr_id =
                               xrspc.stage_cust_attr_id) then
                   'INVALID_CUSTOM_ATTRIBUTE_ID'
                  else
                   NULL
                end error_message
           from xxadeo_rpm_stage_price_change xrspc
          where xrspc.xxadeo_process_id =
                I_xxadeo_rpm_stg_price_change.xxadeo_process_id
            and xrspc.stage_price_change_id =
                I_xxadeo_rpm_stg_price_change.stage_price_change_id
            and error_message is NULL
            and status = 'N') source
  on (target.xxadeo_process_id     = I_xxadeo_rpm_stg_price_change.xxadeo_process_id     and
      target.stage_price_change_id = I_xxadeo_rpm_stg_price_change.stage_price_change_id and
      source.error_message        is NOT NULL                                            and
      target.xxadeo_process_id     = source.xxadeo_process_id                            and
      target.stage_price_change_id = source.stage_price_change_id)
  when MATCHED then
    update
       set error_message           = source.error_message,
           status                  = LP_error_status,
           price_change_id         = NULL,
           price_change_display_id = NULL;

    update xxadeo_rpm_stage_price_change xrspc
       set status                      = LP_error_status,
           error_message               = 'ITEM_LOC_LINK_CODE_EXISTS',
           price_change_id             = NULL,
           price_change_display_id     = NULL
     where xrspc.xxadeo_process_id     = I_xxadeo_rpm_stg_price_change.xxadeo_process_id
       and xrspc.stage_price_change_id = I_xxadeo_rpm_stg_price_change.stage_price_change_id
       and item                        is NOT NULL
       and location                    is NOT NULL
       and zone_node_type              IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       and status                      = LP_worksheet_status
       and EXISTS (select 1
                    from rpm_link_code_attribute rlca,
                         item_master im
                   where im.item_parent = xrspc.item
                     and im.item        = rlca.item
                     and rlca.location  = xrspc.location
                     and (xrspc.diff_id is NULL
                          or (   diff_1 = xrspc.diff_id
                              or diff_2 = xrspc.diff_id
                              or diff_3 = xrspc.diff_id
                              or diff_4 = xrspc.diff_id)));
  --
  update xxadeo_rpm_stage_price_change xrspc
     set status                  = LP_error_status,
         error_message           = 'ITEM_LOC_LINK_CODE_EXISTS',
         price_change_id         = NULL,
         price_change_display_id = NULL
   where xrspc.xxadeo_process_id =
         I_xxadeo_rpm_stg_price_change.xxadeo_process_id
     and xrspc.stage_price_change_id =
         I_xxadeo_rpm_stg_price_change.stage_price_change_id
     and item is NOT NULL
     and zone_id is NOT NULL
     and zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
     and status = 'N'
     and EXISTS(select 1
                  from rpm_link_code_attribute rlca,
                       rpm_zone_location       rzl,
                       item_master             im
                 where im.item_parent = xrspc.item
                   and im.item = rlca.item
                   and rlca.location = rzl.location
                   and rzl.zone_id = xrspc.zone_id
                   and (xrspc.diff_id is NULL or
                       (diff_1 = xrspc.diff_id or diff_2 = xrspc.diff_id or
                        diff_3 = xrspc.diff_id or diff_4 = xrspc.diff_id)));

  update xxadeo_rpm_stage_price_change xrspc
     set error_message           = 'INVALID_MULTI_UNITS',
         status                  = LP_error_status,
         price_change_id         = NULL,
         price_change_display_id = NULL
   where xrspc.xxadeo_process_id     = I_xxadeo_rpm_stg_price_change.xxadeo_process_id
     and xrspc.stage_price_change_id = I_xxadeo_rpm_stg_price_change.stage_price_change_id
     and null_multi_ind              = 1
     and status                      = 'N'
     and (multi_units is NULL or multi_unit_retail is NULL or multi_selling_uom is NULL);

  merge into xxadeo_rpm_stage_price_change target
  using (select distinct
                stage_price_change_id,
                xxadeo_process_id,
                case
                   when NOT EXISTS (-- item/loc level deal
                                    select 1
                                      from deal_itemloc
                                     where deal_id        = rspc.deal_id
                                       and deal_detail_id = rspc.deal_detail_id
                                       and merch_level    = RPM_CONSTANTS.DIL_ITEM_LVL
                                       and org_level      = RPM_CONSTANTS.DIL_LOCATION_LVL
                                       and item           = rspc.item
                                       and location       = rspc.location
                                       and loc_type       = rspc.loc_type
                                    union all
                                    -- item/district level deal
                                    select 1
                                      from deal_itemloc dil,
                                           store s
                                     where dil.deal_id        = rspc.deal_id
                                       and dil.deal_detail_id = rspc.deal_detail_id
                                       and dil.merch_level    = RPM_CONSTANTS.DIL_ITEM_LVL
                                       and dil.org_level      = RPM_CONSTANTS.DIL_DISTRICT_LVL
                                       and dil.item           = rspc.item
                                       and dil.district       = s.district
                                       and s.store            = rspc.location
                                    union all
                                    -- item/region level deal
                                    select 1
                                      from deal_itemloc dil,
                                           district d,
                                           store s
                                     where dil.deal_id        = rspc.deal_id
                                       and dil.deal_detail_id = rspc.deal_detail_id
                                       and dil.merch_level    = RPM_CONSTANTS.DIL_ITEM_LVL
                                       and dil.org_level      = RPM_CONSTANTS.DIL_REGION_LVL
                                       and dil.item           = rspc.item
                                       and dil.region         = d.region
                                       and d.district         = s.district
                                       and s.store            = rspc.location
                                    union all
                                    -- item/area level deal
                                    select 1
                                      from deal_itemloc dil,
                                           region r,
                                           district d,
                                           store s
                                     where dil.deal_id        = rspc.deal_id
                                       and dil.deal_detail_id = rspc.deal_detail_id
                                       and dil.merch_level    = RPM_CONSTANTS.DIL_ITEM_LVL
                                       and dil.org_level      = RPM_CONSTANTS.DIL_AREA_LVL
                                       and dil.item           = rspc.item
                                       and dil.area           = r.area
                                       and r.region           = d.region
                                       and d.district         = s.district
                                       and s.store            = rspc.location
                                    union all
                                    -- parent/parent diff - loc level deal
                                    select 1
                                      from deal_itemloc dil,
                                           item_master im
                                     where dil.deal_id        = rspc.deal_id
                                       and dil.deal_detail_id = rspc.deal_detail_id
                                       and dil.merch_level    IN (RPM_CONSTANTS.DIL_ITEM_PARENT_LVL,
                                                                  RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF1_LVL,
                                                                  RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF2_LVL,
                                                                  RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF3_LVL,
                                                                  RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF4_LVL)
                                       and dil.org_level      = RPM_CONSTANTS.DIL_LOCATION_LVL
                                       and dil.item_parent    = im.item_parent
                                       and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                       and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                       and im.item_level      = im.tran_level
                                       and rspc.item          = im.item
                                       and dil.location       = rspc.location
                                       and dil.loc_type       = rspc.loc_type
                                    union all
                                    -- parent/parent diff - district level deal
                                    select 1
                                      from deal_itemloc dil,
                                           store s,
                                           item_master im
                                     where dil.deal_id        = rspc.deal_id
                                       and dil.deal_detail_id = rspc.deal_detail_id
                                       and dil.merch_level    IN (RPM_CONSTANTS.DIL_ITEM_PARENT_LVL,
                                                                  RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF1_LVL,
                                                                  RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF2_LVL,
                                                                  RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF3_LVL,
                                                                  RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF4_LVL)
                                       and dil.org_level      = RPM_CONSTANTS.DIL_DISTRICT_LVL
                                       and dil.district       = s.district
                                       and s.store            = rspc.location
                                       and dil.item_parent    = im.item_parent
                                       and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                       and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                       and im.item_level      = im.tran_level
                                       and rspc.item          = im.item
                                    union all
                                    -- parent/parent diff - region level deal
                                    select 1
                                      from deal_itemloc dil,
                                           district d,
                                           store s,
                                           item_master im
                                     where dil.deal_id        = rspc.deal_id
                                       and dil.deal_detail_id = rspc.deal_detail_id
                                       and dil.merch_level    IN (RPM_CONSTANTS.DIL_ITEM_PARENT_LVL,
                                                                  RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF1_LVL,
                                                                  RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF2_LVL,
                                                                  RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF3_LVL,
                                                                  RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF4_LVL)
                                       and dil.org_level      = RPM_CONSTANTS.DIL_REGION_LVL
                                       and dil.region         = d.region
                                       and d.district         = s.district
                                       and s.store            = rspc.location
                                       and dil.item_parent    = im.item_parent
                                       and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                       and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                       and im.item_level      = im.tran_level
                                       and rspc.item          = im.item
                                     union all
                                     -- parent/parent diff - area level deal
                                     select 1
                                       from deal_itemloc dil,
                                            region r,
                                            district d,
                                            store s,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    IN (RPM_CONSTANTS.DIL_ITEM_PARENT_LVL,
                                                                   RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF1_LVL,
                                                                   RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF2_LVL,
                                                                   RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF3_LVL,
                                                                   RPM_CONSTANTS.DIL_ITEM_PARENT_DIFF4_LVL)
                                        and dil.org_level      = RPM_CONSTANTS.DIL_AREA_LVL
                                        and dil.area           = r.area
                                        and r.region           = d.region
                                        and d.district         = s.district
                                        and s.store            = rspc.location
                                        and dil.item_parent    = im.item_parent
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and rspc.item          = im.item
                                     union all
                                     -- subclass - loc level deal
                                     select 1
                                       from deal_itemloc dil,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_SUBCLASS_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_LOCATION_LVL
                                        and dil.dept           = im.dept
                                        and dil.class          = im.class
                                        and dil.subclass       = im.subclass
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                        and dil.location       = rspc.location
                                        and dil.loc_type       = rspc.loc_type
                                     union all
                                     -- subclass - district level deal
                                     select 1
                                       from deal_itemloc dil,
                                            store s,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_SUBCLASS_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_DISTRICT_LVL
                                        and dil.district       = s.district
                                        and s.store            = rspc.location
                                        and dil.dept           = im.dept
                                        and dil.class          = im.class
                                        and dil.subclass       = im.subclass
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- subclass - region level deal
                                     select 1
                                       from deal_itemloc dil,
                                            district d,
                                            store s,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_SUBCLASS_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_REGION_LVL
                                        and dil.region         = d.region
                                        and d.district         = s.district
                                        and s.store            = rspc.location
                                        and dil.dept           = im.dept
                                        and dil.class          = im.class
                                        and dil.subclass       = im.subclass
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- subclass - area level deal
                                     select 1
                                       from deal_itemloc dil,
                                            region r,
                                            district d,
                                            store s,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_SUBCLASS_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_AREA_LVL
                                        and dil.area           = r.area
                                        and r.region           = d.region
                                        and d.district         = s.district
                                        and s.store            = rspc.location
                                        and dil.dept           = im.dept
                                        and dil.class          = im.class
                                        and dil.subclass       = im.subclass
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- class/loc level deal
                                     select 1
                                       from deal_itemloc dil,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_CLASS_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_LOCATION_LVL
                                        and dil.loc_type       = rspc.loc_type
                                        and dil.location       = rspc.location
                                        and dil.dept           = im.dept
                                        and dil.class          = im.class
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- class/district level deal
                                     select 1
                                       from deal_itemloc dil,
                                            store s,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_CLASS_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_DISTRICT_LVL
                                        and dil.district       = s.district
                                        and s.store            = rspc.location
                                        and dil.dept           = im.dept
                                        and dil.class          = im.class
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- class/region level deal
                                     select 1
                                       from deal_itemloc dil,
                                            district d,
                                            store s,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_CLASS_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_REGION_LVL
                                        and dil.region         = d.region
                                        and d.district         = s.district
                                        and s.store            = rspc.location
                                        and dil.dept           = im.dept
                                        and dil.class          = im.class
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- class/area level deal
                                     select 1
                                       from deal_itemloc dil,
                                            region r,
                                            district d,
                                            store s,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_CLASS_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_AREA_LVL
                                        and dil.area           = r.area
                                        and r.region           = d.region
                                        and d.district         = s.district
                                        and s.store            = rspc.location
                                        and dil.dept           = im.dept
                                        and dil.class          = im.class
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- dept/loc level deal
                                     select 1
                                       from deal_itemloc dil,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_DEPT_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_LOCATION_LVL
                                        and dil.loc_type       = rspc.loc_type
                                        and dil.location       = rspc.location
                                        and dil.dept           = im.dept
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- dept/district level deal
                                     select 1
                                       from deal_itemloc dil,
                                            store s,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_DEPT_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_DISTRICT_LVL
                                        and dil.district       = s.district
                                        and s.store            = rspc.location
                                        and dil.dept           = im.dept
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- dept/region level deal
                                     select 1
                                       from deal_itemloc dil,
                                            district d,
                                            store s,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_DEPT_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_REGION_LVL
                                        and dil.region         = d.region
                                        and d.district         = s.district
                                        and s.store            = rspc.location
                                        and dil.dept           = im.dept
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- dept/area level deal
                                     select 1
                                       from deal_itemloc dil,
                                            region r,
                                            district d,
                                            store s,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_DEPT_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_AREA_LVL
                                        and dil.area           = r.area
                                        and r.region           = d.region
                                        and d.district         = s.district
                                        and s.store            = rspc.location
                                        and dil.dept           = im.dept
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- group/loc level deal
                                     select 1
                                       from deal_itemloc dil,
                                            deps d,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_GROUP_NO_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_LOCATION_LVL
                                        and dil.loc_type       = rspc.loc_type
                                        and dil.location       = rspc.location
                                        and dil.group_no       = d.group_no
                                        and d.dept             = im.dept
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- group/district level deal
                                     select 1
                                       from deal_itemloc dil,
                                            store s,
                                            deps d,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_GROUP_NO_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_DISTRICT_LVL
                                        and dil.district       = s.district
                                        and s.store            = rspc.location
                                        and dil.group_no       = d.group_no
                                        and d.dept             = im.dept
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- group/region level deal
                                     select 1
                                       from deal_itemloc dil,
                                            district d,
                                            store s,
                                            deps,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_GROUP_NO_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_REGION_LVL
                                        and dil.region         = d.region
                                        and d.district         = s.district
                                        and s.store            = rspc.location
                                        and dil.group_no       = deps.group_no
                                        and deps.dept          = im.dept
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- group/area level deal
                                     select 1
                                       from deal_itemloc dil,
                                            region r,
                                            district d,
                                            store s,
                                            deps,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_GROUP_NO_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_AREA_LVL
                                        and dil.area           = r.area
                                        and r.region           = d.region
                                        and d.district         = s.district
                                        and s.store            = rspc.location
                                        and dil.group_no       = deps.group_no
                                        and deps.dept          = im.dept
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- division/loc level deal
                                     select 1
                                       from deal_itemloc dil,
                                            deps d,
                                            groups g,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_DIVISION_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_LOCATION_LVL
                                        and dil.loc_type       = rspc.loc_type
                                        and dil.location       = rspc.location
                                        and dil.division       = g.division
                                        and g.group_no         = d.group_no
                                        and d.dept             = im.dept
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- division/district level deal
                                     select 1
                                       from deal_itemloc dil,
                                            store s,
                                            deps d,
                                            groups g,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_DIVISION_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_DISTRICT_LVL
                                        and dil.district       = s.district
                                        and s.store            = rspc.location
                                        and dil.division       = g.division
                                        and g.group_no         = d.group_no
                                        and d.dept             = im.dept
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- division/region level deal
                                     select 1
                                       from deal_itemloc dil,
                                            district d,
                                            store s,
                                            deps,
                                            groups g,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_DIVISION_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_REGION_LVL
                                        and dil.region         = d.region
                                        and d.district         = s.district
                                        and s.store            = rspc.location
                                        and dil.division       = g.division
                                        and g.group_no         = deps.group_no
                                        and deps.dept          = im.dept
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item
                                     union all
                                     -- division/area level deal
                                     select 1
                                       from deal_itemloc dil,
                                            region r,
                                            district d,
                                            store s,
                                            deps,
                                            groups g,
                                            item_master im
                                      where dil.deal_id        = rspc.deal_id
                                        and dil.deal_detail_id = rspc.deal_detail_id
                                        and dil.merch_level    = RPM_CONSTANTS.DIL_DIVISION_LVL
                                        and dil.org_level      = RPM_CONSTANTS.DIL_AREA_LVL
                                        and dil.area           = r.area
                                        and r.region           = d.region
                                        and d.district         = s.district
                                        and s.store            = rspc.location
                                        and dil.division       = g.division
                                        and g.group_no         = deps.group_no
                                        and deps.dept          = im.dept
                                        and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                        and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                        and im.item_level      = im.tran_level
                                        and im.item            = rspc.item) then
                       'ITEM_LOC_COMBINATION_NOT_EXISTS_IN_DEAL'
                    else
                      NULL
                end error_message
           from (select stage_price_change_id,
                        xxadeo_process_id,
                        deal_id,
                        deal_detail_id,
                        item,
                        location,
                        DECODE (zone_node_type,
                                RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE) loc_type
                   from xxadeo_rpm_stage_price_change
                  where vendor_funded_ind = 1
                    and zone_node_type    IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                    and xxadeo_process_id     = I_xxadeo_rpm_stg_price_change.xxadeo_process_id
                    and stage_price_change_id = I_xxadeo_rpm_stg_price_change.stage_price_change_id
                    and error_message     is NULL
                    and status            = 'N'
                  union all
                 select xrspc.stage_price_change_id,
                        xrspc.xxadeo_process_id,
                        xrspc.deal_id,
                        xrspc.deal_detail_id,
                        xrspc.item,
                        rzl.location,
                        DECODE(rzl.loc_type,
                               RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE) loc_type
                   from xxadeo_rpm_stage_price_change xrspc,
                        rpm_zone_location rzl
                  where xrspc.vendor_funded_ind     = 1
                    and xrspc.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                    and xrspc.zone_id               = rzl.zone_id
                    and xrspc.xxadeo_process_id     = I_xxadeo_rpm_stg_price_change.xxadeo_process_id
                    and xrspc.stage_price_change_id = I_xxadeo_rpm_stg_price_change.stage_price_change_id
                    and xrspc.error_message        is NULL
                    and xrspc.status                = 'N') rspc) source
  on (    target.xxadeo_process_id     = I_xxadeo_rpm_stg_price_change.xxadeo_process_id
      and target.stage_price_change_id = I_xxadeo_rpm_stg_price_change.stage_price_change_id
      and source.error_message         is NOT NULL
      and target.stage_price_change_id = source.stage_price_change_id
      and target.xxadeo_process_id     = source.xxadeo_process_id)
  when MATCHED then
     update
        set error_message           = source.error_message,
            status                  = LP_error_status,
            price_change_id         = NULL,
            price_change_display_id = NULL;
  --
  open C_get_error_count;
  fetch C_get_error_count into O_error_count;
  close C_get_error_count;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    if C_get_error_count%ISOPEN then
      --
      close C_get_error_count;
      --
    end if;
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
end RPM_BASE_VALIDATIONS;

--------------------------------------------------------------------------------
FUNCTION UPDATE_STG_PRICE_REQUEST(O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE,
                                  I_status                      IN     XXADEO_PRICE_REQUEST.STATUS%TYPE,
                                  I_action                      IN     XXADEO_PRICE_REQUEST.ACTION%TYPE,
                                  I_dependency                  IN     XXADEO_PRICE_REQUEST.DEPENDENCY%TYPE DEFAULT NULL,
                                  I_dependency_type             IN     XXADEO_PRICE_REQUEST.DEPENDENCY_TYPE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.UPDATE_STG_PRICE_REQUEST';
  --
  L_xxadeo_process_order  XXADEO_PRICE_REQUEST.PROCESS_ORDER%TYPE;
  --
BEGIN
  --
  select xxadeo_process_order_seq.nextval
    into L_xxadeo_process_order
    from dual;
  --
  BEGIN
    --
    merge into xxadeo_price_request target
      using (select dt.xxadeo_process_id     request_process_id,
                    dt.price_change_id       price_change_id,
                    I_status                 status,
                    LP_type_pc               type,
                    I_action                 action,
                    0                        request_size,
                    I_dependency             dependency,
                    I_dependency_type        dependency_type,
                    dt.create_id             request_user,
                    dt.create_datetime       request_datetime,
                    NULL                     start_proc_datetime,
                    L_xxadeo_process_order   process_order,
                    LP_price_event_id_seq    pe_process_id
               from xxadeo_rpm_stage_price_change dt
              where dt.xxadeo_process_id     = I_xxadeo_rpm_stg_price_change.xxadeo_process_id
                and dt.stage_price_change_id = I_xxadeo_rpm_stg_price_change.stage_price_change_id) source
      on (target.request_process_id = source.request_process_id and
          target.process_order      = source.process_order      and
          target.pe_process_id      = source.pe_process_id)
    when matched then
      update set target.price_change_id     = source.price_change_id,
                 target.status              = source.status,
                 target.type                = source.type,
                 target.action              = source.action,
                 target.request_size        = source.request_size,
                 target.dependency          = source.dependency,
                 target.dependency_type     = source.dependency_type,
                 target.request_user        = source.request_user,
                 target.request_datetime    = source.request_datetime,
                 target.start_proc_datetime = source.start_proc_datetime
    when not matched then
      insert
        (request_process_id,
         price_change_id,
         status,
         type,
         action,
         request_size,
         dependency,
         dependency_type,
         request_user,
         request_datetime,
         start_proc_datetime,
         process_order,
         pe_process_id)
       values
        (I_xxadeo_rpm_stg_price_change.xxadeo_process_id,
         I_xxadeo_rpm_stg_price_change.price_change_id,
         I_status,
         LP_type_pc,
         I_action,
         0,
         I_dependency,
         I_dependency_type,
         USER,
         SYSDATE,
         NULL,
         xxadeo_process_order_seq.nextval,
         LP_price_event_id_seq);
  --
  END;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
  --
END UPDATE_STG_PRICE_REQUEST;

--------------------------------------------------------------------------------
FUNCTION UPDATE_PRICE_CHANGE_PRCSS_ORD(O_error_message         IN OUT LOGGER_LOGS.TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.UPDATE_STG_PRICE_CHANGE';
  --
  L_xxadeo_process_order  XXADEO_PRICE_REQUEST.PROCESS_ORDER%TYPE;
  L_xxadeo_price_request  XXADEO_PRICE_REQUEST%ROWTYPE;
  --
  cursor C_xxadeo_price_request is
    select *
      from xxadeo_price_request
     where action = XXADEO_RPM_CONSTANTS_SQL.ACTION_D
       and status != XXADEO_RPM_CONSTANTS_SQL.STATUS_A
       and type = XXADEO_PRICE_INJECTOR_SQL.GP_emergency_pc
    union all
    select *
      from xxadeo_price_request
     where action = XXADEO_RPM_CONSTANTS_SQL.ACTION_D
       and status != XXADEO_RPM_CONSTANTS_SQL.STATUS_A
       and type = XXADEO_PRICE_INJECTOR_SQL.GP_emergency_pc
    union all
    select *
      from xxadeo_price_request
     where action = XXADEO_RPM_CONSTANTS_SQL.ACTION_U
       and status = XXADEO_RPM_CONSTANTS_SQL.STATUS_A
       and type = XXADEO_PRICE_INJECTOR_SQL.GP_emergency_pc
    union all
    select *
      from xxadeo_price_request
     where action = XXADEO_RPM_CONSTANTS_SQL.ACTION_U
       and status != XXADEO_RPM_CONSTANTS_SQL.STATUS_A
       and type = XXADEO_PRICE_INJECTOR_SQL.GP_emergency_pc
    union all
    select *
      from xxadeo_price_request
     where action = XXADEO_RPM_CONSTANTS_SQL.ACTION_N
       and status != XXADEO_RPM_CONSTANTS_SQL.STATUS_A
       and type = XXADEO_PRICE_INJECTOR_SQL.GP_emergency_pc
    union all
    select *
      from xxadeo_price_request
     where action = XXADEO_RPM_CONSTANTS_SQL.ACTION_D
       and status != XXADEO_RPM_CONSTANTS_SQL.STATUS_A
       and type != XXADEO_PRICE_INJECTOR_SQL.GP_emergency_pc
    union all
    select *
      from xxadeo_price_request
     where action = XXADEO_RPM_CONSTANTS_SQL.ACTION_D
       and status != XXADEO_RPM_CONSTANTS_SQL.STATUS_A
       and type != XXADEO_PRICE_INJECTOR_SQL.GP_emergency_pc
    union all
    select *
      from xxadeo_price_request
     where action = XXADEO_RPM_CONSTANTS_SQL.ACTION_U
       and status = XXADEO_RPM_CONSTANTS_SQL.STATUS_A
       and type != XXADEO_PRICE_INJECTOR_SQL.GP_emergency_pc
    union all
    select *
      from xxadeo_price_request
     where action = XXADEO_RPM_CONSTANTS_SQL.ACTION_U
       and status != XXADEO_RPM_CONSTANTS_SQL.STATUS_A
       and type != XXADEO_PRICE_INJECTOR_SQL.GP_emergency_pc
    union all
    select *
      from xxadeo_price_request
     where action = XXADEO_RPM_CONSTANTS_SQL.ACTION_N
       and status != XXADEO_RPM_CONSTANTS_SQL.STATUS_A
       and type != XXADEO_PRICE_INJECTOR_SQL.GP_emergency_pc;
  --
BEGIN
  --
  open C_xxadeo_price_request;
  loop
    --
    fetch C_xxadeo_price_request into L_xxadeo_price_request;
    exit when C_xxadeo_price_request%NOTFOUND;
    --
    select xxadeo_process_order_seq.nextval
      into L_xxadeo_process_order
      from dual;
    --
    update xxadeo_price_request
       set process_order      = L_xxadeo_process_order
     where request_process_id = L_xxadeo_price_request.request_process_id
       and process_order      = L_xxadeo_price_request.process_order
       and pe_process_id      = L_xxadeo_price_request.pe_process_id;
    --
  end loop;
  --
  close C_xxadeo_price_request;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    close C_xxadeo_price_request;
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
  --
END UPDATE_PRICE_CHANGE_PRCSS_ORD;

--------------------------------------------------------------------------------
FUNCTION UPDATE_STG_PRICE_CHANGE(O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                                 I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.UPDATE_STG_PRICE_CHANGE';
  --
BEGIN
  --
  update xxadeo_rpm_stage_price_change
     set status            = I_xxadeo_rpm_stg_price_change.status,
         error_message     = I_xxadeo_rpm_stg_price_change.error_message
   where xxadeo_process_id     = I_xxadeo_rpm_stg_price_change.xxadeo_process_id
     and stage_price_change_id = I_xxadeo_rpm_stg_price_change.stage_price_change_id;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
  --
END UPDATE_STG_PRICE_CHANGE;

--------------------------------------------------------------------------------
FUNCTION INSERT_STG_PRICE_CHANGE(O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                                 O_xxadeo_rpm_stg_price_change    OUT XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE,
                                 I_status                      IN     XXADEO_RPM_STAGE_PRICE_CHANGE.STATUS%TYPE,
                                 I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.INSERT_STG_PRICE_CHANGE';
  --
  L_xxadeo_process_id      XXADEO_RPM_STAGE_PRICE_CHANGE.XXADEO_PROCESS_ID%TYPE;
  L_stage_price_change_id  XXADEO_RPM_STAGE_PRICE_CHANGE.STAGE_PRICE_CHANGE_ID%TYPE;
  --
  cursor C_get_xxadeo_process_id is
    select xxadeo_price_process_id_seq.nextval
      from dual;
  --
  cursor C_get_stage_price_change_id is
    select rpm_stage_price_change_seq.nextval
      from dual;
  --
BEGIN
  --
  open C_get_xxadeo_process_id;
  fetch C_get_xxadeo_process_id into L_xxadeo_process_id;
  close C_get_xxadeo_process_id;
  --
  open C_get_stage_price_change_id;
  fetch C_get_stage_price_change_id into L_stage_price_change_id;
  close C_get_stage_price_change_id;
  --
  insert into xxadeo_rpm_stage_price_change(xxadeo_process_id,
                                            stage_price_change_id,
                                            reason_code,
                                            item,
                                            diff_id,
                                            zone_id,
                                            location,
                                            zone_node_type,
                                            link_code,
                                            effective_date,
                                            change_type,
                                            change_amount,
                                            change_currency,
                                            change_percent,
                                            change_selling_uom,
                                            null_multi_ind,
                                            multi_units,
                                            multi_unit_retail,
                                            multi_selling_uom,
                                            price_guide_id,
                                            ignore_constraints,
                                            auto_approve_ind,
                                            status,
                                            error_message,
                                            process_id,
                                            price_change_id,
                                            price_change_display_id,
                                            skulist,
                                            thread_num,
                                            exclusion_created,
                                            vendor_funded_ind,
                                            funding_type,
                                            funding_amount,
                                            funding_amount_currency,
                                            funding_percent,
                                            deal_id,
                                            deal_detail_id,
                                            zone_group_id,
                                            stage_cust_attr_id,
                                            cust_attr_id,
                                            processed_date,
                                            create_id,
                                            create_datetime)
                                   values  (L_xxadeo_process_id,
                                            L_stage_price_change_id,
                                            I_xxadeo_rpm_stg_price_change.reason_code,
                                            I_xxadeo_rpm_stg_price_change.item,
                                            I_xxadeo_rpm_stg_price_change.diff_id,
                                            I_xxadeo_rpm_stg_price_change.zone_id,
                                            I_xxadeo_rpm_stg_price_change.location,
                                            I_xxadeo_rpm_stg_price_change.zone_node_type,
                                            I_xxadeo_rpm_stg_price_change.link_code,
                                            I_xxadeo_rpm_stg_price_change.effective_date,
                                            I_xxadeo_rpm_stg_price_change.change_type,
                                            I_xxadeo_rpm_stg_price_change.change_amount,
                                            I_xxadeo_rpm_stg_price_change.change_currency,
                                            I_xxadeo_rpm_stg_price_change.change_percent,
                                            I_xxadeo_rpm_stg_price_change.change_selling_uom,
                                            nvl(I_xxadeo_rpm_stg_price_change.null_multi_ind,0),
                                            I_xxadeo_rpm_stg_price_change.multi_units,
                                            I_xxadeo_rpm_stg_price_change.multi_unit_retail,
                                            I_xxadeo_rpm_stg_price_change.multi_selling_uom,
                                            I_xxadeo_rpm_stg_price_change.price_guide_id,
                                            I_xxadeo_rpm_stg_price_change.ignore_constraints,
                                            I_xxadeo_rpm_stg_price_change.auto_approve_ind,
                                            I_status,
                                            I_xxadeo_rpm_stg_price_change.error_message,
                                            I_xxadeo_rpm_stg_price_change.process_id,
                                            I_xxadeo_rpm_stg_price_change.price_change_id,
                                            I_xxadeo_rpm_stg_price_change.price_change_display_id,
                                            I_xxadeo_rpm_stg_price_change.skulist,
                                            I_xxadeo_rpm_stg_price_change.thread_num,
                                            I_xxadeo_rpm_stg_price_change.exclusion_created,
                                            I_xxadeo_rpm_stg_price_change.vendor_funded_ind,
                                            I_xxadeo_rpm_stg_price_change.funding_type,
                                            I_xxadeo_rpm_stg_price_change.funding_amount,
                                            I_xxadeo_rpm_stg_price_change.funding_amount_currency,
                                            I_xxadeo_rpm_stg_price_change.funding_percent,
                                            I_xxadeo_rpm_stg_price_change.deal_id,
                                            I_xxadeo_rpm_stg_price_change.deal_detail_id,
                                            I_xxadeo_rpm_stg_price_change.zone_group_id,
                                            I_xxadeo_rpm_stg_price_change.stage_cust_attr_id,
                                            I_xxadeo_rpm_stg_price_change.cust_attr_id,
                                            I_xxadeo_rpm_stg_price_change.processed_date,
                                            I_xxadeo_rpm_stg_price_change.create_id,
                                            I_xxadeo_rpm_stg_price_change.create_datetime);
  --
  select a.*
    into O_xxadeo_rpm_stg_price_change
    from xxadeo_rpm_stage_price_change a
   where a.xxadeo_process_id = L_xxadeo_process_id
     and a.stage_price_change_id = L_stage_price_change_id;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
  --
END INSERT_STG_PRICE_CHANGE;

--------------------------------------------------------------------------------
FUNCTION CONV_RPM_PC_TO_XXADEO_STG_PC(O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                                      I_xxadeo_rpm_stg_price_change    OUT XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE,
                                      I_rpm_price_change            IN     RPM_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.CONV_RPM_PC_TO_XXADEO_STG_PC';
  --
BEGIN
  --
  I_xxadeo_rpm_stg_price_change.xxadeo_process_id        := NULL;
  I_xxadeo_rpm_stg_price_change.stage_price_change_id    := NULL;
  I_xxadeo_rpm_stg_price_change.reason_code              := I_rpm_price_change.reason_code;
  I_xxadeo_rpm_stg_price_change.item                     := I_rpm_price_change.item;
  I_xxadeo_rpm_stg_price_change.diff_id                  := I_rpm_price_change.diff_id;
  I_xxadeo_rpm_stg_price_change.zone_id                  := I_rpm_price_change.zone_id;
  I_xxadeo_rpm_stg_price_change.location                 := I_rpm_price_change.location;
  I_xxadeo_rpm_stg_price_change.zone_node_type           := I_rpm_price_change.zone_node_type;
  I_xxadeo_rpm_stg_price_change.link_code                := I_rpm_price_change.link_code;
  I_xxadeo_rpm_stg_price_change.effective_date           := I_rpm_price_change.effective_date;
  I_xxadeo_rpm_stg_price_change.change_type              := I_rpm_price_change.change_type;
  I_xxadeo_rpm_stg_price_change.change_amount            := I_rpm_price_change.change_amount;
  I_xxadeo_rpm_stg_price_change.change_currency          := I_rpm_price_change.change_currency;
  I_xxadeo_rpm_stg_price_change.change_percent           := I_rpm_price_change.change_percent;
  I_xxadeo_rpm_stg_price_change.change_selling_uom       := I_rpm_price_change.change_selling_uom;
  I_xxadeo_rpm_stg_price_change.null_multi_ind           := I_rpm_price_change.null_multi_ind;
  I_xxadeo_rpm_stg_price_change.multi_units              := I_rpm_price_change.multi_units;
  I_xxadeo_rpm_stg_price_change.multi_unit_retail        := I_rpm_price_change.multi_unit_retail;
  I_xxadeo_rpm_stg_price_change.multi_selling_uom        := I_rpm_price_change.multi_selling_uom;
  I_xxadeo_rpm_stg_price_change.price_guide_id           := I_rpm_price_change.price_guide_id;
  I_xxadeo_rpm_stg_price_change.ignore_constraints       := I_rpm_price_change.ignore_constraints;
  I_xxadeo_rpm_stg_price_change.auto_approve_ind         := 1;
  I_xxadeo_rpm_stg_price_change.status                   := XXADEO_RPM_CONSTANTS_SQL.STATUS_W;
  I_xxadeo_rpm_stg_price_change.error_message            := NULL;
  I_xxadeo_rpm_stg_price_change.process_id               := NULL;
  I_xxadeo_rpm_stg_price_change.price_change_id          := I_rpm_price_change.price_change_id;
  I_xxadeo_rpm_stg_price_change.price_change_display_id  := I_rpm_price_change.price_change_display_id;
  I_xxadeo_rpm_stg_price_change.skulist                  := I_rpm_price_change.skulist;
  I_xxadeo_rpm_stg_price_change.thread_num               := NULL;
  I_xxadeo_rpm_stg_price_change.exclusion_created        := TO_CHAR(I_rpm_price_change.sys_generated_exclusion);
  I_xxadeo_rpm_stg_price_change.vendor_funded_ind        := I_rpm_price_change.vendor_funded_ind;
  I_xxadeo_rpm_stg_price_change.funding_type             := I_rpm_price_change.funding_type;
  I_xxadeo_rpm_stg_price_change.funding_amount           := I_rpm_price_change.funding_amount;
  I_xxadeo_rpm_stg_price_change.funding_amount_currency  := I_rpm_price_change.funding_amount_currency;
  I_xxadeo_rpm_stg_price_change.funding_percent          := I_rpm_price_change.funding_percent;
  I_xxadeo_rpm_stg_price_change.deal_id                  := I_rpm_price_change.deal_id;
  I_xxadeo_rpm_stg_price_change.deal_detail_id           := I_rpm_price_change.deal_detail_id;
  I_xxadeo_rpm_stg_price_change.zone_group_id            := NULL;
  I_xxadeo_rpm_stg_price_change.stage_cust_attr_id       := NULL;
  I_xxadeo_rpm_stg_price_change.cust_attr_id             := I_rpm_price_change.cust_attr_id;
  I_xxadeo_rpm_stg_price_change.processed_date           := NULL;
  I_xxadeo_rpm_stg_price_change.create_id                := I_rpm_price_change.create_id;
  I_xxadeo_rpm_stg_price_change.create_datetime          := I_rpm_price_change.create_date;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
  --
END CONV_RPM_PC_TO_XXADEO_STG_PC;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM (O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                        O_status                         OUT V_ITEM_MASTER.STATUS%TYPE,
                        I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.VALIDATE_ITEM';
  --
  cursor C_chk_item_master is
    select status
      from v_item_master
     where item = I_xxadeo_rpm_stg_price_change.item;
  --
BEGIN
  --
  O_status := NULL;
  --
  open C_chk_item_master;
  fetch C_chk_item_master into O_status;
  close C_chk_item_master;
  --
  if O_status is null then
    --
    RETURN FALSE;
    --
  end if;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END VALIDATE_ITEM;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_ZONE (O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                        I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.VALIDATE_ZONE';
  --
  L_zone          VARCHAR2(1)  := 'N';
  --
  cursor C_chk_zone is
    select 'Y'
      from rpm_zone
     where zone_id = I_xxadeo_rpm_stg_price_change.zone_id
       and ( zone_group_id = I_xxadeo_rpm_stg_price_change.zone_group_id
           or I_xxadeo_rpm_stg_price_change.zone_group_id is null);
  --
BEGIN
  --
  open C_chk_zone;
  fetch C_chk_zone into L_zone;
  close C_chk_zone;
  --
  if L_zone = 'N' then
    --
    RETURN FALSE;
    --
  end if;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END VALIDATE_ZONE;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_ZONE_NODE_TYPE (O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.VALIDATE_ZONE_NODE_TYPE';
  --
BEGIN
  --
  if I_xxadeo_rpm_stg_price_change.zone_node_type not in (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
                                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE,
                                                          ZONE_NODE_TYPE_GROUP_ZONE) then
    --
    RETURN FALSE;
    --
  end if;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END VALIDATE_ZONE_NODE_TYPE;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_RECORD_STATUS (O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                                 I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.VALIDATE_RECORD_STATUS';
  --
BEGIN
  --
  if I_xxadeo_rpm_stg_price_change.status not in (XXADEO_GLOBAL_VARS_SQL.RS_NEW,
                                                  XXADEO_GLOBAL_VARS_SQL.RS_UPDATE,
                                                  XXADEO_GLOBAL_VARS_SQL.RS_DELETE) then
    --
    RETURN FALSE;
    --
  end if;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END VALIDATE_RECORD_STATUS;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_REASON_CODE (O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                               I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.VALIDATE_REASON_CODE';
  --
  L_reason_code   VARCHAR2(1)  := 'N';
  --
  cursor C_chk_reason_code is
    select 'Y'
      from rpm_codes
     where code_id = I_xxadeo_rpm_stg_price_change.reason_code;
  --
BEGIN
  --
  open C_chk_reason_code;
  fetch C_chk_reason_code into L_reason_code;
  close C_chk_reason_code;
  --
  if L_reason_code = 'N' then
    --
    RETURN FALSE;
    --
  end if;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END VALIDATE_REASON_CODE;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION (O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                            I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.VALIDATE_LOCATION';
  --
  L_location      VARCHAR2(1)  := 'N';
  --
  cursor C_chk_location is
    select 'Y'
      from v_store
     where store = I_xxadeo_rpm_stg_price_change.location;
  --
BEGIN
  --
  open C_chk_location;
  fetch C_chk_location into L_location;
  close C_chk_location;
  --
  if L_location = 'N' then
    --
    RETURN FALSE;
    --
  end if;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END VALIDATE_LOCATION;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_RPM_ITEM_LOC (O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                                I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.VALIDATE_RPM_ITEM_LOC';
  --
  L_location      VARCHAR2(1)  := 'N';
  --
  cursor C_chk_location is
    select 'Y'
      from rpm_item_loc
     where loc  = I_xxadeo_rpm_stg_price_change.location
       and item = I_xxadeo_rpm_stg_price_change.item;
  --
BEGIN
  --
  open C_chk_location;
  fetch C_chk_location into L_location;
  close C_chk_location;
  --
  if L_location = 'N' then
    --
    RETURN FALSE;
    --
  end if;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END VALIDATE_RPM_ITEM_LOC;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_EFFECTIVE_DATE (O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.VALIDATE_EFFECTIVE_DATE';
  --
  L_effective_date  VARCHAR2(1):= 'N';
  --
  cursor C_chk_effective_date is
    select 'Y'
      from dual
     where I_xxadeo_rpm_stg_price_change.effective_date >= LP_vdate;
  --
BEGIN
  --
  open C_chk_effective_date;
  fetch C_chk_effective_date into L_effective_date;
  close C_chk_effective_date;
  --
  if L_effective_date = 'N' then
    --
    RETURN FALSE;
    --
  end if;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END VALIDATE_EFFECTIVE_DATE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_RETAIL_PRICE_BLOCKING(O_error_message                       IN OUT LOGGER_LOGS.TEXT%TYPE,
                                        I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
 --
 L_program                   VARCHAR2(60) := 'XXADEO_PRICE_CHANGE_SQL.VALIDATE_RETAIL_PRICE_BLOCKING';
 --
 L_bu                        XXADEO_RPM_BU_PRICE_ZONES.BU_ID%TYPE  := NULL;
 L_cfa_attrib_id             CFA_ATTRIB.ATTRIB_ID%TYPE             := NULL;
 L_xxadeo_cfa_key_val_obj    XXADEO_CFA_KEY_VAL_OBJ                := NULL;
 L_xxadeo_cfa_search_obj     XXADEO_CFA_SEARCH_OBJ                 := NULL;
 L_pc_blocking_cfa_value     VARCHAR2(1)                           := 'Y';
 --
 cursor C_check is
   with zone_loc as (select st.store location,
                            rzl.zone_id,
                            RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type
                       from store st,
                            rpm_zone_location rzl
                      where st.store = rzl.location
                     union all
                     select wh.wh location,
                            rzl.zone_id,
                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE zone_node_type
                       from wh wh,
                            rpm_zone_location rzl
                      where wh.wh = rzl.location
                     union all
                     select null location,
                            rz.zone_id,
                            RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type
                       from rpm_zone rz),
       rpm_reason_code as (select code_id
                             from rpm_codes rc
                            where exists (select 1
                                            from code_detail cd
                                           where code_type    = XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_TYPE
                                             and code        in (XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_EM_ST_PORTAL, XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_ST_PORTAL)
                                             and cd.code_desc = rc.code))
   select xrpzb.bu_id bu,
          I_xxadeo_rpm_stg_price_change.item item
     from zone_loc                  zl,
          xxadeo_rpm_bu_price_zones xrpzb,
          rpm_reason_code           rsc
    where I_xxadeo_rpm_stg_price_change.location    = zl.location
      and zl.zone_id                                = xrpzb.zone_id
      and I_xxadeo_rpm_stg_price_change.reason_code = rsc.code_id;
 --
 cursor C_get_cfa_attrib is
   select value_1 attrib_id
     from table(xxadeo_get_mom_dvm(I_func_area => 'CFA',
                                   I_parameter => XXADEO_RPM_CONSTANTS_SQL.CFA_RETAIL_PRICE_BLOCKING,
                                   I_bu        => L_bu));
 --
 cursor C_get_blocking_cfa_value is
   select xrco.pc_blocking_cfa_value
     from xxadeo_rpm_custom_options xrco
    where rownum = 1;
 --
BEGIN
  --
  for rec IN C_check loop
    --
    L_xxadeo_cfa_key_val_obj := XXADEO_CFA_KEY_VAL_OBJ();
    L_bu                     := rec.bu;
    L_cfa_attrib_id          := NULL;
    --
    open C_get_cfa_attrib;
    fetch C_get_cfa_attrib into L_cfa_attrib_id;
    close C_get_cfa_attrib;
    --
    for pc_blocking_rec in (select *
                              from table(XXADEO_CFA_SQL.SEARCH_CFA_BY_ATTRIB(I_attrib_id          => L_cfa_attrib_id,
                                                                             I_xxadeo_cfa_key_val_tbl => CAST (MULTISET (select new XXADEO_CFA_KEY_VAL_OBJ(key_col_1    => 'ITEM',
                                                                                                                                                           key_value_1  => item)
                                                                                                                           from item_master
                                                                                                                          where item= rec.item)as XXADEO_CFA_KEY_VAL_TBL)))) loop
      --
      L_pc_blocking_cfa_value := null;
      --
      open C_get_blocking_cfa_value;
      fetch C_get_blocking_cfa_value into L_pc_blocking_cfa_value;
      close C_get_blocking_cfa_value;
      --
      if pc_blocking_rec.cfa_value = nvl(L_pc_blocking_cfa_value,'Y') then
        --
        RETURN FALSE;
        --
      end if;
      --
    end loop;

  end loop;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END VALIDATE_RETAIL_PRICE_BLOCKING;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_FUTURE_PRICE(O_error_message                       IN OUT LOGGER_LOGS.TEXT%TYPE,
                               I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
 --
 L_program             VARCHAR2(60) := 'XXADEO_PRICE_CHANGE_SQL.VALIDATE_FUTURE_PRICE';
 --
 L_error     VARCHAR2(1) := 'N';
 L_vdate     DATE        := GET_VDATE;
 L_currency  XXADEO_RPM_STAGE_PRICE_CHANGE.CHANGE_CURRENCY%TYPE := NULL;
 L_zone_id   XXADEO_RPM_STAGE_PRICE_CHANGE.ZONE_ID%TYPE := NULL;

 cursor C_CHECK is
   with rpm_zone_tbl as (select rzl.zone_id,
                                rzl.location
                           from rpm_zone_location rzl),
        rpm_reason_code as (select code_id
                             from rpm_codes rc
                            where exists (select 1
                                            from code_detail cd
                                           where code_type    = XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_TYPE
                                             and code        in (XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_EM_ST_PORTAL, XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_ST_PORTAL)
                                             and cd.code_desc = rc.code))
    select rpc.xxadeo_process_id,
           rpc.stage_price_change_id,
           rzt.zone_id
      from xxadeo_rpm_stage_price_change        rpc,
           rpm_zone_tbl          rzt,
           rpm_reason_code       rsc
     where I_xxadeo_rpm_stg_price_change.effective_date       >= L_vdate
       and I_xxadeo_rpm_stg_price_change.xxadeo_process_id     = rpc.xxadeo_process_id
       and I_xxadeo_rpm_stg_price_change.stage_price_change_id = rpc.stage_price_change_id
       and rpc.location                                        = rzt.location
       and rpc.reason_code                                     = rsc.code_id;
  --
  cursor C_check_pc(V_item                   RPM_PRICE_CHANGE.ITEM%TYPE,
                    V_zone_id                RPM_ZONE_LOCATION.ZONE_ID%TYPE,
                    V_change_currency        RPM_PRICE_CHANGE.CHANGE_CURRENCY%TYPE,
                    V_effective_date         RPM_PRICE_CHANGE.EFFECTIVE_DATE%TYPE,
                    V_xxadeo_process_id      XXADEO_RPM_STAGE_PRICE_CHANGE.XXADEO_PROCESS_ID%TYPE,
                    V_stage_price_change_id  XXADEO_RPM_STAGE_PRICE_CHANGE.STAGE_PRICE_CHANGE_ID%TYPE) is
    with zone_price_change as (select max(effective_date) effective_date
                                 from rpm_price_change rpcx
                                where (rpcx.effective_date between L_vdate and V_effective_date)
                                  and rpcx.zone_id         = V_zone_id
                                  and rpcx.item            = V_item
                                  and rpcx.change_currency = V_change_currency),
         zone_future_price as (select max(rfrx.action_date) action_date
                                 from rpm_future_retail rfrx
                                where rfrx.item           = V_item
                                  and rfrx.location       = V_zone_id
                                  and rfrx.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                  and (rfrx.action_date   between L_vdate and V_effective_date)),
         zone_future_retail as (select max(rzfrx.action_date) action_date
                                  from rpm_zone_future_retail rzfrx
                                 where rzfrx.item         = V_item
                                   and rzfrx.zone         = V_zone_id
                                   and (rzfrx.action_date between L_vdate and V_effective_date))
    select 1
      from xxadeo_rpm_stage_price_change rpc
     where (rpc.change_amount >=  (select rpcs.change_amount
                                     from rpm_price_change  rpcs,
                                          zone_price_change zpc
                                    where rpcs.zone_id          = V_zone_id
                                      and rpcs.item             = V_item
                                      and rpcs.change_currency  = V_change_currency
                                      and rpcs.effective_date   = zpc.effective_date
                                      and rpcs.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                                      and rpcs.effective_date  <= rpc.effective_date
                                      and rownum                = 1)
        or  rpc.change_amount >= (select rizp.selling_retail
                                    from rpm_item_zone_price rizp
                                   where rizp.zone_id                 = V_zone_id
                                     and rizp.item                    = rpc.item
                                     and rizp.selling_retail_currency = rpc.change_currency
                                     and not exists (select 1
                                                       from rpm_price_change  rpcs,
                                                            zone_price_change zpc
                                                      where rpcs.zone_id          = V_zone_id
                                                        and rpcs.item             = V_item
                                                        and rpcs.change_currency  = V_change_currency
                                                        and rpcs.effective_date   = zpc.effective_date
                                                        and rpcs.change_amount   >= rpc.change_amount
                                                        and rpcs.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                                                        and rpcs.effective_date  <= rpc.effective_date)
                                     and rownum = 1)
         or rpc.change_amount >=  (select rfr.selling_retail
                                     from rpm_future_retail rfr,
                                          zone_future_price zfr
                                    where rfr.item              = V_item
                                      and rfr.location          = V_zone_id
                                      and rfr.action_date       = zfr.action_date
                                      and rfr.zone_node_type    = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                      and rfr.action_date      <= rpc.effective_date
                                      and rownum                = 1)
         or rpc.change_amount >= (select rzfr.selling_retail
                                    from rpm_zone_future_retail rzfr,
                                         zone_future_retail     zfr
                                   where rzfr.item         = V_item
                                     and rzfr.zone         = V_zone_id
                                     and rzfr.action_date  = zfr.action_date
                                     and rzfr.action_date <= rpc.effective_date
                                     and rownum            = 1))
        and rpc.xxadeo_process_id     = V_xxadeo_process_id
        and rpc.stage_price_change_id = V_stage_price_change_id;
       /*and exists (select 1
                     from rpm_price_change  rpcs
                    where rzt.zone_id          = rpcs.zone_id
                      and rpcs.item            = rpc.item
                      and rpcs.effective_date >= rpc.effective_date
                      and rpcs.change_amount  <= rpc.change_amount
                      and rpcs.state           = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                   union all
                   select 1
                     from rpm_item_zone_price rizp
                    where rizp.zone_id                 = rzt.zone_id
                      and rizp.item                    = rpc.item
                      and rizp.selling_retail         <= rpc.change_amount
                   union all
                   select 1
                     from rpm_future_retail rfr
                    where rfr.item = rpc.item
                      and rfr.action_date    >= rpc.effective_date
                      and rfr.location        = rzt.zone_id
                      and rfr.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                      and rfr.selling_retail <= rpc.change_amount
                   union all
                   select 1
                     from rpm_zone_future_retail rzfr
                    where rzfr.item = rpc.item
                      and rzfr.action_date    >= rpc.effective_date
                      and rzfr.zone            = rzt.zone_id
                      and rzfr.selling_retail <= rpc.change_amount);*/
  --
  cursor C_get_currency is
    select rz.currency_code
      from rpm_zone rz
     where zone_id = L_zone_id;
  --
BEGIN
  --
  /*open C_CHECK;
  fetch C_CHECK  into L_error;
  close C_CHECK;
  --
  if L_error = 'Y' then
    --

    --
    RETURN FALSE;
    --
  end if;*/
  --
  for rec in C_CHECK loop
    --
    if rec.zone_id is NULL then
      --
      continue;
      --
    else
      --
      L_zone_id := rec.zone_id;
      --
    end if;
    --
    open C_get_currency;
    fetch C_get_currency into L_currency;
    close C_get_currency;
    --
    for error_rec in C_check_pc(V_item                  => i_xxadeo_rpm_stg_price_change.item,
                                V_zone_id               => L_zone_id,
                                V_change_currency       => L_currency,
                                V_effective_date        => i_xxadeo_rpm_stg_price_change.effective_date,
                                V_xxadeo_process_id     => i_xxadeo_rpm_stg_price_change.xxadeo_process_id,
                                V_stage_price_change_id => i_xxadeo_rpm_stg_price_change.stage_price_change_id) loop
      --
      L_error := 'Y';
      --
    end loop;
    --
  end loop;
  --
  if L_error = 'Y' then
    --
    RETURN FALSE;
    --
  end if;
  --
 RETURN TRUE;

EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

    RETURN FALSE;

END VALIDATE_FUTURE_PRICE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PRICE_CHANGE_EVENT (O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                                      I_xxadeo_rpm_stg_price_change IN     XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program       VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.VALIDATE_PRICE_CHANGE_EVENT';
  --
  L_price_change_event  VARCHAR2(1)  := 'N';
  --
  cursor C_chk_price_change_event is
    select 'Y'
      from xxadeo_rpm_stage_price_change a
     where a.item = I_xxadeo_rpm_stg_price_change.item
       and a.location = I_xxadeo_rpm_stg_price_change.location
       and to_char(a.effective_date,'ddmmyyyy') = to_char(I_xxadeo_rpm_stg_price_change.effective_date,'ddmmyyyy')
    union all
    select 'Y'
      from rpm_price_change b
     where b.item = I_xxadeo_rpm_stg_price_change.item
       and b.location = I_xxadeo_rpm_stg_price_change.location
       and to_char(b.effective_date,'ddmmyyyy') = to_char(I_xxadeo_rpm_stg_price_change.effective_date,'ddmmyyyy');
  --
BEGIN
  --
  open C_chk_price_change_event;
  fetch C_chk_price_change_event into L_price_change_event;
  close C_chk_price_change_event;
  --
  if L_price_change_event = 'N' then
    --
    RETURN FALSE;
    --
  end if;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END VALIDATE_PRICE_CHANGE_EVENT;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_XXADEO_STG (O_error_message               IN OUT LOGGER_LOGS.TEXT%TYPE,
                              IO_error                      IN OUT BOOLEAN,
                              I_xxadeo_rpm_stg_price_change IN OUT XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE)
RETURN BOOLEAN IS
  --
  L_program         VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.VALIDATE_XXADEO_STG';
  L_rpm_stg_pc_rec  XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE;
  L_store_rec       XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE;
  L_zone_rec        XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE;
  L_reason_code_id  XXADEO_RPM_STAGE_PRICE_CHANGE.REASON_CODE%TYPE;
  --
  cursor C_exists_xxadeo_rpm_stg (I_xxadeo_rpm_stg_pc  XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE) is
    select a.*
      from xxadeo_rpm_stage_price_change a
     where a.item = I_xxadeo_rpm_stg_pc.item
       and ((a.location = I_xxadeo_rpm_stg_pc.location
             and I_xxadeo_rpm_stg_pc.zone_id is null)
          or(a.zone_id = I_xxadeo_rpm_stg_pc.zone_id
             and I_xxadeo_rpm_stg_pc.location is null))
       and trunc(a.effective_date) = trunc(I_xxadeo_rpm_stg_pc.effective_date)
       and a.status in (XXADEO_RPM_CONSTANTS_SQL.ACTION_N, XXADEO_RPM_CONSTANTS_SQL.ACTION_U, XXADEO_RPM_CONSTANTS_SQL.STATUS_P)
       and ((a.stage_price_change_id != I_xxadeo_rpm_stg_pc.stage_price_change_id
       and   a.xxadeo_process_id     != I_xxadeo_rpm_stg_pc.xxadeo_process_id)
        or  (a.stage_price_change_id != I_xxadeo_rpm_stg_pc.stage_price_change_id
       and   a.xxadeo_process_id     = I_xxadeo_rpm_stg_pc.xxadeo_process_id))
       and not exists (select 1
                         from rpm_price_change rpm
                        where rpm.item = I_xxadeo_rpm_stg_pc.item
                          and trunc(rpm.effective_date) = trunc(I_xxadeo_rpm_stg_pc.effective_date)
                          and ((rpm.location = I_xxadeo_rpm_stg_pc.location
                                and I_xxadeo_rpm_stg_pc.zone_id is null)
                             or(rpm.zone_id = I_xxadeo_rpm_stg_pc.zone_id
                                and I_xxadeo_rpm_stg_pc.location is null))
                          and rpm.state not in (RPM_CONSTANTS.PC_REJECTED_STATE_CODE,
                                                RPM_CONSTANTS.PC_DELETED_STATE_CODE,
                                                RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE))
       and exists (select 1
                     from xxadeo_price_request req
                    where req.request_process_id = a.xxadeo_process_id
                      and req.status in (XXADEO_RPM_CONSTANTS_SQL.STATUS_W,XXADEO_RPM_CONSTANTS_SQL.STATUS_P)
                      and req.action in (XXADEO_RPM_CONSTANTS_SQL.ACTION_N, XXADEO_RPM_CONSTANTS_SQL.ACTION_U));
  --
  cursor C_exists_store_xxadeo_rpm_stg (I_xxadeo_rpm_stg_pc  XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE) is
    select stg.*
      from xxadeo_rpm_stage_price_change stg
     where stg.item            = I_xxadeo_rpm_stg_pc.item
       and stg.effective_date  = I_xxadeo_rpm_stg_pc.effective_date
       and stg.location       in (select location
                                    from rpm_zone_location
                                   where zone_id = I_xxadeo_rpm_stg_pc.zone_id)
       and stg.status         in (XXADEO_RPM_CONSTANTS_SQL.ACTION_N, XXADEO_RPM_CONSTANTS_SQL.ACTION_U, XXADEO_RPM_CONSTANTS_SQL.STATUS_P)
       and exists (select 1
                     from xxadeo_price_request req
                    where req.request_process_id = stg.xxadeo_process_id
                      and req.status in (XXADEO_RPM_CONSTANTS_SQL.STATUS_W,XXADEO_RPM_CONSTANTS_SQL.STATUS_P)
                      and req.action in (XXADEO_RPM_CONSTANTS_SQL.ACTION_N, XXADEO_RPM_CONSTANTS_SQL.ACTION_U));
  --
  cursor C_exists_zone_xxadeo_rpm_stg (I_xxadeo_rpm_stg_pc  XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE) is
    select stg.*
      from xxadeo_rpm_stage_price_change stg
     where stg.item            = I_xxadeo_rpm_stg_pc.item
       and stg.effective_date  = I_xxadeo_rpm_stg_pc.effective_date
       and stg.zone_id        in (select zone_id
                                    from rpm_zone_location
                                   where location = I_xxadeo_rpm_stg_pc.location)
       and stg.status         in (XXADEO_RPM_CONSTANTS_SQL.ACTION_N, XXADEO_RPM_CONSTANTS_SQL.ACTION_U, XXADEO_RPM_CONSTANTS_SQL.STATUS_P)
       and exists (select 1
                     from xxadeo_price_request req
                    where req.request_process_id = stg.xxadeo_process_id
                      and req.status in (XXADEO_RPM_CONSTANTS_SQL.STATUS_W,XXADEO_RPM_CONSTANTS_SQL.STATUS_P)
                      and req.action in (XXADEO_RPM_CONSTANTS_SQL.ACTION_N, XXADEO_RPM_CONSTANTS_SQL.ACTION_U));
  --
BEGIN
  --
  IO_error := FALSE;
  --
  -- Get SIMTAR Reason Code
  open C_get_reason_code(XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_SIMTAR);
  fetch C_get_reason_code into L_reason_code_id;
  close C_get_reason_code;
  --
  -- Verify is exists a PC with the some item/effective_date/location, not exists
  -- in RPM table and ready to be approved
  open C_exists_xxadeo_rpm_stg(I_xxadeo_rpm_stg_price_change);
  fetch C_exists_xxadeo_rpm_stg into L_rpm_stg_pc_rec;
  --
  -- If Yes, Reject (Duplicated)
  if C_exists_xxadeo_rpm_stg%FOUND then
    --
    close C_exists_xxadeo_rpm_stg;
    --
    if L_rpm_stg_pc_rec.reason_code = L_reason_code_id then
      --
      L_rpm_stg_pc_rec.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
      L_rpm_stg_pc_rec.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_DUPLICATED;
      --
      if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                     L_rpm_stg_pc_rec) then
        --
        RETURN FALSE;
        --
      end if;
      --
      -- Insert Store to disapprove
      if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                      L_rpm_stg_pc_rec,
                                      XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                      XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
        --
        RETURN FALSE;
        --
      end if;
      --
    else
      --
      IO_error := TRUE;
      --
      I_xxadeo_rpm_stg_price_change.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
      I_xxadeo_rpm_stg_price_change.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_DUPLICATED;
      --
      if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                     I_xxadeo_rpm_stg_price_change) then
        --
        RETURN FALSE;
        --
      end if;
      --
      -- Insert Store to disapprove
      if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                      I_xxadeo_rpm_stg_price_change,
                                      XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                      XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
        --
        RETURN FALSE;
        --
      end if;
      --
    end if;
    --
  end if;
  --
  close C_exists_xxadeo_rpm_stg;
  --
  If IO_error = FALSE then
    --
    -- If Zone Price Change...
    if I_xxadeo_rpm_stg_price_change.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE then
      --
      -- Verify if exists store to be approved
      open C_exists_store_xxadeo_rpm_stg (I_xxadeo_rpm_stg_price_change);
      --
      loop
        --
        fetch C_exists_store_xxadeo_rpm_stg into L_store_rec;
        exit when C_exists_store_xxadeo_rpm_stg%NOTFOUND;
        --
        if I_xxadeo_rpm_stg_price_change.change_amount <= L_store_rec.change_amount then
          --
          IO_error := TRUE;
          --
          L_store_rec.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
          L_store_rec.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_AMOUNT_HIGH;
          --
          if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                         L_store_rec) then
            --
            RETURN FALSE;
            --
          end if;
          --
          -- Insert Store to disapprove
          if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                          L_store_rec,
                                          XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                          XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
            --
            RETURN FALSE;
            --
          end if;
          --
        end if;
        --
      end loop;
      --
      close C_exists_store_xxadeo_rpm_stg;
      --
    end if;
    --
    -- If Store Price Change...
    if I_xxadeo_rpm_stg_price_change.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE then
      --
      -- Verify if exists zone to be approved
      open C_exists_zone_xxadeo_rpm_stg (I_xxadeo_rpm_stg_price_change);
      --
      loop
        --
        fetch C_exists_zone_xxadeo_rpm_stg into L_zone_rec;
        exit when C_exists_zone_xxadeo_rpm_stg%NOTFOUND;
        --
        if I_xxadeo_rpm_stg_price_change.change_amount >= L_zone_rec.change_amount then
          --
          L_store_rec.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
          L_store_rec.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_AMOUNT_HIGH;
          --
          IO_error := TRUE;
          --
          -- Insert Store to disapprove
          if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                         L_store_rec) then
            --
            RETURN FALSE;
            --
          end if;
          --
          -- Insert Store to disapprove
          if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                          L_store_rec,
                                          XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                          XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
            --
            RETURN FALSE;
            --
          end if;
          --
        end if;
        --
      end loop;
      --
      close C_exists_zone_xxadeo_rpm_stg;
      --
    end if;
    --
  end if;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END VALIDATE_XXADEO_STG;

--------------------------------------------------------------------------------
PROCEDURE PROCESS_PRICE_CHANGE (O_error_message         IN OUT LOGGER_LOGS.TEXT%TYPE,
                                I_process_id            IN     XXADEO_RPM_STAGE_PRICE_CHANGE.XXADEO_PROCESS_ID%TYPE,
                                I_stage_price_change_id IN     XXADEO_RPM_STAGE_PRICE_CHANGE.STAGE_PRICE_CHANGE_ID%TYPE) IS
  --
  L_program                 VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.PROCESS_PRICE_CHANGE';
  --
  L_error                   BOOLEAN                := FALSE;
  L_return                  VARCHAR2(1)            := 'N';
  L_chk_stg_duplicated      VARCHAR2(1)            := 'N';
  L_reason_code_id          RPM_CODES.CODE_ID%TYPE := null;
  L_rpo_reason_code         RPM_CODES.CODE_ID%TYPE := null;
  --
  L_temp_rec                XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE;
  L_temp_rec_aux            XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE;
  L_rec_store               XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE;
  L_rec_new_store           XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE;
  L_rec_zone                XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE;
  L_rpm_price_change        RPM_PRICE_CHANGE%ROWTYPE;
  L_rpm_zone_price_change   RPM_PRICE_CHANGE%ROWTYPE;
  L_rec_rpm_bu_price_zones  XXADEO_RPM_BU_PRICE_ZONES%ROWTYPE;
  L_selling_retail_ref      RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE := 0;
  L_selling_retail          RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE := 0;
  L_future_selling_retail   RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE := 0;
  --
  cursor C_price_change (I_xxadeo_process_id     XXADEO_RPM_STAGE_PRICE_CHANGE.XXADEO_PROCESS_ID%TYPE,
                         I_stage_price_change_id XXADEO_RPM_STAGE_PRICE_CHANGE.STAGE_PRICE_CHANGE_ID%TYPE default NULL,
                         I_price_change_id       XXADEO_RPM_STAGE_PRICE_CHANGE.PRICE_CHANGE_ID%TYPE default NULL) is
    select a.*
      from xxadeo_rpm_stage_price_change a
     where (a.xxadeo_process_id = I_xxadeo_process_id
            or I_xxadeo_process_id is null)
       and (a.stage_price_change_id = I_stage_price_change_id
            or I_stage_price_change_id is null)
       and (a.price_change_id = I_price_change_id
            or I_price_change_id is null);
  --
  cursor C_exists_rpm_base (I_xxadeo_rpm_stage_pc  XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE) is
    select rpm.*
      from rpm_price_change rpm
     where rpm.item = I_xxadeo_rpm_stage_pc.item
       and trunc(rpm.effective_date) >= trunc(I_xxadeo_rpm_stage_pc.effective_date)
       and ((rpm.location = I_xxadeo_rpm_stage_pc.location
             and I_xxadeo_rpm_stage_pc.zone_id is null)
          or(rpm.zone_id = I_xxadeo_rpm_stage_pc.zone_id
             and I_xxadeo_rpm_stage_pc.location is null))
       and rpm.price_change_id != nvl(I_xxadeo_rpm_stage_pc.price_change_id,-999)
       and rpm.state not in (RPM_CONSTANTS.PC_REJECTED_STATE_CODE,
                             RPM_CONSTANTS.PC_DELETED_STATE_CODE,
                             RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE);
  --
  cursor C_chk_rpm_duplicated (I_xxadeo_rpm_stage_pc  XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE) is
    select 'Y'
      from rpm_price_change rpm
     where rpm.item = I_xxadeo_rpm_stage_pc.item
       and trunc(rpm.effective_date) = trunc(I_xxadeo_rpm_stage_pc.effective_date)
       and ((rpm.location = I_xxadeo_rpm_stage_pc.location
             and I_xxadeo_rpm_stage_pc.zone_id is null)
          or(rpm.zone_id = I_xxadeo_rpm_stage_pc.zone_id
             and I_xxadeo_rpm_stage_pc.location is null))
       and rpm.price_change_id != nvl(I_xxadeo_rpm_stage_pc.price_change_id,-999)
       and rpm.state in (RPM_CONSTANTS.PC_APPROVED_STATE_CODE);
  --
  cursor C_chk_stg_duplicated (I_xxadeo_rpm_stage_pc  XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE) is
    select 'Y'
      from xxadeo_rpm_stage_price_change xrpm,
           xxadeo_price_request          xpr
     where xrpm.xxadeo_process_id = xpr.request_process_id
       and xrpm.item = I_xxadeo_rpm_stage_pc.item
       and trunc(xrpm.effective_date) = trunc(I_xxadeo_rpm_stage_pc.effective_date)
       and (xrpm.zone_id = I_xxadeo_rpm_stage_pc.zone_id
       and I_xxadeo_rpm_stage_pc.location is null)
       and xrpm.error_message is null
       and xrpm.status not in (XXADEO_RPM_CONSTANTS_SQL.STATUS_R,XXADEO_RPM_CONSTANTS_SQL.STATUS_E)
       and xpr.status in (XXADEO_RPM_CONSTANTS_SQL.STATUS_W)
       and xrpm.reason_code = L_rpo_reason_code;
  --
  cursor C_exists_store_rpm_base (I_xxadeo_rpm_stage_pc  XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE) is
    select rpm.*
      from rpm_price_change rpm
     where rpm.item            = I_xxadeo_rpm_stage_pc.item
       and rpm.effective_date >= I_xxadeo_rpm_stage_pc.effective_date
       and rpm.location       in (select location
                                    from rpm_zone_location
                                   where zone_id = I_xxadeo_rpm_stage_pc.zone_id)
       and rpm.state      not in (RPM_CONSTANTS.PC_REJECTED_STATE_CODE,
                                  RPM_CONSTANTS.PC_DELETED_STATE_CODE,
                                  RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE);
  --
  cursor C_exists_zone_rpm_base (I_xxadeo_rpm_stage_pc  XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE) is
    select price_change_id,
           price_change_display_id,
           state,
           reason_code,
           exception_parent_id,
           item,
           diff_id,
           zone_id,
           location,
           zone_node_type,
           link_code,
           effective_date,
           change_type,
           change_amount,
           change_currency,
           change_percent,
           change_selling_uom,
           null_multi_ind,
           multi_units,
           multi_unit_retail,
           multi_unit_retail_currency,
           multi_selling_uom,
           price_guide_id,
           vendor_funded_ind,
           funding_type,
           funding_amount,
           funding_amount_currency,
           funding_percent,
           deal_id,
           deal_detail_id,
           create_date,
           create_id,
           approval_date,
           approval_id,
           area_diff_parent_id,
           ignore_constraints,
           lock_version,
           skulist,
           sys_generated_exclusion,
           price_event_itemlist,
           cust_attr_id
      from (select rpm.*,
                   row_number() over (partition by rpm.item, rpm.zone_id order by rpm.effective_date desc) rnk
              from rpm_price_change rpm
             where rpm.item            = I_xxadeo_rpm_stage_pc.item
               and rpm.effective_date <= I_xxadeo_rpm_stage_pc.effective_date
               and rpm.zone_id        in (select zone_id
                                            from rpm_zone_location
                                           where location = I_xxadeo_rpm_stage_pc.location)
               and rpm.state      in (RPM_CONSTANTS.PC_APPROVED_STATE_CODE))tbl
     where rnk = 1;
  --
  cursor C_rpm_bu_price_zones (I_zone_id  XXADEO_RPM_BU_PRICE_ZONES.ZONE_ID%TYPE) is
    select bux.*
      from xxadeo_rpm_bu_price_zones bux
     where bux.zone_id != I_zone_id
       and bux.bu_id = (select buz.bu_id
                          from xxadeo_rpm_bu_price_zones buz
                         where buz.bu_zone_type = LP_BU_ZONE_TYPE
                           and buz.zone_id = I_zone_id);
  --
  cursor C_get_selling_retail (I_zone_id  RPM_ITEM_ZONE_PRICE.ZONE_ID%TYPE,
                               I_item     RPM_ITEM_ZONE_PRICE.ITEM%TYPE) is
    select selling_retail
      from RPM_ITEM_ZONE_PRICE
     where zone_id = I_zone_id
       and item = I_item;
  --
  cursor C_future_selling_retail (I_zone_id     RPM_ITEM_ZONE_PRICE.ZONE_ID%TYPE,
                                  I_item        RPM_ITEM_ZONE_PRICE.ITEM%TYPE,
                                  I_action_date RPM_ZONE_FUTURE_RETAIL.ACTION_DATE%TYPE) is
   select selling_retail
     from(select rfr.*,
                 row_number() over (partition by item, zone order by action_date desc) rnk
            from rpm_zone_future_retail rfr
           where rfr.zone              = I_zone_id
             and rfr.item              = I_item
             and action_date          <= I_action_date)
    where rnk = 1;
  --
BEGIN
  --
  LP_vdate        := get_vdate;
  --
  open C_price_change (I_xxadeo_process_id     => I_process_id,
                       I_stage_price_change_id => I_stage_price_change_id);
  fetch C_price_change into L_temp_rec;
  --
  if C_price_change%NOTFOUND
     or not VALIDATE_RECORD_STATUS(O_error_message,
                                   L_temp_rec) then
    --
    close C_price_change;
    RETURN;
    --
  end if;
  --
  close C_price_change;
  --
  -- Get RPO Reason Code
  --
  open C_get_reason_code(XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_RPO);
  fetch C_get_reason_code into L_rpo_reason_code;
  close C_get_reason_code;
  --
  -- Get SIMTAR Reason Code
  --
  open C_get_reason_code(XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_SIMTAR);
  fetch C_get_reason_code into L_reason_code_id;
  close C_get_reason_code;
  --
  -- # Emergency Price Change - EMERGENCY #
  if L_temp_rec.effective_date <= LP_vdate and L_temp_rec.reason_code <> L_reason_code_id then
    --
    LP_type_pc := GP_emergency_type_pc;
    --
    -- If is Duplicate, ERROR
    open C_chk_rpm_duplicated(L_temp_rec);
    --
    fetch C_chk_rpm_duplicated into L_return;
    --
    if C_chk_rpm_duplicated%FOUND then
      --
      close C_chk_rpm_duplicated;
      --
      L_temp_rec.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
      L_temp_rec.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_DUPLICATED;
      --
      if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                     L_temp_rec) then
        --
        RETURN;
        --
      end if;
      --
      RAISE SAVE_N_RETURN;
      --
    else
      --
      close C_chk_rpm_duplicated;
      --
      if L_temp_rec.reason_code <> L_reason_code_id then
        --
        if VALIDATE_FUTURE_PRICE(O_error_message               => O_error_message,
                                 I_xxadeo_rpm_stg_price_change => L_temp_rec) = FALSE then
          --
          L_temp_rec.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
          L_temp_rec.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_AMOUNT_HIGH;
          --
          if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                         L_temp_rec) then
            --
            RETURN;
            --
          end if;
          --
          -- Insert into Request
          if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                          L_temp_rec,
                                          XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                          XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
            --
            RETURN;
            --
          end if;
          --
          RAISE SAVE_N_RETURN;
          --
        end if;
        --
        open C_exists_zone_rpm_base(L_temp_rec);
        fetch C_exists_zone_rpm_base into L_rpm_price_change;
        --
        if C_exists_zone_rpm_base%FOUND then
          --
          close C_exists_zone_rpm_base;
          --
          if L_temp_rec.change_amount >= L_rpm_price_change.change_amount then
            --
            L_temp_rec.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
            L_temp_rec.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_AMOUNT_HIGH;
            --
            if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                           L_temp_rec) then
              --
              RETURN;
              --
            end if;
            --
            -- Insert into Request
            if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                            L_temp_rec,
                                            XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                            XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
              --
              RETURN;
              --
            end if;
            --
            RAISE SAVE_N_RETURN;
            --
          else
            --
            -- Insert into Request with dependency
            if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                            L_temp_rec,
                                            XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                            L_temp_rec.status,
                                            L_rpm_price_change.price_change_id,
                                            DEP_TYPE_SOURCE_RPM) then
              --
              RETURN;
              --
            end if;
            --
          end if;
          --
        else
          --
          -- Insert into Request
          if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                          L_temp_rec,
                                          XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                          L_temp_rec.status) then
            --
            RETURN;
            --
          end if;
          --
        end if;
        --
      end if;
    --
    end if;
  --
  end if;
  --
  -- Get SIMTAR Reason Code
  open C_get_reason_code(XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_SIMTAR);
  fetch C_get_reason_code into L_reason_code_id;
  close C_get_reason_code;
  --
  -- # Regular Price Change - SIMTAR #
  --
  LP_type_pc := GP_regular_type_pc;
  --
  if  L_temp_rec.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE then
    --
    -- If Yes.. SIMTAR
    if L_reason_code_id = L_temp_rec.reason_code then
      --
      open C_get_selling_retail(L_temp_rec.zone_id,
                                L_temp_rec.item);
      fetch C_get_selling_retail into L_selling_retail_ref;
      close C_get_selling_retail;
      --
      -- Approve the SIMTAR Price Change
      L_temp_rec.status    := XXADEO_RPM_CONSTANTS_SQL.STATUS_P;
      --
      -- Change to Processed SIMTAR Price Change
      if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                     L_temp_rec) then
        --
        RETURN;
        --
      end if;
      --
      if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                      L_temp_rec,
                                      XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                      XXADEO_RPM_CONSTANTS_SQL.ACTION_N) then
        --
        RETURN;
        --
      end if;
      --
      -- Get the Zones with the some BU, to create the new Zones PC
      open C_rpm_bu_price_zones (L_temp_rec.zone_id);
      --
      loop
        --
        L_rec_rpm_bu_price_zones := NULL;
        L_return                 := null;
        --
        fetch C_rpm_bu_price_zones into L_rec_rpm_bu_price_zones;
        exit when C_rpm_bu_price_zones%NOTFOUND;
        --
        L_selling_retail := 0;
        --
        open C_get_selling_retail(L_rec_rpm_bu_price_zones.zone_id,
                                  L_temp_rec.item);
        fetch C_get_selling_retail into L_selling_retail;
        close C_get_selling_retail;
        --
        if L_selling_retail_ref = L_selling_retail or L_selling_retail = 0 then
          --
          L_selling_retail_ref := 0;
          L_future_selling_retail := 0;
          --
          open C_future_selling_retail(L_temp_rec.zone_id,
                                       L_temp_rec.item,
                                       L_temp_rec.effective_date);
          fetch C_future_selling_retail into L_selling_retail_ref;
          close C_future_selling_retail;
          --
          open C_future_selling_retail(L_rec_rpm_bu_price_zones.zone_id,
                                       L_temp_rec.item,
                                       L_temp_rec.effective_date);
          fetch C_future_selling_retail into L_future_selling_retail;
          close C_future_selling_retail;
          --
          if L_selling_retail_ref = L_future_selling_retail or L_future_selling_retail = 0 then
            --
            -- If is Duplicate, Rejected
            --
            L_temp_rec_aux         := L_temp_rec;
            L_temp_rec_aux.zone_id := L_rec_rpm_bu_price_zones.zone_id;
            --
            open C_chk_rpm_duplicated(L_temp_rec_aux);
            fetch C_chk_rpm_duplicated into L_return;
            --
            -- If Found duplicate, dont do nothing, because RPO have priority under SIMTAR
            if C_chk_rpm_duplicated%NOTFOUND then
              --
              close C_chk_rpm_duplicated;
              --
              L_chk_stg_duplicated := 'N';
              --
              open C_chk_stg_duplicated(L_temp_rec_aux);
              fetch C_chk_stg_duplicated into L_chk_stg_duplicated;
              close C_chk_stg_duplicated;
              --
              if L_chk_stg_duplicated = 'N' then
                --
                --
                L_temp_rec_aux         := L_temp_rec;
                L_temp_rec_aux.zone_id := L_rec_rpm_bu_price_zones.zone_id;
                --
                -- # Create a new Zone PC #
                if not INSERT_STG_PRICE_CHANGE(O_error_message,
                                               L_rec_zone,
                                               XXADEO_RPM_CONSTANTS_SQL.STATUS_P,
                                               L_temp_rec_aux) then
                  --
                  RETURN;
                  --
                end if;
                --
                if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                                L_rec_zone,
                                                XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                                XXADEO_RPM_CONSTANTS_SQL.ACTION_N) then
                  --
                  RETURN;
                  --
                end if;
                --
                -- Exists Store PC? Store Date >= Zone Date ?
                open C_exists_store_rpm_base(L_rec_zone);
                --
                loop
                  --
                  L_rpm_price_change := NULL;
                  --
                  -- If Yes...(Store Date >= Zone Date)
                  fetch C_exists_store_rpm_base into L_rpm_price_change;
                  exit when C_exists_store_rpm_base%NOTFOUND;
                  --
                  if CONV_RPM_PC_TO_XXADEO_STG_PC(O_error_message,
                                                  L_rec_store,
                                                  L_rpm_price_change) = FALSE then
                    --
                    close C_exists_store_rpm_base;
                    RETURN;
                    --
                  end if;
                  --
                  -- Store Price >= Zone Price ?
                  if L_rec_store.change_amount >= L_rec_zone.change_amount then
                    --
                    L_rec_store.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_AMOUNT_HIGH;
                    --
                    L_rec_new_store       := NULL;
                    --
                    -- Update Store to Rejected
                    if not INSERT_STG_PRICE_CHANGE(O_error_message,
                                                   L_rec_new_store,
                                                   XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                                   L_rec_store) then
                      --
                      close C_exists_store_rpm_base;
                      RETURN;
                      --
                    end if;
                    --
                    -- Update Store to Rejected
                    if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                                    L_rec_new_store,
                                                    XXADEO_RPM_CONSTANTS_SQL.STATUS_A,
                                                    XXADEO_RPM_CONSTANTS_SQL.ACTION_U) then
                      --
                      close C_exists_store_rpm_base;
                      RETURN;
                      --
                    end if;
                    --
                  -- If No...(Store Price < Zone Price)
                  else
                    --
                    if L_rec_store.effective_date = L_rec_zone.effective_date then
                      --
                      -- # Create a new PC to disapprove a exists PC #
                      -- Insert Store to disapprove
                      if not INSERT_STG_PRICE_CHANGE(O_error_message,
                                                     L_rec_new_store,
                                                     XXADEO_RPM_CONSTANTS_SQL.STATUS_P,
                                                     L_rec_store) then
                        --
                        close C_exists_store_rpm_base;
                        RETURN;
                        --
                      end if;
                      --
                      L_rec_new_store       := NULL;
                      --
                      -- Insert Store to disapprove
                      if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                                      L_rec_new_store,
                                                      XXADEO_RPM_CONSTANTS_SQL.STATUS_A,
                                                      XXADEO_RPM_CONSTANTS_SQL.ACTION_U) then
                        --
                        close C_exists_store_rpm_base;
                        RETURN;
                        --
                      end if;
                      --
                      -- # Create a new Store PC for approve with dependency #
                      --
                      L_rec_new_store       := NULL;
                      L_rec_store.price_change_id         := NULL;
                      L_rec_store.price_change_display_id := NULL;
                      --
                      -- Insert Store to approve
                      if not INSERT_STG_PRICE_CHANGE(O_error_message,
                                                     L_rec_new_store,
                                                     XXADEO_RPM_CONSTANTS_SQL.STATUS_P,
                                                     L_rec_store) then
                        --
                        close C_exists_store_rpm_base;
                        RETURN;
                        --
                      end if;
                      --
                      -- Insert Store to approve
                      if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                                      L_rec_new_store,
                                                      XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                                      XXADEO_RPM_CONSTANTS_SQL.ACTION_N,
                                                      L_rec_zone.price_change_id,
                                                      DEP_TYPE_SOURCE_RPM) then
                        --
                        close C_exists_store_rpm_base;
                        RETURN;
                        --
                      end if;
                      --
                    end if;
                    --
                  end if;
                  --
                end loop;
                --
                close C_exists_store_rpm_base;
                --
              end if;
              --
            else
              --
              close C_chk_rpm_duplicated;
              --
            end if;
            --
          end if;
          --
        end if;
        --
      end loop;
      --
      close C_rpm_bu_price_zones;
      --
    end if;
    --
  end if;
  --
  --
  -- # Regular Price Change #
  if L_temp_rec.effective_date > LP_vdate then
    --
    LP_type_pc := GP_regular_type_pc;
    --
    -- # ZONE Price Change - RPO #
    if  L_temp_rec.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and L_temp_rec.reason_code != L_reason_code_id then
      --
      -- # DELETE Price change #
      if L_temp_rec.status = XXADEO_RPM_CONSTANTS_SQL.ACTION_D then
        --
        open C_exists_rpm_base(L_temp_rec);
        fetch C_exists_rpm_base into L_rpm_price_change;
        --
        if C_exists_rpm_base%NOTFOUND then
          --
          close C_exists_rpm_base;
          --
          L_temp_rec_aux               := L_temp_rec;
          L_temp_rec_aux.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_E;
          L_temp_rec_aux.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_NOT_FOUND;
          --
          -- Update to Error
          if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                         L_temp_rec_aux) then
            --
            RETURN;
            --
          end if;
          --
          if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                          L_temp_rec,
                                          XXADEO_RPM_CONSTANTS_SQL.STATUS_E,
                                          XXADEO_RPM_CONSTANTS_SQL.ACTION_D) then
            --
            RETURN;
            --
          end if;
          --
        else
          --
          close C_exists_rpm_base;
          --
          -- Update to Worksheet (disapprove)
          if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                          L_temp_rec,
                                          XXADEO_RPM_CONSTANTS_SQL.STATUS_A,
                                          XXADEO_RPM_CONSTANTS_SQL.ACTION_D) then
            --
            RETURN;
            --
          end if;
          --
          L_temp_rec.status      := XXADEO_RPM_CONSTANTS_SQL.STATUS_P;
          --
          -- Update to Processed
          if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                         L_temp_rec) then
            --
            RETURN;
            --
          end if;
          --
        end if;
        --
      end if;
      --
      -- # UPDATE Price Change #
      if L_temp_rec.status = XXADEO_RPM_CONSTANTS_SQL.ACTION_U then
        --
        -- Exists in RPM Base table ?
        open C_exists_rpm_base(L_temp_rec);
        fetch C_exists_rpm_base into L_rpm_price_change;
        --
        -- If No (not exists in RPM), Update to NEW PC
        if C_exists_rpm_base%NOTFOUND then
          --
          close C_exists_rpm_base;
          --
          L_temp_rec.status        := XXADEO_RPM_CONSTANTS_SQL.ACTION_N;
          --
          -- Update Zone to NEW
          if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                         L_temp_rec) then
            --
            RETURN;
            --
          end if;
          --
        else
          --
          close C_exists_rpm_base;
          --
        end if;
        --
        -- Approve Zone
        if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                        L_temp_rec,
                                        XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                        L_temp_rec.status) then
          --
          RETURN;
          --
        end if;
        --
        L_temp_rec.status      := XXADEO_RPM_CONSTANTS_SQL.STATUS_P;
        --
        -- Update to Processed
        if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                       L_temp_rec) then
          --
          RETURN;
          --
        end if;
        --
        -- If Yes...
        -- Exists Store PC? Store Date >= Zone Date ?
        open C_exists_store_rpm_base(L_temp_rec);
        --
        loop
          --
          L_rpm_price_change := NULL;
          --
          -- If Yes...(Store Date >= Zone Date)
          fetch C_exists_store_rpm_base into L_rpm_price_change;
          exit when C_exists_store_rpm_base%NOTFOUND;
          --
          if CONV_RPM_PC_TO_XXADEO_STG_PC(O_error_message,
                                          L_rec_store,
                                          L_rpm_price_change) = FALSE then
            --
            close C_exists_store_rpm_base;
            RETURN;
            --
          end if;
          --
          -- Store Price >= Zone Price ?
          if L_rec_store.change_amount >= L_temp_rec.change_amount then
            --
            L_rec_store.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_AMOUNT_HIGH;
            --
            L_rec_new_store       := NULL;
            --
            -- Update Store to Rejected
            if not INSERT_STG_PRICE_CHANGE(O_error_message,
                                           L_rec_new_store,
                                           XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                           L_rec_store) then
              --
              close C_exists_store_rpm_base;
              RETURN;
              --
            end if;
            --
            -- Update Store to Rejected
            if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                            L_rec_new_store,
                                            XXADEO_RPM_CONSTANTS_SQL.STATUS_A,
                                            XXADEO_RPM_CONSTANTS_SQL.ACTION_U) then
              --
              close C_exists_store_rpm_base;
              RETURN;
              --
            end if;
            --
          -- If No...(Store Price < Zone Price)
          else
            --
            if L_rec_store.effective_date = L_temp_rec.effective_date then
              --
              -- # Create a new PC to disapprove a exists PC #
              -- Insert Store to disapprove
              if not INSERT_STG_PRICE_CHANGE(O_error_message,
                                             L_rec_new_store,
                                             XXADEO_RPM_CONSTANTS_SQL.STATUS_P,
                                             L_rec_store) then
                --
                close C_exists_store_rpm_base;
                RETURN;
                --
              end if;
              --
              L_rec_new_store       := NULL;
              --
              -- Insert Store to disapprove
              if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                              L_rec_new_store,
                                              XXADEO_RPM_CONSTANTS_SQL.STATUS_A,
                                              XXADEO_RPM_CONSTANTS_SQL.ACTION_U) then
                --
                close C_exists_store_rpm_base;
                RETURN;
                --
              end if;
              --
              -- # Create a new Store PC for approve with dependency #
              --
              L_rec_store.price_change_id         := NULL;
              L_rec_store.price_change_display_id := NULL;
              --
              -- Insert Store to approve
              if not INSERT_STG_PRICE_CHANGE(O_error_message,
                                             L_rec_new_store,
                                             XXADEO_RPM_CONSTANTS_SQL.STATUS_P,
                                             L_rec_store) then
                --
                close C_exists_store_rpm_base;
                RETURN;
                --
              end if;
              --
              -- Insert Store to approve
              if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                              L_rec_new_store,
                                              XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                              XXADEO_RPM_CONSTANTS_SQL.ACTION_N,
                                              L_temp_rec.price_change_id,
                                              DEP_TYPE_SOURCE_RPM) then
                --
                close C_exists_store_rpm_base;
                RETURN;
                --
              end if;
              --
            end if;
            --
          end if;
          --
        end loop;
        --
        close C_exists_store_rpm_base;
        --
      end if;
      --
      -- # NEW Price Change #
      if L_temp_rec.status = XXADEO_RPM_CONSTANTS_SQL.ACTION_N then
        --
        -- Exists in RPM Base table ?
        open C_exists_rpm_base(L_temp_rec);
        fetch C_exists_rpm_base into L_rpm_price_change;
        --
        -- If Yes (exists in RPM)
        if C_exists_rpm_base%FOUND then
          --
          close C_exists_rpm_base;
          -- If is Duplicate, ERROR
          open C_chk_rpm_duplicated(L_temp_rec);
          --
          fetch C_chk_rpm_duplicated into L_return;
          --
          if C_chk_rpm_duplicated%FOUND then
            --
            close C_chk_rpm_duplicated;
            --
            L_temp_rec_aux               := L_temp_rec;
            L_temp_rec_aux.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_E;
            L_temp_rec_aux.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_DUPLICATED;
            --
            -- UPDATE XXADEO_STG_PRICE_CHANGE
            if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                           L_temp_rec_aux) then
              --
              RETURN;
              --
            end if;
            --
            close C_chk_rpm_duplicated;
            RAISE SAVE_N_RETURN;
            --
          end if;
          --
          close C_chk_rpm_duplicated;
          --
        else
          --
          close C_exists_rpm_base;
          --
        end if;
        -- INSERT INTO STG XXADEO_PRICE_REQUEST
        if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                        L_temp_rec,
                                        XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                        XXADEO_RPM_CONSTANTS_SQL.ACTION_N) then
          --
          RETURN;
          --
        end if;
        --
        L_temp_rec.status      := XXADEO_RPM_CONSTANTS_SQL.STATUS_P;
        --
        -- Update to Processed
        if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                       L_temp_rec) then
          --
          RETURN;
          --
        end if;
        --
        -- If not...
        -- Exists Store PC? Store Date >= Zone Date ?
        open C_exists_store_rpm_base(L_temp_rec);
        --
        loop
          --
          L_temp_rec_aux := L_temp_rec;
          L_rpm_price_change := NULL;
          --
          -- If Yes...(Store Date >= Zone Date)
          fetch C_exists_store_rpm_base into L_rpm_price_change;
          exit when C_exists_store_rpm_base%NOTFOUND;
          --
          if CONV_RPM_PC_TO_XXADEO_STG_PC(O_error_message,
                                          L_rec_store,
                                          L_rpm_price_change) = FALSE then
            --
            close C_exists_store_rpm_base;
            RETURN;
            --
          end if;
          --
          -- Store Price >= Zone Price ?
          if L_rec_store.change_amount >= L_temp_rec.change_amount then
            --
            L_rec_store.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_AMOUNT_HIGH;
            --
            -- UPDATE XXADEO_STG_PRICE_CHANGE
            if not INSERT_STG_PRICE_CHANGE(O_error_message,
                                           L_rec_new_store,
                                           XXADEO_RPM_CONSTANTS_SQL.ACTION_R,
                                           L_rec_store) then
              --
              close C_exists_store_rpm_base;
              RETURN;
              --
            end if;
            --
            -- UPDATE XXADEO_STG_PRICE_REQUEST
            if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                            L_rec_new_store,
                                            XXADEO_RPM_CONSTANTS_SQL.STATUS_A,
                                            XXADEO_RPM_CONSTANTS_SQL.ACTION_U) then
              --
              close C_exists_store_rpm_base;
              RETURN;
              --
            end if;
            --
          -- If No...(Store Date = Zone Date)
          else
            --
            if L_rec_store.effective_date = L_temp_rec.effective_date then
              --
              -- # Create a new Store PC to disapprove a exists PC #
              -- INSERT XXADEO_STG_PRICE_CHANGE
              if not INSERT_STG_PRICE_CHANGE(O_error_message,
                                             L_rec_new_store,
                                             XXADEO_RPM_CONSTANTS_SQL.STATUS_P,
                                             L_rec_store) then
                --
                close C_exists_store_rpm_base;
                RETURN;
                --
              end if;
              --
              -- UPDATE XXADEO_STG_PRICE_REQUEST
              if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                              L_rec_new_store,
                                              XXADEO_RPM_CONSTANTS_SQL.STATUS_A,
                                              XXADEO_RPM_CONSTANTS_SQL.ACTION_U) then
                --
                close C_exists_store_rpm_base;
                RETURN;
                --
              end if;
              --
              -- # Create a new PC for approve with dependency #
              --
              L_rec_store.price_change_id         := NULL;
              L_rec_store.price_change_display_id := NULL;
              --
              -- INSERT XXADEO_STG_PRICE_CHANGE
              if not INSERT_STG_PRICE_CHANGE(O_error_message,
                                             L_rec_new_store,
                                             XXADEO_RPM_CONSTANTS_SQL.STATUS_P,
                                             L_rec_store) then
                --
                close C_exists_store_rpm_base;
                RETURN;
                --
              end if;
              --
              -- UPDATE XXADEO_STG_PRICE_REQUEST
              if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                              L_rec_new_store,
                                              XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                              XXADEO_RPM_CONSTANTS_SQL.ACTION_N,
                                              L_temp_rec.stage_price_change_id,
                                              DEP_TYPE_SOURCE_RPM) then
                --
                close C_exists_store_rpm_base;
                RETURN;
                --
              end if;
              --
            end if;
            --
          end if;
          --
        end loop;
        --
        close C_exists_store_rpm_base;
        --
      end if;
      --
    end if;
    --
    -- # STORE Price Change - STORE PORTAL #
    if L_temp_rec.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE then
      --
      -- start EFB
      --
      L_rpm_price_change := NULL;
      --
      open C_exists_rpm_base(L_temp_rec);
      fetch C_exists_rpm_base into L_rpm_price_change;
      close C_exists_rpm_base;
      --
      -- end EFB
      --
      /*open C_chk_rpm_duplicated_w(L_temp_rec);
      fetch C_chk_rpm_duplicated_w into L_chk_rpm_duplicated_w;
      close C_chk_rpm_duplicated_w;*/
      --
      open C_chk_rpm_duplicated(L_temp_rec);
      fetch C_chk_rpm_duplicated into L_return;
      --
      -- If is Duplicate, ERROR
      if C_chk_rpm_duplicated%FOUND then
        --
        close C_chk_rpm_duplicated;
        --
        -- RETAIL PRICE BLOCKING
        --
        if VALIDATE_RETAIL_PRICE_BLOCKING(O_error_message               => O_error_message,
                                          I_xxadeo_rpm_stg_price_change => L_temp_rec) = FALSE then
          --
          L_temp_rec.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
          L_temp_rec.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_PORTAL;
          --
          -- Update Store PC to Rejected
          --
          if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                         L_temp_rec) then
            --
            RETURN;
            --
          end if;
          --
          -- Update Store PC to Rejected
          --
          if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                          L_temp_rec,
                                          XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                          XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
            --
            close C_exists_store_rpm_base;
            RETURN;
            --
          end if;
          --
          RAISE SAVE_N_RETURN;
          --
        end if;
        --
        L_rpm_zone_price_change := NULL;
        --
        if VALIDATE_FUTURE_PRICE(O_error_message               => O_error_message,
                                 I_xxadeo_rpm_stg_price_change => L_temp_rec) = FALSE then
          --
          L_temp_rec.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
          L_temp_rec.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_AMOUNT_HIGH;
          --
          if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                         L_temp_rec) then
            --
            RETURN;
            --
          end if;
          --
          -- Insert into Request
          if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                          L_temp_rec,
                                          XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                          XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
            --
            RETURN;
            --
          end if;
          --
          RAISE SAVE_N_RETURN;
          --
        end if;
        --
        -- Store Price >= Zone Price ?
        --
        -- Exists Zone PC? Store Date >= Zone Date ?
        open C_exists_zone_rpm_base(L_temp_rec);
        --
        -- If Yes...(Store Date >= Zone Date)
        fetch C_exists_zone_rpm_base into L_rpm_zone_price_change;
        --
        if C_exists_zone_rpm_base%FOUND then
          --
          close C_exists_zone_rpm_base;
          --
          if L_temp_rec.change_amount >= L_rpm_zone_price_change.change_amount then
            --
            L_temp_rec.status         := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
            L_temp_rec.error_message  := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_AMOUNT_HIGH;
            --
            -- Update Store PC to Rejected
            if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                           L_temp_rec) then
              --
              RETURN;
              --
            end if;
            --
            -- Update Store PC to Rejected
            if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                            L_temp_rec,
                                            XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                            XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
              --
              close C_exists_store_rpm_base;
              RETURN;
              --
            end if;
            --
          end if;
          --
          close C_exists_zone_rpm_base;
          --
          RAISE SAVE_N_RETURN;
          --
        end if;
        --
        if L_temp_rec.status not in (XXADEO_RPM_CONSTANTS_SQL.STATUS_R,XXADEO_RPM_CONSTANTS_SQL.STATUS_E) and L_temp_rec.error_message  is null then
          --
          if CONV_RPM_PC_TO_XXADEO_STG_PC(O_error_message,
                                          L_rec_store,
                                          L_rpm_price_change) = FALSE then
            --
            close C_exists_store_rpm_base;
            RETURN;
            --
          end if;
          -- # Create a new Store PC to disapprove a exists PC #
          if not INSERT_STG_PRICE_CHANGE(O_error_message,
                                         L_rec_new_store,
                                         XXADEO_RPM_CONSTANTS_SQL.STATUS_P,
                                         L_rec_store) then
            --
            RETURN;
            --
          end if;
          --
          update xxadeo_rpm_stage_price_change rspc
             set rspc.price_change_id         = L_rec_new_store.price_change_id,
                 rspc.price_change_display_id = L_rec_new_store.price_change_display_id,
                 rspc.status                  = XXADEO_RPM_CONSTANTS_SQL.ACTION_U
           where rspc.xxadeo_process_id     = L_temp_rec.xxadeo_process_id
             and rspc.stage_price_change_id = L_temp_rec.stage_price_change_id;
          --
          -- UPDATE XXADEO_STG_PRICE_REQUEST
          if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                          L_rec_new_store,
                                          XXADEO_RPM_CONSTANTS_SQL.STATUS_A,
                                          XXADEO_RPM_CONSTANTS_SQL.ACTION_U) then
            --
            RETURN;
            --
          end if;
          --
          L_temp_rec.price_change_id := L_rec_new_store.price_change_id;
          L_temp_rec.price_change_display_id := L_rec_new_store.price_change_display_id;
          --
          if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                          L_temp_rec,
                                          XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                          XXADEO_RPM_CONSTANTS_SQL.ACTION_U) then
            --
            RETURN;
            --
          end if;
          --
        end if;
        --
      else
        --
        close C_chk_rpm_duplicated;
        --
        -- RETAIL PRICE BLOCKING
        --
        if VALIDATE_RETAIL_PRICE_BLOCKING(O_error_message               => O_error_message,
                                          I_xxadeo_rpm_stg_price_change => L_temp_rec) = FALSE then
         --
         L_temp_rec.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
         L_temp_rec.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_PORTAL;
         --
         -- Update Store PC to Rejected
         --
         if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                        L_temp_rec) then
           --
           RETURN;
           --
         end if;
         --
         -- Update Store PC to Rejected
         --
         if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                         L_temp_rec,
                                         XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                         XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
           --
           close C_exists_store_rpm_base;
           RETURN;
           --
         end if;
         --
         RAISE SAVE_N_RETURN;
         --
        end if;
        --
        if L_temp_rec.status not in (XXADEO_RPM_CONSTANTS_SQL.STATUS_E,XXADEO_RPM_CONSTANTS_SQL.STATUS_R) and L_temp_rec.error_message is null then
          --
          if VALIDATE_FUTURE_PRICE(O_error_message               => O_error_message,
                                   I_xxadeo_rpm_stg_price_change => L_temp_rec) = FALSE then
            --
            L_temp_rec.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
            L_temp_rec.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_AMOUNT_HIGH;
            --
            if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                           L_temp_rec) then
              --
              RETURN;
              --
            end if;
            --
            -- Insert into Request
            if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                            L_temp_rec,
                                            XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                            XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
              --
              RETURN;
              --
            end if;
            --
            RAISE SAVE_N_RETURN;
            --
          end if;
          --
          --
          -- Exists Zone PC? Store Date >= Zone Date ?
          open C_exists_zone_rpm_base(L_temp_rec);
          --
          -- If Yes...(Store Date >= Zone Date)
          fetch C_exists_zone_rpm_base into L_rpm_price_change;
          --
          if C_exists_zone_rpm_base%FOUND then
            --
            close C_exists_zone_rpm_base;
            --
            -- Store Price >= Zone Price ?
            --
            if L_temp_rec.change_amount >= L_rpm_price_change.change_amount then
              --
              L_temp_rec.status        := XXADEO_RPM_CONSTANTS_SQL.STATUS_R;
              L_temp_rec.error_message := XXADEO_RPM_CONSTANTS_SQL.VAL$PC_STORE_AMOUNT_HIGH;
              --
              -- Update Store PC to Rejected
              if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                             L_temp_rec) then
                --
                RETURN;
                --
              end if;
              --
              -- Update Store PC to Rejected
              if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                              L_temp_rec,
                                              XXADEO_RPM_CONSTANTS_SQL.STATUS_R,
                                              XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
                --
                close C_exists_store_rpm_base;
                RETURN;
                --
              end if;
              --
            -- If No...(Store Price < Zone Price)
            else
              --
              if L_temp_rec.effective_date = L_rpm_price_change.effective_date then
                --
                -- # Approve Store PC with Zone dependency #
                -- UPDATE XXADEO_STG_PRICE_REQUEST
                if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                                L_temp_rec/*L_rec_new_store*/,
                                                XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                                XXADEO_RPM_CONSTANTS_SQL.ACTION_N,
                                                L_rpm_price_change.price_change_id,
                                                DEP_TYPE_SOURCE_RPM) then
                  --
                  close C_exists_store_rpm_base;
                  RETURN;
                  --
                end if;
                --
              else
                --
                -- # Approve Store PC #
                -- UPDATE XXADEO_STG_PRICE_REQUEST
                if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                                L_temp_rec,
                                                XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                                XXADEO_RPM_CONSTANTS_SQL.ACTION_N) then
                  --
                  close C_exists_store_rpm_base;
                  RETURN;
                  --
                end if;
                --
              end if;
              --
            end if;
            --
          else
            --
            -- # Approve Store PC #
            -- UPDATE XXADEO_STG_PRICE_REQUEST
            if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                            L_temp_rec,
                                            XXADEO_RPM_CONSTANTS_SQL.STATUS_W,
                                            XXADEO_RPM_CONSTANTS_SQL.ACTION_N) then
              --
              close C_exists_store_rpm_base;
              RETURN;
              --
            end if;
            --
          end if;
          --
        end if;
        --
      end if;
      --
    end if;
    --
  end if;
    --
  --
  if L_temp_rec.status not in (XXADEO_RPM_CONSTANTS_SQL.STATUS_E,XXADEO_RPM_CONSTANTS_SQL.STATUS_R) then
    --
    -- # Regular Price Change - XXADEO_STG #
    if VALIDATE_XXADEO_STG(O_error_message               => O_error_message,
                           IO_error                      => L_error,
                           I_xxadeo_rpm_stg_price_change => L_temp_rec) = FALSE then
      --
      RETURN;
      --
    end if;
    --
    if L_error = TRUE then
      --
      RAISE SAVE_N_RETURN;
      --
    else
      --
      L_temp_rec.status      := XXADEO_RPM_CONSTANTS_SQL.STATUS_P;
      --
      -- Update to Processed
      if not UPDATE_STG_PRICE_CHANGE(O_error_message,
                                     L_temp_rec) then
        --
        RETURN;
        --
      end if;
      --
    end if;
    --
  end if;
  --

  --
  COMMIT;
  --
  EXCEPTION
    --
    when SAVE_N_RETURN then
      --
      COMMIT;
      RETURN;
    --
    when OTHERS then
      --
      if C_exists_rpm_base%ISOPEN then
        close C_exists_rpm_base;
      end if;
      --
      if C_chk_rpm_duplicated%ISOPEN then
        close C_chk_rpm_duplicated;
      end if;
      --
      if C_exists_store_rpm_base%ISOPEN then
        close C_exists_store_rpm_base;
      end if;
      --
      if C_exists_zone_rpm_base%ISOPEN then
        close C_exists_zone_rpm_base;
      end if;
      --
      if C_exists_zone_rpm_base%ISOPEN then
        close C_exists_zone_rpm_base;
      end if;
      --
      if C_rpm_bu_price_zones%ISOPEN then
        close C_rpm_bu_price_zones;
      end if;
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN;
    --
END PROCESS_PRICE_CHANGE;

--------------------------------------------------------------------------------
FUNCTION PROCESS_EM_STORE_PORTAL_PC (O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                     I_custom_obj_rec IN     XXADEO_CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program          VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.PROCESS_EM_STORE_PORTAL_PC';
  --
  L_error            BOOLEAN := FALSE;
  L_code_id          RPM_CODES.CODE_ID%TYPE := NULL;
  L_return           VARCHAR2(1) := NULL;
  --
  cursor C_xxadeo_rpm_stage_pc (I_reason_code  rpm_codes.code_id%type) is
    select decode(a.status,
                  XXADEO_RPM_CONSTANTS_SQL.ACTION_D,1,
                  XXADEO_RPM_CONSTANTS_SQL.ACTION_U,2,3) status_priority,
           a.*
      from xxadeo_rpm_stage_price_change a,
           table(I_custom_obj_rec.price_events_tbl) b
     where a.status in (XXADEO_RPM_CONSTANTS_SQL.ACTION_D, XXADEO_RPM_CONSTANTS_SQL.ACTION_U, XXADEO_RPM_CONSTANTS_SQL.ACTION_N)
       and a.reason_code = I_reason_code
       and trunc(a.effective_date) <= trunc(LP_vdate)
       and a.xxadeo_process_id = b.xxadeo_process_id
       and a.stage_price_change_id = b.stage_price_change_id
    order by status_priority asc;
  --
  cursor C_chk_xxadeo_rpm_stage_pc (I_xxadeo_process_id      XXADEO_RPM_STAGE_PRICE_CHANGE.XXADEO_PROCESS_ID%TYPE,
                                    I_stage_price_change_id  XXADEO_RPM_STAGE_PRICE_CHANGE.STAGE_PRICE_CHANGE_ID%TYPE,
                                    I_status                 XXADEO_RPM_STAGE_PRICE_CHANGE.STATUS%TYPE ) is
    select 'Y'
      from xxadeo_rpm_stage_price_change a
     where a.xxadeo_process_id = I_xxadeo_process_id
       and a.stage_price_change_id = I_stage_price_change_id
       and a.status = I_status;
  --
  L_xxadeo_rpm_stage_pc     C_xxadeo_rpm_stage_pc%ROWTYPE;
  --
BEGIN
  --
  open C_get_reason_code(XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_EM_ST_PORTAL);
  fetch C_get_reason_code into L_code_id;
  close C_get_reason_code;
  --
  open C_xxadeo_rpm_stage_pc (L_code_id);
  loop
    --
    L_xxadeo_rpm_stage_pc := NULL;
    --
    fetch C_xxadeo_rpm_stage_pc into L_xxadeo_rpm_stage_pc;
    exit when C_xxadeo_rpm_stage_pc%NOTFOUND;
    --
    L_error := FALSE;
    --
    open C_chk_xxadeo_rpm_stage_pc (L_xxadeo_rpm_stage_pc.xxadeo_process_id,
                                    L_xxadeo_rpm_stage_pc.stage_price_change_id,
                                    L_xxadeo_rpm_stage_pc.status);
    fetch C_chk_xxadeo_rpm_stage_pc into L_return;
    --
    if C_chk_xxadeo_rpm_stage_pc%FOUND then
      --
      PROCESS_PRICE_CHANGE(O_error_message         => O_error_message,
                           I_process_id            => L_xxadeo_rpm_stage_pc.xxadeo_process_id,
                           I_stage_price_change_id => L_xxadeo_rpm_stage_pc.stage_price_change_id);
      --
    end if;
    --
    close C_chk_xxadeo_rpm_stage_pc;
    --
  end loop;
  --
  close C_xxadeo_rpm_stage_pc;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END PROCESS_EM_STORE_PORTAL_PC;

--------------------------------------------------------------------------------
FUNCTION PROCESS_EMERGENCY_PC (O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                               I_custom_obj_rec IN     XXADEO_CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program          VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.PROCESS_EMERGENCY_PC';
  --
  L_error            BOOLEAN := FALSE;
  L_code_id          RPM_CODES.CODE_ID%TYPE := NULL;
  L_return           VARCHAR2(1) := NULL;
  --
  cursor C_xxadeo_rpm_stage_pc (I_reason_code  RPM_CODES.CODE_ID%TYPE) is
    select decode(a.status,
                  XXADEO_RPM_CONSTANTS_SQL.ACTION_D,1,
                  XXADEO_RPM_CONSTANTS_SQL.ACTION_U,2,3) status_priority,
           a.*
      from xxadeo_rpm_stage_price_change a,
           table(I_custom_obj_rec.price_events_tbl) b
     where a.status in (XXADEO_RPM_CONSTANTS_SQL.ACTION_D, XXADEO_RPM_CONSTANTS_SQL.ACTION_U, XXADEO_RPM_CONSTANTS_SQL.ACTION_N)
       and a.reason_code != I_reason_code
       and trunc(a.effective_date) <= trunc(LP_vdate)
       and a.xxadeo_process_id = b.xxadeo_process_id
       and a.stage_price_change_id = b.stage_price_change_id
    order by status_priority asc;
  --
  cursor C_chk_xxadeo_rpm_stage_pc (I_xxadeo_process_id      XXADEO_RPM_STAGE_PRICE_CHANGE.XXADEO_PROCESS_ID%TYPE,
                                    I_stage_price_change_id  XXADEO_RPM_STAGE_PRICE_CHANGE.STAGE_PRICE_CHANGE_ID%TYPE,
                                    I_status                 XXADEO_RPM_STAGE_PRICE_CHANGE.STATUS%TYPE ) is
    select 'Y'
      from xxadeo_rpm_stage_price_change a
     where a.xxadeo_process_id = I_xxadeo_process_id
       and a.stage_price_change_id = I_stage_price_change_id
       and a.status = I_status;
  --
  L_xxadeo_rpm_stage_pc     C_xxadeo_rpm_stage_pc%ROWTYPE;
  --
BEGIN
  --
  open C_get_reason_code(XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_EM_ST_PORTAL);
  fetch C_get_reason_code into L_code_id;
  close C_get_reason_code;
  --
  open C_xxadeo_rpm_stage_pc (L_code_id);
  loop
    --
    L_xxadeo_rpm_stage_pc := NULL;
    --
    fetch C_xxadeo_rpm_stage_pc into L_xxadeo_rpm_stage_pc;
    exit when C_xxadeo_rpm_stage_pc%NOTFOUND;
    --
    L_error := FALSE;
    --
    open C_chk_xxadeo_rpm_stage_pc (L_xxadeo_rpm_stage_pc.xxadeo_process_id,
                                    L_xxadeo_rpm_stage_pc.stage_price_change_id,
                                    L_xxadeo_rpm_stage_pc.status);
    fetch C_chk_xxadeo_rpm_stage_pc into L_return;
    --
    if C_chk_xxadeo_rpm_stage_pc%FOUND then
      --
      PROCESS_PRICE_CHANGE(O_error_message         => O_error_message,
                           I_process_id            => L_xxadeo_rpm_stage_pc.xxadeo_process_id,
                           I_stage_price_change_id => L_xxadeo_rpm_stage_pc.stage_price_change_id);
      --
    end if;
    --
    close C_chk_xxadeo_rpm_stage_pc;
    --
  end loop;
  --
  close C_xxadeo_rpm_stage_pc;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END PROCESS_EMERGENCY_PC;

--------------------------------------------------------------------------------
FUNCTION PROCESS_RPO_PC (O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                         I_custom_obj_rec IN     XXADEO_CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program          VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.PROCESS_RPO_PC';
  --
  L_error            BOOLEAN := FALSE;
  L_code_id          RPM_CODES.CODE_ID%TYPE := NULL;
  L_return           VARCHAR2(1) := NULL;
  --
  cursor C_xxadeo_rpm_stage_pc (I_reason_code  RPM_CODES.CODE_ID%TYPE) is
    select decode(a.status,
                  XXADEO_RPM_CONSTANTS_SQL.ACTION_D,1,
                  XXADEO_RPM_CONSTANTS_SQL.ACTION_U,2,3) status_priority,
           a.*
      from xxadeo_rpm_stage_price_change a,
           table(I_custom_obj_rec.price_events_tbl) b
     where a.status in (XXADEO_RPM_CONSTANTS_SQL.ACTION_D, XXADEO_RPM_CONSTANTS_SQL.ACTION_U, XXADEO_RPM_CONSTANTS_SQL.ACTION_N)
       and a.reason_code = I_reason_code
       and trunc(a.effective_date) > trunc(LP_vdate)
       and a.xxadeo_process_id = b.xxadeo_process_id
       and a.stage_price_change_id = b.stage_price_change_id
    order by status_priority asc;
  --
  cursor C_chk_xxadeo_rpm_stage_pc (I_xxadeo_process_id      XXADEO_RPM_STAGE_PRICE_CHANGE.XXADEO_PROCESS_ID%TYPE,
                                    I_stage_price_change_id  XXADEO_RPM_STAGE_PRICE_CHANGE.STAGE_PRICE_CHANGE_ID%TYPE,
                                    I_status                 XXADEO_RPM_STAGE_PRICE_CHANGE.STATUS%TYPE ) is
    select 'Y'
      from xxadeo_rpm_stage_price_change a
     where a.xxadeo_process_id = I_xxadeo_process_id
       and a.stage_price_change_id = I_stage_price_change_id
       and a.status = I_status;
  --
  L_xxadeo_rpm_stage_pc     C_xxadeo_rpm_stage_pc%ROWTYPE;
  --
BEGIN
  --
  open C_get_reason_code(XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_RPO);
  fetch C_get_reason_code into L_code_id;
  close C_get_reason_code;
  --
  open C_xxadeo_rpm_stage_pc (L_code_id);
  loop
    --
    L_xxadeo_rpm_stage_pc := NULL;
    --
    fetch C_xxadeo_rpm_stage_pc into L_xxadeo_rpm_stage_pc;
    exit when C_xxadeo_rpm_stage_pc%NOTFOUND;
    --
    L_error := FALSE;
    --
    open C_chk_xxadeo_rpm_stage_pc (L_xxadeo_rpm_stage_pc.xxadeo_process_id,
                                    L_xxadeo_rpm_stage_pc.stage_price_change_id,
                                    L_xxadeo_rpm_stage_pc.status);
    fetch C_chk_xxadeo_rpm_stage_pc into L_return;
    --
    if C_chk_xxadeo_rpm_stage_pc%FOUND then
      --
      PROCESS_PRICE_CHANGE(O_error_message         => O_error_message,
                           I_process_id            => L_xxadeo_rpm_stage_pc.xxadeo_process_id,
                           I_stage_price_change_id => L_xxadeo_rpm_stage_pc.stage_price_change_id);
      --
    end if;
    --
    close C_chk_xxadeo_rpm_stage_pc;
    --
  end loop;
  --
  close C_xxadeo_rpm_stage_pc;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END PROCESS_RPO_PC;

--------------------------------------------------------------------------------
FUNCTION PROCESS_STORE_PORTAL_PC (O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_custom_obj_rec IN     XXADEO_CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program          VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.PROCESS_STORE_PORTAL_PC';
  --
  L_error            BOOLEAN := FALSE;
  L_code_id          RPM_CODES.CODE_ID%TYPE := NULL;
  L_return           VARCHAR2(1) := NULL;
  --
  cursor C_xxadeo_rpm_stage_pc (I_reason_code  RPM_CODES.CODE_ID%TYPE) is
    select decode(a.status,
                  XXADEO_RPM_CONSTANTS_SQL.ACTION_D,1,
                  XXADEO_RPM_CONSTANTS_SQL.ACTION_U,2,3) status_priority,
           a.*
      from xxadeo_rpm_stage_price_change a,
           table(I_custom_obj_rec.price_events_tbl) b
     where a.status in (XXADEO_RPM_CONSTANTS_SQL.ACTION_D, XXADEO_RPM_CONSTANTS_SQL.ACTION_U, XXADEO_RPM_CONSTANTS_SQL.ACTION_N)
       and a.reason_code = I_reason_code
       and trunc(a.effective_date) > trunc(LP_vdate)
       and a.xxadeo_process_id = b.xxadeo_process_id
       and a.stage_price_change_id = b.stage_price_change_id
    order by status_priority asc;
  --
  cursor C_chk_xxadeo_rpm_stage_pc (I_xxadeo_process_id      XXADEO_RPM_STAGE_PRICE_CHANGE.XXADEO_PROCESS_ID%TYPE,
                                    I_stage_price_change_id  XXADEO_RPM_STAGE_PRICE_CHANGE.STAGE_PRICE_CHANGE_ID%TYPE,
                                    I_status                 XXADEO_RPM_STAGE_PRICE_CHANGE.STATUS%TYPE ) is
    select 'Y'
      from xxadeo_rpm_stage_price_change a
     where a.xxadeo_process_id = I_xxadeo_process_id
       and a.stage_price_change_id = I_stage_price_change_id
       and a.status = I_status;
  --
  L_xxadeo_rpm_stage_pc     C_xxadeo_rpm_stage_pc%ROWTYPE;
  --
BEGIN
  --
  open C_get_reason_code(XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_ST_PORTAL);
  fetch C_get_reason_code into L_code_id;
  close C_get_reason_code;
  --
  open C_xxadeo_rpm_stage_pc (L_code_id);
  loop
    --
    L_xxadeo_rpm_stage_pc := NULL;
    --
    fetch C_xxadeo_rpm_stage_pc into L_xxadeo_rpm_stage_pc;
    exit when C_xxadeo_rpm_stage_pc%NOTFOUND;
    --
    L_error := FALSE;
    --
    open C_chk_xxadeo_rpm_stage_pc (L_xxadeo_rpm_stage_pc.xxadeo_process_id,
                                    L_xxadeo_rpm_stage_pc.stage_price_change_id,
                                    L_xxadeo_rpm_stage_pc.status);
    fetch C_chk_xxadeo_rpm_stage_pc into L_return;
    --
    if C_chk_xxadeo_rpm_stage_pc%FOUND then
      --
      PROCESS_PRICE_CHANGE(O_error_message         => O_error_message,
                           I_process_id            => L_xxadeo_rpm_stage_pc.xxadeo_process_id,
                           I_stage_price_change_id => L_xxadeo_rpm_stage_pc.stage_price_change_id);
      --
    end if;
    --
    close C_chk_xxadeo_rpm_stage_pc;
    --
  end loop;
  --
  close C_xxadeo_rpm_stage_pc;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END PROCESS_STORE_PORTAL_PC;

--------------------------------------------------------------------------------
FUNCTION PROCESS_SIMTAR_PC (O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                            I_custom_obj_rec IN     XXADEO_CUSTOM_OBJ_REC)
RETURN BOOLEAN IS
  --
  L_program          VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.PROCESS_SIMTAR_PC';
  --
  L_error            BOOLEAN := FALSE;
  L_code_id          RPM_CODES.CODE_ID%TYPE := NULL;
  L_return           VARCHAR2(1) := NULL;
  --
  cursor C_xxadeo_rpm_stage_pc (I_reason_code  RPM_CODES.CODE_ID%TYPE) is
    select decode(a.status,
                  XXADEO_RPM_CONSTANTS_SQL.ACTION_D,1,
                  XXADEO_RPM_CONSTANTS_SQL.ACTION_U,2,3) status_priority,
           a.*
      from xxadeo_rpm_stage_price_change a,
           table(I_custom_obj_rec.price_events_tbl) b
     where a.status in (XXADEO_RPM_CONSTANTS_SQL.ACTION_D, XXADEO_RPM_CONSTANTS_SQL.ACTION_U, XXADEO_RPM_CONSTANTS_SQL.ACTION_N)
       and a.reason_code = I_reason_code
       and trunc(a.effective_date) > trunc(LP_vdate)
       and a.xxadeo_process_id = b.xxadeo_process_id
       and a.stage_price_change_id = b.stage_price_change_id
    order by status_priority asc;
  --
  cursor C_chk_xxadeo_rpm_stage_pc (I_xxadeo_process_id      XXADEO_RPM_STAGE_PRICE_CHANGE.XXADEO_PROCESS_ID%TYPE,
                                    I_stage_price_change_id  XXADEO_RPM_STAGE_PRICE_CHANGE.STAGE_PRICE_CHANGE_ID%TYPE,
                                    I_status                 XXADEO_RPM_STAGE_PRICE_CHANGE.STATUS%TYPE ) is
    select 'Y'
      from xxadeo_rpm_stage_price_change a
     where a.xxadeo_process_id = I_xxadeo_process_id
       and a.stage_price_change_id = I_stage_price_change_id
       and a.status = I_status;
  --
  L_xxadeo_rpm_stage_pc     C_xxadeo_rpm_stage_pc%ROWTYPE;
  --
BEGIN
  --
  open C_get_reason_code(XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_SIMTAR);
  fetch C_get_reason_code into L_code_id;
  close C_get_reason_code;
  --
  open C_xxadeo_rpm_stage_pc (L_code_id);
  loop
    --
    L_xxadeo_rpm_stage_pc := NULL;
    --
    fetch C_xxadeo_rpm_stage_pc into L_xxadeo_rpm_stage_pc;
    exit when C_xxadeo_rpm_stage_pc%NOTFOUND;
    --
    L_error := FALSE;
    --
    open C_chk_xxadeo_rpm_stage_pc (L_xxadeo_rpm_stage_pc.xxadeo_process_id,
                                    L_xxadeo_rpm_stage_pc.stage_price_change_id,
                                    L_xxadeo_rpm_stage_pc.status);
    fetch C_chk_xxadeo_rpm_stage_pc into L_return;
    --
    if C_chk_xxadeo_rpm_stage_pc%FOUND then
      --
      PROCESS_PRICE_CHANGE(O_error_message         => O_error_message,
                           I_process_id            => L_xxadeo_rpm_stage_pc.xxadeo_process_id,
                           I_stage_price_change_id => L_xxadeo_rpm_stage_pc.stage_price_change_id);
      --
    end if;
    --
    close C_chk_xxadeo_rpm_stage_pc;
    --
  end loop;
  --
  close C_xxadeo_rpm_stage_pc;
  --
  RETURN TRUE;
  --
  EXCEPTION
    when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      --
      RETURN FALSE;
    --
END PROCESS_SIMTAR_PC;

--------------------------------------------------------------------------------
FUNCTION RUN_PROCESS_PRICE_CHANGE(O_error_message            IN OUT LOGGER_LOGS.TEXT%TYPE,
                                  I_xxadeo_price_events_tbl  IN     XXADEO_PRICE_EVENTS_TBL)
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(64) := 'XXADEO_PRICE_CHANGE_SQL.RUN_PROCESS_PRICE_CHANGE';
  --
  L_execute_string VARCHAR2(2000);
  L_return         VARCHAR2(1);
  L_error_count    NUMBER;
  L_temp_rec       XXADEO_RPM_STAGE_PRICE_CHANGE%ROWTYPE;
  L_custom_obj_rec XXADEO_CUSTOM_OBJ_REC :=  XXADEO_CUSTOM_OBJ_REC();
  L_xxadeo_price_events_tbl_fix  XXADEO_PRICE_EVENTS_TBL := XXADEO_PRICE_EVENTS_TBL();
  --
  cursor C_get_price_event_id_seq is
    select xxadeo_price_event_id_seq.nextval
      from dual;
  --
  cursor C_xxadeo_rpm_stage_pc (L_xxadeo_process_id      xxadeo_rpm_stage_price_change.xxadeo_process_id%type,
                                L_stage_price_change_id  xxadeo_rpm_stage_price_change.stage_price_change_id%type) is
    select a.*
      from xxadeo_rpm_stage_price_change a
     where xxadeo_process_id = L_xxadeo_process_id
       and stage_price_change_id = L_stage_price_change_id;
  --
BEGIN
  --
  for i in 1 .. I_xxadeo_price_events_tbl.last loop
    --
    L_temp_rec := NULL;
    --
    open C_xxadeo_rpm_stage_pc(I_xxadeo_price_events_tbl(i).xxadeo_process_id,
                               I_xxadeo_price_events_tbl(i).stage_price_change_id);
    fetch C_xxadeo_rpm_stage_pc into L_temp_rec;
    close C_xxadeo_rpm_stage_pc;
    --
    L_error_count := 0;
    --
    if RPM_BASE_VALIDATIONS(O_error_message               => O_error_message,
                            O_error_count                 => L_error_count,
                            I_xxadeo_rpm_stg_price_change => L_temp_rec) = FALSE then
      --
      RETURN FALSE;
      --
    end if;
    --
    if L_error_count > 0 then
      --
      if not UPDATE_STG_PRICE_REQUEST(O_error_message,
                                      L_temp_rec,
                                      XXADEO_RPM_CONSTANTS_SQL.STATUS_E,
                                      XXADEO_RPM_CONSTANTS_SQL.ACTION_R) then
        --
        RETURN FALSE;
        --
      end if;
      --
    else
      --
      L_xxadeo_price_events_tbl_fix.extend;
      L_xxadeo_price_events_tbl_fix(L_xxadeo_price_events_tbl_fix.count) := new I_xxadeo_price_events_tbl(i);
      --
    end if;
    --
  end loop;
  --
  COMMIT;
  --
  open C_get_price_event_id_seq;
  fetch C_get_price_event_id_seq into LP_price_event_id_seq;
  close C_get_price_event_id_seq;
  --
  L_custom_obj_rec.function_key     := LP_func_area;
  L_custom_obj_rec.price_events_tbl := L_xxadeo_price_events_tbl_fix;
  --
  if XXADEO_CUSTOM_CONFIG_WRP_SQL.RUN_SYSTEM_RULES_WRP(O_error_message,
                                                       L_custom_obj_rec) = FALSE then
     return FALSE;
  end if;
  --
  if UPDATE_PRICE_CHANGE_PRCSS_ORD(O_error_message) = FALSE then
    --
    RETURN FALSE;
    --
  end if;
  --
  COMMIT;
  --
  RETURN TRUE;
  --
EXCEPTION
  --
  WHEN OTHERS THEN
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN FALSE;
  --
END RUN_PROCESS_PRICE_CHANGE;
--------------------------------------------------------------------------------
END XXADEO_PRICE_CHANGE_SQL;
/
