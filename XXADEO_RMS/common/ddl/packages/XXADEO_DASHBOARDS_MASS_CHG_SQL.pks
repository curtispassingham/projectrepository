CREATE OR REPLACE PACKAGE XXADEO_DASHBOARDS_MASS_CHG_SQL IS

  /******************************************************************************/
  /* CREATE DATE - August 2018                                                  */
  /* CREATE USER - Tiago Torres                                                 */
  /* PROJECT     - ADEO                                                         */
  /* DESCRIPTION - Item list to be used in static report                        */
  /******************************************************************************/

  ------------------------------------------------------------------------------------------
  --Function Name : XXADEO_MASS_CHG_ITEM_PROCESS
  --Purpose       : Store item IDs with a process ID associated to be used in static report.
  ------------------------------------------------------------------------------------------
  FUNCTION XXADEO_MASS_CHG_ITEM_PROCESS(I_item_list XXADEO_ITEM_MASS_CHANGE_TBL := XXADEO_ITEM_MASS_CHANGE_TBL(),
                                        O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN NUMBER;
    
  ------------------------------------------------------------------------------------------
  --Function Name : XXADEO_ITEM_LIST_PURGE
  --Purpose       : Dashboard item list purge
  ------------------------------------------------------------------------------------------
  FUNCTION XXADEO_ITEM_LIST_PURGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN BOOLEAN;

END XXADEO_DASHBOARDS_MASS_CHG_SQL;
/