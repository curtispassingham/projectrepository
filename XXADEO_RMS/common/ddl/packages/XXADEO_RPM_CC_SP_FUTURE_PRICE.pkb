CREATE OR REPLACE PACKAGE BODY XXADEO_RPM_CC_SP_FUTURE_PRICE AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS
  --
  L_program             VARCHAR2(60) := 'XXADEO_RPM_CC_SP_FUTURE_PRICE.VALIDATE';
  --
  L_error_key VARCHAR2(255)             := NULL;
  L_error_rec CONFLICT_CHECK_ERROR_REC  := NULL;
  L_error_tbl CONFLICT_CHECK_ERROR_TBL  := CONFLICT_CHECK_ERROR_TBL();
  L_vdate     DATE                      := GET_VDATE;
  --
  cursor C_CHECK is
    with rpm_zone_tbl as (select rzl.zone_id,
                                 rzl.location
                            from rpm_zone_location rzl),
         rpm_reason_code as (select code_id
                               from rpm_codes rc
                              where exists (select 1
                                              from code_detail cd
                                             where code_type    = XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_TYPE
                                               and code        in (XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_EM_ST_PORTAL, XXADEO_RPM_CONSTANTS_SQL.REASON_CODE_ST_PORTAL)
                                               and cd.code_desc = rc.code))
    select gtt.price_event_id,
           gtt.future_retail_id,
           gtt.item,
           (select rzt.zone_id 
              from rpm_price_change rpc,
                   rpm_reason_code  rsc
             where rpc.price_change_id = gtt.price_change_id
               and rpc.location        = gtt.location
               and rpc.reason_code     = rsc.code_id) zone_id, 
           gtt.pc_change_currency change_currency,
           gtt.action_date effective_date
      from rpm_future_retail_gtt gtt,
           rpm_zone_tbl          rzt
     where gtt.price_event_id NOT IN (select ccet.price_event_id
                                        from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
       and gtt.action_date    >= L_vdate
       and gtt.location        = rzt.location
       and gtt.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE;
  --
  cursor C_check_pcs(V_item RPM_PRICE_CHANGE.ITEM%TYPE,
                     V_zone_id RPM_ZONE_LOCATION.ZONE_ID%TYPE,
                     V_change_currency RPM_PRICE_CHANGE.CHANGE_CURRENCY%TYPE,
                     V_effective_date RPM_PRICE_CHANGE.EFFECTIVE_DATE%TYPE,
                     V_price_change_id RPM_PRICE_CHANGE.PRICE_CHANGE_ID%TYPE,
                     V_future_retail_id RPM_FUTURE_RETAIL_GTT.FUTURE_RETAIL_ID%TYPE) is
    with zone_price_change as (select max(effective_date) effective_date
                                 from rpm_price_change rpcx 
                                where (rpcx.effective_date between L_vdate and V_effective_date)
                                  and rpcx.zone_id         = V_zone_id
                                  and rpcx.item            = V_item
                                  and rpcx.change_currency = V_change_currency),
         zone_future_price as (select max(rfrx.action_date) action_date
                                 from rpm_future_retail rfrx
                                where rfrx.item           = V_item
                                  and rfrx.location       = V_zone_id
                                  and rfrx.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                  and (rfrx.action_date   between L_vdate and V_effective_date)),
         zone_future_retail as (select max(rzfrx.action_date) action_date
                                  from rpm_zone_future_retail rzfrx
                                 where rzfrx.item         = V_item
                                   and rzfrx.zone         = V_zone_id
                                   and (rzfrx.action_date between L_vdate and V_effective_date))
    select 1
      from rpm_price_change  rpc
     where (rpc.change_amount >=  (select rpcs.change_amount
                                     from rpm_price_change  rpcs,
                                          zone_price_change zpc
                                    where rpcs.zone_id          = V_zone_id
                                      and rpcs.item             = V_item
                                      and rpcs.change_currency  = V_change_currency
                                      and rpcs.effective_date   = zpc.effective_date
                                      and rpcs.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                                      and rpcs.price_change_id <> rpc.price_change_id 
                                      and rpcs.effective_date  <= rpc.effective_date
                                      and rownum                = 1)
        or  rpc.change_amount >= (select rizp.selling_retail
                                    from rpm_item_zone_price rizp
                                   where rizp.zone_id                 = V_zone_id
                                     and rizp.item                    = rpc.item
                                     and rizp.selling_retail_currency = rpc.change_currency
                                     and not exists (select 1
                                                       from rpm_price_change  rpcs,
                                                            zone_price_change zpc
                                                      where rpcs.zone_id          = V_zone_id
                                                        and rpcs.item             = V_item
                                                        and rpcs.change_currency  = V_change_currency
                                                        and rpcs.effective_date   = zpc.effective_date
                                                        and rpcs.price_change_id <> rpc.price_change_id
                                                        and rpcs.change_amount   >= rpc.change_amount
                                                        and rpcs.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE 
                                                        and rpcs.effective_date  <= rpc.effective_date)
                                     and rownum = 1)
         or rpc.change_amount >=  (select rfr.selling_retail
                                     from rpm_future_retail rfr,
                                          zone_future_price zfr
                                    where rfr.item              = V_item
                                      and rfr.location          = V_zone_id
                                      and rfr.action_date       = zfr.action_date
                                      and rfr.zone_node_type    = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                      and rfr.future_retail_id <> V_future_retail_id
                                      and rfr.action_date      <= rpc.effective_date
                                      and rownum                = 1)
         or rpc.change_amount >= (select rzfr.selling_retail
                                    from rpm_zone_future_retail rzfr,
                                         zone_future_retail     zfr
                                   where rzfr.item         = V_item
                                     and rzfr.zone         = V_zone_id
                                     and rzfr.action_date  = zfr.action_date
                                     and rzfr.action_date <= rpc.effective_date
                                     and rownum            = 1))
        and rpc.price_change_id = V_price_change_id;

BEGIN
  --
  if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE) then
    --
    if IO_error_table is NOT NULL and
       IO_error_table.COUNT > 0 then
      --
      L_error_tbl := IO_error_table;
      --
    else
       L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                               NULL,
                                               NULL,
                                               NULL);
       L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
    end if;
    --
    for rec IN C_CHECK loop
      --
      if rec.zone_id is NULL then
        --
        continue;
        --
      end if;
      --
      for i in C_check_pcs(rec.item,
                           rec.zone_id,
                           rec.change_currency,
                           rec.effective_date,
                           rec.price_event_id,
                           rec.future_retail_id) loop 
      --
      L_error_rec := NEW CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                  rec.future_retail_id,
                                                  RPM_CONSTANTS.CONFLICT_ERROR,
                                                  'xxadeo_pc_st_amount_high',
                                                  NULL);
      --
      if IO_error_table is NULL then
        --
        IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
        --
      else
        --
        IO_error_table.EXTEND;
        IO_error_table(IO_error_table.COUNT) := L_error_rec;
        --
      end if;
      --
      end loop;
      --
    end loop;
    --
  end if;
  --
  return 1;
  --
EXCEPTION
   when OTHERS then
     --
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
--------------------------------------------------------
END XXADEO_RPM_CC_SP_FUTURE_PRICE; 
/
