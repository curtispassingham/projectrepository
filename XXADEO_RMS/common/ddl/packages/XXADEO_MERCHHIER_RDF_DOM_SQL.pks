CREATE OR REPLACE PACKAGE XXADEO_MERCHHIER_RDF_DOM_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/*               Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package "XXADEO_MERCHHIER_RDF_DOM_SQL"                       */
/******************************************************************************/

/******************************************************************************/
/* CHANGES                                                                    */
/* 2018-08-23 - Jorge Agra - Validation of new domains in STG table           */
/******************************************************************************/

--------------------------------------------------------------------------------

  template_key                     CONSTANT VARCHAR2(255):='XXADEO_MERCHANDISE_HIERARCHY_RDF_DOMAINS';
  action_new                       VARCHAR2(25)          :='NEW';
  action_mod                       VARCHAR2(25)          :='MOD';
  action_del                       VARCHAR2(25)          :='DEL';
  
  SHEET_NAME_TRANS                 S9T_PKG.TRANS_MAP_TYP;

  GP_upload_create                 VARCHAR2(6)           := 'NEW';
  GP_upload_update                 VARCHAR2(6)           := 'MOD';
  GP_LOAD_SLS_CODE_Y               VARCHAR2(6)           := 'MHLSY';
  GP_LOAD_SLS_CODE_N               VARCHAR2(6)           := 'MHLSN';
  
  TABLE$DOMAIN                     VARCHAR2(255)         := 'DOMAIN';
  TABLE$DOMAIN_DEPS                VARCHAR2(255)         := 'DOMAIN_DEPS';
  TABLE$DOMAIN_CLASS               VARCHAR2(255)         := 'DOMAIN_CLASS';
  TABLE$DOMAIN_SUBCLASS            VARCHAR2(255)         := 'DOMAIN_SUBCLASS';

  RDF_DOMAINS_SHEET                VARCHAR2(255)         := 'RDF_DOMAINS';
  RDF_DOMAINS$ACTION               VARCHAR2(255)         := 'ACTION';
  RDF_DOMAINS$DOMAIN_ID            VARCHAR2(255)         := 'DOMAIN_ID';
  RDF_DOMAINS$DOMAIN_NAME          VARCHAR2(255)         := 'DOMAIN_NAME';

  RDF_DOMAINS_DEPT_SHEET           VARCHAR2(255)         := 'RDF_DOMAINS_BY_DEPT';
  RDF_DOMAINS_DEPT$ACTION          VARCHAR2(255)         := 'ACTION';
  RDF_DOMAINS_DEPT$GROUP_ID        VARCHAR2(255)         := 'GROUP_ID';
  RDF_DOMAINS_DEPT$GROUP_NAME      VARCHAR2(255)         := 'GROUP_NAME';
  RDF_DOMAINS_DEPT$DEPT            VARCHAR2(255)         := 'DEPT';
  RDF_DOMAINS_DEPT$DEPT_NAME       VARCHAR2(255)         := 'DEPT_NAME';
  RDF_DOMAINS_DEPT$DOMAIN_ID       VARCHAR2(255)         := 'DOMAIN_ID';
  RDF_DOMAINS_DEPT$LOAD_SLS_IND    VARCHAR2(255)         := 'LOAD_SALES_IND';

  RDF_DOMAINS_CLASS_SHEET          VARCHAR2(255)         := 'RDF_DOMAINS_BY_CLASS';
  RDF_DOMAINS_CLASS$ACTION         VARCHAR2(255)         := 'ACTION';
  RDF_DOMAINS_CLASS$GROUP_ID       VARCHAR2(255)         := 'GROUP_ID';
  RDF_DOMAINS_CLASS$GROUP_NAME     VARCHAR2(255)         := 'GROUP_NAME';
  RDF_DOMAINS_CLASS$DEPT           VARCHAR2(255)         := 'DEPT';
  RDF_DOMAINS_CLASS$DEPT_NAME      VARCHAR2(255)         := 'DEPT_NAME';
  RDF_DOMAINS_CLASS$CLASS          VARCHAR2(255)         := 'CLASS';
  RDF_DOMAINS_CLASS$CLASS_NAME     VARCHAR2(255)         := 'CLASS_NAME';
  RDF_DOMAINS_CLASS$DOMAIN_ID      VARCHAR2(255)         := 'DOMAIN_ID';
  RDF_DOMAINS_CLASS$LOAD_SLS_IND   VARCHAR2(255)         := 'LOAD_SALES_IND';
  
  RDF_DOMAINS_SC_SHEET             VARCHAR2(255)         := 'RDF_DOMAINS_BY_SUBCLASS';
  RDF_DOMAINS_SC$ACTION            VARCHAR2(255)         := 'ACTION';
  RDF_DOMAINS_SC$GROUP_ID          VARCHAR2(255)         := 'GROUP_ID';
  RDF_DOMAINS_SC$GROUP_NAME        VARCHAR2(255)         := 'GROUP_NAME';
  RDF_DOMAINS_SC$DEPT              VARCHAR2(255)         := 'DEPT';
  RDF_DOMAINS_SC$DEPT_NAME         VARCHAR2(255)         := 'DEPT_NAME';
  RDF_DOMAINS_SC$CLASS             VARCHAR2(255)         := 'CLASS';
  RDF_DOMAINS_SC$CLASS_NAME        VARCHAR2(255)         := 'CLASS_NAME';
  RDF_DOMAINS_SC$SUBCLASS          VARCHAR2(255)         := 'SUBCLASS';
  RDF_DOMAINS_SC$SUBCLASS_NAME     VARCHAR2(255)         := 'SUBCLASS_NAME';
  RDF_DOMAINS_SC$DOMAIN_ID         VARCHAR2(255)         := 'DOMAIN_ID';
  RDF_DOMAINS_SC$LOAD_SLS_IND      VARCHAR2(255)         := 'LOAD_SALES_IND';
  
  action_column                    VARCHAR2(255)         :='ACTION';
  template_category                CODE_DETAIL.CODE%TYPE :='RMSADM';
-------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind IN       CHAR DEFAULT 'N')
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
END XXADEO_MERCHHIER_RDF_DOM_SQL;
/
