set serveroutput on size unlimited
set escape on

declare
    param_1 varchar2(30):=sys.dbms_assert.schema_name(upper('&1'));
    param_2 varchar2(30):=sys.dbms_assert.schema_name(upper('&2'));
	
BEGIN
    
	execute immediate 'GRANT SELECT ON '||param_2||'.CFA_ATTRIB_GROUP_SET TO '||param_1||' WITH GRANT OPTION';

END;
/ 
  