/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Grants                                                       */
/******************************************************************************/
set serveroutput on size unlimited
set escape on

declare
  param_1 varchar2(30):=sys.dbms_assert.schema_name(upper('&2'));
  param_2 varchar2(30):=sys.dbms_assert.schema_name(upper('&1'));
begin
  execute immediate 'GRANT EXECUTE ON '||param_2||'.XXADEO_RMS_ASYNC_PROCESS_SQL TO '||param_1;
  execute immediate 'GRANT SELECT ON '||param_2||'.XXADEO_V_CFA_ATTRIB_GROUP_SET TO '||param_1;
end;
/   