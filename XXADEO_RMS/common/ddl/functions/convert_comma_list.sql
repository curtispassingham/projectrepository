create or replace function convert_comma_list(I_list in clob)
return list_type as
  --
  L_string       long        := I_list || ',';
  L_comma_index  pls_integer;
  L_index        pls_integer := 1;
  L_list_tbl     list_type   := list_type();
  --
begin
  --
  if instr(L_string, ',') > 0 then
    --
    L_string := replace(L_string,';',',');
    --
  end if;
  --
  loop
    --
    L_comma_index := instr(L_string, ',', l_index);
    exit when L_comma_index = 0;
    --
    L_list_tbl.extend();
    L_list_tbl(L_list_tbl.count) := trim(substr(l_string, L_index, l_comma_index - L_index));
    --
    L_index := L_comma_index + 1;
    --
  end loop;
  --
  return L_list_tbl;
  --
end convert_comma_list;
/