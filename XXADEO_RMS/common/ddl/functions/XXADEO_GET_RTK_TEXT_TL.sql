CREATE OR REPLACE FUNCTION XXADEO_GET_RTK_TEXT_TL (O_error_message IN OUT LOGGER_LOGS.TEXT%TYPE,
                                                   I_rtk_key       IN     RTK_ERRORS_TL.RTK_KEY%TYPE)

--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Function to get RTK_TEXT from RTK_ERRORS_TL table for RB129  */
/******************************************************************************/
--------------------------------------------------------------------------------
RETURN VARCHAR2 IS
  --
  L_program          VARCHAR2(64) := 'GET_RTK_TEXT_TL';
  --
  L_rtk_text         RTK_ERRORS_TL.RTK_TEXT%TYPE := I_rtk_key;
  --
  cursor C_get_rtk_text is
    select rtk_text
      from v_rtk_errors_tl
     where rtk_key = I_rtk_key
       and lang = language_sql.get_user_language();
  --
BEGIN
  -- get rtk message by rtk key and lang
  --
  open C_get_rtk_text;
  fetch C_get_rtk_text into L_rtk_text;
  close C_get_rtk_text;
  --
  RETURN L_rtk_text;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN L_rtk_text;
  --
END XXADEO_GET_RTK_TEXT_TL;
--------------
