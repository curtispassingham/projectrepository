  --------------------------------------------------------
  --  DDL for Function XXADEO_ITEM_LOC_WRP
  --------------------------------------------------------

  CREATE OR REPLACE FUNCTION "XXADEO_RMS"."XXADEO_ITEM_LOC_WRP"(O_ERROR_MESSAGE IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                                                I_ITEM          IN ITEM_LOC.ITEM%TYPE,
                                                                I_LOC           IN ITEM_LOC.LOC%TYPE,
                                                                I_LOC_TYPE      IN ITEM_LOC.LOC_TYPE%TYPE)
  RETURN BOOLEAN IS
  --
  L_PROGRAM VARCHAR2(100) := 'XXADEO_ITEM_LOC_WRP';
  --
  L_INPUT_REC              NEW_ITEM_LOC_SQL.NIL_INPUT_RECORD;
  L_INPUT                  NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;
  L_PRIMARY_SUPP           SUPS.SUPPLIER%TYPE;
  L_PRIMARY_CNTRY          COUNTRY.COUNTRY_ID%TYPE;
  L_STATUS                 VARCHAR2(1) := NULL;
  L_RANGED_IND             VARCHAR2(1);
  L_CFA_ATTRIB_TBL         XXADEO_CFA_ATTRIB_TBL := XXADEO_CFA_ATTRIB_TBL();
  L_XXADEO_CFA_KEY_VAL_TBL XXADEO_CFA_KEY_VAL_TBL := XXADEO_CFA_KEY_VAL_TBL();

  L_CHILDREN_IND  VARCHAR2(1);
  L_RANGE_WH      WH.WH%TYPE;
  L_SOURCE_METHOD VARCHAR2(1);
  L_DSD_IND       SUPS.DSD_IND%TYPE;
  --
BEGIN
  --
  -- get the item's primary supplier and primary country, which will be used as
  -- the new item_loc's primary supplier and primary country.
  --
  DBG_SQL.MSG(L_PROGRAM,
              'ITEM=' || TO_CHAR(I_ITEM) || ', LOC=' || I_LOC);

  IF NOT ITEM_SUPP_COUNTRY_SQL.GET_PRIM_SUPP_CNTRY(O_ERROR_MESSAGE,
                                                   L_PRIMARY_SUPP,
                                                   L_PRIMARY_CNTRY,
                                                   I_ITEM) THEN
    -- ERROR??
    RETURN FALSE;
    --
  END IF;
  ---
  DBG_SQL.MSG(L_PROGRAM,
              'PRIM SUP==' || L_PRIMARY_SUPP || ', PRIM SUPP COUNTRY=' ||
              L_PRIMARY_CNTRY);



  --Validate if the item is "Code 48";
  --If it's a Code 48 Item, should prevent ranging to cascade down to SKUs
  SELECT CASE
           WHEN COUNT(1) = 0 THEN
            'Y'
           ELSE
            'N'
         END
    INTO L_CHILDREN_IND
    FROM XXADEO_MOM_DVM DVM,
         UDA_ITEM_LOV   UIL
   WHERE DVM.PARAMETER = 'ITEM_SUBTYPE'
        --AND DVM.BU = ST.AREA
     AND DVM.VALUE_1 = UIL.UDA_ID
     AND UIL.ITEM = I_ITEM
     AND UIL.UDA_VALUE IN (SELECT CODE
                             FROM CODE_DETAIL
                            WHERE CODE_TYPE = 'RC48');



  --Validate if the Supplier is NOT DSD
  SELECT S.DSD_IND
    INTO L_DSD_IND
    FROM SUPS S
   WHERE S.SUPPLIER = L_PRIMARY_SUPP;


  --If NOT DSD Create WH Range
  --The rule to choose WH follows the same applier in RMS
  IF L_DSD_IND = 'N' THEN
    SELECT X.WH,
           'W'
      INTO L_RANGE_WH,
           L_SOURCE_METHOD
      FROM (SELECT CASE
                     WHEN WH.WH = ST.DEFAULT_WH THEN
                      1 -- Store default wh if exist.
                     WHEN (WH.CHANNEL_ID = ST.CHANNEL_ID AND
                          WH.TSF_ENTITY_ID = ST.TSF_ENTITY_ID) THEN
                      2 -- Same entity and channel
                     WHEN (WH.CHANNEL_ID = ST.CHANNEL_ID AND WH.ORG_UNIT_ID = ST.ORG_UNIT_ID) THEN
                      3 -- Same Org Unit and Channel
                     WHEN WH.CHANNEL_ID = ST.CHANNEL_ID THEN
                      4 -- Same Channel
                     ELSE
                      5
                   END PRIORITY,
                   WH.*
              FROM WH    WH,
                   STORE ST
             WHERE ST.STORE = I_LOC
               AND ((WH.CHANNEL_ID = ST.CHANNEL_ID) OR
                   (WH.ORG_UNIT_ID = ST.ORG_UNIT_ID) OR
                   (WH.TSF_ENTITY_ID = ST.TSF_ENTITY_ID))) X
     ORDER BY PRIORITY ASC FETCH FIRST ROW ONLY;
  ELSE
    L_RANGE_WH      := NULL;
    L_SOURCE_METHOD := NULL;
  END IF;

  ---
  -- Create item_loc
  ---
  L_INPUT_REC.ITEM                    := I_ITEM;
  L_INPUT_REC.ITEM_PARENT             := NULL;
  L_INPUT_REC.ITEM_GRANDPARENT        := NULL;
  L_INPUT_REC.ITEM_DESC               := NULL;
  L_INPUT_REC.ITEM_SHORT_DESC         := NULL;
  L_INPUT_REC.DEPT                    := NULL;
  L_INPUT_REC.ITEM_CLASS              := NULL;
  L_INPUT_REC.SUBCLASS                := NULL;
  L_INPUT_REC.ITEM_LEVEL              := NULL;
  L_INPUT_REC.TRAN_LEVEL              := NULL;
  L_INPUT_REC.ITEM_STATUS             := NULL;
  L_INPUT_REC.WASTE_TYPE              := NULL;
  L_INPUT_REC.SELLABLE_IND            := NULL;
  L_INPUT_REC.ORDERABLE_IND           := NULL;
  L_INPUT_REC.PACK_IND                := NULL;
  L_INPUT_REC.PACK_TYPE               := NULL;
  L_INPUT_REC.DIFF_1                  := NULL;
  L_INPUT_REC.DIFF_2                  := NULL;
  L_INPUT_REC.DIFF_3                  := NULL;
  L_INPUT_REC.DIFF_4                  := NULL;
  L_INPUT_REC.LOC                     := I_LOC;
  L_INPUT_REC.LOC_TYPE                := I_LOC_TYPE;
  L_INPUT_REC.DAILY_WASTE_PCT         := NULL;
  L_INPUT_REC.UNIT_COST_LOC           := NULL;
  L_INPUT_REC.UNIT_RETAIL_LOC         := NULL;
  L_INPUT_REC.SELLING_RETAIL_LOC      := NULL;
  L_INPUT_REC.SELLING_UOM             := NULL;
  L_INPUT_REC.MULTI_UNITS             := NULL;
  L_INPUT_REC.MULTI_UNIT_RETAIL       := NULL;
  L_INPUT_REC.MULTI_SELLING_UOM       := NULL;
  L_INPUT_REC.ITEM_LOC_STATUS         := 'A';
  L_INPUT_REC.TAXABLE_IND             := NULL;
  L_INPUT_REC.TI                      := NULL;
  L_INPUT_REC.HI                      := NULL;
  L_INPUT_REC.STORE_ORD_MULT          := NULL;
  L_INPUT_REC.MEAS_OF_EACH            := NULL;
  L_INPUT_REC.MEAS_OF_PRICE           := NULL;
  L_INPUT_REC.UOM_OF_PRICE            := NULL;
  L_INPUT_REC.PRIMARY_VARIANT         := NULL;
  L_INPUT_REC.PRIMARY_SUPP            := L_PRIMARY_SUPP;
  L_INPUT_REC.PRIMARY_CNTRY           := L_PRIMARY_CNTRY;
  L_INPUT_REC.LOCAL_ITEM_DESC         := NULL;
  L_INPUT_REC.LOCAL_SHORT_DESC        := NULL;
  L_INPUT_REC.PRIMARY_COST_PACK       := NULL;
  L_INPUT_REC.RECEIVE_AS_TYPE         := NULL;
  L_INPUT_REC.STORE_PRICE_IND         := NULL;
  L_INPUT_REC.UIN_TYPE                := NULL;
  L_INPUT_REC.UIN_LABEL               := NULL;
  L_INPUT_REC.CAPTURE_TIME            := NULL;
  L_INPUT_REC.EXT_UIN_IND             := NULL;
  L_INPUT_REC.SOURCE_METHOD           := L_SOURCE_METHOD;
  L_INPUT_REC.SOURCE_WH               := L_RANGE_WH; --VALIDATE
  L_INPUT_REC.INBOUND_HANDLING_DAYS   := NULL;
  L_INPUT_REC.CURRENCY_CODE           := NULL;
  L_INPUT_REC.LIKE_STORE              := NULL;
  L_INPUT_REC.DEFAULT_TO_CHILDREN_IND := L_CHILDREN_IND;
  L_INPUT_REC.CLASS_VAT_IND           := NULL;
  L_INPUT_REC.HIER_LEVEL              := I_LOC_TYPE;
  L_INPUT_REC.HIER_NUM_VALUE          := I_LOC;
  L_INPUT_REC.HIER_CHAR_VALUE         := NULL;
  L_INPUT_REC.RANGED_IND              := 'N';
  L_INPUT_REC.COSTING_LOC             := NULL;
  L_INPUT_REC.COSTING_LOC_TYPE        := NULL;
  ---
  L_INPUT(0) := L_INPUT_REC;
  ---
  DBG_SQL.MSG(L_PROGRAM,
              'PRE: NEW_ITEM_LOC_SQL.NEW_ITEM_LOC');
  ---
  O_ERROR_MESSAGE := NULL;
  ---
  IF NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_ERROR_MESSAGE,
                                   L_INPUT) = FALSE THEN
    --
    DBG_SQL.MSG(L_PROGRAM,
                'RANGING Error: ' || O_ERROR_MESSAGE);
    RETURN FALSE;
    --
  END IF;
  ---
  DBG_SQL.MSG(L_PROGRAM,
              'After NEW_ITEM_LOC');
  ---
  RETURN TRUE;
  --
EXCEPTION
  --
  WHEN OTHERS THEN
    --
    O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_PROGRAM,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;
    --
END XXADEO_ITEM_LOC_WRP;

/
