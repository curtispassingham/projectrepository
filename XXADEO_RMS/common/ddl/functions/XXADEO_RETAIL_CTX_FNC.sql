----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Function Updated:  XXADEO_RETAIL_CTX_FNC
----------------------------------------------------------------------------
--
whenever sqlerror exit  
--
--------------------------------------
--       UPDATING VIEW               
--------------------------------------
CREATE OR REPLACE FUNCTION XXADEO_RETAIL_CTX_FNC (I_attribute_name  IN VARCHAR2,
                                                     I_attribute_value IN VARCHAR2)
RETURN BOOLEAN AS
BEGIN
   RETAIL_CTX_PKG.SET_APP_CTX (I_attribute_name, upper(I_attribute_value));
   return TRUE;
EXCEPTION
   when OTHERS then
      RETURN FALSE;
END;
/