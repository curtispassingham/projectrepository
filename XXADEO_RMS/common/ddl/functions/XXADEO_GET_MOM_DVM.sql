CREATE OR REPLACE FUNCTION XXADEO_GET_MOM_DVM(I_func_area       IN     XXADEO_MOM_DVM.FUNC_AREA%TYPE,
                                              I_parameter       IN     XXADEO_MOM_DVM.PARAMETER%TYPE,
                                              I_bu              IN     XXADEO_MOM_DVM.BU%TYPE DEFAULT NULL,
                                              I_value_1         IN     XXADEO_MOM_DVM.VALUE_1%TYPE DEFAULT NULL,
                                              I_value_2         IN     XXADEO_MOM_DVM.VALUE_2%TYPE DEFAULT NULL)
RETURN XXADEO_MOM_DVM_TBL IS
  --
  L_program        VARCHAR2(64) := 'XXADEO_GET_MOM_DVM';
  --
  NO_CONFIG        EXCEPTION;
  L_error_message  LOGGER_LOGS.TEXT%TYPE;
  --
  L_mom_dvm_values XXADEO_MOM_DVM_TBL;
  --
  cursor C_get_mom_dvm is
    select new XXADEO_MOM_DVM_OBJ(xmd.func_area,
                                  xmd.parameter,
                                  xmd.bu,
                                  xmd.value_1,
                                  xmd.value_2,
                                  xmd.description)
      from xxadeo_mom_dvm xmd
     where xmd.func_area  = I_func_area
       and xmd.parameter  = I_parameter
       and nvl(xmd.bu,-999)         = nvl(I_bu, nvl(xmd.bu, -999))
       and nvl(xmd.value_1,-999)    = nvl(I_value_1, nvl(xmd.value_1, -999))
       and nvl(xmd.value_2,-999)    = nvl(I_value_2, nvl(xmd.value_2, -999));
  --
  --
BEGIN
  -- get values of the xxadeo_mom_ddm table
  --
  open C_get_mom_dvm;
  fetch C_get_mom_dvm bulk collect into L_mom_dvm_values;
  close C_get_mom_dvm;
  --
  if L_mom_dvm_values.count = 0 then
    --
    RAISE NO_CONFIG;
    --
  end if;
  --
  RETURN L_mom_dvm_values;
  --
EXCEPTION
  --
  when NO_CONFIG then
    --
    L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          'FUNC_ARE: '||I_func_area||' and PARAMETER: '||I_parameter ||' and BU: '||I_bu||' combination does not exist configured on the table XXADEO_MOM_DVM',
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN L_mom_dvm_values;
  when OTHERS then
    --
    if C_get_mom_dvm%ISOPEN then
      --
      close C_get_mom_dvm;
      --
    end if;
    --
    L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    --
    RETURN L_mom_dvm_values;
  --
END XXADEO_GET_MOM_DVM;
--------------------------------------------------------------------------------
/
