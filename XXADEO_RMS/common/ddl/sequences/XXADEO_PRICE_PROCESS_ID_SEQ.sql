/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Sequence "XXADEO_PRICE_PROCESS_ID_SEQ" for RB34a             */
/******************************************************************************/
begin
  execute immediate 'drop sequence XXADEO_PRICE_PROCESS_ID_SEQ';
exception
  when others then
    null;
end;
/
--
create sequence XXADEO_PRICE_PROCESS_ID_SEQ
minvalue 1
maxvalue 999999999999999
start with 1
increment by 1
nocache
order;