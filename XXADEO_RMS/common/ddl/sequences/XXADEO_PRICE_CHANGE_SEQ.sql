/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Sequence "XXADEO_PRICE_CHANGE_SEQ" for RB34a                 */
/******************************************************************************/
begin
  execute immediate 'drop sequence XXADEO_PRICE_CHANGE_SEQ';
exception
  when others then
    null;
end;
/
--
create sequence XXADEO_PRICE_CHANGE_SEQ
minvalue 1
maxvalue 999999999999999
start with 27561
increment by 1
nocache
cycle
order;