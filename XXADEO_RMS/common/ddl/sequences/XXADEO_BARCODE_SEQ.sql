/*---------------------------------------------------------------------*/
/*
* Object Name:   XXADEO_BARCODE_SEQ.sql
* Description:   Sequence to generate Barcodes identifier
* Version:       1.0
* Author:        Liliana Soraia Ferreira
* Creation Date: 23/05/2018
* Last Modified: 23/05/2018
* History:
*               1.0 - Initial version
*/
/*------------------------------------------------------------------------*/
drop sequence XXADEO_BARCODE_SEQ;

create sequence XXADEO_BARCODE_SEQ
minvalue 1
maxvalue 999999999
start with 1
increment by 1
cache 2;
/