/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Sequence "XXADEO_PROCESS_ORDER_SEQ" for RB34a                */
/******************************************************************************/
begin
  execute immediate 'drop sequence XXADEO_PROCESS_ORDER_SEQ';
exception
  when others then
    null;
end;
/
--
create sequence XXADEO_PROCESS_ORDER_SEQ
minvalue 1
maxvalue 999999999999999
start with 1
increment by 1
nocache
cycle
order;