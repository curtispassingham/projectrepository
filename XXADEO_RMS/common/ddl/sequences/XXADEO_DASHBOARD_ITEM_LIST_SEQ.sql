
/******************************************************************************/
/* CREATE DATE - Sep 2018                                                     */
/* CREATE USER - Tiago Torres                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Sequence "XXADEO_DASHBOARD_ITEM_LIST_SEQ"                    */
/******************************************************************************/

begin
  execute immediate 'drop sequence XXADEO_DASHBOARD_ITEM_LIST_SEQ';
exception
  when others then
    null;
end;
/
--
create sequence XXADEO_DASHBOARD_ITEM_LIST_SEQ
minvalue 1
maxvalue 999999999999999
start with 1
increment by 1
nocache
cycle
order;
