
/******************************************************************************/
/* CREATE DATE - Jul 2018                                                     */
/* CREATE USER - Jorge Agra                                                   */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Sequence "XXADEO_LFCY_CFG_PK_SEQ"                            */
/******************************************************************************/

begin
  execute immediate 'CREATE SEQUENCE  "XXADEO_LFCY_CFG_PK_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOPARTITION';
exception
  when others then
    null;
end;
/

