/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DDL of "XXADEO_UPL_PROCESS_ID_SEQ" table for RB115           */
/******************************************************************************/

begin
  execute immediate 'drop sequence XXADEO_UPL_PROCESS_ID_SEQ';
exception
  when others then
    null;
end;
/

-- Create sequence 
create sequence XXADEO_UPL_PROCESS_ID_SEQ
minvalue 1
maxvalue 999999999999999
start with 1
increment by 1
nocache
order;