--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of sec_user                                              */
/******************************************************************************/
begin
  update sec_user 
     set application_user_id = 'XXADEO_RMS'
   where database_user_id = 'XXADEO_RMS';
   --
   if SQL%ROWCOUNT = 0 then
     --
     insert into sec_user
       (USER_SEQ,
        DATABASE_USER_ID,
        APPLICATION_USER_ID,
        CREATE_ID,
        CREATE_DATETIME,
        RMS_USER_IND,
        RESA_USER_IND,
        REIM_USER_IND,
        ALLOCATION_USER_IND)
       (select sec_user_sequence.NEXTVAL,
               'XXADEO_RMS',
               'XXADEO_RMS',
               user,
               SYSDATE,
               'Y',
               'N',
               'N',
               'N'
          from dual);  
     --  
   end if;
   --
end;
/
begin
  update sec_user 
     set application_user_id = 'RMS'
   where database_user_id = 'RMS';
   --
   if SQL%ROWCOUNT = 0 then
     --
     insert into sec_user
       (USER_SEQ,
        DATABASE_USER_ID,
        APPLICATION_USER_ID,
        CREATE_ID,
        CREATE_DATETIME,
        RMS_USER_IND,
        RESA_USER_IND,
        REIM_USER_IND,
        ALLOCATION_USER_IND)
       (select sec_user_sequence.NEXTVAL,
               'RMS',
               'RMS',
               user,
               SYSDATE,
               'Y',
               'N',
               'N',
               'N'
          from dual);  
     --  
   end if;
   --
end;
/
