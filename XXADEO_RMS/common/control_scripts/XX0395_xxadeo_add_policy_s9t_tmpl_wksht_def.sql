SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--  
DECLARE
   L_policy_count   NUMBER := 0;
   --
   cursor C_DBA_POLICIES is
      select count(1)
        from dba_policies
       where object_owner = 'RMS'
         and package      = 'FILTER_POLICY_SQL'
		 and object_name in ('V_S9T_TMPL_WKSHT_DEF')
		 and policy_name in ('V_S9T_TMPL_WKSHT_DEF_S');
   --
   cursor C_GET_DBA_POLICY is
      select UPPER(policy_name) policy_name
        from dba_policies
       where object_owner = 'RMS'
         and package      = 'FILTER_POLICY_SQL'
		 and object_name in ('V_S9T_TMPL_WKSHT_DEF')
		 and policy_name in ('V_S9T_TMPL_WKSHT_DEF_S');
BEGIN
   open C_DBA_POLICIES;
   fetch C_DBA_POLICIES into L_policy_count;
   close C_DBA_POLICIES;
   --
   if L_policy_count > 0 then
      FOR rec in C_GET_DBA_POLICY LOOP
         /* Organizational Hierarchy Filtering Policies */
         if rec.policy_name = 'V_S9T_TMPL_WKSHT_DEF_S' then
            DBMS_RLS.DROP_POLICY('RMS', 'V_S9T_TMPL_WKSHT_DEF', 'V_S9T_TMPL_WKSHT_DEF_S');
         end if;
      END LOOP;
   end if;
   --
   dbms_rls.add_policy('RMS', 'V_S9T_TMPL_WKSHT_DEF','V_S9T_TMPL_WKSHT_DEF_S','XXADEO_RMS', 'filter_policy_sql.V_S9T_TMPL_WKSHT_DEF_S','select', FALSE, TRUE, FALSE, 5, TRUE,NULL,NULL);
   --
END;
/   