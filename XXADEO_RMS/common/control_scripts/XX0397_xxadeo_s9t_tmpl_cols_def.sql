--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of s9t_tmpl_cols_def for RB116                           */
/******************************************************************************/
--------------------------------------------------------------------------------
begin
  --
  for i in (select ext.base_rms_table base_rms_table
              from cfa_ext_entity ext 
             where ext.base_rms_table in ('ITEM_MASTER','ITEM_SUPPLIER','ITEM_SUPP_COUNTRY')) loop
    --
    for rec in (select 'XXADEO_ITEM_MASTER' template_key,
                       ca.attrib_id attrib_id, 
                       cags.group_set_view_name wksht_key,
                       ca.view_col_name column_key,
                       regexp_replace(cal.label,'\s*\([^()]*\)', '') column_name,
                       ca.active_ind mandatory,
                       user create_id,
                       sysdate create_datetime, 
                       user last_update_id,
                       sysdate last_update_datetime,
                       null default_value, 
                       'N' required_visual_ind
                  from cfa_attrib           ca,
                       cfa_attrib_labels    cal,
                       cfa_attrib_group     cag,
                       cfa_attrib_group_set cags,
                       cfa_ext_entity       cee
                 where cee.base_rms_table = i.base_rms_table
                   and cee.ext_entity_id  = cags.ext_entity_id
                   and cag.group_set_id   = cags.group_set_id
                   and ca.group_id        = cag.group_id
                   and ca.attrib_id       = cal.attrib_id
                   and cal.lang           = 1
                 group by cags.group_set_view_name,
                          ca.view_col_name,
                          cal.label,
                          cags.display_seq,
                          ca.display_seq,
                          ca.attrib_id,
                          ca.active_ind
                 order by cags.display_seq asc, ca.display_seq asc) loop
      --
      merge into s9t_tmpl_cols_def target
      using (select rec.template_key template_key,
                    rec.wksht_key wksht_key,
                    rec.column_key column_key,
                    rec.column_name column_name,
                    rec.mandatory mandatory,
                    rec.create_id create_id,
                    rec.create_datetime create_datetime,
                    rec.last_update_id last_update_id,
                    rec.last_update_datetime last_update_datetime,
                    rec.default_value default_value,
                    rec.required_visual_ind required_visual_ind
               from dual) source
      on (target.template_key = source.template_key and 
          target.wksht_key    = source.wksht_key and 
          target.column_key   = source.column_key)
      when matched then
        update
           set target.column_name          = source.column_name,
               target.mandatory            = source.mandatory,
               target.last_update_id       = source.last_update_id,
               target.last_update_datetime = source.last_update_datetime,
               target.default_value        = source.default_value,
               target.required_visual_ind  = source.required_visual_ind
      when not matched then
        insert
          (template_key,
           wksht_key,
           column_key,
           column_name,
           mandatory,
           create_id,
           create_datetime,
           last_update_id,
           last_update_datetime,
           default_value,
           required_visual_ind)
        values
          (rec.template_key,
           rec.wksht_key,
           rec.column_key,
           rec.column_name,
           rec.mandatory,
           rec.create_id,
           rec.create_datetime,
           rec.last_update_id,
           rec.last_update_datetime,
           rec.default_value,
           rec.required_visual_ind);
      --
      merge into s9t_tmpl_cols_def_tl target
      using (select rec.template_key template_key,
                    rec.wksht_key wksht_key,
                    rec.column_key column_key,
                    cal.lang lang,
                    regexp_replace(cal.label,'\s*\([^()]*\)', '') column_name
               from cfa_attrib ca, cfa_attrib_labels cal
              where ca.attrib_id = rec.attrib_id
                and ca.attrib_id = cal.attrib_id) source
      on (source.template_key = target.template_key and 
          source.wksht_key    = target.wksht_key and 
          source.column_key   = target.column_key and 
          source.lang         = target.lang)
      when matched then
        update set target.column_name = source.column_name
      when not matched then
        insert
          (template_key, 
           wksht_key, 
           column_key, 
           lang, 
           column_name)
        values
          (source.template_key,
           source.wksht_key,
           source.column_key,
           source.lang,
           source.column_name);
      --
    end loop;
    --
  end loop;
  --
  update s9t_tmpl_cols_def_tl 
     set column_name  = 'Date souhaité actif commerce LMCY'
   where template_key = 'XXADEO_ITEM_MASTER' 
     and wksht_key    = 'V_ITEM_AGS8'
     and column_key   = 'LMCY_ACTIF_DDATE_ITEM'
     and lang         = 3;
  --  
end;
/




