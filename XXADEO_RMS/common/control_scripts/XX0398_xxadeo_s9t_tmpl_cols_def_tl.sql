--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of s9t_tmpl_cols_def_tl for RB116                        */
/******************************************************************************/
--------------------------------------------------------------------------------
begin
  for i in (select *
              from s9t_tmpl_cols_def target
             where wksht_key like 'V\_%' ESCAPE'\'
               and column_key in ('ITEM', 'SUPPLIER')) loop
    --
    insert into s9t_tmpl_cols_def_tl
      (select 'XXADEO_ITEM_MASTER',
              i.wksht_key,
              column_key,
              lang,
              column_name
         from s9t_tmpl_cols_def_tl tl
        where template_key = 'XXADEO_ITEM_MASTER'
          and column_key = i.column_key
		  and not exists (select 1 
			                from s9t_tmpl_cols_def_tl source
				           where source.template_key = 'XXADEO_ITEM_MASTER'
						     and source.wksht_key    = tl.wksht_key
					         and source.column_key   = tl.column_key
							 and source.lang 		 = tl.lang)
        group by column_key, lang, column_name);
    --
  end loop;
end;
/