--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - September 2018                                               */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of s9t_tmpl_wksht_def for RB116                          */
/******************************************************************************/
--------------------------------------------------------------------------------
declare 
  --
  L_seq_no NUMBER(20) := 0;
  L_seq_no_nextval NUMBER(20) := 0;
  L_seq_no_curval  NUMBER(20) := 1;
  --
  cursor C_get_seq_no is
    select max(seq_no)
      from s9t_tmpl_wksht_def 
     where template_key = 'XXADEO_ITEM_MASTER';
  --
begin
  --
  for i in (select ext.base_rms_table base_rms_table
              from cfa_ext_entity ext 
             where ext.base_rms_table in ('ITEM_MASTER','ITEM_SUPPLIER','ITEM_SUPP_COUNTRY')) loop
    --
    for rec in (select 'XXADEO_ITEM_MASTER' template_key,
                       cags.group_set_view_name wksht_key,
                       replace(cagsl.label,' ','_') wksht_name,
                       cags.active_ind mandatory,
                       cags.group_set_id group_set_id
                  from cfa_attrib                  ca,
                       cfa_attrib_group            cag,
                       cfa_attrib_group_set        cags,
                       cfa_attrib_group_set_labels cagsl,
                       cfa_ext_entity              cee
                 where cee.base_rms_table = i.base_rms_table
                   and cee.ext_entity_id  = cags.ext_entity_id
                   and cag.group_set_id   = cags.group_set_id
                   and cag.group_set_id   = cagsl.group_set_id
                   and cagsl.lang         = 1
                   and ca.group_id        = cag.group_id
                 group by cags.group_set_view_name,cagsl.label, cags.active_ind,cags.group_set_id, cags.display_seq
                 order by cags.display_seq asc) loop
      --
      L_seq_no := 0;
      --
      open C_get_seq_no;
      fetch C_get_seq_no into L_seq_no;
      close C_get_seq_no; 
      --
      L_seq_no_nextval := L_seq_no + SQL%ROWCOUNT;
      --
      dbms_output.put_line('L_seq_no :'||L_seq_no);
      dbms_output.put_line('SQL%ROWCOUNT :'||SQL%ROWCOUNT);
      dbms_output.put_line('L_seq_no_nextval :'||L_seq_no_nextval);
      --
      merge into s9t_tmpl_wksht_def target
      using (select rec.template_key template_key,
                    rec.wksht_key wksht_key,
                    rec.wksht_name wksht_name,
                    rec.mandatory mandatory
               from dual) source
      on (target.template_key = source.template_key and 
          target.wksht_key    = source.wksht_key)
      when matched then
        update
           set target.wksht_name = source.wksht_name,
               target.mandatory  = source.mandatory
      when not matched then
        insert
          (template_key, 
           wksht_key, 
           wksht_name, 
           mandatory, 
           seq_no)
        values
          (source.template_key,
           source.wksht_key,
           source.wksht_name,
           source.mandatory,
           L_seq_no_nextval);
      --
      merge into s9t_tmpl_wksht_def_tl target
      using (select rec.template_key template_key, 
                    rec.wksht_key wksht_key, 
                    cagsl.lang lang, 
                    replace(cagsl.label,' ','_') wksht_name
               from cfa_attrib_group_set        ca,
                    cfa_attrib_group_set_labels cagsl
              where ca.group_set_id = rec.group_set_id
                and ca.group_set_id = cagsl.group_set_id) source
      on (source.template_key = target.template_key and 
          source.wksht_key    = target.wksht_key and 
          source.lang         = target.lang)
      when matched then
        update set target.wksht_name = source.wksht_name
      when not matched then
        insert
          (template_key, 
           wksht_key, 
           lang, 
           wksht_name)
        values
          (source.template_key,
           source.wksht_key,
           source.lang,
           source.wksht_name);
      --
    end loop;
    --
  end loop;
end;
/ 
