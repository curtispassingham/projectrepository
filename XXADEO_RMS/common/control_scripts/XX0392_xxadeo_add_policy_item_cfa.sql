SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
   
DECLARE
   L_policy_count   NUMBER := 0;
   
   cursor C_DBA_POLICIES is
      select count(1)
        from dba_policies
       where object_owner = 'XXADEO_RMS'
         and package      = 'FILTER_POLICY_SQL'
		 and object_name in ('V_ITEM_MASTER','CFA_ATTRIB_GROUP_SET')
		 and policy_name in ('XXADEO_V_ITEM_MASTER_S','V_CFA_S');

   cursor C_GET_DBA_POLICY is
      select UPPER(policy_name) policy_name
        from dba_policies
       where object_owner = 'XXADEO_RMS'
         and package      = 'FILTER_POLICY_SQL'
		 and object_name in ('V_ITEM_MASTER','CFA_ATTRIB_GROUP_SET')
		 and policy_name in ('XXADEO_V_ITEM_MASTER_S','V_CFA_S');
BEGIN
   open C_DBA_POLICIES;
   fetch C_DBA_POLICIES into L_policy_count;
   close C_DBA_POLICIES;

   if L_policy_count > 0 then
      FOR rec in C_GET_DBA_POLICY LOOP
         /* Organizational Hierarchy Filtering Policies */
         if rec.policy_name = 'V_CFA_S' then
            DBMS_RLS.DROP_POLICY(user, 'CFA_ATTRIB_GROUP_SET', 'V_CFA_S');
         elsif rec.policy_name = 'XXADEO_V_ITEM_MASTER_S' then
            DBMS_RLS.DROP_POLICY(USER, 'v_item_master', 'xxadeo_v_item_master_s');
         end if;
      END LOOP;
   end if;
   --
   dbms_rls.add_policy(user, 'CFA_ATTRIB_GROUP_SET','V_CFA_S',user, 'filter_policy_sql.V_CFA_S','select', FALSE, TRUE, FALSE, 5, TRUE,NULL,NULL);
   dbms_rls.add_policy('RMS', 'v_item_master', 'xxadeo_v_item_master_s', 'XXADEO_RMS', 'filter_policy_sql.xxadeo_v_item_master_s', 'select', FALSE, TRUE);
   --
END;
/