#!/bin/ksh

#########################################################################
#                                                                       #
# Object Name:   xxadeo_rmse_catman_article_photo.ksh                   #
# Description:   The program extracts the article photo from            #
#                RMS tables and places this data into a flat file       #
#                (rmse_catman_article_photo.csv.rpl)                    #
#                to be loaded into RPAS.                                #
#                                                                       #
# Version:       1.4                                                    #
# Author:        CGI                                                    #
# Creation Date: 28/06/2018                                             #
# Last Modified: 10/12/2018                                             #
# History:                                                              #
#               1.0 - Initial version                                   #
#               1.1 - Update the variables for environment, librairies  #
#                     and directory names. We have to use $RETL_OUT     #
#                     to put output files because this IR is            #
#                     an extraction (RMS side)                          #
#               1.2 - Update the program, the output schema and the     #
#                     output file names. Update the RETL to join        #
#                     ITEM_MASTER and ITEM_IMAGE, and adding filters.   #
#               1.3 - Correct the issue with concatenation of LIEN_PHOTO#
#               1.4 - Update output file name (.ovr by .rpl)            #
#                                                                       #
#########################################################################

########################### PROGRAM DEFINE ##############################
###################     (must be the first define)     ##################

export PROGRAM_NAME="xxadeo_rmse_catman_article_photo"

############################### INCLUDES ################################
############## (this section must come after PROGRAM DEFINE) ############

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh


#########################################################################
#  Log start message and define the RETL flow file                      #
#########################################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

##########################  OUTPUT DEFINES ##############################
################ (this section must come after INCLUDES) ################

O_DATA_DIR_XXADEO=$RETL_OUT/CATMAN
export OUTPUT_FILE=${O_DATA_DIR_XXADEO}/rmse_catman_article_photo.csv.rpl

SCHEMA_DIR_XXADEO=${RETLforXXADEO}/rfx/schema
export OUTPUT_SCHEMA=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema


#########################################################################
#  Copy the RETL flow to a file to be executed by rfx:
#########################################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read the wh data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
                        SELECT iti.item ARTICLE,
                                '"' || listagg('<image id=""'|| iti.display_priority ||
                                '"" label=""' || iti.image_desc || '""><url size=""thumb"">'||iti.image_addr||iti.image_name||'</url><url size=""full"">'||iti.image_addr||iti.image_name||'</url></image>','')
                        WITHIN GROUP (ORDER BY iti.display_priority) || '"' LIEN_PHOTO
                        FROM item_image iti, item_master itm
                        WHERE iti.item=itm.item
                        AND itm.item_level=1
                        AND itm.status='A'
                        GROUP BY iti.item
         ]]>
      </PROPERTY>
      <OUTPUT   name = "item_photo_data.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
     <INPUT name="item_photo_data.v"/>
     <PROPERTY name="convertspec">
       <![CDATA[
         <CONVERTSPECS>
           <CONVERT destfield="LIEN_PHOTO" sourcefield="LIEN_PHOTO">
           <CONVERTFUNCTION name="make_not_nullable">
             <FUNCTIONARG name="nullvalue" value="-1"/>
           </CONVERTFUNCTION>
           </CONVERT>
         </CONVERTSPECS>
       ]]>
     </PROPERTY>
     <OUTPUT name="item_photo_data_1.v"/>
   </OPERATOR>


   <!--  Write out the wh data to a flat file:  -->
   <OPERATOR type="export">
      <INPUT    name="item_photo_data_1.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

#########################################################################
#  Execute the RETL flow that had previously
#  been copied into xxadeo_rmse_catman_article_photo.xml:
#########################################################################


${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
checkerror -e $? -m "Program failed - check $ERR_FILE"


##########################################################################
#  Log the number of records written to the final output files and add
#  up the total:
##########################################################################

log_num_recs ${OUTPUT_FILE}

#  Remove the status file:
if [ -f  $STATUS_FILE ]; then rm $STATUS_FILE; fi

message "Program completed successfully"

#  Clean up and exit:

rmse_terminate 0
