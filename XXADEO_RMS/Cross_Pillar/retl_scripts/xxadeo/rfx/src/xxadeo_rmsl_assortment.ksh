#!/bin/ksh

#################################################################################
#                                                                               #
# Object Name:  xxadeo_rmsl_assortment.ksh                                      #
# Description:  The xxadeo_rmsl_assortment.ksh program import data of 5 files   #
#                   XXRMS208_ASSORTMENT_ITEMBU_ONOF_*.dat                       #
#                   XXRMS208_ASSORTMENT_ITEMBU_AM_*.dat                         #
#                   XXRMS208_ASSORTMENT_ITEMBU_RS_*.dat                         #
#                   XXRMS208_ASSORTMENT_REG_ITEMLOC_*.dat                       #
#                   XXRMS208_ASSORTMENT_PROMO_ITEMLOC_*.dat                     #
#               into RMS Tables.                                                #
# Version:      1.1                                                             #
# Author:       CGI                                                             #
# Creation Date:11/09/2018                                                      #
# Last Update:  05/10/2018                                                      #
# History:                                                                      #
#               1.0 - Initial Version                                           #
#               1.1 - Update the environment variables, libraries and directory #
#               paths.                                                          #
#                                                                               #
#################################################################################

#################################################################################
#  This script extract assortment data from CATMAN files to RMS staging tables
#
#################################################################################

#################################################################################
#  PROGRAM NAME DEFINE
#################################################################################

PROGRAM_NAME='xxadeo_rmsl_assortment'
I_DATA_DIR_XXADEO=${RETL_IN}/MOM
SCHEMA_DIR_XXADEO=${RETLforXXADEO}/rfx/schema
ETC_DIR=${RETLforXXADEO}/rfx/etc


#################################################################################
#  INCLUDES
#################################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh


#################################################################################
#  INPUT/OUTPUT DEFINES
#################################################################################

export I_ITEMBU_ONOF_PATTERN="XXRMS208_ASSORTMENT_ITEMBU_ONOF_*.dat"
export I_ITEMBU_ONOF_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmsl_assortment_itembu_onof.schema

export I_ITEMBU_AM_PATTERN="XXRMS208_ASSORTMENT_ITEMBU_AM_*.dat"
export I_ITEMBU_AM_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmsl_assortment_itembu_am.schema

export I_ITEMBU_RS_PATTERN="XXRMS208_ASSORTMENT_ITEMBU_RS_*.dat"
export I_ITEMBU_RS_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmsl_assortment_itembu_rs.schema

export I_ITEMLOC_PATTERN="XXRMS208_ASSORTMENT_REG_ITEMLOC_*.dat"
export I_ITEMLOC_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmsl_assortment_reg_itemloc.schema

export I_ITEMLOC_PROMO_PATTERN="XXRMS208_ASSORTMENT_PROMO_ITEMLOC_*.dat"
export I_ITEMLOC_PROMO_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmsl_assortment_promo_itemloc.schema


#Control variables
export PROCESS_ONOFF="Y"
export PROCESS_AM="Y"
export PROCESS_RS="Y"
export PROCESS_REG="Y"
export PROCESS_PROMO="Y"

#################################################################################
#  FUNCTIONS
#################################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
                rmse_terminate $1
        fi
}


###############################################################################
# Interface Configuration file
###############################################################################

CONFIG_FILE=${ETC_DIR}/${PROGRAM_NAME}.txt
# Evaluate if the interface config file do not exist
if [ ! -r  ${CONFIG_FILE} ]; then
       message "Config file ${CONFIG_FILE} missing!!"
       exit 1
fi

# Get promotion assortment parameter
PROMO_ASSORT_KEY_CONFIG_LINE=$(cat ${CONFIG_FILE} | grep -v "^[[:blank:]]*#" | grep "PROMO_ASSORT_LOAD")
PROMO_ASSORT_KEY=$(echo ${PROMO_ASSORT_KEY_CONFIG_LINE} | cut -d"|" -f3 | tr -d '[[:blank:]]')

# Evaluate if the key was retrieved successfully
if [ -z ${PROMO_ASSORT_KEY} ]; then
       message "ERROR: Interface config file not correctly set. Please check config file ${CONFIG_FILE}"
       exit 1
else
    PROMO_ASSORT_LOAD=${PROMO_ASSORT_KEY}
fi


#################################################################################
#  Get latest available input files, because since this interface works in a
#  FULL mode there is no need to process all available input files.
#################################################################################

I_ITEMBU_ONOF_FILE=`find $I_DATA_DIR_XXADEO -name ${I_ITEMBU_ONOF_PATTERN} | sort | tail -n 1`
if [[ ! -f $I_ITEMBU_ONOF_FILE ]];  then
    message "XXRMS208_ASSORTMENT_ITEMBU_ONOF_*.dat  does not exist"
    PROCESS_ONOFF='N'
fi

I_ITEMBU_AM_FILE=`find $I_DATA_DIR_XXADEO -name ${I_ITEMBU_AM_PATTERN} | sort | tail -n 1`
if [[ ! -f $I_ITEMBU_AM_FILE ]];  then
    message "XXRMS208_ASSORTMENT_ITEMBU_AM_*.dat  does not exist"
    PROCESS_AM='N'
fi

I_ITEMBU_RS_FILE=`find $I_DATA_DIR_XXADEO -name ${I_ITEMBU_RS_PATTERN} | sort | tail -n 1`
if [[ ! -f $I_ITEMBU_RS_FILE ]];  then
    message "XXRMS208_ASSORTMENT_ITEMBU_RS_.dat does not exist"
    PROCESS_RS='N'
fi

I_ITEMLOC_FILE=`find $I_DATA_DIR_XXADEO -name ${I_ITEMLOC_PATTERN} | sort | tail -n 1`
if [[ ! -f $I_ITEMLOC_FILE ]];  then
    message "XXRMS208_ASSORTMENT_REG_ITEMLOC_*.dat does not exist"
    PROCESS_REG='N'
fi

I_ITEMLOC_PROMO_FILE=`find $I_DATA_DIR_XXADEO -name ${I_ITEMLOC_PROMO_PATTERN} | sort | tail -n 1`
if [[ ! -f $I_ITEMLOC_PROMO_FILE ]];  then
    message "XXRMS208_ASSORTMENT_PROMO_ITEMLOC_*.dat does not exist"
    PROCESS_PROMO='N'
fi


#################################################################################
#  MAIN PROGRAM CONTENT
#################################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

message "Program started ..."
message "Promotion Assortment Load is: $PROMO_ASSORT_LOAD"

if [ ! -s "${I_ITEMBU_ONOF_FILE}" ];then
        message "${I_ITEMBU_ONOF_FILE} is empty"
        PROCESS_ONOFF='N'
fi

if [ ! -s "${I_ITEMBU_AM_FILE}" ];then
        message "${I_ITEMBU_AM_FILE} is empty"
        PROCESS_AM='N'
fi

if [ ! -s "${I_ITEMBU_RS_FILE}" ];then
        message "${I_ITEMBU_RS_FILE} is empty"
        PROCESS_RS='N'
fi

if [ ! -s "${I_ITEMLOC_FILE}" ];then
        message "${I_ITEMLOC_FILE} is empty"
        PROCESS_REG='N'
fi

if [ ! -s "${I_ITEMLOC_PROMO_FILE}" ] && [ $PROCESS_PROMO = 'Y' ];then
        message "${I_ITEMLOC_PROMO_FILE} is empty"
        PROCESS_PROMO='N'
fi


#################################################################################
#  XXADEO_ITEM_SALES_DATES FLOW
#################################################################################
#IF1: Check if ONOFF files should be processed
if [ $PROCESS_ONOFF = "Y" ]; then

cat > ${FLOW_FILE} << EOF

<FLOW name="${PROGRAM_NAME}.flw">

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_ITEMBU_ONOF_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_ITEMBU_ONOF_SCHEMA}"/>
                <OUTPUT name="itembu_onof_import.v"/>
        </OPERATOR>

        ${DBPREPSTMT}
                <INPUT name="itembu_onof_import.v"/>
                <PROPERTY name="tablename" value="${XXADEO_RMS}.XXADEO_ITEM_SALES_DATES"/>
                <PROPERTY name="schemaowner" value="${XXADEO_RMS}" />
                <PROPERTY name="statement">  <![CDATA[
                 MERGE INTO ${XXADEO_RMS}.XXADEO_ITEM_SALES_DATES
                 USING dual
                 ON (ITEM = ? AND BUSINESS_UNIT_ID = ? )
                 WHEN NOT MATCHED THEN
                       INSERT
                       (
                           ITEM,
                           BUSINESS_UNIT_ID,
                           SALES_START_DATE,
                           SALES_END_DATE,
                           RECORD_UPLOAD_DATETIME,
                           LAST_UPDATE_DATETIME,
                           READY_FOR_EXPORT
                       )
                       VALUES
                       (
                            ?,
                            ?,
                            ?,
                            ?,
                            SYSDATE,
                            SYSDATE,
                            'N'
                       )
                 WHEN MATCHED THEN
                               UPDATE
                               SET
                                      SALES_START_DATE       = ?,
                                      SALES_END_DATE         = ?,
                                      RECORD_UPLOAD_DATETIME = SYSDATE,
                                      LAST_UPDATE_DATETIME   = SYSDATE,
                                      READY_FOR_EXPORT       = 'N'
                ]]> </PROPERTY>
                <PROPERTY name="fields" value="ITEM BUSINESS_UNIT_ID ITEM BUSINESS_UNIT_ID SALES_START_DATE SALES_END_DATE SALES_START_DATE SALES_END_DATE"/>
        </OPERATOR>

</FLOW>

EOF

#  Execute the flow
${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?
message "Assortment-ONOFF loaded successfully"

else
    message "Warning: Assortment-ONOFF not loaded"

fi
#IF1: END OF ONOFF FLOW


#################################################################################
#  XXADEO_ITEM_ASSORTMENT_MODE FLOW
#################################################################################
#IF1: Check if AM files should be processed
if [ $PROCESS_AM = "Y" ]; then

cat > ${FLOW_FILE} << EOF

<FLOW name="${PROGRAM_NAME}.flw">

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_ITEMBU_AM_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_ITEMBU_AM_SCHEMA}"/>
                <OUTPUT name="itembu_am_import.v"/>
        </OPERATOR>

        <OPERATOR type="parser">
                <INPUT name="itembu_am_import.v"/>
                        <PROPERTY name="expression">
                        <![CDATA[
                                {
                                        if (RECORD.RECORD_TYPE == "U")
                                        {
                                             RECORD.RECORD_TYPE = "C";
                                        }
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="itembu_am_import2.v"/>
        </OPERATOR>
		
        ${DBPREPSTMT}
                <INPUT name="itembu_am_import2.v"/>
                <PROPERTY name="tablename" value="${XXADEO_RMS}.XXADEO_ITEM_ASSORTMENT_MODE"/>
                <PROPERTY name="schemaowner" value="${XXADEO_RMS}" />
                <PROPERTY name="statement">  <![CDATA[
                 MERGE INTO ${XXADEO_RMS}.XXADEO_ITEM_ASSORTMENT_MODE
                 USING dual
                 ON (ITEM = ? AND BUSINESS_UNIT_ID = ? AND EFFECTIVE_DATE = ? )
                 WHEN NOT MATCHED THEN
                       INSERT
                       (
                           ITEM,
                           BUSINESS_UNIT_ID,
                           ASSORTMENT_MODE,
                           EFFECTIVE_DATE,
                           RECORD_TYPE,
                           RECORD_UPLOAD_DATETIME,
                           LAST_UPDATE_DATETIME,
                           READY_FOR_EXPORT
                       )
                       VALUES
                       (
                           ?,
                           ?,
                           ?,
                           ?,
                           ?,
                           SYSDATE,
                           SYSDATE,
                           'N'
                       )
                       WHEN MATCHED THEN
                               UPDATE
                               SET
                                     ASSORTMENT_MODE        = ?,
                                     RECORD_TYPE            = ?,
                                     RECORD_UPLOAD_DATETIME = SYSDATE,
                                     LAST_UPDATE_DATETIME   = SYSDATE,
                                     READY_FOR_EXPORT       = 'N'
                ]]> </PROPERTY>
                <PROPERTY name="fields" value="ITEM BUSINESS_UNIT_ID EFFECTIVE_DATE ITEM BUSINESS_UNIT_ID ASSORTMENT_MODE EFFECTIVE_DATE RECORD_TYPE ASSORTMENT_MODE RECORD_TYPE" />
        </OPERATOR>

</FLOW>

EOF

#  Execute the flow
${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?
message "Assortment-AM loaded successfully"

else
    message "Warning: Assortment-AM not loaded"

fi
#IF1: END OF AM FLOW

#################################################################################
#  XXADEO_ITEM_RANGE_SIZE FLOW
#################################################################################
#IF1: Check if RS files should be processed
if [ $PROCESS_RS = "Y" ]; then

cat > ${FLOW_FILE} << EOF

<FLOW name="${PROGRAM_NAME}.flw">

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile"  value="${I_ITEMBU_RS_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_ITEMBU_RS_SCHEMA}"/>
                <OUTPUT name="itembu_rs_import.v"/>
        </OPERATOR>
		
		<OPERATOR type="parser">
                <INPUT name="itembu_rs_import.v"/>
                        <PROPERTY name="expression">
                        <![CDATA[
                                {
                                        if (RECORD.RECORD_TYPE == "U")
                                        {
                                             RECORD.RECORD_TYPE = "C";
                                        }
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="itembu_rs_import2.v"/>
        </OPERATOR>

        ${DBPREPSTMT}
                <INPUT name="itembu_rs_import2.v"/>
                <PROPERTY name="tablename" value="${XXADEO_RMS}.XXADEO_ITEM_RANGE_SIZE"/>
                <PROPERTY name="schemaowner" value="${XXADEO_RMS}" />
                <PROPERTY name="statement">  <![CDATA[
                 MERGE INTO ${XXADEO_RMS}.XXADEO_ITEM_RANGE_SIZE
                 USING dual
                 ON (ITEM = ? AND BUSINESS_UNIT_ID = ? AND EFFECTIVE_DATE = nvl(?,trunc(sysdate)) )
                 WHEN NOT MATCHED THEN
                       INSERT
                       (
                           ITEM,
                           BUSINESS_UNIT_ID,
                           RANGE_SIZE,
                           EFFECTIVE_DATE,
                           RECORD_TYPE,
                           RECORD_UPLOAD_DATETIME,
                           LAST_UPDATE_DATETIME,
                           READY_FOR_EXPORT
                       )
                       VALUES
                       (
                          ?, 
                          ?,
                          ?,
                          nvl(?,trunc(sysdate)),
                          nvl(?,'C'),
                          SYSDATE,
                          SYSDATE,
                          'N'
                       )
                       WHEN MATCHED THEN
                               UPDATE
                               SET
                                      RANGE_SIZE             = ?,
                                      RECORD_TYPE            = nvl(?,'C'),
                                      RECORD_UPLOAD_DATETIME = SYSDATE,
                                      LAST_UPDATE_DATETIME   = SYSDATE,
                                      READY_FOR_EXPORT       = 'N'
                ]]> </PROPERTY>
                <PROPERTY name="fields" value="ITEM BUSINESS_UNIT_ID EFFECTIVE_DATE ITEM BUSINESS_UNIT_ID RANGE_SIZE EFFECTIVE_DATE RECORD_TYPE RANGE_SIZE RECORD_TYPE"/>
        </OPERATOR>

</FLOW>

EOF

#  Execute the flow
${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?
message "Assortment-RS loaded successfully"

else
    message "Warning: Assortment-RS not loaded"

fi
#IF1: END OF AM FLOW

#################################################################################
#  XXADEO_ITEM_LOC_ASSORTMENT REGULAR FLOW
#################################################################################
#IF1: Check if REG files should be processed
if [ $PROCESS_REG = "Y" ]; then

cat > ${FLOW_FILE} << EOF

<FLOW name="${PROGRAM_NAME}.flw">

        <OPERATOR type="import">
                <PROPERTY  name="inputfile"  value="${I_ITEMLOC_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_ITEMLOC_SCHEMA}"/>
                <OUTPUT name="itemloc_reg_import.v"/>
        </OPERATOR>
        
        <OPERATOR type="fieldmod">
                <INPUT name="itemloc_reg_import.v"/>
                <PROPERTY name="duplicate" value="AVAILABILITY=ASSORTMENT_STATUS"/>
                <OUTPUT name="itemloc_reg1_mod.v" />
        </OPERATOR>
        
        <OPERATOR type="fieldmod">
                <INPUT name="itemloc_reg1_mod.v"/>
                <PROPERTY name="rename" value="VISIBILITY=ASSORTMENT_STATUS"/>
                <OUTPUT name="itemloc_reg2_mod.v" />
        </OPERATOR>
		
		<OPERATOR type="parser">
                <INPUT name="itemloc_reg2_mod.v"/>
                        <PROPERTY name="expression">
                        <![CDATA[
                                {
                                        if (RECORD.RECORD_TYPE == "U")
                                        {
                                             RECORD.RECORD_TYPE = "C";
                                        }
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="itemloc_reg3_mod.v"/>
        </OPERATOR>

        ${DBPREPSTMT}
                <INPUT name="itemloc_reg3_mod.v"/>
                <PROPERTY name="tablename" value="${XXADEO_RMS}.XXADEO_ITEM_LOC_ASSORTMENT"/>
                <PROPERTY name="schemaowner" value="${XXADEO_RMS}" />
                <PROPERTY name="statement">  <![CDATA[
                    MERGE INTO ${XXADEO_RMS}.XXADEO_ITEM_LOC_ASSORTMENT
                    USING dual
                    ON (ITEM = ? AND HIER_LEVEL = 'S' AND HIER_VALUE = ? AND EFFECTIVE_DATE = ? )
                    WHEN NOT MATCHED THEN
                          INSERT
                          (
                              ITEM,
                              HIER_LEVEL,
                              HIER_VALUE,
                              VISIBILITY,
                              AVAILABILITY,
                              RECORD_TYPE,
                              READY_FOR_EXPORT,
                              RECORD_UPLOAD_DATETIME,
                              LAST_UPDATE_DATETIME,
                              EFFECTIVE_DATE,
                              ASSORTMENT_TYPE
                          )
                          VALUES
                          (
                              ?,
                              'S',
                              ?,
                              SUBSTR(?,1,1),
                              SUBSTR(?,2,1),
                              ?,
                              'N',
                              SYSDATE,
                              SYSDATE,
                              ?,
                              'R'
                          )
                          WHEN MATCHED THEN
                                  UPDATE
                                  SET
                                         VISIBILITY             = SUBSTR(?,1,1),
                                         AVAILABILITY           = SUBSTR(?,2,1),
                                         RECORD_TYPE            = ?,
                                         READY_FOR_EXPORT       = 'N',
                                         RECORD_UPLOAD_DATETIME = SYSDATE,
                                         LAST_UPDATE_DATETIME   = SYSDATE,
                                         ASSORTMENT_TYPE        = 'R'
            ]]> 
            </PROPERTY>
            <PROPERTY name="fields" value="ITEM HIER_VALUE EFFECTIVE_DATE ITEM HIER_VALUE VISIBILITY AVAILABILITY RECORD_TYPE EFFECTIVE_DATE VISIBILITY AVAILABILITY RECORD_TYPE"/>
        </OPERATOR>

</FLOW>

EOF

#  Execute the flow
${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?
message "Assortment-REG loaded successfully"

else
    message "Warning: Assortment-REG not loaded"

fi
#IF1: END OF REG FLOW

#################################################################################
#  XXADEO_ITEM_LOC_ASSORTMENT PROMO FLOW
#################################################################################
#IF1: LOC FLOW: Check if PROMO files should be processed
if [ $PROCESS_PROMO = "Y" ] && [ $PROMO_ASSORT_LOAD = "Y" ]; then

cat > ${FLOW_FILE} << EOF
<FLOW name="${PROGRAM_NAME}.flw">

        <OPERATOR type="import">
                <PROPERTY  name="inputfile"  value="${I_ITEMLOC_PROMO_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_ITEMLOC_PROMO_SCHEMA}"/>
                <OUTPUT name="itemloc_promo_import.v"/>
        </OPERATOR>
        
        <OPERATOR type="fieldmod">
                <INPUT name="itemloc_promo_import.v"/>
                <PROPERTY name="duplicate" value="AVAILABILITY=ASSORTMENT_STATUS"/>
                <OUTPUT name="itemloc_promo1_mod.v" />
        </OPERATOR>
        
        <OPERATOR type="fieldmod">
                <INPUT name="itemloc_promo1_mod.v"/>
                <PROPERTY name="rename" value="VISIBILITY=ASSORTMENT_STATUS"/>
                <OUTPUT name="itemloc_promo2_mod.v" />
        </OPERATOR>
		
		<OPERATOR type="parser">
                <INPUT name="itemloc_promo2_mod.v"/>
                        <PROPERTY name="expression">
                        <![CDATA[
                                {
                                        if (RECORD.RECORD_TYPE == "U")
                                        {
                                             RECORD.RECORD_TYPE = "C";
                                        }
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="itemloc_promo3_mod.v"/>
        </OPERATOR>

        ${DBPREPSTMT}
                <INPUT name="itemloc_promo3_mod.v"/>
                <PROPERTY name="tablename" value="${XXADEO_RMS}.XXADEO_ITEM_LOC_ASSORTMENT"/>
                <PROPERTY name="schemaowner" value="${XXADEO_RMS}" />
                <PROPERTY name="statement">  
                <![CDATA[
                    MERGE INTO ${XXADEO_RMS}.XXADEO_ITEM_LOC_ASSORTMENT
                    USING dual
                    ON (ITEM = ? AND HIER_LEVEL = 'S' AND HIER_VALUE = ? AND EFFECTIVE_DATE = ? )
                    WHEN NOT MATCHED THEN
                        INSERT
                        (
                            ITEM,
                            HIER_LEVEL,
                            HIER_VALUE,
                            VISIBILITY,
                            AVAILABILITY,
                            RECORD_TYPE,
                            READY_FOR_EXPORT,
                            RECORD_UPLOAD_DATETIME,
                            LAST_UPDATE_DATETIME,
                            EFFECTIVE_DATE,
                            ASSORTMENT_TYPE
                        )
                        VALUES
                        (
                            ?,
                            'S',
                            ?,
                            SUBSTR(?,1,1),
                            SUBSTR(?,2,1),
                            ?,
                            'N',
                            SYSDATE,
                            SYSDATE,
                            ?,
                            'P'
                        )
                        WHEN MATCHED THEN
                                UPDATE
                                SET
                                        VISIBILITY             = SUBSTR(?,1,1),
                                        AVAILABILITY           = SUBSTR(?,2,1),
                                        RECORD_TYPE            = ?,
                                        READY_FOR_EXPORT       = 'N',
                                        RECORD_UPLOAD_DATETIME = SYSDATE,
                                        LAST_UPDATE_DATETIME   = SYSDATE,
                                        ASSORTMENT_TYPE        = 'P'

            ]]> 
            </PROPERTY>
            <PROPERTY name="fields" value="ITEM HIER_VALUE EFFECTIVE_DATE ITEM HIER_VALUE VISIBILITY AVAILABILITY RECORD_TYPE EFFECTIVE_DATE VISIBILITY AVAILABILITY RECORD_TYPE"/>
        </OPERATOR>
</FLOW>
EOF

#  Execute the flow
${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?

else
    message "Warning: Assortment-PROMO not loaded"

fi
#IF1: END OF PROMO FLOW

#################################################################################
#  cleanup and exit:
#################################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE} ; fi
message "Program completed successfully."

check_error_and_clean_before_exit 0 "NO_CHECK"
