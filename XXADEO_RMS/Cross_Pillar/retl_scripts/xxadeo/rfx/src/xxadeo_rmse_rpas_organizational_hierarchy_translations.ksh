#!/bin/ksh

##########################################################################################
#                                                                                        #
# Object Name:  xxadeo_rmse_rpas_organizational_hierarchy_translations.ksh               #
# Description:  The xxadeo_rmse_rpas_organizational_hierarchy_translations.ksh program   #
#               extracts data from RMS tables and places this data into 6 flat file      #
#                                                                                        #
# Version:       1.3                                                                     #
# Author:        CGI                                                                     #
# Creation Date: 18/08/2018                                                              #
# Last Modified: 05/11/2018                                                              #
# History:                                                                               #
#               1.0 - Initial version                                                    #
#               1.1 - Update the variables for environment, librairies                   #
#                     and directory names. We have to use $RETL_OUT                      #
#                     to put output files because this IR is an extraction (RMS side)    #
#               1.2 - Update program name and use space                                  #
#                     instead of  "-" in the concatenation                               #
#               1.3 - Modify Output path, sript name and schema name                     #
#                                                                                        #
##########################################################################################

PROGRAM_NAME='xxadeo_rmse_rpas_organizational_hierarchy_translations'

date=`date +"%G%m%d-%H%M%S"`

RFX_EXE=${RFX_EXE:=rfx}

###############################################################################
#  INCLUDES
###############################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

O_DATA_DIR_XXADEO=${RETL_OUT}/RPAS
SCHEMA_DIR_XXADEO=${RETLforXXADEO}/rfx/schema

RETL_OUTPUT_FILE1=${O_DATA_DIR_XXADEO}/XXADEO_XXRMS227_ORGHIER_TRANS_STORE_${date}.dat
RETL_OUTPUT_FILE2=${O_DATA_DIR_XXADEO}/XXADEO_XXRMS227_ORGHIER_TRANS_DISTRI_${date}.dat
RETL_OUTPUT_FILE3=${O_DATA_DIR_XXADEO}/XXADEO_XXRMS227_ORGHIER_TRANS_REGION_${date}.dat
RETL_OUTPUT_FILE4=${O_DATA_DIR_XXADEO}/XXADEO_XXRMS227_ORGHIER_TRANS_AREA_${date}.dat
RETL_OUTPUT_FILE5=${O_DATA_DIR_XXADEO}/XXADEO_XXRMS227_ORGHIER_TRANS_CHAIN_${date}.dat
RETL_OUTPUT_FILE6=${O_DATA_DIR_XXADEO}/XXADEO_XXRMS227_ORGHIER_TRANS_COMP_${date}.dat

O_SCHEMA=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema

###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
        rm -f "${RETL_OUTPUT_FILE1}.retl"
        rm -f "${RETL_OUTPUT_FILE2}.retl"
        rm -f "${RETL_OUTPUT_FILE3}.retl"
        rm -f "${RETL_OUTPUT_FILE4}.retl"
        rm -f "${RETL_OUTPUT_FILE5}.retl"
        rm -f "${RETL_OUTPUT_FILE6}.retl"
        rm -f "${RETL_OUTPUT_FILE1}"
        rm -f "${RETL_OUTPUT_FILE2}"
        rm -f "${RETL_OUTPUT_FILE3}"
        rm -f "${RETL_OUTPUT_FILE4}"
        rm -f "${RETL_OUTPUT_FILE5}"
        rm -f "${RETL_OUTPUT_FILE6}"
        checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
        rm -f "${RETL_OUTPUT_FILE1}.retl"
        rm -f "${RETL_OUTPUT_FILE2}.retl"
        rm -f "${RETL_OUTPUT_FILE3}.retl"
        rm -f "${RETL_OUTPUT_FILE4}.retl"
        rm -f "${RETL_OUTPUT_FILE5}.retl"
        rm -f "${RETL_OUTPUT_FILE6}.retl"
        checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        rmse_terminate $1
        fi
}


####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml


########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read in the item translation data from the RMS tables:  -->


   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
                SELECT STORE.STORE ID,
                 CONCAT(STORE.STORE, CONCAT(' ',STORE_tl.STORE_name)) LABEL_TRANSLATED ,
                STORE_TL.lang LANG_ID
                FROM STORE
                INNER JOIN STORE_TL ON STORE.STORE = STORE_tl.STORE

         ]]>
      </PROPERTY>
      <OUTPUT   name = "store.v"/>
   </OPERATOR>



   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
                SELECT district.district ID,
                 CONCAT(district.district, CONCAT(' ',district_tl.district_name)) LABEL_TRANSLATED ,
                lang LANG_ID
                FROM DISTRICT
                INNER JOIN DISTRICT_TL ON district.district = district_tl.district

         ]]>
      </PROPERTY>
      <OUTPUT   name = "district.v"/>
   </OPERATOR>





   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[

            SELECT REGION.REGION ID,
                CONCAT(REGION.REGION, CONCAT(' ',REGION_tl.REGION_name)) LABEL_TRANSLATED ,
                lang LANG_ID
                FROM REGION
                INNER JOIN REGION_TL ON REGION.REGION = REGION_tl.REGION

         ]]>
      </PROPERTY>
      <OUTPUT   name = "region.v"/>
   </OPERATOR>



   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[

                SELECT AREA.AREA ID,
                        CONCAT(AREA.AREA, CONCAT(' ',AREA_tl.AREA_name)) LABEL_TRANSLATED ,
                        lang LANG_ID
                FROM AREA
                INNER JOIN AREA_TL ON AREA.AREA = AREA_tl.AREA

         ]]>
      </PROPERTY>
      <OUTPUT   name = "area.v"/>
   </OPERATOR>




   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[

                SELECT CHAIN.CHAIN ID,
                        CONCAT(CHAIN.CHAIN, CONCAT(' ',CHAIN_tl.CHAIN_name)) LABEL_TRANSLATED ,
                        lang LANG_ID
                        FROM CHAIN
                        INNER JOIN CHAIN_TL ON CHAIN.CHAIN = CHAIN_tl.CHAIN
         ]]>
      </PROPERTY>
      <OUTPUT   name = "chain.v"/>
   </OPERATOR>


      ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[

                    SELECT comphead.COMPANY ID,
                        CONCAT(comphead.COMPANY, CONCAT(' ',comphead_tl.CO_NAME)) LABEL_TRANSLATED ,
                        lang LANG_ID
                        FROM comphead
                        INNER JOIN comphead_TL ON comphead.COMPANY = comphead_tl.COMPANY
         ]]>
      </PROPERTY>
      <OUTPUT   name = "comphead.v"/>
   </OPERATOR>


   <OPERATOR type="convert">
        <INPUT name="store.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="store_1.v"/>
</OPERATOR>

   <OPERATOR type="convert">
        <INPUT name="district.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="district_1.v"/>
</OPERATOR>

   <OPERATOR type="convert">
        <INPUT name="region.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="region_1.v"/>
</OPERATOR>


   <OPERATOR type="convert">
        <INPUT name="area.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="area_1.v"/>
</OPERATOR>

   <OPERATOR type="convert">
        <INPUT name="chain.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="chain_1.v"/>
</OPERATOR>

   <OPERATOR type="convert">
        <INPUT name="comphead.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="comphead_1.v"/>
</OPERATOR>





      <!--  Write out the item translation data to a flat file:  -->


   <OPERATOR type="export">
      <INPUT    name="store_1.v"/>
      <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE1}.retl"/>
      <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
   </OPERATOR>

   <OPERATOR type="export">
      <INPUT    name="district_1.v"/>
      <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE2}.retl"/>
      <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
   </OPERATOR>

      <OPERATOR type="export">
      <INPUT    name="region_1.v"/>
      <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE3}.retl"/>
      <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
   </OPERATOR>

      <OPERATOR type="export">
      <INPUT    name="area_1.v"/>
      <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE4}.retl"/>
      <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
   </OPERATOR>

      <OPERATOR type="export">
      <INPUT    name="chain_1.v"/>
      <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE5}.retl"/>
      <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
   </OPERATOR>

      <OPERATOR type="export">
      <INPUT    name="comphead_1.v"/>
      <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE6}.retl"/>
      <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

###############################################
#  Execute the RETL flow that had previouslyXXADEO_RMSE_RPAS_OrgHier.ksh
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
check_error_and_clean_before_exit $?

###############################################################################
#  Convert output data files to CSV format
###############################################################################

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE1}.retl" "${RETL_OUTPUT_FILE1}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE2}.retl" "${RETL_OUTPUT_FILE2}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE3}.retl" "${RETL_OUTPUT_FILE3}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE4}.retl" "${RETL_OUTPUT_FILE4}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE5}.retl" "${RETL_OUTPUT_FILE5}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE6}.retl" "${RETL_OUTPUT_FILE6}"
check_error_and_clean_before_exit $?


###############################################################################
#  Log the number of records written to the final output files and add up the total
###############################################################################

log_num_recs ${RETL_OUTPUT_FILE1}
log_num_recs ${RETL_OUTPUT_FILE2}
log_num_recs ${RETL_OUTPUT_FILE3}
log_num_recs ${RETL_OUTPUT_FILE4}
log_num_recs ${RETL_OUTPUT_FILE5}
log_num_recs ${RETL_OUTPUT_FILE6}


###############################################################################
#  cleanup and exit:
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE} ; fi
message "Program completed successfully."

check_error_and_clean_before_exit 0 "NO_CHECK"