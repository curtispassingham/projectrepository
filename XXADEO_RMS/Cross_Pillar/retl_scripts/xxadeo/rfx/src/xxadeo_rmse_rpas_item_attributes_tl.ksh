#!/bin/ksh

###############################################################################
#
#  The xxadeo_rmse_rpas_item_attributes_tl.ksh program extract UDA and CFA
#  Attributes, Attributes Translations and Item Attributes and places thoses
#  data into three flat files: XXRMS220_ATTR_TL
#
###############################################################################


################ settings of the local DIR ##############
###### according to the local variables (.profile) ######

exeDate=`date +"%G%m%d-%H%M%S"`


################### PROGRAM DEFINE ######################
##########     (must be the first define)     ###########

export PROGRAM_NAME="xxadeo_rmse_rpas_item_attributes_tl"


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) #####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export DATA_DIR="${RETL_OUT}/RPAS"

#------------File='XXRMS220_ATTR_TL'
export OUTPUT_FILE=${DATA_DIR}/XXRMS220_ATTR_TL_${exeDate}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_attr_tl.schema


###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${OUTPUT_FILE}.retl"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
                rm -f "${OUTPUT_FILE}.retl"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
                rmse_terminate $1
        fi
}


#-------------------------------------------------
#  Log start message and define the RETL flow file
#-------------------------------------------------

message "Program started..."
RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml


#-----------------------------------------------------
#  Copy the RETL flow to a file to be executed by rfx:
#-----------------------------------------------------

cat > $RETL_FLOW_FILE << EOF

<FLOW name="${PROGRAM_NAME}.flw">

<!--  Read in the UDA and CFA data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name="query">
         <![CDATA[
            select 'UDA' ATTR_TYPE,
                   u.display_type ATTR_DATA_TYPE,
                   mdvm.BU,
                   mdvm.parameter KEY,
                   ut.lang LANG_ISO,
                   lower(l.description) as LANG_NAME,
                   u.uda_id ATTR_ID,
                   ut.uda_desc ATTR_DESC,
                   uvt.uda_value ATTR_VALUE,
                   uvt.uda_value_desc ATTR_VALUE_DESC
              from ${XXADEO_RMS}.UDA u,
                   ${XXADEO_RMS}.UDA_TL ut,
                   ${XXADEO_RMS}.UDA_VALUES_TL uvt,
                   ${XXADEO_RMS}.XXADEO_MOM_DVM mdvm,
                   ${XXADEO_RMS}.lang l
             where u.uda_id = ut.uda_id
               and l.lang = ut.lang
               and uvt.uda_id (+)= ut.uda_id
               and uvt.lang (+)= ut.lang
               and mdvm.value_1 (+)= ut.uda_id
               and mdvm.func_area (+)= 'UDA'
               and l.lang in (select value_1 
                                    from ${XXADEO_RMS}.xxadeo_mom_dvm 
                                   where (1=1)
                                     and func_area = 'ATTR_TL_LANG'
                                     and parameter = 'ATTR_TL_ALLOWED_LANG_CODE')
         ]]>
      </PROPERTY>
      <OUTPUT   name="xxadeo_rmse_rpas_attr_tl_uda.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
      <INPUT    name="xxadeo_rmse_rpas_attr_tl_uda.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
          <CONVERTSPECS>
                <CONVERT destfield="ATTR_TYPE" sourcefield="ATTR_TYPE">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ATTR_DATA_TYPE" sourcefield="ATTR_DATA_TYPE">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="LANG_ISO" sourcefield="LANG_ISO">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="LANG_NAME" sourcefield="LANG_NAME">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ATTR_ID" sourcefield="ATTR_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ATTR_DESC" sourcefield="ATTR_DESC">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ATTR_VALUE" sourcefield="ATTR_VALUE">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ATTR_VALUE_DESC" sourcefield="ATTR_VALUE_DESC">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>

        </CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="xxadeo_rmse_rpas_attr_tl_uda_1.v"/>
   </OPERATOR>



   ${DBREAD}
      <PROPERTY name="query">
         <![CDATA[
            select 'CFA' ATTR_TYPE,
                   cfa.ui_widget ATTR_DATA_TYPE,
                   mdvm.BU,
                   mdvm.parameter KEY,
                   cfal.lang LANG_ISO,
                   lower(l.description) as LANG_NAME,
                   cfa.attrib_id ATTR_ID,
                   cfal.label ATTR_DESC,
                   cdt.code ATTR_VALUE,
                   cdt.code_desc ATTR_VALUE_DESC
              from ${XXADEO_RMS}.CFA_ATTRIB cfa,
                   ${XXADEO_RMS}.CFA_ATTRIB_LABELS cfal,
                   ${XXADEO_RMS}.CODE_DETAIL_TL cdt,
                   ${XXADEO_RMS}.XXADEO_MOM_DVM mdvm,
                   ${XXADEO_RMS}.lang l
             where cfa.attrib_id = cfal.attrib_id
               and l.lang = cfal.lang
               and cdt.code_type (+)= cfa.code_type
               and cdt.lang (+)= cfal.lang
               and mdvm.value_1 (+)= cfa.attrib_id
               and mdvm.func_area (+)= 'CFA'
               and l.lang in (select value_1 
                                    from ${XXADEO_RMS}.xxadeo_mom_dvm 
                                   where (1=1)
                                     and func_area = 'ATTR_TL_LANG'
                                     and parameter = 'ATTR_TL_ALLOWED_LANG_CODE')
         ]]>
      </PROPERTY>
      <OUTPUT   name="xxadeo_rmse_rpas_attr_tl_cfa.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
      <INPUT    name="xxadeo_rmse_rpas_attr_tl_cfa.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
          <CONVERTSPECS>
                <CONVERT destfield="ATTR_TYPE" sourcefield="ATTR_TYPE">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ATTR_DATA_TYPE" sourcefield="ATTR_DATA_TYPE">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="LANG_ISO" sourcefield="LANG_ISO">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="LANG_NAME" sourcefield="LANG_NAME">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ATTR_ID" sourcefield="ATTR_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ATTR_DESC" sourcefield="ATTR_DESC">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ATTR_VALUE" sourcefield="ATTR_VALUE">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ATTR_VALUE_DESC" sourcefield="ATTR_VALUE_DESC">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>

        </CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="xxadeo_rmse_rpas_attr_tl_cfa_1.v"/>
   </OPERATOR>

<!--  Write out the UDA/CFA translations data to a flat file:  -->

   <OPERATOR type="export">
      <INPUT    name="xxadeo_rmse_rpas_attr_tl_uda_1.v"           />
      <PROPERTY name="outputmode" value="append"             />
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}.retl" />
   </OPERATOR>

   <OPERATOR type="export">
      <INPUT    name="xxadeo_rmse_rpas_attr_tl_cfa_1.v"           />
      <PROPERTY name="outputmode" value="append"                />
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}.retl" />
   </OPERATOR>

</FLOW>

EOF



#-----------------------
#  Execute the RETL flow
#-----------------------

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
check_error_and_clean_before_exit $?


#-----------------------------------------
#  Convert output data files to CSV format
#-----------------------------------------

csv_converter "retlsv-csv" "${OUTPUT_FILE}.retl" "${OUTPUT_FILE}"
check_error_and_clean_before_exit $?



#-----------------------------------------------------------------------------------
#  Log the number of records written to the final output files and add up the total:
#-----------------------------------------------------------------------------------

log_num_recs ${OUTPUT_FILE}


#--------------------
#  Do error checking:
#--------------------

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"


#-------------------------
#  Remove the status file:
#-------------------------

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."
message "xxadeo_rmse_rpas_item_attributes_tl.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${PROGRAM_NAME} ${pgmPID}


#----------------------
#  Cleanup and exit:
#----------------------

check_error_and_clean_before_exit 0 "NO_CHECK"