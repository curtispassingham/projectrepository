#!/bin/ksh

########################################################
#  Script extracts supplier information from the
#  RMS sups table filtered with an address type 04 Order
########################################################

########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################

export PROGRAM_NAME="xxadeo_rmse_simtar_orders_suppliers"

########################################################
#  INCLUDES
#  This section must come after PROGRAM DEFINES
########################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

########################################################
#  OUTPUT DEFINES
#  This section must come after INCLUDES
########################################################


function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${OUTPUT_FILE}"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        fi
}
###############################################################################
#  Log start message and define the RETL flow file
###############################################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

###############################################################################
################### (this section must come after INCLUDES) ###################

export O_DATA_DIR_XXADEO=${RETL_OUT}/SIMTAR

export OUTPUT_FILE=${O_DATA_DIR_XXADEO}/splr.csv.dat

export SCHEMA_DIR_XXADEO=${RETLforXXADEO}/rfx/schema

export OUTPUT_SCHEMA=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema


###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"



cat > $RETL_FLOW_FILE << EOF

   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
               SELECT s.SUPPLIER,
                       to_char(s.supplier) || ' ' || s.sup_name as SUP_NAME
               FROM ${RMS_OWNER}.SUPS s
                           where exists (select 'Y' from ${RMS_OWNER}.addr adr where adr.module = 'SUPP' and adr.addr_type = '04' and adr.key_value_1 = s.supplier)
                           and not exists (select '1' from ${RMS_OWNER}.addr where module = 'SUPP' and addr_type = '06' and key_value_1 = s.supplier)

            ]]>
         </PROPERTY>
         <OUTPUT name = "output.v"/>
      </OPERATOR>
      <OPERATOR type="export">
         <INPUT    name="output.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
      </OPERATOR>
   </FLOW>

EOF

###############################################################################
#  Execute the RETL flow that was
#  previously copied into rmse_rpas_orders_suppliers.xml:
###############################################################################

$RFX_EXE  $RFX_OPTIONS  -f $RETL_FLOW_FILE
check_error_and_clean_before_exit $?

###############################################################################
#  Convert output data files to CSV format
###############################################################################

#csv_converter "retlsv-csv" "${OUTPUT_FILE}.retl" "${OUTPUT_FILE}"
#check_error_and_clean_before_exit $?


###############################################################################
#  Log the number of records written to the final output files
#  and add up the total:
###############################################################################

log_num_recs ${OUTPUT_FILE}


###############################################################################
#  Do error checking:
###############################################################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#  Remove the status file:

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."

#  Cleanup and exit:

check_error_and_clean_before_exit 0 "NO_CHECK"
