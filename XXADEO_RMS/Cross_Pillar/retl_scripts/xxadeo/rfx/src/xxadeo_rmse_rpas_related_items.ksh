#!/bin/ksh

###################################################################
#
#  This script exports Substitute Items information 
#  from RMS to RPO and other RPAS modules.
#  
#  _________________________________________________________
# | Workfile:      | xxadeo_rmse_rpas_related_items.ksh     |
# | Created by:    | PR                                     |
# | Creation Date: | 20180405-1204                          |
# | Modify Date:   | 20180405-1806                          |
# | Version:       | 0.2                                    |
#  --------------------------------------------------------- 
#
###################################################################

################## PROGRAM DEFINITIONS ####################

export PROGRAM_NAME="xxadeo_rmse_rpas_related_items"

##########                                       ##########
######################## INCLUDES #########################

# Environment configuration script
. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env

# Script Library Collection
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

##########                                      ##########
###################  OUTPUT DEFINES ######################


export DATE_FORMAT=YYYYMMDDHHMISS

export OUTBOUND_MOM_FOLDER=${RETL_OUT}/RPAS
export CEMLI=XXRMS218
export DATA_EXTRACTED=RELATEDITEMS
export FILE_FORMAT=$(date '+%Y%m%d-%H%M%S').dat

export OUTPUT_FILE=${OUTBOUND_MOM_FOLDER}/${CEMLI}_${DATA_EXTRACTED}_${FILE_FORMAT}
export OUTPUT_SCHEMA_FILE=$SCHEMA_DIR/${PROGRAM_NAME}.schema

##########                                      ##########
##################  CREATE RETL FLOWS ####################

message "Program start..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name="${PROGRAM_NAME}.flw">

	${DBREAD}
		<PROPERTY name="query">
		<![CDATA[
	
	select				
		rel_ih.item,
		rel_ih.relationship_type,
		rel_id.related_item,
		to_char(rel_id.start_date,'${DATE_FORMAT}') as start_date,
		to_char(rel_id.end_date,'${DATE_FORMAT}') as end_date     
	from			
		${XXADEO_RMS}.RELATED_ITEM_HEAD rel_ih,		
		${XXADEO_RMS}.RELATED_ITEM_DETAIL rel_id		
	where			
		rel_ih.relationship_id=rel_id.relationship_id
	
	]]>  
	</PROPERTY>
	<OUTPUT name="rms_qry_rel_items.v"/>
	</OPERATOR>
	
	<OPERATOR type="convert">
      <INPUT    name="rms_qry_rel_items.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
          <CONVERTSPECS>
		        <CONVERT destfield="START_DATE" sourcefield="START_DATE">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value=""/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="END_DATE" sourcefield="END_DATE">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value=""/>
                </CONVERTFUNCTION>
                </CONVERT>
          </CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="rms_qry_rel_items2.v"/>
   </OPERATOR>
		  
	<OPERATOR type="export">
	<INPUT    name="rms_qry_rel_items2.v"/>
	<PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
	<PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
	</OPERATOR>
</FLOW>
EOF

##########                                      ##########
#################  EXECUTE RETL FLOWS ####################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
		
checkerror -e $? -m "Program failed - check ${ERR_FILE}"

log_num_recs ${OUTPUT_FILE}

retl_stat=$?

##########                                      ##########
#################  REMOVE STATUS FILE ####################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

##########                                      ##########
###################  REPORT AND EXIT #####################

case $retl_stat in
  0 )
	 msg="Program completed successfully with" ;;
  * )
	 msg="Program exited with error" ;;
esac

message "$msg code: $retl_stat"
rmse_terminate $retl_stat	 
