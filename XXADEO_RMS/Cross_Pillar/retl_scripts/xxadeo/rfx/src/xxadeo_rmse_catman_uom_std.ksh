#!/bin/ksh

###################################################################
#
#  This script extracts Standard units of measure from RMS to be
#  used by RPAS.
#  RICEW #: 1183
#  IR ID: IR0336
#
#  Expected files:
#    1. mes_std.csv.rpl
#
#  Usage: ./xxadeo_rmse_catman_uom_std.ksh
#  _____________________________________________________________
# | Workfile:      | xxadeo_rmse_rpas_uom_std.ksh               |
# | Created by:    | PR <paulo.reis@oracle.com>                 |
# | Creation Date: | 20180717-1000                              |
# | Modify Date:   | 20181106-1545                              |
# | Version:       | 1.5                                        |
#  -------------------------------------------------------------
# History:
#	1.4 - Bug Fixing (BUG_631): Correction of the output file name
#	1.5 - Bug Fixing: BUG_638 - The applied changes were:
#		- Changed the name of script, schema and program
#		- Applied a new filter condition. They only want the
#		items where item level equal 1
#		- Changed the output subfolder to 'CATMAN'
#
######################################################################

################## PROGRAM DEFINITIONS ####################

export PROGRAM_NAME="xxadeo_rmse_catman_uom_std"

RETL_ENV=$RETLforXXADEO

# Environment configuration script
. ${RETL_ENV}/rfx/etc/xxadeo_rmse_config.env

# Script Library Collection
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

###########                                        #########
##################  INPUT / OUTPUT DEFINES #################

INBOUND_DIR=$RETL_IN
OUTBOUND_DIR=$RETL_OUT/CATMAN

FILE_NAME=mes_std.csv.rpl
OUTPUT_FILE_FORMAT=${FILE_NAME}

OUTPUT_FILE=${OUTBOUND_DIR}/${OUTPUT_FILE_FORMAT}
OUTPUT_SCHEMA_FILE=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

##########                                      ##########
##################  CREATE RETL FLOWS ####################

message "Program start..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

    ${DBREAD}
    <PROPERTY name = "query">
    <![CDATA[

	SELECT
	  im.item ITEM_NUMBER,                    -- MAP: Source 1,01 -> Target 1.01
	  im.standard_uom STANDARD_UOM_VALUE      -- MAP: Source 1,29 -> Target 1.02
	FROM
	  ${RMS_OWNER}.ITEM_MASTER im
	WHERE
		  im.status='A'
	  AND	  im.item_level=1
	ORDER BY im.item
    ]]>
	</PROPERTY>
    <OUTPUT name="rmsdb_qry.v"/>
    </OPERATOR>

    <OPERATOR type="export">
    <INPUT    name="rmsdb_qry.v"/>
    <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
    <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
    </OPERATOR>
</FLOW>
EOF

##########                                      ##########
#################  EXECUTE RETL FLOWS ####################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

log_num_recs ${OUTPUT_FILE}

retl_stat=$?

##########                                      ##########
#################  REMOVE STATUS FILE ####################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

##########                                      ##########
###################  REPORT AND EXIT #####################

case $retl_stat in
  0 )
     msg="Program completed successfully with" ;;
  * )
     msg="Program exited with error" ;;
esac

message "$msg code: $retl_stat"
rmse_terminate $retl_stat
