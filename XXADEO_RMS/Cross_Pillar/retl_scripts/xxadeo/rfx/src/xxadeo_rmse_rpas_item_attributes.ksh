#!/bin/ksh

###############################################################################
#
#  The xxadeo_rmse_rpas_item_attributes_tl.ksh program extract UDA and CFA
#  Attributes, Attributes Translations and Item Attributes and places thoses
#  data into three flat files: XXRMS209_ATTR
#                              XXRMS209_ITEMATTR
#  Version: 1.1
#
#  History: 1.0 - Initial Release
#           1.1 - Add filter item status = 'A'
#
###############################################################################


################ settings of the local DIR ##############
###### according to the local variables (.profile) ######

exeDate=`date +"%G%m%d-%H%M%S"`


################### PROGRAM DEFINE ######################
##########     (must be the first define)     ###########

export PROGRAM_NAME="xxadeo_rmse_rpas_item_attributes"


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) #####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export DATA_DIR="${RETL_OUT}/RPAS"


#------------File_1='XXRMS209_ATTR'
export OUTPUT_FILE_1=${DATA_DIR}/XXRMS209_ATTR_${exeDate}.dat
export OUTPUT_FILE_11=${DATA_DIR}/XXRMS209_ATTR_${exeDate}_1.dat
export OUTPUT_FILE_12=${DATA_DIR}/XXRMS209_ATTR_${exeDate}_2.dat
export OUTPUT_SCHEMA_1=${SCHEMA_DIR}/xxadeo_rmse_rpas_attr.schema

#------------File_2='XXRMS209_ITEMATTR'
export OUTPUT_FILE_2=${DATA_DIR}/XXRMS209_ITEMATTR_${exeDate}.dat
export OUTPUT_FILE_21=${DATA_DIR}/XXRMS209_ITEMATTR_${exeDate}_1.dat
export OUTPUT_FILE_22=${DATA_DIR}/XXRMS209_ITEMATTR_${exeDate}_2.dat
export OUTPUT_SCHEMA_2=${SCHEMA_DIR}/xxadeo_rmse_rpas_itemattr.schema


###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${OUTPUT_FILE_11}"
                rm -f "${OUTPUT_FILE_12}"
                rm -f "${OUTPUT_FILE_21}"
                rm -f "${OUTPUT_FILE_22}"
                rm -f "${OUTPUT_FILE_11}.retl"
                rm -f "${OUTPUT_FILE_12}.retl"
                rm -f "${OUTPUT_FILE_21}.retl"
                rm -f "${OUTPUT_FILE_22}.retl"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
                rm -f "${OUTPUT_FILE_11}"
                rm -f "${OUTPUT_FILE_12}"
                rm -f "${OUTPUT_FILE_21}"
                rm -f "${OUTPUT_FILE_22}"
                rm -f "${OUTPUT_FILE_11}.retl"
                rm -f "${OUTPUT_FILE_12}.retl"
                rm -f "${OUTPUT_FILE_21}.retl"
                rm -f "${OUTPUT_FILE_22}.retl"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
                rmse_terminate $1
        fi
}


#-------------------------------------------------
#  Log start message and define the RETL flow file
#-------------------------------------------------

message "Program started..."
RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml


#-----------------------------------------------------
#  Copy the RETL flow to a file to be executed by rfx:
#-----------------------------------------------------

cat > $RETL_FLOW_FILE << EOF

<FLOW name="${PROGRAM_NAME}.flw">

<!--  Read in the UDA and CFA data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name="query">
         <![CDATA[
            select 'UDA' ATTR_TYPE,
                   u.uda_id ATTR_ID,
                   u.uda_desc ATTR_NAME,
                   u.display_type ATTR_DATA_TYPE,
                   mdvm.BU,
                   mdvm.parameter KEY,
                   uv.uda_value ATTR_VALUE,
                   uv.uda_value_desc ATTR_VALUE_NAME
              from ${XXADEO_RMS}.uda u,
                   ${XXADEO_RMS}.uda_values uv,
                   ${XXADEO_RMS}.XXADEO_MOM_DVM mdvm
             where uv.uda_id (+)= u.uda_id
               and mdvm.value_1 (+)= u.uda_id
               and mdvm.func_area(+) = 'UDA'
         ]]>
      </PROPERTY>
      <OUTPUT   name="xxadeo_rmse_rpas_attr_uda_1.v"/>
   </OPERATOR>


   <OPERATOR type="convert">
        <INPUT name="xxadeo_rmse_rpas_attr_uda_1.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="ATTR_TYPE" sourcefield="ATTR_TYPE">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ATTR_ID" sourcefield="ATTR_ID">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ATTR_NAME" sourcefield="ATTR_NAME">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ATTR_DATA_TYPE" sourcefield="ATTR_DATA_TYPE">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="xxadeo_rmse_rpas_attr_uda.v"/>
</OPERATOR>


   ${DBREAD}
      <PROPERTY name="query">
         <![CDATA[
            select 'CFA' ATTR_TYPE,
                   cfa.attrib_id ATTR_ID,
                   cfa.view_col_name ATTR_NAME,
                   cfa.ui_widget ATTR_DATA_TYPE,
                   mdvm.BU,
                   mdvm.parameter KEY,
                   cd.code ATTR_VALUE,
                   cd.code_desc ATTR_VALUE_NAME
              from ${XXADEO_RMS}.CFA_EXT_ENTITY cee,
                   ${XXADEO_RMS}.CFA_ATTRIB_GROUP_SET cags,
                   ${XXADEO_RMS}.CFA_ATTRIB_GROUP cag,
                   ${XXADEO_RMS}.CFA_ATTRIB cfa,
                   ${XXADEO_RMS}.XXADEO_MOM_DVM mdvm,
                   ${XXADEO_RMS}.CODE_DETAIL cd
             where cee.ext_entity_id = cags.ext_entity_id
               and cags.group_set_id = cag.group_set_id
               and cee.base_rms_table = 'ITEM_MASTER'
               and cfa.group_id = cag.group_id
               and mdvm.value_1 (+)= cfa.attrib_id
               and mdvm.func_area (+)= 'CFA'
               and cd.code_type (+)= cfa.code_type
         ]]>
      </PROPERTY>
      <OUTPUT   name="xxadeo_rmse_rpas_attr_cfa_1.v"/>
   </OPERATOR>

<OPERATOR type="convert">
        <INPUT name="xxadeo_rmse_rpas_attr_cfa_1.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="ATTR_TYPE" sourcefield="ATTR_TYPE">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ATTR_ID" sourcefield="ATTR_ID">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ATTR_NAME" sourcefield="ATTR_NAME">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ATTR_DATA_TYPE" sourcefield="ATTR_DATA_TYPE">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="xxadeo_rmse_rpas_attr_cfa.v"/>
</OPERATOR>


   ${DBREAD}
      <PROPERTY name="query">
         <![CDATA[
               select uil.item ITEM_ID,
                      'UDA' ATTR_TYPE,
                      uil.uda_id ATTR_ID,
                      to_char(uil.uda_value) ATTR_VALUE
                 from ${XXADEO_RMS}.item_master im,
                      ${XXADEO_RMS}.uda_item_lov uil
                where im.item = uil.item
                  and im.status = 'A'
                union
               select uff.item ITEM_ID,
                      'UDA' ATTR_TYPE,
                      uff.uda_id ATTR_ID,
                      uff.uda_text ATTR_VALUE
                 from ${XXADEO_RMS}.item_master im,
                      ${XXADEO_RMS}.uda_item_ff uff
                where im.item = uff.item
                  and im.status = 'A'
                union
               select udt.item ITEM_ID,
                      'UDA' ATTR_TYPE,
                      udt.uda_id ATTR_ID,
                      to_char(udt.uda_date) ATTR_VALUE
                 from ${XXADEO_RMS}.item_master im,
                      ${XXADEO_RMS}.uda_item_date udt
                where im.item = udt.item
                  and im.status = 'A'
         ]]>
      </PROPERTY>
      <OUTPUT   name="xxadeo_rmse_rpas_itemattr_uda_1.v"/>
   </OPERATOR>



<OPERATOR type="convert">
        <INPUT name="xxadeo_rmse_rpas_itemattr_uda_1.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="ITEM_ID" sourcefield="ITEM_ID">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ATTR_TYPE" sourcefield="ATTR_TYPE">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ATTR_ID" sourcefield="ATTR_ID">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ATTR_VALUE" sourcefield="ATTR_VALUE">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="xxadeo_rmse_rpas_itemattr_uda.v"/>
</OPERATOR>





   ${DBREAD}
      <PROPERTY name="query">
         <![CDATA[
              select icfaval.item ITEM_ID,
                     'CFA' ATTR_TYPE,
                     cfa.attrib_id ATTR_ID,
                     icfaval.cfa_value ATTR_VALUE
                from ${XXADEO_RMS}.cfa_attrib cfa,
                     (
                      select imcfa.item,
                             imcfa.group_id,
                             imcfa.cfa_attribute,
                             imcfa.cfa_value
                        from ${XXADEO_RMS}.item_master im,
                             ${XXADEO_RMS}.item_master_cfa_ext 
                             unpivot
                             (
                              cfa_value for cfa_attribute in
                               (
                                varchar2_1,
                                varchar2_2,
                                varchar2_3,
                                varchar2_4,
                                varchar2_5,
                                varchar2_6,
                                varchar2_7,
                                varchar2_8,
                                varchar2_9,
                                varchar2_10
                               )
                             ) imcfa
                       where im.item = imcfa.item
                         and im.status = 'A'
                       union
                      select imcfa.item,
                             imcfa.group_id,
                             imcfa.cfa_attribute,
                             to_char(imcfa.cfa_value)
                        from ${XXADEO_RMS}.item_master im,
                             ${XXADEO_RMS}.item_master_cfa_ext 
                             unpivot
                             (
                              cfa_value for cfa_attribute in
                               (
                                number_11,
                                number_12,
                                number_13,
                                number_14,
                                number_15,
                                number_16,
                                number_17,
                                number_18,
                                number_19,
                                number_20
                               )
                             ) imcfa
                       where im.item = imcfa.item
                         and im.status = 'A'
                       union
                      select imcfa.item,
                             imcfa.group_id,
                             imcfa.cfa_attribute,
                             to_char(imcfa.cfa_value)
                        from ${XXADEO_RMS}.item_master im,
                             ${XXADEO_RMS}.item_master_cfa_ext
                             unpivot
                             (
                              cfa_value for cfa_attribute in
                               (
                                date_21,
                                date_22,
                                date_23,
                                date_24,
                                date_25
                               )
                             ) imcfa
                       where im.item = imcfa.item
                         and im.status = 'A'
                     ) icfaval
               where cfa.group_id = icfaval.group_id
                 and upper(cfa.storage_col_name) = upper(icfaval.cfa_attribute)
            order by ITEM_ID,
                     ATTR_ID,
                     ATTR_VALUE
         ]]>
      </PROPERTY>
      <OUTPUT   name="xxadeo_rmse_rpas_itemattr_cfa_1.v"/>
   </OPERATOR>




   <OPERATOR type="convert">
        <INPUT name="xxadeo_rmse_rpas_itemattr_cfa_1.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="ITEM_ID" sourcefield="ITEM_ID">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ATTR_TYPE" sourcefield="ATTR_TYPE">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ATTR_ID" sourcefield="ATTR_ID">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ATTR_VALUE" sourcefield="ATTR_VALUE">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="xxadeo_rmse_rpas_itemattr_cfa.v"/>
</OPERATOR>



<!--  Write out the UDA translations data to a flat file:  -->

   <OPERATOR type="export">
      <INPUT    name="xxadeo_rmse_rpas_attr_uda.v"              />
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_1}"    />
      <PROPERTY name="outputfile" value="${OUTPUT_FILE_11}.retl" />
   </OPERATOR>

   <OPERATOR type="export">
      <INPUT    name="xxadeo_rmse_rpas_attr_cfa.v"              />
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_1}"    />
      <PROPERTY name="outputfile" value="${OUTPUT_FILE_12}.retl" />
   </OPERATOR>

   <OPERATOR type="export">
      <INPUT    name="xxadeo_rmse_rpas_itemattr_uda.v"          />
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_2}"    />
      <PROPERTY name="outputfile" value="${OUTPUT_FILE_21}.retl" />
   </OPERATOR>

   <OPERATOR type="export">
      <INPUT    name="xxadeo_rmse_rpas_itemattr_cfa.v"          />
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_2}"    />
      <PROPERTY name="outputfile" value="${OUTPUT_FILE_22}.retl" />
   </OPERATOR>


</FLOW>

EOF


#-----------------------
#  Execute the RETL flow
#-----------------------

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
check_error_and_clean_before_exit $?


#-----------------------------------------
#  Convert output data files to CSV format
#-----------------------------------------

csv_converter "retlsv-csv" "${OUTPUT_FILE_11}.retl" "${OUTPUT_FILE_11}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${OUTPUT_FILE_12}.retl" "${OUTPUT_FILE_12}"
check_error_and_clean_before_exit $?


csv_converter "retlsv-csv" "${OUTPUT_FILE_21}.retl" "${OUTPUT_FILE_21}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${OUTPUT_FILE_22}.retl" "${OUTPUT_FILE_22}"
check_error_and_clean_before_exit $?


#-----------------------------------------------------------------------------------
#  Log the number of records written to the final output files and add up the total:
#-----------------------------------------------------------------------------------

cat ${OUTPUT_FILE_11} ${OUTPUT_FILE_12} > ${OUTPUT_FILE_1}
cat ${OUTPUT_FILE_21} ${OUTPUT_FILE_22} > ${OUTPUT_FILE_2}

log_num_recs ${OUTPUT_FILE_11}
log_num_recs ${OUTPUT_FILE_12}
log_num_recs ${OUTPUT_FILE_21}
log_num_recs ${OUTPUT_FILE_22}
log_num_recs ${OUTPUT_FILE_1}
log_num_recs ${OUTPUT_FILE_2}

#--------------------
#  Do error checking:
#--------------------

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"


#-------------------------
#  Remove the status file:
#-------------------------

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."
message "xxadeo_rmse_rpas_item_attributes.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${PROGRAM_NAME} ${pgmPID}


#----------------------
#  Cleanup and exit:
#----------------------

check_error_and_clean_before_exit 0 "NO_CHECK"
