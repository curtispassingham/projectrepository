#!/bin/ksh

#############################################################################
#                                                                           #
# Object Name:   xxadeo_rmse_simtar_itemvat.ksh                             #
# Description:   The xxadeo_rmse_simtar_itemvat.ksh program                 #
#                extracts the VAT information from                          #
#                RMS tables and places this data into a flat file           #
#                (tva.csv.rpl) to be loaded into RPAS                       #
#                                                                           #
# Version:       1.5                                                        #
# Author:        CGI                                                        #
# Creation Date: 27/08/2018                                                 #
# Last Modified: 27/11/2018                                                 #
# History:                                                                  #
#               1.0 - Initial version                                       #
#               1.1 - Update the variables for environment, librairies      #
#                     and directory names. We have to use $RETL_OUT         #
#                     to put output files because this IR is                #
#                     an extraction (RMS side)                              #
#               1.2 - update output file name                               #
#               1.3 - update output directory value and output file name    #
#               1.4 - Add new table ITEM_MASTER and filters STATUS = A and  #
#                     ITEM_LEVEL = 1. Also replace the extension .ovr by    #
#                     .rpl                                                  #
#               1.5 - Change the comparison operator for the filter         #
#                     active_date "<=" instead of "=>" {vdate+1}            #
#                                                                           #
#############################################################################

############################ PROGRAM DEFINE #################################
###################     (must be the first define)     ######################

export PROGRAM_NAME="xxadeo_rmse_simtar_itemvat"

################################# INCLUDES ##################################
############### (this section must come after PROGRAM DEFINE) ###############

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

#############################################################################
#  Log start message and define the RETL flow file
#############################################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

DATE_FILENAME=`date +%Y%m%d-%H%M%S`

#############################################################################
#  FUNCTIONS
#############################################################################
function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${OUTPUT_FILE}"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        fi
}

############################  OUTPUT DEFINES ################################
################## (this section must come after INCLUDES) ##################

export O_DATA_DIR_XXADEO="${RETL_OUT}/SIMTAR"

export OUTPUT_FILE=${O_DATA_DIR_XXADEO}/tva.csv.rpl

export SCHEMA_DIR_XXADEO=${RETLforXXADEO}/rfx/schema

export OUTPUT_SCHEMA=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema


#############################################################################
#  Copy the RETL flow to a file to be executed by rfx:
#############################################################################
input_date=""

if [[ $VDATE != [0-9]* ]]; then
   echo "ERROR - VDATE is not valid"
   exit 1
fi

if [ $VDATE -ne "" ];then
   input_date=`date --date="$VDATE + 1 day" '+%y/%m/%d'`
   if [ $? -ne 0 ]; then
     echo "ERROR - VDATE is not valid"
     exit 1
  fi
else
     echo "ERROR - VDATE is empty"
     exit 1
fi

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read the vat data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
         SELECT SUBSTR(it.ITEM, 0, 20)  ARTICLE
               ,uda.UDA_VALUE           BU
               ,MAX(it.VAT_RATE/100)    TVA
          
          FROM ${XXADEO_RMS}.VAT_ITEM     it
              ,${XXADEO_RMS}.VAT_REGION   rg
              ,${XXADEO_RMS}.STORE        s
              ,${XXADEO_RMS}.XXADEO_BU_OU b
              ,${XXADEO_RMS}.UDA_ITEM_LOV uda
              ,${XXADEO_RMS}.ITEM_MASTER  itm
         
         WHERE (1=1)
           AND it.vat_region = rg.vat_region
           AND rg.vat_region = s.vat_region
           AND s.org_unit_id = b.ou
           AND uda.uda_id = 750
           AND uda.uda_value = b.bu
           AND uda.item = it.item
           --AND to_char(it.active_date, 'YY/MM/DD') <= '${input_date}'
           AND uda.item = itm.item
           AND itm.status = 'A'
           AND itm.item_level = 1
           AND it.active_date = (select max(x.active_date) active_date
                                       from vat_item x
                                      where x.vat_region = it.vat_region
                                        and x.item = it.item
                                        and x.active_date <= to_date('${input_date}', 'YY/MM/DD')) 
         GROUP BY it.ITEM
                 ,uda.UDA_VALUE
         ORDER BY ARTICLE
                 ,BU
         ]]>
      </PROPERTY>
      <OUTPUT   name = "vat_data.v"/>
   </OPERATOR>


   <OPERATOR type="convert">
      <INPUT    name="vat_data.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
          <CONVERTSPECS>
                <CONVERT destfield="ARTICLE" sourcefield="ARTICLE">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="TVA" sourcefield="TVA">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="BU" sourcefield="BU" newtype="string">
                        <CONVERTFUNCTION name="string_from_int64"/>
                </CONVERT>
        </CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="vat_data_1.v"/>
   </OPERATOR>


   <!--  Write out the vat data to a flat file:  -->

   <OPERATOR type="export">
      <INPUT    name="vat_data_1.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

#############################################################################
#  Execute the RETL flow that was
#  previously copied into rmse_rpas_wh.xml:
#############################################################################

$RFX_EXE  $RFX_OPTIONS  -f $RETL_FLOW_FILE
exit_stat=$?

message "Program completed. Exit status code: $exit_stat"

#############################################################################
#  Log the number of records written to the final output files
#  and add up the total:
#############################################################################

log_num_recs ${OUTPUT_FILE}

#############################################################################
#  Do error checking on results of the RETL execution:
#############################################################################

checkerror -e $exit_stat -m "Program failed - check $ERR_FILE"

#############################################################################
#  Remove the status file, log the completion message
#  to the log and error files and clean up the files:
#############################################################################

if [ -f $STATUS_FILE ]; then rm $STATUS_FILE; fi

message "Program completed successfully"

#  Cleanup and exit:

check_error_and_clean_before_exit 0 "NO_CHECK"
