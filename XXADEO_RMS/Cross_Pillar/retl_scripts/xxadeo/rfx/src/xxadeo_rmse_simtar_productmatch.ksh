#!/bin/ksh

###############################################################################
#
#  Title: Extraction product match indicator from RMS to SimTar
#
#  Description:
#  The xxadeo_rmse_simtar_productmatch.ksh program extract the product 
#  match information from RMS tables and places the data into a flat file 
#  (XXRMS233_PRODUITMATCHE_YYYYMMDD-HH24MISS.dat) to be be loaded into RPAS 
#  module (SimTar).
#
#  Workfile:		|  xxadeo_rmse_simtar_productmatch.ksh
#  Version:		|  0.3
#  Owner: 		|  Oracle Dev Center - (PVI)
#  Creation Date:	|  20180919-1800
#  Modify Date:		|  20181123-1530
#  ---------------------------------------------------------------------------
#  History: 	0.1 - Initial Release
#		0.2 - Change name of output file to Target System requirement
#		0.3 - Bug Fixing: BUG_699 - The changes are:
#			- Added ITEM_MASTER on the query to get only the items
#			that where status equal 'A' and item level equal '1'
#
#
###############################################################################

################## PROGRAM DEFINE #####################
#########     (must be the first define)     ##########

export PROGRAM_NAME="xxadeo_rmse_simtar_productmatch"

EXE_DATE=`date +"%G%m%d-%H%M%S"`;

#FIELD_SEP="\x1F";

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

# Environment configuration script
. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env	# (Adeo ENV - RMS)

# Script Library Collection
. ${LIB_DIR}/xxadeo_rmse_lib.ksh			# (Adeo ENV - RMS)

##################  INPUT/OUTPUT DEFINES ######################
########### (this section must come after INCLUDES) ###########

# --INPUTS

#export DATA_DIR_IN="${RETL_IN}/CATMAN"

# --Get Name of the last file that has been provided by RMS about Item Translations
#INPUT_FILE_NAME=`ls -t ${DATA_DIR_IN} | grep 'XXRMS219_ITEMMASTERTL_.*\.dat$' | head -1`

#export INPUT_FILE=${DATA_DIR_IN}/${INPUT_FILE_NAME}

#export INPUT_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_itemtranslation.schema

# --OUTPUTS

export DATA_DIR_OUT=${RETL_OUT}/SIMTAR

#export OUTPUT_FILE=${DATA_DIR_OUT}/XXRMS233_PRODUITMATCHE_${EXE_DATE}.dat
export OUTPUT_FILE=${DATA_DIR_OUT}/produitMatche.csv.rpl

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema


######## (this section must come after FUNCTIONS) ########

######################################################################
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
######################################################################
function USAGE
{
   echo "USAGE: . $pgmName "
}

######################################################################
# Function Name: PROCESS
# Purpose      : Defines the flow of the program should be invoked
######################################################################
function PROCESS
{
####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

#message "The input file is "${INPUT_FILE}

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

###############################################################################
#  Convert input CSV file to data file format
###############################################################################

# -- The default field delimiter: delimiter="0x1F"
# -- With parameter csv-retlsv, convert file from CSV to DATA file.
#csv_converter "csv-retlsv" "${INPUT_FILE}" "${INPUT_FILE}.retl"
#checkerror -e $? -m "Convert from CSV file to Data file failed - check $ERR_FILE"


########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">
    ${DBREAD}
	<PROPERTY name="query">
	<![CDATA[
		SELECT
    			ITEM_LOV.ITEM,
    			ITEM_LOV.UDA_VALUE AS BU,
    			COUNT(DISTINCT PRICE_HIST.COMP_STORE) AS COMP_STORE_COUNT
		FROM ${XXADEO_RMS}.XXADEO_MOM_DVM MOM_DVM, 
     		     ${XXADEO_RMS}.UDA_ITEM_LOV ITEM_LOV,
     		     ${XXADEO_RMS}.COMP_PRICE_HIST PRICE_HIST,
		     ${XXADEO_RMS}.ITEM_MASTER ITEM_M
		WHERE MOM_DVM.PARAMETER = 'BU_SUBSCRIPTION'
  		  AND MOM_DVM.VALUE_1 = ITEM_LOV.UDA_ID
		  AND ITEM_M.STATUS = 'A'
		  AND ITEM_M.ITEM_LEVEL = 1
		  AND ITEM_M.ITEM = ITEM_LOV.ITEM
  		  AND ITEM_LOV.ITEM = PRICE_HIST.ITEM
		GROUP BY ITEM_LOV.ITEM, ITEM_LOV.UDA_VALUE
	]]> 
	</PROPERTY>
	<OUTPUT name="product_match.v"/>
    </OPERATOR>

    <OPERATOR type="convert">
	<INPUT name="product_match.v"/>
	<PROPERTY name="convertspec">
	<![CDATA[
		<CONVERTSPECS>
			<CONVERT destfield="NR_COMP_STORE" sourcefield="COMP_STORE_COUNT"
				newtype="int64">
				<CONVERTFUNCTION name="int64_from_dfloat"/>
				<TYPEPROPERTY name="nullable" value="true"/>
			</CONVERT>
		</CONVERTSPECS>
	]]>
	</PROPERTY>
	<OUTPUT name="product_match_convert.v"/>
    </OPERATOR>

    <OPERATOR type="export">
        <INPUT    name="product_match_convert.v"/>
        <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
        <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
     </OPERATOR>
</FLOW>

EOF

###############################################
#  Execute the RETL flow
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
checkerror -e $? -m "RETL flow failed - check $ERR_FILE"


###############################################################################
#  Convert output data files to CSV format
###############################################################################

# -- The default field delimiter: delimiter="0x1F"
#csv_converter "retlsv-csv" "${OUTPUT_FILE}.retl" "${OUTPUT_FILE}"
#checkerror -e $? -m "Convert from Data file to CSV file failed - check $ERR_FILE"

###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${OUTPUT_FILE}

#######################################
#  Do error checking:
#######################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#  Remove the status file:

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."
}

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
######################################################################
#     MAIN
######################################################################
#////////////////////////////////////////////////////////////////////

#----------------------------------------------
#  Log start message and define the RETL flow file
#----------------------------------------------

if [[ $# -gt 0  ]]; then
   USAGE
   rmse_terminate 1
else
  PROCESS
fi

# --Clean

message  "${PROGRAM_NAME}.ksh  - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

rmse_terminate 0
