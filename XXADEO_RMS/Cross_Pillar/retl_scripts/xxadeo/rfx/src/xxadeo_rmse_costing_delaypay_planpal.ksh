#!/bin/ksh

#####################################################################################
#                                                                                   #
# Object Name:  xxadeo_rmse_costing_delaypay_planpal.ksh                            #
# Description:  The xxadeo_rmse_costing_delaypay_planpal.ksh program                #
#               extracts from RMS tables the plan pal, delay payment                #
#               information to generate 2 output files provided to RPAS             #
#                                                                                   #
# Version:       2.3                                                                #
# Author:        CGI                                                                #
# Creation Date: 18/03/2018                                                         #
# Last Modified: 15/11/2018                                                         #
# History:                                                                          #
#               1.0 - Initial version                                               #
#               1.1 - Update the variables for environment, librairies              #
#                     and directory names. We have to use $RETL_OUT                 #
#                     to put output files because this IR is an extraction (RMS)    #
#               1.2 - Update the output filenames and change the output             #
#                     folder from RPAS to CATMAN (as CATMAN, SIMTAR AND COSTING     #
#                     use the same domain on RPAS side)                             #
#               2.0 - Delete an output file on the script, modify the               #
#                     query of plan_pal file and modify the name of the ksh.        #
#               2.1 - Update O_DATA_DIR_XXADEO (RPAS to COSTING)                    #
#                     Add filter Status=A and item_level=1 on the queries           #
#               2.2 - Add DISTINCT to the request and delete the conversion         #
#                     centimeter to meter.                                          #
#               2.3 - Add new filter PRIMARY_COUNTRY_IND on table ITEM_SUPP_COUNTRY
#               2.4 - ADD a filter to export only orders suppliers
#                                                                                   #
#####################################################################################

###############################################################################
# Script extracts Supplier, Item Supplier and Item Supplier Country
# information from the RMS and then generate 2 flat files to RPAS
#
# Filenames generated are:
#       - XXOR330_DELAYPAYMENT_YYYYMMDD-HHMMSS.dat (Destination: SimTar) --> replaced by delay_pay.csv.rpl
#       - XXOR330_PLANPAL_YYYYMMDD-HHMMSS.dat      (Destination: SimTar) --> replaced by plan_pal.csv.rpl
#
###############################################################################

###############################################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
###############################################################################

export PROGRAM_NAME='xxadeo_rmse_costing_delaypay_planpal'
RFX_EXE=${RFX_EXE:=rfx}


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export O_DATA_DIR_XXADEO="${RETL_OUT}/COSTING"

# OUTPUT FILES in outbound
export OUTPUT_FILE_1="${O_DATA_DIR_XXADEO}/plan_pal.csv.rpl"
export OUTPUT_FILE_2="${O_DATA_DIR_XXADEO}/delay_pay.csv.rpl"

# OUTPUT SCHEMA files
export SCHEMA_DIR_XXADEO=${RETLforXXADEO}/rfx/schema
export OUTPUT_SCHEMA_1=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_costing_planpal.schema
export OUTPUT_SCHEMA_2=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_costing_delaypayment.schema


###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${OUTPUT_FILE_1}.retl"
                rm -f "${OUTPUT_FILE_2}.retl"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
                rm -f "${OUTPUT_FILE_1}.retl"
                rm -f "${OUTPUT_FILE_2}.retl"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
                rmse_terminate $1
        fi
}



####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."


###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

        ${DBREAD}
                <PROPERTY name = "query">
                <![CDATA[
                 select
                     supplier_dim.ARTICLE,
                     supplier_bu.BU,
                     supplier_dim.SUPPLIER_SITE,
                     supplier_dim.EACH_WIDTH,
                     supplier_dim.EACH_LENGTH,
                     supplier_dim.EACH_HEIGHT,
                     supplier_dim.EACH_WEIGHT,
                     supplier_dim.INNER_WIDTH,
                     supplier_dim.INNER_LENGTH,
                     supplier_dim.INNER_HEIGHT,
                     supplier_dim.INNER_WEIGHT,
                     supplier_dim.CASE_WIDTH,
                     supplier_dim.CASE_LENGTH,
                     supplier_dim.CASE_HEIGHT,
                     supplier_dim.CASE_WEIGHT,
                     supplier_dim.PALLET_WIDTH,
                     supplier_dim.PALLET_LENGTH,
                     supplier_dim.PALLET_HEIGHT,
                     supplier_dim.PALLET_WEIGHT,
                     supplier_dim.NB_OF_ARTICLES_IN_THE_PALLET,
                     supplier_dim.NB_OF_ARTICLES_IN_THE_CASE,
                     supplier_dim.NB_OF_ARTICLES_IN_THE_INNER
                 from
                     (
                         SELECT
                             im.item ARTICLE
                             , s.supplier SUPPLIER_SITE
                             -- Eaches content
                             , isd_ea.width EACH_WIDTH
                             , isd_ea.length EACH_LENGTH
                             , isd_ea.height EACH_HEIGHT
                             , isd_ea.weight EACH_WEIGHT
                             -- Inner content
                             , isd_inner.width INNER_WIDTH
                             , isd_inner.length INNER_LENGTH
                             , isd_inner.height INNER_HEIGHT
                             , isd_inner.weight INNER_WEIGHT
                             -- Case content
                             , isd_case.width CASE_WIDTH
                             , isd_case.length CASE_LENGTH
                             , isd_case.height CASE_HEIGHT
                             , isd_case.weight CASE_WEIGHT
                             -- Pallet content
                             , isd_pallet.width PALLET_WIDTH
                             , isd_pallet.length PALLET_LENGTH
                             , isd_pallet.height PALLET_HEIGHT
                             , isd_pallet.weight PALLET_WEIGHT
                             -- Number of units (at Item / Supp / Country level)
                             , isc.TI*isc.HI*isc.SUPP_PACK_SIZE NB_OF_ARTICLES_IN_THE_PALLET
                             , isc.SUPP_PACK_SIZE NB_OF_ARTICLES_IN_THE_CASE
                             , isc.INNER_PACK_SIZE NB_OF_ARTICLES_IN_THE_INNER
                         FROM  ${XXADEO_RMS}.ITEM_SUPP_COUNTRY_DIM isd_ea
                             , ${XXADEO_RMS}.ITEM_SUPP_COUNTRY_DIM isd_inner
                             , ${XXADEO_RMS}.ITEM_SUPP_COUNTRY_DIM isd_case
                             , ${XXADEO_RMS}.ITEM_SUPP_COUNTRY_DIM isd_pallet
                             , ${XXADEO_RMS}.item_supp_country isc
                             , ${XXADEO_RMS}.ITEM_MASTER im
                             , ${XXADEO_RMS}.sups s
                         WHERE
                         -- Dimmensions for Eaches
                             isd_ea.item (+)= im.item
                             AND isd_ea.supplier (+)= s.supplier
                             AND isd_ea.dim_object (+)= 'EA'
                             and isd_ea.origin_country (+)= isc.origin_country_id
                             -- Dimmensions for Inner
                             AND isd_inner.item (+)= im.item
                             AND isd_inner.supplier (+)= s.supplier
                             AND isd_inner.dim_object (+)= 'IN'
                             and isd_inner.origin_country (+)= isc.origin_country_id
                             -- Dimmensions for Cases
                             AND isd_case.item (+)= im.item
                             AND isd_case.supplier (+)= s.supplier
                             AND isd_case.dim_object (+)= 'CA'
                             and isd_case.origin_country (+)= isc.origin_country_id

                             -- Dimmensions for Pallets
                             AND isd_pallet.item (+)= im.item
                             AND isd_pallet.supplier (+)= s.supplier
                             AND isd_pallet.dim_object (+)= 'PA'
                             and isd_pallet.origin_country (+)= isc.origin_country_id
                             -- Quantities
                             and isc.item = im.item
                             and isc.supplier = s.supplier
                             -- Primary country for the item/supplier
                             and isc.primary_country_ind = 'Y'
                             -- Supplier Sites Only
                             and s.supplier_parent is not null
                             -- Approved and Level 1 items only
                             AND im.status = 'A'
                             AND im.item_level = 1
                             AND exists (select 'Y' from ${XXADEO_RMS}.addr adr where adr.module = 'SUPP' and adr.addr_type = '04' and adr.key_value_1 = s.supplier)
                             AND not exists (select '1' from ${XXADEO_RMS}.addr where module = 'SUPP' and addr_type = '06' and key_value_1 = s.supplier)
                     ) supplier_dim,
                     (
                     -- Join Supplier with Partner to then join with the BU OU
                         select distinct s.supplier, bo.bu BU
                         from
                         ${XXADEO_RMS}.sups s, ${XXADEO_RMS}.partner_org_unit pou, ${XXADEO_RMS}.xxadeo_bu_ou bo
                         where
                         s.supplier = pou.partner
                         and pou.org_unit_id = bo.ou
                     ) supplier_bu
                     where
                         supplier_dim.supplier_site = supplier_bu.supplier
                  ]]>
                </PROPERTY>
                <OUTPUT name = "o_planpal.v"/>
         </OPERATOR>


        <OPERATOR type="convert">
                <INPUT    name="o_planpal.v"/>
                <PROPERTY name="convertspec">
                        <![CDATA[
                                <CONVERTSPECS>
                                        <CONVERT destfield="BU" sourcefield="BU">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                                </CONVERTFUNCTION>
                                                <CONVERTFUNCTION     name="string_from_int64"/>
                                        </CONVERT>
                                        <CONVERT destfield="PALLET_WIDTH" sourcefield="PALLET_WIDTH">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="PALLET_LENGTH" sourcefield="PALLET_LENGTH">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="PALLET_HEIGHT" sourcefield="PALLET_HEIGHT">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="PALLET_WEIGHT" sourcefield="PALLET_WEIGHT">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="EACH_WIDTH" sourcefield="EACH_WIDTH">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="EACH_LENGTH" sourcefield="EACH_LENGTH">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="EACH_HEIGHT" sourcefield="EACH_HEIGHT">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="EACH_WEIGHT" sourcefield="EACH_WEIGHT">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="INNER_WIDTH" sourcefield="INNER_WIDTH">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="INNER_LENGTH" sourcefield="INNER_LENGTH">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="INNER_HEIGHT" sourcefield="INNER_HEIGHT">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="INNER_WEIGHT" sourcefield="INNER_WEIGHT">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="CASE_WIDTH" sourcefield="CASE_WIDTH">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="CASE_LENGTH" sourcefield="CASE_LENGTH">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="CASE_HEIGHT" sourcefield="CASE_HEIGHT">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="CASE_WEIGHT" sourcefield="CASE_WEIGHT">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="0.0001"/>
                                                </CONVERTFUNCTION>
                                        </CONVERT>
                                        <CONVERT destfield="NB_OF_ARTICLES_IN_THE_PALLET" sourcefield="NB_OF_ARTICLES_IN_THE_PALLET">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                                </CONVERTFUNCTION>
                                                <CONVERTFUNCTION     name="int64_from_dfloat"/>
                                        </CONVERT>
                                        <CONVERT destfield="NB_OF_ARTICLES_IN_THE_CASE" sourcefield="NB_OF_ARTICLES_IN_THE_CASE">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                                </CONVERTFUNCTION>
                                                <CONVERTFUNCTION     name="int64_from_dfloat"/>
                                        </CONVERT>
                                        <CONVERT destfield="NB_OF_ARTICLES_IN_THE_INNER" sourcefield="NB_OF_ARTICLES_IN_THE_INNER">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                                </CONVERTFUNCTION>
                                                <CONVERTFUNCTION     name="int64_from_dfloat"/>
                                        </CONVERT>
                                </CONVERTSPECS>
                        ]]>
                </PROPERTY>
                <OUTPUT name="o_planpal_2.v"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT    name="o_planpal_2.v"/>
                <PROPERTY name="outputfile" value="${OUTPUT_FILE_1}"/>
                <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_1}"/>
        </OPERATOR>



        ${DBREAD}
                <PROPERTY name = "query">
                <![CDATA[
                       select s.supplier site_fournisseur
                            , bu.bu bu
                            , s.terms payment_terms
                        from ${XXADEO_RMS}.xxadeo_bu_ou bu
                           , ${XXADEO_RMS}.sups s
                           , ${XXADEO_RMS}.partner_org_unit p
                        where (1=1)
                          and s.supplier = p.partner
                          and bu.ou = p.org_unit_id
                          and exists (select 'Y' from ${XXADEO_RMS}.addr adr where adr.module = 'SUPP' and adr.addr_type = '04' and adr.key_value_1 = s.supplier)
                          and not exists (select '1' from ${XXADEO_RMS}.addr where module = 'SUPP' and addr_type = '06' and key_value_1 = s.supplier)
                       group by s.supplier, bu.bu, s.terms
                ]]>
                </PROPERTY>
                <OUTPUT name = "o_delaypay.v"/>
        </OPERATOR>

        <OPERATOR type="convert">
                <INPUT name="o_delaypay.v"/>
                <PROPERTY name="convertspec">
                        <![CDATA[
                                <CONVERTSPECS>
                                        <CONVERT destfield="BU" sourcefield="BU">
                                                <CONVERTFUNCTION     name="make_not_nullable">
                                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                                </CONVERTFUNCTION>
                                                <CONVERTFUNCTION     name="string_from_int64"/>
                                        </CONVERT>
                                </CONVERTSPECS>
                        ]]>
                </PROPERTY>
                <OUTPUT name="o_delaypay_1.v"/>
        </OPERATOR>

   <OPERATOR type="export">
        <INPUT    name="o_delaypay_1.v"/>
        <PROPERTY name="outputfile" value="${OUTPUT_FILE_2}"/>
        <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_2}"/>
   </OPERATOR>

</FLOW>

EOF



###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?

echo "fin flow"
###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${OUTPUT_FILE_1}
log_num_recs ${OUTPUT_FILE_2}


###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check $ERR_FILE"

###############################################################################
# Remove the status file
###############################################################################

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

###############################################################################
#  Report success!
###############################################################################
message "Program completed successfully"

###############################################################################
# cleanup and exit
###############################################################################

check_error_and_clean_before_exit 0 "NO_CHECK"

