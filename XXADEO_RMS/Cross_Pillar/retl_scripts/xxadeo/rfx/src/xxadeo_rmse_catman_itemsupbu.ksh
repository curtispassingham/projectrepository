#!/bin/ksh

######################################################################################
#                                                                                    #
# Object Name:   xxadeo_rmse_catman_itemsupbu.ksh                                    #
# Description:   The program extracts the links between item/suppliers/BU from       #
#                RMS tables and places this data into 2 flat files                   #
#                (art_Four_BU.csv.rpl / art_Four.csv.rpl) to be be loaded into RPAS. #
#                                                                                    #
# Version:       2.1                                                                 #
# Author:        CGI                                                                 #
# Creation Date: 29/06/2018                                                          #
# Last Modified: 02/11/2018                                                          #
# History:                                                                           #
#               1.0 - Initial version                                                #
#               1.1 - Update the variables for environment, librairies               #
#                     and directory names. We have to use $RETL_OUT                  #
#                     to put output files because this IR is                         #
#                     an extraction (RMS side)                                       #
#               2.0 - CV027 modification                                             #
#                                                                                    #
######################################################################################

################## PROGRAM DEFINE #####################
#########     (must be the first define)     ##########

export PROGRAM_NAME="xxadeo_rmse_catman_itemsupbu"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh


####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

SCHEMA_DIR_XXADEO=${RETLforXXADEO}/rfx/schema

O_DATA_DIR_XXADEO=$RETL_OUT/CATMAN

export OUTPUT_FILE_ISB=${O_DATA_DIR_XXADEO}/art_Four_BU.csv.rpl
export OUTPUT_SCHEMA_ISB=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_catman_itemsupbu.schema
export OUTPUT_FILE_IS=${O_DATA_DIR_XXADEO}/art_Four.csv.rpl
export OUTPUT_SCHEMA_IS=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_catman_itemsup.schema

###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
    error_check=1
    if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
    then
        message "ERROR: Parameter 2 is not valid: ${2}"
        message "    Parameter 2 must be true, false or empty (true)"
        rmse_terminate 1
    elif [[ "${2}" = "NO_CHECK" ]]
    then
        error_check=0
    fi
    #message "check_error_and_clean_before_exit: error_check: ${error_check}"

    if [[ $1 -ne 0 && error_check -eq 1 ]]
    then
        rm -f "${OUTPUT_FILE_ISB}.retl"
                rm -f "${OUTPUT_FILE_IS}.retl"
        checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
    elif [[ error_check -eq 0 ]]
    then
        rm -f "${OUTPUT_FILE_ISB}.retl"
                rm -f "${OUTPUT_FILE_IS}.retl"
        checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        rmse_terminate $1
    fi
}


########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read the item/supplier/BU data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            SELECT its.item as "ARTICLE",
                   its.supplier as "FOURNISSEUR",
                   xbo.BU as "BU",
                   'TRUE' as "STATUS"
              FROM ${XXADEO_RMS}.xxadeo_bu_ou xbo,
                   ${XXADEO_RMS}.partner_org_unit pou,
                   ${XXADEO_RMS}.item_supplier its,
                   ${XXADEO_RMS}.item_master itm
             WHERE xbo.ou = pou.org_unit_id
               AND pou.partner = its.supplier
               AND pou.partner_type = 'U'
               AND itm.item_level = '1'
               AND itm.status = 'A'
               AND its.item = itm.item
         ]]>
      </PROPERTY>
      <OUTPUT   name = "isb_data.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
     <INPUT name="isb_data.v"/>
     <PROPERTY name="convertspec">
       <![CDATA[
         <CONVERTSPECS>
           <CONVERT destfield="BU" sourcefield="BU">
           <CONVERTFUNCTION name="make_not_nullable">
             <FUNCTIONARG name="nullvalue" value="-1"/>
           </CONVERTFUNCTION>
           <CONVERTFUNCTION name="string_from_int64"/>
           </CONVERT>
           <CONVERT destfield="STATUS" sourcefield="STATUS">
           <CONVERTFUNCTION name="make_not_nullable">
             <FUNCTIONARG name="nullvalue" value="-1"/>
           </CONVERTFUNCTION>
           </CONVERT>
         </CONVERTSPECS>
       ]]>
     </PROPERTY>
     <OUTPUT name="isb_data_1.v"/>
   </OPERATOR>

   <!--  Write out the wh data to a flat file:  -->

   <OPERATOR type="export">
      <INPUT    name="isb_data_1.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE_ISB}.retl"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_ISB}"/>
   </OPERATOR>


   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            SELECT its_cfa.item as "ARTICLE",
                   its_cfa.supplier as "FOURNISSEUR",
                   its_cfa.varchar2_1 as "STATUS"
              FROM ${XXADEO_RMS}.xxadeo_mom_dvm xmd,
                   ${XXADEO_RMS}.cfa_attrib cfa,
                   ${XXADEO_RMS}.item_supplier_cfa_ext its_cfa,
                   ${XXADEO_RMS}.partner_org_unit pou,
                   ${XXADEO_RMS}.xxadeo_bu_ou xbo,
                   ${XXADEO_RMS}.item_master itm
             WHERE xmd.value_1 = TO_CHAR(cfa.attrib_id)
               AND cfa.group_id = its_cfa.group_id
               AND its_cfa.supplier = pou.partner
               AND pou.org_unit_id = xbo.ou
               AND itm.item = its_cfa.item
               AND pou.partner_type = 'U'
               AND xmd.func_area = 'CFA'
               AND xmd.parameter = 'LINK_STATUS_ITEM_SUPP'
               AND itm.item_level = '1'
               AND itm.status = 'A'
         ]]>
      </PROPERTY>
      <OUTPUT   name = "is_data.v"/>
   </OPERATOR>


   <OPERATOR type="convert">
     <INPUT name="is_data.v"/>
     <PROPERTY name="convertspec">
       <![CDATA[
         <CONVERTSPECS>
           <CONVERT destfield="STATUS" sourcefield="STATUS">
           <CONVERTFUNCTION name="make_not_nullable">
             <FUNCTIONARG name="nullvalue" value="-1"/>
           </CONVERTFUNCTION>
           </CONVERT>
         </CONVERTSPECS>
       ]]>
     </PROPERTY>
     <OUTPUT name="is_data_1.v"/>
   </OPERATOR>




   <!--  Write out the wh data to a flat file:  -->

   <OPERATOR type="export">
      <INPUT    name="is_data_1.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE_IS}.retl"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_IS}"/>
   </OPERATOR>


</FLOW>

EOF

###############################################
#  Execute the RETL flow that had previously
#  been copied into xxadeo_rmse_rpas_itemsupbu.xml:
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
check_error_and_clean_before_exit $?


###############################################################################
#  Convert output data files to CSV format
###############################################################################

csv_converter "retlsv-csv" "${OUTPUT_FILE_ISB}.retl" "${OUTPUT_FILE_ISB}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${OUTPUT_FILE_IS}.retl" "${OUTPUT_FILE_IS}"
check_error_and_clean_before_exit $?

###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${OUTPUT_FILE_ISB}
log_num_recs ${OUTPUT_FILE_IS}

#######################################
#  Do error checking:
#######################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#  Remove the status file:

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."

#  Cleanup and exit:
check_error_and_clean_before_exit 0 "NO_CHECK"
