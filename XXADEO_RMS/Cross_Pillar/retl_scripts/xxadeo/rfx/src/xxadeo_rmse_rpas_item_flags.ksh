#!/bin/ksh

########################################################
# This custom ADEO script extracts the Item Flags
#  Version: 1.1
#
#  History: 1.0 - Initial Release
#           1.1 - Add filter item status = 'A'
#
###############################################################################


########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################

export PROGRAM_NAME='xxadeo_rmse_rpas_item_flags'
export extractDate=`date +"%Y%m%d-%H%M%S"`

########################################################
#  INCLUDES ########################
#  This section must come after PROGRAM DEFINES
########################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

########################################################
#  OUTPUT DEFINES
#  This section must come after INCLUDES
########################################################

export OUTPUT_FILE=$RETL_OUT/RPAS/XXRMS214_ITEMFLAGS_${extractDate}.dat
export OUTPUT_SCHEMA=$SCHEMA_DIR/${PROGRAM_NAME}.schema

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF
   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query" >
                <![CDATA[
               select im.PACK_IND,
                      im.ITEM_AGGREGATE_IND,
                      im.DIFF_1_AGGREGATE_IND,
                      im.DIFF_2_AGGREGATE_IND,
                      im.DIFF_3_AGGREGATE_IND,
                      im.DIFF_4_AGGREGATE_IND,
                      im.PRIMARY_REF_ITEM_IND,
                      im.MERCHANDISE_IND,
                      im.FORECAST_IND,
                      im.CATCH_WEIGHT_IND,
                      im.CONST_DIMEN_IND,
                      im.SIMPLE_PACK_IND,
                      im.CONTAINS_INNER_IND,
                      im.SELLABLE_IND,
                      im.ORDERABLE_IND,
                      im.GIFT_WRAP_IND,
                      im.SHIP_ALONE_IND,
                      im.CHECK_UDA_IND,
                      im.ITEM_XFORM_IND,
                      im.INVENTORY_IND,
                      im.PERISHABLE_IND,
                      im.SOH_INQUIRY_AT_PACK_IND,
                      im.NOTIONAL_PACK_IND,
                      im.ITEM
                 from ${XXADEO_RMS}.item_master im
                where im.status = 'A'
            ]]>
         </PROPERTY>
         <OUTPUT   name = "output.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <INPUT    name="output.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
      </OPERATOR>
   </FLOW>
EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
exit_stat=$?


###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check $ERR_FILE"

#######################################################
#  Remove the status file, log the completion message
#  to the log and error files and clean up the files:
#######################################################

if [ -f $STATUS_FILE ]; then rm $STATUS_FILE; fi

message "Program completed successfully"

#  Cleanup and exit:

rmse_terminate 0

