#!/bin/ksh

###############################################################################
#  $Workfile:   xxadeo_rmse_rpo_competitor_prices.ksh
#  $Modtime:    Apr 30 2018
#  Version:    1.4
#
#  History: 1.0 - Initial Release
#                       1.1 - Adjustments to the new file structure RETLforXXADEO,
#                                 DATA_DIR and XXADEO_RMS
#                       1.2 - Change 001
#                       1.3 - Replace get_vdate() by $VDATE
#                       1.4 - Add filter item status = 'A'
#
#  001 - Change to adapt to the new price zone sctructure.
#
###############################################################################

###############################################################################
# Script extracts competitor prices (min and target)
# information per each zone_id and item
# from the RMS to RPAS
#
# This script has to generate 2 files:
#      - ol1cmpcurpc.csv.ovr
#      - ol1cmpcchkdt.csv.ovr
###############################################################################

###############################################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
###############################################################################

export PROGRAM_NAME="xxadeo_rmse_rpo_competitor_prices"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${OUTPUT_FILE}"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
                rm -f "${OUTPUT_FILE}"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
                rmse_terminate $1
        fi
}

####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export DATA_DIR="${RETL_OUT}/RPO"

# OUTPUT files in outbound
export OUTPUT_FILE_CMP_PC="${DATA_DIR}/ol1cmpcurpc.csv.ovr_`date +%Y%m%d-%H%M%S`.dat"
export OUTPUT_FILE_CMP_CHK_DT="${DATA_DIR}/ol1cmppcchkdt.csv.ovr_`date +%Y%m%d-%H%M%S`.dat"

# OUTPUT TMP files
export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema



###############################################################################
#  Create a disk-based flow file
###############################################################################


cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

   ${DBREAD}
        <PROPERTY name = "query">
          <![CDATA[
               --COMPETITOR_PRICE
               WITH COMPETITOR_PRICE AS (
                select cs.item   
                     , cs.competitor
                     , cs.comp_store
                     , cs.rec_date
                     , cs.comp_retail 
                 from ${XXADEO_RMS}.item_master im,
                      ${XXADEO_RMS}.comp_shop_list  cs
                where (1=1)
                  and im.item = cs.item
                  and to_char(cs.rec_date, 'YYYYMMDD') = $VDATE
                  and im.status = 'A'
                  and exists (select 1
                                from comp_store_link           csl
                                   , rpm_zone_location         zl
                                   , xxadeo_rpm_bu_price_zones pzb
                               where (1=1)
                                 and csl.store = zl.location
                                 and zl.zone_id = pzb.zone_id
                                 and pzb.bu_zone_type in (select dvm.value_1
                                                            from xxadeo_mom_dvm dvm
                                                           where (1=1)
                                                             and dvm.func_area = 'RPO_ZONES'
                                                             and dvm.parameter = 'RPO_ALLOWED_ZONE_TYPE'))
               ),
               --ZONE_DATA
               ZONE_DATA AS (
                select distinct rzp.item
                     , rz.zone_id 
                     , rz.name   
                     , csl.comp_store
                  from ${XXADEO_RMS}.xxadeo_rpm_bu_price_zones rbp
                     , ${XXADEO_RMS}.rpm_zone                  rz
                     , ${XXADEO_RMS}.rpm_item_zone_price       rzp
                     , ${XXADEO_RMS}.rpm_zone_location         rzl
                     , ${XXADEO_RMS}.comp_store_link           csl
                     , ${XXADEO_RMS}.comp_shop_list            cs
                     , ${XXADEO_RMS}.item_master               im
                 where (1=1)
                   and rbp.zone_id  = rz.zone_id
                   and rz.zone_id   = rzp.zone_id
                   and rz.zone_id   = rzl.zone_id
                   and rzl.location = csl.store
                   and rzp.item     = cs.item
                   and cs.item      = im.item
                   and im.status = 'A'
               )
               --QUERY
               SELECT A.ITEM        as ITEM_ID
                    , A.COMPETITOR  as COMPETITOR_ID
                    , B.ZONE_ID     as PRICE_ZONE_ID 
                    , A.COMP_RETAIL as COMP_CURRENT_PRICE
                    , A.REC_DATE    as COMP_CURRENT_LAST_DATE_CCK
                    , B.NAME        as BU_NAME 
                 FROM COMPETITOR_PRICE A
                    , ZONE_DATA        B
                WHERE (1=1)
                  AND A.ITEM = B.ITEM
                  AND A.COMP_STORE = B.COMP_STORE
               ORDER BY B.NAME
                      , B.ZONE_ID 
                      , A.COMPETITOR
                      , A.ITEM 
                      , A.COMP_RETAIL
          ]]>
        </PROPERTY>
        <OUTPUT name = "o_comp_prices.v"/>
   </OPERATOR>
   
   <!-- Use a convert operator to make not nullable some fields that are strings from the query  -->
   <OPERATOR type="convert">
      <INPUT    name="o_comp_prices.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
          <CONVERTSPECS>
                <CONVERT destfield="ITEM_ID" sourcefield="ITEM_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
        </CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="o_comp_prices2.v"/>
   </OPERATOR>

   <OPERATOR type="export">
        <INPUT    name="o_comp_prices2.v"/>
        <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
        <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
check_error_and_clean_before_exit $?
###############################################################################
#  Convert output data files to CSV format
###############################################################################

#csv_convert "retlsv-csv" "${OUTPUT_FILE}.retl" "${OUTPUT_FILE}"
#check_error_and_clean_before_exit $?

###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################
#log_num_recs ${RETL_OUTPUT_FILE}


################## GLOBAL VARIABLES #####################
#########           Initialisation             ##########
idx_zone_comp_item_new=""
idx_zone_comp_item_old=""
bu_old=""
bu_new=""
item_old=""
competitor_old=""
zone_id_old=""
date_old=""
i=0
target_price=""

typeset -A TAB_PRICES
typeset -A ParamBU


##################################################################################
#
#       FUNCTION 1
#   This function will extract the min prices for each zone_id, competitor, item
#   The input file is the file generated by the RETL, this file is sorted by
#   zone_id, competitor, item and prices. As the input file is well sorted
#   we have to extract the first line when the couple zone_id, competitor and item
#   is different.
#
##################################################################################
get_min_prices() {
        if [ -e ${DATA_DIR}/file_min.txt ] ; then
                rm ${DATA_DIR}/file_min.txt
        fi

                                cat "${OUTPUT_FILE}" |
                                while IFS=, read item competitor zone_id price date bu
                                do
                                                idx_zone_comp_item_new=$zone_id$competitor$item
                                                if [[ $idx_zone_comp_item_old != $idx_zone_comp_item_new ]]; then
                                                                echo "$item,$competitor,$zone_id,$price,$date,_min" >> "${DATA_DIR}/file_min.txt"
                                                                idx_zone_comp_item_old=$zone_id$competitor$item
                                                fi
                                done

}



####################################################################################
#
#       FUNCTION 2
#   This function will extract the target prices for each zone_id, competitor, item
#   The input file is the file generated by the RETL, this file is sorted by
#   zone_id, competitor, item and prices. As the input file is well sorted
#   we have to retrieve prices for each couple of zone_id, competitor and item.
#   Then we have to retrieve the target price by using the configuration file
#   that contains the parameters (A, B, C and D) for each BU
#
####################################################################################


####################################################################################
#
#  Function that allow Retrieve parameters for each BUs and copy value from an array.
#  Parameters are available in a file xxadeo_rmse_rpo_competitor_prices_config
#  Here is a sample of the format file |BU_ID|PARAMETER_ID|VALUE|
#
#  Array information:
#       index --> concatenation of BU_ID and PARAMETER_ID
#       value --> VALUE
#
####################################################################################
get_price_fct2() {
        ######################## VARAIBLES ########################
        #############              Initialisation    ##################
        valA=""
        valB=""
        valC=""
        valD=""
        caseC=false
        caseD=false
        price_avg=""
        price_most_freq=""
        price_min=""
        target_price=""

        ##################################################################################
        #
        #  Retrieve the param for the BU by using the $1 of this function
        #
        ##################################################################################
        #echo "nb element paramBU " ${#ParamBU[@]}
        if [[ ${#ParamBU[@]} -gt 0 ]]; then
                for elem in ${!ParamBU[*]} ; do
                        if [ ${elem:0:4} = $1 ]; then
                                case ${elem:5:6} in
                                        A) valA=${ParamBU[${elem}]};;
                                        B) valB=${ParamBU[${elem}]};;
                                        C) valC=${ParamBU[${elem}]};;
                                        D) valD=${ParamBU[${elem}]};;
                                        *) valD=1 ;;
                                esac
                        else
                                valD=1
                        fi
                done
        else
                valD=1
        fi
        message "Param A : ${valA}  Param B : ${valB}  Param C : ${valC}  Param D : ${valD}  target_price ${target_price} and bu  $1"

        ##################################################################################
        #
        #       Copy each prices into a file that will be used to manage:
        #       Min price
        #       Average
        #       Most Frequent
        #
        ##################################################################################
        for elem in ${!TAB_PRICES[*]} ; do
                echo "${TAB_PRICES[${elem}]}" >> file_tmp.txt
        done

        ######################## AVERAGE ########################
        ############  input file used file_tmp.txt  #############
        if [[ $valC -eq 2 || $valC -eq 3 || $valD -eq 2 || $valD -eq 4 || $valD -eq 5 ]]; then
                price_avg=$(echo $(awk "BEGIN {print ($(awk '{s+=$1} END {print s}' file_tmp.txt)/${#TAB_PRICES[*]})}")| sed "s/\([0-9]*\.[0-9][0-9]\).*/\1/") # --> regex used to retrieve only 2 num after the dot
                price_avg=$(echo $price_avg | awk '{$1=sprintf(fmt,$1)}1' fmt='%0.6f') # --> regex to add the remaining padding with zeros
                echo "AVERAGE : " $price_avg
        fi

        ##################  MOST FREQUENT  ######################
        ############  input file used file_tmp.txt  #############
        cat file_tmp.txt | sort > file_sorted.txt                                               # --> file_sorted.txt will be reused to define the min price later
        cat file_sorted.txt | uniq -c | sort -k1nr,1nr -k2n,2n > file_MF_Sorted.txt
        price_most_freq=$(sed '1q;d' file_MF_Sorted.txt | awk '{print $2}');                    # --> retrieve only the second parameter of the first line


        ##################################################################################
        #
        #       Extraction Parameters, a configuration file will allow ADEO to manipulate
        #       the parameters per BU. There are 4 parameters:
        #       A Min # of Competitor Stores
        #       B Min ration between # of Competitor Stores & # of Competitor Stores with
        #       Most Frequent Price
        #       C Algo used when criteria A and B are meet
        #       D Algo used when criteria A and B are not meet
        #
        ##################################################################################
        if [[ $valA -gt 0 && ${#TAB_PRICES[*]} -ge $valA ]]; then                               # Check Param A
                valeurB=$(awk "BEGIN {print ($valB*100)}")
                if [[ $valeurB -ge 0 && $valeurB -lt 100 ]]; then                               # Check Param B

                        mf=$(sed '1q;d' file_MF_Sorted.txt | awk '{print $1}')                  # As file_MF_Sorted.txt contains the most frequent sorted by desc, we can retrieve this value in the first line column 1
                        nb_mf_comp=$(awk "BEGIN {print ($mf/${#TAB_PRICES[*]}*100)}")           # Nb Most Frequent

                        if [[ ${nb_mf_comp%%.*} -ge $valeurB ]]; then
                                caseC=true
                        else
                                caseD=true
                        fi
                else
                        caseD=true
                fi
        else
                caseD=true
        fi

        if $caseC; then                                                                         # Check Param C
                case $valC in
                        1)  target_price=$price_most_freq
                                ;;
                        2)      if [ $(awk "BEGIN {print ($price_most_freq*100)}") -gt $(awk "BEGIN {print ($price_avg*100)}") ]; then
                                        target_price=$price_most_freq
                                else
                                        target_price=$price_avg
                                fi
                                ;;
                        3)  if [ $(awk "BEGIN {print ($price_most_freq*100)}") -gt $(awk "BEGIN {print ($price_avg*100)}") ]; then
                                        target_price=$price_avg
                                else
                                        target_price=$price_most_freq
                                fi
                                ;;
                        *) message "Incorrect value for parameter C  $valC  most frequent has to be retrieved  $price_most_freq"
                                target_price=$price_most_freq
                                ;;
                esac
        fi

        if $caseD; then                                                                         # Check Param D
                case $valD in
                        1)      price_min=$(sed '1q;d' file_tmp.txt | awk '{print $1}')
                                target_price=$price_min
                                ;;
                        2)      target_price=$price_avg
                                ;;
                        3)      target_price=$price_most_freq
                                ;;
                        4)  if [ $(awk "BEGIN {print ($price_most_freq*100)}") -gt $(awk "BEGIN {print ($price_avg*100)}") ]; then
                                        target_price=$price_most_freq
                                else
                                        target_price=$price_avg
                                fi
                                ;;
                        5)      if [ $(awk "BEGIN {print ($price_most_freq*100)}") -gt $(awk "BEGIN {print ($price_avg*100)}") ]; then
                                        target_price=$price_avg
                                else
                                        target_price=$price_most_freq
                                fi
                                ;;
                        *)      price_min=$(sed '1q;d' file_tmp.txt | awk '{print $1}')
                                target_price=$price_min
                                ;;
                esac
        fi
        message "Case C  $caseC  or Case D  $caseD  and target price is  $target_price"


        ##################  DELETE FILES   ######################
        ############  input file used file_tmp.txt  #############
        if [ -e file_tmp.txt ] ; then
                rm file_tmp.txt file_sorted.txt
        fi
        if [ -e file_most_freq.txt ] ; then
                rm file_most_freq.txt
        fi
        if [ -e file_MF_Sorted.txt ] ; then
                rm file_MF_Sorted.txt
        fi
}


#############################################################################################
#
#  LOOP the input file that is extracted from RETL.
#  As the input file is sorted (by zone_id, competitor, item and price), we can manage the
#  file line by line.
#  We retrieve the prices for each zone_id, competitor and item into an array
#  Array information:
#       index --> concatenation of ZONE_ID, COMPETITOR, ITEM and i. i is used to retrieve in
#                 the same array the multiple prices for same zone_id, competitor ant item
#       value --> PRICE
#
#############################################################################################
get_target_prices() {
        idx_zone_comp_item_new=""
        idx_zone_comp_item_old=""
        ############################################################################
        #  Retrieve parameters for each BUs and copy value from an array.
        #  Parameters are available in a file xxadeo_rmse_rpo_competitor_prices_config
        #  Here is a sample of the format file |BU_ID|PARAMETER_ID|VALUE|
        #
        #  Array information:
        #       index --> concatenation of BU_ID and PARAMETER_ID
        #       value --> VALUE
        #
        ###########################################################################
        if [ -e "${RETLforXXADEO}/rfx/etc/xxadeo_rmse_rpo_competitor_prices_config.txt" ]; then
                cat "${RETLforXXADEO}/rfx/etc/xxadeo_rmse_rpo_competitor_prices_config.txt" |
                grep "^|" > "${RETLforXXADEO}/rfx/etc/parm.txt"
                while IFS="|" read ignore BU Param Value
                do
                        ParamBU[$BU"_"$Param]=$Value
                done < "${RETLforXXADEO}/rfx/etc/parm.txt"
        else
                message "Configuration file xxadeo_rmse_rpo_competitor_prices_config.txt is missing"
        fi

        if [ -e ${DATA_DIR}/file_target_price.txt ] ; then
                rm ${DATA_DIR}/file_target_price.txt
        fi
                                nbLines=$(awk 'END {print NR}' "${OUTPUT_FILE}")
                                while IFS=, read item competitor zone_id price date bu
                                ((nbLines--))
                                do
                                                echo -e "Info line - $item,$competitor,$zone_id,$price,$date,$bu  ----------------- nbLines " $nbLines
                                                idx_zone_comp_item_new=$zone_id$competitor$item
                                                bu_new=$bu
                                                if [ -z $idx_zone_comp_item_old ];
                                                then
                                                                #echo -e "Element est vide!!!"
                                                                TAB_PRICES[$zone_id$competitor$item$i]=$price
                                                                var_bu_old=$bu_new
                                                else
                                                                if [ $idx_zone_comp_item_new = $idx_zone_comp_item_old ];
                                                                then
                                                                                TAB_PRICES[$zone_id$competitor$item$i]=$price
                                                                                if [[ $nbLines -eq 0 ]];
                                                                                then
                                                                                                get_price_fct2 $bu_old ${TAB_PRICES[*]}
                                                                                                echo "$item_old,$competitor_old,$zone_id_old,$target_price,$date_old,_tp" >> "${DATA_DIR}/file_target_price.txt"
                                                                                                unset TAB_PRICES
                                                                                fi
                                                                else
                                                                                get_price_fct2 $bu_old ${TAB_PRICES[*]}
                                                                                echo "$item_old,$competitor_old,$zone_id_old,$target_price,$date_old,_tp" >> "${DATA_DIR}/file_target_price.txt"
                                                                                unset TAB_PRICES
                                                                                i=0
                                                                                typeset -A TAB_PRICES
                                                                                TAB_PRICES[$zone_id$competitor$item$i]=$price
                                                                                if [[ $nbLines -eq 0 ]] ; then
                                                                                                get_price_fct2 ${bu_new:0:4} ${TAB_PRICES[*]}
                                                                                                echo "$item,$competitor,$zone_id,$target_price,$date,_tp" >> "${DATA_DIR}/file_target_price.txt"
                                                                                fi
                                                                fi
                                                fi
                                                idx_zone_comp_item_old=$idx_zone_comp_item_new
                                                bu_old=${bu_new:0:4}
                                                item_old=$item
                                                competitor_old=$competitor
                                                zone_id_old=$zone_id
                                                date_old=$date
                                                ((i++))
                                done < "${OUTPUT_FILE}"

}

# Create the min prices files
get_min_prices
# Create the target prices files
get_target_prices



############   CONCATENATION FILES  #####################
if [ -e "${DATA_DIR}/file_min.txt" -e "${DATA_DIR}/file_target_price.txt" ]; then
        cat "${DATA_DIR}/file_min.txt" "${DATA_DIR}/file_target_price.txt" | sort -t, -k3n,3n -k2,2 -k1,1 -k6,6 > "${DATA_DIR}/comp_prices.txt"

        while IFS=, read item competitor zone_id price date price_kind
        do
                        echo "$item;$competitor$price_kind;$zone_id;$price;$date" >> "${DATA_DIR}/comp_prices_final.txt"
        done < "${DATA_DIR}/comp_prices.txt"
fi



############   GENERATE OUTPUT FILES  ###################
if [ -e "${DATA_DIR}/comp_prices_final.txt" ]; then
        awk -F";" '{print $1","$2","$3","$4}' "${DATA_DIR}/comp_prices_final.txt" > "${OUTPUT_FILE_CMP_PC}"
        awk -F";" '{print $1","$2","$3","$5}' "${DATA_DIR}/comp_prices_final.txt" > "${OUTPUT_FILE_CMP_CHK_DT}"
else
        touch "${OUTPUT_FILE_CMP_PC}"
        touch "${OUTPUT_FILE_CMP_CHK_DT}"
        message "No data to be extracted, files were generated empty"
fi




##################  DELETE FILES   ######################
############       Delete Tmp files         #############
if [ -e "${RETLforXXADEO}/rfx/etc/parm.txt" ]; then
        rm "${RETLforXXADEO}/rfx/etc/parm.txt"
fi

if [ -e "${DATA_DIR}/file_min.txt" ]; then
        rm "${DATA_DIR}/file_min.txt"
fi

if [ -e "${DATA_DIR}/file_target_price.txt" ]; then
        rm "${DATA_DIR}/file_target_price.txt"
fi

if [ -e "${DATA_DIR}/comp_prices.txt" ]; then
        rm "${DATA_DIR}/comp_prices.txt"
fi

if [ -e "${DATA_DIR}/comp_prices_final.txt" ]; then
        rm "${DATA_DIR}/comp_prices_final.txt"
fi

if [ -e "${OUTPUT_FILE}" ]; then
                rm "${OUTPUT_FILE}"
fi

#######################################
#  Do error checking:
#######################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#  Remove the status file:

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."

#  Cleanup and exit:

check_error_and_clean_before_exit 0 "NO_CHECK"
