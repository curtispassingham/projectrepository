#!/bin/ksh

##########################################################
#
# Object Name:   xxadeo_rmse_rpo_orghier.ksh
# Description:   The xxadeo_rmse_rpo_orghier.ksh program extracts organizational hierarchy data
#                from the COMPHEAD, CHAIN, AREA, REGION, DISTRICT and STORE RMS tables and
#                places this data into a flat file (loc.csv.dat) to be be loaded into RPO.
# Version:       1.1
# Author:        CGI
# Creation Date: 23/05/2018
# Last Modified: 23/05/2018
# History: 1.0 - Initial Release
#                  1.1 - Adjustments to the new file structure RETLforXXADEO,
#                                DATA_DIR and XXADEO_RMS
#
##########################################################

################## PROGRAM DEFINE #####################
#########     (must be the first define)     ##########

export PROGRAM_NAME="xxadeo_rmse_rpo_orghier"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
    error_check=1
    if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
    then
        message "ERROR: Parameter 2 is not valid: ${2}"
        message "    Parameter 2 must be true, false or empty (true)"
        rmse_terminate 1
    elif [[ "${2}" = "NO_CHECK" ]]
    then
        error_check=0
    fi
    #message "check_error_and_clean_before_exit: error_check: ${error_check}"

    if [[ $1 -ne 0 && error_check -eq 1 ]]
    then
        rm -f "${OUTPUT_FILE}.retl"
        checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
    elif [[ error_check -eq 0 ]]
    then
        rm -f "${OUTPUT_FILE}.retl"
        checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        rmse_terminate $1
    fi
}

####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export DATA_DIR="${RETL_OUT}/RPO"

export OUTPUT_FILE=${DATA_DIR}/loc.csv.dat

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read in the organizational hierarchy data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            SELECT s.store                         STORE_ID,
                   SUBSTR(s.store_name, 1, 100)    STORE_LABEL,
                   d.district                      ZONE_ID,
                   SUBSTR(d.district_name, 1, 100) ZONE_LABEL,
                   r.region                        REGION_ID,
                   SUBSTR(r.region_name, 1, 100)   REGION_LABEL,
                   a.area                          COUNTRY_ID,
                   SUBSTR(a.area_name, 1, 100)     COUNTRY_LABEL,
                   ch.chain                        CHANNEL_ID,
                   SUBSTR(ch.chain_name, 1, 100)   CHANNEL_LABEL,
                   co.company                      ENTERPRISE_ID,
                   SUBSTR(co.co_name, 1, 100)      ENTERPRISE_LABEL,
                   rz.zone_id                      PRICE_ZONE_ID,
                   SUBSTR(rz.name, 1, 100)         PRICE_ZONE_LABEL
              FROM ${XXADEO_RMS}.rpm_zone rz,
                   ${XXADEO_RMS}.rpm_zone_location rzl,
                   ${XXADEO_RMS}.store s,
                   ${XXADEO_RMS}.district d,
                   ${XXADEO_RMS}.region r,
                   ${XXADEO_RMS}.area a,
                   ${XXADEO_RMS}.chain ch,
                   ${XXADEO_RMS}.comphead co
             WHERE rz.zone_id = rzl.zone_id
               AND rzl.location = s.store
               AND s.district = d.district
               AND d.region = r.region
               AND r.area = a.area
               AND a.chain = ch.chain
               AND rzl.loc_type = 0
               AND rz.zone_id IN (SELECT pzb.zone_id
                                    FROM ${XXADEO_RMS}.xxadeo_mom_dvm dvm,
                                         ${XXADEO_RMS}.xxadeo_rpm_bu_price_zones pzb
                                   WHERE pzb.bu_zone_type = dvm.value_1
                                     AND dvm.func_area = 'RPO_ZONES'
                                     AND dvm.parameter = 'RPO_ALLOWED_ZONE_TYPE') -- Only RPO allowed Zones
          ORDER BY rz.zone_id,
                   ch.chain,
                   a.area,
                   r.region,
                   d.district,
                   s.store
         ]]>
      </PROPERTY>
      <OUTPUT   name = "org_data.v"/>
   </OPERATOR>

   <!-- Use a convert operator to make not nullable some fields that use substring into the query  -->
   <OPERATOR type="convert">
      <INPUT    name="org_data.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
          <CONVERTSPECS>
                <CONVERT destfield="STORE_ID" sourcefield="STORE_ID" newtype="string">
                <CONVERTFUNCTION name="string_from_int64"/>
                </CONVERT>
                <CONVERT destfield="ZONE_ID" sourcefield="ZONE_ID" newtype="string">
                <CONVERTFUNCTION name="string_from_int64"/>
                </CONVERT>
                <CONVERT destfield="REGION_ID" sourcefield="REGION_ID" newtype="string">
                <CONVERTFUNCTION name="string_from_int64"/>
                </CONVERT>
                <CONVERT destfield="COUNTRY_ID" sourcefield="COUNTRY_ID" newtype="string">
                <CONVERTFUNCTION name="string_from_int64"/>
                </CONVERT>
                <CONVERT destfield="CHANNEL_ID" sourcefield="CHANNEL_ID" newtype="string">
                <CONVERTFUNCTION name="string_from_int64"/>
                </CONVERT>
                <CONVERT destfield="ENTERPRISE_ID" sourcefield="ENTERPRISE_ID" newtype="string">
                <CONVERTFUNCTION name="string_from_int16"/>
                </CONVERT>
                <CONVERT destfield="PRICE_ZONE_ID" sourcefield="PRICE_ZONE_ID" newtype="string">
                <CONVERTFUNCTION name="string_from_int64"/>
                </CONVERT>

                <CONVERT destfield="STORE_LABEL" sourcefield="STORE_LABEL">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ZONE_LABEL" sourcefield="ZONE_LABEL">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="REGION_LABEL" sourcefield="REGION_LABEL">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="COUNTRY_LABEL" sourcefield="COUNTRY_LABEL">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>

                <CONVERT destfield="CHANNEL_LABEL" sourcefield="CHANNEL_LABEL">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ENTERPRISE_LABEL" sourcefield="ENTERPRISE_LABEL">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="PRICE_ZONE_LABEL" sourcefield="PRICE_ZONE_LABEL">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
        </CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="org_data_conv.v"/>
   </OPERATOR>

   <!--  Write out the organizational heirarchy data to a flat file:  -->

   <OPERATOR type="export">
      <INPUT    name="org_data_conv.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}.retl"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

###############################################
#  Execute the RETL flow that had previously
#  been copied into xxadeo_rmse_rpo_orghier.xml:
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
check_error_and_clean_before_exit $?


###############################################################################
#  Convert output data files to CSV format
###############################################################################

csv_converter "retlsv-csv" "${OUTPUT_FILE}.retl" "${OUTPUT_FILE}"
check_error_and_clean_before_exit $?


###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${OUTPUT_FILE}


#######################################
#  Do error checking:
#######################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#  Remove the status file:

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."

#  Cleanup and exit:

check_error_and_clean_before_exit 0 "NO_CHECK"
