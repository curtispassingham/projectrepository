#!/bin/ksh
###############################################################################
#  $Workfile:   xxadeo_rmsl_pcs.ksh
#  $Modtime:    Oct 19 2018
#  $Autor:      CGI - Wilson Sturaro
#
#  $Desc:   This interface will load PCS information into tables XXADEO_AREA_PCS
#           and XXADEO_LOCATION_PCS.
#
#           During load, reject records due to bad formating are stored into files:
#            XXRMS239_PCS_BU_BASA_[BU]_YYYYMMDD-HHMMSS.dat.rej
#            XXRMS239_PCS_LOC_BASA_[BU]_YYYYMMDD-HHMMSS.dat.rej
#
#           Bad Area/Currency combinations will be stored into files:
#            XXRMS239_PCS_BU_BASA_[BU]_YYYYMMDD-HHMMSS.dat.bad_area-curr
#            XXRMS239_PCS_LOC_BASA_[BU]_YYYYMMDD-HHMMSS.dat.bad_area-curr
#
#           Bad Store/Area/Currency combinations will be stored into files:
#            XXRMS239_PCS_BU_BASA_[BU]_YYYYMMDD-HHMMSS.dat.bad_store-area-curr
#            XXRMS239_PCS_LOC_BASA_[BU]_YYYYMMDD-HHMMSS.dat.bad_store-area-curr
#
#           During Merge of the records not rejected from the validations above,
#           any errors will be logged into table:
#            XXADEO_PCS_IN_ERR
#
###############################################################################

###############################################################################
#  This interface consists in the integration of the Prix de cession into RMS
###############################################################################

###############################################################################
#  PROGRAM NAME DEFINE
###############################################################################
export PROGRAM_NAME='xxadeo_rmsl_pcs'


###############################################################################
#  INCLUDES
###############################################################################
. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh


###############################################################################
#  INPUT/OUTPUT DEFINES
###############################################################################
#Input Files
export I_DATA_DIR=$RETL_IN/MOM
export I_PCS_PATTERN="XXRMS239_PCS"
export I_PCS_BU_PATTERN="XXRMS239_PCS_BU_BASA"
export I_PCS_LOC_PATTERN="XXRMS239_PCS_LOC_BASA"


#Output/Temp Files
export O_PCS_IN_ERR=${RETLforXXADEO}/rfx/etc/xxadeo_rmsl_pcs_in_err.dat   #Error_Check
export O_PCS_SAC_TMP=${I_DATA_DIR}/xxadeo_rmsl_pcs_sac.dat                #Store/Area/Currency


#Schema Files
export O_PCS_SAC_SCHEMA=${RETLforXXADEO}/rfx/schema/xxadeo_rmsl_pcs_sac.schema
export I_PCS_BU_SCHEMA=${RETLforXXADEO}/rfx/schema/xxadeo_rmsl_pcs_bu.schema
export I_PCS_LOC_SCHEMA=${RETLforXXADEO}/rfx/schema/xxadeo_rmsl_pcs_loc.schema


#Control variables
export PROCESS_BU="Y"
export PROCESS_LOC="Y"


###############################################################################
#  Start program
###############################################################################
message "Program starting ..."
RETL_FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

###############################################################################
#  Check input files / Get the last file in the directory and
#  delete any previous rej file
#
#  1)Check how many BU files there are for the processing day
#  2)Only BU files that have been sent on vdate will be processed
#
###############################################################################

#####TMP/REJECT FILES#####
#Clear any previous reject file (schema error)
I_PCS_REF_LIST=`find $I_DATA_DIR -name ${I_PCS_PATTERN}*.rej | sort -u`
for I_REJ_FILE in $(echo $I_PCS_REF_LIST); do rm $I_REJ_FILE; done

#Clear any previous reject file (bad area/currency combination)
I_PCS_REF_LIST=`find $I_DATA_DIR -name ${I_PCS_PATTERN}*.bad_area-curr | sort -u`
for I_REJ_FILE in $(echo $I_PCS_REF_LIST); do rm $I_REJ_FILE; done

#Clear any previous reject file (bad store/area/currency combination)
I_PCS_REF_LIST=`find $I_DATA_DIR -name ${I_PCS_PATTERN}*.bad_store-area-curr | sort -u`
for I_REJ_FILE in $(echo $I_PCS_REF_LIST); do rm $I_REJ_FILE; done


#####INPUT FILES#####
#PCS_BU
I_PCS_BU_LIST=`find $I_DATA_DIR -name ${I_PCS_BU_PATTERN}*.dat | awk -F '/' '{print $NF}' | awk -F '[_]' '{print $5}' | sort -u`
I_PCS_BU_FILES=`for i in $(echo ${I_PCS_BU_LIST}); do find $I_DATA_DIR -name ${I_PCS_BU_PATTERN}_$i*.dat | sort | tail -n 1; done;`

#PCS_LOC
I_PCS_LOC_LIST=`find $I_DATA_DIR -name ${I_PCS_LOC_PATTERN}*.dat | awk -F '/' '{print $NF}' | awk -F '[_]' '{print $5}' | sort -u`
I_PCS_LOC_FILES=`for i in $(echo ${I_PCS_LOC_LIST}); do find $I_DATA_DIR -name ${I_PCS_LOC_PATTERN}_$i*.dat | sort | tail -n 1; done;`


#Check if input files exists
if [[ -z $I_PCS_BU_FILES ]] && [[ -z $I_PCS_LOC_FILES ]];  then
    message "ERROR: $I_PCS_BU_PATTERN and $I_PCS_LOC_PATTERN files do not exists in $DATA_DIR."
    message "Program completed with error: No file to be loaded!"
    exit 1
elif  [[ -z $I_PCS_BU_FILES ]];  then
    message "Warning: No $I_PCS_BU_PATTERN files exists in $DATA_DIR."
    PROCESS_BU="N"
elif [[ -z $I_PCS_LOC_FILES ]];  then
    message "Warning: No $I_PCS_LOC_PATTERN files exists for $DATA_DIR"
    PROCESS_LOC="N"
fi


##############################################################################################################################################################
#  MAIN PROGRAM CONTENT (pre flow) extract STORE/AREA/CURRENCY and AREA/CURRENCY Information
##############################################################################################################################################################
#Check if BU or LOC files should be processed
if [ $PROCESS_BU = "Y" ] || [ $PROCESS_LOC = "Y" ]; then

    if [ -s ${O_PCS_SAC_TMP} ]; then
        rm ${O_PCS_SAC_TMP}
    fi

cat > ${RETL_FLOW_FILE} << EOF
        <FLOW name = "${PROGRAM_NAME}.flw">
        <!-- Get the next sequence val from XXADEO_RPM_STAGE_PROC_ID_SEQ -->
        ${DBREAD}
            <PROPERTY name = "query">
                 <![CDATA[
                     SELECT SH.STORE         AS STORE
                          , SH.AREA          AS BU
                           , S.CURRENCY_CODE AS CURRENCY
                       FROM ${XXADEO_RMS}.STORE           S
                          , ${XXADEO_RMS}.STORE_HIERARCHY SH
                      WHERE S.STORE = SH.STORE
                 ]]>
            </PROPERTY>
            <OUTPUT   name = "xxadeo_sac.v"/>
         </OPERATOR>

        <OPERATOR type="sort">
                <INPUT name="xxadeo_sac.v"/>
                <PROPERTY name="key" value="BU CURRENCY"/>
                <OUTPUT name="sort_xxadeo_sac.v"/>
        </OPERATOR>

         <OPERATOR type="export">
            <INPUT name="sort_xxadeo_sac.v"/>
            <PROPERTY name="schemafile" value="${O_PCS_SAC_SCHEMA}"/>
            <PROPERTY name="outputfile" value="${O_PCS_SAC_TMP}"/>
        </OPERATOR>
        </FLOW>
EOF

    ###############################################################################
    #  Execute the flow
    ###############################################################################
    ${RFX_EXE} ${RFX_OPTIONS} -f ${RETL_FLOW_FILE}
    exit_stat=$?
    checkerror -e $exit_stat -m "Program failed - check $ERR_FILE"

    #Evaluate if the file is readable
    if [ ! -r  ${O_PCS_SAC_TMP} ]; then
        message "ERROR: Store/Area/Currency file ${O_PCS_SAC_TMP} missing!"
        exit 1
    fi

    #Evaluate if the file exists but has no values
    if [ -f ${O_PCS_SAC_TMP} ] && [ ! -s ${O_PCS_SAC_TMP} ]; then
        message "ERROR: Store/Area/Currency file ${O_PCS_SAC_TMP} is empty. Exiting program."
        exit 1
    fi

fi

##############################################################################################################################################################
#  MAIN PROGRAM CONTENT (main flow) BU
##############################################################################################################################################################
#IF1: BU FLOW: Check if BU files should be processed
if [ $PROCESS_BU = "Y" ]; then

    #FOR START
    for I_BU_FILE in $(echo $I_PCS_BU_FILES); do

        #set reject file names
        O_BU_REJ_FILE=${I_BU_FILE}.rej
        O_BU_AC_REJ_FILE=${I_BU_FILE}.bad_area-curr

        #get BU number
        log_bu=`echo $I_BU_FILE |  awk -F '/' '{print $NF}' | awk -F '[_]' '{print $5}'`

        #IF2: check empty file, if empty skips to next file
        if [ -f ${I_BU_FILE} ] && [ ! -s ${I_BU_FILE} ]; then
            message "Warning: PCS-AREA file for BU: ${log_bu} is empty. Skipping the file"
        else


cat > ${RETL_FLOW_FILE} << EOF
        <FLOW name = "${PROGRAM_NAME}.flw">

        <!-- Import data from file -->
        <OPERATOR type="import">
                <PROPERTY  name="inputfile"  value="${I_BU_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_PCS_BU_SCHEMA}"/>
                <PROPERTY  name="rejectfile" value="${O_BU_REJ_FILE}"/>
                <OUTPUT name="import_bu_pcs.v"/>
        </OPERATOR>

        <OPERATOR type="sort">
                <INPUT name="import_bu_pcs.v"/>
                <PROPERTY name="key" value="BU CURRENCY"/>
                <OUTPUT name="sort_bu_pcs.v"/>
        </OPERATOR>

        <OPERATOR type="filter">
                <INPUT name="sort_bu_pcs.v"/>
                <PROPERTY name="filter"  value="(((STATUS EQ 'U') OR STATUS EQ 'D') AND PCS GE 0)"/>
                <PROPERTY name="rejects" value="true"/>
                <OUTPUT name="sort_bu_pcs_valid.v"/>
                <OUTPUT name="rejected_bu_pcs.v"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="rejected_bu_pcs.v"/>
                <PROPERTY name="schemafile" value="${I_PCS_BU_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${O_BU_REJ_FILE}"/>
                <PROPERTY name="outputmode" value="append"/>
        </OPERATOR>

        <OPERATOR type="copy">
            <INPUT name="sort_bu_pcs_valid.v"/>
            <OUTPUT name="sort_bu_pcs_valid_1.v"/>
            <OUTPUT name="sort_bu_pcs_valid_2.v"/>
        </OPERATOR>

        <!-- filter input file with valid area/currency combination                       -->
        <!-- and then join both files (lookup) and exludes bad area/currency combinations -->
        <OPERATOR type="import">
                <PROPERTY  name="inputfile"  value="${O_PCS_SAC_TMP}"/>
                <PROPERTY  name="schemafile" value="${O_PCS_SAC_SCHEMA}"/>
                <OUTPUT name="import_bu_pcs_ac.v"/>
        </OPERATOR>

        <OPERATOR type="sort">
                <INPUT name="import_bu_pcs_ac.v"/>
                <PROPERTY name="key" value="BU CURRENCY"/>
                <OUTPUT name="sort_import_bu_pcs_ac.v"/>
        </OPERATOR>

         <OPERATOR type="fieldmod">
            <INPUT name="sort_import_bu_pcs_ac.v" />
            <PROPERTY name="keep" value="BU CURRENCY"/>
            <OUTPUT name="filter_bu_pcs_ac.v"/>
        </OPERATOR>

        <OPERATOR type="removedup">
            <INPUT name="filter_bu_pcs_ac.v"/>
            <PROPERTY name="key" value="BU CURRENCY"/>
            <PROPERTY name="keep" value="FIRST"/>
            <OUTPUT name="final_bu_pcs_ac.v"/>
        </OPERATOR>

        <!-- join files -->
        <OPERATOR type="innerjoin">
                <INPUT name="final_bu_pcs_ac.v"/>
                <INPUT name="sort_bu_pcs_valid_1.v"/>
                <PROPERTY name="key" value="BU CURRENCY"/>
                <OUTPUT name="join_bu_pcs_valid.v"/>
        </OPERATOR>

        <!--lookup records from input-file with valid area/currency combination -->
        <OPERATOR type="lookup">
            <INPUT name="sort_bu_pcs_valid_2.v"/>
            <INPUT name="join_bu_pcs_valid.v"/>
                <PROPERTY name="tablekeys" value="ITEM SUPPLIER BU PCS CURRENCY CIRCUIT_PRINCIPAL EFFECTIVE_DATE LAST_UPDATE_ID STATUS PCS_SOURCE"/>
                <PROPERTY name="ifnotfound" value="reject"/>
            <OUTPUT name="load_bu_pcs_valid.v"/>
            <OUTPUT name="reject_bu_pcs_ac.v"/>
        </OPERATOR>

        <OPERATOR type="export">
            <INPUT name="reject_bu_pcs_ac.v"/>
               <PROPERTY name="schemafile" value="${I_PCS_BU_SCHEMA}"/>
               <PROPERTY name="outputfile" value="${O_BU_AC_REJ_FILE}"/>
               <PROPERTY name="outputmode" value="append"/>
        </OPERATOR>

        <OPERATOR type="parser">
                <INPUT name="load_bu_pcs_valid.v"/>
                        <PROPERTY name="expression">
                        <![CDATA[
                                {
                                        if (RECORD.STATUS == "U")
                                        {
                                             RECORD.STATUS = "A";
                                        }
                                        else if (RECORD.STATUS == "D")
                                        {
                                            RECORD.STATUS = "I";
                                        }
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="load_bu_pcs_valid2.v"/>
        </OPERATOR>

        <OPERATOR type="fieldmod">
            <INPUT name="load_bu_pcs_valid2.v" />
            <PROPERTY name="keep" value="ITEM SUPPLIER BU PCS CIRCUIT_PRINCIPAL EFFECTIVE_DATE LAST_UPDATE_ID STATUS PCS_SOURCE"/>
            <OUTPUT name="load_bu_pcs_valid3.v"/>
        </OPERATOR>

        <!-- load input file -->
            ${DBPREPSTMT}
                    <INPUT name="load_bu_pcs_valid3.v"/>
                    <PROPERTY name="tablename" value="${XXADEO_RMS}.XXADEO_AREA_PCS"/>
                    <PROPERTY name="schemaowner" value="${XXADEO_RMS}" />
                    <PROPERTY name="statement">
                    <![CDATA[
                             MERGE /*+ parallel(A) */ INTO ${XXADEO_RMS}.XXADEO_AREA_PCS A
                                 USING dual ON ( ITEM = ? AND SUPPLIER = ? AND BU = ? AND EFFECTIVE_DATE = ? )
                                     WHEN NOT MATCHED THEN
                                                    INSERT
                                                    (
                                                     ITEM
                                                    ,SUPPLIER
                                                    ,BU
                                                    ,PCS
                                                    ,CIRCUIT_PRINCIPAL
                                                    ,PCS_SOURCE
                                                    ,STATUS
                                                    ,EFFECTIVE_DATE
                                                    ,CREATE_DATETIME
                                                    ,LAST_UPDATE_DATETIME
                                                    ,LAST_UPDATE_ID
                                                    )
                                                    VALUES
                                                    (
                                                         ?               --ITEM
                                                       , ?               --SUPPLIER
                                                       , ?               --BU
                                                       , ?               --PCS
                                                       , ?               --CIRCUIT_PRINCIPAL
                                                       , ?               --PCS_SOURCE
                                                       , ?               --STATUS
                                                       , ?               --EFFECTIVE_DATE
                                                       , sysdate         --CREATE_DATETIME
                                                       , sysdate         --LAST_UPDATE_DATETIME
                                                       , ?               --LAST_UPDATE_ID
                                                    )
                                     WHEN MATCHED THEN
                                                    UPDATE
                                                    SET  PCS               = ?
                                                       , CIRCUIT_PRINCIPAL = ?
                                                       , PCS_SOURCE        = ?
                                                       , STATUS            = ?
                                                       , LAST_UPDATE_DATETIME = sysdate
                                                       , LAST_UPDATE_ID       = ?
                                LOG ERRORS INTO ${XXADEO_RMS}.XXADEO_PCS_IN_ERR REJECT LIMIT UNLIMITED
                    ]]>
                    </PROPERTY>
                    <PROPERTY name="fields" value="ITEM SUPPLIER BU EFFECTIVE_DATE ITEM SUPPLIER BU PCS CIRCUIT_PRINCIPAL PCS_SOURCE STATUS EFFECTIVE_DATE LAST_UPDATE_ID PCS CIRCUIT_PRINCIPAL PCS_SOURCE STATUS LAST_UPDATE_ID"/>
            </OPERATOR>

        </FLOW>
EOF

            ###############################################################################
            #  Execute the flow
            ###############################################################################
            ${RFX_EXE} ${RFX_OPTIONS} -f ${RETL_FLOW_FILE}
            exit_stat=$?
            checkerror -e $exit_stat -m "Program failed - check $ERR_FILE"
            message "PCS-AREA loaded for BU: ${log_bu} file ${I_BU_FILE}"

            # Evaluate if any records have been rejected
            if [ -s ${O_BU_REJ_FILE} ]; then
                    message "Warning: Records have been rejected: bad format, please check ${O_BU_REJ_FILE}"
            else
                    rm ${O_BU_REJ_FILE}
            fi

            # Evaluate if any there are area/currency mismatch
            if [ -s ${O_BU_AC_REJ_FILE} ]; then
                    message "Warning: Records have been rejected: AREA/CURRENCY does not exists, please check ${O_BU_AC_REJ_FILE}"
            else
                    rm ${O_BU_AC_REJ_FILE}
            fi

        #FI2: end check empty file
        fi

    #FOR END
    done

else
    message "Warning: PCS-AREA not loaded"
fi
#IF1: END OF BU FLOW


##############################################################################################################################################################
#  MAIN PROGRAM CONTENT (main flow) LOC
##############################################################################################################################################################
#IF1: LOC FLOW: Check if LOC files should be processed
if [ $PROCESS_LOC = "Y" ]; then

    #FOR START
    for I_LOC_FILE in $(echo $I_PCS_LOC_FILES); do

        #set reject file name
        O_LOC_REJ_FILE=${I_LOC_FILE}.rej
        O_LOC_REJ_SAC_FILE=${I_LOC_FILE}.bad_store-area-curr

        #get BU number
        log_bu=`echo $I_LOC_FILE |  awk -F '/' '{print $NF}' | awk -F '[_]' '{print $5}'`

        #IF2: check empty file, if empty skips to next file
        if [ -f ${I_LOC_FILE} ] && [ ! -s ${I_LOC_FILE} ]; then
            message "Warning: PCS-LOCATION file for BU: ${log_bu} is empty. Skipping the file"
        else


cat > ${RETL_FLOW_FILE} << EOF
        <FLOW name = "${PROGRAM_NAME}.flw">

    <!-- Insert data from file -->
        <OPERATOR type="import">
                <PROPERTY  name="inputfile"  value="${I_LOC_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_PCS_LOC_SCHEMA}"/>
                <PROPERTY  name="rejectfile" value="${O_LOC_REJ_FILE}"/>
                <OUTPUT name="import_loc_pcs.v"/>
        </OPERATOR>

        <OPERATOR type="sort">
                <INPUT name="import_loc_pcs.v"/>
                <PROPERTY name="key" value="STORE BU CURRENCY"/>
                <OUTPUT name="sort_loc_pcs.v"/>
        </OPERATOR>

        <OPERATOR type="filter">
                <INPUT name="sort_loc_pcs.v"/>
                <PROPERTY name="filter"  value="(((STATUS EQ 'U') OR STATUS EQ 'D') AND PCS GE 0)"/>
                <PROPERTY name="rejects" value="true"/>
                <OUTPUT name="sort_loc_pcs_valid.v"/>
                <OUTPUT name="rejected_loc_pcs.v"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="rejected_loc_pcs.v"/>
                <PROPERTY name="schemafile" value="${I_PCS_LOC_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${O_LOC_REJ_FILE}"/>
                <PROPERTY name="outputmode" value="append"/>
        </OPERATOR>

        <OPERATOR type="copy">
            <INPUT name="sort_loc_pcs_valid.v"/>
            <OUTPUT name="sort_loc_pcs_valid_1.v"/>
            <OUTPUT name="sort_loc_pcs_valid_2.v"/>
        </OPERATOR>

        <!-- filter input file with valid area/currency combination                       -->
        <!-- and then join both files (lookup) and exludes bad area/currency combinations -->
        <OPERATOR type="import">
                <PROPERTY  name="inputfile"  value="${O_PCS_SAC_TMP}"/>
                <PROPERTY  name="schemafile" value="${O_PCS_SAC_SCHEMA}"/>
                <OUTPUT name="import_loc_pcs_sac.v"/>
        </OPERATOR>

        <OPERATOR type="sort">
                <INPUT name="import_loc_pcs_sac.v"/>
                <PROPERTY name="key" value="STORE BU CURRENCY"/>
                <OUTPUT name="sort_import_loc_pcs_sac.v"/>
        </OPERATOR>

         <!-- join files -->
         <OPERATOR type="innerjoin">
                <INPUT name="sort_loc_pcs_valid_1.v"/>
                <INPUT name="sort_import_loc_pcs_sac.v"/>
                <PROPERTY name="key" value="STORE BU CURRENCY"/>
                <OUTPUT name="join_loc_pcs_valid.v"/>
        </OPERATOR>

        <!--lookup records from input-file with valid area/currency combination -->
        <OPERATOR type="lookup">
            <INPUT name="sort_loc_pcs_valid_2.v"/>
            <INPUT name="join_loc_pcs_valid.v"/>
                <PROPERTY name="tablekeys" value="ITEM SUPPLIER BU STORE PCS CURRENCY CIRCUIT_PRINCIPAL EFFECTIVE_DATE LAST_UPDATE_ID STATUS PCS_SOURCE"/>
                <PROPERTY name="ifnotfound" value="reject"/>
            <OUTPUT name="load_loc_pcs_valid.v"/>
            <OUTPUT name="reject_loc_pcs_sac.v"/>
        </OPERATOR>

        <OPERATOR type="export">
            <INPUT name="reject_loc_pcs_sac.v"/>
               <PROPERTY name="schemafile" value="${I_PCS_LOC_SCHEMA}"/>
               <PROPERTY name="outputfile" value="${O_LOC_REJ_SAC_FILE}"/>
               <PROPERTY name="outputmode" value="append"/>
        </OPERATOR>

        <OPERATOR type="parser">
                <INPUT name="load_loc_pcs_valid.v"/>
                        <PROPERTY name="expression">
                        <![CDATA[
                                {
                                        if (RECORD.STATUS == "U")
                                        {
                                             RECORD.STATUS = "A";
                                        }
                                        else if (RECORD.STATUS == "D")
                                        {
                                            RECORD.STATUS = "I";
                                        }
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="load_loc_pcs_valid2.v"/>
        </OPERATOR>

        <OPERATOR type="fieldmod">
            <INPUT name="load_loc_pcs_valid2.v" />
            <PROPERTY name="keep" value="ITEM SUPPLIER STORE PCS CIRCUIT_PRINCIPAL EFFECTIVE_DATE LAST_UPDATE_ID STATUS PCS_SOURCE"/>
            <OUTPUT name="load_loc_pcs_valid3.v"/>
        </OPERATOR>

           <!-- load input file -->
            ${DBPREPSTMT}
                    <INPUT name="load_loc_pcs_valid3.v"/>
                    <PROPERTY name="tablename" value="${XXADEO_RMS}.XXADEO_LOCATION_PCS"/>
                    <PROPERTY name="schemaowner" value="${XXADEO_RMS}" />
                    <PROPERTY name="statement">
                    <![CDATA[
                             MERGE /*+ parallel(A) */ INTO ${XXADEO_RMS}.XXADEO_LOCATION_PCS A
                                 USING dual ON ( ITEM = ? AND STORE = ? AND EFFECTIVE_DATE = ? )
                                     WHEN NOT MATCHED THEN
                                                    INSERT
                                                    (
                                                     ITEM
                                                    ,SUPPLIER
                                                    ,STORE
                                                    ,PCS
                                                    ,CIRCUIT_PRINCIPAL
                                                    ,PCS_SOURCE
                                                    ,STATUS
                                                    ,EFFECTIVE_DATE
                                                    ,CREATE_DATETIME
                                                    ,LAST_UPDATE_DATETIME
                                                    ,LAST_UPDATE_ID
                                                    )
                                                    VALUES
                                                    (
                                                         ?               --ITEM
                                                       , ?               --SUPPLIER
                                                       , ?               --STORE
                                                       , ?               --PCS
                                                       , ?               --CIRCUIT_PRINCIPAL
                                                       , ?               --PCS_SOURCE
                                                       , ?               --STATUS
                                                       , ?               --EFFECTIVE_DATE
                                                       , sysdate         --CREATE_DATETIME
                                                       , sysdate         --LAST_UPDATE_DATETIME
                                                       , ?               --LAST_UPDATE_ID
                                                    )
                                     WHEN MATCHED THEN
                                                    UPDATE
                                                    SET  SUPPLIER          = ?
                                                       , PCS               = ?
                                                       , CIRCUIT_PRINCIPAL = ?
                                                       , PCS_SOURCE        = ?
                                                       , STATUS            = ?
                                                       , LAST_UPDATE_DATETIME = sysdate
                                                       , LAST_UPDATE_ID       = ?
                                LOG ERRORS INTO ${XXADEO_RMS}.XXADEO_PCS_IN_ERR REJECT LIMIT UNLIMITED
                    ]]>
                    </PROPERTY>
                    <PROPERTY name="fields" value="ITEM STORE EFFECTIVE_DATE ITEM SUPPLIER STORE PCS CIRCUIT_PRINCIPAL PCS_SOURCE STATUS EFFECTIVE_DATE LAST_UPDATE_ID SUPPLIER PCS CIRCUIT_PRINCIPAL PCS_SOURCE STATUS LAST_UPDATE_ID"/>
            </OPERATOR>
        </FLOW>
EOF

            ###############################################################################
            #  Execute the flow
            ###############################################################################
            ${RFX_EXE} ${RFX_OPTIONS} -f ${RETL_FLOW_FILE}
            exit_stat=$?
            checkerror -e $exit_stat -m "Program failed - check $ERR_FILE"
            message "PCS-LOCATION loaded for BU: ${log_bu} file ${I_LOC_FILE}"

            # Evaluate if any records have been rejected
            if [ -s ${O_LOC_REJ_FILE} ]; then
                    message "Warning: Records have been rejected: bad format, please check ${O_LOC_REJ_FILE}"
            else
                    rm ${O_LOC_REJ_FILE}
            fi

            # Evaluate if any there are area/currency mismatch
            if [ -s ${O_LOC_REJ_SAC_FILE} ]; then
                    message "Warning: Records have been rejected: STORE/AREA/CURRENCY does not exists, please check ${O_LOC_REJ_SAC_FILE}"
            else
                    rm ${O_LOC_REJ_SAC_FILE}
            fi

        #FI2: end check empty file
        fi

    #FOR END
    done

else
    message "Warning: PCS-LOCATION not loaded"
fi
#IF1: END OF LOC FLOW


##############################################################################################################################################################
#  MAIN PROGRAM CONTENT (main flow) CHECK - ERROR
##############################################################################################################################################################
if [ $PROCESS_BU = "Y" ] || [ $PROCESS_LOC = "Y" ]; then

    if [ -s ${O_PCS_IN_ERR} ]; then
        rm ${O_PCS_IN_ERR}
    fi

cat > ${RETL_FLOW_FILE} << EOF
        <FLOW name = "${PROGRAM_NAME}.flw">
        <!-- Check if any record was rejected during merge -->
        ${DBREAD}
            <PROPERTY name = "query">
                 <![CDATA[
                     select distinct 1 from ${XXADEO_RMS}.XXADEO_PCS_IN_ERR
                 ]]>
            </PROPERTY>
            <OUTPUT   name = "xxadeo_pcs_in_err.v"/>
        </OPERATOR>

        <OPERATOR type="export">
            <INPUT name="xxadeo_pcs_in_err.v"/>
            <PROPERTY name="outputfile" value="${O_PCS_IN_ERR}"/>
        </OPERATOR>
        </FLOW>
EOF

    ###############################################################################
    #  Execute the flow
    ###############################################################################
    ${RFX_EXE} ${RFX_OPTIONS} -f ${RETL_FLOW_FILE}
    exit_stat=$?
    checkerror -e $exit_stat -m "Program failed - check $ERR_FILE"


    # Evaluate if the file has any value
    if [ -s ${O_PCS_IN_ERR} ]; then
            message "ERROR: Records have been rejected on load, please check table ${XXADEO_RMS}.XXADEO_PCS_IN_ERR for more information."
            message "Program completed with errors"

            #remove store/area/currency file
            rm ${O_PCS_SAC_TMP}

            #Exit program
            exit 254
    fi

fi #END OF CHECK_ERROR FLOW


###############################################################################
#Cleanup and exit:
###############################################################################
if [ -f $STATUS_FILE ]; then rm $STATUS_FILE; fi

#remove store/area/currency file
rm ${O_PCS_SAC_TMP}

message "Program completed successfully"
rmse_terminate 0


