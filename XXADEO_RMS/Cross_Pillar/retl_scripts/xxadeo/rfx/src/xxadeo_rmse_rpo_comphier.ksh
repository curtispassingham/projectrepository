#!/bin/ksh

##########################################################################
#
#  The xxadeo_rmse_rpo_comphier.ksh program extracts competitor hierarchy data
#  from the COMPETITOR table and places this data into a
#  flat file (comp.csv.dat) to be be loaded into RPAS.
#
##########################################################################

################## PROGRAM DEFINE #####################
#########     (must be the first define)     ##########

export PROGRAM_NAME="xxadeo_rmse_rpo_comphier"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
    error_check=1
    if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
    then
        message "ERROR: Parameter 2 is not valid: ${2}"
        message "    Parameter 2 must be true, false or empty (true)"
        rmse_terminate 1
    elif [[ "${2}" = "NO_CHECK" ]]
    then
        error_check=0
    fi
    #message "check_error_and_clean_before_exit: error_check: ${error_check}"

    if [[ $1 -ne 0 && error_check -eq 1 ]]
    then
        rm -f "${OUTPUT_FILE}.retl"
        checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
    elif [[ error_check -eq 0 ]]
    then
        rm -f "${OUTPUT_FILE}.retl"
        checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        rmse_terminate $1
    fi
}

####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export DATA_DIR="${RETL_OUT}/RPO"

export OUTPUT_FILE=${DATA_DIR}/comp.csv.dat

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read in the competitor hierarchy data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            SELECT comp.competitor || '_min'                             COMPETITOR_ID,
                   SUBSTR(comp.comp_name, 1, 20) || '@Min'               COMPETITOR_LABEL
              FROM ${XXADEO_RMS}.competitor comp
            UNION ALL
            SELECT comp.competitor || '_tp'                              COMPETITOR_ID,
                   SUBSTR(comp.comp_name, 1, 100) || '@Target Price'     COMPETITOR_LABEL
              FROM ${XXADEO_RMS}.competitor comp
          ORDER BY COMPETITOR_ID
         ]]>
      </PROPERTY>
      <OUTPUT   name = "comp_data.v"/>
   </OPERATOR>


   <OPERATOR type="convert">
      <INPUT    name="comp_data.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
          <CONVERTSPECS>
                <CONVERT destfield="COMPETITOR_ID" sourcefield="COMPETITOR_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="COMPETITOR_LABEL" sourcefield="COMPETITOR_LABEL">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
        </CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="comp_data_1.v"/>
   </OPERATOR>


   <!--  Write out the organizational heirarchy data to a flat file:  -->
   <OPERATOR type="export">
      <INPUT    name="comp_data_1.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}.retl"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

###############################################
#  Execute the RETL flow that had previously
#  been copied into xxadeo_rmse_rpo_comphier.xml:
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
check_error_and_clean_before_exit $?


###############################################################################
#  Convert output data files to CSV format
###############################################################################

csv_converter "retlsv-csv" "${OUTPUT_FILE}.retl" "${OUTPUT_FILE}"
check_error_and_clean_before_exit $?

###############################################################################
#  Add information in output data files
###############################################################################
echo 'MIN,@Min Competitor Price' >> ${OUTPUT_FILE}
echo 'MAX,@Max Competitor Price' >> ${OUTPUT_FILE}
echo 'AVG,@Average Competitor Price' >> ${OUTPUT_FILE}


###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

#log_num_recs ${RETL_OUTPUT_FILE}


#######################################
#  Do error checking:
#######################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#  Remove the status file:

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."

#  Cleanup and exit:

check_error_and_clean_before_exit 0 "NO_CHECK"