#!/bin/ksh

##########################################################################
#
#  Title: Extraction of the Incoterm Cost Paramenters
#  Description:
#    The xxadeo_rmse_costing_incoterm.ksh program extract the incoterm cost 
#    parameters from RMS tables and places the data into a flat file
#    according target required format to be be loaded into RPAS module (Costing).
#  Version:		0.2
#  Owner: Oracle Retail (Nearshore - PVI )
#  History: 	0.1 - Initial Release
#		0.2 - Changed name of output file according target system.
#
#
###############################################################################

################## PROGRAM DEFINE #####################
#########     (must be the first define)     ##########

export PROGRAM_NAME="xxadeo_rmse_costing_incoterm"
EXE_DATE=`date +"%G%m%d-%H%M%S"`;

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

##################  INPUT/OUTPUT DEFINES ######################
########### (this section must come after INCLUDES) ###########

#EXE_DATE=`date +"%G%m%d-%H%M%S"`

# --INPUTS

#export DATA_DIR_IN="${RETL_IN}/CATMAN"

# --Get Name of the last file that has been provided by RMS about Item Translations
#INPUT_FILE_NAME=`ls -t ${DATA_DIR_IN} | grep 'XXRMS219_ITEMMASTERTL_.*\.dat$' | head -1`

#export INPUT_FILE=${DATA_DIR_IN}/${INPUT_FILE_NAME}

#export INPUT_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_itemtranslation.schema

# --OUTPUTS

export DATA_DIR_OUT="${RETL_OUT}/COSTING"

#export OUTPUT_FILE=${DATA_DIR_OUT}/"XXADEO_XXRMS235_INCOTERM_${EXE_DATE}.dat"
export OUTPUT_FILE=${DATA_DIR_OUT}/"incoterm.csv.ovr"

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema


###################  FUNCTIONS DEFINES ###################
######## (this section must come after FUNCTIONS) ########

######################################################################
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
######################################################################
function USAGE
{
   echo "USAGE: . $pgmName "
}

######################################################################
# Function Name: PROCESS
# Purpose      : Defines the flow of the program should be invoked
######################################################################
function PROCESS
{
####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

###############################################################################
#  Convert input CSV file to data file format
###############################################################################

# -- The default field delimiter: delimiter="0x1F"
# -- With parameter csv-retlsv, convert file from CSV to DATA file.
#csv_converter "csv-retlsv" "${INPUT_FILE}" "${INPUT_FILE}.retl"
#checkerror -e $? -m "Convert from CSV file to Data file failed - check $ERR_FILE"


########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">
    ${DBREAD}
	<PROPERTY name="query">
		<![CDATA[
		  SELECT BU.BU
			,SUPPLIER.SUPPLIER
			,INCOTERM.PURCHASE_TYPE AS INCOTERM
		    FROM ${XXADEO_RMS}.XXADEO_BU_OU BU,
			 ${XXADEO_RMS}.PARTNER_ORG_UNIT PARTNER_OU,
			 ${XXADEO_RMS}.SUPS SUPPLIER,
			 ${XXADEO_RMS}.SUP_INV_MGMT INCOTERM
		   WHERE BU.OU = PARTNER_OU.ORG_UNIT_ID
		     AND PARTNER_OU.PARTNER = SUPPLIER.SUPPLIER
		     AND SUPPLIER.SUPPLIER = INCOTERM.SUPPLIER (+)
		     AND INCOTERM.DEPT IS NULL
		     AND INCOTERM.LOCATION IS NULL
		     AND exists (select 'Y' from ${XXADEO_RMS}.addr adr where adr.module = 'SUPP' and adr.addr_type = '04' and adr.key_value_1 = SUPPLIER.supplier)
		     AND not exists (select '1' from ${XXADEO_RMS}.addr where module = 'SUPP' and addr_type = '06' and key_value_1 = SUPPLIER.supplier)
		]]> 
	</PROPERTY>
	<OUTPUT name="incoterm_foreach_bu_supplier.v"/>
    </OPERATOR>

    <OPERATOR type="export">
        <INPUT    name="incoterm_foreach_bu_supplier.v"/>
        <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
        <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
     </OPERATOR>
</FLOW>

EOF

###############################################
#  Execute the RETL flow
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

###############################################################################
#  Convert output data files to CSV format
###############################################################################

# -- The default field delimiter: delimiter="0x1F"
#csv_converter "retlsv-csv" "${OUTPUT_FILE}.retl" "${OUTPUT_FILE}"
#checkerror -e $? -m "Convert from Data file to CSV file failed - check $ERR_FILE"

###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${OUTPUT_FILE}

#######################################
#  Do error checking:
#######################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#  Remove the status file:

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."
}

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
######################################################################
#     MAIN
######################################################################
#////////////////////////////////////////////////////////////////////

#----------------------------------------------
#  Log start message and define the RETL flow file
#----------------------------------------------

if [[ $# -gt 0  ]]; then
   USAGE
   rmse_terminate 1
else
  PROCESS
fi

# --Clean
#rm -f "${INPUT_FILE}.retl"
#rm -f "${OUTPUT_FILE}.retl"
#rm -f "${OUTPUT_FILE}.retl.tmp"

message  "${PROGRAM_NAME}.ksh  - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

rmse_terminate 0
