#!/bin/ksh

##########################################################################
#
#  Title: Extraction retail price of the items per store
#
#  Description:
#  The xxadeo_rpme_simtar_retailprince.ksh program extract the item price information for each store from
#  RPM tables and places the data into a flat file (XXADEO_XXRPM204_RETAIL_PRICE_YYYYMMDD-HH24MISS.dat)
#  to be be loaded into RPAS module (SimTar).
#
#  Workfile:            |  xxadeo_rpme_simtar_retailprice.ksh
#  Version:             |  0.3
#  Owner:               |  Oracle Dev Center - (PVI)
#  Creation Date:       |  20180928-1800
#  Modify Date:         |  20181129-1800
#  ---------------------------------------------------------------------------
#  History: 	0.1 - Initial Release
#		0.2 - Chnaged name of output file according target system.
#		0.3 - Bug Fixing: BUG_701: The changes are:
#			- Added ITEM_MASTER on the query to get only the items
#			that where status equal 'A' and item level equal '1'
#			- Changed the source table and change from full interface
#			to delta as default.
#			- Script allow to methods. Full and Delta. 
#
###############################################################################

################## PROGRAM DEFINE #####################
#########     (must be the first define)     ##########

export PROGRAM_NAME="xxadeo_rpme_simtar_retailprice"
EXE_DATE=`date +"%G%m%d-%H%M%S"`;
FIELD_SEP="\x1F";

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env

. ${LIB_DIR}/xxadeo_rmse_lib.ksh

##################  INPUT/OUTPUT DEFINES ######################
########### (this section must come after INCLUDES) ###########

# --INPUTS

#export DATA_DIR_IN="${RETL_IN}/CATMAN"

# --Get Name of the last file that has been provided by RMS about Item Translations
#INPUT_FILE_NAME=`ls -t ${DATA_DIR_IN} | grep 'XXRMS219_ITEMMASTERTL_.*\.dat$' | head -1`

#export INPUT_FILE=${DATA_DIR_IN}/${INPUT_FILE_NAME}

#export INPUT_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_itemtranslation.schema

# --OUTPUTS

export DATA_DIR_OUT=${RETL_OUT}/SIMTAR

#export OUTPUT_FILE=${DATA_DIR_OUT}/XXADEO_XXRPM204_RETAIL_PRICE_${EXE_DATE}.dat
export OUTPUT_FILE=${DATA_DIR_OUT}/pr_vente_mag.csv.ovr
#export OUTPUT_FILE_REJECTED=${LOG_DIR}/${PROGRAM_NAME}.rejected

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

OUTPUT_FORMAT_DATE="YYYYMMDD"

#  Query(s)

QUERY_FULL=`echo "SELECT
        ACTION_DATE,
        ITEM,
        STORE,
        ITEM_PRICE
FROM (SELECT    PRICE_H.ACTION_DATE,
                PRICE_H.ITEM,
                PRICE_H.LOC AS STORE,
                PRICE_H.UNIT_RETAIL AS ITEM_PRICE,
                ROW_NUMBER() OVER(PARTITION BY PRICE_H.ITEM, PRICE_H.LOC ORDER BY PRICE_H.ACTION_DATE DESC, PRICE_H.TRAN_TYPE DESC) RANKING
        FROM    PRICE_HIST PRICE_H,
                ITEM_MASTER ITEM_M
        WHERE PRICE_H.TRAN_TYPE  IN (0,4)
          AND PRICE_H.ITEM       = ITEM_M.ITEM
          AND PRICE_H.LOC_TYPE   = 'S'
          AND ITEM_M.ITEM_LEVEL  = 1
          AND ITEM_M.STATUS      = 'A')
WHERE RANKING = 1"`

QUERY_DELTA=`echo "SELECT
        ACTION_DATE,
        ITEM,
        STORE,
        ITEM_PRICE
FROM (SELECT    PRICE_H.ACTION_DATE,
                PRICE_H.ITEM,
                PRICE_H.LOC AS STORE,
                PRICE_H.UNIT_RETAIL AS ITEM_PRICE,
                ROW_NUMBER() OVER(PARTITION BY PRICE_H.ITEM, PRICE_H.LOC ORDER BY PRICE_H.ACTION_DATE DESC, PRICE_H.TRAN_TYPE DESC) RANKING
        FROM    PRICE_HIST PRICE_H,
                ITEM_MASTER ITEM_M
        WHERE PRICE_H.TRAN_TYPE  IN (0,4)
          AND PRICE_H.ITEM       = ITEM_M.ITEM
          AND TRUNC(PRICE_H.ACTION_DATE) BETWEEN TRUNC(GET_VDATE-3) AND TRUNC(GET_VDATE+1)
          AND PRICE_H.LOC_TYPE   = 'S'
          AND ITEM_M.ITEM_LEVEL  = 1
          AND ITEM_M.STATUS      = 'A')
WHERE RANKING = 1"`

######## (this section must come after FUNCTIONS) ########

######################################################################
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
######################################################################
function USAGE
{
   echo "USAGE: . $pgmName <mode>

	<mode>	[FULL|full|DELTA|delta]
		Operation mode of the Script.Default, it is Delta Mode.
"
}

######################################################################
# Function Name: PROCESS
# Purpose      : Defines the flow of the program should be invoked
######################################################################
function PROCESS
{
####################################################
#  Log start message and define the RETL flow file
####################################################

MODE=$1

message "Program started in ${MODE} mode..."

#message "The input file is "${INPUT_FILE}


if [[ $MODE == "FULL" ]] || [[ $MODE == "full" ]]; then
	QUERY=${QUERY_FULL}
elif [[ $MODE == "DELTA" ]] || [[ $MODE == "delta" ]]; then
	QUERY=${QUERY_DELTA}
else
	QUERY=${QUERY_DELTA}
fi

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

###############################################################################
#  Convert input CSV file to data file format
###############################################################################

# -- The default field delimiter: delimiter="0x1F"
# -- With parameter csv-retlsv, convert file from CSV to DATA file.
#csv_converter "csv-retlsv" "${INPUT_FILE}" "${INPUT_FILE}.retl"
#checkerror -e $? -m "Convert from CSV file to Data file failed - check $ERR_FILE"


########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
		${QUERY}
         ]]>
      </PROPERTY>
      <OUTPUT name="currentItemPrice_L1.v"/>
   </OPERATOR>

  <OPERATOR type="export">
        <INPUT    name="currentItemPrice_L1.v"/>
        <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
        <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
  </OPERATOR>
</FLOW>

EOF

###############################################
#  Execute the RETL flow
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
checkerror -e $? -m "RETL flow failed - check $ERR_FILE"


###############################################################################
#  Convert output data files to CSV format
###############################################################################

# -- The default field delimiter: delimiter="0x1F"
#csv_converter "retlsv-csv" "${OUTPUT_FILE}.retl" "${OUTPUT_FILE}"
#checkerror -e $? -m "Convert from Data file to CSV file failed - check $ERR_FILE"

###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${OUTPUT_FILE}

#log_num_recs ${OUTPUT_FILE_REJECTED}

#######################################
#  Do error checking:
#######################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#  Remove the status file:

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

#  Remove Rejected File if is Empty
#[[ -s ${OUTPUT_FILE_REJECTED} ]] || rm ${OUTPUT_FILE_REJECTED}

message "Program completed successfully."
}

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
######################################################################
#     MAIN
######################################################################
#////////////////////////////////////////////////////////////////////

#----------------------------------------------
#  Log start message and define the RETL flow file
#----------------------------------------------

if [[ $# -gt 2  ]]; then
   USAGE
   rmse_terminate 1
else
  if [[ $# -eq 0 ]]; then
  	# DELTA MODE AS DEFAULT
	PROCESS "DELTA"
  else
	PROCESS $1
  fi
fi

# --Clean

message  "${PROGRAM_NAME}.ksh  - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

rmse_terminate 0
