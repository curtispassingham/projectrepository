#!/bin/ksh

#########################################################################
#
#  xxadeo_pre_rmse.ksh defines the system parameters. The parameters
#  are obtained from the data base tables and written into
#  ".txt" files in the "etc" directory.
#
#  Usage: xxadeo_pre_rmse.ksh  [-c]
#
#  -c: This option is used to determine what option is to
#      be placed on the xxadeo_rmse_config.env command line when
#      it called by this script.
#
#      This option will cause xxadeo_rmse_config.env to use the current
#      date to initialize FILE_DATE instead of setting it to VDATE.
#      The current date will be used regardless of how DATE_TYPE is
#      set in xxadeo_rmse_config.env. That way there is no need to manually
#      set up the vdate.txt file before running this script. (FILE_DATE
#      is the date that is used to name the error, log and status files.)
#
#      The normal mode for xxadeo_pre_rmse.ksh (without the -c option)
#      is that when it calls xxadeo_rmse_config.env, FILE_DATE will be
#      set to VDATE or the current date, depending on how DATE_TYPE
#      is set in xxadeo_rmse_config.env. If DATE_TYPE is set to "vdate",
#      and if the vdate.txt file does not exist or is empty,
#      xxadeo_rmse_config.env (and this program) will exit with an error
#      message.
#
#      The use of this option does not affect what date is used
#      by any of the other RETL scripts that run after this
#      script is done.
#
#      After xxadeo_pre_rmse.ksh has run, when the other RETL scripts
#      are run they will call xxadeo_rmse_config.env with no options
#      on the command line and their files will be named using
#      VDATE or the current date, depending on how DATE_TYPE is
#      set in xxadeo_rmse_config.env.
#
#          This file has been modified for XXADEO custom scripts,
#      It is configured to update the variables VDATE and NEXT_VDATE
#      Should new variables are needed, please refer to the original
#      pre_rmse_rpas.ksh found in $RETLforRPAS
#
#########################################################################

export PROGRAM_NAME="xxadeo_pre_rmse"

#  Get the command line option, if present:

if [ "$1" = "-c" ]; then
  date_option=-c
  shift
else  date_option=""
fi

####################################################################
#  xxadeo_rmse_config.env is invoked (included) with two options.
#  The -t option will prevent xxadeo_rmse_config.env from trying to
#  access the ".txt" files which may not be set up yet. The
#  $date_option will be a "-c" or blank. See the comments above
#  for an explanation of the purpose of the "-c" option. The "$*"
#  at the end of the command line is only necessary to preserve
#  any additional xxadeo_pre_rmse.ksh command line arguments or options
#  that may be added to a future version of xxadeo_pre_rmse.ksh.
####################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env  -t  $date_option  $*

. ${LIB_DIR}/xxadeo_rmse_lib.ksh

message "Program started ..."

########################################################
#  Back up existing output files
########################################################

# Note that prime_exch_rate is treated separately
OUT_LIST="vdate \
          next_vdate"

for out in ${OUT_LIST}; do
  if [[ -f ${ETC_DIR}/${out}.txt ]] ; then
    mv -f ${ETC_DIR}/${out}.txt ${ETC_DIR}/${out}.txt.old
  fi
done

########################################################
#  Create RETL flow in a logged file
#  This flow reads database-based system parameters.
########################################################
FLOW_FILE_1="${LOG_DIR}/${PROGRAM_NAME}_1.xml"
cat > ${FLOW_FILE_1} << EOF
   <FLOW name="${PROGRAM_NAME}_1.flw">

      ${DBREAD}
         <PROPERTY name="query">
            <![CDATA[
               SELECT VDATE,
                      VDATE + 1 NEXT_VDATE
                 FROM ${XXADEO_RMS}.PERIOD
            ]]>
         </PROPERTY>
         <OUTPUT name="sys_var.v"/>
      </OPERATOR>

      <OPERATOR type="copy">
         <INPUT name="sys_var.v"/>
         <OUTPUT name="c_sys_var1.v"/>
         <OUTPUT name="c_sys_var2.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod">
         <PROPERTY  name="keep" value="VDATE"/>
         <INPUT name="c_sys_var1.v"/>
         <OUTPUT name="o_sys_var1.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/vdate.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_var1.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod">
         <PROPERTY  name="keep" value="NEXT_VDATE"/>
         <INPUT name="c_sys_var2.v"/>
         <OUTPUT name="o_sys_var2.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/next_vdate.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_var2.v"/>
      </OPERATOR>

   </FLOW>
EOF

########################################################
#  Execute the RETL flow file to read database-based
#  system parameters.
########################################################
${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE_1}

########################################################
#  RETL error check
########################################################
checkerror -e $? -m "Loading of all system variable from system options failed to complete"

########################################################
#  Verify non-zero-length output files were written
########################################################

for out in ${OUT_LIST}; do
  if [[ ! -s ${ETC_DIR}/${out}.txt ]] ; then
    checkerror -e 1 -m "Error: ${out}.txt is either missing in ${ETC_DIR} or is a zero byte file"
  fi
done

########################################################
#  Remove the status file, if present
########################################################
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

########################################################
#  Successful completion!
########################################################
message "Program completed successfully"

