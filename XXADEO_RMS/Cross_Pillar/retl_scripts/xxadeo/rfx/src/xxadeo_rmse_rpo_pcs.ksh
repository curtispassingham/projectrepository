#!/bin/ksh

########################################################
# Script extracts item, price zone and price session information from the RMS
# XXADEO_RMSE_RPO_PCS table
########################################################

########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################

export PROGRAM_NAME='xxadeo_rmse_rpo_pcs'
export OUTPUT_FILE_NAME='dl1orgitcst.csv.ovr'
export extractDate=`date +"%G%m%d-%H%M%S"`

########################################################
#  INCLUDES ########################
#  This section must come after PROGRAM DEFINES
########################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

########################################################
#  OUTPUT DEFINES
#  This section must come after INCLUDES
########################################################

export DATA_DIR="${RETL_OUT}/RPO"
export OUTPUT_FILE_PCS=$DATA_DIR/${OUTPUT_FILE_NAME}_${extractDate}.dat
export OUTPUT_SCHEMA_PCS=$SCHEMA_DIR/${PROGRAM_NAME}.schema

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Function get_date to retrieve format date YY/MM/DD from an input date YYMMDD
#  This function has 2 input parameters:
#     1 input date
#     2 information about which date is provided (vdate or input date from ksh)
###############################################################################
export NEXT_VDATE=$(cat ${ETC_DIR}/next_vdate.txt)

get_date()
{
        input_date=""
        if [ $1 -ne "" ]; then
                input_date=`date --date="$1" '+%y/%m/%d'`
                if [ $? -ne 0 ]; then
                        echo "ERROR - $2 is not valid"
                        exit 1
                fi
        else
                echo "ERROR - $2 is empty"
                exit 1
        fi
        echo $input_date
}

# Get the  input_date_END
input_date_END=$(get_date $NEXT_VDATE 'next_vdate')

# Get the input_date_START
input_date_START=$input_date_END
if [[ $# -eq 1 ]]; then
        input_date_START=$(get_date $1 'input argument')
else
        if [[ $input_date_END -eq "" ]]; then
                if [ $? -ne 0 ]; then
                        echo "ERROR - input argument and vdate is not valid"
                        exit 1
                fi
        fi
fi

message "Data extracted from: ${input_date_START} to: ${input_date_END} (Date format YY/MM/DD)"

###############################################################################
#  Create a disk-based flow file
###############################################################################
FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"
cat > ${FLOW_FILE} << EOF
   <FLOW name = "${PROGRAM_NAME}.flw">

      ${DBREAD}
         <PROPERTY name = "query" >
                <![CDATA[
                select pcs.item ITEM_ID, zl.zone_id PRICE_ZONE_ID, max(pcs.pcs) ITEM_COST_PRICE
                  from  Xxadeo_Location_Pcs pcs, rpm_zone_location zl, (select distinct pcs.item, zl.zone_id
                                                                          from Xxadeo_Location_Pcs pcs, rpm_zone_location zl
                                                                          where store in (select location
                                                                                          from rpm_zone_location zl1, xxadeo_rpm_bu_price_zones bpz
                                                                                          where zl1.zone_id = bpz.zone_id
                                                                                          and bu_zone_type in (select value_1 from xxadeo_mom_dvm
                                                                                          where func_area = 'RPO_ZONES'
                                                                                          and parameter = 'RPO_ALLOWED_ZONE_TYPE'))
                                                                          and pcs.effective_date >= to_date('${input_date_START}','YY/MM/DD')
                                                                          and pcs.effective_date <= to_date('${input_date_END}','YY/MM/DD')
                                                                                          ) aux, item_master im
                  where pcs.item = aux.item
                  and zl.location = pcs.store
                  and zl.zone_id = aux.zone_id
                  and pcs.effective_date <= to_date('${input_date_END}','YY/MM/DD')
                  and pcs.Status = 'A'
                  -- the below clauses will force that all items extracted are L2 Items (so only defined by a SKU and Barcode)
                  and pcs.item = im.item
                  and im.item_level = 1
                  and im.tran_level= 1
                  and im.status = 'A'
                  group by pcs.item, zl.zone_id
                union
                select im.item ITEM_ID, zl.zone_id PRICE_ZONE_ID, max(pcs.pcs) ITEM_COST_PRICE
                from  Xxadeo_Location_Pcs pcs, rpm_zone_location zl, (select distinct pcs.item, zl.zone_id
                                                                        from Xxadeo_Location_Pcs pcs, rpm_zone_location zl
                                                                        where store in (select location
                                                                                        from rpm_zone_location zl1, xxadeo_rpm_bu_price_zones bpz
                                                                                        where zl1.zone_id = bpz.zone_id
                                                                                        and bu_zone_type in (select value_1 from xxadeo_mom_dvm
                                                                                        where func_area = 'RPO_ZONES'
                                                                                        and parameter = 'RPO_ALLOWED_ZONE_TYPE'))
                                                                        and pcs.effective_date >= to_date('${input_date_START}','YY/MM/DD')
                                                                        and pcs.effective_date <= to_date('${input_date_END}','YY/MM/DD')
                                                                                        ) aux, item_master im
                  where pcs.item = aux.item
                  and zl.location = pcs.store
                  and zl.zone_id = aux.zone_id
                  and pcs.effective_date <= to_date('${input_date_END}','YY/MM/DD')
                  and pcs.Status = 'A'
                  -- the below clauses will force that all items extracted are L3 Items (so defined by a STYLE, SKU and Barcode)
                  and pcs.item = im.item_parent
                  and im.item_level = 2
                  and im.tran_level= 2
                  and im.status = 'A'
                  group by im.item, zl.zone_id
                  ]]>
         </PROPERTY>
         <OUTPUT   name = "output_pcs.v"/>
      </OPERATOR>

        <OPERATOR type="convert">
      <INPUT    name="output_pcs.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
          <CONVERTSPECS>
                <CONVERT destfield="ITEM_ID" sourcefield="ITEM_ID">
                 <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="PRICE_ZONE_ID" sourcefield="PRICE_ZONE_ID">
                 <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                <CONVERT destfield="ITEM_COST_PRICE" sourcefield="ITEM_COST_PRICE">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
        </CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="output_conv_pcs.v"/>
   </OPERATOR>

      <OPERATOR type="export">
         <INPUT    name="output_conv_pcs.v"/>
         <PROPERTY name="outputfile" value="$OUTPUT_FILE_PCS"/>
         <PROPERTY name="schemafile" value="$OUTPUT_SCHEMA_PCS"/>
      </OPERATOR>

   </FLOW>
EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check $ERR_FILE"

###############################################################################
# Remove the status file
###############################################################################

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
# cleanup and exit
###############################################################################

rmse_terminate 0