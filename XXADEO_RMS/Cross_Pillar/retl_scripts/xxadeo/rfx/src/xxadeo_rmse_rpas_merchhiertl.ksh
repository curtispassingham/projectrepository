#!/bin/ksh

########################################################
# Script extracts item information from the RMS
# item_master table
########################################################

########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################

export PROGRAM_NAME="xxadeo_merchhiertl"
extractDate=`date +"%G%m%d-%H%M%S"`


########################################################
#  INCLUDES ########################
#  This section must come after PROGRAM DEFINES
########################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

########################################################
#  OUTPUT DEFINES
#  This section must come after INCLUDES
########################################################
export DATA_DIR="${RETL_OUT}/RPAS"


export OUTPUT_FILE_COM=${DATA_DIR}/XXRMS216_COMPHEADTL_${extractDate}.dat
export OUTPUT_FILE_DIV=${DATA_DIR}/XXRMS216_DIVISIONTL_${extractDate}.dat
export OUTPUT_FILE_GRP=${DATA_DIR}/XXRMS216_GROUPSTL_${extractDate}.dat
export OUTPUT_FILE_DEP=${DATA_DIR}/XXRMS216_DEPSTL_${extractDate}.dat
export OUTPUT_FILE_CLS=${DATA_DIR}/XXRMS216_CLASSTL_${extractDate}.dat
export OUTPUT_FILE_SUB=${DATA_DIR}/XXRMS216_SUBCLASSTL_${extractDate}.dat

export OUTPUT_SCHEMA_COM=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhiertl_comphead.schema
export OUTPUT_SCHEMA_DIV=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhiertl_division.schema
export OUTPUT_SCHEMA_GRP=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhiertl_groups.schema
export OUTPUT_SCHEMA_DEP=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhiertl_deps.schema
export OUTPUT_SCHEMA_CLS=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhiertl_class.schema
export OUTPUT_SCHEMA_SUB=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhiertl_subclass.schema



###############################################################################
#  FUNCTIONS
###############################################################################
function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${OUTPUT_FILE_COM}.retl"
                rm -f "${OUTPUT_FILE_COM}"
                rm -f "${OUTPUT_FILE_DIV}.retl"
                rm -f "${OUTPUT_FILE_DIV}"
                rm -f "${OUTPUT_FILE_GRP}.retl"
                rm -f "${OUTPUT_FILE_GRP}"
                rm -f "${OUTPUT_FILE_DEP}.retl"
                rm -f "${OUTPUT_FILE_DEP}"
                rm -f "${OUTPUT_FILE_CLS}.retl"
                rm -f "${OUTPUT_FILE_CLS}"
                rm -f "${OUTPUT_FILE_SUB}.retl"
                rm -f "${OUTPUT_FILE_SUB}"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
                rm -f "${OUTPUT_FILE_COM}.retl"
                rm -f "${OUTPUT_FILE_DIV}.retl"
                rm -f "${OUTPUT_FILE_GRP}.retl"
                rm -f "${OUTPUT_FILE_DEP}.retl"
                rm -f "${OUTPUT_FILE_CLS}.retl"
                rm -f "${OUTPUT_FILE_SUB}.retl"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
                rmse_terminate $1
        fi
}



###############################################################################
# Program start
###############################################################################

message "Program started ..."
RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml



###############################################################################
#  Create a disk-based flow file
###############################################################################


cat > ${RETL_FLOW_FILE} << EOF
   <FLOW name = "${PROGRAM_NAME}.flw">

      ${DBREAD}
         <PROPERTY name = "query" >
                <![CDATA[
                         select iso_code                            AS lang_iso_code
                               ,LOWER(description)                  AS lang_name
                               ,company                             AS rpas_company_id
                               ,lpad(company,5,0) || ' ' || co_name AS rpas_company_name
                          from ${XXADEO_RMS}.comphead_tl ctl,
                               ${XXADEO_RMS}.lang        lg
                         where lg.lang = ctl.lang
                    ]]>
         </PROPERTY>
         <OUTPUT   name = "comphead_tl.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <INPUT    name="comphead_tl.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE_COM}.retl"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_COM}"/>
      </OPERATOR>

      ${DBREAD}
         <PROPERTY name = "query" >
                <![CDATA[
                         select iso_code                              AS lang_iso_code
                               ,LOWER(description)                    AS lang_name
                               ,division                              AS rpas_division_id
                               ,lpad(division,5,0) || ' ' || div_name AS rpas_div_name
                          from ${XXADEO_RMS}.division_tl dtl,
                               ${XXADEO_RMS}.lang        lg
                         where lg.lang = dtl.lang
            ]]>
         </PROPERTY>
         <OUTPUT   name = "division_tl.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <INPUT    name="division_tl.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE_DIV}.retl"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_DIV}"/>
      </OPERATOR>
     ${DBREAD}
         <PROPERTY name = "query" >
                <![CDATA[
                         select iso_code                                AS lang_iso_code
                               ,LOWER(description)                      AS lang_name
                               ,group_no                                AS rpas_group_no
                               ,lpad(group_no,5,0) || ' ' || group_name AS rpas_group_name
                          from ${XXADEO_RMS}.groups_tl gtl,
                               ${XXADEO_RMS}.lang lg
                         where lg.lang = gtl.lang
            ]]>
         </PROPERTY>
         <OUTPUT   name = "groups_tl.v"/>
      </OPERATOR>
      <OPERATOR type="export">
         <INPUT    name="groups_tl.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE_GRP}.retl"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_GRP}"/>
      </OPERATOR>

          ${DBREAD}
         <PROPERTY name = "query" >
                <![CDATA[
                         select iso_code                           AS lang_iso_code
                               ,LOWER(description)                 AS lang_name
                               ,dept                               AS rpas_dept_id
                               ,lpad(dept,5,0) || ' ' || dept_name AS rpas_dept_name
                          from ${XXADEO_RMS}.deps_tl dtl,
                               ${XXADEO_RMS}.lang lg
                         where lg.lang = dtl.lang
            ]]>
         </PROPERTY>
         <OUTPUT   name = "depts_tl.v"/>
      </OPERATOR>
      <OPERATOR type="export">
         <INPUT    name="depts_tl.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE_DEP}.retl"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_DEP}"/>
      </OPERATOR>

          ${DBREAD}
         <PROPERTY name = "query" >
                <![CDATA[
                         select  iso_code                             AS lang_iso_code
                                ,LOWER(description)                   AS lang_name
                                ,dept || '_' || class                 AS rpas_class_id
                                ,lpad(class,5,0) || ' ' || class_name AS rpas_class_name
                           from ${XXADEO_RMS}.class_tl ctl,
                                ${XXADEO_RMS}.lang lg
                          where lg.lang = ctl.lang
            ]]>
         </PROPERTY>
         <OUTPUT   name = "class_tl.v"/>
      </OPERATOR>
      <OPERATOR type="export">
         <INPUT    name="class_tl.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE_CLS}.retl"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_CLS}"/>
      </OPERATOR>

          ${DBREAD}
         <PROPERTY name = "query" >
                <![CDATA[
                         select iso_code                                AS lang_iso_code
                               ,LOWER(description)                      AS lang_name
                               ,dept || '_' || class || '_' || subclass AS rpas_subclass_id
                               ,lpad(subclass,5,0) || ' ' || sub_name   AS rpas_subclass_name
                          from ${XXADEO_RMS}.subclass_tl stl,
                               ${XXADEO_RMS}.lang lg
                         where lg.lang = stl.lang
            ]]>
         </PROPERTY>
         <OUTPUT   name = "subclass_tl.v"/>
      </OPERATOR>
      <OPERATOR type="export">
         <INPUT    name="subclass_tl.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE_SUB}.retl"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_SUB}"/>
      </OPERATOR>

   </FLOW>
EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${RETL_FLOW_FILE}
check_error_and_clean_before_exit $?

###############################################################################
#  Handle RETL errors
###############################################################################
#-----------------------------------------
#  Convert output data files to CSV format
#-----------------------------------------

csv_converter "retlsv-csv" "${OUTPUT_FILE_SUB}.retl" "${OUTPUT_FILE_SUB}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${OUTPUT_FILE_CLS}.retl" "${OUTPUT_FILE_CLS}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${OUTPUT_FILE_COM}.retl" "${OUTPUT_FILE_COM}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${OUTPUT_FILE_DIV}.retl" "${OUTPUT_FILE_DIV}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${OUTPUT_FILE_GRP}.retl" "${OUTPUT_FILE_GRP}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${OUTPUT_FILE_DEP}.retl" "${OUTPUT_FILE_DEP}"
check_error_and_clean_before_exit $?


###############################################################################
#  cleanup and exit:
###############################################################################

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully"

check_error_and_clean_before_exit 0 "NO_CHECK"

