#!/bin/ksh

##########################################################
#
# Object Name:   xxadeo_rmse_rpo_itemvat.ksh
# Description:   The xxadeo_rmse_rpas_itemvat.ksh program extracts the VAT information from
#                RMS tables and places this data into a flat file (ol1itvat.csv.ovr) to be be loaded
#                into RPAS.
# Version:       1.6
# Author:        CGI
# Creation Date: 23/05/2018
# Last Modified: 23/05/2018
# History:
#               1.0 - Initial version
#               1.1 - Adjustments to the new file structure RETLforXXADEO,
#                     DATA_DIR and XXADEO_RMS
#               1.2 - Change 001 reflects the new rpm zone structure
#               1.3 - Add new control of VDATE format
#               1.4 - Add convert operator to make not nullable some fields on output schema
#               1.5 - Script modified to include the capability to extract data for an interval defined by
#                     START and END dates. The END date is always equal to the vdate
#               1.6 - Add filter item status = 'A'
#
##########################################################

################## PROGRAM DEFINE #####################
#########     (must be the first define)     ##########

export PROGRAM_NAME="xxadeo_rmse_rpo_itemvat"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export DATA_DIR="${RETL_OUT}/RPO"

export OUTPUT_FILE=${DATA_DIR}/ol1itvat.csv.ovr

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

###############################################################################
#  Function get_date to retrieve format date YY/MM/DD from an input date
#  This function has 2 input parameters:
#     1 input date
#     2 information about which date is provided (vdate or input date from ksh)
###############################################################################
get_date()
{
        input_date=""
        if [ $1 -ne "" ]; then
                input_date=`date --date="$1 + 1 day" '+%y/%m/%d'`
                if [ $? -ne 0 ]; then
                        echo "ERROR - $2 is not valid"
                        exit 1
                fi
        else
                echo "ERROR - $2 is empty"
                exit 1
        fi
        echo $input_date
}

# Get the  input_date_END
input_date_END=$(get_date $VDATE 'vdate')

# Get the input_date_START
input_date_START=$input_date_END
if [[ $# -eq 1 ]]; then
        input_date_START=$(get_date $1 'input argument')
else
        if [[ $input_date_END -eq "" ]]; then
                if [ $? -ne 0 ]; then
                        echo "ERROR - input argument and vdate is not valid"
                        exit 1
                fi
        fi
fi


########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################
cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read the vat data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
         SELECT ITEM_ID, PRICE_ZONE_ID, MAX(VAT_RATE) AS VAT_VALUE
         FROM
         (
         SELECT vit.item             ITEM_ID,
                  rz.zone_id           PRICE_ZONE_ID,
                  (vit.vat_rate / 100) VAT_RATE
             FROM ${XXADEO_RMS}.rpm_zone rz,
                  ${XXADEO_RMS}.rpm_zone_location rzl,
                  ${XXADEO_RMS}.store s,
                  ${XXADEO_RMS}.item_loc itl,
                  ${XXADEO_RMS}.item_master itm,
                  ${XXADEO_RMS}.vat_item vit
            WHERE rz.zone_id = rzl.zone_id
              AND rzl.location = s.store
              AND rzl.location = itl.loc
              AND itl.item = itm.item
              AND itm.item = vit.item
              AND rzl.loc_type = 0
              AND rz.zone_id IN (SELECT pzb.zone_id
                                   FROM ${XXADEO_RMS}.xxadeo_mom_dvm dvm,
                                        ${XXADEO_RMS}.xxadeo_rpm_bu_price_zones pzb
                                  WHERE pzb.bu_zone_type = dvm.value_1
                                    AND dvm.func_area = 'RPO_ZONES'
                                    AND dvm.parameter = 'RPO_ALLOWED_ZONE_TYPE') -- Only RPO allowed Zones
              AND itm.item_level = itm.tran_level -- Only SKU Items
              AND itm.sellable_ind = 'Y'
			  AND itm.status = 'A'
              AND vit.vat_type IN ('B', 'R') -- Only allowed types
              -- The VAT Region record of the Item must must match the one for the location
              AND vit.vat_region = s.vat_region
              AND vit.active_date <=  to_date('${input_date_END}', 'YY/MM/DD')
              -- Only get the changes for the Items that have been affected by a VAT change (during a cretain period or just for the ones changed between START END dates)
              AND itm.item in
                (
                select distinct item from ${XXADEO_RMS}.vat_item vitaux
                -- Delta extract coupled to the Start and End dates
                where vitaux.active_date >=  to_date('${input_date_START}', 'YY/MM/DD')
                and vitaux.active_date <=  to_date('${input_date_END}', 'YY/MM/DD')
                AND vitaux.vat_type IN ('B', 'R')
                )
           )
           GROUP BY item_id, price_zone_id
           ORDER BY price_zone_id,
                    item_id
      ]]>
      </PROPERTY>
      <OUTPUT   name = "vat_data.v"/>
   </OPERATOR>


   <OPERATOR type="convert">
      <INPUT    name="vat_data.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
          <CONVERTSPECS>
                <CONVERT destfield="VAT_VALUE" sourcefield="VAT_VALUE">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
        </CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="vat_data_1.v"/>
   </OPERATOR>


   <!--  Write out the vat data to a flat file:  -->

   <OPERATOR type="export">
      <INPUT    name="vat_data_1.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

##############################################
#  Execute the RETL flow that was
#  previously copied into rmse_rpas_wh.xml:
##############################################

$RFX_EXE  $RFX_OPTIONS  -f $RETL_FLOW_FILE
exit_stat=$?

message "Program completed. Exit status code: $exit_stat"

#######################################################
#  Do error checking on results of the RETL execution:
#######################################################

checkerror -e $exit_stat -m "Program failed - check $ERR_FILE"

#######################################################
#  Remove the status file, log the completion message
#  to the log and error files and clean up the files:
#######################################################

if [ -f $STATUS_FILE ]; then rm $STATUS_FILE; fi

message "Program completed successfully"

#  Cleanup and exit:

rmse_terminate 0
