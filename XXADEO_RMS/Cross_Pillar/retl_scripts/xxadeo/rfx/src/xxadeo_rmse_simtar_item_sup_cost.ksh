#!/bin/ksh

###############################################################################
#                                                                             #
# Object Name:   xxadeo_rmse_simtar_item_sup_cost.ksh                         #
# Description:   This custom ADEO script extracts the Item Supplier Cost      #
#                                                                             #
# Version:       1.5                                                          #
# Author:        CGI                                                          #
# Creation Date: 18/08/2018                                                   #
# Last Modified: 04/12/2018                                                   #
# History:                                                                    #
#               1.0 - Initial version                                         #
#               1.1 - Update the variables for environment, librairies        #
#                     and directory names. We have to use $RETL_OUT           #
#                     to put output files because this IR is                  #
#                     an extraction (RMS side)                                #
#               1.2 - Update the output filenames and change the output       #
#                     folder from RPAS to CATMAN (as CATMAN AND SIMTAR        #
#                     use the same domain on RPAS side).                      #
#                     Update ksh name and schemas name                        #
#               1.3 - Update the output data directory and change the output  #
#                     folder from CATMAN to RPAS                              #
#               1.4 - Update file name and output directory.                  #
#                     Update the request : add join table and filter.         #
#               1.5 - Update output file names:                               #
#                       Replace prix_achat.csv.rpl by prix_achat_bu.csv.rpl   #
#                       Replace prix_achat_futur.csv.rpl by                   #
#                       prix_achat_futur_app.csv.rpl                          #
#                     And add loc_type = 'S' for the extraction of output file#
#                     prix_achat_futur_app.csv.rpl                            #
#                                                                             #
###############################################################################


###############################################################################
# PROGRAM DEFINE
# (must be the first define)
###############################################################################

export PROGRAM_NAME="xxadeo_rmse_simtar_item_sup_cost"


###############################################################################
#  INCLUDES
#  This section must come after PROGRAM DEFINES
###############################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh


###############################################################################
#  OUTPUT DEFINES
#  This section must come after INCLUDES
###############################################################################

SCHEMA_DIR_XXADEO=${RETLforXXADEO}/rfx/schema

export O_COSTBU_SCHEMA=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}_bu.schema
export O_COSTLOC_SCHEMA=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}_loc.schema
export O_FUTURCOST_SCHEMA=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}_fut.schema


O_DATA_DIR_XXADEO=$RETL_OUT/SIMTAR
export O_COSTBU_FILE=$O_DATA_DIR_XXADEO/prix_achat_bu.csv.rpl
export O_COSTLOC_FILE=$O_DATA_DIR_XXADEO/prix_achat_mag.csv.rpl
export O_FUTURCOST_FILE=$O_DATA_DIR_XXADEO/prix_achat_mag_futur_app.csv.rpl


###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${O_COSTBU_FILE}"
                rm -f "${O_COSTLOC_FILE}"
                rm -f "${O_FUTURCOST_FILE}"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        fi
}


###############################################################################
#  MAIN PROGRAM CONTENT
###############################################################################

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE=${LOG_DIR}/${PROGRAM_NAME}.xml

cat > ${FLOW_FILE} << EOF
<FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query" >
                <![CDATA[
               select distinct ISC.ITEM ARTICLE,
                      XBO.BU,
                      ISC.SUPPLIER SITE_FOURNISSEUR,
                      ISC.UNIT_COST PRIX_ACHAT
                 FROM ${XXADEO_RMS}.ITEM_SUPP_COUNTRY ISC,
                      ${XXADEO_RMS}.XXADEO_BU_OU XBO,
                      ${XXADEO_RMS}.PARTNER_ORG_UNIT POU,
                      ${XXADEO_RMS}.ITEM_MASTER IM
                WHERE ISC.SUPPLIER = POU.PARTNER
                  AND POU.ORG_UNIT_ID = XBO.OU
                  AND ISC.ITEM=IM.ITEM
                  AND IM.STATUS='A'
                  AND IM.ITEM_LEVEL=1
                  ORDER BY ARTICLE, XBO.BU, SITE_FOURNISSEUR
          ]]>
         </PROPERTY>
         <OUTPUT   name = "costbu.v"/>
      </OPERATOR>

      <OPERATOR type="convert">
         <INPUT name="costbu.v"/>
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="BU" sourcefield="BU">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value=""/>
                     </CONVERTFUNCTION>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OUTPUT name="costbu_converted.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <INPUT    name="costbu_converted.v"/>
         <PROPERTY name="outputfile" value="${O_COSTBU_FILE}"/>
         <PROPERTY name="schemafile" value="${O_COSTBU_SCHEMA}"/>
      </OPERATOR>




        ${DBREAD}
         <PROPERTY name = "query" >
                <![CDATA[
               select distinct ISCL.ITEM ARTICLE,
                      ISCL.LOC SITE,
                      ISCL.SUPPLIER SITE_FOURNISSEUR,
                      ISCL.UNIT_COST PRIX_ACHAT
                 FROM ${XXADEO_RMS}.ITEM_SUPP_COUNTRY_LOC ISCL,
                      ${XXADEO_RMS}.ITEM_MASTER IM
                  WHERE ISCL.LOC_TYPE='S'
                  AND ISCL.ITEM=IM.ITEM
                  AND IM.STATUS='A'
                  AND IM.ITEM_LEVEL=1
                  ORDER BY ARTICLE, SITE, SITE_FOURNISSEUR
            ]]>
         </PROPERTY>
         <OUTPUT   name = "costloc.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <INPUT    name="costloc.v"/>
         <PROPERTY name="outputfile" value="${O_COSTLOC_FILE}"/>
         <PROPERTY name="schemafile" value="${O_COSTLOC_SCHEMA}"/>
      </OPERATOR>




        ${DBREAD}
         <PROPERTY name = "query" >
                <![CDATA[
               select distinct partialReq.item     ARTICLE,
                      partialReq.location SITE,
                      partialReq.supplier SITE_FOURNISSEUR,
                      partialReq.dates    DATES,
                      fc.base_cost        VALEUR_APP
                 FROM (  select item,
                                location,
                                supplier,
                                min(active_date) dates
                           from ${XXADEO_RMS}.future_cost
                          where to_char(active_date, 'YYYYMMDD') > ${VDATE}
                                AND loc_type = 'S'
                       group by item,
                                location,
                                supplier
                       order by item, location, supplier, dates
                      ) partialReq,
                      ${XXADEO_RMS}.future_cost fc,
                      ${XXADEO_RMS}.ITEM_MASTER IM
                where partialReq.item = fc.item
                  and partialReq.location = fc.location
                  and partialReq.supplier = fc.supplier
                  and partialReq.dates = fc.active_date
                  AND fc.ITEM=IM.ITEM
                  AND IM.STATUS='A'
                  AND IM.ITEM_LEVEL=1
                  AND fc.LOC_TYPE = 'S'
                  ORDER BY ARTICLE, SITE, SITE_FOURNISSEUR
            ]]>
         </PROPERTY>
         <OUTPUT   name = "futurcost.v"/>
      </OPERATOR>

      <OPERATOR type="convert">
         <INPUT name="futurcost.v"/>
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="DATES" sourcefield="DATES">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value=""/>
                     </CONVERTFUNCTION>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OUTPUT name="futurcost_converted.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <INPUT    name="futurcost_converted.v"/>
         <PROPERTY name="outputfile" value="${O_FUTURCOST_FILE}"/>
         <PROPERTY name="schemafile" value="${O_FUTURCOST_SCHEMA}"/>
      </OPERATOR>
</FLOW>
EOF


###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?


###############################################################################
#  Log the number of records written to the final output files
#  and add up the total:
###############################################################################

log_num_recs ${O_COSTBU_FILE}
log_num_recs ${O_COSTLOC_FILE}
log_num_recs ${O_FUTURCOST_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check $ERR_FILE"

###############################################################################
# Remove the status file
###############################################################################

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
# cleanup and exit
###############################################################################

check_error_and_clean_before_exit 0 "NO_CHECK"
