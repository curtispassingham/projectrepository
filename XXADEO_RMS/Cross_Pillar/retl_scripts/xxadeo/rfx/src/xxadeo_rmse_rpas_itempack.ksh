#!/bin/ksh

##########################################################################
#
#  The xxadeo_rmse_rpas_itempack.ksh program extracts the item packs information from
#  RMS tables and places this data into a flat file (xxadeo_rmse_rpas_itempack_yyyymmdd-hhmmss.dat)
#  to be be loaded into RPAS.
#  Version:             1.2
#
#  History: 1.0 - Initial Release
#           1.1 - Adjustments to the new file structure RETLforXXADEO,
#                 DATA_DIR and XXADEO_RMS
#           1.2 - Add filter item status = 'A' 
#
###############################################################################

################## PROGRAM DEFINE #####################
#########     (must be the first define)     ##########

export PROGRAM_NAME="xxadeo_rmse_rpas_itempack"
export EXE_DATE=`date +"%G%m%d-%H%M%S"`;
export LAST_EXTRACT_FILE=${RETLforXXADEO}/rfx/etc/xxadeo_rmse_rpas_itempack_lastextract.txt

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export DATA_DIR="${RETL_OUT}/RPAS"

export OUTPUT_FILE=${DATA_DIR}/XXRMS221_ITEMPACK_${EXE_DATE}.dat

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema


######################################################################
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
######################################################################
function USAGE
{
   message "USAGE: ./$PROGRAM_NAME.ksh  <mode>
               <mode> mode of extraction. full or delta"
}

function PROCESS_FULL
{

message "Program started ..."
RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read the item pack data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            SELECT pb.pack_no,
                   pb.item,
                   trunc(pb.pack_item_qty,0) AS PACK_ITEM_QTY
              FROM ${XXADEO_RMS}.item_master im,
                   ${XXADEO_RMS}.packitem_breakout pb
             WHERE im.item = pb.item
               AND im.status = 'A'
         ]]>
      </PROPERTY>
      <OUTPUT   name = "itempack_data.v"/>
   </OPERATOR>

	<OPERATOR type="convert">
      <INPUT    name="itempack_data.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
			<CONVERTSPECS>	
				<CONVERT destfield="PACK_ITEM_QTY" sourcefield="PACK_ITEM_QTY" newtype="int32">
					<CONVERTFUNCTION name="int32_from_dfloat"/>
				</CONVERT>
			</CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="itempack_data_1.v"/>
   </OPERATOR>   
   
   <!--  Write out the item pack data to a flat file:  -->
   <OPERATOR type="export">
      <INPUT    name="itempack_data_1.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

###############################################
#  Execute the RETL flow 
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
exit_stat=$?

checkerror -e $exit_stat -m "Program failed - check ${ERR_FILE}"

}

function PROCESS_DELTA
{

message "Program started ..."
RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml


########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read in the item pack data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
             SELECT pb.pack_no,
                    pb.item,
                    trunc(pb.pack_item_qty,0) AS PACK_ITEM_QTY
               FROM ${XXADEO_RMS}.item_master im,
                    ${XXADEO_RMS}.packitem_breakout pb
              WHERE im.item = pb.item
                AND im.status = 'A'
                AND pb.last_update_datetime > TO_DATE('$last_extractDate','YYYYMMDDHHMISS')
         ]]>
      </PROPERTY>
      <OUTPUT   name = "itempack_data.v"/>
   </OPERATOR>
   
   	<OPERATOR type="convert">
      <INPUT    name="itempack_data.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
			<CONVERTSPECS>	
				<CONVERT destfield="PACK_ITEM_QTY" sourcefield="PACK_ITEM_QTY" newtype="int32">
					<CONVERTFUNCTION name="int32_from_dfloat"/>
				</CONVERT>
			</CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="itempack_data_1.v"/>
   </OPERATOR>  

   <!--  Write out the item pack data to a flat file:  -->

   <OPERATOR type="export">
      <INPUT    name="itempack_data_1.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

###############################################
#  Execute the RETL flow 
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
exit_stat=$?

checkerror -e $exit_stat -m "Program failed - check ${ERR_FILE}"

}

######################################################################
#     MAIN
######################################################################

#----------------------------------------------
#  Log start message and define the RETL flow file
#----------------------------------------------

if [[ $# -eq 0 || $# -gt 1  ]]; then
   USAGE
   exit 1
fi


if [[ $1 = "full" ]]; then
        if PROCESS_FULL ; then
                        message "Program completed successfully in FULL mode."
                        extractDate=`date +"%G%m%d%I%M%S"`    # get the extraction date
                        echo $extractDate > ${LAST_EXTRACT_FILE}
                else
                        message "Program completed with erros, please refer to the Log/Error files."
        fi
elif [[ $1 = "delta" ]]; then
    if [[ -e ${LAST_EXTRACT_FILE} ]];  then
                last_extractDate=`cat ${LAST_EXTRACT_FILE}`
                if PROCESS_DELTA ; then
                        message "Program completed successfully in DELTA mode."
                        extractDate=`date +"%G%m%d%I%M%S"`    # get the extraction date
                        echo $extractDate > ${LAST_EXTRACT_FILE}
                else
                        message "Program completed with erros, please refer to the Log/Error files."
                fi
        else
                message "ERROR: no previous execution date encoutered, please run the full extraction mode if this is the first time the script is being executed"
        exit 1
    fi

else
  USAGE
  exit 1
fi


###############################################################################
#  cleanup and exit:
###############################################################################

#  Log the number of records written to the final output files and add up the total:
log_num_recs ${OUTPUT_FILE}


if [ -f $STATUS_FILE ]; then rm $STATUS_FILE; fi

message "Program completed successfully"

#  Cleanup and exit:
rmse_terminate 0

