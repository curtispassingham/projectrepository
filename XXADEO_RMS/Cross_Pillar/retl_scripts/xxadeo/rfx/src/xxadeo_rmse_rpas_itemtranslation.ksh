#!/bin/ksh

##########################################################################
#
#  The xxadeo_rmse_rpas_itemtranslation.ksh program extracts item translation data
#  from the ITEM_MASTER_TL and LANG RMS tables and
#  places this data into a flat file
#  Version:             1.3
#
#  History: 1.0 - Initial Release
#                       1.1 - Adjustments to the new file structure RETLforXXADEO,
#                                 DATA_DIR and XXADEO_RMS
#           1.2 - Inclusion of Secondary Description to accommodate CatMan needs
#           1." - Add filter item status = 'A'
#
###############################################################################


################## PROGRAM DEFINE #####################
#########     (must be the first define)     ##########

export PROGRAM_NAME="xxadeo_rmse_rpas_itemtranslation"
export EXE_DATE=`date +"%G%m%d-%H%M%S"`;
export LAST_EXTRACT_FILE=${RETLforXXADEO}/rfx/etc/xxadeo_rmse_rpas_itemtranslation_lastextract.txt

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export DATA_DIR="${RETL_OUT}/RPAS"

export OUTPUT_FILE=${DATA_DIR}/XXRMS219_ITEMMASTERTL_${EXE_DATE}.dat

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema


###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${OUTPUT_FILE}.retl"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
                rm -f "${OUTPUT_FILE}.retl"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
                rmse_terminate $1
        fi
}

######################################################################
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
######################################################################
function USAGE
{
   message "USAGE: ./$PROGRAM_NAME.ksh  <mode>
               <mode> mode of extraction. full or delta"
}


function PROCESS_FULL
{
####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml


########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read in the item translation data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            select imtl.item                          as item,
                   l.iso_code                         as lang_iso,
                   l.description                      as lang_name,
                   imtl.item || ' ' || imtl.item_desc as item_desc,
                   imtl.item_desc_secondary           as item_desc_secondary
              from ${XXADEO_RMS}.item_master    im,
                   ${XXADEO_RMS}.item_master_tl imtl,
                   ${XXADEO_RMS}.lang           l
             where im.item = imtl.item
               and imtl.lang = l.lang
               and im.status = 'A'
         ]]>
      </PROPERTY>
      <OUTPUT   name = "item_translation.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
        <INPUT name="item_translation.v"/>
        <PROPERTY name="convertspec">
        <![CDATA[
            <CONVERTSPECS>
                <CONVERT destfield="ITEM_DESC" sourcefield="ITEM_DESC">
                    <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                    </CONVERTFUNCTION>
                </CONVERT>
            </CONVERTSPECS>
        ]]>
        </PROPERTY>
        <OUTPUT name="item_translation1.v"/>
    </OPERATOR>

   <OPERATOR type="export">
      <INPUT    name="item_translation1.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}.retl"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

###############################################
#  Execute the RETL flow
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
check_error_and_clean_before_exit $?

}

function PROCESS_DELTA
{
####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read in the item translation data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
              select imtl.item                          as item,
                     l.iso_code                         as lang_iso,
                     l.description                      as lang_name,
                     imtl.item || ' ' || imtl.item_desc as item_desc,
                     imtl.item_desc_secondary           as item_desc_secondary
                from ${XXADEO_RMS}.item_master    im,
                     ${XXADEO_RMS}.item_master_tl imtl,
                     ${XXADEO_RMS}.lang           l
               where im.item = imtl.item
                 and imtl.lang = l.lang
                 and im.status = 'A'
                 and imtl.last_update_datetime > to_date('$last_extractDate','YYYYMMDDHHMISS')
         ]]>
      </PROPERTY>
      <OUTPUT   name = "item_translation.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
        <INPUT name="item_translation.v"/>
        <PROPERTY name="convertspec">
        <![CDATA[
            <CONVERTSPECS>
                <CONVERT destfield="ITEM_DESC" sourcefield="ITEM_DESC">
                    <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                    </CONVERTFUNCTION>
                </CONVERT>
            </CONVERTSPECS>
        ]]>
        </PROPERTY>
        <OUTPUT name="item_translation1.v"/>
    </OPERATOR>

   <OPERATOR type="export">
      <INPUT    name="item_translation1.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}.retl"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

###############################################
#  Execute the RETL flow
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
check_error_and_clean_before_exit $?

}


######################################################################
#     MAIN
######################################################################

#----------------------------------------------
#  Log start message and define the RETL flow file
#----------------------------------------------



if [[ $# -eq 0 || $# -gt 1  ]]; then
   USAGE
   exit 1
fi


if [[ $1 = "full" ]]; then
        if PROCESS_FULL ; then
                        message "Program completed successfully in FULL mode."
                        extractDate=`date +"%G%m%d%I%M%S"`    # get the extraction date
                        echo $extractDate > ${LAST_EXTRACT_FILE}
                else
                        message "Program completed with erros, please refer to the Log/Error files."
        fi
elif [[ $1 = "delta" ]]; then
    if [[ -e ${LAST_EXTRACT_FILE} ]];  then
                last_extractDate=`cat ${LAST_EXTRACT_FILE}`
                if PROCESS_DELTA ; then
                        message "Program completed successfully in DELTA mode."
                        extractDate=`date +"%G%m%d%I%M%S"`    # get the extraction date
                        echo $extractDate > ${LAST_EXTRACT_FILE}
                else
                        message "Program completed with erros, please refer to the Log/Error files."
                fi
        else
                message "ERROR: no previous execution date encoutered, please run the full extraction mode if this is the first time the script is being executed"
        exit 1
    fi

else
  USAGE
  exit 1
fi


###############################################################################
#  cleanup and exit:
###############################################################################

#  Convert output data files to CSV format
csv_converter "retlsv-csv" "${OUTPUT_FILE}.retl" "${OUTPUT_FILE}"
check_error_and_clean_before_exit $?

#  Log the number of records written to the final output files and add up the total:
log_num_recs ${OUTPUT_FILE}


if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

check_error_and_clean_before_exit 0 "NO_CHECK"


