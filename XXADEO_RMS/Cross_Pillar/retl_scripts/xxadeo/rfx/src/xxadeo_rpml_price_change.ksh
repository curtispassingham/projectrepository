#!/bin/ksh

###############################################################################
#                                                                       
# Object Name:   xxadeo_rpml_price_change.ksh   
#                
# Description:   This interface consists in the integration of the Regular Price data
#                from RPO and SIMTAR to RPM.                
#
#                During load, reject records due to bad formating are stored into files: 
#                 XXRPM203_RPOPriceChanges_YYYYMMDD-HHMMSS.csv.rej
#                 XXRPM203_SIMTARPriceChanges_YYYYMMDD-HHMMSS.csv.rej
#                                                                       
# Version:       1.1                                                    
# Author:        CGI                                                    
# Creation Date: 24/06/2018                                             
# Last Modified: 07/02/2019                                             
# History:                                                              
#               1.0 - Initial version                                   
#               1.1 - BugFix VANSOLP2-45 - Update log messages                                    
#                                                                       
###############################################################################



###############################################################################
#  PROGRAM NAME DEFINE
###############################################################################
export PROGRAM_NAME='xxadeo_rpml_price_change'


###############################################################################
#  INCLUDES
###############################################################################
. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh



###############################################################################
#  INPUT/OUTPUT DEFINES
###############################################################################
#Input Files
export I_DATA_DIR=$RETL_IN/MOM
export I_RPO_PRICE_CHANGE_PATTERN="XXRPM203_RPOPriceChanges_*.csv"
export I_SIMTAR_PRICE_CHANGE_PATTERN="XXRPM203_SIMTARPriceChanges_*.csv"


#Output/Temp Files
export O_PROCESS_ID_TMP=${RETLforXXADEO}/rfx/etc/xxadeo_rpml_price_change.txt

#Schema Files
export I_PRICE_CHANGE_SCHEMA=${RETLforXXADEO}/rfx/schema/xxadeo_rpml_price_change.schema
export O_PRICE_ID_SCHEMA=${RETLforXXADEO}/rfx/schema/xxadeo_rpml_price_change_id.schema

#Control variables
export PROCESS_RPO="Y"
export PROCESS_SIMTAR="Y"



###############################################################################
#  Start program
###############################################################################
message "Program started ..."
RETL_FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"



###############################################################################
#  Check input files / Get the last file in the directory and create rej file
###############################################################################
#RPO
I_RPO_PRICECHANGE_FILE=`find $I_DATA_DIR -name ${I_RPO_PRICE_CHANGE_PATTERN} | sort | tail -n 1`
O_RPO_REJ_FILE=${I_RPO_PRICECHANGE_FILE}.rej

#SIMTAR
I_SIMTAR_PRICECHANGE_FILE=`find $I_DATA_DIR -name ${I_SIMTAR_PRICE_CHANGE_PATTERN} | sort | tail -n 1`
O_SIMTAR_REJ_FILE=${I_SIMTAR_PRICECHANGE_FILE}.rej


#Check if input files exists
if [[ ! -f $I_RPO_PRICECHANGE_FILE ]] && [[ ! -f $I_SIMTAR_PRICECHANGE_FILE ]];  then
    message "ERROR: XXRPM203_RPOPriceChanges_.csv and XXRPM203_SIMTARPriceChanges_.csv do not exists."
    exit 1
elif  [[ ! -f $I_RPO_PRICECHANGE_FILE ]];  then
    message "Warning: XXRPM203_RPOPriceChanges_.csv does not exist."
    PROCESS_RPO="N"
elif [[ ! -f $I_SIMTAR_PRICECHANGE_FILE ]];  then
    message "Warning: XXRPM203_SIMTARPriceChanges_.csv does not exist."
    PROCESS_SIMTAR="N"
fi

#Check if input files are not empty
if [ -f ${I_RPO_PRICECHANGE_FILE} ] && [ ! -s ${I_RPO_PRICECHANGE_FILE} ] && [ -f ${I_SIMTAR_PRICECHANGE_FILE} ] && [ ! -s ${I_SIMTAR_PRICECHANGE_FILE} ]; then
       message "Warning: Both Input files are empty."
       PROCESS_RPO="N"
       PROCESS_SIMTAR="N"
elif [ -f ${I_RPO_PRICECHANGE_FILE} ] && [ ! -s ${I_RPO_PRICECHANGE_FILE} ]; then
       message "Warning: Input file ${I_RPO_PRICECHANGE_FILE} is empty."
       PROCESS_RPO="N"
elif [ -f ${I_SIMTAR_PRICECHANGE_FILE} ] && [ ! -s ${I_SIMTAR_PRICECHANGE_FILE} ]; then
       message "Warning: Input file ${I_SIMTAR_PRICECHANGE_FILE} is empty."
       PROCESS_SIMTAR="N"
fi



###############################################################################
#  PRE PROGRAM CONTENT (pre flow) and get next sequence
###############################################################################
#Check if RPO and SIMTAR files should be processed
if [ $PROCESS_RPO = "Y" ] || [ $PROCESS_SIMTAR = "Y" ]; then

cat > ${RETL_FLOW_FILE} << EOF
        <FLOW name = "${PROGRAM_NAME}.flw">
        <!-- Get the next sequence val from XXADEO_RPM_STAGE_PROC_ID_SEQ -->
            ${DBREAD}
                   <PROPERTY name = "query">
                       <![CDATA[
                               select to_char(sysdate,'YYYYMMDDHH24MISS') AS PROCESS_ID from dual
                       ]]>
                   </PROPERTY>
                   <OUTPUT   name = "xxadeo_process_id.v"/>
            </OPERATOR>

            <OPERATOR type="convert">
                <INPUT    name="xxadeo_process_id.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                    <CONVERTSPECS>
                        <CONVERT destfield="PROCESS_ID" sourcefield="PROCESS_ID">
                        <CONVERTFUNCTION name="make_not_nullable">
                            <FUNCTIONARG name="nullvalue" value="-1"/>
                        </CONVERTFUNCTION>
                        </CONVERT>
                    </CONVERTSPECS>
                ]]>
                </PROPERTY>
                <OUTPUT name="xxadeo_process_id2.v"/>
            </OPERATOR>

            <OPERATOR type="export">
                    <INPUT name="xxadeo_process_id2.v"/>
                    <PROPERTY name="schemafile" value="${O_PRICE_ID_SCHEMA}"/>
                    <PROPERTY name="outputfile" value="${O_PROCESS_ID_TMP}"/>
            </OPERATOR>
        </FLOW>
EOF

    ###############################################################################
    #  Execute the flow
    ###############################################################################
    ${RFX_EXE} ${RFX_OPTIONS} -f ${RETL_FLOW_FILE}
    exit_stat=$?

    checkerror -e $exit_stat -m "Program failed - check $ERR_FILE"

    ###############################################################################
    #  Check Process_ID file / Create Reject file
    ###############################################################################
    #Process_ID file
    if [ ! -r  ${O_PROCESS_ID_TMP} ]; then
            message "ERROR: Config file ${O_PROCESS_ID_TMP} missing!"
            exit 1
    fi

    I_PRICECHANGE_ID=$(cat ${O_PROCESS_ID_TMP})

    # Evaluate if the config file has any value
    if [ -z ${I_PRICECHANGE_ID} ]; then
            message "ERROR: Interface config file not correctly set. Please check config file ${O_PROCESS_ID_TMP}"
            exit 1
    fi

fi #END OF PROCESS ID FLOW



###############################################################################
#  MAIN PROGRAM CONTENT (main flow) RPO
###############################################################################
#Check if RPO file should be processed
if [ $PROCESS_RPO = "Y" ]; then

    #RPO Reject file
    if [ ! -r  ${O_RPO_REJ_FILE} ]; then
        touch ${O_RPO_REJ_FILE}
    fi

cat > ${RETL_FLOW_FILE} << EOF
        <FLOW name = "${PROGRAM_NAME}.flw">

    <!-- Insert data from file -->
            <OPERATOR type="import">
                    <PROPERTY  name="inputfile"  value="${I_RPO_PRICECHANGE_FILE}"/>
                    <PROPERTY  name="schemafile" value="${I_PRICE_CHANGE_SCHEMA}"/>
                    <PROPERTY  name="rejectfile" value="${O_RPO_REJ_FILE}"/>
                    <OUTPUT name="import_rpo_price_changes.v"/>
            </OPERATOR>

            <OPERATOR type="filter">
                    <INPUT name="import_rpo_price_changes.v"/>
                    <PROPERTY name="filter"  value="(((STATUS EQ 'C') OR STATUS EQ 'U') OR STATUS EQ 'D')"/>
                    <PROPERTY name="rejects" value="true"/>
                    <OUTPUT name="import_rpo_price_changes_valid.v"/>
                    <OUTPUT name="rejected_rpo_price_changes.v"/>
            </OPERATOR>

            <OPERATOR type="export">
                    <INPUT name="rejected_rpo_price_changes.v"/>
                    <PROPERTY name="schemafile" value="${I_PRICE_CHANGE_SCHEMA}"/>
                    <PROPERTY name="outputfile" value="${O_RPO_REJ_FILE}"/>
                    <PROPERTY name="outputmode" value="append"/>
            </OPERATOR>

            <OPERATOR type="parser">
                    <INPUT name="import_rpo_price_changes_valid.v"/>
                            <PROPERTY name="expression">
                            <![CDATA[
                                    {
                                            if (RECORD.STATUS == "C")
                                            {
                                                    RECORD.STATUS = "N";
                                            }
                                    }
                                    ]]>
                            </PROPERTY>
                    <OUTPUT name="conv_rpo_price_changes.v"/>
            </OPERATOR>

    <!-- load input file -->
            ${DBPREPSTMT}
                    <INPUT name="conv_rpo_price_changes.v"/>
                    <PROPERTY name="tablename" value="${XXADEO_RMS}.XXADEO_RPM_STAGE_PRICE_CHANGE"/>
                    <PROPERTY name="schemaowner" value="${XXADEO_RMS}" />
                    <PROPERTY name="statement">
                                    <![CDATA[
                                            INSERT INTO ${XXADEO_RMS}.XXADEO_RPM_STAGE_PRICE_CHANGE (
                                                              XXADEO_PROCESS_ID
                                                            , STAGE_PRICE_CHANGE_ID
                                                            , REASON_CODE
                                                            , ITEM
                                                            , ZONE_ID
                                                            , ZONE_NODE_TYPE
                                                            , EFFECTIVE_DATE
                                                            , CHANGE_TYPE
                                                            , CHANGE_AMOUNT
                                                            , NULL_MULTI_IND
                                                            , IGNORE_CONSTRAINTS
                                                            , AUTO_APPROVE_IND
                                                            , STATUS
                                                            , VENDOR_FUNDED_IND
                                                            , CREATE_ID
                                                            , CREATE_DATETIME
                                                            ) VALUES (
                                                                      ${I_PRICECHANGE_ID}                            --XXADEO_PROCESS_ID
                                                                    , ${XXADEO_RMS}.RPM_PRICE_CHANGE_SEQ.NEXTVAL   --STAGE_PRICE_CHANGE_ID
                                                                    , ?     --REASON_CODE
                                                                    , ?     --ITEM
                                                                    , ?     --ZONE_ID
                                                                    , 1     --ZONE_NODE_TYPE
                                                                    , ?     --EFFECTIVE_DATE
                                                                    , 2     --CHANGE_TYPE
                                                                    , ?     --CHANGE_AMOUNT
                                                                    , 0     --NULL_MULTI_IND
                                                                    , 0     --IGNORE_CONSTRAINTS
                                                                    , 0     --AUTO_APPROVE_IND
                                                                    , ?     --STATUS
                                                                    , 0     --VENDOR_FUNDED_IND
                                                                    , ?     --CREATE_ID
                                                                    , ?     --CREATE_DATETIME
                                                            )
                                    ]]>
                    </PROPERTY>
                    <PROPERTY name="fields" value="REASON_CODE ITEM ZONE_ID EFFECTIVE_DATE CHANGE_AMOUNT STATUS CREATE_ID CREATE_DATETIME"/>
            </OPERATOR>

        </FLOW>
EOF

    ###############################################################################
    #  Execute the flow
    ###############################################################################
    ${RFX_EXE} ${RFX_OPTIONS} -f ${RETL_FLOW_FILE}
    exit_stat=$?

    # Evaluate if any records have been rejected
    if [ -s ${O_RPO_REJ_FILE} ]; then
            message "Warning: Records have been rejected, please check ${O_RPO_REJ_FILE}"
    else
            rm ${O_RPO_REJ_FILE}
    fi

    ###############################################################################
    #Do error checking on results of the RETL execution: and remove status file
    ###############################################################################
    checkerror -e $exit_stat -m "Program failed - check $ERR_FILE"

    if [ -f $STATUS_FILE ]; then rm $STATUS_FILE; fi
    message "RPO price changes loaded successfully"

else
    message "RPO price changes not loaded"
fi #END OF RPO FLOW



################################################################################
#  MAIN PROGRAM CONTENT (main flow) SIMTAR
################################################################################
#Check if SIMTAR file should be processed
if [ $PROCESS_SIMTAR = "Y" ]; then

    #RPO Reject file
    if [ ! -r  ${O_SIMTAR_REJ_FILE} ]; then
        touch ${O_SIMTAR_REJ_FILE}
    fi

cat > ${RETL_FLOW_FILE} << EOF
    <FLOW name = "${PROGRAM_NAME}.flw">

    <!-- Insert data from file -->
            <OPERATOR type="import">
                    <PROPERTY  name="inputfile"  value="${I_SIMTAR_PRICECHANGE_FILE}"/>
                    <PROPERTY  name="schemafile" value="${I_PRICE_CHANGE_SCHEMA}"/>
                    <PROPERTY  name="rejectfile" value="${O_SIMTAR_REJ_FILE}"/>
                    <OUTPUT name="import_simtar_price_changes.v"/>
            </OPERATOR>

            <OPERATOR type="filter">
                    <INPUT name="import_simtar_price_changes.v"/>
                    <PROPERTY name="filter"  value="(((STATUS EQ 'C') OR STATUS EQ 'U') OR STATUS EQ 'D')"/>
                    <PROPERTY name="rejects" value="true"/>
                    <OUTPUT name="import_simtar_price_changes_valid.v"/>
                    <OUTPUT name="rejected_simtar_price_changes.v"/>
            </OPERATOR>

            <OPERATOR type="export">
                    <INPUT name="rejected_simtar_price_changes.v"/>
                    <PROPERTY name="schemafile" value="${I_PRICE_CHANGE_SCHEMA}"/>
                    <PROPERTY name="outputfile" value="${O_SIMTAR_REJ_FILE}"/>
                    <PROPERTY name="outputmode" value="append"/>
            </OPERATOR>

            <OPERATOR type="parser">
                    <INPUT name="import_simtar_price_changes_valid.v"/>
                            <PROPERTY name="expression">
                            <![CDATA[
                                    {
                                            if (RECORD.STATUS == "C")
                                            {
                                                    RECORD.STATUS = "N";
                                            }
                                    }
                                    ]]>
                            </PROPERTY>
                    <OUTPUT name="conv_simtar_price_changes.v"/>
            </OPERATOR>

    <!-- load input file -->
            ${DBPREPSTMT}
                    <INPUT name="conv_simtar_price_changes.v"/>
                    <PROPERTY name="tablename" value="${XXADEO_RMS}.XXADEO_RPM_STAGE_PRICE_CHANGE"/>
                    <PROPERTY name="schemaowner" value="${XXADEO_RMS}" />
                    <PROPERTY name="statement">
                                    <![CDATA[
                                            INSERT INTO ${XXADEO_RMS}.XXADEO_RPM_STAGE_PRICE_CHANGE (
                                                              XXADEO_PROCESS_ID
                                                            , STAGE_PRICE_CHANGE_ID
                                                            , REASON_CODE
                                                            , ITEM
                                                            , ZONE_ID
                                                            , ZONE_NODE_TYPE
                                                            , EFFECTIVE_DATE
                                                            , CHANGE_TYPE
                                                            , CHANGE_AMOUNT
                                                            , NULL_MULTI_IND
                                                            , IGNORE_CONSTRAINTS
                                                            , AUTO_APPROVE_IND
                                                            , STATUS
                                                            , VENDOR_FUNDED_IND
                                                            , CREATE_ID
                                                            , CREATE_DATETIME
                                                            ) VALUES (
                                                                      ${I_PRICECHANGE_ID}                          --XXADEO_PROCESS_ID
                                                                    , ${XXADEO_RMS}.RPM_PRICE_CHANGE_SEQ.NEXTVAL   --STAGE_PRICE_CHANGE_ID
                                                                    , ?        --REASON_CODE
                                                                    , ?        --ITEM
                                                                    , (SELECT ZONE_ID FROM ${XXADEO_RMS}.XXADEO_RPM_BU_PRICE_ZONES WHERE BU_ZONE_TYPE = 'NATZ' AND BU_ID = ?) --ZONE_ID
                                                                    , 1        --ZONE_NODE_TYPE
                                                                    , ?        --EFFECTIVE_DATE
                                                                    , 2        --CHANGE_TYPE
                                                                    , ?        --CHANGE_AMOUNT
                                                                    , 0        --NULL_MULTI_IND
                                                                    , 0        --IGNORE_CONSTRAINTS
                                                                    , 0        --AUTO_APPROVE_IND
                                                                    , ?        --STATUS
                                                                    , 0        --VENDOR_FUNDED_IND
                                                                    , ?        --CREATE_ID
                                                                    , ?        --CREATE_DATETIME
                                                            )
                                    ]]>
                    </PROPERTY>
                    <PROPERTY name="fields" value="REASON_CODE ITEM ZONE_ID EFFECTIVE_DATE CHANGE_AMOUNT STATUS CREATE_ID CREATE_DATETIME"/>
            </OPERATOR>

    </FLOW>
EOF

    ###############################################################################
    #  Execute the flow
    ###############################################################################
    ${RFX_EXE} ${RFX_OPTIONS} -f ${RETL_FLOW_FILE}
    exit_stat=$?

    # Evaluate if any records have been rejected
    if [ -s ${O_SIMTAR_REJ_FILE} ]; then
            message "Warning: Records have been rejected, please check ${O_SIMTAR_REJ_FILE}"
    else
            rm ${O_SIMTAR_REJ_FILE}
    fi

    ###############################################################################
    #Do error checking on results of the RETL execution: and remove status file
    ###############################################################################
    checkerror -e $exit_stat -m "Program failed - check $ERR_FILE"

    if [ -f $STATUS_FILE ]; then rm $STATUS_FILE; fi
    message "SIMTAR price changes loaded successfully"

else
    message "SIMTAR price changes not loaded"
fi #END OF SIMTAR FLOW



###############################################################################
#Cleanup and exit:
###############################################################################
if [ -f $STATUS_FILE ]; then rm $STATUS_FILE; fi

message "Program completed successfully"
rmse_terminate 0

