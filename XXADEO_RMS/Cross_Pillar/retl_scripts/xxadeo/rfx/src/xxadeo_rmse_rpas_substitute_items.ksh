#!/bin/ksh

###################################################################
#
#  This script exports Substitute Items information
#  from RMS to RPO and other RPAS modules.
#
#  _________________________________________________________
# | Workfile:      | xxadeo_rmse_rpas_substitute_items.ksh  |
# | Created by:    | PR                                     |
# | Creation Date: | 20180327-1645                          |
# | Modify Date:   | 20180706-1727                          |
# | Version:       | 1.1                                    |
#  ---------------------------------------------------------
#
#  History: 1.0 - Initial Release
#           1.1 - Add filter item status = 'A'
#
###################################################################


################## PROGRAM DEFINITIONS ####################

export PROGRAM_NAME="xxadeo_rmse_rpas_substitute_items"

##########                                       ##########
######################## INCLUDES #########################

# Environment configuration script
. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env

# Script Library Collection
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

##########                                      ##########
###################  OUTPUT DEFINES ######################
export SCHEMA_DIR=${RETLforXXADEO}/rfx/schema

export extractDate=`date +"%G%m%d-%H%M%S"`
export DATA_DIR=${RETL_OUT}/RPAS
export FILE_FORMAT=${extractDate}.dat

export OUTPUT_FILE=${DATA_DIR}/XXRMS217_SUBSTITUTEITEMS_${FILE_FORMAT}
export OUTPUT_SCHEMA_FILE=$SCHEMA_DIR/${PROGRAM_NAME}.schema

##########                                      ##########
##################  CREATE RETL FLOWS ####################

message "Program start..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

    ${DBREAD}
    <PROPERTY name = "query">
    <![CDATA[
          select sub_ih.item,                    
                 sub_id.location,                
                 sub_id.loc_type,                
                 sub_ih.fill_priority,           
                 sub_ih.use_sales_ind,           
                 sub_ih.use_stock_ind,           
                 sub_ih.use_forecast_sales_ind,  
                 sub_id.sub_item,                
                 sub_id.primary_repl_pack,       
                 sub_id.pick_priority,           
                 to_char(sub_id.start_date, 'yyyymmddhhmiss') as start_date,
                 to_char(sub_id.end_date, 'yyyymmddhhmiss')   as end_date,
                 sub_id.substitute_reason       
            
            from ${XXADEO_RMS}.item_master      im,
                 ${XXADEO_RMS}.sub_items_head   sub_ih,
                 ${XXADEO_RMS}.sub_items_detail sub_id
           
           where (1=1)
             and im.item = sub_ih.item
             and sub_ih.item = sub_id.item
             and sub_ih.location = sub_id.location
             and sub_ih.loc_type = sub_id.loc_type
             and im.status = 'A'
    ]]>
        </PROPERTY>
    <OUTPUT name = "rms_qry_sub_items.v"/>
    </OPERATOR>

    <OPERATOR type="export">
    <INPUT    name="rms_qry_sub_items.v"/>
    <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
    <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
    </OPERATOR>
</FLOW>
EOF

##########                                      ##########
#################  EXECUTE RETL FLOWS ####################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

log_num_recs ${OUTPUT_FILE}

retl_stat=$?

##########                                      ##########
#################  REMOVE STATUS FILE ####################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

##########                                      ##########
###################  REPORT AND EXIT #####################

case $retl_stat in
  0 )
     msg="Program completed successfully with" ;;
  * )
     msg="Program exited with error" ;;
esac

message "$msg code: $retl_stat"
rmse_terminate $retl_stat
