#! /bin/ksh  

#-------------------------------------------------------------------------

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib


pgmName='xxadeo_itemloc_in.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=$(date +"%h_%d")             # get the execution date
DIR=`pwd`

# File locations

CTLFILE="${MMHOME}/oracle/proc/bin/xxadeo_itemloc_in.ctl"
LOGDIR="${MMHOME}/log"
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.XXRMS279_ITEMLOC_"$exeDate

# Initialize variables and constants
inputfile_ext=0
REJECT="R"

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "
   USAGE: $pgmName <connect string> <number threads>
		<connect string>   Username/password@db;
		<thread indicator>     Can be 'Y' or 'N';
		<number threads>   Number of threads to process
		
"
}

#-------------------------------------------------------------------------
# Function Name: PROCESS
# Purpose      : 
#-------------------------------------------------------------------------
function PROCESS
{
 LOG_MESSAGE "Process records for department ${1} " "PROCESS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
 sqlTxt="
 DECLARE
    O_status_code    VARCHAR2(1);
    O_error_message  VARCHAR2(400);
 BEGIN

 	XXADEO_ITEMLOC_IN.PROCESS(${1}, O_status_code, O_error_message);

	if O_status_code != 'S' then
		rollback;
        :GV_script_error := O_error_message;
        :GV_return_code := ${FATAL};

    else
		COMMIT;
    end if;	

	EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := O_error_message;
            :GV_return_code := ${FATAL};
      END;"


	EXEC_SQL ${sqlTxt}

	if [[ $? -ne ${OK} ]]; then
		LOG_MESSAGE "Fatal error processing records for dept ${1}" "PROCESS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
		exit ${FATAL}
	else
		LOG_MESSAGE "Successfully process records for dept ${1}" "PROCESS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}	
	fi

}


#-------------------------------------------------------------------------
# Function Name: ENRICH_VALIDATE
# Purpose      : 
#-------------------------------------------------------------------------
function ENRICH_VALIDATE
{
 LOG_MESSAGE "Enrich records " "ENRICH_VALIDATE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
 sqlTxt="
 DECLARE
    O_status_code    VARCHAR2(1);
    O_error_message  VARCHAR2(400);
 BEGIN

 	XXADEO_ITEMLOC_IN.ENRICH_VALIDATE(O_status_code, O_error_message);

	if O_status_code != 'S' then
		rollback;
        :GV_script_error := O_error_message;
        :GV_return_code := ${FATAL};

    else
		COMMIT;
    end if;	

	EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := O_error_message;
            :GV_return_code := ${FATAL};
      END;"


	EXEC_SQL ${sqlTxt}

	if [[ $? -ne ${OK} ]]; then
		LOG_MESSAGE "Fatal error enriching records " "ENRICH_VALIDATE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
		exit ${FATAL}
	else
		LOG_MESSAGE "Successfully enrich " "ENRICH_VALIDATE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}	
	fi

}


#-------------------------------------------------------------------------
# Function Name: PURGE
# Purpose      : 
#-------------------------------------------------------------------------
function PURGE
{
 LOG_MESSAGE "Purge records" "PURGE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
 sqlTxt="
 DECLARE
    O_status_code    VARCHAR2(1);
    O_error_message  VARCHAR2(400);
 BEGIN

 	XXADEO_ITEMLOC_IN.PURGE(O_status_code, O_error_message);

	if O_status_code != 'S' then
		rollback;
        :GV_script_error := O_error_message;
        :GV_return_code := ${FATAL};

    else
		COMMIT;
    end if;	

	EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := O_error_message;
            :GV_return_code := ${FATAL};
      END;"


	EXEC_SQL ${sqlTxt}

	if [[ $? -ne ${OK} ]]; then
		LOG_MESSAGE "Fatal error purging records " "PURGE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
		exit ${FATAL}
	else
		LOG_MESSAGE "Successfully purge records " "PURGE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}	
	fi

}




#-------------------------------------------------------------------------
# Function Name: LOAD_FILE
# Purpose      : SQL Load the input file into the staging table enrich and validate
#-------------------------------------------------------------------------
function LOAD_FILE
{

# $MMIN/MOM
if ls ${MMHOME}/oracle/proc/bin/XXRMS279_ITEM_LOC_*.dat  1> /dev/null 2>&1; then

	 for file in "${MMHOME}/oracle/proc/bin/XXRMS279_ITEM_LOC_*.dat";
	 do 		
	   LOG_MESSAGE "Loading Item Loc data into the tables ..." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

	   dtStamp=`date +"%G%m%d%H%M%S"`
	   sqlldrFile='XXRMS279_ITEMLOC_'  
	   sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
	   sqlldrDsc=${MMHOME}/log/$sqlldrFile.dsc   
	   sqlldrBad=${MMHOME}/log/$sqlldrFile.bad
	   sqlldr ${connectStr} silent=feedback,header \
		  control=${CTLFILE} \
		  log=${sqlldrLog} \
		  data=${file} \
		  bad=${sqlldrBad} \
		  discard=${sqlldrDsc} 
	   sqlldr_status=$?	

	   # Check execution status
	   if [[ $sqlldr_status != ${OK} ]]; then

		  # Check if execution status is a warning
		  if [[ $sqlldr_status = 2 ]]; then
			 LOG_MESSAGE "${inputFileNoPath} - Some rows not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

		  else
			 LOG_ERROR "Error while loading file ${file}. See ${sqlldrLog} for details." "LOAD_FILE" ${FATAL} ${ERRORFILE} ${pgmName} ${pgmPID}
			 exit ${FATAL}
		  fi
	   else
		  # Check log file for sql loader errors
		  if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` != ""
			 && `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` != "" ]];
		  then
			 LOG_MESSAGE "${inputFileNoPath} - Completed loading file to staging table." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
		  else
			 LOG_MESSAGE "${inputFileNoPath} - Some rows not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

		  fi
	   fi
	  done;

	if [[ $? -ne ${OK} ]]; then
		exit ${FATAL}
	fi

fi

return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Wrapper for PL/SQL package calls
#-------------------------------------------------------------------------
function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${pgmName} ${pgmPID}
      return ${FATAL}
   fi

   return ${OK}
}


#-------------------------------------------------------------------------
# Function: CREATE_DEPT_LIST
# Desc: Create list of Depts to threads
#-------------------------------------------------------------------------
function CREATE_DEPT_LIST
{
	x=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
	SET SERVEROUT ON SIZE 100000
	SET HEAD OFF
	SET FEEDBACK OFF
	  select count(*) from xxadeo_stg_itemloc_in where pub_status = 'R';
	exit;
EOF`

	zero=0
	if [[  ${zero}!=${x} ]] ; then

		DEPT_LIST=$DIR/DEPT_LIST_${extractDate}.$$
$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF >$DEPT_LIST
		   set pause off
		   set echo off
		   set heading off
		   set feedback off
		   set verify off
		   set linesize 1000;
		   set pages 0

		   select distinct to_char(dept) dept
		   from item_master 
		   where item in
				(select item 
				from xxadeo_stg_itemloc_in 
				where pub_status = 'R'
			);
		exit;
EOF



		if [[ `grep "^ORA-" $DEPT_LIST | wc -l` -gt 0 ]]; then
		   cat $DEPT_LIST >> ${ERRORFILE}
		   status=${FATAL}
		   rm $DEPT_LIST 
		   return $status
		else 
		 status=${OK}
		 return $status
		fi

		status=${FATAL}
		return $status


	fi

	return $status
}



#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for valid input arguments
	if [[ ( $# -lt 2 ) ]];
	then
	   USAGE
	   exit ${NON_FATAL}
	fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
Threads=$2 

USER=${connectStr%/*}

	concheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
	constatus=$?

	if [[ ${constatus} -ne ${OK} ]]; then
	   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" ${constatus} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
	   exit 1
	else
	   LOG_MESSAGE "xxadeo_itemloc_in.ksh - Started by ${USER}" "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	fi

	if [[ $2 = "Y" ]]; then
		   SLOTS=$3
		   if [[ -z ${SLOTS} ]]; then
			 SLOTS=10
		   fi
		   STR=$5
		   echo $SLOTS | egrep '^[0-9]+$'>/dev/null 2>&1
		   status=$?
		   if [ "$status" -ne "0" ]
		   then
			 LOG_ERROR "Thread count should be a Positive Number" "NUM_THREADS" "${status}" ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
			 exit 1
		   fi
		   if [[ ${SLOTS} -gt 20 ]]; then
			SLOTS=10
		   fi
		else
		   if [[ $2 = "N" ]]; then
		   STR=$3
		   SLOTS=10
		   else
			 LOG_ERROR "Thread number indicator either should be Y or N" "INDICATOR" "${2}" ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
			 exit 1
		   fi
		fi
	
	
	LOAD_FILE ;

	process_load=$?

	if [[ ${process_load} -ne ${OK} ]]; then
		LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "LOAD" ${process_load} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
		exit 1
	else 
		LOG_MESSAGE "Job terminated with success." "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	#	exit ${OK}
	fi

	ENRICH_VALIDATE;

	process_enrich=$?

	if [[ ${process_enrich} -ne ${OK} ]]; then
		LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "ENRICH_VALIDATE" ${process_enrich} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
		exit 1
	else 
		LOG_MESSAGE "Job terminated with success." "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	#	exit ${OK}
	fi

    LOG_MESSAGE "Create department list" "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	CREATE_DEPT_LIST 
	crt_status=$?
	
	if [[ ${crt_status} -ne ${OK} ]]; then
	 LOG_ERROR "ORA Error while fetching dept" "CREATE_DEPT_LIST" ${crt_status} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
	 exit 1
	fi
	LOG_MESSAGE "Department list created with sucess" "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

	if [[ -f ${DEPT_LIST}  ]]; then 

		while read dept
		do

			if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
				( 
					PROCESS $dept   
				 ) &
				process_status=$?
				LOG_MESSAGE "Status for Dept ${dept} : ${process_status}" "PROCESS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
				
				if [[ ${process_status} -ne ${OK} ]]; then
					LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS" ${process_status} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
					exit 1
				fi
			else

			   while [ `jobs | wc -l` -ge ${SLOTS} ]
			   do
				 sleep 1
			   done
			   ( 			
				PROCESS $dept  
			   )&
			   process_status=$?
			   LOG_MESSAGE "Status for Dept ${dept} : ${process_status}" "PROCESS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
			   
			   if [[ ${process_status} -ne ${OK} ]]; then
					LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS" ${process_status} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
					exit 1
			   fi
			fi

		done < $DEPT_LIST

		### Wait for all of the threads to complete
		wait
		if [[ $? -eq ${OK} ]]; then
		   rm $DEPT_LIST
		 
		fi

	fi	
		PURGE ;  

		purge_status=$?

		if [[ ${purge_status} -ne ${OK} ]]; then
			LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PURGE" ${purge_status} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
			exit 1
		else 
			LOG_MESSAGE "Job terminated with success." "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
		#	exit ${OK}
		fi


	LOG_MESSAGE "xxadeo_itemloc_in.ksh process finished" "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}


	exit ${OK}



													
