#! /bin/ksh

#---------------------------------------------------------------------------------
#  File:  xxadeo_custom_policy_post.ksh - Disables base policies based on the the 
#         argument passed in the second parameter (custom schema)
#---------------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
traceLog="${MMHOME}/log/${pgmName}_${pgmPID}_tracefiles.log"
OK=0
FATAL=255

echo 'LOGFILE=' $LOGFILE 
echo 'ERRORFILE=' $ERRORFILE 

USAGE="Usage: `basename $0` <connect> <custom_schema>"
#-------------------------------------------------------------------------
# Function Name: LOG_ERROR
# Purpose      : Log the error messages to the error file.
#-------------------------------------------------------------------------
function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: LOG_MESSAGE
# Purpose      : Log the  messages to the log file.
#-------------------------------------------------------------------------
function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: ENABLE_POLICY
#-------------------------------------------------------------------------

function ENABLE_POLICY
{
 ERROR_MESSAGE= `$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
                set serveroutput off
                set feedback off
                set heading off
                set trimspool on
                set pagesize 0
                set linesize 7000
                        
                variable o_error_message char(2000);
             DECLARE
               --
               L_program varchar2(50);
               --
               L_object_schema DBA_POLICIES.OBJECT_OWNER%TYPE;
               L_object_name   DBA_POLICIES.OBJECT_NAME%TYPE;
               L_policy_name   DBA_POLICIES.POLICY_NAME%TYPE; 
			   L_owner_schema  DBA_POLICIES.OBJECT_OWNER%TYPE;
               --
               cursor C_policies(V_custom_schema DBA_POLICIES.PF_OWNER%TYPE) is
                 select object_owner,
                        object_name,
                        policy_name
                   from dba_policies a
                  where pf_owner = L_owner_schema
                    and exists (select 1
                                  from dba_policies b
                                 where b.object_name   = a.object_name
                                   and upper(pf_owner) = upper(V_custom_schema));
               --
               BEGIN
                 --
				 select table_owner
                   into L_owner_schema
                   from system_options;
				 --
                 for rec in C_policies('${CUSTOM_SCHEMA}') loop
                     --
                     dbms_rls.enable_policy(object_schema => rec.object_owner,
                                            object_name   => rec.object_name,
                                            policy_name   => rec.policy_name,
                                            enable        => FALSE);
                   --
                 end loop;
                 --
               EXCEPTION
                 when OTHERS then
                   --
                  ROLLBACK;
                  :o_error_message:='@Error :'||SUBSTR(SQLERRM, 1, 247);
                   --
               END;
              /
	          PRINT :O_error_message; 
EOF`

    if [[ -n ${ERROR_MESSAGE} ]]; then
       LOG_MESSAGE "Aborted in process."          
       echo -e  "" >> ${LOGFILE}
       echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg"          
       echo -e  "${ERROR_MESSAGE}\n" >> ${ERRORFILE}          
       return ${FATAL}
	else
	   LOG_MESSAGE "Successfully Completed"
	   echo "Successfully Completed"
       return ${OK}
    fi
}

#-----------------------------------------------
# Main program starts 
# Parse the command line
#-----------------------------------------------
# Test for the number of input arguments
if [ $# -lt 2 ]
then
   echo "Usage: `basename $0` <connect> <custom_schema>"
   exit ${NON_FATAL}
fi

#Validate input parameters
CONNECT=$1
CUSTOM_SCHEMA=$2

$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >>$ERRORFILE
EOF

if [ `cat $ERRORFILE | wc -l` -gt 1 ]
then
   LOG_MESSAGE "Exiting due to ORA/LOGIN Error. Check error file."
   echo "Exiting due to ORA/LOGIN Error. Check error file."
   exit 1;
fi

USER=`sqlplus -s $CONNECT << EOF
      set heading off;
      set pagesize 0;
      select user from dual;
      exit;
EOF`

LOG_MESSAGE "Started by ${USER}"

ENABLE_POLICY

if [[ ! ( -s $ERRORFILE ) ]]
then
   rm -f $ERRORFILE
fi

exit 0
