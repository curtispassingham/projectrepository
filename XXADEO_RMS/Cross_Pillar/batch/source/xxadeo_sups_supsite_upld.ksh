#! /bin/ksh
#----------------------------------------------------------------------------
#	File:  xxadeo_sups_supsite_upld.ksh
#
#	Desc:  UNIX shell script to load supplier and supplier sites 
#	information from ERP cloud to RMS Database.
#----------------------------------------------------------------------------

# Common functions and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='xxadeo_sups_supsite_upld.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=$(date +"%Y%m%d%H%M%S")

begintime=$SECONDS
# File locations
CTLFILEsup="${MMHOME}/oracle/proc/bin/${pgmName}_sup.ctl"
CTLFILEsite="${MMHOME}/oracle/proc/bin/${pgmName}_site.ctl"
LOGDIR="${MMHOME}/log"
LOGFILE="${MMHOME}/log/${pgmName}_$(date +"%Y%m%d").log"
ERRORFILE="${MMHOME}/error/${pgmName}_$(date +"%Y%m%d").err"

# Initialize variables and constants
inputfile_ext=0
REJECT="R"
FTYPE="SUSI"

# KSH valid mode values
PMLOAD="LOAD"
PMPRC="PROCESS"
PMPRG="PURGE"

connectStr='rms/oracle01@192.168.56.42:1521/oretailpdb'
SQLLDR_SUCC=0
SQLLDR_FAIL=1
SQLLDR_WRNG=2
SQLLDR_FATAL=3

totalsuccess=0
totalfail=0

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string> <process mode> <input file>

  1 <connect string>   Username/password@db.

  2 <process mode>     Required parameter. Valid process modes are:

                      LOAD    - Upload supplier and supplier site information 
				from input file.
                      PROCESS - Process supplier information.
                      PURGE   - Delete supplier information in the staging 
				tables that is older than the 
				retention date.

  3 <input file>       Required only for "'LOAD'" mode. Input file. 
			Can include directory path.
  or
  3 <retention date>   Required only for "'PURGE'" mode. Processed records
			older than this ammount of days will be purged.
"
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Wrapper for PL/SQL package calls
#-------------------------------------------------------------------------
function EXEC_SQL
{
   sqlTxt=$*

	sqlReturn=`echo "set feedback off;
      		set heading off;
      		set term off;
      		set verify off;
      		set serveroutput on size 1000000;

      		VARIABLE GV_return_code    NUMBER;
      		VARIABLE GV_script_error   CHAR(255);

      		EXEC :GV_return_code  := 0;
      		EXEC :GV_script_error := NULL;

      		WHENEVER SQLERROR EXIT ${FATAL}
      		$sqlTxt
      		/

      		print :GV_script_error;
        	exit  :GV_return_code;
		" | sqlplus -s ${connectStr}`
	

	if [[ $? -ne ${OK} ]]; then
		LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
        return ${FATAL}
        fi
	
	totalsuccess=$(echo $sqlReturn | awk '{ print $1 }')
	totalfail=$(echo $sqlReturn | awk '{ print $2 }')
   	return ${OK} ${ARG1} ${ARG2}
}


#-------------------------------------------------------------------------
# Function Name: PARSE_ERROR_MSG
# Purpose      : Parse error message for error log.
#-------------------------------------------------------------------------
function PARSE_ERROR_MSG
{

   sqlTxt="
      DECLARE
         L_error          VARCHAR2(4000) := '"${1}"';
         L_error_key      RTK_ERRORS.RTK_KEY%TYPE;
         FUNCTION_ERROR   EXCEPTION;

      BEGIN
        if SQL_LIB.PARSE_MSG(L_error,
                             L_error_key) = FALSE then
             raise FUNCTION_ERROR;
        end if;

          :GV_script_error := L_error;

      EXCEPTION
         when FUNCTION_ERROR then
            :GV_script_error := 'Error parsing error message';
            :GV_return_code := ${FATAL};
         when OTHERS then
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi

   return ${OK}
}

#------------------------------------------------------------------------
# Function Name: UPDATE_BATCH_ID
# Purpose 	   : Updates the newly loaded staging records to provide 
#				  them a batch id.	
#------------------------------------------------------------------------
function UPDATE_BATCH_ID
{
	batch_id=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
	set pause off
	set pagesize 0
	set feedback off
	set verify off
	set heading off
	set echo off
	select max(XX_BATCH_ID)
	  from XXADEO_SUPS;
	exit;
	EOF`
	
	let batch_id+=1
	
	update=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
	set pause off
	set pagesize 0
	set feedback off
	set verify off
	set heading off
	set echo off
	update XXADEO_SUPS
	   set XX_BATCH_ID=${batch_id}
	 where XX_BATCH_ID=0;
	update XXADEO_SUPSITE_ORG_UNIT
	   set XX_BATCH_ID=${batch_id}
	 where XX_BATCH_ID=0;
	update XXADEO_SUPS_ADDR
	   set XX_BATCH_ID=${batch_id}
	 where XX_BATCH_ID=0;
	update XXADEO_SUPSITE_ADDR
	   set XX_BATCH_ID=${batch_id}
	 where XX_BATCH_ID=0;
	update XXADEO_SUPS_CFA
	   set XX_BATCH_ID=${batch_id}
	 where XX_BATCH_ID=0;
	COMMIT;
 	exit;
	EOF`
}


#-------------------------------------------------------------------------
# Function Name: CHECK_INPUT_FILE
# Purpose      : Reads the given file and checks if it is well formed.
#                 If it is not, throws error and stops the program.
#-------------------------------------------------------------------------
function CHECK_INPUT_FILE
{
	typeset filewithpath=$1
	typeset rejectAll="N"
	pgm=${pgmName}
	PID=${pgmPID}
	if [[ -e ${filewithpath} ]]; then
	
		CHECK_FILENAME ${filewithPath} ${ERRORFILE} ${LOGFILE} ${pgmName}
		if [[ $? -ne ${OK} ]]; then
			exit ${FATAL}
		fi
	else 
		LOG_ERROR "Cannot find input file ${filewithPath}." "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgm} ${PID}
		exit ${FATAL}
	fi
	
	typeset -i FHEADcount=0
	typeset -i THEADcount=0

	
	while read myline;do
		identifier=`echo $myline|cut -c1-5`
   		case $identifier in
   			FHEAD)
   				if [[ $FHEADcount -ge 1 ]]; then
   					LOG_ERROR "Input file not well formed: FHEAD problem" "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgm} ${PID}
   					exit ${FATAL}
   				else 
   					let FHEADcount+=1
   				fi
   				;;
   			THEAD)
   			 	if [[ $THEADcount -ge 1 ]]; then
   					LOG_ERROR "Input file not processed : file not well formed - THEAD problem" "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgm} ${PID}
   					exit ${FATAL}
   				else 
   					let THEADcount+=1
   				fi
   				;;
   			TADDR)
   			 	if [[ $THEADcount -eq 0 ]]; then
   					LOG_ERROR "Input file not processed : file not well formed - TADDR without THEAD problem" "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgm} ${PID}
   					exit ${FATAL}
   				fi
   				;;
   			TSPOU)
   				if [[ $THEADcount -eq 0 ]]; then
   					LOG_ERROR "Input file not processed : file not well formed - TSPOU without THEAD problem" "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgm} ${PID}
   					exit ${FATAL}
   				fi
   				;;
   			TSCFA)
   				if [[ $THEADcount -eq 0 ]]; then
   					LOG_ERROR "Input file not processed : file not well formed - TSCFA without THEAD problem" "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgm} ${PID}
   					exit ${FATAL}
   				fi
   				;;
   			TTAIL)
   				if [[ $THEADcount -lt 1 ]]; then
   					LOG_ERROR "Input file not processed : file not well formed - TTAIL problem" "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgm} ${PID}
   					exit ${FATAL}
   				else 
   					let THEADcount-=1
   				fi
   				;;
   			FTAIL)
   				if [[ $FHEADcount -lt 1 ]]; then
   					LOG_ERROR "Input file not processed : file not well formed - TTAIL problem" "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgm} ${PID}
   					exit ${FATAL}
   				else 
   					let FHEADcount-=1
   				fi
   				;;
  			*)
  				if [[ $FHEADcount -ne 0 ]];then
   					LOG_ERROR "Input file not processed : file not well formed - Unknown Identifier" "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgm} ${PID}
   					exit ${FATAL}
   				fi
   				;;
   		esac
	
	done < $filewithpath

	
	if [[ $FHEADcount -ne 0 || $THEADcount -ne 0 ]];then
		LOG_ERROR "Input file not processed : it did not end properly" "CHECK_INPUT_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgm} ${PID}
   		exit ${FATAL}
   	else 
   		LOG_MESSAGE "Input file is well formed." "CHECK_INPUT_FILE" ${OK} ${LOGFILE} ${pgm} ${PID}
   		echo "Input file well formed, processing ... "
   	fi
   	
}

#-------------------------------------------------------------------------
# Function Name: LOAD_FILE
# Purpose      : Reads the given file and extracts its data and then loads 
#                it to the staging table
#-------------------------------------------------------------------------
function LOAD_FILE
{
	input=$1
	CHECK_INPUT_FILE $input
	supInput="${input%\.*}_sup.dat"
	siteInput="${input%\.*}_site.dat"
	if [[ -f $supInput ]];then
		rm $supInput
	fi
	if [[ -f $siteInput ]];then
		rm $siteInput
	fi
	ind=''
	while read myline;do
   		identifier=`echo $myline|cut -c1-5`
   		case $identifier in
   			THEAD) 
   				if [[ ${#myline} -gt 3342 ]]; then
   					ind="site"
   					echo "$myline" >> $siteInput
   				else
   					ind='sup'
   					echo "$myline" >> $supInput
   				fi
   				;;
   			TADDR)
   				if [[ "$ind" == "site" ]]; then
   					echo "$myline" >> $siteInput
   				else
   					echo "$myline" >> $supInput
   				fi
   				;;
   			TSPOU)
   				if [[ "$ind" == "site" ]]; then
   					echo "$myline" >> $siteInput
   				else
   					echo "$myline" >> $supInput
   				fi
   				;;
   			TSCFA)
   				if [[ "$ind" == "site" ]]; then
   					echo "$myline" >> $siteInput
   				else
   					echo "$myline" >> $supInput
   				fi
   				;;
   		esac
	done < $input
	

#
# Checking and loading supplier data
#
	echo "Checking Supplier Data ..."
	if [[ -f $supInput ]];then
		echo "Loading supplier data into the tables ..."
		dtStamp=`date +"%G%m%d%H%M%S"`
		sqlldrFile=${supInput##*/}
		sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
		sqlldr ${connectStr} silent=feedback,header \
   			control=${CTLFILEsup} \
   			log=${sqlldrLog} \
   			data=${supInput} \
   			bad=${MMHOME}/log/$sqlldrFile.bad \
   			discard=${MMHOME}/log/$sqlldrFile.dsc
		
		sqlldr_status_sup=$?
			
		if [[ $sqlldr_status_sup = ${SQLLDR_FAIL} ||  $sqlldr_status = ${SQLLDR_FATAL} ]]; then
   	  		LOG_ERROR "Error while loading file ${filewithPath}. See ${sqlldrLog} for details." "LOAD_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
   	  		exit ${FATAL}
		else
      		# Check log file for sql loader errors
   	  		if [[ `cat ${sqlldrLog} | grep -zPx "Table XXADEO_SUPS:\n  0 Rows successfully loaded."` != "" ]]; then
	   	     		LOG_MESSAGE "${sqlldrFile} - No records loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	   	     		LOG_MESSAGE "Program suppliersitesprocess.ksh Terminated." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   	     			exit ${FATAL}
			elif [[ $sqlldr_status_sup = ${SQLLDR_WRNG} ]]; then
				LOG_MESSAGE "${sqlldrFile} - Some records not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${NON_FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
				LOG_ERROR "Error while loading file ${sqlldrFile}. Some records not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${NON_FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
   	     		else
				LOG_MESSAGE "${sqlldrFile} - All supplier records loaded to the staging tables. " "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
				LOG_MESSAGE "Completed loading supplier data to the supplier staging tables." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
				echo "Supplier data successfuly loaded to the staging tables."
   	     		fi
		fi
		echo "... done"
	else
		LOG_MESSAGE "No Supplier data found in file" "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
		echo "No Supplier Data found in file, going to next step"	
	fi
	

   	
#
# Checking and loading supplier site data
#
	echo "Checking Supplier Site Data ..."
	if [[ -f $siteInput ]];then
		echo "Loading supplier site data into the tables ... "
		dtStamp=`date +"%G%m%d%H%M%S"`
		sqlldrFile=${siteInput##*/}
		sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
		sqlldr ${connectStr} silent=feedback,header \
   			control=${CTLFILEsite} \
   			log=${sqlldrLog} \
   			data=${siteInput} \
   			bad=${MMHOME}/log/$sqlldrFile.bad \
   			discard=${MMHOME}/log/$sqlldrFile.dsc

		sqlldr_status_site=$?
		
		if [[ $ssqlldr_status_site = ${SQLLDR_FAIL} ||  $sqlldr_status = ${SQLLDR_FATAL} ]]; then
   	  		LOG_ERROR "Error while loading file ${filewithPath}. See ${sqlldrLog} for details." "LOAD_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
   	  		exit ${FATAL}
		else
      		# Check log file for sql loader errors
			if [[ `cat ${sqlldrLog} | grep -zPx "Table XXADEO_SUPS:\n  0 Rows successfully loaded."` != "" ]]; then
   	     			LOG_MESSAGE "${inputFileNoPath} - No records loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   	     			LOG_MESSAGE "Program suppliersitesprocess.ksh Terminated." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   	     			exit ${OK}
			elif [[ $sqlldr_status_site = ${SQLLDR_WRNG} ]]; then
				LOG_MESSAGE "${sqlldrFile} - Some Supplier Site records not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${NON_FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
				LOG_ERROR "Error while loading file ${sqlldrFile}. Some records not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${NON_FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
   	     		else
				LOG_MESSAGE "${sqlldrFile} - All Supplier Site records loaded to the staging tables. " "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
				LOG_MESSAGE "Completed loading supplier site data to the supplier staging tables." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
				echo "Supplier data successfuly loaded to the staging tables."
			fi
		fi
		echo "... done"

	else 
		LOG_MESSAGE "No Supplier Site data found in file" "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}	
		echo "No Supplier Site Data found in file, going to next step"
	fi

	if [[ -f $supInput ]];then
		rm $supInput
	fi

	if [[ -f $siteInput ]];then
		rm $siteInput
	fi
	
	UPDATE_BATCH_ID
	
	MARK_DISCARD
   	if [[ $sqlldr_status_site = ${SQLLDR_WRNG} || $sqlldr_status_sup = ${SQLLDR_WRNG} ]]; then
		return ${SQLLDR_WRNG}
	else 
		return ${SQLLDR_SUCC}
	fi	
}

#-------------------------------------------------------------------------
# Function Name: MARK_DISCARD
# Purpose      : Marks old 'E'rror records as 'D'iscard if the same record 
#                 is loaded once again in the tables.
#-------------------------------------------------------------------------
function MARK_DISCARD
{

	sqlTxt="
		DECLARE
			O_key					RTK_ERRORS.RTK_KEY%TYPE;
			FUNCTION_ERROR				EXCEPTION;
			
		BEGIN
			if NOT XXADEO_SUPS_INBOUND.MARK_DISCARD(:GV_script_error) then
				raise FUNCTION_ERROR;
			end if;
			 
			COMMIT;
		EXCEPTION
			when FUNCTION_ERROR then
				ROLLBACK;
				if SQL_LIB.PARSE_MSG(:GV_script_error,
							O_key) = FALSE then
					NULL;
				end if;
				:GV_return_code := ${FATAL};

			when OTHERS then
				ROLLBACK;
				:GV_script_error := SQLERRM;
				:GV_return_code := ${FATAL};
			END;"
	
	EXEC_SQL ${sqlTxt}

	if [[ $? -ne ${OK} ]]; then
		LOG_MESSAGE "Marking 'E' records for discard failed." "MARK_DISCARD" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
		exit ${FATAL}
	else
	
		LOG_MESSAGE "Successfuly marked old 'E' records as 'D' if needed." "MARK_RECORDS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	fi
	
	return ${OK}
}

# ---------------------------------------------------------------------------------------------------
# Function Name : PURGE_HISTORY
# Purpose 		: Purge the records in the staging tables that are older than the given ammount of 
#				  days (passed as a parameter)	
# ---------------------------------------------------------------------------------------------------
function PURGE_HISTORY
{
	days=$1
	sqlTxt="
		DECLARE
			O_key				RTK_ERRORS.RTK_KEY%TYPE;
			FUNCTION_ERROR			EXCEPTION;
			
		BEGIN
			if NOT XXADEO_SUPS_INBOUND.SUPS_PURGE_HIST(:GV_script_error,
													   ${days}) then
				raise FUNCTION_ERROR;
			end if;
			
			COMMIT;
		EXCEPTION
			when FUNCTION_ERROR then
				ROLLBACK;
				if SQL_LIB.PARSE_MSG(:GV_script_error,
							O_key) = FALSE then
					NULL;
				end if;
				:GV_return_code := ${FATAL};

			when OTHERS then
				ROLLBACK;
				:GV_script_error := SQLERRM;
				:GV_return_code := ${FATAL};
			END;"
	
	EXEC_SQL ${sqlTxt}
	
	if [[ $? -ne ${OK} ]]; then
		exit ${FATAL}
	else
		LOG_MESSAGE "Successfully purged records older than ${days} days." "PURGE_HISTORY" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	fi


	return ${OK}
}

# ---------------------------------------------------------------------------------------------------
# Function Name : PROCESS_RECORDS
# Purpose 		: Processes records marked as 'N' in the suppliers staging tables, then validates them.
#				   If successful, they are loaded to RMS tables and marked as 'P', otherwise as 'E'
# ---------------------------------------------------------------------------------------------------

function PROCESS_RECORDS
{
	sqlTxt="
		DECLARE
			O_key					RTK_ERRORS.RTK_KEY%TYPE;
			FUNCTION_ERROR				EXCEPTION;
			
		BEGIN
			if NOT XXADEO_SUPS_INBOUND.PROCESS_RECORDS(:GV_script_error) then
				raise FUNCTION_ERROR;
			end if;
			 
			COMMIT;
		EXCEPTION
			when FUNCTION_ERROR then
				ROLLBACK;
				if SQL_LIB.PARSE_MSG(:GV_script_error,
							O_key) = FALSE then
					NULL;
				end if;
				:GV_return_code := ${FATAL};

			when OTHERS then
				ROLLBACK;
				:GV_script_error := SQLERRM;
				:GV_return_code := ${FATAL};
			END;"
	
	EXEC_SQL ${sqlTxt}

	if [[ $? -ne ${OK} ]]; then
		LOG_MESSAGE "Processing records failed." "PROCESS_RECORDS" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
		exit ${FATAL}
	else
	
		LOG_MESSAGE "Successfully processed $totalsuccess records. Processed $totalfail records with errors." "PROCESS_RECORDS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	fi
	
	return ${OK}
}




# --------------------------------------------------------------------------------------------------- #
#												      #		
#						MAIN PROGRAM	      				      #
#												      #
# --------------------------------------------------------------------------------------------------- #

	# Test for the number of input arguments

if [ $# -lt 2 ]; then
	USAGE
	exit ${NON_FATAL}
else
	if [[ $2 = "LOAD" || $2 = "PURGE" ]]; then
   		if [ $# -lt 3 ]; then
       		USAGE
       		exit ${NON_FATAL}
   		fi
	fi
fi

connectStr=$1
USER=${connectStr%%/*}

# Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${connectStr}`

if [[ $? -ne ${OK} ]]; then
	echo $ConnCheck
	LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	exit ${FATAL}
else
	LOG_MESSAGE "${pgmName} started by ${USER}." "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

processMode=$2


# Validate the process mode
if [[ ${processMode} != ${PMLOAD} && ${processMode} != ${PMPRC} && ${processMode} != ${PMPRG} ]]; then
	LOG_ERROR "Invalid process mode. Check usage for valide modes." "MAIN" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
	exit ${FATAL}
fi
if [[ ${processMode} == ${PMLOAD} ]]; then
	inputFile=$3
	inputFile_ext=${inputFile##*.}
	inputFileNoPath=${inputfile##*/}
elif [[ ${processMode} == ${PMPRG} ]]; then
	if [[ $3 == +([0-9]) ]]; then	
		retentiondate=$3
	else 
		LOG_ERROR "Invalid parameter for Purge mode. Third parameter must be an integer." "MAIN" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
		USAGE
		exit ${FATAL}
	fi
fi
	

if [[ ${processMode} = ${PMLOAD} ]]; then
	#########################################################################################
	#				     LOAD MODE                           		#
	#########################################################################################
	
	LOAD_FILE $inputFile

	LOAD_CODE=$?
	endtime=$SECONDS
	elapsed=$((endtime - begintime))
	if [[ $LOAD_CODE = ${SQLLDR_WRNG} ]]; then
		LOG_MESSAGE "Load mode terminated in $elapsed seconds. Some records were not loaded. See SQLLoader logs for more information." "LOAD_MODE" ${NON_FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
		LOG_MESSAGE "Program Terminated." "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
		echo "Load mode terminated. Some records were not loaded."
		exit ${NON_FATAL}
	else
		LOG_MESSAGE "Load mode ended successfuly in $elapsed seconds" "LOAD_MODE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
		LOG_MESSAGE "Program Terminated." "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
		echo "Load mode ended successfuly."
		exit ${OK}
	fi

	
elif [[ ${processMode} = ${PMPRC} ]]; then
	#########################################################################################
    	#                                    PROCESS MODE                                       #
    	#########################################################################################
	
	PROCESS_RECORDS

	endtime=$SECONDS
	elapsed=$((endtime - begintime))
	LOG_MESSAGE "Process mode ended succesfuly in $elapsed seconds." "PROCESS_MODE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	LOG_MESSAGE "Program Terminated." "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	echo "Process mode ended successfuly."
	exit ${OK}

	
elif [[ ${processMode} = ${PMPRG} ]]; then
	#########################################################################################
   	#                 	              PURGE MODE                                        #
   	#########################################################################################
    
	PURGE_HISTORY $retentiondate
	
	endtime=$SECONDS
	elapsed=$((endtime - begintime))
	LOG_MESSAGE "Purge mode ended successfyly in $elapsed seconds." "PURGE_MODE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	LOG_MESSAGE "Program Terminated." "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	echo "Purge mode ended successfuly"
	exit ${OK}
	
fi


exit ${OK}
