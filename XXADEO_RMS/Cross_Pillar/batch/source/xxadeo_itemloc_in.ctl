LOAD DATA
APPEND
INTO TABLE xxadeo_stg_itemloc_in
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(item,
   loc,
   loc_type,
   primary_sup,
   src_method,
   source_wh,
   attrib_1,
   attrib_2,
   attrib_3,
   attrib_4,
   attrib_5,
   op_type,
   create_datetime date 'YYYYMMDD',
   last_update_datetime date 'YYYYMMDD',
   pub_status            CONSTANT "N"
)
