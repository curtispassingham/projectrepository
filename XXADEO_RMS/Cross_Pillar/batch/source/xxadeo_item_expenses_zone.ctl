LOAD DATA
APPEND
INTO TABLE XXADEO_STG_ITEM_EXP_IN
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(item,
   supplier,
   discharge_port,
   zone_id,
   comp_id,
   comp_rate,
   comp_currency,
   per_count_uom,
   per_count,
   nom_flag_4,
   last_update_datetime date 'YYYYMMDD' ,
   last_update_id,
   update_type,
   pub_status            CONSTANT "N",
   item_exp_type      CONSTANT "Z"
)
