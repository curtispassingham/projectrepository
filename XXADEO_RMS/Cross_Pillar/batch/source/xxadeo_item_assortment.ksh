#! /bin/ksh
#-------------------------------------------------------------------------
#  File: xxadeo_item_assortment.ksh
#
#  Desc: UNIX shell script to invoke ADEO assortment processing
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='xxadeo_item_assortment.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind

echo "Logfile=" $LOGFILE
echo "Errorfile=" $ERRORFILE

CURR_THREADS=1
OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.
  "
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: PROCESS_SD
# Purpose      : This will call XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_SD 
#                which is responsible for integration of Sales Dates 
#                (Start+End) UDAs.
#-------------------------------------------------------------------------
function PROCESS_SD
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
         L_error_message   VARCHAR2(1000);
      BEGIN
         if XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_SD(:GV_script_error) = FALSE then
            raise FUNCTION_ERROR;
         end if;
         COMMIT;
      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
       LOG_ERROR "XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_SD Failed" "PROCESS_SD" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
       return ${FATAL}
   else
      LOG_MESSAGE "XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_SD - Successfully Completed" "PROCESS_SD" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${OK}
   fi

}

#-------------------------------------------------------------------------
# Function Name: PROCESS_AM
# Purpose      : This will call XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_AM 
#                which is responsible for integration of Assortment Mode 
#                UDAs.
#-------------------------------------------------------------------------
function PROCESS_AM
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
         L_error_message   VARCHAR2(1000);
      BEGIN
         if XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_AM(:GV_script_error) = FALSE then
            raise FUNCTION_ERROR;
         end if;
         COMMIT;
      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
       LOG_ERROR "XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_AM Failed" "PROCESS_AM" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
       return ${FATAL}
   else
      LOG_MESSAGE "XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_AM - Successfully Completed" "PROCESS_AM" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${OK}
   fi

}

#-------------------------------------------------------------------------
# Function Name: PROCESS_RS
# Purpose      : This will call XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_RS 
#                which is responsible for integration of Range Size
#                UDAs.
#-------------------------------------------------------------------------
function PROCESS_RS
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
         L_error_message   VARCHAR2(1000);
      BEGIN
         if XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_RS(:GV_script_error) = FALSE then
            raise FUNCTION_ERROR;
         end if;
         COMMIT;
      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
       LOG_ERROR "XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_RS Failed" "PROCESS_RS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
       return ${FATAL}
   else
      LOG_MESSAGE "XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_RS - Successfully Completed" "PROCESS_RS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${OK}
   fi

}

#-------------------------------------------------------------------------
# Function Name: PROCESS_LOC
# Purpose      : This will call XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_LOC 
#                which is responsible for integration of ITEM_LOC
#                ranging and CFAs create/update (Visibility/Availability).
#-------------------------------------------------------------------------
function PROCESS_LOC
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
         L_error_message   VARCHAR2(1000);
      BEGIN
         if XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_LOC(:GV_script_error) = FALSE then
            raise FUNCTION_ERROR;
         end if;
         COMMIT;
      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
       LOG_ERROR "XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_LOC Failed" "PROCESS_LOC" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
       return ${FATAL}
   else
      LOG_MESSAGE "XXADEO_ITEM_ASSORTMENT_SQL.PROCESS_LOC - Successfully Completed" "PROCESS_LOC" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${OK}
   fi

}

#-----------------------------------------------
# Main program starts
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   USAGE
   exit ${NON_FATAL}
fi

#Validate input parameters
CONNECT=$1

if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "xxadeo_item_assortment.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#--- Invoke PROCESS_SD Function (Sales Dates)
PROCESS_SD

#--- Invoke PROCESS_SD Function (Assortment Mode)
PROCESS_AM

#--- Invoke PROCESS_RS Function (Range Size)
PROCESS_RS

#--- Invoke PROCESS_LOC Function (ITEM_LOC assortment)
PROCESS_LOC

#--  Check for any Oracle errors from the SQLPLUS process
if [[ `grep "${pgmPID}: Aborted" $LOGFILE | wc -l` -gt 0 ]]; then
   LOG_MESSAGE "Errors encountered. See error file" "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "Program xxadeo_item_assortment.ksh terminated successfully." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

exit 0

