#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  xxadeo_export_itemloc.ksh
#
#  Desc:  UNIX shell script to extract the itemloc records.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='xxadeo_export_itemloc.ksh'
pgmName=${pgmName##*/}               # get the file name
pgmExt=${pgmName##*.}                # get the extenstion
pgmName=${pgmName%.*}                # remove the file extenstion
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
DIR=`pwd`

OK=0
FATAL=255
#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   # Those are the execution parameters needed to invoke the program
   echo "USAGE: . $pgmName <connect string>     <mode> <thread indicator>

        <connect string> is a fully qualified connection string in format:   Username/password@db. Use "'$UP'" if using Oracle Wallet.
        <mode> is the mode of extraction. full or delta
        <thread indicator> indicates if user will provide a thread number. either Y or N.
        <thread number indicator> indicates the number of threads to use:    In case of thread Indicator = Y"
}
#-------------------------------------------------------------------------
# Function: UPDATE_ITEM_EXPORT_STG
# Desc: Updating the item_export_stg with current process id
#-------------------------------------------------------------------------
function UPDATE_ITEM_EXPORT_STG
{
process_id=$1
echo "Updating item_export_stg with process id: $process_id" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

  VARIABLE GV_process_id NUMBER;
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_process_id := ${process_id};
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;

   update item_export_stg stg
   set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemloccre','itemlocmod')
     and process_id is null;

   update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemlocdel')
     and process_id is null;

    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi

return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_STG
# Desc: Updating the exported item_export_stg records
#-------------------------------------------------------------------------
function PROCESS_STG
{
process_id=$1
echo "Updating exported records" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

  VARIABLE GV_process_id NUMBER;
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_process_id := ${process_id};
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;

   # Update the value base_extracted_ind to Y after the extraction completed
   update item_export_stg stg
     set stg.base_extracted_ind = 'Y'
   where stg.base_extracted_ind = 'N'
     and stg.action_type in ('itemloccre','itemlocmod','itemlocdel')
     and stg.process_id = trim(:GV_process_id)
     and exists (select 1 from item_master im where im.item = stg.item and im.item_level = 1);

     # Update the value base_extracted_ind to D after the extraction completed
     # Just values that have the item lvl different than 1
   update item_export_stg stg
     set stg.base_extracted_ind = 'D'
   where stg.base_extracted_ind = 'N'
     and stg.action_type in ('itemloccre','itemlocmod','itemlocdel')
     and stg.process_id = trim(:GV_process_id)
     and exists (select 1 from item_master im where im.item = stg.item and im.item_level > 1);

    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}

#-------------------------------------------------------------------------
# Function: CREATE_BU_LIST
# Desc: Write Business Units to a FILE
#-------------------------------------------------------------------------
function CREATE_BU_LIST
{
echo "Creating the location list" >> $LOGFILE

BU_LIST=$DIR/bulist_${extractDate}.$$
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$BU_LIST
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   select to_char(bu.bu)   bu,
   to_char(dvm.value_1)    bu_acronym
   from
        (select distinct bu from xxadeo_bu_ou) bu
                , xxadeo_mom_dvm dvm
                where bu.bu = dvm.bu
                and func_area = 'BU_ACRONYM';
EOF

if [[ `grep "^ORA-" $BU_LIST | wc -l` -gt 0 ]]; then
   cat $BU_LIST >> ${ERRORFILE}
   status=${FATAL}
   rm $BU_LIST
else
   status=${OK}
fi

return $status
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_FULL
# Purpose      : Export full item loc records
#-------------------------------------------------------------------------
function PROCESS_FULL
{
bu=$1
bu_acronym=$2

#It is used the next name to the file generated: XXRMS210_BU_ACRONYM_EXTRACTDATE_FULL
echo "Extracting full item location records for Business Units: ${bu_acronym}" >> $LOGFILE
 ITEMSTORE=$DIR/XXRMS210_ITEMLOC_${bu_acronym}_${extractDate}_FULL

   # Always generate a file even when there are no records
   touch $ITEMSTORE
   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$ITEMSTORE
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   VARIABLE GV_bu NUMBER;
   VARIABLE GV_script_error CHAR(255);
   VARIABLE GV_return_code NUMBER;

   EXEC :GV_bu := ${bu};


select Family                     ||'|'||
       Type                       ||'|'||
       LocType                    ||'|'||
       Location                   ||'|'||
       Item                       ||'|'||
       ItemParent                 ||'|'||
       ItemGrandparent            ||'|'||
       InitialUnitRetail          ||'|'||
       SellingUnitRetail          ||'|'||
       CurrencyCode               ||'|'||
       SellingUOM                 ||'|'||
       TaxableInd                 ||'|'||
       LocalItemDesc              ||'|'||
       LocalShortDesc             ||'|'||
       Ti                         ||'|'||
       Hi                         ||'|'||
       StoreOrderMultiple         ||'|'||
       Status                     ||'|'||
       DailyWastePct              ||'|'||
       MeasureOfEach              ||'|'||
       MeasureOfPrice             ||'|'||
       UomOfPrice                 ||'|'||
       PrimaryVariant             ||'|'||
       PrimaryCostPack            ||'|'||
       PrimarySupplier            ||'|'||
       PrimaryOriginCountry       ||'|'||
       ReceiveAsType              ||'|'||
       InboundHandlingDays        ||'|'||
       SourceMethod               ||'|'||
       SourceWh                   ||'|'||
       UinType                    ||'|'||
       UinLabel                   ||'|'||
       CaptureTimeInProc          ||'|'||
       ExtUinInd                  ||'|'||
       IntentionallyRangedInd     ||'|'||
       CostingLocation            ||'|'||
       CostingLocType             ||'|'||
       LaunchDate                 ||'|'||
       QtyKeyOptions              ||'|'||
       ManualPriceEntry           ||'|'||
       DepositCode                ||'|'||
       FoodStampInd               ||'|'||
       WicInd                     ||'|'||
       ProportionalTarePct        ||'|'||
       FixedTareValue             ||'|'||
       FixedTareUom               ||'|'||
       RewardEligibleInd          ||'|'||
       NatlBrandCompItem          ||'|'||
       ReturnPolicy               ||'|'||
       StopSaleInd                ||'|'||
       ElectMtkClubs              ||'|'||
       ReportCode                 ||'|'||
       ReqShelfLifeOnSelection    ||'|'||
       ReqShelfLifeOnReceipt      ||'|'||
       IBShelfLife                ||'|'||
       StoreReorderableInd        ||'|'||
       RackSize                   ||'|'||
       FullPalletItem             ||'|'||
       InStoreMarketBasket        ||'|'||
       StorageLocation            ||'|'||
       AltStorageLocation         ||'|'||
       ReturnableInd              ||'|'||
       RefundableInd              ||'|'||
       BackOrderInd               ||'|'||
       MerchandiseInd                       ||'|'||
            AssortmentMode                          ||'|'||
            Availability                                    ||'|'||
            Visibility                                 ||'|'||
            VatCode                                            ||'|'||
            UpdateDate                                 ||'|'||
            ModifyPricePos
  from ( select 'ITEMLOC'                                Family
               ,'FULLITEMLOC'                            Type
               ,il.loc_type                              LocType
               ,il.loc                                   Location
               ,il.item                                  Item
               ,il.item_parent                           ItemParent
               ,il.item_grandparent                      ItemGrandparent
               ,il.unit_retail                           InitialUnitRetail
               ,il.selling_unit_retail                   SellingUnitRetail
               ,loc.currency_code                        CurrencyCode
               ,il.selling_uom                           SellingUOM
               ,il.taxable_ind                           TaxableInd
               ,REPLACE(il.local_item_desc,CHR(10),'')   LocalItemDesc
               ,REPLACE(il.local_short_desc,CHR(10),'')  LocalShortDesc
               ,il.ti                                    Ti
               ,il.hi                                    Hi
               ,il.store_ord_mult                        StoreOrderMultiple
               ,il.status                                Status
               ,il.daily_waste_pct                       DailyWastePct
               ,il.meas_of_each                          MeasureOfEach
               ,il.meas_of_price                         MeasureOfPrice
               ,il.uom_of_price                          UomOfPrice
               ,il.primary_variant                       PrimaryVariant
               ,il.primary_cost_pack                     PrimaryCostPack
               ,il.primary_supp                          PrimarySupplier
               ,il.primary_cntry                         PrimaryOriginCountry
               ,il.receive_as_type                       ReceiveAsType
               ,il.inbound_handling_days                 InboundHandlingDays
               ,il.source_method                         SourceMethod
               ,il.source_wh                             SourceWh
               ,il.uin_type                              UinType
               ,il.uin_label                             UinLabel
               ,il.capture_time                          CaptureTimeInProc
               ,il.ext_uin_ind                           ExtUinInd
               ,il.ranged_ind                            IntentionallyRangedInd
               ,il.costing_loc                           CostingLocation
               ,il.costing_loc_type                      CostingLocType
               ,TO_CHAR(ilt.launch_date, 'DD-MON-YYYY')  LaunchDate
               ,ilt.qty_key_options                      QtyKeyOptions
               ,ilt.manual_price_entry                   ManualPriceEntry
               ,ilt.deposit_code                         DepositCode
               ,ilt.food_stamp_ind                       FoodStampInd
               ,ilt.wic_ind                              WicInd
               ,ilt.proportional_tare_pct                ProportionalTarePct
               ,ilt.fixed_tare_value                     FixedTareValue
               ,ilt.fixed_tare_uom                       FixedTareUom
               ,ilt.reward_eligible_ind                  RewardEligibleInd
               ,ilt.natl_brand_comp_item                 NatlBrandCompItem
               ,ilt.return_policy                        ReturnPolicy
               ,ilt.stop_sale_ind                        StopSaleInd
               ,ilt.elect_mtk_clubs                      ElectMtkClubs
               ,ilt.report_code                          ReportCode
               ,ilt.req_shelf_life_on_selection          ReqShelfLifeOnSelection
               ,ilt.req_shelf_life_on_receipt            ReqShelfLifeOnReceipt
               ,ilt.ib_shelf_life                        IBShelfLife
               ,ilt.store_reorderable_ind                StoreReorderableInd
               ,ilt.rack_size                            RackSize
               ,ilt.full_pallet_item                     FullPalletItem
               ,ilt.in_store_market_basket               InStoreMarketBasket
               ,ilt.storage_location                     StorageLocation
               ,ilt.alt_storage_location                 AltStorageLocation
               ,ilt.returnable_ind                       ReturnableInd
               ,ilt.refundable_ind                       RefundableInd
               ,ilt.back_order_ind                       BackOrderInd
               ,im.merchandise_ind                       MerchandiseInd
                           -- Select to get value from UDA - AssortmentMode
                           ,(select uda_value from uda_item_lov
                                  where item = il.item and uda_id = (select value_1 from xxadeo_mom_dvm
                                                                                           where func_area = 'UDA'
                                                                                           and parameter = 'ASSORTMENT_MODE'
                                                                                           and bu = trim(:GV_bu)))        AssortmentMode
                           -- This Package need 3 parameters I_entity / I_primary_key / I_cfa_id (ITEM_LOC / item and loc / select to get the cfa value needed). It is used to get CFA value from xxadeo_mom_dvm when PARAMETER is AVAILABILITY_ITEM_LOC
                           ,XXADEO_CFAS_UTILS.GET_CFA_VALUE_VARCHAR('ITEM_LOC',
                                                                                                                il.item || '|' || il.loc,
                                                                                                                (select value_1 from xxadeo_mom_dvm where func_area = 'CFA' and PARAMETER = 'AVAILABILITY_ITEM_LOC' and bu is null))       Availability
                           -- This Package need 3 parameters I_entity / I_primary_key / I_cfa_id (ITEM_LOC / item and loc / select to get the cfa value needed). It is used to get CFA value from xxadeo_mom_dvm when PARAMETER is VISIBILITY_ITEM_LOC
                           ,XXADEO_CFAS_UTILS.GET_CFA_VALUE_VARCHAR('ITEM_LOC',
                                                                                                                il.item || '|' || il.loc,
                                                                                                                (select value_1 from xxadeo_mom_dvm where func_area = 'CFA' and PARAMETER = 'VISIBILITY_ITEM_LOC' and bu is null))         Visibility
                           -- This select is necessary to get vat_code. To get this value correctly is it necessary compare real time with the active_date from vat_item.
                           ,(select vat_code from vat_item where vat_region = loc.vat_region and item = il.item and vat_type = 'B'
                                                                                           and active_date = (select max(active_date) from vat_item where vat_region = loc.vat_region
                                                                                                                                                                                    and item = il.item and vat_type = 'B' and active_date <= get_vdate()))                     VatCode
                           ,TO_CHAR(il.last_update_datetime, 'YYYYMMDD')     UpdateDate
                           -- This Package need 3 parameters I_entity / I_primary_key / I_cfa_id (ITEM_MASTER / item / select to get the cfa value needed). It is used to get CFA value from xxadeo_mom_dvm when PARAMETER is MODIFY_PRICE_POS
                           ,decode((XXADEO_CFAS_UTILS.GET_CFA_VALUE_VARCHAR('ITEM_MASTER',
                            il.item,
                            (select value_1 from xxadeo_mom_dvm where func_area = 'CFA' and PARAMETER = 'MODIFY_PRICE_POS' and bu = trim(:GV_bu)))), 'N', 0, 1)         ModifyPricePos
          from item_loc il,
               item_loc_traits ilt,
               item_master im,
               (select store  location,
                       currency_code currency_code,
                                           org_unit_id org_unit_id,
                       vat_region vat_region
                  from store
                 union
                select wh     location,
                       currency_code currency_code,
                                           org_unit_id org_unit_id,
                       vat_region
                  from wh
                 union
                select TO_NUMBER(partner_id) location,
                       currency_code,
                                           org_unit_id org_unit_id,
                                           vat_region
                  from partner
                 where partner_type = 'E') loc,
                                 xxadeo_bu_ou bo
         where il.item = im.item
           and im.sellable_ind = 'Y'
           and im.status = 'A'
           and il.item = ilt.item(+)
           and il.loc  = ilt.loc(+)
           and il.loc  = loc.location
           and loc.org_unit_id = bo.ou
                     and bo.bu = trim(:GV_bu)
           order by item,location) itemloc;
EOF
err=$?
   if [[ `grep "^ORA-" $ITEMSTORE | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $ITEMSTORE >> $ERRORFILE
      rm $ITEMSTORE
   else
          mv "${ITEMSTORE}" "${ITEMSTORE}".dat

      status=${OK}
   fi

return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_DELTA
# Purpose      : Export item location delta records
#-------------------------------------------------------------------------
function PROCESS_DELTA
{
bu=$1
bu_acronym=$2
process_id=$3

 #It is used the next name to the file generated: XXRMS210_BU_ACRONYM_EXTRACTDATE_DELTA
 echo "Extracting delta item location records for Business Units: ${bu}" >> $LOGFILE
 ITMSTRSTG=$DIR/XXRMS210_ITEMLOC_${bu_acronym}_${extractDate}

   # Always generate a file even when there are no records
   touch $ITMSTRSTG
   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$ITMSTRSTG
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   VARIABLE GV_bu NUMBER;
   VARIABLE GV_process_id NUMBER;

   EXEC :GV_bu := ${bu};
   EXEC :GV_process_id := ${process_id};

select Family                     ||'|'||
       Type                       ||'|'||
       LocType                    ||'|'||
       Location                   ||'|'||
       Item                       ||'|'||
       ItemParent                 ||'|'||
       ItemGrandparent            ||'|'||
       InitialUnitRetail          ||'|'||
       SellingUnitRetail          ||'|'||
       CurrencyCode               ||'|'||
       SellingUOM                 ||'|'||
       TaxableInd                 ||'|'||
       LocalItemDesc              ||'|'||
       LocalShortDesc             ||'|'||
       Ti                         ||'|'||
       Hi                         ||'|'||
       StoreOrderMultiple         ||'|'||
       Status                     ||'|'||
       DailyWastePct              ||'|'||
       MeasureOfEach              ||'|'||
       MeasureOfPrice             ||'|'||
       UomOfPrice                 ||'|'||
       PrimaryVariant             ||'|'||
       PrimaryCostPack            ||'|'||
       PrimarySupplier            ||'|'||
       PrimaryOriginCountry       ||'|'||
       ReceiveAsType              ||'|'||
       InboundHandlingDays        ||'|'||
       SourceMethod               ||'|'||
       SourceWh                   ||'|'||
       UinType                    ||'|'||
       UinLabel                   ||'|'||
       CaptureTimeInProc          ||'|'||
       ExtUinInd                  ||'|'||
       IntentionallyRangedInd     ||'|'||
       CostingLocation            ||'|'||
       CostingLocType             ||'|'||
       LaunchDate                 ||'|'||
       QtyKeyOptions              ||'|'||
       ManualPriceEntry           ||'|'||
       DepositCode                ||'|'||
       FoodStampInd               ||'|'||
       WicInd                     ||'|'||
       ProportionalTarePct        ||'|'||
       FixedTareValue             ||'|'||
       FixedTareUom               ||'|'||
       RewardEligibleInd          ||'|'||
       NatlBrandCompItem          ||'|'||
       ReturnPolicy               ||'|'||
       StopSaleInd                ||'|'||
       ElectMtkClubs              ||'|'||
       ReportCode                 ||'|'||
       ReqShelfLifeOnSelection    ||'|'||
       ReqShelfLifeOnReceipt      ||'|'||
       IBShelfLife                ||'|'||
       StoreReorderableInd        ||'|'||
       RackSize                   ||'|'||
       FullPalletItem             ||'|'||
       InStoreMarketBasket        ||'|'||
       StorageLocation            ||'|'||
       AltStorageLocation         ||'|'||
       ReturnableInd              ||'|'||
       RefundableInd              ||'|'||
       BackOrderInd               ||'|'||
       MerchandiseInd                       ||'|'||
            AssortmentMode                          ||'|'||
            Availability                                    ||'|'||
            Visibility                                 ||'|'||
            VatCode                                            ||'|'||
            UpdateDate                                 ||'|'||
            ModifyPricePos
  from
     ( select * from
      (  select 'ITEMLOC'                                Family
               ,upper(stg.action_type)                   Type
               ,stg.loc_type                             LocType
               ,stg.loc                                  Location
               ,stg.item                                 Item
               ,il.item_parent                           ItemParent
               ,il.item_grandparent                      ItemGrandparent
               ,DECODE(stg.action_type, 'itemloccre', il.unit_retail, NULL)           InitialUnitRetail
               ,DECODE(stg.action_type, 'itemloccre', il.selling_unit_retail, NULL)   SellingUnitRetail
               ,DECODE(stg.action_type, 'itemloccre', loc.currency_code, NULL)        CurrencyCode
               ,DECODE(stg.action_type, 'itemloccre', il.selling_uom, NULL)           SellingUOM
               ,il.taxable_ind                           TaxableInd
               ,REPLACE(il.local_item_desc,CHR(10),'')   LocalItemDesc
               ,REPLACE(il.local_short_desc,CHR(10),'')  LocalShortDesc
               ,il.ti                                    Ti
               ,il.hi                                    Hi
               ,il.store_ord_mult                        StoreOrderMultiple
               ,il.status                                Status
               ,il.daily_waste_pct                       DailyWastePct
               ,il.meas_of_each                          MeasureOfEach
               ,il.meas_of_price                         MeasureOfPrice
               ,il.uom_of_price                          UomOfPrice
               ,il.primary_variant                       PrimaryVariant
               ,il.primary_cost_pack                     PrimaryCostPack
               ,il.primary_supp                          PrimarySupplier
               ,il.primary_cntry                         PrimaryOriginCountry
               ,il.receive_as_type                       ReceiveAsType
               ,il.inbound_handling_days                 InboundHandlingDays
               ,il.source_method                         SourceMethod
               ,il.source_wh                             SourceWh
               ,il.uin_type                              UinType
               ,il.uin_label                             UinLabel
               ,il.capture_time                          CaptureTimeInProc
               ,il.ext_uin_ind                           ExtUinInd
               ,il.ranged_ind                            IntentionallyRangedInd
               ,il.costing_loc                           CostingLocation
               ,il.costing_loc_type                      CostingLocType
               ,TO_CHAR(ilt.launch_date, 'DD-MON-YYYY')  LaunchDate
               ,ilt.qty_key_options                      QtyKeyOptions
               ,ilt.manual_price_entry                   ManualPriceEntry
               ,ilt.deposit_code                         DepositCode
               ,ilt.food_stamp_ind                       FoodStampInd
               ,ilt.wic_ind                              WicInd
               ,ilt.proportional_tare_pct                ProportionalTarePct
               ,ilt.fixed_tare_value                     FixedTareValue
               ,ilt.fixed_tare_uom                       FixedTareUom
               ,ilt.reward_eligible_ind                  RewardEligibleInd
               ,ilt.natl_brand_comp_item                 NatlBrandCompItem
               ,ilt.return_policy                        ReturnPolicy
               ,ilt.stop_sale_ind                        StopSaleInd
               ,ilt.elect_mtk_clubs                      ElectMtkClubs
               ,ilt.report_code                          ReportCode
               ,ilt.req_shelf_life_on_selection          ReqShelfLifeOnSelection
               ,ilt.req_shelf_life_on_receipt            ReqShelfLifeOnReceipt
               ,ilt.ib_shelf_life                        IBShelfLife
               ,ilt.store_reorderable_ind                StoreReorderableInd
               ,ilt.rack_size                            RackSize
               ,ilt.full_pallet_item                     FullPalletItem
               ,ilt.in_store_market_basket               InStoreMarketBasket
               ,ilt.storage_location                     StorageLocation
               ,ilt.alt_storage_location                 AltStorageLocation
               ,ilt.returnable_ind                       ReturnableInd
               ,ilt.refundable_ind                       RefundableInd
               ,ilt.back_order_ind                       BackOrderInd
               ,stg.merchandise_ind                      MerchandiseInd
               ,stg.seq_no                               seq_no
                           -- Select to get value from UDA - AssortmentMode
                           ,(select uda_value from uda_item_lov
                                  where item = il.item and uda_id = (select value_1 from xxadeo_mom_dvm
                                                                                           where func_area = 'UDA'
                                                                                           and parameter = 'ASSORTMENT_MODE'
                                                                                           and bu = trim(:GV_bu)))        AssortmentMode
                                -- This Package need 3 parameters I_entity / I_entity / I_primary_key (ITEM_LOC / item and loc / select to get the cfa value needed). It is used to get CFA value from xxadeo_mom_dvm when PARAMETER is AVAILABILITY_ITEM_LOC
                                ,XXADEO_CFAS_UTILS.GET_CFA_VALUE_VARCHAR('ITEM_LOC',
                            il.item || '|' || il.loc,
                            (select value_1 from xxadeo_mom_dvm where func_area = 'CFA' and PARAMETER = 'AVAILABILITY_ITEM_LOC' and bu is null))       Availability
                                -- This Package need 3 parameters I_entity / I_entity / I_primary_key (ITEM_LOC / item and loc / select to get the cfa value needed). It is used to get CFA value from xxadeo_mom_dvm when PARAMETER is VISIBILITY_ITEM_LOC
                                ,XXADEO_CFAS_UTILS.GET_CFA_VALUE_VARCHAR('ITEM_LOC',
                            il.item || '|' || il.loc,
                            (select value_1 from xxadeo_mom_dvm where func_area = 'CFA' and PARAMETER = 'VISIBILITY_ITEM_LOC' and bu is null))         Visibility
                                -- This select is necessary to get vat_code. To get this value correctly is it necessary compare real time with the active_date from vat_item.
                                ,(select vat_code from vat_item where vat_region = loc.vat_region and item = il.item and vat_type = 'B'
                         and active_date = (select max(active_date) from vat_item where vat_region = loc.vat_region
                                                    and item = il.item and vat_type = 'B' and active_date <= get_vdate()))        VatCode
                                ,TO_CHAR(stg.transaction_datetime, 'YYYYMMDD')     UpdateDate
                                -- This Package need 3 parameters I_entity / I_entity / I_primary_key (ITEM_MASTER / item / select to get the cfa value needed). It is used to get CFA value from xxadeo_mom_dvm when PARAMETER is MODIFY_PRICE_POS
                                ,decode((XXADEO_CFAS_UTILS.GET_CFA_VALUE_VARCHAR('ITEM_MASTER',
                            il.item,
                            (select value_1 from xxadeo_mom_dvm where func_area = 'CFA' and PARAMETER = 'MODIFY_PRICE_POS' and bu = trim(:GV_bu)))), 'N', 0, 1)         ModifyPricePos
          from item_loc il,
               item_loc_traits ilt,
               item_export_stg stg,
               item_master im,
               (select store  location,
                       currency_code currency_code,
                                           org_unit_id org_unit_id,
                                           vat_region vat_region
                  from store
                 union
                select wh     location,
                       currency_code currency_code,
                                           org_unit_id org_unit_id,
                                           vat_region
                  from wh
                 union
                select TO_NUMBER(partner_id) location,
                       currency_code,
                                           org_unit_id org_unit_id,
                                           vat_region
                  from partner
                 where partner_type = 'E') loc,
                                 xxadeo_bu_ou bo
         where stg.item  = il.item(+)
           and stg.loc = il.loc(+)
           and stg.item  = ilt.item(+)
           and stg.loc = ilt.loc(+)
           and stg.loc = loc.location
           and loc.org_unit_id = bo.ou
           and bo.bu = trim(:GV_bu)
           and im.item = stg.item
           and im.item_level = 1
           and im.status = 'A'
           and stg.process_id = trim(:GV_process_id)
           and stg.action_type in ('itemloccre','itemlocmod','itemlocdel')
           and stg.base_extracted_ind = 'N') order by seq_no
       ) itemlocstg;
EOF
err=$?
   if [[ `grep "^ORA-" $ITMSTRSTG | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $ITMSTRSTG >> $ERRORFILE
      rm $ITMSTRSTG
   else
      mv "${ITMSTRSTG}" "${ITMSTRSTG}".dat

      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: INS_DATA_EXP_HIST
# Purpose : Insert into data_export_hist
#-------------------------------------------------------------------------
function INS_DATA_EXP_HIST
{
mode=$1
if [[ ${mode} = "full" ]]; then
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;
   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('itemloc',
                                'full',
                                 USER,
                                 SYSDATE);
    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`
fi
if [[ ${mode} = "delta" ]]; then
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;
   WHENEVER SQLERROR EXIT 1;
   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('itemloc',
                                'delta',
                                 USER,
                                 SYSDATE);
    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`
fi
if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#-------------------------------------------------------------------------
#                             MAIN
#-------------------------------------------------------------------------
if [[ $# -lt 1 || $# -gt 5 || $# -lt 3 ]]; then
   USAGE
   exit 1
fi

CONNECT=$1
mode=$2
concheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${CONNECT}`
constatus=$?

if [[ ${constatus} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${constatus}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "xxadeo_export_itemloc.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

current_process_id=`$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select export_process_id_seq.nextval
 from dual;
exit;
EOF`
process_id_status=$?
if [[ ${process_id_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "GETTING_PROCESS_ID" "${process_id_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
fi

### Check if no. of threads is passed as arg or not, else default.
if [[ $3 = "Y" ]]; then
   SLOTS=$4
   if [[ -z ${SLOTS} ]]; then
     SLOTS=10
   fi
   STR=$5
   echo $SLOTS | egrep '^[0-9]+$'>/dev/null 2>&1
   status=$?
   if [ "$status" -ne "0" ]
   then
     LOG_ERROR "Thread count should be a Positive Number" "NUM_THREADS" "${status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
     exit 1
   fi
   if [[ ${SLOTS} -gt 20 ]]; then
    SLOTS=10
   fi
else
   if [[ $3 = "N" ]]; then
   STR=$4
   SLOTS=10
   else
     LOG_ERROR "Thread number indicator either should be Y or N" "INDICATOR" "$3" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
     exit 1
   fi
fi

### if the execution parameter is full, it goes to CREATE_BU_LIST function to define the bu and then starts to print all records with PROCESS_FULL function
if [[ $2 = "full" ]]; then
  tbl="base"
  CREATE_BU_LIST
  crt_status=$?
  if [[ ${crt_status} -ne ${OK} ]]; then
     LOG_ERROR "ORA Error while fetching bu" "CREATE_BU_LIST" "${crt_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
     exit 1
  fi
  while read bu bu_acronym
   do
    if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
        (
                        PROCESS_FULL $bu $bu_acronym &
         )
        process_full_status=$?
echo "Status for BU ${bu} : ${process_full_status}" >> $LOGFILE
        if [[ ${process_full_status} -ne ${OK} ]]; then
                        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL" "${process_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                        exit 1
        fi
    else
       while [ `jobs | wc -l` -ge ${SLOTS} ]
       do
         sleep 1
       done
       (
                        PROCESS_FULL $bu $bu_acronym &
       )
       process_full_status=$?
echo "Status for BU ${bu} : ${process_full_status}" >> $LOGFILE
       if [[ ${process_full_status} -ne ${OK} ]]; then
                        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL" "${process_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                        exit 1
       fi
        fi
        done < $BU_LIST
        ### Wait for all of the threads to complete
    wait
    if [[ $? -eq ${OK} ]]; then
       INS_DATA_EXP_HIST ${mode}
       ins_dt_exp_st=$?
       if [[ ${ins_dt_exp_st} -ne ${OK} ]]; then
          LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "INS_DATA_EXP_HIST" "${ins_dt_exp_st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
          exit 1
       fi
           rm $BU_LIST
       LOG_MESSAGE "xxadeo_export_itemloc.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
    fi
fi

### if the execution parameter is delta, it goes to CREATE_BU_LIST function to define the bu and then starts to print all records with PROCESS_DELTA function
if [[ $2 = "delta" ]]; then
   UPDATE_ITEM_EXPORT_STG ${current_process_id}
   st=$?
   if [[ ${st} -ne ${OK} ]]; then
           LOG_ERROR "Error while updating item_export_stg with current process_id" "UPDATE_ITEM_EXPORT_STG" "${st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
   fi
   tbl="stg"
   CREATE_BU_LIST
   crt_status=$?
   if [[ ${crt_status} -ne ${OK} ]]; then
      LOG_ERROR "ORA Error while fetching store" "CREATE_BU_LIST" "${crt_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
      exit 1
   fi
   while read bu bu_acronym
    do
      if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
       (
                        PROCESS_DELTA $bu $bu_acronym $current_process_id
        ) &
       process_delta_status=$?
       if [[ ${process_delta_status} -ne ${OK} ]]; then
          LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA" "${process_delta_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                  exit 1
       fi
      else
         while [ `jobs | wc -l` -ge ${SLOTS} ]
         do
           sleep 1
         done
          (
                                PROCESS_DELTA $bu $bu_acronym $current_process_id
           ) &
          process_delta_status=$?
          if [[ ${process_delta_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA" "${process_delta_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
          fi
      fi
    done < $BU_LIST

    if [[ $? -eq ${OK} ]]; then
       INS_DATA_EXP_HIST ${mode}
       ins_dt_exp_st=$?
       if [[ ${ins_dt_exp_st} -ne ${OK} ]]; then
          LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "INS_DATA_EXP_HIST" "${ins_dt_exp_st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
          exit 1
       fi
       PROCESS_STG ${current_process_id}
       process_stg_status=$?
       if [[ ${process_stg_status} -ne ${OK} ]]; then
          LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" "${process_stg_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
          exit 1
       else
          rm $BU_LIST
          LOG_MESSAGE "xxadeo_export_itemloc.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
       fi
    fi
fi
