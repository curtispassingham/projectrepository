#--- Object file groupings.

XXADEO_MERCHHIER_DNLD_PC_OBJS = xxadeo_merchhier_dnld.o

XXADEO_COSTPRICE_DNLD_PC_OBJS = xxadeo_costprice_dnld.o

XXADEO_SUPSITES_DNLD_PC_OBJS = xxadeo_supsites_dnld.o

XXADEO_DEALS_DNLD_PC_OBJS = xxadeo_deals_dnld.o

XXADEO_PRICECHANGE_DNLD_PC_OBJS = xxadeo_pricechange_dnld.o

XXADEO_ECOTAX_DNLD_PC_OBJS = xxadeo_ecotax_dnld.o

XXADEO_ECOTAX_DNLD_PRE_PC_OBJS = xxadeo_ecotax_dnld_pre.o

XXADEO_ITEM_LFCY_PC_OBJS = xxadeo_item_lfcy.o

RMS_C_SRCS = \
   $(OCIROQ_C_OBJS:.o=.c)

RMS_PC_SRCS = \
   $(XXADEO_MERCHHIER_DNLD_PC_OBJS:.o=.pc) \
   $(XXADEO_COSTPRICE_DNLD_PC_OBJS:.o=.pc) \
   $(XXADEO_SUPSITES_DNLD_PC_OBJS:.o=.pc) \
   $(XXADEO_DEALS_DNLD_PC_OBJS:.o=.pc) \
   $(XXADEO_PRICECHANGE_DNLD_PC_OBJS:.o=.pc) \
   $(XXADEO_ECOTAX_DNLD_PC_OBJS:.o=.pc) \
   $(XXADEO_ECOTAX_DNLD_PRE_PC_OBJS:.o=.pc) \
   $(XXADEO_ITEM_LFCY_PC_OBJS:.o=.pc)

#--- Target groupings.

RMS_GEN_EXECUTABLES = \
   xxadeo_merchhier_dnld \
   xxadeo_costprice_dnld \
   xxadeo_supsites_dnld \
   xxadeo_deals_dnld \
   xxadeo_pricechange_dnld \
   xxadeo_ecotax_dnld \
   xxadeo_ecotax_dnld_pre \
   xxadeo_item_lfcy

RMS_EXECUTABLES= \
   $(RMS_GEN_EXECUTABLES)

RMS_LIBS =

RMS_INCLUDES =

#--- External systems

RDM_RMS_C_SRCS =

RDM_RMS_PC_SRCS =

RDM_RMS_GEN_EXECUTABLES =

RDM_RMS_EXECUTABLES = \
   $(RDM_RMS_GEN_EXECUTABLES)

RDM_RMS_LIBS =

RDM_RMS_INCLUDES =

#--- Put list of all targets for rms.mk here

RMS_TARGETS = \
   $(RMS_EXECUTABLES) \
   $(RMS_EXECUTABLES:=.lint) \
   $(RMS_C_SRCS:.c=.o) \
   $(RMS_PC_SRCS:.pc=.o) \
   $(RMS_PC_SRCS:.pc=.c) \
   $(RMS_LIBS) \
   $(RMS_INCLUDES) \
   $(RDM_RMS_EXECUTABLES) \
   $(RDM_RMS_EXECUTABLES:=.lint) \
   $(RDM_RMS_SRCS:.c=.o) \
   $(RDM_RMS_PC_SRCS:.pc=.o) \
   $(RDM_RMS_PC_SRCS:.pc=.c) \
   $(RDM_RMS_LIBS) \
   $(RDM_RMS_INCLUDES) \
   rms-all \
   rms \
   rms-ALL \
   rms-libs \
   rms-includes \
   rms-clobber \
   rms-libchange \
   rms-clean \
   rms-install \
   rms-lint \
   rms-depend \
   rdm-rms-all \
   rdm-rms \
   rdm-rms-ALL \
   rdm-rms-libs \
   rdm-rms-includes \
   rdm-rms-clobber \
   rdm-rms-libchange \
   rdm-rms-clean \
   rdm-rms-install \
   rdm-rms-lint \
   rdm-rms-depend \
   rms-rdm-all \
   rms-rdm \
   rms-rdm-ALL \
   rms-rdm-libs \
   rms-rdm-includes \
   rms-rdm-clobber \
   rms-rdm-libchange \
   rms-rdm-clean \
   rms-rdm-install \
   rms-rdm-lint \
   rms-rdm-depend
