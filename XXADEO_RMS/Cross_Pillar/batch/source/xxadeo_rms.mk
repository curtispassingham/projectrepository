include ${MMHOME}/oracle/lib/src/oracle.mk
PRODUCT_PROCFLAGS = 
PRODUCT_CFLAGS = 
PRODUCT_LDFLAGS = -L.
PRODUCT_LINT_FLAGS = 
include ${MMHOME}/oracle/lib/src/platform.mk
include ${MMHOME}/oracle/lib/src/rules.mk
include ${MMHOME}/oracle/proc/src/xxadeo_rms_include.mk

#--- Standard targets.

ALL: rms-ALL FORCE

all: rms-all FORCE

libs: rms-libs FORCE

iNCludes: rms-includes FORCE

libchange: rms-libchange rdm-rms-libchange FORCE

clean: rms-clean rdm-rms-clean FORCE

clobber: rms-clobber rdm-rms-clobber FORCE

install: rms-install rdm-rms-install FORCE

lint: rms-lint rdm-rms-lint FORCE

depend: rms-depend rdm-rms-depend

FORCE:

#--- RMS specific

rms: rms-all FORCE

rms-ALL: rms-libs $(RMS_EXECUTABLES) $(RDM_RMS_EXECUTABLES) FORCE

rms-all: rms-libs $(RMS_GEN_EXECUTABLES) FORCE

rms-libs: rms-includes $(RMS_LIBS) FORCE

rms-includes: $(RMS_INCLUDES) FORCE

rms-libchange: FORCE
	rm -f $(RMS_EXECUTABLES)

rms-clean: FORCE
	rm -f $(RMS_C_SRCS:.c=.o)
	rm -f $(RMS_PC_SRCS:.pc=.o)
	rm -f $(RMS_PC_SRCS:.pc=.c)
	rm -f $(RMS_PC_SRCS:.pc=.lis)
	rm -f $(RMS_EXECUTABLES:=.lint)
	RMS_LIBS="$(RMS_LIBS)"; \
	for f in $$RMS_LIBS; \
	do \
		n=llib-l`expr "$$f" : 'lib\(.*\)\.a' \| "$$f" : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		rm -f $$n; \
	done

rms-clobber: rms-libchange rms-clean FORCE
	rm -f $(RMS_LIBS)
	rm -f $(RMS_INCLUDES)

rms-install: FORCE
	-@RMS_EXECUTABLES="$(RMS_EXECUTABLES)"; \
	for f in $$RMS_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_PROC_BIN)/$$f ] || [ $$f -nt $(RETEK_PROC_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_PROC_BIN)/$$f"; \
				cp $$f $(RETEK_PROC_BIN)/$$f; \
				chmod $(MASK) $(RETEK_PROC_BIN)/$$f; \
			fi; \
		fi; \
	done

rms-lint: FORCE
	-@RMS_EXECUTABLES="$(RMS_EXECUTABLES)"; \
	for e in $$RMS_EXECUTABLES; \
	do \
		$(MAKE) -f ${MMHOME}/oracle/proc/src/rms.mk $$e.lint; \
	done


rms-depend: rms.d

rms.d: $(RMS_C_SRCS) $(RMS_PC_SRCS)
	@echo "Making dependencies $@..."
	@$(MAKEDEPEND) $(RMS_C_SRCS) $(RMS_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`

#--- External systems

rdm-rms: rdm-rms-libs $(RDM_RMS_GEN_EXECUTABLES) FORCE

rdm-rms-ALL: rdm-rms-libs $(RDM_RMS_EXECUTABLES) FORCE

rdm-rms-all: rdm-rms FORCE

rdm-rms-libs: rdm-rms-includes $(RDM_RMS_LIBS) FORCE

rdm-rms-includes: $(RDM_RMS_INCLUDES) FORCE

rdm-rms-libchange: FORCE
	rm -f $(RDM_RMS_EXECUTABLES)

rdm-rms-clean: FORCE
	rm -f $(RDM_RMS_C_SRCS:.c=.o)
	rm -f $(RDM_RMS_PC_SRCS:.pc=.o)
	rm -f $(RDM_RMS_PC_SRCS:.pc=.c)
	rm -f $(RDM_RMS_PC_SRCS:.pc=.lis)
	rm -f $(RDM_RMS_EXECUTABLES:=.lint)
	RMS_LIBS="$(RDM_RMS_LIBS)"; \
	for f in $$RDM_RMS_LIBS; \
	do \
		n=llib-l`expr $$f : 'lib\(.*\).a' \| $$f : 'lib\(.*\).$(SHARED_SUFFIX)'`.ln; \
		rm -f $$n; \
	done

rdm-rms-clobber: rdm-rms-libchange rdm-rms-clean FORCE
	rm -f $(RDM_RMS_LIBS)
	rm -f $(RDM_RMS_INCLUDES)

rdm-rms-install: FORCE
	-@RDM_RMS_EXECUTABLES="$(RDM_RMS_EXECUTABLES)"; \
	for f in $$RDM_RMS_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_PROC_BIN)/$$f ] || [ $$f -nt $(RETEK_PROC_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_PROC_BIN)/$$f"; \
				cp $$f $(RETEK_PROC_BIN)/$$f; \
				chmod $(MASK) $(RETEK_PROC_BIN)/$$f; \
			fi; \
		fi; \
	done

rdm-rms-lint: FORCE
	-@RDM_RMS_EXECUTABLES="$(RDM_RMS_EXECUTABLES)"; \
	for e in $$RDM_RMS_EXECUTABLES; \
	do \
		$(MAKE) -f ${MMHOME}/oracle/proc/src/rms.mk $$e.lint; \
	done


rdm-rms-depend: rdm-rms.d

rdm-rms.d: $(RDM_RMS_C_SRCS) $(RDM_RMS_PC_SRCS)
	@echo "Making dependencies $@..."
	@$(MAKEDEPEND) $(RDM_RMS_C_SRCS) $(RDM_RMS_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`


rms-rdm: rdm-rms FORCE

rms-rdm-ALL: rdm-rms-ALL FORCE

rms-rdm-all: rdm-rms-all FORCE

rms-rdm-libs: rdm-rms-libs FORCE

rms-rdm-includes: rdm-rms-includes FORCE

rms-rdm-libchange: rdm-rms-libchange FORCE

rms-rdm-clean: rdm-rms-clean FORCE

rms-rdm-clobber: rdm-rms-clobber FORCE

rms-rdm-install: rdm-rms-install FORCE

rms-rdm-lint: rdm-rms-lint FORCE

rms-rdm-depend: rdm-rms-depend

rms-rdm.d: rdm-rms.d

#--- Executable targets.

xxadeo_merchhier_dnld: $(XXADEO_MERCHHIER_DNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_MERCHHIER_DNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

xxadeo_costprice_dnld: $(XXADEO_COSTPRICE_DNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_COSTPRICE_DNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

xxadeo_supsites_dnld: $(XXADEO_SUPSITES_DNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_SUPSITES_DNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

xxadeo_deals_dnld: $(XXADEO_DEALS_DNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_DEALS_DNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

xxadeo_pricechange_dnld: $(XXADEO_PRICECHANGE_DNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_PRICECHANGE_DNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

xxadeo_ecotax_dnld_pre: $(XXADEO_ECOTAX_DNLD_PRE_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_ECOTAX_DNLD_PRE_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

xxadeo_ecotax_dnld: $(XXADEO_ECOTAX_DNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_ECOTAX_DNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

xxadeo_item_lfcy: $(XXADEO_ITEM_LFCY_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_ITEM_LFCY_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

#--- Executable lint targets.
xxadeo_merchhier_dnld.lint: $(XXADEO_MERCHHIER_DNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(XXADEO_MERCHHIER_DNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

xxadeo_costprice_dnld.lint: $(XXADEO_COSTPRICE_DNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(XXADEO_COSTPRICE_DNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

xxadeo_supsites_dnld.lint: $(XXADEO_SUPSITES_DNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(XXADEO_SUPSITES_DNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

xxadeo_deals_dnld.lint: $(XXADEO_DEALS_DNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(XXADEO_DEALS_DNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

xxadeo_pricechange_dnld.lint: $(XXADEO_PRICECHANGE_DNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(XXADEO_PRICECHANGE_DNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

xxadeo_ecotax_dnld_pre.lint: $(XXADEO_ECOTAX_DNLD_PRE_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(XXADEO_ECOTAX_DNLD_PRE_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@` 

xxadeo_ecotax_dnld.lint: $(XXADEO_ECOTAX_DNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(XXADEO_ECOTAX_DNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

xxadeo_item_lfcy.lint: $(XXADEO_ITEM_LFCY_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(XXADEO_ITEM_LFCY_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

#--- Library targets.

#--- Include targets.

#--- Object dependencies.

include rms.d
include rdm-rms.d
