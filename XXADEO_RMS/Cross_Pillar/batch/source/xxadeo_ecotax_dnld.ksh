#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  batch_ecotax.ksh
#
#  Desc:  UNIX shell script to run the Eco-Taxes program multi-threaded.
#-------------------------------------------------------------------------

# Common functions and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='xxadeo_ecotax_dnld.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

LOGFILE="${MMHOME}/log/$exeDate.log"
LOGDIR="${MMHOME}/log"
ERRORFILE="${MMHOME}/error/${pgmName}_${exeDate}.err"

# Initialize number of parallel threads
SLOTS=0
PURGE=0

function USAGE
{
	echo "USAGE: `basename $0` <connect> <optionnal : retention days>\n
<connect> is the connect string, most of the time will be $UP \n
<retention days> is the number of days for the purge. It is optionnal. \n 
"
}

function PREPROCESS 
{

return_message=${OK}
for i in {1..$SLOTS}
do
	{
		$RETAIL_HOME/oracle/proc/bin/xxadeo_ecotax_dnld $UP PRE 
		if [ $? -ne ${OK} ]; then 
			return_message=${FATAL}
		fi
	} &
done
wait

return return_message
}

function EXTRACT
{

return_message=${OK}
for i in {1..$SLOTS}
do
	{
		$RETAIL_HOME/oracle/proc/bin/xxadeo_ecotax_dnld $UP EXT 
		if [ $? -ne ${OK} ]; then 
			return_message=${FATAL}
		fi
	} &
done
wait

return return_message
}
#####################################################################################
#									MAIN											#
#####################################################################################


if [ $# -lt 1 ]; then
	USAGE
	exit ${NON_FATAL}
else
	LOG_MESSAGE "Starting wrapper program for Eco-Taxes extraction" "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	CONNECT=$1
	
	if [ $# = 2 ]; then
		PURGE = $2
		$RETAIL_HOME/oracle/proc/bin/xxadeo_ecotax_dnld_pre $CONNECT $PURGE
	else
		$RETAIL_HOME/oracle/proc/bin/xxadeo_ecotax_dnld_pre $CONNECT
  	fi
fi

if [ $? -ne $OK ]; then
	LOG_ERROR "Error during the preparation of the Threads" "MAIN" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
	exit ${FATAL}
fi
LOG_MESSAGE "Preparation of the Threads ended successfully" "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

SLOTS=`$ORACLE_HOME/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select num_threads
  from restart_control
 where program_name = 'xxadeo_ecotax_dnld';
EOF`


LOG_MESSAGE "---   There are $SLOTS threads for the execution of this program   ---" "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}


LOG_MESSAGE "Starting multi-threading for enrichment of the staging table..." "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

PREPROCESS

if [ $? -ne $OK ]; then
	LOG_ERROR "An error occured during enrichment." "MAIN" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
	exit ${FATAL}
fi

LOG_MESSAGE "Enrichment of the staging table done!" "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
# If everything worked, then go to the Extraction part, but first restart the threads.

LOG_MESSAGE "Updating the status of threads for next step ..." "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}


update=`$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
set pause off
set pagesize 0
set feedback off
set verify off
set heading off
set echo off
update restart_program_status
   set program_status = 'ready for start'
 where program_name = 'xxadeo_ecotax_dnld';
COMMIT;
exit;
EOF`

LOG_MESSAGE "Update done!" "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}


LOG_MESSAGE "Starting multi-threading for extraction of the Eco-Taxes..." "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
EXTRACT

if [ $? -ne $OK ]; then
	exit ${FATAL}
fi

exit ${OK}

