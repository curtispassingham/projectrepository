#! /bin/ksh
#-------------------------------------------------------------------------------------
#  File:  XXADEO_ITEM_EXPENSES_IN.ksh
#
#  Desc:  

#-------------------------------------------------------------------------------------

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib


pgmName='xxadeo_item_expenses_in.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=$(date +"%Y%m%d%H%M%S")             # get the execution date
DIR=`pwd`

# File locations
CTLFILE_ZONE="${MMHOME}/oracle/proc/bin/xxadeo_item_expenses_zone.ctl"
CTLFILE_COUNTRY="${MMHOME}/oracle/proc/bin/xxadeo_item_expenses_country.ctl"
LOGDIR="${MMHOME}/log"
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.XXRMS332_ITEMEXP_UPLD_"$exeDate

# Initialize variables and constants
inputfile_ext=0
REJECT="R"

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "
   USAGE: $pgmName <connect string> <process mode> <thread flag or number days> <number threads>
	<connect string>   Username/password@db.
	
	<process mode>     Required parameter. Valid process modes are:
		LOAD    - Upload item expenses information from input file to staging tables, enrich information and validate.
		PROCESS - Process item expenses information.
		PURGE   - Delete item expenses information in the staging tables that is older than the retention date.
	
	<thread flag>      Required only for 'PROCESS' mode.  Can be "Y" or "N"
	
	<number days>   Required only for 'PURGE' mode. Processed records older than this amount of days will be purged.
	
	<number threads> Required only for 'PROCESS' mode.  Number of threads
"
}

#-------------------------------------------------------------------------
# Function Name: PROCESS
# Purpose      : 
#-------------------------------------------------------------------------
function PROCESS
{
 LOG_MESSAGE "Process records for department ${1} " "PROCESS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
 sqlTxt="
 DECLARE
    O_status_code    VARCHAR2(400);
    O_error_message  RTK_ERRORS.RTK_TEXT%TYPE;
 BEGIN
	
 	XXADEO_ITEM_EXPENSES.PROCESS_RECORDS(${1}, O_status_code, O_error_message);

	if O_status_code != 'S' then
		rollback;
        :GV_script_error := O_error_message;
        :GV_return_code := ${FATAL};

    else
		COMMIT;
    end if;	
        
	EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := O_error_message;
            :GV_return_code := ${FATAL};
      END;"
   

	EXEC_SQL ${sqlTxt}
  
	if [[ $? -ne ${OK} ]]; then
		LOG_MESSAGE "Fatal error processing records for dept ${1}" "PROCESS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
		exit ${FATAL}
	else
		LOG_MESSAGE "Successfully process records for dept ${1}" "PROCESS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}	
	fi

}


#-------------------------------------------------------------------------
# Function Name: LOAD_FILE
# Purpose      : SQL Load the input file into the staging table enrich and validate
#-------------------------------------------------------------------------
function LOAD_FILE
{


if ls ${MMHOME}/oracle/proc/bin/XXRMS332_ITEMEXP_ZONE_*.dat  1> /dev/null 2>&1; then

	 for file in "${MMHOME}/oracle/proc/bin/XXRMS332_ITEMEXP_ZONE_*.dat";
	 do 		
	   echo "Loading Item expenses at zone level data into the tables ..."
	   
	   dtStamp=`date +"%G%m%d%H%M%S"`
	   sqlldrFile='XXRMS332_ITEMEXP_ZONE'  
	   sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
	   sqlldrDsc=${MMHOME}/log/$sqlldrFile.dsc   
	   sqlldrBad=${MMHOME}/log/$sqlldrFile.bad
	   sqlldr ${connectStr} silent=feedback,header \
		  control=${CTLFILE_ZONE} \
		  log=${sqlldrLog} \
		  data=${file} \
		  bad=${sqlldrBad} \
		  discard=${sqlldrDsc} 
	   sqlldr_status=$?	
		  
	   # Check execution status
	   if [[ $sqlldr_status != ${OK} ]]; then
	   
		  # Check if execution status is a warning
		  if [[ $sqlldr_status = 2 ]]; then
			 LOG_MESSAGE "${inputFileNoPath} - Some rows not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName}
			
		  else
			 LOG_ERROR "Error while loading file ${file}. See ${sqlldrLog} for details." "LOAD_FILE" ${FATAL} ${ERRORFILE} ${pgmName}
			 exit ${FATAL}
		  fi
	   else
		  # Check log file for sql loader errors
		  if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` != ""
			 && `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` != "" ]];
		  then
			 LOG_MESSAGE "${inputFileNoPath} - Completed loading file to staging table." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName}
		  else
			 LOG_MESSAGE "${inputFileNoPath} - Some rows not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName}
			 
		  fi
	   fi
	  done;
	  
	UPDATE_PROCESS_ID;
	LOG_MESSAGE "Update process id finished " "LOAD" ${OK} ${LOGFILE} ${pgmName}

	if [[ $? -ne ${OK} ]]; then
		exit ${FATAL}
	fi
	
	ENRICH_AND_VALIDATE; 
	LOG_MESSAGE "Enrich and validate finished " "LOAD" ${OK} ${LOGFILE} ${pgmName}

	if [[ $? -ne ${OK} ]]; then
		exit ${FATAL}
	fi
  
else
	echo "Not exist file with Item expenses at zone level ..."
fi
 

if ls ${MMHOME}/oracle/proc/bin/XXRMS332_ITEMEXP_COUNTRY_*.dat  1> /dev/null 2>&1; then
 
	for file in "${MMHOME}/oracle/proc/bin/XXRMS332_ITEMEXP_COUNTRY_*.dat";
	do 
	 echo "Loading Item expenses at country level data into the tables ..."

	   sqlldrFile='XXRMS332_ITEMEXP_COUNTRY'
	   dtStamp=`date +"%G%m%d%H%M%S"`
	   sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
	   sqlldrDsc=${MMHOME}/log/$sqlldrFile.dsc
	   sqlldrBad=${MMHOME}/log/$sqlldrFile.bad
	   
	   sqlldr ${connectStr} silent=feedback,header \
		  control=${CTLFILE_COUNTRY} \
		  log=${sqlldrLog} \
		  data=${file} \
		  bad=${sqlldrBad} \
		  discard=${sqlldrDsc} 
	   sqlldr_status=$?

	   
	   # Check execution status
	   if [[ $sqlldr_status != ${OK} ]]; then
	   
		  # Check if execution status is a warning
		  if [[ $sqlldr_status = 2 ]]; then
			 LOG_MESSAGE "${inputFileNoPath} - Some rows not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName}
			 
		  else
			 LOG_ERROR "Error while loading file ${file}. See ${sqlldrLog} for details." "LOAD_FILE" ${FATAL} ${ERRORFILE} ${pgmName}
			 exit ${FATAL}
		  fi
	   else
		  # Check log file for sql loader errors
		  if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` != ""
			 && `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` != "" ]];
		  then
			 LOG_MESSAGE "${inputFileNoPath} - Completed loading file to staging table." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName}
		  else
			 LOG_MESSAGE "${inputFileNoPath} - Some rows not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName}
			
		  fi
	   fi

	done;
	
	UPDATE_PROCESS_ID;	
	LOG_MESSAGE "Update process id finished " "LOAD" ${OK} ${LOGFILE} ${pgmName} 
	
	if [[ $? -ne ${OK} ]]; then
		exit ${FATAL}
	fi
	
	ENRICH_AND_VALIDATE; 
	LOG_MESSAGE "Enrich and validate finished " "LOAD" ${OK} ${LOGFILE} ${pgmName} 
	
	if [[ $? -ne ${OK} ]]; then
		exit ${FATAL}
	fi
else
	echo "Not exist file with Item expenses at country level ..."
fi 

return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Wrapper for PL/SQL package calls
#-------------------------------------------------------------------------
function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}


#-------------------------------------------------------------------------
# Function Name: PURGE
# Purpose      : Purge records published 
#-------------------------------------------------------------------------

## update process_id in stg
function UPDATE_PROCESS_ID
{
	batch_id=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
	set pause off
	set pagesize 0
	set feedback off
	set verify off
	set heading off
	set echo off
	select nvl(max(process_id),1)
	  from XXADEO_STG_ITEM_EXP_IN;
	exit;
EOF`
	
	let batch_id+=1
	
	update=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
	set pause off
	set pagesize 0
	set feedback off
	set verify off
	set heading off
	set echo off
	update XXADEO_STG_ITEM_EXP_IN
	   set process_id=${batch_id}
	 where process_id is null;
	COMMIT;
 	exit;
EOF`
}

#-------------------------------------------------------------------------
# Function Name: ENRICH_AND_VALIDATE
# Purpose      : Purge records published 
#-------------------------------------------------------------------------


function ENRICH_AND_VALIDATE
{
	 sqlTxt="	
	BEGIN
		XXADEO_ITEM_EXPENSES.ENRICH_AND_VALIDATE;
		
	    COMMIT;
	EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   
  EXEC_SQL ${sqlTxt}
  
  if [[ $? -ne ${OK} ]]; then
		exit ${FATAL}
	else
		LOG_MESSAGE "Successfully enrich and validate " "ENRICH_AND_VALIDATE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
		
	fi
}


#-------------------------------------------------------------------------
# Function Name: PURGE
# Purpose      : Purge records published 
#-------------------------------------------------------------------------

function PURGE
{
 LOG_MESSAGE "Purge initialized" "PURGE" ${OK} ${LOGFILE} ${pgmName}

	sqlTxt="
	BEGIN
	  delete from xxadeo_stg_item_exp_in
		where pub_status = 'P'
			and sysdate - last_update_datetime >= ${ndays} ;
		
	   COMMIT;		
	EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   
  EXEC_SQL ${sqlTxt}
 
   
  if [[ $? -ne ${OK} ]]; then
      echo "Function PURGE failed" >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE " Successfully purged records for XXADEO_STG_ITEM_EXPENSE." "PURGE" ${OK} ${LOGFILE} ${pgmName}
   fi

   return ${OK}
}



#-------------------------------------------------------------------------
# Function: CREATE_BU_LIST
# Desc: Write Business Units to a FILE
#-------------------------------------------------------------------------
function CREATE_DEPT_LIST
{
	x=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
	SET SERVEROUT ON SIZE 100000
	SET HEAD OFF
	SET FEEDBACK OFF
	  select count(*) from xxadeo_stg_item_exp_in where pub_status = 'R';
	exit;
EOF`

	zero=0
	if [[  ${zero}!=${x} ]] ; then

		DEPT_LIST=$DIR/DEPT_LIST_${extractDate}.$$
$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF >$DEPT_LIST
		   set pause off
		   set echo off
		   set heading off
		   set feedback off
		   set verify off
		   set linesize 1000;
		   set pages 0
		   
		   select distinct to_char(dept) dept
		   from item_master 
		   where item in
				(select item 
				from xxadeo_stg_item_exp_in 
				where pub_status = 'R');
		exit;
EOF
		
	

		if [[ `grep "^ORA-" $DEPT_LIST | wc -l` -gt 0 ]]; then
		   cat $DEPT_LIST >> ${ERRORFILE}
		   status=${FATAL}
		   rm $DEPT_LIST 
		   return $status
		else 
		 status=${OK}
		 return $status
		fi
		
		status=${FATAL}
		return $status


	fi

	return $status
}



#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for valid input arguments
	if [[ ( $# -lt 2 ) ]];
	then
	   USAGE
	   exit ${NON_FATAL}
	fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
USER=${connectStr%/*}

	concheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
	constatus=$?

	if [[ ${constatus} -ne ${OK} ]]; then
	   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${constatus}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
	   exit 1
	else
	   LOG_MESSAGE "xxadeo_item_expense_in.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
	fi


	if [[ ($2 = "PURGE") || ($2 = "purge") ]]; then
		if [[ $3 == +([0-9]) ]]; then	
			ndays=$3
		else 
			LOG_ERROR "Invalid parameter for Purge mode. Third parameter must be an integer." "MAIN" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
			USAGE
			exit ${FATAL}
		fi
	fi

	if [[ ($2 = "LOAD") || ($2 = "load") ]]; then
		 
		 # Load the external data staging table
		 LOAD_FILE ;
		
		 process_load=$?
		 
		if [[ ${process_load} -ne ${OK} ]]; then
			LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "LOAD" "${process_load}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
			exit 1
		else 
			LOG_MESSAGE "xxadeo_item_expenses:in.ksh load finished" "MAIN function" ${OK} ${LOGFILE} ${pgmName}
			exit ${OK}
		fi
		 
	fi

	if [[ ($2 = "PURGE") || ($2 = "purge") ]]; then
		ndays=$3

		PURGE $ndays
		
		process_purge=$?		 
		if [[ ${process_purge} -ne ${OK} ]]; then
			LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "LOAD" "${process_purge}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
			exit 1
		else 
			LOG_MESSAGE "xxadeo_item_expenses_in.ksh purge finished" "MAIN function" ${OK} ${LOGFILE} ${pgmName}
			exit ${OK}
		fi

	fi


	if [[ ($2 = "PROCESS") || ($2 = "process") ]]; then
		#process 
		
		### Check if no. of threads is passed as arg or not, else default.
		if [[ $3 = "Y" ]]; then
		   SLOTS=$4
		   if [[ -z ${SLOTS} ]]; then
			 SLOTS=10
		   fi
		   STR=$5
		   echo $SLOTS | egrep '^[0-9]+$'>/dev/null 2>&1
		   status=$?
		   if [ "$status" -ne "0" ]
		   then
			 LOG_ERROR "Thread count should be a Positive Number" "NUM_THREADS" "${status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
			 exit 1
		   fi
		   if [[ ${SLOTS} -gt 20 ]]; then
			SLOTS=10
		   fi
		else
		   if [[ $3 = "N" ]]; then
		   STR=$4
		   SLOTS=10
		   else
			 LOG_ERROR "Thread number indicator either should be Y or N" "INDICATOR" "$3" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
			 exit 1
		   fi
		fi
     
	    LOG_MESSAGE "Create department list" "MAIN function" ${OK} ${LOGFILE} ${pgmName}
		CREATE_DEPT_LIST 
		crt_status=$?
		  if [[ ${crt_status} -ne ${OK} ]]; then
			 LOG_ERROR "ORA Error while fetching dept" "CREATE_DEPT_LIST" "${crt_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
			 exit 1
		  fi
		  LOG_MESSAGE "Department list created with sucess" "MAIN function" ${OK} ${LOGFILE} ${pgmName}
		  
		if [[ -f ${DEPT_LIST}  ]]; then 
		  
			while read dept
			do

				if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
					( 
						PROCESS $dept   
					 ) &
					process_status=$?
					echo "Status for Dept ${dept} : ${process_status}"  >> $LOGFILE
					if [[ ${process_status} -ne ${OK} ]]; then
						LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS" "${process_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
						exit 1
					fi
				else
				
				   while [ `jobs | wc -l` -ge ${SLOTS} ]
				   do
					 sleep 1
				   done
				   ( 			
					PROCESS $dept  
				   )&
				   process_status=$?
				   echo "Status for Dept ${dept} : ${process_status}" >> $LOGFILE
				   if [[ ${process_status} -ne ${OK} ]]; then
						LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS" "${process_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
						exit 1
				   fi
				fi
				
			done < $DEPT_LIST
			
			### Wait for all of the threads to complete
			wait
			if [[ $? -eq ${OK} ]]; then
			   rm $DEPT_LIST
			  LOG_MESSAGE "xxadeo_item_expenses.ksh process finished" "MAIN function" ${OK} ${LOGFILE} ${pgmName}
			 exit ${OK}
			
			fi
				
		fi

		LOG_MESSAGE "xxadeo_item_expenses.ksh process finished" "MAIN function" ${OK} ${LOGFILE} ${pgmName}
		exit ${OK}
		
	fi

 
