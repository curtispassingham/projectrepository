#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  XXADEO_refreshauditsuppliertable.ksh
#
#  Desc:  UNIX shell script to update the XXADEO_RR409 table 
#  
#-------------------------------------------------------------------------
. ${RETAIL_HOME}/oracle/lib/src/rmsksh.lib

pgmName='XXADEO_refreshauditsuppliertable.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

LOGFILE="${RETAIL_HOME}/log/$exeDate.log"
ERRORFILE="${RETAIL_HOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind

CURR_THREADS=1
OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> &

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

"
}

#-------------------------------------------------------------------------
# Function Name: REFRESH_RR409_AUDIT_TABLE
# Purpose:       refresh the RR409 table
#-------------------------------------------------------------------------

function REFRESH_RR409_AUDIT_TABLE
{

     sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      
      WHENEVER SQLERROR EXIT ${FATAL}
         declare
         retour boolean;
         O_error_message varchar(240);
         p_date_fin varchar2(19)   := to_char(sysdate -1, 'DD-MM-YYYY');
  
         begin
         retour := XXADEO_RMS.XXADEO_RR409_SQL.XXADEO_RR409_DEL (O_error_message,
                                                      user
                                                      );
         retour := XXADEO_RMS.XXADEO_RR409_SQL.XXADEO_RR409_INS (O_error_message,
                                                      LANGUAGE_SQL.GET_USER_LANGUAGE(),
                                                      p_date_fin
                                                      );
         end;      
      /
      
      print :GV_script_error;
      exit  :GV_return_code;
     "  | sqlplus -s ${CONNECT} `
    
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "REFRESH_RR409_AUDIT_TABLE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${FATAL}
   fi

   return ${OK}
}

#-----------------------------------------------
# Main program starts 
# Parse the command line
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   USAGE
   exit 1
fi


#Validate input parameter
CONNECT=$1
if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "XXADEO_refreshauditsuppliertable.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

REFRESH_RR409_AUDIT_TABLE 
   refresh_st=$?
   if [[ ${refresh_st} -ne ${OK} ]]; then
      LOG_ERROR "Error while while refreshing RR409 table" "REFRESH_RR409_AUDIT_TABLE"  "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
      exit 1
   fi
