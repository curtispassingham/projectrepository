package com.adeo.merchandising.storeportal.model.common;

import com.storeportal.common.model.dbwrappers.manual.XXADEO_SP_RMS_WRP;

import java.sql.SQLException;

import oracle.retail.apps.rms.common.model.adfbc.RmsApplicationModuleImpl;

public class SPApplicationModuleImpl extends RmsApplicationModuleImpl {
    public SPApplicationModuleImpl() {
        super();
    }

    
    public String method() throws SQLException {
        
      return  XXADEO_SP_RMS_WRP.GET_SYSTEM_OPTIONS(getDBTransaction()).getO_SYSTEM_OPTIONS().getHelpUrl();
    }

}