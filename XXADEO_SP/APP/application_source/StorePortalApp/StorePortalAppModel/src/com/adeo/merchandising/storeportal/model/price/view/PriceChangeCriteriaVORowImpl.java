package com.adeo.merchandising.storeportal.model.price.view;

import com.adeo.merchandising.storeportal.model.common.SPViewRowImpl;

import java.sql.Date;

import oracle.jbo.RowSet;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Jul 11 10:15:44 BST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PriceChangeCriteriaVORowImpl extends SPViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    protected enum AttributesEnum {
        GroupId,
        Dept,
        ClassId,
        SubClass,
        Item,
        EAN,
        ItemDesc,
        ItemType,
        Typology,
        SubTypology,
        IsPack,
        SupplierSite,
        ReasonCode,
        ApplicationStartDate,
        ApplicationEndDate,
        GroupLOV,
        DeptLOV,
        ClassLOV,
        SubclassLOV,
        CodeDetailVOItemType,
        UdaLOVTypology,
        UdaLOVSubTypology,
        ReasonCodeLOV,
        SupplierSiteLOV,
        CodeDetailVOIsPack;
        private static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        protected int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        protected static final int firstIndex() {
            return firstIndex;
        }

        protected static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        protected static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int GROUPID = AttributesEnum.GroupId.index();
    public static final int DEPT = AttributesEnum.Dept.index();
    public static final int CLASSID = AttributesEnum.ClassId.index();
    public static final int SUBCLASS = AttributesEnum.SubClass.index();
    public static final int ITEM = AttributesEnum.Item.index();
    public static final int EAN = AttributesEnum.EAN.index();
    public static final int ITEMDESC = AttributesEnum.ItemDesc.index();
    public static final int ITEMTYPE = AttributesEnum.ItemType.index();
    public static final int TYPOLOGY = AttributesEnum.Typology.index();
    public static final int SUBTYPOLOGY = AttributesEnum.SubTypology.index();
    public static final int ISPACK = AttributesEnum.IsPack.index();
    public static final int SUPPLIERSITE = AttributesEnum.SupplierSite.index();
    public static final int REASONCODE = AttributesEnum.ReasonCode.index();
    public static final int APPLICATIONSTARTDATE = AttributesEnum.ApplicationStartDate.index();
    public static final int APPLICATIONENDDATE = AttributesEnum.ApplicationEndDate.index();
    public static final int GROUPLOV = AttributesEnum.GroupLOV.index();
    public static final int DEPTLOV = AttributesEnum.DeptLOV.index();
    public static final int CLASSLOV = AttributesEnum.ClassLOV.index();
    public static final int SUBCLASSLOV = AttributesEnum.SubclassLOV.index();
    public static final int CODEDETAILVOITEMTYPE = AttributesEnum.CodeDetailVOItemType.index();
    public static final int UDALOVTYPOLOGY = AttributesEnum.UdaLOVTypology.index();
    public static final int UDALOVSUBTYPOLOGY = AttributesEnum.UdaLOVSubTypology.index();
    public static final int REASONCODELOV = AttributesEnum.ReasonCodeLOV.index();
    public static final int SUPPLIERSITELOV = AttributesEnum.SupplierSiteLOV.index();
    public static final int CODEDETAILVOISPACK = AttributesEnum.CodeDetailVOIsPack.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PriceChangeCriteriaVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute GroupId.
     * @return the GroupId
     */
    public String getGroupId() {
        return (String)getAttributeInternal(GROUPID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute GroupId.
     * @param value value to set the  GroupId
     */
    public void setGroupId(String value) {
        setAttributeInternal(GROUPID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Dept.
     * @return the Dept
     */
    public String getDept() {
        return (String)getAttributeInternal(DEPT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Dept.
     * @param value value to set the  Dept
     */
    public void setDept(String value) {
        setAttributeInternal(DEPT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ClassId.
     * @return the ClassId
     */
    public String getClassId() {
        return (String)getAttributeInternal(CLASSID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ClassId.
     * @param value value to set the  ClassId
     */
    public void setClassId(String value) {
        setAttributeInternal(CLASSID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute SubClass.
     * @return the SubClass
     */
    public String getSubClass() {
        return (String)getAttributeInternal(SUBCLASS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute SubClass.
     * @param value value to set the  SubClass
     */
    public void setSubClass(String value) {
        setAttributeInternal(SUBCLASS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Item.
     * @return the Item
     */
    public String getItem() {
        return (String)getAttributeInternal(ITEM);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Item.
     * @param value value to set the  Item
     */
    public void setItem(String value) {
        setAttributeInternal(ITEM, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EAN.
     * @return the EAN
     */
    public String getEAN() {
        return (String)getAttributeInternal(EAN);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EAN.
     * @param value value to set the  EAN
     */
    public void setEAN(String value) {
        setAttributeInternal(EAN, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ItemDesc.
     * @return the ItemDesc
     */
    public String getItemDesc() {
        return (String)getAttributeInternal(ITEMDESC);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ItemDesc.
     * @param value value to set the  ItemDesc
     */
    public void setItemDesc(String value) {
        setAttributeInternal(ITEMDESC, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ItemType.
     * @return the ItemType
     */
    public String getItemType() {
        return (String)getAttributeInternal(ITEMTYPE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ItemType.
     * @param value value to set the  ItemType
     */
    public void setItemType(String value) {
        setAttributeInternal(ITEMTYPE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Typology.
     * @return the Typology
     */
    public String getTypology() {
        return (String)getAttributeInternal(TYPOLOGY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Typology.
     * @param value value to set the  Typology
     */
    public void setTypology(String value) {
        setAttributeInternal(TYPOLOGY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute SubTypology.
     * @return the SubTypology
     */
    public String getSubTypology() {
        return (String)getAttributeInternal(SUBTYPOLOGY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute SubTypology.
     * @param value value to set the  SubTypology
     */
    public void setSubTypology(String value) {
        setAttributeInternal(SUBTYPOLOGY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute IsPack.
     * @return the IsPack
     */
    public String getIsPack() {
        return (String)getAttributeInternal(ISPACK);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute IsPack.
     * @param value value to set the  IsPack
     */
    public void setIsPack(String value) {
        setAttributeInternal(ISPACK, value);
    }

    /**
     * Gets the attribute value for the calculated attribute SupplierSite.
     * @return the SupplierSite
     */
    public String getSupplierSite() {
        return (String)getAttributeInternal(SUPPLIERSITE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute SupplierSite.
     * @param value value to set the  SupplierSite
     */
    public void setSupplierSite(String value) {
        setAttributeInternal(SUPPLIERSITE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ReasonCode.
     * @return the ReasonCode
     */
    public String getReasonCode() {
        return (String)getAttributeInternal(REASONCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ReasonCode.
     * @param value value to set the  ReasonCode
     */
    public void setReasonCode(String value) {
        setAttributeInternal(REASONCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ApplicationStartDate.
     * @return the ApplicationStartDate
     */
    public Date getApplicationStartDate() {
        return (Date)getAttributeInternal(APPLICATIONSTARTDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ApplicationStartDate.
     * @param value value to set the  ApplicationStartDate
     */
    public void setApplicationStartDate(Date value) {
        setAttributeInternal(APPLICATIONSTARTDATE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ApplicationEndDate.
     * @return the ApplicationEndDate
     */
    public Date getApplicationEndDate() {
        return (Date)getAttributeInternal(APPLICATIONENDDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ApplicationEndDate.
     * @param value value to set the  ApplicationEndDate
     */
    public void setApplicationEndDate(Date value) {
        setAttributeInternal(APPLICATIONENDDATE, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> GroupLOV.
     */
    public RowSet getGroupLOV() {
        return (RowSet)getAttributeInternal(GROUPLOV);
    }

    /**
     * Gets the view accessor <code>RowSet</code> DeptLOV.
     */
    public RowSet getDeptLOV() {
        return (RowSet)getAttributeInternal(DEPTLOV);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ClassLOV.
     */
    public RowSet getClassLOV() {
        return (RowSet)getAttributeInternal(CLASSLOV);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SubclassLOV.
     */
    public RowSet getSubclassLOV() {
        return (RowSet)getAttributeInternal(SUBCLASSLOV);
    }

    /**
     * Gets the view accessor <code>RowSet</code> CodeDetailVOItemType.
     */
    public RowSet getCodeDetailVOItemType() {
        return (RowSet)getAttributeInternal(CODEDETAILVOITEMTYPE);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UdaLOVTypology.
     */
    public RowSet getUdaLOVTypology() {
        return (RowSet)getAttributeInternal(UDALOVTYPOLOGY);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UdaLOVSubTypology.
     */
    public RowSet getUdaLOVSubTypology() {
        return (RowSet)getAttributeInternal(UDALOVSUBTYPOLOGY);
    }


    /**
     * Gets the view accessor <code>RowSet</code> ReasonCodeLOV.
     */
    public RowSet getReasonCodeLOV() {
        return (RowSet)getAttributeInternal(REASONCODELOV);
    }


    /**
     * Gets the view accessor <code>RowSet</code> SupplierSiteLOV.
     */
    public RowSet getSupplierSiteLOV() {
        return (RowSet)getAttributeInternal(SUPPLIERSITELOV);
    }

    /**
     * Gets the view accessor <code>RowSet</code> CodeDetailVOIsPack.
     */
    public RowSet getCodeDetailVOIsPack() {
        return (RowSet)getAttributeInternal(CODEDETAILVOISPACK);
    }
}

