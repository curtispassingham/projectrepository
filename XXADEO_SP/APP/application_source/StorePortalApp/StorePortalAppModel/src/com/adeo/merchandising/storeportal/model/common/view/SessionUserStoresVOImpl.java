package com.adeo.merchandising.storeportal.model.common.view;

import com.adeo.merchandising.storeportal.model.common.SPViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Mar 14 15:51:46 GMT 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SessionUserStoresVOImpl extends SPViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public SessionUserStoresVOImpl() {
    }
}

