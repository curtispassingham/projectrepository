package com.adeo.merchandising.storeportal.model.item.view;

import com.adeo.merchandising.storeportal.model.common.SPViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Jun 25 17:51:59 BST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class GeneralDataVOImpl extends SPViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public GeneralDataVOImpl() {
    }

    /**
     * Returns the bind variable value for bind_item.
     * @return bind variable value for bind_item
     */
    public String getbind_item() {
        return (String)getNamedWhereClauseParam("bind_item");
    }

    /**
     * Sets <code>value</code> for bind variable bind_item.
     * @param value value to bind as bind_item
     */
    public void setbind_item(String value) {
        setNamedWhereClauseParam("bind_item", value);
    }


    /**
     * Returns the bind variable value for bind_user_store.
     * @return bind variable value for bind_user_store
     */
    public String getbind_user_store() {
        return (String)getNamedWhereClauseParam("bind_user_store");
    }

    /**
     * Sets <code>value</code> for bind variable bind_user_store.
     * @param value value to bind as bind_user_store
     */
    public void setbind_user_store(String value) {
        setNamedWhereClauseParam("bind_user_store", value);
    }

    /**
     * Returns the bind variable value for bind_bu.
     * @return bind variable value for bind_bu
     */
    public String getbind_user_bu() {
        return (String)getNamedWhereClauseParam("bind_user_bu");
    }

    /**
     * Sets <code>value</code> for bind variable bind_bu.
     * @param value value to bind as bind_bu
     */
    public void setbind_user_bu(String value) {
        setNamedWhereClauseParam("bind_user_bu", value);
    }
}

