package com.adeo.merchandising.storeportal.model.common;

import oracle.retail.apps.rms.common.model.adfbc.RmsApplicationModuleDefImpl;

public class SPApplicationModuleDefImpl extends RmsApplicationModuleDefImpl {
    public SPApplicationModuleDefImpl() {
        super();
    }

    public SPApplicationModuleDefImpl(int i, String string) {
        super(i, string);
    }

    public SPApplicationModuleDefImpl(String string) {
        super(string);
    }
}
