package com.adeo.merchandising.storeportal.model.common;

import oracle.retail.apps.rms.common.model.adfbc.RmsViewDefImpl;

public class SPViewDefImpl extends RmsViewDefImpl {
    public SPViewDefImpl() {
        super();
    }

    public SPViewDefImpl(int i, String string) {
        super(i, string);
    }

    public SPViewDefImpl(String string) {
        super(string);
    }

    public SPViewDefImpl(int i, String string, String string1) {
        super(i, string, string1);
    }
}
