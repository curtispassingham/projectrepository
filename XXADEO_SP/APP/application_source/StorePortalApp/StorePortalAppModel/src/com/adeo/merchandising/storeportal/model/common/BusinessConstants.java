package com.adeo.merchandising.storeportal.model.common;


public class BusinessConstants {
    private static final String UTILITY_CLASS = "Utility class";

    public BusinessConstants() {
        throw new IllegalStateException(UTILITY_CLASS);
    }

    public static final class Bundle {
        public static final String BUNDLE_NAME =
            "com.adeo.merchandising.storeportal.view.StorePortalAppViewControllerBundle";

        public static final class BundleKeys {
            public static final String PLEASE_SELECT_AT_LEAST_ONE_FIELD =
                "PLEASE_SELECT_AT_LEAST_ONE_FIELD";
            public static final String UNCOMMITED_PRICE_CHANGES_MSG =
                "UNCOMMITED_PRICE_CHANGES_MSG";
            public static final String NULL_SEARCH_CRITERIA = "NULL_SEARCH_CRITERIA";
    

            private BundleKeys() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }

        private Bundle() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }

    public static final class Utils {
        public static final String PRICE_CHANGE_FLOW_BEAN = "PriceChangeFlowBean";

        private Utils() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }

    public static final class Common {
        public static final class BindVariables {
            public static final String USER_STORE = "bind_user_store";
            public static final String USER_BU = "bind_user_bu";
            public static final String USER = "bind_user";

            private BindVariables() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }

        public static final class Session {
            public static final String USER_STORE_KEY = "userSessionStore";
            public static final String USER_BU_KEY = "userSessionBU";
            public static final String USER_STORE_FORMAT = "userStoreFormat";
            public static final String IS_USER_STORE_SET = "isUserStoreSet";

            private Session() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }

        private Common() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }

    public static final class AppModule {
        public static final class Methods {
            public static final String USER_STORE_INIT = "userStoreInit";

            private Methods() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }

        private AppModule() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }

    public static final class PriceAppModule {
        public static final class Methods {
            public static final String INIT_SEARCH_PRICE = "initSearchPrice";
            public static final String RESET_SEARCH_CRITERIAS = "resetPriceChangeCriterias";
            public static final String APPROVE_CURRENT_PRICE_CHANGES = "approveCurrentPriceChanges";
            public static final String INIT_PRICE_CHANGE_FLOW = "initPriceChangeFlow";

            private Methods() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }
        
        public static final class Parameters {
            public static final String PRICE_CHANGES = "priceChanges";

            private Parameters() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }

        private PriceAppModule() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }
    
    public static final class AssortmentAppModule {
        public static final class Methods {
            public static final String INIT_ASSORTMENT_CRITERIA = "initAssortmentCriteria";
            public static final String FETCH_ASSORTMENT_DETAILS = "fetchAssortmentDetails";
            public static final String APPROVE_ASSORTMENT = "approveAssortment";
            public static final String VALIDATE_STORE_CHOICE = "validateStoreChoice";
            public static final String CANCEL_ASSORT_CHANGES = "cancelAssortChanges";

            private Methods() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }
        
        public static final class Parameters {
            public static final String ROW = "row";
            private Parameters() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }

        private AssortmentAppModule() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }

    public static final class PriceChangeVO {
        public static final String ITERATOR = "PriceChangeIterator";

        public static final class Attributes {
            public static final String GROUP_NO = "GroupNo";
            public static final String DEPT = "Dept";
            public static final String CLASS1 = "Class1";
            public static final String SUB_CLASS = "Subclass";
            public static final String ITEM = "Item";
            public static final String ITEM_DESC = "ItemDesc";
            public static final String ZONE_RETAIL_PRICE = "ZoneRetailPrice";
            public static final String STORE_RETAIL_PRICE = "StoreRetailPrice";


            private Attributes() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }

        public static final class BindVariables {
            public static final String CONTROL = "bind_control";
            public static final String ITEM = "bind_item";
            public static final String GROUP = "bind_group";
            public static final String DEPT = "bind_dept";
            public static final String CLASS = "bind_class";
            public static final String SUBCLASS = "bind_subclass";
            public static final String ITEM_DESC = "bind_item_desc";
            public static final String EAN = "bind_ean";
            public static final String ITEM_TYPE = "bind_item_type";
            public static final String TYPOLOGY = "bind_typology";
            public static final String SUBTYPOLOGY = "bind_subtypology";
            public static final String IS_PACK = "bind_ispack";
            public static final String SUPPLIER = "bind_supplier";
            public static final String REASON = "bind_reason";
            public static final String START_DATE = "bind_price_date_start";
            public static final String END_DATE = "bind_price_date_end";

            private BindVariables() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }
        
        private PriceChangeVO() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }

    public static final class PriceChangeCriteriaVO {
        public static final String ITERATOR = "PriceChangeCriteriaIterator";

        public static final class Attributes {
            public static final String GROUP_ID = "GroupId";
            public static final String DEPT = "Dept";
            public static final String CLASS_ID = "ClassId";
            public static final String SUB_CLASS = "SubClass";
            public static final String ITEM = "Item";
            public static final String EAN = "EAN";
            public static final String ITEM_DESC = "ItemDesc";
            public static final String ITEM_TYPE = "ItemType";
            public static final String TYPOLOGY = "Typology";
            public static final String SUB_TYPOLOGY = "SubTypology";
            public static final String IS_PACK = "IsPack";
            public static final String SUPPLIER_SITE = "SupplierSite";
            public static final String REASON_CODE = "ReasonCode";
            public static final String APPLICATION_START_DATE = "ApplicationStartDate";
            public static final String APPLICATION_END_DATE = "ApplicationEndDate";

            private Attributes() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }

        private PriceChangeCriteriaVO() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }

    public static final class SupsVO {
        public static final String ITERATOR = "SupsIterator";

        public static final class Attributes {
            public static final String SUPPLIER_SITE = "SupplierSite";
            public static final String SUPPLIER_SITE_ID = "SupplierSiteId";
            public static final String SUPPLIER_ID = "SupplierId";
            public static final String SUPPLIER_NAME = "SupplierName";
            public static final String SUPPLIER_SITE_NAME = "SupplierSiteName";

            private Attributes() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }

        public static final class BindVariables {
            public static final String SUPPLIER_ID = "bind_supplier_id";
            public static final String SUP_NAME = "bind_sup_name";
            public static final String SUPPLIER_SITE_ID = "bind_supplier_site_id";
            public static final String SUP_SITE_NAME = "bind_sup_site_name";

            private BindVariables() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }

        private SupsVO() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }
    
    public static final class Exception {
        public static final String SQL_EXCEPTION_RECEIVED = "SQL_EXCEPTION_RECEIVED";
        public static final String DEFAULT_ERROR_AND_DESC_MSG = "DEFAULT_ERROR_AND_DESC_MSG";

        private Exception() {
            throw new IllegalStateException(BusinessConstants.UTILITY_CLASS);
        }
    }
    
    public static final class SupItemsVO {
        public static final String ITERATOR = "SupItemsIterator";

        public static final class BindVariables {
            public static final String SUPPLIER = "bind_supplier";

            private BindVariables() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }

        private SupItemsVO() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }
    
    public static final class AssortmentResultsVO {
        public static final String ITERATOR = "AssortmentResultsVOIterator";
        
        public static final class rowAttrs {
            public static final String COMMENT_CP = "#{row.CommentCp}";
            public static final String ROW_NUM = "#{row.RowNum1}";
            public static final String ITEM = "#{row.Item}";
            
            private rowAttrs() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }
        
        public static final class BindVariables {
            public static final String EXECUTE = "bind_execute";
            public static final String STORE = "bind_store";
            public static final String GROUP = "bind_group";
            public static final String DEPT = "bind_dept";
            public static final String CLASS = "bind_class";
            public static final String SUB_CLASS = "bind_subclass";
            public static final String ITEM = "bind_item";
            public static final String ITEM_DESC = "bind_itemdesc";
            public static final String ASSORT_MODE = "bind_assortmode";
            public static final String RANGE_LETTER = "bind_rangeletter";
            public static final String HQ_REC = "bind_hqrec";
            public static final String STORE_CHOICE = "bind_storechoice";
            public static final String SUPP_SITE = "bind_suppliersite";
            public static final String NOT_MODIFIABLE = "bind_notmodifiable";
            public static final String SEASON_ITEM = "bind_seasonitem";
            public static final String RELATED_ITEMS = "bind_relateditems";
            public static final String PACK_COMP = "bind_packcomp";
            public static final String FROM_ASSORT_START_DATE = "bind_assortstartdatel";
            public static final String TO_ASSORT_START_DATE = "bind_assortstartdateh";
            public static final String FROM_ASSORT_END_DATE = "bind_assortenddatel";
            public static final String TO_ASSORT_END_DATE = "bind_assortenddateh";
            public static final String FROM_OMNI_START_DATE = "bind_omnicsstartdatel";
            public static final String TO_OMNI_START_DATE = "bind_omnicsstartdateh";
            public static final String FROM_OMNI_END_DATE = "bind_omnicsenddatel";
            public static final String TO_OMNI_END_DATE = "bind_omnicsenddateh";
            public static final String FROM_RESPONSE_DATE = "bind_respenddatel";
            public static final String TO_RESPONSE_DATE = "bind_respenddateh";

            private BindVariables() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }
        
        private AssortmentResultsVO() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }
    
    public static final class StoreChoiceLOV {
        public static final class Values{
            public static final String V1 = "V1";
            public static final String V2 = "V2";
            public static final String PO = "PO";
            public static final String P1 = "P1";
            public static final String P2 = "P2";
            public static final String TO = "TO";
            public static final String C = "C";
            
            private Values() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }
        private StoreChoiceLOV() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }
    
    public static final class RecomClustersVO {
        public static final String ITERATOR = "RecomClustersVOIterator";
        
        private RecomClustersVO() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
        
        public static final class BindVariables {
            public static final String ITEM = "bind_item";
            public static final String STORE = "bind_store";
            public static final String START_DATE = "bind_startDate";

            private BindVariables() {
                throw new IllegalStateException(UTILITY_CLASS);
            }
        }
    }
    
    public static final class AssortmentCriteriaVO {
        public static final String ITERATOR = "AssortmentCriteriaVOIterator";
        
        private AssortmentCriteriaVO() {
            throw new IllegalStateException(UTILITY_CLASS);
        }
    }
}
