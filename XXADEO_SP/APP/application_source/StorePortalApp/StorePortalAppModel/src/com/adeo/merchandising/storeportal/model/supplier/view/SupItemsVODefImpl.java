package com.adeo.merchandising.storeportal.model.supplier.view;

import com.adeo.merchandising.storeportal.model.common.SPViewDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Sep 03 17:05:24 BST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SupItemsVODefImpl extends SPViewDefImpl {
    public SupItemsVODefImpl(String name) {
        super(name);
    }

    /**
     * This is the default constructor (do not remove).
     */
    public SupItemsVODefImpl() {
    }
}

