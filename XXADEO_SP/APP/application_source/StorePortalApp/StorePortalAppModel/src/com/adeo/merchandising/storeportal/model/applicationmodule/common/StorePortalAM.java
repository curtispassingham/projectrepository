package com.adeo.merchandising.storeportal.model.applicationmodule.common;

import java.sql.SQLException;

import oracle.jbo.ApplicationModule;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Mar 14 15:55:55 GMT 2018
// ---------------------------------------------------------------------
public interface StorePortalAM extends ApplicationModule {
    void userStoreInit();

    String getHelpUrl() throws SQLException;
}

