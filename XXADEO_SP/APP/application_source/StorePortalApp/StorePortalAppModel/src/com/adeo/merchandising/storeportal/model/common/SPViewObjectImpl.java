package com.adeo.merchandising.storeportal.model.common;

import oracle.adf.share.ADFContext;

import oracle.jbo.VariableValueManager;
import oracle.jbo.server.ViewDefImpl;

import oracle.retail.apps.rms.common.model.adfbc.RmsViewObjectImpl;

public class SPViewObjectImpl extends RmsViewObjectImpl {
    public SPViewObjectImpl() {
        super();
    }

    public SPViewObjectImpl(String string, ViewDefImpl viewDefImpl) {
        super(string, viewDefImpl);
    }
    

    @Override
    protected void executeQueryForCollection(Object object, Object[] object2, int i) {
        VariableValueManager vvman = super.ensureVariableManager();
        try {
            vvman.setVariableValue(BusinessConstants.Common.BindVariables.USER_STORE,
                                   ADFContext.getCurrent().getSessionScope().get(BusinessConstants.Common.Session.USER_STORE_KEY));
        } catch (Exception e) {

        }
        try {
            vvman.setVariableValue(BusinessConstants.Common.BindVariables.USER_BU,
                                   ADFContext.getCurrent().getSessionScope().get(BusinessConstants.Common.Session.USER_BU_KEY));
        } catch (Exception e) {

        }
        super.executeQueryForCollection(object, object2, i);
    }
}
