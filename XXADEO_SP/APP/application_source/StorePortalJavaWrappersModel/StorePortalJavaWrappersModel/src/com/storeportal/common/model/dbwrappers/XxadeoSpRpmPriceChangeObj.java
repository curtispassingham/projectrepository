
package com.storeportal.common.model.dbwrappers;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.jpub.runtime.MutableStruct;
import oracle.sql.Datum;
import java.sql.SQLException;
import oracle.sql.STRUCT;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.retail.apps.framework.jdbc.DbSchemaResolverFactory;
import oracle.retail.apps.framework.jdbc.OracleTypeAdapter;
/*******************************************************
 * THIS CODE IS AUTO-GENERATED.  DO NOT MODIFY THIS CODE!
 * 
 * Modify the Oracle Type this class was based on and 
 * re-generate the contents of this file using the oracle.retail.apps.framework.jdbc.OracleTypeAdapterGenerator application. 
 * 
 * Generator Version : 1.0
 * Generation Date   : 2018-07-12 14:57:053
 * Oracle Type Name  : XXADEO_SP_RPM_PRICE_CHANGE_OBJ
 * Owned by          : XXADEO_SP
 * Requested Schema  : XXADEO_SP
 * Requested DB URL  : jdbc:oracle:thin:@onsdb04:1521/rms16adeo
 * Requested DB User : xxadeo_sp
 *******************************************************/
public class XxadeoSpRpmPriceChangeObj implements ORAData, ORADataFactory, OracleTypeAdapter {
    public static final String _SQL_NAME = "XXADEO_SP_RPM_PRICE_CHANGE_OBJ";

    public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

    public static final int ITEM_IDX = 0;
    public static final int LOCATION_IDX = 1;
    public static final int REASON_CODE_IDX = 2;
    public static final int CHANGE_AMOUNT_IDX = 3;
    public static final int EFFECTIVE_DATE_IDX = 4;
    protected MutableStruct _struct;
    protected static int[] _sqlType = {12,2,2,2,93};
    protected static ORADataFactory[] _factory = new ORADataFactory[5];
    static {
    }
    protected static final XxadeoSpRpmPriceChangeObj _XxadeoSpRpmPriceChangeObjFactory = new XxadeoSpRpmPriceChangeObj();
    public static ORADataFactory getORADataFactory() {
        return _XxadeoSpRpmPriceChangeObjFactory;
    }

    public XxadeoSpRpmPriceChangeObj() {
        _struct = new MutableStruct(new Object[5], _sqlType, _factory);
    }

    public Datum toDatum(Connection c) throws SQLException {
        return _struct.toDatum(c, getSQLTypeName()); 
    }

    public ORAData create(Datum d, int sqlType) throws SQLException {
        return create(null, d, sqlType);
    }

    protected ORAData create(XxadeoSpRpmPriceChangeObj o, Datum d, int sqlType) throws SQLException {
        if (d == null)
            return null;
        if (o == null)
            o = new XxadeoSpRpmPriceChangeObj();
        o._struct = new MutableStruct((STRUCT)d, _sqlType, _factory);
        return o;
    }
    public final java.lang.String  getItem() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(ITEM_IDX);
    }

    public final void setItem(java.lang.String item) throws SQLException 
    { 
        _struct.setAttribute(ITEM_IDX, item);
    }

    public final java.math.BigDecimal  getLocation() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(LOCATION_IDX);
    }

    public final void setLocation(java.math.BigDecimal location) throws SQLException 
    { 
        _struct.setAttribute(LOCATION_IDX, location);
    }

    public final java.math.BigDecimal  getReasonCode() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(REASON_CODE_IDX);
    }

    public final void setReasonCode(java.math.BigDecimal reasonCode) throws SQLException 
    { 
        _struct.setAttribute(REASON_CODE_IDX, reasonCode);
    }

    public final java.math.BigDecimal  getChangeAmount() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(CHANGE_AMOUNT_IDX);
    }

    public final void setChangeAmount(java.math.BigDecimal changeAmount) throws SQLException 
    { 
        _struct.setAttribute(CHANGE_AMOUNT_IDX, changeAmount);
    }

    public final java.sql.Timestamp  getEffectiveDate() throws SQLException 
    {
        return (java.sql.Timestamp)_struct.getAttribute(EFFECTIVE_DATE_IDX);
    }

    public final void setEffectiveDate(java.sql.Timestamp effectiveDate) throws SQLException 
    { 
        _struct.setAttribute(EFFECTIVE_DATE_IDX, effectiveDate);
    }

    public static String getSQLTypeName() {    return DbSchemaResolverFactory.getResolver().resolveObjectName(_SQL_NAME);} 
    public static int getSQLTypeCode() {    return _SQL_TYPECODE;} 
} // end class
