
package com.storeportal.common.model.dbwrappers;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.jpub.runtime.MutableStruct;
import oracle.sql.Datum;
import java.sql.SQLException;
import oracle.sql.STRUCT;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.retail.apps.framework.jdbc.DbSchemaResolverFactory;
import oracle.retail.apps.framework.jdbc.OracleTypeAdapter;
/*******************************************************
 * THIS CODE IS AUTO-GENERATED.  DO NOT MODIFY THIS CODE!
 * 
 * Modify the Oracle Type this class was based on and 
 * re-generate the contents of this file using the oracle.retail.apps.framework.jdbc.OracleTypeAdapterGenerator application. 
 * 
 * Generator Version : 1.0
 * Generation Date   : 2018-08-13 17:54:035
 * Oracle Type Name  : XXADEO_SP_ASSORTMENT_OBJ
 * Owned by          : XXADEO_SP
 * Requested Schema  : XXADEO_SP
 * Requested DB URL  : jdbc:oracle:thin:@onsdb04:1521/rms16adeo
 * Requested DB User : xxadeo_sp
 *******************************************************/
public class XxadeoSpAssortmentObj implements ORAData, ORADataFactory, OracleTypeAdapter {
    public static final String _SQL_NAME = "XXADEO_SP_ASSORTMENT_OBJ";

    public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

    public static final int STORE_IDX = 0;
    public static final int GROUP_NO_IDX = 1;
    public static final int DEPT_IDX = 2;
    public static final int CLASS_IDX = 3;
    public static final int SUBCLASS_IDX = 4;
    public static final int ITEM_IDX = 5;
    public static final int ITEM_DESC_IDX = 6;
    public static final int RETAIL_PRICE_IDX = 7;
    public static final int ASSORTMENT_MODE_IDX = 8;
    public static final int RANGE_LETTER_IDX = 9;
    public static final int GRADING_IDX = 10;
    public static final int CLUSTER_SIZE_IDX = 11;
    public static final int CS_RECOMMENDATION_IDX = 12;
    public static final int HQ_RECOMMENDATION_IDX = 13;
    public static final int CURRENT_REGULAR_ASSO_IDX = 14;
    public static final int STORE_CHOICE_IDX = 15;
    public static final int SUPPLIER_SITE_IDX = 16;
    public static final int ASSORTMENT_START_DATE_IDX = 17;
    public static final int ASSORTMENT_END_DATE_IDX = 18;
    public static final int OMNI_CS_START_DATE_IDX = 19;
    public static final int OMNI_CS_END_DATE_IDX = 20;
    public static final int RESPONSE_END_DATE_IDX = 21;
    public static final int PACK_COMPONENT_IDX = 22;
    public static final int SUBSTITUTE_ITEM_IDX = 23;
    public static final int RELATED_ITEMS_IDX = 24;
    public static final int COMMENT_CP_IDX = 25;
    public static final int SEASON_ITEM_IND_IDX = 26;
    public static final int NOT_MODIFIABLE_IND_IDX = 27;
    public static final int VALIDATED_DATE_IDX = 28;
    public static final int UPDATE_LINE_INTERFACE_IDX = 29;
    public static final int STORE_FORMAT_IDX = 30;
    public static final int CATMAN_STATUS_IDX = 31;
    public static final int HISTORY_FLAG_IDX = 32;
    public static final int CLUSTER_POPUP_FLAG_IDX = 33;
    protected MutableStruct _struct;
    protected static int[] _sqlType = {2,2,2,2,2,12,12,2,12,12,2,12,12,12,12,12,2,93,93,93,93,93,12,12,12,12,12,12,93,93,12,2,12,12};
    protected static ORADataFactory[] _factory = new ORADataFactory[34];
    static {
    }
    protected static final XxadeoSpAssortmentObj _XxadeoSpAssortmentObjFactory = new XxadeoSpAssortmentObj();
    public static ORADataFactory getORADataFactory() {
        return _XxadeoSpAssortmentObjFactory;
    }

    public XxadeoSpAssortmentObj() {
        _struct = new MutableStruct(new Object[34], _sqlType, _factory);
    }

    public Datum toDatum(Connection c) throws SQLException {
        return _struct.toDatum(c, getSQLTypeName()); 
    }

    public ORAData create(Datum d, int sqlType) throws SQLException {
        return create(null, d, sqlType);
    }

    protected ORAData create(XxadeoSpAssortmentObj o, Datum d, int sqlType) throws SQLException {
        if (d == null)
            return null;
        if (o == null)
            o = new XxadeoSpAssortmentObj();
        o._struct = new MutableStruct((STRUCT)d, _sqlType, _factory);
        return o;
    }
    public final java.math.BigDecimal  getStore() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(STORE_IDX);
    }

    public final void setStore(java.math.BigDecimal store) throws SQLException 
    { 
        _struct.setAttribute(STORE_IDX, store);
    }

    public final java.math.BigDecimal  getGroupNo() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(GROUP_NO_IDX);
    }

    public final void setGroupNo(java.math.BigDecimal groupNo) throws SQLException 
    { 
        _struct.setAttribute(GROUP_NO_IDX, groupNo);
    }

    public final java.math.BigDecimal  getDept() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(DEPT_IDX);
    }

    public final void setDept(java.math.BigDecimal dept) throws SQLException 
    { 
        _struct.setAttribute(DEPT_IDX, dept);
    }

    public final java.math.BigDecimal  getClassAttribute() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(CLASS_IDX);
    }

    public final void setClassAttribute(java.math.BigDecimal classAttribute) throws SQLException 
    { 
        _struct.setAttribute(CLASS_IDX, classAttribute);
    }

    public final java.math.BigDecimal  getSubclass() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(SUBCLASS_IDX);
    }

    public final void setSubclass(java.math.BigDecimal subclass) throws SQLException 
    { 
        _struct.setAttribute(SUBCLASS_IDX, subclass);
    }

    public final java.lang.String  getItem() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(ITEM_IDX);
    }

    public final void setItem(java.lang.String item) throws SQLException 
    { 
        _struct.setAttribute(ITEM_IDX, item);
    }

    public final java.lang.String  getItemDesc() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(ITEM_DESC_IDX);
    }

    public final void setItemDesc(java.lang.String itemDesc) throws SQLException 
    { 
        _struct.setAttribute(ITEM_DESC_IDX, itemDesc);
    }

    public final java.math.BigDecimal  getRetailPrice() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(RETAIL_PRICE_IDX);
    }

    public final void setRetailPrice(java.math.BigDecimal retailPrice) throws SQLException 
    { 
        _struct.setAttribute(RETAIL_PRICE_IDX, retailPrice);
    }

    public final java.lang.String  getAssortmentMode() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(ASSORTMENT_MODE_IDX);
    }

    public final void setAssortmentMode(java.lang.String assortmentMode) throws SQLException 
    { 
        _struct.setAttribute(ASSORTMENT_MODE_IDX, assortmentMode);
    }

    public final java.lang.String  getRangeLetter() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(RANGE_LETTER_IDX);
    }

    public final void setRangeLetter(java.lang.String rangeLetter) throws SQLException 
    { 
        _struct.setAttribute(RANGE_LETTER_IDX, rangeLetter);
    }

    public final java.math.BigDecimal  getGrading() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(GRADING_IDX);
    }

    public final void setGrading(java.math.BigDecimal grading) throws SQLException 
    { 
        _struct.setAttribute(GRADING_IDX, grading);
    }

    public final java.lang.String  getClusterSize() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(CLUSTER_SIZE_IDX);
    }

    public final void setClusterSize(java.lang.String clusterSize) throws SQLException 
    { 
        _struct.setAttribute(CLUSTER_SIZE_IDX, clusterSize);
    }

    public final java.lang.String  getCsRecommendation() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(CS_RECOMMENDATION_IDX);
    }

    public final void setCsRecommendation(java.lang.String csRecommendation) throws SQLException 
    { 
        _struct.setAttribute(CS_RECOMMENDATION_IDX, csRecommendation);
    }

    public final java.lang.String  getHqRecommendation() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(HQ_RECOMMENDATION_IDX);
    }

    public final void setHqRecommendation(java.lang.String hqRecommendation) throws SQLException 
    { 
        _struct.setAttribute(HQ_RECOMMENDATION_IDX, hqRecommendation);
    }

    public final java.lang.String  getCurrentRegularAsso() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(CURRENT_REGULAR_ASSO_IDX);
    }

    public final void setCurrentRegularAsso(java.lang.String currentRegularAsso) throws SQLException 
    { 
        _struct.setAttribute(CURRENT_REGULAR_ASSO_IDX, currentRegularAsso);
    }

    public final java.lang.String  getStoreChoice() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(STORE_CHOICE_IDX);
    }

    public final void setStoreChoice(java.lang.String storeChoice) throws SQLException 
    { 
        _struct.setAttribute(STORE_CHOICE_IDX, storeChoice);
    }

    public final java.math.BigDecimal  getSupplierSite() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(SUPPLIER_SITE_IDX);
    }

    public final void setSupplierSite(java.math.BigDecimal supplierSite) throws SQLException 
    { 
        _struct.setAttribute(SUPPLIER_SITE_IDX, supplierSite);
    }

    public final java.sql.Timestamp  getAssortmentStartDate() throws SQLException 
    {
        return (java.sql.Timestamp)_struct.getAttribute(ASSORTMENT_START_DATE_IDX);
    }

    public final void setAssortmentStartDate(java.sql.Timestamp assortmentStartDate) throws SQLException 
    { 
        _struct.setAttribute(ASSORTMENT_START_DATE_IDX, assortmentStartDate);
    }

    public final java.sql.Timestamp  getAssortmentEndDate() throws SQLException 
    {
        return (java.sql.Timestamp)_struct.getAttribute(ASSORTMENT_END_DATE_IDX);
    }

    public final void setAssortmentEndDate(java.sql.Timestamp assortmentEndDate) throws SQLException 
    { 
        _struct.setAttribute(ASSORTMENT_END_DATE_IDX, assortmentEndDate);
    }

    public final java.sql.Timestamp  getOmniCsStartDate() throws SQLException 
    {
        return (java.sql.Timestamp)_struct.getAttribute(OMNI_CS_START_DATE_IDX);
    }

    public final void setOmniCsStartDate(java.sql.Timestamp omniCsStartDate) throws SQLException 
    { 
        _struct.setAttribute(OMNI_CS_START_DATE_IDX, omniCsStartDate);
    }

    public final java.sql.Timestamp  getOmniCsEndDate() throws SQLException 
    {
        return (java.sql.Timestamp)_struct.getAttribute(OMNI_CS_END_DATE_IDX);
    }

    public final void setOmniCsEndDate(java.sql.Timestamp omniCsEndDate) throws SQLException 
    { 
        _struct.setAttribute(OMNI_CS_END_DATE_IDX, omniCsEndDate);
    }

    public final java.sql.Timestamp  getResponseEndDate() throws SQLException 
    {
        return (java.sql.Timestamp)_struct.getAttribute(RESPONSE_END_DATE_IDX);
    }

    public final void setResponseEndDate(java.sql.Timestamp responseEndDate) throws SQLException 
    { 
        _struct.setAttribute(RESPONSE_END_DATE_IDX, responseEndDate);
    }

    public final java.lang.String  getPackComponent() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(PACK_COMPONENT_IDX);
    }

    public final void setPackComponent(java.lang.String packComponent) throws SQLException 
    { 
        _struct.setAttribute(PACK_COMPONENT_IDX, packComponent);
    }

    public final java.lang.String  getSubstituteItem() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(SUBSTITUTE_ITEM_IDX);
    }

    public final void setSubstituteItem(java.lang.String substituteItem) throws SQLException 
    { 
        _struct.setAttribute(SUBSTITUTE_ITEM_IDX, substituteItem);
    }

    public final java.lang.String  getRelatedItems() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(RELATED_ITEMS_IDX);
    }

    public final void setRelatedItems(java.lang.String relatedItems) throws SQLException 
    { 
        _struct.setAttribute(RELATED_ITEMS_IDX, relatedItems);
    }

    public final java.lang.String  getCommentCp() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(COMMENT_CP_IDX);
    }

    public final void setCommentCp(java.lang.String commentCp) throws SQLException 
    { 
        _struct.setAttribute(COMMENT_CP_IDX, commentCp);
    }

    public final java.lang.String  getSeasonItemInd() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(SEASON_ITEM_IND_IDX);
    }

    public final void setSeasonItemInd(java.lang.String seasonItemInd) throws SQLException 
    { 
        _struct.setAttribute(SEASON_ITEM_IND_IDX, seasonItemInd);
    }

    public final java.lang.String  getNotModifiableInd() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(NOT_MODIFIABLE_IND_IDX);
    }

    public final void setNotModifiableInd(java.lang.String notModifiableInd) throws SQLException 
    { 
        _struct.setAttribute(NOT_MODIFIABLE_IND_IDX, notModifiableInd);
    }

    public final java.sql.Timestamp  getValidatedDate() throws SQLException 
    {
        return (java.sql.Timestamp)_struct.getAttribute(VALIDATED_DATE_IDX);
    }

    public final void setValidatedDate(java.sql.Timestamp validatedDate) throws SQLException 
    { 
        _struct.setAttribute(VALIDATED_DATE_IDX, validatedDate);
    }

    public final java.sql.Timestamp  getUpdateLineInterface() throws SQLException 
    {
        return (java.sql.Timestamp)_struct.getAttribute(UPDATE_LINE_INTERFACE_IDX);
    }

    public final void setUpdateLineInterface(java.sql.Timestamp updateLineInterface) throws SQLException 
    { 
        _struct.setAttribute(UPDATE_LINE_INTERFACE_IDX, updateLineInterface);
    }

    public final java.lang.String  getStoreFormat() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(STORE_FORMAT_IDX);
    }

    public final void setStoreFormat(java.lang.String storeFormat) throws SQLException 
    { 
        _struct.setAttribute(STORE_FORMAT_IDX, storeFormat);
    }

    public final java.math.BigDecimal  getCatmanStatus() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(CATMAN_STATUS_IDX);
    }

    public final void setCatmanStatus(java.math.BigDecimal catmanStatus) throws SQLException 
    { 
        _struct.setAttribute(CATMAN_STATUS_IDX, catmanStatus);
    }

    public final java.lang.String  getHistoryFlag() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(HISTORY_FLAG_IDX);
    }

    public final void setHistoryFlag(java.lang.String historyFlag) throws SQLException 
    { 
        _struct.setAttribute(HISTORY_FLAG_IDX, historyFlag);
    }

    public final java.lang.String  getClusterPopupFlag() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(CLUSTER_POPUP_FLAG_IDX);
    }

    public final void setClusterPopupFlag(java.lang.String clusterPopupFlag) throws SQLException 
    { 
        _struct.setAttribute(CLUSTER_POPUP_FLAG_IDX, clusterPopupFlag);
    }

    public static String getSQLTypeName() {    return DbSchemaResolverFactory.getResolver().resolveObjectName(_SQL_NAME);} 
    public static int getSQLTypeCode() {    return _SQL_TYPECODE;} 
} // end class
