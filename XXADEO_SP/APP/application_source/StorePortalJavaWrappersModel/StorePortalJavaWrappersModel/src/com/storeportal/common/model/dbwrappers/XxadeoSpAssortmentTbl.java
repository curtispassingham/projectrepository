
package com.storeportal.common.model.dbwrappers;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.jpub.runtime.MutableArray;
import oracle.sql.Datum;
import java.sql.SQLException;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.retail.apps.framework.jdbc.DbSchemaResolverFactory;
import oracle.retail.apps.framework.jdbc.OracleTypeAdapter;
/*******************************************************
 * THIS CODE IS AUTO-GENERATED.  DO NOT MODIFY THIS CODE!
 * 
 * Modify the Oracle Type this class was based on and 
 * re-generate the contents of this file using the oracle.retail.apps.framework.jdbc.OracleTypeAdapterGenerator application. 
 * 
 * Generator Version : 1.0
 * Generation Date   : 2018-08-13 17:54:035
 * Oracle Type Name  : XXADEO_SP_ASSORTMENT_TBL
 * Owned by          : XXADEO_SP
 * Requested Schema  : XXADEO_SP
 * Requested DB URL  : jdbc:oracle:thin:@onsdb04:1521/rms16adeo
 * Requested DB User : xxadeo_sp
 *******************************************************/
public class XxadeoSpAssortmentTbl implements ORAData, ORADataFactory, OracleTypeAdapter{
    public static final String _SQL_NAME = "XXADEO_SP_ASSORTMENT_TBL";
    public static final int _SQL_TYPECODE = OracleTypes.ARRAY;
    MutableArray  _array;
    private static final XxadeoSpAssortmentTbl _XxadeoSpAssortmentTblFactory = new XxadeoSpAssortmentTbl();
    public static ORADataFactory getORADataFactory() {
        return _XxadeoSpAssortmentTblFactory;
    }
    public XxadeoSpAssortmentTbl() {
        this((XxadeoSpAssortmentObj[])null);
    }
    public XxadeoSpAssortmentTbl(XxadeoSpAssortmentObj[] a) {
        XxadeoSpAssortmentObj[] aclone =  null;
        if (a != null) {
            aclone = a.clone();
        }
        _array = new MutableArray(2002, aclone, XxadeoSpAssortmentObj.getORADataFactory());
    }

    public Datum toDatum(Connection c) throws SQLException {
        return _array.toDatum(c, getSQLTypeName());  
    }
    public int length() throws SQLException {
        return _array.length();
    }

    public int getBaseType() throws SQLException {
        return _array.getBaseType();
    }

    public String getBaseTypeName() throws SQLException {
        return _array.getBaseTypeName();
    }

    public ArrayDescriptor getDescriptor() throws SQLException {
        return _array.getDescriptor();
    }

    public ORAData create(Datum d, int sqlType) throws SQLException {
        if (d == null)
            return null;
        XxadeoSpAssortmentTbl a = new XxadeoSpAssortmentTbl();
        a._array = new MutableArray(2002, (ARRAY)d, XxadeoSpAssortmentObj.getORADataFactory());
        return a;
    }

    public XxadeoSpAssortmentObj[] getArray() throws SQLException {
        return (XxadeoSpAssortmentObj[])_array.getObjectArray(new XxadeoSpAssortmentObj[_array.length()]);
    }

    public XxadeoSpAssortmentObj[] getArray(long index, int count) throws SQLException {
        return (XxadeoSpAssortmentObj[])_array.getObjectArray(index, new XxadeoSpAssortmentObj[_array.sliceLength(index,count)]);
    }

    public void setArray(XxadeoSpAssortmentObj[] a) throws SQLException {
        _array.setObjectArray(a);
    }

    public void setArray(XxadeoSpAssortmentObj[] a, long index) throws SQLException {
        _array.setObjectArray(a, index);
    }

    public XxadeoSpAssortmentObj getElement(long index) throws SQLException {
        return (XxadeoSpAssortmentObj)_array.getObjectElement(index);
    }

    public void setElement(XxadeoSpAssortmentObj a, long index) throws SQLException {
        _array.setObjectElement(a, index);
    }

    public static String getSQLTypeName() {    return DbSchemaResolverFactory.getResolver().resolveObjectName(_SQL_NAME);} 
    public static int getSQLTypeCode() {    return _SQL_TYPECODE;} 
} // end class
