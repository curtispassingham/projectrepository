
package com.storeportal.common.model.dbwrappers;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.jpub.runtime.MutableStruct;
import oracle.sql.Datum;
import java.sql.SQLException;
import oracle.sql.STRUCT;
import java.sql.Connection;
import oracle.jdbc.OracleTypes;
import oracle.retail.apps.framework.jdbc.DbSchemaResolverFactory;
import oracle.retail.apps.framework.jdbc.OracleTypeAdapter;
/*******************************************************
 * THIS CODE IS AUTO-GENERATED.  DO NOT MODIFY THIS CODE!
 * 
 * Modify the Oracle Type this class was based on and 
 * re-generate the contents of this file using the oracle.retail.apps.framework.jdbc.OracleTypeAdapterGenerator application. 
 * 
 * Generator Version : 1.0
 * Generation Date   : 2018-09-10 14:26:053
 * Oracle Type Name  : XXADEO_SP_SYSTEM_OPTIONS_OBJ
 * Owned by          : XXADEO_SP
 * Requested Schema  : XXADEO_SP
 * Requested DB URL  : jdbc:oracle:thin:@onsdb04:1521/rms16adeo
 * Requested DB User : xxadeo_sp
 *******************************************************/
public class XxadeoSpSystemOptionsObj implements ORAData, ORADataFactory, OracleTypeAdapter {
    public static final String _SQL_NAME = "XXADEO_SP_SYSTEM_OPTIONS_OBJ";

    public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

    public static final int STORE_FORMAT_IDX = 0;
    public static final int EMERGENCY_ROLE_ID_IDX = 1;
    public static final int HELP_URL_IDX = 2;
    protected MutableStruct _struct;
    protected static int[] _sqlType = {2,2,12};
    protected static ORADataFactory[] _factory = new ORADataFactory[3];
    static {
    }
    protected static final XxadeoSpSystemOptionsObj _XxadeoSpSystemOptionsObjFactory = new XxadeoSpSystemOptionsObj();
    public static ORADataFactory getORADataFactory() {
        return _XxadeoSpSystemOptionsObjFactory;
    }

    public XxadeoSpSystemOptionsObj() {
        _struct = new MutableStruct(new Object[3], _sqlType, _factory);
    }

    public Datum toDatum(Connection c) throws SQLException {
        return _struct.toDatum(c, getSQLTypeName()); 
    }

    public ORAData create(Datum d, int sqlType) throws SQLException {
        return create(null, d, sqlType);
    }

    protected ORAData create(XxadeoSpSystemOptionsObj o, Datum d, int sqlType) throws SQLException {
        if (d == null)
            return null;
        if (o == null)
            o = new XxadeoSpSystemOptionsObj();
        o._struct = new MutableStruct((STRUCT)d, _sqlType, _factory);
        return o;
    }
    public final java.math.BigDecimal  getStoreFormat() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(STORE_FORMAT_IDX);
    }

    public final void setStoreFormat(java.math.BigDecimal storeFormat) throws SQLException 
    { 
        _struct.setAttribute(STORE_FORMAT_IDX, storeFormat);
    }

    public final java.math.BigDecimal  getEmergencyRoleId() throws SQLException 
    {
        return (java.math.BigDecimal)_struct.getAttribute(EMERGENCY_ROLE_ID_IDX);
    }

    public final void setEmergencyRoleId(java.math.BigDecimal emergencyRoleId) throws SQLException 
    { 
        _struct.setAttribute(EMERGENCY_ROLE_ID_IDX, emergencyRoleId);
    }

    public final java.lang.String  getHelpUrl() throws SQLException 
    {
        return (java.lang.String)_struct.getAttribute(HELP_URL_IDX);
    }

    public final void setHelpUrl(java.lang.String helpUrl) throws SQLException 
    { 
        _struct.setAttribute(HELP_URL_IDX, helpUrl);
    }

    public static String getSQLTypeName() {    return DbSchemaResolverFactory.getResolver().resolveObjectName(_SQL_NAME);} 
    public static int getSQLTypeCode() {    return _SQL_TYPECODE;} 
} // end class
