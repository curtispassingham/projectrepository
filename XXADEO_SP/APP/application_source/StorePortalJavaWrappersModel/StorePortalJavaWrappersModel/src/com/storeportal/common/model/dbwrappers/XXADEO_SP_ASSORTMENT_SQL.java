
package com.storeportal.common.model.dbwrappers;

import oracle.retail.apps.framework.jdbc.util.SQLParam;
import oracle.retail.apps.framework.jdbc.util.ParamType;
import java.sql.Connection;
import java.sql.SQLException;
import oracle.retail.apps.framework.jdbc.util.AppsDBUtils;
import oracle.retail.apps.framework.jdbc.JdbcRuntimeException;
import java.sql.Types;
import com.storeportal.common.model.dbwrappers.*;

 /**
 * XXADEO_SP_ASSORTMENT_SQL
 * This class contains operations to conveniently call PL/SQL operations.
 * Based on PL/SQL:    XXADEO_SP_ASSORTMENT_SQL
 * Based on Database:  oracle.jdbc.OracleDriver : jdbc:oracle:thin:@onsdb04:1521/rms16adeo
 * Based on DB User:   xxadeo_sp
 * Based on DB Schema: XXADEO_SP
 * Generated on:       13/08/2018 17:54:37
 *
 *
 * WARNING:  THIS IS AN AUTOGENERATED FILE.  DO NOT MODIFY THIS FILE DIRECTLY.
 * IF CHANGES TO THE PL/SQL FUNCTIONALITY IS NEEDED,   MODIFY THE ORIGINAL PL/SQL
 * PACKAGE AND REGENERATE THIS FILE USING plsqljavawrappergenerator.
 *
 */


public class XXADEO_SP_ASSORTMENT_SQL {
         

     /**
     *WARNING: THIS IS AN AUTOGENERATED FILE. DO NOT MODIFY THIS FILE DIRECTLY.
     *IF CHANGES TO THE PL/SQL FUNCTIONALITY IS NEED, MODIFY THE ORIGINAL PL/SQL
     *PACKAGE AND REGENERATE THIS FILE USING plsqljavawrappergenerator.
     */
    public static Boolean XXADEO_ASSORTMENT_BATCH (oracle.jbo.server.DBTransaction con) {
        try{
            Boolean methodReturn = null;
            Boolean plsqlCallReturn = null;
             

             String plsqlCall = //
             " DECLARE " + //
             " l_return BOOLEAN; " +//
             " BEGIN " +//
             " l_return := " +//
             " XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_BATCH (" +//
             " ); " +//
             " ?:= RAF_BOOLEAN_WRAPPER_PKG.BOOLEAN2INT(l_return); " +//
             " END; " ;
            SQLParam ___function_return___SQLParam = new SQLParam(null, ParamType.OUT, Types.INTEGER);
            AppsDBUtils.callAnonymousBlock(con, plsqlCall ,
                                        ___function_return___SQLParam);


            methodReturn = AppsDBUtils.int2boolean(___function_return___SQLParam.getValue());
            return methodReturn;
        }
        catch(JdbcRuntimeException sqlex){
            throw new JdbcRuntimeException(sqlex);
        }
    }



         

     /**
     *WARNING: THIS IS AN AUTOGENERATED FILE. DO NOT MODIFY THIS FILE DIRECTLY.
     *IF CHANGES TO THE PL/SQL FUNCTIONALITY IS NEED, MODIFY THE ORIGINAL PL/SQL
     *PACKAGE AND REGENERATE THIS FILE USING plsqljavawrappergenerator.
     */
    public static XXADEO_ASSORTMENT_CHANGES_Result XXADEO_ASSORTMENT_CHANGES (oracle.jbo.server.DBTransaction con, 
                                        String O_ERROR_MESSAGE, 
                                        XxadeoSpAssortmentTbl I_ASSORTMENT_TBL) {
        try{
            XXADEO_ASSORTMENT_CHANGES_Result methodReturn = null;
            Boolean plsqlCallReturn = null;
             

             String plsqlCall = //
             " DECLARE " + //
             " l_return BOOLEAN; " +//
             " O_ERROR_MESSAGE VARCHAR(32767); "+ //
             " I_ASSORTMENT_TBL TABLE; "+ //
             " BEGIN " +//
             " l_return := " +//
             " XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_CHANGES (" +//
             " ?" +//
             " , "+//
             " ?" +//
             " ); " +//
             " ?:= RAF_BOOLEAN_WRAPPER_PKG.BOOLEAN2INT(l_return); " +//
             " END; " ;
            SQLParam ___function_return___SQLParam = new SQLParam(null, ParamType.OUT, Types.INTEGER);
            SQLParam  O_ERROR_MESSAGESQLParam = new SQLParam(O_ERROR_MESSAGE, ParamType.INOUT, Types.VARCHAR);
            SQLParam  I_ASSORTMENT_TBLSQLParam = new SQLParam(I_ASSORTMENT_TBL, ParamType.IN, Types.ARRAY);
            AppsDBUtils.callAnonymousBlock(con, plsqlCall ,
                                        O_ERROR_MESSAGESQLParam,
                                        I_ASSORTMENT_TBLSQLParam,
                                        ___function_return___SQLParam);


            methodReturn=new XXADEO_ASSORTMENT_CHANGES_Result();
            methodReturn.setReturnValue(AppsDBUtils.int2boolean(___function_return___SQLParam.getValue()));
            methodReturn.setO_ERROR_MESSAGE((String)O_ERROR_MESSAGESQLParam.getValue());
            return methodReturn;
        }
        catch(JdbcRuntimeException sqlex){
            throw new JdbcRuntimeException(sqlex);
        }
    }

             

public static class XXADEO_ASSORTMENT_CHANGES_Result {
     /**
     *WARNING: THIS IS AN AUTOGENERATED INNER CLASS. DO NOT MODIFY THIS FILE DIRECTLY.
     *IF CHANGES TO THE PL/SQL FUNCTIONALITY IS NEED, MODIFY THE ORIGINAL PL/SQL
     *PACKAGE AND REGENERATE THIS FILE USING plsqljavawrappergenerator.
     */
     private  String O_ERROR_MESSAGE =null;

     public void setO_ERROR_MESSAGE(String    O_ERROR_MESSAGE) {
         this.O_ERROR_MESSAGE = O_ERROR_MESSAGE ;
     }
     public String getO_ERROR_MESSAGE() {
         return O_ERROR_MESSAGE;
     }
    
    private Boolean returnValue = null;
    

     public void setReturnValue(Boolean returnValue) {
         this.returnValue=returnValue; 
     }
    

     public Boolean getReturnValue() {
         return returnValue; 
     }

}





         

     /**
     *WARNING: THIS IS AN AUTOGENERATED FILE. DO NOT MODIFY THIS FILE DIRECTLY.
     *IF CHANGES TO THE PL/SQL FUNCTIONALITY IS NEED, MODIFY THE ORIGINAL PL/SQL
     *PACKAGE AND REGENERATE THIS FILE USING plsqljavawrappergenerator.
     */
    public static String XXADEO_ASSORTMENT_CLUSTER_REC (oracle.jbo.server.DBTransaction con, 
                                        String I_ITEM, 
                                        java.math.BigDecimal I_STORE) {
        try{
            String methodReturn = null;
            String plsqlCallReturn = null;
            SQLParam  I_ITEMSQLParam = new SQLParam(I_ITEM, ParamType.IN, Types.VARCHAR);
            SQLParam  I_STORESQLParam = new SQLParam(I_STORE, ParamType.IN, Types.NUMERIC);
            plsqlCallReturn = (String)AppsDBUtils.callStoredFunction(con, 
                                        "XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_CLUSTER_REC", 
                                        Types.VARCHAR,
                                        I_ITEMSQLParam,
                                        I_STORESQLParam);
            methodReturn = plsqlCallReturn;
            return methodReturn;
        }
        catch(JdbcRuntimeException sqlex){
            throw new JdbcRuntimeException(sqlex);
        }
    }



         

     /**
     *WARNING: THIS IS AN AUTOGENERATED FILE. DO NOT MODIFY THIS FILE DIRECTLY.
     *IF CHANGES TO THE PL/SQL FUNCTIONALITY IS NEED, MODIFY THE ORIGINAL PL/SQL
     *PACKAGE AND REGENERATE THIS FILE USING plsqljavawrappergenerator.
     */
    public static String XXADEO_ASSORTMENT_HISTORY (oracle.jbo.server.DBTransaction con, 
                                        String I_ITEM, 
                                        java.math.BigDecimal I_STORE) {
        try{
            String methodReturn = null;
            String plsqlCallReturn = null;
            SQLParam  I_ITEMSQLParam = new SQLParam(I_ITEM, ParamType.IN, Types.VARCHAR);
            SQLParam  I_STORESQLParam = new SQLParam(I_STORE, ParamType.IN, Types.NUMERIC);
            plsqlCallReturn = (String)AppsDBUtils.callStoredFunction(con, 
                                        "XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_HISTORY", 
                                        Types.VARCHAR,
                                        I_ITEMSQLParam,
                                        I_STORESQLParam);
            methodReturn = plsqlCallReturn;
            return methodReturn;
        }
        catch(JdbcRuntimeException sqlex){
            throw new JdbcRuntimeException(sqlex);
        }
    }



         

     /**
     *WARNING: THIS IS AN AUTOGENERATED FILE. DO NOT MODIFY THIS FILE DIRECTLY.
     *IF CHANGES TO THE PL/SQL FUNCTIONALITY IS NEED, MODIFY THE ORIGINAL PL/SQL
     *PACKAGE AND REGENERATE THIS FILE USING plsqljavawrappergenerator.
     */
    public static Boolean XXADEO_ASSORTMENT_UPDATE_BATCH (oracle.jbo.server.DBTransaction con) {
        try{
            Boolean methodReturn = null;
            Boolean plsqlCallReturn = null;
             

             String plsqlCall = //
             " DECLARE " + //
             " l_return BOOLEAN; " +//
             " BEGIN " +//
             " l_return := " +//
             " XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_UPDATE_BATCH (" +//
             " ); " +//
             " ?:= RAF_BOOLEAN_WRAPPER_PKG.BOOLEAN2INT(l_return); " +//
             " END; " ;
            SQLParam ___function_return___SQLParam = new SQLParam(null, ParamType.OUT, Types.INTEGER);
            AppsDBUtils.callAnonymousBlock(con, plsqlCall ,
                                        ___function_return___SQLParam);


            methodReturn = AppsDBUtils.int2boolean(___function_return___SQLParam.getValue());
            return methodReturn;
        }
        catch(JdbcRuntimeException sqlex){
            throw new JdbcRuntimeException(sqlex);
        }
    }



         

     /**
     *WARNING: THIS IS AN AUTOGENERATED FILE. DO NOT MODIFY THIS FILE DIRECTLY.
     *IF CHANGES TO THE PL/SQL FUNCTIONALITY IS NEED, MODIFY THE ORIGINAL PL/SQL
     *PACKAGE AND REGENERATE THIS FILE USING plsqljavawrappergenerator.
     */
    public static XXADEO_ASSORTMENT_VALID_DATE_Result XXADEO_ASSORTMENT_VALID_DATE (oracle.jbo.server.DBTransaction con, 
                                        String O_ERROR_MESSAGE, 
                                        XxadeoSpAssortmentObj I_ASSORTMENT_OBJ) {
        try{
            XXADEO_ASSORTMENT_VALID_DATE_Result methodReturn = null;
            Boolean plsqlCallReturn = null;
            String O_RTK_KEY = null; 
             

             String plsqlCall = //
             " DECLARE " + //
             " l_return BOOLEAN; " +//
             " O_ERROR_MESSAGE VARCHAR(32767); "+ //
             " O_RTK_KEY VARCHAR(32767); "+ //
             " I_ASSORTMENT_OBJ OBJECT; "+ //
             " BEGIN " +//
             " l_return := " +//
             " XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_VALID_DATE (" +//
             " ?" +//
             " , "+//
             " ?" +//
             " , "+//
             " ?" +//
             " ); " +//
             " ?:= RAF_BOOLEAN_WRAPPER_PKG.BOOLEAN2INT(l_return); " +//
             " END; " ;
            SQLParam ___function_return___SQLParam = new SQLParam(null, ParamType.OUT, Types.INTEGER);
            SQLParam  O_ERROR_MESSAGESQLParam = new SQLParam(O_ERROR_MESSAGE, ParamType.INOUT, Types.VARCHAR);
            SQLParam  O_RTK_KEYSQLParam = new SQLParam(O_RTK_KEY, ParamType.OUT, Types.VARCHAR);
            SQLParam  I_ASSORTMENT_OBJSQLParam = new SQLParam(I_ASSORTMENT_OBJ, ParamType.IN, Types.STRUCT);
            AppsDBUtils.callAnonymousBlock(con, plsqlCall ,
                                        O_ERROR_MESSAGESQLParam,
                                        O_RTK_KEYSQLParam,
                                        I_ASSORTMENT_OBJSQLParam,
                                        ___function_return___SQLParam);


            methodReturn=new XXADEO_ASSORTMENT_VALID_DATE_Result();
            methodReturn.setReturnValue(AppsDBUtils.int2boolean(___function_return___SQLParam.getValue()));
            methodReturn.setO_ERROR_MESSAGE((String)O_ERROR_MESSAGESQLParam.getValue());
            methodReturn.setO_RTK_KEY((String)O_RTK_KEYSQLParam.getValue());
            return methodReturn;
        }
        catch(JdbcRuntimeException sqlex){
            throw new JdbcRuntimeException(sqlex);
        }
    }

             

public static class XXADEO_ASSORTMENT_VALID_DATE_Result {
     /**
     *WARNING: THIS IS AN AUTOGENERATED INNER CLASS. DO NOT MODIFY THIS FILE DIRECTLY.
     *IF CHANGES TO THE PL/SQL FUNCTIONALITY IS NEED, MODIFY THE ORIGINAL PL/SQL
     *PACKAGE AND REGENERATE THIS FILE USING plsqljavawrappergenerator.
     */
     private  String O_ERROR_MESSAGE =null;

     public void setO_ERROR_MESSAGE(String    O_ERROR_MESSAGE) {
         this.O_ERROR_MESSAGE = O_ERROR_MESSAGE ;
     }
     public String getO_ERROR_MESSAGE() {
         return O_ERROR_MESSAGE;
     }
     private  String O_RTK_KEY =null;

     public void setO_RTK_KEY(String    O_RTK_KEY) {
         this.O_RTK_KEY = O_RTK_KEY ;
     }
     public String getO_RTK_KEY() {
         return O_RTK_KEY;
     }
    
    private Boolean returnValue = null;
    

     public void setReturnValue(Boolean returnValue) {
         this.returnValue=returnValue; 
     }
    

     public Boolean getReturnValue() {
         return returnValue; 
     }

}







}
