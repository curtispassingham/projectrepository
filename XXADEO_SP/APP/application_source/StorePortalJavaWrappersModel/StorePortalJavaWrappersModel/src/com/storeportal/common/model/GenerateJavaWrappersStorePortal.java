/*
* Fábio Andersen
*
* Copyright (c) 2018 Oracle Development Center. All Rights Reserved.
*
* This software is the confidential and proprietary information of Oracle Development Center.
* ("Confidential Information").
*
*/
package com.storeportal.common.model;

import oracle.adf.share.logging.ADFLogger;

/**
 * Generate Java classes to map database objects.
 */
public class GenerateJavaWrappersStorePortal {
    // class
    private static final Class CLASS = GenerateJavaWrappersStorePortal.class;
    private static final String CLASS_NAME = CLASS.getName();
    // log
    private static final ADFLogger LOG = ADFLogger.createADFLogger(GenerateJavaWrappersStorePortal.class);

    /**
     * Empty constructor.
     */
    private GenerateJavaWrappersStorePortal() {
    }

    /**
     * Main method.
     *
     * @param args Arguments list.
     */
    public static void main(String[] args) {
        LOG.entering(CLASS_NAME, "main");
        String jdbcUrl = "jdbc:oracle:thin:@onsdb04:1521/rms16adeo";
        String jdbcUser = "xxadeo_sp";
        String jdbcPassword = "oracle";
        String schemaOwner = "xxadeo_sp";
        String plsqlSourceJavaPackage = "com.storeportal.common.model.dbwrappers";
        String slfPlsqlSourceNames = "XXADEO_SP_RMS_WRP";
        String noTypeGenOutput = "false";
        StorePortalJavaWrappersUtil.generateDatabaseObjects(jdbcUrl, jdbcUser, jdbcPassword, schemaOwner,
                                                    slfPlsqlSourceNames, plsqlSourceJavaPackage,
                                                    noTypeGenOutput);
        LOG.exiting(CLASS_NAME, "main");
    }
}
