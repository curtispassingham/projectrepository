/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO SP                                                      */
/* DESCRIPTION - Table "XXADEO_SP_SYSTEM_OPTIONS" for ADEO SP                 */
/******************************************************************************/
-- drop table
begin
  execute immediate 'drop table XXADEO_SP_SYSTEM_OPTIONS cascade constraints';
exception
  when others then
    null;
end;
/
-- create table
CREATE TABLE XXADEO_SP_SYSTEM_OPTIONS(
  STORE_FORMAT       NUMBER(4),
  EMERGENCY_ROLE_ID  NUMBER(18)
);
-- Add comments to the columns
COMMENT ON COLUMN XXADEO_SP_SYSTEM_OPTIONS.STORE_FORMAT is 'Contains the number which uniquely identifies the store format.'
;
comment on column XXADEO_SP_SYSTEM_OPTIONS.EMERGENCY_ROLE_ID is 'Internal identifier representing the role for emergency price changes.'
;
