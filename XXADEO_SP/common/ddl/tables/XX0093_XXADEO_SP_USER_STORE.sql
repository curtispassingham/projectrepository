/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Fabio Andersen                                               */
/* PROJECT     - ADEO SP                                                      */
/* DESCRIPTION - Table "XXADEO_SP_USER_STORE" for ADEO SP                     */
/******************************************************************************/
-- drop table
begin
  execute immediate 'drop table XXADEO_SP_USER_STORE cascade constraints';
exception
  when others then
    null;
end;
/
-- create table
CREATE TABLE XXADEO_SP_USER_STORE(
  USER_NAME         VARCHAR2(10)  not null,
  STORE_ID     NUMBER(20)    not null
);
-- primary key
ALTER TABLE XXADEO_SP_USER_STORE ADD CONSTRAINT PK_XXADEO_SP_USER_STORE PRIMARY KEY (USER_NAME, STORE_ID);
