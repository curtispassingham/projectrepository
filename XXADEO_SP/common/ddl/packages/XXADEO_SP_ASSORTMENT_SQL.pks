create or replace package XXADEO_SP_ASSORTMENT_SQL is

/********************************************************************************/
/* CREATE DATE - Julho 2018                                                     */
/* CREATE USER - Felipe Villa                                                   */
/*               Pedro Miranda                                                  */
/* PROJECT     - ADEO                                                           */
/* DESCRIPTION - Package "XXADEO_SP_ASSORTMENT_SQL" for Assortment              */
/********************************************************************************/

----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_ITEMS_SEARCH(I_search_control          IN VARCHAR2 DEFAULT 'N',
                                          I_store                   IN XXADEO_SP_V_ASSORTMENT.STORE%TYPE DEFAULT NULL,
                                          I_group_no                IN XXADEO_SP_V_GROUPS.GROUP_NO%TYPE DEFAULT NULL,
                                          I_dept                    IN XXADEO_SP_V_DEPS.DEPT%TYPE DEFAULT NULL,
                                          I_class                   IN XXADEO_SP_V_CLASS.CLASS%TYPE DEFAULT NULL,
                                          I_subclass                IN XXADEO_SP_V_SUBCLASS.SUBCLASS%TYPE DEFAULT NULL,
                                          I_item                    IN XXADEO_SP_V_ITEM_MASTER.ITEM%TYPE DEFAULT NULL,
                                          I_item_desc               IN XXADEO_SP_V_ITEM_MASTER.ITEM_DESC%TYPE DEFAULT NULL,
                                          --
                                          I_assortment_mode         IN VARCHAR2 DEFAULT NULL, --list
                                          I_range_letter            IN VARCHAR2 DEFAULT NULL, --list
                                          I_hq_recommendation       IN VARCHAR2 DEFAULT NULL, --list
                                          I_store_choice            IN VARCHAR2 DEFAULT NULL, --list
                                          I_supplier_site           IN XXADEO_SP_V_SUPS.SUPPLIER%TYPE DEFAULT NULL,
                                          --
                                          I_not_modifiable_ind      IN VARCHAR2 DEFAULT NULL,
                                          I_season_item_ind         IN VARCHAR2 DEFAULT NULL,
                                          I_related_items           IN VARCHAR2 DEFAULT NULL,
                                          I_pack_component          IN VARCHAR2 DEFAULT NULL,
                                          --
                                          I_assrt_start_date_low    IN XXADEO_SP_V_ASSORTMENT_STORE.START_DATE%TYPE DEFAULT NULL,
                                          I_assrt_start_date_high   IN XXADEO_SP_V_ASSORTMENT_STORE.START_DATE%TYPE DEFAULT NULL,
                                          I_assrt_end_date_low      IN XXADEO_SP_V_ASSORTMENT_STORE.END_DATE%TYPE DEFAULT NULL,
                                          I_assrt_end_date_high     IN XXADEO_SP_V_ASSORTMENT_STORE.END_DATE%TYPE DEFAULT NULL,
                                          I_omni_cs_start_date_low  IN DATE DEFAULT NULL,
                                          I_omni_cs_start_date_high IN DATE DEFAULT NULL,
                                          I_omni_cs_end_date_low    IN DATE DEFAULT NULL,
                                          I_omni_cs_end_date_high   IN DATE DEFAULT NULL,
                                          I_response_end_date_low   IN XXADEO_SP_V_ASSORTMENT_STORE.RESPONSE_DATE%TYPE DEFAULT NULL,
                                          I_response_end_date_high  IN XXADEO_SP_V_ASSORTMENT_STORE.RESPONSE_DATE%TYPE DEFAULT NULL)
    RETURN XXADEO_SP_ASSORTMENT_TBL
    PIPELINED;

----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_VALID_DATE(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                                        O_rtk_key           OUT VARCHAR2,
                                        I_assortment_obj  IN     XXADEO_SP_ASSORTMENT_OBJ)
    RETURN BOOLEAN;

----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_CHANGES(O_error_message  IN OUT LOGGER_LOGS.TEXT%TYPE,
                                     I_assortment_tbl   IN XXADEO_SP_ASSORTMENT_TBL)
    RETURN BOOLEAN;

----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_TBL_PROCESS RETURN BOOLEAN;
----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_BATCH(I_stage IN VARCHAR2) RETURN BOOLEAN;
----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_CLUSTER_REC(I_item          IN XXADEO_SP_V_ITEMS_ASSORTMENT.ITEM%TYPE DEFAULT NULL,
                                         I_store         IN XXADEO_SP_V_ITEMS_ASSORTMENT.STORE%TYPE,
                                         I_start_date    IN XXADEO_SP_V_ITEMS_ASSORTMENT.ASSORTMENT_START_DATE%TYPE,
                                         I_end_date      IN XXADEO_SP_V_ITEMS_ASSORTMENT.ASSORTMENT_END_DATE%TYPE)
    RETURN number;
----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_HISTORY(I_item          IN XXADEO_SP_V_ITEMS_ASSORTMENT.ITEM%TYPE,
                                     I_store         IN XXADEO_SP_V_ITEMS_ASSORTMENT.STORE%TYPE)
    RETURN number;

end XXADEO_SP_ASSORTMENT_SQL;
/
