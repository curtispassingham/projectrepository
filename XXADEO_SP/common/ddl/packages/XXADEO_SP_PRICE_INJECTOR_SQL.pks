CREATE OR REPLACE PACKAGE XXADEO_SP_PRICE_INJECTOR_SQL AS
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - Jul 2018                                                     */
/* CREATE USER - Sergio Barbosa                                               */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package "XXADEO_SP_PRICE_INJECTOR_SQL"                       */
/******************************************************************************/
--------------------------------------------------------------------------------
FUNCTION PERSIST_STAGE_PRICE_CHANGES(O_error_message       IN OUT LOGGER_LOGS.TEXT%TYPE,
                                     I_price_change_tbl    IN     XXADEO_SP_RPM_PRICE_CHANGE_TBL,
                                     I_user_name           IN     XXADEO_RPM_STAGE_PRICE_CHANGE.CREATE_ID%TYPE DEFAULT GET_USER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END XXADEO_SP_PRICE_INJECTOR_SQL;
/
