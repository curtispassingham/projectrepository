CREATE OR REPLACE PACKAGE XXADEO_SP_ITEM_ATTRIB_SQL IS

  -- Author  : SNBARBOS
  -- Created : 29/06/2018
  -- Purpose : Package to support Store Portal item operations

  ---------------------------------------------------------------------------------------------
  -- Function Name: CHECK_RELATED_ITEMS_EXISTS
  -- Purpose      : Checks if Relate Items exist for the input Item.
  ---------------------------------------------------------------------------------------------
  FUNCTION CHECK_RELATED_ITEMS_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists        IN OUT BOOLEAN,
                                      I_item          IN V_ITEM_MASTER.ITEM%TYPE)
    RETURN BOOLEAN;
  ---------------------------------------------------------------------------------------------

END XXADEO_SP_ITEM_ATTRIB_SQL;
/
