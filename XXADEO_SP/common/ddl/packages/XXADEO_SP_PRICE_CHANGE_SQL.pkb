create or replace package body XXADEO_SP_PRICE_CHANGE_SQL AS
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Paulo Mamede                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package to run XXADEO_PRICE_CHANGE_SEARCH function           */
/******************************************************************************/
   FUNCTION XXADEO_SP_PRICE_CHANGE_SEARCH(I_search_control    IN VARCHAR2                         DEFAULT 'N',
                                          I_item              IN V_ITEM_MASTER.ITEM%TYPE          DEFAULT NULL,
                                          I_group             IN V_GROUPS.GROUP_NO%TYPE           DEFAULT NULL,
                                          I_dept              IN V_DEPS.DEPT%TYPE                 DEFAULT NULL,
                                          I_class             IN V_ITEM_MASTER.CLASS%TYPE         DEFAULT NULL,
                                          I_subclass          IN V_ITEM_MASTER.SUBCLASS%TYPE      DEFAULT NULL,
                                          I_item_desc         IN V_ITEM_MASTER.SHORT_DESC%TYPE    DEFAULT NULL,
                                          I_ean               IN NUMBER                           DEFAULT NULL,
                                          I_item_type         IN VARCHAR2                         DEFAULT NULL,
                                          I_typology          IN VARCHAR2                         DEFAULT NULL,
                                          I_subtypology       IN VARCHAR2                         DEFAULT NULL,
                                          I_ispack            IN VARCHAR2                         DEFAULT NULL,
                                          I_supp_site         IN V_SUPS.SUPPLIER_PARENT%TYPE      DEFAULT NULL,
                                          I_reason            IN NUMBER                           DEFAULT NULL,
                                          I_price_date_start  IN DATE                             DEFAULT NULL,
                                          I_price_date_end    IN DATE                             DEFAULT NULL,
                                          I_store             IN STORE.STORE%TYPE                 DEFAULT NULL)
                                          --
    RETURN XXADEO_PRICE_RESULT_TBL
    PIPELINED AS
    --
    L_program               VARCHAR2(64) := 'XXADEO_SP_PRICE_CHANGE_SQL.XXADEO_SP_PRICE_CHANGE_SEARCH';
    L_error_message         VARCHAR2(2000);
    --
    L_string_query          VARCHAR2(20000) := NULL;
    L_sys_refcur            SYS_REFCURSOR;
    --
    L_PriceChangeSearchTbl  XXADEO_PRICE_RESULT_TBL;
    --
    BEGIN
    --
    if I_search_control = 'N' then
      --
      RETURN;
      --
    end if;
    --
    EXECUTE IMMEDIATE ('ALTER SESSION SET NLS_DATE_FORMAT = ''DD-MM-YYYY''');
    --
    L_string_query := q'{with t_binds as
                            (select :1  db_item,
                                    :2  db_group,
                                    :3  db_dept,
                                    :4  db_class,
                                    :5  db_subclass,
                                    :6  db_item_desc,
                                    :7  db_ean,
                                    :8  db_item_type,
                                    :9  db_typology,
                                    :10 db_subtypology,
                                    :11 db_ispack,
                                    :12 db_supp_site,
                                    :13 db_reason,
                                    :14 db_price_date_start,
                                    :15 db_price_date_end,
                                    :16 db_store
                               from dual)
                              select new XXADEO_PRICE_CHANGE_OBJ(group_no,
                                                                 dept,
                                                                 class,
                                                                 subclass,
                                                                 item,
                                                                 description,
                                                                 store_price,
                                                                 zone_price,
                                                                 effective_date,
                                                                 new_price,
                                                                 new_apply_date,
                                                                 cfa,
                                                                 origin)
                                from XXADEO_SP_V_PRICE_SEARCH i,
                                     t_binds b
                               where 1 = 1
                                     }';
                                     --
    --
    -- store
    if I_store is not null then
    --
      L_string_query := L_string_query ||
                        q'{ and i.store = b.db_store }';
                      --
    end if;
    --
    -- group
    if I_group is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and (i.group_no = b.db_group) }';
      --
    end if;
    --
    -- dept
    if I_dept is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and (i.dept = b.db_dept) }';
                        --
    end if;
    --
    -- class
    if I_class is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and (i.class = b.db_class) }';
                        --
    end if;

    --subclass
    if I_subclass is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and (i.subclass = b.db_subclass) }';
                        --
    end if;
    --
    -- item desc
    --
    if I_item_desc is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and upper(i.description) like upper('%}' || I_item_desc || q'{%') }';
                        --
    end if;
    --
    -- EAN
    --
    if I_ean is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and (i.item in ( select nvl(im2.item_grandparent,im2.item_parent)
                                               from xxadeo_sp_v_item_master im2
                                              where im2.item       = b.db_ean
                                                and im2.item_level > im2.tran_level)) }';
                                                --
    end if;
    --
    -- Price Start Date
    --
    if I_price_date_start is not null or I_price_date_end is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and trunc(i.new_apply_date) between trunc(b.db_price_date_start) and trunc(NVL(b.db_price_date_end,b.db_price_date_start)) }';
      --
      /*if I_price_date_start is not null and I_price_date_end is null then
         L_string_query := L_string_query ||
                        q'{ and trunc(i.new_apply_date) >= trunc(b.db_price_date_start) }';

      end if;
      --
      if  I_price_date_end is not null and I_price_date_start is null then
         L_string_query := L_string_query ||
                        q'{ and trunc(i.new_apply_date) <= trunc(b.db_price_date_end) }';

      end if;
      --
      if  I_price_date_end is not null and I_price_date_start is not null then
         L_string_query := L_string_query ||
                        q'{ and trunc(i.new_apply_date) between trunc(b.db_price_date_start) and trunc(b.db_price_date_end) }';
                        dbms_output.put_line(L_string_query);

      end if;*/
                        --
    end if;
    --
    -- supplier site
    --
    if I_supp_site is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and exists ( select 1
                                           from xxadeo_sp_v_item_supplier s
                                           where i.item     = s.item and
                                                 s.supplier = b.db_supp_site ) }';
                                                 --
    end if;
    --
    -- item
    --
    if I_item is not null then
     --
     L_string_query := L_string_query ||
                       q'{ and i.item = b.db_item }';
                       --
    end if;
    --
    -- item type
    --
    if I_item_type is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.item_type = CASE b.db_item_type
                                              WHEN 'P' THEN 'Y'
                                              ELSE 'N'
                                               END }';
                                              --q'{ and decode(i.item_type,'P','Y','N') = db.item_type }';
                                              --
    end if;
    --
    -- is_pack
    --
    if I_ispack is not null then
      --
      if I_ispack = 'Y' then
      --
         L_string_query := L_string_query ||
                           q'{ and exists (select 1
                                             from xxadeo_sp_v_packitem pi
                                            where pi.item = i.item)}';
                        --q'{ and i.ispack = db.ISPACK }';
                        --
      else
        --
        L_string_query := L_string_query || q'{ and not exists (select 1
                                                                  from xxadeo_sp_v_packitem pi
                                                                 where pi.pack_no = i.item)}';
                                                                 --
      end if;
      --
    end if;
    --
    -- reason code
    --
    if I_reason is not null then
     --
     L_string_query := L_string_query ||
                       q'{ and exists (select 1
                                         from xxadeo_sp_v_rpm_price_change pc,
                                              (select zone_id, location from xxadeo_sp_v_rpm_zone_location where location = b.db_store) rz1
                                        where i.item           = pc.item
                                          and (pc.location     = b.db_store
                                           or pc.zone_id       = rz1.zone_id)
                                          and pc.reason_code   = b.db_reason
                                          and pc.state         = 'pricechange.state.approved'
                                          ) }';
     --
    end if;
    --
    -- typology
    --
    if I_typology is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and exists ( select  1
                                           from xxadeo_sp_v_mom_dvm mom,
                                                xxadeo_sp_v_uda_item_lov u
                                          where mom.value_1   = u.uda_id
                                            and u.item        = i.item
                                            and mom.parameter = 'ITEM_TYPE') }';
      --
    end if;
    --
    -- sub_typology
    --
    if I_subtypology is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and exists ( select  1
                                           from xxadeo_sp_v_mom_dvm mom,
                                                xxadeo_sp_v_uda_item_lov u
                                          where mom.value_1   = u.uda_id
                                            and u.item        = i.item
                                            and mom.parameter = 'ITEM_SUBTYPE') }';
                                            --
    end if;
    --
    -- bulk query to table type
    --
    open L_sys_refcur for L_string_query
     using I_item,
           I_group,
           I_dept,
           I_class,
           I_subclass,
           I_item_desc,
           I_ean,
           I_item_type,
           I_typology,
           I_subtypology,
           I_ISPACK,
           I_supp_site,
           I_reason,
           I_price_date_start,
           I_price_date_end,
           I_store;
    loop
      --
      fetch L_sys_refcur bulk collect
      into L_PriceChangeSearchTbl;
      exit when L_PriceChangeSearchTbl.count = 0;
      --
      -- pipe data form collection
      --
      for i in 1 .. L_PriceChangeSearchTbl.count loop
        --
        pipe row(XXADEO_PRICE_CHANGE_OBJ(L_PriceChangeSearchTbl(i).group_no,
                                         L_PriceChangeSearchTbl(i).dept,
                                         L_PriceChangeSearchTbl(i).class,
                                         L_PriceChangeSearchTbl(i).subclass,
                                         L_PriceChangeSearchTbl(i).item,
                                         L_PriceChangeSearchTbl(i).description,
                                         L_PriceChangeSearchTbl(i).store_price,
                                         L_PriceChangeSearchTbl(i).zone_price,
                                         L_PriceChangeSearchTbl(i).effective_date,
                                         L_PriceChangeSearchTbl(i).new_price,
                                         L_PriceChangeSearchTbl(i).new_apply_date,
                                         L_PriceChangeSearchTbl(i).cfa,
                                         L_PriceChangeSearchTbl(i).origin));
      end loop;
      --
    end loop;
    --
    close L_sys_refcur;
    --
    return;
    --
   EXCEPTION
    --
    WHEN OTHERS THEN
       L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
                                             --
      RETURN;
      --
   END XXADEO_SP_PRICE_CHANGE_SEARCH;
--------------------------------------------------------------------
END XXADEO_SP_PRICE_CHANGE_SQL;
/
