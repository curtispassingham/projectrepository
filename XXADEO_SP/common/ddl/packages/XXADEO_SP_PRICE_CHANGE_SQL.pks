CREATE OR REPLACE PACKAGE XXADEO_SP_PRICE_CHANGE_SQL AS
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Paulo Mamede                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package to run XXADEO_PRICE_CHANGE_SEARCH function           */
/******************************************************************************/
   FUNCTION XXADEO_SP_PRICE_CHANGE_SEARCH(I_search_control    IN VARCHAR2                      DEFAULT 'N',
                                          I_item              IN V_ITEM_MASTER.ITEM%TYPE       DEFAULT NULL,
                                          I_group             IN V_GROUPS.GROUP_NO%TYPE        DEFAULT NULL,
                                          I_dept              IN V_DEPS.DEPT%TYPE              DEFAULT NULL,
                                          I_class             IN V_ITEM_MASTER.CLASS%TYPE      DEFAULT NULL,
                                          I_subclass          IN V_ITEM_MASTER.SUBCLASS%TYPE   DEFAULT NULL,
                                          I_item_desc         IN V_ITEM_MASTER.SHORT_DESC%TYPE DEFAULT NULL,
                                          I_ean               IN NUMBER                        DEFAULT NULL,
                                          I_item_type         IN VARCHAR2                      DEFAULT NULL,
                                          I_typology          IN VARCHAR2                      DEFAULT NULL,
                                          I_subtypology       IN VARCHAR2                      DEFAULT NULL,
                                          I_ispack            IN VARCHAR2                      DEFAULT NULL,
                                          I_supp_site         IN V_SUPS.SUPPLIER_PARENT%TYPE   DEFAULT NULL,
                                          I_reason            IN NUMBER                        DEFAULT NULL,
                                          I_price_date_start  IN DATE                          DEFAULT NULL,
                                          I_price_date_end    IN DATE                          DEFAULT NULL,
                                          I_store             IN STORE.STORE%TYPE              DEFAULT NULL)
                                          --
   RETURN XXADEO_PRICE_RESULT_TBL
   PIPELINED ;
   --
END XXADEO_SP_PRICE_CHANGE_SQL;
------------------------------------------------------------------------------------------------------------------------------
/
