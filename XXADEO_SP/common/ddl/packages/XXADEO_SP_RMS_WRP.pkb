CREATE OR REPLACE PACKAGE BODY XXADEO_SP_RMS_WRP AS

--------------------------------------------------------------------------------
FUNCTION GET_USER_LANGUAGE
  return NUMBER is
  --
  L_user_lang   NUMBER;
  --
begin
  --
  L_user_lang := LANGUAGE_SQL.GET_USER_LANGUAGE();
  --
  return L_user_lang;
  --
end;
--------------------------------------------------------------------------------
END XXADEO_SP_RMS_WRP;
/
