CREATE OR REPLACE PACKAGE BODY XXADEO_SP_PRICE_INJECTOR_SQL AS

------------------------------------------------------------------------------------------------
FUNCTION PERSIST_STAGE_PRICE_CHANGES(O_error_message       IN OUT LOGGER_LOGS.TEXT%TYPE,
                                     I_price_change_tbl    IN     XXADEO_SP_RPM_PRICE_CHANGE_TBL,
                                     I_user_name           IN     XXADEO_RPM_STAGE_PRICE_CHANGE.CREATE_ID%TYPE DEFAULT GET_USER)
RETURN BOOLEAN IS
  --
  L_program        VARCHAR2(255) := 'XXADEO_SP_PRICE_INJECTOR_SQL.PERSIST_STAGE_PRICE_CHANGES';
  --
BEGIN
  --
  forall price_change_rec in I_price_change_tbl.first..I_price_change_tbl.last SAVE EXCEPTIONS
  insert into XXADEO_RPM_STAGE_PRICE_CHANGE(   xxadeo_process_id,
                                               stage_price_change_id,
                                               reason_code,
                                               item,
                                               location,
                                               zone_node_type,
                                               effective_date,
                                               change_type,
                                               change_amount,
                                               change_selling_uom,
                                               null_multi_ind,
                                               ignore_constraints,
                                               auto_approve_ind,
                                               status,
                                               vendor_funded_ind,
                                               create_id,
                                               create_datetime )
                                       select  --xxadeo_process_id
                                               xxadeo_price_process_id_seq.nextval,
                                               --stage_price_change_id
                                               xxadeo_price_change_seq.nextval,
                                               --reason_code
                                               i_price_change_tbl(price_change_rec).reason_code,
                                               --item
                                               i_price_change_tbl(price_change_rec).item,
                                               --location
                                               i_price_change_tbl(price_change_rec).location,
                                               --zone_node_type
                                               rpm_constants.zone_node_type_store,
                                               --effective_date
                                               trunc(i_price_change_tbl(price_change_rec).effective_date),
                                               --change_type
                                               rpm_constants.retail_fixed_amount_value,
                                               --change_amount
                                               i_price_change_tbl(price_change_rec).change_amount,
                                               --'EA',
                                               NVL(im.curr_selling_uom,im.standard_uom),
                                               --null_multi_ind
                                               0,
                                               --ignore_constraints
                                               1,
                                               --auto_approve_ind
                                               1,
                                               --status
                                               xxadeo_rms.xxadeo_rpm_constants_sql.action_n,
                                               -- vendor_funded_ind
                                               0,
                                               --create_id
                                               get_app_rms_user,
                                               --create_datetime
                                               sysdate
                                         from xxadeo_sp_v_item_master im
                                        where i_price_change_tbl(price_change_rec).item = im.item;
                                        --

  RETURN TRUE;
  --
EXCEPTION
  --
  when OTHERS then
    --
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
    --
    RETURN FALSE;

END;
------------------------------------------------------------------------------------------------
END XXADEO_SP_PRICE_INJECTOR_SQL;
/
