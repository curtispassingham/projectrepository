CREATE OR REPLACE PACKAGE BODY XXADEO_SP_ITEM_ATTRIB_SQL IS
--------------------------------------------------------------------------------
FUNCTION CHECK_RELATED_ITEMS_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists        IN OUT BOOLEAN,
                                    I_item          IN V_ITEM_MASTER.ITEM%TYPE)
  RETURN BOOLEAN IS
  ---
  L_program     VARCHAR2(64) := 'XXADEO_SP_ITEM_SQL.CHECK_RELATED_ITEMS_EXISTS';
  L_dummy_fetch VARCHAR2(1);
  --
  cursor C_CHECK_EXISTS is
    select 'x'
      from xxadeo_sp_v_related_item
     where item = I_item
       and rownum = 1;
       --
BEGIN
  O_exists := TRUE;
  ---
  if I_item is NULL then
    O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_item',
                                          L_program,
                                          NULL);
    return FALSE;
  end if;
  ---
  SQL_LIB.SET_MARK('OPEN',
                   'C_CHECK_EXISTS',
                   'Item : ' || I_item,
                   TO_CHAR(SQLCODE));
                   --
  open C_CHECK_EXISTS;
  --
  SQL_LIB.SET_MARK('FETCH',
                   'C_CHECK_EXISTS',
                   'Item : ' || I_item,
                   TO_CHAR(SQLCODE));
                   --
  fetch C_CHECK_EXISTS
    into L_dummy_fetch;
  --
  if C_CHECK_EXISTS%NOTFOUND then
    O_exists := FALSE;
  end if;
  --
  SQL_LIB.SET_MARK('CLOSE',
                   'C_CHECK_EXISTS',
                   'Item : ' || I_item,
                   TO_CHAR(SQLCODE));
                   --
  close C_CHECK_EXISTS;
  ---
  return TRUE;

EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    return FALSE;

END CHECK_RELATED_ITEMS_EXISTS;
--------------------------------------------------------------------------------
end XXADEO_SP_ITEM_ATTRIB_SQL;
/
