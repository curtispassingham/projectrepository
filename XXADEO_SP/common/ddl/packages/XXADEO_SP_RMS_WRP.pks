CREATE OR REPLACE PACKAGE XXADEO_SP_RMS_WRP AS
--------------------------------------------------------------------------------
/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Package to wrap and run RMS packages and functions           */
/******************************************************************************/
--------------------------------------------------------------------------------
FUNCTION GET_USER_LANGUAGE
  return NUMBER;
--------------------------------------------------------------------------------
END XXADEO_SP_RMS_WRP;
/
