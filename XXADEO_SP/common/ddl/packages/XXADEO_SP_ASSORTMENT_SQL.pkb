create or replace package body XXADEO_SP_ASSORTMENT_SQL is

--------------------------------------------------------------------------------
--   GLOBAL VARIABLES
--------------------------------------------------------------------------------
  Lp_user_id      varchar2(30) := nvl(sys_context('RETAIL_CTX', 'APP_USER_ID'),user);
  g_yes		        varchar2(1)  := 'Y';
  g_no		        varchar2(1)  := 'N';
  g_stage_pre     varchar2(10) := 'PRE';
  g_stage_pos     varchar2(10) := 'POS';

----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_ITEMS_SEARCH(I_search_control          IN VARCHAR2 DEFAULT 'N',
                                          I_store                   IN XXADEO_SP_V_ASSORTMENT.STORE%TYPE DEFAULT NULL,
                                          I_group_no                IN XXADEO_SP_V_GROUPS.GROUP_NO%TYPE DEFAULT NULL,
                                          I_dept                    IN XXADEO_SP_V_DEPS.DEPT%TYPE DEFAULT NULL,
                                          I_class                   IN XXADEO_SP_V_CLASS.CLASS%TYPE DEFAULT NULL,
                                          I_subclass                IN XXADEO_SP_V_SUBCLASS.SUBCLASS%TYPE DEFAULT NULL,
                                          I_item                    IN XXADEO_SP_V_ITEM_MASTER.ITEM%TYPE DEFAULT NULL,
                                          I_item_desc               IN XXADEO_SP_V_ITEM_MASTER.ITEM_DESC%TYPE DEFAULT NULL,
                                          --
                                          I_assortment_mode         IN VARCHAR2 DEFAULT NULL, --list
                                          I_range_letter            IN VARCHAR2 DEFAULT NULL, --list
                                          I_hq_recommendation       IN VARCHAR2 DEFAULT NULL, --list
                                          I_store_choice            IN VARCHAR2 DEFAULT NULL, --list
                                          I_supplier_site           IN XXADEO_SP_V_SUPS.SUPPLIER%TYPE DEFAULT NULL,
                                          --
                                          I_not_modifiable_ind      IN VARCHAR2 DEFAULT NULL,
                                          I_season_item_ind         IN VARCHAR2 DEFAULT NULL,
                                          I_related_items           IN VARCHAR2 DEFAULT NULL,
                                          I_pack_component          IN VARCHAR2 DEFAULT NULL,
                                          --
                                          I_assrt_start_date_low    IN XXADEO_SP_V_ASSORTMENT_STORE.START_DATE%TYPE DEFAULT NULL,
                                          I_assrt_start_date_high   IN XXADEO_SP_V_ASSORTMENT_STORE.START_DATE%TYPE DEFAULT NULL,
                                          I_assrt_end_date_low      IN XXADEO_SP_V_ASSORTMENT_STORE.END_DATE%TYPE DEFAULT NULL,
                                          I_assrt_end_date_high     IN XXADEO_SP_V_ASSORTMENT_STORE.END_DATE%TYPE DEFAULT NULL,
                                          I_omni_cs_start_date_low  IN DATE DEFAULT NULL,
                                          I_omni_cs_start_date_high IN DATE DEFAULT NULL,
                                          I_omni_cs_end_date_low    IN DATE DEFAULT NULL,
                                          I_omni_cs_end_date_high   IN DATE DEFAULT NULL,
                                          I_response_end_date_low   IN XXADEO_SP_V_ASSORTMENT_STORE.RESPONSE_DATE%TYPE DEFAULT NULL,
                                          I_response_end_date_high  IN XXADEO_SP_V_ASSORTMENT_STORE.RESPONSE_DATE%TYPE DEFAULT NULL)
    RETURN XXADEO_SP_ASSORTMENT_TBL
    PIPELINED AS
    --
    L_program       VARCHAR2(64) := 'XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_ITEMS_SEARCH';
    L_error_message VARCHAR2(2000);
    --
    L_items_rec     XXADEO_SP_ASSORTMENT_TBL;
    L_string_query  VARCHAR2(20000) := NULL;
    L_sys_refcur    SYS_REFCURSOR;
    --
    L_assortment_mode_list    xxadeo_sp_list_type_tbl;
    L_range_letter_list       xxadeo_sp_list_type_tbl;
    L_hq_recommendation_list  xxadeo_sp_list_type_tbl;
    L_store_choice_list       xxadeo_sp_list_type_tbl;
    --
  BEGIN
    --
    if I_search_control = 'N' then
      --
      return;
      --
    end if;
    --
    EXECUTE IMMEDIATE ('ALTER SESSION SET NLS_DATE_FORMAT = ''DD-MM-YYYY''');
    --
    L_string_query := q'{with t_binds as
                            (select :1  db_store,
                                    :2  db_group_no,
                                    :3  db_dept,
                                    :4  db_class,
                                    :5  db_subclass,
                                    :6  db_item,
                                    :7  db_item_desc,
                                    :8  db_assortment_mode,
                                    :9  db_range_letter,
                                    :10 db_hq_recommendation,
                                    :11 db_store_choice,
                                    :12 db_supplier_site,
                                    :13 db_not_modifiable_ind,
                                    :14 db_season_item_ind,
                                    :15 db_related_items,
                                    :16 db_pack_component,
                                    :17 db_assrt_start_date_low,
                                    :18 db_assrt_start_date_high,
                                    :19 db_assrt_end_date_low,
                                    :20 db_assrt_end_date_high,
                                    :21 db_omni_cs_start_date_low,
                                    :22 db_omni_cs_start_date_high,
                                    :23 db_omni_cs_end_date_low,
                                    :24 db_omni_cs_end_date_high,
                                    :25 db_response_end_date_low,
                                    :26 db_response_end_date_high
                               from dual)
                              select new XXADEO_SP_ASSORTMENT_OBJ(store,
                                                                  group_no,
                                                                  dept,
                                                                  class,
                                                                  subclass,
                                                                  item,
                                                                  item_desc,
                                                                  retail_price,
                                                                  assortment_mode,
                                                                  range_letter,
                                                                  grading,
                                                                  cluster_size,
                                                                  cs_recommendation,
                                                                  hq_recommendation,
                                                                  current_regular_asso,
                                                                  store_choice,
                                                                  supplier_site,
                                                                  assortment_start_date,
                                                                  assortment_end_date,
                                                                  omni_cs_start_date,
                                                                  omni_cs_end_date,
                                                                  response_end_date,
                                                                  pack_component,
                                                                  substitute_item,
                                                                  related_items,
                                                                  comment_cp,
                                                                  season_item_ind,
                                                                  not_modifiable_ind,
                                                                  validated_date,
                                                                  update_line_interface,
                                                                  store_format,
                                                                  catman_status,
                                                                  history_flag,
                                                                  cluster_popup_flag
                                                                  /*xxadeo_sp_assortment_sql.xxadeo_assortment_history(i_item  => i.item,
                                                                                                                     i_store => i.store),
                                                                  xxadeo_sp_assortment_sql.xxadeo_assortment_cluster_rec(i_item       => i.item,
                                                                                                                         i_store      => i.store,
                                                                                                                         i_start_date => i.assortment_start_date,
                                                                                                                         i_end_date   => i.assortment_end_date)*/)
                                      from XXADEO_SP_V_ITEMS_ASSORTMENT i,
                                           t_binds b
                                      where 1 = 1
                                      and nvl(i.assortment_end_date, to_date('99990101', 'yyyymmdd')) >= get_vdate
                                      }';
    --
    -- Store
    if I_store is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.store = b.db_store }';
      --
    end if;
    --
    -- Group
    if I_group_no is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.group_no = b.db_group_no }';
      --
    end if;
    --
    -- Department
    if I_dept is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.dept = b.db_dept }';
      --
    end if;
    --
    -- Class
    if I_class is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.class = b.db_class }';
      --
    end if;
    --
    -- Subclass
    if I_subclass is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.subclass = b.db_subclass }';
      --
    end if;
    --
    -- Item
    if I_item is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.item = b.db_item }';
      --
    end if;
    --
    -- Item_Description
    if I_item_desc is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and upper(i.item_desc) like upper('%}' || I_item_desc || q'{%') }';
      --
    end if;
    --
    -- Assortment_Mode
    if I_assortment_mode is not null then
      --
      select t.*
        bulk collect
        into L_assortment_mode_list
        from table(convert_comma_list(I_list => I_assortment_mode)) t;

      L_string_query := L_string_query ||
                        q'{ and i.assortment_mode in (select column_value
                                                   from table(b.db_assortment_mode)) }';
      --
    end if;
    --
    -- Range_Letter
    if I_range_letter is not null then
      --
      select t.*
        bulk collect
        into L_range_letter_list
        from table(convert_comma_list(I_list => I_range_letter)) t;

      L_string_query := L_string_query ||
                        q'{ and i.range_letter in (select column_value
                                                   from table(b.db_range_letter)) }';
      --
    end if;
    --
    -- HQ_Recommendation
    if I_hq_recommendation is not null then
      --
      select t.*
        bulk collect
        into L_hq_recommendation_list
        from table(convert_comma_list(I_list => I_hq_recommendation)) t;

      L_string_query := L_string_query ||
                        q'{ and i.hq_recommendation in (select column_value
                                                   from table(b.db_hq_recommendation)) }';
      --
    end if;
    --
    -- Store_Choice
    if I_store_choice is not null then
      --
      select t.*
        bulk collect
        into L_store_choice_list
        from table(convert_comma_list(I_list => I_store_choice)) t;

      L_string_query := L_string_query ||
                        q'{ and i.store_choice in (select column_value
                                                   from table(b.db_store_choice)) }';
      --
    end if;
    --
    -- Supplier_Site
    if I_supplier_site is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.supplier_site = b.db_supplier_site }';
      --
    end if;
    --
    -- Not_Modifiable_Ind
    if I_not_modifiable_ind is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.not_modifiable_ind = b.db_not_modifiable_ind }';
      --
    end if;
    --
    -- Season_Item_Ind
    if I_season_item_ind is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.season_item_ind = b.db_season_item_ind }';
      --
    end if;
    --
    -- Related_Items
    if I_related_items is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.related_items = b.db_related_items }';
      --
    end if;
    --
    -- Pack_Component
    if I_pack_component is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.pack_component = b.db_pack_component }';
      --
    end if;
    --
    -- Assortment_Start_Date
    if I_assrt_start_date_low is not null and I_assrt_start_date_high is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and trunc(i.assortment_start_date) between trunc(b.db_assrt_start_date_low) and trunc(b.db_assrt_start_date_high) }';
      --
    end if;
    --
    -- Assortment_End_Date
    if I_assrt_end_date_low is not null and I_assrt_end_date_high is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and trunc(i.assortment_end_date) between trunc(b.db_assrt_end_date_low) and trunc(b.db_assrt_end_date_high) }';
      --
    end if;
    --
    -- Omni_CS_Start_Date
    if I_omni_cs_start_date_low is not null and I_omni_cs_start_date_high is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and trunc(i.omni_cs_start_date) between trunc(b.db_omni_cs_start_date_low) and trunc(b.db_omni_cs_start_date_high) }';
      --
    end if;
    --
    -- Omni_CS_End_Date
    if I_omni_cs_end_date_low is not null and I_omni_cs_end_date_high is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and trunc(i.omni_cs_end_date) between trunc(b.db_omni_cs_end_date_low) and trunc(b.db_omni_cs_end_date_high) }';
      --
    end if;
    --
    -- Response_End_Date
    if I_response_end_date_low is not null and I_response_end_date_high is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and trunc(i.response_end_date) between trunc(b.db_response_end_date_low) and trunc(b.db_response_end_date_high) }';
      --
    end if;
    
    --
    -- bulk query to table type
    open L_sys_refcur for L_string_query
      using I_store,
            I_group_no,
            I_dept,
            I_class,
            I_subclass,
            I_item,
            I_item_desc,
            L_assortment_mode_list,
            L_range_letter_list,
            L_hq_recommendation_list,
            L_store_choice_list,
            I_supplier_site,
            I_not_modifiable_ind,
            I_season_item_ind,
            I_related_items,
            I_pack_component,
            I_assrt_start_date_low,
            I_assrt_start_date_high,
            I_assrt_end_date_low,
            I_assrt_end_date_high,
            I_omni_cs_start_date_low,
            I_omni_cs_start_date_high,
            I_omni_cs_end_date_low,
            I_omni_cs_end_date_high,
            I_response_end_date_low,
            I_response_end_date_high;
    loop
      --
      fetch L_sys_refcur bulk collect
       into L_items_rec;
      exit when L_items_rec.count = 0;
      --
      -- pipe data form collection
      for i in 1 .. L_items_rec.count loop
        --
        pipe row(XXADEO_SP_ASSORTMENT_OBJ(L_items_rec(i).store,
                                          L_items_rec(i).group_no,
                                          L_items_rec(i).dept,
                                          L_items_rec(i).class,
                                          L_items_rec(i).subclass,
                                          L_items_rec(i).item,
                                          L_items_rec(i).item_desc,
                                          L_items_rec(i).retail_price,
                                          L_items_rec(i).assortment_mode,
                                          L_items_rec(i).range_letter,
                                          L_items_rec(i).grading,
                                          L_items_rec(i).cluster_size,
                                          L_items_rec(i).cs_recommendation,
                                          L_items_rec(i).hq_recommendation,
                                          L_items_rec(i).current_regular_asso,
                                          L_items_rec(i).store_choice,
                                          L_items_rec(i).supplier_site,
                                          L_items_rec(i).assortment_start_date,
                                          L_items_rec(i).assortment_end_date,
                                          L_items_rec(i).omni_cs_start_date,
                                          L_items_rec(i).omni_cs_end_date,
                                          L_items_rec(i).response_end_date,
                                          L_items_rec(i).pack_component,
                                          L_items_rec(i).substitute_item,
                                          L_items_rec(i).related_items,
                                          L_items_rec(i).comment_cp,
                                          L_items_rec(i).season_item_ind,
                                          L_items_rec(i).not_modifiable_ind,
                                          L_items_rec(i).validated_date,
                                          L_items_rec(i).update_line_interface,
                                          L_items_rec(i).store_format,
                                          L_items_rec(i).catman_status,
                                          L_items_rec(i).history_flag,
                                          L_items_rec(i).cluster_popup_flag));
      end loop;
      --
    end loop;
    --
    close L_sys_refcur;
    --
    return;
    --
  EXCEPTION
    --
    WHEN OTHERS THEN
       L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
      --
      dbms_output.put_line(L_error_message);
      --
      return;
      --
  END XXADEO_ASSORTMENT_ITEMS_SEARCH;

----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_VALID_DATE(O_error_message   IN OUT LOGGER_LOGS.TEXT%TYPE,
                                        O_rtk_key         OUT VARCHAR2,
                                        I_assortment_obj  IN     XXADEO_SP_ASSORTMENT_OBJ)
    RETURN BOOLEAN IS
    --
    l_count_records number;
    l_program       varchar2(64) := 'XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_VALID_DATE';
    l_rtk_key       varchar2(64) := 'ERROR_ASSORT_INVALID_DATE';
    --
    l_vdate date;
    --
  BEGIN
    --
    EXECUTE IMMEDIATE ('ALTER SESSION SET NLS_DATE_FORMAT = ''DD-MM-YYYY''');
    --
    select nvl(get_vdate, sysdate) into l_vdate from dual; -- REMOVER SYSDATE !!!!
    --
    select count(1)
      into l_count_records
      from xxadeo_sp_v_items_assortment a
     where a.item = I_assortment_obj.item
       and a.store = I_assortment_obj.store
       and trunc(a.assortment_start_date) = trunc(I_assortment_obj.assortment_start_date)
       and nvl(trunc(a.assortment_end_date), to_date(99990101, 'yyyymmdd')) =
           nvl(trunc(I_assortment_obj.assortment_end_date),
               to_date(99990101, 'yyyymmdd'))
       and a.store_choice = I_assortment_obj.store_choice;
    --
    if l_count_records > 0 then

      --The record already exists, no changes were made.
      --
      O_rtk_key := null;
      return true;

    else

      --The record does not exist. Changes were made
      --Need to validate dates. If difference is less than a week, can't be split

      if nvl(I_assortment_obj.assortment_end_date,
               to_date(99990101, 'yyyymmdd')) - l_vdate <= 7 then

        O_rtk_key := l_rtk_key;
        return false;

      else

        O_rtk_key := null;
        return true;

      end if;

    end if;

  EXCEPTION
    when others then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
      dbms_output.put_line(O_error_message);

      return false;

  END XXADEO_ASSORTMENT_VALID_DATE;

----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_CHANGES(O_error_message    IN OUT LOGGER_LOGS.TEXT%TYPE,
                                     I_assortment_tbl   IN XXADEO_SP_ASSORTMENT_TBL)
    RETURN BOOLEAN IS

    l_count_records           number;
    l_store_choice_id         XXADEO_SP_V_ASSORTMENT_VAL_TL.ASSORTMENT_VALUE_ID%TYPE;
    l_cs_recommendation_id    XXADEO_SP_V_ASSORTMENT_VAL_TL.ASSORTMENT_VALUE_ID%TYPE;
    l_hq_recommendation_id    XXADEO_SP_V_ASSORTMENT_VAL_TL.ASSORTMENT_VALUE_ID%TYPE;
    l_current_regular_asso_id XXADEO_SP_V_ASSORTMENT_VAL_TL.ASSORTMENT_VALUE_ID%TYPE;
    l_assortment_mode_id      XXADEO_SP_V_ASSORTMENT_VAL_TL.ASSORTMENT_VALUE_ID%TYPE;
    l_history_flag            XXADEO_SP_V_ASSORTMENT.HISTORY_FLAG%TYPE;
    l_cluster_popup_flag      XXADEO_SP_V_ASSORTMENT.CLUSTER_POPUP_FLAG%TYPE;

    l_vdate         date;
    l_program       varchar2(64) := 'XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_CHANGES';
    l_error_message varchar2(1000);
    l_rtk_key       varchar2(1000);

  begin
   
   select nvl(get_vdate, sysdate)
   into   l_vdate
   from   dual; -- REMOVER SYSDATE !!!!
   
   for i in 1 .. i_assortment_tbl.count
   loop
   
     if i_assortment_tbl(i).store_choice is not null
     then
       select t.assortment_value_id
       into   l_store_choice_id
       from   xxadeo_sp_v_assortment_val_tl t
       where  to_char(seq_id) = i_assortment_tbl(i).store_choice;
     end if;
     --
     if i_assortment_tbl(i).cs_recommendation is not null
     then
       select t.assortment_value_id
       into   l_cs_recommendation_id
       from   xxadeo_sp_v_assortment_val_tl t
       where  to_char(seq_id) = i_assortment_tbl(i).cs_recommendation;
     end if;
     --
     if i_assortment_tbl(i).hq_recommendation is not null
     then
       select t.assortment_value_id
       into   l_hq_recommendation_id
       from   xxadeo_sp_v_assortment_val_tl t
       where  to_char(seq_id) = i_assortment_tbl(i).hq_recommendation;
     end if;
     --
     if i_assortment_tbl(i).current_regular_asso is not null
     then
       select t.assortment_value_id
       into   l_current_regular_asso_id
       from   xxadeo_sp_v_assortment_val_tl t
       where  to_char(seq_id) = i_assortment_tbl(i).current_regular_asso;
     end if;
     --
     if i_assortment_tbl(i).assortment_mode is not null
     then
       select t.assortment_mode_id
       into   l_assortment_mode_id
       from   xxadeo_sp_v_assortment_mode_tl t
       where  to_char(seq_id) = i_assortment_tbl(i).assortment_mode;
     end if;
     --
     select count(1)
     into   l_count_records
     from   xxadeo_sp_v_items_assortment a
     where  a.item = i_assortment_tbl(i).item
     and    a.store = i_assortment_tbl(i).store
     and    trunc(a.assortment_start_date) =
            trunc(i_assortment_tbl(i).assortment_start_date)
     and    nvl(trunc(a.assortment_end_date),
                to_date(99990101, 'yyyymmdd')) =
            nvl(trunc(i_assortment_tbl(i).assortment_end_date),
                 to_date(99990101, 'yyyymmdd'))
     and    a.store_choice = i_assortment_tbl(i).store_choice;
   
     l_history_flag := xxadeo_assortment_history(i_item  => i_assortment_tbl(i).item,
                                                 i_store => i_assortment_tbl(i)
                                                            .store);
   
     l_cluster_popup_flag := xxadeo_assortment_cluster_rec(i_item       => i_assortment_tbl(i).item,
                                                           i_store      => i_assortment_tbl(i)
                                                                           .store,
                                                           i_start_date => i_assortment_tbl(i)
                                                                           .assortment_start_date,
                                                           i_end_date   => i_assortment_tbl(i)
                                                                           .assortment_end_date);
     if l_count_records > 0
     then
     
       --The record already exists for the PK! Was only validated
       update xxadeo_sp_v_assortment a
       set    a.history_flag         = l_history_flag,
              a.cluster_popup_flag   = l_cluster_popup_flag,
              a.user_action          = 1,
              a.validated_date       = l_vdate,
              a.last_update_id       = lp_user_id,
              a.last_update_datetime = sysdate
       where  a.item = i_assortment_tbl(i).item
       and    a.store = i_assortment_tbl(i).store
       and    a.start_date = i_assortment_tbl(i).assortment_start_date
       and    nvl(a.end_date, to_date(99990101, 'yyyymmdd')) =
              nvl(i_assortment_tbl(i).assortment_end_date,
                   to_date(99990101, 'yyyymmdd'))
       and    a.store_choice = l_store_choice_id;
     else
     
       --if this is the current event SPLIT
       if (l_vdate between i_assortment_tbl(i).assortment_start_date and
          nvl(i_assortment_tbl(i).assortment_end_date,
               to_date(99990101, 'yyyymmdd')))
       then
       
         if xxadeo_assortment_valid_date(o_error_message  => l_error_message,
                                         o_rtk_key        => l_rtk_key,
                                         i_assortment_obj => i_assortment_tbl(i))
         then
         
           update xxadeo_sp_v_assortment a
           set    a.end_date             = next_day(l_vdate, 'SUNDAY'),
                  a.history_flag         = l_history_flag,
                  a.cluster_popup_flag   = l_cluster_popup_flag,
                  a.user_action          = 1,
                  a.validated_date       = l_vdate,
                  a.last_update_id       = lp_user_id,
                  a.last_update_datetime = sysdate
           where  a.item = i_assortment_tbl(i).item
           and    a.store = i_assortment_tbl(i).store
           and    a.start_date = i_assortment_tbl(i).assortment_start_date
           and    nvl(a.end_date, to_date(99990101, 'yyyymmdd')) =
                  nvl(i_assortment_tbl(i).assortment_end_date,
                       to_date(99990101, 'yyyymmdd'));
         
           --FAZER SPLIT
           --insert into
           insert into xxadeo_sp_v_assortment
             (item,
              store,
              start_date,
              end_date,
              validated_date,
              response_date,
              assortment_mode,
              cluster_size,
              cs_recommendation,
              hq_recommendation,
              curr_val_assrt,
              store_choice,
              not_mod_ind,
              status,
              history_flag,
              cluster_popup_flag,
              week,
              user_action,
              comments,
              create_id,
              create_datetime,
              last_update_id,
              last_update_datetime)
           values
             (i_assortment_tbl(i).item,
              i_assortment_tbl(i).store,
              next_day(l_vdate, 'MONDAY'),
              i_assortment_tbl(i).assortment_end_date, --maintain the date if it's null or not
              l_vdate,
              i_assortment_tbl(i).response_end_date,
              l_assortment_mode_id,
              i_assortment_tbl(i).cluster_size,
              l_cs_recommendation_id,
              l_hq_recommendation_id,
              l_current_regular_asso_id,
              l_store_choice_id,
              i_assortment_tbl(i).not_modifiable_ind,
              i_assortment_tbl(i).catman_status,
              l_history_flag,
              l_cluster_popup_flag, --CLUSTER_POPUP_FLAG
              'W' || to_char(next_day(l_vdate, 'MONDAY'), 'WW') || '_' ||
              to_char(next_day(l_vdate, 'MONDAY'), 'yyyy'),
              1, --USER_ACTION
              i_assortment_tbl(i).comment_cp,
              lp_user_id, --CREATE_ID VALIDATE !!!
              sysdate,
              lp_user_id, --LAST_UPDATE_ID --VALIDATE !!!
              sysdate);
         
         else
           rollback;
           return false;
         
         end if;
       
       else
         --future event -> Update only STORE_CHOICE
         update xxadeo_sp_v_assortment a
         set    a.store_choice         = l_store_choice_id, /*USAR LOGICA DO SEQ_ID*/
                a.history_flag         = l_history_flag,
                a.cluster_popup_flag   = l_cluster_popup_flag,
                a.user_action          = 1,
                a.last_update_id       = lp_user_id,
                a.last_update_datetime = sysdate
         where  a.item = i_assortment_tbl(i).item
         and    a.store = i_assortment_tbl(i).store
         and    a.start_date = i_assortment_tbl(i).assortment_start_date
         and    nvl(a.end_date, to_date(99990101, 'yyyymmdd')) =
                nvl(i_assortment_tbl(i).assortment_end_date,
                     to_date(99990101, 'yyyymmdd'));
       end if;
     
     end if;
   
   end loop;
   
   commit;
   -- dbms_mview.refresh(list => 'XXADEO_SP_V_ITEMS_ASSORT_MV');
   return true;
  exception
    when others then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
      dbms_output.put_line(O_error_message);
      rollback;
      return false;

  END XXADEO_ASSORTMENT_CHANGES;
----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_TBL_PROCESS RETURN BOOLEAN IS
  
    l_program            varchar2(64) := 'XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_TBL_PROCESS';
    l_error_message      varchar2(2000);
    
    l_vdate              date := get_vdate;
    l_history_flag       xxadeo_sp_v_assortment.HISTORY_FLAG%type;
    l_cluster_popup_flag xxadeo_sp_v_assortment.CLUSTER_POPUP_FLAG%type;
    
    
    cursor cur_assortment is
      select a.item,
             a.store,
             a.start_date,
             a.end_date
      from   xxadeo_assortment a;
  
  begin

    /*************************************************************************************
    * DELETE RECORDS THAT WERE DELETED ON THE ORIGIN                                     *
    *************************************************************************************/  
    delete from xxadeo_assortment
    where  (nvl(end_date, to_date(99990101, 'yyyymmdd')) < l_vdate)
    or     (item, store, start_date) in
           (select a.item       assort_item,
                    a.store      assort_store,
                    a.start_date assort_start_date /*,
                                        a.end_date   assort_end_date,
                                        s.item       store_item,
                                        s.store      store_store,
                                        s.start_date store_start_date,
                                        s.end_date   store_end_date*/
             from   xxadeo_assortment a
             left   join xxadeo_assortment_store s on a.item = s.item
                                               and    a.store = s.store
                                               and    a.start_date =
                                                      s.start_date
             where  s.item is null);
  
    /*************************************************************************************
    *                                                                                    *
    *************************************************************************************/
  
    merge into xxadeo_assortment a
    using (select s.item,
                  s.store,
                  s.start_date,
                  s.end_date,
                  null validated_date,
                  s.response_date,
                  s.assortment_mode,
                  s.hq_recommendation,
                  null curr_val_assrt,
                  case
                    when s.hq_recommendation = 'C' then
                     s.cs_recommendation
                    else
                     s.hq_recommendation
                  end store_choice,
                  s.not_mod_ind,
                  s.status,
                  s.week,
                  0 user_action,
                  s.comments,
                  lp_user_id create_id,
                  sysdate create_datetime,
                  lp_user_id last_update_id,
                  sysdate last_update_datetime,
                  s.cluster_size,
                  s.cs_recommendation
           from   xxadeo_assortment_store s
           where  nvl(s.end_date, to_date(99990101, 'yyyymmdd')) > l_vdate) xas
    on (a.item = xas.item and a.store = xas.store and trunc(a.start_date) = trunc(xas.start_date))
    when matched then
      update
      set    a.end_date             = xas.end_date,
             a.validated_date       = xas.validated_date,
             a.response_date        = xas.response_date,
             a.assortment_mode      = xas.assortment_mode,
             a.hq_recommendation    = xas.hq_recommendation,
             a.curr_val_assrt       = xas.curr_val_assrt,
             a.store_choice         = xas.store_choice,
             a.not_mod_ind          = xas.not_mod_ind,
             a.status               = xas.status,
             a.week                 = xas.week,
             a.user_action          = xas.user_action,
             a.comments             = xas.comments,
             a.cluster_size         = xas.cluster_size,
             a.cs_recommendation    = xas.cs_recommendation,
             a.last_update_id       = xas.last_update_id,
             a.last_update_datetime = xas.last_update_datetime
      where  a.store_choice != xas.store_choice
    when not matched then
      insert
        (a.item,
         a.store,
         a.start_date,
         a.end_date,
         a.validated_date,
         a.response_date,
         a.assortment_mode,
         a.hq_recommendation,
         a.curr_val_assrt,
         a.store_choice,
         a.not_mod_ind,
         a.status,
         a.week,
         a.user_action,
         a.comments,
         a.create_id,
         a.create_datetime,
         a.last_update_id,
         a.last_update_datetime,
         a.cluster_size,
         a.cs_recommendation)
      values
        (xas.item,
         xas.store,
         xas.start_date,
         xas.end_date,
         xas.validated_date,
         xas.response_date,
         xas.assortment_mode,
         xas.hq_recommendation,
         xas.curr_val_assrt,
         xas.store_choice,
         xas.not_mod_ind,
         xas.status,
         xas.week,
         xas.user_action,
         xas.comments,
         xas.create_id,
         xas.create_datetime,
         xas.last_update_id,
         xas.last_update_datetime,
         xas.cluster_size,
         xas.cs_recommendation);


         --FOR ALL UPDATE HISTORY_FLAG AND CLUSTER_POPUP_FLAG                                                                   
                                                                     
        for l_cur_assortment in cur_assortment
        loop
                  
        l_history_flag := xxadeo_assortment_history(i_item  => l_cur_assortment.item,
                                                               i_store => l_cur_assortment.store);
                    
        l_cluster_popup_flag := xxadeo_assortment_cluster_rec(i_item       => l_cur_assortment.item,
                                                                   i_store      => l_cur_assortment.store,
                                                                   i_start_date => l_cur_assortment.start_date,
                                                                   i_end_date   => l_cur_assortment.end_date);
                  
          update xxadeo_sp_v_assortment a
          set    a.history_flag       = l_history_flag,
                 a.cluster_popup_flag = l_cluster_popup_flag
          where  a.item = l_cur_assortment.item
          and    a.store = l_cur_assortment.store
          and    a.start_date = l_cur_assortment.start_date;
        end loop;                                                                     
  
  
    commit;
    --dbms_mview.refresh(list => 'XXADEO_SP_V_ITEMS_ASSORT_MV');
    return true;
  exception
    when others then
      l_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
      dbms_output.put_line(l_error_message);
      rollback;
      return false;
    
  END XXADEO_ASSORTMENT_TBL_PROCESS;
  ----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_BATCH(I_stage IN VARCHAR2) RETURN BOOLEAN IS
  
    l_vdate         date := trunc(nvl(get_vdate, sysdate));
    l_program       varchar2(64) := 'XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_BATCH';
    l_error_message varchar2(2000);
  
    cursor cur_assortment is
      select a.item,
             a.store,
             a.start_date,
             a.end_date,
             a.store_choice
      from   xxadeo_assortment a
      where  l_vdate between a.start_date and
             nvl(a.end_date, to_date(99990101, 'yyyymmdd'));
  
  begin
  
    if i_stage = g_stage_pre
    then
      /*************************************************************************************
      *  BEFORE NEW CATMAN RECORDS: VALIDATE CURRENT DATA AND UPDATE NECESSARY FIELDS      *
      *************************************************************************************/
      --The Response End Date will be deleted by the Store Portal the next day it is passed (As soon as �Today Date�> �Response End Date�).
      ----Assortment which still has not been validated by user will be automatically validated by the system and sent to CatMan. 
      update xxadeo_sp_v_assortment a
      set    a.response_date        = null,
             a.user_action          = 0,
             a.validated_date       = trunc(sysdate),
             a.last_update_id       = lp_user_id, /*SYSTEM*/
             a.last_update_datetime = sysdate
      where  trunc(a.response_date) < trunc(l_vdate)
      and a.validated_date is null;
    
    elsif i_stage = g_stage_pos
    then
      
    
      --  Cell Current Regular Assortment is filled by Store Portal with the value Store Choice as soon as � Assortment Start Date �
      ---- (for this value of Store Choice) becomes equal to � Date of Today �. 
      ------  Filled at the same time for all lines existing for the Item
    
      for l_cur_assortment in cur_assortment
      loop
        update xxadeo_sp_v_assortment a
        set    a.curr_val_assrt       = l_cur_assortment.store_choice,
               a.last_update_id       = lp_user_id, /*SYSTEM*/
               a.last_update_datetime = sysdate
        where  a.item = l_cur_assortment.item
        and    a.store = l_cur_assortment.store;
      end loop;
    
      --Update Store Choice to HQ Recomendation or value of recommendation for the Cluster Size if "HQ recommendation" contains the value "C".
      -- for all lines that Response End Date is in 45 days or less
      update xxadeo_sp_v_assortment a
      set    a.store_choice = (case
                                when a.hq_recommendation = 'C' then
                                 a.cs_recommendation
                                else
                                 a.hq_recommendation
                              end),
             a.last_update_id       = lp_user_id, /*SYSTEM*/
             a.last_update_datetime = sysdate
      where  trunc(a.response_date) - trunc(l_vdate) <= 45
      and    a.validated_date is null;
      
      
    else
      return false;
    end if;
  
    commit;
    --dbms_mview.refresh(list => 'XXADEO_SP_V_ITEMS_ASSORT_MV');
    return true;
  exception
    when others then
      l_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
      dbms_output.put_line(l_error_message);
      rollback;
      return false;
    
  end XXADEO_ASSORTMENT_BATCH;
  

----------------------------------------------------------------------------------

  FUNCTION XXADEO_ASSORTMENT_HISTORY(I_item          IN XXADEO_SP_V_ITEMS_ASSORTMENT.ITEM%TYPE,
                                     I_store         IN XXADEO_SP_V_ITEMS_ASSORTMENT.STORE%TYPE)
    RETURN number IS

    l_count   number;
    l_program varchar2(64) := 'XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_HISTORY';
    l_error_message LOGGER_LOGS.TEXT%type;
  BEGIN
    
/*    select count(1)
    into   l_count
    from   xxadeo_sp_v_assortment ia
    where  ia.item = i_item
    and    ia.store = i_store
    and    nvl(ia.end_date, to_date(99990101, 'yyyymmdd')) >=
           add_months(get_vdate, -18)
    and rownum = 2;*/

    select count(1)
    into   l_count
    /*,versions_starttime
    ,versions_endtime
    ,versions_operation*/
    from   xxadeo_assortment       versions between timestamp minvalue and maxvalue a
    where  a.item = i_item
    and    a.store = i_store
    and    a.end_date < get_vdate;

    if l_count > 0
    then
      return 1;
    else
      return 0;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      l_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
      dbms_output.put_line(l_error_message);
    
      return 0;
  END XXADEO_ASSORTMENT_HISTORY;  
  

----------------------------------------------------------------------------------  
  FUNCTION XXADEO_ASSORTMENT_CLUSTER_REC(I_item          IN XXADEO_SP_V_ITEMS_ASSORTMENT.ITEM%TYPE DEFAULT NULL,
                                         I_store         IN XXADEO_SP_V_ITEMS_ASSORTMENT.STORE%TYPE,
                                         I_start_date    IN XXADEO_SP_V_ITEMS_ASSORTMENT.ASSORTMENT_START_DATE%TYPE,
                                         I_end_date      IN XXADEO_SP_V_ITEMS_ASSORTMENT.ASSORTMENT_END_DATE%TYPE
                                         )
    RETURN number IS

    l_count   number;
    l_program varchar2(64) := 'XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_CLUSTER_REC';
    l_error_message LOGGER_LOGS.TEXT%type;
  BEGIN


    select count(1)
    into   l_count
    from   (select a.item,
                   a.store,
                   a.start_date,
                   a.end_date,
                   s.area bu,
                   sc.cluster_id,
                   max(sc.start_date) max_start_date
            from   xxadeo_assortment         a,
                   xxadeo_sp_v_item_master   im,
                   xxadeo_sp_v_store_cluster sc,
                   xxadeo_sp_v_store         s
            where  a.item = im.item
            and    (im.dept = sc.dept and im.class = sc.class and
                  im.subclass = sc.subclass)
            and    a.store = sc.store
            and    a.store = s.store
            and    I_start_date = a.start_date
            and    a.item = i_item
            and    a.store = i_store
            group  by a.item,
                      a.store,
                      a.start_date,
                      a.end_date,
                      s.area,
                      sc.cluster_id) src,
           xxadeo_assortment_cluster ac
    where  src.item = ac.item
    and    src.cluster_id = ac.cluster_id
    and    src.bu = ac.bu
    and    (src.start_date between ac.start_date and
          nvl(ac.end_date, to_date(99990101, 'yyyymmdd')) or
          nvl(src.end_date, to_date(99990101, 'yyyymmdd')) between
          ac.start_date and nvl(ac.end_date, to_date(99990101, 'yyyymmdd')) or
          (src.start_date <= ac.start_date and
          nvl(src.end_date, to_date(99990101, 'yyyymmdd')) >=
          nvl(ac.end_date, to_date(99990101, 'yyyymmdd'))));



    if l_count > 0
    then
      return 1;
    else
      return 0;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      l_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
      dbms_output.put_line(l_error_message);
    
      return 0;
  END XXADEO_ASSORTMENT_CLUSTER_REC;


END XXADEO_SP_ASSORTMENT_SQL;
/
