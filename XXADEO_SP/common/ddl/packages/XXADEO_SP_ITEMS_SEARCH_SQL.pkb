create or replace package body XXADEO_SP_ITEMS_SEARCH_SQL is

/********************************************************************************/
/* CREATE DATE - Julho 2018                                                     */
/* CREATE USER - Paulo Mamede                                                   */
/* PROJECT     - ADEO                                                           */
/* DESCRIPTION - Package "XXADEO_SP_ITEMS_SEARCH_SQL" for Items Search          */
/********************************************************************************/

 FUNCTION XXADEO_ITEMS_SEARCH(   I_search_control       IN VARCHAR2 DEFAULT 'N',
                                 I_GROUP_NO             IN XXADEO_SP_V_ITEM_MASTER.GROUP_NO%TYPE DEFAULT NULL,
                                 I_DEPT                 IN XXADEO_SP_V_ITEM_MASTER.DEPT%TYPE DEFAULT NULL,
                                 I_CLASS                IN XXADEO_SP_V_ITEM_MASTER.CLASS%TYPE DEFAULT NULL,
                                 I_SUBCLASS             IN XXADEO_SP_V_ITEM_MASTER.SUBCLASS%TYPE DEFAULT NULL,
                                 I_GRADING              IN NUMBER DEFAULT NULL,
                                 I_ITEM_TYPE            IN VARCHAR2 DEFAULT NULL,
                                 I_IS_PACK              IN XXADEO_SP_V_ITEM_MASTER.PACK_IND%TYPE DEFAULT NULL,
                                 I_TYPOLOGY             IN VARCHAR2 DEFAULT NULL,
                                 I_SUB_TYPOLOGY         IN VARCHAR2 DEFAULT NULL,
                                 I_ITEM                 IN XXADEO_SP_V_ITEM_MASTER.ITEM%TYPE DEFAULT NULL,
                                 I_GTIN                 IN VARCHAR2 DEFAULT NULL,
                                 I_DESCRIPTION          IN XXADEO_SP_V_ITEM_MASTER.SHORT_DESC%TYPE DEFAULT NULL,
                                 I_SUPPLIER_SITE        IN VARCHAR2 DEFAULT NULL,
                                 I_ITEM_STATUS          IN VARCHAR2 DEFAULT NULL,
                                 I_ASSORTMENT           IN VARCHAR2 DEFAULT NULL,
                                 I_CURR_ASSORTMENT      IN VARCHAR2 DEFAULT NULL,   -- list
                                 I_RANGE_LETTER         IN VARCHAR2 DEFAULT NULL,
                                 I_OCSSD                IN XXADEO_SP_V_ITEM_BU.OMNICHANNEL_START_DATE%TYPE DEFAULT NULL,
                                 I_OCSED                IN XXADEO_SP_V_ITEM_BU.OMNICHANNEL_END_DATE%TYPE DEFAULT NULL,
                                 I_STORE                IN XXADEO_SP_V_STORE.STORE%TYPE DEFAULT NULL)
   RETURN XXADEO_SP_ITEMS_TBL
   PIPELINED AS
   --
   L_program       VARCHAR2(64) := 'XXADEO_SP_ITEMS_SEARCH_SQL.XXADEO_ITEMS_SEARCH';
   L_error_message VARCHAR2(2000);
   --
   L_items_rec     XXADEO_SP_ITEMS_TBL;
   L_string_query  VARCHAR2(20000) := NULL;
   L_sys_refcur    SYS_REFCURSOR;
   --
   L_curr_assortment_list     xxadeo_sp_list_type_tbl;
   --
    BEGIN
    --
    if I_search_control = 'N' then
      --
      return;
      --
    end if;
    --
    EXECUTE IMMEDIATE ('ALTER SESSION SET NLS_DATE_FORMAT = ''DD-MM-YYYY''');
    --
    L_string_query := q'{with t_binds as
                            (select :1  db_group_no,
                                    :2  db_dept,
                                    :3  db_class,
                                    :4  db_subclass,
                                    :5  db_grading,
                                    :6  db_item_type,
                                    :7  db_is_pack,
                                    :8  db_typology,
                                    :9  db_sub_typology,
                                    :10 db_item,
                                    :11 db_gtin,
                                    :12 db_description,
                                    :13 db_supplier_site,
                                    :14 db_item_status,
                                    :15 db_assortment,
                                    :16 db_curr_assortment,
                                    :17 db_range_letter,
                                    :18 db_ocssd,
                                    :20 db_ocsed,
                                    :21 db_store
                               from dual)
                              select new XXADEO_SP_ITEMS_OBJ(     group_no,
                                                                  dept,
                                                                  class,
                                                                  subclass,
                                                                  grading,
                                                                  item_type,
                                                                  is_pack,
                                                                  typology,
                                                                  sub_typology,
                                                                  item,
                                                                  gtin,
                                                                  description,
                                                                  supplier_site,
                                                                  ITEM_STATUS,
                                                                  assortment,
                                                                  curr_assortment,
                                                                  range_letter,
                                                                  ocssd,
                                                                  ocsed      )
                                      from XXADEO_SP_V_ITEMS_SEARCH i,
                                           t_binds b
                                      where 1 = 1
                                      }';
    -- store
    if I_store is not null then
    --
      L_string_query := L_string_query ||
                        q'{ and i.store = b.db_store }';
                      --
    end if;
    --
    -- Group
    if I_group_no is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.group_no = b.db_group_no }';
          --
    end if;

    --
    -- Department
    if I_dept is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.dept = b.db_dept }';
      --
    end if;

    --
    -- Class
    if I_class is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.class = b.db_class }';
      --
    end if;

    --
    -- Subclass
    if I_subclass is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.subclass = b.db_subclass }';
      --
    end if;
    --
    -- Item
    if I_item is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.item = b.db_item }';
      --
    end if;
    --
    -- Item_Description
    if I_DESCRIPTION is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and upper(i.DESCRIPTION) like upper('%}' || I_DESCRIPTION || q'{%') }';
      --
    end if;

    -- Grading
    if I_grading is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.grading = b.db_grading }';
      --
    end if;
    --

    -- ASSORTMENT

    if I_ASSORTMENT is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.assortment = b.db_assortment }';
      --
    end if;

    -- Supplier Site
    if I_SUPPLIER_SITE is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.supplier_site = b.db_supplier_site }';
      --
    end if;
    --

    -- Range Letter

    if I_RANGE_LETTER is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.range_letter = b.db_range_letter }';
      --
    end if;
    --

    -- Is_pack

    if I_IS_PACK is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.is_pack = b.db_is_pack }';
      --
    end if;
    --
    -- I_TYPOLOGY

    if I_TYPOLOGY is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.typology = b.db_typology }';
      --
    end if;
    --

    -- SUB_TYPOLOGY

    if I_SUB_TYPOLOGY is not null then
       --
       L_string_query := L_string_query ||
                         q'{ and i.sub_typology = b.db_sub_typology }';
       --
    end if;
    --

    -- GTIN

    if I_GTIN is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.gtin = b.db_gtin }';
      --
    end if;
    --

    -- CURR_ASSORTMENT

    if I_curr_assortment is not null then
      --
      select t.*
        bulk collect
        into L_curr_assortment_list
        from table(convert_comma_list(I_list => I_curr_assortment)) t;

      L_string_query := L_string_query ||
                        q'{ and i.curr_assortment in (select column_value
                                                   from table(b.db_curr_assortment)) }';
      --
    end if;
    --

    -- ITEM_TYPE

    if I_ITEM_TYPE is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.item_type = b.db_item_type }';
      --
    end if;
    --

    -- ITEM_STATUS

    if I_ITEM_STATUS is not null then
      --
      L_string_query := L_string_query ||
                        q'{ and i.item_status = b.db_item_status }';
      --
    end if;
    --
    -- bulk query to table type
    open L_sys_refcur for L_string_query
      using I_group_no,
            I_dept,
            I_class,
            I_subclass,
            I_grading,
            I_item_type,
            I_is_pack,
            I_typology,
            I_sub_typology,
            I_item,
            I_gtin,
            I_description,
            I_supplier_site,
            I_item_status,
            I_assortment,
            L_curr_assortment_list,
            I_range_letter,
            I_OCSSD,
            I_OCSED,
            I_STORE;
    loop
      --
      fetch L_sys_refcur bulk collect
       into L_items_rec;
      exit when L_items_rec.count = 0;
      --
      -- pipe data form collection
      for i in 1 .. L_items_rec.count loop
        --
        pipe row(XXADEO_SP_ITEMS_OBJ(     L_items_rec(i).group_no,
                                          L_items_rec(i).dept,
                                          L_items_rec(i).class,
                                          L_items_rec(i).subclass,
                                          L_items_rec(i).grading,
                                          L_items_rec(i).item_type,
                                          L_items_rec(i).is_pack,
                                          L_items_rec(i).typology,
                                          L_items_rec(i).sub_typology,
                                          L_items_rec(i).item,
                                          L_items_rec(i).gtin,
                                          L_items_rec(i).description,
                                          L_items_rec(i).supplier_site,
                                          L_items_rec(i).item_status,
                                          L_items_rec(i).assortment,
                                          L_items_rec(i).curr_assortment,
                                          L_items_rec(i).range_letter,
                                          L_items_rec(i).ocssd,
                                          L_items_rec(i).ocsed));
      end loop;
      --
    end loop;
    --
    close L_sys_refcur;
    --
    return;
    --
   EXCEPTION
    --
    WHEN OTHERS THEN
      --
       L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
                                             --
      return;

    END XXADEO_ITEMS_SEARCH;
        --
-----------------------------------------------------------------------------------------------------
end XXADEO_SP_ITEMS_SEARCH_SQL;
/
