CREATE OR REPLACE PACKAGE XXADEO_SP_ITEMS_SEARCH_SQL is

/********************************************************************************/
/* CREATE DATE - Julho 2018                                                     */
/* CREATE USER - Paulo Mamede                                                   */
/* PROJECT     - ADEO                                                           */
/* DESCRIPTION - Package "XXADEO_SP_ITEMS_SEARCH_SQL" for Items Search          */
/********************************************************************************/


   FUNCTION XXADEO_ITEMS_SEARCH( I_search_control       IN VARCHAR2 DEFAULT 'N',
                                 I_GROUP_NO             IN XXADEO_SP_V_ITEM_MASTER.GROUP_NO%TYPE DEFAULT NULL,
                                 I_DEPT                 IN XXADEO_SP_V_ITEM_MASTER.DEPT%TYPE DEFAULT NULL,
                                 I_CLASS                IN XXADEO_SP_V_ITEM_MASTER.CLASS%TYPE DEFAULT NULL,
                                 I_SUBCLASS             IN XXADEO_SP_V_ITEM_MASTER.SUBCLASS%TYPE DEFAULT NULL,
                                 I_GRADING              IN NUMBER DEFAULT NULL,
                                 I_ITEM_TYPE            IN VARCHAR2 DEFAULT NULL,
                                 I_IS_PACK              IN XXADEO_SP_V_ITEM_MASTER.PACK_IND%TYPE DEFAULT NULL,
                                 I_TYPOLOGY             IN VARCHAR2 DEFAULT NULL,
                                 I_SUB_TYPOLOGY         IN VARCHAR2 DEFAULT NULL,
                                 I_ITEM                 IN XXADEO_SP_V_ITEM_MASTER.ITEM%TYPE DEFAULT NULL,
                                 I_GTIN                 IN VARCHAR2 DEFAULT NULL,
                                 I_DESCRIPTION          IN XXADEO_SP_V_ITEM_MASTER.SHORT_DESC%TYPE DEFAULT NULL,
                                 I_SUPPLIER_SITE        IN VARCHAR2 DEFAULT NULL,
                                 I_ITEM_STATUS          IN VARCHAR2 DEFAULT NULL,
                                 I_ASSORTMENT           IN VARCHAR2 DEFAULT NULL,
                                 I_CURR_ASSORTMENT      IN VARCHAR2 DEFAULT NULL,
                                 I_RANGE_LETTER         IN VARCHAR2 DEFAULT NULL,
                                 I_OCSSD                IN XXADEO_SP_V_ITEM_BU.OMNICHANNEL_START_DATE%TYPE DEFAULT NULL,
                                 I_OCSED                IN XXADEO_SP_V_ITEM_BU.OMNICHANNEL_END_DATE%TYPE DEFAULT NULL,
                                 I_STORE                IN XXADEO_SP_V_STORE.STORE%TYPE DEFAULT NULL)
   RETURN XXADEO_SP_ITEMS_TBL
   PIPELINED;
------------------------------------------------------------------------------

end XXADEO_SP_ITEMS_SEARCH_SQL;
/
