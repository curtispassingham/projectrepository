/********************************************************************************/
/* CREATE DATE - Julho 2018                                                     */
/* CREATE USER - Sergio BArbosa                                                 */
/* PROJECT     - ADEO                                                           */
/* DESCRIPTION - Object type "XXADEO_SP_RPM_PRICE_CHANGE_OBJ" for PC management */
/********************************************************************************/
begin
  execute immediate 'drop type XXADEO_SP_RPM_PRICE_CHANGE_OBJ';
exception
  when others then
    null;
end;
/
--
create or replace type XXADEO_SP_RPM_PRICE_CHANGE_OBJ FORCE as object(item           VARCHAR2(25),
                                                                      location       NUMBER(10),
                                                                      reason_code    NUMBER(6),
                                                                      change_amount  NUMBER(20,4),
                                                                      effective_date DATE);
/
