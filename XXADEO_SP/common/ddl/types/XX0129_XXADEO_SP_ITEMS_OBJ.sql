begin
  execute immediate 'drop type XXADEO_SP_ITEMS_OBJ';
exception
  when others then
    null;
end;
/
create or replace type XXADEO_SP_ITEMS_OBJ FORCE as object   (group_no               NUMBER(4),
                                                              dept                   NUMBER(4),
                                                              class                  NUMBER(4),
                                                              subclass               NUMBER(4),
                                                              grading                NUMBER(4),
                                                              item_type              VARCHAR2(250),
                                                              IS_PACK                VARCHAR2(4),
                                                              TYPOLOGY               VARCHAR2(30),
                                                              SUB_TYPOLOGY           VARCHAR2(30),
                                                              item                   VARCHAR2(25),
                                                              gtin                   VARCHAR2(25),
                                                              description            VARCHAR2(250),
                                                              supplier_site          NUMBER(10),
                                                              item_status            VARCHAR2(30),
                                                              assortment             VARCHAR2(30),
                                                              curr_assortment        VARCHAR2(7),
                                                              range_letter           VARCHAR2(30),
                                                              ocssd                  DATE,
                                                              ocsed                  DATE);
/
