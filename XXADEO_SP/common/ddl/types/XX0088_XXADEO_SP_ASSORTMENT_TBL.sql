/********************************************************************************/
/* CREATE DATE - Julho 2018                                                     */
/* CREATE USER - Felipe Villa                                                   */
/* PROJECT     - ADEO                                                           */
/* DESCRIPTION - Object type "XXADEO_SP_ASSORTMENT_TBL" for Assortment          */
/********************************************************************************/
begin
  execute immediate 'drop type XXADEO_SP_ASSORTMENT_TBL';
exception
  when others then
    null;
end;
/
--
create or replace type XXADEO_SP_ASSORTMENT_TBL as table of XXADEO_SP_ASSORTMENT_OBJ;
/
