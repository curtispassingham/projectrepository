/********************************************************************************/
/* CREATE DATE - Julho 2018                                                     */
/* CREATE USER - Sergio Barbosa                                                 */
/* PROJECT     - ADEO                                                           */
/* DESCRIPTION - Object type "XXADEO_PRICE_CHANGE_OBJ" for PRice Search Screen  */
/********************************************************************************/

begin
  execute immediate 'drop type XXADEO_PRICE_CHANGE_OBJ';
exception
  when others then
    null;
end;
/

create or replace type XXADEO_PRICE_CHANGE_OBJ as OBJECT( GROUP_NO NUMBER(4),
                                                          DEPT NUMBER(4),
                                                          CLASS NUMBER(4),
                                                          SUBCLASS NUMBER(4),
                                                          ITEM VARCHAR2(25),
                                                          DESCRIPTION VARCHAR2(120),
                                                          STORE_PRICE NUMBER(20,4),
                                                          ZONE_PRICE NUMBER(20,4),
                                                          EFFECTIVE_DATE DATE,
                                                          NEW_PRICE NUMBER(20,4),
                                                          NEW_APPLY_DATE DATE,
                                                          CFA VARCHAR2(1),
                                                          ORIGIN VARCHAR2(20)
                                                          );
/
