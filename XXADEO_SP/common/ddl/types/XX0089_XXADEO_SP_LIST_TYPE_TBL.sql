/********************************************************************************/
/* CREATE DATE - Julho 2018                                                     */
/* CREATE USER - Felipe Villa                                                   */
/* PROJECT     - ADEO                                                           */
/* DESCRIPTION - Object type "XXADEO_SP_LIST_TYPE_TBL"                          */
/********************************************************************************/

begin
  execute immediate 'drop type XXADEO_SP_LIST_TYPE_TBL';
exception
  when others then
    null;
end;
/

CREATE OR REPLACE TYPE XXADEO_SP_LIST_TYPE_TBL AS TABLE OF VARCHAR2(250);
/
