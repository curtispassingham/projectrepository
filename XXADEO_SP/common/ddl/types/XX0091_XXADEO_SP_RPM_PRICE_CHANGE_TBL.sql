/********************************************************************************/
/* CREATE DATE - Julho 2018                                                     */
/* CREATE USER - Sergio BArbosa                                                 */
/* PROJECT     - ADEO                                                           */
/* DESCRIPTION - Object type "XXADEO_SP_RPM_PRICE_CHANGE_OBJ" for PC management */
/********************************************************************************/
begin
  execute immediate 'drop type XXADEO_SP_RPM_PRICE_CHANGE_TBL';
exception
  when others then
    null;
end;
/
--
create or replace type XXADEO_SP_RPM_PRICE_CHANGE_TBL as table of XXADEO_SP_RPM_PRICE_CHANGE_OBJ;
/
