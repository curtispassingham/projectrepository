/********************************************************************************/
/* CREATE DATE - Julho 2018                                                     */
/* CREATE USER - Sergio Barbosa                                                 */
/* PROJECT     - ADEO                                                           */
/* DESCRIPTION - Object type "XXADEO_PRICE_RESULT_TBL" for PRice Search Screen  */
/********************************************************************************/

begin
  execute immediate 'drop type XXADEO_PRICE_RESULT_TBL';
exception
  when others then
    null;
end;
/

CREATE OR REPLACE TYPE XXADEO_PRICE_RESULT_TBL AS TABLE OF XXADEO_PRICE_CHANGE_OBJ;
/
