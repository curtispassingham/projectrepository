/********************************************************************************/
/* CREATE DATE - Julho 2018                                                     */
/* CREATE USER - Felipe Villa                                                   */
/* PROJECT     - ADEO                                                           */
/* DESCRIPTION - Object type "XXADEO_SP_ASSORTMENT_OBJ" for Assortment          */
/********************************************************************************/
begin
  execute immediate 'drop type XXADEO_SP_ASSORTMENT_OBJ';
exception
  when others then
    null;
end;
/
--
create or replace type XXADEO_SP_ASSORTMENT_OBJ FORCE as object(store                  NUMBER(10),
                                                                group_no               NUMBER(4),
                                                                dept                   NUMBER(4),
                                                                class                  NUMBER(4),
                                                                subclass               NUMBER(4),
                                                                item                   VARCHAR2(25),
                                                                item_desc              VARCHAR2(250),
                                                                retail_price           NUMBER(20,4),
                                                                assortment_mode        VARCHAR2(30),
                                                                range_letter           VARCHAR2(250),
                                                                grading                NUMBER(4),
                                                                cluster_size           VARCHAR2(30),
                                                                cs_recommendation      VARCHAR2(30),
                                                                hq_recommendation      VARCHAR2(30),
                                                                current_regular_asso   VARCHAR2(30),
                                                                store_choice           VARCHAR2(30),
                                                                supplier_site          NUMBER(10),
                                                                assortment_start_date  DATE,
                                                                assortment_end_date    DATE,
                                                                omni_cs_start_date     DATE,
                                                                omni_cs_end_date       DATE,
                                                                response_end_date      DATE,
                                                                pack_component         VARCHAR2(10),
                                                                substitute_item        VARCHAR2(25),
                                                                related_items          VARCHAR2(1),
                                                                comment_cp             VARCHAR2(4000),
                                                                season_item_ind        VARCHAR2(25),
                                                                -- Invisible Fields
                                                                not_modifiable_ind     VARCHAR2(10),
                                                                validated_date         DATE,
                                                                update_line_interface  DATE,
                                                                store_format           VARCHAR2(100),
                                                                catman_status          NUMBER(1),
                                                                history_flag           NUMBER(1),
                                                                cluster_popup_flag     NUMBER(1));
/
 
