set serveroutput on size unlimited
set escape on

declare
  -- Destination
  param_1 varchar2(30):=sys.dbms_assert.schema_name(upper('&DESTINATION'));
  -- Owner
  param_2 varchar2(30):=sys.dbms_assert.schema_name(upper('&OWNER'));

BEGIN

	execute immediate 'GRANT SELECT ON '||param_2||'.V_GROUPS TO '||param_1||' WITH GRANT OPTION';
	execute immediate 'GRANT SELECT ON '||param_2||'.V_SUPS TO '||param_1||' WITH GRANT OPTION';
	execute immediate 'GRANT SELECT ON '||param_2||'.V_DEPS TO '||param_1||' WITH GRANT OPTION';
	execute immediate 'GRANT SELECT ON '||param_2||'.V_CLASS TO '||param_1||' WITH GRANT OPTION';
	execute immediate 'GRANT SELECT ON '||param_2||'.V_SUBCLASS TO '||param_1||' WITH GRANT OPTION';
	execute immediate 'GRANT SELECT ON '||param_2||'.V_COUNTRY_TL TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.V_CODE_DETAIL_TL TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.V_STORE TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.ADDR TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.V_ADD_TYPE_TL TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.RPM_CODES TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.ITEM_SUPP_COUNTRY_LOC TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.V_ITEM_MASTER TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.V_UDA TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.ITEM_SUPPLIER TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.V_UDA_VALUES_TL TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.ITEM_LOC TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.V_ITEM_LOC TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.ITEM_IMAGE TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.PACKITEM TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.ITEM_MASTER_CFA_EXT TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.RTK_ERRORS TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.RELATED_ITEM_HEAD TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.RELATED_ITEM_DETAIL TO '||param_1;
	execute immediate 'GRANT EXECUTE ON '||param_2||'.SQL_LIB TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.RPM_FUTURE_RETAIL TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.RPM_PRICE_CHANGE TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.RPM_ITEM_ZONE_PRICE TO '||param_1;
	execute immediate 'GRANT EXECUTE ON '||param_2||'.LANGUAGE_SQL TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.LANG TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.SYSTEM_OPTIONS TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.RPM_ZONE_LOCATION TO '||param_1;
  execute immediate 'GRANT SELECT ON '||param_2||'.RPM_ZONE TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.PRICE_HIST TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.ITEM_LOC_SOH TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.VAT_CODE_RATES TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.COMPETITOR TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.V_COMP_STORE TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.V_COMP_PRICE_HIST TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.V_STORE_FORMAT_TL TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.FILTER_GROUP_ORG TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.RSM_USER_ROLE TO '||param_1;
	execute immediate 'GRANT EXECUTE ON '||param_2||'.GET_VDATE TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.PERIOD TO '||param_1;
	execute immediate 'GRANT EXECUTE ON '||param_2||'.RPM_CONSTANTS TO '||param_1;
	execute immediate 'GRANT EXECUTE ON '||param_2||'.XXADEO_CFA_KEY_VAL_TBL TO '||param_1;
	execute immediate 'GRANT EXECUTE ON '||param_2||'.XXADEO_CFA_KEY_VAL_OBJ TO '||param_1;
	execute immediate 'GRANT EXECUTE ON '||param_2||'.XXADEO_CFA_SQL TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.RPM_ITEM_LOC TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.DAILY_PURGE TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.RPM_ZONE_FUTURE_RETAIL TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.V_WH TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.XXADEO_MOM_DVM TO '||param_1;
	execute immediate 'GRANT SELECT, INSERT, UPDATE ON '||param_2||'.XXADEO_RPM_STAGE_PRICE_CHANGE TO '||param_1;
	execute immediate 'GRANT EXECUTE ON '||param_2||'.XXADEO_GET_MOM_DVM TO '||param_1;
  execute immediate 'GRANT EXECUTE ON '||param_2||'.GET_PRIMARY_LANG TO '||param_1;
  execute immediate 'GRANT SELECT ON '||param_2||'.CFA_ATTRIB TO '||param_1;
  execute immediate 'GRANT SELECT ON '||param_2||'.V_CODE_DETAIL_TL TO '||param_1;
  execute immediate 'GRANT SELECT ON '||param_2||'.V_RTK_ERRORS_TL TO '||param_1;
  execute immediate 'GRANT SELECT ON '||param_2||'.STORE TO '||param_1;
  execute immediate 'GRANT SELECT ON '||param_2||'.UDA_ITEM_LOV TO '||param_1;
  execute immediate 'GRANT SELECT ON '||param_2||'.SUPS_CFA_EXT TO '||param_1;
  execute immediate 'GRANT SELECT ON '||param_2||'.LOGGER_LOGS TO '||param_1;
  execute immediate 'GRANT EXECUTE ON '||param_2||'.CONVERT_COMMA_LIST TO '||param_1;
  execute immediate 'GRANT EXECUTE ON '||param_2||'.GET_USER TO '||param_1;
  execute immediate 'GRANT EXECUTE ON '||param_2||'.GET_APP_USER TO '||param_1;
  execute immediate 'GRANT EXECUTE ON '||param_2||'.XXADEO_RPM_CONSTANTS_SQL TO '||param_1;
  execute immediate 'GRANT SELECT ON '||param_2||'.XXADEO_PRICE_CHANGE_SEQ TO '||param_1;
  execute immediate 'GRANT SELECT ON '||param_2||'.XXADEO_PRICE_PROCESS_ID_SEQ TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.DEPS_CFA_EXT TO '||param_1;

END;
/
