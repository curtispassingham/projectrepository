set serveroutput on size unlimited
set escape on

declare

  -- Owner
  param_1 varchar2(30):=sys.dbms_assert.schema_name(upper('&OWNER'));
  -- DBLink
  param_2 varchar2(30):=upper('&DBLINK');

BEGIN

	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_ITEM_BU FOR XXADEO_ITEM_BU@'||param_2;
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_CLUSTERS FOR XXADEO_CLUSTERS@'||param_2;
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_ASSORTMENT_CLUSTER FOR XXADEO_ASSORTMENT_CLUSTER@'||param_2;
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_ASSORTMENT FOR XXADEO_ASSORTMENT@'||param_2;
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_ASSORTMENT_STORE FOR XXADEO_ASSORTMENT_STORE@'||param_2;
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_STORE_CLUSTER FOR XXADEO_STORE_CLUSTER@'||param_2;
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_ASSORTMENT_MODE_TL FOR XXADEO_ASSORTMENT_MODE_TL@'||param_2;
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_ASSORTMENT_VALUE_TL FOR XXADEO_ASSORTMENT_VALUE_TL@'||param_2;

END;
/
