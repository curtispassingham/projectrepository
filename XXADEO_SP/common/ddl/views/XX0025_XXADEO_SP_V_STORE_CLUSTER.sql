/******************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Pedro Vieira                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_SP_V_STORE_CLUSTER" view of 	   		      		*/
/*					XXADEO_STORE_CLUSTER table				                  							*/
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_SP_V_STORE_CLUSTER AS
SELECT DEPT,
	CLASS,
	SUBCLASS,
	STORE,
	CLUSTER_ID,
	START_DATE,
	LINK_STORE_CLUSTER,
	WEEK,
	CREATE_ID,
	CREATE_DATETIME
FROM XXADEO_STORE_CLUSTER;

-- Add comments to the columns
COMMENT ON COLUMN XXADEO_SP_V_STORE_CLUSTER.DEPT is 'Department Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_STORE_CLUSTER.CLASS is 'Class Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_STORE_CLUSTER.SUBCLASS is 'Subclass Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_STORE_CLUSTER.STORE is 'Store Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_STORE_CLUSTER.CLUSTER_ID is 'Cluster identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_STORE_CLUSTER.START_DATE is 'Date which the association becomes valid. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_STORE_CLUSTER.LINK_STORE_CLUSTER is 'Indicator of the relationship between store and cluster if it is activate (Y) or deactivate (N). Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_STORE_CLUSTER.WEEK is 'Week Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_STORE_CLUSTER.CREATE_ID is 'User that created the record.'
;
COMMENT ON COLUMN XXADEO_SP_V_STORE_CLUSTER.CREATE_DATETIME is 'Date in which the record was created.'
;
