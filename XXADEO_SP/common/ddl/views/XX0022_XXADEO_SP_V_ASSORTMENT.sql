/******************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Pedro Vieira                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_SP_V_ASSORTMENT_CLUSTER" view of 	          */
/*					XXADEO_ASSORTMENT_CLUSTER table		        	          */
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_SP_V_ASSORTMENT_CLUSTER AS
SELECT ITEM
	,BU
	,START_DATE
	,CLUSTER_ID
	,END_DATE
	,HQ_RECOMMENDATION
	,WEEK
	,CREATE_ID
	,CREATE_DATETIME
	,LAST_UPDATE_ID
	,LAST_UPDATE_DATETIME
FROM XXADEO_ASSORTMENT_CLUSTER;

-- Add comments to the columns
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_CLUSTER.ITEM is 'Item Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_CLUSTER.BU is 'BU Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_CLUSTER.START_DATE is 'Date on which the recommended assortment becomes effective. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_CLUSTER.CLUSTER_ID is 'Cluster identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_CLUSTER.END_DATE is 'Date on which the recommended assortment is no longer valid. Calculated as the next start date minus one day or null if no other start date exists..'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_CLUSTER.HQ_RECOMMENDATION is 'Recommended assortment for the item.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_CLUSTER.WEEK is 'Week Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_CLUSTER.CREATE_ID is 'User that created the record.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_CLUSTER.CREATE_DATETIME is 'Date in which the record was created.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_CLUSTER.LAST_UPDATE_ID is 'User that last updated the record.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_CLUSTER.LAST_UPDATE_DATETIME is 'Date in which the record was last updated.'
;
