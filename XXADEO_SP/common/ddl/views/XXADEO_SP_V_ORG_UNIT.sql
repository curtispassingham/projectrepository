--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ORG_UNIT cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ORG_UNIT (ORG_UNIT_ID, 
	                                                   DESCRIPTION, 
	                                                   SET_OF_BOOKS_ID, 
	                                                   CREATE_ID, 
	                                                   CREATE_DATETIME
	                                                  ) AS 
SELECT ORG_UNIT_ID, 
       DESCRIPTION, 
       SET_OF_BOOKS_ID, 
       CREATE_ID, 
       CREATE_DATETIME
  FROM RMS.ORG_UNIT
/
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ORG_UNIT.ORG_UNIT_ID IS 'holds the oracle organizational unit ID';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ORG_UNIT.DESCRIPTION IS 'holds the organizational unit description';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ORG_UNIT.SET_OF_BOOKS_ID IS 'Set of Books Id';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ORG_UNIT.CREATE_ID IS 'This column holds the User id of the user who created the record.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ORG_UNIT.CREATE_DATETIME IS 'This column holds the record creation date.';
