-- Create view
create or replace view XXADEO_SP_V_RPM_FUTURE_RETAIL as
select  future_retail_id              ,
        item                          ,
        dept                          ,
        class                         ,
        subclass                      ,
        zone_node_type                ,
        location                      ,
        action_date                   ,
        selling_retail                ,
        selling_retail_currency       ,
        selling_uom                   ,
        multi_units                   ,
        multi_unit_retail             ,
        multi_unit_retail_currency    ,
        multi_selling_uom             ,
        clear_exception_parent_id     ,
        clear_retail                  ,
        clear_retail_currency         ,
        clear_uom                     ,
        simple_promo_retail           ,
        simple_promo_retail_currency  ,
        simple_promo_uom              ,
        on_simple_promo_ind           ,
        on_complex_promo_ind          ,
        price_change_id               ,
        price_change_display_id       ,
        pc_exception_parent_id        ,
        pc_change_type                ,
        pc_change_amount              ,
        pc_change_currency            ,
        pc_change_percent             ,
        pc_change_selling_uom         ,
        pc_null_multi_ind             ,
        pc_multi_units                ,
        pc_multi_unit_retail          ,
        pc_multi_unit_retail_currency ,
        pc_multi_selling_uom          ,
        pc_price_guide_id             ,
        clearance_id                  ,
        clearance_display_id          ,
        clear_mkdn_index              ,
        clear_start_ind               ,
        clear_change_type             ,
        clear_change_amount           ,
        clear_change_currency         ,
        clear_change_percent          ,
        clear_change_selling_uom      ,
        clear_price_guide_id          ,
        loc_move_from_zone_id         ,
        loc_move_to_zone_id           ,
        location_move_id              ,
        lock_version                  ,
        item_parent                   ,
        diff_id                       ,
        zone_id                       ,
        max_hier_level                ,
        cur_hier_level
  from rpm_future_retail;

-- Add comments to the view
comment on table XXADEO_SP_V_RPM_FUTURE_RETAIL
  is 'this table will hold future retail information for items/locations.';
-- Add comments to the columns
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.future_retail_id
  is 'id that uniquely identifies the future retail record.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.item
  is 'id that uniquely identifies the item on the future retail record. items on the rpm_future_retail table are always transaction level items.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.dept
  is 'the department of the item.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.class
  is 'the class of the item.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.subclass
  is 'the subclass of the item.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.zone_node_type
  is 'indicates the type of location on the rpm_future_retail record. valid values include: 0 store; 2 warehouse.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.location
  is 'id that uniquely identifies the location on the rpm_future_retail record. this field will contain either a store or a warehouse.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.action_date
  is 'the action date of the future retail event.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.selling_retail
  is 'this field contains the regular unit retail price in the selling unit of measure for the item/location combination on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.selling_retail_currency
  is 'the currency for which the selling retail is represented.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.selling_uom
  is 'this field contains the selling unit of measure for the item/locations single selling regular unit retail on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.multi_units
  is 'this field contains the number of units associated with the multi-unit regular retail for the item/location combination on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.multi_unit_retail
  is 'this field holds the multi-unit regular retail in the multi-unit selling unit of measure for the item/location combination on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.multi_unit_retail_currency
  is 'the currency for which the multi-unit retail is represented.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.multi_selling_uom
  is 'this field holds the selling unit of measure for the item/locations regular multi-unit retail on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clear_exception_parent_id
  is 'id that uniquely identifies the clearance that this clearance is an exception to. only filled if this clearance is a clearance exception.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clear_retail
  is 'this field contains the clearance unit retail price in the clearance unit of measure for the item/location combination on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clear_retail_currency
  is 'the currency for which the clearance unit retail is represented';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clear_uom
  is 'this field contains the selling unit of measure for the item/locations clearance unit retail on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.simple_promo_retail
  is 'this field contains the simple promotion unit retail price in the promotion unit of measure for the item/location combination on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.simple_promo_retail_currency
  is 'the currency for which the promotional unit retail is represented.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.simple_promo_uom
  is 'this field contains the selling unit of measure for the item/locations simple promotion unit retail on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.on_simple_promo_ind
  is 'indicates if a simple promotion affects this item/location for this date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.on_complex_promo_ind
  is 'indicates if a complex promotion affects this item/location for this date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.price_change_id
  is 'id that uniquely identifies the price change. this field will be populated if there is either a price change occurring on the action date, or if there is a pricing event occurring on the action date that affects the regular retail for the price change on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.price_change_display_id
  is 'display id that uniquely identifies the price change. this field will be populated if there is either a price change occurring on the action date, or if there is a pricing event occurring on the action date that affects the regular retail for the price change on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.pc_exception_parent_id
  is 'id that uniquely identifies the price change that this price change is an exception to. only filled if this price change is a price change exception.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.pc_change_type
  is 'the type of change being applied to the item/location on the price change. valid values include:  0 change by percent;  2 fixed price;  4 exclude; 5 change by amount;';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.pc_change_amount
  is 'the amount of change applied to the item/location on the price change.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.pc_change_currency
  is 'when the value in the pc_change_type field is either 1  change by amount, or 2  fixed price, this field holds the currency associated with the amount field.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.pc_change_percent
  is 'the percentage amount of change applied to the item/location on the price change.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.pc_change_selling_uom
  is 'the selling uom associated with the price change.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.pc_null_multi_ind
  is 'indicates whether or not the price change is changing the multi-unit regular retail of the item/location. valid values include: 1 price change is not changing the multi-unit retail, and 0 price change is changing the multi-unit retail.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.pc_multi_units
  is 'this field contains the number of units associated with the multi-unit retail for the item/location combination on the price change.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.pc_multi_unit_retail
  is 'this field holds the multi-unit retail in the multi-unit selling unit of measure for the item/location combination on the price change.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.pc_multi_unit_retail_currency
  is 'the currency for which the multi-unit retail is represented.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.pc_multi_selling_uom
  is 'the multi-unit selling uom associated with the price change.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.pc_price_guide_id
  is 'id that uniquely identifies the price guide attached to the price change.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clearance_id
  is 'id that uniquely identifies the clearance. this field will be populated if there is either a clearance occurring on the action date, or if there is a pricing event occurring on the action date that affects the clearance retail on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clearance_display_id
  is 'display id that uniquely identifies the clearance. this field will be populated if there is either a clearance occurring on the action date, or if there is a pricing event occurring on the action date that affects the clearance retail on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clear_mkdn_index
  is 'the current count of clearances for the item/location activated since the last reset date. this field will be populated if there is either a new clearance event occurring on the action date, or if there is a pricing event occurring on the action date that affects the clearance retail on the action date.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clear_start_ind
  is 'this field indicates the status of the clearance markdown. valid values include:  1 clearance is starting;  2 clearance is in progress;  3 clearance is ending;';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clear_change_type
  is 'the type of change being applied to the item/location on the clearance. valid values include: 0 change by percent;  1 change by amount; 2 fixed price;  4  exclude;';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clear_change_amount
  is 'the amount of change applied to the item/location on the clearance. for clearance price changes with a change type of 1 change by amount, this is the amount that will be deducted from the current regular retail. for clearance price changes with a change time of 2  fixed price, this is the price of the item/location on clearance.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clear_change_currency
  is 'when the value in the change_type field is either 1 change by amount, or 2 fixed price, this field holds the currency of the amount.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clear_change_percent
  is 'the percentage amount of change applied to the item/location on the clearance. this field will only hold a value for clearances with a change type of 0 change by percent.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clear_change_selling_uom
  is 'the selling uom associated with the clearance.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.clear_price_guide_id
  is 'id that uniquely identifies the price guide attached to the clearance.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.loc_move_from_zone_id
  is 'unique id of pricing zone this item/location is moving out of. blank if this item/location is being added to a zone group structure.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.loc_move_to_zone_id
  is 'unique id of the pricing zone this item/location is moving into. blank if this item/location is being removed from a zone group structure.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.location_move_id
  is 'unique id of a location move that this item/location is a part of on this day.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.lock_version
  is 'field used for database locking purposes, ensures only one process is modifying this record at a time.';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.item_parent
  is 'The item parent for the item when the item is a transaction level item';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.diff_id
  is 'The diff id for the item parent that the item falls under';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.zone_id
  is 'The zone id for the location when the location field is a location';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.max_hier_level
  is 'The maximum hierarchy level that this record could roll up to';
comment on column XXADEO_SP_V_RPM_FUTURE_RETAIL.cur_hier_level
  is 'The current hierarchy level for this record.';
