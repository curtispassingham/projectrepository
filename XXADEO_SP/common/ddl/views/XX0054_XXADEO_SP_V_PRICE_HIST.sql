CREATE OR REPLACE VIEW XXADEO_SP_V_PRICE_HIST AS
SELECT ph.tran_type,
       ph.reason,
       ph.event,
       ph.item,
       ph.loc,
       ph.loc_type,
       ph.unit_cost,
       ph.unit_retail,
       ph.selling_unit_retail,
       ph.selling_uom,
       ph.action_date,
       ph.multi_units,
       ph.multi_unit_retail,
       ph.multi_selling_uom,
       ph.post_date,
       ph.create_id,
       ph.create_datetime
FROM PRICE_HIST ph;
-- Add comments to the table
comment on table XXADEO_SP_V_PRICE_HIST
  is 'This table contains a history of all price changes that have ever been applied within the system.  History is kept on this table forever by SKU/store combination.';
-- Add comments to the columns
comment on column XXADEO_SP_V_PRICE_HIST.tran_type
  is 'Contains a code number which indicates the type of transaction which caused the price change.  Valid values are: 	 0 = New item added 	 2 = Unit cost was change 	 4 = Single unit retail was changed 	 8 = Single unit retail was changed in Clearance 	 9 = Single unit retail was changed in Promotion                   10 =  Multi-unit retail was changed 	11 = Single unit retail and Multi-unit retail were changed 	99 = Item was deleted from file';
comment on column XXADEO_SP_V_PRICE_HIST.reason
  is 'Contains the reason for the price change.  If the record is written as a result of the creation of a new item, then the reason code is zero.  Otherwise, the values come from the mkd_reason table and are the same as the reason code used on the price change event.';
comment on column XXADEO_SP_V_PRICE_HIST.event
  is 'This field contains the promotional event for which the price of the SKU is being affected.';
comment on column XXADEO_SP_V_PRICE_HIST.item
  is 'This field identifies the unique alphanumeric value for the transaction level item.';
comment on column XXADEO_SP_V_PRICE_HIST.loc
  is 'Contains the number that uniquely identifies the location.';
comment on column XXADEO_SP_V_PRICE_HIST.loc_type
  is 'Identifies the location as a Store or a Warehouse.';
comment on column XXADEO_SP_V_PRICE_HIST.unit_cost
  is 'This field holds the primary supplier cost, i.e. a record is written to this table when the primary supplier cost is changed.';
comment on column XXADEO_SP_V_PRICE_HIST.unit_retail
  is 'Contains the current single unit retail in the standard unit of measure.  If the record is being written as a result of a change in the single unit retail, then this field contains the new single unit retail.  This field is stored in the local currency.';
comment on column XXADEO_SP_V_PRICE_HIST.selling_unit_retail
  is 'Contains the current single unit retail in the selling unit of measure.  If the record is being written as a result of a change in the single unit retail, then this field contains the new single unit retail.  This field is stored in the local currency.';
comment on column XXADEO_SP_V_PRICE_HIST.selling_uom
  is 'Contains the selling unit of measure for an items single-unit retail.';
comment on column XXADEO_SP_V_PRICE_HIST.action_date
  is 'Contains the date on which the price change went effect.';
comment on column XXADEO_SP_V_PRICE_HIST.multi_units
  is 'Contains the current multi-units.  If the record is being written as a result of a change in the multi-unit retail, then this field contains the new multi-units.';
comment on column XXADEO_SP_V_PRICE_HIST.multi_unit_retail
  is 'Contains the current multi-unit retail in the selling unit of measure.  If the record is being written as a result of a change in the multi-unit retail, then this field contains the new multi-unit retail.  This field is stored in the local currency.';
comment on column XXADEO_SP_V_PRICE_HIST.multi_selling_uom
  is 'Contains the selling unit of measure for an items multi-unit retail.';
comment on column XXADEO_SP_V_PRICE_HIST.post_date
  is 'The POST_DATE column will store that date that a record is inserted or updated in the PRICE_HIST table.';
comment on column XXADEO_SP_V_PRICE_HIST.create_id
  is 'This column holds the User id of the user who created the record.';
comment on column XXADEO_SP_V_PRICE_HIST.create_datetime
  is 'This column holds the record creation date.';
