/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Sérgio Barbosa                                               */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_SP_V_MOM_DVM"                            */
/******************************************************************************/
create or replace view XXADEO_SP_V_MOM_DVM as
select func_area,
       parameter,
       bu,
       value_1,
       value_2,
       description,
       last_update_id,
       last_update_datetime
  from xxadeo_mom_dvm a;
-- Add comments to the table
comment on table XXADEO_SP_V_MOM_DVM
  is 'Table used to store any required cross references. The table is specified in a generic format so itâ€™s able to accommodate multiple needs.';
-- Add comments to the columns
comment on column XXADEO_SP_V_MOM_DVM.func_area
  is 'Specifies the functional area of the record. E.g. UDA, CFA';
comment on column XXADEO_SP_V_MOM_DVM.parameter
  is 'Identifies the parameter that is known by the program which needs to read/write the information.';
comment on column XXADEO_SP_V_MOM_DVM.bu
  is 'ADEO Business Unit (If the record is BU specific identifies the BU attached to it). The code used corresponds to the one present in the Organization Hierarchy â€“ Level Area.';
comment on column XXADEO_SP_V_MOM_DVM.value_1
  is 'Holds the first value which relates to the parameter.';
comment on column XXADEO_SP_V_MOM_DVM.value_2
  is 'Holds the second value which relates to the parameter.';
comment on column XXADEO_SP_V_MOM_DVM.description
  is 'Description of the purposes of the record being Cross Referenced.';
comment on column XXADEO_SP_V_MOM_DVM.last_update_id
  is 'User that last updated the record.';
comment on column XXADEO_SP_V_MOM_DVM.last_update_datetime
  is 'Date when the record was last updated.';
