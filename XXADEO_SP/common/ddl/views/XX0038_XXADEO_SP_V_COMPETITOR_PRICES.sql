create or replace view XXADEO_SP_V_COMPETITOR_PRICES as
select ch.item ITEM,
       cs.STORE STORE,
       c.comp_name||' | '|| c.city COMPETITOR_NAME,
       cs.store_name COMPETITOR_STORE,
       ch.comp_retail COMP_RETAIL_PRICE,
       ch.prom_end_date PROMO_END_DATE,
       trunc(ch.rec_date) CAPTURE_DATE
  from competitor c,
       v_comp_store cs,
       v_comp_price_hist ch
 where c.competitor = cs.competitor
   and cs.STORE = ch.comp_store;
comment on table XXADEO_SP_V_COMPETITOR_PRICES is 'This view will be used to display the Competitor Pricing Info using a security policy to filter User acess.';

comment on column XXADEO_SP_V_COMPETITOR_PRICES.ITEM is 'This field identifies the unique alphanumeric value for the transaction level item that was competitively shopped.';
comment on column XXADEO_SP_V_COMPETITOR_PRICES.STORE is 'contains the number to uniquely identify a merchandising store. ';
comment on column XXADEO_SP_V_COMPETITOR_PRICES.COMPETITOR_NAME is 'contains the number to uniquely identify a competitor.';
comment on column XXADEO_SP_V_COMPETITOR_PRICES.COMPETITOR_STORE is 'This field contains the number to uniquely identify a competitors store. ice_hist for update.';
comment on column XXADEO_SP_V_COMPETITOR_PRICES.COMP_RETAIL_PRICE is 'the retail price at the competitors store.';
comment on column XXADEO_SP_V_COMPETITOR_PRICES.PROMO_END_DATE is 'contains the effective end date of the competitors promotional price.';
comment on column XXADEO_SP_V_COMPETITOR_PRICES.CAPTURE_DATE is 'the date that the items price was recorded at the competitors store.';
