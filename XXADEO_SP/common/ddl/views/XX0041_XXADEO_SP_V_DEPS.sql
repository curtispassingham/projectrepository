CREATE OR REPLACE VIEW XXADEO_SP_V_DEPS AS
SELECT "DIVISION",
	   "GROUP_NO",
	   "DEPT",
	   "DEPT_NAME",
	   "PURCHASE_TYPE",
	   "MARKUP_CALC_TYPE",
	   "OTB_CALC_TYPE"
  FROM V_DEPS;
comment on table XXADEO_SP_V_DEPS is 'This view will be used to display the Department LOVs using a security policy to filter User access.';
comment on column XXADEO_SP_V_DEPS.DIVISION is 'Contains the number of the division of which the group is a member.';
comment on column XXADEO_SP_V_DEPS.GROUP_NO is 'Contains the number of the group in which the department exists.';
comment on column XXADEO_SP_V_DEPS.DEPT is 'Contains the number which uniquely identifies the department.';
comment on column XXADEO_SP_V_DEPS.PURCHASE_TYPE is 'Contains a code which indicates whether items in this department are normal merchandise, consignment stock or concession items. Valid values are: 0 = Normal Merchandise,  1 = Consignment Stock, 2 = Concession Items';
comment on column XXADEO_SP_V_DEPS.MARKUP_CALC_TYPE is 'Contains the code letter which determines how markup is calculated in this department.  Valid values are: C = Cost, R = Retail';
comment on column XXADEO_SP_V_DEPS.OTB_CALC_TYPE is 'Contains the code letter which determines how OTB is calculated in this department.  Valid values are: C = Cost, R = Retail';
