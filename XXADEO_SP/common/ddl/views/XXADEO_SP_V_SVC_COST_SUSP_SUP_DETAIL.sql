--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET (PROCESS_ID,
                                                                CHUNK_ID,
                                                                ROW_SEQ,
                                                                ACTION,
                                                                PROCESS$STATUS,
                                                                COST_CHANGE,
                                                                SUPPLIER,
                                                                ORIGIN_COUNTRY_ID,
                                                                ITEM,
                                                                BRACKET_VALUE1,
                                                                BRACKET_UOM1,
                                                                BRACKET_VALUE2,
                                                                UNIT_COST,
                                                                COST_CHANGE_TYPE,
                                                                COST_CHANGE_VALUE,
                                                                RECALC_ORD_IND,
                                                                DEFAULT_BRACKET_IND,
                                                                DEPT,
                                                                SUP_DEPT_SEQ_NO,
                                                                DELIVERY_COUNTRY_ID,
                                                                CREATE_ID,
                                                                CREATE_DATETIME,
                                                                LAST_UPD_ID,
                                                                LAST_UPD_DATETIME
                                                               ) AS 
SELECT PROCESS_ID,
       CHUNK_ID,
       ROW_SEQ,
       ACTION,
       PROCESS$STATUS,
       COST_CHANGE,
       SUPPLIER,
       ORIGIN_COUNTRY_ID,
       ITEM,
       BRACKET_VALUE1,
       BRACKET_UOM1,
       BRACKET_VALUE2,
       UNIT_COST,
       COST_CHANGE_TYPE,
       COST_CHANGE_VALUE,
       RECALC_ORD_IND,
       DEFAULT_BRACKET_IND,
       DEPT,
       SUP_DEPT_SEQ_NO,
       DELIVERY_COUNTRY_ID,
       CREATE_ID,
       CREATE_DATETIME,
       LAST_UPD_ID,
       LAST_UPD_DATETIME
  FROM RMS.SVC_COST_SUSP_SUP_DETAIL 
/
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.CHUNK_ID IS 'Uniquely identifies a chunk. The data for a process is split in multiple chunks. The Chunk information is present in SVC_PROCESS_CHUNKS.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.ROW_SEQ IS 'The rows sequence. Should be unique within a process-ID';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.COST_CHANGE IS 'Refer to COST_SUSP_SUP_DETAIL.COST_CHANGE.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.SUPPLIER IS 'Refer to COST_SUSP_SUP_DETAIL.SUPPLIER.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.ORIGIN_COUNTRY_ID IS 'Refer to COST_SUSP_SUP_DETAIL.ORIGIN_COUNTRY_ID.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.ITEM IS 'Refer to COST_SUSP_SUP_DETAIL.ITEM.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.BRACKET_VALUE1 IS 'Refer to COST_SUSP_SUP_DETAIL.BRACKET_VALUE1.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.BRACKET_UOM1 IS 'Refer to COST_SUSP_SUP_DETAIL.BRACKET_UOM1.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.BRACKET_VALUE2 IS 'Refer to COST_SUSP_SUP_DETAIL.BRACKET_VALUE2.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.UNIT_COST IS 'Refer to COST_SUSP_SUP_DETAIL.UNIT_COST.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.COST_CHANGE_TYPE IS 'Refer to COST_SUSP_SUP_DETAIL.COST_CHANGE_TYPE.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.COST_CHANGE_VALUE IS 'Refer to COST_SUSP_SUP_DETAIL.COST_CHANGE_VALUE.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.RECALC_ORD_IND IS 'Refer to COST_SUSP_SUP_DETAIL.RECALC_ORD_IND.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.DEFAULT_BRACKET_IND IS 'Refer to COST_SUSP_SUP_DETAIL.DEFAULT_BRACKET_IND.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.DEPT IS 'Refer to COST_SUSP_SUP_DETAIL.DEPT.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.SUP_DEPT_SEQ_NO IS 'Refer to COST_SUSP_SUP_DETAIL.SUP_DEPT_SEQ_NO.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.DELIVERY_COUNTRY_ID IS 'Refer to COST_SUSP_SUP_DETAIL.DELIVERY_COUNTRY_ID.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.CREATE_ID IS 'The user -id who inserted this record.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.CREATE_DATETIME IS 'The date and time when the record was inserted.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.LAST_UPD_ID IS 'The user -id who last updated this record.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_DET.LAST_UPD_DATETIME IS 'The date and time when the record was last updated.';