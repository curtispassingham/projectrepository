--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS6_AG1 cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS6_AG1 (ITEM, 
                                                            LMPT_HYPER_100_ITEM, 
                                                            LMPT_PRICE_POLICY_ITEM, 
                                                            LMPT_PURCHASE_BLK_ITEM, 
                                                            LMPT_PV_PUBLICATION_ITEM, 
                                                            LMPT_RANKING_ITEM, 
                                                            LMPT_RTL_BLK_ITEM
                                                           ) AS 
SELECT ITEM,   
       LMPT_HYPER_100_ITEM,
       LMPT_PRICE_POLICY_ITEM,
       LMPT_PURCHASE_BLK_ITEM,
       LMPT_PV_PUBLICATION_ITEM,
       LMPT_RANKING_ITEM,
       LMPT_RTL_BLK_ITEM 
  FROM (SELECT ITEM,
               VARCHAR2_2 LMPT_HYPER_100_ITEM,
               VARCHAR2_3 LMPT_PRICE_POLICY_ITEM,
               VARCHAR2_4 LMPT_PURCHASE_BLK_ITEM,
               VARCHAR2_5 LMPT_PV_PUBLICATION_ITEM,
               NUMBER_11  LMPT_RANKING_ITEM,
               VARCHAR2_1 LMPT_RTL_BLK_ITEM 
          FROM RMS.ITEM_MASTER_CFA_EXT 
         WHERE group_id = 27
        )
/
