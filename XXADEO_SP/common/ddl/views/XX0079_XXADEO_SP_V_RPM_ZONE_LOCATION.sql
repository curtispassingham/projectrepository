/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Sérgio Barbosa                                               */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_SP_V_RPM_ZONE_LOCATION"                         */
/******************************************************************************/
create or replace view xxadeo_sp_v_rpm_zone_location as
select zone_location_id,
       zone_id,
       location,
       loc_type,
       lock_version
  from rpm_zone_location;
-- Add comments to the table
comment on table xxadeo_sp_v_rpm_zone_location
  is 'table that determines which stores and/or warehouses are assigned to a given zone.';
-- Add comments to the columns
comment on column xxadeo_sp_v_rpm_zone_location.zone_location_id
  is 'unique identifier for the location zone assignment.';
comment on column xxadeo_sp_v_rpm_zone_location.zone_id
  is 'the zone that the location is assigned to.';
comment on column xxadeo_sp_v_rpm_zone_location.location
  is 'the location identifier.';
comment on column xxadeo_sp_v_rpm_zone_location.loc_type
  is 'indicates whether the location is a store or warehouse.  valid values include: 0 - store ,1- zone ,2- warehouse.';
comment on column xxadeo_sp_v_rpm_zone_location.lock_version
  is 'field to assist with optimistic locking.';
