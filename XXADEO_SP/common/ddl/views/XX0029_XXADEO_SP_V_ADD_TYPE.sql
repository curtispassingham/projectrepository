CREATE OR REPLACE VIEW XXADEO_SP_V_ADD_TYPE AS
SELECT "ADDRESS_TYPE",
       "TYPE_DESC",
       "EXTERNAL_ADDR_IND",
       "LANG",
       "ISO_CODE" 
  FROM V_ADD_TYPE_TL;
comment on table XXADEO_SP_V_ADD_TYPE is 'This translation view returns descriptions of address type within Oracle Retail in user language. If the description is not defined in user language, it returns the description in the system data integration language.';
comment on column XXADEO_SP_V_ADD_TYPE.ADDRESS_TYPE is 'Code identifying the address type within Oracle Retail.';
comment on column XXADEO_SP_V_ADD_TYPE.TYPE_DESC is 'Description of the address type in user language or in the system data integration language.';
comment on column XXADEO_SP_V_ADD_TYPE.EXTERNAL_ADDR_IND is 'Indicates if the address type is added by an external system.';
comment on column XXADEO_SP_V_ADD_TYPE.LANG is 'Contains the number which uniquely identifies a language in RMS.';
comment on column XXADEO_SP_V_ADD_TYPE.ISO_CODE is 'This field holds the ISO code associated with the given language.';
