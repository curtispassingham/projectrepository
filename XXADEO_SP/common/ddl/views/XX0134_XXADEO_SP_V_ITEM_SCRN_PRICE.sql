create or replace view xxadeo_sp_v_item_scrn_price as
with cfa_val as
 (select item, bu, attr_id, cfa_value, cd.CODE_DESC
    from (select cfa_value,
                 cfa_attribute,
                 cfa.attrib_id attr_id,
                 item,
                 code_type
            from cfa_attrib cfa,
                 (select group_id, cfa_attribute, cfa_value, item
                    from xxadeo_sp_v_item_cfa_ext imcfa unpivot(cfa_value for cfa_attribute in(VARCHAR2_1,
                                                                                               VARCHAR2_2,
                                                                                               VARCHAR2_3,
                                                                                               VARCHAR2_4,
                                                                                               VARCHAR2_5,
                                                                                               VARCHAR2_6,
                                                                                               VARCHAR2_7,
                                                                                               VARCHAR2_8,
                                                                                               VARCHAR2_9,
                                                                                               VARCHAR2_10))) cfa_unc
           where cfa.group_id = cfa_unc.group_id
             and upper(cfa.storage_col_name) = upper(cfa_unc.cfa_attribute)) aux,
         xxadeo_sp_v_mom_dvm dvm,
         xxadeo_sp_v_code_detail cd
   where (dvm.value_1)            = aux.attr_id
     and func_area                = 'CFA'
     and parameter                = 'RTL_BLK_ITEM'
     and aux.code_type            = cd.code_type
     and aux.cfa_value            = cd.code)
select im.item as item,
       st.store as loc,
       st.area as bu,
       fr.curr_s_price as store_price,
       fr.curr_s_date as store_date,
       fr.currency as store_currency,
       fr.curr_z_price as zone_price,
       fr.curr_z_date as zone_date,
       fr.currency as zone_currency,
       NVL(fr.future_s_price, fr.future_z_price) as next_store_price,
       NVL(fr.future_s_date, fr.future_z_date) as next_store_date,
       fr.currency as next_store_currency,
       cfa.cfa_value as code,
       cfa.code_desc as code_desc,
       stc.effective_date as effective_date,
       stc.standart_cost as standart_cost,
       stc.vat_rate as vat_rate,
       stc.sales_margin_store as sales_margin_store,
       stc.sales_margin_zone as sales_margin_zone,
       stc.deal_ind as deal_ind
  from xxadeo_sp_v_item_master   im,
       xxadeo_sp_v_price_retail fr,
       xxadeo_sp_v_standart_tsf_cost stc,
       xxadeo_sp_v_store             st,
       cfa_val                       cfa
 where fr.item      = stc.item (+)
   and fr.loc       = stc.location (+)
   and fr.loc       = st.store
   and fr.item      = im.item
   and cfa.bu(+)    = st.area
   and cfa.item(+)  = im.item;
