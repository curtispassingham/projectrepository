/******************************************************************************/
/* CREATE DATE - Aug 2018                                                     */
/* CREATE USER - Vitor Ferreira                                               */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_SP_V_RPM_STG_PRC_CHANGE" view of              */
/*          XXADEO_RPM_STAGE_PRICE_CHANGE table                               */
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_SP_V_RPM_STG_PRC_CHANGE AS
    SELECT XXADEO_PROCESS_ID,
           STAGE_PRICE_CHANGE_ID,
           REASON_CODE,
           ITEM,
           DIFF_ID,
           ZONE_ID,
           LOCATION,
           ZONE_NODE_TYPE,
           LINK_CODE,
           EFFECTIVE_DATE,
           CHANGE_TYPE,
           CHANGE_AMOUNT,
           CHANGE_CURRENCY,
           CHANGE_PERCENT,
           CHANGE_SELLING_UOM,
           NULL_MULTI_IND,
           MULTI_UNITS,
           MULTI_UNIT_RETAIL,
           MULTI_SELLING_UOM,
           PRICE_GUIDE_ID,
           IGNORE_CONSTRAINTS,
           AUTO_APPROVE_IND,
           STATUS,
           ERROR_MESSAGE,
           PROCESS_ID,
           PRICE_CHANGE_ID,
           PRICE_CHANGE_DISPLAY_ID,
           SKULIST,
           THREAD_NUM,
           EXCLUSION_CREATED,
           VENDOR_FUNDED_IND,
           FUNDING_TYPE,
           FUNDING_AMOUNT,
           FUNDING_AMOUNT_CURRENCY,
           FUNDING_PERCENT,
           DEAL_ID,
           DEAL_DETAIL_ID,
           ZONE_GROUP_ID,
           STAGE_CUST_ATTR_ID,
           CUST_ATTR_ID,
           PROCESSED_DATE,
           CREATE_ID,CREATE_DATETIME
    FROM XXADEO_RPM_STAGE_PRICE_CHANGE;

COMMENT ON TABLE XXADEO_SP_V_RPM_STG_PRC_CHANGE  IS 'This table is a staging table to generate (and approve) price changes using the price event injector batch program (injectorpriceeventbatch.sh).';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.XXADEO_PROCESS_ID IS 'Identifier of the XXADEO Process';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.STAGE_PRICE_CHANGE_ID IS 'Unique indentifier to identify the record in this table.  This should be generated using the XXADEO_RPM_STAGE_PRICE_CHANGE_seq sequence.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.REASON_CODE IS 'reason for the generated price change. possible reason codes are maintained in the rpm code maintenance dialogue. if this column is not populated, the price change will be generated with the default system generated reason code of externalpc.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.ITEM IS 'item the generated price change is being applied to. can be at the transaction level or one level above the transaction level.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.DIFF_ID IS 'optional field that can be populated when the item field holds an item one level above the transaction. when it is populated, only children of the item in the item field with this diff_id value are effected by the generated price change.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.ZONE_ID IS 'price changes are optionally setup at either the zone level or at the location level. when the generated price change is supposed to be at the zone level, this field will be populated.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.LOCATION IS 'price changes are optionally setup at either the zone level or at the location level. when the generated price change is supposed to be at the location level, this field will be populated.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.ZONE_NODE_TYPE IS 'Indicates whether the generated promo detail is to be created at a Store (0), Zone (1), Warehouse (2), or Zone Group (3)';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.LINK_CODE IS 'the link code of the generated price change if the price change to be generated is of the type of link code price change.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.EFFECTIVE_DATE IS 'the date the generated price change will go into effect.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.CHANGE_TYPE IS 'describes the change type of generated price change. types include: change by amount, change by percent, change to amount, reset point of sale price, and no change. valid values : 0 = change by percent 1 = change by amount 2 = change to amount 3 = reset point of sale price -1 = no change';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.CHANGE_AMOUNT IS 'when the change type is change by amount or change to amount this field will hold the amount.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.CHANGE_CURRENCY IS 'the currency code of the link code price change.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.CHANGE_PERCENT IS 'when the change type is change by percent this field will hold percent.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.CHANGE_SELLING_UOM IS 'when the change type is change to amount this field will hold percent will hold the selling uom associated with the amount.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.NULL_MULTI_IND IS 'field to indicate whether or not multi unit retail should be changed as a result of the generated price change.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.MULTI_UNITS IS 'contains the new multi units determined by the generated price change.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.MULTI_UNIT_RETAIL IS 'this field contains the new multi unit retail price in the selling unit of measure determined by the generated price change.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.MULTI_SELLING_UOM IS 'this field holds the selling unit of measure for an items multi-unit retail.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.PRICE_GUIDE_ID IS 'the price guide associated with the generated price change.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.IGNORE_CONSTRAINTS IS 'indicates whether or not pricing constraints should be considered on the generated price change.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.AUTO_APPROVE_IND IS 'this column indicate whether the price event injector should try to approve the generated price change. valid values :  0 = do not automatically approve the price event 1 = automatically approve the price event.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.STATUS IS 'the status of the staging table records during the price event injector process.  valid values :       n  = new       e  = error       w = worksheet       a = approved        f  = failed with unexpected error during conflict check';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.ERROR_MESSAGE IS 'the error message that this record has during the price event injector process.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.PROCESS_ID IS 'the process id that is used internally by the price event injector batch program.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.PRICE_CHANGE_ID IS 'the id of the generated price change.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.PRICE_CHANGE_DISPLAY_ID IS 'the display id of the generated price change.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.SKULIST IS 'the skulist for the staged price change record.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.THREAD_NUM IS 'Thread number of the staged price change';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.EXCLUSION_CREATED IS 'Indicates whether a system generated exclusion has been generated during approval of the staged price change.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.VENDOR_FUNDED_IND IS 'Indicates whether or not the staged price change is vendor funded.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.FUNDING_TYPE IS 'Describes the type of funding the vendor is providing';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.FUNDING_AMOUNT IS 'The amount of the price change that is funded. This field will only be populated when the funding_type is 1 (amount).';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.FUNDING_AMOUNT_CURRENCY IS 'The currency of the funding amount. This field will only be populated when the funding_type is 1 (amount).';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.FUNDING_PERCENT IS 'The percent of the price change this is funded. This field will only be populated when the funding_type is 0 (percent).';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.DEAL_ID IS 'ID of the deal associated with the vendor funded price change. This field will only contain a value when the vendor_funded_ind is 1. ';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.DEAL_DETAIL_ID IS 'ID of the deal detail associated with the vendor funded price change.This field will only contain a value when the vendor_funded_ind is 1.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.ZONE_GROUP_ID IS 'Optional field that can be populated to setup price changes at all zones under the zone group.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.STAGE_CUST_ATTR_ID IS 'This column holds the Staging Custom Attribute ID. ';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.CUST_ATTR_ID IS 'The ID of the generated Custom Attributes data.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.PROCESSED_DATE IS 'As a result of an update to the entity, the DateTime of the update will be captured in this field for maintenance purposes.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.CREATE_ID IS 'User that created the Price Change event.';
COMMENT ON COLUMN XXADEO_SP_V_RPM_STG_PRC_CHANGE.CREATE_DATETIME IS 'Creation Date of the Price Change event.';