/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_SP_V_ASSORTMENT_VAL_TL" view of               */
/*               XXADEO_ASSORTMENT_VALUE_TL table                             */
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_SP_V_ASSORTMENT_VAL_TL AS
SELECT LANG,
       ROWNUM SEQ_ID,
       ASSORTMENT_VALUE_ID,
       ASSORTMENT_VALUE_LABEL,
       CREATE_ID,
       CREATE_DATETIME,
       LAST_UPDATE_ID,
       LAST_UPDATE_DATETIME
FROM XXADEO_ASSORTMENT_VALUE_TL;
-- Add comments to the columns
comment on column XXADEO_SP_V_ASSORTMENT_VAL_TL.lang is 'This field contains the language in which the translated text is maintained. ';
comment on column XXADEO_SP_V_ASSORTMENT_VAL_TL.seq_id is 'This field contains rownum for auxiliary ADF multi-select field. ';
comment on column XXADEO_SP_V_ASSORTMENT_VAL_TL.assortment_value_id is 'Assortmentb Value Unique Identifier. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT_VAL_TL.assortment_value_label is 'Assortment value lablel translated into the different languages. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT_VAL_TL.create_id is 'User that created the record.';
comment on column XXADEO_SP_V_ASSORTMENT_VAL_TL.create_datetime is 'Date in which the record was created.';
comment on column XXADEO_SP_V_ASSORTMENT_VAL_TL.last_update_id is 'User that last updated the record.';
comment on column XXADEO_SP_V_ASSORTMENT_VAL_TL.last_update_datetime is 'Date in which the record was last updated.';
