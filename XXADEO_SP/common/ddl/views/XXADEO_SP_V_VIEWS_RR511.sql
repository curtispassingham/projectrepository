--CREATED BY : BZIOUAT SARRA
--SCRIPT TO CREATE STORE PORTAL VIEWS FOR THE STATIC REPORT RR511


--GRANT RMS TABLES/VIEWS SELECTION FOR THE USER XXADEO_SP 
GRANT SELECT ON RMS.V_CURRENCIES_TL TO XXADEO_SP
GRANT SELECT ON RMS.COST_SUSP_SUP_HEAD TO XXADEO_SP
GRANT SELECT ON RMS.COST_SUSP_SUP_DETAIL TO XXADEO_SP
GRANT SELECT ON RMS.COST_SUSP_SUP_DETAIL_LOC TO XXADEO_SP
GRANT SELECT ON RMS.ITEM_MASTER_CFA_EXT TO XXADEO_SP

----------------------------------

--SP VIEW OF THE VIEW V_CURRENCIES_TL
begin
execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_CURRENCIES_TL cascade constraints';
exception
when others then
   null;
end;
/
  CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_CURRENCIES_TL (CURRENCY_CODE, CURRENCY_DESC, LANG) AS
  select V.CURRENCY_CODE,
       V.CURRENCY_DESC,
       V.LANG
from RMS.V_CURRENCIES_TL V
;
/
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_CURRENCIES_TL"."CURRENCY_CODE" IS 'Contains a number which uniquely identifies the type of currency.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_CURRENCIES_TL"."CURRENCY_DESC" IS 'Contains a description of the currency.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_CURRENCIES_TL"."LANG" IS 'This column contains the language for this record.';
   COMMENT ON TABLE "XXADEO_SP"."XXADEO_SP_V_CURRENCIES_TL"  IS 'This is the translation view for base table CURRENCIES. This view fetches data in user language either from translation table CURRENCIES_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.';

---------------------------------

--SP VIEW OF THE TABLE COST_SUSP_SUP_HEAD
begin
execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_COST_SUSP_SUP_HEAD cascade constraints';
exception
when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_COST_SUSP_SUP_HEAD (COST_CHANGE, COST_CHANGE_DESC, REASON, ACTIVE_DATE, STATUS, COST_CHANGE_ORIGIN, CREATE_DATE, CREATE_ID, APPROVAL_DATE, APPROVAL_ID) AS
  select    V.COST_CHANGE, 
            V.COST_CHANGE_DESC, 
            V.REASON, 
            V.ACTIVE_DATE, 
            V.STATUS, 
            V.COST_CHANGE_ORIGIN, 
            V.CREATE_DATE, 
            V.CREATE_ID, 
            V.APPROVAL_DATE, 
            V.APPROVAL_ID
from RMS.COST_SUSP_SUP_HEAD V
;
/
  COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_HEAD"."COST_CHANGE" IS 'Contains the number which uniquely identifies the cost change.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_HEAD"."COST_CHANGE_DESC" IS 'Contains a description which, along with the cost change number, identifies the cost change.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_HEAD"."REASON" IS 'Contains a number which identifies the reason for the cost change.  The number can be decoded on the cost_chg_reason table.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_HEAD"."ACTIVE_DATE" IS 'Contains the date in which the cost change will become active.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_HEAD"."STATUS" IS 'Contains the status of the cost change.  Valid values are: W = Worksheet, D = Deleted (to be deleted), C = Canceled, A = Approved, E = Extracted, R = Rejected, S = Submitted';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_HEAD"."COST_CHANGE_ORIGIN" IS 'Contains a code which identifies the origin of the cost change.  This code is used to determine whether the event was created by supplier or by SKU.  Cost events with a supplier origin will have multiple detail records all having the same supplier.  Cost events with a SKU origin will have multiple detail records which may have multiple SKUs and multiple suppliers.  Valid Values are: SUP - By Supplier, SKU - By SKU';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_HEAD"."CREATE_DATE" IS 'Contains the date on which the cost change was created.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_HEAD"."CREATE_ID" IS 'Contains the ID of the user who created the cost change.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_HEAD"."APPROVAL_DATE" IS 'Contains the date on which the cost change was last approved.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_HEAD"."APPROVAL_ID" IS 'Contains the user ID of the person who last approved the cost change.';
   COMMENT ON TABLE "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_HEAD"  IS 'This table contains one row for each cost/retail event created within the company.  If the cost event has retail changes associated with it, the retail changes are stored on the price_susp tables.';

----------------------------------

--SP VIEW OF THE TABLE COST_SUSP_SUP_DETAIL
begin
execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_COST_SUSP_SUP_DET cascade constraints';
exception
when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_COST_SUSP_SUP_DET (COST_CHANGE, SUPPLIER, ORIGIN_COUNTRY_ID, ITEM, BRACKET_VALUE1, BRACKET_UOM1, BRACKET_VALUE2, UNIT_COST, COST_CHANGE_TYPE, 
COST_CHANGE_VALUE, RECALC_ORD_IND, DEFAULT_BRACKET_IND, DEPT, SUP_DEPT_SEQ_NO, DELIVERY_COUNTRY_ID) AS
  select    V.COST_CHANGE, 
            V.SUPPLIER, 
            V.ORIGIN_COUNTRY_ID, 
            V.ITEM, 
            V.BRACKET_VALUE1, 
            V.BRACKET_UOM1, 
            V.BRACKET_VALUE2, 
            V.UNIT_COST, 
            V.COST_CHANGE_TYPE, 
            V.COST_CHANGE_VALUE,
            V.RECALC_ORD_IND,
            V.DEFAULT_BRACKET_IND,
            V.DEPT,
            V.SUP_DEPT_SEQ_NO,
            V.DELIVERY_COUNTRY_ID
from RMS.COST_SUSP_SUP_DETAIL V
;
/
  COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."COST_CHANGE" IS 'Contains the number which uniquely identifies the cost change.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."SUPPLIER" IS 'Contains the number which identifies the supplier associated with the cost change.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."ORIGIN_COUNTRY_ID" IS 'The country where the item was manufactured or significantly altered.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."ITEM" IS 'Unique alphanumeric value that identifies the item';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."BRACKET_VALUE1" IS 'The values are the various levels in the vendor price list whereby costing of the items will be affected as the levels are achieved on the Purchase Orders.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."BRACKET_UOM1" IS 'The unit of mesaure defining each value in a bracket. All values within a bracket have the same UOM.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."BRACKET_VALUE2" IS 'This column will contain the value of the secondary bracket.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."UNIT_COST" IS 'Contains the new unit cost which will be implemented as a result of the cost change.   This field is stored in the suppliers currency.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."COST_CHANGE_TYPE" IS 'Describes the type of cost change';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."COST_CHANGE_VALUE" IS 'Holds the value of cost_change';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."RECALC_ORD_IND" IS 'This field indicates whether or not approved order costs should be re-calculated when a supplier cost has changed.  Valid values are: Y - Yes, recalculate order, N - No, do no recalculate order';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."DEFAULT_BRACKET_IND" IS 'This field contains the value which will control which cost is the base cost for processing throughout the system.  Each bracket group (either by supplier, supplier/department, supplier/department/location, supplier/location) must have one bracket defined as the default bracket.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."DEPT" IS 'Contains the number which uniquely identifies the department.  This field is only populated from the supplier bracket costing dialogue when bracket inserts or default bracket changes involved the department level.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."SUP_DEPT_SEQ_NO" IS 'This field will contain the supplier department sequence number.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"."DELIVERY_COUNTRY_ID" IS 'Country to which the item  will be delivered to.';
   COMMENT ON TABLE "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_SUP_DET"  IS 'This table holds the detail SKU/Supplier cost event records.';
   
-----------------------------------

--SP VIEW OF THE TABLE COST_SUSP_SUP_DETAIL_LOC
begin
execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_COST_SUSP_DET_LOC cascade constraints';
exception
when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_COST_SUSP_DET_LOC (COST_CHANGE, SUPPLIER, ORIGIN_COUNTRY_ID, ITEM, LOC_TYPE, LOC, BRACKET_VALUE1, BRACKET_UOM1, BRACKET_VALUE2, UNIT_COST, COST_CHANGE_TYPE, 
COST_CHANGE_VALUE, RECALC_ORD_IND, DEFAULT_BRACKET_IND, DEPT, SUP_DEPT_SEQ_NO, DELIVERY_COUNTRY_ID) AS
  select    V.COST_CHANGE, 
            V.SUPPLIER, 
            V.ORIGIN_COUNTRY_ID, 
            V.ITEM, 
            V.LOC_TYPE,
            V.LOC,
            V.BRACKET_VALUE1, 
            V.BRACKET_UOM1, 
            V.BRACKET_VALUE2, 
            V.UNIT_COST, 
            V.COST_CHANGE_TYPE, 
            V.COST_CHANGE_VALUE,
            V.RECALC_ORD_IND,
            V.DEFAULT_BRACKET_IND,
            V.DEPT,
            V.SUP_DEPT_SEQ_NO,
            V.DELIVERY_COUNTRY_ID
from RMS.COST_SUSP_SUP_DETAIL_LOC V
;
/
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."COST_CHANGE" IS 'Contains the number which uniquely identifies the cost change.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."SUPPLIER" IS 'Contains the number which identifies the supplier associated with the cost change.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."ORIGIN_COUNTRY_ID" IS 'The country where the item was manufactured or significantly altered.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."ITEM" IS 'Unique alphanumeric value that identifies the item';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."LOC_TYPE" IS 'Contains the type of the location.  Valid values are Store and Warehouse.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."LOC" IS 'Number of the location the cost change will occur at.  This can represent a store or a warehouse Cost changes will be managed and stored at the physical warehouse level since the unit cost must remain consistant across all virtual warehouses within the same physical warehouse.  The sccext batch program will handle blowing the cost change down to all virtual warehouses that reside on the physical warehouse stored on this table.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."BRACKET_VALUE1" IS 'The values are the various levels in the vendor price list whereby costing of the items will be affected as the levels are achieved on the Purchase Orders.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."BRACKET_UOM1" IS 'The unit of mesaure defining each value in a bracket. All values within a bracket have the same UOM.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."BRACKET_VALUE2" IS 'This column will contain the value of the secondary bracket.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."UNIT_COST" IS 'Contains the new unit cost which will be implemented as a result of the cost change.   This field is stored in the suppliers currency.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."COST_CHANGE_TYPE" IS 'Describes the type of cost change';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."COST_CHANGE_VALUE" IS 'Holds the value of cost_change';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."RECALC_ORD_IND" IS 'This field indicates whether or not approved order costs should be re-calculated when a supplier cost has changed.  Valid values are: Y - Yes, recalculate order, N - No, do no recalculate order';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."DEFAULT_BRACKET_IND" IS 'This field contains the value which will control which cost is the base cost for processing throughout the system.  Each bracket group (either by supplier, supplier/department, supplier/department/location, supplier/location) must have one bracket defined as the default bracket.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."DEPT" IS 'Contains the number which uniquely identifies the department.  This field is only populated from the supplier bracket costing dialogue when bracket inserts or default bracket changes involved the department level.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."SUP_DEPT_SEQ_NO" IS 'This field will contain the supplier department sequence number.';
   COMMENT ON COLUMN "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"."DELIVERY_COUNTRY_ID" IS 'Country to which the item  will be delivered to.';
   COMMENT ON TABLE "XXADEO_SP"."XXADEO_SP_V_COST_SUSP_DET_LOC"  IS 'This table holds the detail SKU/Supplier cost event records.';
   
--------------------------------

begin
execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS2_AG1 cascade constraints';
exception
when others then
   null;
end;
/
  CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS2_AG1 (ITEM, LMFR_HYPER_100_ITEM, LMFR_PRICE_POLICY_ITEM, LMFR_PURCHASE_BLK_ITEM, LMFR_PV_PUBLICATION_ITEM, LMFR_RANKING_ITEM, LMFR_RTL_BLK_ITEM) AS 
  SELECT ITEM,LMFR_HYPER_100_ITEM,LMFR_PRICE_POLICY_ITEM,LMFR_PURCHASE_BLK_ITEM,LMFR_PV_PUBLICATION_ITEM,LMFR_RANKING_ITEM,LMFR_RTL_BLK_ITEM FROM
(SELECT ITEM,VARCHAR2_2 LMFR_HYPER_100_ITEM,VARCHAR2_3 LMFR_PRICE_POLICY_ITEM,VARCHAR2_4 LMFR_PURCHASE_BLK_ITEM,VARCHAR2_5
LMFR_PV_PUBLICATION_ITEM,NUMBER_11 LMFR_RANKING_ITEM,VARCHAR2_1 LMFR_RTL_BLK_ITEM from ITEM_MASTER_CFA_EXT where group_id =11);

---------------------------------

begin
execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS3_AG1 cascade constraints';
exception
when others then
   null;
end;
/
  CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS3_AG1 (ITEM, LMES_HYPER_100_ITEM, LMES_PRICE_POLICY_ITEM, LMES_PURCHASE_BLK_ITEM, LMES_PV_PUBLICATION_ITEM, LMES_RANKING_ITEM, LMES_RTL_BLK_ITEM) AS 
  SELECT ITEM,LMES_HYPER_100_ITEM,LMES_PRICE_POLICY_ITEM,LMES_PURCHASE_BLK_ITEM,LMES_PV_PUBLICATION_ITEM,LMES_RANKING_ITEM,LMES_RTL_BLK_ITEM FROM
(SELECT ITEM,VARCHAR2_2 LMES_HYPER_100_ITEM,VARCHAR2_3 LMES_PRICE_POLICY_ITEM,VARCHAR2_4 LMES_PURCHASE_BLK_ITEM,VARCHAR2_5
LMES_PV_PUBLICATION_ITEM,NUMBER_11 LMES_RANKING_ITEM,VARCHAR2_1 LMES_RTL_BLK_ITEM from ITEM_MASTER_CFA_EXT where group_id =15);

---------------------------------

begin
execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS4_AG1 cascade constraints';
exception
when others then
   null;
end;
/
  CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS4_AG1 (ITEM, LMPL_HYPER_100_ITEM, LMPL_PRICE_POLICY_ITEM, LMPL_PURCHASE_BLK_ITEM, LMPL_PV_PUBLICATION_ITEM, LMPL_RANKING_ITEM, LMPL_RTL_BLK_ITEM) AS 
  SELECT ITEM,LMPL_HYPER_100_ITEM,LMPL_PRICE_POLICY_ITEM,LMPL_PURCHASE_BLK_ITEM,LMPL_PV_PUBLICATION_ITEM,LMPL_RANKING_ITEM,LMPL_RTL_BLK_ITEM FROM
(SELECT ITEM,VARCHAR2_2 LMPL_HYPER_100_ITEM,VARCHAR2_3 LMPL_PRICE_POLICY_ITEM,VARCHAR2_4 LMPL_PURCHASE_BLK_ITEM,VARCHAR2_5
LMPL_PV_PUBLICATION_ITEM,NUMBER_11 LMPL_RANKING_ITEM,VARCHAR2_1 LMPL_RTL_BLK_ITEM from ITEM_MASTER_CFA_EXT where group_id =19);

---------------------------------

begin
execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS5_AG1 cascade constraints';
exception
when others then
   null;
end;
/
  CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS5_AG1 (ITEM, LMIT_HYPER_100_ITEM, LMIT_PRICE_POLICY_ITEM, LMIT_PURCHASE_BLK_ITEM, LMIT_PV_PUBLICATION_ITEM, LMIT_RANKING_ITEM, LMIT_RTL_BLK_ITEM) AS 
  SELECT ITEM,LMIT_HYPER_100_ITEM,LMIT_PRICE_POLICY_ITEM,LMIT_PURCHASE_BLK_ITEM,LMIT_PV_PUBLICATION_ITEM,LMIT_RANKING_ITEM,LMIT_RTL_BLK_ITEM FROM
(SELECT ITEM,VARCHAR2_2 LMIT_HYPER_100_ITEM,VARCHAR2_3 LMIT_PRICE_POLICY_ITEM,VARCHAR2_4 LMIT_PURCHASE_BLK_ITEM,VARCHAR2_5
LMIT_PV_PUBLICATION_ITEM,NUMBER_11 LMIT_RANKING_ITEM,VARCHAR2_1 LMIT_RTL_BLK_ITEM from ITEM_MASTER_CFA_EXT where group_id =23);

----------------------------------

begin
execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS6_AG1 cascade constraints';
exception
when others then
   null;
end;
/
  CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS6_AG1 (ITEM, LMPT_HYPER_100_ITEM, LMPT_PRICE_POLICY_ITEM, LMPT_PURCHASE_BLK_ITEM, LMPT_PV_PUBLICATION_ITEM, LMPT_RANKING_ITEM, LMPT_RTL_BLK_ITEM) AS 
  SELECT ITEM,LMPT_HYPER_100_ITEM,LMPT_PRICE_POLICY_ITEM,LMPT_PURCHASE_BLK_ITEM,LMPT_PV_PUBLICATION_ITEM,LMPT_RANKING_ITEM,LMPT_RTL_BLK_ITEM FROM
(SELECT ITEM,VARCHAR2_2 LMPT_HYPER_100_ITEM,VARCHAR2_3 LMPT_PRICE_POLICY_ITEM,VARCHAR2_4 LMPT_PURCHASE_BLK_ITEM,VARCHAR2_5
LMPT_PV_PUBLICATION_ITEM,NUMBER_11 LMPT_RANKING_ITEM,VARCHAR2_1 LMPT_RTL_BLK_ITEM from ITEM_MASTER_CFA_EXT where group_id =27);

----------------------------------

begin
execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS7_AG1 cascade constraints';
exception
when others then
   null;
end;
/
  CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS7_AG1 (ITEM, LMGR_HYPER_100_ITEM, LMGR_PRICE_POLICY_ITEM, LMGR_PURCHASE_BLK_ITEM, LMGR_PV_PUBLICATION_ITEM, LMGR_RANKING_ITEM, LMGR_RTL_BLK_ITEM) AS 
  SELECT ITEM,LMGR_HYPER_100_ITEM,LMGR_PRICE_POLICY_ITEM,LMGR_PURCHASE_BLK_ITEM,LMGR_PV_PUBLICATION_ITEM,LMGR_RANKING_ITEM,LMGR_RTL_BLK_ITEM FROM
(SELECT ITEM,VARCHAR2_2 LMGR_HYPER_100_ITEM,VARCHAR2_3 LMGR_PRICE_POLICY_ITEM,VARCHAR2_4 LMGR_PURCHASE_BLK_ITEM,VARCHAR2_5
LMGR_PV_PUBLICATION_ITEM,NUMBER_11 LMGR_RANKING_ITEM,VARCHAR2_1 LMGR_RTL_BLK_ITEM from ITEM_MASTER_CFA_EXT where group_id =31);

---------------------------------

begin
execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS8_AG1 cascade constraints';
exception
when others then
   null;
end;
/
  CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS8_AG1 (ITEM, LMCY_HYPER_100_ITEM, LMCY_PRICE_POLICY_ITEM, LMCY_PURCHASE_BLK_ITEM, LMCY_PV_PUBLICATION_ITEM, LMCY_RANKING_ITEM, LMCY_RTL_BLK_ITEM) AS 
  SELECT ITEM,LMCY_HYPER_100_ITEM,LMCY_PRICE_POLICY_ITEM,LMCY_PURCHASE_BLK_ITEM,LMCY_PV_PUBLICATION_ITEM,LMCY_RANKING_ITEM,LMCY_RTL_BLK_ITEM FROM
(SELECT ITEM,VARCHAR2_2 LMCY_HYPER_100_ITEM,VARCHAR2_3 LMCY_PRICE_POLICY_ITEM,VARCHAR2_4 LMCY_PURCHASE_BLK_ITEM,VARCHAR2_5
LMCY_PV_PUBLICATION_ITEM,NUMBER_11 LMCY_RANKING_ITEM,VARCHAR2_1 LMCY_RTL_BLK_ITEM from ITEM_MASTER_CFA_EXT where group_id =35);

