/******************************************************************************/
/* CREATE DATE - March 2018                                                   */
/* CREATE USER - Fabio Andersen                                               */
/* PROJECT     - ADEO SP                                                      */
/* DESCRIPTION - VIEW "XXADEO_SP_V_STORE" for ADEO SP                         */
/******************************************************************************/
CREATE OR REPLACE VIEW XXADEO_SP_V_STORE AS
SELECT vs.chain,
       vs.area,
       vs.region,
       vs.district,
       vs.store,
       vs.store_name,
       vs.store_name_secondary,
       vs.store_close_date,
       vs.stop_order_days,
       vs.stockholding_ind,
       vs.currency_code,
       vs.store_class,
       vs.transfer_zone,
       vs.default_wh,
       vs.store_type,
       vs.wf_customer_id,
       vs.org_unit_id,
       vs.tsf_entity_id,
       vs.vat_region,
       vs.channel_id,
       vs.customer_order_loc_ind,
       vs.email,
       vs.duns_number,
       vs.duns_loc,
       s.store_format,
       vs.create_id,
       vs.create_datetime
  FROM v_store vs, store s
 WHERE vs.store = s.store;

 -- Add comments to the table
 comment on table XXADEO_SP_V_STORE
   is 'This view contains one row for each store within the company.';
 -- Add comments to the columns
 comment on column XXADEO_SP_V_STORE.CHAIN is 'Contains the chain the store belongs to.';
 comment on column XXADEO_SP_V_STORE.AREA is 'Contains the area the store belongs to.';
 comment on column XXADEO_SP_V_STORE.REGION is 'Contains the region the store belongs to.';
 comment on column XXADEO_SP_V_STORE.DISTRICT is 'Contains the district the store belongs to.';
 comment on column XXADEO_SP_V_STORE.STORE is 'Contains the number which uniquely identifies the store.';
 comment on column XXADEO_SP_V_STORE.STORE_NAME is 'Contains the translated name of the store which, along with the store number, identifies the store.';
 comment on column XXADEO_SP_V_STORE.STORE_NAME_SECONDARY is 'Translated Secondary name of the store.';
 comment on column XXADEO_SP_V_STORE.STORE_CLOSE_DATE is 'Contains the date on which the store closed.';
 comment on column XXADEO_SP_V_STORE.STOP_ORDER_DAYS is 'Contains the number of days before a store closing that the store will stop accepting orders.  This column will be used when the store_close_date is defined.';
 comment on column XXADEO_SP_V_STORE.STOCKHOLDING_IND is 'This column indicates whether the store can hold stock.  In a non-multichannel environment this will always be Y.';
 comment on column XXADEO_SP_V_STORE.CURRENCY_CODE is 'This field contains the currency code under which the store operates.';
 comment on column XXADEO_SP_V_STORE.STORE_CLASS is 'Contains the code letter indicating the class of which the store is a member.  Valid values are A through E.';
 comment on column XXADEO_SP_V_STORE.TRANSFER_ZONE is 'Contains the transfer zone in which the store is located.  Valid values are located on the tsfzone table.';
 comment on column XXADEO_SP_V_STORE.DEFAULT_WH is 'Contains the number of the warehouse that may be used as the default for creating cross-dock masks.  This determines which stores are associated with or sourced from a warehouse. Will hold only virtual warehouses in a multi-channel environment.';
 comment on column XXADEO_SP_V_STORE.STORE_TYPE is 'This will indicate whether a particular store is a franchise or company store.';
 comment on column XXADEO_SP_V_STORE.WF_CUSTOMER_ID is 'Numeric Id of the customer.';
 comment on column XXADEO_SP_V_STORE.ORG_UNIT_ID is 'Column will contain the organizational unit ID value.';
 comment on column XXADEO_SP_V_STORE.VAT_REGION is 'Contains the number of the Value Added Tax region in which this store is contained.';
 comment on column XXADEO_SP_V_STORE.CHANNEL_ID is 'In a multichannel environment this will contain the channel with which the store is associated.  Valid values can be found on the channels table.';
 comment on column XXADEO_SP_V_STORE.CUSTOMER_ORDER_LOC_IND is 'This Column determines whether the location is customer order location or not.If the indicator is Y then the location can be used by OMS for sourcing/ fulfillment or both else it  cannot be used.It is enabled only for the company stores .';
 comment on column XXADEO_SP_V_STORE.EMAIL is 'Holds the email address for the location';
 comment on column XXADEO_SP_V_STORE.DUNS_NUMBER is 'This field holds the Dun and Bradstreet number to identify the store.';
 comment on column XXADEO_SP_V_STORE.DUNS_LOC is 'This field holds the Dun and Bradstreet number to identify the location';
 comment on column XXADEO_SP_V_STORE.STORE_FORMAT is 'Contains the number indicating the format of the store.  Valid values are found on the store format table.';
 comment on column XXADEO_SP_V_STORE.CREATE_ID is 'This column holds the User id of the user who created the record.';
 comment on column XXADEO_SP_V_STORE.CREATE_DATETIME is 'This column holds the record creation date.';
