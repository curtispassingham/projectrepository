CREATE OR REPLACE VIEW XXADEO_SP_V_FILTER_GROUP_ORG AS
SELECT fgo.sec_group_id,
       fgo.filter_org_level,
       fgo.filter_org_id
FROM filter_group_org fgo;

-- Add comments to the table
comment on table XXADEO_SP_V_FILTER_GROUP_ORG
  is 'This table contains the Organization Hierarchy LOV filtering access information for a User Security Group';
-- Add comments to the columns
comment on column XXADEO_SP_V_FILTER_GROUP_ORG.sec_group_id
  is 'ID of the User Security group';
comment on column XXADEO_SP_V_FILTER_GROUP_ORG.filter_org_level
  is 'The Organization hierarchy level assigned to the User Security Group.  Valid values are contained in the CODE_DETIAL table with a CODE_TYPE of FLOW.';
comment on column XXADEO_SP_V_FILTER_GROUP_ORG.filter_org_id
  is 'ID of the Organization hierarchy level assigned to the User Security Group.';
