create or replace view XXADEO_SP_V_PRICE_RETAIL as
select /*+ORDERED */
       ril.item                                                 as item,
       ril.loc                                                  as loc,
       rzl.zone_id                                              as zone_id,
       --
       ph_curr.unit_retail                                      as curr_s_price,
       ph_curr.action_date                                      as curr_s_date,
       --
       izp.selling_retail                                       as curr_z_price,
       coalesce(zone_curr.action_Date,  ph_init.action_date)    as curr_z_date,
       izp.selling_retail_currency                              as currency,
       --
       ph_fut.unit_retail                                       as future_s_price,
       ph_fut.action_date                                       as future_s_date,
       --
       zone_next.selling_retail                                 as future_z_price,
       zone_next.action_date                                    as future_z_date
       --
  from xxadeo_sp_v_rpm_item_loc                        ril,
       xxadeo_sp_v_rpm_zone_location                   rzl,
       xxadeo_sp_v_rpm_izp                 izp,
       -- ph_init
       (select item,
               loc,
               action_date,
               unit_retail
          from (select ph.item,
                       ph.loc,
                       ph.action_date,
                       ph.unit_retail,
                       row_number() over (PARTITION BY ph.item, ph.loc ORDER BY ph.action_date desc, ph.tran_type desc) ranking
                  from xxadeo_sp_v_price_hist ph
                 where ph.tran_type = 0)
         where ranking=1) ph_init,
       -- ph_curr
       (select item,
               loc,
               action_date,
               unit_retail
          from (select ph.item,
                       ph.loc,
                       ph.action_date,
                       ph.unit_retail,
                       row_number() over (PARTITION BY ph.item, ph.loc ORDER BY ph.action_date desc, ph.tran_type desc) ranking
                  from xxadeo_sp_v_price_hist ph
                 where ph.tran_type in (0,4))
         where ranking=1) ph_curr,
         -- ph_fut
         --
         (select item,
                 zone,
                 loc,
                 action_date,
                 selling_retail as unit_retail,
                 price_change_id
           from (select rfr.item,
                        rfr.zone_id as zone,
                        rfr.location as loc,
                        rfr.action_date,
                        rfr.selling_retail,
                        rfr.price_change_id,
                        row_number() over(PARTITION BY rfr.item, rfr.location, rfr.zone_id ORDER BY rfr.action_date asc) ranking
                   from xxadeo_sp_v_rpm_future_retail rfr,
                        xxadeo_sp_v_rpm_price_change rpc
                  where rfr.zone_node_type                = 0
                    and rfr.action_date                   >= get_vdate
                    and rfr.price_change_id               = rpc.price_change_id
                    and rpc.state                         = 'pricechange.state.approved')
         where ranking = 1) ph_fut,
        --
        --zone_curr
       (select item,
               zone,
               loc,
               action_date,
               selling_retail,
               price_change_id
         from( select rzfr.item,
               rzfr.zone,
               rzl.location as loc,
               rzfr.action_date,
               rzfr.selling_retail,
               rzfr.price_change_id,
               row_number() over (PARTITION BY rzfr.item, rzfr.zone ORDER BY rzfr.action_date asc) ranking
               from xxadeo_sp_v_rpm_zfr rzfr,
                    xxadeo_sp_v_rpm_zone_location rzl
              where rzfr.action_date       <= get_vdate
                and rzl.zone_id            = rzfr.zone )
        where ranking=2) zone_curr,
       --
       -- zone_next
       (select item,
               zone,
               loc,
               action_date,
               selling_retail,
               price_change_id
         from( select rzfr.item,
               rzfr.zone,
               rzl.location as loc,
               rzfr.action_date,
               rzfr.selling_retail,
               rzfr.price_change_id,
               row_number() over (PARTITION BY rzfr.item, rzfr.zone ORDER BY rzfr.action_date asc) ranking
               from xxadeo_sp_v_rpm_zfr rzfr,
                    xxadeo_sp_v_rpm_zone_location rzl,
                    xxadeo_sp_v_rpm_price_change rpc
              where rzfr.action_date           >= get_vdate
                and rzl.zone_id                = rzfr.zone
                and rzfr.price_change_id (+)   = rpc.price_change_id
                and rpc.state                  = 'pricechange.state.approved'
       )where ranking=1) zone_next
 where ph_init.item (+)           = ril.item
   and ph_init.loc (+)            = ril.loc
   and ph_curr.item(+)            = ril.item
   and ph_curr.loc(+)             = ril.loc
   and ph_fut.item(+)             = ril.item
   and ph_fut.loc(+)              = ril.loc
   and zone_next.loc (+)          = ril.loc
   and zone_next.item (+)         = ril.item
   and zone_curr.loc (+)          = ril.loc
   and zone_curr.item (+)         = ril.item
   and rzl.location               = ril.loc
   and izp.item                   = ril.item
   and izp.zone_id                = rzl.zone_id;
