-- Create table
create or replace view XXADEO_SP_V_RPM_ZFR as
SELECT zone_future_retail_id,
       item,
       zone,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       price_change_id,
       price_change_display_id,
       pc_change_type,
       pc_change_amount,
       pc_change_currency,
       pc_change_percent,
       pc_change_selling_uom,
       pc_null_multi_ind,
       pc_multi_units,
       pc_multi_unit_retail,
       pc_multi_unit_retail_currency,
       pc_multi_selling_uom,
       pc_price_guide_id,
       lock_version
  FROM RPM_ZONE_FUTURE_RETAIL;
-- Add comments to the table
comment on table XXADEO_SP_V_RPM_ZFR
  is 'this table will hold zone level future retail changes. it only tracks retails for primary zone groups';
-- Add comments to the columns
comment on column XXADEO_SP_V_RPM_ZFR.zone_future_retail_id
  is 'id that uniquely identifies the rpm_zone_future_retail_record.';
comment on column XXADEO_SP_V_RPM_ZFR.item
  is 'id that uniquely identifies the item on the rpm_zone_future_retail record';
comment on column XXADEO_SP_V_RPM_ZFR.zone
  is 'id that uniquely identifies the zone on the rpm_zone_future_retail record';
comment on column XXADEO_SP_V_RPM_ZFR.action_date
  is 'the action date of the future retail event.';
comment on column XXADEO_SP_V_RPM_ZFR.selling_retail
  is 'this field contains the regular unit retail price in the selling unit of measure for the item/zone combination on the action date.';
comment on column XXADEO_SP_V_RPM_ZFR.selling_retail_currency
  is 'the currency for which the selling retail is represented.';
comment on column XXADEO_SP_V_RPM_ZFR.selling_uom
  is 'this field contains the selling unit of measure for the item/zone?s single selling regular unit retail on the action date.';
comment on column XXADEO_SP_V_RPM_ZFR.multi_units
  is 'this field contains the number of units associated with the multi-unit regular retail for the item/zone combination on the action date.';
comment on column XXADEO_SP_V_RPM_ZFR.multi_unit_retail
  is 'this field holds the multi-unit regular retail in the multi-unit selling unit of measure for the item/zone combination on the action date.';
comment on column XXADEO_SP_V_RPM_ZFR.multi_unit_retail_currency
  is 'the currency for which the multi-unit retail is represented.';
comment on column XXADEO_SP_V_RPM_ZFR.multi_selling_uom
  is 'this field holds the selling unit of measure for the item/zone?s regular multi-unit retail on the action date.';
comment on column XXADEO_SP_V_RPM_ZFR.price_change_id
  is 'id that uniquely identifies the price change.  this field will be populated if there is either a price change occurring on the action date, or if there is a pricing event occurring on the action date that affects the regular retail for the price change on the action date.';
comment on column XXADEO_SP_V_RPM_ZFR.price_change_display_id
  is 'display id that uniquely identifies the price change.  this field will be populated if there is either a price change occurring on the action date, or if there is a pricing event occurring on the action date that affects the regular retail for the price change on the action date.';
comment on column XXADEO_SP_V_RPM_ZFR.pc_change_type
  is 'the type of change being applied to the item/zone on the price change.  valid values include: 0  change by percent; 1  change by amount; 2  fixed price; 4  exclude;';
comment on column XXADEO_SP_V_RPM_ZFR.pc_change_amount
  is 'the amount of change applied to the item/location on the price change.';
comment on column XXADEO_SP_V_RPM_ZFR.pc_change_currency
  is 'when the value in the pc_change_type field is either 1 ? change by amount, or 2 ? fixed price, this field holds the currency associated with the amount field.';
comment on column XXADEO_SP_V_RPM_ZFR.pc_change_percent
  is 'the percentage amount of change applied to the item/zone on the price change.';
comment on column XXADEO_SP_V_RPM_ZFR.pc_change_selling_uom
  is 'the selling uom associated with the price change.';
comment on column XXADEO_SP_V_RPM_ZFR.pc_null_multi_ind
  is 'indicates whether or not the price change is changing the multi-unit regular retail of the item/zone. valid values include: 1  price change is not changing the multi-unit retail, and 0  price change is changing the multi-unit retail.';
comment on column XXADEO_SP_V_RPM_ZFR.pc_multi_units
  is 'this field contains the number of units associated with the multi-unit retail for the item/zone combination on the price change.';
comment on column XXADEO_SP_V_RPM_ZFR.pc_multi_unit_retail
  is 'this field holds the multi-unit retail in the multi-unit selling unit of measure for the item/zone combination on the price change.';
comment on column XXADEO_SP_V_RPM_ZFR.pc_multi_unit_retail_currency
  is 'the currency for which the multi-unit retail is represented.';
comment on column XXADEO_SP_V_RPM_ZFR.pc_multi_selling_uom
  is 'the multi-unit selling uom associated with the price change.';
comment on column XXADEO_SP_V_RPM_ZFR.pc_price_guide_id
  is 'id that uniquely identifies the price guide attached to the price change.';
comment on column XXADEO_SP_V_RPM_ZFR.lock_version
  is 'field to help with optimistic locking.';
