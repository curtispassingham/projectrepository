create or replace view xxadeo_sp_v_sups_cfa_ext as
select supplier,
       group_id,
       varchar2_1,
       varchar2_2,
       varchar2_3,
       varchar2_4,
       varchar2_5,
       varchar2_6,
       varchar2_7,
       varchar2_8,
       varchar2_9,
       varchar2_10,
       number_11,
       number_12,
       number_13,
       number_14,
       number_15,
       number_16,
       number_17,
       number_18,
       number_19,
       number_20,
       date_21,
       date_22,
       date_23,
       date_24,
       date_25
  FROM sups_cfa_ext;
comment on table XXADEO_SP_V_SUPS_CFA_EXT is 'This is the custom attribute extension table for the entity SUPS';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.SUPPLIER is 'This column holds the Supplier this extended data is associated with.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.GROUP_ID is 'This column holds the attribute group id that this extended data is associated with.  The logical business meaning of the VARCHAR_ , NUMBER_ and DATE_ columns on this table are determined by the metadata defined for this attribute.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.VARCHAR2_1 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_1 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.VARCHAR2_2 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_2 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.VARCHAR2_3 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_3 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.VARCHAR2_4 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references  VARCHAR2_4 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.VARCHAR2_5 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references  VARCHAR2_5 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.VARCHAR2_6 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references  VARCHAR2_6 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.VARCHAR2_7 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references  VARCHAR2_7 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.VARCHAR2_8 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_8 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.VARCHAR2_9 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_9 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.VARCHAR2_10 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_10 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.NUMBER_11 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_11 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.NUMBER_12 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_12 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.NUMBER_13 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_13 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.NUMBER_14 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_14 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.NUMBER_15 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_15 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.NUMBER_16 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_16 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.NUMBER_17 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_17 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.NUMBER_18 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_18 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.NUMBER_19 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_19 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.NUMBER_20 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_20 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.DATE_21 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_21 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.DATE_22 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_22 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.DATE_23 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.DATE_24 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.';
comment on column XXADEO_SP_V_SUPS_CFA_EXT.DATE_25 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.';
