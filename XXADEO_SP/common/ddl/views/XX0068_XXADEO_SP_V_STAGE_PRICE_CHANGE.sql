-- Create view
create or replace view XXADEO_SP_V_STAGE_PRICE_CHANGE as
select xxadeo_process_id,
       stage_price_change_id,
       reason_code,
       item,
       diff_id,
       zone_id,
       location,
       zone_node_type,
       link_code,
       effective_date,
       change_type,
       change_amount,
       change_currency,
       change_percent,
       change_selling_uom,
       null_multi_ind,
       multi_units,
       multi_unit_retail,
       multi_selling_uom,
       price_guide_id,
       ignore_constraints,
       auto_approve_ind,
       status,
       error_message,
       process_id,
       price_change_id,
       price_change_display_id,
       skulist,
       thread_num,
       exclusion_created,
       vendor_funded_ind,
       funding_type,
       funding_amount,
       funding_amount_currency,
       funding_percent,
       deal_id,
       deal_detail_id,
       zone_group_id,
       stage_cust_attr_id,
       cust_attr_id,
       processed_date,
       create_id,
       create_datetime
  from XXADEO_RPM_STAGE_PRICE_CHANGE;

-- Add comments to the table
comment on table XXADEO_SP_V_STAGE_PRICE_CHANGE
  is 'This table is a staging table to generate (and approve) price changes using the price event injector batch program (injectorpriceeventbatch.sh).';
-- Add comments to the columns
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.xxadeo_process_id
  is 'Identifier of the XXADEO Process';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.stage_price_change_id
  is 'Unique indentifier to identify the record in this table.  This should be generated using the XXADEO_RPM_STAGE_PRICE_CHANGE_seq sequence.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.reason_code
  is 'reason for the generated price change. possible reason codes are maintained in the rpm code maintenance dialogue. if this column is not populated, the price change will be generated with the default system generated reason code of externalpc.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.item
  is 'item the generated price change is being applied to. can be at the transaction level or one level above the transaction level.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.diff_id
  is 'optional field that can be populated when the item field holds an item one level above the transaction. when it is populated, only children of the item in the item field with this diff_id value are effected by the generated price change.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.zone_id
  is 'price changes are optionally setup at either the zone level or at the location level. when the generated price change is supposed to be at the zone level, this field will be populated.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.location
  is 'price changes are optionally setup at either the zone level or at the location level. when the generated price change is supposed to be at the location level, this field will be populated.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.zone_node_type
  is 'Indicates whether the generated promo detail is to be created at a Store (0), Zone (1), Warehouse (2), or Zone Group (3)';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.link_code
  is 'the link code of the generated price change if the price change to be generated is of the type of link code price change.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.effective_date
  is 'the date the generated price change will go into effect.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.change_type
  is 'describes the change type of generated price change. types include: change by amount, change by percent, change to amount, reset point of sale price, and no change. valid values : 0 = change by percent 1 = change by amount 2 = change to amount 3 = reset point of sale price -1 = no change';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.change_amount
  is 'when the change type is change by amount or change to amount this field will hold the amount.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.change_currency
  is 'the currency code of the link code price change.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.change_percent
  is 'when the change type is change by percent this field will hold percent.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.change_selling_uom
  is 'when the change type is change to amount this field will hold percent will hold the selling uom associated with the amount.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.null_multi_ind
  is 'field to indicate whether or not multi unit retail should be changed as a result of the generated price change.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.multi_units
  is 'contains the new multi units determined by the generated price change.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.multi_unit_retail
  is 'this field contains the new multi unit retail price in the selling unit of measure determined by the generated price change.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.multi_selling_uom
  is 'this field holds the selling unit of measure for an items multi-unit retail.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.price_guide_id
  is 'the price guide associated with the generated price change.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.ignore_constraints
  is 'indicates whether or not pricing constraints should be considered on the generated price change.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.auto_approve_ind
  is 'this column indicate whether the price event injector should try to approve the generated price change. valid values :  0 = do not automatically approve the price event 1 = automatically approve the price event.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.status
  is 'the status of the staging table records during the price event injector process.  valid values :       n  = new       e  = error       w = worksheet       a = approved        f  = failed with unexpected error during conflict check';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.error_message
  is 'the error message that this record has during the price event injector process.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.process_id
  is 'the process id that is used internally by the price event injector batch program.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.price_change_id
  is 'the id of the generated price change.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.price_change_display_id
  is 'the display id of the generated price change.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.skulist
  is 'the skulist for the staged price change record.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.thread_num
  is 'Thread number of the staged price change';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.exclusion_created
  is 'Indicates whether a system generated exclusion has been generated during approval of the staged price change.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.vendor_funded_ind
  is 'Indicates whether or not the staged price change is vendor funded.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.funding_type
  is 'Describes the type of funding the vendor is providing';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.funding_amount
  is 'The amount of the price change that is funded. This field will only be populated when the funding_type is 1 (amount).';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.funding_amount_currency
  is 'The currency of the funding amount. This field will only be populated when the funding_type is 1 (amount).';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.funding_percent
  is 'The percent of the price change this is funded. This field will only be populated when the funding_type is 0 (percent).';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.deal_id
  is 'ID of the deal associated with the vendor funded price change. This field will only contain a value when the vendor_funded_ind is 1. ';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.deal_detail_id
  is 'ID of the deal detail associated with the vendor funded price change.This field will only contain a value when the vendor_funded_ind is 1.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.zone_group_id
  is 'Optional field that can be populated to setup price changes at all zones under the zone group.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.stage_cust_attr_id
  is 'This column holds the Staging Custom Attribute ID. ';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.cust_attr_id
  is 'The ID of the generated Custom Attributes data.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.processed_date
  is 'As a result of an update to the entity, the DateTime of the update will be captured in this field for maintenance purposes.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.create_id
  is 'User that created the Price Change event.';
comment on column XXADEO_SP_V_STAGE_PRICE_CHANGE.create_datetime
  is 'Creation Date of the Price Change event.';
