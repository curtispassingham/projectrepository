--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal


begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY (ITEM, 
                                                                SUPPLIER, 
                                                                ORIGIN_COUNTRY_ID, 
                                                                UNIT_COST, 
                                                                LEAD_TIME, 
                                                                PICKUP_LEAD_TIME,
                                                                SUPP_PACK_SIZE,
                                                                INNER_PACK_SIZE,
                                                                ROUND_LVL,
                                                                ROUND_TO_INNER_PCT,
                                                                ROUND_TO_CASE_PCT,
                                                                ROUND_TO_LAYER_PCT,
                                                                ROUND_TO_PALLET_PCT,
                                                                MIN_ORDER_QTY,
                                                                MAX_ORDER_QTY,
                                                                PACKING_METHOD,
                                                                PRIMARY_SUPP_IND,
                                                                PRIMARY_COUNTRY_IND,
                                                                DEFAULT_UOP,
                                                                TI,
                                                                HI,
                                                                SUPP_HIER_TYPE_1,
                                                                SUPP_HIER_LVL_1,
                                                                SUPP_HIER_TYPE_2,
                                                                SUPP_HIER_LVL_2,
                                                                SUPP_HIER_TYPE_3,
                                                                SUPP_HIER_LVL_3,
                                                                CREATE_DATETIME,
                                                                LAST_UPDATE_DATETIME,
                                                                LAST_UPDATE_ID,
                                                                COST_UOM,
                                                                TOLERANCE_TYPE,
                                                                MAX_TOLERANCE,
                                                                MIN_TOLERANCE,
                                                                NEGOTIATED_ITEM_COST,
                                                                EXTENDED_BASE_COST,
                                                                INCLUSIVE_COST,
                                                                BASE_COST,
                                                                CREATE_ID
	                                                         ) AS 
SELECT ITEM, 
       SUPPLIER, 
       ORIGIN_COUNTRY_ID, 
       UNIT_COST, 
       LEAD_TIME, 
       PICKUP_LEAD_TIME,
       SUPP_PACK_SIZE,
       INNER_PACK_SIZE,
       ROUND_LVL,
       ROUND_TO_INNER_PCT,
       ROUND_TO_CASE_PCT,
       ROUND_TO_LAYER_PCT,
       ROUND_TO_PALLET_PCT,
       MIN_ORDER_QTY,
       MAX_ORDER_QTY,
       PACKING_METHOD,
       PRIMARY_SUPP_IND,
       PRIMARY_COUNTRY_IND,
       DEFAULT_UOP,
       TI,
       HI,
       SUPP_HIER_TYPE_1,
       SUPP_HIER_LVL_1,
       SUPP_HIER_TYPE_2,
       SUPP_HIER_LVL_2,
       SUPP_HIER_TYPE_3,
       SUPP_HIER_LVL_3,
       CREATE_DATETIME,
       LAST_UPDATE_DATETIME,
       LAST_UPDATE_ID,
       COST_UOM,
       TOLERANCE_TYPE,
       MAX_TOLERANCE,
       MIN_TOLERANCE,
       NEGOTIATED_ITEM_COST,
       EXTENDED_BASE_COST,
       INCLUSIVE_COST,
       BASE_COST,
       CREATE_ID
  FROM RMS.ITEM_SUPP_COUNTRY
/
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.ITEM                 IS 'Alphanumeric value that identifies the item.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.SUPPLIER             IS 'The unique identifier for the supplier.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID    IS 'The country where the item was manufactured or significantly altered.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.UNIT_COST            IS 'This field contains the current corporate unit cost for the SKU from the supplier/origin country.  This field is stored in item''s standard uom and in supplier currency.  This value will match the cost field for the primary location on item_supp_country_loc.  This field may be edited while the item is in worksheet status.  If edited, the user will have the choice of changing the cost of all locations, or only the primary location.  This cost is the cost that will be written to item_supp_country_loc when new locations are added to an item.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.LEAD_TIME            IS 'This field contains the number of days that will elapse between the date an order is written and the delivery to the store or warehouse from the supplier.  This field is defaulted from the default lead time set at the supplier level.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.PICKUP_LEAD_TIME     IS 'Contains time it takes to get the item from the Supplier to the Initial Receiving Location.  This value will be defaulted to the item_supp_country_loc pickup lead time field.  The ordering dialog will reference the item/supplier/country/location pickup lead time as the value may vary by location.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE       IS 'This field contains the quantity that orders must be placed in multiples of for the supplier for the item.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.INNER_PACK_SIZE      IS 'This field contains the units of an item contained in an inner pack supplied by the supplier.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.ROUND_LVL            IS 'This column will be used to determine how order quantities will be rounded to Case, Layer and Pallet.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.ROUND_TO_INNER_PCT   IS 'This column will hold the Inner Rounding Threshold value.  During rounding, this value is used to determine whether to round partial Inner quantities up or down.  If the Inner-fraction in question is less than the Threshold proportion, it is rounded down; if not, it is rounded up.  For instance, with an Inner size of 10 and a Threshold of 80%, Inner quantities such as 18, 29 and 8 would be rounded up to 20, 30 and 10 respectively, while quantities of 12, 27 and 35 would be rounded down to 10, 20 and 30 respectively.  Quantities are never rounded down to zero; a quantity of 7, in the example above, would be rounded up to 10.  This column will be maintained simply for the purpose of defaulting to the Item/Supplier/Country/Location level.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT    IS 'This column will hold the Case Rounding Threshold value.  During rounding, this value is used to determine whether to round partial Case quantities up or down.  If the Case-fraction in question is less than the Threshold proportion, it is rounded down; if not, it is rounded up.  For instance, with an Case size of 10 and a Threshold of 80%, Case quantities such as 18, 29 and 8 would be rounded up to 20, 30 and 10 respectively, while quantities of 12, 27 and 35 would be rounded down to 10, 20 and 30 respectively.  Quantities are never rounded down to zero; a quantity of 7, in the example above, would be rounded up to 10.  This column will be maintained simply for the purpose of defaulting to the Item/Supplier/Country/Location level';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.ROUND_TO_LAYER_PCT   IS 'This column will hold the Layer Rounding Threshold value.  During rounding, this value is used to determine whether to round partial Layer quantities up or down.  If the Layer-fraction in question is less than the Threshold proportion, it is rounded down; if not, it is rounded up.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.ROUND_TO_PALLET_PCT  IS 'This column will hold the Pallet Rounding Threshold value. During rounding, this value is used to determine whether to round partial Pallet quantities up or down.  If the Pallet -fraction in question is less than the Threshold proportion, it is rounded down; if not, it is rounded up.  For instance, with an Pallet size of 10 and a Threshold of 80%, Pallet quantities such as 18, 29 and 8 would be rounded up to 20, 30 and 10 respectively, while quantities of 12, 27 and 35 would be rounded down to 10, 20 and 30 respectively.  Quantities are never rounded down to zero; a quantity of 7, in the example above, would be rounded up to 10.  This column will be maintained simply for the purpose of defaulting to the Item/Supplier/Country/Location level.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.MAX_ORDER_QTY        IS 'This field contains the maximum quantity that can be ordered at one time from the supplier for the item.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.MIN_ORDER_QTY        IS 'This field contains the minimum quantity that can be ordered at one time from the supplier for the item.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.PACKING_METHOD       IS 'This field indicates whether the packing method of the item in the container is Flat or Hanging.  Values for this field are store in the PKMT code.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.PRIMARY_SUPP_IND     IS 'This field indicates whether this supplier is the primary supplier for the Item.  Each Item can have one and only one primary supplier.  This field is stored on this table for performance purposes only.  Valid values are Y or N.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.PRIMARY_COUNTRY_IND  IS 'This field indicates whether this country is the primary country for the item/supplier.  Each item/supplier combination must have one and only one primary country.  Valid values are Y or N.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.DEFAULT_UOP          IS 'Contains the default unit of purchase for the item/supplier/country.  Valid values include: Standard Units of Measure C for Case  P for Pallet';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.TI                   IS 'Number of shipping units (cases) that make up one tier of a pallet.  Multiply TI x HI to get total number of units (cases) for a pallet.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.HI                   IS 'Number of tiers that make up a complete pallet (height).  Multiply TI x HI to get total number of units (cases) for a pallet.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.SUPP_HIER_TYPE_1     IS 'Identifies partner type of supplier hierarchy level 1.This field will always have the partner type S1 which indicates manufacturer.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.SUPP_HIER_LVL_1      IS 'Highest level of supplier hierarchy which is uder to indicate a partner, such as a manufacturer, in the supply chain that gives rebates to the retailer. This information is stored on item_supp_country for defaulting into item_supp_country_loc.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.SUPP_HIER_TYPE_2     IS 'Identifies partner type of supplier hierarchy level 2 .  This field will always have the partner type S2 which indicates distributor.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.SUPP_HIER_LVL_2      IS 'Second highest level of supplier hierarchy  which is uder to indicate a partner, such as a distributor in the supply chain that gives rebates to the retailer. This information is stored on item_supp_country for defaulting into item_supp_country_loc.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.SUPP_HIER_TYPE_3     IS 'Identifies partner type of supplier hierarchy level 3.   This field will always have the partner type S3 which indicates wholesaler.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.SUPP_HIER_LVL_3      IS 'Third highest level of supplier hierarchy which is uder to indicate a partner, such as a wholesaler in the supply chain that gives rebates to the retailer.  This information is stored on item_supp_country for defaulting into item_supp_country_loc.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.CREATE_DATETIME      IS 'Date/time stamp of when the record was created.  This date/time will be used in export processing.  This value should only be populated on insert  - it should never be updated.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.LAST_UPDATE_DATETIME IS 'Holds the date time stamp of the most recent update by the last_update_id.  This field is required by the database.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.LAST_UPDATE_ID       IS 'Holds the Oracle user-id of the user who most recently updated this record.  This field is required by the database.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.COST_UOM             IS 'A cost UOM is held to allow costs to be managed in a separate UOM than the standard UOM. The unit cost stored in standard UOM is converted to cost UOM for display in the Item Supplier Country screen. Likewise, if the user enters or updates the unit cost via the Item Supplier Country screen, it is converted to standard UOM before being saved to the table.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.TOLERANCE_TYPE       IS 'The unit of the tolerances for catch weight simple packs. Valid values are:  A - actual P - percent';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.MAX_TOLERANCE        IS 'The maximum tolerance value for the catch weight simple pack.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.MIN_TOLERANCE        IS 'The minimum tolerance value for a catch weight simple pack.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST IS 'This will hold the supplier negotiated item cost for the primary delivery country of the item. Once a location is associated with the item, the primary locations negotiated item cost will be stored in this field.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST   IS 'This will hold the extended base cost for the primary delivery country of the item. Once a location is associated with the item, the primary locations extended base cost will be stored in this field. Extended base cost is the cost inclusive of all the taxes that affect the WAC. In case of GTAX , Extended Base Cost = Base Cost + Non-recoverable taxes. In case of VAT, Extended Base Cost = Base Cost.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.INCLUSIVE_COST       IS 'This will hold the inclusive cost for the primary delivery country of the item. Once a location is associated with the item, the primary locations inclusive cost will be stored in this field. This cost will have both the recoverable and non recoverable taxes included. In case of GTAX , Inclusive Cost = Base Cost + Non-recoverable taxes + Recoverable Taxes. In case of VAT,  Inclusive Cost = Base Cost + VAT';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.BASE_COST            IS 'This field will hold the tax exclusive cost of the item.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_COUNTRY.CREATE_ID            IS 'This column holds the User id of the user who created the record.';
