CREATE OR REPLACE VIEW XXADEO_SP_V_SUBCLASS AS
SELECT "DEPT",
	   "CLASS",
	   "SUBCLASS",
	   "SUB_NAME"
  FROM V_SUBCLASS;
comment on column XXADEO_SP_V_SUBCLASS.DEPT is 'Contains the department number of which the subclass belongs to.';
comment on column XXADEO_SP_V_SUBCLASS.CLASS is 'Contains the class number of which the subclass belongs to.';
comment on column XXADEO_SP_V_SUBCLASS.SUBCLASS is 'Contains the number which uniquely identifies the subclass within a department and class.';
comment on column XXADEO_SP_V_SUBCLASS.SUB_NAME is 'Contains the translated name of the subclass.';
