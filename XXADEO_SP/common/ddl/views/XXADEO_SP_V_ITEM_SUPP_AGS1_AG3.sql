--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_AGS1_AG3 cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_AGS1_AG3 (ITEM, 
                                                                 SUPPLIER, 
                                                                 FRST_AUT_PO_DT_ITEM_SUPP, 
                                                                 LINK_STATUS_ITEM_SUPP, 
                                                                 LINK_TERM_DT_ITEM_SUPP
                                                                ) AS 
SELECT ITEM,
       SUPPLIER,
       FRST_AUT_PO_DT_ITEM_SUPP,
       LINK_STATUS_ITEM_SUPP,
       LINK_TERM_DT_ITEM_SUPP 
  FROM (SELECT ITEM,
               SUPPLIER,
               DATE_21 FRST_AUT_PO_DT_ITEM_SUPP,
               VARCHAR2_1 LINK_STATUS_ITEM_SUPP,
               DATE_22 LINK_TERM_DT_ITEM_SUPP 
          FROM RMS.ITEM_SUPPLIER_CFA_EXT
         WHERE GROUP_ID = 48    
        )
/
