create or replace view xxadeo_sp_v_price_search as
select im.group_no                                       as GROUP_NO,
       im.dept                                           as DEPT,
       im.class                                          as CLASS,
       im.subclass                                       as SUBCLASS,
       im.item                                           as ITEM,
       im.pack_ind                                       as ITEM_TYPE,
       im.short_desc                                     as DESCRIPTION,
       il.selling_unit_retail                            as STORE_PRICE,
       izp.selling_retail                                as ZONE_PRICE,
       ph.action_date                                    as EFFECTIVE_DATE,
       pc.change_amount                                  as NEW_PRICE,
       pc.effective_date                                 as NEW_APPLY_DATE,
       CASE (select cfa_value
               from table(XXADEO_CFA_SQL.SEARCH_CFA_BY_ATTRIB(I_attrib_id          => dvm.value_1,
                                                          I_xxadeo_cfa_key_val_tbl => CAST (MULTISET (select new XXADEO_CFA_KEY_VAL_OBJ(key_col_1    => 'ITEM',
                                                                                                             key_value_1                     => item)
                                                                                                    from xxadeo_sp_v_item_master im1 where im1.item = im.item)
                                                                                                      as XXADEO_CFA_KEY_VAL_TBL))))
       WHEN '3'
         THEN 'Y'
       ELSE 'N' END as CFA,
       decode(rc.code_id, 9, 'Store', 10, 'Store', 'HQ') as ORIGIN,
       s.store                                           as STORE
  from xxadeo_sp_v_item_master      im,
       xxadeo_sp_v_item_loc         il,
       xxadeo_sp_v_rpm_price_change pc,
       xxadeo_sp_v_rpm_izp izp,
       (select location loc, zone_id from rpm_zone_location) locs,
       xxadeo_sp_v_price_hist ph,
       xxadeo_sp_v_rpm_codes rc,
       xxadeo_sp_v_store s,
       xxadeo_mom_dvm dvm
 where im.item = il.item
   and pc.item (+) = im.item
   and pc.location (+) = il.loc
   and izp.item = im.item
   and izp.zone_id = locs.zone_id
   and locs.loc = il.loc
   and pc.reason_code = rc.code_id (+)
   and ph.item = im.item
   and ph.loc = il.loc
   and dvm.bu = s.area
   and dvm.parameter = 'RTL_BLK_ITEM'
   and dvm.func_area = 'CFA'
   and s.store = il.loc
   and pc.state(+) = 'pricechange.state.approved'
   and ph.action_date = (select MAX(action_date)
                           from xxadeo_sp_v_price_hist
                          where item = im.item
                            and loc = il.loc
                            and action_date <= get_vdate
                            and tran_type in (0,4))
   and exists (select 1
                 from xxadeo_sp_v_rpm_item_loc ril
                where ril.item = im.item
                  and ril.loc  = il.loc)
   and not exists (select 1
                     from xxadeo_sp_v_daily_purge dly
                    where dly.key_value = im.item
                      and dly.table_name = 'ITEM_MASTER')
 order by group_no, dept, class, subclass, item, effective_date;
