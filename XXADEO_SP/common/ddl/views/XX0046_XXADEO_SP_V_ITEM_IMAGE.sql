CREATE OR REPLACE VIEW XXADEO_SP_V_ITEM_IMAGE AS
SELECT item
	     ,image_name
	     ,image_addr
	     ,image_desc
	     ,create_datetime
	     ,last_update_datetime
	     ,last_update_id
	     ,create_id
	     ,image_type
	     ,primary_ind
	     ,display_priority
FROM ITEM_IMAGE;
-- Add comments to the table
comment on table XXADEO_SP_V_ITEM_IMAGE
  is 'This view holds a copy of all images (pictures) and related documentation associated with an item.  These images and documentation are used to create the Merchandise Specification.';
-- Add comments to the columns
comment on column XXADEO_SP_V_ITEM_IMAGE.item
  is 'This field contains the unique alphanumeric identifier for the item, the image is for.';
comment on column XXADEO_SP_V_ITEM_IMAGE.image_name
  is 'This field contains the name of the image of the item.';
comment on column XXADEO_SP_V_ITEM_IMAGE.image_addr
  is 'This field contains the actual path where the file of the image of the item is stored.';
comment on column XXADEO_SP_V_ITEM_IMAGE.image_desc
  is 'This field contains the description associated with the image of the item.';
comment on column XXADEO_SP_V_ITEM_IMAGE.create_datetime
  is 'This field contains date/time stamp of when the record was created.  This value is only to be populated on insert and never be updated.';
comment on column XXADEO_SP_V_ITEM_IMAGE.last_update_datetime
  is 'This field holds the date time stamp of the most recent update by the last_update_id.  This field is required by the database.';
comment on column XXADEO_SP_V_ITEM_IMAGE.last_update_id
  is 'This field holds the Oracle user-id of the user who most recently updated this record.  This field is required by the database.';
comment on column XXADEO_SP_V_ITEM_IMAGE.create_id
  is 'This column holds the User id of the user who created the record.';
comment on column XXADEO_SP_V_ITEM_IMAGE.image_type
  is 'This field contains the type of the image of the item.  Valid values are defined as member of IITD code type.';
comment on column XXADEO_SP_V_ITEM_IMAGE.primary_ind
  is 'This field will indicate whether this record is the primary image of the item or not.  Valid values are Y(es) and N(o) only.  Default to N value if left blank or set as NULL.';
comment on column XXADEO_SP_V_ITEM_IMAGE.display_priority
  is 'This field will specify the display sequence order of images associated to the item per priority.';
