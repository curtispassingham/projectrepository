CREATE OR REPLACE VIEW XXADEO_SP_V_ITEM_SUPS AS
select  sups.supplier, 
        sups.sup_name, 
        sups.contact_name, 
        sups.contact_email, 
        sups.contact_phone, 
        sups.ret_allow_ind, 
        addr.add_1, 
        addr.city, 
        addr.country_id, 
        addr.state, 
        addr.post
from XXADEO_SP_V_SUPS sups, XXADEO_SP_V_ADDR addr, XXADEO_SP_V_ITEM_SUPPLIER isup 
where isup.supplier = sups.supplier and isup.primary_supp_ind = 'Y'
    and sups.supplier_parent is not null
    and sups.sup_status = 'A'
    and addr.key_value_1 = TO_CHAR(isup.supplier)
    and addr.primary_addr_ind = 'Y';

