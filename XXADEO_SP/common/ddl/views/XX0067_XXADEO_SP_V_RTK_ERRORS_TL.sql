create or replace view XXADEO_SP_V_RTK_ERRORS_TL as
select v.rtk_key,
       v.rtk_text,
       v.lang
from   v_rtk_errors_tl v;
