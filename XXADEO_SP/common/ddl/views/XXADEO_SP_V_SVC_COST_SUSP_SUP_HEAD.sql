--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_SUP_HEAD cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD (PROCESS_ID,
                                                                 CHUNK_ID,
	                                                             ROW_SEQ,
	                                                             ACTION,
	                                                             PROCESS$STATUS,
	                                                             COST_CHANGE,
	                                                             COST_CHANGE_DESC,
	                                                             REASON,
	                                                             ACTIVE_DATE,
	                                                             STATUS,
	                                                             COST_CHANGE_ORIGIN,
	                                                             APPROVAL_DATE,
	                                                             APPROVAL_ID,
	                                                             CREATE_ID,
	                                                             CREATE_DATETIME,
	                                                             LAST_UPD_ID,
	                                                             LAST_UPD_DATETIME
                                                                ) AS 
SELECT PROCESS_ID,
       CHUNK_ID,
       ROW_SEQ,
       ACTION,
       PROCESS$STATUS,
       COST_CHANGE,
       COST_CHANGE_DESC,
       REASON,
       ACTIVE_DATE,
       STATUS,
       COST_CHANGE_ORIGIN,
       APPROVAL_DATE,
       APPROVAL_ID,
       CREATE_ID,
       CREATE_DATETIME,
       LAST_UPD_ID,
       LAST_UPD_DATETIME
  FROM RMS.SVC_COST_SUSP_SUP_HEAD
/
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.CHUNK_ID IS 'Uniquely identifies a chunk. The data for a process is split in multiple chunks. The Chunk information is present in SVC_PROCESS_CHUNKS.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.ROW_SEQ IS 'The rows sequence. Should be unique within a process-ID';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.COST_CHANGE IS 'Refer to COST_SUSP_SUP_HEAD.COST_CHANGE.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.COST_CHANGE_DESC IS 'Refer to COST_SUSP_SUP_HEAD.COST_CHANGE_DESC';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.REASON IS 'Refer to COST_SUSP_SUP_HEAD.REASON.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.ACTIVE_DATE IS 'Refer to COST_SUSP_SUP_HEAD.ACTIVE_DATE.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.STATUS IS 'Refer to COST_SUSP_SUP_HEAD.STATUS.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.COST_CHANGE_ORIGIN IS 'Refer to COST_SUSP_SUP_HEAD.COST_CHANGE_ORIGIN.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.CREATE_ID IS 'The user -id who inserted this record.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.CREATE_DATETIME IS 'The date and time when the record was inserted.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.LAST_UPD_ID IS 'The user -id who last updated this record.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_HEAD.LAST_UPD_DATETIME IS 'The date and time when the record was last updated.';
