CREATE OR REPLACE VIEW XXADEO_SP_V_RPM_CODES AS
SELECT "CODE_ID",
	   "CODE_TYPE",
	   "CODE",
	   "DESCRIPTION",
	   "SIM_IND",
	   "LOCK_VERSION" 
  FROM RPM_CODES;
-- Add comments to the columns 
comment on table XXADEO_SP_V_RPM_CODES is 'table to hold the codes used within rpm.';
-- Add comments to the columns 
comment on column xxadeo_sp_v_rpm_codes.code_id
  is 'id to uniquely identify the code.';
comment on column xxadeo_sp_v_rpm_codes.code_type
  is 'the type of code.  valid values include:  0:clearance price; 1:link code; 2:regular price; 3:regular system price; 4:worksheet generated price;';
comment on column xxadeo_sp_v_rpm_codes.code
  is 'this field holds the code used in rpm.';
comment on column xxadeo_sp_v_rpm_codes.description
  is 'the description of the code.';
comment on column xxadeo_sp_v_rpm_codes.sim_ind
  is 'indicates which reason codes will be assigned to price changes/clearances created by an external system.  valid values include:  0 internal code; 1  external reason code;  when the sim_ind on the rpm_system_options table is set to 1, there must be one record on the rpm_codes table with sim_ind set to 1 for code_type of 0 (clearance price), and one record on the rpm_codes with sim_ind set to 1 for code_type of 2 (regular price).';
comment on column xxadeo_sp_v_rpm_codes.lock_version
  is 'field to help with optimistic locking.';
