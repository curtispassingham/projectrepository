CREATE OR REPLACE VIEW XXADEO_SP_V_GROUPS AS
SELECT "DIVISION",
	   "GROUP_NO",
	   "GROUP_NAME"
  FROM V_GROUPS;
comment on table XXADEO_SP_V_GROUPS is 'This view will be used to display the Merchandising Group LOVs using a security policy to filter User access. The group name is translated based on user language.';
comment on column XXADEO_SP_V_GROUPS.DIVISION is 'Contains the number of the division of which the group is a member.';
comment on column XXADEO_SP_V_GROUPS.GROUP_NO is 'Contains the number which uniquely identifies the group.';
