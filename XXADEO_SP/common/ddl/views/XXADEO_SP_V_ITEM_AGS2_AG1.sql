--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS2_AG1 cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS2_AG1 (ITEM, 
                                                            LMFR_HYPER_100_ITEM, 
                                                            LMFR_PRICE_POLICY_ITEM, 
                                                            LMFR_PURCHASE_BLK_ITEM, 
                                                            LMFR_PV_PUBLICATION_ITEM, 
                                                            LMFR_RANKING_ITEM, 
                                                            LMFR_RTL_BLK_ITEM
                                                           ) AS 
SELECT ITEM,
       LMFR_HYPER_100_ITEM,
       LMFR_PRICE_POLICY_ITEM,
       LMFR_PURCHASE_BLK_ITEM,
       LMFR_PV_PUBLICATION_ITEM,
       LMFR_RANKING_ITEM,
       LMFR_RTL_BLK_ITEM 
  FROM (SELECT ITEM,
               VARCHAR2_2 LMFR_HYPER_100_ITEM,
			   VARCHAR2_3 LMFR_PRICE_POLICY_ITEM,
			   VARCHAR2_4 LMFR_PURCHASE_BLK_ITEM,
			   VARCHAR2_5 LMFR_PV_PUBLICATION_ITEM,
			   NUMBER_11  LMFR_RANKING_ITEM,
			   VARCHAR2_1 LMFR_RTL_BLK_ITEM 
          FROM ITEM_MASTER_CFA_EXT 
         WHERE GROUP_ID = 11
	   )
/	   
