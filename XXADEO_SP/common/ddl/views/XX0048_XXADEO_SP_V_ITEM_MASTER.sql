CREATE OR REPLACE VIEW XXADEO_SP_V_ITEM_MASTER AS
SELECT "DIVISION",
	   "GROUP_NO",
	   "DEPT",
	   "CLASS",
	   "SUBCLASS",
	   "ITEM",
	   "STATUS",
	   "ITEM_LEVEL",
	   "TRAN_LEVEL",
	   "ITEM_DESC",
	   "ITEM_DESC_SECONDARY",
	   "DESC_UP",
	   "PACK_TYPE",
	   "PACK_IND",
	   "SELLABLE_IND",
	   "ORDERABLE_IND",
	   "SIMPLE_PACK_IND",
	   "ITEM_NUMBER_TYPE",
	   "ITEM_PARENT",
	   "ITEM_GRANDPARENT",
	   "DIFF_1",
	   "DIFF_2",
	   "DIFF_3",
	   "DIFF_4",
	   "CONTAINS_INNER_IND",
	   "INVENTORY_IND",
	   "STANDARD_UOM",
	   "ITEM_XFORM_IND",
	   "CATCH_WEIGHT_IND",
	   "DEPOSIT_ITEM_TYPE",
	   "CONTAINER_ITEM",
	   "UOM_CONV_FACTOR",
	   "PERISHABLE_IND",
	   "PACKAGE_UOM",
	   "CATCH_WEIGHT_UOM",
	   "MERCHANDISE_IND",
	   "PREFIX",
	   "NOTIONAL_PACK_IND",
	   "HANDLING_SENSITIVITY",
	   "SHIP_ALONE_IND",
	   "DIFF_1_AGGREGATE_IND",
	   "PACKAGE_SIZE",
	   "COST_ZONE_GROUP_ID",
	   "DEPOSIT_IN_PRICE_PER_UOM",
	   "ORDER_AS_TYPE",
	   "FORECAST_IND",
	   "GIFT_WRAP_IND",
	   "FORMAT_ID",
	   "PRODUCT_CLASSIFICATION",
	   "WASTE_TYPE",
	   "SOH_INQUIRY_AT_PACK_IND",
	   "CATCH_WEIGHT_TYPE",
	   "DIFF_2_AGGREGATE_IND",
	   "WASTE_PCT",
	   "SALE_TYPE",
	   "CONST_DIMEN_IND",
	   "DEFAULT_WASTE_PCT",
	   "LAST_UPDATE_ID",
	   "ORDER_TYPE",
	   "CHECK_UDA_IND",
	   "ITEM_AGGREGATE_IND",
	   "ITEM_SERVICE_LEVEL",
	   "STORE_ORD_MULT",
	   "CREATE_DATETIME",
	   "RETAIL_LABEL_VALUE",
	   "ORIGINAL_RETAIL",
	   "AIP_CASE_TYPE",
	   "SHORT_DESC",
	   "HANDLING_TEMP",
	   "RETAIL_LABEL_TYPE",
	   "DIFF_3_AGGREGATE_IND",
	   "LAST_UPDATE_DATETIME",
	   "MFG_REC_RETAIL",
	   "PRIMARY_REF_ITEM_IND",
	   "BRAND_NAME",
	   "COMMENTS",
	   "CREATE_ID",
	   "DIFF_4_AGGREGATE_IND",
	   "CURR_SELLING_UNIT_RETAIL",
	   "CURR_SELLING_UOM"
  FROM V_ITEM_MASTER;
-- Add comments to the table   
comment on table XXADEO_SP_V_ITEM_MASTER is 'This view will be used to display the Item LOVs using a security policy to filter User access';
-- Add comments to the columns 
comment on column XXADEO_SP_V_ITEM_MASTER.DEPT is 'Number identifying the department to which the item is attached.  The items department will be the same as that of its parent (and, by transitivity, to that of its grandparent).  Valid values for this field are located on the deps table.';
comment on column XXADEO_SP_V_ITEM_MASTER.CLASS is 'Number identifying the class to which the item is attached.  The items class will be the same as that of its parent (and, by transitivity, to that of its grandparent).  Valid values for this field are located on the class table.';
comment on column XXADEO_SP_V_ITEM_MASTER.SUBCLASS is 'Number identifying the subclass to which the item is attached.  The items subclass will be the same as that of its parent (and, by transitivity, to that of its grandparent).  Valid values for this field are located on the subclass table.';
comment on column XXADEO_SP_V_ITEM_MASTER.ITEM is 'Unique alphanumeric value that identifies the item.';
comment on column XXADEO_SP_V_ITEM_MASTER.STATUS is 'Status of the item.  Valid values are: W = Worksheet: item setup in progress, cannot be used in system S = Submitted: item setup complete and awaiting approval, cannot be use in system A = Approved: item is approved and can now be used throughout the system';
comment on column XXADEO_SP_V_ITEM_MASTER.ITEM_LEVEL is 'Number indicating which of the three levels the item resides.  The item level determines if the item stands alone or if it is part of a family of related items.  The item level also determines how the item may be used throughout the system.';
comment on column XXADEO_SP_V_ITEM_MASTER.TRAN_LEVEL is 'Number indicating which of the three levels transactions occur for the items group.  The transaction level is the level at which the items inventory is tracked in the system.  The transaction level item will be counted, transferred, shipped, etc.  The transaction level may be at the current item or up to 2 levels above or below the current item.  Only one level of the hierarchy of an item family may contain transaction level items.';
comment on column XXADEO_SP_V_ITEM_MASTER.ITEM_DESC is 'Long description of the item.  This description is used through out the system to help online users identify the item.  For items that have parents, this description will default to the parents description plus any differentiators.  For items without parents, this description will default to null.';
comment on column XXADEO_SP_V_ITEM_MASTER.ITEM_DESC_SECONDARY is 'Secondary descriptions of the item.  This field can only be populated when system_options.secondary_desc_ind = Y.';
comment on column XXADEO_SP_V_ITEM_MASTER.DESC_UP is 'All upper case description of the item (same as upper(item_desc)).  This field is not displayed in the on-line item maintenance dialog, but is used in item list of value search processing through out the system.';
comment on column XXADEO_SP_V_ITEM_MASTER.PACK_TYPE is 'Indicates if pack item is a vendor pack or a buyer pack.  A vendor pack is a pack that the vendor or supplier recognizes and sells to the retailer.  If the pack item is a vendor pack, communication with the supplier will use the vendor pack number.  A buyer pack is a pack that a buyer has created for internal ease of use.  If the pack item is a buyer pack, communication with the supplier will explode the pack out to its component items.  This field will only be available if the item is a pack item.  If the pack item is not orderable this field must be NULL.  Valid values are:    V = Vendor    B = Buyer';
comment on column XXADEO_SP_V_ITEM_MASTER.PACK_IND is 'Indicates if the item is a pack.  A pack item is a collection of items that may be either ordered or sold as a unit.  Packs require details (i.e. component items and qtys, etc.) that other items do not.  This field is required by the database.';
comment on column XXADEO_SP_V_ITEM_MASTER.SELLABLE_IND is 'Indicates if pack item may be sold as a unit.  If it is Y then the pack will have its own unique unit retail.  If it is N then the packs unit retail is the sum of each individual items total retail within the pack. This field will only be available if the item is a pack item.  Valid values are:     Y = Yes, this pack may be sold as a unit    N = No, this pack may not be sold as a unit';
comment on column XXADEO_SP_V_ITEM_MASTER.ORDERABLE_IND is 'Indicates if pack item is orderable.  If it is Y then the suppliers of the pack must supply all components in the pack.  If it is N then the components may have different suppliers. This field will only be available if the item is a pack item.  Valid values are:    Y = Yes, this pack may be ordered     N = No, this pack may not be ordered';
comment on column XXADEO_SP_V_ITEM_MASTER.SIMPLE_PACK_IND is 'Indicates if pack item is a simple pack or not.  This field will only be available if the item is a pack item.  A simple pack is an item whose components are all the same item (i.e. a six pack of cola, etc).  Valid values are:    Y = Yes, this item is a simple pack     N = No, this item is not a simple pack';
comment on column XXADEO_SP_V_ITEM_MASTER.ITEM_NUMBER_TYPE is 'Code specifying what type the item is.  Valid values for this field are in the code type UPCT on the code_head and code_detail tables.';
comment on column XXADEO_SP_V_ITEM_MASTER.ITEM_PARENT is 'Alphanumeric value that uniquely identifies the item/group at the level above the item.  This value must exist as an item in another row on the XXADEO_SP_V_ITEM_MASTER table.';
comment on column XXADEO_SP_V_ITEM_MASTER.ITEM_GRANDPARENT is 'Alphanumeric value that uniquely identifies the item/group two levels above the item.  This value must exist as both an item and an item parent in another row on the XXADEO_SP_V_ITEM_MASTER table.';
comment on column XXADEO_SP_V_ITEM_MASTER.DIFF_1 is 'Diff_group or diff_id that differentiates the current item from its item_parent.  For an item that is a parent, this field may be either a group (i.e. Mens pant sizes) or a value (6 oz).  For an item that is not a parent, this field may contain a value  (34X34, Red, etc.)  Valid values are found on the diff_group and diff_id tables.';
comment on column XXADEO_SP_V_ITEM_MASTER.DIFF_2 is 'Diff_group or diff_id that differentiates the current item from its item_parent.  For an item that is a parent, this field may be either a group (i.e. Mens pant sizes) or a value (6 oz).  For an item that is not a parent, this field may contain a value  (34X34, Red, etc.)  Valid values are found on the diff_group and diff_id tables.';
comment on column XXADEO_SP_V_ITEM_MASTER.DIFF_3 is 'Diff_group or diff_id that differentiates the current item from its item_parent.  For an item that is a parent, this field may be either a group (i.e. Mens pant sizes) or a value (6 oz).  For an item that is not a parent, this field may contain a value  (34X34, Red, etc.)  Valid values are found on the diff_group and diff_id tables.';
comment on column XXADEO_SP_V_ITEM_MASTER.DIFF_4 is 'Diff_group or diff_id that differentiates the current item from its item_parent.  For an item that is a parent, this field may be either a group (i.e. Mens pant sizes) or a value (6 oz).  For an item that is not a parent, this field may contain a value  (34X34, Red, etc.)  Valid values are found on the diff_group and diff_id tables.';
comment on column XXADEO_SP_V_ITEM_MASTER.CONTAINS_INNER_IND is 'Indicates if pack item contains inner packs.  Vendor packs will never contain inner packs and this field will be defaulted to N.  This field will only be available if the item is a pack item.  Valid values are:    Y = Yes, this pack contains inner packs     N = No, this pack does not contain inner packs';
comment on column XXADEO_SP_V_ITEM_MASTER.INVENTORY_IND is 'This indicator is used to determine if an item holds inventory or not for item transformations.';
comment on column XXADEO_SP_V_ITEM_MASTER.STANDARD_UOM is 'Unit of measure in which stock of the item is tracked at a corporate level.';
comment on column XXADEO_SP_V_ITEM_MASTER.ITEM_XFORM_IND is 'This indicator will show that an item is associated an item transformation. The item will be either the sellable item  or orderable item in the transformation process.';
comment on column XXADEO_SP_V_ITEM_MASTER.CATCH_WEIGHT_IND is 'Indiactes whether the item should be weighed when it arives at a location.  Valid values for this field are Y and N.';
comment on column XXADEO_SP_V_ITEM_MASTER.DEPOSIT_ITEM_TYPE is 'This is the deposit item component type. A NULL value in this field indicates that this item is not part of a deposit item relationship. The possible values are - E - Contents A - Container Z - Crate T - Returned Item (Empty bottle) P - Complex pack (with deposit items) The Returned item is flagged only to enable these items to be mapped to a separate GL account if required.';
comment on column XXADEO_SP_V_ITEM_MASTER.CONTAINER_ITEM is 'This holds the container item number for a contents item. This field is only populated and required if the DEPOSIT_ITEM_TYPE = E.';
comment on column XXADEO_SP_V_ITEM_MASTER.UOM_CONV_FACTOR is 'Conversion factor between an Each and the standard_uom when the standard_uom is not in the quantity class (e.g. if standard_uom = lb and 1 lb = 10 eaches, this factor will be 10).  This factor will be used to convert sales and stock data when an item is retailed in eaches but does not have eaches as its standard unit of measure.';
comment on column XXADEO_SP_V_ITEM_MASTER.PERISHABLE_IND is 'A grocery item attribute used to indicate whether an item is perishable or not.';
comment on column XXADEO_SP_V_ITEM_MASTER.PACKAGE_UOM is 'Holds the unit of measure associated with the package size.  This field is used for reporting purposes and by Oracle Retail Price Management to determine same sized and different sized items.';
comment on column XXADEO_SP_V_ITEM_MASTER.CATCH_WEIGHT_UOM is 'UOM for Catchweight Items.';
comment on column XXADEO_SP_V_ITEM_MASTER.MERCHANDISE_IND is 'Indicates if the item is a merchandise item (Y, N).';
comment on column XXADEO_SP_V_ITEM_MASTER.PREFIX is 'This column holds the prefix for variable weight UPCs.  The prefix determines the format of the eventual UPC and will be used to decode variable weight UPCs that are uploaded from the POS.  It is the clients responsibility to download this value to their scale systems.';
comment on column XXADEO_SP_V_ITEM_MASTER.NOTIONAL_PACK_IND is 'This is to indicate that the pack item should post the transaction at pack level in SIM. If this indicator is checked in RMS, SIM will track pack item at the pack level. If the indicator is not checked in RMS, SIM will store inventory at the component level.';
comment on column XXADEO_SP_V_ITEM_MASTER.HANDLING_SENSITIVITY is 'Holds the sensitivity information associated with the item. Valid values for this field are in the code type HSEN on the code_head and code_detail tables.';
comment on column XXADEO_SP_V_ITEM_MASTER.SHIP_ALONE_IND is 'This field will contain a value of Y if the item should be shipped to the customer is a seperate package versus being grouped together in a box.';
comment on column XXADEO_SP_V_ITEM_MASTER.DIFF_1_AGGREGATE_IND is 'Indicator for the corresponding diff. Indicator to aggregate inventory and sales for an item at Parent/Diff level (e.g Style/Color or Style/Size ). This indicator is currently used by allocation and MFP.';
comment on column XXADEO_SP_V_ITEM_MASTER.PACKAGE_SIZE is 'Holds the size of the product printed on any packaging (i.e. 24 ounces).  This field is used for reporting purposes, as well as by Oracle Retail Price Mangement to determine same sized and different sized items.';
comment on column XXADEO_SP_V_ITEM_MASTER.COST_ZONE_GROUP_ID is 'Cost zone group associated with the item.  This field is only required when elc_ind (landed cost indicator) is set to Y on the system_options table.';
comment on column XXADEO_SP_V_ITEM_MASTER.DEPOSIT_IN_PRICE_PER_UOM is 'This field indicates if the deposit amount is included in the price per UOM calculation for a contents item ticket. This value is only required if the DEPOSIT_ITEM_TYPE = E. Valid values are: I - Include deposit amount E - Exclude deposit amount';
comment on column XXADEO_SP_V_ITEM_MASTER.ORDER_AS_TYPE is 'Indicates if pack item is receivable at the component level or at the pack level (for a buyer pack only).  This field is required if pack item is an orderable buyer pack.  This field must be NULL if the pack is sellable only or a vendor pack. This field will only be available if the item is a pack item.  Valid values are:     E = Eaches (component level)    P = Pack (buyer pack only)';
comment on column XXADEO_SP_V_ITEM_MASTER.FORECAST_IND is 'Indicates if this item will be interfaced to an external forecasting system (Y, N).';
comment on column XXADEO_SP_V_ITEM_MASTER.GIFT_WRAP_IND is 'This field will contain a value of Y if the item is eligible to be gift wrapped.';
comment on column XXADEO_SP_V_ITEM_MASTER.FORMAT_ID is 'This field will hold the format ID that corresponds to the items variable  UPC.   This value is only used for items with variable UPCs.';
comment on column XXADEO_SP_V_ITEM_MASTER.PRODUCT_CLASSIFICATION is 'Product classification is informational only in RMS, but is used by RWMS to determine how to pack customer orders : such as to determine products that may not be able to be packaged together.  Classifications are set up as codes in RMS and should be configured as part of an implementation to include classifications that are applicable for each retailer. Examples of classifications may include fragile, toxic, heavy, etc.';
comment on column XXADEO_SP_V_ITEM_MASTER.WASTE_TYPE is 'Identifies the wastage type as either sales or spoilage wastage.  Sales wastage occurs during processes that make an item saleable (i.e. fat is trimmed off at customer request).  Spoilage wastage occurs during the products shelf life (i.e. evaporation causes the product to weigh less after a period of time).  Valid values are:  SP = Spoilage   SL = Sales  Wastage is not applicable to pack items.';
comment on column XXADEO_SP_V_ITEM_MASTER.SOH_INQUIRY_AT_PACK_IND is 'This indicates to show the stock on hand at pack level in down stream applications when it is called in POS from SIM.';
comment on column XXADEO_SP_V_ITEM_MASTER.CATCH_WEIGHT_TYPE is 'This column will hold catch weight type for a simple pack catch weight item. The value is based on the component items order_type and sale_type:  2 - order_type = Variable Weight, sale_type = Loose Weight 4 - order_type = Variable Weight, sale_type = Variable Weight Each The column will be set only at the time of Item approval. It will be used by the ReIM Invoice Matching process';
comment on column XXADEO_SP_V_ITEM_MASTER.DIFF_2_AGGREGATE_IND is 'Indicator for the corresponding diff. Indicator to aggregate inventory and sales for an item at Parent/Diff level (e.g Style/Color or Style/Size ). This indicator is currently used by allocation and MFP.';
comment on column XXADEO_SP_V_ITEM_MASTER.WASTE_PCT is 'Average percent of wastage for the item over its shelf life.  Used in inflating the retail price for wastage items.';
comment on column XXADEO_SP_V_ITEM_MASTER.SALE_TYPE is 'This indicates the method of how catch weight items are sold in store locations. Valid values are: V - variable weight each L - Loose weight Valid values are held on the CODE_DETAIL table with a code type = STPE';
comment on column XXADEO_SP_V_ITEM_MASTER.CONST_DIMEN_IND is 'Indicates that the dimensions of the product are always the same, regardless of the supplier.  If this field is Y, the dimensions for all suppliers will be defaulted to the primary supplier/primary country dimensions.  Editing of dimensions for the item will only be allowed for the primary supplier/primary country.';
comment on column XXADEO_SP_V_ITEM_MASTER.DEFAULT_WASTE_PCT is 'Default daily wastage percent for spoilage type wastage items.  This value will default to all item locations and represents the average amount of wastage that occurs on a daily basis.';
comment on column XXADEO_SP_V_ITEM_MASTER.LAST_UPDATE_ID is 'Holds the Oracle user-id of the user who most recently updated this record.  This field is required by the database.';
comment on column XXADEO_SP_V_ITEM_MASTER.ORDER_TYPE is 'This determines how catch weight items are ordered. Valid values are: F - fixed weight V - Variable weight Valid values are held on the CODE_DETAIL table with a code type = ORDT';
comment on column XXADEO_SP_V_ITEM_MASTER.CHECK_UDA_IND is 'This field indicates whether the user has called the itemuda form.  Since users may delete defauted UDAs, this will prevent these from being automatically displayed whenever the user enters the itemuda form.';
comment on column XXADEO_SP_V_ITEM_MASTER.ITEM_AGGREGATE_IND is 'Indicator to aggregate inventory and sales for the item. Currently, this indiactor is used by allocation and MFP to aggregate inventory for items. For staple items this indictor should be N.';
comment on column XXADEO_SP_V_ITEM_MASTER.ITEM_SERVICE_LEVEL is 'Holds a value that restricts the type of shipment methods that RCOM can select for an item.';
comment on column XXADEO_SP_V_ITEM_MASTER.STORE_ORD_MULT is 'Merchandise shipped from the warehouses to the stores must be specified in this unit type.  Valid values are: C = Cases I = Inner E = Eaches';
comment on column XXADEO_SP_V_ITEM_MASTER.CREATE_DATETIME is 'Date/time stamp of when the record was created.  This date/time will be used in export processing.  This value should only be populated on insert  - it should never be updated.';
comment on column XXADEO_SP_V_ITEM_MASTER.RETAIL_LABEL_VALUE is 'This field represents the value associated with the retail label type.';
comment on column XXADEO_SP_V_ITEM_MASTER.ORIGINAL_RETAIL is 'The original retail price of the item per unit.  This field is stored in the primary currency.';
comment on column XXADEO_SP_V_ITEM_MASTER.AIP_CASE_TYPE is 'Only used if AIP is integrated.   Determines which case sizes to extract against an item in the AIP interface.  Applicable only to non-pack orderable items.';
comment on column XXADEO_SP_V_ITEM_MASTER.SHORT_DESC is 'Shortened description of the item.  This description is the default for downloading to the POS.  For items that have parents, this description will default to the parents short description.  For items without parents, this description will default to null.';
comment on column XXADEO_SP_V_ITEM_MASTER.HANDLING_TEMP is 'Holds the temperature information associated with the item. Valid values for this field are in the code type HTMP on the code_head and code_detail tables.';
comment on column XXADEO_SP_V_ITEM_MASTER.RETAIL_LABEL_TYPE is 'This field indicates any special lable type assoctiated with an item (i.e. pre-priced or cents off).  This field is used for reporting purposes only.  Values for this field are defined by the RTLT code on code detail.';
comment on column XXADEO_SP_V_ITEM_MASTER.DIFF_3_AGGREGATE_IND is 'Indicator for the corresponding diff. Indicator to aggregate inventory and sales for a fashion item at Parent/Diff level (e.g Style/Color or Style/Size ). This indicator is currently used by allocation and MFP';
comment on column XXADEO_SP_V_ITEM_MASTER.LAST_UPDATE_DATETIME is 'Holds the date time stamp of the most recent update by the last_update_id.  This field is required by the database.';
comment on column XXADEO_SP_V_ITEM_MASTER.MFG_REC_RETAIL is 'Manufacturers recommended retail price for the item.  Used for informational purposes only.  This field is stored in the primary currency.';
comment on column XXADEO_SP_V_ITEM_MASTER.PRIMARY_REF_ITEM_IND is 'Indicates if the sub-transation level item is designated as the primary sub-transaction level item.  For transaction level items and above the value in this field will be No.';
comment on column XXADEO_SP_V_ITEM_MASTER.BRAND_NAME is 'This field contains the brand associated to an item';
comment on column XXADEO_SP_V_ITEM_MASTER.COMMENTS is 'Holds any comments associated with the item.';
comment on column XXADEO_SP_V_ITEM_MASTER.CREATE_ID is 'This column holds the User id of the user who created the record.';
comment on column XXADEO_SP_V_ITEM_MASTER.DIFF_4_AGGREGATE_IND is 'Indicator for the corresponding diff. Indicator to aggregate inventory and sales for an item at Parent/Diff level (e.g Style/Color or Style/Size ). This indicator is currently used by allocation and MFP.';
comment on column XXADEO_SP_V_ITEM_MASTER.CURR_SELLING_UNIT_RETAIL is 'This field contains the current selling unit retail of the item.';
comment on column XXADEO_SP_V_ITEM_MASTER.CURR_SELLING_UOM is 'This field contains the current selling UOM of the item.';
