CREATE OR REPLACE VIEW XXADEO_SP_V_ADDR AS
SELECT "ADDR_KEY",
       "MODULE",
       "KEY_VALUE_1",
       "KEY_VALUE_2",
       "SEQ_NO",
       "ADDR_TYPE",
       "PRIMARY_ADDR_IND",
       "ADD_1",
       "ADD_2",
       "ADD_3",
       "CITY",
       "STATE",
       "COUNTRY_ID",
       "POST",
       "CONTACT_NAME",
       "CONTACT_PHONE",
       "CONTACT_TELEX",
       "CONTACT_FAX",
       "CONTACT_EMAIL",
       "ORACLE_VENDOR_SITE_ID",
       "EDI_ADDR_CHG","COUNTY",
       "PUBLISH_IND",
       "JURISDICTION_CODE",
       "EXTERNAL_REF_ID",
       "CREATE_ID",
       "CREATE_DATETIME" 
  FROM ADDR;
-- Add comments to the table 
comment on table XXADEO_SP_V_ADDR
  is 'The ADDR view shows the address of the store, ware house, supplier, and  partner. Seq_no is required as multiple addresses can exist for each address type.';
-- Add comments to the columns 
comment on column XXADEO_SP_V_ADDR.addr_key
  is 'This column contains a unique number used to distinguish between different addresses.';
comment on column XXADEO_SP_V_ADDR.module
  is 'This column holds the code for the module that the address is attached to.  Valid values are supplier (SUPP) and partner (PTNR).';
comment on column XXADEO_SP_V_ADDR.key_value_1
  is 'This column contains specific ID or type that the address is attached to.  If the module is Partner, then key_value_1 holds the type of Partner [BANK (BK), Freight Forwarder (FF), Factory (FA), Agent (AG), Broker (BR), and Importer (IM)], else it will hold the supplier number.';
comment on column XXADEO_SP_V_ADDR.key_value_2
  is 'If the module is Partner (PTNR), then this field will contain the partners ID, else this field will be null.';
comment on column XXADEO_SP_V_ADDR.seq_no
  is 'This column indicates the sequence that addresses within the same type were entered.';
comment on column XXADEO_SP_V_ADDR.addr_type
  is 'This column indicates the type for the address.  Valid values are:  01 - Business, 02 - Postal, 03 - Returns, 04 - Order, 05 - Invoice, 06 - Remittance';
comment on column XXADEO_SP_V_ADDR.primary_addr_ind
  is 'This column indicates whether the address is the primary address for the address type.';
comment on column XXADEO_SP_V_ADDR.add_1
  is 'This column contains the first line of the address.';
comment on column XXADEO_SP_V_ADDR.add_2
  is 'This column contains the second line of the address.';
comment on column XXADEO_SP_V_ADDR.add_3
  is 'This column contains the third line of the address.';
comment on column XXADEO_SP_V_ADDR.city
  is 'This column contains the name of the city that is associated with the address.';
comment on column XXADEO_SP_V_ADDR.state
  is 'This column contains the state abbreviation for the address.';
comment on column XXADEO_SP_V_ADDR.country_id
  is 'This column contains the country where the address exists.';
comment on column XXADEO_SP_V_ADDR.post
  is 'This column contains the zip code for the address.';
comment on column XXADEO_SP_V_ADDR.contact_name
  is 'This column contains the name of the contact for the supplier at this address.';
comment on column XXADEO_SP_V_ADDR.contact_phone
  is 'This column contains the phone number of the contact person at this address.';
comment on column XXADEO_SP_V_ADDR.contact_telex
  is 'This column contains the telex number of the partner or suppliers representative contact.';
comment on column XXADEO_SP_V_ADDR.contact_fax
  is 'This column contains the fax number of the contact person at this address.';
comment on column XXADEO_SP_V_ADDR.contact_email
  is 'This column contains the email address of the partner or suppliers representative contact.';
comment on column XXADEO_SP_V_ADDR.oracle_vendor_site_id
  is 'This column will only be used for the Oracle Financial Interfaces.  It holds the value of the Oracle vendor site ID for a supplier address.  This Oracle vendor site ID is the unique identifier of the address on the Oracle side, it must be stored on the RMS addr table to allow updates from Oracle.  This site ID can be found on the Oracle po_vendor_sites table.';
comment on column XXADEO_SP_V_ADDR.edi_addr_chg
  is 'This column indicates if the address has been changed. This flag is used by the EDI process.';
comment on column XXADEO_SP_V_ADDR.county
  is 'This column holds the county name for the location.';
comment on column XXADEO_SP_V_ADDR.publish_ind
  is 'This column indicates if the detail has been published.';
comment on column XXADEO_SP_V_ADDR.jurisdiction_code
  is 'Identifies the jurisdiction code for the country-state relationship.';
comment on column XXADEO_SP_V_ADDR.external_ref_id
  is 'This field holds the unique address ID from the external source system. Although the ADDR_KEY is the unique key in RMS but it is a system generated oneup number. Hence, external systems will not be aware of this number. The purpose of external_ref_id is to store external system provided identifier of the same row.';
comment on column XXADEO_SP_V_ADDR.create_id
  is 'This column holds the User id of the user who created the record.';
comment on column XXADEO_SP_V_ADDR.create_datetime
  is 'This column holds the record creation date.';