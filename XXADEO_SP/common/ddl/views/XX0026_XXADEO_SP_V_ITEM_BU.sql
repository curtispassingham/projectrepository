/******************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Pedro Vieira                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_SP_V_ITEM_BU" view of 	              		  */
/*					XXADEO_ITEM_BU table				        	          */
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_SP_V_ITEM_BU AS
SELECT ITEM
	,BU
	,RANGE_LETTER
	,GRADING
	,OMNICHANNEL_START_DATE
	,OMNICHANNEL_END_DATE
	,SUBSTITUTE_ITEM
	,SEASON_ITEM_IND
	,CREATE_ID
	,CREATE_DATETIME
	,LAST_UDPATE_ID
	,LAST_UDPATE_DATETIME
FROM XXADEO_ITEM_BU;

-- Add comments to the columns
COMMENT ON COLUMN XXADEO_SP_V_ITEM_BU.ITEM is 'Item Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ITEM_BU.BU is 'BU Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ITEM_BU.RANGE_LETTER is 'Indicates the Range Letter. Sent from CATMAN.'
/
COMMENT ON COLUMN XXADEO_SP_V_ITEM_BU.GRADING is 'Indicates the item grading. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ITEM_BU.OMNICHANNEL_START_DATE is 'Item omnichannel selling start date. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ITEM_BU.OMNICHANNEL_END_DATE is 'Item omnichannel selling end date. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ITEM_BU.SUBSTITUTE_ITEM is 'Substitute item unique identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ITEM_BU.SEASON_ITEM_IND is 'Indicates if it''s a seasonal item. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ITEM_BU.CREATE_ID is 'User that created the record.'
;
COMMENT ON COLUMN XXADEO_SP_V_ITEM_BU.CREATE_DATETIME is 'Date in which the record was created.'
;
COMMENT ON COLUMN XXADEO_SP_V_ITEM_BU.LAST_UDPATE_ID is 'User that last updated the record.'
;
COMMENT ON COLUMN XXADEO_SP_V_ITEM_BU.LAST_UDPATE_DATETIME is 'Date the record was last updated.'
;
