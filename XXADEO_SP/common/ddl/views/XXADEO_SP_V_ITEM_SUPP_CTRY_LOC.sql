--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
  execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC (ITEM,
	                                                             SUPPLIER,
	                                                             ORIGIN_COUNTRY_ID,
	                                                             LOC,
	                                                             LOC_TYPE,
	                                                             PRIMARY_LOC_IND,
                                                                 UNIT_COST,
                                                                 ROUND_LVL,
                                                                 ROUND_TO_INNER_PCT,
                                                                 ROUND_TO_CASE_PCT,
                                                                 ROUND_TO_LAYER_PCT,
                                                                 ROUND_TO_PALLET_PCT,
                                                                 SUPP_HIER_TYPE_1,
                                                                 SUPP_HIER_LVL_1,
                                                                 SUPP_HIER_TYPE_2,
                                                                 SUPP_HIER_LVL_2,
                                                                 SUPP_HIER_TYPE_3,
                                                                 SUPP_HIER_LVL_3,
                                                                 PICKUP_LEAD_TIME,
                                                                 CREATE_DATETIME,
                                                                 LAST_UPDATE_DATETIME,
                                                                 LAST_UPDATE_ID,
                                                                 NEGOTIATED_ITEM_COST,
                                                                 EXTENDED_BASE_COST,
                                                                 INCLUSIVE_COST,
                                                                 BASE_COST,
                                                                 CREATE_ID
	                                                         ) AS 
SELECT ITEM,
	   SUPPLIER,
	   ORIGIN_COUNTRY_ID,
	   LOC,
	   LOC_TYPE,
	   PRIMARY_LOC_IND,
       UNIT_COST,
       ROUND_LVL,
       ROUND_TO_INNER_PCT,
       ROUND_TO_CASE_PCT,
       ROUND_TO_LAYER_PCT,
       ROUND_TO_PALLET_PCT,
       SUPP_HIER_TYPE_1,
       SUPP_HIER_LVL_1,
       SUPP_HIER_TYPE_2,
       SUPP_HIER_LVL_2,
       SUPP_HIER_TYPE_3,
       SUPP_HIER_LVL_3,
       PICKUP_LEAD_TIME,
       CREATE_DATETIME,
       LAST_UPDATE_DATETIME,
       LAST_UPDATE_ID,
       NEGOTIATED_ITEM_COST,
       EXTENDED_BASE_COST,
       INCLUSIVE_COST,
       BASE_COST,
       CREATE_ID
  FROM RMS.ITEM_SUPP_COUNTRY_LOC
/
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.ITEM                 IS 'Alphanumeric value that identifies the item.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.SUPPLIER             IS 'Unique identifier for the supplier.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.ORIGIN_COUNTRY_ID    IS 'The country where the item was manufactured or significantly altered.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.LOC                  IS 'Store/Warehouse locations of the retailer in a country where a given supplier can supply an item.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.LOC_TYPE             IS 'Type of location in the location field.  Valid values are: 		S = Store 		W = Warehouse';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.PRIMARY_LOC_IND      IS 'Store/Warehouse locations of the retailer in a country where a given supplier is the primary or main supplier of an item. Each item/supplier combination must have exactly one primary location.  If item is not primarily supplied by the supplier in a given location then the cost of the primary location is used for the supplier while ordering. Valid values are: 		Y = Yes, this is primary location for this item/supplier 		N = No, this is not the primary location for this item/supplier.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.UNIT_COST            IS 'The current unit cost of the item for the item/supplier/origin_country combination.  This field is stored in the suppliers currency.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.ROUND_LVL            IS 'This column will be used to determine how order quantities will be rounded to Case, Layer and Pallet.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.ROUND_TO_INNER_PCT   IS 'This column will hold the Inner Rounding Threshold value.  During rounding, this value is used to determine whether to round partial Inner quantities up or down.  If the Inner-fraction in question is less than the Threshold proportion, it is rounded down; if not, it is rounded up.  For instance, with an Inner size of 10 and a Threshold of 80%, Inner quantities such as 18, 29 and 8 would be rounded up to 20, 30 and 10 respectively, while quantities of 12, 27 and 35 would be rounded down to 10, 20 and 30 respectively.  Quantities are never rounded down to zero; a quantity of 7, in the example above, would be rounded up to 10.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.ROUND_TO_CASE_PCT    IS 'This column will hold the Case Rounding Threshold value.  During rounding, this value is used to determine whether to round partial Case quantities up or down.  If the Case-fraction in question is less than the Threshold proportion, it is rounded down; if not, it is rounded up.  For instance, with an Case size of 10 and a Threshold of 80%, Case quantities such as 18, 29 and 8 would be rounded up to 20, 30 and 10 respectively, while quantities of 12, 27 and 35 would be rounded down to 10, 20 and 30 respectively.  Quantities are never rounded down to zero; a quantity of 7, in the example above, would be rounded up to 10.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.ROUND_TO_LAYER_PCT   IS 'This column will hold the Layer Rounding Threshold value.  During rounding, this value is used to determine whether to round partial Layer quantities up or down.  If the Layer-fraction in question is less than the Threshold proportion, it is rounded down; if not, it is rounded up.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.ROUND_TO_PALLET_PCT  IS 'This column will hold the Pallet Rounding Threshold value. During rounding, this value is used to determine whether to round partial Pallet quantities up or down.  If the Pallet -fraction in question is less than the Threshold proportion, it is rounded down; if not, it is rounded up.  For instance, with an Pallet size of 10 and a Threshold of 80%, Pallet quantities such as 18, 29 and 8 would be rounded up to 20, 30 and 10 respectively, while quantities of 12, 27 and 35 would be rounded down to 10, 20 and 30 respectively.  Quantities are never rounded down to zero; a quantity of 7, in the example above, would be rounded up to 10.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.SUPP_HIER_TYPE_1     IS 'Identifies partner type of supplier hierarchy level 1.This field will always have the partner type S1 which indicates manufacturer.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.SUPP_HIER_LVL_1      IS 'Highest level of supplier hierarchy which is used to indicate a partner, such as a manufacturer, in the supply chain that gives rebates to the retailer.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.SUPP_HIER_TYPE_2     IS 'Identifies partner type of supplier hierarchy level 2 .  This field will always have the partner type S2 which indicates distributor.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.SUPP_HIER_LVL_2      IS 'Second Highest level of supplier hierarchy which is used to indicate a partner, such as a distributor in the supply chain that gives rebates to the retailer.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.SUPP_HIER_TYPE_3     IS 'Identifies partner type of supplier hierarchy level 3.   This field will always have the partner type S3 which indicates wholesaler.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.SUPP_HIER_LVL_3      IS 'Third highest level of supplier hierarchy which is used to indicate a partner, such as a wholesaler in the supply chain that gives rebates to the retailer';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.PICKUP_LEAD_TIME     IS 'Contains the time it takes to get the item from the Supplier to the Initial Receiving Location.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.CREATE_DATETIME      IS 'Date/time stamp of when the record was created.  This date/time will be used in export processing.  This value should only be populated on insert  - it should never be updated.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.LAST_UPDATE_DATETIME IS 'Holds the date time stamp of the most recent update by the last_update_id.  This field is required by the database.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.LAST_UPDATE_ID       IS 'Holds the Oracle user-id of the user who most recently updated this record.  This field is required by the database.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.NEGOTIATED_ITEM_COST IS 'This will hold the supplier negotiated item cost based on the location of the item.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.EXTENDED_BASE_COST   IS 'This will hold the extended base cost based on the location of the item.  Extended base cost is the cost inclusive of all the taxes that affect the WAC.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.INCLUSIVE_COST       IS 'This will hold the inclusive cost based on the location of the item. This cost will have both the recoverable and non recoverable taxes included.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.BASE_COST            IS 'This will hold the tax exclusive cost of the item.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_ITEM_SUPP_CTRY_LOC.CREATE_ID            IS 'This column holds the User id of the user who created the record.';
