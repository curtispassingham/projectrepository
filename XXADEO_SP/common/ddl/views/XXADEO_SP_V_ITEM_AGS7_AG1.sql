--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS7_AG1 cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS7_AG1 (ITEM, 
                                                            LMGR_HYPER_100_ITEM, 
                                                            LMGR_PRICE_POLICY_ITEM, 
                                                            LMGR_PURCHASE_BLK_ITEM, 
                                                            LMGR_PV_PUBLICATION_ITEM, 
                                                            LMGR_RANKING_ITEM, 
                                                            LMGR_RTL_BLK_ITEM
                                                           ) AS 
SELECT ITEM,   
       LMGR_HYPER_100_ITEM,
       LMGR_PRICE_POLICY_ITEM,
       LMGR_PURCHASE_BLK_ITEM,
       LMGR_PV_PUBLICATION_ITEM,
       LMGR_RANKING_ITEM,
       LMGR_RTL_BLK_ITEM 
  FROM (SELECT ITEM,
               VARCHAR2_2 LMGR_HYPER_100_ITEM,
               VARCHAR2_3 LMGR_PRICE_POLICY_ITEM,
               VARCHAR2_4 LMGR_PURCHASE_BLK_ITEM,
               VARCHAR2_5 LMGR_PV_PUBLICATION_ITEM,
               NUMBER_11  LMGR_RANKING_ITEM,
               VARCHAR2_1 LMGR_RTL_BLK_ITEM 
          FROM RMS.ITEM_MASTER_CFA_EXT 
         WHERE group_id = 31
        )
/
