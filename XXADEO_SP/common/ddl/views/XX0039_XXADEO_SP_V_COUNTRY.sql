CREATE OR REPLACE VIEW XXADEO_SP_V_COUNTRY AS
SELECT COUNTRY_ID,
       COUNTRY_DESC
FROM V_COUNTRY_TL;
comment on table XXADEO_SP_V_COUNTRY is 'This is the translation view for base table COUNTRY. This view fetches data in user langauge either from translation table COUNTRY_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.';
comment on column XXADEO_SP_V_COUNTRY.COUNTRY_ID is 'Contains a number which uniquely identifies the country.';
comment on column XXADEO_SP_V_COUNTRY.COUNTRY_DESC is 'Contains the name of the country.';
