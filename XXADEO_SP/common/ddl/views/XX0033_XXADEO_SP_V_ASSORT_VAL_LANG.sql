CREATE OR REPLACE VIEW XXADEO_SP_V_ASSORT_VAL_LANG AS
select lang,
       rownum seq_id,
       assortment_value_id,
       assortment_value_label,
       create_id,
       create_datetime,
       last_update_id,
       last_update_datetime
from   (select lang,
               seq_id,
               assortment_value_id,
               assortment_value_label,
               create_id,
               create_datetime,
               last_update_id,
               last_update_datetime,
               case
                 when language_sql.get_user_language() >= get_primary_lang() then
                  rank()
                  over(partition by assortment_value_id order by lang desc)
                 else
                  rank()
                  over(partition by assortment_value_id order by lang asc)
               end as lang_rank
        from   XXADEO_SP_V_ASSORTMENT_VAL_TL t
        where  t.lang in
               (language_sql.get_user_language(), get_primary_lang()))
where  lang_rank = 1;
comment on column XXADEO_SP_V_ASSORT_VAL_LANG.LANG is 'This field contains the language in which the translated text is maintained. ';
comment on column XXADEO_SP_V_ASSORT_VAL_LANG.SEQ_ID is 'This field contains rownum for auxiliary ADF multi-select field. ';
comment on column XXADEO_SP_V_ASSORT_VAL_LANG.ASSORTMENT_VALUE_ID is 'Assortmentb Value Unique Identifier. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORT_VAL_LANG.ASSORTMENT_VALUE_LABEL is 'Assortment value lablel translated into the different languages. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORT_VAL_LANG.CREATE_ID is 'User that created the record.';
comment on column XXADEO_SP_V_ASSORT_VAL_LANG.CREATE_DATETIME is 'Date in which the record was created.';
comment on column XXADEO_SP_V_ASSORT_VAL_LANG.LAST_UPDATE_ID is 'User that last updated the record.';
comment on column XXADEO_SP_V_ASSORT_VAL_LANG.LAST_UPDATE_DATETIME is 'Date in which the record was last updated.';
