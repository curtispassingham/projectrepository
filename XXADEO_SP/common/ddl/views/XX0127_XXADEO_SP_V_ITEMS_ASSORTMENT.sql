/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_SP_V_ITEMS_ASSORTMENT"                          */
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_SP_V_ITEMS_ASSORTMENT
 (store,
  group_no,
  dept,
  class,
  subclass,
  item,
  item_desc,
  retail_price,
  assortment_mode,
  range_letter,
  grading,
  cluster_size,
  cs_recommendation,
  hq_recommendation,
  current_regular_asso,
  store_choice,
  supplier_site,
  assortment_start_date,
  assortment_end_date,
  omni_cs_start_date,
  omni_cs_end_date,
  response_end_date,
  pack_component,
  substitute_item,
  related_items,
  comment_cp,
  season_item_ind,
  not_modifiable_ind,
  validated_date,
  update_line_interface,
  store_format,
  catman_status,
  history_flag,
  cluster_popup_flag)
AS
select a.store,
       b.group_no,
       b.dept,
       b.class,
       b.subclass,
       b.item,
       b.item_desc,
       c.unit_retail,
       (select seq_id
        from   xxadeo_sp_v_assort_mode_lang
        where  assortment_mode_id = a.assortment_mode) assortment_mode,
       g.range_letter,
       g.grading,
       a.cluster_size,
       (select seq_id
        from   xxadeo_sp_v_assort_val_lang
        where  assortment_value_id = a.cs_recommendation) cs_recommendation,
       (select seq_id
        from   xxadeo_sp_v_assort_val_lang
        where  assortment_value_id = a.hq_recommendation) hq_recommendation,
       (select seq_id
        from   xxadeo_sp_v_assort_val_lang
        where  assortment_value_id = a.curr_val_assrt) curr_val_assrt,
       (select seq_id
        from   xxadeo_sp_v_assort_val_lang
        where  assortment_value_id = a.store_choice) store_choice,
       d.supplier,
       a.start_date,
       a.end_date,
       g.omnichannel_start_date,
       g.omnichannel_end_date,
       a.response_date,
       (select max(pack_component)
        from   (select 'P' pack_component
                from   xxadeo_sp_v_packitem
                where  pack_no = a.item
                union all
                select 'C' pack_component
                from   xxadeo_sp_v_packitem
                where  item = a.item)) pack_component,
       g.substitute_item,
       (select decode(max('Y'), 'Y', 'Y', 'N')
        from   xxadeo_sp_v_related_item
        where  item = a.item
        and    rownum = 1) related_items,
       a.comments,
       g.season_item_ind,
       a.not_mod_ind,
       a.validated_date,
       a.last_update_datetime,
       (select sf.store_format
        from   xxadeo_sp_v_store_format_tl sf,
               xxadeo_sp_system_options    so
        where  sf.store_format = so.store_format) store_format,
       a.status,
       a.history_flag,
       a.cluster_popup_flag
from   xxadeo_sp_v_assortment a,
       xxadeo_sp_v_item_master b,
       xxadeo_sp_v_item_loc c,
       xxadeo_sp_v_item_supplier d,
       xxadeo_sp_v_item_bu g,
       xxadeo_sp_v_store h
where  a.item = b.item
and    a.item = c.item
and    a.store = c.loc
and    a.item = d.item
and    a.item = g.item
and    a.store = h.store
and    g.bu = h.area
and    not exists (select 1
        from   xxadeo_sp_v_daily_purge dly
        where  dly.key_value = b.item
        and    dly.table_name = 'ITEM_MASTER')
order  by cluster_popup_flag desc,
          b.group_no,
          b.dept,
          b.class,
          b.subclass,
          g.grading,
          b.item,
          a.start_date;
