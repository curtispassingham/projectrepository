--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC (PROCESS_ID,
                                                                CHUNK_ID,
                                                                ROW_SEQ,
                                                                ACTION,
                                                                PROCESS$STATUS,
                                                                BRACKET_UOM1,
                                                                BRACKET_VALUE2,
                                                                UNIT_COST,
                                                                COST_CHANGE_TYPE,
                                                                COST_CHANGE_VALUE,
                                                                RECALC_ORD_IND,
                                                                DEFAULT_BRACKET_IND,
                                                                DEPT,
                                                                SUP_DEPT_SEQ_NO,
                                                                DELIVERY_COUNTRY_ID,
                                                                COST_CHANGE,
                                                                SUPPLIER,
                                                                ORIGIN_COUNTRY_ID,
                                                                ITEM,
                                                                LOC_TYPE,
                                                                LOC,
                                                                BRACKET_VALUE1,
                                                                CREATE_ID,
                                                                CREATE_DATETIME,
                                                                LAST_UPD_ID,
                                                                LAST_UPD_DATETIME
                                                               ) AS 
SELECT PROCESS_ID,
       CHUNK_ID,
       ROW_SEQ,
       ACTION,
       PROCESS$STATUS,
       BRACKET_UOM1,
       BRACKET_VALUE2,
       UNIT_COST,
       COST_CHANGE_TYPE,
       COST_CHANGE_VALUE,
       RECALC_ORD_IND,
       DEFAULT_BRACKET_IND,
       DEPT,
       SUP_DEPT_SEQ_NO,
       DELIVERY_COUNTRY_ID,
       COST_CHANGE,
       SUPPLIER,
       ORIGIN_COUNTRY_ID,
       ITEM,
       LOC_TYPE,
       LOC,
       BRACKET_VALUE1,
       CREATE_ID,
       CREATE_DATETIME,
       LAST_UPD_ID,
       LAST_UPD_DATETIME
  FROM RMS.SVC_COST_SUSP_SUP_DETAIL_LOC
/
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.CHUNK_ID IS 'Uniquely identifies a chunk. The data for a process is split in multiple chunks. The Chunk information is present in SVC_PROCESS_CHUNKS';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.ROW_SEQ IS 'The rows sequence. Should be unique within a process-ID';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.BRACKET_UOM1 IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.BRACKET_UOM1.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.BRACKET_VALUE2 IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.BRACKET_VALUE2.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.UNIT_COST IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.UNIT_COST.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.COST_CHANGE_TYPE IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE_TYPE.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.COST_CHANGE_VALUE IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE_VALUE.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.RECALC_ORD_IND IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.RECALC_ORD_IND.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.DEFAULT_BRACKET_IND IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.DEFAULT_BRACKET_IND.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.DEPT IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.DEPT.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.SUP_DEPT_SEQ_NO IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.SUP_DEPT_SEQ_NO.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.DELIVERY_COUNTRY_ID IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.DELIVERY_COUNTRY_ID.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.COST_CHANGE IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.SUPPLIER IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.SUPPLIER.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.ORIGIN_COUNTRY_ID IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.ORIGIN_COUNTRY_ID.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.ITEM IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.ITEM.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.LOC_TYPE IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.LOC_TYPE.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.LOC IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.LOC.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.BRACKET_VALUE1 IS 'Refer to COST_SUSP_SUP_DETAIL_LOC.BRACKET_VALUE1.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.CREATE_ID IS 'The user -id who inserted this record.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.CREATE_DATETIME IS 'The date and time when the record was inserted.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.LAST_UPD_ID IS 'The user -id who last updated this record.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_SVC_COST_SUSP_LOC.LAST_UPD_DATETIME IS 'The date and time when the record was last updated.';
