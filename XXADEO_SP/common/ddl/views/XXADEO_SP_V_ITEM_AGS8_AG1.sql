--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS8_AG1 cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS8_AG1 (ITEM, 
                                                            LMCY_HYPER_100_ITEM, 
                                                            LMCY_PRICE_POLICY_ITEM, 
                                                            LMCY_PURCHASE_BLK_ITEM, 
                                                            LMCY_PV_PUBLICATION_ITEM, 
                                                            LMCY_RANKING_ITEM, 
                                                            LMCY_RTL_BLK_ITEM
                                                           ) AS 
SELECT ITEM,   
       LMCY_HYPER_100_ITEM,
       LMCY_PRICE_POLICY_ITEM,
       LMCY_PURCHASE_BLK_ITEM,
       LMCY_PV_PUBLICATION_ITEM,
       LMCY_RANKING_ITEM,
       LMCY_RTL_BLK_ITEM 
  FROM (SELECT ITEM,
               VARCHAR2_2 LMCY_HYPER_100_ITEM,
               VARCHAR2_3 LMCY_PRICE_POLICY_ITEM,
               VARCHAR2_4 LMCY_PURCHASE_BLK_ITEM,
               VARCHAR2_5 LMCY_PV_PUBLICATION_ITEM,
               NUMBER_11  LMCY_RANKING_ITEM,
               VARCHAR2_1 LMCY_RTL_BLK_ITEM 
          FROM RMS.ITEM_MASTER_CFA_EXT 
         WHERE group_id = 35
        )
/
