CREATE OR REPLACE VIEW XXADEO_SP_V_UDA_VALUES AS
SELECT  uda_id,
        uda_value,
        uda_value_desc
  FROM  V_UDA_VALUES_TL tl;
comment on table XXADEO_SP_V_UDA_VALUES is 'This is the translation view for base table UDA_VALUES. This view fetches data in user langauge either from translation table UDA_VALUES_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.';
comment on column XXADEO_SP_V_UDA_VALUES.UDA_ID is 'This field contains a unique number identifying the User Defined Attribute.';
comment on column XXADEO_SP_V_UDA_VALUES.UDA_VALUE is 'This field contains a unique number identifying the User Defined Attribute value for the UDA. A UDA can have multiple values. For example, Color can be a UDA and it can have different values like Green, Red, Blue, etc.';
comment on column XXADEO_SP_V_UDA_VALUES.UDA_VALUE_DESC is 'This field contains a description of the UDA value.';
