--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR (PROCESS_ID,
                                                                 ERROR_SEQ,
                                                                 CHUNK_ID,
                                                                 TABLE_NAME,
                                                                 ROW_SEQ,
                                                                 COLUMN_NAME,
                                                                 ERROR_MSG,
                                                                 ITEM,
                                                                 SUPPLIER,
                                                                 COUNTRY_ID,
                                                                 ERROR_TYPE
                                                                ) AS 
SELECT PROCESS_ID,
       ERROR_SEQ,
       CHUNK_ID,
       TABLE_NAME,
       ROW_SEQ,
       COLUMN_NAME,
       ERROR_MSG,
       ITEM,
       SUPPLIER,
       COUNTRY_ID,
       ERROR_TYPE
  FROM RMS.CORESVC_COSTCHG_ERR
/
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR.PROCESS_ID IS 'The process Id to which this error belongs.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR.ERROR_SEQ IS 'Unique sequence number for each error.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR.CHUNK_ID IS 'The chunk Id to which this error belongs.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR.TABLE_NAME IS 'The cost change service staging table-name to which the error belongs.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR.ROW_SEQ IS 'The row_seq for the staging table row. Helps to locate the row that has the error.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR.COLUMN_NAME IS 'The column key for the error. Maps to s9t_tmpl_cols_def.column_key.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR.ERROR_MSG IS 'The error message.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR.ITEM IS 'The item to which the error belongs.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR.SUPPLIER IS 'The supplier to which error belongs.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR.COUNTRY_ID IS 'The country to which the error belongs.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_CORESVC_COSTCH_ERR.ERROR_TYPE IS 'Represents Issue Type, describing the Issue is of the type ERROR or WARNING.';
