create or replace view XXADEO_SP_V_SUPS as
select
v.supplier,
v.sup_name,
v.sup_name_secondary,
v.supplier_parent,
v.contact_name,
v.contact_phone,
v.contact_fax,
v.contact_pager,
v.sup_status,
v.qc_ind,
v.qc_pct,
v.qc_freq,
v.vc_ind,
v.vc_pct,
v.vc_freq,
v.currency_code,
v.lang,
v.terms,
v.freight_terms,
v.ret_allow_ind,
v.ret_auth_req,
v.ret_min_dol_amt,
v.ret_courier,
v.handling_pct,
v.edi_po_ind,
v.edi_po_chg,
v.edi_po_confirm,
v.edi_asn,
v.edi_sales_rpt_freq,
v.edi_supp_available_ind,
v.edi_contract_ind,
v.edi_invc_ind,
v.edi_channel_id,
v.cost_chg_pct_var,
v.cost_chg_amt_var,
v.replen_approval_ind,
v.ship_method,
v.payment_method,
v.contact_telex,
v.contact_email,
v.settlement_code,
v.pre_mark_ind,
v.auto_appr_invc_ind,
v.dbt_memo_code,
v.freight_charge_ind,
v.auto_appr_dbt_memo_ind,
v.prepay_invc_ind,
v.backorder_ind,
v.vat_region,
v.inv_mgmt_lvl,
v.service_perf_req_ind,
v.invc_pay_loc,
v.invc_receive_loc,
v.addinvc_gross_net,
v.delivery_policy,
v.comment_desc,
v.default_item_lead_time,
v.duns_number,
v.duns_loc,
v.bracket_costing_ind,
v.vmi_order_status,
v.dsd_ind,
v.scale_aip_orders,
v.final_dest_ind,
v.sup_qty_level,
v.external_ref_id,
v.create_id,
v.create_datetime,
v.status_upd_by_rms
from v_sups v;

comment on table XXADEO_SP_V_SUPS
  is 'This table contains one row for each supplier within the company. When ever a supplier name etc. is used by Oracle Retail, or a supplier number is validated, it is always selected from this table.';
-- Add comments to the columns 
comment on column XXADEO_SP_V_SUPS.supplier
  is 'Unique identifying number for a supplier within the system.  The user determines this number when a new supplier is first added to the system.';
comment on column XXADEO_SP_V_SUPS.sup_name
  is 'Contains the suppliers trading name.';
comment on column XXADEO_SP_V_SUPS.sup_name_secondary
  is 'Secondary name of the supplier.';
comment on column XXADEO_SP_V_SUPS.supplier_parent
  is 'PARENT_SUPPLIER field will store supplier number for the supplier sites. For Suppliers, this field will be NULL.';
comment on column XXADEO_SP_V_SUPS.contact_name
  is 'Contains the name of the suppliers representative contact.';
comment on column XXADEO_SP_V_SUPS.contact_phone
  is 'Contains a telephone number for the suppliers representative contact.';
comment on column XXADEO_SP_V_SUPS.contact_fax
  is 'Contains a fax number for the suppliers representative contact.';
comment on column XXADEO_SP_V_SUPS.contact_pager
  is 'Contains the number for the pager of the suppliers representative contact.';
comment on column XXADEO_SP_V_SUPS.sup_status
  is 'Determines whether the supplier is currently active.   Valid values include:  A for an active supplier or I for an inactive supplier.  The status of a supplier will be checked when an order is being created to make certain the supplier is active.';
comment on column XXADEO_SP_V_SUPS.qc_ind
  is 'Determines whether orders from this supplier will default as requiring quality control.   A value of Y  means that all orders from this supplier will require quality control unless overridden by the user when the order is created.  An N in this field means that quality control will not be required unless indicated by the user during order creation.';
comment on column XXADEO_SP_V_SUPS.qc_pct
  is 'Indicates the percentage of items per receipt that will be marked for quality checking.';
comment on column XXADEO_SP_V_SUPS.qc_freq
  is 'Indicates the frequency in which items per receipt will be marked for quality checking.';
comment on column XXADEO_SP_V_SUPS.vc_ind
  is 'Determines whether orders from this supplier will default as requiring vendor control.   A value of Y  means that all orders from this supplier will require vendor control. An N in this field means that vendor control will not be required.';
comment on column XXADEO_SP_V_SUPS.vc_pct
  is 'Indicates the percentage of items per receipt that will be marked for vendor checking.';
comment on column XXADEO_SP_V_SUPS.vc_freq
  is 'Indicates the frequency in which items per receipt will be marked for vendor checking.';
comment on column XXADEO_SP_V_SUPS.currency_code
  is 'Contains a code identifying the currency the supplier uses for business transactions.';
comment on column XXADEO_SP_V_SUPS.lang
  is 'This field contains the suppliers preferred language.  This field is provided for custom purchase orders in a specified language.';
comment on column XXADEO_SP_V_SUPS.terms
  is 'Indicator identifying the sales terms that will default when an order is created for the supplier.  These terms specify when payment is due and if any discounts exist for early payment.';
comment on column XXADEO_SP_V_SUPS.freight_terms
  is 'Indicator that references what freight terms will default when a order is created for the supplier.';
comment on column XXADEO_SP_V_SUPS.ret_allow_ind
  is 'Indicates whether or not the supplier will accept returns.  Valid values are Y and N.';
comment on column XXADEO_SP_V_SUPS.ret_auth_req
  is 'Indicates if returns must be accompanied by an authorization number when sent back to the vendor.   Valid values are Y and N.';
comment on column XXADEO_SP_V_SUPS.ret_min_dol_amt
  is 'Contains a value if the supplier requires a minimum dollar amount to be returned in order to accept the return.  Returns of less than this amount will not be processed by the system.  This field is stored in the suppliers currency.';
comment on column XXADEO_SP_V_SUPS.ret_courier
  is 'Contains the name of the courier that should be used for all returns to the supplier.';
comment on column XXADEO_SP_V_SUPS.handling_pct
  is 'Percentage multiplied by the total order cost to determine the handling cost for the return.';
comment on column XXADEO_SP_V_SUPS.edi_po_ind
  is 'Indicates whether purchase orders will be sent to the supplier via Electronic Data Interchange.   Valid values are Y and N.';
comment on column XXADEO_SP_V_SUPS.edi_po_chg
  is 'Indicates whether purchase order changes will be sent to the supplier via Electronic Data Interchange.   Valid values are Y and N.';
comment on column XXADEO_SP_V_SUPS.edi_po_confirm
  is 'Indicates whether this supplier will send acknowledgment of a purchase orders sent via Electronic Data Interchange.   Valid values are Y and N.';
comment on column XXADEO_SP_V_SUPS.edi_asn
  is 'Indicates whether this supplier will send Advance Shipment Notifications electronically.   Valid values are Y and N.';
comment on column XXADEO_SP_V_SUPS.edi_sales_rpt_freq
  is 'This field contains the EDI sales report frequency for this supplier. Valid values are:   D - Sales and stock information will be downloaded daily  W - Sales and stock information will be downloaded weekly';
comment on column XXADEO_SP_V_SUPS.edi_supp_available_ind
  is 'This field indicates whether the supplier will send availability via EDI.';
comment on column XXADEO_SP_V_SUPS.edi_contract_ind
  is 'This field indicates whether contracts will be sent to the supplier via EDI.';
comment on column XXADEO_SP_V_SUPS.edi_invc_ind
  is 'Indicates whether invoices, debit memos and credit note requests will be sent to/from the supplier via Electronic Data Interchange.   Valid values are Y and N.';
comment on column XXADEO_SP_V_SUPS.edi_channel_id
  is 'If the supplier is an EDI supplier and supports vendor initiated ordering, this field will contain the channel ID for the channel to which all inventory for these types of orders will flow.  This field is used when a vendor initiated order is created for a physical warehouse to determine the virtual warehouse within the physical warehouse to which the inventory will flow.  The virtual warehouse belonging to the indicated channel will be used.  This will only be used in a multichannel environment';
comment on column XXADEO_SP_V_SUPS.cost_chg_pct_var
  is 'This field contains the cost change variance by percent.  If an EDI cost change is accepted and falls within these boundaries, it will be approved when inserted into the cost change dialogue.';
comment on column XXADEO_SP_V_SUPS.cost_chg_amt_var
  is 'This field contains the cost change variance by amount.  If an EDI cost change is accepted and falls within these boundaries, it will be approved when inserted into the cost change dialogue.';
comment on column XXADEO_SP_V_SUPS.replen_approval_ind
  is 'Indicates whether contract orders for the supplier should be created in Approved status.   Valid values are Y and N.';
comment on column XXADEO_SP_V_SUPS.ship_method
  is 'The method used to ship the items on the purchase order from the country of origin to the country of import.  Valid values are:   10 (Vessel, Non-container)    11 (Vessel, Container)  12 (Border Water-borne (Only Mexico and Canada))  20 (Rail, Non-container)  21 (Rail, Container)  30 (Truck, Non-container)   31 (Truck, Container)   32 (Auto)   33 (Pedestrian)   34 (Road, other, includes foot and animal borne)  40 (Air, Non-container)   41 (Air, Container)   50 (Mail)   60 (Passenger, Hand carried)  70 (Fixed Transportation Installation)  80 (Not used at this time)';
comment on column XXADEO_SP_V_SUPS.payment_method
  is '  LC(Letter of Credit)';
comment on column XXADEO_SP_V_SUPS.contact_telex
  is 'This field contains the telex number of the partner or suppliers representative contact.';
comment on column XXADEO_SP_V_SUPS.contact_email
  is 'This field contains the email address of the partner or suppliers representative contact.';
comment on column XXADEO_SP_V_SUPS.settlement_code
  is 'This field indicates which payment process method is used for this supplier.  Valid values are:   E - Evaluated Receipts Settlement (ERS)   N - Not Applicable Choosing a settlement code of type E will cause an Accounts Payable transaction to be written for an item received from this supplier.';
comment on column XXADEO_SP_V_SUPS.pre_mark_ind
  is 'This field indicates whether or not the supplier has agreed to break an order into separate boxes (and mark them) that can be shipped directly to the stores.   Valid values are Y and N.';
comment on column XXADEO_SP_V_SUPS.auto_appr_invc_ind
  is 'Indicates whether or not the suppliers invoice matches can be automatically approved for payment.  Valid values are Y or N.  This field will only be populated if invoice matching is installed.';
comment on column XXADEO_SP_V_SUPS.dbt_memo_code
  is 'Indicates when a debit memo will be sent to the supplier to resolve a discrepancy.  Valid values are Y if debit memos are always to be sent, L if debit memos are used only if a credit note is not sent by the invoice due date, or N if debit memos are never to be sent.  This field will only be populated if invoice matching is installed. Debit memo codes will be held on the codes table under the code type  IMDC.';
comment on column XXADEO_SP_V_SUPS.freight_charge_ind
  is 'Indicates if a supplier is allowed to charge freight costs to the client.  This field will only be populated if invoice matching is installed.  Valid values are Y and N.';
comment on column XXADEO_SP_V_SUPS.auto_appr_dbt_memo_ind
  is 'Indicates whether or not debit memos sent to the supplier can be automatically approved on creation.  Valid values are Y or N.  This field will only be populated if invoice matching is installed.';
comment on column XXADEO_SP_V_SUPS.prepay_invc_ind
  is 'Indicates whether or not all invoices for the supplier can be considered pre-paid invoices.  This field will only be populated if invoice matching is installed.  Valid values are Y and N.';
comment on column XXADEO_SP_V_SUPS.backorder_ind
  is 'Indicates if backorders or partial shipments will be accepted.';
comment on column XXADEO_SP_V_SUPS.vat_region
  is 'Contains the unique identifying number for the VAT region in the system.';
comment on column XXADEO_SP_V_SUPS.inv_mgmt_lvl
  is 'Indicator that determines whether supplier inventory management information can be set up at the supplier/department level or just at the supplier level.  Also determines if orders created through replenishment for this supplier should be split by department or not. If the supplier is returns-only, this field will be null.   Otherwise, this field must have value. Values include: D - Department.  Split orders by department. S - Supplier. Split orders by supplier.';
comment on column XXADEO_SP_V_SUPS.service_perf_req_ind
  is 'Indicates if the suppliers services (e.g. shelf stocking) must be confirmed as performed before paying an invoice from that supplier.  Valid values are Y (all service non-merchandise lines on an invoice from this supplier must be confirmed before the invoice can be paid) and N (services do not need to be confirmed).';
comment on column XXADEO_SP_V_SUPS.invc_pay_loc
  is 'Indicates where invoices from this supplier are paid - at the store or centrally through corporate accounting.  Valid values are S (paid at the store) and C (paid centrally).  This field should only be S if using ReSA to accept payment at the store, and only populated when using invoice matching.';
comment on column XXADEO_SP_V_SUPS.invc_receive_loc
  is 'Indicates where invoices from this supplier are received - at the store or centrally through corporate accounting.  Valid values are S (received at the store) and C (received centrally).  This field should only be populated when using invoice matching.';
comment on column XXADEO_SP_V_SUPS.addinvc_gross_net
  is 'Indicates if the supplier invoice lists items at gross cost instead of net (off-invoice discounts subtracted). Valid values are G (invoices at gross) or N (invoices at net). These are stored on the code table under a code type of INGN.';
comment on column XXADEO_SP_V_SUPS.delivery_policy
  is 'Contains the delivery policy of the supplier.  Next Day indicates that the if a location is closed, the supplier will deliver on the next day.  Next Valid Delivery Day indicates that the supplier will wait until the next scheduled delivery day before delivering.  Valid values come from the DLVY code on code_head/code_detail.';
comment on column XXADEO_SP_V_SUPS.comment_desc
  is 'Any miscellaneous comments associated with the supplier.';
comment on column XXADEO_SP_V_SUPS.default_item_lead_time
  is 'Holds the default lead time for the supplier.  The lead time is the time the supplier needs between receiving an order and having the order ready to ship.  This value will be  defaulted to item/supplier relationships.';
comment on column XXADEO_SP_V_SUPS.duns_number
  is 'This field holds the Dun and Bradstreet number to identify the supplier.';
comment on column XXADEO_SP_V_SUPS.duns_loc
  is 'This field holds the Dun and Bradstreet number to identify the location of the supplier.';
comment on column XXADEO_SP_V_SUPS.bracket_costing_ind
  is 'This field will determine if the supplier uses bracket costing pricing structures.  Valid values:  Y = Yes, N = No.';
comment on column XXADEO_SP_V_SUPS.vmi_order_status
  is 'This column determines the status in which any inbound POs from this supplier are created.  A NULL value indicates that the supplier is not a VMI supplier.  Orders from these suppliers will be still be created in worksheet status.';
comment on column XXADEO_SP_V_SUPS.dsd_ind
  is 'Indicates whether the supplier can ship direct to store.  Valid values are Y and N.';
comment on column XXADEO_SP_V_SUPS.scale_aip_orders
  is 'Depending upon the value in this column, scaling is done for AIP orders. Default value is N.';
comment on column XXADEO_SP_V_SUPS.final_dest_ind
  is 'Supplier can ship to final destination as per allocation or not.';
comment on column XXADEO_SP_V_SUPS.sup_qty_level
  is 'This will hold the level at which quantity is ordered at. Valid values are CA for cases; EA for eaches. Default value if NULL is EA';
comment on column XXADEO_SP_V_SUPS.external_ref_id
  is 'This column holds the ID for the supplier used in the external financial system. It is populated by the integration that brings suppliers from external financial systems into RMS. This ID and the supplier site ID can be used to join to information about the supplier in the external system.';
comment on column XXADEO_SP_V_SUPS.create_id
  is 'This column holds the User id of the user who created the record.';
comment on column XXADEO_SP_V_SUPS.create_datetime
  is 'This column holds the record creation date.';
comment on column XXADEO_SP_V_SUPS.status_upd_by_rms
  is 'Column indicates that Supplier is inactivated from RMS. Suppliers inactivated in RMS will have this column updated as Y. Otherwise this column will be null.';
