-- Create view
create or replace view XXADEO_SP_V_RPM_PRICE_CHANGE as
select price_change_id,
       price_change_display_id,
       state,
       reason_code,
       exception_parent_id,
       item,
       diff_id,
       zone_id,
       location,
       zone_node_type,
       link_code,
       effective_date,
       change_type,
       change_amount,
       change_currency,
       change_percent,
       change_selling_uom,
       null_multi_ind,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       price_guide_id,
       vendor_funded_ind,
       funding_type,
       funding_amount,
       funding_amount_currency,
       funding_percent,
       deal_id,
       deal_detail_id,
       create_date,
       create_id,
       approval_date,
       approval_id,
       area_diff_parent_id,
       ignore_constraints,
       lock_version,
       skulist,
       sys_generated_exclusion,
       price_event_itemlist,
       cust_attr_id
  from RPM_PRICE_CHANGE;
-- Add comments to the table
comment on table XXADEO_SP_V_RPM_PRICE_CHANGE
  is 'Table to hold price changes.  Price changes consist of items (transaction or parent level), locations (single locations or zone) retail changes, multi unit changes, and vendor funding information.  When price changes are defined above the transaction level, transaction levels can be pull off the above the transaction level price change as item exception price changes.  The exception price changes are tied back to the original price change.when price changes are defined at the zone level, individual locations can be pull off the zone level price change as location exception price changes.  The exception price changes are tied back to the original price change.';
-- Add comments to the columns
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.price_change_id
  is 'primary key.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.price_change_display_id
  is 'the id displayed to the user for the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.state
  is 'the state of the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.reason_code
  is 'reason for the price change.  possible reasons are maintained in the rpm code maintenance dialogue.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.exception_parent_id
  is 'If the price change is a transaction item level price change that has been pulled off from a parent/zone level price change, this field holds the id of the parent price change the exception was pulled from.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.item
  is 'item the price change is being applied to.  can be at the transaction level or one level above the transaction level.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.diff_id
  is 'optional field that can be populated when the item field holds an item one level above the transaction.  when it is populated, only children of the item in the item field with this diff_id value are effected by the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.zone_id
  is 'price changes are optionally setup at either the zone level or at the location level.  when a price change is at the zone level, this field will be populated.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.location
  is 'price changes are optionally setup at either the zone level or at the location level.  when a price change is at the location level, this field will be populated.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.zone_node_type
  is 'indicates whether the price change was created at a store, warehouse or zone';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.link_code
  is 'the link code of the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.effective_date
  is 'the date the price change will go into effect.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.change_type
  is 'describes the type of price change exception. types include: change by amount, change by percent, change to amount and reset point of sale price.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.change_amount
  is 'when the change type is change by amount or change to amount this field will hold the amount.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.change_currency
  is 'when the change type is change by amount or change to amount this field will hold the currency of the amount.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.change_percent
  is 'when the change type is change by percent this field will hold percent.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.change_selling_uom
  is 'when the change type is change to amount this field will hold percent will hold the selling uom associated with the amount.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.null_multi_ind
  is 'field to indicate whether or not multi unit retail should be changed as a result of this price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.multi_units
  is 'contains the new multi units determined by the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.multi_unit_retail
  is 'this field contains the new multi unit retail price in the selling unit of measure determined by the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.multi_unit_retail_currency
  is 'this field contains the currency of the new multi unit retail price in the selling unit of measure determined by the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.multi_selling_uom
  is 'this field holds the selling unit of measure for an items multi-unit retail.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.price_guide_id
  is 'the price guide associated with the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.vendor_funded_ind
  is 'indicates whether or not the price change is funded.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.funding_type
  is 'describes the type funding being provided.  types include: change by amount change by percent.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.funding_amount
  is 'the amount of the change that is funded.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.funding_amount_currency
  is 'the currenct of the funding amount.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.funding_percent
  is 'the percent of the change that is funded.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.deal_id
  is 'deal that was used to construct the items and location for the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.deal_detail_id
  is 'deal that was used to construct the items and location for the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.create_date
  is ' the date of the creation of the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.create_id
  is 'the user that created the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.approval_date
  is 'the date of the last approval of the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.approval_id
  is 'the user that last approved the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.area_diff_parent_id
  is 'the id of the parent price change that generated this price change for area differentials';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.ignore_constraints
  is 'indicates whether or not pricing constraints should be considered on this price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.lock_version
  is 'field to help with optimistic locking.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.skulist
  is 'the skulist/itemlist for the price change.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.sys_generated_exclusion
  is 'Indicates if the price change is a system generated exclusion or not.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.price_event_itemlist
  is 'This is the ID for the price event that is created at the Price Event Item List level.';
comment on column XXADEO_SP_V_RPM_PRICE_CHANGE.cust_attr_id
  is 'This column holds the custom attribute defined.';
