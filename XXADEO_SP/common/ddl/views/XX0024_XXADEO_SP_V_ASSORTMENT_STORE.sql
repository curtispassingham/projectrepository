/******************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Pedro Vieira                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_SP_V_ASSORTMENT_STORE" view of 	              */
/*					XXADEO_ASSORTMENT_STORE table				        	  									*/
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_SP_V_ASSORTMENT_STORE AS
SELECT ITEM
	,STORE
	,START_DATE
	,END_DATE
	,RESPONSE_DATE
	,ASSORTMENT_MODE
	,HQ_RECOMMENDATION
	,NOT_MOD_IND
	,STATUS
	,WEEK
	,COMMENTS
	,CREATE_ID
	,CREATE_DATETIME
	,LAST_UPDATE_ID
	,LAST_UPDATE_DATETIME
FROM XXADEO_ASSORTMENT_STORE;

-- Add comments to the columns
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.ITEM is 'Item Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.STORE is 'Store Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.START_DATE is 'Date on which the recommended assortment becomes effective. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.END_DATE is 'Date on which the recommended assortment is no longer valid. Calculated as the next start date minus one day or null if no other start date exists.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.RESPONSE_DATE is 'Date limit which a user will have to act on the assortment from the Store perspective. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.ASSORTMENT_MODE is 'Indicates if the assortment if national, local or test.Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.HQ_RECOMMENDATION is 'Recommended assortment for the item.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.NOT_MOD_IND is 'Indicator which prevents store to change the assortment.Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.STATUS is 'Assortment change status in CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.WEEK is 'Week Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.COMMENTS is 'Comments of the CATMAN user.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.CREATE_ID is 'User that created the record.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.CREATE_DATETIME is 'Date in which the record was created.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.LAST_UPDATE_ID is 'User that last updated the record.'
;
COMMENT ON COLUMN XXADEO_SP_V_ASSORTMENT_STORE.LAST_UPDATE_DATETIME is 'Date in which the record was last updated.'
;
