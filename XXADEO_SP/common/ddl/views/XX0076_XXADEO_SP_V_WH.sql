CREATE OR REPLACE VIEW XXADEO_SP_V_WH AS
SELECT WH_NAME ,
    WH_NAME_SECONDARY ,
    WH WH ,
    CURRENCY_CODE CURRENCY_CODE ,
    PHYSICAL_WH PHYSICAL_WH ,
    PRIMARY_WH PRIMARY_WH ,
    STOCKHOLDING_IND STOCKHOLDING_IND ,
    REPL_IND REPL_IND ,
    IB_IND IB_IND ,
    ORG_UNIT_ID ORG_UNIT_ID,
    VAT_REGION ,
    CHANNEL_ID ,
    TSF_ENTITY_ID ,
    CUSTOMER_ORDER_LOC_IND ,
    EMAIL ,
    DUNS_NUMBER ,
    DUNS_LOC ,
    CREATE_ID ,
    CREATE_DATETIME
  FROM V_WH;
comment on table XXADEO_SP_V_WH is 'This view will be used to display the Warehouse LOVs using a security policy to filter User access.';
comment on column XXADEO_SP_V_WH.WH_NAME is 'Contains the name of the warehouse which, along with the warehouse number, identifies the warehouse.';
comment on column XXADEO_SP_V_WH.WH_NAME_SECONDARY is 'Secondary name of the warehouse.';
comment on column XXADEO_SP_V_WH.WH is 'Contains the number which uniquely identifies the warehouse. The wh table stores all warehouses in the system.  Both virtual and physical warehouses will be stored on this table.  The addition of the new column, physical_wh, helps determine which warehouses are physical and which are virtual.  All physical warehouses will have a physical_wh column value equal to their wh number.  Virtual warehouses will have a valid physical warehouse in this column.';
comment on column XXADEO_SP_V_WH.CURRENCY_CODE is 'This field contains the currency code under which the warehouse operates.';
comment on column XXADEO_SP_V_WH.PHYSICAL_WH is 'This column will contain the number of the physical warehouse that is assigned to the virtual warehouse.';
comment on column XXADEO_SP_V_WH.PRIMARY_WH is 'This field holds the virtual warehouse that will used as the basis for all transactions for which only a physical warehouse and not a virtual warehouse has not been specified.';
comment on column XXADEO_SP_V_WH.STOCKHOLDING_IND is 'This column will indicate if the warehouse is a stock holding location.  In a non-multichannel environment, this will always be Y.     In a multichannel environment it will be N for a physical warehouse and Y for a virtual warehouse.';
comment on column XXADEO_SP_V_WH.REPL_IND is 'This indicator determines if a warehouse is replenishable.';
comment on column XXADEO_SP_V_WH.IB_IND is 'This field indicates if the warehouse is an investment buy warehouse.';
comment on column XXADEO_SP_V_WH.ORG_UNIT_ID is 'this column will hold the oracle oraganizational unit id value.';
comment on column XXADEO_SP_V_WH.VAT_REGION is 'warehouse is located.';
comment on column XXADEO_SP_V_WH.CHANNEL_ID is 'This column will contain the channel for which the virtual warehouse will be assigned.';
comment on column XXADEO_SP_V_WH.TSF_ENTITY_ID is 'ID of the transfer entity with which this warehouse is associated. Valid values are found on the TSF_ENTITY table. A transfer entity is a group of locations that share legal requirements around product management.';
comment on column XXADEO_SP_V_WH.CUSTOMER_ORDER_LOC_IND is 'This Column determines if the location is customer order location or not, i.e. if the indicator is checked then the location can be used by OMS for sourcing/ fulfillment or both else it cannot be used.   It is enabled only for virtual warehouses. For a physical warehouse, this column would be NULL. For virtual warehouses, the valid values are ''Y'' or ''N''.';
comment on column XXADEO_SP_V_WH.EMAIL is 'Holds the email address for the location';
comment on column XXADEO_SP_V_WH.DUNS_NUMBER is 'This field holds the Dun and Bradstreet number to identify the warehouse';
comment on column XXADEO_SP_V_WH.DUNS_LOC is 'This field holds the Dun and Bradstreet number to identify the location';
comment on column XXADEO_SP_V_WH.CREATE_ID is 'This column holds the User id of the user who created the record.';
comment on column XXADEO_SP_V_WH.CREATE_DATETIME is 'This column holds the record creation date.';
