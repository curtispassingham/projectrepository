/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_SP_V_ASSORTMENT_MODE_TL" view of              */
/*               XXADEO_ASSORTMENT_MODE_TL table                              */
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_SP_V_ASSORTMENT_MODE_TL AS
SELECT LANG,
       ROWNUM SEQ_ID,
       ASSORTMENT_MODE_ID,
       ASSORTMENT_MODE_LABEL,
       CREATE_ID,
       CREATE_DATETIME,
       LAST_UPDATE_ID,
       LAST_UPDATE_DATETIME
FROM XXADEO_ASSORTMENT_MODE_TL;
-- Add comments to the columns
comment on column XXADEO_SP_V_ASSORTMENT_MODE_TL.LANG is 'This field contains the language in which the translated text is maintained.';
comment on column XXADEO_SP_V_ASSORTMENT_MODE_TL.SEQ_ID is 'This field contains rownum for auxiliary ADF multi-select field.';
comment on column XXADEO_SP_V_ASSORTMENT_MODE_TL.ASSORTMENT_MODE_ID is 'Assortmentb Mode Unique Identifier. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT_MODE_TL.ASSORTMENT_MODE_LABEL is 'Assortment mode lablel translated into the different languages. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT_MODE_TL.CREATE_ID is 'User that created the record.';
comment on column XXADEO_SP_V_ASSORTMENT_MODE_TL.CREATE_DATETIME is 'Date in which the record was created.';
comment on column XXADEO_SP_V_ASSORTMENT_MODE_TL.LAST_UPDATE_ID is 'User that last updated the record.';
comment on column XXADEO_SP_V_ASSORTMENT_MODE_TL.LAST_UPDATE_DATETIME is 'Date in which the record was last updated.';
