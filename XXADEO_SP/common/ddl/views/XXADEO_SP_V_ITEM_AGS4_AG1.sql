--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS4_AG1 cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS4_AG1 (ITEM, 
                                                            LMPL_HYPER_100_ITEM, 
                                                            LMPL_PRICE_POLICY_ITEM, 
                                                            LMPL_PURCHASE_BLK_ITEM, 
                                                            LMPL_PV_PUBLICATION_ITEM, 
                                                            LMPL_RANKING_ITEM, 
                                                            LMPL_RTL_BLK_ITEM
                                                           ) AS 
SELECT ITEM,   
       LMPL_HYPER_100_ITEM,
       LMPL_PRICE_POLICY_ITEM,
       LMPL_PURCHASE_BLK_ITEM,
       LMPL_PV_PUBLICATION_ITEM,
       LMPL_RANKING_ITEM,
       LMPL_RTL_BLK_ITEM 
  FROM (SELECT ITEM,
               VARCHAR2_2 LMPL_HYPER_100_ITEM,
               VARCHAR2_3 LMPL_PRICE_POLICY_ITEM,
               VARCHAR2_4 LMPL_PURCHASE_BLK_ITEM,
               VARCHAR2_5 LMPL_PV_PUBLICATION_ITEM,
               NUMBER_11  LMPL_RANKING_ITEM,
               VARCHAR2_1 LMPL_RTL_BLK_ITEM 
          FROM RMS.ITEM_MASTER_CFA_EXT 
         WHERE group_id = 19
        )
/
