--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_PARTNER_ORG_UNIT cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_PARTNER_ORG_UNIT (PARTNER, 
	                                                           ORG_UNIT_ID, 
	                                                           PARTNER_TYPE, 
	                                                           PRIMARY_PAY_SITE, 
	                                                           CREATE_ID, 
	                                                           CREATE_DATETIME
	                                                         ) AS 
SELECT PARTNER, 
       ORG_UNIT_ID, 
       PARTNER_TYPE, 
       PRIMARY_PAY_SITE, 
       CREATE_ID, 
       CREATE_DATETIME 
  FROM RMS.PARTNER_ORG_UNIT
/
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_PARTNER_ORG_UNIT.PARTNER          IS 'This field contains either Suppler or Supplier Site.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_PARTNER_ORG_UNIT.ORG_UNIT_ID      IS 'This field contains org_unit_id.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_PARTNER_ORG_UNIT.PARTNER_TYPE     IS 'Identifies the type of the partner. S for Supplier and U for Supplier Site.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_PARTNER_ORG_UNIT.PRIMARY_PAY_SITE IS 'Primary payment site indicator.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_PARTNER_ORG_UNIT.CREATE_ID        IS 'This column holds the User id of the user who created the record.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_PARTNER_ORG_UNIT.CREATE_DATETIME  IS 'This column holds the record creation date.';

   