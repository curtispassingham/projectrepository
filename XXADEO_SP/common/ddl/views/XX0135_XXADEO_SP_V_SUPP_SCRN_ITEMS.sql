create or replace view xxadeo_sp_v_supp_scrn_items as
WITH UDA AS
    (select parameter, bu, to_number(value_1) value_1
       from xxadeo_sp_v_mom_dvm dvm,
            xxadeo_sp_v_uda uda
      where uda.uda_id = dvm.value_1
     and func_area = 'UDA')
select im.group_no                group1,
       im.dept                    department,
       im.class                   class1,
       im.subclass                subclass,
       ibu.grading                grading,
       im.item                    item,
       im.item_desc               description,
       assrt.assortment_mode      assortment_mode,
       im.pack_ind                item_type,
       ibu.range_letter           range_letter,
       assrt.curr_val_assrt       current_regular_assortment,
       isp.supplier               supplier_site,
       ibu.omnichannel_start_date omnichannel_start_date,
       ibu.omnichannel_end_date   omnichannel_end_date,
      (select udaval.uda_value_desc
         from (select uda_value, uda_id
                 from xxadeo_sp_v_uda_item_lov uiv, uda
                where uiv.uda_id = value_1
                  and uda.parameter = 'LIFECYCLE_STATUS'
                  and item = im.item
                  and uda.bu = ibu.bu) uil2, xxadeo_sp_v_uda_values udaval
        where udaval.uda_id = uil2.uda_id and udaval.uda_value = uil2.uda_value) lifecycle_status,
      (select udaval.uda_value_desc
         from (select item, uda_value, uda_id
                 from xxadeo_sp_v_uda_item_lov uiv, uda
                where uiv.uda_id = value_1
                  and uda.parameter = 'ITEM_TYPE'
                  and item = im.item) uil2, xxadeo_sp_v_uda_values udaval
        where udaval.uda_id = uil2.uda_id and udaval.uda_value = uil2.uda_value ) typology,
      (select udaval.uda_value_desc
         from (select item, uda_value, uda_id
                 from xxadeo_sp_v_uda_item_lov uiv, uda
                where uiv.uda_id = value_1
                  and uda.parameter = 'ITEM_SUBTYPE'
                  and item = im.item) uil2, xxadeo_sp_v_uda_values udaval
        where udaval.uda_id = uil2.uda_id and udaval.uda_value = uil2.uda_value ) subtypology,
        st.store                   as STORE
  from xxadeo_sp_v_item_master im,
       xxadeo_sp_v_item_supplier isp,
       xxadeo_sp_v_item_bu ibu,
       xxadeo_sp_v_store st,
       xxadeo_sp_v_item_loc il,
       (select item, store, curr_val_assrt, assortment_mode from xxadeo_sp_v_assortment where get_vdate (+) between start_date and nvl(end_date, to_date(99990101, 'YYYYMMDD'))) assrt
 where im.item             = ibu.item
   and im.item             = assrt.item (+)
   and im.item             = isp.item
   and st.store            = assrt.store (+)
   and st.area             = ibu.bu (+)
   and im.item             = il.item
   and st.store            = il.loc;
