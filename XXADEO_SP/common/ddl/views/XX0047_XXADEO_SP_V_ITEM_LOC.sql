-- XXADEO_SP------------------
------------------------------
CREATE OR REPLACE VIEW XXADEO_SP_V_ITEM_LOC AS
SELECT itl.item
       ,itl.loc
       ,itl.item_parent
       ,itl.item_grandparent
       ,itl.loc_type
       ,itl.unit_retail
       ,il.regular_unit_retail
       ,il.multi_units
       ,il.multi_unit_retail
       ,il.multi_selling_uom
       ,itl.selling_unit_retail
       ,itl.selling_uom
       ,itl.promo_retail
       ,itl.promo_selling_retail
       ,itl.promo_selling_uom
       ,itl.clear_ind
       ,itl.taxable_ind
       ,itl.local_item_desc
       ,itl.local_short_desc
       ,itl.ti
       ,itl.hi
       ,itl.store_ord_mult
       ,itl.status
       ,itl.status_update_date
       ,itl.daily_waste_pct
       ,itl.meas_of_each
       ,itl.meas_of_price
       ,itl.uom_of_price
       ,itl.primary_variant
       ,itl.primary_cost_pack
       ,itl.primary_supp
       ,itl.primary_cntry
       ,itl.receive_as_type
       ,itl.create_datetime
       ,itl.last_update_datetime
       ,itl.last_update_id
       ,itl.inbound_handling_days
       ,il.source_method
       ,il.source_wh
       ,il.store_price_ind
       ,il.rpm_ind
       ,il.uin_type
       ,il.uin_label
       ,il.capture_time
       ,il.ext_uin_ind
       ,il.ranged_ind
       ,il.costing_loc
       ,il.costing_loc_type
       ,il.create_id
  FROM v_item_loc itl, item_loc il
 WHERE il.item = itl.item
   AND il.loc = itl.loc;
  -- Add comments to the table
  comment on table XXADEO_SP_V_ITEM_LOC
    is 'This table contains one row for each item stocked at each location within the company.';
  -- Add comments to the columns
  comment on column XXADEO_SP_V_ITEM_LOC.item
    is 'Unique alphanumeric value that identifies the item.';
  comment on column XXADEO_SP_V_ITEM_LOC.loc
    is 'Numeric identifier of the location in which the item is to be found.  This field may contain a store, warehouse, or external finisher.';
  comment on column XXADEO_SP_V_ITEM_LOC.item_parent
    is 'Alphanumeric value that uniquely identifies the item/group at the level above the item.';
  comment on column XXADEO_SP_V_ITEM_LOC.item_grandparent
    is 'Alphanumeric value that uniquely identifies the item/group two levels above the item.';
  comment on column XXADEO_SP_V_ITEM_LOC.loc_type
    is 'Type of location in the location field.  Valid values are S (store), W (warehouse), and E (external finisher).';
  comment on column XXADEO_SP_V_ITEM_LOC.unit_retail
    is 'Contains the unit retail price in the standard unit of measure for the item/location combination. This field is stored in the local currency.';
  comment on column XXADEO_SP_V_ITEM_LOC.regular_unit_retail
    is 'This field holds the unit retail in the standard unit of measure for the item/location (zone) combination.  This field is stored in the local currency.';
  comment on column XXADEO_SP_V_ITEM_LOC.multi_units
    is 'This field contains the multi-units for the item/location (zone) combination.';
  comment on column XXADEO_SP_V_ITEM_LOC.multi_unit_retail
    is 'This field holds the multi-unit retail in the multi-selling unit of measure for the item/location (zone) combination.  This field is stored in teh local currency.';
  comment on column XXADEO_SP_V_ITEM_LOC.multi_selling_uom
    is 'This field holds the selling unit of measure for this item/location (zone) combinations multi-unit retail.';
  comment on column XXADEO_SP_V_ITEM_LOC.selling_unit_retail
    is 'Contains the unit retail price in the selling unit of measure for the item/location combination. This field is stored in the local currency.';
  comment on column XXADEO_SP_V_ITEM_LOC.selling_uom
    is 'Contains the selling unit of measure for an items single-unit retail.';
  comment on column XXADEO_SP_V_ITEM_LOC.promo_retail
    is 'Contains the current promotional unit retail price in the standard unit of measure for the item/store combination.  This field will only contain a value if the item is on promotion.  This field is stored in the local currency.';
  comment on column XXADEO_SP_V_ITEM_LOC.promo_selling_retail
    is 'Contains the current promotional unit retail price in the selling unit of measure for the item/store combination.  This field will only contain a value if the item is on promotion.  This field is stored in the local currency.';
  comment on column XXADEO_SP_V_ITEM_LOC.promo_selling_uom
    is 'Contains the selling unit of measure for an items promotional single-unit retail.  This field will only contain a value if the item is on promotion.';
  comment on column XXADEO_SP_V_ITEM_LOC.clear_ind
    is 'Indicates if item is on clearance at the store.  Valid values are:    Y = Yes, the item is on clearance     N = No, the item is not on clearance';
  comment on column XXADEO_SP_V_ITEM_LOC.taxable_ind
    is 'Indicates if item is taxable at the store.  Valid values are:     Y = Yes, the item is taxable    N = No, the item is not taxable';
  comment on column XXADEO_SP_V_ITEM_LOC.local_item_desc
    is 'Contains the local description of the item. This field will default to the items description but will be over-ridable. This value will be downloaded to the POS.';
  comment on column XXADEO_SP_V_ITEM_LOC.local_short_desc
    is 'Contains the local short description of the item.  This field will default to the items short description but will be over-ridable.  This value will be downloaded to the POS.';
  comment on column XXADEO_SP_V_ITEM_LOC.ti
    is 'Number of shipping units (cases) that make up one tier of a pallet.  Multiply TI x HI to get total number of cases for a pallet.';
  comment on column XXADEO_SP_V_ITEM_LOC.hi
    is 'Number of tiers that make up a complete pallet (height).  Multiply TI x HI to get total number of cases for a pallet.';
  comment on column XXADEO_SP_V_ITEM_LOC.store_ord_mult
    is 'This column contains the multiple in which the item needs to be shipped from a warehouse to the location.';
  comment on column XXADEO_SP_V_ITEM_LOC.status
    is 'Current status of item at the store.  Valid values are:     A = Active, item is valid and can be ordered and sold     I = Inactive, item is valid but cannot be ordered or sold     C = Discontinued, item is valid and sellable but no longer orderable    D = Delete, item is invalid and cannot be ordered or sold';
  comment on column XXADEO_SP_V_ITEM_LOC.status_update_date
    is 'Date on which the status for item at the store was most recently changed.';
  comment on column XXADEO_SP_V_ITEM_LOC.daily_waste_pct
    is 'Average percentage lost from inventory on a daily basis due to natural wastage.';
  comment on column XXADEO_SP_V_ITEM_LOC.meas_of_each
    is 'Size of an each in terms of the uom_of_price.  For example 12 oz.  Used in ticketing.';
  comment on column XXADEO_SP_V_ITEM_LOC.meas_of_price
    is 'Size to be used on the ticket in terms of the uom_of_price.  For example, if the user wants the ticket to have the label print the price per ounce, this value would be 1.  If the user wanted the price per 100 grams this value would be 100.  Used in ticketing.';
  comment on column XXADEO_SP_V_ITEM_LOC.uom_of_price
    is 'Unit of measure that will be used on the ticket for this item.';
  comment on column XXADEO_SP_V_ITEM_LOC.primary_variant
    is 'This field is used to address sales of PLUs (i.e. above transaction level items) when inventory is tracked at a lower level (i.e. UPC). This field will only contain a value for items one level higher than the transaction level. Valid
  choices will be any transaction level item that is a child of this item. In order to select a transaction level item as the primary variant, the item/location relationship must exist at the transaction level.  Both the transaction level item (i.e. UPC) and the higher than transcation level item (i.e. PLU) will be sent to the POS to allow the store to sell the PLU. The information sent for the PLU will be the same information sent for the Transaction level item (i.e. UPC). ';
  comment on column XXADEO_SP_V_ITEM_LOC.primary_cost_pack
    is 'This field contains an item number that is a simple pack containing the item in the item column for this record.  If populated, the cost of the future cost table will be driven from the simple pack and the deals and cost changes for the simple pack.';
  comment on column XXADEO_SP_V_ITEM_LOC.primary_supp
    is 'Numeric identifier of the supplier who will be considered the primary supplier for the specified item/loc.  The supplier/origin country combination will determine the value of the unit cost field on XXADEO_SP_V_ITEM_LOC.  If the supplier is changed and ELC = N, the unit cost field on XXADEO_SP_V_ITEM_LOC will be updated with the new suppliers cost.';
  comment on column XXADEO_SP_V_ITEM_LOC.primary_cntry
    is 'Contains the identifier of the origin country which will be considered the primary country for the specified item/location.';
  comment on column XXADEO_SP_V_ITEM_LOC.receive_as_type
    is 'This column determines whether the stock on hand for a pack component item or the buyer pack itself will be updated when a buyer pack is received at a warehouse. Valid values are Each or Pack.';
  comment on column XXADEO_SP_V_ITEM_LOC.create_datetime
    is 'Date/time stamp of when the record was created.  This date/time will be used in export processing.  This value should only be populated on insert  - it should never be updated';
  comment on column XXADEO_SP_V_ITEM_LOC.last_update_datetime
    is 'Holds the date time stamp of the most recent update by the last_update_id.  This field is required by the database.';
  comment on column XXADEO_SP_V_ITEM_LOC.last_update_id
    is 'Holds the Oracle user-id of the user who most recently updated this record.  This field is required by the database.';
  comment on column XXADEO_SP_V_ITEM_LOC.inbound_handling_days
    is 'This field indicates the number of inbound handling days for an item at a warehouse type location.';
  comment on column XXADEO_SP_V_ITEM_LOC.source_method
    is 'This value will be used to specify how the ad-hoc PO/TSF creation process should source the item/location request. If the value is Warehouse, the process will attempt to fill the request by creating a transfer from the warehouse mentioned in the source_wh field. If this warehouse doesn''t have enough inventory to fill the request, a purchase order will be created for the item/location''s primary supplier. For warehouses, it is used by Oracle Retail Allocation to determine the valid sources and destinations for warehouse to warehouse allocations.';
  comment on column XXADEO_SP_V_ITEM_LOC.source_wh
    is 'This value will be used by the ad-hoc PO/Transfer creation process to determine which warehouse to fill the stores request from. It will also be used by the Allocation process to support warehouse to warehouse allocations. A value will be required in this field if the sourcing method is Warehouse.';
  comment on column XXADEO_SP_V_ITEM_LOC.store_price_ind
    is 'This field indicates if an item at a particular store location can have the unit retail marked down by the store.';
  comment on column XXADEO_SP_V_ITEM_LOC.rpm_ind
    is 'This column indicates whether or not RPM has processed the item/location combination.';
  comment on column XXADEO_SP_V_ITEM_LOC.uin_type
    is 'This column will contain the unique identification number (UIN) used to identify the instances of the item at the location.';
  comment on column XXADEO_SP_V_ITEM_LOC.uin_label
    is 'This column will contain the label for the UIN when displayed in SIM.';
  comment on column XXADEO_SP_V_ITEM_LOC.capture_time
    is 'This column will indicate when the UIN should be captured for an item during transaction processing.';
  comment on column XXADEO_SP_V_ITEM_LOC.ext_uin_ind
    is 'This Yes/No indicator indicates if UIN is being generated in the external system.';
  comment on column XXADEO_SP_V_ITEM_LOC.ranged_ind
    is 'This column determines if the location is ranged intentionally by the user for replenishment/selling or incidentally ranged by the RMS programs when item is not ranged to a specific location on the transaction.';
  comment on column XXADEO_SP_V_ITEM_LOC.costing_loc
    is 'Numeric identifier of the costing location for the franchise store. This field may contain a store or a warehouse.';
  comment on column XXADEO_SP_V_ITEM_LOC.costing_loc_type
    is 'This field holds the type of costing location in the costing location field.';
  comment on column XXADEO_SP_V_ITEM_LOC.create_id
    is 'This column holds the User id of the user who created the record.';
