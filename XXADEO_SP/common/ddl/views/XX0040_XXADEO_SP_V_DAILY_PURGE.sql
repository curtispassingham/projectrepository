-- Create view
create or replace view XXADEO_SP_V_DAILY_PURGE as
select key_value,
       table_name,
       delete_type,
       delete_order
  from daily_purge;

-- Add comments to the view
comment on table XXADEO_SP_V_DAILY_PURGE
  is 'This table is used to hold the table name and a key value for a record that needs to be deleted. Records are inserted into this table on-line as part of the delete logic. The actual deletes are performed in a nightly batch process by running the dlypurge.pc program.';
-- Add comments to the columns
comment on column xxadeo_sp_v_daily_purge.key_value
  is 'This column holds the key value that will be used to delete records from the given table name.  For example, if a SKU was to be deleted from win_skus, this field would hold the SKU number.  If a class is to be deleted, where both the dept and class number are needed,  the record should be inserted into this column as:  dept,class.';
comment on column xxadeo_sp_v_daily_purge.table_name
  is 'This column holds the name of the table from which the record will be deleted.';
comment on column xxadeo_sp_v_daily_purge.delete_type
  is 'The value in this column indicates what type of delete this record represents, from pressing the delete button, D, or from pressing the cancel button, C.  This field will only have a value if different logic is performed determined by which button was pressed. ';
comment on column xxadeo_sp_v_daily_purge.delete_order
  is 'This field indicates the order that the record should be processed in the nightly batch run.  All records that should be processed last should have a value of 2.  All other records will have a value of 1.';
