CREATE OR REPLACE VIEW XXADEO_SP_V_RTK_ERRORS
AS
SELECT RE.RTK_TYPE,
			 RE.RTK_KEY,
			 RE.RTK_TEXT,
			 RE.RTK_USER,
			 RE.RTK_APPROVED
	FROM XXADEO_RMS.RTK_ERRORS RE;
-- Add comments to the table
comment on table xxadeo_sp_v_rtk_errors
  is 'This table contains one row for each error message used on the client side in the system. This table is populated during installation of the system and must be maintained by the database administrator.';
-- Add comments to the columns
comment on column xxadeo_sp_v_rtk_errors.rtk_type
  is 'This column will represent the type of error that each key and message is describing.  Valid values are BL - business logic, OR - API sequencing out of order, LK - record locked, OE - oracle error.';
comment on column xxadeo_sp_v_rtk_errors.rtk_key
  is 'Contains a key that will be used to access an error message from a form.  This key is used to call the message from inside the emessage or F_YES_NO message.  Example: emessage(INV_SKU);';
comment on column xxadeo_sp_v_rtk_errors.rtk_text
  is 'Contains the actual text of the message.  This text will be written to the screen when the message box appears.';
comment on column xxadeo_sp_v_rtk_errors.rtk_user
  is 'Contains the user name or the initials of the person who created the message.';
comment on column xxadeo_sp_v_rtk_errors.rtk_approved
  is 'Indicates whether or not the message has been approved.  Valid values are: Y or N.';
