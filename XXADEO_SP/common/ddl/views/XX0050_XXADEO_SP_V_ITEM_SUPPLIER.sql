CREATE OR REPLACE VIEW XXADEO_SP_V_ITEM_SUPPLIER AS
SELECT item,
       supplier,
       primary_supp_ind,
       vpn,
       supp_label,
       consignment_rate,
       supp_diff_1,
       supp_diff_2,
       supp_diff_3,
       supp_diff_4,
       pallet_name,
       case_name,
       inner_name,
       supp_discontinue_date,
       direct_ship_ind,
       create_datetime,
       last_update_datetime,
       last_update_id,
       concession_rate,
       primary_case_size,
       create_id
  FROM ITEM_SUPPLIER;

  -- Add comments to the table
comment on table XXADEO_SP_V_ITEM_SUPPLIER
  is 'This view holds all item supplier relationships for all items.';
-- Add comments to the columns
comment on column XXADEO_SP_V_ITEM_SUPPLIER.item
  is 'Alphanumeric value that identifies the item';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.supplier
  is 'This field contains the number of the supplier of the item.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.primary_supp_ind
  is 'This field Indicates whether this supplier is the primary supplier for the item..  Each item can have one and only one primary supplier.  Valid values are Y or N.  This field is meaningless for sub-transaction level items.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.vpn
  is 'This field contains the Vendor Product Number associated with this item.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.supp_label
  is 'This field will hold the supplier laber for an item (Parent/Child)';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.consignment_rate
  is 'This field contains the consignment rate for this item for the supplier.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.supp_diff_1
  is 'This field contains the first supplier differentiator and/or description.  This field may only contain a value for items with an item_parent.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.supp_diff_2
  is 'This field contains the second supplier differentiator and/or description.  This field may only contain a value for items with an item_parent.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.supp_diff_3
  is 'This field contains the third supplier differentiator and/or description.  This field may only contain a value for items with an item_parent.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.supp_diff_4
  is 'This field contains the fourth supplier differentiator and/or description.  This field may only contain a value for items with an item_parent.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.pallet_name
  is 'Code referencing the name used by supplier to refer to the pallet.  Valid codes are defined in the PALN code type.  Examples are flat, pallet.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.case_name
  is 'Code referencing the name used by supplier to refer to the case.  Valid codes are defined in the CASN code type.  Examples are pack, box, bag.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.inner_name
  is 'Code referencing the name used by supplier to refer to the inner.  Valid codes are defined in the INRN code type.  Examples are sub-case, sub-pack.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.supp_discontinue_date
  is 'Date which the supplier discontinues an item.  The retailor should be aware that the supplier is able to reuse a UPC after 30 months and should work to ensure that no data exists in RMS for a UPC 30 months after it has been discontinued.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.direct_ship_ind
  is 'This field will  contain a value of Yes to indicate that any item asssociated with this supplier is eligible for a direct shipment from the supplier to the customer.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.create_datetime
  is 'Date/time stamp of when the record was created.  This date/time will be used in export processing.  This value should only be populated on insert  - it should never be updated.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.last_update_datetime
  is 'Holds the date time stamp of the most recent update by the last_update_id.  This field is required by the database.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.last_update_id
  is 'Holds the Oracle user-id of the user who most recently updated this record.  This field is required by the database.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.concession_rate
  is 'The concession rate is the margin that a particular supplier receives for the sale of a concession item.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.primary_case_size
  is 'Used only if AIP is interfaced. Indicates the primary case size for the item supplier when an orderable item is configured for informal case types.';
comment on column XXADEO_SP_V_ITEM_SUPPLIER.create_id
  is 'This column holds the User id of the user who created the record.';
