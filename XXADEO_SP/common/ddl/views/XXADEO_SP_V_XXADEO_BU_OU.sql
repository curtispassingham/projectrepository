--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR249
--View to access RMS data from Store Portal

GRANT SELECT ON XXADEO_RMS.XXADEO_BU_OU TO XXADEO_SP
/
begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_XXADEO_BU_OU cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_XXADEO_BU_OU (BU, 
                                                           OU, 
                                                           DESCRIPTION, 
                                                           LAST_UPDATE_ID, 
                                                           LAST_UPDATE_DATETIME
                                                          ) AS 
SELECT BU, 
       OU, 
       DESCRIPTION, 
       LAST_UPDATE_ID , 
       LAST_UPDATE_DATETIME 
  FROM XXADEO_RMS.XXADEO_BU_OU
/

COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_XXADEO_BU_OU.BU                   IS 'ADEO Business Unit (If the record is BU specific identifies the BU attached to it). The code used corresponds to the one present in the Organization Hierarchy � Level Area';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_XXADEO_BU_OU.OU                   IS 'Organization Unit ID';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_XXADEO_BU_OU.DESCRIPTION          IS 'Description of the purposes of the record being Cross Referenced';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_XXADEO_BU_OU.LAST_UPDATE_ID       IS 'User that last updated the record.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_XXADEO_BU_OU.LAST_UPDATE_DATETIME IS 'Date when the record was last updated';
