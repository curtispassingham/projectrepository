
/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Sérgio Barbosa                                               */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - VIEW "XXADEO_SP_V_ASSORTMENT"                            */
/******************************************************************************/
create or replace view XXADEO_SP_V_ASSORTMENT as
select item,
       store,
       start_date,
       end_date,
       validated_date,
       response_date,
       assortment_mode,
       cluster_size,
       cs_recommendation,
       hq_recommendation,
       curr_val_assrt,
       store_choice,
       not_mod_ind,
       status,
       history_flag,
       cluster_popup_flag,
       week,
       user_action,
       comments,
       create_id,
       create_datetime,
       last_update_id,
       last_update_datetime
  from xxadeo_assortment;
-- Add comments to the columns
comment on column XXADEO_SP_V_ASSORTMENT.item
  is 'Item Unique Identifier. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT.store
  is 'Store Unique Identifier. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT.start_date
  is 'Date on which the recommended assortment becomes effective. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT.end_date
  is 'Date on which the recommended assortment is no longer valid. Calculated as the next start date minus one day or null if no other start date exists.';
comment on column XXADEO_SP_V_ASSORTMENT.validated_date
  is 'Date which the user has made the assortment validation';
comment on column XXADEO_SP_V_ASSORTMENT.response_date
  is 'Date limit which a user will have to act on the assortment from the Store perspective. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT.assortment_mode
  is 'Indicates if the assortment if national, local or test. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT.cluster_size
  is 'Contains different values of size cluster.';
comment on column XXADEO_SP_V_ASSORTMENT.cs_recommendation
  is 'Recommended assortment for the size cluster.';
comment on column XXADEO_SP_V_ASSORTMENT.hq_recommendation
  is 'Recommended assortment for the item. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT.curr_val_assrt
  is 'This column is calculated by the Store Portal with the value Store Choice when the start date is reached.';
comment on column XXADEO_SP_V_ASSORTMENT.store_choice
  is 'The assortment chosen by the Store User.';
comment on column XXADEO_SP_V_ASSORTMENT.not_mod_ind
  is 'Indicator which prevents store to change the assortment.Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT.status
  is 'Assortment change status in CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT.history_flag
  is 'Flag that indicates if the record as history records.';
comment on column XXADEO_SP_V_ASSORTMENT.cluster_popup_flag
  is 'Flag that indicates in the record has valid options for the Cluster Pop Up.';
comment on column XXADEO_SP_V_ASSORTMENT.week
  is 'Week Unique Identifier. Sent from CATMAN.';
comment on column XXADEO_SP_V_ASSORTMENT.user_action
  is 'Determine if the change has been made by user or system.';
comment on column XXADEO_SP_V_ASSORTMENT.comments
  is 'Comments of the CATMAN user.';
comment on column XXADEO_SP_V_ASSORTMENT.create_id
  is 'User that created the record.';
comment on column XXADEO_SP_V_ASSORTMENT.create_datetime
  is 'Date in which the record was created.';
comment on column XXADEO_SP_V_ASSORTMENT.last_update_id
  is 'User that last updated the record.';
comment on column XXADEO_SP_V_ASSORTMENT.last_update_datetime
  is 'Date in which the record was last updated.';
