CREATE OR REPLACE VIEW XXADEO_SP_V_CODE_DETAIL AS
SELECT CODE_TYPE,
       CODE,
       CODE_DESC,
       REQUIRED_IND,
       CODE_SEQ
FROM V_CODE_DETAIL_TL;
comment on table XXADEO_SP_V_CODE_DETAIL is 'This is the translation view for base table CODE_DETAIL. This view fetches data in user langauge either from translation table CODE_DETAIL_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.';
comment on column XXADEO_SP_V_CODE_DETAIL.CODE_TYPE is 'Contains the valid code type for the row. Valid values are defined in the table code_head';
comment on column XXADEO_SP_V_CODE_DETAIL.CODE is 'Contains the code for the row.';
comment on column XXADEO_SP_V_CODE_DETAIL.CODE_DESC is 'This field contains the description associated with the code and code type.';
comment on column XXADEO_SP_V_CODE_DETAIL.REQUIRED_IND is 'This field indicates whether or not the code is required.';
comment on column XXADEO_SP_V_CODE_DETAIL.CODE_SEQ is 'This is a number used to order the elements so that they appear consistently when using them to populate a list.';
