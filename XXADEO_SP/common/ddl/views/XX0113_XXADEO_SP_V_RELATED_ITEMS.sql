CREATE OR REPLACE VIEW XXADEO_SP_V_RELATED_ITEMS AS
SELECT  RI.related_item,
        (SELECT IM.ITEM_DESC FROM XXADEO_SP_V_ITEM_MASTER IM WHERE IM.ITEM = RI.related_item) ITEM_DESC,
        IM.ITEM,
        (select cfa_value
            from (select cfa_value, cfa_attribute, cfa.attrib_id ATTR_ID
                    from cfa_attrib cfa,
                        (select group_id, cfa_attribute, cfa_value
                            from XXADEO_SP_V_ITEM_CFA_EXT unpivot(cfa_value for cfa_attribute
                                in(number_11,number_12,number_13,number_14,number_15,number_16,number_17,number_18,number_19,number_20))
                            ) cfa_unc
                     where cfa.group_id                = cfa_unc.group_id
                       and upper(cfa.storage_col_name) = upper(cfa_unc.cfa_attribute)) aux, XXADEO_SP_V_MOM_DVM x
            where (x.value_1) = aux.attr_id
              and func_area   = 'CFA'
              and parameter   = 'CONVERSION_FACTOR') AS CONVERSION_FACTOR
FROM    XXADEO_SP_V_ITEM_MASTER IM,
        XXADEO_SP_V_RELATED_ITEM RI
WHERE   IM.ITEM = RI.ITEM;
