--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS5_AG1 cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS5_AG1 (ITEM, 
                                                            LMIT_HYPER_100_ITEM, 
                                                            LMIT_PRICE_POLICY_ITEM, 
                                                            LMIT_PURCHASE_BLK_ITEM, 
                                                            LMIT_PV_PUBLICATION_ITEM, 
                                                            LMIT_RANKING_ITEM, 
                                                            LMIT_RTL_BLK_ITEM
                                                           ) AS 
SELECT ITEM,   
       LMIT_HYPER_100_ITEM,
       LMIT_PRICE_POLICY_ITEM,
       LMIT_PURCHASE_BLK_ITEM,
       LMIT_PV_PUBLICATION_ITEM,
       LMIT_RANKING_ITEM,
       LMIT_RTL_BLK_ITEM 
  FROM (SELECT ITEM,
               VARCHAR2_2 LMIT_HYPER_100_ITEM,
               VARCHAR2_3 LMIT_PRICE_POLICY_ITEM,
               VARCHAR2_4 LMIT_PURCHASE_BLK_ITEM,
               VARCHAR2_5 LMIT_PV_PUBLICATION_ITEM,
               NUMBER_11  LMIT_RANKING_ITEM,
               VARCHAR2_1 LMIT_RTL_BLK_ITEM 
          FROM RMS.ITEM_MASTER_CFA_EXT 
         WHERE group_id = 23
        )
/
