/******************************************************************************/
/* CREATE DATE - August 2018                                                  */
/* CREATE USER - Sérgio Barbosa                                               */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_SP_V_UDA_ITEM_LOV"                              */
/******************************************************************************/
CREATE OR REPLACE VIEW XXADEO_SP_V_UDA_ITEM_LOV AS
SELECT "ITEM",
       "UDA_ID",
       "UDA_VALUE",
       "CREATE_DATETIME",
       "LAST_UPDATE_DATETIME",
       "LAST_UPDATE_ID",
       "CREATE_ID"
  FROM UDA_ITEM_LOV;
