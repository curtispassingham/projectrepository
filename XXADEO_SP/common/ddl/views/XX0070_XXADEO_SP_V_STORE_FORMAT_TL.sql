/******************************************************************************/
/* CREATE DATE - July 2018                                                    */
/* CREATE USER - Felipe Villa                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - View "XXADEO_SP_V_STORE_FORMAT_TL"                           */
/******************************************************************************/
CREATE OR REPLACE VIEW XXADEO_SP_V_STORE_FORMAT_TL AS
SELECT STORE_FORMAT,
       FORMAT_NAME,
       LANG
FROM V_STORE_FORMAT_TL;
comment on column XXADEO_SP_V_STORE_FORMAT_TL.STORE_FORMAT is 'Contains the number which uniquely identifies the store format.';
comment on column XXADEO_SP_V_STORE_FORMAT_TL.FORMAT_NAME is 'Contains the name or description of the store format.';
comment on column XXADEO_SP_V_STORE_FORMAT_TL.LANG is 'This column contains the language for this record.';
