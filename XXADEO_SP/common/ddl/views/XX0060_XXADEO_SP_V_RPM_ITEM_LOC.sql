create or replace view XXADEO_SP_V_RPM_ITEM_LOC as
select   dept,
         item,
         loc
from rpm_item_loc;

-- Add comments to the table
comment on table XXADEO_SP_V_RPM_ITEM_LOC
  is 'this table will hold a single record for all item/location relationships in rpm.  all items will be transaction level items only.';
-- Add comments to the columns
comment on column XXADEO_SP_V_RPM_ITEM_LOC.DEPT
  is 'the department of the item.';
comment on column XXADEO_SP_V_RPM_ITEM_LOC.ITEM
  is 'the item id';
comment on column XXADEO_SP_V_RPM_ITEM_LOC.LOC
  is 'the location id';
