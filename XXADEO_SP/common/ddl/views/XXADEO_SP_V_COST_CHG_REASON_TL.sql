--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_COST_CHG_REASON_TL cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_COST_CHG_REASON_TL (REASON, 
                                                                 LANG, 
                                                                 REASON_DESC, 
                                                                 ORIG_LANG_IND, 
                                                                 REVIEWED_IND, 
                                                                 CREATE_ID, 
                                                                 CREATE_DATETIME, 
                                                                 LAST_UPDATE_ID, 
                                                                 LAST_UPDATE_DATETIME
                                                                ) AS 
SELECT REASON, 
       LANG, 
       REASON_DESC, 
       ORIG_LANG_IND, 
       REVIEWED_IND, 
       CREATE_ID, 
       CREATE_DATETIME, 
       LAST_UPDATE_ID, 
       LAST_UPDATE_DATETIME
  FROM RMS.COST_CHG_REASON_TL
/
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_COST_CHG_REASON_TL.REASON               IS 'Contains the number which uniquely identifies the reason for the cost change.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_COST_CHG_REASON_TL.LANG                 IS 'Contains the number which uniquely identifies a language.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_COST_CHG_REASON_TL.REASON_DESC          IS 'Holds the description of the cost change reason code in a given language.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_COST_CHG_REASON_TL.ORIG_LANG_IND        IS 'Indicates if the description is in the original language entered for the cost change reason code. It is set to ''Y'' when the first record is written to the table for the cost change reason code.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_COST_CHG_REASON_TL.REVIEWED_IND         IS 'Indicates if the description needs to be reviewed for translation. It is set to ''N'' when the description in the original language is inserted or updated. We assume that clients will regularly run reports on all strings that are not reviewed (i.e. reviewed_ind = ''N''). When translation either provides a new string, or OKs that the existing string is correct, the reviewed_ind should be set to ''Y''.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_COST_CHG_REASON_TL.CREATE_ID            IS 'This field contains the user that created the record.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_COST_CHG_REASON_TL.CREATE_DATETIME      IS 'This field contains the timestamp when the record is created.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_COST_CHG_REASON_TL.LAST_UPDATE_ID       IS 'This field contains the user that last updated the record.';
COMMENT ON COLUMN XXADEO_SP.XXADEO_SP_V_COST_CHG_REASON_TL.LAST_UPDATE_DATETIME IS 'This field contains the timestamp when the record is last updated.';
