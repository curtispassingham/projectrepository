/******************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Pedro Vieira                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_SP_V_CLUSTERS" view of 	 		             		  */
/*					XXADEO_CLUSTERS table				        	          									*/
/******************************************************************************/

CREATE OR REPLACE VIEW XXADEO_SP_V_CLUSTERS AS
SELECT CLUSTER_ID
	,CLUSTER_LABEL
	,CLUSTER_TYPE_ID
	,CLUSTER_TYPE_LABEL
	,BU_ID
	,BU_LABEL
	,CREATE_ID
	,CREATE_DATETIME
FROM XXADEO_CLUSTERS;

-- Add comments to the columns
COMMENT ON COLUMN XXADEO_SP_V_CLUSTERS.BU_ID is 'BU Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_CLUSTERS.BU_LABEL is 'BU Name. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_CLUSTERS.CLUSTER_TYPE_ID is 'Cluster Type Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_CLUSTERS.CLUSTER_TYPE_LABEL is 'Cluster Type Name. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_CLUSTERS.CLUSTER_ID is 'Cluster Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_CLUSTERS.CLUSTER_LABEL is 'Cluster Name. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_SP_V_CLUSTERS.CREATE_ID is 'User that created the record.'
;
COMMENT ON COLUMN XXADEO_SP_V_CLUSTERS.CREATE_DATETIME is 'Date in which the record was created.'
;
