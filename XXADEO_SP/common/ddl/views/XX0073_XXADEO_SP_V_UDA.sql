CREATE OR REPLACE VIEW XXADEO_SP_V_UDA AS
SELECT UDA.UDA_ID UDA_ID
      ,UDA.UDA_DESC UDA_DESC
      ,UDA.DISPLAY_TYPE DISPLAY_TYPE
      ,UDA.FILTER_MERCH_ID FILTER_MERCH_ID
      ,UDA.FILTER_ORG_ID FILTER_ORG_ID
      ,UDA.SINGLE_VALUE_IND SINGLE_VALUE_IND
      ,UDA.FILTER_MERCH_ID_CLASS FILTER_MERCH_ID_CLASS
      ,UDA.FILTER_MERCH_ID_SUBCLASS FILTER_MERCH_ID_SUBCLASS
FROM V_UDA UDA;

-- Add comments to the table
comment on table XXADEO_SP_V_UDA is 'This view will be used to display the UDA LOVs using a security policy to filter User access.';
-- Add comments to the columns
comment on column XXADEO_SP_V_UDA.uda_id
  is 'This field contains a unique number identifying the User Defined Attribute.';
comment on column XXADEO_SP_V_UDA.uda_desc
  is 'This field contains a description of the User-Defined Attribute.';
comment on column XXADEO_SP_V_UDA.display_type
  is 'This field is used to store the display type (how the UDA values will be displayed to the user) for the given UDA. The valid values are DT-Date, FF - Free From, LV - List of values';
comment on column XXADEO_SP_V_UDA.single_value_ind
  is 'This field indicates whether or not the UDA should be constrained to having at most one value.';
comment on column XXADEO_SP_V_UDA.filter_org_id
  is 'This field contains the ID of the Organizational Hierarchy that the UDA is assigned to. This field will be used to control the UDAs a user can see in the UDA LOV when the link between the user and the Organizational Hierarchy has been established.  The level of the organizational hierarchy held in this field is based on the UDA_ORG_LEVEL_CODE on the SECURITY_CONFIG_OPTIONS table.';
comment on column XXADEO_SP_V_UDA.filter_merch_id_class
  is 'This field contains the class ID of the Merchandise Hierarchy that the UDA is assigned to.';
comment on column XXADEO_SP_V_UDA.filter_merch_id_subclass
  is 'This field contains the subclass ID of the Merchandise Hierarchy that the UDA is assigned to.';
