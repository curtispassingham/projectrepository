--Create date : 2018-11-22
--Author : Eric Levy
--Use by report RR432
--View to access RMS data from Store Portal

begin
 execute immediate 'drop view XXADEO_SP.XXADEO_SP_V_ITEM_AGS3_AG1 cascade constraints';
exception
 when others then
   null;
end;
/
CREATE OR REPLACE VIEW XXADEO_SP.XXADEO_SP_V_ITEM_AGS3_AG1 (ITEM, 
                                                            LMES_HYPER_100_ITEM, 
                                                            LMES_PRICE_POLICY_ITEM, 
                                                            LMES_PURCHASE_BLK_ITEM, 
                                                            LMES_PV_PUBLICATION_ITEM, 
                                                            LMES_RANKING_ITEM, 
                                                            LMES_RTL_BLK_ITEM
                                                           ) AS 
SELECT ITEM,   
       LMES_HYPER_100_ITEM,
       LMES_PRICE_POLICY_ITEM,
       LMES_PURCHASE_BLK_ITEM,
       LMES_PV_PUBLICATION_ITEM,
       LMES_RANKING_ITEM,
       LMES_RTL_BLK_ITEM 
  FROM (SELECT ITEM,
               VARCHAR2_2 LMES_HYPER_100_ITEM,
               VARCHAR2_3 LMES_PRICE_POLICY_ITEM,
               VARCHAR2_4 LMES_PURCHASE_BLK_ITEM,
               VARCHAR2_5 LMES_PV_PUBLICATION_ITEM,
               NUMBER_11  LMES_RANKING_ITEM,
               VARCHAR2_1 LMES_RTL_BLK_ITEM 
          FROM RMS.ITEM_MASTER_CFA_EXT 
         WHERE group_id = 15
        )
/
