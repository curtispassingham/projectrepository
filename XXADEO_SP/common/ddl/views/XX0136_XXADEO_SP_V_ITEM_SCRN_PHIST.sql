create or replace view xxadeo_sp_v_item_scrn_phist as
select item        item,
       loc         loc,
       unit_retail retail_price,
       action_date start_date_retail,
       null        start_date_promo,
       null        end_date_promo,
       null        promo_price,
       event       promo_orig,
       create_id   create_id
  from xxadeo_sp_v_price_hist
 where tran_type in (0, 4)
union all
select item        item,
       loc         loc,
       null        retail_price,
       null        start_date_retail,
       action_date start_date_promo,
       null        end_date_promo,
       unit_retail promo_price,
       null        promo_orig,
       create_id   user_id
  from xxadeo_sp_v_price_hist
 where tran_type in (9);
