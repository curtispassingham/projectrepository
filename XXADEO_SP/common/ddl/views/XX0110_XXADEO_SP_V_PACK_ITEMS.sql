CREATE OR REPLACE VIEW XXADEO_SP_V_PACKITEM AS
SELECT  pack_no             ,
        seq_no              ,
        item                ,
        item_parent         ,
        pack_tmpl_id        ,
        pack_qty            ,
        create_datetime     ,
        last_update_datetime,
        last_update_id
  FROM PACKITEM;
comment on table XXADEO_SP_V_PACKITEM is 'This view contains simple pack and complex packs created in RMS along with the component items of the packs.';
comment on column XXADEO_SP_V_PACKITEM.PACK_NO is 'Alphanumeric value that uniquely identifies the pack for which details are held in this table.';
comment on column XXADEO_SP_V_PACKITEM.SEQ_NO is 'Contains a sequence number used to uniquely identify a row in the PACKITEM table.';
comment on column XXADEO_SP_V_PACKITEM.ITEM is 'Alphanumeric value that identifies the component item within the pack. If pack item is created using pack template then the component items are stored in the PACKITEM_BREAKOUT table and this field is null.';
comment on column XXADEO_SP_V_PACKITEM.ITEM_PARENT is 'This field contains the parent item (if any) associated  with the component item of the pack.';
comment on column XXADEO_SP_V_PACKITEM.PACK_TMPL_ID is 'Contains the pack template ID associated with the pack item.';
comment on column XXADEO_SP_V_PACKITEM.PACK_QTY is 'Contains the quantity of component items within the pack. If the pack item is created using a pack template then the quantity specified here is 1 and the actual component item quantities in the pack are stored in PACKITEM_BREAKOUT table.';
comment on column XXADEO_SP_V_PACKITEM.CREATE_DATETIME is 'Date/time stamp of when the record was created.  This date/time will be used in export processing.  This value should only be populated on insert and it should never be updated.';
comment on column XXADEO_SP_V_PACKITEM.LAST_UPDATE_DATETIME is 'Holds the date time stamp of the most recent update by the last_update_id.';
comment on column XXADEO_SP_V_PACKITEM.LAST_UPDATE_ID is 'Holds the Oracle user-id of the user who most recently updated this record.';
