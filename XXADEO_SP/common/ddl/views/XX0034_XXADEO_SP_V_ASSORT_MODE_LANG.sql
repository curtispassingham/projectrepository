CREATE OR REPLACE VIEW XXADEO_SP_V_ASSORT_MODE_LANG AS
select lang,
       seq_id,
       assortment_mode_id,
       assortment_mode_label,
       create_id,
       create_datetime,
       last_update_id,
       last_update_datetime
from   (select lang,
               seq_id,
               assortment_mode_id,
               assortment_mode_label,
               create_id,
               create_datetime,
               last_update_id,
               last_update_datetime,
               case
                 when language_sql.get_user_language() >= get_primary_lang() then
                  rank()
                  over(partition by assortment_mode_id order by lang desc)
                 else
                  rank()
                  over(partition by assortment_mode_id order by lang asc)
               end as lang_rank
        from   XXADEO_SP_V_ASSORTMENT_MODE_TL t
        where  t.lang in
               (language_sql.get_user_language(), get_primary_lang()))
where  lang_rank = 1;

comment on column xxadeo_sp_v_assort_mode_lang.lang is 'This field contains the language in which the translated text is maintained.';
comment on column xxadeo_sp_v_assort_mode_lang.seq_id is 'This field contains rownum for auxiliary ADF multi-select field.';
comment on column xxadeo_sp_v_assort_mode_lang.assortment_mode_id is 'Assortmentb Mode Unique Identifier. Sent from CATMAN.';
comment on column xxadeo_sp_v_assort_mode_lang.assortment_mode_label is 'Assortment mode lablel translated into the different languages. Sent from CATMAN.';
comment on column xxadeo_sp_v_assort_mode_lang.create_id is 'User that created the record.';
comment on column xxadeo_sp_v_assort_mode_lang.create_datetime is 'Date in which the record was created.';
comment on column xxadeo_sp_v_assort_mode_lang.last_update_id is 'User that last updated the record.';
comment on column xxadeo_sp_v_assort_mode_lang.last_update_datetime is 'Date in which the record was last updated.';
