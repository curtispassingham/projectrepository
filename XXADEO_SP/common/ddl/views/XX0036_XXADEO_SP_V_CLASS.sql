CREATE OR REPLACE VIEW XXADEO_SP_V_CLASS AS
SELECT "DEPT",
	   "CLASS",
	   "CLASS_NAME"
  FROM V_CLASS;
comment on table XXADEO_SP_V_CLASS is 'This vew will be used to display the Class LOVs using a security policy to filter User access. The class name is translated based on user language.';
comment on column XXADEO_SP_V_CLASS.DEPT is 'Contains the number of the department of which the class is a member.';
comment on column XXADEO_SP_V_CLASS.CLASS is 'Contains the number which uniquely identifies the class within the system.';
