-- Create view
create or replace view XXADEO_SP_V_RPM_IZP as
select item_zone_price_id,
       item,
       zone_id,
       standard_retail,
       standard_retail_currency,
       standard_uom,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       lock_version
  from rpm_item_zone_price;
-- Add comments to the table
comment on table XXADEO_SP_V_RPM_IZP
  is 'This view will store the retail information for an item/zone where the zone is within the primary zone group of the item. The information in this table will be updated by executed price changes, and will also be used to determine what zone retails to assign to new child items.';
-- Add comments to the columns
comment on column XXADEO_SP_V_RPM_IZP.item_zone_price_id
  is 'id that uniquely identifies the rpm_item_zone_price record.';
comment on column XXADEO_SP_V_RPM_IZP.item
  is 'id that uniquely identifies the item.';
comment on column XXADEO_SP_V_RPM_IZP.zone_id
  is 'id that uniquely identifies the zone on the price record.';
comment on column XXADEO_SP_V_RPM_IZP.standard_retail
  is 'the zone level retail in terms of the item/zone?s standard unit of measure.';
comment on column XXADEO_SP_V_RPM_IZP.standard_retail_currency
  is 'the currency associated with the zone.  the value in the standard_retail field is conveyed in this currency.';
comment on column XXADEO_SP_V_RPM_IZP.standard_uom
  is 'this field holds the unit of measure for the item?s standard retail.';
comment on column XXADEO_SP_V_RPM_IZP.selling_retail
  is 'the zone level retail in terms of the item/zone?s selling unit of measure.';
comment on column XXADEO_SP_V_RPM_IZP.selling_retail_currency
  is 'the currency associated with the zone.  the value in the selling_retail field is conveyed in this currency.';
comment on column XXADEO_SP_V_RPM_IZP.selling_uom
  is 'this field holds the unit of measure for the item?s selling retail.';
comment on column XXADEO_SP_V_RPM_IZP.multi_units
  is 'this field contains the number of units associated with the multi-unit retail for the item/zone combination.';
comment on column XXADEO_SP_V_RPM_IZP.multi_unit_retail
  is 'this field holds the multi-unit retail in terms of the multi-unit selling unit of measure for the item/zone combination.';
comment on column XXADEO_SP_V_RPM_IZP.multi_unit_retail_currency
  is 'the currency associated with the zone.  the value in the multi_unit_retail field is conveyed in this currency.';
comment on column XXADEO_SP_V_RPM_IZP.multi_selling_uom
  is 'this field holds the selling unit of measure for an item?s multi-unit retail';
comment on column XXADEO_SP_V_RPM_IZP.lock_version
  is 'field to help with optimistic locking.';
