-- Create table
create or replace view XXADEO_SP_V_RELATED_ITEM as
SELECT   H.RELATIONSHIP_ID   ,
         H.ITEM              ,
         D.RELATED_ITEM      ,
         D.PRIORITY          ,
         D.START_DATE        ,
         D.END_DATE          ,
         H.MANDATORY_IND
FROM RELATED_ITEM_HEAD H,
     RELATED_ITEM_DETAIL D
WHERE H.RELATIONSHIP_ID = D.RELATIONSHIP_ID;
-- Add comments to the table
comment on table XXADEO_SP_V_RELATED_ITEM
  is 'This is the header table for related items functionality. This table will contain one row for each item and relationship type (Cross Sell, Sub Sell, Substitute etc). E.g. if one needs to setup item2 and item3 as substitutes for item1, then related_item_head table will contain one row with item1  and relationship-type as substitute. The related_item_detail table will have item2 and item3 (joined using relationship_id foreign key).';
-- Add comments to the columns
comment on column XXADEO_SP_V_RELATED_ITEM.RELATIONSHIP_ID
  is 'Unique identifier for each relationship header.';
comment on column XXADEO_SP_V_RELATED_ITEM.ITEM
  is 'Item for which the relationships are defined.';
comment on column XXADEO_SP_V_RELATED_ITEM.related_item
  is 'Item id of the related item.';
  comment on column XXADEO_SP_V_RELATED_ITEM.priority
  is 'Applicable only in case of relationship type SUBS. In case of multiple related substitute items, this column could be used (optional) to define relative priority.';
  comment on column XXADEO_SP_V_RELATED_ITEM.start_date
  is 'From this date related item can be used on transactions.';
comment on column XXADEO_SP_V_RELATED_ITEM.end_date
  is 'Till this date related item can be used on transactions. A value of null means that it is effective forever.';
comment on column XXADEO_SP_V_RELATED_ITEM.MANDATORY_IND
  is 'Indicates whether the relationship is mandatory.';
