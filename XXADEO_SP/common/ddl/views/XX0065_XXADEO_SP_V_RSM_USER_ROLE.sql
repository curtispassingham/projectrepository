CREATE OR REPLACE VIEW XXADEO_SP_V_RSM_USER_ROLE AS 
SELECT RUR.ID,
       RUR.USER_ID,
       RUR.ROLE_ID,
       RUR.START_DATE_TIME,
       RUR.END_DATE_TIME
  FROM RSM_USER_ROLE RUR;
  
COMMENT ON TABLE XXADEO_SP_V_RSM_USER_ROLE  IS 'the association of specific roles to users within the platform application system.';
COMMENT ON COLUMN XXADEO_SP_V_RSM_USER_ROLE.ID IS 'the primary key for user_role.';
COMMENT ON COLUMN XXADEO_SP_V_RSM_USER_ROLE.USER_ID IS 'the internal or external identifier of the user being granted the associated role.';
COMMENT ON COLUMN XXADEO_SP_V_RSM_USER_ROLE.ROLE_ID IS 'an internal identifier representing the role being assigned to the user.';
COMMENT ON COLUMN XXADEO_SP_V_RSM_USER_ROLE.START_DATE_TIME IS 'the date and time that this user/role relationship becomes active.';
COMMENT ON COLUMN XXADEO_SP_V_RSM_USER_ROLE.END_DATE_TIME IS 'the date and time that this user/role relationship becomes inactive';