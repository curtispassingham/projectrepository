create or replace view xxadeo_sp_v_item_scrn_gnrl as
WITH UDA AS
    (select parameter, bu, to_number(value_1) value_1
       from xxadeo_sp_v_mom_dvm dvm,
            xxadeo_sp_v_uda uda
      where uda.uda_id = dvm.value_1
     and func_area = 'UDA')
        select  im.item,
                bu.bu,
                loc.loc,
                im.item_desc,
                im.orderable_ind,
                im.sellable_ind,
                im.pack_ind,
                im.inventory_ind,
                loc.store_ord_mult,
                im.package_size,
                im.package_uom,
                grp.group_no,
                grp.group_name,
                dpt.dept,
                dpt.dept_name,
                cls.class,
                cls.class_name,
                scls.subclass,
                scls.sub_name,
                bu.omnichannel_start_date,
                bu.omnichannel_end_date,
                NVL(bu.range_letter,'-') range_letter,
                bu.grading,
                NVL(ast.hq_recommendation,'-') hq_recommendation,
                ast.not_mod_ind,
                ast.assortment_mode,
                NVL(ast.store_choice,'-') store_choice,
                isp.vpn,
                (select im1.item
                   from xxadeo_sp_v_item_master im1
                  where nvl(im1.item_grandparent, im1.item_parent) = isp.item
                    and im1.item_level > im1.tran_level
                    and im1.create_datetime = (select max(im1.create_datetime)
                                                 from xxadeo_sp_v_item_master im2
                                                where im1.item = im2.item
                                                  and nvl(im1.item_grandparent, im1.item_parent) = isp.item) and rownum = 1) gtin,
                loc.selling_uom,
                loc.unit_retail,
                img.image_addr,
                (select udaval.uda_value_desc
                   from (select item, uda_value, uda_id
                           from xxadeo_sp_v_uda_item_lov uiv, uda
                          where uiv.uda_id = value_1
                            and uda.parameter = 'PRODUCT_BRAND_ITEM'
                            and item = im.item) uil2, xxadeo_sp_v_uda_values udaval
                  where udaval.uda_id = uil2.uda_id and udaval.uda_value = uil2.uda_value ) brand,
                (select udaval.uda_value_desc
                   from (select item, uda_value, uda_id
                           from xxadeo_sp_v_uda_item_lov uiv, uda
                          where uiv.uda_id = value_1
                            and uda.parameter = 'TOP_1000'
                            and item = im.item) uil3, xxadeo_sp_v_uda_values udaval
                            where udaval.uda_id = uil3.uda_id and udaval.uda_value = uil3.uda_value ) top1000,
                (select udaval.uda_value_desc
                   from (select item, uda_value, uda_id
                           from xxadeo_sp_v_uda_item_lov uiv, uda
                          where uiv.uda_id = value_1
                            and uda.parameter = 'ITEM_TYPE'
                            and item = im.item) uil4, xxadeo_sp_v_uda_values udaval
                  where udaval.uda_id = uil4.uda_id and udaval.uda_value = uil4.uda_value ) typo,
                (select udaval.uda_value_desc
                   from (select item, uda_value, uda_id
                           from xxadeo_sp_v_uda_item_lov uiv, uda
                          where uiv.uda_id = value_1
                            and uda.parameter = 'ITEM_SUBTYPE'
                            and item = im.item) uil5, xxadeo_sp_v_uda_values udaval
                  where udaval.uda_id = uil5.uda_id and udaval.uda_value = uil5.uda_value ) subtypo,
                  (select cd.code_desc
                    from (select cfa_value,
                                 cfa_attribute,
                                 cfa.attrib_id attr_id,
                                 item,
                                 code_type
                            from xxadeo_sp_v_cfa_attrib cfa,
                                 (select group_id, cfa_attribute, cfa_value, item
                                    from xxadeo_sp_v_item_cfa_ext imcfa unpivot(cfa_value for cfa_attribute in(VARCHAR2_1,
                                                                                                                    VARCHAR2_2,
                                                                                                                    VARCHAR2_3,
                                                                                                                    VARCHAR2_4,
                                                                                                                    VARCHAR2_5,
                                                                                                                    VARCHAR2_6,
                                                                                                                    VARCHAR2_7,
                                                                                                                    VARCHAR2_8,
                                                                                                                    VARCHAR2_9,
                                                                                                                    VARCHAR2_10))) cfa_unc
                           where cfa.group_id = cfa_unc.group_id
                             and upper(cfa.storage_col_name) = upper(cfa_unc.cfa_attribute)) aux,
                         xxadeo_sp_v_mom_dvm      dvm,
                         xxadeo_sp_v_code_detail  cd
                   where (dvm.value_1)            = aux.attr_id
                     and dvm.func_area                = 'CFA'
                     and dvm.parameter                = 'PRICE_POLICY_ITEM'
                     and dvm.bu                   = st.area
                     and aux.code_type            = cd.code_type
                     and aux.cfa_value            = cd.code
                     and aux.item                 = im.item) cross_mkt,
                  (select cd.code_desc
                    from (select cfa_value,
                                 cfa_attribute,
                                 cfa.attrib_id attr_id,
                                 item,
                                 code_type
                            from xxadeo_sp_v_cfa_attrib cfa,
                                 (select group_id, cfa_attribute, cfa_value, item
                                    from xxadeo_sp_v_item_cfa_ext imcfa unpivot(cfa_value for cfa_attribute in(VARCHAR2_1,
                                                                                                                    VARCHAR2_2,
                                                                                                                    VARCHAR2_3,
                                                                                                                    VARCHAR2_4,
                                                                                                                    VARCHAR2_5,
                                                                                                                    VARCHAR2_6,
                                                                                                                    VARCHAR2_7,
                                                                                                                    VARCHAR2_8,
                                                                                                                    VARCHAR2_9,
                                                                                                                    VARCHAR2_10))) cfa_unc
                           where cfa.group_id = cfa_unc.group_id
                             and upper(cfa.storage_col_name) = upper(cfa_unc.cfa_attribute)) aux,
                         xxadeo_sp_v_mom_dvm      dvm,
                         xxadeo_sp_v_code_detail  cd
                   where (dvm.value_1)            = aux.attr_id
                     and dvm.func_area                = 'CFA'
                     and dvm.parameter                = 'PRICE_POLICY_ITEM'
                     and dvm.bu                   = st.area
                     and aux.code_type            = cd.code_type
                     and aux.cfa_value            = cd.code
                     and aux.item                 = im.item) price_policy,
                  (select cd.code_desc
                    from (select cfa_value,
                                 cfa_attribute,
                                 cfa.attrib_id attr_id,
                                 item,
                                 code_type
                            from xxadeo_sp_v_cfa_attrib cfa,
                                 (select group_id, cfa_attribute, cfa_value, item
                                    from xxadeo_sp_v_item_cfa_ext imcfa unpivot(cfa_value for cfa_attribute in(VARCHAR2_1,
                                                                                                                    VARCHAR2_2,
                                                                                                                    VARCHAR2_3,
                                                                                                                    VARCHAR2_4,
                                                                                                                    VARCHAR2_5,
                                                                                                                    VARCHAR2_6,
                                                                                                                    VARCHAR2_7,
                                                                                                                    VARCHAR2_8,
                                                                                                                    VARCHAR2_9,
                                                                                                                    VARCHAR2_10))) cfa_unc
                           where cfa.group_id = cfa_unc.group_id
                             and upper(cfa.storage_col_name) = upper(cfa_unc.cfa_attribute)) aux,
                         xxadeo_sp_v_mom_dvm      dvm,
                         xxadeo_sp_v_code_detail  cd
                   where (dvm.value_1)            = aux.attr_id
                     and dvm.func_area                = 'CFA'
                     --and dvm.parameter                = 'PRICE_POLICY_ITEM'
                     and dvm.parameter                = 'CIRCUIT_ITEM'
                     and dvm.bu                   = st.area
                     and aux.code_type            = cd.code_type
                     and aux.cfa_value            = cd.code
                     and aux.item                 = im.item) circuit_item,
                ast.start_date,
                ast.end_date,
                ast.cluster_size,
                (select assortment_mode_label
                   from xxadeo_sp_v_assort_mode_lang
                  where assortment_mode_id = ast.assortment_mode) assortment_mode_desc,
                loc.source_method,
                loc.promo_retail,
                loc.promo_selling_uom,
                null promo_source,
                isp.supp_discontinue_date promo_start_date,
                isp.supp_discontinue_date promo_end_date,
                CASE  loc.source_method
                  WHEN 'W' THEN (select wh.WH||' - '||wh.wh_name from xxadeo_sp_v_wh wh where loc.source_wh = wh.wh)
                  WHEN 'S' THEN (select sup.supplier||' - '||sup.sup_name from xxadeo_sp_v_sups sup where isp.supplier = sup.supplier)
                  ELSE null
                END source_details,
                st.currency_code
        from xxadeo_sp_v_item_master im,
             xxadeo_sp_v_groups grp,
             xxadeo_sp_v_deps dpt,
             xxadeo_sp_v_class cls,
             xxadeo_sp_v_subclass scls,
             xxadeo_sp_v_item_bu bu,
             xxadeo_sp_v_item_supplier isp,
             xxadeo_sp_v_item_loc loc,
             xxadeo_sp_v_item_image img,
             xxadeo_sp_v_store st,
             xxadeo_sp_v_wh wh,
             (select item, store, cluster_size, store_choice, assortment_mode, hq_recommendation, not_mod_ind, start_date, end_date from xxadeo_sp_v_assortment where get_vdate (+) between start_date and nvl(end_date, to_date(99990101, 'YYYYMMDD'))) ast
       WHERE grp.group_no           = im.group_no
         AND dpt.dept               = im.dept
         AND cls.class              = im.class
         AND scls.subclass          = im.subclass
         AND bu.item (+)            = im.item
         AND ast.item (+)           = im.item
         AND ast.store (+)          = loc.loc
         AND loc.item               = im.item
         AND img.item (+)           = im.item
         AND isp.item               = im.item
         AND loc.loc                = st.store
         AND bu.bu  (+)             = st.area
         AND wh.wh (+)              = loc.source_wh
         AND isp.primary_supp_ind   = 'Y';
