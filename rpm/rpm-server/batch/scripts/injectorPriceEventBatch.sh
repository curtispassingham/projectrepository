#!/bin/sh
#
# usage: 	injectorPriceEventBatch.sh <user_alias> [status=<status>] [event_type=<event_type>] 
#			[polling_interval=<polling_interval>]
# 			where status is
#			A(Approved), N(New), E(Error) or W(Worksheet)  - N(New) is the default
#			and event_type is
#			PC(Price Change), CL(Clearance), NR(New Clearance Reset), SP(Simple Promo), TP(Threshold Promo), 
#			FP(Finance Promo), CP(Complex/Multibuy Promo), TXN(Transaction Promo) - PC is the default
# 			and polling_interval is the interval on how often (in seconds) to check if the conflict checking process is complete
#           Valid values for the interval are 1 to 1000 - 10 seconds is the default. 
#            
# Example: launchRpmBatch.sh com.retek.rpm.batch.InjectorPriceEventBatch <user_alias> status=N event_type=SP polling_interval=20
launchRpmBatch.sh com.retek.rpm.batch.InjectorPriceEventBatch $@
