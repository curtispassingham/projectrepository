#!/bin/sh
#
# usage: nightlyBatchCleanup.sh </@tnsaliasname> <pre -or- post> <log path>

PRE_VALUE="PRE"
POST_VALUE="POST"

if [ $# -lt 3 ]
then
   echo 'USAGE: nightlyBatchCleanup.sh </@tnsaliasname> <pre -or- post> <log path>'
   exit 1
fi

CONNECT=$1
PRE_POST=$2
LOG_PATH=$3

# get the PRE_POST value to be in all caps to make for easier checking...
PRE_POST=`echo $PRE_POST | tr "[a-z]" "[A-Z]"`

if [ -z "$CONNECT" ] ; then
   echo 'You must specify a TNS Alias /</@tnsaliasname>'
   exit 1
fi

if [ -z "$PRE_POST" ] ; then
   echo 'You must specify a pre-post value'
   exit 1
fi

if [ "$PRE_POST" != "$PRE_VALUE" ] && [ "$PRE_POST" != "$POST_VALUE" ] ; then
   echo 'Invalid value for pre-post: ' $PRE_POST
   exit 1
fi

if [ -d "$LOG_PATH" ]; then
  if [ ! -w "$LOG_PATH" ]; then
    echo 'You do not have write permissions to Log path $LOG_PATH'
    exit 1
  fi
else
  echo 'Specified Log path does not exist'
  exit 1
fi

LOG_FILE=$LOG_PATH'/nightlyBatchCleanup_'$PRE_POST'_'`date +'%b_%d_%Y'`'.log'

### Check if log file already exists. If yes, rename it.
if [ -e $LOG_FILE ]
then
   mv -f $LOG_FILE $LOG_FILE.`date +%H%M%S`
fi

########################################################################

# Starting the function write_log()
write_log()
{
   `$ORACLE_HOME/bin/sqlplus -s $CONNECT >> $LOG_FILE <<EOF
   set serveroutput on;
   whenever sqlerror exit 1;

   DECLARE

      L_error_msg  VARCHAR2(500) := NULL;
      L_return     NUMBER        := NULL;
      L_status     VARCHAR2(50)  := '$1';
      L_start_date DATE          := NULL;
      L_end_date   DATE          := NULL;
      L_log_file   VARCHAR2(500) := NULL;
      L_pre_post   VARCHAR(10)   := NULL;

   BEGIN

      if L_status = 'STARTED' then
         L_start_date := SYSDATE;
         L_status := NULL;
         L_log_file := '$LOG_FILE';
         L_pre_post := '$PRE_POST';
      else
         L_end_date := SYSDATE;
      end if;

      L_return := RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY(L_error_msg,
                                                              'nightlyBatchCleanup.sh',
                                                              USER,
                                                              NULL, -- luw_value
                                                              NULL, -- concurrent_threads
                                                              L_pre_post,
                                                              L_log_file,
                                                              NULL,
                                                              NULL,
                                                              NULL, -- I_thread_number
                                                              L_start_date,
                                                              L_end_date,
                                                              L_status);

      if L_return = 0 then
         DBMS_OUTPUT.PUT_LINE('SQL Error when calling RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY: '|| L_error_msg);
         return;
      end if;

      return;

   EXCEPTION
      when OTHERS then
         DBMS_OUTPUT.PUT_LINE('SQL Error when calling RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY: '||SQLERRM);
      return;

   END;
   /
   exit;
EOF`

#check if there is error
if [ `grep "SP2-" $LOG_FILE | wc -l` -gt 0 ]
then
   echo "SP2 Error in write_log function" >> $LOG_FILE
   exit 1
fi

if [ `grep "ORA-" $LOG_FILE | wc -l` -gt 0 ]
then
   echo "ORA Error in write_log function" >> $LOG_FILE
   exit 1
fi
}
#end write_log function
###############################################################################################

write_log STARTED

echo "----------------------------------------------------------------" >> $LOG_FILE
echo "Started processing at: "`date +'%b %d %Y %H:%M:%S'`               >> $LOG_FILE
echo "----------------------------------------------------------------" >> $LOG_FILE
echo " " >> $LOG_FILE

if [ "$PRE_POST" == "$PRE_VALUE" ] ; then
   $ORACLE_HOME/bin/sqlplus -s $CONNECT >> $LOG_FILE <<EOF
      set serveroutput on;
      exec RPM_NIGHTLY_BATCH_CLEANUP_SQL.PRE();
EOF

else
   $ORACLE_HOME/bin/sqlplus -s $CONNECT >> $LOG_FILE <<EOF
      set serveroutput on;
      exec RPM_NIGHTLY_BATCH_CLEANUP_SQL.POST();
EOF
fi

#check if there is error
if [ `grep "SP2-" $LOG_FILE | wc -l` -gt 0 ]
then
   echo "SP2 Error when calling RPM_NIGHTLY_BATCH_CLEANUP_SQL" >> $LOG_FILE
   write_log FAILED
   exit 1
fi

if [ `grep "ORA-" $LOG_FILE | wc -l` -gt 0 ]
then
   echo "ORA Error when calling RPM_NIGHTLY_BATCH_CLEANUP_SQL" >> $LOG_FILE
   write_log FAILED
   exit 1
fi

write_log SUCCESS

echo "----------------------------------------------------------------" >> $LOG_FILE
echo "Finished processing at: "`date +'%b %d %Y %H:%M:%S'`              >> $LOG_FILE
echo "----------------------------------------------------------------" >> $LOG_FILE

exit 0
