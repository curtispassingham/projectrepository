#! /bin/sh

# Test for the number of input arguments
if [ $# -lt 3 ]
then
   echo 'Usage: progam_name connect_string logpath errpath processtype'
   echo "Usage: processtype is optional :'1'- process all promos at a time ,'2'- process one promo at a time. By default processtype is 1"
   exit 1
fi

CONNECT=$1
LOGPATH=$2
ERRPATH=$3
PROCESSTYPE=$4

if [ -d "$LOGPATH" ]; then
  if [ ! -w "$LOGPATH" ]; then
    echo 'You do not have write permissions to Log path $LOGPATH'
    exit 1
  fi
else
  echo 'Specified Log path does not exist'
  exit 1
fi

if [ -d "$ERRPATH" ]; then
  if [ ! -w "$ERRPATH" ]; then
    echo 'You do not have write permissions to Error path $ERRPATH'
    exit 1
  fi
else
  echo 'Specified Error path does not exist'
  exit 1
fi

if [ "$PROCESSTYPE" == "" ]; then
    PROCESSTYPE=1
elif [ "$PROCESSTYPE" != 1 ] &&
     [ "$PROCESSTYPE" != 2 ] ;
then
   echo "Invalid status parameter passed - $PROCESSTYPE.  Valid values are '1'- process all promos at a time ,'2'- process one promo at a time."
   exit 1
fi

echo 'Running PromotionArchiveBatch Batch'
echo 'LOGPATH=' $LOGPATH
echo 'ERRPATH=' $ERRPATH
echo 'PROCESSTYPE=' $PROCESSTYPE

LOGFILE=$LOGPATH'/PromotionArchiveBatch.'`date +'%b_%d_%Y'`'.log'
ERRFILE=$ERRPATH'/err.PromotionArchiveBatch.'`date +'%b_%d_%Y'`

echo 'LOGFILE=' $LOGFILE
echo 'ERRFILE=' $ERRFILE

## Check if err file already exists. If yes, rename it.
if [ -f $ERRFILE ]
then
   mv -f $ERRFILE $ERRFILE.`date +%H%M%S`
fi


echo "----------------------------------------------------------------" >> $LOGFILE
echo "Processing started at:"`date +'%b_%d_%Y %H:%M:%S'`                >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE

# Call the function RPM_ARCHIVE_PROMOTIONS.ARCHIVE()
`$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $ERRFILE <<EOF
whenever sqlerror exit 1;
set serveroutput on;
set pagesize 0;
set feedback off;

DECLARE

 L_program   VARCHAR2(50)  := 'RPM_ARCHIVE_PROMOTIONS.ARCHIVE';
 L_vdate     DATE  := GET_VDATE();
 O_error_msg VARCHAR2(20000) := NULL;
 L_default_end_date    DATE  := to_date('3000', 'YYYY');
 L_status NUMBER :=NULL;
 L_process_type NUMBER:=NULL;

 cursor c_promo_ids is
      select promo_id
        from rpm_promo
       where NVL(end_date, L_default_end_date) < L_vdate;
	   
BEGIN

L_process_type:='$PROCESSTYPE';

 if L_process_type =1 then

    L_status := RPM_ARCHIVE_PROMOTIONS.ARCHIVE(null,O_error_msg);

    IF L_status <> 1 THEN
      DBMS_OUTPUT.PUT_LINE('Call to RPM_ARCHIVE_PROMOTIONS.ARCHIVE Failed....'|| O_error_msg);
       RETURN;
    END IF;

    COMMIT;
     DBMS_OUTPUT.PUT_LINE('Function RPM_ARCHIVE_PROMOTIONS.ARCHIVE executed successfully');

 elsif L_process_type =2 then

   FOR rec IN c_promo_ids
   LOOP

    DBMS_OUTPUT.PUT_LINE('Calling the program for promo_id  : '|| rec.promo_id);
    L_status := RPM_ARCHIVE_PROMOTIONS.ARCHIVE(rec.promo_id,O_error_msg);

    IF L_status <> 1 THEN
      DBMS_OUTPUT.PUT_LINE('Call to RPM_ARCHIVE_PROMOTIONS.ARCHIVE Failed....'|| O_error_msg);
       RETURN;
    END IF;

    COMMIT;
    END LOOP ;

    DBMS_OUTPUT.PUT_LINE('Function RPM_ARCHIVE_PROMOTIONS.ARCHIVE executed successfully');

 END IF;

EXCEPTION

WHEN OTHERS THEN

DBMS_OUTPUT.PUT_LINE('Error while executing '|| L_program ||' : '|| O_error_msg);
                                                                                   
 END;
/
exit;

EOF`

echo "----------------------------------------------------------------" >> $LOGFILE
echo "Finished processing at:"`date +'%b_%d_%YT%H:%M:%S'`               >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE


                                                        
