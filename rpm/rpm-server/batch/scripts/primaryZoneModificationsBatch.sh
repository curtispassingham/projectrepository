#!/bin/sh

# Test for the number of input arguments
if [ $# -lt 3 ]
then
   echo 'USAGE: primaryZoneModificationsBatch.sh </@tnsaliasname> <log_path> <error_path>'
   exit 1
fi

CONNECT=$1
LOGPATH=$2
ERRPATH=$3

if [ -d "$LOGPATH" ]; then
   if [ ! -w "$LOGPATH" ]; then
      echo 'You do not have write permissions to Log path $LOGPATH'
      exit 1
   fi
else
   echo 'Specified Log path does not exist'
   exit 1
fi

if [ -d "$ERRPATH" ]; then
   if [ ! -w "$ERRPATH" ]; then
      echo 'You do not have write permissions to Error path $ERRPATH'
      exit 1
   fi
else
   echo 'Specified Error path does not exist'
   exit 1
fi

echo 'CONNECT=' $CONNECT
echo 'LOGPATH=' $LOGPATH
echo 'ERRPATH=' $ERRPATH

LOGFILE=$LOGPATH'/primaryZoneModificationsBatch'`date +'%b_%d_%Y'`'.log'
ERRFILE=$ERRPATH'/err.primaryZoneModificationsBatch.'`date +'%b_%d_%Y'`

echo "LOG FILENAME = " $LOGFILE
echo "ERR FILENAME = " $ERRFILE

### Check if log file already exists. If yes, rename it.
if [ -e $LOGFILE ]
then
   mv -f $LOGFILE $LOGFILE.`date +%H%M%S`
fi

### Check if err file already exists. If yes, rename it.
if [ -e $ERRFILE ]
then
   mv -f $ERRFILE $ERRFILE.`date +%H%M%S`
fi

# Starting the function write_log()
write_log()
{
   `$ORACLE_HOME/bin/sqlplus -s $CONNECT >> $ERRFILE <<EOF
   set serveroutput on;
   whenever sqlerror exit 1;

   DECLARE

      L_error_msg  VARCHAR2(500) := NULL;
      L_return     NUMBER        := NULL;
      L_start_date DATE          := NULL;
      L_end_date   DATE          := NULL;
      L_status     VARCHAR2(50)  := '$1';
      L_log_path   VARCHAR2(500) := NULL;
      L_error_path VARCHAR2(500) := NULL;

   BEGIN

      if L_status = 'STARTED' then
         L_status := NULL;
         L_start_date := SYSDATE;
         L_log_path := '$LOGPATH';
         L_error_path := '$ERRPATH';

      else
         L_end_date := SYSDATE;
      end if;

      L_return := RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY(L_error_msg,
                                                              'primaryZoneModificationsBatch.sh',
                                                              USER,
                                                              NULL, -- luw_value
                                                              NULL, -- concurrent_threads
                                                              NULL, -- other_parameters
                                                              L_log_path,
                                                              L_error_path,
                                                              NULL,
                                                              NULL, -- I_thread_number
                                                              L_start_date,
                                                              L_end_date,
                                                              L_status);


      if L_return = 0 then
         DBMS_OUTPUT.PUT_LINE('SQL Error when calling RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY: '|| L_error_msg);
         return;
      end if;

      return;

   EXCEPTION
      when OTHERS then
         DBMS_OUTPUT.PUT_LINE('SQL Error when calling RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY: '||SQLERRM);
      return;

   END;
   /
   exit;
EOF`

#check if there is error
if [ `grep "SP2-" $ERRFILE | wc -l` -gt 0 ]
then
   echo "SP2 Error in write_log function" >> $LOGFILE
   exit 1
fi

if [ `grep "ORA-" $ERRFILE | wc -l` -gt 0 ]
then
   echo "ORA Error in write_log function" >> $LOGFILE
   exit 1
fi
}
#end write_log function

write_log STARTED

echo "----------------------------------------------------------------" >> $LOGFILE
echo "Processing started at: "`date +'%b %d %Y %H:%M:%S'`               >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE

program_value=`$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $ERRFILE <<EOF
whenever sqlerror exit 1;
set serveroutput on format wrapped;
set pagesize 0;
set feedback off;

DECLARE

  O_error_msg VARCHAR2(200) := NULL;
  L_return NUMBER := NULL;

BEGIN

   L_return := RPM_PRIMARY_ZONE_UPDATE_SQL.EXECUTE(O_error_msg);

   if L_return = 0 then
      DBMS_OUTPUT.PUT_LINE('SQL Error - ' || O_error_msg);
      rollback;
      return;
   end if;

   commit;

EXCEPTION
   when OTHERS then
       DBMS_OUTPUT.PUT_LINE('error: primaryZoneModificationsBatch' || SQLERRM);
       rollback;
       return;
END;
/
exit;
EOF`

if [ `grep "ORA-" $ERRFILE | wc -l` -gt 0 ]
   then
      echo "ORA Error while processing modifications to PZG definitions" >> $LOGFILE
      write_log FAILED
      exit 1
fi

if [ `grep "SP2-" $ERRFILE | wc -l` -gt 0 ]
   then
      echo "SP2 Error while processing modifications to PZG definitions" >> $LOGFILE
      write_log FAILED
      exit 1
fi

write_log SUCCESS

rm -f $ERRFILE

echo "----------------------------------------------------------------" >> $LOGFILE
echo "Finished processing at: "`date +'%b %d %Y %H:%M:%S'`              >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE

exit 0
