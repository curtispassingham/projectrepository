#!/bin/sh

# Initialize number of parallel threads
SLOTS=0

# Test for the number of input arguments
if [ $# -lt 5 ]
then
   echo 'Usage: progam_name connect_string load_data slots logpath errpath'
   exit 1
fi

CONNECT=$1
LOAD_DATA=$2
SLOTS=$3
LOGPATH=$4
ERRPATH=$5

DUMMY_FILE=dataConversionSeedFutureRetailInit.out
DATA_FILE=dc_item_loc.dat

if [ -d "$LOGPATH" ]; then
  if [ ! -w "$LOGPATH" ]; then
    echo 'You do not have write permissions to Log path $LOGPATH'
    exit 1
  fi
else
  echo 'Specified Log path does not exist'
  exit 1
fi

if [ -d "$ERRPATH" ]; then
  if [ ! -w "$ERRPATH" ]; then
    echo 'You do not have write permissions to Error path $ERRPATH'
    exit 1
  fi
else
  echo 'Specified Error path does not exist'
  exit 1
fi

echo 'Running dataConversionSeedFutureRetail Batch'
echo 'SLOTS=' $SLOTS
echo 'LOGPATH=' $LOGPATH
echo 'ERRPATH=' $ERRPATH

LOGFILE=$LOGPATH'/dataConversionSeedFutureRetail.'`date +'%b_%d_%Y'`'.log'
ERRFILE=$ERRPATH'/err.dataConversionSeedFutureRetail.'`date +'%b_%d_%Y'`

echo 'LOGFILE=' $LOGFILE
echo 'ERRFILE=' $ERRFILE

## Check if err file already exists. If yes, rename it.
if [ -f $ERRFILE ]
then
   mv -f $ERRFILE $ERRFILE.`date +%H%M%S`
fi

# Starting the function load_data  -- this will load data from the data file dc_item_loc.dat into RPM_DC_ITEM_LOC
load_data()
{

echo "----------------------------------------------------------------" >> $LOGFILE
echo "Data Loading started at:"`date +'%b_%d_%Y %H:%M:%S'`              >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE

## Check if data file exists
if [ ! -f $DATA_FILE ]
then
       echo 'Please place the data file dc_item_loc.dat in the same directory as the script' >> $LOGFILE
       exit 1
fi

BAD_FILE=bad_file.dat
DISC_FILE=discard_file.dat
CTL_FILE=ctl_file.ctl
SQLLDR_LOG_FILE=sqlldr_log_file.log

## Check if Bad file already exists. If yes, rename it.
if [ -f $BAD_FILE ]
then
   mv -f $BAD_FILE $BAD_FILE.`date +%H%M%S`
fi

## Check if Discard file already exists. If yes, rename it.
if [ -f $DISC_FILE ]
then
   mv -f $DISC_FILE $DISC_FILE.`date +%H%M%S`
fi

## Check if Control file already exists. If yes, rename it.
if [ -f $CTL_FILE ]
then
   mv -f $CTL_FILE $CTL_FILE.`date +%H%M%S`
fi

## Check if Control file already exists. If yes, rename it.
if [ -f $SQLLDR_LOG_FILE ]
then
   mv -f $SQLLDR_LOG_FILE $SQLLDR_LOG_FILE.`date +%H%M%S`
fi

## Create Ctl file
echo "       ">> $CTL_FILE
echo "LOAD DATA ">> $CTL_FILE
echo "  INFILE $DATA_FILE ">> $CTL_FILE
echo "  BADFILE $BAD_FILE ">> $CTL_FILE
echo "  TRUNCATE ">> $CTL_FILE
echo "  INTO TABLE RPM_DC_ITEM_LOC ">> $CTL_FILE
echo "  FIELDS TERMINATED BY \"|\" TRAILING NULLCOLS ">> $CTL_FILE
echo "  (   ">> $CTL_FILE
echo "     ITEM, ">> $CTL_FILE
echo "     LOCATION, ">> $CTL_FILE
echo "     LOC_TYPE, ">> $CTL_FILE
echo "     PRIMARY_LOC_IND FILLER, ">> $CTL_FILE
echo "     SELLING_UNIT_RETAIL, ">> $CTL_FILE
echo "     SELLING_UOM, ">> $CTL_FILE
echo "     TAXABLE_IND FILLER, ">> $CTL_FILE
echo "     LOCAL_ITEM_DESC FILLER, ">> $CTL_FILE
echo "     LOCAL_SHORT_DESC FILLER, ">> $CTL_FILE
echo "     TI FILLER, ">> $CTL_FILE
echo "     HI FILLER, ">> $CTL_FILE
echo "     STORE_ORD_MULT FILLER, ">> $CTL_FILE
echo "     MEAS_OF_EACH FILLER, ">> $CTL_FILE
echo "     MEAS_OF_PRICE FILLER, ">> $CTL_FILE
echo "     UOM_OF_PRICE FILLER, ">> $CTL_FILE
echo "     PRIMARY_COST_PACK FILLER, ">> $CTL_FILE
echo "     INBOUND_HANDLING_DAYS FILLER, ">> $CTL_FILE
echo "     SOURCE_WH FILLER, ">> $CTL_FILE
echo "     SOURCE_METHOD FILLER, ">> $CTL_FILE
echo "     MULTI_UNITS, ">> $CTL_FILE
echo "     MULTI_UNIT_RETAIL, ">> $CTL_FILE
echo "     MULTI_SELLING_UOM,  ">> $CTL_FILE
echo "     AVERAGE_WEIGHT FILLER ">> $CTL_FILE
echo "  ) ">> $CTL_FILE
echo "    ">> $CTL_FILE

$ORACLE_HOME/bin/sqlldr userid=$CONNECT,control=$CTL_FILE,log=$SQLLDR_LOG_FILE,errors=0 >> $LOGFILE

}
#end of the function load_data

echo "----------------------------------------------------------------" >> $LOGFILE
echo "Processing started at:"`date +'%b_%d_%Y %H:%M:%S'`                >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE


if [ $LOAD_DATA == Y ] || [ $LOAD_DATA == y ]; then
  #Call load_data function to load data from data file
  load_data

  if [ $? -ne 0 ]; then
    echo "Error! While Loading data from Data File." >> $LOGFILE
    exit 1
  fi

  echo "Finished Loading Data at " `date +'%b_%d_%Y %H:%M:%S'` >> $LOGFILE

fi



echo "----------------------------------------------------------------" >> $LOGFILE
echo "Data Initializing Process started at:"`date +'%b_%d_%Y %H:%M:%S'` >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE

# Call the function RPM_SEED_RFR_SQL.INIT()
`$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $ERRFILE <<EOF
whenever sqlerror exit 1;
set serveroutput on;
set pagesize 0;
set feedback off;

variable max_thread_num NUMBER

DECLARE

   L_error_msg       VARCHAR2(2000) := NULL;
   L_max_thread_num  NUMBER         := NULL;
   L_return          NUMBER         := NULL;

BEGIN

   L_return := RPM_SEED_RFR_SQL.INIT(L_error_msg,
                                     L_max_thread_num);

   if L_return = 0 then

      DBMS_OUTPUT.PUT_LINE('SQL Error while initializing data: ' || L_error_msg);
      rollback;

      return;

   end if;

   commit;

   :max_thread_num := NVL(L_max_thread_num, 0);

   EXCEPTION
      when OTHERS then
         DBMS_OUTPUT.PUT_LINE('ERROR while initializing data: ' || sqlerrm);

END;
/

spool $DUMMY_FILE

select :max_thread_num from dual;

spool off

EOF`

if [ `grep "ORA-" $ERRFILE | wc -l` -gt 0 ]
   then
      echo "ORA Error while threading the staged data " >> $LOGFILE
      exit 1
fi

if [ `grep "SP2-" $ERRFILE | wc -l` -gt 0 ]
   then
      echo "SP2 Error while threading the staged data " >> $LOGFILE
      exit 1
fi

read max_thread_value < $DUMMY_FILE

rm $DUMMY_FILE

# At this point, the error file should only have output from the spool above - clean that up...
rm $ERRFILE

if [ $max_thread_value -eq 0 ]
   then

   echo "No Threads to process...  Exiting." >> $LOGFILE
   exit 1

fi

# Starting the function process_thread  -- this will call the function RPM_SEED_RFR_SQL.PROCESS_THREAD()
process_thread()
{
   fthreadnum=$1
   THREAD_ERR_FILE=ERRFILE_$fthreadnum

   echo "Started processing thread $fthreadnum: " `date +'%b_%d_%Y %H:%M:%S'` >> $LOGFILE

   `$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $THREAD_ERR_FILE <<EOF
   whenever sqlerror exit 1;
   set serveroutput on format wrapped;
   set pagesize 0;
   set feedback off;

   DECLARE

      L_error_msg VARCHAR2(2000) := NULL;
      L_return    NUMBER         := NULL;

   BEGIN

      L_return := RPM_SEED_RFR_SQL.PROCESS_THREAD(L_error_msg,
                                                  $fthreadnum);

      if L_return = 0 then

         DBMS_OUTPUT.PUT_LINE('SQL Error while processing thread $fthreadnum: '|| L_error_msg);
         rollback;

         return;

      end if;

      commit;

      EXCEPTION
         when others then
            DBMS_OUTPUT.PUT_LINE('ERROR while processing thread '||$fthreadnum||':');
            DBMS_OUTPUT.PUT_LINE(sqlerrm);

            rollback;

   END;
   /
   exit
EOF`

   echo "Finished processing thread $fthreadnum: " `date +'%b_%d_%Y %H:%M:%S'` >> $LOGFILE

   if [ `grep "ORA-" $THREAD_ERR_FILE | wc -l` -gt 0 ]
      then
         echo "ORA Error while processing thread number: "$fthreadnum >> $LOGFILE

         cat $THREAD_ERR_FILE >> $ERRFILE
         rm $THREAD_ERR_FILE

         exit 1
   fi

   cat $THREAD_ERR_FILE >> $ERRFILE
   rm $THREAD_ERR_FILE

   exit 0
}
#end of the function process_thread

i=1

while [ $i -le $max_thread_value ]
do
   # verify the number of threads
   while true
   do

      NUM_JOBS=`jobs | wc -l`

      if [ $NUM_JOBS -lt $SLOTS ]
      then
         break
      fi
   done

   ### Call fn. process_thread and send it to the background
   process_thread $i &

   i=`expr $i + 1`

done

# Wait for all of the threads to complete
wait

`$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $ERRFILE <<EOF
whenever sqlerror exit 1;
set serveroutput on;
set feedback off;

   truncate table rpm_subclass_tmp;
   truncate table rpm_zone_location_tmp;

EOF`

echo "----------------------------------------------------------------" >> $LOGFILE
echo "Finished processing at:"`date +'%b_%d_%Y %H:%M:%S'`               >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE

exit

