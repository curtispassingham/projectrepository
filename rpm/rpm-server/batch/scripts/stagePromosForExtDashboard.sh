#!/bin/sh

USAGE='USAGE: stagePromosForExtDashboard.sh <connetion_alias> <slots> <luw> <log_path> <error_path>'

if [ $# -lt 5 ] ; then
   echo $USAGE
   exit 1;
fi

CONNECT=$1
SLOTS=$2
LUW=$3
LOG_PATH=$4
ERR_PATH=$5

DUMMY_FILE=stagePromosForExtDashboard.threads.dat

if [ -d $LOG_PATH ] ; then
   if [ ! -w $LOG_PATH ] ; then
      echo "You do not have write permissions to the log path $LOG_PATH"
      exit 1
   fi
else
   echo "Specified log path does not exist: $LOG_PATH"
   exit 1
fi

if [ -d $ERR_PATH ] ; then
   if [ ! -w $ERR_PATH ] ; then
      echo "You do not have write permissions to the log path $ERR_PATH"
      exit 1
   fi
else
   echo "Specified error path does not exist: $ERR_PATH"
   exit 1
fi

LOGFILE=$LOG_PATH'/stagePromosForExtDashboard_'`date +'%b_%d_%Y'`'.log'
ERRFILE=$ERR_PATH'/err.stagePromosForExtDashboard_'`date +'%b_%d_%Y'`

FAIL_FILE='stagePromosForExtDashboard_'`date +'%Y%m%d'`'.fail'

echo 'Running stagePromosForExtDashboard Batch'

echo SLOTS = $SLOTS
echo LUW = $LUW
echo LOGPATH = $LOG_PATH
echo ERRPATH = $ERR_PATH
echo LOGFILE = $LOGFILE
echo ERRFILE = $ERRFILE

if [ -f $LOGFILE ] ; then
   mv -f $LOGFILE $LOG_PATH'/stagePromosForExtDashboard_'`date +'%b_%d_%Y_%H%M%S'`.log
fi

## Check if err file already exists. If yes, rename it.
if [ -f $ERRFILE ] ; then
   mv -f $ERRFILE $ERRFILE.`date +%H%M%S`
fi

# Starting the function write_log()
write_log()
{
   WR_ERR_FILE=$ERRFILE"_WRITE_LOG_"$2

   `$ORACLE_HOME/bin/sqlplus -s $CONNECT >> $WR_ERR_FILE <<EOF
   set serveroutput on;
   whenever sqlerror exit 1;

   DECLARE

      L_error_msg         VARCHAR2(500) := NULL;
      L_return            NUMBER        := NULL;
      L_start_date        DATE          := NULL;
      L_end_date          DATE          := NULL;
      L_status            VARCHAR2(50)  := '$1';
      L_luw               NUMBER        := NULL;
      L_thread_num        NUMBER        := '$2';
      L_concurrent_thread NUMBER        := '$SLOTS';
      L_log_path          VARCHAR2(500) := NULL;
      L_error_path        VARCHAR2(500) := NULL;

   BEGIN

      if L_status = 'STARTED' then

         L_status := NULL;
         L_start_date := SYSDATE;

         if L_thread_num is NULL then
            L_log_path := '$LOG_PATH';
            L_error_path := '$ERR_PATH';
            L_luw := '$LUW';
         elsif L_thread_num is NOT NULL then
            L_concurrent_thread := NULL;
         end if;

      else
         L_end_date := SYSDATE;
         L_concurrent_thread := NULL;
      end if;

      L_return := RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY(L_error_msg,
                                                              'stagePromosForExtDashboard.sh',
                                                              USER,
                                                              L_luw, -- luw_value
                                                              L_concurrent_thread, -- concurrent_threads
                                                              NULL,
                                                              L_log_path,
                                                              L_error_path,
                                                              NULL,
                                                              L_thread_num, -- I_thread_number
                                                              L_start_date,
                                                              L_end_date,
                                                              L_status);

      if L_return = 0 then
         DBMS_OUTPUT.PUT_LINE('SQL Error when calling RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY: '|| L_error_msg);
         return;
      end if;

      return;

   EXCEPTION
      when OTHERS then
         DBMS_OUTPUT.PUT_LINE('SQL Error when calling RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY: '||SQLERRM);
      return;

   END;
   /
   exit;
EOF`

#check if there is error
if [ `grep "SP2-" $WR_ERR_FILE | wc -l` -gt 0 ]
then
   echo "SP2 Error in write_log function" >> $LOGFILE
   cat $WR_ERR_FILE >> $ERRFILE
   rm $WR_ERR_FILE
   exit 1
fi

if [ `grep "ORA-" $WR_ERR_FILE | wc -l` -gt 0 ]
then
   echo "ORA Error in write_log function" >> $LOGFILE
   cat $WR_ERR_FILE >> $ERRFILE
   rm $WR_ERR_FILE
   exit 1
fi

rm $WR_ERR_FILE
}
#end write_log function

write_log STARTED

echo "----------------------------------------------------------------" >> $LOGFILE
echo "Processing started at: "`date +'%b %d %Y %H:%M:%S'`               >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE

`$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $ERRFILE <<EOF
whenever sqlerror exit 1;
set serveroutput on;
set pagesize 0;
set feedback off;

variable max_thread_num NUMBER

DECLARE

   L_error_msg      VARCHAR2(2000) := NULL;
   L_max_thread_num NUMBER         := NULL;
   L_return         NUMBER         := NULL;

BEGIN

   L_return := RPM_EXT_DASHBOARD_DATA_SQL.INITIALIZE_THREADS(L_error_msg,
                                                             L_max_thread_num,
                                                             $LUW);

   if L_return = 0 then

      DBMS_OUTPUT.PUT_LINE('SQL Error while initializing threads: ' || L_error_msg);
      rollback;

      return;

   end if;

   commit;

   :max_thread_num := NVL(L_max_thread_num, 0);

EXCEPTION
   when OTHERS then
      DBMS_OUTPUT.PUT_LINE('ERROR while initializing threads: ' || SQLERRM);
      rollback;

END;
/

spool $DUMMY_FILE

select :max_thread_num
  from dual;

spool off

EOF`

if [ `grep "ORA-" $ERRFILE | wc -l` -gt 0 ] ; then
   echo "ORA Error while threading the data " >> $LOGFILE
   write_log FAILED
   exit 1
fi

if [ `grep "SP2-" $ERRFILE | wc -l` -gt 0 ] ; then
   echo "SP2 Error while threading the data " >> $LOGFILE
   write_log FAILED
   exit 1
fi

read MAX_THREAD_VALUE < $DUMMY_FILE

rm $DUMMY_FILE

#At this point, the error file should only have output from the spool above - clean that up...
rm $ERRFILE

if [ $MAX_THREAD_VALUE -eq 0 ] ; then
   echo "No Threads to process...  Exiting." >> $LOGFILE
   write_log SUCCESS
   exit 1
fi

echo "Total number of threads to process: $MAX_THREAD_VALUE" >> $LOGFILE

process_thread()
{
   THREAD_NUM=$1

   THREAD_ERR_FILE=$ERRFILE"_"$THREAD_NUM

   echo "Started processing thread $THREAD_NUM: " `date +'%b %d %Y %H:%M:%S'` >> $LOGFILE

   write_log STARTED $THREAD_NUM

   `$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $THREAD_ERR_FILE <<EOF
   whenever sqlerror exit 1;
   set serveroutput on format wrapped;
   set pagesize 0;
   set feedback off;

   DECLARE

      L_error_msg VARCHAR2(2000) := NULL;
      L_return    NUMBER         := NULL;

   BEGIN

      L_return := RPM_EXT_DASHBOARD_DATA_SQL.STAGE_PROMO_DATA(L_error_msg,
                                                              $THREAD_NUM);

      if L_return = 0 then

         DBMS_OUTPUT.PUT_LINE('SQL Error while processing thread $THREAD_NUM: '|| L_error_msg);
         rollback;

         return;

      end if;

      commit;

   EXCEPTION
      when OTHERS then
         DBMS_OUTPUT.PUT_LINE('ERROR while processing thread '|| $THREAD_NUM || ':' || SQLERRM);

         rollback;

   END;
   /

   exit
EOF`

   if [ `grep "ORA-" $THREAD_ERR_FILE | wc -l` -gt 0 ] ; then
      echo "ORA Error while processing thread number: "$THREAD_NUM >> $LOGFILE

      cat $THREAD_ERR_FILE >> $ERRFILE
      rm $THREAD_ERR_FILE
      touch $FAIL_FILE
      write_log FAILED $THREAD_NUM

      exit 1
   fi

   if [ `grep "SP2-" $THREAD_ERR_FILE | wc -l` -gt 0 ] ; then
      echo "SP2 Error while processing thread number: "$THREAD_NUM >> $LOGFILE

      cat $THREAD_ERR_FILE >> $ERRFILE
      rm $THREAD_ERR_FILE
      touch $FAIL_FILE
      write_log FAILED $THREAD_NUM

      exit 1
   fi

   echo "Finished processing thread $THREAD_NUM: " `date +'%b_%d_%Y %H:%M:%S'` >> $LOGFILE

   write_log SUCCESS $THREAD_NUM

   cat $THREAD_ERR_FILE >> $ERRFILE
   rm $THREAD_ERR_FILE

   exit 0
} #end of the function process_thread

i=1

while [ $i -le $MAX_THREAD_VALUE ]
do

   # verify the number of threads
   while true
   do

      NUM_JOBS=`jobs -r | wc -l`

      if [ $NUM_JOBS -lt $SLOTS ] ; then
         break
      fi
   done

   # If a thread failed, stop the processing...
   if [ -f $FAIL_FILE ] ; then
      break
   fi

   ### Call process_thread and send it to the background
   process_thread $i &

   i=`expr $i + 1`

done

# Wait for all of the threads to complete
wait

if [ -f $FAIL_FILE ] ; then
   rm $FAIL_FILE
   echo "At least one thread encountered an error - stopping processing potentially without executing all threads" >> $LOGFILE
   write_log FAILED
   exit 1
fi

write_log SUCCESS

if [[ ! -s $ERRFILE  ]] ; then
   rm $ERRFILE
fi

echo "----------------------------------------------------------------" >> $LOGFILE
echo "Processing ended at: "`date +'%b %d %Y %H:%M:%S'`                 >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE

exit 0