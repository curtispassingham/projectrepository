#!/bin/ksh
#
# usage: promotionPriceChangePublishExport.sh </@tnsaliasname> <slots> <log_path> <error_path> <export-path(optional)>

# Test for the number of input arguments
if [ $# -lt 4 ]
then
   echo 'USAGE: promotionPriceChangePublishExport.sh </@tnsaliasname> <slots> <log_path> <error_path> <export_path(optional)>'
   exit 1
fi

CONNECT=$1
SLOTS=$2
LOGPATH=$3
ERRPATH=$4
EXPORTPATH=$5

if [ -d "$LOGPATH" ]; then
   if [ ! -w "$LOGPATH" ]; then
      echo 'You do not have write permissions to Log path $LOGPATH'
      exit 1
   fi
else
   echo 'Specified Log path does not exist'
   exit 1
fi

if [ -d "$ERRPATH" ]; then
   if [ ! -w "$ERRPATH" ]; then
      echo 'You do not have write permissions to Error path $ERRPATH'
      exit 1
   fi
else
   echo 'Specified Error path does not exist'
   exit 1
fi

### Check if path where extracts are to be created is passed as arg.
### If this arg is Null, then default to PWD.

if [ "$EXPORTPATH" == "" ]
then
   EXPORTPATH=`pwd`
   echo "Export Path is not sent. Defaulting to PWD: "`pwd`
else
   if [ -d "$EXPORTPATH" ]; then
      if [ ! -w "$EXPORTPATH" ]; then
         echo 'You do not have write permissions to Export path $EXPORTPATH'
         exit 1
      fi
   else
      echo 'Specified Export path does not exist'
      exit 1
   fi
fi

echo 'Running promotionPriceChangePublishExport Batch'
echo 'SLOTS=' $SLOTS
echo 'LOGPATH=' $LOGPATH
echo 'ERRPATH=' $ERRPATH
echo 'EXPORTPATH=' $EXPORTPATH

LOGFILE=$LOGPATH'/promotionPriceChangePublishExport'`date +'%b_%d_%Y'`'.log'
ERRFILE=$ERRPATH'/err.promotionPriceChangePublishExport.'`date +'%b_%d_%Y'`
FAILFILE='promotionPriceChangePublishExport'`date +'%b_%d_%Y'`'.fail'
PUBLIST='promotionPriceChangePublishExport'`date +'%b_%d_%Y'`'.lst'

echo 'LOGFILE=' $LOGFILE
echo 'ERRFILE=' $ERRFILE

########################################################################

# Starting the function write_log()
write_log()
{
   #The string manipulation below is to get the loc id that only pull the numeric value
   #from the second parameter which contains the loc id and loc type in a format "123_S"

   LOC_TYPE=$(echo $2 | sed 's/[^0-9]*//g')

   THREAD_ERR_FILE=$ERRFILE"_WRITE_LOG_"$LOC_TYPE

   `$ORACLE_HOME/bin/sqlplus -s $CONNECT >> $THREAD_ERR_FILE <<EOF
   set serveroutput on;
   whenever sqlerror exit 1;

   DECLARE

      L_error_msg         VARCHAR2(500) := NULL;
      L_return            NUMBER        := NULL;
      L_start_date        DATE          := NULL;
      L_end_date          DATE          := NULL;
      L_status            VARCHAR2(50)  := '$1';
      L_thread_num        NUMBER        := '$LOC_TYPE';
      L_concurrent_thread NUMBER        := '$SLOTS';
      L_log_path          VARCHAR2(500) := NULL;
      L_error_path        VARCHAR2(500) := NULL;
      L_export_path       VARCHAR2(500) := NULL;

   BEGIN

      if L_status = 'STARTED' then

         L_status := NULL;
         L_start_date := SYSDATE;

         if L_thread_num is NULL then
            L_log_path := '$LOGPATH';
            L_error_path := '$ERRPATH';
            L_export_path := '$EXPORTPATH';
         elsif L_thread_num is NOT NULL then
            L_concurrent_thread := NULL;
         end if;

      else
         L_end_date := SYSDATE;
         L_concurrent_thread := NULL;
      end if;

      L_return := RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY(L_error_msg,
                                                              'promotionPriceChangePublishExport.sh',
                                                              USER,
                                                              NULL, -- luw_value
                                                              L_concurrent_thread, -- concurrent_threads
                                                              NULL,
                                                              L_log_path,
                                                              L_error_path,
                                                              L_export_path,
                                                              L_thread_num, -- I_thread_number
                                                              L_start_date,
                                                              L_end_date,
                                                              L_status);

      if L_return = 0 then
         DBMS_OUTPUT.PUT_LINE('SQL Error when calling RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY: '|| L_error_msg);
         return;
      end if;

      return;

   EXCEPTION
      when OTHERS then
         DBMS_OUTPUT.PUT_LINE('SQL Error when calling RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY: '||SQLERRM);
      return;

   END;
   /
   exit;
EOF`

#check if there is error
if [ `grep "SP2-" $THREAD_ERR_FILE | wc -l` -gt 0 ]
then
   echo "SP2 Error in write_log function" >> $LOGFILE
   cat $THREAD_ERR_FILE >> $ERRFILE
   rm $THREAD_ERR_FILE
   exit 1
fi

if [ `grep "ORA-" $THREAD_ERR_FILE | wc -l` -gt 0 ]
then
   echo "ORA Error in write_log function" >> $LOGFILE
   cat $THREAD_ERR_FILE >> $ERRFILE
   rm $THREAD_ERR_FILE
   exit 1
fi

rm $THREAD_ERR_FILE
}

#end write_log function
###############################################################################################

### Check if err file already exists. If yes, rename it.
if [ -e $ERRFILE ]
then
   mv -f $ERRFILE $ERRFILE.`date +%H%M%S`
fi

### Check if log file already exists. If yes, rename it.
if [ -e $LOGFILE ]
then
   mv -f $LOGFILE $LOGFILE.`date +%H%M%S`
fi

### Call funtion write_log
write_log STARTED

# Set filename to contain this runs stores list
STORES=promotionPriceChangePublishExportLoc_`date +'%b_%d_%Y'`

$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >$STORES
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0

select distinct location
       ||'_'||
       location_type
  from rpm_price_publish_location
 where event_family = 'PrmPrcChg';

EOF

# Check for any Oracle errors from the SQLPLUS process
if [ `grep "ORA-" $STORES | wc -l` -gt 0 ]
then
   cat $STORES >> $ERRFILE
   echo "ORA Error while creating location file " $STORES >> $LOGFILE
   rm $STORES
   write_log FAILED
   exit 1
fi

if [ `grep "SP2-" $STORES | wc -l` -gt 0 ]
then
   cat $STORES >> $ERRFILE
   echo "SP2 Error while creating location file " $STORES >> $LOGFILE
   write_log FAILED
   exit 1
fi

if [[ ! -s $STORES ]] ; then
   rm -f $STORES
   echo "---------------------------------" >> $LOGFILE
   echo "No Data to process               " >> $LOGFILE
   echo "---------------------------------" >> $LOGFILE
   write_log SUCCESS
   exit
fi

echo "----------------------------------------------------------------" >> $LOGFILE
echo "Processing started at: "`date +'%b %d %Y %H:%M:%S'`               >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE

# Update the publish status
echo "Update Publish Status " >> $LOGFILE

`$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $ERRFILE <<EOF
whenever sqlerror exit 1;
set serveroutput on format wrapped;
set pagesize 0;
set feedback off;

DECLARE

  O_error_msg VARCHAR2(2000) := NULL;
  L_return    NUMBER         := NULL;

BEGIN

   L_return := RPM_NO_RIB_PRICE_PUBLISH_SQL.UPDATE_PUBLISH_STATUS(O_error_msg,
                                                                  'PrmPrcChg');

   if L_return != 1 then
      DBMS_OUTPUT.PUT_LINE('SQL error while update publish status - ' || O_error_msg);
      rollback;
      return;
   end if;

   commit;

   EXCEPTION
      when OTHERS then
         DBMS_OUTPUT.PUT_LINE('error: RPM_NO_RIB_PRICE_PUBLISH_SQL.UPDATE_PUBLISH_STATUS' || sqlerrm);
         rollback;
END;
/
exit;
EOF`

if [ `grep "ORA-" $ERRFILE | wc -l` -gt 0 ]
then
   echo "ORA Error while update publish status " >> $LOGFILE
   write_log FAILED
   exit 1
fi

if [ `grep "SP2-" $ERRFILE | wc -l` -gt 0 ]
then
   echo "SP2 Error while update publish status " >> $LOGFILE
   write_log FAILED
   exit 1
fi

echo "Finished update publish status " >> $LOGFILE

# Starting the function thread_store
thread_store()
{
   loc_and_type=$1
   echo "Started processing Location " $loc_and_type" at: "`date +'%b %d %Y %H:%M:%S'` >> $LOGFILE

   write_log STARTED $loc_and_type

   PUBFILE=$EXPORTPATH'/PRMPC_'`date +'%Y%m%d%H%M%S'`'_'$loc_and_type'.dat'
   echo $PUBFILE >> $PUBLIST

   $ORACLE_HOME/bin/sqlplus -s $CONNECT @spoolprmpcmsg.sql $PUBFILE $loc_and_type;

   if [ `grep "ORA-" $PUBFILE | wc -l` -gt 0 ]
   then
      cat $PUBFILE >> $ERRFILE
      echo "Exiting due to ORA Error in Location extract files for Location: "$loc_and_type >> $LOGFILE
      touch $FAILFILE
      write_log FAILED $loc_and_type
      exit 1
   fi

   if [ `grep "SP2-" $PUBFILE | wc -l` -gt 0 ]
   then
      cat $PUBFILE >> $ERRFILE
      echo "Exiting due to SP2 Error in Location extract files for Location: "$loc_and_type >> $LOGFILE
      touch $FAILFILE
      write_log FAILED $loc_and_type
      exit 1
   fi

   write_log SUCCESS $loc_and_type

   echo "Successfully generated flat file for Location: "$loc_and_type "at: "`date +'%b %d %Y %H:%M:%S'` >> $LOGFILE

   exit 0
}
#end the function thread_store

while read store
do
   while true
   do

      NUM_JOBS=`jobs | wc -l`

      if [ $NUM_JOBS -lt $SLOTS ]
      then
         break
      fi
   done

   ### Call funtion thread_store and send it to background.
   thread_store $store &

   # If the thread_store function failed, the script will create an empty fail file.
   # The batch should stop processing if the fail file exists.
   if [ -f $FAILFILE ]
   then
      break
   fi

done < $STORES

# Wait for all of the threads to complete
wait

if [ -f $FAILFILE ]
then
   xargs rm < $PUBLIST
   rm $FAILFILE
   rm $STORES
   rm $PUBLIST
   write_log FAILED
   exit 1
fi

write_log SUCCESS

rm -f $STORES
rm -f $PUBLIST
rm -f $ERRFILE

echo "----------------------------------------------------------------" >> $LOGFILE
echo "Finished processing at: "`date +'%b %d %Y %H:%M:%S'`              >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE

exit 0
