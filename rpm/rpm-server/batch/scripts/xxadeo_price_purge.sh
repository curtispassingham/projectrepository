#!/bin/sh
#----------------------------------------------------------------------------------------#
#  File   : xxadeo_price_purge.sh                                                        #
#  Desc   : This script purge records processed from staging tables                      #
#  Company: ADEO                                                                         #
#  Created: Elsa Barros                                                                  #
#  Date   : 04-07-2018                                                                   #
#----------------------------------------------------------------------------------------#

# Test for the number of input arguments
if [ $# -lt 3 ]
then
   echo 'USAGE: xxadeo_price_purge.sh </@tnsaliasname> <log_path> <error_path> <export_path(optional)> '
   exit 1
fi

# global variables

CONNECT=$1
LOGPATH=$2
ERRPATH=$3
EXPORTPATH=$4

pgmName=`basename $0`
pgmPID=$$                 # get the process ID

exeDate=`date +%Y%m%d%H%M%S`

if [ -d "$LOGPATH" ]; then
   if [ ! -w "$LOGPATH" ]; then
      echo 'You do not have write permissions to Log path $LOGPATH'
      exit 1
   fi
else
   echo 'Specified Log path does not exist'
   exit 1
fi

if [ -d "$ERRPATH" ]; then
   if [ ! -w "$ERRPATH" ]; then
      echo 'You do not have write permissions to Error path $ERRPATH'
      exit 1
   fi
else
   echo 'Specified Error path does not exist'
   exit 1
fi

### Check if path where extracts are to be created is passed as arg.
### If this arg is Null, then default to PWD.

if [ "$EXPORTPATH" == "" ]
then
   EXPORTPATH=`pwd`
   echo "Export Path is not sent. Defaulting to PWD: "`pwd`
else
   if [ -d "$EXPORTPATH" ]; then
      if [ ! -w "$EXPORTPATH" ]; then
         echo 'You do not have write permissions to Export path $EXPORTPATH'
         exit 1
      fi
   else
      echo 'Specified Export path does not exist'
      exit 1
   fi
fi

echo 'Running xxadeo_price_purge Batch'
echo 'LOGPATH=' $LOGPATH
echo 'ERRPATH=' $ERRPATH
echo 'EXPORTPATH=' $EXPORTPATH

LOGFILE=$LOGPATH'/'$pgmName'_'$exeDate'.log'
ERRFILE=$ERRPATH'/err_'$pgmName'_'$exeDate'.log'
FAILFILE='${pgmName}_${exeDate}.fail'
PUBLIST='${pgmName}_${exeDate}.lst'

echo 'LOGFILE=' $LOGFILE
echo 'ERRFILE=' $ERRFILE

###############################################################################################

### Check if err file already exists. If yes, rename it.
if [ -e $ERRFILE ]
then
   mv -f $ERRFILE $ERRFILE.`date +%H%M%S`
fi

### Check if log file already exists. If yes, rename it.
if [ -e $LOGFILE ]
then
   mv -f $LOGFILE $LOGFILE.`date +%H%M%S`
fi


USAGE="Usage: `basename $0`  <connect>\n"

#-------------------------------------------------------------------------
# Function Name: LOG_ERROR
# Purpose      : Log the error messages to the error file.
#-------------------------------------------------------------------------
function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: LOG_MESSAGE
# Purpose      : Log the messages to the log file.
#-------------------------------------------------------------------------
function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE
   return $retCode
}

#-------------------------------------------------------------------------------
# Function Name: PRICE_PURGE_PROCESS
# Purpose      : calls the package XXADEO_PRICE_INJECTOR_SQL.PRICE_PURGE_PROCESS
#-------------------------------------------------------------------------------
function PRICE_PURGE_PROCESS
{

   echo "set serveroutput off
         set feedback off
         set heading off
         set trimspool on
         set pagesize 0
         set linesize 7000
                 
         variable o_error_message char(2000);
       DECLARE		 
         --
         PACKAGE_EXCEPTION EXCEPTION; 
         --
         L_error_message VARCHAR2(2000) := null;
         --     
       BEGIN
         --
		 
		 IF XXADEO_PRICE_INJECTOR_SQL.PRICE_PURGE_PROCESS(O_error_message    => L_error_message) = FALSE THEN
           --
           RAISE PACKAGE_EXCEPTION;
           --
         END IF;
         --
         COMMIT;
         --     
       EXCEPTION     
         WHEN PACKAGE_EXCEPTION THEN
           :o_error_message:=SUBSTR(L_error_message, 1, 255); 
         WHEN OTHERS THEN
           ROLLBACK;
           :o_error_message:='@Error :'||SUBSTR(SQLERRM, 1, 247);
       END;
	   /
       " | $ORACLE_HOME/bin/sqlplus -s $CONNECT | awk '{print $0}' |read ERROR_MESSAGE    
	  
    if [[ -n ${ERROR_MESSAGE} ]]; then
       LOG_MESSAGE "Aborted in process."          
       echo -e  "" >> $LOGFILE
       echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg"          
       echo -e  "${ERROR_MESSAGE}\n" >> $ERRFILE          
       
       return ${FATAL}
    fi
	
    
    return ${OK}
}

#-----------------------------------------------
# Main program starts
# Parse the command line
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   echo $USAGE
   exit 1
fi

CONNECT=$1
echo $CONNECT
#Check Oracle Connectivity 
. ./set_env_db.sh 
echo $ORACLE_HOME 

output=$($ORACLE_HOME/bin/sqlplus $CONNECT < /dev/null | grep 'Connected to' | wc -l) 
echo ${output}
if [ ${output} -eq 0 ];then 
   LOG_MESSAGE "Logon Error" 
   echo  -e "" >> ${LOGFILE} 
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg" 
   exit ${FATAL} 
fi 

USER=`sqlplus -s $CONNECT << EOF
      set heading off;
      set pagesize 0;
      select user from dual;
      exit;
EOF`
	  
LOG_MESSAGE "Started by ${USER}"

PRICE_PURGE_PROCESS
exit ${OK} 