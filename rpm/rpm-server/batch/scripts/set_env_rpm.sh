
echo "Set the RPM Middleware profile"

#Middleware Variables


PATH=$PATH:$HOME/bin

export PATH
export MW_HOME=/u01/product/rms/fmw_12.2.1.2
export ORACLE_HOME=$MW_HOME
export WLS_HOME=$MW_HOME/wlserver
export WLS_DOMAINS=/u01/data/domains
export DOMAIN_HOME=/u01/data/domains/RPMDomain
export LD_LIBRARY_PATH=$ORACLE_HOME/lib
export WEBLOGIC_DOMAIN_HOME=/u01/data/domains/RPMDomain



#export ORACLE_SID=D015RMSH
export ORACLE_SID=svc_dev01_sys_rms
export NLS_NUMERIC_CHARACTERS=.,
export NLS_DATE_FORMAT=DD-MON-RR
export NLS_SORT=binary
export NLS_LANG=American_America.AL32UTF8


export JAVA_HOME=/u01/product/jdk/jdk1.8.0_131
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/X11R6/lib:/usr/openwin/lib:/usr/dt/lib:$LD_LIBRARY_PATH
export PATH=$JAVA_HOME/bin:$WLS_HOME/server/bin:$DOMAIN_HOME/bin:$ORACLE_INSTANCE/bin:$ORACLE_HOME/bin:$PATH

export TNS_NAMES=$ORACLE_HOME/network/admin
export J2EE_ORACLE_HOME=$MW_HOME
export J2EE_DOMAIN_HOME=$DOMAIN_HOME
export LOG_DIR=/u01/logs/dev01/$HOSTNAME
export SCRIPT_DIR=/u01/scripts/dev01/$HOSTNAME
export RETAIL_HOME=/u01/data/app/retail_home_rpm


# ORC - For RPM batch
export ORACLE_HOME_DB=/u01/data/app/client
export ORACLE_HOME_app=$MW_HOME
export PATH=$ORACLE_HOME_DB/bin:$PATH
export TNS_ADMIN=/u01/data/app/retail_home_rpm/rpm-batch/.wallet
export LD_LIBRARY_PATH=$ORACLE_HOME_DB/lib:$LD_LIBRARY_PATH

