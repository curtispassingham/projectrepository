set termout off;
set newpage 0;
set space 0;
set linesize 255;
set pagesize 0;
set trimspool on;
set echo off;
set feedback off;
set heading off;
set verify off;

WHENEVER SQLERROR EXIT SQL.SQLCODE

VARIABLE loc_and_type  VARCHAR2(255);
VARIABLE loc_type      VARCHAR2(1);
VARIABLE location      NUMBER;
VARIABLE trans_ts_date VARCHAR2(30);
VARIABLE file_name1    VARCHAR2(255);

------------------------------------------------
--- Get the full file name for spooling
------------------------------------------------

BEGIN

   :file_name1 := '&1';
   :loc_and_type := '&2';

   :location := SUBSTR(:loc_and_type, 1, INSTR(:loc_and_type, '_')-1);
   :loc_type := SUBSTR(:loc_and_type, INSTR(:loc_and_type, '_')+1, LENGTH(:loc_and_type));

   select TO_CHAR(SYSDATE, 'YYYYMMDDHHMISS')
     into :trans_ts_date
     from dual;

END;
/
------------------------------------------------
--- Spool the message
------------------------------------------------

column filename new_value outfile noprint;

select :file_name1 filename from dual;

SPOOL &outfile;

select record_descriptor || '|' ||
       rownum ||
       DECODE(record_descriptor,
              'TTAIL', NULL,
              '|') ||
       DECODE(record_descriptor,
              'FTAIL', TO_CHAR(rownum - 2),
              message)
  from (select row_id,
               record_descriptor,
               message
          from (select 0 row_id,
                       'FHEAD' record_descriptor,
                       'PRMPC'||'|'||
                       :trans_ts_date||'|'||
                       :location||'|'||
                       :loc_type message
                  from dual
                union all
                select (t.max_rowid * t.thread_number + t.row_id) row_id,
                        t.record_descriptor,
                        t.message
                  from (select rpd.row_id,
                               rpd.record_descriptor,
                               rpd.message,
                               rpd.thread_number,
                               MAX(row_id) OVER() max_rowid
                          from rpm_price_publish_data rpd
                         where rpd.event_family  = 'PrmPrcChg'
                           and rpd.location      = :location
                           and rpd.location_type = :loc_type
                         order by rpd.thread_number,
                                  rpd.row_id) t
                union all
                select 99999999999999999999 row_id,
                       'FTAIL' record_descriptor,
                       NULL message
                  from dual)
                 order by row_id);

SPOOL OFF;

EXIT;
/
