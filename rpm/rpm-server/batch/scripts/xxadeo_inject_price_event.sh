#!/bin/sh  
#----------------------------------------------------------------------------------------# 
#  File   : xxadeo_inject_price_event.sh                                                 # 
#  Desc   : This script will inject price event on RPM stagging from XXADEO_RMS schema   # 
#  Company: XXADEO_RMS                                                                   # 
#  Created: Elsa Barros                                                                  # 
#  Date   : 27-06-2018                                                                   # 
#----------------------------------------------------------------------------------------# 
 
# Test for the number of input arguments 
if [ $# -lt 5 ] 
then 
   echo 'USAGE: xxadeo_inject_price_event.sh <price event([pc|pc_e|pc_r|cl|sp|cp]) > </@tnsaliasname> </@rpmaliasname> <log_path> <error_path> <export_path(optional)> ' 
   exit 1 
fi 
 
# global variables 
 
PRICE_EVENT=`echo $1 | tr "[a-z]" "[A-Z]"` 
 
 
CONNECT=$2 
rpm_alias=$3
LOGPATH=$4 
ERRPATH=$5 
EXPORTPATH=$6 
 
pgmName=`basename $0` 
pgmPID=$$                 # get the process ID 
 
exeDate=`date +%Y%m%d%H%M%S`   # get the execution date 
 
if [ -d "$LOGPATH" ]; then 
   if [ ! -w "$LOGPATH" ]; then 
      echo 'You do not have write permissions to Log path $LOGPATH' 
      exit 1 
   fi 
else 
   echo 'Specified Log path does not exist' 
   exit 1 
fi 
 
if [ -d "$ERRPATH" ]; then 
   if [ ! -w "$ERRPATH" ]; then 
      echo 'You do not have write permissions to Error path $ERRPATH' 
      exit 1 
   fi 
else 
   echo 'Specified Error path does not exist' 
   exit 1 
fi 
 
### Check if path where extracts are to be created is passed as arg. 
### If this arg is Null, then default to PWD. 
 
if [ "$EXPORTPATH" == "" ] 
then 
   EXPORTPATH=`pwd` 
   echo "Export Path is not sent. Defaulting to PWD: "`pwd` 
else 
   if [ -d "$EXPORTPATH" ]; then 
      if [ ! -w "$EXPORTPATH" ]; then 
         echo 'You do not have write permissions to Export path $EXPORTPATH' 
         exit 1 
      fi 
   else 
      echo 'Specified Export path does not exist' 
      exit 1 
   fi 
fi 
 
echo 'Running xxadeo_inject_price_event Batch' 
echo 'LOGPATH=' $LOGPATH 
echo 'ERRPATH=' $ERRPATH 
echo 'EXPORTPATH=' $EXPORTPATH 
 
LOGFILE=$LOGPATH'/'$pgmName'_'$exeDate'.log' 
ERRORFILE=$ERRPATH'/'$pgmName'_'$exeDate'.err' 
FAILFILE='${pgmName}_${exeDate}.fail' 
PUBLIST='${pgmName}_${exeDate}.lst' 

ERRINDFILE=err.ind 
OK=0 
FATAL=255 
NON_FATAL=1 
 
 
echo 'LOGFILE=' $LOGFILE 
echo 'ERRORFILE=' $ERRORFILE 
 
 
 
 
 
USAGE="Usage: `basename $0`  <connect>\n" 
 
 
# Starting the function write_log() 
function WRITE_LOG 
{ 
   #The string manipulation below is to get the loc id that only pull the numeric value 
   #from the second parameter which contains the loc id and loc type in a format "123_S" 
 
   LOC_TYPE=$(echo $2 | sed 's/[^0-9]*//g') 
 
   THREAD_ERR_FILE=$ERRORFILE"_WRITE_LOG_"$LOC_TYPE 
 
   `$ORACLE_HOME/bin/sqlplus -s $CONNECT >> $THREAD_ERR_FILE <<EOF 
   set serveroutput on; 
   whenever sqlerror exit 1; 
 
   DECLARE 
 
      L_error_msg         VARCHAR2(500) := NULL; 
      L_return            NUMBER        := NULL; 
      L_start_date        DATE          := NULL; 
      L_end_date          DATE          := NULL; 
      L_status            VARCHAR2(50)  := '$1'; 
      L_thread_num        NUMBER        := '$LOC_TYPE'; 
      L_log_path          VARCHAR2(500) := NULL; 
      L_error_path        VARCHAR2(500) := NULL; 
      L_export_path       VARCHAR2(500) := NULL; 
 
   BEGIN 
 
      if L_status = 'STARTED' then 
 
         L_status := NULL; 
         L_start_date := SYSDATE; 
 
      else 
         L_end_date := SYSDATE; 
      end if; 
 
      L_return := RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY(L_error_msg, 
                                                              'xxadeo_inject_price_event.sh', 
                                                              USER, 
                                                              NULL, -- luw_value 
                                                              1, -- concurrent_threads 
                                                              NULL, 
                                                              L_log_path, 
                                                              L_error_path, 
                                                              L_export_path, 
                                                              L_thread_num, -- I_thread_number 
                                                              L_start_date, 
                                                              L_end_date, 
                                                              L_status); 
 
      if L_return = 0 then 
         DBMS_OUTPUT.PUT_LINE('SQL Error when calling RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY: '|| L_error_msg); 
         return; 
      end if; 
 
      return; 
 
   EXCEPTION 
      when OTHERS then 
         DBMS_OUTPUT.PUT_LINE('SQL Error when calling RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY: '||SQLERRM); 
      return; 
 
   END; 
   / 
   exit; 
EOF` 
 
#check if there is error 
if [ `grep "SP2-" $THREAD_ERR_FILE | wc -l` -gt 0 ] 
then 
   echo "SP2 Error in write_log function" >> $LOGFILE 
   cat $THREAD_ERR_FILE >> $ERRORFILE 
   rm $THREAD_ERR_FILE 
   exit 1 
fi 
 
if [ `grep "ORA-" $THREAD_ERR_FILE | wc -l` -gt 0 ] 
then 
   echo "ORA Error in write_log function" >> $LOGFILE 
   cat $THREAD_ERR_FILE >> $ERRORFILE 
   rm $THREAD_ERR_FILE 
   exit 1 
fi 
 
rm $THREAD_ERR_FILE 
} 
 
#end write_log function 
############################################################################################### 
 
### Check if err file already exists. If yes, rename it. 
if [ -e $ERRORFILE ] 
then 
   mv -f $ERRORFILE $ERRORFILE.`date +%H%M%S` 
fi 
 
### Check if log file already exists. If yes, rename it. 
if [ -e $LOGFILE ] 
then 
   mv -f $LOGFILE $LOGFILE.`date +%H%M%S` 
fi 
 
 
 
#------------------------------------------------------------------------- 
# Function Name: LOG_ERROR 
# Purpose      : Log the error messages to the error file. 
#------------------------------------------------------------------------- 
function LOG_ERROR 
{ 
   errMsg=`echo $1` 
   errFunc=$2 
   retCode=$3 
   dtStamp=`date +"%G%m%d%H%M%S"` 
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> ${ERRORFILE} 
   if [[ $retCode -eq ${FATAL} ]]; then 
      LOG_MESSAGE "Aborted in" $errFunc $retCode 
   fi 
return $retCode 
} 
 
#------------------------------------------------------------------------- 
# Function Name: LOG_MESSAGE 
# Purpose      : Log the  messages to the log file. 
#---------------------G---------------------------------------------------- 
function LOG_MESSAGE 
{ 
logMsg=`echo $1` 
logFunc=$2 
retCode=$3 
dtStamp=`date +"%a %b %e %T"` 
echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> ${LOGFILE} 
return $retCode 
} 
 
 
#------------------------------------------------------------------------- 
# Function Name: RUN_BATCH_PRE_INJECTOR 
# Purpose      : This function will run price event pre injector 
#------------------------------------------------------------------------- 
function RUN_BATCH_PRE_INJECTOR 
{ 
 
   LOG_MESSAGE  "${pgmName}.${pgmExt} RUN_BATCH_PRE_INJECTOR started" "RUN_BATCH_PRE_INJECTOR" ${OK} ${LOGFILE} ${pgmName} ${pgmPID} 
  
	
	
   OUTPUT_MESSAGE=`$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
                   set serveroutput off 
   				   set feedback off 
   				   set heading off 
   				   set trimspool on 
   				   set pagesize 0 
   				   set linesize 7000 
   				   
   				   VARIABLE O_result_message  CHAR(2000);    
   				   
   				   DECLARE		  
   				   -- 
   				   PACKAGE_EXCEPTION EXCEPTION;
				   NO_PRICE_CHANGE   EXCEPTION;
   				   -- 
   				   L_error_message      VARCHAR2(2000); 
   				   L_batch_event_type   VARCHAR2(10);   
   				   L_batch_event_action VARCHAR2(10); 
   				   --      
   				   BEGIN 
   				   -- 
   				   L_batch_event_type   := '${PRICE_EVENT}'; 
   				   -- 
   				   IF XXADEO_PRICE_INJECTOR_SQL.RUN_BATCH_PRE_INJECTOR(O_error_message        => L_error_message, 
   				   												       IO_price_event_type    => L_batch_event_type, 
   				   													   IO_price_event_action  => L_batch_event_action) = FALSE THEN 
   				     --
                     if L_error_message = 'There are no Price changes.'	then
                       --
					   RAISE NO_PRICE_CHANGE;
                       --
                     end if;					   
					 --
   				   	 RAISE PACKAGE_EXCEPTION; 
   				   	 -- 
   				   END IF; 
   				   -- 
   				   :O_result_message:= nvl(L_batch_event_action,'E')||'#'||nvl(L_batch_event_type,'E')||'#-1'; 
   				   -- 
   				   COMMIT; 
   				   --      
   				   EXCEPTION      
				     WHEN NO_PRICE_CHANGE THEN
					   :O_result_message:='-1#-2#'||SUBSTR(L_error_message, 1, 255);
   				     WHEN PACKAGE_EXCEPTION THEN 
   				   	   :O_result_message:='-1#-1#'||SUBSTR(L_error_message, 1, 255);  
   				   	   ROLLBACK; 
   				     WHEN OTHERS THEN 
   				   	   ROLLBACK; 
   				   	   :O_result_message:='-1#-1#' ||'@Error :'||SUBSTR(SQLERRM, 1, 247); 
   				     END; 
   				     / 
   				     PRINT :O_result_message;  
EOF`
	 
     if [[ -n ${OUTPUT_MESSAGE} ]]; then 
	   
       set -f      
       array=(${OUTPUT_MESSAGE//#/ })  
	   
       if [[ ${array[0]} = "E" ]]; then  
           LOG_MESSAGE  "${pgmName}.${pgmExt} RUN_BATCH_PRE_INJECTOR Nothing to process Return" "RUN_BATCH_PRE_INJECTOR" ${OK} ${LOGFILE} ${pgmName} ${pgmPID} 
           WRITE_LOG FAILED ${OUTPUT_MESSAGE} 
		   echo  "norequests" 
           return ${OK} 
       fi        
       
	   if [[ ${array[1]} = -2 ]]; then    	   
           LOG_MESSAGE  "${pgmName}.${pgmExt} RUN_BATCH_PRE_INJECTOR There are no Price changes to process." "RUN_BATCH_PRE_INJECTOR" ${OK} ${LOGFILE} ${pgmName} ${pgmPID} 
           WRITE_LOG FAILED ${OUTPUT_MESSAGE} 
		   echo  "norequests"     
           return ${OK}  
       fi 	
	   
	   if [[ ${array[1]} = -1 ]]; then    	   
           LOG_MESSAGE  "${pgmName}.${pgmExt} RUN_BATCH_PRE_INJECTOR Aborted in process." "RUN_BATCH_PRE_INJECTOR" ${OK} ${LOGFILE} ${pgmName} ${pgmPID} 
           WRITE_LOG FAILED ${OUTPUT_MESSAGE} 
		   echo -e  "" >> $LOGFILE 
           echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg"           
           echo -e  ${OUTPUT_MESSAGE} >> $ERRORFILE           
              
           return ${FATAL} 
       fi 
	   
       if [[ ${array[2]} -ne -1 ]]; then    	   
           LOG_MESSAGE  "${pgmName}.${pgmExt} RUN_BATCH_PRE_INJECTOR Aborted in process." "RUN_BATCH_PRE_INJECTOR" ${OK} ${LOGFILE} ${pgmName} ${pgmPID} 
           WRITE_LOG FAILED ${OUTPUT_MESSAGE} 
		   echo -e  "" >> $LOGFILE 
           echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg"           
           echo -e  ${OUTPUT_MESSAGE} >> $ERRORFILE           
              
           return ${FATAL} 
       fi 
      
       echo  "status=${array[0]} event_type=${array[1]}" 
	   
         
    fi 
	 
    return ${OK} 
} 
 
#------------------------------------------------------------------------- 
# Function Name: RUN_BATCH_POST_INJECTOR 
# Purpose      : This function will run price event post injector 
#------------------------------------------------------------------------- 
function RUN_BATCH_POST_INJECTOR 
{ 
 
   LOG_MESSAGE  "${pgmName}.${pgmExt} RUN_BATCH_POST_INJECTOR started" "RUN_BATCH_POST_INJECTOR" ${OK} ${LOGFILE} ${pgmName} ${pgmPID} 
 
   ERROR_MESSAGE=`$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
                  set serveroutput off 
                  set feedback off 
                  set heading off 
                  set trimspool on 
                  set pagesize 0 
                  set linesize 7000 
         
                  VARIABLE O_error_message   CHAR(2000);          
                  
                DECLARE		  
                  -- 
                  PACKAGE_EXCEPTION EXCEPTION;  
                  -- 
                  L_error_message VARCHAR2(2000); 
                  --      
                BEGIN 
                  -- 
                  IF XXADEO_PRICE_INJECTOR_SQL.RUN_BATCH_POST_INJECTOR(O_error_message    => L_error_message) = FALSE THEN 
                    -- 
                    RAISE PACKAGE_EXCEPTION; 
                    -- 
                  END IF; 
                  -- 
                  COMMIT; 
                  --      
                EXCEPTION      
                  WHEN PACKAGE_EXCEPTION THEN 
                    :O_error_message:=SUBSTR(L_error_message, 1, 255); 
                    ROLLBACK; 
                  WHEN OTHERS THEN 
                    ROLLBACK; 
                    :O_error_message:='@Error :'||SUBSTR(SQLERRM, 1, 247); 
                END; 
                / 
                PRINT :O_error_message; 
EOF`
 
    if [[ -n ${ERROR_MESSAGE} ]]; then 
       LOG_MESSAGE "Aborted in process."           
       WRITE_LOG FAILED ${ERROR_MESSAGE} 
	   echo -e  "" >> $LOGFILE 
       echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg"           
       echo -e  "${ERROR_MESSAGE}\n" >> $ERRORFILE           
        
       return ${FATAL} 
    fi 
     
    return ${OK} 
} 
 
#------------------------------------------------------------------------- 
# Function Name: MAIN_PROCESS 
# Purpose      : Function to spool the files to the file system 
#------------------------------------------------------------------------- 
function MAIN_PROCESS 
{    
   #Runs pre injector 
   . ./set_env_db.sh 
    
   result="$(RUN_BATCH_PRE_INJECTOR)" 
   
   if [[ $? -ne ${OK} ]]; then
       return ${FATAL}   
   fi
   
   if [ "$result" = "norequests" ]; then
       return ${NON_FATAL}
   fi
   
   #call run injector 
   . ./set_env_rpm.sh 
  
   
   ./injectorPriceEventBatch.sh $rpm_alias $result 
  
   #Runs post injector 
   . ./set_env_db.sh 

   
   RUN_BATCH_POST_INJECTOR   

   
   if [[ $? -ne ${OK} ]]; then 

   
	return ${FATAL}    
   fi 

   
   return ${OK} 
} 
 
#----------------------------------------------- 
# Main Program Starts 
# Parse the command line 
#----------------------------------------------- 
 
# Test for the number of input arguments 
if [ $# -lt 1 ] 
then 
   echo $USAGE 
   exit ${FATAL} 
fi 
 
CONNECT=$2 
PRICE_EVENT=$1

#Check Oracle Connectivity 
. ./set_env_db.sh 
echo $ORACLE_HOME 

output=$($ORACLE_HOME/bin/sqlplus $CONNECT < /dev/null | grep 'Connected to' | wc -l) 

if [ ${output} -eq 0 ];then 
   LOG_MESSAGE "Logon Error" 
   echo  -e "" >> ${LOGFILE} 
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg" 
   exit ${FATAL} 
fi 

USER=`sqlplus -s $CONNECT << EOF
      set heading off;
      set pagesize 0;
      select user from dual;
      exit;
EOF`

#Check if another instance is running 
opid=`pgrep -f $0` 
opid_count=`wc -w <<< $opid` 
if [ $opid_count -gt 1 ]; 
then 
    LOG_MESSAGE  "${pgmName}.${pgmExt} Program already running with pid $opid. Exiting No Error" "MAIN" ${OK} ${LOGFILE} ${pgmName} ${pgmPID} 
    exit ${OK} 
fi 
 
# Log Begin Work 
LOG_MESSAGE "Started xxadeo_inject_price_event.sh by ${USER}" 
echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg" 
 
 
while true 
do 
 
	# Call Process 
	
	MAIN_PROCESS
	 
	resultMain=`echo $?` 
	LOG_MESSAGE "RPM Batch injectorPriceEventBatch.sh result: $resultMain" 
	 
	if [[ $resultMain -ne ${OK} ]]; then 
	 
		if [[ $resultMain -eq ${FATAL} ]]; then 
		   LOG_MESSAGE "Terminated with Fatal error xxadeo_inject_price_event.sh by ${USER} -> ${FATAL}" 
		   echo "Terminated with Fatal error xxadeo_inject_price_event.sh by ${USER} -> ${FATAL}" 
		   exit ${FATAL} 
		    
		else 
		    
		   #Log finished work 
		   LOG_MESSAGE "Terminated Successfully xxadeo_inject_price_event.sh by ${USER}" 
		   echo "Terminated Successfully xxadeo_inject_price_event.sh by ${USER}" 
		   echo  -e "" >> ${LOGFILE} 
		   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg" 
		   exit ${OK} 
		    
		fi 
		 
	fi 
	 
done 
 
exit ${OK} 
 
 
 
 
