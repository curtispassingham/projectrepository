#!/bin/sh
# a shell script for launching java batch programs in RPM
# usage: launchRpmBatch.sh <batch_class_name> <user_alias>

### this property should be changed to the appropriate value for your environment
PROVIDER_URL=-Djava.naming.provider.url=@jndi.naming.server.url@
###

### these properties should not need to change
AUTH_LOGIN=-Djava.security.auth.login.config=jaas_client.conf
INITIAL_CONTEXT=-Djava.naming.factory.initial=weblogic.jndi.WLInitialContextFactory
###

BATCH_CLASSNAME=$1
BATCH_USER_ALIAS=$2
OPTIONAL_1=$3
OPTIONAL_2=$4

if [ -z "$BATCH_CLASSNAME" ] ; then
    echo "You must specify a batch class."
    exit 1
fi

if [ -z "$BATCH_USER_ALIAS" ] ; then
    echo "You must specify a user alias."
    exit 1
fi

if [ -z "$JAVA_HOME" ] ; then
    echo "You must define \$JAVA_HOME.  It is currently not defined."
    exit 1
fi

if [ -z "$ORACLE_HOME" ] ; then
    echo "You must define \$ORACLE_HOME.  It is currently not defined."
    exit 1
fi

JARS=`find ../lib -name \*.jar`

for i in $JARS; do
   CLASSPATH=$CLASSPATH:$i
done

JPSJARS=`find $ORACLE_HOME/oracle_common/modules/oracle.jps* -name \*jps\*.jar`

if [ -z "$JPSJARS" ] ; then
    echo "Unable to find Jps jars."
    exit 1
fi

for i in $JPSJARS; do
	CLASSPATH=$CLASSPATH:$i
done

FMWJAR=`find $ORACLE_HOME/oracle_common/modules/oracle.iau* -name fmw_audit.jar`

if [ -z "$FMWJAR" ] ; then
    echo "Unable to find fmw_audit.jar."
    exit 1
fi

for i in $FMWJAR; do
	CLASSPATH=$CLASSPATH:$i
done

CLASSPATH=`echo $CLASSPATH | sed 's#^:##g'`

echo Classpath ...
echo $CLASSPATH


CLASSPATH=$CLASSPATH:../conf
SYSPROPS="$PROVIDER_URL $AUTH_LOGIN $CORBA_UTIL $INITIAL_CONTEXT"
VMARGS="-Xmx512M -Xms128m -classpath $CLASSPATH"

CMD="$JAVA_HOME/bin/java  $VMARGS $SYSPROPS $BATCH_CLASSNAME $BATCH_USER_ALIAS $OPTIONAL_1 $OPTIONAL_2"

echo $CMD

$CMD
