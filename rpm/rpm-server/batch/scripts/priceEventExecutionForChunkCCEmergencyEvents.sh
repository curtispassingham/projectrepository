#!/bin/ksh

# Test for the number of input arguments
if [ $# -lt 3 ]
then
   echo 'USAGE: priceEventExecutionForChunkCCEmergencyEvents.sh <connection_alias> <log_path> <error_path>'
   exit 1
fi

CONNECT=$1
LOGPATH=$2
ERRPATH=$3

DUMMY_FILE=priceEventExecutionForChunkCCEmergencyEvents.dummy.out
FAIL_FILE='priceEventExecutionForChunkCCEmergencyEvents'`date +'%b_%d_%Y'`'.fail'

if [ -z "$CONNECT" ] ; then
   echo 'You must specify a TNS Alias /</@tnsaliasname>'
   exit 1
fi

if [ -d "$LOGPATH" ]; then
   if [ ! -w "$LOGPATH" ]; then
      echo 'You do not have write permissions to Log path $LOGPATH'
      exit 1
   fi
else
   echo 'Specified Log path does not exist'
   exit 1
fi

if [ -d "$ERRPATH" ]; then
   if [ ! -w "$ERRPATH" ]; then
      echo 'You do not have write permissions to Error path $ERRPATH'
      exit 1
   fi
else
   echo 'Specified Error path does not exist'
   exit 1
fi

echo 'Running priceEventExecutionForChunkCCEmergencyEvents Batch'
echo 'LOGPATH=' $LOGPATH
echo 'ERRPATH=' $ERRPATH

LOGFILE=$LOGPATH'/priceEventExecutionForChunkCCEmergencyEvents'`date +'%b_%d_%Y'`'.log'
ERRFILE=$ERRPATH'/err.priceEventExecutionForChunkCCEmergencyEvents.'`date +'%b_%d_%Y'`

echo 'LOGFILE=' $LOGFILE
echo 'ERRFILE=' $ERRFILE

# Check if err file already exists. If yes, rename it.
if [ -f $ERRFILE ]
then
   mv -f $ERRFILE $ERRFILE.`date +%H%M%S`
fi

# Check if log file already exists. If yes, rename it.
if [ -f $LOGFILE ]
then
   mv -f $LOGFILE $LOGFILE.`date +%H%M%S`
fi

###############################################################################
# Starting the function write_log()
write_log()
{
   WR_ERR_FILE=$ERRFILE"_WRITE_LOG_"$3

   `$ORACLE_HOME/bin/sqlplus -s $CONNECT >> $WR_ERR_FILE <<EOF
   set serveroutput on;
   whenever sqlerror exit 1;

   DECLARE

      L_error_msg        VARCHAR2(500) := NULL;
      L_return           NUMBER        := NULL;
      L_status           VARCHAR2(50)  := '$1';
      L_function_name    VARCHAR2(50)  := '$2';
      L_thread_num       NUMBER        := '$3';
      L_luw              NUMBER        := '$4';
      L_concurent_thread NUMBER        := '$5';
      L_log_path         VARCHAR2(500) := NULL;
      L_error_path       VARCHAR2(500) := NULL;
      L_start_date       DATE          := NULL;
      L_end_date         DATE          := NULL;

   BEGIN

      if L_function_name is NOT NULL then
         L_function_name := '-'||L_function_name;
      end if;

      if L_status = 'STARTED' then
         L_status := NULL;
         L_start_date := SYSDATE;

         if L_function_name is NULL then
            L_log_path := '$LOGPATH';
            L_error_path := '$ERRPATH';
         end if;

      elsif L_status = 'THREAD_STARTED' then
         L_status := NULL;
         L_start_date := SYSDATE;

      else
         L_end_date := SYSDATE;
         L_concurent_thread := NULL;
         L_luw := NULL;
      end if;

      L_return := RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY(L_error_msg,
                                                              'priceEventExecutionForChunkCCEmergencyEvents.sh'||L_function_name,
                                                              USER,
                                                              L_luw, -- luw_value
                                                              L_concurent_thread,
                                                              NULL,
                                                              L_log_path,
                                                              L_error_path,
                                                              NULL, -- export_path
                                                              L_thread_num,
                                                              L_start_date,
                                                              L_end_date,
                                                              L_status);

      if L_return = 0 then
         DBMS_OUTPUT.PUT_LINE('SQL Error when calling RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY: '|| L_error_msg);
         return;
      end if;

   EXCEPTION
      when OTHERS then
         DBMS_OUTPUT.PUT_LINE('SQL Error when calling RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY: '||SQLERRM);
   END;
   /
   exit
EOF`

#check if there is error
if [ `grep "SP2-" $WR_ERR_FILE | wc -l` -gt 0 ]
then
   echo "SP2 Error in write_log() function" >> $LOGFILE
   cat $WR_ERR_FILE >> $ERRFILE
   exit 1
fi

if [ `grep "ORA-" $WR_ERR_FILE | wc -l` -gt 0 ]
then
   echo "ORA Error in write_log() function" >> $LOGFILE
   cat $WR_ERR_FILE >> $ERRFILE
   exit 1
fi

rm $WR_ERR_FILE

}
#end write_log function

###############################################################################################

roll_back()
{
   THREAD_ERR_FILE=ERRFILE_$process_id
   echo "Rolling back for process_id: " $process_id >> $LOGFILE

   `$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $THREAD_ERR_FILE<<EOF
   whenever sqlerror exit 1;
   set serveroutput on format wrapped;
   set pagesize 0;
   set feedback off;

   BEGIN

      update rpm_chunk_cc_stage_pee
         set process_id = NULL
       where process_id = $process_id;

      commit;

      EXCEPTION
         when others then
            DBMS_OUTPUT.PUT_LINE('ERROR updating rpm_chunk_cc_stage_pee '||$process_id||':');
            DBMS_OUTPUT.PUT_LINE(SQLERRM);
            rollback;

   END;
   /
   exit
EOF`

   echo "Finished roll_back() for process_id: " $process_id >> $LOGFILE

   if [ `grep "ORA-" $THREAD_ERR_FILE | wc -l` -gt 0 ]
      then
         echo "ORA Error while roll_back() for process_id: " $process_id >> $LOGFILE
         cat $THREAD_ERR_FILE >> $ERRFILE
         rm $THREAD_ERR_FILE
         write_log FAILED
         exit 1
   fi

   if [ `grep "SP2-" $THREAD_ERR_FILE | wc -l` -gt 0 ]
      then
         echo "SP2 Error while roll_back for process_id: " $process_id >> $LOGFILE
         cat $THREAD_ERR_FILE >> $ERRFILE
         rm $THREAD_ERR_FILE
         write_log FAILED
         exit 1
   fi

   rm $THREAD_ERR_FILE

   if [ -f $FAIL_FILE ]
   then
      rm $FAIL_FILE
   fi

   write_log FAILED

   exit 1
}

###############################################################################

# Call funtion write_log
write_log STARTED

echo "----------------------------------------------------------------" >> $LOGFILE
echo "Processing started at: "`date +'%b %d %Y %H:%M:%S'`               >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE

`$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $ERRFILE <<EOF
whenever sqlerror exit 1;
set serveroutput on format wrapped;
set pagesize 0;
set feedback off;

variable max_thread_num NUMBER
variable thread_allowed NUMBER
variable item_loc_thread NUMBER
variable process_id NUMBER

DECLARE

  O_error_msg VARCHAR2(2000) := NULL;

  L_max_thread_num        NUMBER := 0;
  L_thread_allowed        NUMBER := 0;
  L_min_itemloc_in_thread NUMBER := 0;
  L_effective_date        DATE   := GET_VDATE;
  L_return                NUMBER := 0;
  L_process_id            NUMBER := RPM_CHUNK_CC_STAGE_PEE_PID_SEQ.NEXTVAL;

BEGIN

   select num_threads,
          thread_luw_count
     into L_thread_allowed,
          L_min_itemloc_in_thread
     from rpm_batch_control
    where program_name = 'com.retek.rpm.batch.PriceEventExecutionBatch';

   -- if the thread_luw_count is zero or NULL, set it to 10000
   if L_min_itemloc_in_thread is NULL or
      L_min_itemloc_in_thread = 0 then
      ---
      L_min_itemloc_in_thread := 10000;
   end if;

   update rpm_chunk_cc_stage_pee
      set process_id = L_process_id
    where process_id is NULL;

   if SQL%ROWCOUNT > 0 then

      L_return := RPM_PRICE_EVENT_SQL.INITIALIZE_PRICEEVENT_THREADS(O_error_msg,
                                                                    L_max_thread_num,
                                                                    L_min_itemloc_in_thread,
                                                                    L_effective_date,
                                                                    0,
                                                                    L_process_id);
      if L_return != 1 then
         DBMS_OUTPUT.PUT_LINE('SQL error while initialize price event threads - ' || O_error_msg);
         rollback;
         return;
      end if;

      COMMIT;

   end if;

   :max_thread_num := NVL(L_max_thread_num, 0);
   :thread_allowed := L_thread_allowed;
   :item_loc_thread := L_min_itemloc_in_thread;
   :process_id := L_process_id;

EXCEPTION
   when OTHERS then
      DBMS_OUTPUT.PUT_LINE('error: RPM_PRICE_EVENT_SQL.INITIALIZE_PRICEEVENT_THREADS ' || SQLERRM);
      rollback;
END;
/

spool $DUMMY_FILE
select :max_thread_num,
       :thread_allowed,
       :item_loc_thread,
       :process_id
  from dual;

spool off

exit

EOF`

if [ `grep "ORA-" $ERRFILE | wc -l` -gt 0 ]
then
   echo "ORA Error while initialize price event thread " >> $LOGFILE
   roll_back
fi

if [ `grep "SP2-" $ERRFILE | wc -l` -gt 0 ]
then
   echo "SP2 Error while threading the staged data " >> $LOGFILE
   roll_back
fi

while read var
   do
      max_thread_num=`echo $var | awk '{print $1}'`
      thread_allowed=`echo $var | awk '{print $2}'`
      item_loc_thread=`echo $var | awk '{print $3}'`
      process_id=`echo $var | awk '{print $4}'`
   done < $DUMMY_FILE

rm $DUMMY_FILE

# At this point, the error file should only have output from the spool above - clean that up...
rm $ERRFILE

if [ $max_thread_num -eq 0 ]
then
   echo "No Threads to process...  Exiting." >> $LOGFILE
   write_log SUCCESS
   rm $ERRFILE
   exit 0
fi

###############################################################################
# Starting the function process_thread  -- this will call the function RPM_PRICE_EVENT_SQL.PROCESS_PRICE_EVENT_LUWS()
process_thread()
{
   FTHREADNUM=$1
   THREAD_ERR_FILE=$ERRFILE"_"$FTHREADNUM
   echo "Started processing thread $FTHREADNUM: " `date +'%b %d %Y %H:%M:%S'` >> $LOGFILE

   write_log THREAD_STARTED PROCESS_PRICE_EVENT_LUWS $FTHREADNUM $item_loc_thread $thread_allowed

   `$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $THREAD_ERR_FILE <<EOF
   whenever sqlerror exit 1;
   set serveroutput on format wrapped;
   set pagesize 0;
   set feedback off;

   DECLARE

      L_error_msg VARCHAR2(2000) := NULL;
      L_return    NUMBER         := NULL;

   BEGIN

      L_return := RPM_PRICE_EVENT_SQL.PROCESS_PRICE_EVENT_LUWS(L_error_msg,
                                                               OBJ_NUMERIC_ID_TABLE($FTHREADNUM));

      if L_return = 0 then
         DBMS_OUTPUT.PUT_LINE('SQL Error while processing thread $FTHREADNUM: '|| L_error_msg);
         rollback;
         return;
      end if;

      commit;

      EXCEPTION
         when OTHERS then

            DBMS_OUTPUT.PUT_LINE('ERROR while processing thread '||$FTHREADNUM||':');
            DBMS_OUTPUT.PUT_LINE(sqlerrm);
            rollback;

   END;
   /
   exit
EOF`

   if [ `grep "ORA-" $THREAD_ERR_FILE | wc -l` -gt 0 ]
   then
      echo "ORA Error while processing PROCESS_PRICE_EVENT_LUWS for thread number: "$FTHREADNUM >> $LOGFILE
      cat $THREAD_ERR_FILE >> $ERRFILE
      rm $THREAD_ERR_FILE
      touch $FAIL_FILE
      write_log FAILED PROCESS_PRICE_EVENT_LUWS $FTHREADNUM
      exit 1
   fi

   if [ `grep "SP2-" $THREAD_ERR_FILE | wc -l` -gt 0 ]
   then
      echo "SP2 Error while processing PROCESS_PRICE_EVENT_LUWS for thread number: "$FTHREADNUM >> $LOGFILE
      cat $THREAD_ERR_FILE >> $ERRFILE
      rm $THREAD_ERR_FILE
      touch $FAIL_FILE
      write_log FAILED PROCESS_PRICE_EVENT_LUWS $FTHREADNUM
      exit 1
   fi

   echo "Finished processing PROCESS_PRICE_EVENT_LUWS for thread $FTHREADNUM: " `date +'%b %d %Y %H:%M:%S'` >> $LOGFILE
   write_log SUCCESS PROCESS_PRICE_EVENT_LUWS $FTHREADNUM

   cat $THREAD_ERR_FILE >> $ERRFILE
   rm $THREAD_ERR_FILE

   exit 0
}
#end of the function process_thread
##################################################################################

i=1

while [ $i -le $max_thread_num ]
do
   # verify the number of threads
   while true
   do

      NUM_JOBS=`jobs | wc -l`

      if [ $NUM_JOBS -lt $thread_allowed ]
      then
         break
      fi
   done

   ### Call fn. process_thread and send it to the background
   process_thread $i &

   i=`expr $i + 1`

done

# Wait for all of the threads to complete
wait

### Check if fail file already exists. If yes, stop processing
if [ -f $FAIL_FILE ]
then
   echo "Process_thread function encounter an error" >> $LOGFILE
   roll_back
fi

#######################################################################
#Processing Item Loc in RMS

`$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $ERRFILE <<EOF
whenever sqlerror exit 1;
set serveroutput on format wrapped;
set pagesize 0;
set feedback off;

variable max_il_thread_num NUMBER

DECLARE

  O_error_msg VARCHAR2(2000) := NULL;

  L_max_thread_num NUMBER := NULL;
  L_return         NUMBER := NULL;

BEGIN

   L_return := RPM_PRICE_EVENT_SQL.INITIALIZE_ITEMLOC_THREADS(O_error_msg,
                                                              L_max_thread_num,
                                                              $item_loc_thread);
   :max_il_thread_num  := NVL(L_max_thread_num, 0);

   if L_return != 1 then
      DBMS_OUTPUT.PUT_LINE('SQL error while initialize item loc threads - ' || O_error_msg);
      rollback;
      return;
   end if;

   commit;

   EXCEPTION
      when OTHERS then
         DBMS_OUTPUT.PUT_LINE('error: RPM_PRICE_EVENT_SQL.INITIALIZE_ITEMLOC_THREADS' || sqlerrm);
         rollback;
END;
/

spool $DUMMY_FILE
select :max_il_thread_num
  from dual;

spool off

exit

EOF`

if [ `grep "ORA-" $ERRFILE | wc -l` -gt 0 ]
then
   echo "ORA Error while calling INITIALIZE_ITEMLOC_THREADS" >> $LOGFILE
   roll_back
fi

if [ `grep "SP2-" $ERRFILE | wc -l` -gt 0 ]
then
   echo "SP2 Error while calling INITIALIZE_ITEMLOC_THREADS" >> $LOGFILE
   roll_back
fi

read max_il_thread_num < $DUMMY_FILE

rm $DUMMY_FILE

# At this point, the error file should only have output from the spool above - clean that up...
rm $ERRFILE

if [ $max_il_thread_num -eq 0 ]
then
   echo "No Item Loc Threads to process in INITIALIZE_ITEMLOC_THREADS...  Exiting." >> $LOGFILE
   write_log SUCCESS
   rm $ERRFILE
   exit 1
fi

echo "----Process Item Loc in RMS----" >> $LOGFILE

###############################################################################
# Starting the function process_il_luw_in_rms  -- this will call the function RPM_PRICE_EVENT_SQL.PROCESS_ITEM_LOC_LUWS_IN_RMS()
process_il_luw_in_rms()
{
   FTHREADNUM=$1
   THREAD_ERR_FILE=$ERRFILE"_"$FTHREADNUM

   echo "Started processing item loc thread in RMS $FTHREADNUM: " `date +'%b %d %Y %H:%M:%S'` >> $LOGFILE

   write_log THREAD_STARTED PROCESS_ITEM_LOC_LUWS_IN_RMS $FTHREADNUM

   `$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $THREAD_ERR_FILE <<EOF
   whenever sqlerror exit 1;
   set serveroutput on format wrapped;
   set pagesize 0;
   set feedback off;

   DECLARE

      L_error_msg VARCHAR2(2000) := NULL;
      L_return    NUMBER         := NULL;

   BEGIN

      L_return := RPM_PRICE_EVENT_SQL.PROCESS_ITEM_LOC_LUWS_IN_RMS(L_error_msg,
                                                                   OBJ_NUMERIC_ID_TABLE($FTHREADNUM));

      if L_return = 0 then

         DBMS_OUTPUT.PUT_LINE('SQL Error while processing item loc thread in RMS $FTHREADNUM: '|| L_error_msg);
         rollback;

         return;

      end if;

      commit;

      EXCEPTION
         when others then
            DBMS_OUTPUT.PUT_LINE('ERROR while processing item loc thread in RMS '||$FTHREADNUM||':');
            DBMS_OUTPUT.PUT_LINE(sqlerrm);

            rollback;

   END;
   /
   exit
EOF`

   if [ `grep "ORA-" $THREAD_ERR_FILE | wc -l` -gt 0 ]
   then
      echo "ORA Error while processing item loc thread in RMS thread number: "$FTHREADNUM >> $LOGFILE
      cat $THREAD_ERR_FILE >> $ERRFILE
      rm $THREAD_ERR_FILE
      touch $FAIL_FILE
      write_log FAILED PROCESS_ITEM_LOC_LUWS_IN_RMS $FTHREADNUM
      exit 1
   fi

   if [ `grep "SP2-" $THREAD_ERR_FILE | wc -l` -gt 0 ]
   then
      echo "SP2 Error while processing item loc thread in RMS thread number: "$FTHREADNUM >> $LOGFILE
      cat $THREAD_ERR_FILE >> $ERRFILE
      rm $THREAD_ERR_FILE
      touch $FAIL_FILE
      write_log FAILED PROCESS_ITEM_LOC_LUWS_IN_RMS $FTHREADNUM
      exit 1
   fi

   echo "Finished processing item loc thread in RMS $FTHREADNUM: " `date +'%b %d %Y %H:%M:%S'` >> $LOGFILE

   write_log SUCCESS PROCESS_ITEM_LOC_LUWS_IN_RMS $FTHREADNUM

   cat $THREAD_ERR_FILE >> $ERRFILE
   rm $THREAD_ERR_FILE

   exit 0
}
#end of the function process_il_luw_in_rms
##################################################################################

i=1

while [ $i -le $max_il_thread_num ]
do
   # verify the number of item loc threads
   while true
   do

      NUM_JOBS=`jobs | wc -l`

      if [ $NUM_JOBS -lt $thread_allowed ]
      then
         break
      fi
   done

   ### Call fn. process_il_luw_in_rms and send it to the background
   process_il_luw_in_rms $i &

   i=`expr $i + 1`

done

# Wait for all of the threads to complete
wait

### Check if fail file already exists. If yes, stop processing
if [ -f $FAIL_FILE ]
then
   echo "Process_thread function encounter an error" >> $LOGFILE
   roll_back
fi

##################################################################################
#Processing Deal Item Loc

echo "----Process Deal Item Loc----" >> $LOGFILE
echo "Started processing Deal item loc : " `date +'%b %d %Y %H:%M:%S'` >> $LOGFILE

`$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $ERRFILE <<EOF
whenever sqlerror exit 1;
set serveroutput on format wrapped;
set pagesize 0;
set feedback off;

DECLARE

  O_error_msg VARCHAR2(2000) := NULL;
  L_return    NUMBER         := NULL;

BEGIN

   L_return := RPM_PRICE_EVENT_SQL.PROCESS_ITEM_LOC_DEALS(O_error_msg);

   if L_return != 1 then
      DBMS_OUTPUT.PUT_LINE('SQL error while Process Item Loc Deals - ' || O_error_msg);
      rollback;
      return;
   end if;

   commit;

   EXCEPTION
      when OTHERS then
         DBMS_OUTPUT.PUT_LINE('error: RPM_PRICE_EVENT_SQL.PROCESS_ITEM_LOC_DEALS' || sqlerrm);
         rollback;
END;
/
exit;
EOF`

if [ `grep "ORA-" $ERRFILE | wc -l` -gt 0 ]
then
   echo "ORA Error while processing Deal Item Loc " >> $LOGFILE
   roll_back
fi

if [ `grep "SP2-" $ERRFILE | wc -l` -gt 0 ]
then
   echo "SP2 Error while processing Deal Item Loc " >> $LOGFILE
   roll_back
fi

echo "Finished processing Deal item loc : " `date +'%b %d %Y %H:%M:%S'` >> $LOGFILE

###############################################################################
#Clean up staging table

`$ORACLE_HOME/bin/sqlplus -s $CONNECT>> $ERRFILE <<EOF
whenever sqlerror exit 1;
set serveroutput on format wrapped;
set pagesize 0;
set feedback off;

BEGIN

   delete
     from rpm_chunk_cc_stage_pee
    where process_id = $process_id;

   commit;

   EXCEPTION
      when OTHERS then
         DBMS_OUTPUT.PUT_LINE('error when deleting rpm_chunk_cc_stage_pee' || sqlerrm);
         rollback;
END;
/
exit
EOF`

if [ `grep "ORA-" $ERRFILE | wc -l` -gt 0 ]
then
   echo "ORA Error when deleting rpm_chunk_cc_stage_pee " >> $LOGFILE
   write_log FAILED
   exit 1
fi

if [ `grep "SP2-" $ERRFILE | wc -l` -gt 0 ]
then
   echo "SP2 Error when deleting rpm_chunk_cc_stage_pee " >> $LOGFILE
   write_log FAILED
   exit 1
fi

# Call funtion write_log
write_log SUCCESS

rm $ERRFILE

echo "----------------------------------------------------------------" >> $LOGFILE
echo "Finished processing at: "`date +'%b %d %Y %H:%M:%S'`              >> $LOGFILE
echo "----------------------------------------------------------------" >> $LOGFILE

exit 0
