########################################################################
# Imported modules
########################################################################

import sys
import re

import java

import wl

########################################################################
# Example class
# This class serves as a template for future classes of weblogic commands.
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
########################################################################
class Example:

   def __init__(self):
      """Initialize a WebLogic MBean connection."""

   def printError(self):
      print '********************'
      print '* Unexpected error *'
      print '********************'
      wl.dumpStack()
      print '*********************************************************'
      print '* Please view the weblogic server logs for more details *'
      print '*********************************************************'

   def exampleMethod1(self, arg1, arg2):

   def exampleMethod2(self, arg1, arg2):
