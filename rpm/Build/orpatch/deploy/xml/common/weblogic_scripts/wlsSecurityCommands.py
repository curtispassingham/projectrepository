########################################################################
# Imported modules
########################################################################

import sys

import java.lang.String as String
import java.util.ArrayList as ArrayList
import java.util.HashMap as HashMap
import javax.management.MBeanException as MBeanException

import wl

########################################################################
# SecurityCommands class
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
########################################################################
class SecurityCommands:

   def __init__(self):
      """Initialize a WebLogic MBean connection."""

   # print an error message
   def printError(self):
      print '********************'
      print '* Unexpected error *'
      print '********************'
      wl.dumpStack()
      print '*********************************************************'
      print '* Please view the weblogic server logs for more details *'
      print '*********************************************************'

   # Get weblogic authentication object
   def getDefaultAuthenticator(self):
      """Get the default authenticator to perform security operations on."""
      return self.getDefaultRealm().lookupAuthenticationProvider("DefaultAuthenticator")

   # Get weblogic security realm object
   def getDefaultRealm(self):
      """Get the default realm to operate on."""
      return wl.cmo.getSecurityConfiguration().getDefaultRealm()

   # list all group members
   def getGroupUsers(self, group):
      users = ArrayList()
      groupLister = self.getDefaultAuthenticator()
      cursor = groupLister.listGroupMembers(group, "*", 0)
      while groupLister.haveCurrent(cursor):
         user = groupLister.getCurrentName(cursor)
         users.add(user)
         groupLister.advance(cursor)
      return users

   # Create 1 or more users.  users variable is of syntax: user/password:group-list,user/password:group-list,user/password:group-list,etc.
   # where group-list is group1#group2#group3 etc.
   # if no groups desired can just be user/password,user/password,user/password,etc. or user,user,user
   def createUsers(self, users):
      print 'Creating users from the input given'
      userEditor = self.getDefaultAuthenticator()
      pairlist = String(users).split(",")
      for pair in pairlist:
        tmplist = String(pair).split(":")
        userpasslist = String(tmplist[0]).split("/")
        user = userpasslist[0]
        if len(userpasslist)  > 1:
          password = userpasslist[1]
        else:
          password = "welcome1"
        if len(tmplist) > 1:
          groups = tmplist[1]
          print "Creating user '%s' with groups %s" % (user, groups)
          description = "User '%s' with groups %s" % (user, groups)
        else:
          groups = None
          print "Creating user '%s' with no groups" % (user)
          description = "User '%s' with no groups" % (user)
        if userEditor.userExists(user) == 0:
          print 'User '+user+' does not exist, and will be added.'
          try:
            userEditor.createUser(user, password, description)
          except:
            self.printError()
            print 'Could not create user '+user+'...'
            return 1
          if groups != None:
            grouplist = String(groups).split("#")
            for group in grouplist:
              if userEditor.groupExists(group) == 0:
                print 'Group '+group+' does not exist, user '+user+' will not be added.'
              else:
                try:
                  print 'Group '+group+' does exists, attempting to add user '+user+'...'
                  if userEditor.isMember(group,user,1) > 0:
                    print 'User '+user+' is already in group '+group+', so it will not be added.'
                  else:
                    userEditor.addMemberToGroup(group, user)
                    print 'User '+user+' has been added to group '+group+'.'
                except:
                  self.printError()
                  print 'Could not add user '+user+' to group '+group+'...'
                  return 1
        else:
          print 'User '+user+' already exists, so it will not be created.'

      return 0


   # Delete 1 or more users.  users variable is of syntax user1,user2,user3,etc.  Do not delete groups.
   def deleteUsers(self, users):
     print 'Deleting users from input given'
     userEditor = self.getDefaultAuthenticator()
     userlist = String(users).split(",")
     for user in userlist:
       print 'Deleting user '+user+'...'
       if userEditor.userExists(user) == 0:
         print 'User '+user+' does not exist, so it will not be deleted.'
       else:
         try:
           print 'User '+user+' exists, so it will be deleted.'
           userEditor.removeUser(user)
         except:
           self.printError()
           print 'Could not delete user '+user+'...'
           return 1

     return 0
 
   # Create 1 or more groups.  groups variable is of syntax: group1,group2,group3,etc.
   def createGroups(self, groups):
      print 'Creating groups from input: '+groups+'...'
      groupEditor = self.getDefaultAuthenticator()
      grouplist = String(groups).split(",")
      for group in grouplist:
        if groupEditor.groupExists(group) > 0:
          print 'Group '+group+' exists, so it will not be added.'
        else:
          print 'Group '+group+' does not exist, and will be added.'
          description = "The %s group" % (group)
          try:
            groupEditor.createGroup(group, description)
          except:
            self.printError()
            print 'Could not add group '+group+'...'
            return 1

      return 0

   # Delete 1 or more groups.  groups variable is of syntax: group1,group2,group3,etc.
   def deleteGroups(self, groups):
      print 'Deleting groups from input: '+groups+'...'
      groupEditor = self.getDefaultAuthenticator()
      grouplist = String(groups).split(",")
      for group in grouplist:
        if groupEditor.groupExists(group) == 0:
          print 'Group '+group+' does not exist, so it will not be removed.'
        else:
          print 'Group '+group+' exists, attempting to remove.'
          groupmembers = self.getGroupUsers(group)
          if groupmembers.size() == 0:
            try:
              groupEditor.removeGroup(group)
              print 'Group '+group+' has been removed.'
            except:
              self.printError()
              print 'Could not remove group '+group+'...'
              return 1
          else:
            print 'This group is not empty: it contains members.  Remove these from the group before attempting to remove it.'

      return 0
	 
   # Deletes application policies with the given appStripe name.
   def deleteAppPolicies(self, appStripe):
    m = HashMap()
    m.put("appStripe", appStripe) 
    try :
        wl.domainRuntime()
        on = wl.ObjectName("com.oracle.jps:type=JpsApplicationPolicyStore")

        params = [m.get("appStripe")]

        sign = ["java.lang.String"]

        try :
            wl.mbs.invoke(on, "getApplicationPolicy", params, sign)
        except :
            print "Application Stripe '" +appStripe+ "' not found in Application Policies. No action taken"
            return 0

        print "Deleting Application Stripe '" +appStripe+ "' from Application Policies"
        wl.mbs.invoke(on, "deleteApplicationPolicy", params, sign)
    except MBeanException, e:
        print 'Command FAILED, Reason: '+ e.getLocalizedMessage() + "\n"
        return 1
    except :
        print 'COMMAND FAILED due to an unknown reason, Check the stack trace for details'
        return 1
		
    return 0

   
   # grants application policy with the given appStripe name.	
   def grantAppPermission(self, appStripe, principalClass, principalName, permClass, permTarget, permActions):
    from javax.management.openmbean import CompositeData
    from oracle.security.jps.mas.mgmt.jmx.policy import PortablePrincipal
    from oracle.security.jps.mas.mgmt.jmx.policy.PortablePrincipal import PrincipalType
    from oracle.security.jps.mas.mgmt.jmx.policy import PortablePermission
    from oracle.security.jps.mas.mgmt.jmx.policy import PortableCodeSource
    from oracle.security.jps.mas.mgmt.jmx.policy import PortableGrantee
    from oracle.security.jps.mas.mgmt.jmx.policy import PortableGrant
    from oracle.security.jps.mas.mgmt.jmx.util import JpsJmxConstants   
    m = HashMap()
    m.put("appStripe", appStripe)
    m.put("principalClass", principalClass)
    m.put("principalName", principalName)
    m.put("permClass", permClass)
    m.put("permTarget", permTarget)
    m.put("permActions", permActions)    
    try :
        wl.domainRuntime()		
        pPl = None      
        pPl = PortablePrincipal(m.get("principalClass"), m.get("principalName"), PrincipalType.CUSTOM)
        pCs = PortableCodeSource(m.get("codeBaseURL"))
        pPlArray = None
        if pPl is not None:		
		print 'permission create successful'
        pPlArray = wl.array([pPl], PortablePrincipal)
        pGe = PortableGrantee(pPlArray, pCs)
        pPm = PortablePermission(m.get("permClass"), m.get("permTarget"), m.get("permActions"))
        pGt = PortableGrant(pGe, wl.array([pPm], PortablePermission))
		 
        if m.get("appStripe") is not None:
		print 'appstripe found'
        o =  wl.ObjectName(JpsJmxConstants.MBEAN_JPS_APPLICATION_POLICY_STORE)
        params = [m.get("appStripe"), wl.array([pGt.toCompositeData(None)], CompositeData)]
        sign = ["java.lang.String", "[Ljavax.management.openmbean.CompositeData;"]
        perms =  wl.mbs.invoke(o, "grantToApplicationPolicy", params, sign)
    except MBeanException, e:
        print 'Command FAILED, Reason: '+ e.getLocalizedMessage() + "\n"
        # For our installers, the MBeanException exception 
        # should only be thrown in the case where the 
        # permission already exists.  It doesn't need to be
        # recreated, so we can continue
        print 'Continuing installation...'
        return 0
    except :        
        self.printError()
        return 1
		
    return 0