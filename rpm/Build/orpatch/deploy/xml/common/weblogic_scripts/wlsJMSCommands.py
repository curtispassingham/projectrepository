########################################################################
# Imported modules
########################################################################

import sys
import traceback
import re

import java.lang.String as String
import java.io.File as File

import wlsServerCommands

import wl

########################################################################
# Example class
# This class serves as a template for future classes of weblogic commands.
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
########################################################################
class JMSCommands:

   def __init__(self):
      """Initialize a WebLogic MBean connection."""

   # print an error message
   def printError(self):
      print '********************'
      print '* Unexpected error *'
      print '********************'
      wl.dumpStack()
      apply(traceback.print_exception, sys.exc_info())
      print '*********************************************************'
      print '* Please view the weblogic server logs for more details *'
      print '*********************************************************'

   # Utility method to check if a JMS Module exists
   def checkJMSModuleExists(self, jmsModuleName):
      """Check if a JMS Module exists."""
      try:
        target=wl.lookup(jmsModuleName,"JMSSystemResource")
        type=target.getType()
        print 'JMS Module check: '+type+' '+jmsModuleName+' exists.'
        return type
      except:
        type=None
        print 'JMS Module check: '+jmsModuleName+' does not exist.'

      return type

   # Utility method to check if a JMS SubDeployment exists
   def checkJMSSubDeploymentExists(self, jmsModuleName, subDeploymentName):
      """Check if a JMS SubDeployment exists."""
      try:
        targetModule=wl.lookup(jmsModuleName,"JMSSystemResource")
        targetSubDeployment = targetModule.lookupSubDeployment(subDeploymentName)
        type=targetSubDeployment.getType()
        print 'JMS SubDeployment check: '+type+' '+subDeploymentName+' exists.'
        return type
      except:
        type=None
        print 'JMS SubDeployment check: '+subDeploymentName+' does not exist.'

      return type

   def createJMSQueues(self, jmsModuleName, targetServerName, queueNames, jndiNames=None):
      jmsServerName = "JMSServer"

      #Check that the number of queues specified matches
      #the number of jndi names specified, before we take the edit lock
      queues = String(queueNames).split(",")
      if jndiNames is None or len(jndiNames)==0:
        jndis = []
        for queue in queues:
          jndis.append("jms/"+queue)
      else:
        jndis = String(jndiNames).split(",")
        if len(jndis) != len(queues):
          print "Error: number of jndi names ( ",len(jndis)," ) must match number of JMS queues ( ",len(queues)," )"
          return 1
      
      print "Creating JMS Queues [" + queueNames + "]"

      server = wlsServerCommands.ServerCommands()
      type = server.checkServerOrClusterExists(targetServerName)
      if type == None:
        print "Error: server or cluster " + targetServerName + " must exist in order to create JMS queues."
        return 1
      servers = server.getServers(targetServerName)
      if type == "Server":
        targetServer = wl.getMBean("/Servers/" + targetServerName)
      else:
        targetServer = wl.getMBean("/Clusters/" + targetServerName)
      
      # delete old JMS Server/Modules if they exist
      print "Before creating any queues, any existing JMS components will be deleted."
      self.deleteJMSQueues(jmsModuleName, targetServerName)    

      wl.edit()
      wl.startEdit(600000,600000,"true")
  
      try: 
        # create the JMS Module
        print "Creating JMS Module [" + jmsModuleName + "]"
        jmsModule = wl.create(jmsModuleName, "JMSSystemResource")
        print "Targeting JMS Module [" + jmsModuleName + "] to " + type + " [" + targetServerName + "]"
        jmsModule.addTarget(targetServer)

        #Create a subdeployment first, so we can target it to the JMSServers below
        subDeploymentName = jmsModuleName + "-" + targetServerName + "-SubDeployment"
        print "Creating JMS SubDeployment [" + subDeploymentName + "]"
        subDeployment = jmsModule.createSubDeployment(subDeploymentName)
        #subDeployment.addTarget(targetServer)

        
        for server in servers:
          # create the JMS Server
          tmpTargetServerName = server.getName()
          tmpJmsServerName = jmsServerName + "-" + tmpTargetServerName
          print "Creating JMS Server [" + tmpJmsServerName + "]"
          jmsServer = wl.create(tmpJmsServerName, "JMSServer")
          print "Targeting JMS Server [" + tmpJmsServerName + "] to Server [" + tmpTargetServerName + "]"
          jmsServer.addTarget(server)
          print "Targeting JMS SubDeployment [" + subDeploymentName + "] to JMS server [" + tmpJmsServerName + "]"
          subDeployment.addTarget(jmsServer)
        
        # create the JMS Queue
        jmsResource = jmsModule.getJMSResource()
        
        # Create distributed queue for cluster, non-distributed for a server

        if type == "Cluster":
          # create queues
          for index,queue in enumerate(queues):
            print "Creating JMS queue [" + queue + "]"
            jmsQueue = jmsResource.createUniformDistributedQueue(queue)
            queueJndiName=jndis[index]
            print "Setting JNDI name [" + queueJndiName + "]"
            jmsQueue.setJNDIName(queueJndiName)
            #jmsQueue.setDefaultTargetingEnabled(1)
            jmsQueue.setSubDeploymentName(subDeploymentName)
        else:
          # create queues
          for index,queue in enumerate(queues):
            print "Creating JMS queue [" + queue + "]"
            jmsQueue = jmsResource.createQueue(queue)
            queueJndiName=jndis[index]
            print "Setting JNDI name [" + queueJndiName + "]"
            jmsQueue.setJNDIName(queueJndiName)
            jmsQueue.setSubDeploymentName(subDeploymentName)

      except: 
        self.printError()
        wl.cancelEdit('y')
        print "Error configuring JMS changes, rolling back to previous state."
        return 1
 
      try:
        wl.save()
        wl.activate(600000,block="true")
        print "Done creating JMS queues [" + queueNames + "]"
      except:
        self.printError()
        wl.cancelEdit('y')
        print "Error saving and activating JMS creation changes, rolling back to previous state."
        return 1

      return 0

   def deleteJMSQueues(self, jmsModuleName, targetServerName):
      jmsServerName = "JMSServer"
      jmsDescriptorFile = None
      jmsDescriptorString = ""

      serverobj = wlsServerCommands.ServerCommands()
      type = serverobj.checkServerOrClusterExists(targetServerName)      
      if type == None:
        print "Error: server or cluster " + targetServerName + " must exist in order to delete JMS queues."
        return 1
      managedServers = serverobj.getServers(targetServerName)

      print "All JMS Queues in module " + jmsModuleName + " will be deleted."

      wl.edit()
      wl.startEdit(600000,600000,"true")


      try:
        # Delete the Jms if it already exists
        jmsResources = wl.cmo.getJMSSystemResources()

        for jms in jmsResources:
          if jms.getName() == jmsModuleName:
            
            # Save the absolute path to the JMS Descriptor file to delete 
            jmsDescriptorString = wl.cmo.getRootDirectory() + "/" + jms.getSourcePath()
            jmsDescriptorFile = File(jmsDescriptorString)

            print "Deleting JMS resource " + jmsModuleName
            wl.delete(jmsModuleName,"JMSSystemResource")

        # Delete JMS server(s) if they exist
        jmsServers = wl.cmo.getJMSServers()
        for server in jmsServers:
          tmpjmsname = server.getName()
          for managedServer in managedServers:
            tmpname = jmsServerName+"-"+managedServer.getName()
            if tmpjmsname == tmpname: 
              print "Deleting JMS server " + tmpname
              wl.delete(tmpname,"JMSServer")

      except:
        self.printError()
        wl.cancelEdit('y')
        print "Error configuring JMS deletion, rolling back to previous state."
        return 1

      try:
        wl.save()
        wl.activate(600000,block="true")
        print "Done deleting JMS queues"

        # Delete JMS Descriptor file if it still exists
        print "Cleaning up JMS descriptor file ..."
        if (jmsDescriptorFile != None) and (jmsDescriptorFile.exists()):
          print "Deleting JMS descriptor file: " + jmsDescriptorString
          jmsDescriptorFile.delete()
        else:
          print "Nothing to delete, JMS descriptor file not found: " + jmsDescriptorString

      except:
        self.printError()
        wl.cancelEdit('y')
        print "Error saving and activating JMS deletion changes, rolling back to previous state."
        return 1
      
      return 0

   def createJMSConnectionFactory(self, jmsModuleName, targetServerName, factoryName, jndiName, messagesMaximum, transactionTimeout):

      subDeploymentName = jmsModuleName + "-" + targetServerName + "-SubDeployment"

      server = wlsServerCommands.ServerCommands()

      type = server.checkServerOrClusterExists(targetServerName)
      if type == None:
        print "Error: server or cluster " + targetServerName + " must exist in order to create JMS Connection Factory."
        return 1
      servers = server.getServers(targetServerName)
      if type == "Server":
        targetServer = wl.getMBean("/Servers/" + targetServerName)
      else:
        targetServer = wl.getMBean("/Clusters/" + targetServerName)

      jmsModuleCheck = self.checkJMSModuleExists(jmsModuleName)
      if jmsModuleCheck == None:
        print "Error: JMS Module " + jmsModuleName + " must exist in order to create JMS Connection Factory."
        return 1

      jmsSubDeploymentCheck = self.checkJMSSubDeploymentExists(jmsModuleName, subDeploymentName)
      if jmsSubDeploymentCheck == None:
        print "Error: JMS SubDeployment " + subDeploymentName + " must exist in the JMS Module " + jmsModuleName + " in order to create JMS Connection Factory."
        return 1

      wl.edit()
      wl.startEdit(600000,600000,"true")
      print ""

      try:
        jmsModule = wl.lookup(jmsModuleName, "JMSSystemResource")
        subDeployment = jmsModule.lookupSubDeployment(subDeploymentName)

        # Create Connection Factory
        jmsResource = jmsModule.getJMSResource()
        print "Creating JMS Connection Factory [" + factoryName + "]"
        jmsConnectionFactory = jmsResource.createConnectionFactory(factoryName)
        print "Setting JNDI name [" + jndiName + "]"
        jmsConnectionFactory.setJNDIName(jndiName)
        print "Setting SubDeployment [" + subDeploymentName + "]"
        jmsConnectionFactory.setSubDeploymentName(subDeploymentName)
        print "Setting XA Connection Factory Enabled [True]"
        transactionParams = jmsConnectionFactory.getTransactionParams()
        transactionParams.setXAConnectionFactoryEnabled(1)

        if len(messagesMaximum) != 0:
          messagesMaximumInt=int(messagesMaximum) 
          print "Setting Max Messages per Session [" + str(messagesMaximumInt) + "]"
          clientParams = jmsConnectionFactory.getClientParams()
          clientParams.setMessagesMaximum(messagesMaximumInt)

        if len(transactionTimeout) != 0:
          transactionTimeoutInt=int(transactionTimeout) 
          print "Setting Transaction Timeout [" + str(transactionTimeoutInt) + "]" 
          transactionParams.setTransactionTimeout(transactionTimeoutInt)

      except:
        self.printError()
        wl.cancelEdit('y')
        print "Error configuring JMS changes, rolling back to previous state."
        return 1

      try:
        wl.save()
        wl.activate(600000,block="true")
        print "Done creating JMS Connection Factory [" + factoryName + "]"
      except:
        self.printError()
        wl.cancelEdit('y')
        print "Error saving and activating JMS creation changes, rolling back to previous state."
        return 1

      return 0

   def setRedeliveryLimit(self,redeliveryLimit,queueName,jmsModule):

      wl.edit()
      wl.startEdit(600000,600000,"true")

      #Search the Queues for an mbean with the matching Queue name (for non-cloud envs)
      jmsQueue = wl.getMBean('/JMSSystemResources/'+jmsModule+'/JMSResource/'+jmsModule+'/Queues/'+queueName+'/DeliveryFailureParams/'+queueName)
      if (jmsQueue == None):
        #Queue with matching name not found, next search the UniformDistributedQueues for an mbean with the matching Queue name (for cloud envs)
        jmsQueue = wl.getMBean('/JMSSystemResources/'+jmsModule+'/JMSResource/'+jmsModule+'/UniformDistributedQueues/'+queueName+'/DeliveryFailureParams/'+queueName)    
      if (jmsQueue == None):
        print 'Failed to find queue with name '+queueName+' to set redelivery limit on.'
        wl.cancelEdit('y')
        return 1

      try:
        print 'Setting redelivery limit on '+queueName
        jmsQueue.setRedeliveryLimit(int(redeliveryLimit))
        wl.save()
        wl.activate(600000,block="true")
        print 'Setting redelivery limit completed'
      except:
        self.printError()
        wl.cancelEdit('y')
        print "Error saving setting redelivery limit, rolling back to previous state."
        return 1

      return 0


