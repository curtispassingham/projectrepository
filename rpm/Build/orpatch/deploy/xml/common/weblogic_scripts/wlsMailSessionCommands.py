########################################################################
# Imported modules
########################################################################

import sys

import wl

import java.lang.Integer as Integer

########################################################################
# MailSessionCommands class
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
# JavaMail API reference: http://java.sun.com/products/javamail/javadocs/index.html
########################################################################
class MailSessionCommands:

   def __init__(self):
      """Initialize a WebLogic MBean connection."""

   # print an error message
   def printError(self):
      print '********************'
      print '* Unexpected error *'
      print '********************'
      wl.dumpStack()
      print '*********************************************************'
      print '* Please view the weblogic server logs for more details *'
      print '*********************************************************'

   def createMailSession(self, mailName, jndiName, protocol, host, port, serverTarget):
      # If mailsession does not exist, create it
      mailExists = "false"
      sources = wl.cmo.getMailSessions()
      for mail in sources:
        if mail.getName() == mailName:   
          mailExists = "true"
          break

      # Check for a valid protocol: smtp, imap, or pop3  
      if protocol != "smtp" and protocol != "imap" and protocol != "pop3":
        print 'Error: an invalid protocol - '+protocol+' - was specified.  Please use smtp, imap, or pop3.'
        return 1

      # This can be a server or a cluster.  Try server first, then cluster.  Otherwise an error will occur.
      target=wl.getMBean("/Servers/"+serverTarget)
      if target == None:
        target=wl.getMBean("/Clusters/"+serverTarget)
        if target == None:
          self.printError()
          print 'Error: No server or cluster with name '+serverTarget+' was found, cannot set up MailSession.'
          return 1

      # Recreate MailSession if need be
      if mailExists == "true":  
        print 'MailSession '+mailName+' already exists, it will be deleted and re-created...'
        self.deleteMailSession(mailName)

      print 'Creating mail session with name '+mailName

      # Start editing
      wl.edit()
      wl.startEdit(600000,600000,"true")

      try:
        # Create object, set jndi name      
        mailSR = wl.create(mailName,"MailSession")
        mailSR.setJNDIName(jndiName) 

        #Can use properties defined by the JavaMail API Design Specification.
        mailSR.setProperties(wl.makePropertiesObject("mail.transport.protocol="+protocol+";"+
                                                     "mail."+protocol+".host="+host+";"+
                                                     "mail."+protocol+".port="+port))

        # Add server or cluster target
        mailSR.addTarget(target)
      except:
        self.printError()
        print 'Could not configure mail session '+mailName+', rolling back changes.'
        wl.cancelEdit('y')
        return 1

      # Save changes
      try:
        wl.save()
      except:
        self.printError()
        print 'Could not save the addition of mail session '+mailName+', rolling back changes.'
        wl.cancelEdit('y')
        return 1

      # Activate changes
      try:
        wl.activate(600000,block="true")
        print 'Done configuring the mail '+mailName
      except:
        self.printError()
        print 'Could not activate mail session creation for '+mailName+', rolling back changes.'
        wl.cancelEdit('y')
        return 1

      return 0

   def deleteMailSession(self, mailName):
      # If mailsession does not exist, create it
      mailExists = "false"
      sources = wl.cmo.getMailSessions()
      for mail in sources:
        if mail.getName() == mailName:   
          mailExists = "true"

          # Start editing
          wl.edit()
          wl.startEdit(600000,600000,"true")
 
          try: 
            print 'Deleting mail sessin with name '+mailName

            # Delete the object
            wl.delete(mailName,"MailSession")
          except:
            self.printError()
            print 'Could not delete mail session '+mailName+', rolling back changes.'
            wl.cancelEdit('y')
            return 1
 
          # Save changes
          try:
            wl.save()
          except:
            self.printError()
            print 'Could not save the deletion of mail session '+mailName+', rolling back changes.'
            wl.cancelEdit('y')
            return 1
    
          # Activate changes
          try:
            wl.activate(600000,block="true")
            print 'Done deleting the mail session '+mailName
          except:
            self.printError()
            print 'Could not activate mail session deletion for '+mailName+', rolling back changes.'
            wl.cancelEdit('y')
            return 1
 
      if mailExists == "false":
        print 'Error: could not delete mail session ' +mailName+', because it does not exist.'
        return 1

      return 0
