########################################################################
# Imported modules
########################################################################

import sys

import wlsConnection
import wlsJMSCommands
import wlsDatasourceCommands
import wlsSecureDatasourceCommands
import wlsDeployCommands
import wlsDomainCommands
import wlsMailSessionCommands
import wlsMigrationBasis
import wlsSecureMailSessionCommands
import wlsSecurityCommands
import wlsServerCommands

import wl

########################################################################
# MAIN SCRIPT
########################################################################

def usage():
   print "Usage: wlsMain.py"
   print "  All arguments must come as lines on stdin"
   print "   <command> [<admin user> <admin password> <admin url>] [<arg1> <arg2> ... <argn>]"
   exit(exitcode=2)

args=[sys.argv[0]]
for line in sys.stdin:
   args.append(line.rstrip())

# Get the command-line arguments
if len(args) < 3:
   usage()

command = args[1]
print "Executing command: "+command

if len(args) > 4:
  user=args[2]
  password = args[3]
  url = args[4]
else:
  user = ""
  password = ""
  url = ""

# Only go only for certain commands that require it
if command != "createDomain" and command != "deleteDomain" and command != "startNodeManager" and command != "checkAdminServer" and command != "getAdminServerPort":
  online="true"
else:
  online="false"

if online == "true":
  try:
    wl.connect(user, password, url)
  except:
    exit(exitcode=1)

# Figure out which command to run
# Connection Commands
if command == "checkAdminServer":
  connection = wlsConnection.Connection(user,password,url)
  retval = connection.checkAdminServer()

# JMS commands
elif command == "createJMSQueues":
  moduleName = args[5]
  serverName = args[6]
  queues = args[7]
  if len(args)>8:
    jndis = args[8]
  else:
    jndis = None
  jmscommands = wlsJMSCommands.JMSCommands()
  retval = jmscommands.createJMSQueues(moduleName, serverName, queues, jndis)
elif command == "deleteJMSQueues":
  moduleName = args[5]
  serverName = args[6]
  jmscommands = wlsJMSCommands.JMSCommands()
  retval = jmscommands.deleteJMSQueues(moduleName, serverName)
elif command == "createJMSConnectionFactory":
  moduleName = args[5]
  serverName = args[6]
  factoryName = args[7]
  jndiName = args[8]
  messagesMaximum = args[9]
  transactionTimeout = args[10]
  jmscommands = wlsJMSCommands.JMSCommands()
  retval = jmscommands.createJMSConnectionFactory(moduleName, serverName, factoryName, jndiName, messagesMaximum, transactionTimeout)
elif command == "setRedeliveryLimit":
  redeliveryLimit = args[5]
  queueName = args[6]
  jmsModule = args[7]
  jmscommands = wlsJMSCommands.JMSCommands()
  retval = jmscommands.setRedeliveryLimit(redeliveryLimit,queueName,jmsModule)

# MigrationBasis commands
elif command == "SetMigrationBasis":
  serverName = args[5]
  datasourceName = args[6]
  mbcommands = wlsMigrationBasis.MigrationBasisCommands()
  retval = mbcommands.SetMigrationBasis(serverName, datasourceName)
 
# Datasource commands
elif command == "createDatasource":
  datasourceName = args[5]
  serverName = args[6]
  jndiName = args[7]
  databaseUrl = args[8]
  databaseUser = args[9]
  databasePassword = args[10]
  databaseClassname = args[11]
  wrapdatatypes = args[12]
  transactionoption = args[13]
  dbcommands = wlsDatasourceCommands.DatasourceCommands()
  retval = dbcommands.createDatasource(datasourceName, serverName, jndiName, databaseUrl, databaseUser, databasePassword, databaseClassname, wrapdatatypes, transactionoption)
  
# SecureDatasource commands
elif command == "createSecureDatasource":
  datasourceName = args[5]
  serverName = args[6]
  jndiName = args[7]
  databaseUrl = args[8]
  databaseUser = args[9]
  databasePassword = args[10]
  databaseClassname = args[11]
  wrapdatatypes = args[12]
  transactionoption = args[13]
  keyStoreType = args[14]
  keyStorelocation = args[15]
  keyStorePassword = args[16]
  trustStoreType = args[17]
  trustStorelocation = args[18]
  trustStorePassword = args[19]
  dbcommands = wlsSecureDatasourceCommands.SecureDatasourceCommands()
  retval = dbcommands.createDatasource(datasourceName, serverName, jndiName, databaseUrl, databaseUser, databasePassword, databaseClassname, wrapdatatypes, transactionoption, keyStoreType, keyStorelocation, keyStorePassword, trustStoreType, trustStorelocation, trustStorePassword)
  
elif command == "deleteDatasource":
  datasourceName = args[5]
  serverName = args[6]
  dbcommands = wlsDatasourceCommands.DatasourceCommands()  
  retval = dbcommands.deleteDatasource(datasourceName, serverName)

elif command == "setDataSourceXATransactionTimeout":
  datasourceName = args[5]
  transactionTimeout = args[6]
  dbcommands = wlsDatasourceCommands.DatasourceCommands()
  retval = dbcommands.setDataSourceXATransactionTimeout(datasourceName, transactionTimeout)
   
# Mail Session commands
elif command == "createSecureMailSession":
  mailName = args[5]
  jndiName = args[6]
  protocol = args[7]
  host = args[8]
  enableSSL = args[9]
  port = args[10]
  username = args[11]
  password = args[12]
  enableAUTH = args[13]
  enableSTARTTLS = args[14]
  serverName = args[15]
  mailcommands = wlsSecureMailSessionCommands.MailSessionCommands()
  retval = mailcommands.createMailSession(mailName, jndiName, protocol, host, enableSSL, port, username, password, enableAUTH, enableSTARTTLS, serverName)

elif command == "createMailSession":
  mailName = args[5]
  jndiName = args[6]
  protocol = args[7]
  host = args[8]
  port = args[9]
  serverName = args[10]
  mailcommands = wlsMailSessionCommands.MailSessionCommands()
  retval = mailcommands.createMailSession(mailName, jndiName, protocol, host, port, serverName)
elif command == "deleteMailSession":
  mailName = args[5]
  mailcommands = wlsMailSessionCommands.MailSessionCommands()  
  retval = mailcommands.deleteMailSession(mailName)

# Deployment commands
elif command == "deploy":
  archiveName = args[5]
  applicationName = args[6]
  serverTarget = args[7]
  stagingMode = args[8]
  isLibrary = args[9]
  deploycommands = wlsDeployCommands.DeployCommands()
  retval = deploycommands.deployArchive(archiveName, applicationName, serverTarget, stagingMode, isLibrary)
elif command == "undeploy":
  applicationName = args[5]
  serverTarget = args[6]
  isLibrary = args[7]
  deploycommands = wlsDeployCommands.DeployCommands()  
  retval = deploycommands.undeployArchive(applicationName, serverTarget, isLibrary)
elif command == "redeploy":
  archiveName = args[5]
  applicationName = args[6]
  serverTarget = args[7]
  isLibrary = args[8]
  deploycommands = wlsDeployCommands.DeployCommands()
  retval = deploycommands.redeployArchive(archiveName, applicationName, serverTarget, isLibrary)
  
# Command to get AbsolutePath
elif command == "getAbsolutePath":
  applicationName = args[5]
  deploycommands = wlsDeployCommands.DeployCommands()
  retval = deploycommands.getAbsolutePath(applicationName)

  # Command to Apply Policy
elif command == "applyPolicy":
  applicationName = args[5]
  serverTarget = args[6]
  appplanPath = args[7]
  deploycommands = wlsDeployCommands.DeployCommands()
  retval = deploycommands.applyPolicy(applicationName, serverTarget, appplanPath)
  
# Domain commands  
elif command == "createDomain":
  nmport = args[4]
  listenaddress = args[5]
  domainDir = args[6]
  adminport = args[7]
  mode = args[8]
  domaincommands = wlsDomainCommands.DomainCommands(user, password, listenaddress, nmport, domainDir)
  retval = domaincommands.makeDomain(adminport,mode)
elif command == "deleteDomain":
  nmport = args[4]
  listenaddress = args[5]
  domainDir = args[6]
  domaincommands = wlsDomainCommands.DomainCommands(user, password, listenaddress, nmport, domainDir)
  retval = domaincommands.removeDomain()
   
# Security commands
elif command == "createGroups":
  groups = args[5]
  securitycommands = wlsSecurityCommands.SecurityCommands()
  retval = securitycommands.createGroups(groups)
elif command == "deleteGroups":
  # Note: do not delete users, just groups
  groups = args[5]
  securitycommands = wlsSecurityCommands.SecurityCommands()
  retval = securitycommands.deleteGroups(groups)
elif command == "createUsers":
  # Note: argument comes in comma-separated list of user:group-list;user:group-list;etc.
  users = args[5]
  securitycommands = wlsSecurityCommands.SecurityCommands()  
  retval = securitycommands.createUsers(users)
elif command == "deleteUsers":
  # Note: delete only users, not groups
  users = args[5]
  securitycommands = wlsSecurityCommands.SecurityCommands()  
  retval = securitycommands.deleteUsers(users)
elif command == "deleteAppPolicies":
  # Note: deletes application policies
  appStripe = args[5]
  securitycommands = wlsSecurityCommands.SecurityCommands()  
  retval = securitycommands.deleteAppPolicies(appStripe)
elif command == "grantAppPermission":
  # Note: grants application policies
  appStripe = args[5]
  principalClass = args[6]
  principalName = args[7]
  permClass = args[8]
  permTarget = args[9]
  permActions = args[10]
  securitycommands = wlsSecurityCommands.SecurityCommands()  
  retval = securitycommands.grantAppPermission(appStripe, principalClass, principalName, permClass, permTarget, permActions)

# Server commands
elif command == "startServer":
  serverName = args[5]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.startServerOrCluster(serverName)
elif command == "stopServer":
  serverName = args[5]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.stopServerOrCluster(serverName)
elif command == "restartServer":
  serverName = args[5]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.restartServerOrCluster(serverName)
elif command == "createServer":
  serverName = args[5]
  machineName = args[6]
  ssl = args[7]
  port = args[8]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.createServerOrCluster(serverName,'Server',machineName,None,None,ssl,port)
elif command == "getPortNumber":
  serverName = args[5]
  ssl = args[6]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.getPort(serverName, ssl)
elif command == "getClusterServers":
  serverName = args[5]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.getClusterServers(serverName)
elif command == "getClusterMachines":
  serverName = args[5]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.getClusterMachines(serverName)
elif command == "getPrimaryServerName":
  serverName = args[5]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.getPrimaryServerName(serverName)
elif command == "checkTargetExists":
  serverName = args[5]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.checkTargetExists(serverName)
elif command == "checkNodeManager":
  serverName = args[5]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.checkNodeManager(user,password,serverName)
elif command == "deleteServer":
  serverName = args[5]
  servercommands = wlsServerCommands.ServerCommands()  
  retval = servercommands.deleteServerOrCluster(serverName,'Server',None)
elif command == "createCluster":
  serverName = args[5]
  machineName = args[6]
  serverList = args[7]
  docreate = args[8]
  ssl = args[9]
  port = args[10]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.createServerOrCluster(serverName,'Cluster',machineName,serverList,docreate,ssl,port)
elif command == "deleteCluster":
  serverName = args[5]
  dodelete = args[6]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.deleteServerOrCluster(serverName,'Cluster',dodelete)
elif command == "createMachine":
  machineName = args[5]
  listenAddress = args[6]
  listenPort = args[7]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.createMachine(machineName,listenAddress,listenPort)
elif command == "deleteMachine":
  machineName = args[5]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.deleteMachine(machineName)
elif command == "setServerStartOptions":
  serverTarget = args[5]
  arguments = args[6]
  classpath = args[7]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.setServerStartOptions(serverTarget,arguments,classpath)
elif command == "DisableNonSSLPort":
  serverTarget = args[5]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.DisableNonSSLPort(serverTarget)
elif command == "startNodeManager":
  wlsHome = args[5]
  listenAddress = args[6]
  listenPort = args[7]
  servercommands = wlsServerCommands.ServerCommands()
  retval = servercommands.startNodeManager(wlsHome,listenAddress,listenPort)
else:
  usage()

if online == "true":
  wl.disconnect("true")

# Exit cleanly
exitvalue = Integer.toString(retval)
print 'Command '+command+' exited with code '+exitvalue+'.'
exit(exitcode=retval)
