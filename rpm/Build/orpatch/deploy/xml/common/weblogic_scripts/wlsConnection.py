########################################################################

import sys
import re
import os

import wl

import java.lang.String as String

########################################################################
# ServerCommands class
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
########################################################################
class Connection:

   # Constructor
   def __init__(self, adminuser, password, url):
      """Initialize a WebLogic MBean connection."""
      self.adminuser = adminuser
      self.password = password
      self.url = url

   # Method to open a weblogic connection
   def open(self):
      """Open a WebLogic MBean connection."""
      print 'Connecting to '+self.url+' as user '+self.adminuser+'...'
      wl.connect(self.adminuser, self.password, self.url)

   # Method to close a weblogic connection
   def close(self):
      """Close a Weblogic MBean connection."""
      print 'Disconnecting from '+self.url+'...'
      wl.disconnect("true")

   # Method to check a weblogic connection
   def checkAdminServer(self):
      """Open a WebLogic MBean connection."""
      print 'Testing '+self.url+' as user '+self.adminuser+'...'
      running="true"
      try:
        wl.connect(self.adminuser, self.password, self.url)
        print 'Server at '+self.url+' is running.'
        if wl.isAdminServer == "true":
          print 'Server is an AdminServer'
        else:
          print 'Error: server is NOT an AdminServer'
          running="false"
      except:
        running="false"
        print 'Error: Admin Server at '+self.url+' is NOT running.'

      output = open('/tmp/adminserver.txt','w')
      output.write('adminserver.running='+running)
      output.close()
      return 0
