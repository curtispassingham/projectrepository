<project name="common-installation-macros" basedir=".">

    <description>RGBU Common Installation Macros. Library of macros with installation steps shared across multiple products</description>

    <!-- Bring in ant-contrib.jar -->
    <taskdef resource="net/sf/antcontrib/antlib.xml">
        <classpath>
            <pathelement location="ant-ext/ant-contrib.jar" />
        </classpath>
    </taskdef>

    <!-- Run sql statements through ojdbc -->
    <macrodef name="run-sql-ojdbc">
        <attribute name="jdbc.url" default="${datasource.url.trimmed}" description="jdbc url to connect with" />
        <attribute name="userid" default="${datasource.username.trimmed}" description="Schema to connect to" />
        <attribute name="password" default="${datasource.password.trimmed}" />
        <attribute name="classpath" default="${ojdbc.path}" description="Must contain ojdbc jar" />
        <attribute name="print" default="false" description="Print output from the commands" />
        <attribute name="output" default="false" description="Output file for result sets (if false, defaults to System.out)" />
        <attribute name="showheaders" default="true" description="Print headers for result sets from the statements" />
        <attribute name="showtrailers" default="true" description="Print trailer for number of rows affected" />
        <attribute name="ssl" default="${enable.securejdbc.trimmed}" description="True or false, used to toggle SSL connection" />
        <attribute name="ssl.trustStore" default="${datasource.trustStore.trimmed}" />
        <attribute name="ssl.trustStoreType" default="${datasource.trustStoreType.trimmed}" />
        <attribute name="ssl.trustStorePassword" default="${datasource.trustStorePassword.trimmed}" />
        <attribute name="ssl.keyStore" default="${datasource.keyStore.trimmed}" />
        <attribute name="ssl.keyStoreType" default="${datasource.keyStoreType.trimmed}" />
        <attribute name="ssl.keyStorePassword" default="${datasource.keyStorePassword.trimmed}" />
        <text name="commands" optional="no"/> <!--The sql commands to run-->

        <sequential>
            <echo message="Connection URL: @{jdbc.url}" />
            <echo message="Schema: @{userid}" />
            <echo message="Run commands:" />
            <echo>@{commands}</echo>
            <if>
                <istrue value="@{ssl}"/>
                <then>
                    <if>
                        <equals arg1="@{output}" arg2="false"/>
                        <then> 
                            <sql driver="oracle.jdbc.OracleDriver"
                                 url="@{jdbc.url}"
                                 userid="@{userid}"
                                 password="@{password}"
                                 classpath="@{classpath}"
                                 print="@{print}"
                                 showheaders="@{showheaders}"
                                 showtrailers="@{showheaders}">
                                <connectionProperty name="javax.net.ssl.trustStore" value="@{ssl.trustStore}" />
                                <connectionProperty name="javax.net.ssl.trustStoreType" value="@{ssl.trustStoreType}" />
                                <connectionProperty name="javax.net.ssl.trustStorePassword" value="@{ssl.trustStorePassword}" />
                                <connectionProperty name="javax.net.ssl.keyStore" value="@{ssl.keyStore}" />
                                <connectionProperty name="javax.net.ssl.keyStoreType" value="@{ssl.keyStoreType}" />
                                <connectionProperty name="javax.net.ssl.keyStorePassword" value="@{ssl.keyStorePassword}" />
                                @{commands}
                            </sql>
                        </then>
                        <else>
                            <sql driver="oracle.jdbc.OracleDriver"
                                 url="@{jdbc.url}"
                                 userid="@{userid}"
                                 password="@{password}"
                                 classpath="@{classpath}"
                                 print="@{print}"
                                 output="@{output}"
                                 showheaders="@{showheaders}"
                                 showtrailers="@{showheaders}">
                                <connectionProperty name="javax.net.ssl.trustStore" value="@{ssl.trustStore}" />
                                <connectionProperty name="javax.net.ssl.trustStoreType" value="@{ssl.trustStoreType}" />
                                <connectionProperty name="javax.net.ssl.trustStorePassword" value="@{ssl.trustStorePassword}" />
                                <connectionProperty name="javax.net.ssl.keyStore" value="@{ssl.keyStore}" />
                                <connectionProperty name="javax.net.ssl.keyStoreType" value="@{ssl.keyStoreType}" />
                                <connectionProperty name="javax.net.ssl.keyStorePassword" value="@{ssl.keyStorePassword}" />
                                @{commands}
                            </sql>
                        </else>  
                    </if>
                </then>
                <else>
                    <if>
                        <equals arg1="@{output}" arg2="false"/>
                        <then>
                            <sql driver="oracle.jdbc.OracleDriver"
                                 url="@{jdbc.url}"
                                 userid="@{userid}"
                                 password="@{password}"
                                 classpath="@{classpath}"
                                 print="@{print}"
                                 showheaders="@{showheaders}"
                                 showtrailers="@{showheaders}">
                                @{commands}
                            </sql>
                        </then>
                        <else>
                            <sql driver="oracle.jdbc.OracleDriver"
                                 url="@{jdbc.url}"
                                 userid="@{userid}"
                                 password="@{password}"
                                 classpath="@{classpath}"
                                 print="@{print}"
                                 output="@{output}"
                                 showheaders="@{showheaders}"
                                 showtrailers="@{showheaders}">
                                @{commands}
                            </sql>
                        </else>
                    </if>
                </else>
            </if>
        </sequential>
    </macrodef>

    <!-- Add new row to RMS's RAF_INSTALLED_APPS table for the installed app -->
    <macrodef name="update-RAF_INSTALLED_APPS">
        <attribute name="app.stripe" description="The application Stripe for column APPLICATION_ID. Must be unique. Rows with matching stripe and SYSTEM_ENTRY=Y will be replaced"  />
        <attribute name="app.name" description="The application name for column NAME"  />
        <attribute name="app.desc" description="A description of the application for column DESCRIPTION"  />
        <attribute name="app.url" description="The url of the application for column URL"  />
        <attribute name="jdbc.url" default="${datasource.url.trimmed}" description="jdbc url to connect with" />
        <attribute name="userid" default="${datasource.username.trimmed}" description="Schema to connect to" />
        <attribute name="password" default="${datasource.password.trimmed}" />
        <attribute name="classpath" default="${ojdbc.path}" description="Must contain ojdbc jar" />
        <attribute name="ssl" default="${enable.securejdbc.trimmed}" description="True or false, used to toggle SSL connection" />
        <attribute name="ssl.trustStore" default="${datasource.trustStore.trimmed}" />
        <attribute name="ssl.trustStoreType" default="${datasource.trustStoreType.trimmed}" />
        <attribute name="ssl.trustStorePassword" default="${datasource.trustStorePassword.trimmed}" />
        <attribute name="ssl.keyStore" default="${datasource.keyStore.trimmed}" />
        <attribute name="ssl.keyStoreType" default="${datasource.keyStoreType.trimmed}" />
        <attribute name="ssl.keyStorePassword" default="${datasource.keyStorePassword.trimmed}" />
        <sequential>

            <!-- This parallel task is being used as a one minute timeout wrapper of the sql call.  
                 The sql script fails to return if it runs into the case where it is trying to update
                 an existing row with values duplicate to another existing row-->
            <parallel threadCount="1" timeout="60000">
                <sequential>

                    <run-sql-ojdbc jdbc.url="@{jdbc.url}"
                                   userid="@{userid}"
                                   password="@{password}"
                                   classpath="@{classpath}"
                                   ssl="@{ssl}"
                                   ssl.trustStore="@{ssl.trustStore}"
                                   ssl.trustStoreType="@{ssl.trustStoreType}"
                                   ssl.trustStorePassword="@{ssl.trustStorePassword}"
                                   ssl.keyStore="@{ssl.keyStore}"
                                   ssl.keyStoreType="@{ssl.keyStoreType}"
                                   ssl.keyStorePassword="@{ssl.keyStorePassword}" >
                        <![CDATA[
MERGE INTO RAF_INSTALLED_APPS A
USING
  (select
    '@{app.stripe}'    AS APPLICATION_ID,
    '@{app.name}'  AS NAME,
    '@{app.desc}'   AS DESCRIPTION,
    '@{app.url}'  AS URL,
    null AS ROLE,
    'Y' AS SYSTEM_ENTRY,
    user AS CREATED_BY,
    sysdate  AS CREATION_DATE,
    user  AS LAST_UPDATED_BY,
    sysdate AS LAST_UPDATED_DATE from dual) USE_THIS
  ON (A.APPLICATION_ID = USE_THIS.APPLICATION_ID and A.SYSTEM_ENTRY=USE_THIS.SYSTEM_ENTRY)
WHEN MATCHED THEN
  update set
  A.NAME=USE_THIS.NAME,
  A.DESCRIPTION=USE_THIS.DESCRIPTION,
  A.URL=USE_THIS.URL,
  A.ROLE=USE_THIS.ROLE,
  A.CREATED_BY=USE_THIS.CREATED_BY,
  A.CREATION_DATE=USE_THIS.CREATION_DATE,
  A.LAST_UPDATED_BY=USE_THIS.LAST_UPDATED_BY,
  A.LAST_UPDATED_DATE=USE_THIS.LAST_UPDATED_DATE
WHEN NOT MATCHED THEN
  INSERT
  (
    APPLICATION_ID,
    NAME,
    DESCRIPTION,
    URL,
    ROLE,
    SYSTEM_ENTRY,
    CREATED_BY,
    CREATION_DATE,
    LAST_UPDATED_BY,
    LAST_UPDATED_DATE
  )
  VALUES
  (
    USE_THIS.APPLICATION_ID,
    USE_THIS.NAME,
    USE_THIS.DESCRIPTION,
    USE_THIS.URL,
    USE_THIS.ROLE,
    USE_THIS.SYSTEM_ENTRY,
    USE_THIS.CREATED_BY,
    USE_THIS.CREATION_DATE,
    USE_THIS.LAST_UPDATED_BY,
    USE_THIS.LAST_UPDATED_DATE
  )
                        ]]>
                    </run-sql-ojdbc>
                </sequential>
            </parallel>
        </sequential>
    </macrodef>

    <!-- Configures extracted files to enable secure cookies.-->
    <macrodef name="config-for-secure-cookies">
        <attribute name="weblogic.application.xml.location" description="path to the weblogic-application.xml file to configure" />
        <attribute name="cookie.name" description="name of the cookie to configure. Must already exist within a session-descriptor in weblogic-application.xml or this macro will fail " />
        <sequential>
            <var name="weblogic.application.xml.content" unset="true" />
            <var name="cookie.name.found" unset="true" />
            <var name="secure.cookies.insert.content" unset="true" />

            <!-- Check if entry for cookie.name already exists in the weblogic-application.xml, fail if not found -->
            <loadfile srcFile="@{weblogic.application.xml.location}" property="weblogic.application.xml.content"/>
            <condition property="cookie.name.found">
                <matches string="${weblogic.application.xml.content}" pattern="&lt;cookie-name&gt;(.*)@{cookie.name}(.*)&lt;/cookie-name&gt;"/>
            </condition>
            <fail unless="cookie.name.found"
                  message="ERROR: cookie-name @{cookie.name} not found in @{weblogic.application.xml.location}" />

            <property name="secure.cookies.insert.content"><![CDATA[<cookie-name>@{cookie.name}</cookie-name>
        <tracking-enabled>true</tracking-enabled>
        <cookie-secure>true</cookie-secure>
        <cookie-http-only>true</cookie-http-only> ]]></property>

            <!-- Configure weblogic-application.xml file -->
            <replaceregexp file="@{weblogic.application.xml.location}">
                <regexp pattern="&lt;cookie-name&gt;(.*)@{cookie.name}(.*)&lt;/cookie-name&gt;"/>
                <substitution expression="${secure.cookies.insert.content}" />
            </replaceregexp>

        </sequential>
    </macrodef>

</project>
