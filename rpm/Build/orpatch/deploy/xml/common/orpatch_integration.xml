<?xml version="1.0"?>
<!--
/*===========================================================================
* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. 
* ===========================================================================
*/
-->
<project name="orpatch_integration" basedir=".">
    <description>ANT macros for integration of ant scripts with orpatch.</description>

    <!-- get path to this script so that it can find classes when called via import -->
    <dirname property="imported.basedir" file="${ant.file.weblogic}"/>
	
	<property name="deploy.base.dir" value="${input.retail.home}/orpatch/deploy" />
	<property name="ksh.location" value="/bin/ksh" />
	<property name="orpatch.env.conf.file.name" value="env_info.cfg" />
	
	<macrodef name="convert-to-upper">
        <attribute name="input.property" />
        <attribute name="output.property" />
        <sequential>
            <var name="UpProp" unset="true"/>
            <script language="javascript"> <![CDATA[
                    myProp="@{input.property}";			
                    myUpProp = project.getProperty(myProp).toUpperCase();
                    project.setUserProperty("UpProp",myUpProp)
            ]]> </script>
	    <property name="@{output.property}" value="${UpProp}" />
        </sequential>
    </macrodef>
	
	<macrodef name="loadInstallerCred">
		<attribute name="aliasproperty" />
	    <attribute name="usernameproperty" />
		<attribute name="passwordproperty" />
		<sequential>
			<property name="retail.installer.app.name" value="retail_installer" />
			<credManager usernameproperty="@{usernameproperty}" passwordproperty="@{passwordproperty}"
				aliasproperty="@{aliasproperty}" appnameproperty="retail.installer.app.name" wallet="${input.orpatch.deploy.config}" />
		</sequential>
	</macrodef>
	
	<macrodef name="saveInstallerCred">
		<attribute name="aliasproperty" />
	    <attribute name="usernameproperty" />
		<attribute name="passwordproperty" />
        <attribute name="orpatchintdir" />
		<sequential>
			<property name="retail.installer.app.name" value="retail_installer" />
			<credManager usernameproperty="@{usernameproperty}" passwordproperty="@{passwordproperty}"
				aliasproperty="@{aliasproperty}" appnameproperty="retail.installer.app.name" wallet="@{orpatchintdir}" createscript="false"/>
		</sequential>
	</macrodef>
	
	<macrodef name="copyCredManagerScripts">
		<attribute name="destdir" />
		<attribute name="srcdir" default="${deploy.base.dir}" />
		<sequential>
	       <!-- copy wallet scripts to domain -->
			<copy todir="@{destdir}">
				<fileset dir="@{srcdir}" includes="retail-public-security-api/**/*" />
			</copy>
 
			<!-- update wallet location in scripts --> 
			<replaceregexp match="@{srcdir}" replace="@{destdir}" flags="g">
				<fileset dir="@{destdir}/retail-public-security-api/bin" includes="*.sh"/>
			</replaceregexp>
			<chmod perm="700">
				<fileset dir="@{destdir}/retail-public-security-api/bin" includes="*.sh" />
			</chmod>
		
			<!-- Remove update wallet scripts -->
			<delete failonerror="false">
				<fileset dir="@{srcdir}/retail-public-security-api/bin" includes="update-*.sh"/>
			</delete>
		</sequential>
	</macrodef>
	
	<macrodef name="credManager" description="read or store credentials">
        <attribute name="usernameproperty" />
        <attribute name="passwordproperty" />
        <attribute name="aliasproperty" />
        <attribute name="appnameproperty" />
        <attribute name="wallet" />
		<attribute name="createscript" default="true" />
        <attribute name="apidir" default="${deploy.base.dir}/retail-public-security-api/bin" />
    	
        <sequential>
		<if>
			<equals arg1="@{wallet}" arg2="" trim="true" />
			<then>
				<fail message="ERROR: Wallet directory must be specified" />
			</then>
		</if>
        <!-- check alias and appname -->
        <if>
            <not><isset property="@{aliasproperty}" /></not>
            <then>
                <fail message="ERROR: The alias for property @{aliasproperty} must be set."/>
            </then>
        </if>
        <if>
            <not><isset property="@{appnameproperty}" /></not>
            <then>
                <fail message="ERROR: The appname for property @{appnameproperty} must be set."/>
            </then>
        </if>
        <var name="temp.alias" unset="true"/>
        <var name="temp.appname" unset="true"/>
        <propertycopy name="temp.alias" from="@{aliasproperty}" />
        <propertycopy name="temp.appname" from="@{appnameproperty}" />
        <if>
            <equals arg1="${temp.alias}" arg2="" trim="true" />
            <then>
                <fail message="ERROR: The alias for property @{aliasproperty} must be set."/>
            </then>
        </if>
        <if>
            <equals arg1="${temp.appname}" arg2="" trim="true" />
            <then>
                <fail message="ERROR: The appname for property @{appnameproperty} must be set."/>
            </then>
        </if>

        <!-- Check username and password -->
        <if>
            <and>
                <not><isset property="@{usernameproperty}" /></not>
                <not><isset property="@{passwordproperty}" /></not>
            </and>
            <then>
                <!-- read credentials from wallet -->
                <credentialStoreManager userNameAlias="${temp.alias}"
                    appLevelKeyPartitionName="${temp.appname}"
                    secretStoreWalletLocation="@{wallet}"
                    readUserNameInto="@{usernameproperty}"
                    readPasswordInto="@{passwordproperty}" />
            </then>
            <else>
                <var name="temp.username" unset="true"/>
                <var name="temp.password" unset="true"/>
                <propertycopy name="temp.username" from="@{usernameproperty}" />
                <propertycopy name="temp.password" from="@{passwordproperty}" />
                <if>
                    <and>
                        <equals arg1="${temp.username}" arg2="" trim="true" />
                        <equals arg1="${temp.password}" arg2="" trim="true" />
                    </and>
                    <then>
                        <!-- read credentials from wallet -->
                        <var name="@{usernameproperty}" unset="true"/>
                        <var name="@{passwordproperty}" unset="true"/>
                        <credentialStoreManager userNameAlias="${temp.alias}"
                            appLevelKeyPartitionName="${temp.appname}"
                            secretStoreWalletLocation="@{wallet}"
                            readUserNameInto="@{usernameproperty}"
                            readPasswordInto="@{passwordproperty}" />
                    </then>
                    <else>
                        <!-- write credentials to wallet - this may be first time install is run, app won't work if credentials are not in wallet -->
                        <credentialStoreManager userNameAlias="${temp.alias}"
                            userNameToSave="${temp.username}"
                            passwordToSave="${temp.password}"
                            appLevelKeyPartitionName="${temp.appname}"
                            secretStoreWalletLocation="@{wallet}" />
                    </else>
                </if>

                <!-- create alias update scripts -->
				<if>
					<equals arg1="@{createscript}" arg2="true" />
				<then>
                <echo file="@{apidir}/update-${temp.alias}.sh">
#!/bin/sh

usage() {
  echo "usage: update-${temp.alias}.sh &lt;username&gt;"
  echo ""
  echo "&lt;username&gt;: the username to update into this alias."
  echo ""
  echo "Example: update-${temp.alias}.sh myuser"
  echo ""
  echo "Note: this script will ask you for the password for the username that you pass in."
  echo ""
  exit 1
}

USER=$1
if [ -z "$USER" ]; then
  usage
fi

cd `dirname $0`
./save_credential.sh -a ${temp.alias} -p ${temp.appname} -u $USER -l @{wallet}

exit $$?
                </echo>
                <chmod perm="700" file="@{apidir}/update-${temp.alias}.sh" />
				</then>
				</if>
            </else>
        </if>
        </sequential>
    </macrodef>
	
	<macrodef name="echoTimestamp">
	<sequential>
      <var name="current.time" unset="true"/>
         <tstamp>
            <format property="current.time" pattern="yyyy-MM-dd HH:mm:ss" />
         </tstamp>
         <echo message="${current.time}" />
	</sequential> 
	</macrodef>
	
	<!-- Required since mkstore script doesn't seem to return non-zero exit codes, even on errors -->
    <macrodef name="mkstore-error-check">
        <attribute name="input" />
        <sequential>
            <var name="mkstore.error.message" unset="true"/>

            <!-- Regex matches string starting with "PKI-", any 5 digit number, and ": "...
                 Example: "PKI-02002: Error Message".  This is the error output format of mkstore,
                 which doesn't return error codes, making this check necessary -->
            <property name="mkstore.error.pattern" value="PKI-[0-9]{5}: .*$"/>

            <!-- Check for a PKI error in the output of the wallet command and fail if found -->
            <if>
                <matches string="@{input}" pattern="${mkstore.error.pattern}"/>

                <then>
                    <propertyregex property="mkstore.error.message"
                                   input="@{input}"
                                   regexp="(${mkstore.error.pattern})"
                                   select="\1" />
                    <fail message="ERROR: ${mkstore.error.message}"/>
                </then>
                <else/>
            </if>
        </sequential>
    </macrodef>

    <!-- This macro is used to create a wallet to store login information -->
    <macrodef name="create-wallet">
        <attribute name="wallet.password" />
        <attribute name="wallet.dir" />
        <attribute name="oracle.home" />
        <sequential>

            <!-- Initialize variables -->
            <var name="wallet.create.output" unset="true"/>

            <echo message="Creating wallet..." />
            <mkdir dir="@{wallet.dir}" />
            <!--Locks down the directory -->
            <chmod dir="@{wallet.dir}" perm="700" />
            <trycatch>
                <try>
                    <exec dir="@{wallet.dir}" executable="@{oracle.home}/bin/mkstore" failonerror="true"
                          inputstring="@{wallet.password}${line.separator}@{wallet.password}"
                          outputproperty="wallet.create.output">
                        <arg value="-wrl" />
                        <arg value="@{wallet.dir}" />
                        <arg value="-create" />
                    </exec>
                </try>
                <catch>
					<echo>${wallet.create.output}</echo>
                    <fail message="ERROR:  Unable to create wallet." />
                </catch>
            </trycatch>

			<echo>${wallet.create.output}</echo>

            <!-- Check for issues with mkstore -list command -->
            <mkstore-error-check input="${wallet.create.output}" />

            <!-- Creates custom sqlnet.ora -->
            <echo message="Creating file @{wallet.dir}/sqlnet.ora" />
            <echo file="@{wallet.dir}/sqlnet.ora">
WALLET_LOCATION = (SOURCE = (METHOD = FILE) (METHOD_DATA = (DIRECTORY = @{wallet.dir})))
SQLNET.WALLET_OVERRIDE=TRUE
SSL_CLIENT_AUTHENTICATION=TRUE
#SQLNET.AUTHENTICATION_SERVICES=(TCPS,NTS)</echo>
        </sequential>
    </macrodef>

    <!-- This macro is used to create a wallet to store login information -->
    <macrodef name="verify-wallet-password">
        <attribute name="wallet.password" />
        <attribute name="wallet.dir"/>
        <attribute name="oracle.home" />
        <sequential>
            <!-- Initialize variables -->
            <var name="wallet.password.test.output" unset="true"/>

            <echo message="Verifying password for wallet in @{wallet.dir}" />
            <trycatch>
                <try>
                    <!-- use mkstore -list command to test the wallet password provided -->
                    <exec dir="@{wallet.dir}" executable="@{oracle.home}/bin/mkstore" failonerror="true"
                          inputstring="@{wallet.password}${line.separator}@{wallet.password}" 
                          outputproperty="wallet.password.test.output">
                        <arg value="-wrl" />
                        <arg value="@{wallet.dir}" />
                        <arg value="-list" />
                    </exec>
                </try>
                <catch>
					<echo>${wallet.password.test.output}</echo>
                    <fail message="ERROR:  Unable to verify wallet password." />
                </catch>
            </trycatch>

            <!-- Check for issues with mkstore -list command -->
            <mkstore-error-check input="${wallet.password.test.output}" />

            <echo message="Password verified successfully" />
        </sequential>
    </macrodef>

    <!-- This macro adds credentials, username/passwords, to the wallet -->
    <macrodef name="add-wallet-credential">
        <attribute name="username" />
        <attribute name="password" />
        <attribute name="sid" />
        <attribute name="wallet.alias" />
        <attribute name="wallet.password" />
        <attribute name="wallet.dir" />
        <attribute name="oracle.home" />
        <sequential>
            <!-- Initialize variables -->
            <var name="tnsping.output" unset="true"/>
            <var name="tnsnames" unset="true"/>
            <var name="alias.check.output" unset="true"/>
            <var name="mod.credential.output" unset="true"/>
            <var name="add.credential.output" unset="true"/>

            <!-- Updates the custom tnsnames.ora -->
            <trycatch>
                <try>
                    <exec dir="@{wallet.dir}" outputproperty="tnsping.output"
                          executable="@{oracle.home}/bin/tnsping" failonerror="true">
                        <arg value="@{sid}" />
                    </exec>
                </try>
                <catch>
					<echo>${tnsping.output}</echo>
                    <fail message="ERROR:  Unable to ping database." />
                </catch>
            </trycatch>
            <propertyregex property="tnsnames"
                           input="${tnsping.output}"
                           regexp="\(DESCRIPTION.*"
                           select="\0" />

            <!-- Check to see if there is already an ifile entry in the existing tnsnames.ora for the alias -->
            <fileset id="alias.matches.in.tnsnames.ora" dir="@{wallet.dir}">
                <include name="tnsnames.ora"/>
                <containsregexp expression="ifile=.*tnsnames.ora.@{wallet.alias}" />
            </fileset>
            <if>
                <not><resourcecount when="greater" count="0" refid="alias.matches.in.tnsnames.ora" /></not>
                <then>
                    <echo message="Adding new ifile reference for @{wallet.dir}/tnsnames.ora.@{wallet.alias} to @{wallet.dir}/tnsnames.ora" />
                    <echo file="@{wallet.dir}/tnsnames.ora" append="true">
ifile=@{wallet.dir}/tnsnames.ora.@{wallet.alias}
</echo>
                </then>
                <else/>
            </if>

            <echo message="Adding entry for alias @{wallet.alias} to @{wallet.dir}/tnsnames.ora.@{wallet.alias}" />
            <echo file="@{wallet.dir}/tnsnames.ora.@{wallet.alias}">
@{wallet.alias} =
    ${tnsnames}
</echo> 

            <!-- Check if the alias already exists in the wallet -->
            <trycatch>
                <try>
                    <echo message="Checking for existence of credential with alias @{wallet.alias} in wallet @{wallet.dir}" />
                    <exec dir="@{wallet.dir}" executable="@{oracle.home}/bin/mkstore" failonerror="true"
                          outputproperty="alias.check.output"
                          inputstring="@{wallet.password}">
                        <arg value="-wrl" />
                        <arg value="@{wallet.dir}" />
                        <arg value="-listCredential" />
                    </exec>
                </try>
                <catch>
					<echo>${alias.check.output}</echo>
                    <fail message="ERROR:  Unable to check for alias in the wallet." />
                </catch>
            </trycatch>

            <!-- Check for issues with mkstore -listCredential command -->
            <mkstore-error-check input="${alias.check.output}" />

            <if>
                <contains string="${alias.check.output}" substring=": @{wallet.alias} " casesensitive="false"/> 
                <then>

                    <!-- Modify existing credential if it already exists -->
                    <trycatch>
                        <try>
                            <echo message="Modifying existing credential for alias @{wallet.alias} with user @{username} for wallet @{wallet.dir}" />
                            <exec dir="@{wallet.dir}" executable="@{oracle.home}/bin/mkstore" failonerror="true"
                                  outputproperty="mod.credential.output"
                                  inputstring="@{wallet.password}">
                                <arg value="-wrl" />
                                <arg value="@{wallet.dir}" />
                                <arg value="-modifyCredential" />
                                <arg value="@{wallet.alias}" />
                                <arg value="@{username}" />
                                <arg value="@{password}" />
                            </exec>
                        </try>
                        <catch>
							<echo>${mod.credential.output}</echo>
                            <fail message="ERROR:  Unable to modify credential in wallet." />
                        </catch>
                    </trycatch>

                    <echo>${mod.credential.output}</echo>

                    <!-- Check for issues with mkstore -modifyCredential command -->
                    <mkstore-error-check input="${mod.credential.output}" />

                </then>
                <else>
   
                    <!-- Adds credential if it doesn't already exist -->
                    <trycatch>
                        <try>
                            <echo message="Adding credential for alias @{wallet.alias} with user @{username} to wallet in @{wallet.dir}" />
                            <exec dir="@{wallet.dir}" executable="@{oracle.home}/bin/mkstore" failonerror="true"
                                  outputproperty="add.credential.output"
                                  inputstring="@{wallet.password}">
                                <arg value="-wrl" />
                                <arg value="@{wallet.dir}" />
                                <arg value="-createCredential" />
                                <arg value="@{wallet.alias}" />
                                <arg value="@{username}" />
                                <arg value="@{password}" />
                            </exec>
                        </try>
                        <catch>
							<echo>${add.credential.output}</echo>
                            <fail message="ERROR:  Unable to add credential to wallet." />
                        </catch>
                    </trycatch>

                    <echo>${add.credential.output}</echo>

                    <!-- Check for issues with mkstore -createCredential command -->
                    <mkstore-error-check input="${add.credential.output}" />

                </else>
            </if>

            <!-- write alias update script to config directory -->
            <echo message="Creating update script for alias @{wallet.alias}: @{wallet.dir}/update-@{wallet.alias}.sh" />
            <echo file="@{wallet.dir}/update-@{wallet.alias}.sh" append="false">
#!/bin/sh

ORACLE_HOME=@{oracle.home}
WALLET=@{wallet.dir}
ALIAS=@{wallet.alias}

echo ""
echo "Note: when prompted for your secret/Password, enter the NEW value you want to use."
echo ""

echo -n "Enter the username to associate with $$ALIAS (this can be the existing username or a new username): "
read USERNAME

echo ""
echo "Running $$ORACLE_HOME/bin/mkstore -wrl $$WALLET -modifyCredential $$ALIAS $$USERNAME"
echo ""
$$ORACLE_HOME/bin/mkstore -wrl $$WALLET -modifyCredential $$ALIAS $$USERNAME

exit $$?
            </echo>
            <chmod file="@{wallet.dir}/update-@{wallet.alias}.sh" perm="700" />
        </sequential>
    </macrodef>
	
	<!--
	 # Setup the initial RETAIL_HOME
	 -->
	<macrodef name="setup-base-orpatch">
		<attribute name="retail.home.dir" />
		<attribute name="src.orpatch.dir" />
		<attribute name="template.config.file" />
		<attribute name="filterset.refid" />
		<attribute name="extraconfig.fileset.refid" default="NOT_SET" />
        <sequential>
            <echo message="" />
            <echo message="Setting up ORPatch in @{retail.home.dir}" />
            <echo message="" />

            <!-- Make the RETAIL_HOME dir if it doesn't already exist -->
            <mkdir dir="@{retail.home.dir}" />

            <!-- Make the orpatch dir in RETAIL_HOME if it doesn't already exist -->
            <if>
                <not>
                    <available file="@{retail.home.dir}/orpatch/" />
                </not> 
                <then> 
                    <!-- Copy orpatch files packaged with the installer to RETAIL_HOME dir -->
                    <copy todir="@{retail.home.dir}/orpatch" overwrite="true">
                        <fileset dir="@{src.orpatch.dir}">
							<include name="**"/>
							<exclude name="**/templates/**"/>
						</fileset>
                    </copy>
					<if><not><equals arg1="@{extraconfig.fileset.refid}" arg2="NOT_SET"/></not>
					<then>
						<copy todir="@{retail.home.dir}/orpatch/config" overwrite="true">
							<fileset refid="@{extraconfig.fileset.refid}" />
						</copy>
					</then>
					</if>
					<chmod dir="@{retail.home.dir}/orpatch/bin" perm="740" includes="**/*" />
					<chmod dir="@{retail.home.dir}/orpatch/deploy/bin" perm="740" includes="**/*" />
					<chmod dir="@{retail.home.dir}/orpatch/deploy/ant/bin" perm="740" includes="**/*" />
				</then>
                <else/>
            </if>
			
			<mkdir dir="@{retail.home.dir}/orpatch/tmp" />
			<chmod dir="@{retail.home.dir}/orpatch/tmp" perm="700" />
			
            <!-- Create the Orpatch configuration file from templates --> 
            <copy tofile="@{retail.home.dir}/orpatch/tmp/${orpatch.env.conf.file.name}.temp" overwrite="true" filtering="true" verbose="true" >
				<fileset file="@{template.config.file}" />
				<filterset refid="@{filterset.refid}" />
            </copy>

            <if>
                <!-- If configuration file already exists in RETAIL_HOME (maybe from a previous install or another component installation)... -->
                <available file="@{retail.home.dir}/orpatch/config/${orpatch.env.conf.file.name}" />
                <then>
                    <!-- Then merge the changes from this config file into the pre-existing one -->
                    <run-orpatch-conf-update sourcefile="@{retail.home.dir}/orpatch/tmp/${orpatch.env.conf.file.name}.temp"
                                             retailhome="@{retail.home.dir}" />
                </then>
                <else>
                    <!-- Otherwise drop the new file out into RETAIL_HOME -->
                    <copy file="@{retail.home.dir}/orpatch/tmp/${orpatch.env.conf.file.name}.temp"
                          tofile="@{retail.home.dir}/orpatch/config/${orpatch.env.conf.file.name}"
                          overwrite="true" verbose="true" />
                </else>
            </if>
			<delete file="@{retail.home.dir}/orpatch/tmp/${orpatch.env.conf.file.name}.temp" />
        </sequential>
    </macrodef>

    <!-- Checks the list of *.retailhome.already.run properties to see if the RETAIL_HOME set for a specific component has already been
         used to run orpatch for a different component.  This is done to prevent orpatch from running multiple times in an installation
         where different components share the same RETAIL_HOME.  Sets a property <component>.already.run if a match is found -->
    <macrodef name="check-if-retailhome-already-run">
		<attribute name="component" />
        <attribute name="retailhome" />
        <sequential>
            <var name="retailhome.property.list" unset="true"/>

            <!-- Get a list of all properties with a name like *.retailhome.already.run -->
            <propertyselector property="retailhome.property.list"
                              delimiter=","
                              match=".*\.retailhome\.already\.run"
                              override="true" />

            <!-- Loop through the list and compare their RETAIL_HOME values with the RETAIL_HOME for the component passed into this macro -->
            <for list="${retailhome.property.list}" param="retailhome.already.run">
                <sequential>
                    <if>
                        <equals arg1="${@{retailhome.already.run}}" arg2="@{retailhome}" />
                        <then>
                            <!-- Set property for component if match is found -->
                            <property name="@{component}.retailhome.already.run" location="@{retailhome}" />
                        </then>
                        <else/>
                    </if>
                </sequential>
            </for>

        </sequential>
    </macrodef>

    <macrodef name="run-orpatch">
		<attribute name="component" />
        <attribute name="retailhome" />
		<attribute name="patch.dir" />
		<attribute name="freshinstall" default="false" />
        <sequential>

            <check-if-retailhome-already-run component="@{component}" retailhome="@{retailhome}" />

			<var name="freshinstall.arg" unset="true"/>
			<if>
				<equals arg1="@{freshinstall}" arg2="true" />
				<then>
					<property name="freshinstall.arg" value="-freshinstall" />
				</then>
				<else>
					<property name="freshinstall.arg" value="" />
				</else>
			</if>
			
            <if>
                <isset property="@{component}.retailhome.already.run" />
                <then>
                    <echo message="Orpatch has already been run during this installation for component @{component} at RETAIL_HOME @{retailhome}" />
                </then>
                <else>
                    <echo message="Running orpatch for RETAIL_HOME: @{retailhome}" />

                    <!-- Property used to determine later on if an retailhome for two different components matches,
                         so we don't run orpatch twice for the same retailhome -->
                    <property name="@{component}.retailhome.already.run" location="@{retailhome}" />

                    <exec dir="@{retailhome}/orpatch/bin" executable="${ksh.location}" failonerror="true" >
                        <arg value="./orpatch" />
                        <arg value="apply" />
                        <arg value="-s" />
                        <arg value="@{patch.dir}" />
						<arg value="${freshinstall.arg}" />
                        <env key="RETAIL_HOME" value="@{retailhome}"/>
						<env key="MMHOME" value="@{retailhome}"/>
                    </exec>
                </else>
            </if>
        </sequential>
    </macrodef>
	
    <macrodef name="run-orpatch-conf-update">
        <attribute name="sourcefile" />
        <attribute name="retailhome" />
        <sequential>
            <exec dir="@{retailhome}/orpatch/bin" executable="${ksh.location}" failonerror="true" >
                <arg value="./orcfgupd" />
                <arg value="-s" />
                <arg value="@{sourcefile}" />
                <arg value="-d" />
                <arg value="@{retailhome}/orpatch/config/${orpatch.env.conf.file.name}" />
                <env key="RETAIL_HOME" value="@{retailhome}"/>
                <env key="MMHOME" value="@{retailhome}"/>
            </exec>
        </sequential>
    </macrodef>

    <!-- Sets the value for an Orpatch action in an existing config file to N -->
    <macrodef name="disable-orpatch-action">
        <attribute name="action" />
        <attribute name="retailhome" />
        <sequential>
            <replaceregexp match="@{action}=.*" replace="@{action}=N" flags="g">
                <fileset file="@{retailhome}/orpatch/config/${orpatch.env.conf.file.name}"/>
            </replaceregexp>
        </sequential>
    </macrodef>

    <macrodef name="credManager_silentmode" description="read credentials from wallet for silent installer">
            <attribute name="usernameproperty" />
            <attribute name="passwordproperty" />
            <attribute name="alias" default="@{usernameproperty}"/>
            <attribute name="appname" default="DEFAULT_KEY_PARTITION_NAME" />
            <attribute name="wallet" default="${input.wallet.dir}" />
            <sequential>

            <if>
                <equals arg1="@{alias}" arg2="" trim="true" />
                <then>
                    <fail message="ERROR: The alias for the user must be set."/>
                </then>
            </if>
            <if>
                <equals arg1="@{usernameproperty}" arg2="" trim="true" />
                <then>
                    <fail message="ERROR: The attribute usernameproperty must be set."/>
                </then>
            </if>
            <if>
                <equals arg1="@{passwordproperty}" arg2="" trim="true" />
                <then>
                    <fail message="ERROR: The attribute passwordproperty must be set."/>
                </then>
            </if> 
            
            <var name="temp.username" value=""/>
            <var name="temp.password" value=""/>
            <propertycopy name="temp.username" from="@{usernameproperty}" silent="true" override="true" />
            <propertycopy name="temp.password" from="@{passwordproperty}" silent="true" override="true" />

            <if>
                <or>
				    <equals arg1="${temp.username}" arg2="" trim="true" />
                    <equals arg1="${temp.username}" arg2="" trim="true" />                    <equals arg1="${temp.password}" arg2="" trim="true" />
                </or>
                <then>
                    <credentialStoreManager userNameAlias="@{alias}"
                        appLevelKeyPartitionName="@{appname}"
                        secretStoreWalletLocation="@{wallet}"
                        readUserNameInto="@{usernameproperty}"
                        readPasswordInto="@{passwordproperty}" />
                </then>
                <else>
  
                    <credentialStoreManager userNameAlias="@{alias}"
                        userNameToSave="${temp.username}"
                        passwordToSave="${temp.password}"
                        appLevelKeyPartitionName="@{appname}"
                        secretStoreWalletLocation="@{wallet}" />
                     
                    <chmod dir="@{wallet}" type="file" perm="${wallet.files.permission}" includes="**" />
                </else>
            </if>
           </sequential>
            
      </macrodef> 
	
</project>
