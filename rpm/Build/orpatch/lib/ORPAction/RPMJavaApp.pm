############################################################################
# Copyright © 2014,2015, Oracle and/or its affiliates. All rights reserved.
############################################################################

package ORPAction::RPMJavaApp;
use strict;
our @ISA = qw(ORPAction::JavaApp);

#########################
# Sub:  CompileJarsForType
# Desc: We override the base JavaApp::CompileJarsForType slightly so that we can compile the OneSignAllSign jar list from the rpm JNLP template
#       once that is collected, we call the base version.
# Arg:  base_type - the base type to process archives for
#       dest_info - a ref to the filtered dest manifest list of base files
#       full_dest - the full destination directory
#       full_dest_info_ref - Ref to the unfiltered destination manifest
# Ret:  1 on success, 0 on failure
#########################
sub CompileJarsForType {
	my $self=shift;
	my ($base_type,$dest_info_ref,$full_dest,$full_dest_info_ref)=@_;

	#If this is the base type for the RPM ear file
	if ($base_type eq 'rpm_java_app') {		
		my $config_dir=$self->_GetConfigSubDirPrefix();
		my $found=0;
		for(my $i=17;$i>13;$i--) {
			my $jnlp_template=&::ConcatPathString($full_dest,$config_dir,sprintf('rpm.ear/WebLaunchServlet.war/WEB-INF/classes/template/rpm_jnlp_template.vm',$i));
			
			if (-f $jnlp_template) {
				if ($self->_SetAllSignListFromJNLP($jnlp_template)!=1) {
					return 0;
				}
				$found=1;
				last;
			}
		}
		if ($found!=1) {
			&::LogMsg("ERROR: Unable to locate rpm ear file in config tree!");
			return 0;
		}
	}
	
	#Now call the default JavaApp version of this method
	return $self->SUPER::CompileJarsForType($base_type,$dest_info_ref,$full_dest,$full_dest_info_ref);
}

#########################
# Sub:  _SetAllSignListFromJNLP
# Desc: Read the list of jars from a JNLP file and set the OneSignAllSign list based on them
# Arg:  jnlp_template - The path to the JNLP template file that has the list of jars to sign
# Ret:  1 on success, 0 on failure
#########################
sub _SetAllSignListFromJNLP {
	my $self=shift;
	my ($jnlp_template)=@_;
	
	&::LogMsg("Collecting JNLP jar list");

	#Read the template file and grab all the jars that need to be signed with the same cert
	my @all_sign=();
	unless(open(JNLP_FILE,"<$jnlp_template")) {
		&::LogMsg("Error opening JNLP template $jnlp_template: $!");
		return 0;
	}
	while(<JNLP_FILE>) {
		chomp;
		#        <jar href="lib/castor-1.3.2-xml.jar" />
		if (/<jar href="([^"]+)"/) {
			my $jar=$1;
			push(@all_sign,$jar);
		}
	}
	close(JNLP_FILE);
	
	&::LogMsg("   Identified ".scalar(@all_sign)." jars in JNLP");
	
	$self->SetOneSignAllSign(\@all_sign);
	
	return 1;
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='JAVAAPP_RPM';
	my $batch_subdir='rpm-batch';
	
	my $batch_type='rpm_java_batch';
	my $batch_fcp=new FileCopyParams($batch_type,$batch_subdir,-2);
	#Permissions are set in javaapp_rpm.xml
	#$batch_fcp->SetFilePerms('0755');
	
	my @file_type_info=(
	#Type               Archive copies, Sign jars,	FileCopyParam
	['rpm_java_app',	1,				1,			undef],
	#Non-Archive copies
	#Type               Archive copies, Sign jars, FileCopyParam
	[$batch_type,		0,				0,		   $batch_fcp],
	);
	
	my $self=$class->SUPER::new($action_name,\@file_type_info,$config_ref);
	
	$self->{'BATCH_SUBDIR'}=$batch_subdir;
	
    return $self;
}

1;
