############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RMSForms;
our @ISA = qw(ORPAction::OraForms);

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
    my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='ORAFORMS_RMS';	
	my $toolset_name='rms_frm_toolset';
	my $form_name='rms_frm_forms';
	my $rfm_form_name='rfm_frm_forms';
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, don't compile rwms reference forms
	#
	#[file_type,when_run,process_function,std_run_catch_errors,keep_plx]
	#   file_type_ref is a reference to an array of file types to process
	#   when_run is 0 - never run, 1 - run if updated, 2 - always run, 
	#               3 - force compile of subsequent types on any changes, 4 - full compile if any file changes
	#   process_function is '' to use the standard OraForms logic for compiling files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	#   keep_plx is 1 if we should keep plx's after compilation, 0 if not
	my @file_type_info=(
	[$toolset_name,		4,'',0,0],
	[$form_name,		2,'',0,0],
	);


	my @form_types=($form_name);


	# If the RFM is enabled in the Config ,migrate the Forms.
	my $rfm_form_mode=$config_ref->ValidateTFVar("${action_name}_INCLUDE_RFM",0);
	if ($rfm_form_mode==1) {
		&::LogMsg("Retail Fiscal Management Forms Enabled. ");
		
	my @rfm_file_type_info=(
	[$rfm_form_name,		2,'',0,0],
	);
	  push (@file_type_info,@rfm_file_type_info);	
      push(@form_types,$rfm_form_name); 
		}
		
	my @file_copy_params=();
	push(@file_copy_params,new FileCopyParams($toolset_name,'base/toolset/src',0));
	push(@file_copy_params,new FileCopyParams(\@form_types,'base/forms/src',0));
	
	my $self=$class->SUPER::new('ORAFORMS_RMS',\@file_copy_params,\@file_type_info,$config_ref);
    return $self;
}

1;
