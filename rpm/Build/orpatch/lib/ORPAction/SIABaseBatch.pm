############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::SIABaseBatch;
our @ISA = qw(ORPAction);


#########################
# Sub:  PatchAction
# Desc: Create empty directories
#########################
sub PatchAction {
	my $self=shift;

	#Get the configuration for our patch
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	my $dest_prefix=$self->GetDestPrefix();
	
	foreach my $dir ('data/infile','data/outfile','data/reject','data/log','scripts/log') {
		my $create_dir=&::ConcatPathString($dest_dir,$dest_prefix,$dir);
		if (!-d $create_dir) {
			&::CreateDirs('0755',$create_dir);
		}
	}
	
	#Setup i18n symlinks
	my $lang=$self->{'RASEBATCH_LANG_CODE'};
	$lang='en' if $lang eq '';

	if (&RASECoreBatch::SetupI18NLinks($dest_dir,$dest_prefix,$lang)!=1) {
		return 0;
	}
	
	return 1;
}

sub GetCommonExportTypes {
	my $self=shift;
	return @{$self->{'COMMON_EXPORT'}};
}

sub GetDestPrefix {
	my $self=shift;
	return $self->{'DEST_PREFIX'};
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref,$action_name,$module)=@_;
	
    my $dest_prefix="rse-home/cdm/${module}";
       $dest_prefix=(($module eq 'aso') ? "rse-home/so" : "$dest_prefix");
       $dest_prefix=(($module eq 'rl') ? "rse-home/ai/rl" : "$dest_prefix");
       
	my $type_prefix="rase${module}";
	
	#ASO has one fewer directories to strip
	my $seed_strip=(($module eq 'aso') ? -2 : -3);
	my $scripts_strip=(($module eq 'aso') ? -2 : -3);
	#Strip off directories cdm/dist/<app>/artifacts/*.jar or so/dist/so/artifacts/*.jar
	my $export_strip=(($module eq 'aso') ? -4 : -4);
	
	my @file_copy_params=();
	
	my $seed_fcp=new FileCopyParams(["${type_prefix}_batch_ctlfiles","${type_prefix}_batch_seedfiles"],"$dest_prefix/data",$seed_strip);
	push(@file_copy_params,$seed_fcp);
	
	my $scripts_fcp=new FileCopyParams(["${type_prefix}_batch_script_bin","${type_prefix}_batch_script_lib"],"$dest_prefix/scripts",$scripts_strip);
	$scripts_fcp->SetFilePerms({"${type_prefix}_batch_script_bin"=>'0755'});
	push(@file_copy_params,$scripts_fcp);
	
	my $self=$class->SUPER::new($action_name,\@file_copy_params,$config_ref);
	bless($self,$class);
	
	$self->{'COMMON_EXPORT'}=[["${type_prefix}_batch_application",$export_strip]];
	$self->{'DEST_PREFIX'}=$dest_prefix;
	$self->{'MODULE'}=$module;
	$self->{'RASEBATCH_LANG_CODE'}=$config_ref->Get('RASEBATCH_LANG_CODE');
	
	return $self;
}

1;
