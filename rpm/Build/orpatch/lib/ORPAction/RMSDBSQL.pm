############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RMSDBSQL;
our @ISA = qw(ORPAction::DBSQL);

use Cwd;

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects and create any new synonyms for RMS users
#########################
sub PostAction {
	my $self=shift;
	
	#Compile Invalid Objects
	if ($self->CompileInvalidsInSharedSchema('DBSQL_RMS')!=1) {
		return 0;
	}
	
	#Create synonyms for users using our utilities/synonym_check.sql script
	#We do this as a postaction so that we only have to create synonyms once
	#after RMS/RPM/ReIM/Alloc have all had a chance to create their new objects
	if ($self->CreateUserSynonyms('RMS')!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  LoadRestartThread
# Desc: Custom processing to load rstthret.pls prior to views to reduce the number of invalids
#       Only applicable on initial install
#########################
sub LoadRestartThread {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,$subpatch_ref)=@_;

	#Loading rstthret.pls before the views reduces the number of invalids significantly
	#Not strictly required but reduces the amount of recompiling at the end

	my $db_proc_name=$self->{'RMS_DB_PROC_NAME'};

	my @dest_info=&Manifest::ReadManifest($dest_manifest);
	my $filtered=[];
	foreach my $ref (@dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		if ($dfile=~/rstthret\.pls/) {
			push(@$filtered,$ref);
			#We only run this out of sequence on the first install
			#which would require the file to have been updated by this patch
			my $filtered=&Manifest::FilterDestManifest($filtered,'',$patch_name,$subpatch_ref);
			#And not have already been run into the database
			$filtered=$self->FilterFilesByDBManifest($filtered,$db_proc_name);
			
			if (scalar(@$filtered)==0) {
				#Does not need to be run
			} else {
				if ($self->RunDestFileList($full_dest,$filtered,1)!=1) {
					#Problems running a script, abort
					return 0;
				}
			}
			last;
		}
	}
	
	return 1;
}

#########################
# Sub:  LoadRPMTypes
# Desc: a custom function to load RPM types and skip the CreateAllTypes.sql file
#########################
sub LoadRPMTypes {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
	
	my $filtered=[];
	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		if ($dfile=~/CreateAllTypes.sql/) {
			#Skip the all types file
		} else {
			push(@$filtered,$ref);
		}
	}
	
	return $self->LoadTypes($full_dest,$filtered,$patch_name,$dest_manifest);
}

#########################
# Sub:  LoadRMSPackagesForUTLPLSQL
# Desc: a custom function to run an alter session before running RMS packages
#########################
sub LoadRMSPackagesForUTLPLSQL {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
	
	return $self->RunDestFileListWithInitCmd($full_dest,$self->{'UTPLSQL_ALTER_SESSION'},$filtered_dest_info,[]);
}

########################
# Sub:  CompileInvalidsOnce
# Desc: Override CompileInvalidsOnce so that we can do an alter system when compiling objects in a UTPLSQL env
# Ret:  1 if successful, 0 if not
########################
sub CompileInvalidsOnce {
	my $self=shift;
	my ($num)=@_;
	
	my @lines=('@@recompile_invalids.sql',"select 'COMPLETE - recompile_invalids' from dual");
	if ($self->{'UTPLSQL_ALTER_SESSION'} ne '') {
		unshift(@lines,$self->{'UTPLSQL_ALTER_SESSION'});
	}

	my $SQL=join("\n",@lines);
	
	&::LogMsg("Compiling $num invalid objects...");
	
	return $self->DoSQL('recompile_invalids.sql',$SQL,[],1);
}

#########################
# Sub:  LoadRIBClob
# Desc: a custom function to load the RIB clob objects
#########################
sub LoadRIBClob {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;

	my $rib_dir=&::ConcatPathString($full_dest,'Cross_Pillar/rib_objects/ddl/clob');
	my $rib_dir2=&::ConcatPathString($full_dest,'Cross_Pillar/rib_objects/ddl/kernel');
	local $ENV{SQLPATH}="$rib_dir:$rib_dir2:$ENV{SQLPATH}";
	
	my @rib_scripts=(
	'run_1_CLOB_CREATE_OBJECTS.SQL',
	'run_2_CLOB_INSERT_VALUES.SQL',
    'run_1_KERNEL_CREATE_OBJECTS.SQL',
	);
	
	return $self->FindAndRunScriptList($full_dest,$filtered_dest_info,$orig_f_dest_info,@rib_scripts);
}

#########################
# Sub:  LoadRIBControlScripts
# Desc: a custom function to load the RIB control scripts
#########################
sub LoadRIBControlScripts {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;

	#This type is not currently used because the script is not re-runnable and couldn't be moved
    #from ddl/clob.  In the future, when the script is moved, the run_2_CLOB_INSERT_VALUES.SQL
    #can be removed from LoadRIBClob and this return can be removed.
    return 1;
	
	my $rib_dir=&::ConcatPathString($full_dest,'Cross_Pillar/rib_objects/control_scripts');
	local $ENV{SQLPATH}="$rib_dir:$ENV{SQLPATH}";
	
	my @rib_scripts=(
	'run_2_CLOB_INSERT_VALUES.SQL',
	);
	
	return $self->FindAndRunScriptList($full_dest,$filtered_dest_info,$orig_f_dest_info,@rib_scripts);
}

#########################
# Sub:  LoadRPMInstallScripts
# Desc: a custom function to load the RPM Install Scripts
#########################
sub LoadRPMInstallScripts {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
	
	return $self->FindAndRunScriptList($full_dest,$filtered_dest_info,$orig_f_dest_info,'rpmctl.sql','rsmrpm.sql');
}

#########################
# Sub:  LoadRMSInstallScripts
# Desc: a custom function to load the RMS Install Scripts
#########################
sub LoadRMSInstallScripts {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,$subpatch_ref)=@_;
	
	my $lang_dir=&::ConcatPathString($full_dest,'Cross_Pillar/languages');
	my $felm=&::ConcatPathString($full_dest,'Cross_Pillar/ui_toolset/form_elements/source');
	my $cscripts=&::ConcatPathString($full_dest,'Cross_Pillar/control_scripts/source');
	local $ENV{SQLPATH}="$lang_dir:$felm:$cscripts:$ENV{SQLPATH}";
	
	my $iscripts_dir=&::ConcatPathString($full_dest,'Cross_Pillar/install_scripts/source');
	
	my @install_scripts=(
	'run_rmsseeddata.sql'
	);
	
	my ($proceed,$found_scripts_ref,$missing_scrips_ref)=$self->FindFilteredFilesInOrder($filtered_dest_info,$orig_f_dest_info,@install_scripts);
	
	#If required files were missing, abort
	unless($proceed==1) {
		return 0;
	}

	if (scalar(@$found_scripts_ref)==0) {
		#No install scripts to run
		return 1;
	}
	
	#Run rmsseeddata from the install scripts directory
	return $self->RunDestFileListByBaseName($full_dest,$found_scripts_ref,[]);
}

#########################
# Sub:  LoadLanguages
# Desc: Load the appropriate primary and secondary language scripts
#########################
sub LoadLanguages {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,$subpatch_ref)=@_;
	
	#Language scripts can take a long time to run, so we have an option to only run the ones that changed
	#note that we're not careful about whether a language changed only in the primary
	#and we have secondary installed or vice-versa.  If anything about the language changed
	#we rerun the appropriate type for that language based on what is installed
	
	my $only_upd_langs=$self->{'ONLY_UPDATED_LANGS'};
	
	#If this is a hotfix, then we always only run updated langs
	if ($self->{'PATCH_MODE'} eq 'HOTFIX') {
		$only_upd_langs=1;
	}
	
	#Build a list of the changed files
	my $f_dest_info;
	if ($only_upd_langs==1) {
		#If we are only running updated files, filter the complete dest manifest for things updated with this patch
		&::LogMsg("Filtering language scripts for only those updated in this patch");
		$f_dest_info=&Manifest::FilterDestManifest($orig_f_dest_info,'',$patch_name,$subpatch_ref);
	} else {
		#Otherwise pretend like all files were updated with this patch
		$f_dest_info=$orig_f_dest_info;
	}
	
	#Go through the changed files and determine what languages they apply to
	my %updated_langs=();
	foreach my $ref (@$f_dest_info) {
		my ($dfile,@discard)=&Manifest::DecodeDestLine($ref);
		
		#There various types of file names but they always end in _<lang code>.sql
		if ($dfile=~/\.sql$/) {
			if ($dfile=~/_([a-zA-Z]+)\.sql$/) {
				my $lang=$1;
				$lang=~tr/A-Z/a-z/;
				$updated_langs{$lang}=1;
			} else {
				&::LogMsg("Unable to identify language for file: $dfile");
				return 0;
			}
		} else {
			#Ignore non sql files
		}
	}
	
	if (scalar(keys(%updated_langs))==0) {
		&::LogMsg("No language scripts were updated");
	}
	
	#Get the primary language
	my $prim_lang=$self->GetPrimaryLanguage();
	return 0 if ($prim_lang eq '');
	
	my @lang_scripts=();
	
	#If the primary language is not English and the script was updated, run the primary script
	#English is always run during form elements scripts
	if ($prim_lang ne 'en') {
		if ($updated_langs{$prim_lang}==1) {
			push(@lang_scripts,"rms_primary_${prim_lang}.sql");
		}
	}
	
	#Get any secondary languages that are installed
	my ($status,@sec_langs)=$self->GetSecondaryLanguages();
	return 0 if ($status!=1);
		
	foreach my $lang (@sec_langs) {
		if ($updated_langs{$lang}==1) {
			push(@lang_scripts,"rms_secondary_${lang}.sql");
		}
	}
	
	if (scalar(@lang_scripts)==0) {
		&::LogMsg("No installed languages were updated.");
		return 1;
	}

	#Find the language scripts we need to run, always looking in the unfiltered manifest
	my ($found_scripts_ref,$missing_scripts_ref)=$self->FindReqFilesInOrder($orig_f_dest_info,@lang_scripts);
		
	#It is possible that a customer has installed a custom language that we don't have scripts for
	#so don't abort if there are language scripts that we wanted to run but can't find.  Just log it.
	if (scalar(@$missing_scripts_ref)!=0) {
		foreach my $file (@$missing_scripts_ref) {
			&::LogMsg("Language Script $file not found, skipping");
		}
	}
	
	#Run the languages scripts from the language directory
	return $self->RunDestFileListByBaseName($full_dest,$found_scripts_ref,[]);
}

#########################
# Sub:  CreateSynonymsToBDISchema
# Desc: Create the synonyms from RMS schema to the BDI Integration schema objects
#       required for RMS BDI packages to compile
#########################
sub CreateSynonymsToBDISchema {
	my $self=shift;

	#Get the BDI Integration schema name using the DBSQL_RMSBDIINT connection information
	my $bdi_int_schema_name=$self->GetCrossActionSchemaName('DBSQL_RMSBDIINT');
	return 0 if $bdi_int_schema_name eq '';
	
	#Create synonyms in RMS schema pointing to all objects in BDI Integration schema
	unless($self->CreateSynonymsForUser('',$bdi_int_schema_name)==1) {
		&::LogMsg("Failed to create synonyms to $bdi_int_schema_name schema");
		return 0;
	}
}

#########################
# Sub:  LoadWSConsumerPackages
# Desc: Load the WebService Consumer PL/SQL packages
#########################
sub LoadWSConsumerPackages {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,$subpatch_ref)=@_;

	#Find and drop and create scripts as those are the only ones we run
	my @drop_scripts=();
	my @create_scripts=();
	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		if ($dfile=~/_drop\.sql$/) {
			push(@drop_scripts,$ref);
		} elsif ($dfile=~/_create\.sql$/) {	
			push(@create_scripts,$ref);
		} else {
			#Ignore grant/revoke scripts
		}
	}
	
	my @ws_scripts=(@drop_scripts,@create_scripts);

	my @filter=(
	qr/ORA\-0*4043/,	#ORA-04043: object OBJ_RPM_SYSTEM_OPTIONS_REC does not exist
	qr/ORA\-0*2303/,	#ORA-02303: cannot drop or replace a type with type or table dependents
	);
	
	return $self->RunDestFileListWithErrorFilter($full_dest,\@ws_scripts,\@filter);
}

#########################
# Sub:  LoadARIInterface
# Desc: Load the ARI interface package
#########################
sub LoadARIInterface {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,$subpatch_ref)=@_;
	
	if (scalar(@$filtered_dest_info)==0) {
		#Nothing to run
		return 1;
	}
	
	#If ARI is installed, the package in RMS schema is actually a synonym so we do NOT load the package.
	#If there is not a synonym, then we load the package
	
	my $ari_status=$self->IsARIInterfaceASynonym();
	
	if ($ari_status==-1) {
		#errors, abort this load
		return 0;
	} elsif ($ari_status==1) {
		#ARI_INTERFACE_SQL is a synonym, don't load the package and return success
		&::LogMsg("ARI appears to be installed, skipping stub interface load");
		return 1;
	}
	
	#Load the packages
	return $self->RunDestFileList($full_dest,$filtered_dest_info,1);
}

#########################
# Sub:  IsARIInterfaceASynonym
# Desc: Get the status of the ARI package
# Arg:  None
# Ret:  1 if ARI Interface SQL is a synonym, 0 if not, -1 if there was an error
#########################
sub IsARIInterfaceASynonym {
	my $self=shift;
	
	&::LogMsg("Checking state of ARI Interface package");
	my $exists_sql="select count(1) from user_objects where object_name = 'ARI_INTERFACE_SQL' and object_type='SYNONYM'";

	my ($output_ref,$error_ref)=$self->CallExecSQL($exists_sql);
	
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Unable to check ARI Interface object existence");
		&::DebugOutput("ARI Interface Exists",@$output_ref);
		return -1;
	}

	my $obj_count=$$output_ref[0];
	chomp($obj_count);
	$obj_count=~s/\s*//g;
	
	if ($obj_count==0) {
		#ARI Interface SQL is not a synonym
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  GetPrimaryLanguage
# Desc: Get the primary language in RMS
# Arg:  None
# Ret:  primary language iso code, or '' if errors
#########################
sub GetPrimaryLanguage {
	my $self=shift;
	
	&::LogMsg("Getting Primary Language");
	my $sql="select l.iso_code from system_options s, lang l where s.data_integration_lang = l.lang";
	my ($output_ref,$error_ref)=$self->CallExecSQL($sql);
	
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Unable to retrieve primary language!");
		&::DebugOutput("get primary language",@$output_ref);
		return '';
	}

	my $prim_lang=$$output_ref[0];
	chomp($prim_lang);
	$prim_lang=~tr/A-Z/a-z/;
	$prim_lang=~s/\s*$//g;
	
	if ($prim_lang eq '') {
		&::LogMsg("Unable to determine primary language!");
		&::LogMsg("No results returned from $sql");
	}
	
	return $prim_lang;
}

#########################
# Sub:  GetSecondaryLanguages
# Desc: Get the secondary languages in RMS
# Arg:  None
# Ret:  status (1 if success),array of secondary language iso codes
#########################
sub GetSecondaryLanguages {
	my $self=shift;
	
	&::LogMsg("Getting Secondary Languages");
	my $sql="select distinct l.iso_code from code_detail_tl f,lang l,system_options o where f.lang = l.lang and f.lang !=1 and l.lang != o.data_integration_lang";
	my ($output_ref,$error_ref)=$self->CallExecSQL($sql);
	
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Unable to retrieve secondary languages!");
		&::DebugOutput("get secondary languages",@$output_ref);
		return(0,'');
	}
	
	my @langs=();
	foreach my $line (@$output_ref) {
		chomp($line);
		$line=~tr/A-Z/a-z/;
		$line=~s/\s*$//g;
		push(@langs,$line);
	}
	
	return (1,@langs);
}

#########################
# Sub:  GetCheckLockedObjectsSQL
# Desc: Override the default check locked object SQL statement so that we only look for RPM objects if
#       only RPM objects are being migrated
# Ret:  SQL statement to use when checking locked objects
#########################
sub GetCheckLockedObjectSQL {
	my $self=shift;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	my @product_list=$patch_info->GetProductList();
	
	my $SQL='';
	
	if (scalar(@product_list)==1 && $product_list[0] eq 'RPM') {
		#If there is only one product and it is RPM, only look for locked object names starting with RPM_
		$SQL=<<ENDSQL;
alter session set "_optimizer_cartesian_enabled"=false;
SELECT a.session_id||'|'||a.oracle_username||'|'||count(1)
FROM V\$LOCKED_OBJECT A,user_objects b
WHERE A.OBJECT_ID = B.OBJECT_ID
and (b.object_name like 'RPM_\%' or b.object_name like 'RSM_\%')
group by a.session_id,a.oracle_username
ENDSQL
	} else {
		#If multiple products are involved, use the standard SQL
		$SQL=$self->SUPER::GetCheckLockedObjectSQL();
	}

	return $SQL;
}

#########################
# Sub:  LoadPLSQLLogger
# Desc: a custom function to load the PL/SQL Logger objects
#########################
sub LoadPLSQLLogger {
               my $self=shift;
               my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
               
               my @script_list=('logger_install.sql');

               return $self->FindAndRunScriptList($full_dest,$filtered_dest_info,$orig_f_dest_info,@script_list);
}


#########################
# Sub:  RunGrantsToRARMSBATCH
# Desc: Run the grants to the RARMSBATCH Core schema from RMS.
#       Note: This function is only called as part of the RARMSBatchDBSQL actions, it is not run during RMS patches
#########################
sub RunGrantsToRARMSBATCH {
	my $self=shift;
	my ($full_dest,$rarmsbatch_schema_name,$filtered_dest_info)=@_;
	
	if (scalar(@$filtered_dest_info)==0) {
		return 1;
	}
	
	#RARMSBATCH installs to call RunGrantsToRARMSBATCH without a PatchAction
	if ($self->CreateDBManifestTable()!=1) {
		&::LogMsg("Failed to create manifest table!");
		return 0;
	}
	
	return $self->RunDestFileListWithArgs($full_dest,$rarmsbatch_schema_name,$filtered_dest_info,[]);
}


#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='DBSQL_RMS';
	
	#We use this in the db_rib_top function, so we want to make sure the
	#name matches identically
	my $rib_subtype_name='rms_db_rib';
	
	#We use this in LoadRestartThread
	my $rms_db_procs='rms_db_procedures';
	
	#Allow skipping all DBC scripts when DBSQL_RMS_RUN_DBC=N
	my $dbc_mode=&DBSQL::ValidateRunDBC($config_ref,$action_name);
	
	#If this is a UTPLSQL environment, make sure all RMS packages get an alter session command
	my $utplsql_mode=$config_ref->ValidateTFVar("${action_name}_UTPLSQL",0);
	my $load_pkg_sub='';
	my $utlplsql_alter='';
	if ($utplsql_mode==1) {
		$load_pkg_sub='LoadRMSPackagesForUTLPLSQL';	
		$utlplsql_alter="alter session set plsql_ccflags = 'UTPLSQL:TRUE';";
	}
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors,product_name,run_phase,del_func]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   6 - always run all files of this type as if it was a cumulative patch, even during an incremental patch
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	#   product_name if a type should only be run with a patch for a particular product
	#   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
	#   del_func - Optional function that should be run whenever a file of this type is deleted
	my @file_type_info=(
	['rms_db_ddl',					3,'',						1,'RMS','PATCHACTION',''],
	['rpm_db_ddl',					3,'',						1,'RPM','PATCHACTION',''],
	['rms_db_logger_install',       2,'',						1,'RMS','PATCHACTION',''],
	['restart_thread',				3,'LoadRestartThread',		1,'RMS','PATCHACTION',''],
	['rms_db_ddl_mv',				3,'',						1,'RMS','PATCHACTION',''],
	['rms_db_ddl_vw',				3,'',						1,'RMS','PATCHACTION',''],
	['rpm_db_types',				2,'LoadRPMTypes',			1,'RPM','PATCHACTION',''],
	['rms_db_types',				2,'LoadTypes',				1,'RMS','PATCHACTION',''],
	['rms_db_ddl_rib_kernel_clob',	3,'LoadRIBClob',			1,'RMS','PATCHACTION',''],
	['rms_db_ddl_rib_clob_data',	3,'LoadRIBControlScripts',	1,'RMS','PATCHACTION',''],
	['rms_db_change_scripts_rib',	$dbc_mode,'',				1,'RMS','PATCHACTION',''],
	['rms_db_ddl_rib_packages',		2,'',						1,'RMS','PATCHACTION',''],
	[$rib_subtype_name,				1,'LoadRIBSubTypes',		1,'RMS','PATCHACTION',''],
	['rms_db_rib_top',				1,'LoadRIBTopTypes',		1,'RMS','PATCHACTION',''],
	['rpm_db_change_scripts',		$dbc_mode,'',				1,'RPM','PATCHACTION',''],
	['rms_db_change_scripts',		$dbc_mode,'',				1,'RMS','PATCHACTION',''],
	[$rms_db_procs,					2,$load_pkg_sub,			1,'RMS','PATCHACTION',''],
	['rpm_db_packages',				2,'',						1,'RPM','PATCHACTION',''],
	['rms_db_packages',				2,$load_pkg_sub,			1,'RMS','PATCHACTION',''],
	['rmsbdi_db_rms_packages',		2,$load_pkg_sub,			1,'RMS','PATCHACTION',''],
	['rms_db_external_ref',			2,'LoadARIInterface',		1,'RMS','PATCHACTION',''],
	['rms_db_triggers',				2,'',						1,'RMS','PATCHACTION',''],
	['rpm_db_triggers',				2,'',						1,'RPM','PATCHACTION',''],
	['rpm_db_install_scripts',		3,'LoadRPMInstallScripts',	1,'RPM','PATCHACTION',''],
	['rms_db_form_elements',		2,'',						1,'RMS','PATCHACTION',''],
	['rms_db_installscripts',		3,'LoadRMSInstallScripts',	1,'RMS','PATCHACTION',''],
	['rms_db_control_scripts',		2,'',						1,'RMS','PATCHACTION',''],
	['rpm_db_control_scripts',		2,'',						1,'RPM','PATCHACTION',''],	
	['rms_dummy_libraries',			3,'',						1,'RMS','PATCHACTION',''],
	['rms_db_languages',			2,'',						1,'RMS','PATCHACTION',''],
	['rms_db_languages_seed',		0,'',						0,'RMS','PATCHACTION',''],
	['rms_db_replace_dynamic_token',6,'',						1,'RMS','PATCHACTION',''],
	['rms_db_ws_consumer_libs',		2,'LoadDBJarsGroup',		1,'RMS','PATCHACTION','DropDBJar'],
	['rms_db_ws_consumer_jars',		2,'LoadDBJarsSingly',		1,'RMS','PATCHACTION','DropDBJar'],
	['rms_db_ws_consumer_sql',		2,'LoadWSConsumerPackages',	1,'RMS','PATCHACTION',''],
	);
	
	if ($utplsql_mode==1) {
		#Include the PLSQL file types
		&::LogMsg("Including UTPLSQL types in processing list.");
		my @utplsql_type_info=(
		['rms_db_ut_dbc',			$dbc_mode,'',				1,'RMS','PATCHACTION',''],
		['rpm_db_ut_dbc',			$dbc_mode,'',				1,'RPM','PATCHACTION',''],
		['rpm_db_ut_packages',		2,'',						1,'RPM','PATCHACTION',''],
		['rms_db_ut_packages',		2,$load_pkg_sub,			1,'RMS','PATCHACTION',''],
		['rms_db_ut_control',		2,'',						1,'RMS','PATCHACTION',''],
		['rpm_db_ut_control',		2,'',						1,'RPM','PATCHACTION',''],
		);
		push(@file_type_info,@utplsql_type_info);
	}
	
	my $self=$class->SUPER::new($action_name,\@file_type_info,$config_ref);
	$self->{'RIB_SUBTYPE_NAME'}=$rib_subtype_name;
	$self->{'RMS_DB_PROC_NAME'}=$rms_db_procs;
	$self->{'UTPLSQL_ALTER_SESSION'}=$utlplsql_alter;
	
	#Allow languages to be only run when updated
	my $only_updated_langs=0;
	if ($config_ref->ValidateTFVar("${action_name}_RUN_ONLY_UPDATED_LANGS",0)==1) {
		$only_updated_langs=1;
	}
	$self->{'ONLY_UPDATED_LANGS'}=$only_updated_langs;
	
	$self->SetRequiredTablespaces('RETAIL_DATA','RETAIL_INDEX','LOB_DATA','ENCRYPTED_RETAIL_DATA','ENCRYPTED_RETAIL_INDEX');

	return $self;
}

1;
