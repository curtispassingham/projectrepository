############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::ORPatchUpdate;
our @ISA = qw(ORPAction);

#Note this isn't a real action, we only ever execute CopyPatchFiles

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
    my $class=shift;
	my ($config_ref)=@_;
	
	my @file_copy_params=();
	
	#Most orpatch directories are handled without preserve dirs and with special permissions
	foreach my $ref (['orpatch_utils','orpatch/utilities'],
	                 ['orpatch_bin','orpatch/bin'],
					 ['orpatch_lib','orpatch/lib'],
					 ['orpatch_lib_actions','orpatch/lib/ORPAction']) {
		my ($type,$dir)=@$ref;
		my $fcp=new FileCopyParams($type,$dir,0);
		$fcp->SetFilePerms('0750');
		push(@file_copy_params,$fcp);
	}
	
	#ORPatch deploy files are handled with preserve dirs
	my $fcp=new FileCopyParams('orpatch_deploy','orpatch/deploy',-3);
	$fcp->SetFilePerms('0750');
	push(@file_copy_params,$fcp);
	
	my $self=$class->SUPER::new('ORPATCH',\@file_copy_params,$config_ref);
	
	$self->SetValidOnAnyHost(1);

    return $self;
}

1;
