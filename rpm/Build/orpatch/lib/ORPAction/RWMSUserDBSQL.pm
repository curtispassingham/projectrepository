############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RWMSUserDBSQL;
our @ISA = qw(ORPAction::DBSQL);

#########################
# Sub:  PatchAction
# Desc: Nothing
#########################
sub PatchAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=@_;
	
	return 1;
}

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=@_;
	
	#Compile Invalid Objects
	if ($self->CompileInvalids()!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	my @file_type_info=(
	#Nothing can be run here, as we do not have create table privilege
	);
	
	my $self=$class->SUPER::new('DBSQL_RWMSUSER',\@file_type_info,$config_ref);
	$self->SetCheckLockedObjects(0);
	$self->SetUniqueDBManifest(0);
	
    bless($self,$class);

	return $self;
}

1;
