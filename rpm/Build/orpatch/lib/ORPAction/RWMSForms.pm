############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RWMSForms;
our @ISA = qw(ORPAction::OraForms);

#########################
# Sub:  CopyWMSRefForms
# Desc: A custom processing function for WMS reference forms, which are not compiled
#########################
sub CopyWMSRefForms {
	my $self=shift;
	my ($src_dir,$bin_dir,$filtered_dest_info,$orig_f_dest_info,$patch_name,$dest_manifest,$restart_state,$subpatch_ref,$detail_log_subcat)=@_;

	#If we have not yet created a backup directory
	if ($self->GetBackupDirNeededFlag()==1) {
		#Create one if any of our files were modified by this patch
		if (scalar(@$filtered_dest_info)!=0) {
			if ($self->CreateBackupDir($bin_dir)!=1) {
				return 0;
			}
		}

		#Future types going to this directory do not need to create a directory
		$self->SetBackupDirNeededFlag(0);
	}
	
	#If any file was modified, copy all files
	if (scalar(@$filtered_dest_info)!=0) {
		$filtered_dest_info=$orig_f_dest_info;
	}
	
	#Copy files from src to bin
	if ($self->CopyFormsFiles($filtered_dest_info,$src_dir,$bin_dir,1)!=1) {
		return 0;
	}
	
	#The WMS reference forms are not compiled, so copying them is sufficient
	
	return 1;
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
    my $class=shift;
	my ($config_ref)=@_;
	
	my $form_name='rwms_frm_forms';
	my $ref_form_name='rwms_frm_reference';
	
	my $sys_reports='rwms_report_sys_repos';
	my $reg_reports='rwms_report_repos';
	my $both_reports='rwms_report_both';
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, don't compile rwms reference forms
	#
	#[file_type,when_run,process_function,std_run_catch_errors,keep_plx]
	#   file_type_ref is a reference to an array of file types to process
	#   when_run is 0 - never run, 1 - run if updated, 2 - always run
	#   process_function is '' to use the standard OraForms logic for compiling files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	#   keep_plx is 1 if we should keep plx's after compilation, 0 if not
	my @file_type_info=(
	[$ref_form_name,	3,'CopyWMSRefForms',0,0],
	[$form_name,		2,'',0,1],
	[$sys_reports,		0,'',0,0],
	[$reg_reports,		0,'',0,0],
	[$both_reports,		0,'',0,0],
	);
	
	my @file_copy_params=(new FileCopyParams([$ref_form_name,$form_name],'forms/src',0));
	my $reg_fcp=new FileCopyParams([$reg_reports,$both_reports],'reports/RWMS/RWMS REPOSITORY',-3);
	push(@file_copy_params,$reg_fcp);
	my $sys_fcp=new FileCopyParams([$sys_reports,$both_reports],'reports/RWMS/RWMS SYSTEM REPOSITORY',-3);
	push(@file_copy_params,$sys_fcp);
	
	my $self=$class->SUPER::new('ORAFORMS_RWMS',\@file_copy_params,\@file_type_info,$config_ref);
	
    return $self;
}

1;
