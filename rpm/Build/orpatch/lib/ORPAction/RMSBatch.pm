############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RMSBatch;
our @ISA = qw(ORPAction);

use File::Copy;

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;
	
	my $name=$self->GetActionName();
	&::LogMsg("Pre-patch check for $name");

	my $bp=$self->{'BATCH_PROFILE'};
	if ($bp ne '' && ! -f $bp) {
		&::LogMsg("WARNING: specified batch profile $bp does not exist, ignoring");
	}
	
	#Make sure our compile environment will have a C compiler and make
	foreach my $exe ('cc','make','ar') {
		if (&::CheckRequiredBinary($exe,$bp)!=1) {
			return 0;
		}
	}
	
	#If RFM is enabled, we need RFM_TAX_PROVIDER set to a valid value
	if ($self->{'RFMBATCH_MODE'}==1) {
		# Check RFM_TAX_PROVIDER is set to TAXWEB or SYNCHRO, when RFM is enabled.	
		if ($self->{'TAX_PROVIDER'} eq 'INVALID') {
			&::LogMsg("Invalid setting for RFM_TAX_PROVIDER.");		
			return 0;
		}
	}
	
	my $global_state=$self->GetORPatchGlobalState();
	
	#Register a new dependent prepatchcheck to run
	$global_state->AddExtraPrePatchCheck('DBSQL_RMS');

	return 1;
}
	
###############
# Sub:  PostAction
# Desc: Compile RMS Batch - Note that we do this as a PostAction rather than a PatchAction
#       So that the database actions can use their postaction ahead of us to do things like
#       compile objects and create synonyms
###############
sub PostAction {
	my $self=shift;
	
	#Get the configuration for our patch
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	my $dest_dir=$global_state->GetDestDir();
	my $restart_state=$global_state->GetRestartState();
	my $patch_name=$patch_info->GetPatchName();
	my @patch_list=$patch_info->GetPatchList();
	
	my $os=$self->SetupCompileEnvironment();
	
	#Our action info is either blank or proc
	my $restart_action_info=$restart_state->GetActionInfo();
	
	my $lib_src=&::ConcatPathString($dest_dir,'oracle/lib/src');
	my $lib_bin=&::ConcatPathString($dest_dir,'oracle/lib/bin');
	my $proc_src=&::ConcatPathString($dest_dir,'oracle/proc/src');
	my $proc_bin=&::ConcatPathString($dest_dir,'oracle/proc/bin');
	
	my $orig_umask=umask(0022);
	
	my $lib_exit_status=1;
	
	if ($restart_action_info ne 'proc') {
		$lib_exit_status=$self->CompileLibraries($lib_src,$lib_bin,$os);
		
		if ($lib_exit_status!=1) {
			&::LogMsg("Errors while compiling libraries, attempting proc compile");
		} else {
			$restart_state->SetAndSaveActionInfo('proc');
		}
	}

	my $proc_exit_status=$self->CompileProc($proc_src,$proc_bin,$os);
	
	if ($proc_exit_status!=1) {
		&::LogMsg("Errors while compiling Proc*C batch");
	}
	
	umask($orig_umask);
	
	#We only return success if both lib and proc successfully compiled
	if ($lib_exit_status==1 && $proc_exit_status==1) {
		return 1;
	} else {
		return 0;
	}
}

###############
# Sub:  CompileLibraries
# Desc: Compile RMS Batch Libraries
# Args: path to lib/src, path to lib/bin, output from uname
###############
sub CompileLibraries {
	my $self=shift;
	my ($lib_src,$lib_bin,$os)=@_;
	
	&::LogMsg("Compiling batch libraries in $lib_src");
	
	if ($self->CreateBackupDir($lib_bin)!=1) {
		return 0;
	}
	
	my $detail_subcat='lib';
	&DetailLog::CleanDetailLogDir('rmsbatch',$detail_subcat);

	my $platform_mk=&::ConcatPathString($lib_src,'platform.mk');
	if (!-f $platform_mk) {
		my $os_platform_mk='';
		if ($os eq 'AIX') {
			$os_platform_mk='platform_aix_64bit.mk';
		} elsif ($os eq 'HP-UX') {
			$os_platform_mk='platform_hpux_64bit.mk';
		} elsif ($os eq 'SunOS') {
			$os_platform_mk='platform_sunsol_64bit.mk';
		} elsif ($os eq 'Linux') {
			$os_platform_mk='platform_linux_oel_x86_64.mk';
		}
		if ($os_platform_mk eq '') {
			&::LogMsg("Unable to determine correct OS for platform.mk");
			return 0;
		}
		my $src_platform_mk=&::ConcatPathString($lib_src,$os_platform_mk);
		&::LogMsg("Symlinking $os_platform_mk to platform_mk");
		symlink($src_platform_mk,$platform_mk);
	}
	
	&::LogMsg("Running oramake");
	chmod 0755,"$lib_src/oramake";
	
	my $cmd="cd $lib_src ; $lib_src/oramake 2>&1";
	my $source_profile=$self->{'BATCH_PROFILE'};	
	if ($source_profile ne '' && -f $source_profile) {
		$cmd=". $source_profile ; $cmd";
	}
	
	my @output=`$cmd`;
	
	if ($?!=0) {
		&::LogMsg("oramake returned non-zero status");
		&::DebugOutput('oramake',@output);
		return 0;
	}
	
	my $dash_r='';
	if ($os eq 'Linux') {
		$dash_r='-r';
	}
	
	my $ret=1;
	
	if ($self->RunCompileCommand($lib_src,"make -f retek.mk $dash_r clobber 2>&1",'libclobber.log',$detail_subcat)!=1) {
		$ret=0;
	}
	if ($self->RunCompileCommand($lib_src,"{ make -f retek.mk $dash_r depend || make -f retek.mk $dash_r depend ; } 2>&1",'libdpnd.log',$detail_subcat)!=1) {
		$ret=0;
	}
	
	if ($self->RunCompileCommand($lib_src,"make -f retek.mk $dash_r retek rms resa 2>&1",'libretek.log',$detail_subcat)!=1) {
		$ret=0;
	}
	
	if ($self->RunCompileCommand($lib_src,"make -f retek.mk install 2>&1",'libinstall.log',$detail_subcat)!=1) {
		$ret=0;
	}
	
	## RFM Batch - Lib Compilation
	if ($self->{'RFMBATCH_MODE'}==1) {
	     &::LogMsg("Compiling ORFM batch libraries in $lib_src");
		
		if ($self->RunCompileCommand($lib_src,"{ make -f l10n_rmslib.mk $dash_r depend || make -f l10n_rmslib.mk $dash_r depend ; } 2>&1",'libdep_rfm.log',$detail_subcat)!=1) {
			$ret=0;
		}

		if ($self->RunCompileCommand($lib_src,"make -f l10n_rmslib.mk $dash_r 2>&1",'libcomp_rfm.log',$detail_subcat)!=1) {
			$ret=0;
		}
		
		if ($self->RunCompileCommand($lib_src,"make -f l10n_rmslib.mk install 2>&1",'libinstall_rfm.log',$detail_subcat)!=1) {
			$ret=0;
		}
		
	}
	
	if ($self->CopyCompiledOutput($lib_src,$lib_bin)!=1) {
		$ret=0;
	}

	if ($ret!=1) {
		if ($self->{'IGNORE_FAILED_LIB'}==1) {
			&::LogMsg("Ignoring failed compilation based on configuration");
			$ret=1;
		}
	}
	
	return $ret;
}

###############
# Sub:  CompileProc
# Desc: Compile RMS Batch Proc Src files
# Args: path to proc/src, path to proc/bin, output from uname
###############
sub CompileProc {
	my $self=shift;
	my ($proc_src,$proc_bin,$os)=@_;
	
	&::LogMsg("Compiling Pro*C batch in $proc_src");
	
	if ($self->CreateBackupDir($proc_bin)!=1) {
		return 0;
	}
	
	my $detail_subcat='proc';
	&DetailLog::CleanDetailLogDir('rmsbatch',$detail_subcat);
	
	my $dash_r='';
	my $dash_j='';
	if ($os eq 'Linux') {
		$dash_r='-r';
		$dash_j='-j 5';
	}
	
	my $ret=1;
	
	if ($self->RunCompileCommand($proc_src,"make -f mts.mk $dash_r clobber 2>&1",'srcclobber.log',$detail_subcat)!=1) {
		$ret=0;
	}
	if ($self->RunCompileCommand($proc_src,"{ make -f mts.mk $dash_r depend || make -f mts.mk $dash_r depend ; } 2>&1",'srcdpnd.log',$detail_subcat)!=1) {
		$ret=0;
	}
	if ($self->RunCompileCommand($proc_src,"make -f mts.mk $dash_r PRODUCT_PROCFLAGS=dynamic=ansi ditinsrt 2>&1",'srcditinsrt.log',$detail_subcat)!=1) {
		$ret=0;
	}
	if ($self->RunCompileCommand($proc_src,"make $dash_j -f mts.mk $dash_r rms-ALL recs-ALL resa-ALL rtm-ALL fif-ALL 2>&1",'srcall.log',$detail_subcat)!=1) {
		$ret=0;
	}
	if ($self->RunCompileCommand($proc_src,"make -f mts.mk install 2>&1",'srcinstall.log',$detail_subcat)!=1) {
		$ret=0;
	}
	
	## RFM Batch - proc Compilation
	if ($self->{'RFMBATCH_MODE'}==1) {
	     &::LogMsg("Compiling ORFM Pro*C batch in $proc_src");
		
		if ($self->RunCompileCommand($proc_src,"{ make -f l10n_br_rms.mk $dash_r depend || make -f l10n_br_rms.mk $dash_r depend ; } 2>&1",'procdeps_rfm.log',$detail_subcat)!=1) {
			$ret=0;
		}

		if ($self->RunCompileCommand($proc_src,"make -f l10n_br_rms.mk $dash_r 2>&1",'proccomp_rfm.log',$detail_subcat)!=1) {
			$ret=0;
		}
		
		if ($self->RunCompileCommand($proc_src,"make -f l10n_br_rms.mk install 2>&1",'procinstall_rfm.log',$detail_subcat)!=1) {
			$ret=0;
		}
		
	}
	
	if ($self->CopyCompiledOutput($proc_src,$proc_bin)!=1) {
		$ret=0;
	}

	if ($ret!=1) {
		if ($self->{'IGNORE_FAILED_PROC'}==1) {
			&::LogMsg("Ignoring failed compilation based on configuration");
			$ret=1;
		}
	}
	
	return $ret;
}

###############
# Sub:  RunCompileCommand
# Desc: Run a specific compile command in the right src dir with a tee to a log file
# Args: path to src dir, command to execute, logfile to tee into
###############
sub RunCompileCommand {
	my $self=shift;
	my ($src_dir,$cmd_string,$log_file,$detail_subcat)=@_;

	&::LogMsg("Executing $cmd_string");
	
	my $full_cmd_string="cd $src_dir ; $cmd_string";
	
	my $source_profile=$self->{'BATCH_PROFILE'};	
	if ($source_profile ne '' && -f $source_profile) {
		$full_cmd_string=". $source_profile ; $full_cmd_string";
	}
	
	my @output=`$full_cmd_string`;
	my $exit_status=$?;
	
	#Write out output to the logs in src directory that RMS admins would expect
	my $full_log_name=&::ConcatPathString($src_dir,$log_file);
	if (open(COMPILE_LOG,">$full_log_name")) {
		foreach my $line (@output) {
			chomp($line);
			print COMPILE_LOG "$line\n";
		}
		close(COMPILE_LOG);
	} else {
		&::LogMsg("Unable to open $full_log_name for writing: $!");
	}
	
	#Consider this command to have failed if there were any error or csf- strings in the output
	my @error_lines=grep(/^error|^csf-|Command failed for target|Error code 127|Fatal error|error code from the last command|errors detected|Error exit code|fatal:|Error \d/i,@output);
	my $num_errors=scalar(@error_lines);

	my $failed=0;
	if ($exit_status!=0 || $num_errors>0) {
		$failed=1;
	}
	
	#Write the detail log that orpatch users will expect
	my ($ldir,$lfile,$lext)=&::GetFileParts($log_file,3);
	&DetailLog::CreateLog('rmsbatch',$detail_subcat,$lfile,\@output,($failed==1) ? 'err' : 'log');
	
	if ($failed==1) {
		&::DebugOutput("Pro*C Compilation Errors",@error_lines);
		&::LogMsg("ERROR: $num_errors errors while compiling, see $src_dir/$log_file for full details");
		return 0;
	} else {
		&::LogMsg("Command succeeded");
	}
	return 1;
}

###############
# Sub:  CopyCompiledOutput
# Desc: copy the compiled results of our compiled commands to bin
#       this is required because make install doesn't copy ksh and sql scripts to bin
# Args: path to src dir, path to bin dir
###############
sub CopyCompiledOutput {
	my $self=shift;
	my ($src_dir,$bin_dir)=@_;
	
	unless(opendir(DIR,"$src_dir")) {
		&::LogMsg("Unable to open directory $src_dir: $!");
		return 0;
	}
	
	my $ret=1;
	while(my $this_file=readdir(DIR)) {
		next if ($this_file eq '.' || $this_file eq '..');
		
		next if ($this_file=~/env_manifest.csv/);
		next if ($this_file=~/\.(c|d|doc|h|lis|log|mk|o|pc)$/);
		next if ($this_file=~/^(oramake|resa2dw|resa2sim)$/);
		next if ($this_file=~/patch\.ksh/);
		next if ($this_file=~/sacodes\.sql|saerrcodes\.sql/);
		
		my $src_file=&::ConcatPathString($src_dir,$this_file);
		my $dest_file=&::ConcatPathString($bin_dir,$this_file);
		
		if (&File::Copy::copy($src_file,$dest_file)!=1) {
			&::LogMsg("Failed to copy $src_file to $dest_file: $!");
			$ret=0;
		}
		
		#Make sure shell scripts end up executable
		if ($this_file=~/\.ksh|\.sh/) {
			chmod 0755,$dest_file;
		}
	}
	closedir(DIR);

	return $ret;
}

sub SetupCompileEnvironment {
	my $self=shift;
	
	my $user_string=$self->{'CONNECT_STRING'};
	$ENV{TNS_ADMIN}=$self->{'TNS_ADMIN'};
	&::SetOracleHome($self->{'ORACLE_HOME'});
	$ENV{UP}=$user_string;
	$ENV{MMHOME}=$ENV{RETAIL_HOME};
	
	my $os=`uname`;
	chomp($os);
	
	return $os;
}

sub CreateBackupDir {
	my $self=shift;
	my ($bin_dir)=@_;
	
	my $bkp_dir="${bin_dir}-".&::GetTimeStamp('DATE-TIME');
	
	if (-l $bin_dir) {
		#If the bin_dir is a symlink
		&::LogMsg("Unlinking $bin_dir");
		unlink($bin_dir);
	} elsif (-d $bin_dir) {
		&::LogMsg("Moving $bin_dir to $bkp_dir");
		if (&File::Copy::move($bin_dir,$bkp_dir)!=1) {
			&::LogMsg("Unable to rename $bin_dir to $bkp_dir: $!");
			return 0;
		}
	}
	
	if (&::CreateDirs('0755',$bin_dir)!=1) {
		return 0;
	}
	
	return 1;
}	

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
    my $class=shift;
	my ($config_ref)=@_;

	my $action_name='RMSBATCH';
	my $rms_lib_name='rms_pc_batch_lib';
	my $rms_proc_name='rms_pc_batch_proc';
	my $rfm_lib_name='rfm_pc_batch_lib';
	my $rfm_proc_name='rfm_pc_batch_proc';
	my $rfm_tw_proc_name='rfm_tw_pc_batch_proc';
	my $rfm_sy_proc_name='rfm_sy_pc_batch_proc';	
		
	my @file_copy_params=();
	my @libtypes=($rms_lib_name);
	my @proctypes=($rms_proc_name);
	
	# If the RFM is enabled in the Config ,copy and compile RFM Batch.
	my $rfmbatchmode=$config_ref->ValidateTFVar("${action_name}_INCLUDE_RFM",0);
	
	if ($rfmbatchmode==1) {
		my	$tax_provider=$config_ref->ValidateStringVar('RFM_TAX_PROVIDER','INVALID','SYNCHRO','TAXWEB');
        push(@libtypes,$rfm_lib_name); 

		if ($tax_provider eq 'TAXWEB') {
	        push(@proctypes,$rfm_proc_name,$rfm_tw_proc_name); 
	  	}	
	  	elsif ($tax_provider eq 'SYNCHRO') {
	        push(@proctypes,$rfm_proc_name,$rfm_sy_proc_name); 
		}			
	}
	
	push(@file_copy_params,new FileCopyParams(\@libtypes,'oracle/lib/src',0));
	push(@file_copy_params,new FileCopyParams(\@proctypes,'oracle/proc/src',0));
	push(@file_copy_params,new FileCopyParams(['rms_s9t_templates'],'oracle/templates',0));
	
	my $self=$class->SUPER::new('RMSBATCH',\@file_copy_params,$config_ref);
	
	#We get our batch profile before the RMS DB connection info to ensure name is correct
	my $bname=$self->GetActionName();
	my $bp=$config_ref->Get("${bname}_PROFILE");
	if ($bp ne '') {
		$self->{'BATCH_PROFILE'}=$self->ResolveConfigPath($bp);
	}
	
	#We need the  RFM BATCH Mode to enable the RFM batches during the compilation.
	$self->{'RFMBATCH_MODE'}=$config_ref->ValidateTFVar("${action_name}_INCLUDE_RFM",0);
	$self->{'TAX_PROVIDER'}=$config_ref->ValidateStringVar('RFM_TAX_PROVIDER','INVALID','SYNCHRO','TAXWEB');
	
	#We need the RMS database's connection information to be able to setup UP
	my $name='DBSQL_RMS';
	$self->{'CONNECT_STRING'}=$config_ref->Get("${name}_CONNECT");
	$self->{'TNS_ADMIN'}=$self->ResolveConfigPath($config_ref->Get("${name}_WALLET"));
	$self->{'ORACLE_HOME'}=$config_ref->Get("DB_ORACLE_HOME");
	
	$self->SetTFFlagFromConfigVar($config_ref,'IGNORE_FAILED_PROC','RMSBATCH',0);
	$self->SetTFFlagFromConfigVar($config_ref,'IGNORE_FAILED_LIB','RMSBATCH',0);
	
    return $self;
}

1;
