############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RDEBatchDBSQL;
our @ISA = qw(ORPAction::DBSQL);

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
    my $self=shift;
    my ($full_check)=@_;

    #Run the default DBSQL check
    if ($self->SUPER::PrePatchCheck($full_check)!=1) {
        return 0;
    }

    #If this is a full check, register our need for a working DBSQL_RDEDM configuration
    if ($full_check==1) {
        my $global_state=$self->GetORPatchGlobalState();

        #Register a new dependent prepatchcheck to run
        $global_state->AddExtraPrePatchCheck('DBSQL_RDEDM');
    }


    return 1;
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
    my $class=shift;
    my ($config_ref)=@_;


    #This is a more detailed data structure then what is sent to the ORPAction constructor
    #It allows us to keep track of how to handle files in a particular file type category
    #For example, always run all files or never run files (because they are run some other way),etc
    #
    #[file_type,when_run,process_function,std_run_catch_errors,patch_product,run_phase]
    #   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
    #   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
    #   std_run_catch_errors is 1 if we should catch errors, 0 if not
    #   patch_product
    #   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
    my @file_type_info=(
    ['rde_batch_db_ddl',                  3,'',                       1,'RDE','PATCHACTION'],
    ['rde_batch_db_change_scripts',       1,'',                       1,'RDE','PATCHACTION'],
    ['rde_batch_db_control_scripts',      2,'',                       1,'RDE','PATCHACTION'],
    );

    my $self=$class->SUPER::new('DBSQL_RDEBATCH',\@file_type_info,$config_ref);


    return $self;
}

1;
