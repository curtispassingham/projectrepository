############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
# This is the base Action class for actions which need to run SQL scripts into a database
# It contains helper functions related to reading sql files, running them, etc

use strict;
package ORPAction::DBSQL;
our @ISA = qw(ORPAction);

use Cwd;

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;
	
	my $name=$self->GetActionName();
	&::LogMsg("Pre-patch check for $name");
	&::LogMsg("Validating database configuration");
	
	#Try our best to make sure the Oracle environment looks OK
	my $user_string=$self->{'CONNECT_STRING'};
	my $wallet_dir=$self->{'TNS_ADMIN'};
	my $oh=$self->{'ORACLE_HOME'};
	
	if ($user_string eq '') {
		&::LogMsg("ERROR: Missing connection string for $name");
		&::LogMsg("Should be defined in config file as ${name}_CONNECT");
		return 0;
	}
	
	if (!-d $wallet_dir) {
		&::LogMsg("ERROR: Wallet directory $wallet_dir does not exist!");
		&::LogMsg("Should be defined in config file as ${name}_WALLET");
		return 0;
	}
	
	if (!-d $oh) {
		&::LogMsg("ERROR: ORACLE_HOME $oh does not exist!");
		&::LogMsg("Should be defined in config file as DB_ORACLE_HOME");
		return 0;
	}

	#Check our SQL connection
	if ($self->CheckDBStatus()!=1) {
		&::LogMsg("Unable to connect to DB, aborting");
		return 0;
	}
	
	#Store our schema name in case someone needs it later
	if ($self->SaveSchemaName()!=1) {
		&::LogMsg("Unable to determine schema name, aborting");
		return 0;
	}
	
	#If we are only doing a configuration check, we don't have to worry about locked objects and tablespaces
	if ($full_check==1) {
		#Check for locked objects
		if ($self->GetCheckedLockedObjects==1) {
			if ($self->CheckLockedObjects()!=1) {
				&::LogMsg("Aborting, please ensure no sessions are connected to $user_string schema");
				return 0;
			}
		}
		
		#Check for required tablespaces
		if ($self->CheckRequiredTablespaces()!=1) {
			&::LogMsg("Unable to proceed without all required tablespaces");
			return 0;
		}
	}
	
	return 1;
}

########################
# Sub:  PatchAction
# Desc: Run our SQL files based on file_type_info
# Ret:  1 if all files ran successfully, 0 if not
########################
sub PatchAction {
	my $self=shift;

	return $self->RunSQLFilesForPhase('PATCHACTION');
}

########################
# Sub:  PreAction
# Desc: Run our SQL files based on file_type_info
# Ret:  1 if all files ran successfully, 0 if not
########################
sub PreAction {
	my $self=shift;

	return $self->RunSQLFilesForPhase('PREACTION');
}

########################
# Sub:  PostAction
# Desc: Run our SQL files based on file_type_info
# Ret:  1 if all files ran successfully, 0 if not
########################
sub PostAction {
	my $self=shift;

	return $self->RunSQLFilesForPhase('POSTACTION');
}

########################
# Sub:  CleanupAction
# Desc: Run our SQL files based on file_type_info
# Ret:  1 if all files ran successfully, 0 if not
########################
sub CleanupAction {
	my $self=shift;
	
	return $self->RunSQLFilesForPhase('CLEANUPACTION');
}

#########################
# Sub:  PreFileDelete
# Desc: This is called just prior to a file that we are responsible for being deleted
# Args: delete_file - The full path to the file that is going to be deleted
#       dm_info_ref - The destination manifest row related to this file
# Ret:  1 on success, 0 on failure
# Note: returning 0 stops removing the file, but it will already have been removed from the dest manifest
#########################
sub PreFileDelete {
	my $self=shift;
	my ($delete_file,$dm_info_ref)=@_;
	
	return 1 if ($dm_info_ref eq '');
	
	#Get the delete function associated with this file type
	my ($dfile,$sfile,$ftype,@discard)=&Manifest::DecodeDestLine($dm_info_ref);
	my ($when_run,$process_func,$std_catch_errors,$patch_product,$run_phase,$del_func)=$self->GetFileTypeInfo($ftype);
	
	return 1 if ($del_func eq '');
	
	#A delete function was registered, call it
	return $self->$del_func($delete_file,$dm_info_ref);
}

########################
# Sub:  RunSQLFilesForPhase
# Desc: Run all SQL files for a specific processing phase
# Ret:  1 if all files ran successfully, 0 if not
########################
sub RunSQLFilesForPhase {
	my $self=shift;
	my ($phase_name)=@_;
	
	#Most phases besides PatchAction do not have files to process, so avoid all the work below if there are no file types for this phase
	my @phase_types=$self->_GetFileTypeInfoForPhase($phase_name);
	if (scalar(@phase_types)==0) {
		#&::LogMsg("No SQL files to run for $phase_name");
		return 1;
	}
	
	if ($self->CreateDBManifestTable()==0) {
		&::LogMsg("Failed to create manifest table!");
		return 0;
	}
	
	my $fcp_ref=$self->{'FILE_COPY_PARAMS'};
	my $patch_mode=$self->{'PATCH_MODE'};
	
	#Get the configuration for our patch
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	my $dest_dir=$global_state->GetDestDir();
	my $restart_state=$global_state->GetRestartState();
	my $patch_name=$patch_info->GetPatchName();
	my @patch_list=$patch_info->GetPatchList();
	
	#Our action info is in the form dest_subdir:file_type
	my $restart_action_info=$restart_state->GetActionInfo();
	my ($restart_dest_subdir,$restart_file_type)=$self->DecodeRestartPoint($restart_action_info);
	
	my $fcp_index=0;
	if ($restart_dest_subdir ne '') {
		#Skip ahead to the appropriate file copy param
		&::LogMsg("Restarting with subdirectory: $restart_dest_subdir");
		$fcp_index=$self->FindFileCopyParamsIndex($restart_dest_subdir,$fcp_ref);
	}
	
	my $fresh_install=$self->IsFreshInstall();
	if ($fresh_install==-1) {
		&::LogMsg("Unable to determine if this is a fresh install.");
		return 0;
	}

	#Loop through each file destination
	for (;$fcp_index<scalar(@$fcp_ref);$fcp_index++) {
		my $ref=$$fcp_ref[$fcp_index];
		my ($ft_ref,$dest_subdir)=$ref->GetBasicParams();
		
		my ($full_dest,$dest_manifest)=$self->GetDestDirAndManifest($dest_dir,$dest_subdir);
		
		#Read the destination manifest
		my @dest_info=&Manifest::ReadManifest($dest_manifest);
		
		my $ft_index=0;
		if ($restart_file_type ne '') {
			#Skip ahead to the appropriate file type
			&::LogMsg("Restarting with file type $restart_file_type");
			$ft_index=$self->FindFTIndex($restart_file_type,$ft_ref);
		}
		
		#Loop through file types
		for(;$ft_index<scalar(@$ft_ref);$ft_index++) {
			my $type=$$ft_ref[$ft_index];
			my ($when_run,$process_func,$std_catch_errors,$patch_product,$run_phase,$del_func)=$self->GetFileTypeInfo($type);
			
			if ($run_phase ne $phase_name) {
				next;
			}
			
			&::LogMsg("Processing DBSQL file type $type");
	
			if ($when_run==0) {
				#File type we never run, go to the next type
				&::LogMsg("Skipping file type $type as run_type is 0");
				next;
			}
			
			#When_run 4 is handled exactly like run type 1 when not a fresh install
			if ($when_run==4 && $fresh_install!=1) {
				$when_run=1;
			}
			#When_run 5 is handled exactly like run type 1 when this is a fresh install
			if ($when_run==5 && $fresh_install==1) {
				$when_run=1;
			}
			
			if ($patch_product ne '' && !$patch_info->IsProductInPatch($patch_product)) {
				#a type applies only to a particular product, and it wasn't included in the patch
				&::LogMsg("Skipping file type $type as this is not a $patch_product patch");
				next;
			}
			
			$self->MarkRestartPoint($restart_state,$dest_subdir,$type);
	
			#Filter dest manifest by type
			my $all_f_dest_info=&Manifest::FilterDestManifest(\@dest_info,$type,'');
			
			#Filter out any files in env_manifest.csv that don't actually exist in the environment
			my $orig_f_dest_info=&Manifest::FilterDestManifestNonExistentFiles($full_dest,$all_f_dest_info);
			
			#Ensure further filtering is done on a copy so custom actions can see what the 'pre-filtered' list looked like
			my $filtered_dest_info=$orig_f_dest_info;
			
			#Filter files by whether they were updated with this patch
			#If this is a file type where we only load updated files (i.e. when_run 3)
			#Or we would normally run all files but we are applying a hotfix and only loading updated files
			#Note: We use when_run==6 to ignore this and always run all of a type, even during a hotfix
			if ($when_run==3 ||
				($patch_mode eq 'HOTFIX' && ($when_run==2 || $when_run==1 || $when_run==4 || $when_run==5))) {
				$filtered_dest_info=&Manifest::FilterDestManifest($filtered_dest_info,'',$patch_name,\@patch_list);
			}
			
			#Filter files by whether they have already been run
			#if this is a file type where we care about that (DBCs and ddl scripts primarily)
			if ($when_run==1 || $when_run==3 || $when_run==4 || $when_run==5) {
				$filtered_dest_info=$self->FilterFilesByDBManifest($filtered_dest_info,$type);
			}
			
			my $std_function='RunDestFileList';
			if ($when_run==4 || $when_run==5) {
				$std_function='UpdateDBMWithDestFileList';
				$process_func='';
			}
			
			if ($process_func eq '') {
				#Run the files in order using the standard function
				if ($self->$std_function($full_dest,$filtered_dest_info,$std_catch_errors)!=1) {
					#Problems running a script, abort
					return 0;
				}
			} else {
				#A non-standard function
				if ($self->$process_func($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,\@patch_list)!=1) {
					#Problems running a script, abort
					return 0;
				}
			}
		}
	}
	
	return 1;
}

#########################
# Sub:  CallCustomHook
# Desc: DBSQL extends the default CallCustomHook to run SQL files
# Arg:  custom script to execute
# Ret:  1 if the script exited with 0 status, 0 if not
#########################
sub CallCustomHook {
	my $self=shift;
	my ($custom_script,$hook_name)=@_;
	
	my ($dir,$file,$ext)=&::GetFileParts($custom_script,3);
	
	#If this does not look like a sql file
	unless ($ext=~/^sql$/i) {
		#Call the default custom hook handler
		return $self->SUPER::CallCustomHook($custom_script,$hook_name);
	}
	
	#We were handed what looks like a SQL script
	
	#Verify it exists
	unless (-f $custom_script) {
		&::LogMsg("Custom hook file $custom_script does not exist!");
		return 0;
	}
	
	#Run it, without recording the details in the dbmanifest table
	my $user_string=$self->{'CONNECT_STRING'};
	
	&::LogMsg("Executing $custom_script against $user_string");
	
	#We run the script directly with CallExecSQL so we can DetailLog the output
	my @contents=("set feedback on","\@$custom_script");
	my ($output_ref,$error_ref)=$self->CallExecSQL(join("\n",@contents,"select 'COMPLETE - $file' from dual"));
	
	my $rc=1;
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("File $custom_script failed");
		&::DebugOutput("$custom_script ERRORS",@$error_ref);
		&::DebugOutput("$custom_script LOG",@$output_ref);
		$rc=0;
	} else {
		&::LogMsg("File $custom_script successfully executed");
		$rc=1;
	}
	
	&DetailLog::CreateLog('hooks',$hook_name,$custom_script,$output_ref,($rc!=1) ? 'err' : 'log');
	
	return $rc;
}

########################
# Sub:  RunSQLFile
# Desc: Run the contents of a SQL file and record it in the dbmanifest
# Ret:  1 if the file was successful, 0 if not
########################
sub RunSQLFile {
	my $self=shift;
	my ($file,$manifest_file_name,$revision,$file_type,$patch_name,$error_filter_ref,$upd_dbmanifest,$init_sql_cmd)=@_;
	$upd_dbmanifest=1 if ($upd_dbmanifest eq '');
	$manifest_file_name=$file if $manifest_file_name eq '';
	
	my @contents=("set feedback on");
	
	if ($init_sql_cmd ne '') {
		push(@contents,$init_sql_cmd);
	}
	
	#Note that RunSQLFileWithArgs depends on us passing the $file string directly through to this \@$file
	push(@contents,"\@$file");

	my $sql_ret=$self->DoSQL($manifest_file_name,join("\n",@contents,"select 'COMPLETE - $file' from dual"),$error_filter_ref);
	
	if ($upd_dbmanifest==1 && $sql_ret==1) {
		my $mfst_ret=$self->UpdateDBManifestTable($manifest_file_name,$revision,$file_type,$patch_name);
	}
	
	return $sql_ret;
}

########################
# Sub:  RunSQLFileWithArgs
# Desc: Run the contents of a SQL file and record it in the dbmanifest
#       This version allows arguments to be passed to the script
# Ret:  1 if the file was successful, 0 if not
########################
sub RunSQLFileWithArgs {
	my $self=shift;
	my ($file,$args,$manifest_file_name,$revision,$file_type,$patch_name,$error_filter_ref,$upd_dbmanifest)=@_;
	
	my $file_w_args="$file $args";
	
	return $self->RunSQLFile($file_w_args,$manifest_file_name,$revision,$file_type,$patch_name,$error_filter_ref,$upd_dbmanifest,'');
}
	
#########################
# Sub:  CreateDBManifestTable
# Desc: Create the table where we track SQL scripts that have been applied
#########################
sub CreateDBManifestTable {
	my $self=shift;
	my $SQL=<<ENDSQL;
DECLARE
	table_count int;
	synonym_count int;
BEGIN
	select count(1) into table_count from user_tables where table_name = 'DBMANIFEST';
	if table_count <> 1 then
		select count(1) into synonym_count from user_synonyms where synonym_name = 'DBMANIFEST';
		if synonym_count = 1 then
			execute immediate 'drop synonym dbmanifest';
		end if;
		execute immediate 'create table dbmanifest (file_name varchar2(1024),version varchar2(12),file_type varchar2(50),patch_name varchar2(200),applied_date date)';
	end if;
END;
/
select 'COMPLETE - create table dbmanifest' from dual
ENDSQL

	my @filter=(qr/already exists|ORA\-0*955/);

	return $self->DoSQL('cr_dbmanifest.sql',$SQL,\@filter);
}

#########################
# Sub:  CheckDBStatus
# Desc: Check if we have enough of an environment to connect to the database
# Ret:  1 if successful, 0 if not
#########################
sub CheckDBStatus {
	my $self=shift;
	
	my $user_string=$self->{'CONNECT_STRING'};
	
	&::LogMsg("Checking database status for $user_string");
	my $sql="select 'UP' from dual";
	my ($output_ref,$error_ref)=$self->CallExecSQL($sql);
	
	my $status=$$output_ref[0];
	chomp($status);
	
	if ($status ne 'UP') {
		&::LogMsg("Failed to connect to database as $user_string");
		&::DebugOutput("select 'UP' from dual",@$output_ref);
		return 0;
	} else {
		&::LogMsg("Database connection check successful");
		return 1;
	}
}
	
#########################
# Sub:  GetCheckLockedObjectsSQL
# Desc: Return the SQL statement that should be used to check for locked objects
#       must return three columns concatenated with '|' - session id, username, locked object count
# Ret:  SQL statement to use when checking locked objects
#########################
sub GetCheckLockedObjectSQL {
	my $self=shift;
	
	my $SQL=<<ENDSQL;
alter session set "_optimizer_cartesian_enabled"=false;
SELECT a.session_id||'|'||a.oracle_username||'|'||count(1)
FROM V\$LOCKED_OBJECT A,user_objects b
WHERE A.OBJECT_ID = B.OBJECT_ID
group by a.session_id,a.oracle_username
ENDSQL

	return $SQL;
}
	
#########################
# Sub:  CheckLockedObjects
# Desc: Check if any objects are locked in the database
# Ret:  1 if successful, 0 if not
#########################
sub CheckLockedObjects {
	my $self=shift;

	my $SQL=$self->GetCheckLockedObjectSQL();

	my $user_string=$self->{'CONNECT_STRING'};
	&::LogMsg("Checking for locked objects in $user_string");
	my ($output_ref,$error_ref)=$self->CallExecSQL($SQL);
	
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Errors encountered while checking locked objects!");
		&::DebugOutput('locked object check',@$output_ref);
		return 0;
	}
	
	my $locks=scalar(@$output_ref);
	if ($locks!=0) {
		#Locked objects found
		&::LogMsg("Found $locks session(s) with locked objects:");
		
		foreach my $line (@$output_ref) {
			chomp($line);
			my ($s,$u,$c)=split(/\|/,$line);
			&::LogMsg("Session $s User $u - $c locked objects");
		}
		return 0;
	}

	return 1;
}

#########################
# Sub:  GetDBManifestForType
# Desc: Get the rows in the dbmanifest table that match a particular file_type
# Args: file_type
# Ret:  ref to an array of [file_name,revision]
#########################
sub GetDBManifestForType {
	my $self=shift;
	my ($file_type)=@_;
	
	my $sql="select file_name||'|'||version from dbmanifest where file_type = '$file_type'";
	
	&::LogMsg("Getting dbmanifest contents for file_type $file_type");
	my ($output_ref,$error_ref)=$self->CallExecSQL($sql);
	
	my @results=();
	foreach my $line (@$output_ref) {
		chomp($line);
		my ($f,$v)=split(/\|/,$line);
		push(@results,[$f,$v]);
	}
	
	return \@results;
}

#########################
# Sub:  FilterFilesByDBManifest
# Desc: Filter a dest info ref by removing anything in the dbmanifest table
# Args: dest_info_ref,file_type
# Ret:  ref to an array of dest info
#########################
sub FilterFilesByDBManifest {
	my $self=shift;
	my ($dest_info,$type)=@_;
	
	#Read dbmanifest table
	my $db_manifest_ref=$self->GetDBManifestForType($type);
	
	my @filtered_dest_info=();
	foreach my $ref (@$dest_info) {
		my $add=1;
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		#loop through the db manifest array looking for matching file names
		foreach my $dbref (@$db_manifest_ref) {
			my ($db_file,$db_version)=@$dbref;
			
			#If we find the file in the db manifest, do not add it to the filtered list
			if ($db_file eq $sfile) {
				$add=0;
				last;
			}
		}
		if ($add==1) {
			push(@filtered_dest_info,$ref);
		}
	}
	
	return \@filtered_dest_info;
}

#########################
# Sub:  RunDestFileList
# Desc: Run a list of files into the database
# Args: full_dest - the directory where the files are based
#       dest_info_ref - ref to an array of destination manifest info to run
#       catch_errors - 1 to abort on any errors, 0 to ignore all errors
# Ret:  1 if successful, 0 if not
#########################
sub RunDestFileList {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$catch_errors)=@_;
	
	my @error_filter=();
	if ($catch_errors==0) {
		@error_filter=(qr/.*/);
	}
	
	return $self->RunDestFileListWithErrorFilter($full_dest,$filtered_dest_info,\@error_filter);
}

#########################
# Sub:  RunDestFileListWithErrorFilter
# Desc: Run a list of files into the database using a specific error filter
# Args: full_dest - the directory where the files are based
#       dest_info_ref - ref to an array of destination manifest info to run
#       error_filter_ref - ref to an array of patterns for excluding errors
# Ret:  1 if successful, 0 if not
#########################
sub RunDestFileListWithErrorFilter {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$error_filter_ref)=@_;

	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		my $full_file=&::ConcatPathString($full_dest,$dfile);

		&::LogMsg("Running $dfile");
		if ($self->RunSQLFile($full_file,$sfile,$frevision,$ftype,$fdesc,$error_filter_ref,1,'')!=1) {
			return 0;
		}
	}
	
	return 1;
}

#########################
# Sub:  RunDestFileListWithSubstitutions
# Desc: Run a list of files into the database substituting strings in the file
# Args: full_dest - the directory where the files are based
#       dest_info_ref - ref to an array of destination manifest info to run
#       error_filter_ref - ref to an array of patterns for excluding errors
#       method_ref - ref to a FileCopyMethodSubst object
# Ret:  1 if successful, 0 if not
#########################
sub RunDestFileListWithSubstitutions {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$error_filter_ref,$method_ref)=@_;

	my $temp_name=&::GetTempName('.sql');
	my ($temp_dir,$discard2)=&::GetFileParts($temp_name,2);
	
	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		my $full_file=&::ConcatPathString($full_dest,$dfile);
		
		my ($discard3,$base_sql_file)=&::GetFileParts($dfile,2);
		
		my $temp_sql_file=&::ConcatPathString($temp_dir,$base_sql_file);
		
		&::LogMsg("Creating substituted version of $dfile");
		my $errors='';
		if ($method_ref->DoCopy($full_file,$temp_sql_file,\$errors)!=1) {
			&::LogMsg("ERROR: Failed to create substituted version of $full_file: $errors");
			return 0;
		}

		&::LogMsg("Running $dfile");
		if ($self->RunSQLFile($temp_sql_file,$sfile,$frevision,$ftype,$fdesc,$error_filter_ref,1,'')!=1) {
			return 0;
		}
		unlink($temp_sql_file);
	}
	
	return 1;
}

#########################
# Sub:  RunDestFileListByBaseName
# Desc: Run a list of files into the database, changing into the directory of each one and calling it by it's basename
# Args: full_dest - the directory where the files are based
#       dest_info_ref - ref to an array of destination manifest info to run
#       error_filter_ref - ref to an array of patterns for excluding errors
# Ret:  1 if successful, 0 if not
#########################
sub RunDestFileListByBaseName {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$error_filter_ref)=@_;
	
	my $ret=1;
	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		my ($file_dir,$base_file)=&::GetFileParts($dfile,2);
		my $full_file_dir=&::ConcatPathString($full_dest,$file_dir);
		
		#Change into the directory
		my $original_cwd=cwd();
		unless (chdir($full_file_dir)) {
			&::LogMsg("Failed to chdir to $full_file_dir: $!");
			return 0;
		}
		
		#Call the script by it's base name
		&::LogMsg("Running $dfile");
		if ($self->RunSQLFile($base_file,$sfile,$frevision,$ftype,$fdesc,$error_filter_ref,1,'')!=1) {
			$ret=0;
		}
		
		chdir($original_cwd);
		
		last if ($ret!=1);
	}
	
	return $ret;
}

#########################
# Sub:  RunDestFileListWithArgs
# Desc: Run a list of files into the database, passing the same argument to each
# Args: full_dest - the directory where the files are based
#       args  - the argument to pass to each script
#       dest_info_ref - ref to an array of destination manifest info to run
#       error_filter_ref - ref to an array of patterns for excluding errors
# Ret:  1 if successful, 0 if not
#########################
sub RunDestFileListWithArgs {
	my $self=shift;
	my ($full_dest,$args,$filtered_dest_info,$error_filter_ref)=@_;
	
	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);

		my $full_file=&::ConcatPathString($full_dest,$dfile);
		
		&::LogMsg("Running $dfile");
		if ($self->RunSQLFileWithArgs($full_file,$args,$sfile,$frevision,$ftype,$fdesc,$error_filter_ref,1)!=1) {
			return 0;
		}
	}
	
	return 1;
}

#########################
# Sub:  RunDestFileListWithInitCmd
# Desc: Run a list of files into the database executing a SQL command before each file (like an alter session command)
# Args: full_dest - the directory where the files are based
#       init_cmd - the initial SQL command to run before the contents of each file
#       dest_info_ref - ref to an array of destination manifest info to run
#       error_filter_ref - ref to an array of patterns for excluding errors
# Ret:  1 if successful, 0 if not
#########################
sub RunDestFileListWithInitCmd {
	my $self=shift;
	my ($full_dest,$init_cmd,$filtered_dest_info,$error_filter_ref)=@_;

	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		my $full_file=&::ConcatPathString($full_dest,$dfile);

		&::LogMsg("Running $dfile");
		if ($self->RunSQLFile($full_file,$sfile,$frevision,$ftype,$fdesc,$error_filter_ref,1,$init_cmd)!=1) {
			return 0;
		}
	}
	
	return 1;
}

#########################
# Sub:  FindAndRunScriptList
# Desc: find specific scripts by short name and run them in order
#########################
sub FindAndRunScriptList {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$orig_f_dest_info,@desired_scripts)=@_;
	
	my ($proceed,$found_scripts_ref,$missing_scripts_ref)=$self->FindFilteredFilesInOrder($filtered_dest_info,$orig_f_dest_info,@desired_scripts);

	unless($proceed==1) {
		return 0;
	}

	if ($self->RunDestFileList($full_dest,$found_scripts_ref,1)!=1) {
		#Problems running a script, abort
		return 0;
	}

	return 1;
}

#########################
# Sub:  UpdateDBMWithDestFileList
# Desc: Update the dbmanifest table with all files in the dest file list 
# Args: full_dest - the directory where the files are based
#       dest_info_ref - ref to an array of destination manifest info to run
#       std_catch_errors - ignored
# Ret:  1 if successful, 0 if not
#########################
sub UpdateDBMWithDestFileList {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$std_catch_errors)=@_;

	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);

		&::LogMsg("Registering $dfile in dbmanifest");
		if ($self->UpdateDBManifestTable($dfile,$frevision,$ftype,$fdesc)!=1) {
			&::LogMsg("DBManifest registration failed, aborting!");
			return 0;
		}
	}

	return 1;
}

#########################
# Sub:  CompileInvalidsInSharedSchema
# Desc: Compile invalid objects in a schema that hosts multiple actions
# Args: parent_name - the common name for all hosted objects
# Ret:  1 on success, 0 on failure
#########################
sub CompileInvalidsInSharedSchema {
	my $self=shift;
	my ($parent_name)=@_;
	
	my $objs_compiled=$self->GetORPatchGlobalState()->GetCrossActionFlag("${parent_name}_COMPILE");
	unless ($objs_compiled==1) {
		if ($self->CompileInvalids()!=1) {
			return 0;
		}
		$self->GetORPatchGlobalState()->SetCrossActionFlag("${parent_name}_COMPILE",1);
	}
	
	return 1;
}

#########################
# Sub:  CompileInvalids
# Desc: compile any invalid objects
#########################
sub CompileInvalids {
	my $self=shift;
	my (@orig_invalid)=@_;

	my $detail_dir=$self->GetActionName();
	$detail_dir=~tr/A-Z/a-z/;
	my $detail_subdir='invalids';
	&DetailLog::CleanDetailLogDir($detail_dir,$detail_subdir);
	
	my @now_invalid=$self->GetInvalids();
	my @prev_invalid=@orig_invalid;
	
	if (scalar(@now_invalid)==0) {
		&::LogMsg("No invalid objects to compile");
		return 1;
	}
	
	my $invalids_ok=0;
	
	my $try=0;
	while($try++<10) {
		&::LogMsg("Invalid Compilation attempt $try");
		if ($self->CompileInvalidsOnce(scalar(@now_invalid))!=1) {
			&::LogMsg("Errors while compiling invalid objects, aborting");
			return 0;
		}
		
		@prev_invalid=@now_invalid;
		@now_invalid=$self->GetInvalids();
		
		if (scalar(@now_invalid)==0) {
			#We're always done if nothing is invalid anymore
			last;
		}
		
		if ($self->InvalidListsMatch(\@prev_invalid,\@now_invalid)==1) {
			#Invalid list stopped changing
			last;
		}
		
		if ($self->InvalidListsMatch(\@orig_invalid,\@now_invalid)==1) {
			#Invalid list matches the list from when we started the migration
			$invalids_ok=1;
			last;
		}
	}

	my $count_exclude_syns=0;
	my $invalid_count=0;
	
	#Count the number of invalids versus the number of excluding invalid synonyms
	foreach my $ref (@now_invalid) {
		$invalid_count++;
		if ($$ref[0] ne 'SYNONYM') {
			$count_exclude_syns++;
		}
	}

	&::LogMsg("$invalid_count invalid objects remain".(($invalid_count==0) ? '' : ':'));
	if ($invalid_count!=0) {
		$self->_LogInvalidObjectDetails(\@now_invalid,\@orig_invalid,$detail_dir,$detail_subdir);
		
		#If we only have invalid synonyms, consider this OK
		if ($count_exclude_syns==0) {
			&::LogMsg("Only synonyms are invalid in this schema, ignoring");
			$invalids_ok=1;
		}
	} else {
		$invalids_ok=1;
	}
	
	if ($invalids_ok!=1) {
		#If we are ignoring invalid objects, say that this was  OK
		if ($self->{'IGNORE_INVALIDS'}==1) {
			&::LogMsg("Ignoring invalid objects based on configuration");
			$invalids_ok=1;
		}
	}
	
	return $invalids_ok;
}

#########################
# Sub:  GetInvalids
# Desc: Get the list of invalid objects
#########################
sub GetInvalids {
	my $self=shift;
	
	my $sql="select object_type||'|'||object_name from user_objects where status <> 'VALID' order by object_type,object_name";
	
	my ($output_ref,$error_ref)=$self->CallExecSQL($sql);
	
	my @results=();
	foreach my $line (@$output_ref) {
		chomp($line);
		my ($f,$v)=split(/\|/,$line);
		push(@results,[$f,$v]);
	}	

	return @results;
}

#########################
# Sub:  CreateUserSynonyms
# Desc: Create private synonyms for all users
# Ret:  1 on success, 0 on failure
#########################
sub CreateUserSynonyms {
	my $self=shift;
	my ($parent_name)=@_;
	
	my $syns_created=$self->GetORPatchGlobalState()->GetCrossActionFlag("${parent_name}_SYNONYMS");
	unless ($syns_created==1) {	
		my $SQL=<<ENDSQL;
\@\@synonym_check.sql
select 'COMPLETE - synonym_check.sql' from dual
ENDSQL

		&::LogMsg("Creating missing synonyms for $parent_name users...");
		if ($self->DoSQL('synonym_check.sql',$SQL,[])!=1) {
			&::LogMsg("Failed to create synonyms, aborting");
			return 0;
		}
		
		$self->GetORPatchGlobalState()->SetCrossActionFlag("${parent_name}_SYNONYMS",1);
	}
	
	return 1;
}

#########################################################################################
# Helper functions

sub InvalidListsMatch {
	my $self=shift;
	my ($list_one_ref,$list_two_ref)=@_;
	
	my $len1=scalar(@$list_one_ref);
	my $len2=scalar(@$list_two_ref);
	
	if ($len1 != $len2) {
		return 0;
	}
	
	for(my $i=0;$i<$len1;$i++) {
		my $ref1=$$list_one_ref[$i];
		my $ref2=$$list_two_ref[$i];
		if ($$ref1[0] ne $$ref2[0] || $$ref1[1] ne $$ref2[1]) {
			return 0;
		}
	}

	return 1;
}

########################
# Sub:  CompileInvalidsOnce
# Desc: Make one pass through compiling invalid objects.  It is better to call CompileInvalids, which will repeatedly
#       recompile until the number of invalids stops changing
# Ret:  1 if successful, 0 if not
########################
sub CompileInvalidsOnce {
	my $self=shift;
	my ($num)=@_;
	
	my $SQL=<<ENDSQL;
\@\@recompile_invalids.sql
select 'COMPLETE - recompile_invalids' from dual
ENDSQL
	
	&::LogMsg("Compiling $num invalid objects...");
	
	return $self->DoSQL('recompile_invalids.sql',$SQL,[],1);
}

########################
# Sub:  FindFilteredFilesInOrder
# Desc: Find a list of files in order when the list may have been filtered by the dbmanifest
# Ret:  proceed - a 1 if it is OK to proceed with the (possibly) smaller list of files, 0 if not
#       a ref to an array of dest_info rows for found scripts
#       a ref to an array files that couldn't be found
########################
sub FindFilteredFilesInOrder {
	my $self=shift;
	my ($filtered_dest_info,$orig_f_dest_info,@required_scripts)=@_;

	my ($found_scripts_ref,$missing_scripts_ref)=$self->FindReqFilesInOrder($filtered_dest_info,@required_scripts);
	
	if (scalar(@$missing_scripts_ref)!=0) {
		#Check if the scripts were in the original unfiltered list
		my ($orig_found_scripts_ref,$orig_missing_scripts_ref)=$self->FindReqFilesInOrder($orig_f_dest_info,@required_scripts);

		if (scalar(@$orig_missing_scripts_ref)!=0) {
			#The scripts weren't even in the original dest manifest, meaning they weren't packaged/copied
			foreach my $file (@$orig_missing_scripts_ref) {
				&::LogMsg("Required script $file not found in patch contents");
			}
			return (0,undef,$missing_scripts_ref);
		} else {
			#Scripts were filtered out because they were in dbmanifest
			#Proceed with the reduced list
		}
	}
	
	return (1,$found_scripts_ref,$missing_scripts_ref);
}

########################
# Sub:  FindReqFilesInOrder
# Desc: Find a list of files in order, and complain about ones we don't find
# Ret:  a ref to an array of dest_info rows for found scripts, a ref to an array of file names that couldn't be found
########################
sub FindReqFilesInOrder {
	my $self=shift;
	my ($filtered_dest_info,@files)=@_;
	
	my @dest_list=();
	my @missing_list=();
	foreach my $file (@files) {
		my $ref=$self->FindFileByShortName($filtered_dest_info,$file);
		
		if ($ref eq '') {
			push(@missing_list,$file);
		} else {
			push(@dest_list,$ref);
		}
	}
	
	return (\@dest_list,\@missing_list);
}

########################
# Sub:  FindFileByShortName
# Desc: A function to find a script in the destination manifest by short name (rpmctl.sql for example)
# Ret:  the ref for the dest_info_ref row if found, '' if not
########################
sub FindFileByShortName {
	my $self=shift;
	my ($filtered_dest_info,$short_name)=@_;
	
	foreach my $ref (@$filtered_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		if ($dfile=~/${short_name}$/) {
			return $ref;
		}
	}
	return '';
}


########################
# Sub:  LoadTypes
# Desc: A function to processing type files with custom error handling to ignore failures
#       to drop a type that doesn't exist
# Ret:  1 if the file ran successfully, 0 if not
########################
sub LoadTypes {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name)=@_;
	
	my @filter=(
	qr/ORA\-0*4043/,	#ORA-04043: object OBJ_RPM_SYSTEM_OPTIONS_REC does not exist
	qr/ORA\-0*942/,		#ORA-00942: table or view does not exist
	);
	
	if ($self->RunDestFileListWithErrorFilter($full_dest,$filtered_dest_info,\@filter)!=1) {
		#Problems running a script, abort
		return 0;
	}
	return 1;
}

########################
# Sub:  DoSQL
# Desc: Run a SQL command and filter the errors
# Ret:  1 if the file ran successfully, 0 if not
########################
sub DoSQL {
	my $self=shift;
	my ($file_name,$sql,$filter_ref,$no_success_msg)=@_;
	
	my $user_string=$self->{'CONNECT_STRING'};
	$ENV{TNS_ADMIN}=$self->{'TNS_ADMIN'};
	&::SetOracleHome($self->{'ORACLE_HOME'});
	
	#We override SQLPath so that copy_partitioning can be found
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	
	my $utils_dir=&::ConcatPathString($dest_dir,'orpatch','utilities');
	local $ENV{SQLPATH}="$utils_dir:$ENV{SQLPATH}";
	
	#Run the SQL in the database
	my ($output_ref,$error_ref)=&::ExecSQL($user_string,$sql,0);
	
	#Filter the results
	my @filtered_errors=();
	if (scalar(@$error_ref)!=0) {
		#loop through all errors
		foreach my $error (@$error_ref) {
			my $ignore=0;
			#Check each error against all our 'ignore' patterns
			#If it matches, we ignore the error
			foreach my $pat (@$filter_ref) {
				if ($error=~/$pat/) {
					$ignore=1;
					last;
				}
			}
			if ($ignore==0) {
				push(@filtered_errors,$error);
			}
		}
	}
	
	my $ret=1;
	if (scalar(@filtered_errors)!=0) {
		&::LogMsg("File $file_name failed");
		&::DebugOutput("$file_name ERRORS",@filtered_errors);
		&::DebugOutput("$file_name LOG",@$output_ref);
		$ret=0;
	} else {
		unless($no_success_msg==1) {
			&::LogMsg("File $file_name successfully executed");
		}
		$ret=1;
	}
	return $ret;
}

########################
# Sub:  CallExecSQL
# Desc: Setup our environment and call ExecSQL for a single statement
#       DoSQL is prefered for use but this is a convenience for running a simple query
# Args: SQL statement to run, without trailing ;
# Ret:  ref to the array of output, ref to the array of errors
########################
sub CallExecSQL {
	my $self=shift;
	my ($sql)=@_;
	
	my $user_string=$self->{'CONNECT_STRING'};
	$ENV{TNS_ADMIN}=$self->{'TNS_ADMIN'};
	&::SetOracleHome($self->{'ORACLE_HOME'});
	
	my ($output_ref,$error_ref)=&::ExecSQL($user_string,$sql,0);
	
	return ($output_ref,$error_ref);
}

########################
# Sub:  CallLoadorDropJava
# Desc: Setup our environment and call LoadJava or dropjava for one or more jar files
# Args: Path containing jar,jar to load,1 to abort on errors,jar type,filter_ref,load_or_drop
# Ret:  1 if successful, 0 if not
########################
sub CallLoadorDropJava {
	my $self=shift;
	my ($jar_dir,$jar,$abort_on_errors,$jar_type,$filter_ref,$load_or_drop)=@_;
	
	my $user_string=$self->{'CONNECT_STRING'};
	$ENV{TNS_ADMIN}=$self->{'TNS_ADMIN'};
	my $oh=$self->{'ORACLE_HOME'};
	&::SetOracleHome($oh);
	
	my $original_cwd=cwd();
	unless (chdir($jar_dir)) {
		&::LogMsg("Failed to chdir to $jar_dir: $!");
		return 0;
	}
	
	my $load_name=(($load_or_drop==1) ? 'loadjava' : 'dropjava');
	
	my ($output_ref,$error_ref)=$self->_ExecLoadorDropJava($user_string,$oh,$jar,0,$load_name);
	
	chdir($original_cwd);
	
	#Filter the results
	my @filtered_errors=();
	if (scalar(@$error_ref)!=0) {
		#loop through all errors
		foreach my $error (@$error_ref) {
			my $ignore=0;
			#Check each error against all our 'ignore' patterns
			#If it matches, we ignore the error
			foreach my $pat (@$filter_ref) {
				if ($error=~/$pat/) {
					$ignore=1;
					last;
				}
			}
			if ($ignore==0) {
				push(@filtered_errors,$error);
			}
		}
	}
	
	my $failed=0;
	if (scalar(@filtered_errors)!=0) {
		&::LogMsg("$load_or_drop of $jar failed");
		&::DebugOutput("$load_or_drop ERRORS",@filtered_errors);
		$failed=1;
	}
	
	#If they passed multiple jar names, concatenate the basenames and append _group to the name
	if ($jar=~/\s+/) {
		$jar=~s/\s+/_/g;
		$jar.='_group';
	}
	
	#If we are dropping, prepend drop_ to the name
	if ($load_or_drop==0) {
		$jar="drop_$jar";
	}
	
	my $detail_dir=$self->GetActionName();
	$detail_dir=~tr/A-Z/a-z/;
	&DetailLog::CreateLog($detail_dir,$jar_type,$jar,$output_ref,($failed==1) ? 'err' : 'log');
	
	if ($abort_on_errors==1 && $failed==1) {
		return 0;
	}
	
	return 1;
}

########################
# Sub:  UpdateDBManifestTable
# Desc: Update the dbmanifest table about a script that was just run
########################
sub UpdateDBManifestTable {
	my $self=shift;
	my ($file,$revision,$file_type,$patch_name)=@_;

	my $SQL=<<ENDSQL;
merge into dbmanifest mm using
( select '$file' file_name, '$revision' version, '$file_type' file_type, '$patch_name' patch_name,sysdate sdate from dual ) use_this
on ( mm.file_name = use_this.file_name )
when matched then update
set mm.version  = use_this.version,
	mm.file_type = use_this.file_type,
	mm.patch_name = use_this.patch_name,
	mm.applied_date = use_this.sdate
when not matched then
insert ( file_name, version, file_type, patch_name, applied_date )
values ( use_this.file_name, use_this.version, use_this.file_type, use_this.patch_name, use_this.sdate );
commit
ENDSQL
	
	my $ret=$self->DoSQL('upd_dbmanifest.sql',$SQL,[],1);
	
	if ($ret!=1) {
		&::LogMsg("WARNING: Update of dbmanifest table failed, $file will likely be run again");
	}
	
	return $ret;
}

########################
# Sub:  RemoveFromDBManifestTable
# Desc: Remove a row about a script from the dbmanifest table
########################
sub RemoveFromDBManifestTable {
	my $self=shift;
	my ($file,$file_type)=@_;

	my $SQL=<<ENDSQL;
delete from dbmanifest
where file_name = '$file'
and   file_type = '$file_type';
commit
ENDSQL
	
	my $ret=$self->DoSQL('del_dbmanifest.sql',$SQL,[],1);
	
	if ($ret!=1) {
		&::LogMsg("ERROR: delete from dbmanifest table failed!");
	}
	
	return $ret;
}

#########################
# Sub:  GetFileTypeInfo
# Desc: Get the details on how to run a specific file type
# Arg:  file type
# Ret:  when_run,process_function,std_catch_errors,patch_product,run_phase,delete file function
#########################
sub GetFileTypeInfo {
	my $self=shift;
	my ($file_type)=@_;
	
	my $found_ref='';
	foreach my $ref (@{$self->{'FILE_TYPE_INFO'}}) {
		if ($$ref[0] eq $file_type) {
			$found_ref=$ref;
			last;
		}
	}
	
	return $self->DecodeFileTypeInfo($found_ref);
}

#########################
# Sub:  DecodeFileTypeInfo
# Desc: Extract the components from a FileTypeInfo ref
# Arg:  file type info ref - Ref to the file type info to decode
# Ret:  when_run,process_function,std_catch_errors,patch_product,run_phase,delete file function
#########################
sub DecodeFileTypeInfo {
	my $self=shift;
	my ($ref)=@_;
	
	my $def_when_run=1;
	my $def_process_func='';
	my $def_std_catch_errors=1;
	my $def_patch_product='';
	my $def_run_phase='PATCHACTION';
	my $def_del_func='';
	
	if ($ref ne '') {
		my $when_run=$$ref[1];
		$when_run=$def_when_run if ($when_run eq '');
		my $process_func=$$ref[2];
		$process_func=$def_process_func if ($process_func eq '');
		my $std_catch_errors=$$ref[3];
		$std_catch_errors=$def_std_catch_errors if ($std_catch_errors eq '');
		my $patch_product=$$ref[4];
		$patch_product=$def_patch_product if ($patch_product eq '');
		my $run_phase=$$ref[5];
		$run_phase=$def_run_phase if ($run_phase eq '');
		my $del_func=$$ref[6];
		$del_func=$def_del_func if ($del_func eq '');
		return ($when_run,$process_func,$std_catch_errors,$patch_product,$run_phase,$del_func);
	}
	
	return ($def_when_run,$def_process_func,$def_std_catch_errors,$def_patch_product,$def_run_phase,$def_del_func);
}

#########################
# Sub:  SetCheckLockedObjects
# Desc: Configure whether or not this action should check for locked objects during pre-check
#       since not all schemas have the privileges to query v$locked_object, we make this configurable
#       the default is 1 (on)
# Arg:  on_off - 1 if we should check for locked objects, 0 if not
# Ret:  on_off
#########################
sub SetCheckLockedObjects {
	my $self=shift;
	my ($on_off)=@_;
	$self->{'CHECK_LOCKED_OBJECTS'}=$on_off;
	return $self->{'CHECK_LOCKED_OBJECTS'};
}

#########################
# Sub:  GetCheckLockedObjects
# Desc: Get the current setting for checking locked objects
# Arg:  none
# Ret:  1 if we check locked objects, 0 if not
#########################
sub GetCheckedLockedObjects {
	my $self=shift;
	return $self->{'CHECK_LOCKED_OBJECTS'};
}

#########################
# Sub:  SetUniqueDBManifest
# Desc: Configure whether or not this action has a unique manifest
#       or if it shares a schema with some other action (i.e. Reim and AllocRMS)
# Arg:  on_off - 1 if we have a unique manifest, 0 if not
# Ret:  on_off
#########################
sub SetUniqueDBManifest {
	my $self=shift;
	my ($on_off)=@_;
	$self->{'UNIQUE_DBMANIFEST'}=$on_off;
	return $self->{'UNIQUE_DBMANIFEST'};
}

#########################
# Sub:  GetUniqueDBManifest
# Desc: Get the current setting for checking locked objects
# Arg:  none
# Ret:  1 if we check locked objects, 0 if not
#########################
sub GetUniqueDBManifest {
	my $self=shift;
	return $self->{'UNIQUE_DBMANIFEST'};
}

sub DecodeRestartPoint {
	my $self=shift;
	my ($restart_point)=@_;
	
	my ($dest_subdir,$file_type)=split(/\:/,$restart_point);
	return ($dest_subdir,$file_type);
}

sub MarkRestartPoint {
	my $self=shift;
	my ($restart_state,$dest_subdir,$type)=@_;
	
	return $restart_state->SetAndSaveActionInfo("$dest_subdir:$type");
}

sub FindFTIndex {
	my $self=shift;
	my ($restart_file_type,$ft_ref)=@_;
	
	my $ft_index=0;
	for(;$ft_index<scalar(@$ft_ref);$ft_index++) {
		my $this_type=$$ft_ref[$ft_index];
		if ($this_type eq $restart_file_type) {
			return $ft_index;
		}
	}
	#Couldn't find it, start from beginning
	return 0;
}

#########################
# Sub:  LoadDBJarsSingly
# Desc: a custom processing function that will use loadjava to install jars one at a time into the database
#########################
sub LoadDBJarsSingly {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,$subpatch_ref)=@_;
	
	#Filter the full manifest for jars updated with this patch
	#We do this because we tell PatchAction to treat us like a run type 2 (always run all) but really want to only be run when files are updated
	#We can't use run type 1 or 3 because those both filter against the dbmanifest
	my $f_dest_info=&Manifest::FilterDestManifest($orig_f_dest_info,'',$patch_name,$subpatch_ref);
	
	#No files have changed
	if (scalar(@$f_dest_info)==0) {
		return 1;
	}
	
	#Get the type for these files to create the DetailLog directory
	my $first_ref=$$f_dest_info[0];
	my ($first_dfile,$first_sfile,$first_ftype,@discard)=&Manifest::DecodeDestLine($first_ref);
	my $detail_dir=$self->GetActionName();
	$detail_dir=~tr/A-Z/a-z/;
	&DetailLog::CleanDetailLogDir($detail_dir,$first_ftype);
	
	#Loop through jars and load them one at a time
	foreach my $ref (@$f_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		my $full_file=&::ConcatPathString($full_dest,$dfile);

		my ($full_dir,$short_file)=&::GetFileParts($full_file,2);
		
		&::LogMsg("Loading jar $dfile");
		if ($self->CallLoadorDropJava($full_dir,$short_file,1,$ftype,[],1)!=1) {
			return 0;
		}
	}
	
	return 1;
}

#########################
# Sub:  LoadDBJarsGroup
# Desc: a custom processing function that will use loadjava to install jars into the database using a single loadjava command
#       Note this function ignores ORA-29552 errors that are generated when loading dbws jars
#########################
sub LoadDBJarsGroup {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,$subpatch_ref)=@_;
	
	#Filter the full manifest for jars updated with this patch
	#We do this because we tell PatchAction to treat us like a run type 2 (always run all) but really want to only be run when files are updated
	#We can't use run type 1 or 3 because those both filter against the dbmanifest
	my $f_dest_info=&Manifest::FilterDestManifest($orig_f_dest_info,'',$patch_name,$subpatch_ref);
	
	#No files have changed
	if (scalar(@$f_dest_info)==0) {
		return 1;
	}
	
	#Get the type for these files to create the DetailLog directory
	my $first_ref=$$f_dest_info[0];
	my ($first_dfile,$first_sfile,$first_ftype,@discard)=&Manifest::DecodeDestLine($first_ref);
	my $detail_dir=$self->GetActionName();
	$detail_dir=~tr/A-Z/a-z/;
	&DetailLog::CleanDetailLogDir($detail_dir,$first_ftype);
	
	my @jars=();
	my $run_dir='';
	my $jar_type='';

	#If any file has changed, we collect all jars of this type to load in a single command	
	foreach my $ref (@$orig_f_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($ref);
		
		my $full_file=&::ConcatPathString($full_dest,$dfile);
		
		my ($full_dir,$short_file)=&::GetFileParts($full_file,2);
		
		push(@jars,$short_file);
		$jar_type=$ftype;
		
		#Make sure all jars are in a single directory
		if ($run_dir eq '') {
			$run_dir=$full_dir;
		} else {
			if ($run_dir ne $full_dir) {
				&::LogMsg("$full_file directory path does not match directory of other jars in group ($run_dir)");
				return 0;
			}
		}
	}
	
	my @ignore_filter=(qr/ORA\-29552/);
	
	#Now run one loadjava command to load the files
	&::LogMsg("Loading jars ".join(',',@jars));
	if ($self->CallLoadorDropJava($run_dir,join(' ',@jars),1,$jar_type,\@ignore_filter,1)!=1) {
			return 0;
	}
	
	return 1;
}

#########################
# Sub:  DropDBJar
# Desc: a custom processing function that will use dropjava to drop a jar from the database
# Args: full_file_name - Full path to the jar that will be removed
#       dm_info_ref - Ref to the destination manifest information
# Ret:  1 on success, 0 on failure
#########################
sub DropDBJar {
	my $self=shift;
	my ($full_file_name,$dm_info_ref)=@_;
	
	my ($dfile,$sfile,$ftype,$frevision,$fdesc,$customized,@discard)=&Manifest::DecodeDestLine($dm_info_ref);
	
	my $detail_dir=$self->GetActionName();
	$detail_dir=~tr/A-Z/a-z/;
	
	#Only clean the dropjar directory once per run
	if ($self->{'DROP_JAR_CLEAN'} eq '') {
		&DetailLog::CleanDetailLogDir($detail_dir,$ftype);
		$self->{'DROP_JAR_CLEAN'}=1;
	}

	my ($full_dir,$short_file)=&::GetFileParts($full_file_name,2);
		
	&::LogMsg("Dropping jar $dfile");
	return $self->CallLoadorDropJava($full_dir,$short_file,1,$ftype,[],0);
}

#########################
# Sub:  LoadRIBSubTypes
# Desc: a custom function to check if the RIB types in sub scripts have changed so that
#       we can trigger a full reload of the RIBTop scripts
#########################
sub LoadRIBSubTypes {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,$subpatch_ref)=@_;

	#Note that we do not run any RIB scripts at this point, we are only checking if any
	#of the RIB scripts we don't track in DBmanifest have changed
	#If so, we set a flag so the RIBTopTypes function forces a full RIB load
	
	#Filter the complete dest manifest for things updated with this patch
	my $f_dest_info=&Manifest::FilterDestManifest($orig_f_dest_info,'',$patch_name,$subpatch_ref);
	
	if (scalar(@$f_dest_info)!=0) {
		&::LogMsg("One or more RIB second-level scripts have been updated, forcing RIB load");
		$self->{'RIB_TYPE_LOAD'}=1;
	}
	
	return 1;
}

#########################
# Sub:  LoadRIBTopTypes
# Desc: a custom function for loading RIB types
#       Using this function requires that you have defined $self->{'RIB_SUBTYPE_NAME'}
#########################
sub LoadRIBTopTypes {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,$subpatch_ref)=@_;

	#First, we set our restart point back to the RIB sub scripts so if there is a problem
	#we will restart their to reset the RIB_TYPE_LOAD flag
	#
	#Extract our dest_subdir out of full_dest
	my @parts=split(/\//,$full_dest);
	my $dest_subdir=pop(@parts);
	$self->MarkRestartPoint($restart_state,$dest_subdir,$self->{'RIB_SUBTYPE_NAME'});
	
	#Filter for top-level scripts updated with this patch
	my $f_dest_info=&Manifest::FilterDestManifest($orig_f_dest_info,'',$patch_name,$subpatch_ref);
	
	if ($self->{'RIB_TYPE_LOAD'}!=1 && scalar(@$f_dest_info)==0) {
		&::LogMsg("No changes to RIB scripts in this patch, skipping RIB load");
		return 1;
	}
	
	#At least one RIB type changed, we need to load all RIB objects
	
	#Drop all RIB types
	if ($self->DropRIBTypes()!=1) {
		return 0;
	}
	
	#Load all RIB types (we pass orig_f_dest_info so that all RIB types are re-loaded)
	#We don't use the standard LoadTypes function because we also need to filter ORA-02303 errors
	my @filter=(
	qr/ORA\-0*4043/,	#ORA-04043: object OBJ_RPM_SYSTEM_OPTIONS_REC does not exist
	qr/ORA\-0*942/,		#ORA-00942: table or view does not exist
	qr/ORA\-0*2303/,	#ORA-02303: cannot drop or replace a type with type or table dependents
	);

	if ($self->RunDestFileListWithErrorFilter($full_dest,$orig_f_dest_info,\@filter)!=1) {
		return 0;
	}
	
	#Force compile all types
	if ($self->ForceCompileTypes()!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  DropRIBTypes
# Desc: drop all the RIB types from the database
# Arg:  None
# Ret:  1 on success, 0 on failure
#########################
sub DropRIBTypes {
	my $self=shift;
	
	if ($self->GetCheckedLockedObjects==1) {
		if ($self->CheckRIBObjectAccess()!=1) {
			return 0;
		}
	}
	
	my $SQL=<<ENDSQL;
set serveroutput on size unlimited
set escape on
declare 
begin
    for c_rib_drop in (select object_name from user_objects where object_type = 'TYPE' and object_name like 'RIB\_%') loop
    begin
        execute immediate 'drop type "' || c_rib_drop.object_name || '" force';
            dbms_output.put_line( 'info: dropped ' || c_rib_drop.object_name );
        exception
            when others then
            dbms_output.put_line( 'error: drop rib type failed ' || c_rib_drop.object_name );
    end;
    end loop;
end;
/
select count(1) from user_objects where object_type = 'TYPE' and object_name like 'RIB\_%'
ENDSQL

	&::LogMsg("Dropping RIB types...");
	
	if ($self->DoSQL('drop_rib_types.sql',$SQL,[],1)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  CheckRIBObjectAccess
# Desc: Verify that no one is accessing RIB_OBJECT in our schema
# Arg:  None
# Ret:  1 on success, 0 on failure
#########################
sub CheckRIBObjectAccess {
	my $self=shift;

	my $SQL=<<ENDSQL;
alter session set "_optimizer_cartesian_enabled"=false;
select b.sid||'|'||b.username||'|'||b.program||'|'||b.osuser
from v\$session b,v\$access a
where a.sid = b.sid
and a.owner = '&_USER'
and upper(a.object) like '\%RIB_OBJECT\%'
ENDSQL

	my $user_string=$self->{'CONNECT_STRING'};
	&::LogMsg("Checking for RIB_OBJECT access in $user_string");
	my ($output_ref,$error_ref)=$self->CallExecSQL($SQL);
	
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Errors encountered while checking RIB_OBJECT access!");
		&::DebugOutput('RIB Object access check',@$output_ref);
		return 0;
	}
	
	my $locks=scalar(@$output_ref);
	if ($locks!=0) {
		#Accessing schemas found
		&::LogMsg("Found $locks session(s) accessing RIB_OBJECT:");
		
		foreach my $line (@$output_ref) {
			chomp($line);
			my ($s,$u,$p,$os)=split(/\|/,$line);
			&::LogMsg("Session $s User $u - Program $p OS User $os");
		}
		&::LogMsg("Aborting, please ensure no sessions are connected to $user_string schema");
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  ForceCompileTypes
# Desc: Compile all types in the database, even if their status is valid
#       this works around occasional object marked for delete errors
# Arg:  None
# Ret:  1 on success, 0 on failure
#########################
sub ForceCompileTypes {
	my $self=shift;
	
	my $SQL=<<ENDSQL;
set serveroutput on size unlimited
declare cursor c_types IS
	select object_type, object_name, 'compile' c from  user_objects where object_type = 'TYPE'
	union all
	select 'TYPE', object_name, 'compile body' c from user_objects where object_type = 'TYPE BODY';
begin
	for rec_types in c_types
	loop
	begin
		execute immediate 'alter ' || rec_types.object_type || ' "' || rec_types.object_name || '" ' || rec_types.c;
			dbms_output.put_line( 'info: recompiled ' || rec_types.object_name );
		exception
			when others then
			dbms_output.put_line( 'error: recompile failed ' || rec_types.object_name );
		end;
	end loop;
end;
/
select 'compile complete' from dual
ENDSQL

	&::LogMsg("Force compiling all types...");
	
	return $self->DoSQL('compile_all_types.sql',$SQL,[],1);
}

#########################
# Sub:  CreateSynonymsForUser
# Desc: Create synonyms in one schema for all objects in another schema
# Args: syn_schema - The schema that will have synonyms
#       owning_schema - The schema that has the objects the synonyms will point to
#       Either syn_schema or owning schema can be blank, but not both
# Ret:  1 on success, 0 on failure
#########################
sub CreateSynonymsForUser {
	my $self=shift;
	my ($syn_schema,$owning_schema)=@_;
	
	if ($syn_schema eq '' && $owning_schema eq '') {
		&::LogMsg("Unable to create synonyms from and to the same schema");
		return 0;
	}
	
	$syn_schema='&_USER' if ($syn_schema eq '');
	$owning_schema='&_USER' if ($owning_schema eq '');

	my $SQL=<<ENDSQL;
\@\@create_synonyms_one_user.sql $syn_schema $owning_schema
select 'COMPLETE - create_synonyms_one_user.sql' from dual
ENDSQL

	my $os_text=($owning_schema eq '&_USER') ? $self->GetSchemaName() : $owning_schema;
	my $ss_text=($syn_schema eq '&_USER') ? $self->GetSchemaName() : $syn_schema;
	&::LogMsg("Creating synonyms in $ss_text pointing to $os_text ...");
	if ($self->DoSQL('create_synonyms_one_user.sql',$SQL,[])!=1) {
		&::LogMsg("Failed to create synonyms, aborting");
		return 0;
	}
	return 1;
}

#########################
# Sub:  CheckRequiredTablespaces
# Desc: Check if all our required tablespaces exist
# Args: None
# Ret:  1 on success, 0 on failure
#########################
sub CheckRequiredTablespaces {
	my $self=shift;
	
	my $req_ts_ref=$self->GetRequiredTablespaces();
	
	if (scalar(@$req_ts_ref)==0) {
		return 1;
	}
	
	&::LogMsg("Checking required tablespaces");
	
	#Get a list of all tablespaces
	my $sql="select tablespace_name from dba_tablespaces";
	
	my ($output_ref,$error_ref)=$self->CallExecSQL($sql);
	
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Unable to get list of tablespaces");
		return 0;
	}
	
	my @ts_list=();
	foreach my $line (@$output_ref) {
		chomp($line);
		push(@ts_list,$line);
	}	

	#Find missing tablespaces
	my @missing=();
	foreach my $req_ts (@$req_ts_ref) {
		if (grep(/^$req_ts$/i,@ts_list)!=1) {
			push(@missing,$req_ts);
		}
	}
	
	if (scalar(@missing)!=0) {
		&::LogMsg("One or more required tablespaces are missing:");
		foreach my $ts (@missing) {
			&::LogMsg("   $ts");
		}
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  SetRequiredTablespaces
# Desc: Register a list of tablespaces as required during precheck
# Args: a list of tablespace names that must exist during precheck
# Ret:  a ref to the list
#########################
sub SetRequiredTablespaces {
	my $self=shift;
	my (@ts_list)=@_;
	
	$self->{'REQUIRED_TABLESPACES'}=\@ts_list;
}

#########################
# Sub:  GetRequiredTablespaces
# Desc: Get a reference to the list of tablespaces
# Args: None
# Ret:  a ref to the list of required tablespaces
#########################
sub GetRequiredTablespaces {
	my $self=shift;
	
	return $self->{'REQUIRED_TABLESPACES'};
}

#########################
# Sub:  SaveSchemaName
# Desc: Get the database username for this DBSQL action and save it for later
# Args: None
# Ret:  1 on success, 0 on failure
#########################
sub SaveSchemaName {
	my $self=shift;
	
	my $sql="select '&_USER' from dual";
	
	my ($output_ref,$error_ref)=$self->CallExecSQL($sql);
	
	my $name=$self->GetActionName();
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Unable to retrieve $name schema owner name");
		&::DebugOutput("get schema owner",@$output_ref);
		return 0;
	}
	
	my $schema=$$output_ref[0];
	chomp($schema);
	
	$self->{'SCHEMA_NAME'}=$schema;

	return 1;
}

#########################
# Sub:  GetSchemaName
# Desc: return the schema name
# Args: None
# Ret:  schema name used by this action
#########################
sub GetSchemaName {
	my $self=shift;
	return $self->{'SCHEMA_NAME'};
}

#########################
# Sub:  GetCrossActionSchemaName
# Desc: return the schema name of a different action
# Args: action_name
# Ret:  schema name or '' on errors
#########################
sub GetCrossActionSchemaName {
	my $self=shift;
	my ($action_name)=@_;
	
	my $global_state=$self->GetORPatchGlobalState();
	
	my $dbsql_handle=$global_state->GetActionHandle($action_name);
	if (!defined($dbsql_handle)) {
		&::LogMsg("Unable to get handle to $action_name action");
		return '';
	}
	
	my $schema_name=$dbsql_handle->GetSchemaName();
	
	if ($schema_name eq '') {
		&::LogMsg("Unable to retrieve $action_name schema name");
		return '';
	}
	
	return $schema_name;
}

#########################
# Sub:  ExportGeneratedConfig
# Desc: Export any configuration that does not exist as a file
# Args: gen_dir - path to the dir to place generated files (must be created before use)
#       metadata_map - handle to the ORPMetadataMap object to register fies with
#       manifest_list_ref - ref to the list to add exported files to
#       support_dir - ref to the base directory for exports
# Ret:  1 if successf, 0 if not
#########################
sub ExportGeneratedConfig {
	my $self=shift;
	my ($gen_dir,$metadata_map,$manifest_list_ref,$support_dir)=@_;
	
	my $action_name=$self->GetActionName();
	my $lower_name=$action_name;
	$lower_name=~tr/A-Z/a-z/;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	
	my $db_manifest=&::ConcatPathString($gen_dir,"${lower_name}_dbmanifest.csv");
		
	if ($self->ExportDBManifestToFile($db_manifest)!=1) {
		&::LogMsg("Unable to save dbmanifest for $action_name");
		return 0;
	}
	
	#Some DBSQL actions don't have their own DBManifest and so don't export it (ReIM, ALCRMS)
	if (-f $db_manifest) {
		chmod(0750,$db_manifest);
		push(@$manifest_list_ref,$db_manifest);
		
		#Files must be registered with the MetadataMap as if they were relative to RETAIL_HOME
		#so pretend that these existed at $RETAIL_HOME/generated
		my $fake_name=&::ConcatPathString($dest_dir,&::GetRelativePathName($db_manifest,$support_dir));
		$metadata_map->RegisterFile($action_name,$metadata_map->GetDBManifestType(),$fake_name);
	}
	
	return 1;
}


#########################
# Sub:  ExportDBManifestToFile
# Desc: Write all rows in the dbmanifest table to a file
# Args: file_name
# Ret:  1 on success, 0 on failure
#########################
sub ExportDBManifestToFile {
	my $self=shift;
	my ($file_name)=@_;
	
	my ($proceed,$manifest_ref)=$self->GetFullDBManifestContents();
	if ($proceed!=1) {
		return 0;
	}
	
	if (scalar(@$manifest_ref)==0) {
		return 1;
	}
	
	my ($dir,$file)=&::GetFileParts($file_name,2);
	
	&::CreateDirs('0750',$dir);
	
	unless (open(EXPORT_FILE,">$file_name")) {
		&::LogMsg("Unable to open $file_name: $!");
		return 0;
	}
	print EXPORT_FILE "#file name,revision,file type,applied date,patch name\n";
	foreach my $ref (@$manifest_ref) {
		my ($fn,$v,$ft,$pn,$ad)=@$ref;
		print EXPORT_FILE join(',',$fn,$v,$ft,$ad,$pn)."\n";
	}
	close(EXPORT_FILE);
	
	return 1;
}
	
#########################
# Sub:  GetFullDBManifestContents
# Desc: Get all rows from dbmanifest
# Args: None
# Ret:  proceed - 1 on success, 0 on failure
#       [[file name,revision,file type,patch name,applied date]]
#########################
sub GetFullDBManifestContents {
	my $self=shift;
	
	if ($self->GetUniqueDBManifest()!=1) {
		#This schema doesn't have a unique dbmanifest
		return (1,[]);
	}
	
	#Check for DBManifest table existence
	my $dbmanifest_status=$self->_CheckDBManifestExists();
	if ($dbmanifest_status!=1) {
		if ($dbmanifest_status==0) {
			#Not existing is OK
			return (1,[]);
		} else {
			#errors checking for existence is not
			return (0,[]);
		}
	}

	my $export_sql=<<ENDSQL;
select file_name||'|'||version||'|'||file_type||'|'||patch_name||'|'||to_char(applied_date,'MM/DD/YYYY HH24:MI:SS')
from dbmanifest
order by file_name
ENDSQL
	
	my $name=$self->GetActionName();
	&::LogMsg("Getting dbmanifest contents for $name");
	my ($output_ref,$error_ref)=$self->CallExecSQL($export_sql);
	
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Unable to get DBManifest contents");
		&::DebugOutput("DBManifest Table Contents",@$output_ref);
		return (0,[]);
	}
	
	my @results=();
	foreach my $line (@$output_ref) {
		chomp($line);
		my ($f,$v,$ft,$pn,$ad)=split(/\|/,$line);
		push(@results,[$f,$v,$ft,$pn,$ad]);
	}
	
	return (1,\@results);
}

#########################
# Sub:  IsFreshInstall
# Desc: Check if this install is the initial install of this component
# Args: None
# Ret:  1 if it is an initial install, 0 if not, -1 on error
#########################
sub IsFreshInstall {
	my $self=shift;
	my $global_state=$self->GetORPatchGlobalState();
	my $restart_state=$global_state->GetRestartState();
	return $restart_state->GetCurrentFreshInstall();
}

#########################
# Sub:  _CheckDBManifestExists
# Desc: Check if the DBManifestExists
# Args: None
# Ret:  1 if it does, 0 if it does not, -1 on error
#########################
sub _CheckDBManifestExists {
	my $self=shift;
	
	my $check_sql="select count(1) from user_tables where table_name = 'DBMANIFEST'";
	
	my ($output_ref,$error_ref)=$self->CallExecSQL($check_sql);
	
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Unable to check DBManifest table existence");
		&::DebugOutput("DBManifest Table Exists",@$output_ref);
		return -1;
	}

	my $obj_count=$$output_ref[0];
	chomp($obj_count);
	$obj_count=~s/\s*//g;
	
	if ($obj_count==0) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  _ExecLoadorDropJava
# Desc: Load a jar into the database.
# Args: userid,oracle home,jar to load,error level,load_exe_name
# Ret:  ref to list of output,ref to list of errors
#########################
sub _ExecLoadorDropJava {
	my $self=shift;
	my ($userid,$oh,$jar_file,$error_level,$load_exe_name)=@_;
	
	my $command='';
	if ($load_exe_name eq 'loadjava') {
		$command="$oh/bin/$load_exe_name -stdout -u $userid -v -r -f $jar_file";
	} else {
		$command="$oh/bin/$load_exe_name -u $userid -v $jar_file";
	}
	
    my @cmd_output=`$command 2>&1`;
	my $status=$?;
	
	&::DebugOutput($load_exe_name,@cmd_output) if ($error_level==4);
	
	my @errors=grep(/ORA\-|SP2\-|exiting:|Errors: (?!0)/,@cmd_output);
	
	if (scalar(@errors)>0) {
		&::DebugOutput($load_exe_name,@cmd_output) if ($error_level==3);
		
		if ($error_level>0) {
			my $message="$load_exe_name of $jar_file produced errors";
            &::LogMsg($message,@errors);
			push(@errors,$message);

            if ($error_level==2) {
                die "Error while running $load_exe_name: $errors[0]";
            }
        }
	}
	
	if (wantarray) {
        return(\@cmd_output,\@errors);
    } else {
        return(scalar(@errors)==0);
    }
}

#########################
# Sub:  _LogInvalidObjectDetails
# Desc: Create detail logs of the reason for invalid objects
# Args: ref to current invalid list [object_type,object_name],ref to original invalid list,detail dir name, detail subdir name
# Ret:  1
#########################
sub _LogInvalidObjectDetails {
	my $self=shift;
	my ($now_invalid_ref,$orig_invalid_ref,$detail_dir,$detail_subdir)=@_;
	
	my $log_user_errors=$self->{'DBSQL_PRINT_USER_ERRORS'};
	
	my $invalid_count=scalar(@$now_invalid_ref);
	foreach my $ref (@$now_invalid_ref) {
		&::LogMsg("$$ref[0] - $$ref[1]");
		my @errors=$self->_GetUserErrors($$ref[0],$$ref[1]);
		my $detail_filename="$$ref[0]_$$ref[1]";
		$detail_filename=~s/ /_/;
		$detail_filename=~s/[^a-zA-Z0-9\-\_\$]//;
		&DetailLog::CreateLog($detail_dir,$detail_subdir,$detail_filename,\@errors,'err');
		if ($log_user_errors==1) {
			&::DebugOutput("user_errors for $$ref[1]",@errors);
		}
	}
	if ($log_user_errors!=1) {
		&::LogMsg("See logs in detail_logs/$detail_dir/$detail_subdir for user_errors information");
	}
	&::LogMsg("$invalid_count invalid objects");
	return 1;
}

#########################
# Sub:  _GetUserErrors
# Desc: Get the contents of user_errors for a particular object
# Args: object type, object_name
# Ret:  list of lines from user_errors
#########################
sub _GetUserErrors {
	my $self=shift;
	my ($type,$obj)=@_;
	
	my $sql="select line||'|'||position||'|'||text from user_errors where type = '$type' and name = '$obj' order by sequence";
	
	my ($output_ref,$error_ref)=$self->CallExecSQL($sql);
	#Unfortunately this doesn't work since the text in user_errors is by definition filled with errors
	#if (scalar(@$error_ref)!=0) {
	#	&::LogMsg("Errors while getting user_errors for $type - $obj");
	#	&::DebugOutput('user_errors check',@$output_ref);
	#}
	
	my @results=();
	foreach my $line (@$output_ref) {
		chomp($line);
		my ($l,$p,$t)=split(/\|/,$line);
		push(@results,"$l,$p: $t");
	}	
	
	if (scalar(@results)==0) {
		push(@results,'No user_errors entry');
	}

	return @results;
}

sub _GetFileTypeInfoForPhase {
	my $self=shift;
	my ($phase_name)=@_;
	
	my $run_phases=$self->{'RUN_PHASE_INFO'};
	my $rp_ref=$run_phases->{$phase_name};
	
	my @file_types=();
	if ($rp_ref ne '') {
		@file_types=@$rp_ref;
	}
	
	return @file_types;
}

#########################
# Sub:  DBSQL::ValidateRunDBC
# Desc: Static method to check if DBCs should be run for this action
# Arg:  config_ref - Handle to the ConfigFile object
#       action_name - Action name to use
# Ret:  1 or 0 depending on the RUN_DBC setting
#########################
sub DBSQL::ValidateRunDBC {
	my ($config_ref,$action_name)=@_;
	
	return &ORPAction::SetTFFlagFromConfigVar({},$config_ref,'RUN_DBC','DBSQL',1,$action_name);
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($name,$file_type_info_ref,$config_ref)=@_;
	
	my $dest_dir=$name;
	$dest_dir=~tr/A-Z/a-z/;
	
	my @file_types=();
	foreach my $ref (@$file_type_info_ref) {
		push(@file_types,$$ref[0]);
	}
	
	my @file_copy_params=(new FileCopyParams(\@file_types,$dest_dir,1));

	my $self=$class->SUPER::new($name,\@file_copy_params,$config_ref);
	$self->{'FILE_TYPE_INFO'}=$file_type_info_ref;
	
	my %run_phases=();
	foreach my $ft_ref (@$file_type_info_ref) {
		my ($when_run,$process_func,$std_catch_errors,$patch_product,$run_phase,$del_func)=$self->DecodeFileTypeInfo($ft_ref);
		my $rp_ref=$run_phases{$run_phase};
		if ($rp_ref eq '') {
			$rp_ref=[];
		}
		push(@$rp_ref,$ft_ref);
		$run_phases{$run_phase}=$rp_ref;
	}
	$self->{'RUN_PHASE_INFO'}=\%run_phases;
	
	$self->SetCheckLockedObjects(1);
	$self->SetRequiredTablespaces();
	$self->SetUniqueDBManifest(1);
	
	my $cs=$self->{'CONNECT_STRING'}=$config_ref->Get("${name}_CONNECT");
	my $ta=$self->{'TNS_ADMIN'}=$self->ResolveConfigPath($config_ref->Get("${name}_WALLET"));
	my $oh=$self->{'ORACLE_HOME'}=$config_ref->Get("DB_ORACLE_HOME");
	my $hfm=$self->{'PATCH_MODE'}=$config_ref->Get('PATCH_MODE');
	
	my $ue_var='DBSQL_PRINT_USER_ERRORS';
	my $user_errors_def=0;
	if ($ENV{$ue_var} ne '') {
		$config_ref->Set($ue_var,$ENV{$ue_var});
	}
	my $log_invalid=$config_ref->ValidateTFVar($ue_var,0);
	$self->{$ue_var}=$log_invalid;
	
	$self->SetTFFlagFromConfigVar($config_ref,'IGNORE_INVALIDS','DBSQL',0);
	
    return $self;
}

1;
