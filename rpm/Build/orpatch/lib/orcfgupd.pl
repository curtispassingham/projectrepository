#!/usr/bin/perl
############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;
use File::Copy;

($ENV{RETAIL_HOME} eq '') && (die "ERROR: RETAIL_HOME must be defined!");
$::ORP_DIR="$ENV{RETAIL_HOME}/orpatch";
$::ORP_LIB_DIR="$::ORP_DIR/lib";

require "$::ORP_LIB_DIR/global_ora.pm";

$::SOURCE_FILE='';
$::DEST_FILE='';

&ValidateConfig();

my $ret=&MergeFiles($::SOURCE_FILE,$::DEST_FILE);

&::LogMsg("ORCfgUpd session complete");

exit (($ret==1) ? 0 : 1);

#########################
# Sub:  MergeFiles
# Desc: Merge a config file partially updated by the installer into the env_info.cfg
#########################
sub MergeFiles {
	my ($source,$dest)=@_;

	my $merge_vars=&ReadMergeVars($source);
	return 0 if $merge_vars eq '';
	
	my $dc_ref=&ReadDestFile($dest);
	return 0 if $dc_ref eq '';

	my @new_products=();
	my @new_misc=();
	my @new_actions=();
	foreach my $ref (@$merge_vars) {
		my ($k,$value,$comment_ref)=@$ref;
		
		my $new_line="$k=$value";
		
		#Look through the dest contents for the line that matches
		my $found=0;
		for(my $i=0;$i<scalar(@$dc_ref);$i++) {
			if ($dc_ref->[$i]=~/^$k\s*=/) {
				$found=1;
				&::LogMsg("Updating $k to $value on line $i");
				$dc_ref->[$i]=$new_line;
				last;
			}
		}
		if (!$found) {
			my @lines=($new_line);
			if (scalar(@$comment_ref)!=0) {
				unshift(@lines,@$comment_ref);
			}
			if ($k=~/^PRODUCT_/i) {
				&::LogMsg("Adding $new_line as new product");
				push(@new_products,@lines);
			} elsif ($k=~/^ACTION_/i) {
				&::LogMsg("Adding $new_line as new action");
				push(@new_actions,@lines);
			} else {
				&::LogMsg("Adding $new_line as a new miscellaneous line");
				push(@new_misc,@lines);
			}
		}
	}
	
	&SpliceNewLines($dc_ref,\@new_products,\@new_actions,\@new_misc);
	
	#Check to make sure the resulting combination is acceptable
	unless (&CheckValidMerge($dc_ref)==1) {
		return 0;
	}
	
	unless(&WriteUpdatedFile($dest,$dc_ref)==1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  ReadMergeVars
# Desc: Read the source file and find any keys we should be updating
# Ret:  a ref to the hash of values to update, '' if errors
#########################
sub ReadMergeVars {
	my ($source)=@_;
	
	unless(open(SOURCE_FILE,"<$source")) {
		&::LogMsg("Unable to open $source: $!");
		return '';
	}
	
	&::LogMsg("Reading source file $source");
	my @comments=();
	my @saved_vars=();
	my $first_action=1;
	my $first_product=1;
	while(<SOURCE_FILE>) {
		chomp;
		my $line=$_;
		
		if ($line=~/^#/ || $line=~/^\s*$/) {
			push(@comments,$line);
		} else {
			#A line for a key
			my ($key,$value)=&GetKeyValue($line);
			
			#We only care about things with values
			if ($value ne '') {
				my $save=1;
				
				#And actions/products that aren't N
				if ($key=~/^(ACTION_|PRODUCT_)/ && $value eq 'N') {
					$save=0;
				}
				
				#We don't save comments for the first product or first action because it is boilerplate
				if ($key=~/^ACTION/i && $first_action==1) {
					@comments=();
					$first_action=0;
				}
				if ($key=~/PRODUCT/i && $first_product==1) {
					@comments=();
					$first_product=0;
				}
				
				if ($save==1) {
					push(@saved_vars,[$key,$value,[@comments]]);
				}
			}
			#Make sure comments are reset for the next variable regardless of whether we saved it
			@comments=();
		}
	}

	close(SOURCE_FILE);

	return \@saved_vars;
}

#########################
# Sub:  ReadDestFile
# Desc: Read the contents of the file we are updating
# Args: dest file
# Rets: ref to an array of the contents, '' if errors
#########################
sub ReadDestFile {
	my ($dest)=@_;
	
	&::LogMsg("Reading destination file $dest");
	unless(open(ENV_INFO,"<$dest")) {
		&::LogMsg("Unable to open $dest: $!");
		return '';
	}
	
	my @contents=();
	while(<ENV_INFO>) {
		chomp;
		push(@contents,$_);
	}
	close(ENV_INFO);
	
	return \@contents;
}

sub WriteUpdatedFile {
	my ($dest,$dc_ref)=@_;
	
	my $interim_name="${dest}.tmp";
	
	&::LogMsg("Saving updated configuration to $interim_name");
	unless(open(NEW_ENV,">$interim_name")) {
		&::LogMsg("Unable to open $interim_name: $!");
		return 0;
	}
	foreach my $line (@$dc_ref) {
		print NEW_ENV "$line\n";
	}
	close(NEW_ENV);

	&::LogMsg("Swapping $interim_name to $dest");
	
	if (File::Copy::move($interim_name,$dest)) {
		#Success
	} else {
		&::LogMsg("Unable to move $interim_name to $dest: $!");
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  SpliceNewLines
# Desc: Splice product, action and miscellaneous lines into an env_info.cfg
# Args: ref to destination config lines, ref to new product lines, ref to new action lines, ref to new misc lines
# Rets: 1
#########################
sub SpliceNewLines {
	my ($dc_ref,$new_products,$new_actions,$new_misc)=@_;
	
	#Splice new product lines on the first non-product line after a product line
	if (scalar(@$new_products)!=0) {
		my $seen_products=0;
		my $prod_index=-1;
		for(my $i=0;$i<scalar(@$dc_ref);$i++) {
			if ($dc_ref->[$i]=~/^PRODUCT_/i) {
				$seen_products=1;
			} else {
				if ($seen_products==1) {
					$prod_index=$i;
					last;
				}
			}
		}
		if ($prod_index==-1) {
			#Unable to find the product section, just put the lines at the end
			push(@$dc_ref,@$new_products);
		} else {
			splice(@$dc_ref,$prod_index,0,@$new_products);
		}
	}
	
	#Splice new misc lines at the last line prior to the comment preceding the ACTION line
	if (scalar(@$new_misc)!=0) {
		my $seen_action=0;
		my $last_non_comment=0;
		for(my $i=0;$i<scalar(@$dc_ref);$i++) {
			if ($dc_ref->[$i]=~/^ACTION/i) {
				$seen_action=1;
				last;
			} else {
				if ($dc_ref->[$i]!~/^#/) {
					$last_non_comment=$i;
				}
			}
		}
		if ($seen_action!=1) {
			#Unable to find the action section, just put the lines at the end
			push(@$dc_ref,@$new_misc);
		} else {
			splice(@$dc_ref,$last_non_comment+1,0,@$new_misc,'');
		}
	}
		
	if (scalar(@$new_actions)!=0) {
		#New actions always go at the end
		push(@$dc_ref,@$new_actions);
	}
	
	return 1;
}

#########################
# Sub:  CheckValidMerge
# Desc: Verify that the merged file does not contain combinations we won't accept
# Args: ref to destination config lines
# Rets: 1
#########################
sub CheckValidMerge {
	my ($dc_ref)=@_;
	
	my %keys=();
	foreach my $line (@$dc_ref) {
		next if ($line=~/^#/ || $line=~/^\s*$/);
		my ($key,$value)=&GetKeyValue($line);
		$key=~tr/a-z/A-Z/;
		$keys{$key}=$value;
	}
	
	#Now check for invalid combinations
	
	#RAFTEST and RMS cannot be in the same RETAIL_HOME
	if ($keys{'PRODUCT_RMS'}=~/^y$/i && $keys{'PRODUCT_RAFTEST'}=~/^y$/i) {
		&::LogMsg("ERROR: RAFTEST and RMS cannot be merged into the same RETAIL_HOME");
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  GetKeyValue
# Desc: Convert a line of key = value into key,value pair
# Args: line
# Rets: key,value
#########################
sub GetKeyValue {
	my ($line)=@_;
	
	my ($key,$value)=split(/\s*\=\s*/,$line);
	$key=~s/^\s*//;
	$value=~s/\s*$//;
	#Remove trailing comments
	$value=~s/\#.*$//;
	
	return ($key,$value);
}

#########################
# Sub:  ValidateConfig
# Desc: parse our command-line arguments and validate our configuration
#########################
sub ValidateConfig {
	while(my $arg=shift(@ARGV)) {
		$arg=~tr/a-z/A-Z/;
		if ($arg eq '-D') {
			$::DEST_FILE=shift(@ARGV);
		} elsif ($arg eq '-S') {
			$::SOURCE_FILE=shift(@ARGV);
		} else {
			&::LogMsg("Unknown argument: $arg");
			&Usage();
		}
	}

	if ($::SOURCE_FILE eq '') {
		&::LogMsg("Required argument -s <source file> missing");
		&Usage();
	}

	unless (-r $::SOURCE_FILE ) {
		&::LogMsg("Source file: $::SOURCE_FILE does not exist");
		&::Usage();
	}
	
	if ($::DEST_FILE eq '') {
		&::LogMsg("Required argument -d <dest file> missing");
		&Usage();
	}
	
	unless (-w $::DEST_FILE ) {
		&::LogMsg("Destination file: $::DEST_FILE does not exist or is not writable");
		&Usage();
	}
}

sub Usage {
	&::LogMsg("Usage:");
	&::LogMsg("orcfgupd -s <source file> -d <dest file>");
	exit 1;
}
