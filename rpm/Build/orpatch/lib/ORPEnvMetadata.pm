############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################

use strict;
package ORPEnvMetadata;

$ORPEnvMetadata::ACTIONCONFIG_TYPE='CONFIG';

use File::Copy;
use Cwd;

#########################
# Sub:  Export
# Desc: Collect all the manifest files and dbmanifest contents and put them in a single directory
# Args: action_list_ref - Ref to the full list of action handles
#       dest_dir - the destination dir (RETAIL_HOME)
#       base_support_dir - The path to the main support directory $RETAIL_HOME/support normally
#       support_dir - The path to the specific support dir we should compare with
#       global_state - handle to the ORP Global State object
#       create_zip - 1 to automatically create a zip file of the exported files
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::Export {
	my ($action_list_ref,$dest_dir,$base_support_dir,$support_dir,$global_state,$create_zip)=@_;
	
	foreach my $action (@$action_list_ref) {
		next unless ($action->IsValidOnThisHost()==1);

		if ($action->SetupMetadataEnvironment()!=1) {
			return 0;
		}
	}
	
	&::LogMsg("Exporting manifest and inventory files to $support_dir");
	
	unless (&_CleanExportDir($support_dir,$base_support_dir)==1) {
		&::LogMsg("Unable to clean export destination area");
		return 0;
	}
	
	&::CreateDirs('0750',$support_dir);
	
	my $metadata_map=new ORPMetadataMap($support_dir);
	
	my @manifest_list=&_GetManifestList($global_state,$dest_dir,$metadata_map);
	
	#include the inventory file in our copies
	my $inv=$global_state->GetPatchInventory();
	my $inv_file=$inv->GetInventoryFile();
	
	if (-f $inv_file) {
		push(@manifest_list,$inv_file);
		$metadata_map->RegisterInventory($inv_file);
	}
	
	#Copy all manifest files to the support dir
	foreach my $dest_manifest (@manifest_list) {
		if (&_CopyFileToExport($dest_manifest,$dest_dir,$support_dir)!=1) {
			return 0;
		}
	}

	my $count=scalar(@manifest_list);
	&::LogMsg("  $count files exported");
	
	if (&_CopyORPatchConfig($global_state,$dest_dir,$support_dir,\@manifest_list,$metadata_map,$action_list_ref)!=1) {
		&::LogMsg("Unable to save ORPatch configuration to export area");
		return 0;
	}
	
	my $new_count=scalar(@manifest_list)-$count;
	&::LogMsg("  $new_count files exported");
	$count=scalar(@manifest_list);
	
	if (&_ExportActionConfigs($action_list_ref,$support_dir,$metadata_map,$dest_dir,\@manifest_list)!=1) {
		return 0;
	}
	
	if (&_ExportGeneratedConfigs($action_list_ref,$support_dir,$metadata_map,$dest_dir,\@manifest_list)!=1) {
		return 0;
	}
	
	#Make sure our metadata map makes it into the zip
	if ($metadata_map->WriteFile($dest_dir)!=1) {
		&::LogMsg("Unable to save metadata map");
		return 0;
	}
	push(@manifest_list,$metadata_map->GetMapFileName());
	
	if ($create_zip==1) {
		if (&_ZipExportFiles(\@manifest_list,$support_dir,$base_support_dir)!=1) {
			return 0;
		}
	}

	return 1;
}

our $DETAIL_DIR='compare';
our $DETAIL_SUBDIR='details';
our $ANALYZE_DETAIL_DIR='analyze';
our $JARMANIFEST_INCLUDE_CONFIG=1;

#########################
# Sub:  Compare
# Desc: Compare two sets of metadata and print the differences
#       Note that this exports the current metadata before starting, so that all comparisons are done between consolidated metadata
# Args: action_list_ref - Ref to the full list of action handles
#       dest_dir - the destination dir (RETAIL_HOME)
#       base_support_dir - The path to the main support directory $RETAIL_HOME/support normally
#       support_dir - The path to the specific support dir we should compare with
#       export_env - The name of the environment that was exported
#       compare_detail_type - 1 to compare ENVMANIFEST details, 2 to compare DBMANIFEST details, 3 to compare JARMANIFEST details
#       global_state - handle to the ORP Global State object
#       src_name - The name of 'this' environment
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::Compare {
	my ($action_list_ref,$dest_dir,$base_support_dir,$support_dir,$export_env,$compare_detail_type,$global_state,$src_name)=@_;
	
	my $compare_dir=&::ConcatPathString($base_support_dir,'compare');
	
	if ($export_env==1) {
		#First export the current environment metadata
		&::LogMsg("Exporting existing environment metadata for comparison");
		if (&Export($action_list_ref,$dest_dir,$base_support_dir,$compare_dir,$global_state,0)!=1) {
			&::LogMsg("Failed extracting existing environment metadata");
			return 0;
		}
	} else {
		&::LogMsg("Skipping export of existing environment for comparison");
	}
	
	&DetailLog::CleanDetailLogDir($DETAIL_DIR,$DETAIL_SUBDIR);
	
	#Setup ExportedMetadata handles for the two environments
	my $other_name=&::GetRelativePathName($support_dir,$base_support_dir);
	my $other_metadata=new ORPExportedMetadata($support_dir,$other_name,$global_state);
	if ($other_metadata->Load()!=1) {
		&::LogMsg("Unable to load metadata in $support_dir");
		return 0;
	}
	
	my $this_metadata=new ORPExportedMetadata($compare_dir,$src_name,$global_state);
	if ($this_metadata->Load()!=1) {
		&::LogMsg("Unable to load metadata in $compare_dir");
		return 0;
	}
	
	#Now compare the directories
	if (&_CompareEnvMetadata($this_metadata,$other_metadata,$compare_detail_type)!=1) {
		return 0;
	}
	
	my $detail_dir=&DetailLog::GetDetailLogDir($ORPEnvMetadata::DETAIL_DIR,$ORPEnvMetadata::DETAIL_SUBDIR);
	&::LogMsg("Comparison details saved to $detail_dir");
	return 1;
}

#########################
# Sub:  ListExportedInventory
# Desc: list the inventory file from an exported metadata directory
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::ListExportedInventory {
	my ($dest_dir,$support_dir,$global_state,$short_support_name,$show_obsolete)=@_;
	
	if (!-d $support_dir) {
		&::LogMsg("Specified export directory $support_dir does not exist");
		return 0;
	}
	
	my $other_metadata=new ORPExportedMetadata($support_dir,$short_support_name,$global_state);
	if ($other_metadata->Load()!=1) {
		&::LogMsg("Unable to load metadata in $support_dir");
		return 0;
	}
	
	my $this_inv_file=$other_metadata->GetInventoryHandle();

	&::LogMsg("Printing inventory for $support_dir");
	
	return $this_inv_file->PrintInventory(1,$show_obsolete);
}

#########################
# Sub:  PrintAnalyzeResults
# Desc: Print the results of a patch analysis
# Args: update_file_href - ref to a hash mapping action names to the list returned from CheckPatchForAction
#       patch_info_ref - handle to the Patch Info object
#       analyze_details - handle to the ORPAnalyzeDetails object
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::PrintAnalyzeResults {
	my ($updated_file_href,$patch_info_ref,$analyze_details)=@_;
	
	&DetailLog::CleanDetailLogDir($ANALYZE_DETAIL_DIR,$DETAIL_SUBDIR);
	
	my $custom_details_file='custom_files_impacted';
	my $conflict_details_file='potential_conflict_files';
	my $module_details_file='module_details';
	my $detail_file_ext='csv';
	
	my @detail_output=();
	push(@detail_output,"#Action Name,Activity,Source File,Revision,Env File,Env Revision,Env Patch,Customized");
	
	my $total_updated=0;
	my $total_deleted=0;
	my $total_new=0;
	
	my @custom_list=();
	my @conflict_list=();
	foreach my $name (sort(keys(%$updated_file_href))) {
		my @files=@{$updated_file_href->{$name}};
		
		if (scalar(@files)==0) {
			&::LogMsg("No files to update for $name");
		} else {
			my ($new,$updated,$deleted,$custom,$conflict)=&_PrintActionDetails(\@files,$name,\@custom_list,\@conflict_list,\@detail_output,$patch_info_ref);
			$total_new+=$new;
			$total_updated+=$updated;
			$total_deleted+=$deleted;
		}
		
		if (&_PrintExtraAnalyzeDetails($analyze_details,$name)!=1) {
			return 0;
		}
	}
	
	my $customized=scalar(@custom_list);
	if ($customized!=0) {
		&::LogMsg("--------------- Begin custom file impact list ------------");
		foreach my $name (@custom_list) {
			&::LogMsg($name);
		}
		&::LogMsg("---------------  End  custom file impact list ------------");
	}
	
	my $conflict=scalar(@conflict_list);
	$conflict-- if $conflict!=0;  #Don't count the header if there were conflicts
	if ($conflict!=0) {
		&::LogMsg("---------------   POTENTIAL CONFLICTS FOUND   ------------");
		&::LogMsg("Some existing files in the environment are newer than files included in patch");
		&::LogMsg("These files will NOT be updated by the patch, and so may conflict with the patch");
		&::LogMsg("It is advisable to check with support to verify compatibility");
		&::LogMsg("See ${conflict_details_file}.$detail_file_ext for specific inforomation");
		&::LogMsg("---------------   POTENTIAL CONFLICTS FOUND   ------------");
	}
	
	#Print a summary
	&::LogMsg("--------------- Begin Overall Patch Analysis Summary ------------");
	&::LogMsg(sprintf("%10s : %5d files to create","NEW",$total_new));
	&::LogMsg(sprintf("%10s : %5d files to update","UPDATE",$total_updated));
	&::LogMsg(sprintf("%10s : %5d files to delete","DELETE",$total_deleted));
	if ($conflict!=0) {
		&::LogMsg(sprintf("%10s : %5d files may conflict","CONFLICT",$conflict));
		&DetailLog::CreateLog($ANALYZE_DETAIL_DIR,$DETAIL_SUBDIR,$conflict_details_file,\@conflict_list,$detail_file_ext);
	}
	if ($customized!=0) {
		&::LogMsg(sprintf("%10s : %5d files are impacted (included above)","CUSTOM",$customized));
		&DetailLog::CreateLog($ANALYZE_DETAIL_DIR,$DETAIL_SUBDIR,$custom_details_file,\@custom_list,$detail_file_ext);
	}
	&::LogMsg("---------------  End  Overall Patch Analysis Summary ------------");
	
	&DetailLog::CreateLog($ANALYZE_DETAIL_DIR,$DETAIL_SUBDIR,$module_details_file,\@detail_output,$detail_file_ext);	
	
	my $detail_dir=&DetailLog::GetDetailLogDir($ANALYZE_DETAIL_DIR,$DETAIL_SUBDIR);
	if ($customized!=0) {
		&::LogMsg("Customized impact details saved to $detail_dir/${custom_details_file}.${detail_file_ext}");
	}
	if ($conflict!=0) {
		&::LogMsg("Possible conflict details saved to $detail_dir/${conflict_details_file}.${detail_file_ext}");
	}
	&::LogMsg("Module analysis details saved to $detail_dir/${module_details_file}.${detail_file_ext}");
	
	return 1;
}

#########################
# Sub:  SetupAnalyzeDetailArea
# Desc: Setup an area where actions can put detail files for later patch analysis/comparison
# Args: analyze_detail_sref - Ref to the scalar that will receive the handle to the analyze detail object
#       base_support_dir - The path to the main support directory $RETAIL_HOME/support normally
#       support_dir - The path to the specific support dir we should compare with
#       extra_details_always - 1 if we should always calculate extra details
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::SetupAnalyzeDetailArea {
	my ($analyze_detail_sref,$base_support_dir,$support_dir,$global_state,$extra_details_always)=@_;
	
	unless (&_CleanExportDir($support_dir,$base_support_dir)==1) {
		&::LogMsg("Unable to clean analyze detail destination area");
		return 0;
	}
	
	&::CreateDirs('0750',$support_dir);
	
	my $current_support_dir=&::ConcatPathString($support_dir,'current');
	my $new_support_dir=&::ConcatPathString($support_dir,'incoming');
	&::CreateDirs('0750',$current_support_dir,$new_support_dir);
	
	$$analyze_detail_sref=new ORPAnalyzeDetails($global_state,$current_support_dir,$new_support_dir,$extra_details_always);
	
	return 1;
}

##########################################################################################################

#########################
# Sub:  _PrintActionDetails
# Desc: Print the summary and optionally details of files a particular action would update
# Ret:  new,updated,deleted,custom,conflict - counts of each type of update that would be undertaken
#########################
sub ORPEnvMetadata::_PrintActionDetails {
	my ($file_ref,$action_name,$custom_ref,$conflict_ref,$doutput_ref,$patch_info_ref)=@_;
	
	my ($deleted,$new,$updated,$custom,$conflict)=(0,0,0,0,0);
	
	foreach my $ref (@$file_ref) {
		my ($fname,$rev,$dfname,$di_ref,$source_patch_name)=@$ref;
		
		my $conflicting_file=0;
		my $env_patch='NA';
		my $env_rev='NA';
		my $env_custom='N';
		
		if ($di_ref ne '') {
			my ($dfile,$sfile,$ftype,$frevision,$fdesc,$fcustomized,@discard)=&Manifest::DecodeDestLine($di_ref);
			$env_patch=$fdesc;
			$env_rev=$frevision;
			if ($fcustomized=~/^y$/i) {
				#Customized file will be overwritten!
				push(@$custom_ref,$dfname);
				$env_custom='Y';
				$custom++;
			}
		}

		my @detail_info=();
		if ($rev eq 'DELETE') {
			@detail_info=($action_name,'DELETE','NA','NA',$fname);
			$deleted++;
		} else {
			my $activity='';
			if ($di_ref eq '') {
				$activity='NEW';
				$new++;
			} else {
				#Determine whether the environment file is newer than the patch rev, as there might be conflict
				my $rev_compare=&PatchFileCopy::RevisionCompare($rev,$env_rev);
				if ($rev_compare>0) {
					#Normal update
					$activity='UPDATE';
					$updated++;
				} else {
					#Incoming file is lower revision than the environment
					if ($patch_info_ref->IsPatchCummulative()) {
						#Cumulative patch will rollback the file, call it out but don't make a deal of it
						$activity='ROLLBACK';
						$updated++;
					} else {
						#Incremental patches are more likely to have problems with conflicts
						$activity='POTENTIAL_CONFLICT';
						$conflict++;
						if (scalar(@$conflict_ref)==0) {
							push(@$conflict_ref,"#Action Name,Activity,Source File,Revision,Source Patch,Env File,Env Revision,Env Patch,Customized");
						}
						push(@$conflict_ref,join(',',$action_name,$activity,$fname,$rev,$source_patch_name,$dfname,$env_rev,$env_patch,$env_custom));
					}
				}
			}
			@detail_info=($action_name,$activity,$fname,$rev,$dfname);
		}
		
		push(@detail_info,$env_rev,$env_patch,$env_custom);
		
		push(@$doutput_ref,join(',',@detail_info));
	}
	
	&_LogAnalyzeBlock("$action_name analysis list",$new,$updated,$deleted,$conflict,$custom);

	return ($new,$updated,$deleted,$custom,$conflict);
}

#########################
# Sub:  _LogAnalyzeBlock
# Desc: LogMsg the analyze results
# Args: header_name - The text to print at the start/end of the ---- block
#       new,updated,deleted,conflict,custom - The numbers to print for each type of result
# Ret:  1
#########################
sub _LogAnalyzeBlock {
	my ($header_name,$new,$updated,$deleted,$conflict,$custom)=@_;
	
	&::LogMsg("--------------- Begin $header_name ------------");
	&::LogMsg(sprintf("%10s : %5d files to create","NEW",$new)) if ($new!=0);
	&::LogMsg(sprintf("%10s : %5d files to update","UPDATE",$updated)) if ($updated!=0);
	&::LogMsg(sprintf("%10s : %5d files to delete","DELETE",$deleted)) if ($deleted!=0);
	&::LogMsg(sprintf("%10s : %5d files may conflict","CONFLICT",$conflict)) if ($conflict!=0);
	&::LogMsg(sprintf("%10s : %5d files are impacted (included above)","CUSTOM",$custom)) if ($custom!=0);
	&::LogMsg("---------------  End  $header_name ------------");
	return 1;
}

#########################
# Sub:  _PrintExtraAnalyzeDetails
# Desc: Print the additional analyze details that some actions provide
# Args: analyze_detail - Handle to an ORPAnalyzeDetails object
#       name - Name of the action we are processing
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_PrintExtraAnalyzeDetails {
	my ($analyze_details,$action_name)=@_;
	
	my $lower_name=$action_name;
	$lower_name=~tr/A-Z/a-z/;
	my $ear_compare_name="${lower_name}_archive_compare_details";
	my $custom_details_file="${lower_name}_archive_custom_impacts";
	my $detail_file_ext='csv';
	
	my @files=$analyze_details->GetArchiveCustomImpacts($action_name);
	my $custom=scalar(@files);
	
	if ($custom!=0) {
		&::LogMsg("---------------   POSSIBLE JAVA CUSTOM IMPACTS FOUND   ------------");
		&::LogMsg("$custom Java custom files have been found which may require updates to integrate with");
		&::LogMsg("this patch.  Detailed analysis should be done to ensure customizations are compatible");
		&::LogMsg("See: ");
		&::LogMsg("  ${custom_details_file}.$detail_file_ext for a list of files");
		&::LogMsg("  ${ear_compare_name}.$detail_file_ext for full comparison details");
		&::LogMsg("---------------   POSSIBLE JAVA CUSTOM IMPACTS FOUND   ------------");
		&DetailLog::CreateLog($ANALYZE_DETAIL_DIR,$DETAIL_SUBDIR,$custom_details_file,\@files,$detail_file_ext);
	}
	
	foreach my $compare_type ($analyze_details->GetDetailFileType($action_name)) {
		if (&_CompareAnalyzeDetailFiles($action_name,$compare_type,$analyze_details,$ear_compare_name,$detail_file_ext,$custom)!=1) {
			return 0;
		}
	}
	
	return 1;
}

#########################
# Sub:  _CompareAnalyzeDetailFiles
# Desc: Compare detailed files to assist a customer in analyzing the impacts of a patch
# Args: action_name - The name of the action we are processing
#       compare_type - The type of detail files to compare
#       analyze_detail - Handle to the analyze detail object
#       ear_compare_name - Name of the file to write ear comparison details to, without extension
#       detail_file_ext - The extension to put on the ear compare detail files
#       custom - The count of files to list as custom impacts
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_CompareAnalyzeDetailFiles {
	my ($action_name,$compare_type,$analyze_detail,$ear_compare_name,$detail_file_ext,$custom)=@_;

	local $JARMANIFEST_INCLUDE_CONFIG=0;
	
	my $current_metadata=$analyze_detail->GetCurrentExportedMetadata();
	my $new_metadata=$analyze_detail->GetNewExportedMetadata();
	
	my $pure_custom=$analyze_detail->GetPureCustomCount($action_name);

	if (&_AnalyzeJavaArchiveDetails($current_metadata,$new_metadata,$compare_type,$action_name,$ANALYZE_DETAIL_DIR,$DETAIL_SUBDIR,$ear_compare_name,$custom,$pure_custom)!=1) {
		return 0;
	}
	
	my $detail_dir=&DetailLog::GetDetailLogDir($ANALYZE_DETAIL_DIR,$DETAIL_SUBDIR);
	&::LogMsg("Full archive content comparison details saved to ${detail_dir}/${ear_compare_name}.csv");
	
	return 1;
}

#########################
# Sub:  _GetManifestList
# Desc: Get a list of all env_manifest.csv and deleted_env_manifest.csv files
# Ret:  list of manifest files that exist on this host
#########################
sub ORPEnvMetadata::_GetManifestList {
	my ($global_state,$dest_dir,$metadata_map)=@_;
	
	my $em_list_ref=$global_state->GetEnvManifestList();
	my $dm_list_ref=$global_state->GetDelManifestList();
	
	my @manifest_list=();
	foreach my $info_ref ([$metadata_map->GetEnvManifestType(),$em_list_ref],[$metadata_map->GetDelEnvManifestType(),$dm_list_ref]) {
		my ($type,$list_ref)=@$info_ref;
		#Add all the manifest files to the metadata map with the appropriate type
		#and add to list of manifest files
		foreach my $ref (@$list_ref) {
			my ($name,$full_dest,$m)=@$ref;
			
			$metadata_map->RegisterFile($name,$type,$m);
			push(@manifest_list,$m);
		}
	}
	
	return @manifest_list;
}

#########################
# Sub:  _ExportGeneratedConfigs
# Desc: Export any generated action configurations (dbmanifest exports, jar manifests, etc)
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_ExportGeneratedConfigs {
	my ($action_list_ref,$support_dir,$metadata_map,$dest_dir,$manifest_list_ref)=@_;

	&::LogMsg("Exporting generated configuration information");
	
	my $manifest_dir=&::ConcatPathString($support_dir,'generated');
	&::CreateDirs('0750',$manifest_dir);
	
	my $orig_count=scalar(@$manifest_list_ref);
	
	foreach my $action (@$action_list_ref) {
		next unless ($action->IsValidOnThisHost()==1);
	
		my $name=$action->GetActionName();
		$name=~tr/A-Z/a-z/;
		
		my $gen_dir=&::ConcatPathString($manifest_dir,$name);
		
		if ($action->ExportGeneratedConfig($gen_dir,$metadata_map,$manifest_list_ref,$support_dir)!=1) {
			&::LogMsg("Unable to export generated configuration for action $name");
			return 0;
		}
	}
	
	my $count=scalar(@$manifest_list_ref)-$orig_count;
	&::LogMsg("  $count files exported");
	
	return 1;
}

#########################
# Sub:  _ExportActionConfigs
# Desc: Export all of the config files for an action
# Ret:  proceed - 1 on success, 0 on failure
#       manifest_list_ref - ref to the list of manifests exported
#########################
sub ORPEnvMetadata::_ExportActionConfigs {
	my ($action_list_ref,$support_dir,$metadata_map,$dest_dir,$manifest_list_ref)=@_;
	
	&::LogMsg("Exporting action-specific config information");
	
	my $count=0;
	
	foreach my $action (@$action_list_ref) {
		next unless ($action->IsValidOnThisHost()==1);
	
		my $name=$action->GetActionName();
		
		my @config_list=();
		if ($action->GetActionConfigList(\@config_list)!=1) {
			&::LogMsg("Unable to list config files for action $name");
			return 0;
		}
		
		#Copy all config files to the support dir
		foreach my $config_file (@config_list) {
			if (&_CopyFileToExport($config_file,$dest_dir,$support_dir)!=1) {
				return 0;
			}
			$count++;
			$metadata_map->RegisterFile($name,$ORPEnvMetadata::ACTIONCONFIG_TYPE,$config_file);
		}
	}
	
	&::LogMsg("  $count files exported");
	
	return 1;
}

#########################
# Sub:  _ZipExportFiles
# Desc: Zip up all of the exported metadata files
# Ret:  1 on success, 0 on failure
#       note that we are not guarenteed to create a zip, if the zip binary can't be found
#########################
sub ORPEnvMetadata::_ZipExportFiles {
	my ($manifest_list_ref,$support_dir,$base_support_dir)=@_;

	if (&::CheckRequiredBinary('zip','')!=1) {
		&::LogMsg("WARNING: Unable to locate zip, not creating zip of exported metadata");
		return 1;
	}
	
	if (scalar(@$manifest_list_ref)==0) {
		&::LogMsg("No metadata files found to zip");
		return 1;
	}
	
	#Change into the directory
	my $original_cwd=cwd();
	unless (chdir($base_support_dir)) {
		&::LogMsg("Failed to chdir to $base_support_dir: $!");
		return 0;
	}

	my $support_name=&::GetRelativePathName($support_dir,$base_support_dir);
	my $zip_name="${support_name}.zip";
	my $full_zip_name="$base_support_dir/$zip_name";
	
	&::LogMsg("Creating zip of $support_dir");
	
	if (-f $full_zip_name) {
		&::RemoveFile($full_zip_name);
	}
	
	#create the zip
	my @output=`zip -r $zip_name $support_name`;
	my $status=$?;
	chdir($original_cwd);
	
	if ($status!=0 || !-f $full_zip_name) {
		&::DebugOutput('zip command',@output);
		&::LogMsg("Zip command failed!");
		return 0;
	}
	
	&::LogMsg("Zip file $full_zip_name created.");
	
	return 1;
}

#########################
# Sub:  _CompareEnvMetadata
# Desc: Compare two environment extracts
# Args: this_metadata - handle to the extracted metadata object of 'this' environment
#       other_metadata - handle to the extracted metadata object of the 'other' environment
#       compare_detail_type - 1 to compare ENVMANIFEST files, 2 to compare DBMANIFEST files, 3 to compare JARMANIFEST files
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_CompareEnvMetadata {
	my ($this_metadata,$other_metadata,$compare_detail_type)=@_;
	
	#Comparing metadata has four parts
	
	#1 compare product lists to identify mismatched products
	my @common_products=();
	if (&_CompareProductList($this_metadata,$other_metadata,\@common_products)!=1) {
		return 0;
	}
	
	#2 Compare the patch inventories for common products
	if (&_ComparePatchInventory($this_metadata,$other_metadata,\@common_products)!=1) {
		return 0;
	}
	
	#3 compare enabled action lists to identify mismatched component installs (i.e. db+batch versus db-only)
	my @common_actions=();
	if (&_CompareActionList($this_metadata,$other_metadata,\@common_actions)!=1) {
		return 0;
	}
	
	#4 compare module details for common actions
	if (&_CompareModuleDetails($this_metadata,$other_metadata,$compare_detail_type,\@common_actions,$DETAIL_DIR,$DETAIL_SUBDIR)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  _CompareModuleDetails
# Desc: Compare the module-level details of two environment extracts
# Args: this metadata - handle to the exported metadata object for 'this' environment
#       other metadata - handle to the exported metadata object for the 'other' environment
#       compare_detail_type - 1 to compare ENVMANIFEST files, 2 to compare DBMANIFEST files, 3 to compare JARMANIFEST files
#       common_action_ref - ref to list of common actions
#       detail_dir - directory name within detail_logs to place files
#       detail_subdir - subdirectory within detail_logs/detail_dir to place files
#       detail_logname - name to call the output report, default 'module_differences'
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_CompareModuleDetails {
	my ($this_metadata,$other_metadata,$compare_detail_type,$common_action_ref,$detail_dir,$detail_subdir,$detail_logname)=@_;
	
	$detail_logname='module_differences' if $detail_logname eq '';
	
	my ($total_this_only,$total_differ,$total_other_only)=(0,0,0);
	my ($one_side,$differ);
	
	my @detail_output=();
	
	my $this_export_dir=$this_metadata->GetExportDir();
	my $other_export_dir=$other_metadata->GetExportDir();
	
	my $this_name=$this_metadata->GetEnvironmentName();
	my $other_name=$other_metadata->GetEnvironmentName();
	
	my ($header,$begin_text,$detail_text,$compare_sub,$log_text,$metadata_type,$file_type);
	
	if ($compare_detail_type==1) {
		$file_type=$this_metadata->GetMetadataMapHandle()->GetEnvManifestType();
		$log_text='file';
		$header="#FileName,$this_name Revision,$this_name PatchName,$this_name Customized,$other_name Revision,$other_name PatchName,$other_name Customized";
		$compare_sub=\&ORPEnvMetadata::_CompareManifestFileLists;
		$begin_text='module';
		$detail_text='Modules';
	} elsif ($compare_detail_type==2) {
		$file_type=$this_metadata->GetMetadataMapHandle()->GetDBManifestType();
		$log_text='dbmanifest';
		$header="#DB FileName,$this_name Revision,$this_name PatchName,$this_name Applied Date,$other_name Revision,$other_name PatchName,$other_name Applied Date";
		$compare_sub=\&ORPEnvMetadata::_CompareDBManifestFileLists;
		$begin_text='DB module';
		$detail_text='DB Modules';
	} else {
		$file_type=$this_metadata->GetMetadataMapHandle()->GetJarManifestType();
		$log_text='jarmanifest';
		$header="#FileName,$this_name Revision,$this_name Customized,$other_name Revision,$other_name Customized";
		$compare_sub=\&ORPEnvMetadata::_CompareJarManifestFileLists;
		$begin_text='jar module';
		$detail_text='Jar Modules';
	}
	
	&::LogMsg("Comparing module-level $log_text details");
	push(@detail_output,$header);
	
	my $this_manifest_ref=$this_metadata->GetMetadataFileHash($common_action_ref,$file_type);
	my $other_manifest_ref=$other_metadata->GetMetadataFileHash($common_action_ref,$file_type);
	
	#Find items that are only on this environment, or are on both but differ in some way
	($one_side,$differ)=&$compare_sub($this_manifest_ref,$other_manifest_ref,$this_export_dir,$other_export_dir,0,\@detail_output);
	$total_this_only+=$one_side;
	$total_differ+=$differ;
	
	#Find items that are only on the other environment
	($one_side,$differ)=&$compare_sub($other_manifest_ref,$this_manifest_ref,$other_export_dir,$this_export_dir,1,\@detail_output);
	$total_other_only+=$one_side;

	my $middle_text='Different';
	
	#Calculate the number of spaces necessary to align the first/last lines with the middle line
	my $mid_shift=length($detail_text)-length($middle_text);
	my $top_shift=0;
	if ($mid_shift<0) {
		$top_shift=0-$mid_shift;
	}
	
	&::LogMsg("--------------- Begin $begin_text difference summary ------------");
	&::LogMsg(sprintf("$detail_text on $this_name but not $other_name:  %s%5d"," "x$top_shift,$total_this_only));
	&::LogMsg(sprintf("$middle_text on $this_name and $other_name:      %s%5d"," "x$mid_shift,$total_differ));
	&::LogMsg(sprintf("$detail_text on $other_name but not $this_name:  %s%5d"," "x$top_shift,$total_other_only));
	&::LogMsg("---------------  End  $begin_text difference summary ------------");	
	
	&DetailLog::CreateLog($detail_dir,$detail_subdir,$detail_logname,\@detail_output,'csv');
	
	return 1;
}

#########################
# Sub:  _AnalyzeJavaArchiveDetails
# Desc: Compare the impacts from a java ear file copy during an orpatch analyze
#       this is very similar to comparing module details, but reports the results the way analyze reports results
# Args: this metadata - handle to the exported metadata object for 'this' environment
#       other metadata - handle to the exported metadata object for the 'other' environment
#       action_name - name of the action we are processing
#       detail_dir - directory name within detail_logs to place files
#       detail_subdir - subdirectory within detail_logs/detail_dir to place files
#       detail_logname - name to call the output report
#       custom_impacts - Number of custom impacts to report
#       pure_custom_count - Count of purely custom files to subtract from the 'delete' count
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_AnalyzeJavaArchiveDetails {
	my ($this_metadata,$other_metadata,$compare_detail_type,$action_name,$detail_dir,$detail_subdir,$detail_logname,$custom_impacts,$pure_custom_count)=@_;
	
	my ($total_new,$total_update,$total_delete)=(0,0,0);
	my ($one_side,$differ);
	
	my @detail_output=();
	
	my $this_export_dir=$this_metadata->GetExportDir();
	my $other_export_dir=$other_metadata->GetExportDir();
	
	my $this_name=$this_metadata->GetEnvironmentName();
	my $other_name=$other_metadata->GetEnvironmentName();
	
	my $file_type=$this_metadata->GetMetadataMapHandle()->GetJarManifestType();
	my $header="#FileName,$this_name Revision,$this_name Customized,$other_name Revision,$other_name Customized";
	my $compare_sub=\&ORPEnvMetadata::_AnalyzeJarManifestFileLists;
	
	&::LogMsg("Comparing java ear module-level details");
	push(@detail_output,$header);
	
	my $this_manifest_ref=$this_metadata->GetMetadataFileHash([$action_name],$file_type);
	my $other_manifest_ref=$other_metadata->GetMetadataFileHash([$action_name],$file_type);
	
	#Find items that are only on this environment, or are on both but differ in some way
	($one_side,$differ)=&$compare_sub($this_manifest_ref,$other_manifest_ref,$this_export_dir,$other_export_dir,0,\@detail_output);
	$total_new+=$one_side;
	$total_update+=$differ;
	
	#Find items that are only on the other environment
	($one_side,$differ)=&$compare_sub($other_manifest_ref,$this_manifest_ref,$other_export_dir,$this_export_dir,1,\@detail_output);
	$total_delete+=$one_side;
	#Subtract off pure custom files, as they are not actually deleted or impacted
	$total_delete-=$pure_custom_count;
	$total_delete=0 if $total_delete<0;
	
	&_LogAnalyzeBlock("$action_name ear content analysis",$total_new,$total_update,$total_delete,0,$custom_impacts);
	
	&DetailLog::CreateLog($detail_dir,$detail_subdir,$detail_logname,\@detail_output,'csv');
	
	return 1;
}

#########################
# Sub:  _CompareDBManifestFileLists
# Desc: Compare a list of dbmanifest files
# Arg:  this_ref - ref to a list of dbmanifests for 'this' environment
#       other_ref - ref to a list of dbmanifests for the 'other' environment
#       this_dir - the directory path where 'this' environment's information is relative to
#       other_dir - the directoy path where 'other' environment's information is relative to
#       reversed    - 1 if this we are secretly reversing the sense of 'this environment' and 'other environment'
#       doutput_ref - ref to the detail output array
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_CompareDBManifestFileLists {
	my ($this_ref,$other_ref,$this_dir,$other_dir,$reversed,$doutput_ref)=@_;

	my $get_subdir_ref=sub {my ($rel_file)=@_;my ($dest_subdir,$dbm_file)=&::GetFileParts($rel_file,2);$dbm_file=~s/_dbmanifest\.csv//;return $dbm_file;};
	my $read_ref=\&_ReadDBManifest;
	my $decode_ref=\&ORPEnvMetadata::_DecodeDBManifestLine;
	
	#($this_dfile,$this_revision,$this_ftype,$this_applied,$this_patch)
	#Compare revision
	my $compare_ref=[1];
	#print revision, patch, applied date
	my $print_ref=[1,4,3];
	
	return &_CompareGenericFileLists($this_ref,$other_ref,$this_dir,$other_dir,$reversed,$doutput_ref,$get_subdir_ref,$read_ref,$decode_ref,$compare_ref,$print_ref);
}
	
#########################
# Sub:  _CompareManifestFileLists
# Desc: Compare a list of destination manifest files
# Arg:  this_ref - ref to a list of destination manifests for 'this' environment
#       other_ref - ref to a list of destination manifests for the 'other' environment
#       this_dir - the directory path where 'this' environment's information is relative to
#       other_dir - the directoy path where 'other' environment's information is relative to
#       reversed    - 1 if this we are secretly reversing the sense of 'this environment' and 'other environment'
#       doutput_ref - ref to the detail output array
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_CompareManifestFileLists {
	my ($this_ref,$other_ref,$this_dir,$other_dir,$reversed,$doutput_ref)=@_;
	
	my $get_subdir_ref=sub {my ($rel_file)=@_;my ($dest_subdir,$env_file)=&::GetFileParts($rel_file,2);return $dest_subdir;};
	my $read_ref=\&Manifest::ReadManifest;
	my $decode_ref=\&ORPEnvMetadata::_ManifestDecodeWrapper;

	#($this_dfile,$this_sfile,$this_ftype,$this_revision,$this_patch,$this_custom,...)
	#Compare revision and custom
	my $compare_ref=[3,5];
	#revision, patch, custom
	my $print_ref=[3,4,5];
	
	return &_CompareGenericFileLists($this_ref,$other_ref,$this_dir,$other_dir,$reversed,$doutput_ref,$get_subdir_ref,$read_ref,$decode_ref,$compare_ref,$print_ref);
}

#########################
# Sub:  _CompareJarManifestFileLists
# Desc: Compare a list of jar manifest files
# Arg:  this_ref - ref to a list of jar manifests for 'this' environment
#       other_ref - ref to a list of jar manifests for the 'other' environment
#       this_dir - the directory path where 'this' environment's information is relative to
#       other_dir - the directoy path where 'other' environment's information is relative to
#       reversed    - 1 if this we are secretly reversing the sense of 'this environment' and 'other environment'
#       doutput_ref - ref to the detail output array
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_CompareJarManifestFileLists {
	my ($this_ref,$other_ref,$this_dir,$other_dir,$reversed,$doutput_ref)=@_;
	
	#Convert generated/${action_name}/base/rpm-build/dist/rpm14.ear into ${action_name}/base/rpm-build/dist
	my $get_subdir_ref=sub {my ($rel_file)=@_;my ($dir,$file)=&::GetFileParts($rel_file,2);my @parts=&::SplitPathString($dir);shift(@parts);pop(@parts);$parts[0]=~tr/A-Z/a-z/;return &::ConcatPathString(@parts);};
	my $read_ref=\&ORPEnvMetadata::_ReadJarManifest;
	if ($JARMANIFEST_INCLUDE_CONFIG!=1) {
		$read_ref=\&ORPEnvMetadata::_ReadJarManifestNoConfig;
	}
	my $decode_ref=\&ORPEnvMetadata::_ManifestDecodeWrapper;
	
	#($this_dfile,$this_sfile,$this_ftype,$this_revision,$this_patch,$this_custom,$fwp_rev,$has_jar_manifest,...)
	#Compare revision, custom
	my $compare_ref=[3,5];
	#revision, custom
	my $print_ref=[3,5];
	
	return &_CompareGenericFileLists($this_ref,$other_ref,$this_dir,$other_dir,$reversed,$doutput_ref,$get_subdir_ref,$read_ref,$decode_ref,$compare_ref,$print_ref);
}

#########################
# Sub:  _AnalyzeJarManifestFileLists
# Desc: compare the differences between a list of jar manifest files for the purposes of reporting analyze extra details
# Arg:  this_ref - ref to a list of jar manifests for 'this' environment
#       other_ref - ref to a list of jar manifests for the 'other' environment
#       this_dir - the directory path where 'this' environment's information is relative to
#       other_dir - the directoy path where 'other' environment's information is relative to
#       reversed    - 1 if this we are secretly reversing the sense of 'this environment' and 'other environment'
#       doutput_ref - ref to the detail output array
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_AnalyzeJarManifestFileLists {
	my ($this_ref,$other_ref,$this_dir,$other_dir,$reversed,$doutput_ref)=@_;
	
	#Convert generated/${action_name}/base/rpm-build/dist/rpm14.ear into ''
	my $get_subdir_ref=sub {return ''};
	my $read_ref=\&ORPEnvMetadata::_ReadJarManifestNoConfig;
	my $decode_ref=\&ORPEnvMetadata::_ManifestDecodeWrapper;
	
	#($this_dfile,$this_sfile,$this_ftype,$this_revision,$this_patch,$this_custom,$fwp_rev,$has_jar_manifest,...)
	#Compare revision, custom
	my $compare_ref=[3,5];
	#revision, custom
	my $print_ref=[3,5];
	
	return &_CompareGenericFileLists($this_ref,$other_ref,$this_dir,$other_dir,$reversed,$doutput_ref,$get_subdir_ref,$read_ref,$decode_ref,$compare_ref,$print_ref);
}

#########################
# Sub:  _CompareGenericFileLists
# Desc: Compare a list of destination manifest files
# Arg:  this_ref - ref to a list of destination manifests for 'this' environment
#       other_ref - ref to a list of destination manifests for the 'other' environment
#       this_dir - the directory path where 'this' environment's information is relative to
#       other_dir - the directoy path where 'other' environment's information is relative to
#       reversed    - 1 if this we are secretly reversing the sense of 'this environment' and 'other environment'
#       doutput_ref - ref to the list of detail output
#       get_subdir_ref - ref to the function that will take a relative name and return the appropriate dest subdir
#       read_ref - ref to the function to read the manifest file
#       decode_ref - ref to the function to decode a manifest line
#       compare_ref - ref to the list of array indexes to compare in order to call a line in the manifests 'different'
#       print_ref - ref to the list of array indexes to print when a row differs
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_CompareGenericFileLists {
	my ($this_ref,$other_ref,$this_dir,$other_dir,$reversed,$doutput_ref,$get_subdir_ref,$read_ref,$decode_ref,$compare_ref,$print_ref)=@_;
	
	my $total_one_side=0;
	my $total_differ=0;
	
	foreach my $this_manifest_file (sort(keys(%$this_ref))) {
		#Get the path to the manifest file relative to the directory of metadata export
		my $rel_file=$this_ref->{$this_manifest_file};
		
		#Use that path to construct the path to the manifest in the other metadata export directory
		my $other_file=&::ConcatPathString($other_dir,$rel_file);
		
		my $dest_subdir=&$get_subdir_ref($rel_file);
		
		my $other_file_check=$other_ref->{$other_file};
		my @this_dest_info=&$read_ref($this_manifest_file);
		my @other_dest_info=();
		if ($other_file_check ne '') {
			@other_dest_info=&$read_ref($other_file);
		}
		
		#Convert the other dest info into a hash so filename lookups
		#don't have to search the entire array every time
		my %other_dest_hash=();
		foreach my $ref (@other_dest_info) {
			my ($dfile,@discard)=&$decode_ref($ref);
			$other_dest_hash{$dfile}=$ref;
		}
		
		my ($one_side,$differ)=&_CompareInfoGeneric(\@this_dest_info,\%other_dest_hash,$dest_subdir,$reversed,$decode_ref,$compare_ref,$print_ref,$doutput_ref);
		$total_one_side+=$one_side;
		$total_differ+=$differ;
	}
	
	return ($total_one_side,$total_differ);
}

#########################
# Sub:  _CompareInfoGeneric
# Desc: Compare info arrays and find the files that differ or exist on only one side
# Arg:  this_other_ref - ref to a list of destination info refs
#       other_info_href - ref to hash by destination file name with values the destination manifest ref
#       dest_subdir - The path relative to retail_home where the relative file names start
#       reversed    - 1 if this we are secretly reversing the sense of 'this environment' and 'other environment'
#       decode_sub  - ref to the decode subroutine for each row
#       compare_ref - ref to a list of row array indexes to compare
#       print_ref   - ref to a list of row array indexes to print
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_CompareInfoGeneric {
	my ($this_info_ref,$other_info_href,$dest_subdir,$reversed,$decode_sub,$compare_ref,$print_ref,$doutput_ref)=@_;
	
	#Accumulators for summary information
	my $one_side=0;
	my $differ=0;
	
	foreach my $ref (@$this_info_ref) {
		my @this_detail=&$decode_sub($ref);
		
		my $oref=$other_info_href->{$this_detail[0]};
		
		#Initialize the other info to all NA
		my @other_detail=();
		for(my $i=0;$i<scalar(@this_detail);$i++) {
			$other_detail[$i]='NA';
		}

		if ($oref ne '') {
			#other environment has this file
			if ($reversed==1) {
				#In a reversed comparison we don't check files that exist on both environments
				#because they were already printed during the un-reversed phase if they differed
				next;
			}
			@other_detail=&$decode_sub($oref);
		} else {
			#only one side has this file
			$one_side++;
		}
		
		#Check if all of the details at the compare indexes are the same, if not this entry differs
		my $different=0;
		foreach my $ind (@$compare_ref) {
			if ($other_detail[$ind] ne $this_detail[$ind]) {
				$different=1;
				last;
			}
		}
		
		if ($different==1) {
			#only count this as different if the other side did not have it
			$differ++ if ($oref ne '');
			
			#Present the relative file names from the destination manifest as being relative to RETAIL_HOME for easier use by administrators
			$this_detail[0]=&::ConcatPathString($dest_subdir,$this_detail[0]) if ($dest_subdir ne '');
			$other_detail[0]=&::ConcatPathString($dest_subdir,$other_detail[0]) if ($other_detail[0] ne 'NA' && $dest_subdir ne '');
			my @sidea=();
			my @sideb=();
			foreach my $ind (@$print_ref) {
				push(@sidea,$this_detail[$ind]);
				push(@sideb,$other_detail[$ind]);
			}
			
			#When we are doing reversed comparisons, make sure the 'this' and 'other' information end up in the same order
			#That is, make sure the overall 'this' information ends up first and the overall 'other' information ends up second
			#even if we passed them into this function opposite in order to find files that exist in the 'other' environment
			#that don't exist in this environment
			if ($reversed==1) {
				push(@$doutput_ref,join(',',$this_detail[0],@sideb,@sidea));
			} else {
				push(@$doutput_ref,join(',',$this_detail[0],@sidea,@sideb));
			}
		}
	}
	return ($one_side,$differ);
}

#########################
# Sub:  _ComparePatchInventory
# Desc: Compare this patch inventory with some other patch inventory
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_ComparePatchInventory {
	my ($this_metadata,$other_metadata,$common_prod_ref)=@_;
	
	&::LogMsg("Comparing patch inventories");
	
	my ($this_top_ref,$this_sub_ref)=$this_metadata->GetInventoryDetails();
	my ($other_top_ref,$other_sub_ref)=$other_metadata->GetInventoryDetails();

	my $inv_converter=$this_metadata->GetInventoryHandle();
	
	my $this_patch_ref=&_ConsolidatePatchList($this_top_ref,$this_sub_ref,$common_prod_ref,$inv_converter);
	my $other_patch_ref=&_ConsolidatePatchList($other_top_ref,$other_sub_ref,$common_prod_ref,$inv_converter);
	
	my $this_name=$this_metadata->GetEnvironmentName();
	my $other_name=$other_metadata->GetEnvironmentName();

	my @detail_output=();
	push(@detail_output,"#$this_name Patch,$this_name Src Merge Patch,$this_name Applied Date,$this_name Patch Type,$other_name Patch,$other_name Src Merge Patch,$other_name Applied Date,$other_name Patch Type");
	my $here_only=&_ComparePatchLists($this_patch_ref,$other_patch_ref,0,\@detail_output,$inv_converter);
	my $other_only=&_ComparePatchLists($other_patch_ref,$this_patch_ref,1,\@detail_output,$inv_converter);
	
	&::LogMsg("--------------- Begin patch difference summary ------------");
	&::LogMsg(sprintf("Patches on $this_name but not $other_name:    %5d",$here_only));
	&::LogMsg(sprintf("Patches on $other_name but not $this_name:    %5d",$other_only));
	&::LogMsg("---------------  End  patch difference summary ------------");	
	
	&DetailLog::CreateLog($DETAIL_DIR,$DETAIL_SUBDIR,'patch_differences',\@detail_output,'csv');
	
	return 1;
}

#########################
# Sub:  _CompareProductList
# Desc: Compare the product list of one environment to another and save the list of common products
# Ret:  1 on success, 0 on failure
#########################
sub _CompareProductList {
	my ($this_metadata,$other_metadata,$common_product_ref)=@_;
	return &_CompareGenericList($this_metadata,$other_metadata,$common_product_ref,'Products','GetProductList','product_details');
}

#########################
# Sub:  _CompareActionList
# Desc: Compare the action list of one environment to another and save the list of common actions
# Ret:  1 on success, 0 on failure
#########################
sub _CompareActionList {
	my ($this_metadata,$other_metadata,$common_action_ref)=@_;
	return &_CompareGenericList($this_metadata,$other_metadata,$common_action_ref,'Actions','GetActionList','action_details');
}

#########################
# Sub:  _CompareGenericList
# Desc: Compare a list and save the common elements, used for products and actions
# Ret:  1 on success, 0 on failure
#########################
sub _CompareGenericList {
	my ($this_metadata,$other_metadata,$common_product_ref,$list_name,$get_list_ref,$detail_name)=@_;
	
	&::LogMsg("Comparing list of ${list_name}");
	
	my $this_plist_ref=$this_metadata->$get_list_ref();
	my $other_plist_ref=$other_metadata->$get_list_ref();

	my $this_name=$this_metadata->GetEnvironmentName();
	my $other_name=$other_metadata->GetEnvironmentName();

	my @detail_output=();
	push(@detail_output,"#$this_name $list_name,$other_name $list_name");
	
	my @this_only=();
	foreach my $this_product (@$this_plist_ref) {
		if (grep(/^$this_product$/,@$other_plist_ref)!=0) {
			push(@$common_product_ref,$this_product);
		} else {
			push(@this_only,$this_product);
			push(@detail_output,"$this_product,NA");
		}
	}
	
	my @other_only=();
	foreach my $other_product (@$other_plist_ref) {
		if (grep(/^$other_product$/,@$this_plist_ref)!=0) {
			#common product already saved
		} else {
			push(@other_only,$other_product);
			push(@detail_output,"NA,$other_product");
		}
	}
	
	foreach my $p (@$common_product_ref) {
		push(@detail_output,"$p,$p");
	}
	
	my $this_count=scalar(@this_only);
	my $both_count=scalar(@$common_product_ref);
	my $other_count=scalar(@other_only);
	
	&::LogMsg("--------------- Begin $list_name difference summary ------------");
	&::LogMsg(sprintf("$list_name on $this_name but not $other_name:    %5d",$this_count));
	&::LogMsg(sprintf("$list_name on $other_name but not $this_name:    %5d",$other_count));
	&::LogMsg("---------------  End  $list_name difference summary ------------");	
	
	&DetailLog::CreateLog($DETAIL_DIR,$DETAIL_SUBDIR,$detail_name,\@detail_output,'csv');
	
	return 1;
}

#########################
# Sub:  _ComparePatchLists
# Desc: Compare two lists of patches and print the ones that exist in the first list but not in the second
# Args: first patch ref,second patch ref,header that is printed if any patches exist in first list but not second
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_ComparePatchLists {
	my ($this_patch_ref,$other_patch_ref,$reversed,$doutput_ref,$inv)=@_;
	
	my $total=0;
	
	#Look for patches only on this environment
	foreach my $patch (keys(%$this_patch_ref)) {
		my $this_ref=$this_patch_ref->{$patch};
		my $other_ref=$other_patch_ref->{$patch};
		
		my ($this_source,$this_applied,$this_type)=@$this_ref;
		$this_type=$inv->DecodePatchType($this_type);
		
		if ($other_ref eq '') {
			#Patch exists only on 'this' environment

			my @missing=('NA','NA','NA','NA');
			my @info=();
			
			#If this patch came as part of a merged patch, include the merged patch source of it
			if ($patch ne $this_source) {
				push(@info,$patch,$this_source,$this_applied,$this_type);
			} else {
				push(@info,$patch,'NA',$this_applied,$this_type);
			}
			
			if ($reversed==1) {
				push(@$doutput_ref,join(',',@missing,@info));
			} else {
				push(@$doutput_ref,join(',',@info,@missing));
			}
			$total++;
		}
	}

	return $total;
}

#########################
# Sub:  _ConsolidatePatchList
# Desc: Take a top-level and sub-patch list and squish it down to base-level patches
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_ConsolidatePatchList {
	my ($patch_ref,$sub_patch_ref,$cproduct_ref,$inv)=@_;
	
	my %patch_list=();
	foreach my $ref (@$patch_ref) {
		my ($patch_name,$applied_date,$patch_type,$products,$obsolete_date)=$inv->DecodeTopPatchInfo($ref);
		
		#Ignore patches which are obsoleted
		next unless ($obsolete_date eq '');
		
		my $common=0;
		foreach my $product (split(/\,/,$products)) {
			#Check if this patch was against at least one common product
			if (grep(/^$product$/,@$cproduct_ref)!=0) {
				$common=1;
				last;
			}
		}
		
		#Ignore this patch if it is not on a common product
		next unless ($common==1);
		
		my $sub_ref=$sub_patch_ref->{$patch_name};
		
		if ($sub_ref eq '') {
			#Not a merged patch, record it directly
			$patch_list{$patch_name}=[$patch_name,$applied_date,$patch_type];
		} else {
			#there are sub-patches, record them and where they came from
			foreach my $subpatch (@$sub_ref) {
				$patch_list{$subpatch}=[$patch_name,$applied_date,$patch_type];
			}
		}
	}
		
	return \%patch_list;
}


#########################
# Sub:  _CleanExportDir
# Desc: Clean any existing files in the export area
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_CleanExportDir {
	my ($exp_dir,$dest_dir)=@_;
	
	if (-f $exp_dir) {
		if (&::RemoveFile($exp_dir)==1) {
			return 1;
		} else {
			return 0;
		}
	} 
	
	if (!-d $exp_dir) {
		return 1;
	}

	&::LogMsg("Removing: $exp_dir");
	
	if ($exp_dir!~m!^$dest_dir/!) {
		&::LogMsg("$exp_dir must be below $dest_dir");
		return 0;
	}
	
	my @output=`rm -rf "$exp_dir"`;
	my $status=$?;
	
	if ($status!=0) {
		&::DebugOutput("rm output",@output);
		&::LogMsg("Failed to remove $exp_dir");
		return 0;
	}
	
	return 1;
}


#########################
# Sub:  _ReadDBManifest
# Desc: Read a dbmanifest csv file and split it into an in-memory structure
# Ret:  list of row refs that should be passed to _DecodeDBManifestLine
#########################
sub ORPEnvMetadata::_ReadDBManifest {
	my ($manifest)=@_;
	
	my @info=();

	#it is OK for the dbmanifest to not exist
	if (-f $manifest) {
		open(MANIFEST,"<$manifest") || die "Unable to open $manifest: $!";
		while(<MANIFEST>) {
			chomp;
			next if (/^#/);
			my @parts=split(/\,/);
			push(@info,\@parts);
		}
		close(MANIFEST);
	}

	return @info;
}

#########################
# Sub:  _DecodeDBManifestLine
# Desc: Take a ref to a DBManifest row and turn it into the pieces
# Ret:  relative file name,version,file type,patch name,applied date
#########################
sub ORPEnvMetadata::_DecodeDBManifestLine {
	my ($ref)=@_;
	my ($fn,$v,$ft,$ad,$pn)=@$ref;
	
	return ($fn,$v,$ft,$ad,$pn);
}

#########################
# Sub:  _CopyORPatchConfig
# Desc: copy all of the orpatch configuration files that we can find into the export area
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_CopyORPatchConfig {
	my ($global_state,$dest_dir,$support_dir,$file_list_ref,$metadata_map,$action_list)=@_;

	&::LogMsg("Exporting ORPatch configuration files");
	
	#Find the env_info.cfg file 
	my $cfg_ref=$global_state->GetConfig();
	my $cfg_file=$cfg_ref->GetLoadFile();

	#We simplify the ACTION_ settings during copy, so first make a map of whether actions are enabled or not
	my %action_map=();
	foreach my $action (@$action_list) {
		my $name=$action->GetActionName();
		my $enabled='N';
		if ($action->IsValidOnThisHost()==1) {
			$enabled='Y';
		}
		$action_map{"ACTION_${name}"}=$enabled;
	}
	
	#We don't use _CopyFileToExport so that we can try to make sure _CONNECT parameters don't contain passwords	
	my $rel_cfg=&_CreateRelSupportPath($cfg_file,$dest_dir,$support_dir);

	unless (open(ENV_INFO,"<$cfg_file")) {
		&::LogMsg("Unable to open $cfg_file: $!");
		return 0;
	}
	unless (open(SUPPORT_ENV_INFO,">$rel_cfg")) {
		&::LogMsg("Unable to open $rel_cfg: $!");
		return 0;
	}
	while(<ENV_INFO>) {
		chomp;
		my $line=$_;
		#There should not be passwords in connect strings because we use a wallet
		#but if there are (i.e. .+ matches between the / and the @ in the connect string), make sure they are not exported
		if ($line=~m!^DBSQL.*_CONNECT\s*=.*/.+\@.*!) {
			$line=~s!^(DBSQL.*\_CONNECT\s*=.*/)(.*)(\@.*)!${1}XXXXXXX${3}!;
		}
		if ($line=~/^(ACTION_.*)\s*=(.*)/) {
			#If the action's enablement is a hostname
			if ($2!~/^y|n$/i) {
				#Simplify actions, but save the original value as a commented line
				print SUPPORT_ENV_INFO "#EXPORT ORIG LINE - $line\n";
				$line="$1=$action_map{$1}";
			}
		}
		print SUPPORT_ENV_INFO "$line\n";
	}
	close(ENV_INFO);
	close(SUPPORT_ENV_INFO);
	
	chmod(0750,$rel_cfg);
	push(@$file_list_ref,$rel_cfg);
	
	$metadata_map->RegisterEnvInfo($cfg_file);

	#Include the custom hooks configuration
	my $hooks_cfg=$global_state->GetCustomHooks()->_GetHookConfigFile();
	if (-f $hooks_cfg) {
		if (&_CopyFileToExport($hooks_cfg,$dest_dir,$support_dir)!=1) {
			return 0;
		}
		push(@$file_list_ref,$hooks_cfg);
		$metadata_map->RegisterFile('CONFIG','CUSTOMHOOKS',$hooks_cfg);
	}
	
	#Try to get the profiles if they exist in the standard place
	my ($cfg_dir,$f)=&::GetFileParts($cfg_file,2);
	foreach my $file ('rmsbatch_profile','rwmsforms_profile','rmsforms_profile') {
		my $profile=&::ConcatPathString($cfg_dir,$file);
		if (-f $profile) {
			if (&_CopyFileToExport($profile,$dest_dir,$support_dir)!=1) {
				return 0;
			}
			push(@$file_list_ref,$profile);
			$metadata_map->RegisterFile('CONFIG','PROFILE',$profile);
		}
	}
	
	return 1;
}

#########################
# Sub:  _CopyFileToExport
# Desc: Copy a file into the export area.  
#       The path above the common source dir will be preseved within the support dir
# Args: source file, common source dir, support dir
# Ret:  1 on success, 0 on failure
#########################
sub ORPEnvMetadata::_CopyFileToExport {
	my ($src_file,$src_dir,$support_dir)=@_;
	
	my $rel_name=&_CreateRelSupportPath($src_file,$src_dir,$support_dir);
	
	if (&File::Copy::copy($src_file,$rel_name)) {
		#Success
		chmod(0750,$rel_name);
		return 1;
	} else {
		&::LogMsg("Unable to copy $src_file to $rel_name: $!");
		return 0;
	}
}

#########################
# Sub:  _CreateRelSupportPath
# Desc: Create the necessary directory structure within support dir to support a source file being exported to it
# Args: source file, common source dir, support dir
# Ret:  rel_name - full path to the file within the support directory
#########################
sub ORPEnvMetadata::_CreateRelSupportPath {
	my ($src_file,$src_dir,$support_dir)=@_;
	#Remove the RETAIL_HOME
	my $rel_name=&::GetRelativePathName($src_file,$src_dir);
	#Prepend the path to our support dir
	$rel_name=&::ConcatPathString($support_dir,$rel_name);
		
	my ($dir,$file)=&::GetFileParts($rel_name,2);
	#Create the directory structure under $RETAIL_HOME
	&::CreateDirs('0750',$dir);
	
	return $rel_name;
}

#########################
# Sub:  _ReadJarManifest
# Desc: Read a full extracted jar manifest and filter out the lines that shouldn't be used for comparison which include
#          Where has_jar_manifest=Y (because the subjarmanifest is already included)
# Args: file - The manifest file to read
# Ret:  @info - An array of refs to the rows in the manifest
#########################
sub ORPEnvMetadata::_ReadJarManifest {
	my ($file)=@_;
	return &ORPEnvMetadata::_ReadFilteredJarManifest($file,1);
}

#########################
# Sub:  _ReadJarManifestNoConfig
# Desc: Read a full extracted jar manifest and filter out the lines that shouldn't be used for comparison which include
#          Where has_jar_manifest=Y (because the subjarmanifest is already included)
#          Rows that appear to be configuration related
# Args: file - The manifest file to read
# Ret:  @info - An array of refs to the rows in the manifest
#########################
sub ORPEnvMetadata::_ReadJarManifestNoConfig {
	my ($file)=@_;
	return &ORPEnvMetadata::_ReadFilteredJarManifest($file,0);
}

#########################
# Sub:  _ReadFilteredJarManifest
# Desc: Read a full extracted jar manifest and filter out the lines that shouldn't be used for comparison which include
#         Where has_jar_manifest=Y (because the subjarmanifest is already included)
#         and optionally any rows that look like config rows
# Args: file - The manifest file to read
#       include_config - 1 to include _config rows, 0 to exclude
# Ret:  @info - An array of refs to the rows in the manifest
#########################
sub ORPEnvMetadata::_ReadFilteredJarManifest {
	my ($file,$include_config)=@_;
	
	my @output=();
	foreach my $ref (&Manifest::ReadManifest($file)) {
		my ($this_dfile,$this_sfile,$this_ftype,$this_revision,$this_patch,$this_custom,$fwp_rev,$has_jar_manifest,@discard)=&Manifest::DecodeDestLine($ref);
		next if ($has_jar_manifest=~/^Y/i);
		next if ($this_ftype=~/_config$/ && $include_config==0);
		push(@output,[$this_dfile,$this_sfile,$this_ftype,$this_revision,$this_patch,$this_custom,$fwp_rev,$has_jar_manifest,@discard]);
	}
	
	return @output;
}

#This wraps the normal Manifest::DecodeDestLine and ensures that the custom flag is always N if it is blank
sub ORPEnvMetadata::_ManifestDecodeWrapper {
	my ($ref)=@_;
	my ($this_dfile,$this_sfile,$this_ftype,$this_revision,$this_patch,$this_custom,$fwp_rev,$future1,$future2,$future3,$future4)=&Manifest::DecodeDestLine($ref);
	#Early environments don't always have a customized flag populated
	$this_custom='N' if $this_custom eq '';

	return ($this_dfile,$this_sfile,$this_ftype,$this_revision,$this_patch,$this_custom,$fwp_rev,$future1,$future2,$future3,$future4);
}


1;
