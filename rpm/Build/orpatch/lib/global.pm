############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################

&_InitVars();

#########################
# Sub:  DebugOutput
# Desc: Print rows of output without a date and between start/stop markers
#########################
sub DebugOutput {
	my ($header,@lines)=@_;
	
	&::LogMsg("------- Start $header output -------");
	$::LOG->LogMsgNoDate(@lines);
	&::LogMsg("-------  End  $header output -------");
}

#########################
# Sub:  DebugOutputLogOnly
# Desc: Run DebugOutput, but have output only go to the log file, not the screen
#########################
sub DebugOutputLogOnly {
	my ($header,@lines)=@_;
	
	my $old_verbose=$::LOG->GetVerbose();
	$::LOG->SetVerbose(0);
	&::DebugOutput($header,@lines);	
	$::LOG->SetVerbose($old_verbose);
}

#########################
# Sub:  LogMsg
# Desc: Log a message to our global log file
#########################
sub LogMsg {
	$::LOG->LogMsg(@_);
}

#########################
# Sub:  CreateDirs
# Desc: more-or-less a mkdir -p
#########################
sub CreateDirs {
    my ($mode,@dirs)=@_;

	#convert mode to octal for mkdir call
	$mode=oct($mode);

    my $created=0;
    foreach my $dir (@dirs) {
        my $dir_accum='';
		
		#If this is not a fully-qualified path, use ./ to refer to it
		$dir_accum='.' if ($dir!~/^\//);
		
        $created++;
        foreach my $d (split(/\//,$dir)) {
            next if $d eq '';
            $dir_accum.="/$d";
            if (!-d $dir_accum) {
                unless (mkdir($dir_accum,$mode)==1) {
                    &::LogMsg("Unable to create directory $dir_accum: $!");
                    $created--;
                    last;
                }
            }
        }
    }

    return $created;
}

########################
# Sub:  CleanDir
# Desc: Clean up a directory
# Args: directory - path to the directory to clean
#       mode - mode to recreate the directory or '' to not recreate it
#       silent - 1 if we should not talk about cleaning the directory
# Ret:  1 on success, 0 on failure
########################
sub CleanDir {
	my ($clean_dir,$mode,$silent)=@_;
	
	if (-d $clean_dir) {
		&::LogMsg("Cleaning directory: $clean_dir") unless ($silent==1);
		my @output=`rm -rf "$clean_dir" 2>&1`;
		if ($?!=0) {
			&::LogMsg("Failed removing $clean_dir");
			&::DebugOutput("rm -rf $clean_dir",@output);
			return 0;
		}
	}
	
	if ($mode ne '') {
		&::CreateDirs($mode,$clean_dir);
	}
	return 1;
}

########################
# Sub:  CleanString
# Desc: Take a string and convert code page 1252 characters into normal characters, also strip quotes
# Args: string
# Ret:  a string with characters converted and quotes stripped
########################
sub CleanString {
	my ($string)=@_;
	
	$string =~ s/\x82/,/g;
    $string =~ s/\x83/f/g;
    $string =~ s/\x84/,,/g;
    $string =~ s/\x85/.../g;

    $string =~ s/\x88/^/g;
    $string =~ s/\x89/°\/°°/g;

    $string =~ s/\x8B/</g;
    $string =~ s/\x8C/Oe/g;

    $string =~ s/\x91/`/g;
    $string =~ s/\x92/'/g;
    $string =~ s/\x93/"/g;
    $string =~ s/\x94/"/g;
    $string =~ s/\x95/*/g;
    $string =~ s/\x96/-/g;
    $string =~ s/\x97/--/g;
    $string =~ s/\x98/~/g;
    $string =~ s/\x99/TM/g;

    $string =~ s/\x9B/>/g;
    $string =~ s/\x9C/oe/g;
	
	$string =~ s/"//g;
	$string =~ s/'//g;
	
	return $string;
}

#########################
# Sub:  GetFileParts
# Desc: Split a path/file into 2 (dir and file name) or three parts (dir, file name, extension)
#########################
sub GetFileParts {
    my ($name,$parts)=@_;
	$parts=2 if $parts eq '';

    my ($file,$dir)=&File::Basename::fileparse($name);
	$dir=~s/\/$//;  #Remove trailing / from the end of the dir
    return ($dir,$file) if $parts==2;
	
	my @suffix=split(/\./,$file);
    my $ext=pop(@suffix);

    return ($dir,join('.',@suffix),$ext);
}

#########################
# Sub:  ConcatPathString
# Desc: Join some number of path elements with / in between
#########################
sub ConcatPathString {
    my (@parts)=@_;
    return join('/',@parts);
}

#########################
# Sub:  SplitPathString
# Desc: Split a path into a list of directories/files
#       If the path is fully qualified (starts with /), you must remember to add this back after manipulating the path
#       This means &::ConcatPathString(&::SplitPathString('/steve/test')) ne '/steve/test'
#########################
sub SplitPathString {
    my ($dir_string)=@_;
	$dir_string=~s/^[\/\\]//;
    return split(/[\/\\]/,$dir_string);
}

#########################
# Sub:  GetRelativePathName
# Desc: Convert a path that contains a directory to a path relative to that directory
#       for example, convert /u00/oracle/rms/dbsql_rms/env_manifest.csv to dbsql_rms/env_manifest.csv
# Args: Full path string  - full path to convert
#       base directory    - base directory to return the full path string as relative to
# Ret:  The relative file name
#########################
sub GetRelativePathName {
	my ($full_name,$relative_dir)=@_;
	
	$full_name=~s!$relative_dir/!!;
	
	return $full_name;
}

#########################
# Sub:  GetFilesInDir
# Desc: Get a list of files in a specific directory that meet particular criteria
# Args: base_dir - base directory to search below
#       output_ref - ref to a list that will be filled with the files
#       recursive - 1 to recurse into subdirectories, 0 to not
#       include_pats - ref to a list of patterns of file names to include
#       exclude_pats - ref to a list of patterns of file names to exclude
#       include_dirs - 1 if directories should be included in the list
# Ret:  1 on success, 0 on failure
#########################
sub GetFilesInDir {
	my ($base_dir,$output_ref,$recursive,$include_pats,$exclude_pats,$include_dirs)=@_;
	
	if (!-d $base_dir) {
		&::LogMsg("GetFilesInDir base directory $base_dir does not exist!");
		return 0;
	}
	
	my @search_dirs=($base_dir);
	while(my $dir=shift(@search_dirs)) {
		unless(opendir(SEARCH_DIR,$dir)) {
			&::LogMsg("Unable to open $dir: $!");
			return 0;
		}
		while(my $this_file=readdir(SEARCH_DIR)) {
			next if ($this_file eq '.' || $this_file eq '..');
			
			my $full_file=&::ConcatPathString($dir,$this_file);
			if (-d $full_file) {
				if ($recursive==1) {
					push(@search_dirs,$full_file);
				}
				if ($include_dirs==1) {
					push(@$output_ref,$full_file);
				}
				next;
			}

			if ($include_pats ne '' && scalar(@$include_pats)!=0) {
				my $include=0;
				foreach my $pat (@$include_pats) {
					if ($this_file=~/$pat/) {
						$include=1;
						last;
					}
				}
				
				next unless $include==1;
			}
			
			if ($exclude_pats ne '' && scalar(@$exclude_pats)!=0) {
				my $exclude=0;
				foreach my $pat (@$exclude_pats) {
					if ($this_file=~/$pat/) {
						$exclude=1;
						last;
					}
				}
				next if $exclude==1;
			}
			
			push(@$output_ref,$full_file);
		}
		closedir(SEARCH_DIR);
	}

	return 1;
}

#########################
# Sub:  TimeStampFile
# Desc: Take a file name and insert a date-time string in the file name
#########################
sub TimeStampFile {
    my ($file_name,$num_days)=@_;

    $num_days=7 if $num_days eq '';

    my $ts=&GetTimeStamp('DATE-TIME');

    my ($dir,$file)=&GetFileParts($file_name,2);

    #Consider everything after the first . an extension
    my @parts=split(/\./,$file);
    my $base_file=shift(@parts);
    my $ext=join('.',@parts);

	&CreateDirs('0755',$dir) if (!-d $dir);

	#Clean up old log files based on $num_days
    opendir(DIR,"$dir") || die "Unable to open directory $dir: $!";
    while(my $this_file=readdir(DIR)) {
        my $full_file=&::ConcatPathString($dir,$this_file);

        next unless -f $full_file;

        if ($this_file=~/^${base_file}\-/) {
            if (-M $full_file>=$num_days) {
                unlink($full_file);
            }
        }
    }
    closedir(DIR);

    return &::ConcatPathString($dir,"${base_file}-${ts}.$ext");
}

#########################
# Sub:  GetTimeStamp
# Desc: Format localtime into a time string of either DATE-TIME or TIME_DATE format
#########################
sub GetTimeStamp {
	my ($ts_format)=@_;
	$ts_format='DATE-TIME' if $ts_format eq '';
	
	my @lt_array=localtime;
	#Extract the pieces we want from the localtime array
	my @pieces=@lt_array[4,3,5,2,1,0];
    $pieces[0]++;
    $pieces[2]+=1900;
	
	my $format='';
	if ($ts_format eq 'DATE-TIME') {
		return sprintf('%02d%02d%04d-%02d%02d%02d',@pieces[0,1,2,3,4,5]);
	} elsif ($ts_format eq 'TIME_DATE') {
		return sprintf('%02d:%02d:%02d %02d/%02d/%04d',@pieces[3,4,5,0,1,2]);
	} elsif ($ts_format eq 'DATE_TIME') {
		return sprintf('%02d/%02d/%04d %02d:%02d:%02d',@pieces[0,1,2,3,4,5]);
	}
}

#########################
# Sub:  RemoveFile
# Desc: Remove files
#########################
sub RemoveFile {
	my @files=@_;
	my $files_removed=0;
	foreach my $f (@files) {
		&::LogMsg("Removing: $f");
		if (unlink($f)!=1) {
			&::LogMsg("Failed to remove $f: $!");
		} else {
			$files_removed++;
		}
	}
	return $files_removed;
}

#########################
# Sub:  GetTempName
# Desc: generate a temp file name to use
# Args: extension of the temp file
# Ret:  the fully-qualified file name to use as a temporary file
#########################
sub GetTempName {
	my ($ext)=@_;
	$ext='tmp' if ($ext eq '');
	
	my $dir=$ENV{OR_TEMP_DIR};
	$dir='/tmp/Oratmp' if $dir eq '';

    &::CreateDirs('0700',$dir);
    my $num=$::TEMP_FILE_INCREMENTER++;
	my $file="$dir/${$}_${num}.$ext";

	unlink($file) if (-e $file);

    return $file;
}

###############
# Sub:  CheckRequiredBinary
# Desc: Check if a given binary exists somewhere in the path and is executable
# Args: binary name to check for,optional profile to run before trying to find executable
###############
sub CheckRequiredBinary {
	my ($exe_name,$source_profile)=@_;

	my $full_cmd_string="which $exe_name 2>&1";
	
	if ($source_profile ne '' && -f $source_profile) {
		$full_cmd_string=". $source_profile ; $full_cmd_string";
	}
	
	my @output=`$full_cmd_string`;
	my $exe_path=$output[0];
	chomp($exe_path);
	
	if (-x $exe_path) {
		&::LogMsg("Using $exe_name: $exe_path");
		return 1;
	} else {
		&::DebugOutput("'which $exe_name'",@output);
		&::LogMsg("ERROR: Unable to locate required executable '$exe_name'");
		return 0;
	}
}

#Initialize our Log Variable
sub _InitVars {
	require File::Basename;
	require "$ENV{RETAIL_HOME}/orpatch/lib/Log.pm";
	$::LOG=new Log('',0,"$ENV{RETAIL_HOME}/orpatch/logs",1);	
	$::TEMP_FILE_INCREMENTER=0;
}

1;
