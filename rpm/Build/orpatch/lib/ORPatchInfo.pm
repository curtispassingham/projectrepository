############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPatchInfo;

sub GetPatchName {
	my $self=shift;
	
	return $self->_GetVar('PATCH_NAME');
}

sub IsMergedPatch {
	my $self=shift;
	
	return $self->_GetVar('MERGED_PATCH');
}

sub GetPatchList {
	my $self=shift;
	
	my $ref=$self->{'PATCH_LIST'};
	
	return @$ref;
}

sub GetPatchType {
	my $self=shift;
	
	my $patch_type=$self->_GetRawPatchType();

	return ($patch_type eq &GetCummulativePatchType()) ? 'Cummulative' : 'Incremental';
}



sub IsPatchCummulative {
	my $self=shift;
	my $patch_type=$self->_GetRawPatchType();
	return ($patch_type eq &GetCummulativePatchType());
}

sub IsPatchIncremental {
	my $self=shift;
	my $patch_type=$self->_GetRawPatchType();
	return ($patch_type eq &GetIncrementalPatchType());
}

sub GetCummulativePatchType {
	return 'C';
}

sub GetIncrementalPatchType {
	return 'I';
}

sub GetProductList {
	my $self=shift;
	my $pname=$self->_GetVar('PRODUCT_NAME');
	return split(/\,/,$pname);
}

sub GetProductName {
	my $self=shift;
	return $self->_GetVar('PRODUCT_NAME');
}

sub IsProductInPatch {
	my $self=shift;
	my ($product)=@_;
	
	return (grep(/\Q$product\E/,$self->GetProductList())>0);
}

sub RegisterWithInventory {
	my $self=shift;
	my ($patch_inventory,$patch_name_ref)=@_;
	
	my $pname=$self->GetPatchName();
	my $patch_type=$self->_GetRawPatchType();
	my $products=$self->GetProductName();
	my @sub_patches_only=grep(!/^$pname$/,$self->GetPatchList());
		
	unless ($patch_inventory->RegisterPatch($pname,$patch_type,$products,\@sub_patches_only,$patch_name_ref)==1) {
		return 0;
	}
	return 1;
}
	
#########################
# Sub:  ReadPatchInfo
# Desc: Read the patch_info.cfg file
# Args: None
# Ret:  1 if successful, 0 if not
#########################
sub ReadPatchInfo {
	my $self=shift;
	
	my $cfg_file=$self->GetPatchInfoFile();
	
	if (!-f $cfg_file) {
		&::LogMsg("Patch Info file $cfg_file does not exist!");
		return 0;
	}
	
	unless ($self->{'PATCH_INFO'}->LoadFile($cfg_file)==1) {
		return 0;
	}
	
	#Parse the patch info into things we need
	
	my $patch_name=$self->_GetVar('PATCH_NAME');
	if ($patch_name eq '') {
		&::LogMsg("Unable to determine patch name, invalid patch?");
		return 0;
	}
	
	my @patch_names=();
	if ($self->{'PATCH_INFO'}->ValidateTFVar('MERGED_PATCH',0)==1) {
		@patch_names=$self->_FindListVars('SUBPATCH_NAME');
	} else {
		push(@patch_names,$patch_name);
	}
	$self->{'PATCH_LIST'}=\@patch_names;
	
	&::LogMsg(sprintf("Patch $patch_name for product %s (%s)",
						$self->_GetVar('PRODUCT_NAME'),
						$self->GetPatchType()));
	
	return 1;
}


#########################
# Sub:  DummyPatchInfo
# Desc: Create a dummy patch info structure
# Args: patch name,patch_type,product_name
# Ret:  1
#########################
sub DummyPatchInfo {
	my $self=shift;
	my ($patch_name,$patch_type,$product_name)=@_;
	
	$self->_SetVar('PATCH_NAME',$patch_name);
	$self->_SetVar('MERGED_PATCH',0);
	$self->_SetVar('PATCH_TYPE',$patch_type);
	$self->_SetVar('PRODUCT_NAME',$product_name);
	$self->{'PATCH_LIST'}=[$patch_name];
	
	return 1;
}

#########################
# Sub:  GetPatchHooks
# Desc: Get a list of custom hook-style scripts that are defined in the patch info
# Args: None
# Ret:  list of [hook_name,hook_script],[hook_name,hook_script]
#########################
sub GetPatchHooks {
	my $self=shift;
	
	my @hooks=();
	foreach my $key ($self->{'PATCH_INFO'}->Keys()) {
		$key=~tr/a-z/A-Z/;
		if ($key=~/^HOOK_/) {
			my $script=$self->_GetVar($key);
			$key=~s/^HOOK_//;
			if ($script ne '') {
				&::LogMsg("Adding required patch hook $key : $script");
				push(@hooks,[$key,$script]);
			}
		}
	}

	return @hooks;
}
	

#####################################################

#########################
# Sub:  FindListVars
# Desc: Find a sequence of numbered configuration variables
# Args: key_prefix - the name prefix to search
# Rets: an array of values for all prefixed variable names
#########################
sub _FindListVars {
	my $self=shift;
	my ($key_prefix)=@_;
	
	my @keys=();
	my $num=1;
	while(1) {
		my $val=$self->_GetVar("${key_prefix}_$num");
		if ($val eq '') {
			last;
		}
		push(@keys,$val);
		$num++;
	}
	
	return @keys;
}


sub _GetVar {
	my $self=shift;
	my ($name)=@_;
	return $self->{'PATCH_INFO'}->Get($name);
}

sub _SetVar {
	my $self=shift;
	my ($name,$value)=@_;
	return $self->{'PATCH_INFO'}->Set($name,$value);
}

sub GetPatchInfoFile {
	my $self=shift;
	return $self->{'PATCH_INFO_CFG'};
}

sub _GetRawPatchType {
	my $self=shift;
	return $self->_GetVar('PATCH_TYPE');
}

#####################################################################
# Sub:  new
# Desc: Initialize the custom hook configuration
# Args: config_file - The name of the custom hook config file
#####################################################################
sub new {
    my $class=shift;
	my ($config_file)=@_;
    my $self={};
	
	$self->{'PATCH_INFO_CFG'}=$config_file;
	$self->{'PATCH_INFO'}=new ConfigFile();
	
    bless($self,$class);	
    return $self;
}

1;
