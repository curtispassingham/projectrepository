############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

&::RequireORPatchModules();

sub RequireORPatchModules {
    #Common libraries
    require "$::ORP_LIB_DIR/global_ora.pm";
    require "$::ORP_LIB_DIR/Manifest.pm";
    require "$::ORP_LIB_DIR/JarFile.pm";
    require "$::ORP_LIB_DIR/JarManifest.pm";
    require "$::ORP_LIB_DIR/ConfigFile.pm";
    require "$::ORP_LIB_DIR/DetailLog.pm";
    require "$::ORP_LIB_DIR/Parallel.pm";
    require "$::ORP_LIB_DIR/FileCopyMethod.pm";
    require "$::ORP_LIB_DIR/FileCopyMethodSubst.pm";
    require "$::ORP_LIB_DIR/FileCopyParams.pm";
    require "$::ORP_LIB_DIR/PatchFileCopy.pm";
    require "$::ORP_LIB_DIR/ORPRestartState.pm";
    require "$::ORP_LIB_DIR/ORPatchInventory.pm";
    require "$::ORP_LIB_DIR/ORPCustomHook.pm";
    require "$::ORP_LIB_DIR/ORPatchInfo.pm";
    require "$::ORP_LIB_DIR/ORPatchGlobalState.pm";
    require "$::ORP_LIB_DIR/ORManifestOrder.pm";
    require "$::ORP_LIB_DIR/ORPExportedMetadata.pm";
    require "$::ORP_LIB_DIR/ORPMetadataMap.pm";
    require "$::ORP_LIB_DIR/ORPRestorePoint.pm";
    require "$::ORP_LIB_DIR/ORPEnvMetadata.pm";
    require "$::ORP_LIB_DIR/ORPAnalyzeDetails.pm";

    #Base actions
    require "$::ORP_LIB_DIR/ORPAction.pm";
    require "$::ORP_LIB_DIR/ORPAction/DBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/OraForms.pm";
    require "$::ORP_LIB_DIR/ORPAction/ORPatchUpdate.pm";
    require "$::ORP_LIB_DIR/ORPAction/ORPDeleteAction.pm";
    require "$::ORP_LIB_DIR/ORPAction/JavaApp.pm";

    #RMS Actions
    require "$::ORP_LIB_DIR/ORPAction/RMSBatch.pm";
    require "$::ORP_LIB_DIR/ORPAction/RMSForms.pm";
    require "$::ORP_LIB_DIR/ORPAction/RMSDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RETLScripts.pm";
    require "$::ORP_LIB_DIR/ORPAction/RMSDCScripts.pm";
    require "$::ORP_LIB_DIR/ORPAction/ReIMDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/AllocDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/AllocRMSDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RMSBDIINTDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RMSBDIINFRDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RMSDemoDataDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RMSDASDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RAFDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RMSJavaApp.pm";

    #RWMS Actions
    require "$::ORP_LIB_DIR/ORPAction/RWMSDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RWMSForms.pm";
    require "$::ORP_LIB_DIR/ORPAction/RWMSADFDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RWMSUserDBSQL.pm";

    #RPM Actions
    require "$::ORP_LIB_DIR/ORPAction/RPMJavaApp.pm";

    #ReIM Actions
    require "$::ORP_LIB_DIR/ORPAction/ReIMJavaApp.pm";

    #Alloc Actions
    require "$::ORP_LIB_DIR/ORPAction/AllocJavaApp.pm";

    #Resa Actions
    require "$::ORP_LIB_DIR/ORPAction/ResaJavaApp.pm";

    #RASRM Actions
    require "$::ORP_LIB_DIR/ORPAction/RASRMJavaApp.pm";

    #RA Actions
    require "$::ORP_LIB_DIR/ORPAction/RARMSBatchDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RADMDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RAFEDMDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RABatchDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RACoreBatch.pm";
    

    #RDE Actions
    require "$::ORP_LIB_DIR/ORPAction/RDERMSBatchDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RDEDMDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RDEBatchDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RDECoreBatch.pm";
    
    #RASE Actions
    require "$::ORP_LIB_DIR/ORPAction/SIABaseDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASECoreDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASEASODBSQL.pm";
	require "$::ORP_LIB_DIR/ORPAction/RASERLDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASECDTDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASEDTDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASEAEDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASEMBADBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASECISDBSQL.pm";
    require "$::ORP_LIB_DIR/ORPAction/SIABaseBatch.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASECoreBatch.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASEASOBatch.pm";
	require "$::ORP_LIB_DIR/ORPAction/RASERLBatch.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASECDTBatch.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASEDTBatch.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASEAEBatch.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASEMBABatch.pm";
    require "$::ORP_LIB_DIR/ORPAction/RASECISBatch.pm";


    #RAFTest Actions
    require "$::ORP_LIB_DIR/ORPAction/RAFTestDBSQL.pm";

    #RFM Actions
    require "$::ORP_LIB_DIR/ORPAction/RFMDBSQL.pm";
    
    #EDGEUI Actions
    require "$::ORP_LIB_DIR/ORPAction/EDGEUIDBSQL.pm";

    return 1;
}

sub PopulateActions {
    my ($action_ref,$config_ref)=@_;


    #ORPatch Action
    my $orpatch_update=new ORPAction::ORPatchUpdate($config_ref);
    my $name=$orpatch_update->GetActionName();
    push(@$action_ref,$orpatch_update);
    push(@$action_ref,new ORPAction::ORPDeleteAction($config_ref));

    #RMS Actions
    push(@$action_ref,new ORPAction::RMSBDIINTDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RMSBDIINFRDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RAFDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RMSDBSQL($config_ref));
    push(@$action_ref,new ORPAction::ReIMDBSQL($config_ref));
    push(@$action_ref,new ORPAction::AllocRMSDBSQL($config_ref));
    push(@$action_ref,new ORPAction::AllocDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RMSDemoDataDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RMSDASDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RMSBatch($config_ref));
    push(@$action_ref,new ORPAction::RMSForms($config_ref));
    push(@$action_ref,new ORPAction::RETLScripts($config_ref));
    push(@$action_ref,new ORPAction::RMSDCScripts($config_ref));
    push(@$action_ref,new ORPAction::RMSJavaApp($config_ref));

    #RWMS Actions
    push(@$action_ref,new ORPAction::RWMSDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RWMSADFDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RWMSUserDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RWMSForms($config_ref));

    #RPM Actions
    push(@$action_ref,new ORPAction::RPMJavaApp($config_ref));

    #ReIM Actions
    push(@$action_ref,new ORPAction::ReIMJavaApp($config_ref));

    #Alloc Actions
    push(@$action_ref,new ORPAction::AllocJavaApp($config_ref));

    #Resa Actions
    push(@$action_ref,new ORPAction::ResaJavaApp($config_ref));

    #RASRM Actions
    push(@$action_ref,new ORPAction::RASRMJavaApp($config_ref));

    #RA Actions
    push(@$action_ref,new ORPAction::RARMSBatchDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RADMDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RAFEDMDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RABatchDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RACOREBATCH($config_ref));
    

    #RDE Actions
    push(@$action_ref,new ORPAction::RDERMSBatchDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RDEDMDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RDEBatchDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RDECOREBATCH($config_ref));

    #RASE Actions
    push(@$action_ref,new ORPAction::RASECoreDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RASEASODBSQL($config_ref));
	push(@$action_ref,new ORPAction::RASERLDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RASECDTDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RASECISDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RASEDTDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RASEAEDBSQL($config_ref));
    push(@$action_ref,new ORPAction::RASEMBADBSQL($config_ref));
    push(@$action_ref,new ORPAction::RASECoreBatch($config_ref));
    push(@$action_ref,new ORPAction::RASEASOBatch($config_ref));
	push(@$action_ref,new ORPAction::RASERLBatch($config_ref));
    push(@$action_ref,new ORPAction::RASECDTBatch($config_ref));
    push(@$action_ref,new ORPAction::RASECISBatch($config_ref));
    push(@$action_ref,new ORPAction::RASEDTBatch($config_ref));
    push(@$action_ref,new ORPAction::RASEAEBatch($config_ref));
    push(@$action_ref,new ORPAction::RASEMBABatch($config_ref));

    #RAFTest Actions
    push(@$action_ref,new ORPAction::RAFTestDBSQL($config_ref));

    #RFM Actions
    push(@$action_ref,new ORPAction::RFMDBSQL($config_ref));
    
    #EDGEUI Actions
    push(@$action_ref,new ORPAction::EDGEUIDBSQL($config_ref));

    return $name;
}

1;
