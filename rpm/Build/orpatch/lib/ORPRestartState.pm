############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPRestartState;

sub GetCurrentAction {
	my $self=shift;
	return $self->{'CURRENT_ACTION'};
}

sub GetCurrentPhase {
	my $self=shift;
	return $self->{'CURRENT_PHASE'};
}

sub SetCurrentAction {
	my $self=shift;
	my ($action_name)=@_;
	$self->{'CURRENT_ACTION'}=$action_name;
}

sub SetCurrentPhase {
	my $self=shift;
	my ($phase_num)=@_;
	$self->{'CURRENT_PHASE'}=$phase_num;
}

sub SetCurrentRestoreDir {
	my $self=shift;
	my ($restore_dir)=@_;
	$self->{'CURRENT_RESTORE_DIR'}=$restore_dir;
}

sub SetCurrentFreshInstall {
	my $self=shift;
	my ($fresh_install)=@_;
	$self->{'CURRENT_FRESH_INSTALL'}=$fresh_install;
}

sub GetActionInfo {
	my $self=shift;
	return $self->{'CURRENT_ACTION_INFO'};
}

sub GetCurrentPatchName {
	my $self=shift;
	return $self->{'CURRENT_PATCH_NAME'};
}

sub SetCurrentPatchName {
	my $self=shift;
	my ($patch_name)=@_;
	$self->{'CURRENT_PATCH_NAME'}=$patch_name;
}

sub GetCurrentRestoreDir {
	my $self=shift;
	return $self->{'CURRENT_RESTORE_DIR'};
}

sub GetCurrentFreshInstall {
	my $self=shift;
	return $self->{'CURRENT_FRESH_INSTALL'};
}

sub GetPatchMadeChangesFlag {
	my $self=shift;
	return $self->{'CURRENT_PATCH_MADE_CHANGES'};
}

sub SetPatchMadeChangesFlag {
	my $self=shift;
	my ($made_changes)=@_;
	$self->{'CURRENT_PATCH_MADE_CHANGES'}=$made_changes;
	return 1;
}

sub GetCopyPatchPhaseNum {
	my $self=shift;
	return $self->{'COPY_PATCH_PHASE'};
}

#########################
# Sub:  GetCrossPhaseActionInfo
# Desc: Get an action's cross-phase action information
# Args: action_name - The name of the action's cross phase action info to retrieve
# Ret:  list of cross-phase action info
#########################
sub GetCrossPhaseActionInfo {
	my $self=shift;
	my ($action_name)=@_;
	my $cphref=$self->_GetCrossPhaseAIHRef();
	my $cpai_ref=$cphref->{$action_name};
	
	my @cpai_data=();
	if ($cpai_ref ne '') {
		@cpai_data=(@$cpai_ref);
	}
	
	return @cpai_data;
}

#########################
# Sub:  SetAndSaveActionInfo
# Desc: Save some action specific restart information
# Args: action_info - A string that makes sense to an action which wants to be able to restart
#                     in the middle of a action (i.e. when loading DB files, the file type to restart with
# Ret:  1 on success, 0 on failure
#
# Note: This action specific info gets erased when either SetAndSaveRestartPoint or IncrementPhase is called
#########################
sub SetAndSaveActionInfo {
	my $self=shift;
	my ($action_info)=@_;
	$self->_SetActionInfo($action_info);
	return $self->SaveRestartPoint();
}
	
#########################
# Sub:  SetAndSaveCrossPhaseActionInfo
# Desc: Save some cross-phase action specific restart information
# Args: action_name - The name of the action that owns this information
#       action_info_ref - A ref to a list of data that the action wants to save across orpatch phases and restarts
# Ret:  1 on success, 0 on failure
#
# Note: This information is preserved across phase changes, where as action info is reset at each phase/new action
#########################
sub SetAndSaveCrossPhaseActionInfo {
	my $self=shift;
	my ($action_name,$action_info_ref)=@_;
	$self->SetCrossPhaseActionInfo($action_name,$action_info_ref);
	return $self->SaveRestartPoint();
}

#########################
# Sub:  SetCrossPhaseActionInfo
# Desc: Save some cross-phase action specific restart information.  Note this does NOT write the restart file
#       and is only safe if you immediately follow this with something that saves the restart state
# Args: action_name - The name of the action that owns this information
#       action_info_ref - A ref to a list of data that the action wants to save across orpatch phases and restarts
# Ret:  1 on success, 0 on failure
#########################
sub SetCrossPhaseActionInfo {
	my $self=shift;
	my ($action_name,$action_info_ref)=@_;
	my $cphref=$self->_GetCrossPhaseAIHRef();
	$cphref->{$action_name}=[@$action_info_ref];
}

#########################
# Sub:  GetORPMethodForPhase
# Desc: Get the ORPAction method to call for a particular phase
# Args: phase_num - Phase number to lookup
# Ret:  method_name,phase name if successful, '','' if the phase num is not known
#########################
sub GetORPMethodForPhase {
	my $self=shift;
	my ($phase_num)=@_;

	foreach my $ref (@{$self->{'PHASE_MAP'}}) {
		my ($num,$name,$method)=@$ref;
		if ($phase_num==$num) {
			return ($method,$name);
		}
	}
	return ('','');
}

sub IncrementPhase {
	my $self=shift;
	my $phase_num=$self->GetCurrentPhase();
	my $next_phase_num=$phase_num+$self->{'RESTART_INCREMENT'};
	$self->SetCurrentPhase($next_phase_num);
	$self->SetCurrentAction('');
	$self->_SetActionInfo('');
	return $next_phase_num;
}

sub RemoveStateFile {
	my $self=shift;
	
	my $restart_file=$self->GetRestartFile();
	if (-f $restart_file) {
		&::RemoveFile($restart_file);
	}
	return 1;
}

#########################
# Sub:  SetAndSaveRestartPoint
# Desc: update our restart point and then save the state file
#########################
sub SetAndSaveRestartPoint {
	my $self=shift;
	my ($action_name,$action_phase)=@_;
	
	my $cur_action_name=$self->GetCurrentAction();
	$self->SetCurrentAction($action_name);
	$self->SetCurrentPhase($action_phase);
	
	#Don't reset our action info if we are saving the same action
	if ($cur_action_name ne $action_name) {
		$self->_SetActionInfo('');
	}
	
	return $self->SaveRestartPoint();
}
	
#########################
# Sub:  SaveRestartPoint
# Desc: Save our restart state to the file
#########################
sub SaveRestartPoint {
	my $self=shift;
	
	my $restart_file=$self->GetRestartFile();
	
	my $action_name=$self->GetCurrentAction();
	my $action_phase=$self->GetCurrentPhase();
	my $action_info=$self->GetActionInfo();
	my $patch_name=$self->GetCurrentPatchName();
	my $restore_dir=$self->GetCurrentRestoreDir();
	my $patch_changes=$self->GetPatchMadeChangesFlag();
	my $fresh_install=$self->GetCurrentFreshInstall();
	
	open(RESTART_FILE,">$restart_file") || die "Unable to save $restart_file: $!";
	my $main_string=<<ENDSTRING;
#This file is automatically generated by orpatch.pl to track the
#current action and phase.  This allows us to restart at specific points
#in the event of a problem applying a patch
#Do not modify this file directly, unless instructed by Oracle support
CURRENT_ACTION=$action_name
CURRENT_PHASE=$action_phase
CURRENT_ACTION_INFO=$action_info
CURRENT_PATCH_NAME=$patch_name
CURRENT_RESTORE_DIR=$restore_dir
CURRENT_MADE_CHANGES=$patch_changes
CURRENT_FRESH_INSTALL=$fresh_install
ENDSTRING

	unless(print RESTART_FILE $main_string) {
		die "Unable to write to $restart_file: $!";
	}

	my $cphref=$self->_GetCrossPhaseAIHRef();
	foreach my $action (keys(%{$cphref})) {
		my $cpai_ref=$cphref->{$action};
		if ($cpai_ref ne '') {
			foreach my $data (@$cpai_ref) {
				my $name=$self->_MakeCrossPhaseAIName($action);
				unless(print RESTART_FILE "$name=$data\n") {
					die "Unable to write to $restart_file: $!";
				}
			}
		}
	}

	close(RESTART_FILE);
	
	return $action_phase;
}

#########################
# Sub:  LoadRestartPoint
# Desc: Load our restart point from the restart state file
#########################
sub LoadRestartPoint {
	my $self=shift;
	my ($this_patch_name)=@_;

	$self->SetCurrentPhase($self->{'RESTART_INCREMENT'});
	$self->_SetActionInfo('');
	$self->SetCurrentPatchName($this_patch_name);
	$self->SetCurrentRestoreDir('');
	$self->SetPatchMadeChangesFlag(0);
	$self->SetCurrentFreshInstall(0);
	$self->_ResetCrossPhaseAI();
	
	my $restart_file=$self->GetRestartFile();
	return 1 unless (-f $restart_file);

	&::LogMsg("Attempting to load restart state file: $restart_file");
	
	my $made_changes='';
	my $fresh_install='';
	open(RESTART_FILE,"<$restart_file") || die "Unable to open $restart_file: $!";
	while(<RESTART_FILE>) {
		chomp;
		next if (/^#/);
		my ($name,$value)=split(/\s*\=\s*/);
		$name=~s/^\s*//;
		$value=~s/\s*$//;
	
		if ($name=~/^CURRENT_ACTION$/i) {
			$self->SetCurrentAction($value);
		} elsif ($name=~/^CURRENT_PHASE$/i) {
			$self->SetCurrentPhase($value);
		} elsif ($name=~/^CURRENT_ACTION_INFO$/i) {
			$self->_SetActionInfo($value);
		} elsif ($name=~/^CURRENT_PATCH_NAME$/i) {
			$self->SetCurrentPatchName($value);
		} elsif ($name=~/^CURRENT_RESTORE_DIR$/i) {
			$self->SetCurrentRestoreDir($value);
		} elsif ($name=~/^CURRENT_MADE_CHANGES$/i) {
			if ($value=~/^(FALSE|OFF|NO|N|0)$/) {
				$made_changes=0;
			} else {
				$made_changes=1;
			}
			$self->SetPatchMadeChangesFlag($made_changes);
		} elsif ($name=~/^CURRENT_FRESH_INSTALL$/i) {
			if ($value ne '' && $value=~/^(TRUE|ON|YES|Y|1)$/) {
				$fresh_install=1;
			} else {
				$fresh_install=0;
			}
			$self->SetCurrentFreshInstall($fresh_install);
		} elsif ($name=~/^(.*)_CROSSPHASE_ACTION_INFO$/) {
			my $cpai_action=$1;
			$cpai_action=~tr/a-z/A-Z/;
			$self->_AddCrossPhaseAI($cpai_action,$value);
		} else {
			&::LogMsg("Unknown value in restart file: $name = $value");
		}
	}
	close(RESTART_FILE);
	
	if ($made_changes eq '') {
		#A restart file without a made changes setting
		#for backwards compatibility, assume that means some action made changes
		&::LogMsg("Assuming patch made changes = Y, as CURRENT_MADE_CHANGES is missing from restart state");
		$self->SetPatchMadeChangesFlag(1);
	}
	
	if ($fresh_install eq '') {
		#A restart file without a fresh install setting
		#for backwards compatibility, assume that means this is not a fresh install
		&::LogMsg("Assuming fresh install = N, as CURRENT_FRESH_INSTALL is missing from restart state");
		$self->SetCurrentFreshInstall(0);
	}
	
	#If this restart state is for a patch other than the current one, abort
	my $rs_patch_name=$self->GetCurrentPatchName();
	if ($this_patch_name ne $rs_patch_name) {
		&::LogMsg("ERROR: Restart state appears to be for a different patch then the current patch");
		&::LogMsg("Restart State Patch is $rs_patch_name - Current patch is $this_patch_name");
		&::LogMsg("Remove $restart_file to force orpatch to start a new session");
		return 0;
	}

	my $phase_name=$self->ConvertPhaseNumberToName($self->GetCurrentPhase());
	my $action_name=$self->GetCurrentAction();
	
	&::LogMsg("Restarting with: Phase $phase_name - Action ($action_name)");
	
	#Don't remove the restart file after loading it as we may be interrupted before orpatch can create a new one
	#&::RemoveFile($restart_file);
	
	return 1;
}
	
#########################
# Sub:  ConvertPhaseNumberToName
# Desc: convert a phase number (like 10, 20, 30, etc) to a phase name
#########################
sub ConvertPhaseNumberToName {
	my ($self,$phase_num)=@_;
	
	foreach my $ref (@{$self->{'PHASE_MAP'}}) {
		my ($num,$name)=@$ref;
		if ($phase_num<=$num) {
			return $name;
		}
	}
	return 'UNKNOWN';
}

#########################
# Sub:  ConvertPhaseNameToNumber
# Desc: convert a phase name to a phase number
# Rets: phase number on success, 0 on failure
#########################
sub ConvertPhaseNameToNumber {
	my ($self,$phase_name)=@_;
	
	foreach my $ref (@{$self->{'PHASE_MAP'}}) {
		my ($num,$name)=@$ref;
		if ($name=~/^$phase_name$/i) {
			return $num;
		}
	}

	return 0;
}

#This is not safe to call outside of ORPRestartState, use SetAndSaveActionInfo instead
sub _SetActionInfo {
	my $self=shift;
	my ($action_info)=@_;
	$self->{'CURRENT_ACTION_INFO'}=$action_info;
	return 1;
}

sub SetRestartFile {
	my $self=shift;
	my ($restart_file)=@_;
	$self->{'RESTART_FILE'}=$restart_file;
}

sub GetRestartFile {
	my $self=shift;
	return $self->{'RESTART_FILE'};
}

sub _GetCrossPhaseAIHRef {
	my $self=shift;
	return $self->{'CROSSPHASE_AI'};
}

sub _ResetCrossPhaseAI {
	my $self=shift;
	$self->{'CROSSPHASE_AI'}={};
}

sub _AddCrossPhaseAI {
	my $self=shift;
	my ($action,$data)=@_;
	my $cphref=$self->_GetCrossPhaseAIHRef();
	my $cpai_ref=$cphref->{$action};
	if ($cpai_ref eq '') {
		$cpai_ref=[];
	}
	push(@$cpai_ref,$data);
	$cphref->{$action}=$cpai_ref;
}

sub _MakeCrossPhaseAIName {
	my $self=shift;
	my ($action_name)=@_;
	return "${action_name}_CROSSPHASE_ACTION_INFO";
}

#####################################################################
# Sub:  new
# Desc: Initialize the restart state
# Args: restart_file - The name of the restart state file
#####################################################################
sub new {
    my $class=shift;
	my ($restart_file)=@_;
    my $self={};
	
	my $inc=$self->{'RESTART_INCREMENT'}=10;
	my $phase=0;
	
	my $copy_patch_name='Copy-Patch';
	
	#This is the mapping of restart points to phase names
	my @phase_map=(
	[$phase+=$inc,'Pre-action','PreAction'],
	[$phase+=$inc,$copy_patch_name,'CopyPatchFiles'],
	[$phase+=$inc,'Patch-Action','PatchAction'],
	[$phase+=$inc,'Post-action','PostAction'],
	[$phase+=$inc,'Clean-up','CleanupAction'],
	);
	
	$self->{'PHASE_MAP'}=\@phase_map;
    bless($self,$class);
	
	$self->SetRestartFile($restart_file);
	$self->{'COPY_PATCH_PHASE'}=$self->ConvertPhaseNameToNumber($copy_patch_name);
	
	$self->_ResetCrossPhaseAI();
	
    return $self;
}

1;
