############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
package FileCopyMethod;
use strict;

use File::Copy;

#########################
# Sub:  DoesMethodApply
# Desc: Check if this method applies to a specific file name
# Args: filename
# Ret:  1 if this method applies, 0 if not
#########################
sub DoesMethodApply {
	my $self=shift;
	my ($filename)=@_;
	
	foreach my $pat (@{$self->{'FILE_NAME_PATTERNS'}}) {
		if ($filename=~/$pat/) {
			return 1;
		}
	}
	return 0;
}

#########################
# Sub:  DoCopy
# Desc: Do the actual copy of a source file to a destination file
# Args: full path to source file, full path to dest file, ref to a scalar that receives error details
# Ret:  1 on success, 0 on failure
#########################
sub DoCopy {
	my $self=shift;
	my ($src_file,$dest_file,$error_ref)=@_;
	
	my $ret=0;
	if (&File::Copy::copy($src_file,$dest_file)) {
		#Successful
		$ret=1;
	} else {
		#Copy failed!
		$$error_ref="Unable to copy $src_file to $dest_file: $!";
		$ret=0;
	}
	return $ret;
}

#########################
# Sub:  GetIsPrepared
# Desc: Check if this FileCopyMethod has prepared for copies
# Args: None
# Ret:  1 if prepared, 0 if not
#########################
sub GetIsPrepared {
	my $self=shift;
	if ($self->{'FILE_COPY_METHOD_PREPARED'}==1) {
		return 1;
	} else {
		return 0;
	}
}

#########################
# Sub:  SetIsPrepared
# Desc: Set if this FileCopyMethod has prepared for copies
# Args: prepared - 1 if we are prepared, 0 if not
# Ret:  1
#########################
sub SetIsPrepared {
	my $self=shift;
	my ($prepared)=@_;
	$self->{'FILE_COPY_METHOD_PREPARED'}=$prepared;
	return 1;
}

#########################
# Sub:  PrepareForCopies
# Desc: Prepare this FileCopyMethod for copying
# Args: None
# Ret:  1 if preparations were successful, 0 if not
#########################
sub PrepareForCopies {
	my $self=shift;
	return $self->SetIsPrepared(1);
}

#####################################################################
# Sub:  new
# Desc: A class encapsulating a method for accomplishing file copies
# Args: ref to a list of filename patterns this method applies to
#####################################################################
sub new {
    my $class=shift;
	my ($filename_pat_ref)=@_;
    my $self={};
	
    bless($self,$class);
	
	$self->{'FILE_NAME_PATTERNS'}=$filename_pat_ref;
	
	$self->PrepareForCopies();

	return $self;
}

1;
