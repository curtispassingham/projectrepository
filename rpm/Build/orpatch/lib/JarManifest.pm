############################################################################
# Copyright © 2014,2015, Oracle and/or its affiliates. All rights reserved.
############################################################################
package JarManifest;
use strict;

$JarManifest::JAR_MANIFEST="META-INF/env_manifest.csv";

############################
# Sub:  HasJarManifest
# Desc: Determine if a jar has a jar manifest
# Args: jar_file - path to the jar file
# Returns: 1 if it has a manifest, 0 if not, -1 if an error occurred
############################
sub JarManifest::HasJarManifest {
	my ($jar_file)=@_;
	
	return &JarFile::CheckJarForFile($jar_file,$JarManifest::JAR_MANIFEST);
}

############################
# Sub:  ExtractAndReadJarManifest
# Desc: Extract a jar manifest and read the contents
# Args: jar_file - path to the jar file
#       jm_info_ref - ref to a list that will receive the info on the jar manifest lines
#       hr_ref - ref to a scalar that will receive the highest revision in the jar
#       expand_dir - An optional expand directory.  If this is specified the jar manifest and expand directory is not removed after reading
#       require_extract - 1 to required the manifest to be successfully extracted, otherwise ignore it
# Returns: 1 on success, 0 on failure
############################
sub JarManifest::ExtractAndReadJarManifest {
	my ($jar_file,$jm_info_ref,$hr_ref,$orig_expand_dir,$require_extract)=@_;
	
	my $expand_dir=$orig_expand_dir;
	if ($expand_dir eq '') {
		$expand_dir=&JarFile::GetTempExpandDir($jar_file);
	}
	
	#&::LogMsg("Extracting existing $JarManifest::JAR_MANIFEST");
	if (&JarFile::Expand($jar_file,$expand_dir,$JarManifest::JAR_MANIFEST,(($require_extract==1) ? 1 : 0))!=1) {
		return 0;
	}
	
	my $full_jar_manifest=&::ConcatPathString($expand_dir,$JarManifest::JAR_MANIFEST);
	
	@$jm_info_ref=&Manifest::ReadManifest($full_jar_manifest);

	if (defined($hr_ref) && $hr_ref ne '') {
		my $high_rev=0;
		foreach my $ref (@$jm_info_ref) {
			my ($dfile,$sfile,$ftype,$frevision)=&DecodeLine($ref);
			
			if (&PatchFileCopy::RevisionCompare($frevision,$high_rev)==1) {
				$high_rev=$frevision;
			}
		}
		$$hr_ref=$high_rev;
	}
	
	if ($orig_expand_dir eq '') {
		&JarFile::RemoveExpandDir($expand_dir,1);
	}

	return 1;
}

#########################
# Sub:  CreateJarManifest
# Desc: Create a jar manifest in 
# Arg:  jar_file - The jar file to update
#       jm_info_ref - Ref to the list of jar manifest lines
#       orig_expand_dir - An optional expand directory.  If specified the expand directory is not cleaned.
#       silent - 1 if we should not take about updating the jar
#       extra_update_files_ref - Optional ref to a list of [base_dir,update_file] to include on the update of the jar
#                                this allows the manifest and a bunch of files to update to be updated in a jar in one jar command.
#                                a new entry for the jar manifest will be added to the list
# Ret:  1 on success, 0 on failure
#########################
sub JarManifest::CreateJarManifest {
	my ($jar_file,$jm_info_ref,$orig_expand_dir,$silent,$extra_update_files_ref)=@_;
	
	my $base_dir=$orig_expand_dir;
	if ($base_dir eq '') {
		$base_dir=&JarFile::GetTempExpandDir($jar_file);
		if (&JarFile::CreateExpandDir($base_dir)!=1) {
			return 0;
		}
	}
	
	my $rel_jar_manifest=$JarManifest::JAR_MANIFEST;
	my $full_jar_manifest=&::ConcatPathString($base_dir,$rel_jar_manifest);
	
	my ($dir,$file)=&::GetFileParts($full_jar_manifest,2);
	&::CreateDirs('0750',$dir);
	
	#&::LogMsg("Writing ".scalar(@$jm_info_ref)." entries to $full_jar_manifest");
	if (&Manifest::SaveDestManifest($full_jar_manifest,[],$jm_info_ref,{},1)!=1) {
		&::LogMsg("Unable to create jar manifest file for $jar_file");
		return 0;
	}
	
	if ($extra_update_files_ref eq '') {
		#Archive the manifest in the jar
		if (&JarFile::Update($jar_file,$base_dir,$rel_jar_manifest,$silent)!=1) {
			return 0;
		}
	} else {
		push(@$extra_update_files_ref,[$base_dir,$rel_jar_manifest]);
		if (&JarFile::UpdateMulti($jar_file,$extra_update_files_ref,$silent)!=1) {
			return 0;
		}
	}
	
	#Cleanup after ourselves
	if ($orig_expand_dir eq '') {
		&JarFile::RemoveExpandDir($base_dir,1);
	}
	
	return 1;
}

#########################
# Sub:  ExtractFullManifest
# Desc: Extract the full jar manifest from an archive, making relative file names based on the path of the jar
# Arg:  jar_file - The jar file to extract a full manifest from
#       jm_ref - Ref to the list where the jar manifest information will be stored
#       high_rev_ref - Ref to a scalar where the overall highest rev will be stored
#       require_extract - 1 to require the extract of the jarmanifest, otherwise return an empty list if it doesn't exit
#       silent - 1 if we should not take about extracting the jar manifest
# Ret:  1 on success, 0 on failure
#########################
sub JarManifest::ExtractFullManifest {
	my ($jar_file,$jm_ref,$high_rev_ref,$require_extract,$silent)=@_;
	
	&::LogMsg("Exporting full manifest from $jar_file") unless ($silent==1);
	
	return &_ExtractFullManifest($jar_file,$jm_ref,$high_rev_ref,'',$require_extract,$silent);
}

#The recursive function that does all the work of ExtractFullManifest
sub JarManifest::_ExtractFullManifest {
	my ($jar_file,$jm_ref,$high_rev_ref,$jar_path,$require_extract,$silent)=@_;
	
	my ($dir,$base_file)=&::GetFileParts($jar_file,2);
	if ($jar_path eq '') {
		$jar_path=$base_file;
	} else {
		$jar_path=&::ConcatPathString($jar_path,$base_file);
	}
	
	#&::LogMsg("Reading manifest in $jar_file");
	my @jm_info=();
	if (&ExtractAndReadJarManifest($jar_file,\@jm_info,'','',$require_extract)!=1) {
		return 0;
	}
	
	my @saved_jars=();
	my $this_highest_rev=0;
	
	#Now re-base our manifest info and save any jars that have embedded jar manifests
	foreach my $ref (@jm_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$custom,$fwp_rev,$has_jar_manifest,@future)=&DecodeLine($ref);
		my $rebased_dfile=&::ConcatPathString($jar_path,$dfile);
		push(@$jm_ref,[$rebased_dfile,$sfile,$ftype,$frevision,$fdesc,$custom,$fwp_rev,$has_jar_manifest,@future]);
		if ($has_jar_manifest=~/^Y$/i) {
			push(@saved_jars,$dfile);
		}
		if (&PatchFileCopy::RevisionCompare($frevision,$this_highest_rev)==1) {
			$this_highest_rev=$frevision;
		}
	}

	#Process any saved jars recursively
	foreach my $sub_jar (@saved_jars) {
		my $expand_dir=&JarFile::GetTempExpandDir($jar_file);
		my $full_sub_jar=&::ConcatPathString($expand_dir,$sub_jar);
		
		#&::LogMsg("Extracting sub jar: $sub_jar to $expand_dir");
		
		#Extract the sub jar
		if (&JarFile::Expand($jar_file,$expand_dir,$sub_jar,1)!=1) {
			return 0;
		}
		
		#Make sure we don't lose any path information within this jar
		my $recurse_jar_path=$jar_path;
		my ($sub_jar_path,$discard)=&::GetFileParts($sub_jar,2);
		if ($sub_jar_path ne '.') {
			$recurse_jar_path=&::ConcatPathString($jar_path,$sub_jar_path);
		}
		
		#Recursively process it's manifest
		my $sub_highest_rev=0;
		if (&_ExtractFullManifest($full_sub_jar,$jm_ref,\$sub_highest_rev,$recurse_jar_path,$require_extract,$silent)!=1) {
			return 0;
		}
		if (&PatchFileCopy::RevisionCompare($sub_highest_rev,$this_highest_rev)==1) {
			$this_highest_rev=$sub_highest_rev;
		}
		&JarFile::RemoveExpandDir($expand_dir,1);
	}
	
	$$high_rev_ref=$this_highest_rev;
	
	return 1;
}

#########################
# Sub:  SplicDestManifestIntoJar
# Desc: Splice a list of destination manifest entries into a jar
# Arg:  jar_file - full path to the jar to modify
#       full_dest - path the destination manifest is relative to
#       dest_info_ref - ref to the list of destination manifest lines to splice
#       all_sign_ref - ref to a list of jars where if one jar in the list is re-signed, all jars in the list must be re-signed
# Ret:  1 if successful, 0 if not
#########################
sub JarManifest::SpliceDestManifestIntoJar {
	my ($jar_file,$full_dest,$dest_info_ref,$all_sign_ref)=@_;
	
	my $file_count=scalar(@$dest_info_ref);
	&::LogMsg("Splicing $file_count files into $jar_file");
	
	#Construct the splicing info structure based on the dest_info_ref
	my @splice_info=();
	foreach my $ref (@$dest_info_ref) {
		#This dfile is relative to full_dest
		my ($dfile,@info)=&Manifest::DecodeDestLine($ref);
		push(@splice_info,[$full_dest,$dfile,$ref]);
	}
	
	my $high_rev=0;
	return &_SpliceJar($jar_file,\$high_rev,\@splice_info,1,$all_sign_ref);
}

#########################
# Sub:  ExtractEmbeddedFilesFromJar
# Desc: Extract a list of files from a jar, and/or from embedded jars
#       This function is more-or-less the reverse of SpliceDestManifestIntoJar
# Arg:  jar_file - full path to the jar to modify
#       extract_dir - Directory to extract files into
#       file_list_ref - ref to the list of files to extract
# Ret:  1 if successful, 0 if not
#########################
sub JarManifest::ExtractEmbeddedFilesFromJar {
	my ($jar_file,$extract_dir,$file_list_ref)=@_;
	
	my $file_count=scalar(@$file_list_ref);
	&::LogMsg("Extracting $file_count files from $jar_file");
	
	#Construct the splicing info structure based on the file_list_ref
	my @splice_info=();
	foreach my $file (@$file_list_ref) {
		push(@splice_info,[$extract_dir,$file,&Manifest::AssembleDestLine($file)]);
	}
	
	return &_UnSpliceJar($jar_file,\@splice_info,1);
}

#########################
# Sub:  AssembleLine
# Desc: Create a jar manifest line
# Arg:  arguments in the same order as Manifest::AssembleDestLine
# Ret:  a ref to the new jar manifest/destination manifest line
#########################
sub JarManifest::AssembleLine {
	my @parts=@_;
	
	return &Manifest::AssembleDestLine(@parts);
}

#########################
# Sub:  DecodeLine
# Desc: Decode a jar manifest line
# Arg:  ref - Ref to the jar manifest line (array)
# Ret:  Returns list of jar manifest components in Manifest::DecodeDestLine order
#########################
sub JarManifest::DecodeLine {
	my ($ref)=@_;
	
	return &Manifest::DecodeDestLine($ref);
}

#########################
# Sub:  _SpliceJar
# Desc: Do the work of splicing a list of files into a jar
# Arg:  jar_file - full path to the jar to modify
#       high_rev_sref - Ref to a scalar that will contain the high rev of the jar after splicing
#       splice_info_ref - A ref to a list of [file_path,relative_file,destination manifest ref for this file]
#       indent_level - Level of indenting in log messages
#       all_sign_ref - A ref to a list of jars where if one jar is signed, all the jars in the list should be signed
#       needed_signing_sref - A ref to a scalar that is set to 1 if the jar needed to be signed (or unsigned)
# Ret:  1 if successful, 0 if not
#########################
sub _SpliceJar {
	my ($jar_file,$high_rev_sref,$splice_info_ref,$indent_level,$all_sign_ref,$needed_signing_sref)=@_;
	
	#Update our splicing information based on the jar we are processing
	my ($jar_dir,$base_jar)=&::GetFileParts($jar_file,2);
	my @rebase_splice_info=();
	if (&_RebaseSpliceInfo($base_jar,$splice_info_ref,\@rebase_splice_info)!=1) {
		&::LogMsg("Splice info for $jar_file contains splicing entries for other jars!");
		return 0;
	}
	
	#Get the table of contents for the jar
	my @jar_manifest_info=();
	my $jar_highest_rev=0;
	my %jm_lookup=();
	if (&_BuildSplicingLookup($jar_file,\@jar_manifest_info,\$jar_highest_rev,\%jm_lookup)!=1) {
		return 0;
	}
	
	#Look through the splicing entries and determine which are in this jar versus sub-jars
	my @this_jar_archive_info=();
	my %sub_jar_archive_info=();
	if (&_FindSpliceDestination(\@rebase_splice_info,\%jm_lookup,\@this_jar_archive_info,\%sub_jar_archive_info)!=1) {
		return 0;
	}
	
	my @update_files=();
	my $expand_dir=&JarFile::GetTempExpandDir($jar_file);
	my @expand_dirs=($expand_dir);

	#Process the files that go directly into this jar
	if (scalar(@this_jar_archive_info)!=0) {
		my $spaces=" "x($indent_level*3);
		&::LogMsg(sprintf("%sArchiving %4d file(s) into %s",$spaces,scalar(@this_jar_archive_info),$base_jar));
		if (&_UpdateJarManifest(\@jar_manifest_info,\$jar_highest_rev,\@this_jar_archive_info,\@update_files)!=1) {
			return 0;
		}
	}
	
	#Process the files that go into sub-jars
	my %extracted_sub_jars=();
	foreach my $rel_sub_jar (keys(%sub_jar_archive_info)) {
		my $sj_ref=$sub_jar_archive_info{$rel_sub_jar};
		my ($sub_base_dir,$sub_base_jar)=&::GetFileParts($rel_sub_jar,2);
		my $sub_expand_dir="${expand_dir}_${sub_base_jar}";
		
		#Extract the sub jar
		if (&JarFile::Expand($jar_file,$sub_expand_dir,$rel_sub_jar,1)!=1) {
			&::LogMsg("Unable to extract $rel_sub_jar from $jar_file");
			return 0;
		}
		
		my $sub_jar=&::ConcatPathString($sub_expand_dir,$rel_sub_jar);
		
		#Find the manifest row for the sub-jar
		my $jm_l_ref=$jm_lookup{$rel_sub_jar};
		my ($jm_rev,$jm_dinfo)=@$jm_l_ref;
		
		#Splice the files into the sub jar
		my $spaces=" "x($indent_level*3);
		my $jar_needed_signing=0;
		&::LogMsg(sprintf("%sSplicing  %4d file(s) into %s",$spaces,scalar(@$sj_ref),$rel_sub_jar));
		if (&_SpliceJar($sub_jar,\$jm_rev,$sj_ref,$indent_level+1,$all_sign_ref,\$jar_needed_signing)!=1) {
			return 0;
		}
		
		$extracted_sub_jars{$rel_sub_jar}=$jar_needed_signing;
		
		my $new_dinfo=&_AssembleLineWithUpdatedRev($jm_dinfo,$jm_rev,'Y');
		
		if (&_UpdateJarManifest(\@jar_manifest_info,\$jar_highest_rev,[[$sub_expand_dir,$rel_sub_jar,$new_dinfo]],\@update_files)!=1) {
			return 0;
		}
		
		push(@expand_dirs,$sub_expand_dir);
	}
	
	#Check if any of the modified sub-jars were signed and so trigger 'all sign' requirements
	foreach my $all_sign_jar (@$all_sign_ref) {
		if ($extracted_sub_jars{$all_sign_jar}==1) {
			#We need to sign everything in the all sign list that hasn't already been spliced
			my $spaces=" "x(($indent_level)*3);
			&::LogMsg(sprintf("%sResigning all jnlp jars as %s was re-signed",$spaces,$all_sign_jar));
			if (&_DoAllSign($jar_file,$expand_dir,\@expand_dirs,$all_sign_ref,\%extracted_sub_jars,\@update_files,$indent_level+1)!=1) {
				return 0;
			}
			last;
		}
	}
	
	#Write our updated jar manifest and the various files back into the jar
	&::LogMsg("Running final update for $base_jar") if ($indent_level==1);
	if (&CreateJarManifest($jar_file,\@jar_manifest_info,$expand_dir,1,\@update_files)!=1) {
		return 0;
	}
	
	#remove all our temporary expand dirs
	foreach my $ed (@expand_dirs) {
		&JarFile::RemoveExpandDir($ed,1);
	}
	
	#If this was a signed jar, unsign and possibly resign it
	if (&_HandleSignedJar($jar_file,\%jm_lookup,$indent_level,$needed_signing_sref)!=1) {
		return 0;
	}
	
	$$high_rev_sref=$jar_highest_rev;
	
	return 1;
}

#########################
# Sub:  _UnSpliceJar
# Desc: Do the work of extracting a list of files from a jar
# Arg:  jar_file - full path to the jar to extract from
#       splice_info_ref - A ref to a list of [extract_file_path,relative_file,destination manifest ref for this file]
#       indent_level - The level of indentation for logging (start at 1)
# Ret:  1 if successful, 0 if not
#########################
sub _UnSpliceJar {
	my ($jar_file,$splice_info_ref,$indent_level)=@_;
	
	#Update our splicing information based on the jar we are processing
	my ($jar_dir,$base_jar)=&::GetFileParts($jar_file,2);
	my @rebase_splice_info=();
	if (&_RebaseSpliceInfo($base_jar,$splice_info_ref,\@rebase_splice_info)!=1) {
		&::LogMsg("Extract info for $jar_file contains entries for other jars!");
		return 0;
	}
	
	#Get the table of contents for the jar
	my @jar_manifest_info=();
	my $jar_highest_rev=0;
	my %jm_lookup=();
	if (&_BuildSplicingLookup($jar_file,\@jar_manifest_info,\$jar_highest_rev,\%jm_lookup)!=1) {
		return 0;
	}
	
	#Look through the splicing entries and determine which are in this jar versus sub-jars
	my @this_jar_archive_info=();
	my %sub_jar_archive_info=();
	if (&_FindSpliceDestination(\@rebase_splice_info,\%jm_lookup,\@this_jar_archive_info,\%sub_jar_archive_info)!=1) {
		return 0;
	}

	#Extract files directly from this jar
	if (scalar(@this_jar_archive_info)!=0) {
		my $spaces=" "x($indent_level*3);
		&::LogMsg(sprintf("%sExtracting %4d file(s) from %s",$spaces,scalar(@this_jar_archive_info),$base_jar));
		if (&_ExtractSpliceInfoListFromJar($jar_file,\@this_jar_archive_info)!=1) {
			return 0;
		}
	}

	my $expand_dir=&JarFile::GetTempExpandDir($jar_file);
	
	#Process the files that come from sub-jars
	foreach my $rel_sub_jar (keys(%sub_jar_archive_info)) {
		my $sj_ref=$sub_jar_archive_info{$rel_sub_jar};
		my ($sub_base_dir,$sub_base_jar)=&::GetFileParts($rel_sub_jar,2);
		my $sub_expand_dir="${expand_dir}_${sub_base_jar}";
		
		#Extract the sub jar
		my $spaces=" "x($indent_level*3);
		&::LogMsg(sprintf("%sExtracting sub-jar %s",$spaces,$rel_sub_jar));
		if (&JarFile::Expand($jar_file,$sub_expand_dir,$rel_sub_jar,1)!=1) {
			&::LogMsg("Unable to extract $rel_sub_jar from $jar_file");
			return 0;
		}
		
		my $sub_jar=&::ConcatPathString($sub_expand_dir,$rel_sub_jar);
		
		#Extract the files from the sub jar
		if (&_UnSpliceJar($sub_jar,$sj_ref,$indent_level+1)!=1) {
			return 0;
		}
		
		&JarFile::RemoveExpandDir($sub_expand_dir,1);
	}
	
	return 1;
}

#########################
# Sub:  _UpdateJarManifest
# Desc: Update a jar manifest based on a number of incoming changes, create a structure that can be passed
#       UpdateJarMulti to actually archive all the files into the jar
# Arg:  jm_info_ref - ref to the list of jar manifest entries
#       jar_high_rev_sref - ref to a scalar containing the high revision of this jar
#       jar_archive_info_ref - ref to a list of [file_path,relative_file_name,destination manifest ref for this file]
#       update_files_ref - ref to a list that will be populated with [file_path,update_file] entries for each update
# Ret:  1 if successful, 0 if not
#########################
sub _UpdateJarManifest {
	my ($jm_info_ref,$jar_high_rev_sref,$jar_archive_info_ref,$update_files_ref)=@_;
	
	#Update the jar manifest for each file and create an update_files entry
	foreach my $ref (@$jar_archive_info_ref) {
		my ($file_path,$cur_rel_dfile,$dinfo_ref)=@$ref;
		
		push(@$update_files_ref,[$file_path,$cur_rel_dfile]);
		
		#Update the Jar Manifest with new file info
		#Use the relative name that was stored in the jar, rather than the relative name in the destination manifest	
		my ($dfile,$sfile,$ftype,$frevision,$patch_name,$custom,$fwp_rev,$has_jar_manifest,$future2,$future3,$future4)=&Manifest::DecodeDestLine($dinfo_ref);
		&Manifest::UpdateDestWithLimitedInfo($sfile,$ftype,$frevision,$cur_rel_dfile,$patch_name,$custom,$fwp_rev,$has_jar_manifest,$future2,$future3,$future4,$jm_info_ref);
		
		#Update our highest revision if this file pushed it up
		if (&PatchFileCopy::RevisionCompare($frevision,$$jar_high_rev_sref)==1) {
			$$jar_high_rev_sref=$frevision;
		}
	}

	return 1;
}

#########################
# Sub:  _FindSpliceDestination
# Desc: Determine where a list of files should be archived, either into this jar directly or into a sub-jar
# Arg:  splice_info_ref - ref to a list of [file_path,relative_file_name,destination manifest ref for this file]
#       jm_lookup_href - ref to a hash that maps file names in the jar manifest to their revision
#       this_jar_ref - ref to a list that will be filled with the splice info of files that will go into this jar
#       sub_jar_href - ref to a hash that will be filled with the splice info of files that will into a sub-jar (keyed by sub-jar path)
# Ret:  1
#########################
sub _FindSpliceDestination {
	my ($splice_info_ref,$jm_lookup_href,$this_jar_ref,$sub_jar_href)=@_;
	
	foreach my $ref (@$splice_info_ref) {
		my ($file_path,$cur_rel_dfile,$dinfo_ref)=@$ref;
		
		#The algorithm for this splicing is to split the relative file name by directory and search
		#for the file in this jar adding one level of info at a time until we find a match.
		#If there are parts of the relative file name left when we find a match then the destination
		#for splicing is in a sub jar.  If there are no parts left, then the destination is in this jar.
		#
		# Example: relative file name - lib/sjxp.jar
		#     First check if a file name 'lib' exists, it doesn't
		#     then check if 'lib/sjxp.jar' exists, it does
		#     there are no parts left so this file should be placed in the current jar
		#     at lib/sjxp.jar
		# Example: relative file name - WebLaunchServlet.war/lib/rpm14.jar/com/oracle/retail/steve.class
		#     First check if a file name 'WebLaunchServlet.war' exists, it does
		#     so 'lib/rpm41.jar/com/oracle/retail/steve.class' should be spliced into WebLaunchServlet.jar
		#     On recursion then we'd determine that 'lib/rpm14.jar' exists and splice the remainder into that
		#     eventually 'com/oracle/retail/steve.class' would be found (or we would run out of parts)
		#     and it would be put into lib/rpm14.jar inside WebLaunchServlet.war inside the current jar
		my @file_parts=&::SplitPathString($cur_rel_dfile);
		my $search_file='';
		my $found=0;
		while(my $next_dir=shift(@file_parts)) {
			if ($search_file eq '') {
				$search_file.=$next_dir;
			} else {
				$search_file=join('/',$search_file,$next_dir);
			}
			
			#&::LogMsg("Checking for $search_file in jar manifest");
		
			my $jm_l_ref=$jm_lookup_href->{$search_file};
			if ($jm_l_ref ne '') {
				my ($jm_rev,$discard)=@$jm_l_ref;
				#search file exists in this jar
				$found=1;
				if (scalar(@file_parts)==0) {
					#exact match
					#Double check the revision number, a blank revision number is always forced in (config file)
					my ($dfile,$sfile,$ftype,$frevision,@discard)=&DecodeLine($dinfo_ref);
					if ($jm_rev eq '' || &PatchFileCopy::RevisionCompare($frevision,$jm_rev)==1 || $frevision eq '') {
						#&::LogMsg("Found location for $cur_rel_dfile at $search_file in this jar ($file_path,$search_file,$cur_rel_dfile)");
						push(@$this_jar_ref,$ref);
					} else {
						&::LogMsg("Ignoring $cur_rel_dfile:$frevision as jar contains revision $jm_rev");
					}
				} else {
					#found a valid sub-jar
					#&::LogMsg("Found sub jar: $search_file ($file_path,$search_file,$cur_rel_dfile)");
						
					my $sj_ref=$sub_jar_href->{$search_file};
					if ($sj_ref eq '') {
						$sj_ref=[];
					}
					push(@$sj_ref,$ref);
					$sub_jar_href->{$search_file}=$sj_ref;
				}
				last;
			}
		}
		
		if ($found==0) {
			#A new file
			#&::LogMsg("Found new file $cur_rel_dfile for this jar");
			push(@$this_jar_ref,$ref);
		}
	}
			
	return 1;
}

#########################
# Sub:  _RebaseSpliceInfo
# Desc: Adjust splice info (file_path,relative_file,destination manifest ref for this file) based on moving down to a new jar
#       For example:
#          We might start with this info:
#           [/u00/oracle/rms/javaapp_rpm,internal/rpm14.ear/steve.class,<dest manifest ref>]
#          When we start processing rpm14.ear, we want it changed to be relative to the rpm14.ear directory:
#           [/u00/oracle/rms/javaapp_rpm/internal/rpm14.ear,steve.class,<dest manifest ref>]
#          This function will effectively take part of the relative file name and put it into the file path
# Arg:  base_jar - the basename of the jar we are processing
#       splice_info_ref - ref to a list of [file_path,relative_file_name,destination manifest ref for this file]
#       new_splice_info_ref - ref to the list that will get the adjusted splice info
# Ret:  1
#########################
sub _RebaseSpliceInfo {
	my ($base_jar,$splice_info_ref,$new_splice_info_ref)=@_;
	
	foreach my $ref (@$splice_info_ref) {
		my ($file_path,$cur_rel_dfile,$dinfo_ref)=@$ref;
		
		#cur_rel_dfile will look like this:
		#   internal/rpm14.ear/rpm14.jar/blah/blah/file.class
		#   config/rpm14.ear/path/path/rpm.properties
		#   rpm14.jar/blah/blah/blah/file.class
		#split the path into two pieces, everything up to rpm14.ear, and everything after
		
		my @prefix=($file_path);
		my @parts=&::SplitPathString($cur_rel_dfile);
		while(my $dir=shift(@parts)) {
			push(@prefix,$dir);
			#If we just put the base_jar directory onto the prefix, we are done rebasing this path
			if ($dir eq $base_jar) {
				last;
			}
		}
		
		if (scalar(@parts)==0) {
			#we ran out of directory parts before finding $base_jar
			&::LogMsg("Splice candidate $cur_rel_dfile does not contain $base_jar, ignoring");
		} else {
			#Create the new adjusted file path and relative destination name
			my $new_file_path=&::ConcatPathString(@prefix);
			my $new_rel_dfile=&::ConcatPathString(@parts);
			push(@$new_splice_info_ref,[$new_file_path,$new_rel_dfile,$dinfo_ref]);
		}
	}
	
	return 1
}

#########################
# Sub:  _BuildSplicingLookup
# Desc: Build the file lookup hash from the jar file's TOC and populate the jar manifest information if it is available
# Arg:  jar_file - Jar file to build the splicing maps for
#       jm_info_ref - Ref to the list that will receive the jar manifest info if it is available
#       jar_high_rev_sref - Ref to the scalar that will receive the highest revision in the jar if it is available
#       jm_lookup_href - Ref to the hash that will get the splicing lookup map
# Ret:  1
#########################
sub _BuildSplicingLookup {
	my ($jar_file,$jm_info_ref,$jar_high_rev_sref,$jm_lookup_href)=@_;
	
	#We always splice based on the Jar's table of contents
	my %toc_hash=();
	if (&JarFile::GetTOC($jar_file,\%toc_hash,0,'')!=1) {
		&::LogMsg("Unable to get table-of-contents for $jar_file during splicing!");
		return 0;
	}
	
	#Jar has a jar manifest file, make sure we can extract it
	if ($toc_hash{$JarManifest::JAR_MANIFEST} eq 'FILE') {
		if (&ExtractAndReadJarManifest($jar_file,$jm_info_ref,$jar_high_rev_sref,'',1)!=1) {
			&::LogMsg("Unable to extract jar manifest from $jar_file during splicing");
			return 0;
		}
	}
	
	#Create a file lookup map with dummy jar manifest lines to start
	foreach my $dfile (keys(%toc_hash)) {
		$jm_lookup_href->{$dfile}=['',&AssembleLine($dfile,$dfile)];
	}
	
	#Update the file lookup map with the jar manifest info if we have any
	foreach my $jm_ref (@$jm_info_ref) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$custom,$fwp_rev,$has_jar_manifest,@future)=&DecodeLine($jm_ref);
		#If the destination file exists in the jar, save it's frevision and the ref to the manifest info
		if ($toc_hash{$dfile} ne '') {
			$jm_lookup_href->{$dfile}=[$frevision,$jm_ref];
		}
	}
	
	return 1;
}

#########################
# Sub:  _ExtractSpliceInfoListFromJar
# Desc: Extract a list of files (in splice info format) from a jar file
# Arg:  jar_file - Jar file to build the splicing maps for
#       splice_info_ref - Ref to the list of splice info entries [[full_path,file,ignored],...]
# Ret:  1 on success, 0 on failure
#########################
sub JarManifest::_ExtractSpliceInfoListFromJar {
	my ($jar_file,$splice_info_ref)=@_;

	my $extract_loc='';
	my @extract_files=();
	foreach my $ref (@$splice_info_ref) {
		my ($extract_path,$file,$discard)=@$ref;
		if ($extract_loc ne $extract_path && $extract_loc ne '') {
			&::LogMsg("Error while UnSplicing from $jar_file:");
			&::LogMsg("Extract paths from unexpectedly differ ($extract_loc!=$extract_path)");
			foreach my $dref (@$splice_info_ref) {
				&::LogMsg("   $$dref[0] , $$dref[1]");
			}
			return 0;
		}
		$extract_loc=$extract_path;
		push(@extract_files,$file);
	}
	
	&::CreateDirs('0750',$extract_loc);
	
	if (&JarFile::Expand($jar_file,$extract_loc,\@extract_files,1)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  _HandleSignedJar
# Desc: Check if a jar that has been spliced is signed and unsign it, resigning it if jar signing is enabled
# Arg:  jar_file - path to the jar file to unsign/resign
#       jm_lookup_href - ref to the hash of the table of contents of the jar, used to determine if it is signed
#       needed_signing_sref - ref to a scalar that is set to 1 if the jar needed to be signed
# Ret:  1 on success, 0 on failure
#########################
sub _HandleSignedJar {
	my ($jar_file,$jm_lookup_href,$indent,$needed_signing_sref)=@_;

	#Several jar-like files can be spliced, but only jars can be signed
	my ($dir,$base_file,$ext)=&::GetFileParts($jar_file,3);
	unless($ext=~/^jar$/i) {
		return 1;
	}
	
	my $pattern_ref=&JarFile::GetSignedJarPatterns(1);
	
	#Look for META-INF/*.dsa, *.rsa, *.sf files
	my $signed=0;
	foreach my $file (keys(%$jm_lookup_href)) {
		foreach my $pr (@$pattern_ref) {
			if ($file=~/$pr/) {
				$signed=1;
				last;
			}
		}
	}
	
	#If it is not signed, we're done here
	return 1 if $signed==0;
	
	$$needed_signing_sref=1;
	
	if (&JarFile::Unsign($jar_file,$indent)!=1) {
		return 0;
	}
	
	return &JarFile::Sign($jar_file,$indent);
}

#########################
# Sub:  _DoAllSign
# Desc: Sign a list of jars that haven't already been spliced
# Arg:  jar_file - path to the jar file to expand jars from
#       expand_dir - The base directory to use for creating temporary extract directories
#       expand_dirs_ref - ref to the list of all expand directories we create
#       all_sign_ref - ref to the list of files which need to be signed if they haven't already
#       spliced_jar_href - Ref to a hash of jars which have already been signed keyed by relative jar name, value of 1 if they were already signed/unsigned
#       update_files_ref - Ref to the list that will receive [path_to_file,relative destination in jar] for all files that need to be put back into the containing jar
#       indent - The level of indentation for log messages
# Ret:  1 on success, 0 on failure
#########################
sub _DoAllSign {
	my ($jar_file,$expand_dir,$expand_dirs_ref,$all_sign_ref,$spliced_jar_href,$update_files_ref,$indent)=@_;
	
	my $sub_expand_dir="${expand_dir}_allsign";

	#Make a list of all the jars not already processed during splicing
	my @expand_list=();
	foreach my $rel_sub_jar (@$all_sign_ref) {
		#If the jar was already spliced and signed, don't resign it
		next if ($spliced_jar_href->{$rel_sub_jar}==1);
		
		push(@expand_list,$rel_sub_jar);
		push(@$update_files_ref,[$sub_expand_dir,$rel_sub_jar]);
	}
	
	#Extract all the files in need of signing
	if (&JarFile::Expand($jar_file,$sub_expand_dir,\@expand_list,1)!=1) {
		&::LogMsg("Unable to extract JNLP jars from $jar_file");
		return 0;
	}
	
	#Unsign and resign all jars
	foreach my $rel_sub_jar (@expand_list) {
		my $sub_jar=&::ConcatPathString($sub_expand_dir,$rel_sub_jar);
			
		#Remove any existing signature
		if (&JarFile::Unsign($sub_jar,$indent)!=1) {
			return 0;
		}
	
		#Re-sign the jar
		if (&JarFile::Sign($sub_jar,$indent)!=1) {
			return 0;
		}
	}

	push(@$expand_dirs_ref,$sub_expand_dir);
	
	return 1;
}


sub _AssembleLineWithUpdatedRev {
	my ($old_dinfo,$new_rev,$new_has_jar_manifest)=@_;
	my ($jm_dfile,$jm_sfile,$jm_ftype,$jm_frevision,$jm_fdesc,$jm_custom,$jm_fwp_rev,$jm_has_jar_manifest,@future)=&DecodeLine($old_dinfo);
	$jm_has_jar_manifest=$new_has_jar_manifest if $new_has_jar_manifest ne '';
	return &AssembleLine($jm_dfile,$jm_sfile,$jm_ftype,$new_rev,$jm_fdesc,$jm_custom,$jm_fwp_rev,$jm_has_jar_manifest,@future);
}

1;