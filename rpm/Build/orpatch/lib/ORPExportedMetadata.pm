############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################

use strict;
package ORPExportedMetadata;

#########################
# Sub:  Load
# Desc: Load an exported metadata area
# Args: None
# Ret:  1 on success, 0 on failure
#########################
sub Load {
	my $self=shift;

	&::LogMsg("Loading Metadata from ".$self->GetExportDir());
	
	my $map=$self->GetMetadataMapHandle();
	if ($map->LoadFile()!=1) {
		&::LogMsg("Unable to load metadata map");
		return 0;
	}
	
	if ($self->LoadEnvInfo()!=1) {
		&::LogMsg("Unable to load environment config in exported metadata");
		return 0;
	}
	
	if ($self->LoadInventory()!=1) {
		&::LogMsg("Unable to load patch inventory in exported metadata");
		return 0;
	}

	return 1;
}

sub GetEnvInfoHandle {
	my $self=shift;
	return $self->{'ENV_INFO'};
}

sub GetDestDir {
	my $self=shift;
	return $self->GetGlobalState()->GetDestDir();
}

sub GetGlobalState {
	my $self=shift;
	return $self->{'GLOBAL_STATE'};
}

sub GetExportDir {
	my $self=shift;
	return $self->{'SUPPORT_DIR'};
}

sub GetEnvironmentName {
	my $self=shift;
	return $self->{'ENV_NAME'};
}

sub GetInventoryHandle {
	my $self=shift;
	return $self->{'INV_HANDLE'};
}

# Returns (top_patch_ref,sub_patch_ref)
sub GetInventoryDetails {
	my $self=shift;
	return @{$self->{'_INV_DETAILS'}};
}

sub GetMetadataMapHandle {
	my $self=shift;
	return $self->{'METADATA_MAP'};
}

#########################
# Sub:  GetProductList
# Desc: Get the list of products installed in this exported metadata
# Args: None
# Ret:  ref to list of products
#########################
sub GetProductList {
	my $self=shift;
	my $env_cfg=$self->GetEnvInfoHandle();
	
	my @products=();
	#Having PRODUCT_<whatever>=Y in the environment info means the product should be installed
	foreach my $key ($env_cfg->Keys()) {
		if ($key=~/^PRODUCT_(.*)/) {
			my $product=$1;
			my $value=$env_cfg->Get($key);
			if ($value=~/^y$/i) {
				push(@products,$product);
			}
		}
	}
	
	return \@products;
}

#########################
# Sub:  GetActionList
# Desc: Get the list of actions that are not disabled here
# Args: None
# Ret:  ref to list of actions
#########################
sub GetActionList {
	my $self=shift;
	my $env_cfg=$self->GetEnvInfoHandle();
	
	my @actions=();
	#Having ACTION_<whatever>=<something other than N> in the environment info means the action is probably active
	foreach my $key ($env_cfg->Keys()) {
		if ($key=~/^ACTION_(.*)/) {
			my $action=$1;
			my $value=$env_cfg->Get($key);
			if ($value!~/^n$/i) {
				push(@actions,$action);
			}
		}
	}
	
	#Add ORPATCH since it is always valid on every environment
	push(@actions,'ORPATCH');
	
	return \@actions;
}

#########################
# Sub:  GetMetadataFileHash
# Desc: Get the list of metadata files
# Args: action filter,type filter
# Ret:  ref to hash of full filenames with values of relative file names
#########################
sub GetMetadataFileHash {
	my $self=shift;
	my ($action_filter,$type_filter)=@_;
	
	my $map=$self->GetMetadataMapHandle();
	my $list_ref=$map->GetMetadataFileList($action_filter,$type_filter);
	
	my %info=();
	foreach my $full_file (@$list_ref) {
		$info{$full_file}=$self->GetRelFilePath($full_file);
	}
	
	return \%info;
}

#####################################################################################	

#########################
# Sub:  LoadEnvInfo
# Desc: Get a handle to a ConfigFile pointed to the env_info.cfg for a particular environment
# Args: None
# Ret:  1 on success, 0 on failure
#########################
sub LoadEnvInfo {
	my $self=shift;
	
	my $map=$self->GetMetadataMapHandle();
	
	#Find the env_info.cfg file 
	my $cfg_file=$map->GetEnvInfoFile();
	
	if ($cfg_file eq '') {
		&::LogMsg("Unable to find exported environment config in metadata map");
		return 0;
	}
	
	if (!-f $cfg_file) {
		&::LogMsg("Unable to file exported environment config $cfg_file");
		return 0;
	}

	my $exported_cfg=new ConfigFile();	
	if ($exported_cfg->LoadFile($cfg_file)!=1) {
		&::LogMsg("Unable to read $cfg_file");
		return 0;
	}
	
	$self->{'ENV_INFO'}=$exported_cfg;
	
	#Setup our environment name
	my $env_name='UNKNOWN';
	
	my $env_var=$exported_cfg->Get('ENVIRONMENT_NAME');
	if ($env_var eq '') {
		#Couldn't find an environment name, use what they specified on the command-line
		my $src_name=$self->{'SRC_NAME'};
		if ($src_name ne '') {
			$env_name=$src_name;
		}
	} else {
		$env_name=$env_var;
	}
	
	$self->{'ENV_NAME'}=$env_name;
	
	return 1;
}

#########################
# Sub:  LoadInventory
# Desc: Setup a handle to the patch inventory
# Args: None
# Ret:  1 on success, 0 on failure
#########################
sub LoadInventory {
	my $self=shift;
	
	my $map=$self->GetMetadataMapHandle();
	
	#Find the inventory file 
	my $cfg_file=$map->GetInventoryFile();
	
	if ($cfg_file eq '') {
		&::LogMsg("Unable to find exported inventory in metadata map");
		return 0;
	}
	
	if (!-f $cfg_file) {
		&::LogMsg("Unable to find exported inventory file $cfg_file");
		return 0;
	}
	
	my $this_inventory=new ORPatchInventory($cfg_file);
	
	$self->{'INV_HANDLE'}=$this_inventory;
	
	my ($this_top_ref,$this_sub_ref)=$this_inventory->ReadInventoryFile(1);
	if ($this_top_ref eq '') {
		&::LogMsg("Failed to open inventory file $cfg_file");
		return 0;
	}
	
	$self->{'_INV_DETAILS'}=[$this_top_ref,$this_sub_ref];
	return 1;
}

#########################
# Sub:  GetRelExportPath
# Desc: Construct the patch to an file within our export area, based on the path relative to RETAIL_HOME
# Args: source file - the full path to a file under RETAIL_HOME
# Ret:  rel_name - full path to the same file within the support directory
#########################
sub GetRelExportPath {
	my $self=shift;
	my ($src_file)=@_;
	
	#strip the RETAIL_HOME part of the path
	my $rel_name=&::GetRelativePathName($src_file,$self->GetDestDir());
	#Prepend the path to our support dir
	$rel_name=&::ConcatPathString($self->GetExportDir(),$rel_name);
	
	return $rel_name;
}

#########################
# Sub:  GetRelFilePath
# Desc: Convert a filename within our export area into a relative filename
# Args: exported filename
# Ret:  rel_name - path relative to our exported directory
#########################
sub GetRelFilePath {
	my $self=shift;
	my ($exported_file)=@_;
	
	#Remove the export dir
	my $rel_name=&::GetRelativePathName($exported_file,$self->GetExportDir());
	
	return $rel_name;
}

#####################################################################
# Sub:  new
# Desc: Initialize an interface to an exported set of Metadata
# Args: support_dir,env_name,dest_dir,global_state
#####################################################################
sub new {
    my $class=shift;
	my ($support_dir,$env_name,$global_state)=@_;
    my $self={};
	
    bless($self,$class);

	$self->{'GLOBAL_STATE'}=$global_state;
	$self->{'SUPPORT_DIR'}=$support_dir;
	$self->{'ENV_NAME'}=$self->{'SRC_NAME'}=$env_name;
	$self->{'METADATA_MAP'}=new ORPMetadataMap($support_dir);
	
    return $self;
}

1;
