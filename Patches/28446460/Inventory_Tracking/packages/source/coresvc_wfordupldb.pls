CREATE OR REPLACE PACKAGE BODY CORESVC_WF_ORDER_UPLOAD_SQL AS
---------------------------------------------------------------------------------------------
LP_system_options       SYSTEM_OPTIONS%ROWTYPE;
LP_wf_order_new         SVC_WF_ORD_HEAD.PROCESS_STATUS%TYPE := 'N';
LP_wf_order_validate    SVC_WF_ORD_HEAD.PROCESS_STATUS%TYPE := 'V';
LP_wf_order_complete    SVC_WF_ORD_HEAD.PROCESS_STATUS%TYPE := 'C';
LP_wf_order_error       SVC_WF_ORD_HEAD.PROCESS_STATUS%TYPE := 'E';
LP_user                 SVCPROV_CONTEXT.USER_NAME%TYPE      := GET_USER;
LP_vdate                PERIOD.VDATE%TYPE                   := GET_VDATE;
---------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: VALIDATE_HEAD
-- Purpose: This private function will perform validation on Franchisee Order header record 
--          present in table SVC_WF_ORD_HEAD.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_HEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_process_id      IN       SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                       I_chunk_id        IN       SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: VALIDATE_DETAIL
-- Purpose: This private function will perform validation on Franchisee Order detail records 
--          present in table SVC_WF_ORD_DETAIL.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: APPROVE_WF_ORDER
-- Purpose: This private function will create the Franchisee Order and will approve the order
--          if there are no validation errors.Internally it will call WF_PO_SQL.CREATE_FRANCHISE_PO()
--          to create Purchase Order if source location is 'SU'pplier and 
--          WF_TRANSFER_SQL.BUILD_TSF_STORE_ORDER() to create Transfer and Store Orders .
---------------------------------------------------------------------------------------------
FUNCTION APPROVE_WF_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id      IN       SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                          I_chunk_id        IN       SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: ADD_CONTAINER_ITEMS
-- Purpose: This private function will add deposit container items to detail records for the valid
--          deposit content items present in Franchisee Order.
---------------------------------------------------------------------------------------------
FUNCTION ADD_CONTAINER_ITEMS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_order_detail_tbl   IN OUT OBJ_F_ORDUPLD_DTL_TBL,
                             I_wf_order_line_no    IN     WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: VALIDATE_BUYER_PACK
-- Purpose: This private function will validate if component items of buyer pack passed in
--          Franchisee Order already exists in Order.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_BUYER_PACK(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN     SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                             I_chunk_id        IN     SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-- Function Name: EXPLODE_BUYER_PACK
-- Purpose: This private function will explode buyer pack into svc_wf_ord_detail staging table.
---------------------------------------------------------------------------------------------
FUNCTION EXPLODE_BUYER_PACK(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN     SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                            I_chunk_id        IN     SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION WF_CREATE_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN AS

   L_program             VARCHAR2(50)    := 'CORESVC_WF_ORDER_UPLOAD_SQL.CREATE_FORDUPLD';
   L_table               VARCHAR2(50);

   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_WF_ORD_HEAD is
      select 'x'
        from svc_wf_ord_head
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         for update nowait;

   cursor C_LOCK_SVC_WF_ORD_DETAIL is
      select 'x'
        from svc_wf_ord_detail
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         for update nowait;

BEGIN

   L_table := 'SVC_WF_ORD_HEAD';
   open C_LOCK_SVC_WF_ORD_HEAD;
   close C_LOCK_SVC_WF_ORD_HEAD;

   L_table := 'SVC_WF_ORD_DETAIL';
   open C_LOCK_SVC_WF_ORD_DETAIL;
   close C_LOCK_SVC_WF_ORD_DETAIL;

   if VALIDATE_HEAD(O_error_message,
                    I_process_id,
                    I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if VALIDATE_DETAIL(O_error_message,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if VALIDATE_BUYER_PACK(O_error_message,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXPLODE_BUYER_PACK(O_error_message,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if APPROVE_WF_ORDER(O_error_message,
                       I_process_id,
                       I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('STG_TABLE_LOCKED',
                                             L_table,
                                             I_process_id,
                                             I_chunk_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END WF_CREATE_ORDER;
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_HEAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_process_id      IN     SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                       I_chunk_id        IN     SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN AS

   L_program             VARCHAR2(50)    := 'CORESVC_WF_ORDER_UPLOAD_SQL.WFORDUPLD_VAL_HEAD';
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   /* Validation covered for header records are as below :-
      * Currency Code passed should be a valid Currency Code
      * DEFAULT_BILL_TO_LOC should be a valid Franchise Store belonging to the customer id
      * CUSTOMER_ID should be a valid CUSTOMER_ID
*/

   merge into svc_wf_ord_head target
   using(
         select * from 
         (
          select swoh.rowid hdr_rowid,
                 case 
                    when cur.currency_code is NULL then 
                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_CURR',swoh.cust_ord_ref_no,swoh.wf_customer_id,swoh.line_no)||';'
                 end ||
                 case 
                    when wc.wf_customer_id is NULL then 
                    SQL_LIB.GET_MESSAGE_TEXT('WF_INV_CUSTOMER_ID',swoh.cust_ord_ref_no,swoh.wf_customer_id,swoh.line_no)||';'
                 end ||
                 case 
                    when swoh.default_bill_to_loc is NOT NULL and (s.store is NULL or NVL(s.wf_customer_id,-999) != swoh.wf_customer_id) then 
                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_BILL_LOC',swoh.cust_ord_ref_no,swoh.wf_customer_id,swoh.line_no)||';'
                 end err_msg
            from svc_wf_ord_head swoh,
                 currencies cur,
                 wf_customer wc,
                 (select store,
                         wf_customer_id
                    from store
                   where store_type = 'F') s
           where swoh.process_id = I_process_id
             and swoh.chunk_id = I_chunk_id
             and swoh.process_status = LP_wf_order_new
             and swoh.currency_code = cur.currency_code(+)
             and swoh.default_bill_to_loc = s.store(+)
             and swoh.wf_customer_id = wc.wf_customer_id(+)
         )
          where err_msg is not NULL
        ) source
   on (target.rowid = source.hdr_rowid)
   when matched then
      update set target.error_msg = SUBSTR(target.error_msg || source.err_msg, 1, 2000),
                 target.process_status = LP_wf_order_error,
                 target.last_update_datetime = SYSDATE,
                 target.last_update_id = LP_user;

   if SQL%ROWCOUNT > 0 then
      return FALSE;
   else
      update svc_wf_ord_head
         set process_status = LP_wf_order_validate,
             last_update_datetime = SYSDATE,
             last_update_id = LP_user
       where process_id = I_process_id
         and chunk_id = I_chunk_id;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_HEAD;
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN AS

   L_program             VARCHAR2(50)          := 'CORESVC_WF_ORDER_UPLOAD_SQL.VALIDATE_DETAIL';
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
      /* Validation covered for detail records are as below :-
        - Invalid field check for UOP
        - NEED_DATE should not be < VDATE
        - NOT_AFTER_DATE should be > than NEED_DATE
        - REQUESTED_QTY calculated based on UOP should not be <= 0
        - SOURCE_LOC_ID should be valid Store, Supplier or Physical warehouse
        - CUSTOMER_LOC should be a valid 'F'ranchise Store and item/customer location status should be 'A'pproved
        - Source location should not be a store for non stockholding customer location. This is for both source location
        -  provided or source location is null and item_loc.costing_loc_type = S.
        - CUSTOMER_ID passed in header record should belong to customer location passed in message
        - Item should exist in RMS and should be or should have a valid transactional level item
        - Item status must be approved and should be inventory item
        - Consignment/Concession Items are not allowed in Franchisee Order
        - Incase of Deposit item container item is not allowed
        - Incase of transformable item only orderable transformable items are allowed
        - Fixed cost should be passed if there is no cost template associated with item and fixed cost should always be
        -  non-negative value if passed in input file.
        - Incase of source_loc_id is 'ST'ore it should be a valid stockholding company store and should be ranged to item
        - Incase of source_loc_id is 'SU'pplier it should be a active DSD supplier in SUPS table and org_unit_id should be same 
        -  as that of costing location if system_options org_unit_ind is 'Y'. Item/SUPPLIER combination should exist
        - Incase of source_loc_id is Warehouse it should be a valid physical warehouse 
        - The status of item at the franchise store should be 'A' for supplier sourced and 'A', 'C' for store/warehouse sourced.
        - The stauts of item at the sending store or virtual warehouse cannot be in 'D' status.
        - Pack item cannot be ordered from source location 'ST'ore
        - Pack should be orderable in case of source location is a 'SU'pplier
        - Check for duplicate records for item,customer location and source location combination
      */

   -- To get the org unit indicator from system options
   if LP_system_options.org_unit_ind is NULL then
      if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                   LP_system_options) then
         return FALSE;
      end if;
   end if;
   ---

   merge into svc_wf_ord_detail target
   using (select * 
            from (select wi.d_rowid,
                        -- Invalid field value check (need date)
                        case
                           when wi.need_date < LP_vdate then
                              SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_NEED_DATE',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                        end ||
                        -- Invalid field value check (not after date)
                        case
                           when wi.not_after_date < wi.need_date then
                              SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_AFT_DT',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                        end ||
                       -- Invalid field value check (requested qty)
                        case 
                           when wi.im_item is NOT NULL and (wi.requested_qty <= 0 or wi.requested_qty is NULL) then
                              SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_REQ_QTY',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                        end ||
                        -- Invalid field value check (CUSTOMER_LOC)
                        case
                           when wi.store is NULL or wi.store_type != 'F' then
                              SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_CUST_LOC',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                           -- Check the customer location status is not inactive or delete for store/WH sourced order and is active for supplier sourced
                           when wi.cust_il_status is not NULL 
                              and ((NVL(wi.source_loc_type,'X') = 'SU' and cust_il_status <> 'A') 
                                 or (NVL(wi.source_loc_type,'X') <> 'SU' and cust_il_status in ('I','D')))then
                              SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_ITM_LOC',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                           -- Check source location cannot be store for non stockholding franchise store
                           when wi.stockholding_ind = 'N' and NVL(wi.source_loc_type,NVL(wi.costing_loc_type,'WH')) = 'ST' then
                              SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_SOURCE_LOC',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                        end ||
                        -- Invalid field value check (WF_CUSTOMER_ID)
                        case
                           when wi.store is NOT NULL and wi.s_wf_customer_id != wi.hdr_wf_customer_id then
                              SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_CUST_ID',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                        end ||
                        -- Check if ITEM exist
                        case
                           when wi.im_item is NULL then
                           SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_NO_ITM',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                        end ||
                        case
                           when wi.im_item is NOT NULL then
                              -- Invalid field value check (UOP)
                              case
                                 when wi.uop not in (wi.standard_uom, wi.pallet_name, wi.case_name, wi.inner_name) then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_UOP',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                                 when wi.uop = 'EA' and ROUND(wi.requested_qty) <> wi.requested_qty then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_DEC_QTY',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                              end ||
                              -- Check if ITEM is an approved item
                              case
                                 when wi.im_status != 'A' then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_ITM_NOT_APP',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                              end ||
                              -- Check if ITEM is inventory item
                               case
                                 when wi.inventory_ind != 'Y' then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_ITM_NON_INV',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                              end ||
                              -- Check if item level < tran level or ref item passed has transactional level parent item
                              case
                                 when wi.item_level < wi.tran_level or wi.txn_item is NULL then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_NO_TRAN_ITM',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                              end ||
                              -- Check if deposit item passed is container item
                              case
                                when NVL(wi.deposit_item_type,'E') = 'A' then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_DEP_ITM',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                              end ||
                              -- Check if ITEM passed is Concession/Consignment
                              case
                                when wi.purchase_type != 0 then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_CONSIGN_ITM',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                              end ||
                              --Incase there is no cost template associated fixed cost should not be NULL
                              case 
                                 when wi.fixed_cost is NULL and wi.templ_id is NULL then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_FXD_CST',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                                 when wi.fixed_cost is NOT NULL and wi.fixed_cost < 0 then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_NEG_FXD_CST',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                              end ||
                              case 
                                 --Pack item cannot be ordered from source location 'ST'ore
                                 when wi.pack_ind = 'Y' and ((wi.valid_source_loc_id is NOT NULL and wi.source_loc_type = 'ST') 
                                    or (wi.source_loc_type is null and wi.costing_loc_type = 'ST')) then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_SRC_ST_PCK',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                                 --Pack should be orderable in case of source location is a 'SU'pplier
                                 when wi.pack_ind = 'Y' and wi.valid_source_loc_id is NOT NULL and wi.source_loc_type = 'SU' and wi.orderable_ind = 'N' then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_PACK_NON_ORD',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                              end
                        end ||
                        case
                           when wi.source_loc_type is NOT NULL then
                              case 
                                 -- Invalid field value check (SOURCE_LOC_ID)
                                 when wi.valid_source_loc_id is NULL then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_SRC_LOC_ID',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                                 -- Incase of source loc id is STORE check for Item/Loc combination
                                 when wi.source_loc_type = 'ST' and wi.txn_item is not NULL and wi.src_il_item is NULL then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_ITM_LOC',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                                 -- Incase of source loc id is STORE check for Item/Loc status
                                 when wi.source_loc_type = 'ST' and wi.txn_item is not NULL and wi.src_il_item is not NULL and wi.src_il_status = 'D' then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_SRC_STATUS',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                                 -- Incase of source loc id is SUPPLIER check for Item/Supplier combination
                                 when wi.source_loc_type = 'SU' and wi.txn_item is not NULL and wi.is_item is NULL then
                                    SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_NO_ITM_SUPP',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                                 when wi.source_loc_type = 'SU' and wi.txn_item is not NULL and LP_system_options.org_unit_ind = 'Y' and wi.store is not NULL and wi.store_type = 'F'
                                     and NOT exists (select 'x'
                                                       from partner_org_unit p,
                                                            (select store location,
                                                                    'ST' loc_type,
                                                                    org_unit_id
                                                               from store 
                                                             UNION ALL
                                                             select wh location,
                                                                    'WH' loc_type,
                                                                    org_unit_id
                                                               from wh) loc
                                                      where p.partner = wi.source_loc_id
                                                        and NVL(wi.costing_loc, NVL(wi.default_wh,LP_system_options.wf_default_wh)) = loc.location
                                                        and NVL(wi.costing_loc_type, 'WH') = loc.loc_type
                                                        and p.org_unit_id = loc.org_unit_id) then
                                         SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_INV_ORG_UNT',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                                 end
                           when wi.txn_item is not NULL and wi.source_loc_type is NULL
                              and NOT exists (select 'x'
                                                from item_loc il
                                               where il.item = wi.txn_item
                                                 and il.loc  = NVL(wi.costing_loc, NVL(wi.default_wh,LP_system_options.wf_default_wh))) then
                              SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_ITM_NOT_RNG',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                        end ||
                        --Check for duplicate records
                        case 
                           when dup_seq != 1 then
                           SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_DUP_REQ',wi.cust_ord_ref_no,wi.hdr_wf_customer_id,wi.line_no)||';'
                        end err_msg
                   from (select dsv.d_rowid,
                                 dsv.process_id,
                                 dsv.wf_order_id,
                                 dsv.chunk_id,
                                 dsv.line_no,
                                 dsv.cust_ord_ref_no,
                                 dsv.hdr_wf_customer_id,
                                 dsv.customer_loc,
                                 dsv.source_loc_type,
                                 dsv.source_loc_id,
                                 s.store,
                                 s.store_type,
                                 s.default_wh,
                                 s.stockholding_ind,
                                 s.wf_customer_id s_wf_customer_id,
                                 dsv.item,
                                 dsv.im_item,
                                 dsv.txn_item,
                                 dsv.dept,
                                 dsv.status im_status,
                                 il1.item src_il_item,
                                 il1.status src_il_status,
                                 il.item il_item,
                                 DECODE(il.costing_loc_type,'S','ST','W','WH',il.costing_loc_type) costing_loc_type,
                                 il.costing_loc,
                                 il.source_wh source_wh,
                                 il.status cust_il_status,
                                 dsv.requested_qty,
                                 dsv.uop,
                                 iuop.item is_item,
                                 NVL(iuop.case_name,'-999') case_name,
                                 NVL(iuop.inner_name,'-999') inner_name,
                                 NVL(iuop.pallet_name,'-999') pallet_name,
                                 dsv.standard_uom,
                                 dsv.inventory_ind,
                                 dsv.item_xform_ind,
                                 dsv.item_level,
                                 dsv.tran_level,
                                 dsv.orderable_ind,
                                 dsv.deposit_item_type,
                                 dsv.need_date,
                                 dsv.not_after_date,
                                 dsv.pack_type,
                                 dsv.pack_ind,
                                 dsv.order_as_type,
                                 dsv.fixed_cost,
                                 ct.templ_id,
                                 dsv.purchase_type,
                                 valid_loc_tbl.source_loc_type valid_source_loc_type,
                                 valid_loc_tbl.source_loc_id valid_source_loc_id,
                                 row_number()
                                    over(partition by dsv.process_id,
                                                      dsv.wf_order_id,
                                                      dsv.item,
                                                      dsv.customer_loc,
                                                      NVL(dsv.source_loc_type,DECODE(il.costing_loc_type,'S','ST','WH')),
                                                      NVL(dsv.source_loc_id,NVL(il.costing_loc,NVL(s.default_wh,LP_system_options.wf_default_wh)))
                                             order by dsv.process_id) dup_seq
                            from (select ds.*,
                                         ds.rowid d_rowid,
                                         swoh.cust_ord_ref_no,
                                         swoh.wf_customer_id hdr_wf_customer_id,
                                         im.item im_item,
                                         im.status,
                                         im.item_parent,
                                         decode(SIGN(im.tran_level -im.item_level), -1, im.item_parent, im.item) txn_item,
                                         im.item_level,
                                         im.tran_level,
                                         im.orderable_ind,
                                         im.inventory_ind,
                                         im.item_xform_ind,
                                         im.deposit_item_type,
                                         im.standard_uom,
                                         im.dept,
                                         im.class,
                                         im.subclass,
                                         im.pack_ind,
                                         im.pack_type,
                                         im.order_as_type,
                                         d.purchase_type
                                    from svc_wf_ord_detail ds,
                                         svc_wf_ord_head swoh,
                                         item_master im,
                                         deps d
                                   where ds.process_id = I_process_id
                                     and ds.chunk_id = I_chunk_id
                                     and ds.process_id = swoh.process_id
                                     and ds.chunk_id = swoh.chunk_id
                                     and ds.wf_order_id = swoh.wf_order_id
                                     and ds.item = im.item(+)
                                     and NVL(im.dept,-999) = d.dept(+)) dsv,
                                 (select 'ST' source_loc_type,
                                     store source_loc_id
                                      from store
                                     where store_type = 'C'
                                       and stockholding_ind = 'Y'
                                  UNION ALL
                                  select 'WH' source_loc_type,
                                         physical_wh source_loc_id
                                    from wh
                                   where physical_wh = wh
                                 UNION ALL
                                 select 'SU' source_loc_type,
                                        supplier source_loc_id
                                   from sups
                                  where sup_status = 'A'
                                    and dsd_ind = 'Y') valid_loc_tbl,
                                 item_loc il,
                                 item_loc il1,
                                 store s,
                                 (select d.rowid d_rowid,
                                         isp.item,
                                         isp.case_name,
                                         isp.inner_name,
                                         isp.pallet_name
                                    from svc_wf_ord_detail d,
                                         item_supplier isp
                                   where d.process_id = I_process_id
                                     and d.chunk_id = I_chunk_id
                                     and d.source_loc_type = 'SU'
                                     and d.item = isp.item(+)
                                     and d.source_loc_id = isp.supplier(+)
                                  UNION
                                  select d.rowid d_rowid,
                                         isp.item,
                                         isp.case_name,
                                         isp.inner_name,
                                         isp.pallet_name
                                    from svc_wf_ord_detail d,
                                         item_supplier isp
                                   where d.process_id = I_process_id
                                     and d.chunk_id = I_chunk_id
                                     and d.source_loc_type <> 'SU'
                                     and d.item = isp.item(+)
                                     and NVL(isp.primary_supp_ind,'N') = 'Y') iuop,       -- To validate the UOP is correct. 
                                 (select item, 
                                         location, 
                                         templ_id,
                                         row_number() over (partition by item, location order by rnk asc) rnk
                                    from (select im.item,
                                                 cr.location,
                                                 cr.templ_id,
                                                 4 rnk
                                            from wf_cost_relationship cr,
                                                 item_master im
                                           where LP_vdate between start_date and end_date
                                             and cr.item = '-1'
                                             and cr.dept = im.dept
                                             and cr.class = '-1'
                                             and cr.subclass = '-1'
                                          UNION ALL
                                          select im.item,
                                                 cr.location,
                                                 cr.templ_id,
                                                 3 rnk
                                            from wf_cost_relationship cr,
                                                 item_master im
                                           where LP_vdate between start_date and end_date
                                             and cr.item = '-1'
                                             and cr.dept = im.dept
                                             and cr.class = im.class
                                             and cr.class != '-1'
                                             and cr.subclass = '-1'
                                          UNION ALL
                                          select im.item,
                                                 cr.location,
                                                 cr.templ_id,
                                                 2 rnk
                                            from wf_cost_relationship cr,
                                                 item_master im
                                           where LP_vdate between start_date and end_date
                                             and cr.item = '-1'
                                             and cr.class != '-1'
                                             and cr.subclass != '-1'
                                             and cr.dept = im.dept
                                             and cr.class = im.class
                                             and cr.subclass = im.subclass
                                          UNION ALL
                                          select cr.item,
                                                 cr.location,
                                                 cr.templ_id,
                                                 1 rnk
                                            from wf_cost_relationship cr
                                           where LP_vdate between start_date and end_date
                                             and cr.item <> '-1')) ct
                          where dsv.process_id = I_process_id
                            and dsv.chunk_id = I_chunk_id 
                            and dsv.customer_loc = s.store(+)
                            and NVL(dsv.txn_item, '-999') = il.item(+)                             -- item is ranged to franchise store. 
                            and dsv.customer_loc = il.loc(+)
                            and NVL(dsv.txn_item, '-999') = il1.item(+)                            -- item is ranged to source location (only store)
                            and NVL(dsv.source_loc_id,-999) = il1.loc(+)
                            and NVL(dsv.source_loc_id,-999) = valid_loc_tbl.source_loc_id(+)       -- Valid store/supp/physical wh
                            and NVL(dsv.source_loc_type,'WH') = valid_loc_tbl.source_loc_type(+)
                            and dsv.d_rowid = iuop.d_rowid (+)                                     -- Needs to be order item regardless of level
                            and NVL(dsv.txn_item, '-999') = ct.item(+)                              
                            and ct.rnk(+) = 1                                                      -- Pick wf_cost_relationship item row if exists else -1
                            and dsv.customer_loc = ct.location (+)) wi)
            where err_msg is not NULL) source
   on (target.rowid = source.d_rowid)
   when matched then
      update set target.error_msg = SUBSTR(target.error_msg || source.err_msg, 1, 2000),
                 target.last_update_datetime = SYSDATE,
                 target.last_update_id = LP_user;

   if SQL%ROWCOUNT > 0 then
      --Update process_status in svc_wf_ord_head to 'E'rror
      update svc_wf_ord_head
         set error_msg = error_msg||SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_ERR_DTL',NULL,NULL,NULL)||';',
             process_status = LP_wf_order_error,
             last_update_datetime = SYSDATE,
             last_update_id = LP_user
          where process_id =I_process_id
            and chunk_id = I_chunk_id;

      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_DETAIL;
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_BUYER_PACK(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN     SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                             I_chunk_id        IN     SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN AS

   L_program            VARCHAR2(50)          := 'CORESVC_WF_ORDER_UPLOAD_SQL.VALIDATE_BUYER_PACK';

BEGIN
   -- Buyer pack order as eaches will be exploded to its components before adding to order.
   -- If the component already exists in the upload file for the same from loc/to-loc, error out.
   merge into svc_wf_ord_detail target
   using (select swod.rowid swod_rowid
            from svc_wf_ord_detail swod,
                 item_master im
           where swod.process_id  = I_process_id
             and swod.chunk_id    = I_chunk_id
             and swod.item        = im.item
             and im.pack_ind      = 'Y'
             and im.pack_type     = 'B'
             and im.order_as_type = 'E'
             and exists (select 'x'
                           from svc_wf_ord_detail swod1,
                                v_packsku_qty vpq
                          where swod1.process_id      = swod.process_id
                            and swod1.chunk_id        = swod.chunk_id
                            and swod1.source_loc_id   = swod.source_loc_id
                            and swod1.source_loc_type = swod.source_loc_type
                            and swod1.customer_loc    = swod.customer_loc
                            and vpq.pack_no           = swod.item
                            and vpq.item              = swod1.item
                         UNION ALL
                         select 'x'                                        -- check if component exists as a component of another buyer pack in the file
                           from svc_wf_ord_detail swod1,
                                item_master im1,
                                v_packsku_qty vpq1,
                                v_packsku_qty vpq
                          where swod1.process_id      = swod.process_id
                            and swod1.chunk_id        = swod.chunk_id
                            and swod1.source_loc_id   = swod.source_loc_id
                            and swod1.source_loc_type = swod.source_loc_type
                            and swod1.customer_loc    = swod.customer_loc
                            and vpq.pack_no           = swod.item
                            and swod.item            != swod1.item
                            and swod1.item            = im1.item
                            and im1.pack_ind          = 'Y'
                            and im1.pack_type         = 'B'
                            and im1.order_as_type     = 'E'
                            and swod1.item            = vpq1.pack_no
                            and vpq.item              = vpq1.item)) source
   on (target.rowid = source.swod_rowid)
   when matched then
      update set target.error_msg            = target.error_msg || SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_PCK_COMP_EXST',NULL,NULL,NULL)||';',
                 target.last_update_datetime = SYSDATE,
                 target.last_update_id       = LP_user;

   if SQL%ROWCOUNT > 0 then
      update svc_wf_ord_head
         set error_msg = error_msg||SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_ERR_DTL',NULL,NULL,NULL)||';',
             process_status = LP_wf_order_error,
             last_update_datetime = SYSDATE,
             last_update_id = LP_user
       where process_id =I_process_id
         and chunk_id = I_chunk_id;

      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_BUYER_PACK;
---------------------------------------------------------------------------------------------
FUNCTION EXPLODE_BUYER_PACK(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN     SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                            I_chunk_id        IN     SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN AS

   L_program                VARCHAR2(50)          := 'CORESVC_WF_ORDER_UPLOAD_SQL.EXPLODE_BUYER_PACK';
   L_item_row               ITEM_MASTER%ROWTYPE;
   L_ti                     ITEM_SUPP_COUNTRY.TI%TYPE;
   L_hi                     ITEM_SUPP_COUNTRY.HI%TYPE;
   L_inner_pack_size        ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE;
   L_pallet_desc            CODE_DETAIL.CODE_DESC%TYPE;
   L_case_desc              CODE_DETAIL.CODE_DESC%TYPE;
   L_inner_desc             CODE_DETAIL.CODE_DESC%TYPE;
   L_pallet_name            CODE_DETAIL.CODE%TYPE;
   L_case_name              CODE_DETAIL.CODE%TYPE;
   L_inner_name             CODE_DETAIL.CODE%TYPE;
   L_isc_supp_pack_size     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_supplier               SUPS.SUPPLIER%TYPE := NULL;
   L_pack_cost_prim         ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_comp_cost_prim         ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_std_unit_retail_prim   ITEM_LOC.UNIT_RETAIL%TYPE;
   L_std_uom_prim           ITEM_MASTER.STANDARD_UOM%TYPE;
   L_sell_unit_retail_prim  ITEM_LOC.UNIT_RETAIL%TYPE;
   L_sell_uom_prim          ITEM_MASTER.STANDARD_UOM%TYPE;

   cursor C_EXP_BUY_PACK is
      select swod.rowid swod_rowid,
             swod.wf_order_id,
             swod.process_id,
             swod.chunk_id,
             swod.record_descriptor,
             swod.item,
             vpq.item comp_item,
             swod.customer_loc,
             swod.source_loc_type,
             swod.source_loc_id,
             swod.requested_qty,
             swod.uop,
             swod.fixed_cost,
             swod.need_date,
             swod.not_after_date,
             im.dept,
             im.class,
             im.subclass,
             vpq.qty pack_qty
        from svc_wf_ord_detail swod,
             item_master im,
             v_packsku_qty vpq
       where swod.process_id = I_process_id
         and swod.chunk_id = I_chunk_id
         and swod.item = im.item
         and im.pack_ind = 'Y'
         and im.pack_type = 'B'
         and im.order_as_type = 'E'
         and im.item = vpq.pack_no;

   TYPE TYP_buyer_pack           is TABLE of C_EXP_BUY_PACK%ROWTYPE;
   TBL_buyer_pack                TYP_buyer_pack;

BEGIN

   open C_EXP_BUY_PACK;
   fetch C_EXP_BUY_PACK BULK COLLECT into TBL_buyer_pack;
   close C_EXP_BUY_PACK;

   if TBL_buyer_pack is NOT NULL and TBL_buyer_pack.COUNT > 0 then
      FOR i in TBL_buyer_pack.FIRST..TBL_buyer_pack.LAST LOOP
         if TBL_buyer_pack(i).source_loc_type = 'SU' then
            L_supplier :=  TBL_buyer_pack(i).source_loc_id;
         else
            L_supplier := NULL;
         end if;

         -- If the buyer pack UOP is not in standard UOM, convert the requested quantity to standard UOM of buyer pack.
         if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                            L_item_row,
                                            TBL_buyer_pack(i).item) = FALSE then
            return FALSE;
         end if;

         if L_item_row.standard_uom <> TBL_buyer_pack(i).uop then
            --- Get pallet and case size, desc, and name fields
            if SUPP_ITEM_ATTRIB_SQL.GET_PACK_SIZES (O_error_message,
                                                    L_ti,
                                                    L_hi,
                                                    L_isc_supp_pack_size,
                                                    L_inner_pack_size,
                                                    L_pallet_desc,
                                                    L_case_desc,
                                                    L_inner_desc,
                                                    L_pallet_name,
                                                    L_case_name,
                                                    L_inner_name,
                                                    TBL_buyer_pack(i).item,
                                                    L_supplier,
                                                    NULL) = FALSE then
               return FALSE;
            end if;

            if TBL_buyer_pack(i).uop = L_inner_name then
               TBL_buyer_pack(i).requested_qty := TBL_buyer_pack(i).requested_qty * L_inner_pack_size;
            elsif TBL_buyer_pack(i).uop = L_case_name then
               TBL_buyer_pack(i).requested_qty := TBL_buyer_pack(i).requested_qty * L_isc_supp_pack_size;
            elsif TBL_buyer_pack(i).uop = L_pallet_name then
               TBL_buyer_pack(i).requested_qty := TBL_buyer_pack(i).requested_qty * L_isc_supp_pack_size * L_ti * L_hi;
            end if;
         end if;

         -- The Component item UOP will be Standard UOM. 
         if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                            L_item_row,
                                            TBL_buyer_pack(i).comp_item) = FALSE then
            return FALSE;
         end if;
         TBL_buyer_pack(i).requested_qty := TBL_buyer_pack(i).requested_qty * TBL_buyer_pack(i).pack_qty;
         TBL_buyer_pack(i).uop           := L_item_row.standard_uom;

         -- Prorate the fixed cost if populated to its component. Buyer pack proration will use component supplier cost. 
         if TBL_buyer_pack(i).fixed_cost is NOT NULL then
           -- Get packs unit cost
           if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                                   L_pack_cost_prim,
                                                   L_std_unit_retail_prim,
                                                   L_std_uom_prim,
                                                   L_sell_unit_retail_prim,
                                                   L_sell_uom_prim,
                                                   TBL_buyer_pack(i).item,
                                                   NULL) = FALSE then
              return FALSE;
           end if;

           -- Get Component unit cost
           if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                                   L_comp_cost_prim,
                                                   L_std_unit_retail_prim,
                                                   L_std_uom_prim,
                                                   L_sell_unit_retail_prim,
                                                   L_sell_uom_prim,
                                                   TBL_buyer_pack(i).comp_item,
                                                   NULL) = FALSE then
              return FALSE;
           end if;

           -- Prorate packs fixed cost to component 
           if L_pack_cost_prim is NULL or L_pack_cost_prim = 0 then
              L_pack_cost_prim := 1;      -- handle divide by zero
           end if;
           TBL_buyer_pack(i).fixed_cost := TBL_buyer_pack(i).fixed_cost * NVL(L_comp_cost_prim,0) / L_pack_cost_prim;
         end if;
      END LOOP;

      FORALL i in TBL_buyer_pack.FIRST..TBL_buyer_pack.LAST
         insert into svc_wf_ord_detail (wf_order_id,
                                        process_id,
                                        chunk_id,
                                        record_descriptor,
                                        line_no,
                                        item,
                                        customer_loc,
                                        source_loc_type,
                                        source_loc_id,
                                        requested_qty,
                                        uop,
                                        fixed_cost,
                                        need_date,
                                        not_after_date,
                                        error_msg,
                                        create_datetime,
                                        create_id,
                                        last_update_datetime,
                                        last_update_id)
                                values (TBL_buyer_pack(i).wf_order_id,
                                        TBL_buyer_pack(i).process_id,
                                        TBL_buyer_pack(i).chunk_id,
                                        TBL_buyer_pack(i).record_descriptor,
                                        NULL,
                                        TBL_buyer_pack(i).comp_item,
                                        TBL_buyer_pack(i).customer_loc,
                                        TBL_buyer_pack(i).source_loc_type,
                                        TBL_buyer_pack(i).source_loc_id,
                                        TBL_buyer_pack(i).requested_qty,
                                        TBL_buyer_pack(i).uop,
                                        TBL_buyer_pack(i).fixed_cost,
                                        TBL_buyer_pack(i).need_date,
                                        TBL_buyer_pack(i).not_after_date,
                                        NULL,
                                        SYSDATE,
                                        LP_user,
                                        SYSDATE,
                                        LP_user);

      -- Delete buyer pack with order_as_type = 'E'aches from staging table
      FORALL i in TBL_buyer_pack.FIRST..TBL_buyer_pack.LAST
         delete from svc_wf_ord_detail
            where rowid = TBL_buyer_pack(i).swod_rowid;

   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXPLODE_BUYER_PACK;
---------------------------------------------------------------------------------------------
FUNCTION APPROVE_WF_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id      IN      SVC_WF_ORD_HEAD.PROCESS_ID%TYPE,
                          I_chunk_id        IN      SVC_WF_ORD_HEAD.CHUNK_ID%TYPE)
   RETURN BOOLEAN AS

   L_program           VARCHAR2(50)                            := 'CORESVC_WF_ORDER_UPLOAD_SQL.APPROVE_WF_ORDER';
   L_wf_order_id       SVC_WF_ORD_HEAD.WF_ORDER_ID%TYPE        := NULL;
   L_cust_ord_ref_no   WF_ORDER_HEAD.CUST_ORD_REF_NO%TYPE      := NULL;
   L_order_no          WF_ORDER_HEAD.WF_ORDER_NO%TYPE          := NULL;
   L_currency_code     WF_ORDER_HEAD.CURRENCY_CODE%TYPE        := NULL;
   L_need_date         WF_ORDER_DETAIL.NEED_DATE%TYPE          := NULL;
   L_not_after_date    WF_ORDER_DETAIL.NOT_AFTER_DATE%TYPE     := NULL;
   L_requested_qty     WF_ORDER_DETAIL.REQUESTED_QTY%TYPE      := NULL;
   L_str_ord_qty       WF_ORDER_DETAIL.REQUESTED_QTY%TYPE      := NULL;
   L_avail_qty         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE         := NULL;
   L_store_ord_exists  VARCHAR2(1)                             := NULL;
   L_pack_ind          ITEM_MASTER.SIMPLE_PACK_IND%TYPE        := NULL;
   L_repl_ind          VARCHAR2(1)                             := NULL;
   L_source_loc_type   SVC_WF_ORD_DETAIL.SOURCE_LOC_TYPE%TYPE  := NULL;
   L_source_loc_id     SVC_WF_ORD_DETAIL.SOURCE_LOC_ID%TYPE    := NULL;
   L_source_loc_status ITEM_LOC.STATUS%TYPE                    := NULL;
   L_virtual_wh        SVC_WF_ORD_DETAIL.SOURCE_LOC_ID%TYPE    := NULL; 
   L_exchange_type     CURRENCY_RATES.EXCHANGE_TYPE%TYPE       := NULL;
   L_lead_time         NUMBER;
   L_undo_scaling      VARCHAR2(1)                             := 'N';
   L_scaling           VARCHAR2(1)                             := 'N';
   L_create_po_ind     VARCHAR2(1)                             := 'N';
   L_cust_cost         FUTURE_COST.PRICING_COST%TYPE           := NULL;
   L_acq_cost          FUTURE_COST.ACQUISITION_COST%TYPE       := NULL;
   L_header_cnt        NUMBER                                  := 0;
   L_wf_order_line_no  WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE   := NULL;
   L_wf_tsf_ind        VARCHAR2(1)                             := 'N';
   L_wf_str_ord_ind    VARCHAR2(1)                             := 'N';
   L_approve_ind       VARCHAR2(1)                             := 'Y';
   L_fo_exists         VARCHAR2(1)                             := 'N';
   L_detail_rec_count  NUMBER(10)                              := 0;
   L_count_dtl_in_file NUMBER(10);
   L_diff_costing_loc  VARCHAR2(1)                             := NULL;
   L_diff_repl_wh      VARCHAR2(1)                             := NULL;
   L_tot_prev_tsf_qty  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE         := 0;
   L_prev_source_id    SVC_WF_ORD_DETAIL.SOURCE_LOC_ID%TYPE    := NULL;
   L_prev_source_type  SVC_WF_ORD_DETAIL.SOURCE_LOC_TYPE%TYPE  := NULL;
   L_prev_item         SVC_WF_ORD_DETAIL.ITEM%TYPE             := NULL;

   L_source_loc_tbl    OBJ_F_ITEM_SOURCE_LOC_TBL               := OBJ_F_ITEM_SOURCE_LOC_TBL();
   L_wf_tsf_tbl        OBJ_F_TRANSFER_TBL                      := OBJ_F_TRANSFER_TBL();
   L_wf_str_ord_tbl    OBJ_F_STORE_ORDER_TBL                   := OBJ_F_STORE_ORDER_TBL();
   L_wford_dtl_rec     OBJ_F_ORDUPLD_DTL_REC                   := OBJ_F_ORDUPLD_DTL_REC();
   L_wford_dtl_tbl     OBJ_F_ORDUPLD_DTL_TBL                   := OBJ_F_ORDUPLD_DTL_TBL();

   cursor C_GET_ORD_HEAD is
      select wf_order_id,
             wf_rma_no_seq.nextval wf_order_no,
             swoh.wf_customer_id,
             swoh.cust_ord_ref_no,
             'E' order_type,     --EDI
             swoh.currency_code,
             cur.exchange_rate,
             NULL freight,
             decode(swoh.default_bill_to_loc, NULL, 'Y', 'N') all_location_billing,
             swoh.default_bill_to_loc,
             swoh.comments,
             ad.bill_to_addr_type,
             wc.credit_ind,
             wc.auto_approve_ind
        from svc_wf_ord_head swoh,
             wf_customer wc,
             (select currency_code,
                     exchange_rate,
                     rank () over (partition by currency_code order by effective_date desc) rnk
               from currency_rates cr
              where cr.exchange_type = L_exchange_type
                and cr.effective_date <= LP_vdate)cur,
             (select a.addr_type bill_to_addr_type,
                     a.key_value_1
                from addr a,
                     add_type_module m
               where a.module = 'ST'
                 and m.module = 'ST'
                 and m.address_type = a.addr_type
                 and m.primary_ind = 'Y'
                 and a.primary_addr_ind='Y') ad
      where swoh.process_id = I_process_id
        and swoh.chunk_id = I_chunk_id
        and swoh.process_status = LP_wf_order_validate
        and swoh.wf_customer_id = wc.wf_customer_id
        and NVL(swoh.default_bill_to_loc,-99) = ad.key_value_1(+)
        and swoh.currency_code = cur.currency_code
        and cur.rnk = 1;

   cursor C_GET_SOURCE_LOC is
      select OBJ_F_ITEM_SOURCE_LOC_REC
               (item,
                customer_loc,
                source_loc_type,
                source_loc_id,
                loc_type,
                loc_id,
                txn_item)
        from 
            (select swod.item,
                    swod.customer_loc customer_loc,
                    swod.source_loc_type,
                    swod.source_loc_id,
                    case 
                       when swod.source_loc_type = 'WH' then NULL
                       else swod.source_loc_type
                    end                                           loc_type,
                    case 
                       when swod.source_loc_type = 'WH' then NULL
                       else swod.source_loc_id
                    end                                           loc_id,      -- Will be populated as VWH by the call to DISTRIBUTION_SQL
                    decode(SIGN(im.tran_level -im.item_level), -1, im.item_parent, im.item) txn_item
               from svc_wf_ord_detail swod,
                    item_master im
              where swod.process_id = I_process_id
                and swod.chunk_id   = I_chunk_id
                and swod.wf_order_id = L_wf_order_id
                and swod.item = im.item
                and swod.source_loc_id is NOT NULL
             UNION 
             select swod.item,
                    swod.customer_loc customer_loc,
                    NULL source_loc_type,
                    NULL source_loc_id,
                    NVL(DECODE(il.costing_loc_type,'S','ST','W','WH'), 'WH')               loc_type,
                    NVL(il.costing_loc,NVL(s.default_wh,LP_system_options.wf_default_wh))  loc_id,
                    swod.txn_item
               from (select wd.item,
                            wd.customer_loc customer_loc,
                            decode(SIGN(im.tran_level -im.item_level), -1, im.item_parent, im.item) txn_item
                       from svc_wf_ord_detail wd,
                            item_master im
                      where wd.process_id = I_process_id
                        and wd.chunk_id   = I_chunk_id
                        and wd.wf_order_id = L_wf_order_id
                        and wd.item = im.item
                        and wd.source_loc_id is NULL )swod,
                     item_loc il,
                     store s
              where swod.customer_loc = il.loc(+)
                and swod.txn_item = il.item(+)
                and swod.customer_loc = s.store);

      cursor C_GET_ORD_DETAIL is
         select di.txn_item item,
                di.customer_loc,
                'S' customer_loc_type,
                di.loc_type source_loc_type,       -- Input source_loc_type or costing location type
                di.loc_id   source_loc_id,         -- Input source_loc_id or costing location id or virtual warehouse
                il.status   source_loc_status,
                case
                   when di.uop = supsz.inner_name then
                      di.requested_qty * supsz.inner_pack_size
                   when di.uop = supsz.case_name then
                      di.requested_qty * supsz.supp_pack_size
                   when di.uop = supsz.pallet_name then
                      di.requested_qty * supsz.supp_pack_size * supsz.ti * supsz.hi
                   else
                      di.requested_qty
                end requested_qty,
                di.uop,
                di.need_date,
                di.not_after_date,
                di.avail_qty,
                di.fc_curr_code,
                di.acquisition_cost,
                di.customer_cost,
                di.fixed_cost,
                tmpl.templ_id,
                tmpl.margin_pct,
                tmpl.calc_type,
                tmpl.templ_desc,
                im.pack_ind,
                NVL(di.store_ord_exists,'N') store_ord_exists,
                di.repl_ind,
                di.lead_time,
                di.supp_case_chk,
                count(*) over (partition by di.txn_item, di.customer_loc, di.loc_type, di.loc_id) cnt
          from  item_master im,
                item_loc il,
                ((select swod.*,
                         tbl.txn_item,
                         tbl.loc_id,
                         tbl.loc_type,
                         tbl.lead_time,
                         case
                            when
                               (case when swod.uop = isp.inner_name then
                                  MOD((swod.requested_qty * isc.inner_pack_size),isc.supp_pack_size)
                               when swod.uop = isp.case_name or swod.uop = isp.pallet_name then
                                  0
                               else
                                  MOD(swod.requested_qty,isc.supp_pack_size)
                               end) = 0 then 'Y'
                            else
                               'N'
                         end supp_case_chk,
                         NULL avail_qty,
                         NULL repl_ind,
                         NULL store_ord_exists,
                         fc.currency_code fc_curr_code,
                         fc.acquisition_cost,
                         fc.pricing_cost customer_cost,
                         rank () over (partition by tbl.item, tbl.loc_id, tbl.customer_loc order by fc.active_date desc) rnk
                    from svc_wf_ord_detail swod,
                         item_supplier isp,
                         item_supp_country isc,
                         (select sloc.item, 
                                 sloc.source_loc_type,
                                 sloc.source_loc_id,
                                 sloc.loc_type,
                                 sloc.loc_id,
                                 sloc.txn_item,
                                 sloc.customer_loc,
                                 isc.origin_country_id,
                                 NVL(isc.lead_time,0) lead_time
                            from TABLE(CAST(L_source_loc_tbl as OBJ_F_ITEM_SOURCE_LOC_TBL)) sloc,
                                 item_supp_country isc
                           where sloc.item = isc.item
                             and sloc.source_loc_type = 'SU'
                             and sloc.source_loc_id = isc.supplier
                             and isc.primary_country_ind = 'Y') tbl,
                         future_cost fc
                   where swod.process_id = I_process_id
                     and swod.chunk_id = I_chunk_id
                     and swod.wf_order_id = L_wf_order_id
                     and swod.source_loc_type is NOT NULL
                     and swod.source_loc_type = 'SU'
                     and swod.source_loc_type = tbl.source_loc_type
                     and swod.source_loc_id = tbl.source_loc_id
                     and swod.customer_loc = tbl.customer_loc
                     and swod.item = tbl.item
                     and swod.item = isp.item
                     and swod.source_loc_id = isp.supplier
                     and isp.supplier = isc.supplier
                     and isp.item = isc.item
                     and isc.primary_country_ind = 'Y'
                     and tbl.txn_item = fc.item (+)
                     and tbl.customer_loc = fc.location (+)
                     and tbl.source_loc_id = fc.supplier(+)
                     and tbl.origin_country_id = fc.origin_country_id(+)
                     and fc.active_date(+) <= LP_vdate)
                 UNION ALL
                 (select swod.*,
                         tbl.txn_item,
                         tbl.loc_id,
                         tbl.loc_type,
                         0    lead_time,
                         NULL supp_case_rounding_chk,
                         GREATEST(ils.stock_on_hand - (GREATEST(ils.tsf_reserved_qty,0)
                                                     + GREATEST(ils.customer_resv,0)
                                                     + GREATEST(ils.rtv_qty,0)
                                                     + GREATEST(ils.non_sellable_qty,0)),0) avail_qty,
                         NULL repl_ind,
                         NULL store_ord_exists,
                         fc.currency_code fc_curr_code,
                         fc.acquisition_cost,
                         fc.pricing_cost,
                         rank () over (partition by tbl.item, tbl.loc_id, tbl.customer_loc order by fc.active_date desc) rnk
                    from svc_wf_ord_detail swod,
                         item_loc_soh ils,
                         TABLE(CAST(L_source_loc_tbl as OBJ_F_ITEM_SOURCE_LOC_TBL)) tbl,
                         future_cost fc
                   where swod.process_id = I_process_id
                     and swod.chunk_id = I_chunk_id
                     and swod.wf_order_id = L_wf_order_id
                     and ((swod.source_loc_type is not NULL and swod.source_loc_type = 'ST')
                          or tbl.loc_type = 'ST')                                       
                     and swod.item = tbl.item
                     and swod.customer_loc = tbl.customer_loc
                     and NVL(swod.source_loc_id,-999) = NVL(tbl.source_loc_id,-999)
                     and NVL(swod.source_loc_type,'x') = NVL(tbl.source_loc_type,'x')
                     and tbl.txn_item = ils.item
                     and tbl.loc_id = ils.loc
                     and tbl.txn_item = fc.item (+)
                     and tbl.customer_loc = fc.location (+)
                     and fc.primary_supp_country_ind (+) = 'Y'
                     and fc.active_date(+) <= LP_vdate)
                 UNION ALL
                 (select swod.*,
                         tbl.txn_item,
                         tbl.loc_id,
                         tbl.loc_type,
                         tbl.wh_lead_time + tbl.pickup_lead_time + NVL(isc.lead_time,0)+ NVL(il.inbound_handling_days,0) lead_time,
                         NULL supp_case_rounding_chk,
                         GREATEST(ils.stock_on_hand - (GREATEST(ils.tsf_reserved_qty,0)
                                                     + GREATEST(ils.customer_resv,0)
                                                     + GREATEST(ils.rtv_qty,0)
                                                     + GREATEST(ils.non_sellable_qty,0)),0) avail_qty,
                         case when ril.repl_method is NULL
                                 then 'N'
                              when ril.reject_store_ord_ind = 'Y' and NVL(ril.next_delivery_date,LP_vdate) < swod.not_after_date 
                                 then 'N'
                              else 'Y'
                         end repl_ind,
                         NVL2(so.store,'Y','N') store_ord_exists,
                         fc.currency_code fc_curr_code,
                         fc.acquisition_cost,
                         fc.pricing_cost,
                         rank () over (partition by tbl.item, tbl.loc_id, tbl.customer_loc order by fc.active_date desc) rnk
                    from svc_wf_ord_detail swod,
                         item_loc_soh ils,
                         item_loc il,
                         (select wod.rowid wod_rowid,
                                 wod.customer_loc,
                                 wod.need_date,
                                 sloc.item, 
                                 sloc.source_loc_type,
                                 sloc.source_loc_id,
                                 sloc.loc_type,
                                 sloc.loc_id,
                                 sloc.txn_item,
                                 NVL(ril.wh_lead_time,0) wh_lead_time,
                                 NVL(ril.pickup_lead_time,0) pickup_lead_time,
                                 ril.primary_repl_supplier,
                                 ril.origin_country_id
                            from svc_wf_ord_detail wod,
                                 TABLE(CAST(L_source_loc_tbl as OBJ_F_ITEM_SOURCE_LOC_TBL)) sloc,
                                 repl_item_loc ril
                           where wod.process_id                = I_process_id
                             and wod.chunk_id                  = I_chunk_id
                             and wod.wf_order_id               = L_wf_order_id
                             and ((wod.source_loc_type is not NULL and wod.source_loc_type = 'WH')
                                  or sloc.loc_type = 'WH')
                             and wod.item                      = sloc.item
                             and wod.customer_loc              = sloc.customer_loc
                             and NVL(wod.source_loc_id,-999)   = NVL(sloc.source_loc_id,-999)
                             and NVL(wod.source_loc_type,'x')  = NVL(sloc.source_loc_type,'x')
                             and sloc.txn_item                 = ril.item(+)
                             and sloc.customer_loc             = ril.location(+)
                             and ril.repl_method (+)           = 'SO'     -- Store Orders
                             and ril.stock_cat (+)             = 'L'      -- WH/Cross linked
                            ) tbl,
                         item_supp_country isc,
                         repl_item_loc ril,
                         store_orders so,
                         future_cost fc
                   where swod.rowid    = tbl.wod_rowid
                     and tbl.txn_item  = ils.item
                     and tbl.loc_id    = ils.loc
                     and il.item       = ils.item
                     and il.loc        = ils.loc
                     and tbl.item      = isc.item
                     and isc.supplier  = NVL(tbl.primary_repl_supplier, isc.supplier)
                     and isc.origin_country_id = NVL(tbl.origin_country_id, isc.origin_country_id)
                     and (tbl.primary_repl_supplier is NOT NULL 
                          OR (    tbl.primary_repl_supplier is NULL  
                              and isc.primary_supp_ind = 'Y' 
                              and isc.primary_country_ind = 'Y'))
                     and tbl.txn_item        = ril.item(+)
                     and tbl.customer_loc    = ril.location(+)
                     and ril.repl_method (+) = 'SO'
                     and ril.stock_cat (+)   = 'L'
                     and tbl.txn_item        = so.item (+)
                     and tbl.customer_loc    = so.store (+)
                     and tbl.need_date       = so.need_date(+)
                     and tbl.txn_item        = fc.item (+)
                     and tbl.customer_loc    = fc.location (+)
                     and fc.primary_supp_country_ind (+) = 'Y'
                     and fc.active_date(+)  <= LP_vdate)) di,
                (select inner1.templ_id templ_id,
                        inner1.templ_desc templ_desc,
                        inner1.first_applied calc_type,
                        inner1.margin_pct margin_pct,
                        inner1.item,
                        inner1.loc_id,
                        inner1.loc_type,
                        inner1.customer_loc,
                        row_number() over (partition by inner1.item,
                                                        inner1.loc_id,
                                                        inner1.loc_type,
                                                        inner1.customer_loc
                                               order by inner1.rnk_item) rnum
                   from (select wcr.templ_id ,
                                wct.templ_desc,
                                wct.first_applied,
                                wct.margin_pct,
                                sloc.item,
                                sloc.loc_id,
                                sloc.loc_type,
                                sloc.customer_loc,
                                case 
                                when (wcr.class  = '-1' and wcr.subclass  = '-1' and wcr.item = '-1') then 4
                                when (wcr.class != '-1' and wcr.subclass  = '-1' and wcr.item = '-1') then 3
                                when (wcr.class != '-1' and wcr.subclass != '-1' and wcr.item = '-1') then 2
                                else 1
                                end rnk_item
                           from wf_cost_buildup_tmpl_head wct,
                                wf_cost_relationship wcr,
                                item_master im,
                                TABLE(CAST(L_source_loc_tbl as OBJ_F_ITEM_SOURCE_LOC_TBL)) sloc
                          where im.item        = sloc.txn_item
                            and wcr.location   = sloc.customer_loc
                            and DECODE(wcr.item,'-1',im.item,wcr.item) = im.item
                            and im.dept        = wcr.dept
                            and im.class       = decode(wcr.class,'-1',im.class,wcr.class)
                            and im.subclass    = decode(wcr.subclass,'-1',im.subclass,wcr.class)
                            and wct.templ_id   = wcr.templ_id
                            and wcr.start_date <= LP_vdate
                            and wcr.end_date   >= LP_vdate)inner1) tmpl,
                (select isc.origin_country_id,
                        isc.ti,
                        isc.hi,
                        isc.supp_pack_size,
                        isc.inner_pack_size,
                        isp.pallet_name,
                        isp.case_name,
                        isp.inner_name,
                        sloc.item,
                        sloc.loc_id,
                        sloc.loc_type,
                        sloc.customer_loc
                   from item_supplier isp,
                        item_supp_country isc,
                        TABLE(CAST(L_source_loc_tbl as OBJ_F_ITEM_SOURCE_LOC_TBL)) sloc
                  where isp.item = sloc.item
                    and isp.item = isc.item
                    and isp.supplier = isc.supplier
                    and isc.primary_country_ind = 'Y'
                    and ((sloc.source_loc_type = 'SU' and sloc.source_loc_id = isp.supplier)
                       or (NVL(sloc.source_loc_type,'WH') <> 'SU' and isp.primary_supp_ind = 'Y'))) supsz
        where di.rnk = 1
          and di.txn_item = im.item
          and di.txn_item = il.item(+)
          and di.loc_id = il.loc(+)
          and DECODE(di.loc_type,'ST','S','WH','W','X') = il.loc_type(+)
          and di.item = tmpl.item(+)
          and di.loc_id = tmpl.loc_id(+)
          and di.loc_type = tmpl.loc_type(+)
          and di.customer_loc = tmpl.customer_loc(+)
          and NVL(tmpl.rnum, 1) = 1
          and di.item = supsz.item
          and di.loc_id = supsz.loc_id
          and di.loc_type = supsz.loc_type
          and di.customer_loc = supsz.customer_loc
     order by di.txn_item, di.loc_type, di.loc_id, di.customer_loc, di.repl_ind asc;     -- Order by repl_ind to use WH inventory for non repl item/loc first and create store orders for others.

   cursor C_FRANCH_ORDER_EXISTS is
      select 'Y'
        from wf_order_detail wod,
             wf_order_head woh,
             wf_order_head woh1,
             wf_order_detail wod1
       where wod.wf_order_no     = woh.wf_order_no
         and wod.wf_order_no    != L_order_no
         and woh1.wf_order_no    = L_order_no
         and wod1.wf_order_no    = woh1.wf_order_no
         and wod.customer_loc    = wod1.customer_loc
         and wod.source_loc_id   = wod1.source_loc_id
         and wod.source_loc_type = wod1.source_loc_type
         and wod.need_date       = wod1.need_date
         and wod.item            = wod1.item
         and woh.status          = 'A'
         and woh.order_type     in ('M','E')                 -- Duplicate check is only for existing Manual and EDI orders. 
         and rownum = 1;

   cursor C_COUNT_DTL_IN_FILE is
      select count(*)
        from svc_wf_ord_detail
       where process_id  = I_process_id
         and chunk_id    = I_chunk_id
         and wf_order_id = L_wf_order_id;

   cursor C_CHECK_COSTING_LOC is
      select 'x'
        from item_master im,
             item_loc il1,
             item_loc il2,
             wf_order_detail wod
       where wod.wf_order_no               = L_order_no
         and wod.source_loc_type           = 'SU'
         and wod.item                      = im.item
         and NVL(im.deposit_item_type,'x') = 'E'
         and il1.item                      = wod.item
         and il1.loc                       = wod.customer_loc
         and il1.loc                       = il2.loc
         and il2.item                      = im.container_item
         and il1.costing_loc              <> il2.costing_loc
         and rownum = 1;

   cursor C_CHECK_REPL_SOURCE_WH is
      select 'x'
        from wf_order_detail wod,
             repl_item_loc ril,
             TABLE(CAST(L_wf_str_ord_tbl as OBJ_F_STORE_ORDER_TBL)) so
       where wod.wf_order_no      = so.wf_order_no
         and wod.wf_order_line_no = so.wf_order_line_no
         and wod.item             = ril.item
         and wod.customer_loc     = ril.location
         and wod.source_loc_id    <> ril.source_wh
         and rownum = 1;

   TYPE TYP_header_info           is TABLE of C_GET_ORD_HEAD%ROWTYPE;
   TYPE TYP_detail_info           is TABLE of C_GET_ORD_DETAIL%ROWTYPE;

   TBL_header_info                TYP_header_info;
   TBL_detail_info                TYP_detail_info;

BEGIN

   -- Get the lead days and consolidation_ind from system options
   if LP_system_options.wf_order_lead_days is NULL or LP_system_options.consolidation_ind is NULL then
      if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                   LP_system_options) then
         return FALSE;
      end if;
   end if;
   ---

   if LP_system_options.consolidation_ind = 'Y' then
      L_exchange_type := 'C';
   else
      L_exchange_type := 'O';
   end if;

   -- For empty detail files update process status to Completed
   update svc_wf_ord_head swoh
      set swoh.process_status = LP_wf_order_complete,
          swoh.last_update_datetime = SYSDATE,
          swoh.last_update_id = LP_user
    where swoh.process_id = I_process_id
      and swoh.chunk_id   = I_chunk_id
      and swoh.process_status = LP_wf_order_validate
      and not exists (select 1
                        from svc_wf_ord_detail swod
                       where swoh.process_id  = swod.process_id
                         and swoh.chunk_id    = swod.chunk_id
                         and swoh.wf_order_id = swod.wf_order_id);
   --
   open C_GET_ORD_HEAD;
   fetch C_GET_ORD_HEAD BULK COLLECT into TBL_header_info;
   close C_GET_ORD_HEAD;

   if TBL_header_info is NOT NULL and TBL_header_info.COUNT > 0 then
      FOR hd_idx in TBL_header_info.FIRST..TBL_header_info.LAST LOOP
         L_order_no           := TBL_header_info(hd_idx).wf_order_no;
         L_wf_order_id        := TBL_header_info(hd_idx).wf_order_id;
         L_cust_ord_ref_no    := TBL_header_info(hd_idx).cust_ord_ref_no;
         L_currency_code      := TBL_header_info(hd_idx).currency_code;
         L_wf_order_line_no   := 0;
         L_wf_tsf_tbl         := OBJ_F_TRANSFER_TBL();
         L_wf_str_ord_tbl     := OBJ_F_STORE_ORDER_TBL();
         L_wford_dtl_rec      := OBJ_F_ORDUPLD_DTL_REC();
         L_wford_dtl_tbl      := OBJ_F_ORDUPLD_DTL_TBL();
         L_approve_ind        := 'Y';  -- Franchise order will be approved unless some issue.
         L_create_po_ind      := 'N';
         L_diff_costing_loc   := NULL;
         L_diff_repl_wh       := NULL;

         insert into wf_order_head (wf_order_no,
                                    cust_ord_ref_no,
                                    status,
                                    order_type,
                                    currency_code,
                                    exchange_rate,
                                    freight,
                                    other_charges,
                                    all_location_billing,
                                    default_bill_to_loc,
                                    bill_to_addr_type,
                                    comments,
                                    cancel_reason,
                                    approval_date,
                                    create_id,
                                    create_datetime,
                                    last_update_id,
                                    last_update_datetime
                                   )
                            values (
                                   TBL_header_info(hd_idx).wf_order_no,
                                   TBL_header_info(hd_idx).cust_ord_ref_no,
                                   'I',                                                -- Initially load as Input. Will be approved post detail processing. 
                                   'E',                                                -- order type 'E'DI
                                   TBL_header_info(hd_idx).currency_code,
                                   TBL_header_info(hd_idx).exchange_rate,
                                   NULL,                                               -- freight charge
                                   NULL,                                               -- other charges
                                   TBL_header_info(hd_idx).all_location_billing,
                                   TBL_header_info(hd_idx).default_bill_to_loc,
                                   TBL_header_info(hd_idx).bill_to_addr_type,
                                   TBL_header_info(hd_idx).comments,
                                   NULL,                                               -- cancel reason
                                   NULL,                                               -- approval_date
                                   LP_user,                                            -- create id
                                   SYSDATE,                                            -- create datetime
                                   LP_user,
                                   SYSDATE
                                   );

         -- The input will hold the physical warehouse. Update the input source location for warehouse sourced
         -- to get the virtual warehouse based on distribution logic. Retain same location id for Store or supplier sourced. 
         open C_GET_SOURCE_LOC;
         fetch C_GET_SOURCE_LOC BULK COLLECT into L_source_loc_tbl;
         close C_GET_SOURCE_LOC;

         -- Order should be created with 'I'nput Status if customer has bad credit or not eligible for auto approval of externally generated orders
         if TBL_header_info(hd_idx).credit_ind = 'N' or TBL_header_info(hd_idx).auto_approve_ind = 'N' then
            L_approve_ind := 'N';
         end if;

         --Get the virtual warehouse if passed source location is warehouse
         if L_source_loc_tbl is NOT NULL and L_source_loc_tbl.COUNT > 0 then
            FOR i in L_source_loc_tbl.FIRST..L_source_loc_tbl.LAST LOOP
               if NVL(L_source_loc_tbl(i).source_loc_type,'x') = 'WH' and L_source_loc_tbl(i).loc_type is NULL then    -- Loc_type is populated for costing location (VWH)
                  if DISTRIBUTION_SQL.FIND_MAPPING_VWH(O_error_message,
                                                       L_virtual_wh,
                                                       L_source_loc_tbl(i).source_loc_id,
                                                       'F',                                     -- I_to_from_ind
                                                       L_source_loc_tbl(i).customer_loc,
                                                       'S',                                     -- I_other_loc_type
                                                       L_source_loc_tbl(i).txn_item,            -- Range the item if no ranging existing for the identified VWH
                                                       'N') = FALSE then
                     return FALSE;
                  end if;
                  L_source_loc_tbl(i).loc_id  := L_virtual_wh;
                  L_source_loc_tbl(i).loc_type  := 'WH';
                end if;
            END LOOP;
         end if;

         -- Get details info for the order
         open C_GET_ORD_DETAIL;
         fetch C_GET_ORD_DETAIL BULK COLLECT into TBL_detail_info;
         close C_GET_ORD_DETAIL;
         
         -- Validate that there are no duplicates to be inserted into the wf_order_detail table. 
         -- VALIDATE_DETAIL checks that there are no duplicate rows in the input file. 
         -- However, post the PWH to VWH distribution, duplicates are still possible for rows
         -- where source location is not provided and the identified costing loc/default wh matches 
         -- with VWH identified by distribute_SQL.
         if TBL_detail_info is NOT NULL and TBL_detail_info.COUNT > 0 then
            FOR dt_idx in TBL_detail_info.FIRST..TBL_detail_info.LAST LOOP
               if TBL_detail_info(dt_idx).cnt > 1 then
                  O_error_message := SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_DUP_ROWS', 
                                                              TBL_header_info(hd_idx).cust_ord_ref_no, 
                                                              TBL_detail_info(dt_idx).item, 
                                                              TBL_detail_info(dt_idx).source_loc_id || '/' || TBL_detail_info(dt_idx).customer_loc);
                  return FALSE;            
               end if;
            end LOOP;
         end if;

         --Check if count of item passed in file is not less than the count of detail records to be inserted in wf_order_detail
         --to make sure no unexpected error occured while fetching detail records
         open C_COUNT_DTL_IN_FILE;
         fetch C_COUNT_DTL_IN_FILE into L_count_dtl_in_file;
         close C_COUNT_DTL_IN_FILE;

         L_detail_rec_count := NVL(TBL_detail_info.COUNT,0);

         if L_count_dtl_in_file > L_detail_rec_count then
            O_error_message := SQL_LIB.GET_MESSAGE_TEXT('WF_ORDUPLD_UNEXP_ERR_DTL',L_detail_rec_count,L_count_dtl_in_file,NULL);
            return FALSE;
         end if;

         if TBL_detail_info is NOT NULL and TBL_detail_info.COUNT > 0 then
            FOR dt_idx in TBL_detail_info.FIRST..TBL_detail_info.LAST LOOP
               L_requested_qty     := TBL_detail_info(dt_idx).requested_qty;
               L_need_date         := TBL_detail_info(dt_idx).need_date;
               L_not_after_date    := TBL_detail_info(dt_idx).not_after_date;
               L_pack_ind          := TBL_detail_info(dt_idx).pack_ind;
               L_store_ord_exists  := TBL_detail_info(dt_idx).store_ord_exists;
               L_repl_ind          := TBL_detail_info(dt_idx).repl_ind;
               L_lead_time         := TBL_detail_info(dt_idx).lead_time;
               L_source_loc_type   := TBL_detail_info(dt_idx).source_loc_type;
               L_source_loc_id     := TBL_detail_info(dt_idx).source_loc_id;
               L_source_loc_status := TBL_detail_info(dt_idx).source_loc_status;
               L_wf_tsf_ind        := 'N';
               L_wf_str_ord_ind    := 'N';

               -- Adjust the available quantity for transfer already created in the loop for the same item and source store/warehouse.
               if L_source_loc_type in ('ST','WH')
                  and NVL(L_prev_source_id,-999)       = TBL_detail_info(dt_idx).source_loc_id 
                  and NVL(L_prev_source_type,'xx')     = TBL_detail_info(dt_idx).source_loc_type 
                  and NVL(L_prev_item,'-999')          = TBL_detail_info(dt_idx).item then
                  ---
                  L_avail_qty         := GREATEST(TBL_detail_info(dt_idx).avail_qty - L_tot_prev_tsf_qty,0);
               else
                  L_avail_qty         := TBL_detail_info(dt_idx).avail_qty;
                  L_tot_prev_tsf_qty  := 0;
               end if;
                                                       
               L_prev_source_id   := TBL_detail_info(dt_idx).source_loc_id;
               L_prev_source_type := TBL_detail_info(dt_idx).source_loc_type;
               L_prev_item        := TBL_detail_info(dt_idx).item;

               -- Loop through the detail record to check franchise order is not failing any validation.
               -- For validation issues, create the order in input status. No transfer/Store Order or PO 
               -- will be created in that case. 
               if L_approve_ind = 'N' then
                  NULL;    -- Do nothing
               elsif TBL_detail_info(dt_idx).acquisition_cost is NULL then       -- Acquisition cost not available because of missing item/loc ranging or no future_cost record
                  L_approve_ind := 'N';
               elsif L_source_loc_type = 'SU' then
                  -- Supplier source item is not order in multiple of supplier case size
                  -- or Lead time for supplier will be after the not after date of franchisee order.
                  if ((TBL_detail_info(dt_idx).supp_case_chk is NOT NULL and TBL_detail_info(dt_idx).supp_case_chk = 'N') 
                     or ((L_not_after_date - L_lead_time) < LP_vdate)) then
                     L_approve_ind := 'N';
                  end if;
                  L_create_po_ind := 'Y';
               elsif L_source_loc_type = 'ST' then
                  if L_need_date > LP_vdate + NVL(LP_system_options.wf_order_lead_days,0) then         -- Cannot reserve inventory beyond order lead days.
                     L_approve_ind := 'N';
                  elsif L_avail_qty < L_requested_qty then
                     L_approve_ind := 'N';
                  elsif L_source_loc_status = 'D' then 
                     L_approve_ind := 'N';
                  else
                     L_wf_tsf_ind  := 'Y';
                  end if;
               elsif L_source_loc_type = 'WH'  then
                  if L_source_loc_status = 'D' then 
                     L_approve_ind := 'N';
                  elsif L_need_date <= LP_vdate + NVL(LP_system_options.wf_order_lead_days,0) then
                     if L_avail_qty >= L_requested_qty then
                        L_wf_tsf_ind := 'Y';
                     elsif L_avail_qty < L_requested_qty then
                        if L_store_ord_exists = 'Y' then
                           L_approve_ind := 'N';
                        elsif L_pack_ind = 'Y' then
                           L_approve_ind := 'N';
                        else
                           if L_avail_qty >= 0 and (L_repl_ind = 'Y' and ((L_not_after_date - L_lead_time) >= LP_vdate)) then
                              L_str_ord_qty    := L_requested_qty - L_avail_qty;
                              L_wf_str_ord_ind := 'Y';
                              if L_avail_qty > 0 then
                                 L_requested_qty  := L_avail_qty;
                                 L_wf_tsf_ind     := 'Y';
                              end if;
                           else
                              L_approve_ind := 'N';
                           end if;
                        end if;
                     end if;
                  elsif L_need_date > LP_vdate + NVL(LP_system_options.wf_order_lead_days,0) then
                     if L_store_ord_exists = 'Y' then
                        L_approve_ind := 'N';
                     elsif L_pack_ind = 'Y' then
                        L_approve_ind := 'N';
                     elsif L_repl_ind = 'Y' and (L_not_after_date - L_lead_time >= LP_vdate) then
                        L_str_ord_qty := L_requested_qty;
                        L_wf_str_ord_ind := 'Y';
                     else
                        L_approve_ind := 'N';
                     end if;
                  end if;
               end if;     -- Source loc type = 'WH'

               L_wf_order_line_no := L_wf_order_line_no + 1;
               L_wford_dtl_rec:= OBJ_F_ORDUPLD_DTL_REC(L_order_no,
                                                       L_wf_order_line_no,
                                                       TBL_detail_info(dt_idx).item,
                                                       TBL_detail_info(dt_idx).source_loc_type,
                                                       TBL_detail_info(dt_idx).source_loc_id,
                                                       TBL_detail_info(dt_idx).customer_loc,
                                                       TBL_detail_info(dt_idx).requested_qty,
                                                       TBL_detail_info(dt_idx).need_date,
                                                       TBL_detail_info(dt_idx).not_after_date,
                                                       TBL_detail_info(dt_idx).acquisition_cost,
                                                       TBL_detail_info(dt_idx).customer_cost,
                                                       TBL_detail_info(dt_idx).fixed_cost,
                                                       TBL_detail_info(dt_idx).templ_id,
                                                       TBL_detail_info(dt_idx).margin_pct,
                                                       TBL_detail_info(dt_idx).calc_type,
                                                       TBL_detail_info(dt_idx).templ_desc,
                                                       TBL_detail_info(dt_idx).fc_curr_code);
               L_wford_dtl_tbl.EXTEND;
               L_wford_dtl_tbl(L_wford_dtl_tbl.COUNT):= L_wford_dtl_rec;

               if L_wf_tsf_ind = 'Y' and L_approve_ind = 'Y' then
                   L_wf_tsf_tbl.EXTEND;
                   L_wf_tsf_tbl(L_wf_tsf_tbl.COUNT) := OBJ_F_TRANSFER_REC(TBL_detail_info(dt_idx).item,
                                                                          TBL_detail_info(dt_idx).source_loc_type,
                                                                          TBL_detail_info(dt_idx).source_loc_id,
                                                                          TBL_detail_info(dt_idx).customer_loc_type,
                                                                          TBL_detail_info(dt_idx).customer_loc,
                                                                          'FO',     -- Transfer Type
                                                                          TBL_detail_info(dt_idx).need_date,
                                                                          L_order_no,
                                                                          L_requested_qty);
                  L_tot_prev_tsf_qty := L_tot_prev_tsf_qty + L_requested_qty;
                end if;

                if L_wf_str_ord_ind = 'Y' and L_approve_ind = 'Y' then
                   L_wf_str_ord_tbl.EXTEND;
                   L_wf_str_ord_tbl(L_wf_str_ord_tbl.COUNT) := OBJ_F_STORE_ORDER_REC(L_order_no,
                                                                                     L_wf_order_line_no,
                                                                                     TBL_detail_info(dt_idx).item,
                                                                                     TBL_detail_info(dt_idx).customer_loc,
                                                                                     TBL_detail_info(dt_idx).need_date,
                                                                                     L_str_ord_qty);
                end if;
            END LOOP; --Detail loop

            -- For any content item in the detail collection, get the container item. 
            if ADD_CONTAINER_ITEMS(O_error_message,
                                   L_wford_dtl_tbl,
                                   L_wf_order_line_no) = FALSE then
               return FALSE;
            end if;

            -- If the future cost currency is not same as order currency, convert customer cost and acquisition cost to order currency. 
            FOR i in L_wford_dtl_tbl.FIRST..L_wford_dtl_tbl.LAST LOOP
               --If customer location currency is different with order currency convert the cost into order currency
               if L_wford_dtl_tbl(i).fc_curr_code != L_currency_code then
                  L_cust_cost    := NULL;
                  L_acq_cost     := NULL;
                  if CURRENCY_SQL.CONVERT(O_error_message,
                                          L_wford_dtl_tbl(i).acquisition_cost,
                                          L_wford_dtl_tbl(i).fc_curr_code,
                                          L_currency_code,
                                          L_acq_cost,
                                          'C',
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL) = FALSE then
                     return FALSE;
                  end if;

                  if CURRENCY_SQL.CONVERT(O_error_message,
                                          L_wford_dtl_tbl(i).customer_cost,
                                          L_wford_dtl_tbl(i).fc_curr_code,
                                          L_currency_code,
                                          L_cust_cost,
                                          'C',
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL) = FALSE then
                     return FALSE;
                  end if;
                  L_wford_dtl_tbl(i).customer_cost    := L_cust_cost;
                  L_wford_dtl_tbl(i).acquisition_cost := L_acq_cost;
               end if;
            END LOOP;

            FORALL i in L_wford_dtl_tbl.FIRST..L_wford_dtl_tbl.LAST
              insert into wf_order_detail (wf_order_no,
                                           wf_order_line_no,
                                           item,
                                           source_loc_type,
                                           source_loc_id,
                                           customer_loc,
                                           requested_qty,
                                           cancel_reason,
                                           need_date,
                                           not_after_date,
                                           acquisition_cost,
                                           customer_cost,
                                           fixed_cost,
                                           templ_id,
                                           margin_pct,
                                           calc_type,
                                           templ_desc,
                                           create_datetime,
                                           create_id,
                                           last_update_datetime,
                                           last_update_id)
                                   values (L_wford_dtl_tbl(i).wf_order_no,
                                           L_wford_dtl_tbl(i).wf_order_line_no,
                                           L_wford_dtl_tbl(i).item,
                                           L_wford_dtl_tbl(i).source_loc_type,
                                           L_wford_dtl_tbl(i).source_loc_id,
                                           L_wford_dtl_tbl(i).customer_loc,
                                           L_wford_dtl_tbl(i).requested_qty,
                                           NULL,                         --cancel_reason,
                                           L_wford_dtl_tbl(i).need_date,
                                           L_wford_dtl_tbl(i).not_after_date,
                                           L_wford_dtl_tbl(i).acquisition_cost,
                                           L_wford_dtl_tbl(i).customer_cost,
                                           L_wford_dtl_tbl(i).fixed_cost,
                                           L_wford_dtl_tbl(i).templ_id,
                                           L_wford_dtl_tbl(i).margin_pct,
                                           L_wford_dtl_tbl(i).calc_type,
                                           L_wford_dtl_tbl(i).templ_desc,
                                           SYSDATE,
                                           LP_user,
                                           SYSDATE,
                                           LP_user);

           --Populate Expense information
           FOR i in L_wford_dtl_tbl.FIRST..L_wford_dtl_tbl.LAST LOOP
              if WF_ORDER_SQL.GET_UPCHARGE_INFORMATION(O_error_message,
                                                       L_wford_dtl_tbl(i).wf_order_no,
                                                       L_currency_code,
                                                       L_wford_dtl_tbl(i).item,
                                                       L_wford_dtl_tbl(i).source_loc_type,
                                                       L_wford_dtl_tbl(i).source_loc_id,
                                                       L_wford_dtl_tbl(i).customer_loc) = FALSE then
                 return FALSE;
              end if;
           END LOOP;
         end if;  -- Detail collection is not NULL

         -- If there is an existing Franchisee PO for same item/customer location/source location/need date place the order in 'I'nput Status
         open C_FRANCH_ORDER_EXISTS;
         fetch C_FRANCH_ORDER_EXISTS into L_fo_exists;
         close C_FRANCH_ORDER_EXISTS;

         if L_fo_exists = 'Y' then
            L_approve_ind   := 'N';
         end if;

         ---- Check the deposit item content and container has the same costing location for supplier sourced rows.
         open C_CHECK_COSTING_LOC;
         fetch C_CHECK_COSTING_LOC into L_diff_costing_loc;
         close C_CHECK_COSTING_LOC;

         if L_diff_costing_loc is NOT NULL then
            L_approve_ind   := 'N';
         end if;
         ---

         -- For records for which store order is being created, check that the source warehouse matches with the 
         -- source_wh in repl_item_loc table. 
         open C_CHECK_REPL_SOURCE_WH;
         fetch C_CHECK_REPL_SOURCE_WH into L_diff_repl_wh;
         close C_CHECK_REPL_SOURCE_WH;

         if L_diff_repl_wh is NOT NULL then
            L_approve_ind   := 'N';
         end if;
         ---
         
         if L_approve_ind = 'Y' then -- create the linked PO, transfers and Store orders. 
            if L_create_po_ind = 'Y' then
              if WF_PO_SQL.CREATE_FRANCHISE_PO(O_error_message,            -- Create order initially in worksheet status for catching any error
                                               L_order_no,
                                               L_scaling,
                                               L_undo_scaling) = FALSE then
                 return FALSE;
              end if;
            end if;

            --Approve franchisee order
            update wf_order_head
               set status = 'A',
                   approval_date = SYSDATE,
                   last_update_id = LP_user,
                   last_update_datetime = SYSDATE
             where wf_order_no = L_order_no;

            if L_create_po_ind = 'Y' then
               if WF_PO_SQL.MODIFY_FRANCHISE_PO(O_error_message,           -- Approve the Purchase orders.
                                                L_order_no,
                                                L_scaling,
                                                L_undo_scaling) = FALSE then
                  return FALSE;
               end if;
            end if;

            -- Build Transfer and Store Order
            if ((L_wf_str_ord_tbl is NOT NULL and L_wf_str_ord_tbl.COUNT > 0) 
                  or (L_wf_tsf_tbl is NOT NULL and L_wf_tsf_tbl.COUNT > 0))then
               if NOT WF_TRANSFER_SQL.BUILD_TSF_STORE_ORDER(O_error_message,
                                                            L_order_no,
                                                            'A',
                                                            L_wf_tsf_tbl,
                                                            L_wf_str_ord_tbl) then
                  return FALSE;
               end if;
            end if;
         end if; -- L_approve_ind = 'Y'

         update svc_wf_ord_head
            set process_status = LP_wf_order_complete,
                last_update_datetime = SYSDATE,
                last_update_id = LP_user
          where process_id =I_process_id
            and chunk_id = I_chunk_id
            and wf_order_id = L_wf_order_id;

      END LOOP; -- Header loop
   end if;      -- Header collection is not null

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END APPROVE_WF_ORDER;
---------------------------------------------------------------------------------------------
FUNCTION ADD_CONTAINER_ITEMS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_order_detail_tbl    IN OUT OBJ_F_ORDUPLD_DTL_TBL,
                             I_wf_order_line_no     IN     WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE)
   RETURN BOOLEAN AS

   L_program                  VARCHAR2(50)                          := 'CORESVC_WF_ORDER_UPLOAD_SQL.ADD_CONTAINER_ITEMS';
   L_container_item           ITEM_MASTER.ITEM%TYPE                 := NULL;
   L_source_loc_id            WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE    := NULL;
   L_source_loc_type          WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE  := NULL;
   L_customer_loc             WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE     := NULL;
   L_content_item             ITEM_MASTER.ITEM%TYPE                 := NULL;
   L_item_src_loc_exist       VARCHAR2(1)                           := NULL;
   L_container_cust_loc_exist VARCHAR2(1)                           := NULL;
   L_templ_id                 WF_ORDER_DETAIL.TEMPL_ID%TYPE         := NULL;
   L_templ_desc               WF_ORDER_DETAIL.TEMPL_DESC%TYPE       := NULL;
   L_margin_pct               WF_ORDER_DETAIL.MARGIN_PCT%TYPE       := NULL;
   L_calc_type                WF_ORDER_DETAIL.CALC_TYPE%TYPE        := NULL;
   L_cust_cost                WF_ORDER_DETAIL.CUSTOMER_COST%TYPE    := NULL;
   L_acqui_cost               WF_ORDER_DETAIL.ACQUISITION_COST%TYPE := NULL;
   L_currency_code            WF_ORDER_HEAD.CURRENCY_CODE%TYPE      := NULL;
   L_wf_order_line_no         WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE := 0;
   L_src_ranged_ind           VARCHAR2(1)                           := NULL;
   L_cust_ranged_ind          VARCHAR2(1)                           := NULL;

   cursor C_CONTAINER_ITEMS is
      select tbl.wf_order_no,
             tbl.item content_item,
             im.container_item item,
             tbl.source_loc_type,
             tbl.source_loc_id,
             tbl.customer_loc,
             SUM(tbl.requested_qty) requested_qty,
             tbl.need_date,
             tbl.not_after_date
        from TABLE(CAST(IO_order_detail_tbl AS OBJ_F_ORDUPLD_DTL_TBL)) tbl,
             item_master im
       where im.item = tbl.item
         and NVL(im.deposit_item_type,'X') = 'E'
       group by tbl.wf_order_no,
                tbl.item,
                im.container_item,
                tbl.customer_loc,
                tbl.source_loc_type,
                tbl.source_loc_id,
                tbl.need_date,
                tbl.not_after_date;

   cursor C_ITEM_LOC_EXIST(cv_item ITEM_MASTER.ITEM%TYPE,
                           cv_loc ITEM_LOC.LOC%TYPE,
                           cv_loc_type ITEM_LOC.LOC_TYPE%TYPE) is
      select ranged_ind 
        from item_loc 
       where item = cv_item
         and loc = cv_loc
         and loc_type = cv_loc_type;

   TYPE TYP_container_item        is TABLE of C_CONTAINER_ITEMS%ROWTYPE;
   TBL_container_item             TYP_container_item;
   L_container_item_rec           OBJ_F_ORDUPLD_DTL_REC                   := OBJ_F_ORDUPLD_DTL_REC;
   L_container_item_tbl           OBJ_F_ORDUPLD_DTL_TBL                   := OBJ_F_ORDUPLD_DTL_TBL();

BEGIN

   open C_CONTAINER_ITEMS;
   fetch C_CONTAINER_ITEMS BULK COLLECT into TBL_container_item;
   close C_CONTAINER_ITEMS;

   if TBL_container_item is NOT NULL and TBL_container_item.COUNT > 0 then
      FOR i in TBL_container_item.FIRST..TBL_container_item.LAST LOOP
         L_container_item             := NULL;
         L_source_loc_id              := NULL;
         L_source_loc_type            := NULL;
         L_customer_loc               := NULL;
         L_content_item               := NULL;
         L_item_src_loc_exist         := NULL;
         L_src_ranged_ind             := NULL;
         L_cust_ranged_ind            := NULL;
         L_container_cust_loc_exist   := NULL;
         L_templ_id                   := NULL;
         L_templ_desc                 := NULL;
         L_margin_pct                 := NULL;
         L_calc_type                  := NULL;
         L_cust_cost                  := NULL;
         L_acqui_cost                 := NULL;
         L_currency_code              := NULL;

         --Only if item is not supplier sourced check for item/customer location
         -- and item/source location ranging
         if TBL_container_item(i).source_loc_type != 'SU' then
            L_container_item  := TBL_container_item(i).item;
            L_source_loc_id   := TBL_container_item(i).source_loc_id;
            L_customer_loc    := TBL_container_item(i).customer_loc;
            L_content_item    := TBL_container_item(i).content_item;
            if TBL_container_item(i).source_loc_type = 'ST' then
               L_source_loc_type := 'S';
            elsif TBL_container_item(i).source_loc_type = 'WH' then
               L_source_loc_type := 'W';
            end if;
            -- Check if container item is ranged to source location
            open C_ITEM_LOC_EXIST(L_container_item,L_source_loc_id,L_source_loc_type);
            fetch C_ITEM_LOC_EXIST into L_item_src_loc_exist;
            close C_ITEM_LOC_EXIST;

            if L_item_src_loc_exist is NULL then
               --If container is not ranged to source location range it with ranged indicator same as that of content item
               open C_ITEM_LOC_EXIST(L_content_item,L_source_loc_id,L_source_loc_type);
               fetch C_ITEM_LOC_EXIST into L_src_ranged_ind;
               close C_ITEM_LOC_EXIST;
               --
               if NEW_ITEM_LOC(O_error_message,
                               L_container_item,
                               L_source_loc_id,
                               NULL,
                               NULL,
                               L_source_loc_type,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, L_src_ranged_ind) = FALSE then
                  return FALSE;
               end if;
            end if;
            --Check if content item is ranged to customer location
            open C_ITEM_LOC_EXIST(L_content_item,L_customer_loc,'S');
            fetch C_ITEM_LOC_EXIST into L_cust_ranged_ind;
            close C_ITEM_LOC_EXIST;

            if L_cust_ranged_ind is NOT NULL then
               --Check if container item is ranged to customer location
               open C_ITEM_LOC_EXIST(L_container_item,L_customer_loc,'S');
               fetch C_ITEM_LOC_EXIST into L_container_cust_loc_exist;
               close C_ITEM_LOC_EXIST;
               --If content is ranged to customer location and container is not ranged
               -- then range container item also to customer location
               if L_container_cust_loc_exist is NULL then
                  if NEW_ITEM_LOC(O_error_message,
                                  L_container_item,
                                  L_customer_loc,
                                  NULL,
                                  NULL,
                                  'S',
                                  NULL, NULL, NULL, NULL, NULL,
                                  NULL, NULL, NULL, NULL, NULL,
                                  NULL, NULL, NULL, NULL, NULL,
                                  NULL, NULL, NULL, NULL, NULL,
                                  NULL, NULL, NULL, NULL, NULL,
                                  NULL, NULL, NULL, NULL, NULL,
                                  NULL, NULL, NULL, NULL, NULL,
                                  NULL, NULL, NULL, NULL, NULL,
                                  NULL, NULL, NULL, NULL, NULL,
                                  NULL, NULL, NULL, NULL, NULL,
                                  NULL, NULL, NULL, NULL, NULL,
                                  NULL, L_cust_ranged_ind) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if;
         end if; --if item is not supplier sourced

         --Get costing details for container item.
         if WF_ORDER_SQL.GET_COST_INFORMATION(O_error_message,
                                              L_templ_id,
                                              L_templ_desc,
                                              L_margin_pct,
                                              L_calc_type,
                                              L_cust_cost,
                                              L_acqui_cost,
                                              L_currency_code,
                                              TBL_container_item(i).item,
                                              TBL_container_item(i).customer_loc) = FALSE then
            return FALSE;
         end if;
         --Build the container item record
         L_wf_order_line_no := I_wf_order_line_no + i;
         L_container_item_rec := OBJ_F_ORDUPLD_DTL_REC(TBL_container_item(i).wf_order_no,
                                                       L_wf_order_line_no,
                                                       TBL_container_item(i).item,
                                                       TBL_container_item(i).source_loc_type,
                                                       TBL_container_item(i).source_loc_id,
                                                       TBL_container_item(i).customer_loc,
                                                       TBL_container_item(i).requested_qty,
                                                       TBL_container_item(i).need_date,
                                                       TBL_container_item(i).not_after_date,
                                                       L_acqui_cost,
                                                       L_cust_cost,
                                                       NULL,            --Fixed cost
                                                       L_templ_id,
                                                       L_margin_pct,
                                                       L_calc_type,
                                                       L_templ_desc,
                                                       L_currency_code);
         L_container_item_tbl.EXTEND;
         L_container_item_tbl(L_container_item_tbl.COUNT):= L_container_item_rec;
      END LOOP;
   end if;
   --Add container item records to collection of detail item records
   IO_order_detail_tbl := IO_order_detail_tbl MULTISET UNION L_container_item_tbl;
   --
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_CONTAINER_ITEMS;
---------------------------------------------------------------------------------------------
END CORESVC_WF_ORDER_UPLOAD_SQL;
/