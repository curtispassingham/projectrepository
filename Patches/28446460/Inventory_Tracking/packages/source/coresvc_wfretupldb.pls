CREATE OR REPLACE PACKAGE BODY CORESVC_WF_RETURN_UPLOAD_SQL AS

LP_new                    SVC_WF_RET_HEAD.PROCESS_STATUS%TYPE := 'N';
LP_error                  SVC_WF_RET_HEAD.PROCESS_STATUS%TYPE := 'E';
LP_validate               SVC_WF_RET_HEAD.PROCESS_STATUS%TYPE := 'V';
LP_complete               SVC_WF_RET_HEAD.PROCESS_STATUS%TYPE := 'C';
LP_user                   SVCPROV_CONTEXT.USER_NAME%TYPE      := GET_USER;

-----------------------------------------------------------------------------------------------
---                                   PRIVATE FUNCTION                                   ----
-----------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_WF_RET_UPLD
-- Purpose:       This function will do the validate data in the staging tables
--                for wf return upload. For every error, it will update the error_msg
--                in the staging table. For multiple errors in the same row, it will use the
--                delimiter ";". If there is any error in the validation for any header or detail
--                record for the input process_id and chunk id, it will return failure.
-- Called by:     CREATE_RMA
--------------------------------------------------------------------------------
FUNCTION VALIDATE_WF_RET_UPLD(O_error_message  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_val_ind        IN OUT BOOLEAN,
                              I_process_id     IN  SVC_WF_RET_HEAD.PROCESS_ID%TYPE,
                              I_chunk_id       IN  SVC_WF_RET_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
---                                   PRIVATE FUNCTION                                   ----
-----------------------------------------------------------------------------------------------

-- Function Name: POPULATE_RETURN_REC
-- Purpose:       This function will build the collection for the wf_return_head and the
--                wf_return_detail.The function calls the PERSIST_WF_RET_UPLD which persists the
--                collections into wf_return_head and wf_return_detail.
-- Called by:     CREATE_RMA
----------------------------------------------------------------------------------------------
FUNCTION POPULATE_RETURN_REC(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_f_return_head_tbl   IN OUT OBJ_F_RETURN_HEAD_TBL,
                             O_f_return_detail_tbl IN OUT OBJ_F_RETURN_DETAIL_TBL,
                             I_process_id          IN  SVC_WF_RET_HEAD.PROCESS_ID%TYPE,
                             I_chunk_id            IN  SVC_WF_RET_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
---                                   PRIVATE FUNCTION                                   ----
-----------------------------------------------------------------------------------------------
-- Function Name: RANGE_CONTAINER
-- Purpose:       This private function will range  deposit container items to customer location and 
--                return location of the Franchise Return.
-- Called by:     CREATE_RMA 
--------------------------------------------------------------------------------------------------
FUNCTION RANGE_CONTAINER(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_f_return_head_tbl   IN OBJ_F_RETURN_HEAD_TBL,
                         I_f_return_detail_tbl IN OBJ_F_RETURN_DETAIL_TBL)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
---                                   PRIVATE FUNCTION                                   ----
-----------------------------------------------------------------------------------------------
-- Function Name: PERSIST_WF_RET_UPLD
-- Purpose:       This function will persist the return head and detail collections into wf_return_head
--                and wf_return_detail tables and  will call the wf_return_sql.approve
--                to approve all the valid RMA's for which the item return_loc combination
--                exists.
--Called By:      CREATE_RMA
--------------------------------------------------------------------------------------------------
FUNCTION PERSIST_WF_RET_UPLD(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_f_return_head_tbl   IN OBJ_F_RETURN_HEAD_TBL,
                             I_f_return_detail_tbl IN OBJ_F_RETURN_DETAIL_TBL)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------

FUNCTION CREATE_RMA(O_error_message OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id    IN     SVC_WF_RET_HEAD.PROCESS_ID%TYPE,
                    I_chunk_id      IN     SVC_WF_RET_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(50)            := 'CORESVC_WF_RETURN_UPLOAD_SQL.CREATE_RMA';
   L_f_return_head_tbl     OBJ_F_RETURN_HEAD_TBL   := OBJ_F_RETURN_HEAD_TBL();
   L_f_return_detail_tbl   OBJ_F_RETURN_DETAIL_TBL := OBJ_F_RETURN_DETAIL_TBL();
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_val_ind               BOOLEAN;

   cursor C_LOCK_SVC_WF_RET_HEAD is
      select 'x'
        from svc_wf_ret_head
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         for update nowait;

   cursor C_LOCK_SVC_WF_RET_DETAIL is
      select 'x'
        from svc_wf_ret_detail
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         for update nowait;
BEGIN

   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_chunk_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if VALIDATE_WF_RET_UPLD(O_error_message,
                           L_val_ind,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if L_val_ind then


      if POPULATE_RETURN_REC(O_error_message,
                             L_f_return_head_tbl,
                             L_f_return_detail_tbl,
                             I_process_id,
                             I_chunk_id) = FALSE then
         return FALSE;
      end if;
 
      if RANGE_CONTAINER(O_error_message,
                         L_f_return_head_tbl,
                         L_f_return_detail_tbl) = FALSE then
         return FALSE;
      end if; 
      if PERSIST_WF_RET_UPLD(O_error_message,
                             L_f_return_head_tbl,
                             L_f_return_detail_tbl) = FALSE then
         return FALSE;
      end if;
      --update the status of all records in the head table to 'C'
      update svc_wf_ret_head
         set process_status = LP_complete,
             last_update_datetime = sysdate,
             last_update_id = LP_user
      where process_id = I_process_id
         and chunk_id = I_chunk_Id
         and process_status = LP_Validate;
   end if;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
       if C_LOCK_SVC_WF_RET_HEAD%ISOPEN then
         close C_LOCK_SVC_WF_RET_HEAD;
      end if;

      if C_LOCK_SVC_WF_RET_DETAIL%ISOPEN then
         close C_LOCK_SVC_WF_RET_DETAIL;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END CREATE_RMA;

-----------------------------------------------------------------------------------------------
---                                   PRIVATE FUNCTION                                      ---
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_WF_RET_UPLD(O_error_message   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_val_ind         IN OUT BOOLEAN,
                              I_process_id      IN  SVC_WF_RET_HEAD.PROCESS_ID%TYPE,
                              I_chunk_id        IN  SVC_WF_RET_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(64)            := 'CORESVC_WF_RETURN_UPLOAD_SQL.VALIDATE_WF_RET_UPLD';
   L_table                 VARCHAR2(50);
   L_cust_ret_ref_no       SVC_WF_RET_HEAD.CUST_RET_REF_NO%TYPE;

   --Cursor C_SVC_WF_RET_HEAD is to identify the row in the header table which have error
   --WF_customer_id is invalid
   --Currency Code is invalid

   cursor C_SVC_WF_RET_HEAD is
      select svh.wf_return_id,
             svh.cust_ret_ref_no,
             svh.wf_customer_id,
             wf.wf_customer_id                                                  valid_wf_customer_id,
             svh.currency_code,
             curr.currency_code                                                 valid_currency_code,
             svh.comments,
             svh.error_msg
        from svc_wf_ret_head svh,
             wf_customer wf,
             currencies curr
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and svh.process_status = LP_new
         and svh.wf_customer_id = wf.wf_customer_id(+)
         and svh.currency_code =  curr.currency_code(+)
         and (wf.wf_customer_id is NULL
              or curr.currency_code is NULL);

   --Cursor C_SVC_WF_RET_DETAIL is to identify the row in the detail table which have error
   -- Customer_loc is invalid.
   -- Non stockholding customer_location having return_loc_type -'S'.
   -- Stockholding customer_location having return_method -'D'.
   -- The customer_id is not linked to the the customer_location(franchise store).
   -- Return Location is invalid.
   -- Item is invalid or Item is not ranged to the customer_location.
   -- Item is not a pack item or a container item.
   -- Item is in deleted status at the customer_location(francise store) or return location.
   -- Unit of measure is invalid.
   -- The item and wf_order_no combination does not exist in the wf_order_detail.
   -- The wf_order is in the 'I'nput or 'A'pproved status.
   -- Duplicate detail record

   cursor C_SVC_WF_RET_DETAIL is
      select svd.wf_return_id,
             svh.cust_ret_ref_no,
             svd.line_no,
             svd.item,
             im.item                                                            valid_item,
             il.item                                                            valid_from_loc_itm,
             im.pack_ind                                                        pack_ind,
             im.deposit_item_type                                               deposit_item_type,
             im.standard_uom                                                    valid_unit_of_measure,
             svd.unit_of_measure,
             il.status                                                          itm_from_loc_status,
             il2.status                                                         itm_to_loc_status,
             svd.wf_order_no,
             wfh.wf_order_no                                                    valid_wf_order_no,
             svh.wf_customer_id,
             wf_loc.wf_customer_id                                              valid_wf_customer_id,
             svd.customer_loc,
             wf_loc.store                                                       valid_customer_loc,
             svd.return_loc_id,
             loc.location                                                       valid_return_loc_id,
             svd.return_method,
             svd.return_loc_type,
             wf_loc.stockholding_ind,
             (ils.stock_on_hand - 
              ils.customer_resv -
              ils.tsf_reserved_qty -
              ils.non_sellable_qty) avail_qty,
             (select 'Y'
                from wf_order_detail wf_dtl,
                     store s
               where wf_dtl.customer_loc = s.store
                 and s.wf_customer_id = svh.wf_customer_id
                 and wf_dtl.wf_order_no = svd.wf_order_no
                 and rownum = 1)                                                  valid_ord_custid_ind,
             (select 'Y'
                from wf_order_head wfh2,
                     wf_order_detail wfd
               where wfh2.wf_order_no = wfd.wf_order_no
                     and wfh2.wf_order_no = svd.wf_order_no
                     and svd.item = wfd.item
                     and wfh2.status in ('P', 'D')
                     and rownum = 1)                                               valid_orditm_ind,
             (select count(*)
               from svc_wf_ret_detail svd2
              where svd2.wf_return_id = svd.wf_return_id
                and svd2.customer_loc = svd.customer_loc
                and svd2.wf_order_no = svd.wf_order_no
                and svd2.return_loc_id = svd.return_loc_id
                and svd2.item = svd.item
              group by svd2.wf_return_id,
                       svd2.customer_loc,
                       svd2.return_loc_id,
                       svd.return_method,
                       svd2.wf_order_no,
                       svd2.item) count_rows,
              svd.error_msg,
              svd.rowid
        from svc_wf_ret_detail svd,
             svc_wf_ret_head svh,
             item_master im,
             wf_order_head wfh,
             item_loc il,        -- Item and customer loc
             item_loc il2,       -- Item and return loc
             item_loc_soh ils,
             (select store,
                     stockholding_ind,
                     wf_customer_id
                from store
               where  store_type = 'F') wf_loc,
             (select wh  location,
                     'W' loc_type
                from wh
               where wh = physical_wh
              union all
              select store location,
                     'S' loc_type
                from store
               where stockholding_ind = 'Y'
                 and store_type = 'C') loc
       where svh.process_id = I_process_id
         and svh.chunk_id = I_chunk_id
         and svh.process_status = LP_new
         and svh.wf_return_id = svd.wf_return_id
         and svd.item = im.item(+)
         and svd.item = il.item(+)
         and svd.customer_loc = il.loc(+)
         and svd.item = il2.item(+)
         and svd.return_loc_id = il2.loc(+)
         and svd.return_loc_type = il2.loc_type(+)
         and svd.item = ils.item(+)
         and svd.customer_loc = ils.loc(+)
         and svd.customer_loc = wf_loc.store(+)
         and svd.wf_order_no = wfh.wf_order_no(+)
         and svd.return_loc_id = loc.location(+)
         and svd.return_loc_type = loc.loc_type(+)
         and (   im.item is NULL
              or im.pack_ind = 'Y'
              or NVL(im.deposit_item_type,'X') = 'A'
              or im.standard_uom <> svd.unit_of_measure
              or il.item is NULL
              or NVL(il.status,'x') = 'D'
              or NVL(il2.status,'x') = 'D'
              or wfh.wf_order_no is NULL
              or loc.location is NULL 
              or (wf_loc.store is NULL
                  or (   wf_loc.wf_customer_id <> svh.wf_customer_id
                      or (wf_loc.stockholding_ind = 'N' and svd.return_loc_type = 'S')
                      or (wf_loc.stockholding_ind = 'Y' and svd.return_method = 'D')))
              or  not exists (select 1
                               from wf_order_head wfh2,
                                    wf_order_detail wfd,
                                    item_master im
                              where wfh2.wf_order_no = wfd.wf_order_no
                                and wfh2.wf_order_no = svd.wf_order_no
                                and svd.item         = wfd.item
                                and wfd.item         = im.item
                                and im.pack_ind      = 'N'
                                and wfh2.status in ('P', 'D')
                              UNION ALL
                             select 1
                               from wf_order_head wfh2,
                                    wf_order_detail wfd,
                                    item_master im,
                                    v_packsku_qty vpq
                              where wfh2.wf_order_no = wfd.wf_order_no
                                and wfh2.wf_order_no = svd.wf_order_no
                                and wfd.item         = im.item
                                and im.pack_ind      = 'Y'
                                and wfd.item         = vpq.pack_no
                                and svd.item         = vpq.item
                                and wfh2.status in ('P', 'D'))
              or  not exists (select 1
                                 from wf_order_detail wf_dtl,
                                      store s
                                where wf_dtl.customer_loc = s.store
                                  and s.wf_customer_id = svh.wf_customer_id
                                  and wf_dtl.wf_order_no = svd.wf_order_no)
              or exists(select 1
                          from svc_wf_ret_detail svd2
                         where svd2.wf_return_id = svd.wf_return_id
                           and svd2.customer_loc = svd.customer_loc
                           and svd2.wf_order_no = svd.wf_order_no
                           and svd2.return_loc_id = svd.return_loc_id
                           and svd2.return_method = svd.return_method
                           and svd2.item = svd.item
                           group by svd2.wf_return_id,
                                    svd2.customer_loc,
                                    svd2.return_loc_id,
                                    svd2.return_method,
                                    svd2.wf_order_no,
                                    svd2.item
                           having count(*) > 1));

   TYPE SVC_WFRETHEAD_TBL IS TABLE OF C_SVC_WF_RET_HEAD%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_wfrethead_tbl  SVC_WFRETHEAD_TBL;

   TYPE SVC_WFRETDETAIL_TBL IS TABLE OF C_SVC_WF_RET_DETAIL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_wfretdetail_tbl   SVC_WFRETDETAIL_TBL;

BEGIN

   ---- Process errors for head table.
   SQL_LIB.SET_MARK('OPEN','C_SVC_WF_RET_HEAD','SVC_WF_RET_HEAD',NULL);
   open C_SVC_WF_RET_HEAD;

   SQL_LIB.SET_MARK('FETCH','C_SVC_WF_RET_HEAD','SVC_WF_RET_HEAD',NULL);
   fetch C_SVC_WF_RET_HEAD BULK COLLECT INTO L_svc_wfrethead_tbl;

   SQL_LIB.SET_MARK('CLOSE','C_SVC_WF_RET_HEAD','SVC_WF_RET_HEAD',NULL);
   close C_SVC_WF_RET_HEAD;

   if L_svc_wfrethead_tbl is not NULL and L_svc_wfrethead_tbl.COUNT > 0 then
      FOR i in L_svc_wfrethead_tbl.first..L_svc_wfrethead_tbl.last LOOP
      --Customer_id is invalid
         if L_svc_wfrethead_tbl(i).valid_wf_customer_id is NULL then
            L_svc_wfrethead_tbl(i).error_msg := L_svc_wfrethead_tbl(i).error_msg ||
                                                SQL_LIB.GET_MESSAGE_TEXT('FR_WFCUST_ID_NOT_EXIST',
                                                                         L_svc_wfrethead_tbl(i).cust_ret_ref_no,
                                                                         NULL,
                                                                         NULL)|| ';';
         end if;
        --Currency code is invalid
         if(L_svc_wfrethead_tbl(i).valid_currency_code is NULL)then
            L_svc_wfrethead_tbl(i).error_msg := L_svc_wfrethead_tbl(i).error_msg ||
                                                SQL_LIB.GET_MESSAGE_TEXT('FR_INV_CURR_CODE',
                                                                         L_svc_wfrethead_tbl(i).cust_ret_ref_no,
                                                                         NULL,
                                                                         NULL)|| ';';
         end if;
      end loop;
   end if;

    --Update the status of the process id to error
   if L_svc_wfrethead_tbl is not NULL and L_svc_wfrethead_tbl.COUNT > 0 then
      FORALL i in 1..L_svc_wfrethead_tbl.last
         update svc_wf_ret_head
            set error_msg = L_svc_wfrethead_tbl(i).error_msg,
                process_status = LP_error,
                last_update_datetime = sysdate,
                last_update_id = LP_user
          where wf_return_id = L_svc_wfrethead_tbl(i).wf_return_id;
      O_val_ind := FALSE;
      return FALSE;
   end if;

    -- Process errors for detail table.
   SQL_LIB.SET_MARK('OPEN','C_SVC_WF_RET_DETAIL','SVC_WF_RET_DETAIL',NULL);
   open C_SVC_WF_RET_DETAIL;

   SQL_LIB.SET_MARK('FETCH','C_SVC_WF_RET_DETAIL','SVC_WF_RET_DETAIL',NULL);
   fetch C_SVC_WF_RET_DETAIL BULK COLLECT INTO L_svc_wfretdetail_tbl;

   SQL_LIB.SET_MARK('CLOSE','C_SVC_WF_RET_DETAIL','SVC_WF_RET_DETAIL',NULL);
   close C_SVC_WF_RET_DETAIL;

   if L_svc_wfretdetail_tbl is not NULL and L_svc_wfretdetail_tbl.COUNT > 0 then
      FOR i in 1..L_svc_wfretdetail_tbl.last loop
         -- Item is invalid
         if(L_svc_wfretdetail_tbl(i).valid_item is NULL or
            (L_svc_wfretdetail_tbl(i).valid_customer_loc is not NULL and L_svc_wfretdetail_tbl(i).valid_from_loc_itm is NULL)or
            (L_svc_wfretdetail_tbl(i).valid_item is not NULL 
             and (L_svc_wfretdetail_tbl(i).pack_ind = 'Y' or L_svc_wfretdetail_tbl(i).deposit_item_type in ('A','Z'))))then
             L_svc_wfretdetail_tbl(i).error_msg := L_svc_wfretdetail_tbl(i).error_msg ||
                                                   SQL_LIB.GET_MESSAGE_TEXT('FR_INV_ITEM',
                                                                            L_svc_wfretdetail_tbl(i).cust_ret_ref_no,
                                                                            L_svc_wfretdetail_tbl(i).line_no,
                                                                            NULL)|| ';';

         end if;
         -- Item is in deleted status at the customer or return location
         if(L_svc_wfretdetail_tbl(i).valid_item is not NULL AND
            L_svc_wfretdetail_tbl(i).valid_from_loc_itm is not NULL and
            (L_svc_wfretdetail_tbl(i).itm_from_loc_status = 'D' or NVL(L_svc_wfretdetail_tbl(i).itm_to_loc_status,'x') = 'D')) then
            L_svc_wfretdetail_tbl(i).error_msg := L_svc_wfretdetail_tbl(i).error_msg ||
                                                  SQL_LIB.GET_MESSAGE_TEXT('FR_INV_ITM_STATUS',
                                                                           L_svc_wfretdetail_tbl(i).cust_ret_ref_no,
                                                                           L_svc_wfretdetail_tbl(i).line_no,
                                                                           NULL)|| ';';
         end if;
         -- Unit of mesasure is invalid
         if(L_svc_wfretdetail_tbl(i).valid_item is not NULL AND
            L_svc_wfretdetail_tbl(i).valid_from_loc_itm is not NULL and
            L_svc_wfretdetail_tbl(i).itm_from_loc_status <> 'D' and
            L_svc_wfretdetail_tbl(i).valid_unit_of_measure <> L_svc_wfretdetail_tbl(i).unit_of_measure)then
            L_svc_wfretdetail_tbl(i).error_msg := L_svc_wfretdetail_tbl(i).error_msg ||
                                                  SQL_LIB.GET_MESSAGE_TEXT('FR_INV_UOM',
                                                                           L_svc_wfretdetail_tbl(i).cust_ret_ref_no,
                                                                           L_svc_wfretdetail_tbl(i).line_no,
                                                                           NULL)|| ';';
         end if;

          --Customer loc is invalid
         if(L_svc_wfretdetail_tbl(i).valid_customer_loc is NULL)then
            L_svc_wfretdetail_tbl(i).error_msg := L_svc_wfretdetail_tbl(i).error_msg ||
                                                  SQL_LIB.GET_MESSAGE_TEXT('FR_INV_CUST_LOC',
                                                                           L_svc_wfretdetail_tbl(i).cust_ret_ref_no,
                                                                           L_svc_wfretdetail_tbl(i).line_no,
                                                                           NULL)|| ';';
         end if;
          -- Return loc type is 'S' for a non stockholding store
         if(L_svc_wfretdetail_tbl(i).valid_customer_loc is not NULL and
            L_svc_wfretdetail_tbl(i).stockholding_ind = 'N' and
            L_svc_wfretdetail_tbl(i).return_loc_type  = 'S')then
            L_svc_wfretdetail_tbl(i).error_msg := L_svc_wfretdetail_tbl(i).error_msg ||
                                                  SQL_LIB.GET_MESSAGE_TEXT('FR_INV_RETLOC_TYPE',
                                                                           L_svc_wfretdetail_tbl(i).cust_ret_ref_no,
                                                                           L_svc_wfretdetail_tbl(i).line_no,
                                                                           NULL)|| ';';
         end if;
         -- Return method is 'D' for a  stockholding store
         if(L_svc_wfretdetail_tbl(i).valid_customer_loc is not NULL and
            L_svc_wfretdetail_tbl(i).stockholding_ind = 'Y' and
            L_svc_wfretdetail_tbl(i).return_method  = 'D')then
            L_svc_wfretdetail_tbl(i).error_msg := L_svc_wfretdetail_tbl(i).error_msg ||
                                                  SQL_LIB.GET_MESSAGE_TEXT('FR_INV_RET_METHOD',
                                                                           L_svc_wfretdetail_tbl(i).cust_ret_ref_no,
                                                                           L_svc_wfretdetail_tbl(i).line_no,
                                                                           NULL)|| ';';
         end if;
         --Wf customer id is not linked to the customer loc/wf_order_no
         if(L_svc_wfretdetail_tbl(i).valid_customer_loc is not NULL and
            L_svc_wfretdetail_tbl(i).valid_from_loc_itm is not NULL and
            (L_svc_wfretdetail_tbl(i).wf_customer_id <> L_svc_wfretdetail_tbl(i).valid_wf_customer_id
             or(L_svc_wfretdetail_tbl(i).valid_wf_order_no is not NULL and NVL(L_svc_wfretdetail_tbl(i).valid_ord_custid_ind,'X') <> 'Y'))) then

             L_svc_wfretdetail_tbl(i).error_msg := L_svc_wfretdetail_tbl(i).error_msg ||
                                                   SQL_LIB.GET_MESSAGE_TEXT('FR_INV_WFCUST_ID',
                                                                            L_svc_wfretdetail_tbl(i).cust_ret_ref_no,
                                                                            L_svc_wfretdetail_tbl(i).line_no,
                                                                            NULL)|| ';';
         end if;
         --Return loc is invalid
          if(L_svc_wfretdetail_tbl(i).valid_return_loc_id is NULL)then
            L_svc_wfretdetail_tbl(i).error_msg := L_svc_wfretdetail_tbl(i).error_msg ||
                                                  SQL_LIB.GET_MESSAGE_TEXT('FR_INV_RET_LOC_ID',
                                                                           L_svc_wfretdetail_tbl(i).cust_ret_ref_no,
                                                                           L_svc_wfretdetail_tbl(i).line_no,
                                                                           NULL)|| ';';

         end if;
         -- wf_order_no is not valid or
         --wf order no and item combination is not valid
         if(L_svc_wfretdetail_tbl(i).valid_wf_order_no is NULL
            or(L_svc_wfretdetail_tbl(i).valid_wf_order_no is not NULL and
               NVL(L_svc_wfretdetail_tbl(i).valid_orditm_ind,'X') <> 'Y'))then

            L_svc_wfretdetail_tbl(i).error_msg := L_svc_wfretdetail_tbl(i).error_msg ||
                                                  SQL_LIB.GET_MESSAGE_TEXT('FR_INV_WF_ITM_ORDR',
                                                                           L_svc_wfretdetail_tbl(i).cust_ret_ref_no,
                                                                           L_svc_wfretdetail_tbl(i).line_no,
                                                                           NULL)|| ';';
         end if;
           -- Duplicate wf return record
         if(L_svc_wfretdetail_tbl(i).count_rows > 1)then
            L_svc_wfretdetail_tbl(i).error_msg := L_svc_wfretdetail_tbl(i).error_msg ||
                                                  SQL_LIB.GET_MESSAGE_TEXT('FR_DUP_RETURN',
                                                                           L_svc_wfretdetail_tbl(i).cust_ret_ref_no,
                                                                           L_svc_wfretdetail_tbl(i).line_no,
                                                                           NULL)|| ';';
    
         end if;
       end loop;
   end if;

   if L_svc_wfretdetail_tbl is not NULL and L_svc_wfretdetail_tbl.COUNT > 0 then
   -- Update the detail table with the error_msg.
      FORALL i in 1..L_svc_wfretdetail_tbl.last
         update svc_wf_ret_detail
            set error_msg = L_svc_wfretdetail_tbl(i).error_msg,
                last_update_datetime = sysdate,
                last_update_id = LP_user
          where rowid = L_svc_wfretdetail_tbl(i).rowid;

      -- Update header table for errors in detail table.
      FORALL i in 1..L_svc_wfretdetail_tbl.COUNT
         update svc_wf_ret_head
            set error_msg = SQL_LIB.GET_MESSAGE_TEXT('FR_ERROR_SVC_DETAIL',
                                                     L_svc_wfretdetail_tbl(i).cust_ret_ref_no,
                                                     NULL,
                                                     NULL)|| ';',
                process_status = LP_error,
                last_update_datetime = sysdate,
                last_update_id = LP_user
          where wf_return_id = L_svc_wfretdetail_tbl(i).wf_return_id;
      O_val_ind := FALSE;
      return FALSE;
   end if;
   -- For no errors, update the status to Validated for all the records.
   update svc_wf_ret_head
      set process_status = LP_validate,
          last_update_datetime = sysdate,
          last_update_id = LP_user
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and process_status = LP_new;
      
   O_val_ind := TRUE;   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_WF_RET_UPLD;
---------------------------------------------------------------------------------------------
FUNCTION POPULATE_RETURN_REC(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_f_return_head_tbl   IN OUT OBJ_F_RETURN_HEAD_TBL,
                             O_f_return_detail_tbl IN OUT OBJ_F_RETURN_DETAIL_TBL,
                             I_process_id          IN  SVC_WF_RET_HEAD.PROCESS_ID%TYPE,
                             I_chunk_id            IN  SVC_WF_RET_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_system_options            SYSTEM_OPTIONS%ROWTYPE;
   L_return_unit_cost          WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE;
   L_return_unit_cost_oth      WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE;
   L_return_unit_cost_ret_curr WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE;
   L_unit_restock_fee          WF_RETURN_DETAIL.UNIT_RESTOCK_FEE%TYPE;
   L_rma_no                    WF_RETURN_HEAD.RMA_NO%TYPE;
   L_virtual_wh                WH.WH%TYPE;
   L_return_loc_id             WF_RETURN_HEAD.RETURN_LOC_ID%TYPE;
   L_f_return_detail_tbl       OBJ_F_RETURN_DETAIL_TBL;
   L_f_cont_detail_tbl         OBJ_F_RETURN_DETAIL_TBL;
   L_f_return_head_rec         OBJ_F_RETURN_HEAD_REC                 := NULL;
   L_f_return_detail_oth_rec   OBJ_F_RETURN_DETAIL_REC               := NULL;
   L_effective_date            CURRENCY_RATES.EFFECTIVE_DATE%TYPE    := GET_VDATE;
   L_exchange_type             CURRENCY_RATES.EXCHANGE_TYPE%TYPE     := NULL;
   L_program                   VARCHAR2(64)                          := 'CORESVC_WF_RETURN_UPLOAD_SQL.POPULATE_RETURN_REC';
   L_table                     VARCHAR2(50);
   L_wf_order_no               WF_ORDER_DETAIL.WF_ORDER_NO%TYPE;
   L_order_currency            WF_ORDER_HEAD.currency_code%TYPE;
   
  -- Cursor C_RETURN_HEAD_TBL is to get the wf return head record
   cursor C_RETURN_HEAD_TBL is
      select svh.wf_return_id,
             svh.cust_ret_ref_no,
             svh.wf_customer_id,
             svh.currency_code   return_currency,
             svh.comments,
             svd.return_method,
             svd.customer_loc,
             svd.return_loc_type,
             svd.return_loc_id,
             'I' status          ---Transfer will be initially created in Input status and later approved
        from svc_wf_ret_head svh,
             svc_wf_ret_detail svd
       where svd.wf_return_id = svh.wf_return_id
         and svh.process_id = I_process_id
         and svh.chunk_id = I_chunk_id
         and svh.process_status = LP_validate
       group by svh.wf_return_id,
                svh.wf_customer_id,
                svh.currency_code,
                svh.comments,
                svd.return_method,
                svd.customer_loc,
                svd.return_loc_type,
                svd.return_loc_id,
                svh.cust_ret_ref_no;

    TYPE RETURN_HEAD_TBL is TABLE OF C_RETURN_HEAD_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
    L_head_tbl  RETURN_HEAD_TBL;

    L_head_rec   C_RETURN_HEAD_TBL%ROWTYPE;

  -- Cursor C_RETURN_DETAIL_TBL is to get the wf return detail record 
  -- when return unit cost is populated.
   cursor C_RETURN_DETAIL_TBL is
      select OBJ_F_RETURN_DETAIL_REC(L_f_return_head_rec.rma_no,
                                     svd.item,
                                     svd.wf_order_no,
                                     svd.returned_qty,
                                     svd.return_reason,
                                     svd.return_unit_cost,
                                     NULL,                --Cancel Reason
                                     svd.restock_type,
                                     svd.unit_restock_fee)
        from svc_wf_ret_detail svd
       where svd.process_id = I_process_id
         and svd.chunk_id = I_chunk_id
         and svd.return_unit_cost is not NULL
         and svd.wf_return_id =  L_head_rec.wf_return_id
         and svd.customer_loc =  L_head_rec.customer_loc
         and svd.return_loc_id = L_head_rec.return_loc_id
         and svd.return_method = L_head_rec.return_method;

   -- For Deposit Container item, the return cost has to be fetched from wf_order_detail table. 
   cursor C_GET_CONTAINER_DETAILS is
      select OBJ_F_RETURN_DETAIL_REC(L_f_return_head_rec.rma_no,
                                     cont_item.item,
                                     cont_item.wf_order_no,
                                     SUM(cont_item.returned_qty),
                                     MIN(cont_item.return_reason),
                                     cont_item.customer_cost,
                                     NULL,                              -- cancel Reason
                                     NULL,                              -- restock_type
                                     NULL)                              -- restock_fee
        from (select im.container_item item,
                     wfd.wf_order_no wf_order_no,
                     SUM(svd.returned_qty) returned_qty,
                     MIN(NVL(wfd.fixed_cost,wfd.customer_cost)) customer_cost,
                     MIN(svd.return_reason) return_reason
                from svc_wf_ret_detail svd,
                     item_master im,
                     wf_order_detail wfd
               where im.deposit_item_type = 'E'
                 and wfd.wf_order_no = svd.wf_order_no
                 and svd.item = im.item
                 and im.container_item = wfd.item
                 and svd.wf_return_id =  L_head_rec.wf_return_id
                 and svd.customer_loc =  L_head_rec.customer_loc
                 and svd.return_loc_id = L_head_rec.return_loc_id
                 and svd.return_method = L_head_rec.return_method
               group by im.container_item,
                        wfd.wf_order_no) cont_item
     group by cont_item.item,
              cont_item.wf_order_no,
              cont_item.customer_cost;

   -- Fetch the rows where return unit cost is not populated.
   cursor C_GET_RETURN_DETAIL_OTHERS is 
      select svd.item,
             svd.wf_order_no,
             svd.returned_qty,
             svd.return_reason,
             NULL return_unit_cost, --Return unit cost - this will be fetched later
             NULL cancel_reason,    --Cancel Reason
             svd.restock_type,
             svd.unit_restock_fee
        from svc_wf_ret_detail svd
       where svd.process_id = I_process_id
         and svd.chunk_id = I_chunk_id
         and svd.return_unit_cost is NULL
         and svd.wf_return_id =  L_head_rec.wf_return_id
         and svd.customer_loc =  L_head_rec.customer_loc
         and svd.return_loc_id = L_head_rec.return_loc_id
         and svd.return_method = L_head_rec.return_method;

   cursor C_GET_ORDER_CURRENCY is 
      select wfh.currency_code  order_currency
        from wf_order_head wfh
       where wfh.wf_order_no = L_wf_order_no;
 
   TYPE RETURN_DETAIL_TBL is TABLE OF C_GET_RETURN_DETAIL_OTHERS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_f_return_detail_oth_tbl  RETURN_DETAIL_TBL;

BEGIN
   --Get the consolidation indicator from system config options
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      return FALSE;
   end if;

   O_f_return_head_tbl          := OBJ_F_RETURN_HEAD_TBL();
   O_f_return_detail_tbl        := OBJ_F_RETURN_DETAIL_TBL();
   L_f_return_detail_tbl        := OBJ_F_RETURN_DETAIL_TBL();
   L_f_cont_detail_tbl          := OBJ_F_RETURN_DETAIL_TBL();

   if L_system_options.consolidation_ind = 'Y' then
      L_exchange_type := 'C';
   else
      L_exchange_type := 'O';
   end if;

   open  C_RETURN_HEAD_TBL;
   fetch C_RETURN_HEAD_TBL BULK COLLECT INTO L_head_tbl;
   close C_RETURN_HEAD_TBL;

   --- update the status to completed for all the empty files
   if(L_head_tbl IS NULL OR L_head_tbl.COUNT = 0) then
      update svc_wf_ret_head
         set process_status = LP_complete,
             last_update_datetime = sysdate,
             last_update_id = LP_user
       where process_id = I_process_id
         and chunk_Id = I_chunk_Id
         and process_Status = LP_Validate;
      ---         
      return TRUE;
   end if;
   ---
   if L_head_tbl is not NULL AND L_head_tbl.COUNT > 0 then
      for i in L_head_tbl.first..L_head_tbl.last loop
         L_head_rec.wf_return_id  := L_head_tbl(i).wf_return_id;
         L_head_rec.customer_loc  := L_head_tbl(i).customer_loc;
         L_head_rec.return_loc_id := L_head_tbl(i).return_loc_id;
         L_head_rec.return_method := L_head_tbl(i).return_method;

         if(L_head_tbl(i).return_loc_type = 'W')then
            --Calling the distribution_sql to find virtual wh when the return loc type is 'W'
            if DISTRIBUTION_SQL.FIND_MAPPING_VWH(O_error_message,
                                                 L_virtual_wh,
                                                 L_head_tbl(i).return_loc_id,
                                                 'T',                          ---I_to_from_ind
                                                 L_head_tbl(i).customer_loc,   ---I_other_loc_type
                                                 'S',                          ---I_cust_order_loc_ind
                                                 'N') = FALSE then
               return FALSE;
            end if;
            L_return_loc_id := L_virtual_wh;
         else 
            L_return_loc_id := L_head_tbl(i).return_loc_id;
         end if; 
         ---
         select wf_rma_no_seq.nextval into L_rma_no from dual;
         ---        
         L_f_return_head_rec :=  OBJ_F_RETURN_HEAD_REC(L_rma_no,
                                                       L_head_tbl(i).cust_ret_ref_no,
                                                       'E',                          ---return_type - EDI
                                                       L_head_tbl(i).customer_loc,
                                                       L_head_tbl(i).return_method,
                                                       L_head_tbl(i).return_loc_type,
                                                       L_return_loc_id,
                                                       L_head_tbl(i).return_currency,
                                                       L_head_tbl(i).status,
                                                       NULL,                          ---cancel_reason
                                                       L_head_tbl(i).comments,
                                                       NULL);                         ----OBJ_F_RETURN_DETAIL_TBL
         O_f_return_head_tbl.extend;
         O_f_return_head_tbl(O_f_return_head_tbl.count) := L_f_return_head_rec;
         
         -- Fetch detail data having the details present in staging table.
         open  C_RETURN_DETAIL_TBL;
         fetch C_RETURN_DETAIL_TBL BULK COLLECT INTO L_f_return_detail_tbl;
         close C_RETURN_DETAIL_TBL;

         O_f_return_detail_tbl := O_f_return_detail_tbl multiset union L_f_return_detail_tbl;

         -- Process deposit container item
         open  C_GET_CONTAINER_DETAILS;
         fetch C_GET_CONTAINER_DETAILS BULK COLLECT INTO L_f_cont_detail_tbl;
         close C_GET_CONTAINER_DETAILS;     

         if L_f_cont_detail_tbl is not NULL and L_f_cont_detail_tbl.COUNT > 0 then
            for j in L_f_cont_detail_tbl.first..L_f_cont_detail_tbl.last loop
			   
			   L_wf_order_no := L_f_cont_detail_tbl(j).wf_order_no;
               
               open C_GET_ORDER_CURRENCY;
               fetch C_GET_ORDER_CURRENCY into L_order_currency;
               close C_GET_ORDER_CURRENCY;
			   
               if(L_head_tbl(i).return_currency <> L_order_currency)then
                  L_return_unit_cost := L_f_cont_detail_tbl(j).return_unit_cost;

                  if CURRENCY_SQL.CONVERT(O_error_message,
                                          L_return_unit_cost,
                                          L_order_currency,
                                          L_head_tbl(i).return_currency,
                                          L_return_unit_cost_ret_curr,
                                          'C',
                                          L_effective_date,
                                          L_exchange_type) = FALSE then
                     return FALSE;
                  end if;
                  L_f_cont_detail_tbl(j).return_unit_cost :=  L_return_unit_cost_ret_curr;
               end if;
            end loop;
            O_f_return_detail_tbl := O_f_return_detail_tbl multiset union L_f_cont_detail_tbl;
         end if;
         
         -- Process records where return unit cost is not present.
         open  C_GET_RETURN_DETAIL_OTHERS;
         fetch C_GET_RETURN_DETAIL_OTHERS BULK COLLECT INTO L_f_return_detail_oth_tbl;
         close C_GET_RETURN_DETAIL_OTHERS;
         
         if L_f_return_detail_oth_tbl is not NULL and L_f_return_detail_oth_tbl.count > 0 then
            for k in L_f_return_detail_oth_tbl.first..L_f_return_detail_oth_tbl.last loop 
               -- Get the return unit cost which will fetch wf order cost if the item is part of the 
               -- franchise order. For return of component item where pack item existed in the original 
               -- order, it will prorate the order cost to its component.
               if WF_RETURN_SQL.GET_ITEM_RETURN_COST(O_error_message,
                                                     L_return_unit_cost_oth,
                                                     L_head_tbl(i).return_currency,
                                                     L_head_tbl(i).customer_loc,
                                                     L_f_return_detail_oth_tbl(k).wf_order_no,
                                                     L_f_return_detail_oth_tbl(k).item) = FALSE then
                  return FALSE;
               end if;
               ---
               L_f_return_detail_oth_rec := OBJ_F_RETURN_DETAIL_REC(L_f_return_head_rec.rma_no,
                                                                    L_f_return_detail_oth_tbl(k).item,
                                                                    L_f_return_detail_oth_tbl(k).wf_order_no,
                                                                    L_f_return_detail_oth_tbl(k).returned_qty,
                                                                    L_f_return_detail_oth_tbl(k).return_reason,
                                                                    L_return_unit_cost_oth,
                                                                    NULL,                                         --cancel reason
                                                                    L_f_return_detail_oth_tbl(k).restock_type,
                                                                    L_f_return_detail_oth_tbl(k).unit_restock_fee);  
               O_f_return_detail_tbl.extend;
               O_f_return_detail_tbl(O_f_return_detail_tbl.count) := L_f_return_detail_oth_rec;  
            end loop;
         end if;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when others then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END POPULATE_RETURN_REC;
--------------------------------------------------------------------------------------
FUNCTION RANGE_CONTAINER(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_f_return_head_tbl   IN OBJ_F_RETURN_HEAD_TBL,
                         I_f_return_detail_tbl IN OBJ_F_RETURN_DETAIL_TBL)
RETURN BOOLEAN IS 

   L_program                  VARCHAR2(50)                         := 'CORESVC_WF_RETURN_UPLOAD_SQL.RANGE_CONTAINER';
   L_content_item             ITEM_MASTER.ITEM%TYPE                := NULL;
   L_container_item           ITEM_MASTER.ITEM%TYPE                := NULL;
   L_return_loc_id            WF_RETURN_HEAD.RETURN_LOC_ID%TYPE    := NULL;
   L_return_loc_type          WF_RETURN_HEAD.RETURN_LOC_TYPE%TYPE  := NULL;
   L_customer_loc             WF_RETURN_HEAD.CUSTOMER_LOC%TYPE     := NULL;
   L_container_retloc_exist   VARCHAR2(1)                          := NULL;
   L_container_cust_loc_exist VARCHAR2(1)                          := NULL;
   L_retloc_ranged_ind        VARCHAR2(1)                          := NULL;
   L_cust_ranged_ind          VARCHAR2(1)                          := NULL;
   
   cursor C_CONTAINER_ITEMS is
      select wf_detail.item content_item,
             im.container_item                    container_item,
             il.ranged_ind                        content_custloc_ranged_ind,
             wf_head.return_loc_id,
             wf_head.return_loc_type,
             wf_head.customer_loc,
             (select ranged_ind
                from item_loc
               where item = im.container_item 
                 and loc = wf_head.customer_loc)  container_custloc_ranged_ind, 
             (select ranged_ind 
                from item_loc
               where item = wf_detail.item 
                 and loc = wf_head.return_loc_id) content_retloc_ranged_ind,
             (select ranged_ind
                from item_loc
               where item = im.container_item
                 and loc = wf_head.return_loc_id) container_retloc_ranged_ind     
        from TABLE(CAST(I_f_return_head_tbl AS OBJ_F_RETURN_HEAD_TBL)) wf_head,
             TABLE(CAST(I_f_return_detail_tbl AS OBJ_F_RETURN_DETAIL_TBL))wf_detail,
             item_master im,
             item_loc il
       where wf_head.rma_no = wf_detail.rma_no
         and wf_detail.item = im.item
         and im.item = il.item
         and il.loc = wf_head.customer_loc  
         and im.deposit_item_type = 'E';
    
     TYPE TYP_container_item is TABLE of C_CONTAINER_ITEMS%ROWTYPE;  
     TBL_container_item             TYP_container_item;
     
BEGIN
   open  C_CONTAINER_ITEMS;
   fetch C_CONTAINER_ITEMS BULK COLLECT into TBL_container_item;
   close C_CONTAINER_ITEMS;
   
   if TBL_container_item is NOT NULL and TBL_container_item.COUNT > 0 then
      FOR i in TBL_container_item.FIRST..TBL_container_item.LAST LOOP
         L_content_item             := TBL_container_item(i).content_item;
         L_container_item           := TBL_container_item(i).container_item;
         L_customer_loc             := TBL_container_item(i).customer_loc;
         L_return_loc_id            := TBL_container_item(i).return_loc_id;
         L_return_loc_type          := TBL_container_item(i).return_loc_type;
         L_container_retloc_exist   := TBL_container_item(i).container_retloc_ranged_ind;
         L_container_cust_loc_exist := TBL_container_item(i).container_custloc_ranged_ind;
         L_retloc_ranged_ind        := TBL_container_item(i).content_retloc_ranged_ind;
         L_cust_ranged_ind          := TBL_container_item(i).content_custloc_ranged_ind;
         
         --- Check if container item is ranged to customer location

         if L_container_cust_loc_exist is NULL then
            ---If container is not ranged to customer location range it with ranged indicator same as that of content item
            if NEW_ITEM_LOC(O_error_message,
                            L_container_item,
                            L_customer_loc,
                            NULL,
                            NULL,
                            'S',
                            NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL,
                            NULL, L_cust_ranged_ind) = FALSE then
                return FALSE;
            end if;
         end if;
          --Check if content item is ranged to return location

         if L_retloc_ranged_ind is NOT NULL then
            --Check if container item is ranged to return location  
            --If content is ranged to return location and container is not ranged
            -- then range container item also to  return location
              
            if L_container_retloc_exist is NULL then
               if NEW_ITEM_LOC(O_error_message,
                               L_container_item,
                               L_return_loc_id,
                               NULL,
                               NULL,
                               L_return_loc_type,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL,
                               NULL, L_retloc_ranged_ind) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if; 
      END LOOP;
   end if;  
   return TRUE;  
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RANGE_CONTAINER;
----------------------------------------------------------------------------------
FUNCTION PERSIST_WF_RET_UPLD(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_f_return_head_tbl   IN OBJ_F_RETURN_HEAD_TBL,
                             I_f_return_detail_tbl IN OBJ_F_RETURN_DETAIL_TBL)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(64) := 'CORESVC_WF_RETURN_UPLOAD_SQL.PERSIST_WF_RET_UPLD';
   L_rma_no                  WF_RETURN_HEAD.RMA_NO%TYPE;
   L_approve_ind             VARCHAR2(1) := NULL;
   
   -- Cursor C_GET_RMA_APPROVAL_IND is to get the approval indicator for the RMA no for which
   -- The item and return location ranging does not exist.
   -- The item does not have inventory at the source location.
   -- The auto_approve_ind  for the the wf_customer 'N'.
   
   cursor C_GET_RMA_APPROVAL_IND IS
        select 'N'
          from TABLE(CAST(I_f_return_head_tbl AS OBJ_F_RETURN_HEAD_TBL)) wf_head,
               TABLE(CAST(I_f_return_detail_tbl AS OBJ_F_RETURN_DETAIL_TBL))wf_detail
         where wf_head.rma_no = wf_detail.rma_no
           and wf_head.rma_no = L_rma_no
           and (not exists(select 'x'                                      
                             from wf_customer wc,
                                  store s
                            where s.store = wf_head.customer_loc
                              and s.wf_customer_id = wc.wf_customer_id
                              and wc.auto_approve_ind = 'Y')    
                or not exists (select 'x'                                 
                                 from item_loc il
                                where il.item = wf_detail.item
                                  and il.loc  = wf_head.return_loc_id
                                  and il.status <> 'D')         
                or exists (select 'x'                                       
                             from item_loc_soh ils,
                                  item_master im,
                                  store s
                            where ils.item = wf_detail.item
                              and ils.loc  = wf_head.customer_loc
                              and ils.item = im.item
                              and im.inventory_ind = 'Y'
                              and ils.loc = s.store
                              and s.stockholding_ind = 'Y' 
                              and wf_detail.returned_qty > GREATEST(ils.stock_on_hand - (GREATEST(ils.tsf_reserved_qty,0)+
                                                                                         GREATEST(ils.customer_resv,0)+
                                                                                         GREATEST(ils.rtv_qty,0)+
                                                                                         GREATEST(ils.non_sellable_qty,0)),0) ))
           and rownum = 1;

BEGIN

   insert into wf_return_head(rma_no,
                              status,
                              cust_ret_ref_no,
                              customer_loc,
                              return_loc_type,
                              return_loc_id,
                              return_type,
                              return_method,
                              total_return_amt,
                              total_restock_amt,
                              total_net_return_amt,
                              currency_code,
                              comments,
                              cancel_reason,
                              create_id,
                              create_datetime,
                              last_update_id,
                              last_update_datetime
                              )
                             (select wf_head.rma_no,
                                     wf_head.status,
                                     wf_head.cust_ret_ref_no,
                                     wf_head.customer_loc,
                                     wf_head.return_loc_type,
                                     wf_head.return_loc_id,
                                     wf_head.return_type,
                                     wf_head.return_method,
                                     SUM(wf_detail.return_unit_cost * wf_detail.returned_qty),                                                            ---total_return_amt,
                                     NVL(SUM(case when wf_detail.restock_type = 'S' then wf_detail.unit_restock_fee 
                                                  when wf_detail.restock_type = 'V' then wf_detail.return_unit_cost * wf_detail.unit_restock_fee/100 
                                                  else 0
                                             end * wf_detail.returned_qty ),0),                                                                           ---total_restock_amt,
                                     SUM(wf_detail.return_unit_cost * wf_detail.returned_qty 
                                          - NVL(case when wf_detail.restock_type = 'S' then wf_detail.unit_restock_fee 
                                                     when wf_detail.restock_type = 'V' then wf_detail.return_unit_cost * wf_detail.unit_restock_fee/100 
                                                     else 0
                                                end * wf_detail.returned_qty ,0)),                                                              ---total_net_return_amt,
                                     wf_head.currency_code,
                                     wf_head.comments,
                                     NULL,                                                                                                      ---cancel reason
                                     LP_user,                                                                                                   ---create_id
                                     sysdate,                                                                                                   ---create_datetime
                                     LP_user,                                                                                                   ---last_update_id
                                     sysdate                                                                                                    ---last_update_datetime
                                from TABLE(CAST(I_f_return_head_tbl AS OBJ_F_RETURN_HEAD_TBL)) wf_head,
                                     TABLE(CAST(I_f_return_detail_tbl AS OBJ_F_RETURN_DETAIL_TBL))wf_detail
                               where wf_head.rma_no = wf_detail.rma_no
                             group by wf_head.rma_no,
                                      wf_head.status,
                                      wf_head.customer_loc,
                                      wf_head.return_loc_id,
                                      wf_head.comments,
                                      wf_head.currency_code,
                                      wf_head.return_type,
                                      wf_head.return_method,
                                      wf_head.return_loc_type,
                                      wf_head.cust_ret_ref_no);

   insert into wf_return_detail(rma_no,
                                item,
                                wf_order_no,
                                returned_qty,
                                return_reason,
                                return_unit_cost,
                                restock_type,
                                unit_restock_fee,
                                net_return_unit_cost,
                                cancel_reason,
                                create_datetime,
                                create_id,
                                last_update_datetime,
                                last_update_id
                                )
                               (select wf_head.rma_no,
                                       wf_detail.item,
                                       wf_detail.wf_order_no,
                                       wf_detail.returned_qty,
                                       wf_detail.return_reason,
                                       wf_detail.return_unit_cost,
                                       wf_detail.restock_type,
                                       wf_detail.unit_restock_fee,
                                       (wf_detail.return_unit_cost -
                                             case when wf_detail.restock_type = 'S' then wf_detail.unit_restock_fee 
                                                  when wf_detail.restock_type = 'V' then wf_detail.return_unit_cost * wf_detail.unit_restock_fee/100
                                                  else 0
                                             end),                                                       ---net_return_unit_cost
                                       NULL,                                                             ---cancel_reason
                                       sysdate,                                                          ---create_datetime
                                       LP_user,                                                          ---create_id
                                       sysdate,                                                          ---last_update_datetime
                                       LP_user                                                           ---last_update_id
                                  from TABLE(CAST(I_f_return_head_tbl AS OBJ_F_RETURN_HEAD_TBL)) wf_head,
                                       TABLE(CAST(I_f_return_detail_tbl AS OBJ_F_RETURN_DETAIL_TBL))wf_detail
                                 where wf_head.rma_no = wf_detail.rma_no);

   -- If for any RMA, the returned item is not ranged to the return location, do not force range the item.
   -- Keep the return in Input status. Approve the remainingg RMA's.
   if I_f_return_head_tbl is not NULL and I_f_return_head_tbl.count > 0 then
      for i in I_f_return_head_tbl.first..I_f_return_head_tbl.last LOOP
         -- Check for ranging and inventory availability. 
         L_rma_no := I_f_return_head_tbl(i).rma_no;
         
         open  C_GET_RMA_APPROVAL_IND;
         fetch C_GET_RMA_APPROVAL_IND into L_approve_ind;
         if (C_GET_RMA_APPROVAL_IND%ROWCOUNT) = 0 then
            L_approve_ind := 'Y';
         end if;
         close C_GET_RMA_APPROVAL_IND;        
         
         if L_approve_ind = 'Y' then
            if wf_return_sql.approve(O_error_message,
                                     L_rma_no) = false then
               return FALSE;
            end if;
         end if;
      end loop;      
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PERSIST_WF_RET_UPLD;
-------------------------------------------------------------------------------------------
END CORESVC_WF_RETURN_UPLOAD_SQL;
/