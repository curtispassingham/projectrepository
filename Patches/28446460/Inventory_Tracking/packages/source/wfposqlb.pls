CREATE OR REPLACE PACKAGE BODY WF_PO_SQL AS

TYPE ORDHEAD_TBL is TABLE of ORDHEAD%ROWTYPE;
TYPE ORDSKU_TBL is TABLE of ORDSKU%ROWTYPE;
TYPE ORDLOC_TBL is TABLE of ORDLOC%ROWTYPE;
TYPE ORDER_REC is RECORD (order_no   ORDHEAD.ORDER_NO%TYPE,
                          supplier   ORDHEAD.SUPPLIER%TYPE);
TYPE ORDER_TBL is TABLE of ORDER_REC;

LP_vdate              DATE := GET_VDATE();
LP_import_ind         SYSTEM_OPTIONS.IMPORT_IND%TYPE;
LP_elc_ind            SYSTEM_OPTIONS.ELC_IND%TYPE;
LP_base_country_id    SYSTEM_OPTIONS.BASE_COUNTRY_ID%TYPE;
LP_latest_ship_days   SYSTEM_OPTIONS.LATEST_SHIP_DAYS%TYPE;
LP_dept_level_orders  SYSTEM_OPTIONS.DEPT_LEVEL_ORDERS%TYPE;
LP_order_type         ORDHEAD.ORDER_TYPE%TYPE;
LP_new_from_mod       VARCHAR2(1) := 'N';

LP_ordhead_tbl        ORDHEAD_TBL := ORDHEAD_TBL();
LP_ordsku_tbl         ORDSKU_TBL  := ORDSKU_TBL();
LP_ordloc_tbl         ORDLOC_TBL  := ORDLOC_TBL();
LP_order_tbl          ORDER_TBL;

----------------------------------------------------------------------------------------
--Name     : POPULATE_CREATE_REC
--Function : This function will populate the pl/sql global collections with data from
--           the WF tables in the database.
----------------------------------------------------------------------------------------
FUNCTION POPULATE_CREATE_REC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : PERSIST_CREATE_PO
--Function : This function will create the actual order records for Franchise POs in the
--           ORDHEAD, ORDSKU and ORDLOC tables.  This will also perform the other
--           processing for the PO such as applying default expenses and apply off
--           invoice deals.
----------------------------------------------------------------------------------------
FUNCTION PERSIST_CREATE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_scaling         IN       VARCHAR2,
                           I_undo_scaling    IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : GET_ITEM_INFO
--Function : This function will fetch order header and detail records to be used in
--           inserting records to the order tables.
----------------------------------------------------------------------------------------
FUNCTION GET_ITEM_INFO (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_import_order_ind    IN OUT   ORDHEAD.IMPORT_ORDER_IND%TYPE,
                        O_import_country_id   IN OUT   ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                        O_import_id           IN OUT   ORDHEAD.IMPORT_ID%TYPE,
                        O_import_type         IN OUT   ORDHEAD.IMPORT_TYPE%TYPE,
                        O_routing_loc_id      IN OUT   ORDHEAD.ROUTING_LOC_ID%TYPE,
                        O_clearing_zone_id    IN OUT   ORDHEAD.CLEARING_ZONE_ID%TYPE,
                        O_supp_pack_size      IN OUT   ORDSKU.SUPP_PACK_SIZE%TYPE,
                        O_unit_cost           IN OUT   ORDLOC.UNIT_COST%TYPE,
                        O_unit_retail         IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                        O_origin_country      IN OUT   ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                        I_item                IN       ITEM_MASTER.ITEM%TYPE,
                        I_supplier            IN       ITEM_SUPPLIER.ITEM%TYPE,
                        I_location            IN       ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : VALIDATE_F_PO_DELETE
--Function : This function will validate the Purchase Order to be deleted.
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_F_PO_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_shipped          IN OUT   BOOLEAN,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : VALIDATE_F_PO_DTL_DELETE
--Function : This function will validate the item of an existing Purchase Order to be deleted.
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_F_PO_DTL_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_shipped         IN OUT   BOOLEAN,
                                  I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                                  I_item            IN       ORDLOC.ITEM%TYPE,
                                  I_location        IN       ORDHEAD.LOCATION%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : CHECK_ORDER_SHIPPED
--Function : This function will check if the PO/item has an existing shipment.
----------------------------------------------------------------------------------------
FUNCTION CHECK_ORDER_SHIPPED(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_shipped         IN OUT   BOOLEAN,
                             I_order_no        IN       ORDLOC.ORDER_NO%TYPE,
                             I_item            IN       ORDLOC.ITEM%TYPE,
                             I_location        IN       ORDLOC.LOCATION%TYPE,
                             I_loc_type        IN       ORDLOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : DELETE_ORDER
--Function : This function will delete the PO based on the given order number.
----------------------------------------------------------------------------------------
FUNCTION DELETE_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_shipped         IN       BOOLEAN,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : DELETE_ORDER_ITEM
--Function : This function will delete an item in an existing order based on the
--           given order number.
----------------------------------------------------------------------------------------
FUNCTION DELETE_ORDER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_shipped         IN       BOOLEAN,
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                           I_item            IN       ORDLOC.ITEM%TYPE,
                           I_location        IN       ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION CREATE_FRANCHISE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                             I_scaling         IN       VARCHAR2,
                             I_undo_scaling    IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program           VARCHAR2(50)   := 'WF_PO_SQL.CREATE_FRANCHISE_PO';
   L_chk_wf_order      VARCHAR2(1)    := NULL;

   cursor C_CHK_WF_ORDER is
      select 'x'
        from wf_order_head wh
       where wh.wf_order_no = I_wf_order_no;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_wf_order_no,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_CHK_WF_ORDER;
   fetch C_CHK_WF_ORDER into L_chk_wf_order;
   close C_CHK_WF_ORDER;

   if L_chk_wf_order is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_WF_ORDER_NO',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if POPULATE_CREATE_REC(O_error_message,
                          I_wf_order_no) = FALSE then
      return FALSE;
   end if;

   if PERSIST_CREATE_PO(O_error_message,
                        I_scaling,
                        I_undo_scaling) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_FRANCHISE_PO;
----------------------------------------------------------------------------------------
FUNCTION POPULATE_CREATE_REC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50)     := 'WF_PO_SQL.POPULATE_CREATE_REC';
   L_order_no            ORDHEAD.ORDER_NO%TYPE;
   L_import_order_ind    ORDHEAD.IMPORT_ORDER_IND%TYPE;
   L_import_country_id   ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   L_routing_loc_id      ORDHEAD.ROUTING_LOC_ID%TYPE;
   L_clearing_zone_id    ORDHEAD.CLEARING_ZONE_ID%TYPE;
   L_prev_na_date        ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_prev_nb_date        ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_na_date             ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_nb_date             ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_dept                ORDHEAD.DEPT%TYPE;
   L_unit_retail         ORDLOC.UNIT_RETAIL%TYPE;
   L_curr_supplier       SUPS.SUPPLIER%TYPE;
   L_prev_supplier       SUPS.SUPPLIER%TYPE;
   L_currency_code       SUPS.CURRENCY_CODE%TYPE;
   L_terms               SUPS.TERMS%TYPE;
   L_freight_terms       SUPS.FREIGHT_TERMS%TYPE;
   L_sup_status          SUPS.SUP_STATUS%TYPE;
   L_qc_ind              SUPS.QC_IND%TYPE;
   L_edi_po_ind          SUPS.EDI_PO_IND%TYPE;
   L_pre_mark_ind        SUPS.PRE_MARK_IND%TYPE;
   L_ship_method         SUPS.SHIP_METHOD%TYPE;
   L_payment_method      SUPS.PAYMENT_METHOD%TYPE;
   L_import_id           SUPS_IMP_EXP.IMPORT_ID%TYPE;
   L_import_type         SUPS_IMP_EXP.IMPORT_TYPE%TYPE;
   L_origin_country      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_unit_cost           ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE;
   L_supp_pack_size      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_curr_cost_loc       ITEM_LOC.LOC%TYPE;
   L_prev_cost_loc       ITEM_LOC.LOC%TYPE;
   L_curr_dept           ITEM_MASTER.DEPT%TYPE;
   L_prev_dept           ITEM_MASTER.DEPT%TYPE;
   L_curr_cust_loc       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE := NULL;
   L_prev_cust_loc       WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE := NULL;
   L_exchange_rate       CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_cre_new_po          VARCHAR2(1) := 'N';
   L_otb_eow_date        DATE;
   L_agent               SUP_IMPORT_ATTR.AGENT%TYPE;
   L_lading_port         SUP_IMPORT_ATTR.LADING_PORT%TYPE;
   L_discharge_port      SUP_IMPORT_ATTR.DISCHARGE_PORT%TYPE;
   L_factory             SUP_IMPORT_ATTR.FACTORY%TYPE;
   L_partner_type_1      SUP_IMPORT_ATTR.PARTNER_TYPE_1%TYPE;
   L_partner1            SUP_IMPORT_ATTR.PARTNER_1%TYPE;
   L_partner_type_2      SUP_IMPORT_ATTR.PARTNER_TYPE_2%TYPE;
   L_partner2            SUP_IMPORT_ATTR.PARTNER_2%TYPE;
   L_partner_type_3      SUP_IMPORT_ATTR.PARTNER_TYPE_3%TYPE;
   L_partner3            SUP_IMPORT_ATTR.PARTNER_3%TYPE;

   cursor C_GET_WF_DTL_ORDER is
      select wd.wf_order_no,
             wd.item,
             wd.source_loc_id,
             wd.customer_loc,
             wd.requested_qty,
             wd.need_date,
             wd.not_after_date,
             il.costing_loc,
             NULL dept
        from wf_order_detail wd,
             item_loc il
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and wd.customer_loc = il.loc
       order by wd.source_loc_id,
                wd.customer_loc,
                il.costing_loc;

   cursor C_GET_WF_DTL_ORDER_DEPT is
      select wd.wf_order_no,
             wd.item,
             wd.source_loc_id,
             wd.customer_loc,
             wd.requested_qty,
             wd.need_date,
             wd.not_after_date,
             il.costing_loc,
             im.dept
        from wf_order_detail wd,
             item_loc il,
             item_master im
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and wd.customer_loc = il.loc
         and il.item = im.item
       order by wd.source_loc_id,
                im.dept,
                wd.customer_loc,
                il.costing_loc;

   cursor C_GET_F_MOD_NEW_PO is
      select wd.wf_order_no,
             wd.item,
             wd.source_loc_id,
             wd.customer_loc,
             wd.requested_qty,
             wd.need_date,
             wd.not_after_date,
             il.costing_loc,
             NULL dept
        from wf_order_detail wd,
             item_loc il
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead
                          where wf_order_no = wd.wf_order_no
                            and supplier = wd.source_loc_id)
       union
      select wd.wf_order_no,
             wd.item,
             wd.source_loc_id,
             wd.customer_loc,
             wd.requested_qty,
             wd.need_date,
             wd.not_after_date,
             il.costing_loc,
             NULL dept
        from wf_order_detail wd,
             item_loc il
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead
                          where wf_order_no = wd.wf_order_no
                            and supplier = wd.source_loc_id
                            and location = wd.customer_loc)
       union
      select wd.wf_order_no,
             wd.item,
             wd.source_loc_id,
             wd.customer_loc,
             wd.requested_qty,
             wd.need_date,
             wd.not_after_date,
             il.costing_loc,
             NULL dept
        from wf_order_detail wd,
             item_loc il
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead oh,
                                ordloc ol,
                                item_loc il2
                          where oh.wf_order_no = wd.wf_order_no
                            and oh.supplier = wd.source_loc_id
                            and oh.location = wd.customer_loc
                            and oh.order_no = ol.order_no
                            and ol.item = il2.item
                            and il2.loc = oh.location
                            and il2.costing_loc = il.costing_loc)
       order by 3,4,8;

   cursor C_GET_F_MOD_NEW_PO_DEPT is
      /* for new suppliers */
      select wd.wf_order_no,
             wd.item,
             wd.source_loc_id,
             wd.customer_loc,
             wd.requested_qty,
             wd.need_date,
             wd.not_after_date,
             il.costing_loc,
             im.dept
        from wf_order_detail wd,
             item_loc il,
             item_master im
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.item = im.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead
                          where wf_order_no = wd.wf_order_no
                            and supplier = wd.source_loc_id)
       union
      /* for new dept */
      select wd.wf_order_no,
             wd.item,
             wd.source_loc_id,
             wd.customer_loc,
             wd.requested_qty,
             wd.need_date,
             wd.not_after_date,
             il.costing_loc,
             im.dept
        from wf_order_detail wd,
             item_loc il,
             item_master im
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.item = im.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead
                          where wf_order_no = wd.wf_order_no
                            and supplier = wd.source_loc_id
                            and dept = im.dept)
       union
      /* for new customer location */
      select wd.wf_order_no,
             wd.item,
             wd.source_loc_id,
             wd.customer_loc,
             wd.requested_qty,
             wd.need_date,
             wd.not_after_date,
             il.costing_loc,
             im.dept
        from wf_order_detail wd,
             item_loc il,
             item_master im
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.item = im.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead
                          where wf_order_no = wd.wf_order_no
                            and supplier = wd.source_loc_id
                            and dept = im.dept
                            and location = wd.customer_loc)
       union
      /* for new costing location */
      select wd.wf_order_no,
             wd.item,
             wd.source_loc_id,
             wd.customer_loc,
             wd.requested_qty,
             wd.need_date,
             wd.not_after_date,
             il.costing_loc,
             im.dept
        from wf_order_detail wd,
             item_loc il,
             item_master im
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.item = im.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead oh,
                                ordloc ol,
                                item_loc il2
                          where oh.wf_order_no = wd.wf_order_no
                            and oh.supplier = wd.source_loc_id
                            and dept = im.dept
                            and oh.location = wd.customer_loc
                            and oh.order_no = ol.order_no
                            and ol.item = il2.item
                            and il2.loc = oh.location
                            and il2.costing_loc = il.costing_loc)
       order by 3,9,4,8;

   TYPE WF_DTL_REC is TABLE OF C_GET_WF_DTL_ORDER%ROWTYPE INDEX BY BINARY_INTEGER;

   L_wf_dtl_rec       WF_DTL_REC;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_wf_order_no,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   LP_ordhead_tbl   := ORDHEAD_TBL();
   LP_ordsku_tbl    := ORDSKU_TBL();
   LP_ordloc_tbl    := ORDLOC_TBL();
   LP_order_tbl     := ORDER_TBL();

   select latest_ship_days,
          base_country_id,
          import_ind,
          elc_ind,
          dept_level_orders
     into LP_latest_ship_days,
          LP_base_country_id,
          LP_import_ind,
          LP_elc_ind,
          LP_dept_level_orders
     from system_options;

   if LP_new_from_mod = 'N' then
      if LP_dept_level_orders = 'N' then
         open C_GET_WF_DTL_ORDER;
         fetch C_GET_WF_DTL_ORDER BULK COLLECT INTO L_wf_dtl_rec;
         close C_GET_WF_DTL_ORDER;
      else
         open C_GET_WF_DTL_ORDER_DEPT;
         fetch C_GET_WF_DTL_ORDER_DEPT BULK COLLECT INTO L_wf_dtl_rec;
         close C_GET_WF_DTL_ORDER_DEPT;
      end if;
   elsif LP_new_from_mod = 'Y' then
      if LP_dept_level_orders = 'N' or LP_order_type in ('ARB', 'BRB') then
         open C_GET_F_MOD_NEW_PO;
         fetch C_GET_F_MOD_NEW_PO BULK COLLECT INTO L_wf_dtl_rec;
         close C_GET_F_MOD_NEW_PO;
      else
         open C_GET_F_MOD_NEW_PO_DEPT;
         fetch C_GET_F_MOD_NEW_PO_DEPT BULK COLLECT INTO L_wf_dtl_rec;
         close C_GET_F_MOD_NEW_PO_DEPT;
      end if;
   end if;

   if L_wf_dtl_rec is NOT NULL and L_wf_dtl_rec.COUNT > 0 then
      L_curr_cust_loc := NULL;
      L_prev_cust_loc := NULL;
      L_curr_cost_loc := NULL;
      L_prev_cost_loc := NULL;
      L_curr_supplier := NULL;
      L_prev_supplier := NULL;
      L_curr_dept     := NULL;
      L_prev_dept     := NULL;

      FOR i in L_wf_dtl_rec.FIRST..L_wf_dtl_rec.LAST LOOP
         L_curr_supplier := L_wf_dtl_rec(i).source_loc_id;
         L_curr_dept     := L_wf_dtl_rec(i).dept;
         L_curr_cust_loc := L_wf_dtl_rec(i).customer_loc;
         L_curr_cost_loc := L_wf_dtl_rec(i).costing_loc;
         L_cre_new_po    := 'N';
         -- Check if the department (if LP_dep_level_orders = 'Y') and customer loc and costing loc combination changed.
         -- If yes, then a new Franchise PO must be created.
         if (((NVL(L_curr_supplier,-999) != NVL(L_prev_supplier,-999)) or
             (LP_dept_level_orders = 'Y' and ((NVL(LP_order_type,'X') != 'BRB') or(NVL(LP_order_type,'X') != 'ARB'))and
             (NVL(L_curr_dept,-999) != NVL(L_prev_dept,-999))) or
             (NVL(L_curr_cust_loc,-999) != NVL(L_prev_cust_loc,-999)) or
             (NVL(L_curr_cost_loc,-999) != NVL(L_prev_cost_loc,-999)))) then
            L_cre_new_po := 'Y';
            -- initialize date varibales if new PO is being processed
            L_nb_date := NULL;
            L_na_date := NULL;
            L_prev_nb_date := NULL;
            L_prev_na_date := NULL;
         end if;

         if GET_ITEM_INFO (O_error_message,
                           L_import_order_ind,
                           L_import_country_id,
                           L_import_id,
                           L_import_type,
                           L_routing_loc_id,
                           L_clearing_zone_id,
                           L_supp_pack_size,
                           L_unit_cost,
                           L_unit_retail,
                           L_origin_country,
                           L_wf_dtl_rec(i).item,
                           L_curr_supplier,
                           L_curr_cust_loc) = FALSE then
            return FALSE;
         end if;

         if L_cre_new_po = 'Y' then
            L_order_no := NULL;
            if ORDER_NUMBER_SQL.NEXT_ORDER_NUMBER(O_error_message,
                                                  L_order_no) = FALSE then
               return FALSE;
            end if;

            if SUPP_ATTRIB_SQL.ORDER_SUP_DETAILS (O_error_message,
                                                  L_currency_code,
                                                  L_terms,
                                                  L_freight_terms,
                                                  L_sup_status,
                                                  L_qc_ind,
                                                  L_edi_po_ind,
                                                  L_pre_mark_ind,
                                                  L_ship_method,
                                                  L_payment_method,
                                                  L_curr_supplier) = FALSE then
               return FALSE;
            end if;

            if DATES_SQL.GET_EOW_DATE(O_error_message,
                                      L_otb_eow_date,
                                      L_wf_dtl_rec(i).not_after_date) = FALSE then
               return FALSE;
            end if;

            if CURRENCY_SQL.GET_RATE(O_error_message,
                                     L_exchange_rate,
                                     L_currency_code,
                                     'P',
                                     LP_vdate) = FALSE then
               return FALSE;
            end if;

            if LP_elc_ind = 'Y' then
               if SUPP_ATTRIB_SQL.ORDER_IMPORT_DETAILS(O_error_message,
                                                       L_agent,
                                                       L_lading_port,
                                                       L_discharge_port,
                                                       L_factory,
                                                       L_partner_type_1,
                                                       L_partner1,
                                                       L_partner_type_2,
                                                       L_partner2,
                                                       L_partner_type_3,
                                                       L_partner3,
                                                       L_curr_supplier) = FALSE then
                  return FALSE;
               end if;
            end if;

            --Building an ordhead record
            LP_ordhead_tbl.EXTEND();
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).order_no               := L_order_no;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).order_type             := 'N/B';
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).dept                   := L_wf_dtl_rec(i).dept;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).buyer                  := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).supplier               := L_curr_supplier;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).supp_add_seq_no        := 1;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).loc_type               := 'S';
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).location               := L_curr_cust_loc;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).promotion              := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).qc_ind                 := L_qc_ind;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).written_date           := SYSDATE;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).not_before_date        := L_wf_dtl_rec(i).need_date;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).not_after_date         := L_wf_dtl_rec(i).not_after_date;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).otb_eow_date           := L_otb_eow_date;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).earliest_ship_date     := L_wf_dtl_rec(i).need_date;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).latest_ship_date       := L_wf_dtl_rec(i).need_date + LP_latest_ship_days;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).close_date             := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).terms                  := L_terms;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).freight_terms          := L_freight_terms;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).orig_ind               := RMSSUB_XORDER_DEFAULT.DEFAULT_ORIG_IND;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).payment_method         := L_payment_method;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).backhaul_type          := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).backhaul_allowance     := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).ship_method            := L_ship_method;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).purchase_type          := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).status                 := 'W';
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).orig_approval_date     := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).orig_approval_id       := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).ship_pay_method        := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).fob_trans_res          := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).fob_trans_res_desc     := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).fob_title_pass         := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).fob_title_pass_desc    := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).edi_sent_ind           := 'N';
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).edi_po_ind             := L_edi_po_ind;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).import_order_ind       := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).import_country_id      := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).po_ack_recvd_ind       := 'N';
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).include_on_order_ind   := 'Y';
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).vendor_order_no        := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).exchange_rate          := L_exchange_rate;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).factory                := L_factory;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).agent                  := L_agent;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).discharge_port         := L_discharge_port;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).lading_port            := L_lading_port;            
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).freight_contract_no    := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).po_type                := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).pre_mark_ind           := L_pre_mark_ind;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).currency_code          := L_currency_code;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).reject_code            := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).contract_no            := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).last_sent_rev_no       := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).split_ref_ordno        := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).pickup_loc             := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).pickup_no              := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).pickup_date            := LP_vdate;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).app_datetime           := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).comment_desc           := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).partner_type_1         := L_partner_type_1;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).partner1               := L_partner1;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).partner_type_2         := L_partner_type_2;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).partner2               := L_partner2;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).partner_type_3         := L_partner_type_3;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).partner3               := L_partner3;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).item                   := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).clearing_zone_id       := L_clearing_zone_id;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).delivery_supplier      := NULL;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).triangulation_ind      := 'N';
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).wf_order_no            := I_wf_order_no;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).import_order_ind       := L_import_order_ind;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).import_country_id      := L_import_country_id;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).import_id              := L_import_id;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).import_type            := L_import_type;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).routing_loc_id         := L_routing_loc_id;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).create_id              := GET_USER;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).create_datetime        := SYSDATE;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).last_update_id         := GET_USER;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).last_update_datetime   := SYSDATE;

            LP_order_tbl.EXTEND();
            LP_order_tbl(LP_order_tbl.COUNT).order_no := L_order_no;
            LP_order_tbl(LP_order_tbl.COUNT).supplier   := L_curr_supplier;
         end if; -- create new PO

         --Building an ordsku record
         LP_ordsku_tbl.EXTEND();
         LP_ordsku_tbl(LP_ordsku_tbl.COUNT).order_no             := L_order_no;
         LP_ordsku_tbl(LP_ordsku_tbl.COUNT).item                 := L_wf_dtl_rec(i).item;
         LP_ordsku_tbl(LP_ordsku_tbl.COUNT).ref_item             := NULL;
         LP_ordsku_tbl(LP_ordsku_tbl.COUNT).origin_country_id    := L_origin_country;
         LP_ordsku_tbl(LP_ordsku_tbl.COUNT).earliest_ship_date   := L_wf_dtl_rec(i).need_date;
         LP_ordsku_tbl(LP_ordsku_tbl.COUNT).latest_ship_date     := L_wf_dtl_rec(i).need_date + LP_latest_ship_days;
         LP_ordsku_tbl(LP_ordsku_tbl.COUNT).supp_pack_size       := L_supp_pack_size;
         LP_ordsku_tbl(LP_ordsku_tbl.COUNT).non_scale_ind        := 'N';
         LP_ordsku_tbl(LP_ordsku_tbl.COUNT).pickup_loc           := NULL;
         LP_ordsku_tbl(LP_ordsku_tbl.COUNT).pickup_no            := NULL;

         --Building an ordloc record
         LP_ordloc_tbl.EXTEND();
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).order_no                         := L_order_no;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).item                             := L_wf_dtl_rec(i).item;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).location                         := L_curr_cust_loc;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).loc_type                         := 'S';
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).unit_retail                      := L_unit_retail;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).qty_ordered                      := L_wf_dtl_rec(i).requested_qty;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).qty_prescaled                    := L_wf_dtl_rec(i).requested_qty;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).unit_cost                        := L_unit_cost;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).unit_cost_init                   := L_unit_cost;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).cost_source                      := 'NORM';
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).non_scale_ind                    := 'N';
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).qty_received                     := NULL;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).last_received                    := NULL;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).last_rounded_qty                 := L_wf_dtl_rec(i).requested_qty;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).last_grp_rounded_qty             := L_wf_dtl_rec(i).requested_qty;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).qty_cancelled                    := NULL;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).cancel_code                      := NULL;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).cancel_date                      := NULL;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).cancel_id                        := NULL;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).original_repl_qty                := NULL;
         LP_ordloc_tbl(LP_ordloc_tbl.COUNT).tsf_po_link_no                   := NULL;

         if L_nb_date is NULL then
            L_nb_date := L_wf_dtl_rec(i).need_date;
         else
            if L_wf_dtl_rec(i).need_date < L_nb_date then
               L_nb_date := L_wf_dtl_rec(i).need_date;
            end if;
         end if;

         if L_na_date is NULL then
            L_na_date := L_wf_dtl_rec(i).not_after_date;
         else
            if L_wf_dtl_rec(i).not_after_date > L_na_date then
               L_na_date := L_wf_dtl_rec(i).not_after_date;
            end if;
         end if;

         -- update not_before_date and not_after_date based on the least need_date and most
         -- not_after_date per PO
         if ((L_cre_new_po = 'N') and (L_nb_date < L_prev_nb_date or L_na_date > L_prev_na_date)) then
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).not_before_date := L_nb_date;
            LP_ordhead_tbl(LP_ordhead_tbl.COUNT).not_after_date  := L_na_date;
         end if;

         L_prev_cust_loc := L_curr_cust_loc;
         L_prev_cost_loc := L_curr_cost_loc;
         L_prev_dept     := L_curr_dept;
         L_prev_supplier := L_curr_supplier;
         L_prev_nb_date  := L_wf_dtl_rec(i).need_date;
         L_prev_na_date  := L_wf_dtl_rec(i).not_after_date;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POPULATE_CREATE_REC;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_CREATE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_scaling         IN       VARCHAR2,
                           I_undo_scaling    IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)            := 'WF_PO_SQL.PERSIST_CREATE_PO';
   L_wf_order_no        WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_order_status       ORDHEAD.STATUS%TYPE;
   L_supplier           ORDHEAD.SUPPLIER%TYPE;
   L_import_ord_ind     ORDHEAD.IMPORT_ORDER_IND%TYPE;
   L_location           ORDHEAD.LOCATION%TYPE;
   L_exists             BOOLEAN;
   L_scaled_ind         BOOLEAN;
   L_not_before_date    ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_not_after_date     ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_pickup_date        ORDHEAD.PICKUP_DATE%TYPE;   

BEGIN

   if LP_ordhead_tbl is NOT NULL and LP_ordhead_tbl.COUNT > 0 then
      FORALL i in LP_ordhead_tbl.FIRST..LP_ordhead_tbl.LAST
         insert into ordhead values LP_ordhead_tbl(i);
   end if;

   if LP_ordsku_tbl is NOT NULL and LP_ordsku_tbl.COUNT > 0 then
      FORALL i in LP_ordsku_tbl.FIRST..LP_ordsku_tbl.LAST
         insert into ordsku values LP_ordsku_tbl(i);
   end if;

   if LP_ordloc_tbl is NOT NULL and LP_ordloc_tbl.COUNT > 0 then
      FORALL i in LP_ordloc_tbl.FIRST..LP_ordloc_tbl.LAST
         insert into ordloc values LP_ordloc_tbl(i);
   end if;

   if LP_ordhead_tbl is NOT NULL and LP_ordhead_tbl.COUNT > 0 then
      FOR rec in LP_ordhead_tbl.FIRST..LP_ordhead_tbl.LAST LOOP
         if ORDER_SETUP_SQL.DEFAULT_ORDER_INV_MGMT_INFO (O_error_message,
                                                         LP_ordhead_tbl(rec).order_no,
                                                         LP_ordhead_tbl(rec).supplier,
                                                         LP_ordhead_tbl(rec).dept,
                                                         LP_ordhead_tbl(rec).location,
                                                         LP_ordhead_tbl(rec).currency_code,
                                                         LP_ordhead_tbl(rec).exchange_rate,
                                                         'O',
                                                         'N') = FALSE then
            return FALSE;
         end if;
         
         if ORDER_CALC_SQL.CALC_HEADER_DATES(O_error_message,
                                       L_pickup_date,
                                       L_not_before_date,
                                       L_not_after_date,
                                       LP_order_tbl(rec).order_no,
                                       LP_order_tbl(rec).supplier) = FALSE then
            return FALSE;
         end if;         

        -- update the header record
        update ordhead
            set pickup_date          = least(L_pickup_date,L_not_after_date)
            where order_no = LP_ordhead_tbl(rec).order_no;
      END LOOP;
   end if;

   if LP_ordsku_tbl is NOT NULL and LP_ordsku_tbl.COUNT > 0 then
      -- Default elc and hts to the items
      FOR i in LP_ordsku_tbl.FIRST..LP_ordsku_tbl.LAST LOOP
         select location
           into L_location
           from ordhead
          where order_no = LP_ordsku_tbl(i).order_no;

         if LP_elc_ind = 'Y' then
            if ORDER_EXPENSE_SQL.INSERT_COST_COMP(O_error_message,
                                                  LP_ordsku_tbl(i).order_no,
                                                  LP_ordsku_tbl(i).item,
                                                  NULL,
                                                  NULL,
                                                  NULL) = FALSE then
               return FALSE;
            end if;

            select wf_order_no,
                   supplier,
                   import_order_ind
              into L_wf_order_no,
                   L_supplier,
                   L_import_ord_ind
              from ordhead
             where order_no = LP_ordsku_tbl(i).order_no;

            if LP_import_ind = 'Y' and L_import_ord_ind = 'Y' then
               if ORDER_HTS_SQL.DEFAULT_CALC_HTS (O_error_message,
                                                  LP_ordsku_tbl(i).order_no,
                                                  LP_ordsku_tbl(i).item,
                                                  NULL) = FALSE then
                  return FALSE;
               end if;
            end if;

            if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                      'PE',
                                      LP_ordsku_tbl(i).item,
                                      NULL,
                                      NULL,
                                      NULL,
                                      LP_ordsku_tbl(i).order_no,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL) = FALSE then
               return FALSE;
            end if;
         end if;
      END LOOP;
   end if;

   if LP_order_tbl is NOT NULL and LP_order_tbl.COUNT> 0 then
      FOR j in LP_order_tbl.FIRST.. LP_order_tbl.LAST LOOP
         if ORDER_SETUP_SQL.POP_CONTAINER_ITEM(O_error_message,
                                               LP_order_tbl(j).order_no,
                                               LP_order_tbl(j).supplier) = FALSE then
            return FALSE;
         end if;

         if I_scaling = 'Y' then
            if ORDER_RECALC_SQL.RECALC_ORDER (O_error_message,
                                              L_scaled_ind,
                                              LP_order_tbl(j).order_no,
                                              'C') = FALSE then
               return FALSE;
            end if;

            if SYNC_F_ORDER(O_error_message,
                            L_wf_order_no,
                            L_order_status,
                            RMS_CONSTANTS.WF_ACTION_SCALING,      --L_action_type,
                            LP_order_tbl(j).order_no,
                            NULL) = FALSE then   --L_status
               return FALSE;
            end if;
         elsif I_undo_scaling = 'Y'  then
            if ORDER_ATTRIB_SQL.CHECK_TEMP_TABLES(O_error_message,
                                                  L_exists,
                                                  LP_order_tbl(j).order_no) = FALSE then
               return FALSE;
            end if;

            if L_exists then
               if ORDER_RECALC_SQL.UNDO_SCALING(O_error_message,
                                                LP_order_tbl(j).order_no) = FALSE then
                  return FALSE;
               end if;

               if SYNC_F_ORDER(O_error_message,
                               L_wf_order_no,
                               L_order_status,
                               RMS_CONSTANTS.WF_ACTION_SCALING,      --L_action_type,
                               LP_order_tbl(j).order_no,
                               NULL) = FALSE then   --L_status
                  return FALSE;
               end if;
            end if;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PERSIST_CREATE_PO;
----------------------------------------------------------------------------------------
FUNCTION DELETE_FRANCHISE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_order_no     IN       WF_ORDER_HEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50)  := 'WF_PO_SQL.DELETE_FRANCHISE_PO';
   L_po_exists   VARCHAR2(1)   := NULL;
   L_shipped     BOOLEAN;

   cursor C_CHK_WF_PO is
      select 'x'
        from ordhead
       where wf_order_no = I_wf_order_no
         and rownum=1;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_CHK_WF_PO;
   fetch C_CHK_WF_PO into L_po_exists;
   close C_CHK_WF_PO;

   if L_po_exists is NOT NULL then
      FOR rec in (select order_no
                    from ordhead
                   where wf_order_no = I_wf_order_no) LOOP
         L_shipped := FALSE;
         if VALIDATE_F_PO_DELETE(O_error_message,
                                 L_shipped,
                                 rec.order_no) = FALSE then
            return FALSE;
         end if;

         if DELETE_ORDER(O_error_message,
                         L_shipped,
                         rec.order_no) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_FRANCHISE_PO;
----------------------------------------------------------------------------------------
FUNCTION GET_ITEM_INFO (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_import_order_ind    IN OUT   ORDHEAD.IMPORT_ORDER_IND%TYPE,
                        O_import_country_id   IN OUT   ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                        O_import_id           IN OUT   ORDHEAD.IMPORT_ID%TYPE,
                        O_import_type         IN OUT   ORDHEAD.IMPORT_TYPE%TYPE,
                        O_routing_loc_id      IN OUT   ORDHEAD.ROUTING_LOC_ID%TYPE,
                        O_clearing_zone_id    IN OUT   ORDHEAD.CLEARING_ZONE_ID%TYPE,
                        O_supp_pack_size      IN OUT   ORDSKU.SUPP_PACK_SIZE%TYPE,
                        O_unit_cost           IN OUT   ORDLOC.UNIT_COST%TYPE,
                        O_unit_retail         IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                        O_origin_country      IN OUT   ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                        I_item                IN       ITEM_MASTER.ITEM%TYPE,
                        I_supplier            IN       ITEM_SUPPLIER.ITEM%TYPE,
                        I_location            IN       ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(50)            := 'WF_PO_SQL.GET_ITEM_INFO';
   L_pack_ind                  ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind              ITEM_MASTER.SELLABLE_IND%TYPE;
   L_selling_unit_retail_loc   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_loc           ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_units_loc           ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail_loc     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom_loc     ITEM_LOC.MULTI_SELLING_UOM%TYPE;
   L_costing_loc               ITEM_LOC.COSTING_LOC%TYPE;
   L_costing_loc_type          ITEM_LOC.COSTING_LOC_TYPE%TYPE;
   L_primary_ind               ITEM_SUPP_COUNTRY.PRIMARY_COUNTRY_IND%TYPE;
   L_country_loc               ADDR.KEY_VALUE_1%TYPE;
   L_outloc_id                 OUTLOC.OUTLOC_ID%TYPE;
   L_outloc_desc               OUTLOC.OUTLOC_DESC%TYPE;
   L_exists                    BOOLEAN;

   cursor C_GET_COUNTRY_LOC is
      select country_id
        from addr
       where key_value_1 = to_char(I_location)
         and addr_type = '07'
         and primary_addr_ind = 'Y';

   cursor C_GET_ROUTING_LOC is
      select routing_loc_id
        from sups_routing_loc
       where supplier = I_supplier;

   cursor C_GET_UNIT_COST_LOC (I_costing_loc   IN   ITEM_LOC.COSTING_LOC%TYPE)is
      select unit_cost
        from item_supp_country_loc
       where item = I_item
         and supplier = I_supplier
         and loc = I_costing_loc
         and origin_country_id = (select origin_country_id
                                    from item_supp_country
                                   where item = I_item
                                     and supplier = I_supplier
                                     and primary_country_ind = 'Y');

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_item,
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_supplier,
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_location,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   O_import_order_ind   := NULL;
   O_import_country_id  := NULL;
   O_import_id          := NULL;
   O_import_type        := NULL;
   O_routing_loc_id     := NULL;
   O_clearing_zone_id   := NULL;
   O_supp_pack_size     := NULL;
   O_unit_cost          := NULL;
   O_unit_retail        := NULL;
   O_origin_country     := NULL;

   select origin_country_id
     into O_origin_country
     from item_supp_country
    where item =I_item
      and supplier = I_supplier
      and primary_country_ind = 'Y';

   L_country_loc := NULL;
   open C_GET_COUNTRY_LOC;
   fetch C_GET_COUNTRY_LOC into L_country_loc;
   close C_GET_COUNTRY_LOC;

   if L_country_loc is NULL then
      L_country_loc := LP_base_country_id;
   end if;

   select costing_loc,
          costing_loc_type
     into L_costing_loc,
          L_costing_loc_type
     from item_loc
    where item = I_item
      and loc = I_location;

   if O_origin_country != L_country_loc then

      open C_GET_ROUTING_LOC;
      fetch C_GET_ROUTING_LOC into O_routing_loc_id;
      close C_GET_ROUTING_LOC;

      if OUTSIDE_LOCATION_SQL.GET_PRIM_CLEAR_ZONE_IMP_CTRY(O_error_message,
                                                           L_exists,
                                                           L_outloc_id,
                                                           L_outloc_desc,
                                                           L_country_loc) = FALSE then
          return FALSE;
      end if;

      if L_exists then
         O_clearing_zone_id   := L_outloc_id;
      else
         O_clearing_zone_id   := NULL;
      end if;
      O_import_order_ind   := 'Y';
      O_import_country_id  := L_country_loc;
   else
      O_import_order_ind   := 'N';
      O_import_country_id  := LP_base_country_id;
   end if; -- end if O_origin_country != L_country_loc

   O_import_id          := L_costing_loc;
   O_import_type        := 'F';

   open C_GET_UNIT_COST_LOC(L_costing_loc);
   fetch C_GET_UNIT_COST_LOC into O_unit_cost;
   close C_GET_UNIT_COST_LOC;

   if O_unit_cost is NULL then

      select unit_cost
        into O_unit_cost
        from item_supp_country
       where item = I_item
         and supplier = I_supplier
         and primary_country_ind = 'Y';

   end if;

   L_pack_ind := 'N';
   L_sellable_ind := 'N';

      select pack_ind,
             sellable_ind
        into L_pack_ind,
             L_sellable_ind
        from item_master
       where item = I_item;

   if L_pack_ind = 'Y'and L_sellable_ind = 'N' then
      if PRICING_ATTRIB_SQL.BUILD_PACK_RETAIL(O_error_message,
                                              O_unit_retail,
                                              I_item,
                                              L_costing_loc_type,
                                              L_costing_loc) = FALSE then
         return FALSE;
      end if;
   else
      if PRICING_ATTRIB_SQL.GET_RETAIL(O_error_message,
                                       O_unit_retail,
                                       L_selling_unit_retail_loc,
                                       L_selling_uom_loc,
                                       L_multi_units_loc,
                                       L_multi_unit_retail_loc,
                                       L_multi_selling_uom_loc,
                                       I_item,
                                       L_costing_loc_type,
                                       L_costing_loc) = FALSE then
         return FALSE;
      end if;
   end if;

   O_unit_retail := NVL(O_unit_retail,0);

   if ITEM_SUPP_COUNTRY_SQL.GET_COUNTRY_PARAMETERS(O_error_message,
                                                   L_primary_ind,
                                                   O_supp_pack_size,
                                                   I_item,
                                                   I_supplier,
                                                   O_origin_country) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEM_INFO;
----------------------------------------------------------------------------------------
FUNCTION CANCEL_FRANCHISE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_order_no     IN       ORDHEAD.WF_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(50)                       := 'WF_PO_SQL.CANCEL_FRANCHISE_PO';
   L_chk_po_exists        VARCHAR2(1)                        := NULL;
   L_chk_ship_exists      VARCHAR2(1)                        := NULL;
   L_order_no             ORDHEAD.ORDER_NO%TYPE              := NULL;
   L_orig_approval_date   ORDHEAD.ORIG_APPROVAL_DATE%TYPE    := NULL;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_CHK_PO_EXISTS is
      select 'x'
        from ordhead
       where wf_order_no = I_wf_order_no
         and rownum = 1;

   cursor C_CHK_SHIPMENT is
      select 'x'
        from ordhead o
       where o.wf_order_no = I_wf_order_no
         and exists (select 'x'
                       from shipment s
                      where s.order_no = o.order_no)
         and rownum = 1;

   cursor C_GET_PO is
      select order_no,
             status,
             orig_approval_date
        from ordhead
       where wf_order_no = I_wf_order_no;

   cursor C_OTB is
      select item,
             location,
             loc_type,
             NVL(qty_received,0) qty_received,
             NVL(qty_cancelled,0) qty_cancelled,
             qty_ordered,
             qty_prescaled
        from ordloc
       where order_no = L_order_no;

   cursor C_LOCK_ORDLOC is
      select 'x'
        from ordloc
       where order_no in (select order_no
                            from ordhead
                           where wf_order_no = I_wf_order_no)
         for update nowait;

   cursor C_LOCK_ORDHEAD is
      select 'x'
        from ordhead
       where order_no in (select order_no
                            from ordhead
                           where wf_order_no = I_wf_order_no)
         for update nowait;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_CHK_PO_EXISTS;
   fetch C_CHK_PO_EXISTS into L_chk_po_exists;
   close C_CHK_PO_EXISTS;

   if L_chk_po_exists is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_FPO',
                                            I_wf_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_CHK_SHIPMENT;
   fetch C_CHK_SHIPMENT into L_chk_ship_exists;
   close C_CHK_SHIPMENT;

   if L_chk_ship_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ORD_SHIP_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   FOR rec in C_GET_PO LOOP
      L_order_no := rec.order_no;
      L_orig_approval_date := rec.orig_approval_date;

      if rec.status = 'A' then
         FOR C_otb_rec in C_OTB LOOP
            if OTB_SQL.BUILD_ORD_CANCEL_REINSTATE(O_error_message,
                                                  L_order_no,
                                                  C_otb_rec.item,
                                                  C_otb_rec.location,
                                                  C_otb_rec.loc_type,
                                                  C_otb_rec.qty_ordered,
                                                  'C') = FALSE then
                return FALSE;
            end if;
         END LOOP;

         if OTB_SQL.FLUSH_ORD_BULK(O_error_message,
                                   'CANCEL_REINSTATE') = FALSE then
            return  FALSE;
         end if;
      end if;
   END LOOP;

   open C_LOCK_ORDLOC;
   close C_LOCK_ORDLOC;

   update ordloc
      set qty_ordered = 0,
          qty_cancelled = NVL(qty_cancelled,0) + qty_ordered,
          qty_prescaled = DECODE(L_orig_approval_date,NULL,0,qty_prescaled),
          cancel_code = 'B',
          cancel_date = LP_vdate,
          cancel_id = GET_USER
    where order_no in (select order_no
                         from ordhead
                        where wf_order_no = I_wf_order_no);

   open C_LOCK_ORDHEAD;
   close C_LOCK_ORDHEAD;

   update ordhead
      set status = 'C',
          close_date = LP_vdate,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where order_no in (select order_no
                         from ordhead
                        where wf_order_no = I_wf_order_no);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('REC_LOCKED',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CANCEL_FRANCHISE_PO;
----------------------------------------------------------------------------------------
FUNCTION MODIFY_FRANCHISE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_order_no     IN       ORDHEAD.WF_ORDER_NO%TYPE,
                             I_scaling         IN       VARCHAR2,
                             I_undo_scaling    IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(50)                      := 'WF_PO_SQL.MODIFY_FRANCHISE_PO';
   L_chk_po_exists             VARCHAR2(1)                       := NULL;
   L_chk_item                  VARCHAR2(1);
   L_chk_ship_rcv              VARCHAR2(1);
   L_item_del_cnt              NUMBER;
   L_chk_shipment_exists       BOOLEAN;
   L_shipped                   BOOLEAN;
   L_chk_wf_order_status       WF_ORDER_HEAD.STATUS%TYPE         := NULL;
   L_qty_expected              SHIPSKU.QTY_EXPECTED%TYPE;
   L_chk_sum_qty               ORDLOC.QTY_ORDERED%TYPE;
   L_qty_ordered               ORDLOC.QTY_ORDERED%TYPE;
   L_qty_prescaled             ORDLOC.QTY_PRESCALED%TYPE;
   L_order_no                  ORDHEAD.ORDER_NO%TYPE;
   L_prev_order_no             ORDHEAD.ORDER_NO%TYPE             := NULL;
   L_import_order_ind          ORDHEAD.IMPORT_ORDER_IND%TYPE;
   L_import_country_id         ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   L_nb_date                   ORDHEAD.NOT_BEFORE_DATE%TYPE      := NULL;
   L_prev_nb_date              ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_na_date                   ORDHEAD.NOT_AFTER_DATE%TYPE       := NULL;
   L_prev_na_date              ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_routing_loc_id            ORDHEAD.ROUTING_LOC_ID%TYPE;
   L_clearing_zone_id          ORDHEAD.CLEARING_ZONE_ID%TYPE;
   L_orig_approval_date        ORDHEAD.ORIG_APPROVAL_DATE%TYPE   := NULL;
   L_order_type                ORDHEAD.ORDER_TYPE%TYPE;
   L_supp_pack_size            ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_unit_cost                 ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE;
   L_origin_country            ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_unit_retail               ITEM_LOC.UNIT_RETAIL%TYPE;
   L_import_id                 SUPS_IMP_EXP.IMPORT_ID%TYPE;
   L_import_type               SUPS_IMP_EXP.IMPORT_TYPE%TYPE;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_CHK_PO_EXISTS is
      select 'x'
        from ordhead
       where wf_order_no = I_wf_order_no
         and rownum = 1;

   cursor C_CHK_WF_ORDER is
      select status
        from wf_order_head wh
       where wh.wf_order_no = I_wf_order_no;

   cursor C_GET_NEW_PO is
      select 'x'
        from wf_order_detail wd,
             item_loc il
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead
                          where wf_order_no = wd.wf_order_no
                            and supplier = wd.source_loc_id)
       union
      select 'x'
        from wf_order_detail wd,
             item_loc il
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead
                          where wf_order_no = wd.wf_order_no
                            and supplier = wd.source_loc_id
                            and location = wd.customer_loc)
       union
      select 'x'
        from wf_order_detail wd,
             item_loc il
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead oh,
                                ordloc ol,
                                item_loc il2
                          where oh.wf_order_no = wd.wf_order_no
                            and oh.supplier = wd.source_loc_id
                            and oh.location = wd.customer_loc
                            and oh.order_no = ol.order_no
                            and ol.item = il2.item
                            and il2.loc = oh.location
                            and il2.costing_loc = il.costing_loc);

   cursor C_GET_NEW_PO_DEPT is
      /* for new suppliers */
      select 'x'
        from wf_order_detail wd,
             item_loc il,
             item_master im
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.item = im.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead
                          where wf_order_no = wd.wf_order_no
                            and supplier = wd.source_loc_id)
       union
      /* for new dept */
      select 'x'
        from wf_order_detail wd,
             item_loc il,
             item_master im
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.item = im.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead
                          where wf_order_no = wd.wf_order_no
                            and supplier = wd.source_loc_id
                            and dept = im.dept)
       union
      /* for new customer location */
      select 'x'
        from wf_order_detail wd,
             item_loc il,
             item_master im
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.item = im.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead
                          where wf_order_no = wd.wf_order_no
                            and supplier = wd.source_loc_id
                            and dept = im.dept
                            and location = wd.customer_loc)
       union
      /* for new costing location */
      select 'x'
        from wf_order_detail wd,
             item_loc il,
             item_master im
       where wd.wf_order_no = I_wf_order_no
         and wd.source_loc_type = 'SU'
         and wd.item = il.item
         and il.item = im.item
         and il.loc = wd.customer_loc
         and NOT EXISTS (select 'x'
                           from ordhead oh,
                                ordloc ol,
                                item_loc il2
                          where oh.wf_order_no = wd.wf_order_no
                            and oh.supplier = wd.source_loc_id
                            and dept = im.dept
                            and oh.location = wd.customer_loc
                            and oh.order_no = ol.order_no
                            and ol.item = il2.item
                            and il2.loc = oh.location
                            and il2.costing_loc = il.costing_loc);

   cursor C_GET_MOD_F_PO is
      select distinct(w.item),
             w.source_loc_id,
             w.customer_loc,
             w.requested_qty,
             w.need_date,
             w.not_after_date,
             w.status
        from (select wd.item,
                     wd.source_loc_id,
                     wd.customer_loc,
                     wd.requested_qty,
                     wd.need_date,
                     wd.not_after_date,
                     wh.status,
                     il.costing_loc
                from wf_order_head wh,
                     wf_order_detail wd,
                     item_loc il
               where wh.wf_order_no = I_wf_order_no
                 and wh.wf_order_no = wd.wf_order_no
                 and wd.source_loc_type = 'SU'
                 and wd.item = il.item
                 and il.loc = wd.customer_loc) w,
             (select ol.item,
                     oh.supplier,
                     oh.location,
                     il.costing_loc
                from ordhead oh,
                     ordloc ol,
                     item_loc il,
                     item_master im
               where oh.wf_order_no = I_wf_order_no
                 and oh.order_no = ol.order_no
                 and ol.item = il.item
                 and il.loc = oh.location
                 and il.item = im.item
                 and (im.deposit_item_type is NULL or
                   im.deposit_item_type != 'A')) o
       where w.source_loc_id = o.supplier
         and w.customer_loc = o.location
         and w.costing_loc = o.costing_loc;

   cursor C_GET_MOD_F_PO_DEPT is
      select distinct(w.item),
             w.source_loc_id,
             w.customer_loc,
             w.requested_qty,
             w.need_date,
             w.not_after_date,
             w.status,
             w.dept
        from (select wd.item,
                     wd.source_loc_id,
                     wd.customer_loc,
                     wd.requested_qty,
                     wd.need_date,
                     wd.not_after_date,
                     wh.status,
                     il.costing_loc,
                     im.dept
                from wf_order_head wh,
                     wf_order_detail wd,
                     item_loc il,
                     item_master im
               where wh.wf_order_no = I_wf_order_no
                 and wh.wf_order_no = wd.wf_order_no
                 and wd.source_loc_type = 'SU'
                 and wd.item = il.item
                 and il.item = im.item
                 and il.loc = wd.customer_loc) w,
             (select ol.item,
                     oh.supplier,
                     oh.location,
                     il.costing_loc,
                     im.dept
                from ordhead oh,
                     ordloc ol,
                     item_loc il,
                     item_master im
               where oh.wf_order_no = I_wf_order_no
                 and oh.order_no = ol.order_no
                 and ol.item = il.item
                 and il.loc = oh.location
                 and il.item = im.item
                 and im.dept = oh.dept
                 and (im.deposit_item_type is NULL or
                     im.deposit_item_type != 'A')) o
       where w.source_loc_id = o.supplier
         and w.dept = o.dept
         and w.customer_loc = o.location
         and w.costing_loc = o.costing_loc;

   cursor C_GET_F_PO_DEL is
   /* get all PO to be deleted */
      select oh.order_no
        from ordhead oh
       where oh.wf_order_no = I_wf_order_no
         and NOT EXISTS (select 'x'
                           from wf_order_detail
                          where wf_order_no = oh.wf_order_no
                            and source_loc_type = 'SU'
                            and source_loc_id = oh.supplier)
       union
      select oh.order_no
        from ordhead oh
       where oh.wf_order_no = I_wf_order_no
         and NOT EXISTS (select 'x'
                          from wf_order_detail
                         where wf_order_no = oh.wf_order_no
                           and source_loc_type = 'SU'
                           and source_loc_id = oh.supplier
                           and customer_loc = oh.location);

   cursor C_GET_F_PO_ITEM_DEL is
   /* get all items to be deleted */
      select oh.order_no,
             oh.location,
             ol.item
        from ordhead oh,
             ordloc ol,
             item_master im
       where oh.wf_order_no = I_wf_order_no
         and oh.order_no = ol.order_no
         and NOT EXISTS (select 'x'
                           from wf_order_detail wd
                          where wd.wf_order_no = oh.wf_order_no
                            and wd.source_loc_type = 'SU'
                            and wd.source_loc_id = oh.supplier
                            and wd.customer_loc = oh.location
                            and wd.item = ol.item)
         AND ol.item = im.item
         and (im.deposit_item_type is NULL or
              im.deposit_item_type != 'A');

   cursor C_CHK_ITEM (I_item  IN ORDLOC.ITEM%TYPE) is
      select 'x'
        from ordloc
       where order_no = L_order_no
         and item = I_item
         and rownum = 1;

   cursor C_GET_ORDER_NO (I_item       IN   ORDLOC.ITEM%TYPE,
                          I_loc        IN   ORDLOC.LOCATION%TYPE,
                          I_supplier   IN   ORDHEAD.SUPPLIER%TYPE) is
      select distinct(oh.order_no),
             oh.orig_approval_date
       from ordhead oh,
            ordloc ol,
            item_loc il,
            item_master im
      where oh.wf_order_no = I_wf_order_no
        and oh.supplier = I_supplier
        and oh.location =I_loc
        and oh.order_no = ol.order_no
        and ol.item = il.item
        and il.loc = oh.location
        and il.item = im.item
        and (im.deposit_item_type is NULL or
             im.deposit_item_type != 'A')
        and il.costing_loc = (select costing_loc
                                from item_loc
                               where loc = I_loc
                                 and item = I_item);

   cursor C_GET_ORDER_NO_DEPT (I_item       IN   ORDLOC.ITEM%TYPE,
                               I_loc        IN   ORDLOC.LOCATION%TYPE,
                               I_supplier   IN   ORDHEAD.SUPPLIER%TYPE,
                               I_dept       IN   ORDHEAD.DEPT%TYPE) is
      select distinct(oh.order_no),
             orig_approval_date
        from ordhead oh,
             ordloc ol,
             item_loc il,
             item_master im
       where oh.wf_order_no = I_wf_order_no
         and oh.supplier = I_supplier
         and oh.dept = I_dept
         and oh.location =I_loc
         and oh.order_no = ol.order_no
         and ol.item = il.item
         and il.loc = oh.location
         and il.item = im.item
         and (im.deposit_item_type is NULL or
              im.deposit_item_type != 'A')
         and il.costing_loc = (select costing_loc
                                 from item_loc
                                where loc = I_loc
                                  and item = I_item);

   cursor C_LOCK_ORDLOC is
      select 'x'
        from ordloc
       where order_no = L_order_no
         for update nowait;

   cursor C_LOCK_ORDHEAD is
      select 'x'
        from ordhead
       where order_no = L_order_no
         for update nowait;

   cursor C_LOCK_WF_ORDHEAD is
      select 'x'
        from ordhead
       where wf_order_no = I_wf_order_no
         and status != 'A'
         for update nowait;

   cursor C_CHK_SHIP_RCV (I_item       IN   ORDLOC.ITEM%TYPE,
                          I_location   IN    ORDLOC.LOCATION%TYPE) is
      select 'x'
        from ordloc
       where order_no = L_order_no
         and qty_ordered <= NVL(qty_received,0)
         and item = I_item
         and location = I_location;

   TYPE NEW_PO_REC is TABLE OF C_GET_NEW_PO%ROWTYPE INDEX BY BINARY_INTEGER;

   L_new_po_rec_exists   NEW_PO_REC;

BEGIN

   LP_new_from_mod := 'N';

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_wf_order_no,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   select latest_ship_days,
          base_country_id,
          import_ind,
          elc_ind,
          dept_level_orders
     into LP_latest_ship_days,
          LP_base_country_id,
          LP_import_ind,
          LP_elc_ind,
          LP_dept_level_orders
     from system_options;

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NULL_WF_ORDER_NO',
                                            I_wf_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_CHK_WF_ORDER;
   fetch C_CHK_WF_ORDER into L_chk_wf_order_status;
   close C_CHK_WF_ORDER;

   if L_chk_wf_order_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_WF_ORDER_NO',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   elsif L_chk_wf_order_status IN ('C','D') then
      O_error_message := SQL_LIB.CREATE_MSG('WF_ORDER_CANT_MOD',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_CHK_PO_EXISTS;
   fetch C_CHK_PO_EXISTS into L_chk_po_exists;
   close C_CHK_PO_EXISTS;

   if L_chk_po_exists is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_FPO',
                                            I_wf_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   select order_type
     into LP_order_type
     from ordhead
    where wf_order_no = I_wf_order_no
      and rownum = 1;

   /* get all new POs to be created */
   if LP_dept_level_orders = 'N' or LP_order_type in ('ARB','BRB') then
      open C_GET_NEW_PO;
      fetch C_GET_NEW_PO BULK COLLECT INTO L_new_po_rec_exists;
      close C_GET_NEW_PO;
   else
      open C_GET_NEW_PO_DEPT;
      fetch C_GET_NEW_PO_DEPT BULK COLLECT INTO L_new_po_rec_exists;
      close C_GET_NEW_PO_DEPT;
   end if;

   if L_new_po_rec_exists is NOT NULL and L_new_po_rec_exists.COUNT > 0 then
      LP_new_from_mod := 'Y';

      if CREATE_FRANCHISE_PO(O_error_message,
                             I_wf_order_no,
                             I_scaling,
                             I_undo_scaling) = FALSE then
         return FALSE;
      end if;
   end if;

   /* get all existing POs to be modified*/
   LP_ordhead_tbl   := ORDHEAD_TBL();
   LP_ordsku_tbl    := ORDSKU_TBL();
   LP_ordloc_tbl    := ORDLOC_TBL();
   LP_order_tbl     := ORDER_TBL();

   if LP_dept_level_orders = 'N' or LP_order_type in ('ARB','BRB') then
      FOR rec in C_GET_MOD_F_PO LOOP
         FOR j in C_GET_ORDER_NO (rec.item,
                                  rec.customer_loc,
                                  rec.source_loc_id)LOOP
            L_order_no := j.order_no;
            L_orig_approval_date := j.orig_approval_date;
            L_chk_item := NULL;
            open C_CHK_ITEM(rec.item);
            fetch C_CHK_ITEM into L_chk_item;
            close C_CHK_ITEM;

            if L_chk_item is NULL then
               -- new item for the PO.
               if GET_ITEM_INFO (O_error_message,
                                 L_import_order_ind,
                                 L_import_country_id,
                                 L_import_id,
                                 L_import_type,
                                 L_routing_loc_id,
                                 L_clearing_zone_id,
                                 L_supp_pack_size,
                                 L_unit_cost,
                                 L_unit_retail,
                                 L_origin_country,
                                 rec.item,
                                 rec.source_loc_id,
                                 rec.customer_loc) = FALSE then
                  return FALSE;
               end if;

               --Building an ordsku record
               LP_ordsku_tbl.EXTEND();
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).order_no             := L_order_no;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).item                 := rec.item;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).ref_item             := NULL;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).origin_country_id    := L_origin_country;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).earliest_ship_date   := rec.need_date;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).latest_ship_date     := rec.need_date + LP_latest_ship_days;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).supp_pack_size       := L_supp_pack_size;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).non_scale_ind        := 'N';
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).pickup_loc           := NULL;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).pickup_no            := NULL;

               --Building an ordloc record
               LP_ordloc_tbl.EXTEND();
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).order_no               := L_order_no;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).item                   := rec.item;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).location               := rec.customer_loc;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).loc_type               := 'S';
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).unit_retail            := L_unit_retail;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).qty_ordered            := rec.requested_qty;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).qty_prescaled          := rec.requested_qty;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).unit_cost              := L_unit_cost;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).unit_cost_init         := L_unit_cost;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).cost_source            := 'NORM';
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).non_scale_ind          := 'N';
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).qty_received           := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).last_received          := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).last_rounded_qty       := rec.requested_qty;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).last_grp_rounded_qty   := rec.requested_qty;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).qty_cancelled          := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).cancel_code            := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).cancel_date            := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).cancel_id              := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).original_repl_qty      := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).tsf_po_link_no         := NULL;
            else
               L_qty_ordered   := 0;
               L_qty_prescaled := 0;

               select qty_ordered,
                      qty_prescaled
                 into L_qty_ordered,
                      L_qty_prescaled
                 from ordloc
                where order_no = L_order_no
                  and item = rec.item
                  and location = rec.customer_loc;

               if L_orig_approval_date is NULL then
                  L_qty_prescaled := rec.requested_qty;
               end if;

               L_chk_shipment_exists := FALSE;
               if CHECK_ORDER_SHIPPED(O_error_message,
                                      L_chk_shipment_exists,
                                      L_order_no,
                                      NULL,
                                      NULL,
                                      NULL) = FALSE then
                  return FALSE;
               end if;

               if rec.requested_qty != 0 then
                  if L_chk_shipment_exists = TRUE then
                     L_qty_expected := 0;
                     select sum(sk.qty_expected)
                       into L_qty_expected
                       from shipment sm,
                            shipsku sk
                      where sm.order_no = L_order_no
                        and sm.to_loc = rec.customer_loc
                        and sm.shipment = sk.shipment
                        and sk.item = rec.item;

                     if L_qty_expected > rec.requested_qty then
                        O_error_message := SQL_LIB.CREATE_MSG('FPO_INV_QTY_CHG',
                                                              rec.item,
                                                              rec.customer_loc,
                                                              NULL);
                        return FALSE;
                     end if;
                  end if;

                  open C_LOCK_ORDLOC;
                  close C_LOCK_ORDLOC;

                  if L_qty_ordered > rec.requested_qty then
                     update ordloc
                        set qty_ordered = rec.requested_qty,
                            qty_prescaled = L_qty_prescaled,
                            qty_cancelled = (NVL(qty_cancelled,0) + (qty_ordered - rec.requested_qty)),
                            cancel_code = 'B',
                            cancel_id = GET_USER,
                            cancel_date = LP_vdate
                      where order_no = L_order_no
                        and item = rec.item
                        and location = rec.customer_loc;
                  else
                     update ordloc
                        set qty_ordered = rec.requested_qty,
                            qty_prescaled = L_qty_prescaled
                      where order_no = L_order_no
                        and item = rec.item
                        and location = rec.customer_loc;
                  end if;
               else
                  if L_chk_shipment_exists = TRUE then
                     L_chk_ship_rcv := NULL;
                     open C_CHK_SHIP_RCV(rec.item,
                                         rec.customer_loc);
                     fetch C_CHK_SHIP_RCV into L_chk_ship_rcv;
                     close C_CHK_SHIP_RCV;

                     if L_chk_ship_rcv is NOT NULL then
                        O_error_message := SQL_LIB.CREATE_MSG('INV_QTY_CHANGE',
                                                              rec.item,
                                                              rec.customer_loc,
                                                              NULL);
                        return FALSE;
                     end if;
                  end if;

                  open C_LOCK_ORDLOC;
                  close C_LOCK_ORDLOC;

                  update ordloc
                     set qty_ordered = 0,
                         qty_cancelled = (NVL(qty_cancelled,0) + (qty_ordered)),
                         qty_prescaled = L_qty_prescaled,
                         cancel_code = 'B',
                         cancel_id = GET_USER,
                         cancel_date = LP_vdate
                   where order_no = L_order_no
                    and item = rec.item
                    and location = rec.customer_loc;

                  L_chk_sum_qty := 0;
                  select sum(qty_ordered)
                    into L_chk_sum_qty
                    from ordloc
                   where order_no = L_order_no;

                  if L_chk_sum_qty = 0 then
                     open C_LOCK_ORDHEAD;
                     close C_LOCK_ORDHEAD;

                     update ordhead
                        set status = 'C',
                            close_date = LP_vdate,
                            last_update_id = get_user,
                            last_update_datetime = sysdate
                      where order_no = L_order_no;
                  end if;
               end if; -- if requested_qty != 0
            end if; -- if L_chk_item is NULL

            if NVL(L_prev_order_no,-999) != L_order_no then
               -- initialize date varibales if new PO is being processed
               L_nb_date := NULL;
               L_na_date := NULL;
               L_prev_nb_date := NULL;
               L_prev_na_date := NULL;

               -- get distinct order numbers only
               LP_order_tbl.EXTEND();
               LP_order_tbl(LP_order_tbl.COUNT).order_no := L_order_no;
               LP_order_tbl(LP_order_tbl.COUNT).supplier := rec.source_loc_id;
            end if;

            if L_nb_date is NULL then
               L_nb_date := rec.need_date;
            else
               if rec.need_date < L_nb_date then
                  L_nb_date := rec.need_date;
               end if;
            end if;

            if L_na_date is NULL then
               L_na_date := rec.not_after_date;
            else
               if rec.not_after_date > L_na_date then
                  L_na_date := rec.not_after_date;
               end if;
            end if;

            -- update not_before_date and not_after_date based on the least need_date and most
            -- not_after_date per PO
            if (L_prev_order_no is NULL and (L_nb_date is NOT NULL or L_na_date is NOT NULL)) or
               (L_prev_order_no != L_order_no) or ((L_prev_order_no = L_order_no) and
               (L_nb_date < L_prev_nb_date or L_na_date > L_prev_na_date)) then
               open C_LOCK_ORDHEAD;
               close C_LOCK_ORDHEAD;

               update ordhead
                  set not_before_date = NVL(L_nb_date,not_before_date),
                      not_after_date = NVL(L_na_date,not_after_date),
                      last_update_id = get_user,
                      last_update_datetime = sysdate
                where order_no = L_order_no;
            end if;

            if rec.status = 'A' then
               open C_LOCK_WF_ORDHEAD;
               close C_LOCK_WF_ORDHEAD;

               update ordhead
                  set status = 'A',
                      orig_approval_date = LP_vdate,
                      orig_approval_id = GET_USER,
                      last_update_id = get_user,
                      last_update_datetime = sysdate
                where wf_order_no = I_wf_order_no
                  and status = 'W';

               if NVL(L_prev_order_no,-999) != L_order_no then
                  if OTB_SQL.ORD_APPROVE(L_order_no,
                                         O_error_message) = FALSE then
                     return FALSE;
                  end if;
               end if;

            end if;

            L_prev_order_no := L_order_no;
            L_prev_nb_date  := L_nb_date;
            L_prev_na_date  := L_na_date;
         END LOOP;
      END LOOP;
   else
      FOR rec in C_GET_MOD_F_PO_DEPT LOOP
         FOR j in C_GET_ORDER_NO_DEPT (rec.item,
                                       rec.customer_loc,
                                       rec.source_loc_id,
                                       rec.dept)LOOP
            L_order_no := j.order_no;
            L_orig_approval_date := j.orig_approval_date;
            L_chk_item := NULL;

            open C_CHK_ITEM(rec.item);
            fetch C_CHK_ITEM into L_chk_item;
            close C_CHK_ITEM;

            if L_chk_item is NULL then
               -- new item for the PO.
               if GET_ITEM_INFO (O_error_message,
                                 L_import_order_ind,
                                 L_import_country_id,
                                 L_import_id,
                                 L_import_type,
                                 L_routing_loc_id,
                                 L_clearing_zone_id,
                                 L_supp_pack_size,
                                 L_unit_cost,
                                 L_unit_retail,
                                 L_origin_country,
                                 rec.item,
                                 rec.source_loc_id,
                                 rec.customer_loc) = FALSE then
                  return FALSE;
               end if;

               --Building an ordsku record
               LP_ordsku_tbl.EXTEND();
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).order_no             := L_order_no;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).item                 := rec.item;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).ref_item             := NULL;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).origin_country_id    := L_origin_country;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).earliest_ship_date   := rec.need_date;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).latest_ship_date     := rec.need_date + LP_latest_ship_days;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).supp_pack_size       := L_supp_pack_size;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).non_scale_ind        := 'N';
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).pickup_loc           := NULL;
               LP_ordsku_tbl(LP_ordsku_tbl.COUNT).pickup_no            := NULL;

               --Building an ordloc record
               LP_ordloc_tbl.EXTEND();
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).order_no               := L_order_no;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).item                   := rec.item;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).location               := rec.customer_loc;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).loc_type               := 'S';
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).unit_retail            := L_unit_retail;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).qty_ordered            := rec.requested_qty;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).qty_prescaled          := rec.requested_qty;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).unit_cost              := L_unit_cost;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).unit_cost_init         := L_unit_cost;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).cost_source            := 'NORM';
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).non_scale_ind          := 'N';
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).qty_received           := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).last_received          := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).last_rounded_qty       := rec.requested_qty;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).last_grp_rounded_qty   := rec.requested_qty;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).qty_cancelled          := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).cancel_code            := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).cancel_date            := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).cancel_id              := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).original_repl_qty      := NULL;
               LP_ordloc_tbl(LP_ordloc_tbl.COUNT).tsf_po_link_no         := NULL;

            else
               L_qty_ordered   := 0;
               L_qty_prescaled := 0;
               select qty_ordered,
                      qty_prescaled
                 into L_qty_ordered,
                      L_qty_prescaled
                 from ordloc
                where order_no = L_order_no
                  and item = rec.item
                  and location = rec.customer_loc;

               if L_orig_approval_date is NULL then
                  L_qty_prescaled := rec.requested_qty;
               end if;

               L_chk_shipment_exists := FALSE;
               if CHECK_ORDER_SHIPPED(O_error_message,
                                      L_chk_shipment_exists,
                                      L_order_no,
                                      NULL,
                                      NULL,
                                      NULL) = FALSE then
                  return FALSE;
               end if;

               if rec.requested_qty != 0 then
                  if L_chk_shipment_exists = TRUE then
                     L_qty_expected := 0;
                     select sum(sk.qty_expected)
                       into L_qty_expected
                       from shipment sm,
                            shipsku sk
                      where sm.order_no = L_order_no
                        and sm.to_loc = rec.customer_loc
                        and sm.shipment = sk.shipment
                        and sk.item = rec.item;

                     if L_qty_expected > rec.requested_qty then
                        O_error_message := SQL_LIB.CREATE_MSG('FPO_INV_QTY_CHG',
                                                              rec.item,
                                                              rec.customer_loc,
                                                              NULL);
                        return FALSE;
                     end if;
                  end if;

                  open C_LOCK_ORDLOC;
                  close C_LOCK_ORDLOC;

                  if L_qty_ordered > rec.requested_qty then
                     update ordloc
                        set qty_ordered = rec.requested_qty,
                            qty_prescaled = L_qty_prescaled,
                            qty_cancelled = (NVL(qty_cancelled,0) + (qty_ordered - rec.requested_qty)),
                            cancel_code = 'B',
                            cancel_id = GET_USER,
                            cancel_date = LP_vdate
                      where order_no = L_order_no
                        and item = rec.item
                        and location = rec.customer_loc;
                  else
                     update ordloc
                        set qty_ordered = rec.requested_qty,
                            qty_prescaled = L_qty_prescaled
                      where order_no = L_order_no
                        and item = rec.item
                        and location = rec.customer_loc;
                  end if;
               else
                  if L_chk_shipment_exists = TRUE then
                     L_chk_ship_rcv := NULL;
                     open C_CHK_SHIP_RCV(rec.item,
                                         rec.customer_loc);
                     fetch C_CHK_SHIP_RCV into L_chk_ship_rcv;
                     close C_CHK_SHIP_RCV;

                     if L_chk_ship_rcv is NOT NULL then
                        O_error_message := SQL_LIB.CREATE_MSG('INV_QTY_CHANGE',
                                                              rec.item,
                                                              rec.customer_loc,
                                                              NULL);
                        return FALSE;
                     end if;
                  end if;

                  open C_LOCK_ORDLOC;
                  close C_LOCK_ORDLOC;

                  update ordloc
                     set qty_ordered = 0,
                         qty_cancelled = (NVL(qty_cancelled,0) + (qty_ordered)),
                         cancel_code = 'B',
                         cancel_id = GET_USER,
                         cancel_date = LP_vdate
                   where order_no = L_order_no
                    and item = rec.item
                    and location = rec.customer_loc;

                  L_chk_sum_qty := 0;
                  select sum(qty_ordered)
                    into L_chk_sum_qty
                    from ordloc
                   where order_no = L_order_no;

                  if L_chk_sum_qty = 0 then
                     open C_LOCK_ORDHEAD;
                     close C_LOCK_ORDHEAD;

                     update ordhead
                        set status = 'C',
                            close_date = LP_vdate,
                            last_update_id = get_user,
                            last_update_datetime = sysdate
                      where order_no = L_order_no;
                  end if;
               end if; -- if requested_qty != 0
            end if; -- if L_chk_item is NULL

            if NVL(L_prev_order_no,-999) != L_order_no then
               -- initialize date varibales if new PO is being processed
               L_nb_date := NULL;
               L_na_date := NULL;
               L_prev_nb_date := NULL;
               L_prev_na_date := NULL;

               -- get distinct order numbers only
               LP_order_tbl.EXTEND();
               LP_order_tbl(LP_order_tbl.COUNT).order_no := L_order_no;
               LP_order_tbl(LP_order_tbl.COUNT).supplier := rec.source_loc_id;
            end if;

            if L_nb_date is NULL then
               L_nb_date := rec.need_date;
            else
               if rec.need_date < L_nb_date then
                  L_nb_date := rec.need_date;
               end if;
            end if;

            if L_na_date is NULL then
               L_na_date := rec.not_after_date;
            else
               if rec.not_after_date > L_na_date then
                  L_na_date := rec.not_after_date;
               end if;
            end if;

            -- update not_before_date and not_after_date based on the least need_date and most
            -- not_after_date per PO
            if (L_prev_order_no is NULL and (L_nb_date is NOT NULL or L_na_date is NOT NULL)) or
               (L_prev_order_no != L_order_no) or ((L_prev_order_no = L_order_no) and
               (L_nb_date < L_prev_nb_date or L_na_date > L_prev_na_date)) then
               open C_LOCK_ORDHEAD;
               close C_LOCK_ORDHEAD;

               update ordhead
                  set not_before_date = NVL(L_nb_date,not_before_date),
                      not_after_date = NVL(L_na_date,not_after_date),
                      last_update_id = get_user,
                      last_update_datetime = sysdate  
                where order_no = L_order_no;
            end if;

            if rec.status = 'A' then
               open C_LOCK_WF_ORDHEAD;
               close C_LOCK_WF_ORDHEAD;

               update ordhead
                  set status = 'A',
                      orig_approval_date = LP_vdate,
                      orig_approval_id = GET_USER,
                      last_update_id = get_user,
                      last_update_datetime = sysdate
                where wf_order_no = I_wf_order_no
                  and status = 'W';

               if NVL(L_prev_order_no,-999) != L_order_no then
                  if OTB_SQL.ORD_APPROVE(L_order_no,
                                         O_error_message) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if;

            L_prev_order_no := L_order_no;
            L_prev_nb_date  := L_nb_date;
            L_prev_na_date  := L_na_date;
         END LOOP;
      END LOOP;
   end if;

   if PERSIST_CREATE_PO(O_error_message,
                        I_scaling,
                        I_undo_scaling) = FALSE then
      return FALSE;
   end if;

   FOR i in C_GET_F_PO_DEL LOOP
      if VALIDATE_F_PO_DELETE(O_error_message,
                              L_shipped,
                              i.order_no) = FALSE then
         return FALSE;
      end if;

      if DELETE_ORDER(O_error_message,
                      L_shipped,
                      i.order_no) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   FOR rec in C_GET_F_PO_ITEM_DEL LOOP
      L_item_del_cnt := 0;
      select count(*)
        into L_item_del_cnt
        from ordloc
       where order_no = rec.order_no;

      if L_item_del_cnt > 1 then
         if VALIDATE_F_PO_DTL_DELETE(O_error_message,
                                     L_shipped,
                                     rec.order_no,
                                     rec.item,
                                     rec.location) = FALSE then
            return FALSE;
         end if;

         if DELETE_ORDER_ITEM(O_error_message,
                              L_shipped,
                              rec.order_no,
                              rec.item,
                              rec.location) = FALSE then
            return FALSE;
         end if;
      elsif L_item_del_cnt = 1 then
         if VALIDATE_F_PO_DELETE(O_error_message,
                                 L_shipped,
                                 rec.order_no) = FALSE then
            return FALSE;
         end if;

         if DELETE_ORDER(O_error_message,
                         L_shipped,
                         rec.order_no) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('REC_LOCKED',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END MODIFY_FRANCHISE_PO;
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_F_PO_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_shipped         IN OUT   BOOLEAN,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50)   := 'WF_PO_SQL.VALIDATE_F_PO_DELETE';

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_order_no,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   O_shipped := FALSE;
   -- Cannot delete order if it is already shipped or received.
   if  CHECK_ORDER_SHIPPED(O_error_message,
                           O_shipped,
                           I_order_no,
                           NULL,
                           NULL,
                           NULL) = FALSE then
      return FALSE;
   end if;

   if O_shipped = TRUE then
      O_error_message := SQL_LIB.CREATE_MSG('CANT_DEL_ORD_SHIP',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_F_PO_DELETE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ORDER_SHIPPED(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_shipped         IN OUT  BOOLEAN,
                             I_order_no        IN      ORDLOC.ORDER_NO%TYPE,
                             I_item            IN      ORDLOC.ITEM%TYPE,
                             I_location        IN      ORDLOC.LOCATION%TYPE,
                             I_loc_type        IN      ORDLOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'WF_PO_SQL.CHECK_ORDER_SHIPPED';

   L_shipped    VARCHAR2(1) := NULL;

   cursor C_SHIPMENT is
     select 'x'
       from shipment sh, shipsku sk
      where sh.shipment = sk.shipment
        and sh.order_no = I_order_no
        and rownum = 1;

   cursor C_SHIPSKU is
     select 'x'
       from shipment sh, shipsku sk
      where sh.shipment = sk.shipment
        and sh.order_no = I_order_no
        and sk.item = I_item
        and sh.to_loc = I_location
        and rownum = 1;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_order_no,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_item is NULL and I_location is NULL and I_loc_type is NULL then
      open C_SHIPMENT;
      fetch C_SHIPMENT into L_shipped;
      close C_SHIPMENT;
   else
      open C_SHIPSKU;
      fetch C_SHIPSKU into L_shipped;
      close C_SHIPSKU;
   end if;

   if L_shipped is NOT NULL then
      O_shipped := TRUE;
   else
      O_shipped := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ORDER_SHIPPED;
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_F_PO_DTL_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_shipped         IN OUT   BOOLEAN,
                                  I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                                  I_item            IN       ORDLOC.ITEM%TYPE,
                                  I_location        IN       ORDHEAD.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50)   := 'WF_PO_SQL.VALIDATE_F_PO_DTL_DELETE';

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_order_no,
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_item,
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_location,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   O_shipped := FALSE;

   -- Cannot delete order line if it is already shipped or received.
   if CHECK_ORDER_SHIPPED(O_error_message,
                          O_shipped,
                          I_order_no,
                          I_item,
                          I_location,
                          'S') = FALSE then
      return FALSE;
   end if;

   if O_shipped = TRUE then
      O_error_message := SQL_LIB.CREATE_MSG('CANT_DEL_LINE_ORD_SHIP',
                                            I_order_no,
                                            I_item,
                                            I_location);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_F_PO_DTL_DELETE;
----------------------------------------------------------------------------------------
FUNCTION DELETE_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_shipped         IN       BOOLEAN,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50)            := 'WF_PO_SQL.DELETE_ORDER';

   cursor C_LOCK_ALLOC_HEADER is
     select 'x'
       from alloc_header
      where order_no = I_order_no
        for update nowait;

   cursor C_LOCK_ALLOC_DETAIL is
     select 'x'
       from alloc_detail
      where alloc_no in (select alloc_no
                           from alloc_header
                          where order_no = I_order_no)
        for update nowait;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_order_no,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_shipped = FALSE then
      -- delete allocation if any
      open C_LOCK_ALLOC_DETAIL;
      close C_LOCK_ALLOC_DETAIL;

      delete from alloc_detail
       where alloc_no in (select alloc_no
                            from alloc_header
                           where order_no = I_order_no);

      open C_LOCK_ALLOC_HEADER;
      close C_LOCK_ALLOC_HEADER;

      delete from alloc_header
       where order_no = I_order_no;
   end if;

   if ORDER_SQL.DELETE_ORDER(O_error_message,
                             I_order_no) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ORDER;
----------------------------------------------------------------------------------------
FUNCTION DELETE_ORDER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_shipped         IN       BOOLEAN,
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                           I_item            IN       ORDLOC.ITEM%TYPE,
                           I_location        IN       ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(50)            := 'WF_PO_SQL.DELETE_ORDER_ITEM';

   L_positive_qty   VARCHAR(1) := NULL;
   L_ordloc_rec     ORDER_SQL.ORDLOC_REC;
   L_index          NUMBER;

   cursor C_TOTAL_QTY is
     select 'x'
       from ordhead oh
      where oh.order_no = I_order_no
        and oh.status = 'A'
        and NOT EXISTS (select 'x'
                          from ordloc ol
                         where ol.order_no = oh.order_no
                           and ol.qty_ordered > 0
                           and rownum = 1);

   cursor C_LOCK_ALLOC_DETAIL is
     select 'x'
       from alloc_detail
      where alloc_no in (select alloc_no
                           from alloc_header
                          where order_no = I_order_no)
        for update nowait;

   cursor C_LOCK_ALLOC_HEADER is
     select 'x'
       from alloc_header
      where order_no = I_order_no
        for update nowait;

   cursor C_LOCK_ORDLOC_EXP is
      select 'x'
        from ordloc_exp
       where order_no = I_order_no
         and location = I_location
         and (item = I_item or
              pack_item = I_item)
         for update nowait;
BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_order_no,
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_item,
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_location,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_shipped = FALSE then
      -- passed validation
      -- delete allocation if any
      open C_LOCK_ALLOC_DETAIL;
      close C_LOCK_ALLOC_DETAIL;

      delete from alloc_detail
       where alloc_no in (select alloc_no
                            from alloc_header
                           where order_no = I_order_no
                             and item = I_item);

      open C_LOCK_ALLOC_HEADER;
      close C_LOCK_ALLOC_HEADER;

      delete from alloc_header
       where order_no = I_order_no
         and item = I_item;
   end if;

   L_ordloc_rec.order_no := I_order_no;

   L_ordloc_rec.items                  := ITEM_TBL();
   L_ordloc_rec.locations              := LOC_TBL();
   L_ordloc_rec.loc_types              := LOC_TYPE_TBL();
   L_ordloc_rec.unit_retails           := UNIT_RETAIL_TBL();
   L_ordloc_rec.ordered_qtys           := QTY_TBL();
   L_ordloc_rec.prescaled_qtys         := QTY_TBL();
   L_ordloc_rec.cancelled_qtys         := QTY_TBL();
   L_ordloc_rec.cancel_codes           := INDICATOR_TBL();
   L_ordloc_rec.cancel_dates           := DATE_TBL();
   L_ordloc_rec.cancel_ids             := USERID_TBL();
   L_ordloc_rec.unit_costs             := UNIT_COST_TBL();
   L_ordloc_rec.cost_sources           := CODE_TBL();
   L_ordloc_rec.non_scale_inds         := INDICATOR_TBL();
   L_ordloc_rec.estimated_instock_date := DATE_TBL();

   L_ordloc_rec.items.EXTEND();
   L_ordloc_rec.locations.EXTEND();

   L_index := L_ordloc_rec.items.COUNT;

   L_ordloc_rec.items(L_index)     := I_item;
   L_ordloc_rec.locations(L_index) := I_location;

   if ORDER_SQL.DELETE_ORDLOCS(O_error_message,
                               L_ordloc_rec) = FALSE then
      return FALSE;
   end if;

   -- check if after deleting the details, the Approved order still has positive qty_ordered
   -- if not, reject the message

   open C_TOTAL_QTY;
   fetch C_TOTAL_QTY into L_positive_qty;
   close C_TOTAL_QTY;

   if L_positive_qty is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DEL_ORD_LINE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if ORDER_SETUP_SQL.DELETE_SINGLE_CONTAINER_ITEM (O_error_message,
                                                    I_order_no,
                                                    I_item,
                                                    I_location,
                                                    'B') = FALSE then
      return FALSE;
   end if;

   open C_LOCK_ORDLOC_EXP;
   close C_LOCK_ORDLOC_EXP;

   delete from ordloc_exp
    where order_no = I_order_no
      and location = I_location
      and (item = I_item or
           pack_item = I_item);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ORDER_ITEM;
-----------------------------------------------------------------------------------------
FUNCTION SYNC_F_ORDER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_wf_order_no     IN OUT   WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                       O_order_status    IN OUT   ORDHEAD.STATUS%TYPE,
                       I_action_type     IN       VARCHAR2,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                       I_order_status    IN       ORDHEAD.STATUS%TYPE)
RETURN BOOLEAN IS
L_program                  VARCHAR2(50)                        := 'WF_PO_SQL.SYNC_F_ORDER';
L_inv_parm                 VARCHAR2(30)                        := NULL;
L_order_status             ORDHEAD.STATUS%TYPE                 := NULL;
L_order_location           ORDLOC.LOCATION%TYPE                := NULL;
L_order_type               ORDHEAD.ORDER_TYPE%TYPE             := NULL;
L_f_order_head_rec         OBJ_F_ORDER_HEAD_REC                := OBJ_F_ORDER_HEAD_REC();
L_f_order_detail_tbl       OBJ_F_ORDER_DETAIL_TBL              := OBJ_F_ORDER_DETAIL_TBL();
L_wf_order_status          WF_ORDER_HEAD.STATUS%TYPE           := NULL;
L_wf_order_type            WF_ORDER_HEAD.ORDER_TYPE%TYPE       := NULL;
L_wf_order_no              WF_ORDER_HEAD.WF_ORDER_NO%TYPE      := NULL;
L_wf_customer_id           WF_CUSTOMER.WF_CUSTOMER_ID%TYPE     := NULL;
L_credit_ind               WF_CUSTOMER.CREDIT_IND%TYPE         := NULL;
L_count_customer           NUMBER(2);
L_action_type              VARCHAR2(10)                        := I_action_type;

RECORD_LOCKED              EXCEPTION;
PRAGMA EXCEPTION_INIT(Record_Locked, -54);
L_table                    VARCHAR2(50)                        := NULL;

   cursor C_CHECK_ORDER is
      select oh.status,
             oh.wf_order_no,
             ol.location,
             oh.order_type
        from ordhead oh,
             ordloc ol
       where oh.order_no = I_order_no
         and oh.order_no = ol.order_no
         and rownum = 1;

   cursor C_GET_WF_ORDER_STATUS is
      select status,
             order_type
        from wf_order_head
       where wf_order_no = L_wf_order_no;

   cursor C_LOCK_WF_ORDER_DETAIL is
      select 'x'
        from wf_order_detail
       where wf_order_no = L_wf_order_no
        for update nowait;

   cursor C_CHECK_SINGLE_WF_CUST is
      select COUNT(DISTINCT s.wf_customer_id)
        from store s,
             ordloc ol
       where ol.order_no = I_order_no
         and s.store = ol.location
         and s.store_type = 'F';

   cursor C_GET_ORDER_HEAD is
      select OBJ_F_ORDER_HEAD_REC(oh.wf_order_no,                                            -- wf_order_no
                                  NVL(L_wf_order_type, DECODE(order_type, 'CO', 'X', 'A')),  -- order_type - 'X' (externally generated) for  customer orders
                                                                                             -- otherwise, 'A'uto for new else existing wf_order_head.order_type
                                  NULL,                                          -- currency code
                                  L_wf_order_status,                             -- status
                                  NULL,                                          -- cancel reason
                                  oh.comment_desc,                               -- comments
                                  L_f_order_detail_tbl)                          -- F_ORDER_DETAILS
        from ordhead oh
       where oh.order_no = I_order_no;

   cursor C_GET_ORDER_DETAIL is
      select OBJ_F_ORDER_DETAIL_REC(oh.wf_order_no,                              -- wf_order_no
                                    ol.item,                                     -- item
                                    'SU',                                        -- source_loc_type
                                    oh.supplier,                                 -- source_loc_id
                                    ol.location,                                 -- customer_loc
                                    ol.qty_ordered,                              -- requested_qty
                                    NULL,                                        -- cancel_reason
                                    GREATEST(NVL(oh.not_before_date,get_vdate),get_vdate),               -- need date
                                    GREATEST(NVL(oh.not_after_date,get_vdate),get_vdate))                -- not_after_date
        from ordhead oh,
             ordloc ol,
             store s
       where oh.order_no = I_order_no
         and oh.order_no = ol.order_no
         and ol.location = s.store
         and s.store_type = 'F';

BEGIN

   if I_order_no is NULL then
      L_inv_parm := 'I_order_no';
   elsif I_action_type is NULL then
      L_inv_parm := 'I_action_type';
   elsif I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE and I_order_status is NULL then
      L_inv_parm := 'I_order_status';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_action_type not in (RMS_CONSTANTS.WF_ACTION_CREATE,
                            RMS_CONSTANTS.WF_ACTION_UPDATE,
                            RMS_CONSTANTS.WF_ACTION_SHIPPED,
                            RMS_CONSTANTS.WF_ACTION_SCALING)then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ACTION_TYPE',
                                            I_action_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- Initialise the output status same as input status. The only scenario this will differ is for failed credit check.
   O_order_status := I_order_status;

   -- Validate that the Order number exists.
   L_table := 'ORDHEAD, ORDLOC';
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ORDER', L_table, I_order_no);
   open C_CHECK_ORDER;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ORDER', L_table, I_order_no);
   fetch C_CHECK_ORDER into L_order_status,
                            L_wf_order_no,
                            L_order_location,
                            L_order_type;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ORDER', L_table, I_order_no);
   close C_CHECK_ORDER;

   if L_order_status is NULL then
   -- Error out if no record exists for the input purchase order.
      O_error_message := SQL_LIB.CREATE_MSG('INV_PO_NO',
                                            I_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- If the request is to create a new purchase order or an update to existing purchase
   -- order with linked franchise order, do the franchise order processing logic. Else, for
   -- a regular purchase order to a non franchise location, do nothing.
   if I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE or L_wf_order_no is not NULL then
      -- For action type as scaling, the scaled quantity from ordloc table has to be applied
      -- back to the wf_order_detail table.
      if I_action_type =  RMS_CONSTANTS.WF_ACTION_SCALING then
         L_table := 'WF_ORDER_DETAIL';
         SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WF_ORDER_DETAIL', L_table, L_wf_order_no);
         open C_LOCK_WF_ORDER_DETAIL;
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WF_ORDER_DETAIL', L_table, L_wf_order_no);
         close C_LOCK_WF_ORDER_DETAIL;

         -- Update wf_order_detail with the latest quantity in ordloc for this order.
         merge into wf_order_detail wod
         using (select wod2.wf_order_no,
                       wod2.wf_order_line_no,
                       ol.qty_ordered
                  from ordhead oh,
                       ordloc ol,
                       wf_order_detail wod2
                 where oh.order_no = I_order_no
                   and oh.order_no = ol.order_no
                   and oh.wf_order_no = wod2.wf_order_no
                   and oh.supplier = wod2.source_loc_id
                   and wod2.source_loc_type = 'SU'
                   and wod2.customer_loc = ol.location
                   and wod2.item = ol.item) odtl
         on (    wod.wf_order_no = odtl.wf_order_no
             and wod.wf_order_line_no = odtl.wf_order_line_no)
         when matched then
            update set requested_qty =  odtl.qty_ordered,
                       last_update_datetime  = sysdate,
                       last_update_id = get_user;
      else -- create, update or shipped.
         -- A franchise order restricts locations belonging to a single franchise customer in the same order.
         -- Adding a check that the purchase order loaction belongs to single franchise customer.
         L_table := 'ORDLOC';
         SQL_LIB.SET_MARK('OPEN', 'C_CHECK_SINGLE_WF_CUST', L_table, I_order_no);
         open C_CHECK_SINGLE_WF_CUST;
         SQL_LIB.SET_MARK('FETCH', 'C_CHECK_SINGLE_WF_CUST', L_table, I_order_no);
         fetch C_CHECK_SINGLE_WF_CUST into L_count_customer;
         SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SINGLE_WF_CUST', L_table, I_order_no);
         close C_CHECK_SINGLE_WF_CUST;

         -- Error if the order locations belongs to multiple Franchise customer.
         -- Do nothing if the PO locations are not franchise store. Else, sync the order.
         if NVL(L_count_customer,0) > 1 then
            O_error_message := SQL_LIB.CREATE_MSG('INV_F_PO',
                                                  I_order_no,
                                                  NULL,
                                                  NULL);
            return FALSE;
         elsif NVL(L_count_customer,0) = 1 then
            -- Get the current stauts of the franchise order. This will be used to check if for udpate, the request will be approving the franchise
            -- order - which requires credit check.
            L_table := 'WF_ORDER_HEAD';
            SQL_LIB.SET_MARK('OPEN', 'C_GET_WF_ORDER_STATUS', L_table, L_wf_order_no);
            open C_GET_WF_ORDER_STATUS;
            SQL_LIB.SET_MARK('FETCH', 'C_GET_WF_ORDER_STATUS', L_table, L_wf_order_no);
            fetch C_GET_WF_ORDER_STATUS into L_wf_order_status, L_wf_order_type;
            SQL_LIB.SET_MARK('CLOSE', 'C_GET_WF_ORDER_STATUS', L_table, L_wf_order_no);
            close C_GET_WF_ORDER_STATUS;

            -- Do credit check for create franchise order when input intended order status is 'A' or
            -- when this is a update request with existing franchise order status as Input and intended PO status as A.
            if (I_order_status is not NULL and I_order_status = 'A') and
               (I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE or (I_action_type = RMS_CONSTANTS.WF_ACTION_UPDATE and L_wf_order_status in (WF_STATUS_INPUT, WF_STATUS_REQ_CREDIT))) then
               -- Get the credit indicator for the franchise customer.
               -- Do not check credit for customer orders.
               if L_order_type != 'CO' then
                  if WF_CUSTOMER_SQL.CHECK_WF_CUST_LOC_CREDIT (O_error_message,
                                                               L_credit_ind,
                                                               L_order_location) = FALSE then
                     return FALSE;
                  end if;
               end if;

               if L_credit_ind = 'N' then
                  O_order_status := L_order_status;               -- The purchase order cannot be approved. Return the ordhead.status back.
                  L_wf_order_status := WF_STATUS_REQ_CREDIT;      -- Keep the franchise order in Require Credit Approval.
               else  -- approve the Purchase order and franchise order.
                  O_order_status := 'A';
                  L_wf_order_status := 'A';
               end if;
            end if;

            -- Populate the Detail collection.
            L_table := 'ORDLOC, ORDHEAD';
            SQL_LIB.SET_MARK('OPEN', 'C_GET_ORDER_DETAIL', L_table, I_order_no);
            open C_GET_ORDER_DETAIL;
            SQL_LIB.SET_MARK('FETCH', 'C_GET_ORDER_DETAIL', L_table, I_order_no);
            fetch C_GET_ORDER_DETAIL BULK COLLECT into L_f_order_detail_tbl;
            SQL_LIB.SET_MARK('CLOSE', 'C_GET_ORDER_DETAIL', L_table, I_order_no);
            close C_GET_ORDER_DETAIL;

            if I_action_type = RMS_CONSTANTS.WF_ACTION_SHIPPED then
               L_wf_order_status := WF_STATUS_INPROGRESS; -- In Progress
               L_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;      -- Treat shipped as update header record for status.
            elsif  L_wf_order_status is NULL then
               L_wf_order_status := WF_STATUS_INPUT;
            end if;

            -- Prepare the header record.
            L_table := 'ORDHEAD';
            SQL_LIB.SET_MARK('OPEN', 'C_GET_ORDER_HEAD', L_table, I_order_no);
            open C_GET_ORDER_HEAD;
            SQL_LIB.SET_MARK('FETCH', 'C_GET_ORDER_HEAD', L_table, I_order_no);
            fetch C_GET_ORDER_HEAD into L_f_order_head_rec;
            SQL_LIB.SET_MARK('CLOSE', 'C_GET_ORDER_HEAD', L_table, I_order_no);
            close C_GET_ORDER_HEAD;

            -- If order is updated to 'C'losed status, set L_action_type to 'cancel' so that CANCEL_F_ORDER
            -- will be called through WF_CREATE_ORDER_SQL.PROCESS_F_ORDER to update wf_order_head.status to 'C'.
            -- Set cancel reason to Externally Deleted (ED).
            if L_order_status = 'C' then
               L_action_type := RMS_CONSTANTS.WF_ACTION_CANCEL;
               L_f_order_head_rec.cancel_reason := 'ED';
            end if;

            if WF_CREATE_ORDER_SQL.PROCESS_F_ORDER(O_error_message,
                                                   O_wf_order_no,    -- returned for a newly created franchise order
                                                   L_f_order_head_rec,
                                                   L_action_type) = FALSE then
               return FALSE;
            end if;
         end if; -- L_count_customer = 0
      end if;  -- Else for Action type = Scaling.
   end if; -- Purchase order to be processed is/will be linked with a franchise order.
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_CHECK_ORDER%ISOPEN then
         close C_CHECK_ORDER;
      end if;

      if C_GET_WF_ORDER_STATUS%ISOPEN then
         close C_GET_WF_ORDER_STATUS;
      end if;

      if C_LOCK_WF_ORDER_DETAIL%ISOPEN then
         close C_LOCK_WF_ORDER_DETAIL;
      end if;

      if C_CHECK_SINGLE_WF_CUST%ISOPEN then
         close C_CHECK_SINGLE_WF_CUST;
      end if;

      if C_GET_ORDER_HEAD%ISOPEN then
         close C_GET_ORDER_HEAD;
      end if;

      if C_GET_ORDER_DETAIL%ISOPEN then
         close C_GET_ORDER_DETAIL;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_wf_order_no,
                                            NULL);
      return FALSE;
   when OTHERS then
      if C_CHECK_ORDER%ISOPEN then
         close C_CHECK_ORDER;
      end if;

      if C_GET_WF_ORDER_STATUS%ISOPEN then
         close C_GET_WF_ORDER_STATUS;
      end if;

      if C_LOCK_WF_ORDER_DETAIL%ISOPEN then
         close C_LOCK_WF_ORDER_DETAIL;
      end if;

      if C_CHECK_SINGLE_WF_CUST%ISOPEN then
         close C_CHECK_SINGLE_WF_CUST;
      end if;

      if C_GET_ORDER_HEAD%ISOPEN then
         close C_GET_ORDER_HEAD;
      end if;

      if C_GET_ORDER_DETAIL%ISOPEN then
         close C_GET_ORDER_DETAIL;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SYNC_F_ORDER;
-----------------------------------------------------------------------------------------
FUNCTION DELETE_F_ORDER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(50)                  := 'WF_PO_SQL.DELETE_F_ORDER';
   L_wf_order_no              WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_status                   ORDHEAD.STATUS%TYPE;
   L_table                    VARCHAR2(50)                  := NULL;
   RECORD_LOCKED              EXCEPTION;
   PRAGMA EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_WF_ORDER_NO is
      select status,
             wf_order_no
        from ordhead
       where order_no = I_order_no
         for update nowait;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- Get the franchise order number
   L_table := 'ORDHEAD';
   SQL_LIB.SET_MARK('OPEN', 'C_GET_WF_ORDER_NO', L_table, I_order_no);
   open C_GET_WF_ORDER_NO;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_WF_ORDER_NO', L_table, I_order_no);
   fetch C_GET_WF_ORDER_NO into L_status, L_wf_order_no;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_WF_ORDER_NO', L_table, I_order_no);
   close C_GET_WF_ORDER_NO;

   if L_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PO_NO',
                                            I_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if L_wf_order_no is NOT NULL then

      -- Update ordhead to remove the foreign key reference of wf_order_head.wf_order_no
      update ordhead
         set wf_order_no = NULL,
             last_update_id = get_user,
             last_update_datetime = sysdate
       where order_no = I_order_no;

      if WF_CREATE_ORDER_SQL.PROCESS_F_ORDER(O_error_message,
                                             L_wf_order_no,
                                             OBJ_F_ORDER_HEAD_REC( L_wf_order_no,           -- wf_order_no
                                                                   NULL,                    -- Order_type
                                                                   NULL,                    -- Currency Code
                                                                   NULL,                    -- Status
                                                                   NULL,                    -- Cancel Reason
                                                                   NULL,                    -- Comments
                                                                   NULL),                   -- F_ORDER_DETAILS,
                                             RMS_CONSTANTS.WF_ACTION_DELETE) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_GET_WF_ORDER_NO%ISOPEN then
         close C_GET_WF_ORDER_NO;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_order_no,
                                            NULL);
      return FALSE;
   when OTHERS then
      if C_GET_WF_ORDER_NO%ISOPEN then
         close C_GET_WF_ORDER_NO;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_F_ORDER;
----------------------------------------------------------------------------------------
END WF_PO_SQL;
/