CREATE OR REPLACE PACKAGE BODY CORESVC_WF_CUSTOMER as
   cursor C_SVC_WF_CUST(I_process_id NUMBER,
                        I_chunk_id   NUMBER) is
      select pk_wf_customer.rowid                as pk_wf_customer_rid,
             wc_wcp_fk.rowid                     as wc_wcp_fk_rid,
             st.rowid                            as st_rid,
             pk_wf_customer.wf_customer_group_id as old_wf_customer_group_id,
             st.wf_customer_id,
             st.wf_customer_name,
             st.credit_ind,
             st.wf_customer_group_id,
             st.auto_approve_ind,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)      as action,
             st.process$status
        from svc_wf_cust        st,
             wf_customer        pk_wf_customer,
             wf_customer_group  wc_wcp_fk,
             dual
       where st.process_id           = I_process_id
         and st.chunk_id             = I_chunk_id
         and st.wf_customer_id       = pk_wf_customer.wf_customer_id (+)
         and st.wf_customer_group_id = wc_wcp_fk.wf_customer_group_id (+);
         
   cursor C_SVC_WFCUSTG(I_process_id NUMBER,
                        I_chunk_id   NUMBER) is
      select pk_wf_customer_group.rowid as pk_wf_customer_group_rid,
             st.rowid                   as st_rid,
             st.wf_customer_group_id,
             st.wf_customer_group_name,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)           as action,
             st.process$status
        from svc_wf_cust_grp st,
             wf_customer_group pk_wf_customer_group,
             dual
       where st.process_id           = I_process_id
         and st.chunk_id             = I_chunk_id
         and st.wf_customer_group_id = pk_wf_customer_group.wf_customer_group_id (+)
         and NVL(st.action, 'X') <> action_del ;
         
   cursor C_SVC_WFCUSTG_DEL(I_process_id NUMBER,
                            I_chunk_id   NUMBER) is
      select pk_wf_customer_group.rowid as pk_wf_customer_group_rid,
             st.rowid                   as st_rid,
             st.wf_customer_group_id,
             st.wf_customer_group_name,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)           as action,
             st.process$status
        from svc_wf_cust_grp st,
             wf_customer_group pk_wf_customer_group,
             dual
       where st.process_id           = I_process_id
         and st.chunk_id             = I_chunk_id
         and st.wf_customer_group_id = pk_wf_customer_group.wf_customer_group_id (+)
         and st.action = action_del ;
         
   Type WF_CUSTOMER_TAB IS TABLE OF WF_CUSTOMER_TL%ROWTYPE;
   Type WF_CUSTOMER_GROUP_TAB IS TABLE OF WF_CUSTOMER_GROUP_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab       errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab   s9t_errors_tab_typ;

   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.EXISTS(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := CORESVC_WF_CUSTOMER.template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).row_seq              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_WF_CUST_VAL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error          IN OUT  BOOLEAN,
                             I_rec            IN      C_SVC_WF_CUST%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_WF_CUST.PROCESS_WF_CUST_VAL';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WF_CUST';
   L_delete_cust VARCHAR2(1) ;
BEGIN

   if I_rec.wf_customer_id is NOT NULL
      and I_rec.wf_customer_id <= 0
      and I_rec.action IN (action_new)   then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'WF_CUSTOMER_ID',
                  'FCUST_CUSTOMER_GREAT_0');
      O_error:=TRUE;
   end if;
   if I_rec.action=action_del
      and I_rec.pk_wf_customer_rid is NOT NULL then
      if WF_CUSTOMER_SQL.DELETE_WF_CUSTOMER(O_error_message,
                                            L_delete_cust,
                                            I_rec.wf_customer_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error:=TRUE;
      end if;

      if L_delete_cust = 'N' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'WF_CUSTOMER_ID',
                     'CUSTOMER_ID_NO_DEL');
         O_error:=TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_WF_CUST_VAL;
---------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_WF_CUST_GRP_VAL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error          IN OUT  BOOLEAN,
                                 I_rec            IN      C_SVC_WFCUSTG%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_WF_CUST.PROCESS_WF_CUST_GRP_VAL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WF_CUST_GRP';
   L_delete_group VARCHAR2(1) ;
BEGIN
   if I_rec.action = action_new
      and I_rec.wf_customer_group_id is NOT NULL
      and I_rec.wf_customer_group_id <=0 then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'WF_CUSTOMER_GROUP_ID',
                  'STR_FORMAT_GREAT_0');
      O_error :=TRUE;
   end if;
   if I_rec.action=action_del
      and NOT O_error then
      if WF_CUSTOMER_SQL.DELETE_WF_CUSTOMER_GROUP(O_error_message,
                                                  L_delete_group,
                                                  I_rec.wf_customer_group_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error :=TRUE;
      end if;
      if L_delete_group = 'N' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'WF_CUSTOMER_GROUP_ID',
                     'WF_CUST_GRP_NO_DEL');
         O_error :=TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_WF_CUST_GRP_VAL;
----------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets        s9t_pkg.names_map_typ;
   WF_CUST_cols    s9t_pkg.names_map_typ;
   WF_CUST_TL_cols s9t_pkg.names_map_typ;
   WFCUSTG_cols    s9t_pkg.names_map_typ;
   WFCUSTG_TL_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                       := s9t_pkg.get_sheet_names(I_file_id);
   WF_CUST_cols                   := s9t_pkg.get_col_names(I_file_id,WF_CUST_sheet);
   WF_CUST$Action                 := WF_CUST_cols('ACTION');
   WF_CUST$wf_customer_id         := WF_CUST_cols('WF_CUSTOMER_ID');
   WF_CUST$wf_customer_name       := WF_CUST_cols('WF_CUSTOMER_NAME');
   WF_CUST$credit_ind             := WF_CUST_cols('CREDIT_IND');
   WF_CUST$wf_customer_group_id   := WF_CUST_cols('WF_CUSTOMER_GROUP_ID');
   WF_CUST$auto_approve_ind       := WF_CUST_cols('AUTO_APPROVE_IND');

   WF_CUST_TL_cols                := s9t_pkg.get_col_names(I_file_id,WF_CUST_TL_sheet);
   WF_CUST_TL$Action              := WF_CUST_TL_cols('ACTION');
   WF_CUST_TL$Lang                := WF_CUST_TL_cols('LANG');
   WF_CUST_TL$wf_customer_id      := WF_CUST_TL_cols('WF_CUSTOMER_ID');
   WF_CUST_TL$wf_customer_name    := WF_CUST_TL_cols('WF_CUSTOMER_NAME');
   
   WFCUSTG_cols                    := s9t_pkg.get_col_names(I_file_id,WFCUSTG_sheet);
   WFCUSTG$Action                  := WFCUSTG_cols('ACTION');
   WFCUSTG$WF_CUSTOMER_GROUP_ID    := WFCUSTG_cols('WF_CUSTOMER_GROUP_ID');
   WFCUSTG$WF_CUSTOMER_GROUP_NAME  := WFCUSTG_cols('WF_CUSTOMER_GROUP_NAME');

   WFCUSTG_TL_cols                 := s9t_pkg.get_col_names(I_file_id,WFCUSTG_TL_sheet);
   WFCUSTG_TL$Action               := WFCUSTG_TL_cols('ACTION');
   WFCUSTG_TL$lang                 := WFCUSTG_TL_cols('LANG');
   WFCUSTG_TL$WF_CUSTG_ID          := WFCUSTG_TL_cols('WF_CUSTOMER_GROUP_ID');
   WFCUSTG_TL$WF_CUSTG_NAME        := WFCUSTG_TL_cols('WF_CUSTOMER_GROUP_NAME');
END POPULATE_NAMES;
--------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_WF_CUST( I_file_id IN NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = WF_CUST_sheet )
   select s9t_row(s9t_cells(action_mod,
                            wf_customer_id,
                            wf_customer_name,
                            credit_ind,
                            wf_customer_group_id,
                            auto_approve_ind ))
     from wf_customer ;
END POPULATE_WF_CUST;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_WFCUSTG( I_file_id  IN  NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = WFCUSTG_sheet )
   select s9t_row(s9t_cells(action_mod,
                            wf_customer_group_id,
                            wf_customer_group_name))
     from wf_customer_group;
END POPULATE_WFCUSTG;
--------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_WF_CUST_TL( I_file_id IN NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = WF_CUST_TL_sheet )
   select s9t_row(s9t_cells(action_mod,
                            lang,
                            wf_customer_id,
                            wf_customer_name))
     from wf_customer_tl ;
END POPULATE_WF_CUST_TL;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_WFCUSTG_TL( I_file_id  IN  NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = WFCUSTG_TL_sheet )
   select s9t_row(s9t_cells(action_mod,
                            lang,
                            wf_customer_group_id,
                            wf_customer_group_name))
     from wf_customer_group_tl;
END POPULATE_WFCUSTG_TL;
--------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file      s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;

   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(WF_CUST_sheet);
   L_file.sheets(L_file.get_sheet_index(WF_CUST_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                     'WF_CUSTOMER_ID',
                                                                                     'WF_CUSTOMER_NAME',
                                                                                     'CREDIT_IND',
                                                                                     'WF_CUSTOMER_GROUP_ID',
                                                                                     'AUTO_APPROVE_IND');

   L_file.add_sheet(WF_CUST_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(WF_CUST_TL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                        'LANG',
                                                                                        'WF_CUSTOMER_ID',
                                                                                        'WF_CUSTOMER_NAME');
                                                                                        
   L_file.add_sheet(WFCUSTG_sheet);
   L_file.sheets(l_file.get_sheet_index(WFCUSTG_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                     'WF_CUSTOMER_GROUP_ID',
                                                                                     'WF_CUSTOMER_GROUP_NAME');

   L_file.add_sheet(WFCUSTG_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(WFCUSTG_TL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                        'LANG',
                                                                                        'WF_CUSTOMER_GROUP_ID',
                                                                                        'WF_CUSTOMER_GROUP_NAME');
   s9t_pkg.SAVE_OBJ(L_file);

END INIT_S9T;
----------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT s9t_folder.file_id%TYPE,
                     I_template_only_ind IN     CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_WF_CUSTOMER.CREATE_S9T';
   L_file    s9t_file;
BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key)=FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_WF_CUST(O_file_id);
      POPULATE_WF_CUST_TL(O_file_id);
      POPULATE_WFCUSTG(O_file_id);
      POPULATE_WFCUSTG_TL(O_file_id);
      Commit;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file)=FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_WF_CUST( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                               I_process_id IN   SVC_WF_CUST.PROCESS_ID%TYPE) IS
   TYPE svc_wf_cust_col_typ is TABLE OF SVC_WF_CUST%ROWTYPE;
   L_temp_rec      SVC_WF_CUST%ROWTYPE;
   svc_wf_cust_col svc_wf_cust_col_typ :=NEW svc_wf_cust_col_typ();
   L_process_id    SVC_WF_CUST.PROCESS_ID%TYPE;
   L_error         BOOLEAN:=FALSE;
   L_default_rec   SVC_WF_CUST%ROWTYPE;
   cursor C_MANDATORY_IND is
      select wf_customer_id_mi,
             wf_customer_name_mi,
             credit_ind_mi,
             wf_customer_group_id_mi,
             auto_approve_ind_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                FROM s9t_tmpl_cols_def
               WHERE template_key = CORESVC_WF_CUSTOMER.template_key
                 AND wksht_key    = 'WF_CUSTOMER')
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ( 'WF_CUSTOMER_ID'       as WF_CUSTOMER_ID,
                                                                 'WF_CUSTOMER_NAME'     as wf_customer_name,
                                                                 'CREDIT_IND'           as credit_ind,
                                                                 'WF_CUSTOMER_GROUP_ID' as wf_customer_group_id,
                                                                 'AUTO_APPROVE_IND'     as auto_approve_ind,
                                                                  NULL                  as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dmL_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_WF_CUST';
   L_pk_columns    VARCHAR2(255)  := 'Customer ID';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select wf_customer_id_dv,
                      wf_customer_name_dv,
                      credit_ind_dv,
                      wf_customer_group_id_dv,
                      auto_approve_ind_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_WF_CUSTOMER.template_key
                          and wksht_key     = 'WF_CUSTOMER')
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('WF_CUSTOMER_ID'       as wf_customer_id,
                                                                             'WF_CUSTOMER_NAME'     as wf_customer_name,
                                                                             'CREDIT_IND'           as credit_ind,
                                                                             'WF_CUSTOMER_GROUP_ID' as wf_customer_group_id,
                                                                             'AUTO_APPROVE_IND'     as auto_approve_ind,
                                                                              NULL                  as dummy)))
   LOOP
      BEGIN
         L_default_rec.wf_customer_id := rec.wf_customer_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'WF_CUSTOMER',
                             NULL,
                            'WF_CUSTOMER_ID',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.wf_customer_name := rec.wf_customer_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'WF_CUSTOMER',
                             NULL,
                            'WF_CUSTOMER_NAME',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.credit_ind := rec.credit_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'WF_CUSTOMER',
                             NULL,
                            'CREDIT_IND',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.wf_customer_group_id := rec.wf_customer_group_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'WF_CUSTOMER',
                             NULL,
                            'WF_CUSTOMER_GROUP_ID',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.auto_approve_ind := rec.auto_approve_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'WF_CUSTOMER',
                             NULL,
                            'AUTO_APPROVE_IND',
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(WF_CUST$action)               as action,
                      r.get_cell(WF_CUST$wf_customer_id)       as wf_customer_id,
                      r.get_cell(WF_CUST$wf_customer_name)     as wf_customer_name,
                      r.get_cell(WF_CUST$credit_ind)           as credit_ind,
                      r.get_cell(WF_CUST$wf_customer_group_id) as wf_customer_group_id,
                      r.get_cell(WF_CUST$auto_approve_ind)     as auto_approve_ind,
                      r.get_row_seq()                          as row_seq
                 FROM s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                WHERE sf.file_id  = I_file_id
                  AND ss.sheet_name = sheet_name_trans(WF_CUST_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         WF_CUST_sheet,
                         rec.row_seq,
                         action_column,
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.wf_customer_id := rec.wf_customer_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WF_CUST_sheet,
                            rec.row_seq,
                            'WF_CUSTOMER_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.wf_customer_name := rec.wf_customer_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WF_CUST_sheet,
                            rec.row_seq,
                            'WF_CUSTOMER_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.credit_ind := rec.credit_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WF_CUST_sheet,
                            rec.row_seq,
                            'CREDIT_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.wf_customer_group_id := rec.wf_customer_group_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WF_CUST_sheet,
                            rec.row_seq,
                            'WF_CUSTOMER_GROUP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.auto_approve_ind := rec.auto_approve_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WF_CUST_sheet,
                            rec.row_seq,
                            'AUTO_APPROVE_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = action_new then
         L_temp_rec.wf_customer_id       := NVL( L_temp_rec.wf_customer_id,
                                                 L_default_rec.wf_customer_id);
         L_temp_rec.wf_customer_name     := NVL( L_temp_rec.wf_customer_name,
                                                 L_default_rec.wf_customer_name);
         L_temp_rec.credit_ind           := NVL( L_temp_rec.credit_ind,
                                                 L_default_rec.credit_ind);
         L_temp_rec.wf_customer_group_id := NVL( L_temp_rec.wf_customer_group_id,
                                                 L_default_rec.wf_customer_group_id);
         L_temp_rec.auto_approve_ind     := NVL( L_temp_rec.auto_approve_ind,
                                                 L_default_rec.auto_approve_ind);
      end if;

      if not (l_temp_rec.wf_customer_id is NOT NULL
              and    1 = 1)then
         WRITE_S9T_ERROR( I_file_id,
                          WF_CUST_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_wf_cust_col.extend();
         svc_wf_cust_col(svc_wf_cust_col.COUNT()):= L_temp_rec;
      end if;

   END LOOP;
   BEGIN
      forall i IN 1..svc_wf_cust_col.COUNT SAVE EXCEPTIONS
      merge into SVC_WF_CUST st
      using(select(case
                  when L_mi_rec.wf_customer_id_mi       = 'N'
                   and svc_wf_cust_col(i).action = action_mod
                   and s1.wf_customer_id             IS NULL
                  then mt.wf_customer_id
                  else s1.wf_customer_id
                  end) as wf_customer_id,
                 (case
                  when L_mi_rec.wf_customer_name_mi     = 'N'
                   and svc_wf_cust_col(i).action = action_mod
                   and s1.wf_customer_name             IS NULL
                  then mt.wf_customer_name
                  else s1.wf_customer_name
                   end) as wf_customer_name,
                 (case
                  when L_mi_rec.credit_ind_mi           = 'N'
                   and svc_wf_cust_col(i).action = action_mod
                   and s1.credit_ind             IS NULL
                  then mt.credit_ind
                  else s1.credit_ind
                  end) as credit_ind,
                 (case
                  when L_mi_rec.wf_customer_group_id_mi = 'N'
                   and svc_wf_cust_col(i).action = action_mod
                   and s1.wf_customer_group_id             IS NULL
                  then mt.wf_customer_group_id
                  else s1.wf_customer_group_id
                  end) as wf_customer_group_id,
                 (case
                  when L_mi_rec.auto_approve_ind_mi     = 'N'
                   and svc_wf_cust_col(i).action = action_mod
                   and s1.auto_approve_ind             IS NULL
                  then mt.auto_approve_ind
                  else s1.auto_approve_ind
                  end) as auto_approve_ind,
                  null as dummy
             from (select svc_wf_cust_col(i).wf_customer_id       as wf_customer_id,
                          svc_wf_cust_col(i).wf_customer_name     as wf_customer_name,
                          svc_wf_cust_col(i).credit_ind           as credit_ind,
                          svc_wf_cust_col(i).wf_customer_group_id as wf_customer_group_id,
                          svc_wf_cust_col(i).auto_approve_ind     as auto_approve_ind,
                          null as dummy
                     from dual ) s1,
                     wf_customer mt
            where mt.wf_customer_id (+)     = s1.wf_customer_id
              and 1 = 1) sq
              on (st.wf_customer_id      = sq.wf_customer_id and
                  svc_wf_cust_col(i).action IN (action_mod,action_del))
      when matched then
         update
            set process_id            = svc_wf_cust_col(i).process_id ,
                action                = svc_wf_cust_col(i).action,
                chunk_id              = svc_wf_cust_col(i).chunk_id ,
                row_seq               = svc_wf_cust_col(i).row_seq ,
                process$status        = svc_wf_cust_col(i).process$status ,
                wf_customer_name      = sq.wf_customer_name ,
                wf_customer_group_id  = sq.wf_customer_group_id ,
                auto_approve_ind      = sq.auto_approve_ind ,
                credit_ind            = sq.credit_ind ,
                create_id             = svc_wf_cust_col(i).create_id ,
                CREATE_DATETIME       = svc_wf_cust_col(i).CREATE_DATETIME ,
                LasT_UPD_ID           = svc_wf_cust_col(i).LasT_UPD_ID ,
                LasT_UPD_DATETIME     = svc_wf_cust_col(i).LasT_UPD_DATETIME
      when NOT matched then
         insert(process_id ,
                chunk_id ,
                row_seq ,
                action ,
                process$status ,
                wf_customer_id ,
                wf_customer_name ,
                credit_ind ,
                wf_customer_group_id ,
                auto_approve_ind ,
                create_id ,
                create_datetime ,
                last_upd_id ,
                last_upd_datetime)
         values(svc_wf_cust_col(i).process_id ,
                svc_wf_cust_col(i).chunk_id ,
                svc_wf_cust_col(i).row_seq ,
                svc_wf_cust_col(i).action ,
                svc_wf_cust_col(i).process$status ,
                sq.wf_customer_id ,
                sq.wf_customer_name ,
                sq.credit_ind ,
                sq.wf_customer_group_id ,
                sq.auto_approve_ind ,
                svc_wf_cust_col(i).create_id ,
                svc_wf_cust_col(i).create_datetime ,
                svc_wf_cust_col(i).last_upd_id ,
                svc_wf_cust_col(i).last_upd_datetime);
EXCEPTION
   when DML_errorS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          WF_CUST_sheet,
                          svc_wf_cust_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_WF_CUST;
---------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_WFCUSTG( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                               I_process_id IN   SVC_WF_CUST_GRP.process_id%TYPE) IS
   TYPE svc_wfcustg_col_typ is TABLE OF SVC_WF_CUST_GRP%ROWTYPE;
   L_temp_rec       SVC_WF_CUST_GRP%ROWTYPE;
   svc_wfcustg_col  svc_wfcustg_col_typ :=NEW svc_wfcustg_col_typ();
   L_process_id     SVC_WF_CUST_GRP.process_id%TYPE;
   L_error          BOOLEAN             :=FALSE;
   L_default_rec    SVC_WF_CUST_GRP%ROWTYPE;
   cursor C_MANDATORY_IND is
      select wf_customer_group_id_mi,
             wf_customer_group_name_mi,
             1 as dummy
        from (SELECT column_key,
                     mandatory
                FROM s9t_tmpl_cols_def
               WHERE template_key  = CORESVC_WF_CUSTOMER.template_key
                 AND wksht_key     = 'WF_CUST_GRP'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ( 'WF_CUSTOMER_GROUP_ID'   as wf_customer_group_id,
                                             'WF_CUSTOMER_GROUP_NAME' as wf_customer_group_name,
                                                                 null as dummy));
   l_mi_rec        c_mandatory_ind%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_WF_CUST_GRP';
   L_pk_columns    VARCHAR2(255)  := 'Customer Group ID';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select wf_customer_group_id_dv,
                      wf_customer_group_name_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = CORESVC_WF_CUSTOMER.template_key
                          and wksht_key    = 'WF_CUST_GRP'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'WF_CUSTOMER_GROUP_ID'   as wf_customer_group_id,
                                                      'WF_CUSTOMER_GROUP_NAME' as wf_customer_group_name,
                                                                          NULL as dummy)))
   LOOP
   BEGIN
      L_default_rec.wf_customer_group_id := rec.wf_customer_group_id_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'WF_CUSTOMER_GROUP',
                         NULL,
                         'WF_CUSTOMER_GROUP_ID',
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.wf_customer_group_name := rec.wf_customer_group_name_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'WF_CUSTOMER_GROUP',
                         NULL,
                         'WF_CUSTOMER_GROUP_NAME',
                         NULL,
                         'INV_DEFAULT');
   END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(WFCUSTG$Action)                  as Action,
                      r.get_cell(WFCUSTG$wf_customer_group_id)    as wf_customer_group_id,
                      r.get_cell(WFCUSTG$wf_customer_group_name)  as wf_customer_group_name,
                      r.get_row_seq()                             as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(WFCUSTG_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WFCUSTG_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.wf_customer_group_id := rec.wf_customer_group_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WFCUSTG_sheet,
                            rec.row_seq,
                            'WF_CUSTOMER_GROUP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.wf_customer_group_name := rec.wf_customer_group_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WFCUSTG_sheet,
                            rec.row_seq,
                            'WF_CUSTOMER_GROUP_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = action_new then
         L_temp_rec.wf_customer_group_id   := NVL( L_temp_rec.wf_customer_group_id,
                                                   L_default_rec.wf_customer_group_id);
         L_temp_rec.wf_customer_group_name := NVL( L_temp_rec.wf_customer_group_name,
                                                   L_default_rec.wf_customer_group_name);
      end if;
      if not (L_temp_rec.wf_customer_group_id is NOT NULL
         and 1 = 1 ) then
         WRITE_S9T_ERROR(I_file_id,
                         WFCUSTG_sheet,
                         rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT l_error then
         svc_wfcustg_col.extend();
         svc_wfcustg_col(svc_wfcustg_col.COUNT()):=l_temp_rec;
      end if;

   END LOOP;
   BEGIN
      forall i IN 1..svc_wfcustg_col.COUNT SAVE EXCEPTIONS
      merge into SVC_WF_CUST_GRP st
      using (select(case
                    when l_mi_rec.wf_customer_group_id_mi    = 'N'
                    and svc_wfcustg_col(i).action = action_mod
                    and s1.wf_customer_group_id  IS NULL
                   then mt.wf_customer_group_id
                   else s1.wf_customer_group_id
                   end) as wf_customer_group_id,
                   (case
                    when l_mi_rec.wf_customer_group_name_mi    = 'N'
                     and svc_wfcustg_col(i).action = action_mod
                     and s1.wf_customer_group_name  IS NULL
                    then mt.wf_customer_group_name
                    else s1.wf_customer_group_name
                    end) as wf_customer_group_name,
                   null as dummy
               from(select svc_wfcustg_col(i).wf_customer_group_id   as wf_customer_group_id,
                           svc_wfcustg_col(i).wf_customer_group_name as wf_customer_group_name,
                           null as dummy
                      from dual) s1,
                      WF_CUSTOMER_GROUP mt
              where mt.wf_customer_group_id (+) = s1.wf_customer_group_id
                and 1 = 1) sq
                 on(st.wf_customer_group_id      = sq.wf_customer_group_id
                    and svc_wfcustg_col(i).action in (action_mod,action_del))
      when matched then
         update
            set process_id             = svc_wfcustg_col(i).process_id ,
                action                 = svc_wfcustg_col(i).action,
                chunk_id               = svc_wfcustg_col(i).chunk_id ,
                row_seq                = svc_wfcustg_col(i).row_seq ,
                process$status         = svc_wfcustg_col(i).process$status ,
                wf_customer_group_name = sq.wf_customer_group_name ,
                create_id              = svc_wfcustg_col(i).create_id ,
                create_datetime        = svc_wfcustg_col(i).create_datetime ,
                last_upd_id            =  svc_wfcustg_col(i).last_upd_id ,
                last_upd_datetime      = svc_wfcustg_col(i).last_upd_datetime
      when NOT matched then
         insert (process_id ,
                 chunk_id ,
                 row_seq ,
                 action ,
                 process$status ,
                 wf_customer_group_id ,
                 wf_customer_group_name ,
                 create_id ,
                 create_datetime ,
                 last_upd_id ,
                 last_upd_datetime )
         values (svc_wfcustg_col(i).process_id ,
                 svc_wfcustg_col(i).chunk_id ,
                 svc_wfcustg_col(i).row_seq ,
                 svc_wfcustg_col(i).action ,
                 svc_wfcustg_col(i).process$status ,
                 sq.wf_customer_group_id ,
                 sq.wf_customer_group_name ,
                 svc_wfcustg_col(i).create_id ,
                 svc_wfcustg_col(i).create_datetime ,
                 svc_wfcustg_col(i).last_upd_id ,
                 svc_wfcustg_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             WFCUSTG_sheet,
                             svc_wfcustg_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_WFCUSTG;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_WF_CUST_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id   IN   SVC_WF_CUSTOMER_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_wf_customer_tl_COL_TYP IS TABLE OF SVC_wf_customer_tl%ROWTYPE;
   L_temp_rec               SVC_WF_CUSTOMER_TL%ROWTYPE;
   SVC_WF_CUSTOMER_TL_COL   SVC_WF_CUSTOMER_TL_COL_TYP := NEW SVC_WF_CUSTOMER_TL_COL_TYP();
   L_process_id             SVC_WF_CUSTOMER_TL.PROCESS_ID%TYPE;
   L_error                  BOOLEAN := FALSE;
   L_default_rec            SVC_WF_CUSTOMER_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select wf_customer_name_mi,
             wf_customer_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = CORESVC_WF_CUSTOMER.template_key
                 and wksht_key    = 'WF_CUSTOMER_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('WF_CUSTOMER_NAME' as wf_customer_name,
                                       'WF_CUSTOMER_ID'   as wf_customer_id,
                                       'LANG'             as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_WF_CUSTOMER_TL';
   L_pk_columns    VARCHAR2(255)  := 'WF Customer ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select wf_customer_name_dv,
                      wf_customer_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = CORESVC_WF_CUSTOMER.template_key
                          and wksht_key    = 'WF_CUSTOMER_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('WF_CUSTOMER_NAME' as wf_customer_name,
                                                'WF_CUSTOMER_ID'   as wf_customer_id,
                                                'LANG'             as lang)))
   LOOP
      BEGIN
         L_default_rec.wf_customer_name := rec.wf_customer_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WF_CUST_TL_SHEET ,
                            NULL,
                           'WF_CUSTOMER_NAME' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.wf_customer_id := rec.wf_customer_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WF_CUST_TL_SHEET ,
                            NULL,
                           'WF_CUSTOMER_ID' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WF_CUST_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(wf_cust_tl$action))           as action,
                      r.get_cell(wf_cust_tl$wf_customer_name)        as wf_customer_name,
                      r.get_cell(wf_cust_tl$wf_customer_id)          as wf_customer_id,
                      r.get_cell(wf_cust_tl$lang)                    as lang,
                      r.get_row_seq()                                as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(WF_CUST_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WF_CUST_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.wf_customer_name := rec.wf_customer_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WF_CUST_TL_SHEET,
                            rec.row_seq,
                            'WF_CUSTOMER_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.wf_customer_id := rec.wf_customer_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WF_CUST_TL_SHEET,
                            rec.row_seq,
                            'WF_CUSTOMER_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WF_CUST_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = action_new then
         L_temp_rec.wf_customer_name := NVL( L_temp_rec.wf_customer_name,L_default_rec.wf_customer_name);
         L_temp_rec.wf_customer_id   := NVL( L_temp_rec.wf_customer_id,L_default_rec.wf_customer_id);
         L_temp_rec.lang             := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.wf_customer_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         WF_CUST_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_wf_customer_tl_col.extend();
         svc_wf_customer_tl_col(SVC_wf_customer_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..SVC_wf_customer_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_wf_customer_tl st
      using(select
                  (case
                   when l_mi_rec.wf_customer_name_mi = 'N'
                    and svc_wf_customer_tl_col(i).action = action_mod
                    and s1.wf_customer_name IS NULL then
                        mt.wf_customer_name
                   else s1.wf_customer_name
                   end) as wf_customer_name,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_wf_customer_tl_col(i).action = action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.wf_customer_id_mi = 'N'
                    and svc_wf_customer_tl_col(i).action = action_mod
                    and s1.wf_customer_id IS NULL then
                        mt.wf_customer_id
                   else s1.wf_customer_id
                   end) as wf_customer_id
              from (select svc_wf_customer_tl_col(i).wf_customer_name as wf_customer_name,
                           svc_wf_customer_tl_col(i).wf_customer_id        as wf_customer_id,
                           svc_wf_customer_tl_col(i).lang              as lang
                      from dual) s1,
                   wf_customer_tl mt
             where mt.wf_customer_id (+) = s1.wf_customer_id
               and mt.lang (+)       = s1.lang) sq
                on (st.wf_customer_id = sq.wf_customer_id and
                    st.lang = sq.lang and
                    svc_wf_customer_tl_col(i).ACTION IN (action_mod,action_del))
      when matched then
      update
         set process_id        = svc_wf_customer_tl_col(i).process_id ,
             chunk_id          = svc_wf_customer_tl_col(i).chunk_id ,
             row_seq           = svc_wf_customer_tl_col(i).row_seq ,
             action            = svc_wf_customer_tl_col(i).action ,
             process$status    = svc_wf_customer_tl_col(i).process$status ,
             wf_customer_name = sq.wf_customer_name
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             wf_customer_name ,
             wf_customer_id ,
             lang)
      values(svc_wf_customer_tl_col(i).process_id ,
             svc_wf_customer_tl_col(i).chunk_id ,
             svc_wf_customer_tl_col(i).row_seq ,
             svc_wf_customer_tl_col(i).action ,
             svc_wf_customer_tl_col(i).process$status ,
             sq.wf_customer_name ,
             sq.wf_customer_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            WF_CUST_TL_SHEET,
                            svc_wf_customer_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_WF_CUST_TL;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_WFC_GROUP_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id   IN   SVC_WF_CUSTOMER_GROUP_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_WF_CUSTG_TL_COL_TYP IS TABLE OF SVC_WF_CUSTOMER_GROUP_TL%ROWTYPE;
   L_temp_rec                     SVC_WF_CUSTOMER_GROUP_TL%ROWTYPE;
   SVC_WF_CUSTOMER_GROUP_TL_COL   SVC_WF_CUSTG_TL_COL_TYP := NEW SVC_WF_CUSTG_TL_COL_TYP();
   L_process_id                   SVC_WF_CUSTOMER_GROUP_TL.PROCESS_ID%TYPE;
   L_error                        BOOLEAN := FALSE;
   L_default_rec                  SVC_WF_CUSTOMER_GROUP_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select wf_customer_group_name_mi,
             wf_customer_group_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = CORESVC_WF_CUSTOMER.template_key
                 and wksht_key    = 'WF_CUSTOMER_GROUP_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('WF_CUSTOMER_GROUP_NAME' as wf_customer_group_name,
                                       'WF_CUSTG_ID'   as wf_customer_group_id,
                                       'LANG'          as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_WF_CUSTOMER_GROUP_TL';
   L_pk_columns    VARCHAR2(255)  := 'WF Customer Group ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select wf_customer_group_name_dv,
                      wf_customer_group_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = CORESVC_WF_CUSTOMER.template_key
                          and wksht_key    = 'WF_CUSTOMER_GROUP_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('WF_CUSTOMER_GROUP_NAME' as wf_customer_group_name,
                                                'WF_CUSTG_ID'   as wf_customer_group_id,
                                                'LANG'          as lang)))
   LOOP
      BEGIN
         L_default_rec.wf_customer_group_name := rec.wf_customer_group_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WFCUSTG_TL_SHEET ,
                            NULL,
                           'WF_CUSTOMER_GROUP_NAME' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.wf_customer_group_id := rec.wf_customer_group_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           WFCUSTG_TL_SHEET ,
                            NULL,
                           'WF_CUSTOMER_GROUP_ID' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WFCUSTG_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(wfcustg_tl$action))     as action,
                      r.get_cell(wfcustg_tl$wf_custg_name)     as wf_customer_group_name,
                      r.get_cell(wfcustg_tl$wf_custg_id)       as wf_customer_group_id,
                      r.get_cell(wfcustg_tl$lang)              as lang,
                      r.get_row_seq()                          as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(WFCUSTG_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WFCUSTG_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.wf_customer_group_name := rec.wf_customer_group_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WFCUSTG_TL_SHEET,
                            rec.row_seq,
                            'WF_CUSTOMER_GROUP_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.wf_customer_group_id := rec.wf_customer_group_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WFCUSTG_TL_SHEET,
                            rec.row_seq,
                            'WF_CUSTOMER_GROUP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WFCUSTG_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = action_new then
         L_temp_rec.wf_customer_group_name := NVL( L_temp_rec.wf_customer_group_name,L_default_rec.wf_customer_group_name);
         L_temp_rec.wf_customer_group_id   := NVL( L_temp_rec.wf_customer_group_id,L_default_rec.wf_customer_group_id);
         L_temp_rec.lang                   := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.wf_customer_group_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         WFCUSTG_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_wf_customer_group_TL_col.extend();
         svc_wf_customer_group_TL_col(svc_wf_customer_group_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_wf_customer_group_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_wf_customer_group_TL st
      using(select
                  (case
                   when l_mi_rec.wf_customer_group_name_mi = 'N'
                    and svc_wf_customer_group_TL_col(i).action = action_mod
                    and s1.wf_customer_group_name IS NULL then
                        mt.wf_customer_group_name
                   else s1.wf_customer_group_name
                   end) as wf_customer_group_name,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_wf_customer_group_TL_col(i).action = action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.wf_customer_group_id_mi = 'N'
                    and svc_wf_customer_group_TL_col(i).action = action_mod
                    and s1.wf_customer_group_id IS NULL then
                        mt.wf_customer_group_id
                   else s1.wf_customer_group_id
                   end) as wf_customer_group_id
              from (select svc_wf_customer_group_TL_col(i).wf_customer_group_name as wf_customer_group_name,
                           svc_wf_customer_group_TL_col(i).wf_customer_group_id        as wf_customer_group_id,
                           svc_wf_customer_group_TL_col(i).lang              as lang
                      from dual) s1,
                   wf_customer_group_TL mt
             where mt.wf_customer_group_id (+) = s1.wf_customer_group_id
               and mt.lang (+)       = s1.lang) sq
                on (st.wf_customer_group_id = sq.wf_customer_group_id and
                    st.lang = sq.lang and
                    svc_wf_customer_group_TL_col(i).ACTION IN (action_mod,action_del))
      when matched then
      update
         set process_id        = svc_wf_customer_group_TL_col(i).process_id ,
             chunk_id          = svc_wf_customer_group_TL_col(i).chunk_id ,
             row_seq           = svc_wf_customer_group_TL_col(i).row_seq ,
             action            = svc_wf_customer_group_TL_col(i).action ,
             process$status    = svc_wf_customer_group_TL_col(i).process$status ,
             wf_customer_group_name = sq.wf_customer_group_name
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             wf_customer_group_name ,
             wf_customer_group_id ,
             lang)
      values(svc_wf_customer_group_TL_col(i).process_id ,
             svc_wf_customer_group_TL_col(i).chunk_id ,
             svc_wf_customer_group_TL_col(i).row_seq ,
             svc_wf_customer_group_TL_col(i).action ,
             svc_wf_customer_group_TL_col(i).process$status ,
             sq.wf_customer_group_name ,
             sq.wf_customer_group_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            WFCUSTG_TL_SHEET,
                            svc_wf_customer_group_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_WFC_GROUP_TL;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count   IN OUT NUMBER,
                     I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id    IN     NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_WF_CUSTOMER.PROCESS_S9T';
   L_file           s9t_file;
   L_sheets         s9t_pkg.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   MAX_CHAR     EXCEPTION;
   PRAGMA       EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE)= FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'S9T_INVALID_TEMPLATE');
   else
       POPULATE_NAMES(I_file_id);
       sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                               L_file.user_lang);
       PROCESS_S9T_WF_CUST(I_file_id,
                           I_process_id);
       PROCESS_S9T_WF_CUST_TL(I_file_id,
                              I_process_id);
      PROCESS_S9T_WFCUSTG(I_file_id,
                          I_process_id);
      PROCESS_S9T_WFC_GROUP_TL(I_file_id,
                               I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
   insert
     into s9t_errors
   values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
    else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   commit;

   return TRUE;
EXCEPTION

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   RETURN FALSE;
END PROCESS_S9T;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_WF_CUST_INS( O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_wf_cust_temp_rec IN     WF_CUSTOMER%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_WF_CUSTOMER.EXEC_WF_CUST_INS';
BEGIN
   insert into wf_customer
      values I_wf_cust_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WF_CUST_INS;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_WF_CUST_UPD( O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_wf_cust_temp_rec   IN       WF_CUSTOMER%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_WF_CUSTOMER.EXEC_WF_CUST_UPD';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WF_CUST';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   cursor C_WF_CUST_LOCK is
      select 'X'
        from svc_wf_cust
       where wf_customer_id = I_wf_cust_temp_rec.wf_customer_id
       for update nowait;
BEGIN
   open C_WF_CUST_LOCK;
   close C_WF_CUST_LOCK;
   update wf_customer
      set row = I_wf_cust_temp_rec
    where 1 = 1
      and wf_customer_id = I_wf_cust_temp_rec.wf_customer_id;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_wf_cust_temp_rec.wf_customer_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_WF_CUST_LOCK%ISOPEN then
         close C_WF_CUST_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WF_CUST_UPD;
-------------------------------------------------------------------------------------------------------
FUNCTION EXEC_WF_CUST_DEL( O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_wf_cust_temp_rec IN     WF_CUSTOMER%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_WF_CUSTOMER.EXEC_WF_CUST_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WF_CUST';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_WF_CUST_TL_LOCK is
      select 'X'
        from wf_customer_tl
       where wf_customer_id = I_wf_cust_temp_rec.wf_customer_id
       for update nowait;

   cursor C_WF_CUST_LOCK is
      select 'X'
        from wf_customer
       where wf_customer_id = I_wf_cust_temp_rec.wf_customer_id
       for update nowait;
BEGIN
   open C_WF_CUST_TL_LOCK;
   close C_WF_CUST_TL_LOCK;
   delete
     from wf_customer_tl
    where wf_customer_id = I_wf_cust_temp_rec.wf_customer_id;

   open C_WF_CUST_LOCK;
   close C_WF_CUST_LOCK;
   delete
     from wf_customer
    where wf_customer_id = I_wf_cust_temp_rec.wf_customer_id;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_wf_cust_temp_rec.wf_customer_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_WF_CUST_TL_LOCK%ISOPEN then
         close C_WF_CUST_TL_LOCK;
      end if;
      if C_WF_CUST_LOCK%ISOPEN then
         close C_WF_CUST_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WF_CUST_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_WF_CUST_TL_INS(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_customer_tl_ins_tab    IN       WF_CUSTOMER_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_WF_CUSTOMER.EXEC_WF_CUST_TL_INS';

BEGIN
   if I_wf_customer_tl_ins_tab is NOT NULL and I_wf_customer_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_wf_customer_tl_ins_tab.COUNT()
         insert into wf_customer_tl
              values I_wf_customer_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_WF_CUST_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_WF_CUST_TL_UPD(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_customer_tl_upd_tab   IN       WF_CUSTOMER_TAB,
                             I_wf_customer_tl_upd_rst   IN       ROW_SEQ_TAB,
                             I_process_id               IN       SVC_WF_CUSTOMER_TL.PROCESS_ID%TYPE,
                             I_chunk_id                 IN       SVC_WF_CUSTOMER_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_WF_CUSTOMER.EXEC_WF_CUST_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WF_CUSTOMER_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_WF_CUSTOMER_TL_UPD(I_wf_customer_id  WF_CUSTOMER_TL.WF_CUSTOMER_ID%TYPE,
                                    I_lang            WF_CUSTOMER_TL.LANG%TYPE) is
      select 'x'
        from wf_customer_tl
       where wf_customer_id = I_wf_customer_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_wf_customer_tl_upd_tab is NOT NULL and I_wf_customer_tl_upd_tab.count > 0 then
      for i in I_wf_customer_tl_upd_tab.FIRST..I_wf_customer_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_wf_customer_tl_upd_tab(i).lang);
            L_key_val2 := 'WF Customer ID:'||to_char(I_wf_customer_tl_upd_tab(i).wf_customer_id);
            open C_LOCK_WF_CUSTOMER_TL_UPD(I_wf_customer_tl_upd_tab(i).wf_customer_id,
                                           I_wf_customer_tl_upd_tab(i).lang);
            close C_LOCK_WF_CUSTOMER_TL_UPD;
            
            update wf_customer_tl
               set wf_customer_name = I_wf_customer_tl_upd_tab(i).wf_customer_name,
                   last_update_id = I_wf_customer_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_wf_customer_tl_upd_tab(i).last_update_datetime
             where lang = I_wf_customer_tl_upd_tab(i).lang
               and wf_customer_id = I_wf_customer_tl_upd_tab(i).wf_customer_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_WF_CUSTOMER_TL',
                           I_wf_customer_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_WF_CUSTOMER_TL_UPD%ISOPEN then
         close C_LOCK_WF_CUSTOMER_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_WF_CUST_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_WF_CUST_TL_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_customer_tl_del_tab   IN       WF_CUSTOMER_TAB,
                             I_wf_customer_tl_del_rst   IN       ROW_SEQ_TAB,
                             I_process_id               IN       SVC_WF_CUSTOMER_TL.PROCESS_ID%TYPE,
                             I_chunk_id                 IN       SVC_WF_CUSTOMER_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_WF_CUSTOMER.EXEC_WF_CUST_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WF_CUSTOMER_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_WF_CUSTOMER_TL_DEL(I_wf_customer_id  WF_CUSTOMER_TL.WF_CUSTOMER_ID%TYPE,
                                    I_lang            WF_CUSTOMER_TL.LANG%TYPE) is
      select 'x'
        from wf_customer_tl
       where wf_customer_id = I_wf_customer_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_wf_customer_tl_del_tab is NOT NULL and I_wf_customer_tl_del_tab.count > 0 then
      for i in I_wf_customer_tl_del_tab.FIRST..I_wf_customer_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_wf_customer_tl_del_tab(i).lang);
            L_key_val2 := 'WF Customer ID:'||to_char(I_wf_customer_tl_del_tab(i).wf_customer_id);
            open C_LOCK_WF_CUSTOMER_TL_DEL(I_wf_customer_tl_del_tab(i).wf_customer_id,
                                           I_wf_customer_tl_del_tab(i).lang);
            close C_LOCK_WF_CUSTOMER_TL_DEL;
            
            delete wf_customer_tl
             where lang = I_wf_customer_tl_del_tab(i).lang
               and wf_customer_id = I_wf_customer_tl_del_tab(i).wf_customer_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_WF_CUSTOMER_TL',
                           I_wf_customer_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_WF_CUSTOMER_TL_DEL%ISOPEN then
         close C_LOCK_WF_CUSTOMER_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_WF_CUST_TL_DEL;
--------------------------------------------------------------------------------------
FUNCTION EXEC_WFCUSTG_INS( O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_wfcustg_temp_rec  IN      WF_CUSTOMER_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_WF_CUSTOMER.EXEC_WFCUSTG_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WF_CUST_GRP';
BEGIN
   insert into wf_customer_group
      values I_wfcustg_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WFCUSTG_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_WFCUSTG_UPD( O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_wfcustg_temp_rec  IN      WF_CUSTOMER_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_WF_CUSTOMER.EXEC_WFCUSTG_UPD';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WF_CUST_GRP';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   cursor C_WFCUSTG_LOCK is
      select 'X'
        from wf_customer_group
       where wf_customer_group_id = I_wfcustg_temp_rec.wf_customer_group_id
       for update nowait;
BEGIN
   open C_WFCUSTG_LOCK;
   close C_WFCUSTG_LOCK;
   update wf_customer_group
      set row = I_wfcustg_temp_rec
    where 1 = 1
      and wf_customer_group_id = I_wfcustg_temp_rec.wf_customer_group_id;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_wfcustg_temp_rec.wf_customer_group_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_WFCUSTG_LOCK%ISOPEN then
         close C_WFCUSTG_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WFCUSTG_UPD;
----------------------------------------------------------------------------------------
FUNCTION EXEC_WFCUSTG_DEL( O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_wfcustg_temp_rec  IN      WF_CUSTOMER_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_WF_CUSTOMER.EXEC_WFCUSTG_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WF_CUST_GRP';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_WFCUSTG_TL_LOCK is
      select 'X'
        from wf_customer_group_tl
       where wf_customer_group_id = I_wfcustg_temp_rec.wf_customer_group_id
       for update nowait;

   cursor C_WFCUSTG_LOCK is
      select 'X'
        from wf_customer_group
       where wf_customer_group_id = I_wfcustg_temp_rec.wf_customer_group_id
       for update nowait;
BEGIN
   open C_WFCUSTG_TL_LOCK;
   close C_WFCUSTG_TL_LOCK;
   delete
     from wf_customer_group_tl
    where wf_customer_group_id = I_wfcustg_temp_rec.wf_customer_group_id;

   open C_WFCUSTG_LOCK;
   close C_WFCUSTG_LOCK;
   delete
     from wf_customer_group
    where wf_customer_group_id = I_wfcustg_temp_rec.wf_customer_group_id;
   return TRUE;
EXCEPTION
  when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_wfcustg_temp_rec.wf_customer_group_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_WFCUSTG_TL_LOCK%ISOPEN then
         close C_WFCUSTG_TL_LOCK;
      end if;
      if C_WFCUSTG_LOCK%ISOPEN then
         close C_WFCUSTG_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      close C_WFCUSTG_LOCK;
      return FALSE;
END EXEC_WFCUSTG_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_WFCUSTG_TL_INS(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_cust_group_tl_ins_tab    IN       WF_CUSTOMER_GROUP_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_WF_CUSTOMER.EXEC_WFCUSTG_TL_INS';

BEGIN
   if I_wf_cust_group_tl_ins_tab is NOT NULL and I_wf_cust_group_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_wf_cust_group_tl_ins_tab.COUNT()
         insert into wf_customer_group_tl
              values I_wf_cust_group_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_WFCUSTG_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_WFCUSTG_TL_UPD(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wf_cust_group_tl_upd_tab   IN       WF_CUSTOMER_GROUP_TAB,
                             I_wf_custg_tl_upd_rst        IN       ROW_SEQ_TAB,
                             I_process_id                 IN       SVC_WF_CUSTOMER_GROUP_TL.PROCESS_ID%TYPE,
                             I_chunk_id                   IN       SVC_WF_CUSTOMER_GROUP_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_WF_CUSTOMER.EXEC_WFCUSTG_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WF_CUSTOMER_GROUP_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_WFCUSTG_TL_UPD(I_wf_customer_group  WF_CUSTOMER_GROUP_TL.WF_CUSTOMER_GROUP_ID%TYPE,
                                I_lang               WF_CUSTOMER_GROUP_TL.LANG%TYPE) is
      select 'x'
        from wf_customer_group_TL
       where wf_customer_group_id = I_wf_customer_group
         and lang = I_lang
         for update nowait;

BEGIN
   if I_wf_cust_group_tl_upd_tab is NOT NULL and I_wf_cust_group_tl_upd_tab.count > 0 then
      for i in I_wf_cust_group_tl_upd_tab.FIRST..I_wf_cust_group_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_wf_cust_group_tl_upd_tab(i).lang);
            L_key_val2 := 'WF Customer Group ID: '||to_char(I_wf_cust_group_tl_upd_tab(i).wf_customer_group_id);
            open C_LOCK_WFCUSTG_TL_UPD(I_wf_cust_group_tl_upd_tab(i).wf_customer_group_id,
                                                 I_wf_cust_group_tl_upd_tab(i).lang);
            close C_LOCK_WFCUSTG_TL_UPD;
            
            update wf_customer_group_tl
               set wf_customer_group_name = I_wf_cust_group_tl_upd_tab(i).wf_customer_group_name,
                   last_update_id = I_wf_cust_group_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_wf_cust_group_tl_upd_tab(i).last_update_datetime
             where lang = I_wf_cust_group_tl_upd_tab(i).lang
               and wf_customer_group_id = I_wf_cust_group_tl_upd_tab(i).wf_customer_group_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_WF_CUSTOMER_GROUP_TL',
                           I_wf_custg_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_WFCUSTG_TL_UPD%ISOPEN then
         close C_LOCK_WFCUSTG_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_WFCUSTG_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_WFCUSTG_TL_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wfcustg_TL_del_tab   IN       WF_CUSTOMER_GROUP_TAB,
                             I_wf_custg_tl_del_rst  IN       ROW_SEQ_TAB,
                             I_process_id           IN       SVC_WF_CUSTOMER_GROUP_TL.PROCESS_ID%TYPE,
                             I_chunk_id             IN       SVC_WF_CUSTOMER_GROUP_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_WF_CUSTOMER.EXEC_WFCUSTG_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WF_CUSTOMER_GROUP_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_WFCUSTG_TL_DEL(I_wf_customer_group_id  WF_CUSTOMER_GROUP_TL.WF_CUSTOMER_GROUP_ID%TYPE,
                                I_lang                  WF_CUSTOMER_GROUP_TL.LANG%TYPE) is
      select 'x'
        from wf_customer_group_tl
       where wf_customer_group_id = I_wf_customer_group_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_wfcustg_TL_del_tab is NOT NULL and I_wfcustg_TL_del_tab.count > 0 then
      for i in I_wfcustg_TL_del_tab.FIRST..I_wfcustg_TL_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_wfcustg_TL_del_tab(i).lang);
            L_key_val2 := 'WF Customer Group ID: '||to_char(I_wfcustg_TL_del_tab(i).wf_customer_group_id);
            open C_LOCK_WFCUSTG_TL_DEL(I_wfcustg_TL_del_tab(i).wf_customer_group_id,
                                       I_wfcustg_TL_del_tab(i).lang);
            close C_LOCK_WFCUSTG_TL_DEL;
           
            delete wf_customer_group_tl
             where lang = I_wfcustg_TL_del_tab(i).lang
               and wf_customer_group_id = I_wfcustg_TL_del_tab(i).wf_customer_group_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_WF_CUSTOMER_GROUP_TL',
                           I_wf_custg_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_WFCUSTG_TL_DEL%ISOPEN then
         close C_LOCK_WFCUSTG_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_WFCUSTG_TL_DEL;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_WF_CUST( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id    IN     SVC_WF_CUST.PROCESS_ID%TYPE,
                          I_chunk_id      IN     SVC_WF_CUST.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      :='CORESVC_WF_CUSTOMER.PROCESS_WF_CUST';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_WF_CUST';
   L_error            BOOLEAN;
   L_process_error    BOOLEAN                           := FALSE;
   L_wf_cust_temp_rec WF_CUSTOMER%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_WF_CUST(I_process_id,
                            I_chunk_id)
   LOOP
      L_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.PK_WF_CUSTOMER_rid is NOT NULL then

         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'WF_CUSTOMER_ID',
                     'PK_WF_CUSTOMER');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.wf_customer_id is NOT NULL
         and rec.pk_wf_customer_rid is NULL then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'WF_CUSTOMER_ID',
                     'INV_CUST');
         L_error :=TRUE;
      end if;
      if rec.action = action_new
         and rec.wf_customer_group_id is NOT NULL
         and rec.wc_wcp_fk_rid is NULL then

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'WF_CUSTOMER_GROUP_ID',
                     'INV_CUST_GROUP');
         L_error :=TRUE;
      end if;
      if rec.action = action_mod
         and NVL(rec.wf_customer_group_id,1) <> NVL(rec.old_wf_customer_group_id,1) then

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'WF_CUSTOMER_GROUP_ID',
                     'INV_CUST_GROUP');
         L_error :=TRUE;
      end if;
      if NOT( upper(rec.auto_approve_ind) IN  ( 'Y','N' )  )
         and rec.auto_approve_ind is NOT NULL
         and rec.action IN (action_new,action_mod) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'AUTO_APPROVE_IND',
                     'INV_Y_N_IND');
         L_error :=TRUE;
      end if;
      if NOT(upper(rec.credit_ind) IN  ( 'Y','N' )  )
         and rec.credit_ind is NOT NULL
         and rec.action IN (action_new,action_mod) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CREDIT_IND',
                     'INV_Y_N_IND');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_new,action_mod)
         and rec.wf_customer_name is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'WF_CUSTOMER_NAME',
                     'ENTER_DESC');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_new,action_mod)
         and rec.credit_ind  is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CREDIT_IND',
                     'ENTER_CREDIT_IND');
         L_error :=TRUE;
      end if;
      if rec.action=action_new
         and rec.wf_customer_group_id is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'WF_CUSTOMER_GROUP_ID',
                     'ENTER_CUST_GRP_ID');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_new,action_mod)
         and rec.auto_approve_ind  is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'AUTO_APPROVE_IND',
                     'ENTER_AUTO_APPR_IND');
         L_error :=TRUE;
      end if;
      if PROCESS_WF_CUST_VAL(O_error_message,
                             L_error,
                             rec)=FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_wf_cust_temp_rec.wf_customer_id          := rec.wf_customer_id;
         L_wf_cust_temp_rec.wf_customer_name        := rec.wf_customer_name;
         L_wf_cust_temp_rec.credit_ind              := rec.credit_ind;
         L_wf_cust_temp_rec.wf_customer_group_id    := rec.wf_customer_group_id;
         L_wf_cust_temp_rec.auto_approve_ind        := rec.auto_approve_ind;
         L_wf_cust_temp_rec.create_id               := GET_USER;
         L_wf_cust_temp_rec.create_datetime         := SYSDATE;
         if rec.action = action_new then
            if EXEC_WF_CUST_INS( O_error_message,
                                 L_wf_cust_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_WF_CUST_UPD( O_error_message,
                                 L_wf_cust_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_WF_CUST_DEL( O_error_message,
                                 L_wf_cust_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;

   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_WF_CUST;
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_WFCUSTG(  O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id    IN     SVC_WF_CUST_GRP.PROCESS_ID%TYPE,
                           I_chunk_id      IN     SVC_WF_CUST_GRP.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_WF_CUSTOMER.PROCESS_WFCUSTG';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WF_CUST_GRP';
   L_error            BOOLEAN;
   L_process_error    BOOLEAN                           := FALSE;
   L_wfcustg_temp_rec WF_CUSTOMER_GROUP%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_WFCUSTG(I_process_id,
                            I_chunk_id)
   LOOP
      L_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.pk_wf_customer_group_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'WF_CUSTOMER_GROUP_ID',
                     'CUST_GROUP_ID_EXISTS');
         L_error := TRUE;
      end if;
      if rec.action = action_mod
         and rec.wf_customer_group_id is NOT NULL
         and rec.pk_wf_customer_group_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'WF_CUSTOMER_GROUP_ID',
                     'INV_CUST_GROUP');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_new)
         and rec.wf_customer_group_name  IS NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                    'WF_CUSTOMER_GROUP_NAME',
                    'FCUST_CUSTOMER_NAME_REQD');
         L_error := TRUE;
      end if;

      if PROCESS_WF_CUST_GRP_VAL(O_error_message,
                                 L_error,
                                 rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_wfcustg_temp_rec.wf_customer_group_id   := rec.wf_customer_group_id;
         L_wfcustg_temp_rec.wf_customer_group_name := rec.wf_customer_group_name;
         L_wfcustg_temp_rec.create_id              := GET_USER;
         L_wfcustg_temp_rec.create_datetime        := SYSDATE;
         if rec.action = action_new then
            if EXEC_WFCUSTG_INS( O_error_message,
                                 L_wfcustg_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_WFCUSTG_UPD( O_error_message,
                                 L_wfcustg_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_WFCUSTG;
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_WFCUSTG_DEL(  O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id    IN     SVC_WF_CUST_GRP.PROCESS_ID%TYPE,
                               I_chunk_id      IN     SVC_WF_CUST_GRP.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_WF_CUSTOMER.PROCESS_WFCUSTG_DEL';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WF_CUST_GRP';
   L_error            BOOLEAN;
   L_process_error    BOOLEAN                           := FALSE;
   L_wfcustg_temp_rec WF_CUSTOMER_GROUP%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_WFCUSTG_DEL(I_process_id,
                                I_chunk_id)
   LOOP
      L_error       := FALSE;
      if rec.wf_customer_group_id is NOT NULL
         and rec.pk_wf_customer_group_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'WF_CUSTOMER_GROUP_ID',
                     'INV_CUST_GROUP');
         L_error := TRUE;
      end if;

      if PROCESS_WF_CUST_GRP_VAL(O_error_message,
                                 L_error,
                                 rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_wfcustg_temp_rec.wf_customer_group_id   := rec.wf_customer_group_id;
         L_wfcustg_temp_rec.wf_customer_group_name := rec.wf_customer_group_name;
         L_wfcustg_temp_rec.create_id              := GET_USER;
         L_wfcustg_temp_rec.create_datetime        := SYSDATE;

         if rec.action = action_del then
            if EXEC_WFCUSTG_DEL( O_error_message,
                                 L_wfcustg_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_WFCUSTG_DEL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_WF_CUSTOMER_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN       SVC_WF_CUSTOMER_TL.PROCESS_ID%TYPE,
                                I_chunk_id        IN       SVC_WF_CUSTOMER_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_WF_CUSTOMER.PROCESS_WF_CUSTOMER_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WF_CUSTOMER_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WF_CUSTOMER';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WF_CUSTOMER_TL';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_wf_customer_tl_temp_rec       WF_CUSTOMER_TL%ROWTYPE;
   L_wf_customer_tl_upd_rst        ROW_SEQ_TAB;
   L_wf_customer_tl_del_rst        ROW_SEQ_TAB;

   cursor C_SVC_WF_CUSTOMER_TL(I_process_id NUMBER,
                               I_chunk_id NUMBER) is
      select pk_wf_customer_tl.rowid  as pk_wf_customer_tl_rid,
             fk_wf_customer.rowid     as fk_wf_customer_rid,
             fk_lang.rowid            as fk_lang_rid,
             st.lang,
             st.wf_customer_id,
             st.wf_customer_name,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_wf_customer_tl  st,
             wf_customer         fk_wf_customer,
             wf_customer_tl      pk_wf_customer_tl,
             lang               fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.wf_customer_id  =  fk_wf_customer.wf_customer_id (+)
         and st.lang        =  pk_wf_customer_tl.lang (+)
         and st.wf_customer_id  =  pk_wf_customer_tl.WF_CUSTOMER_ID (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_WF_CUSTOMER_TL is TABLE OF C_SVC_WF_CUSTOMER_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_wf_customer_tab        SVC_WF_CUSTOMER_TL;

   L_wf_customer_tl_ins_tab         WF_CUSTOMER_TAB         := NEW WF_CUSTOMER_TAB();
   L_wf_customer_tl_upd_tab         WF_CUSTOMER_TAB         := NEW WF_CUSTOMER_TAB();
   L_wf_customer_tl_del_tab         WF_CUSTOMER_TAB         := NEW WF_CUSTOMER_TAB();

BEGIN
   if C_SVC_WF_CUSTOMER_TL%ISOPEN then
      close C_SVC_WF_CUSTOMER_TL;
   end if;

   open C_SVC_WF_CUSTOMER_TL(I_process_id,
                             I_chunk_id);
   LOOP
      fetch C_SVC_WF_CUSTOMER_TL bulk collect into L_svc_wf_customer_tab limit LP_bulk_fetch_limit;
      if L_svc_wf_customer_tab.COUNT > 0 then
         FOR i in L_svc_wf_customer_tab.FIRST..L_svc_wf_customer_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_wf_customer_tab(i).action is NULL
               or L_svc_wf_customer_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wf_customer_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_wf_customer_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wf_customer_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_wf_customer_tab(i).action = action_new
               and L_svc_wf_customer_tab(i).pk_wf_customer_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wf_customer_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_wf_customer_tab(i).action IN (action_mod, action_del)
               and L_svc_wf_customer_tab(i).lang is NOT NULL
               and L_svc_wf_customer_tab(i).wf_customer_id is NOT NULL
               and L_svc_wf_customer_tab(i).pk_wf_customer_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_wf_customer_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_wf_customer_tab(i).action = action_new
               and L_svc_wf_customer_tab(i).wf_customer_id is NOT NULL
               and L_svc_wf_customer_tab(i).fk_wf_customer_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_wf_customer_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_wf_customer_tab(i).action = action_new
               and L_svc_wf_customer_tab(i).lang is NOT NULL
               and L_svc_wf_customer_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wf_customer_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_wf_customer_tab(i).action in (action_new, action_mod) then
               if L_svc_wf_customer_tab(i).wf_customer_name is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_wf_customer_tab(i).row_seq,
                              'WF_CUSTOMER_NAME',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_wf_customer_tab(i).wf_customer_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wf_customer_tab(i).row_seq,
                           'WF_CUSTOMER_ID',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_wf_customer_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wf_customer_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_wf_customer_tl_temp_rec.lang := L_svc_wf_customer_tab(i).lang;
               L_wf_customer_tl_temp_rec.wf_customer_id := L_svc_wf_customer_tab(i).wf_customer_id;
               L_wf_customer_tl_temp_rec.wf_customer_name := L_svc_wf_customer_tab(i).wf_customer_name;
               L_wf_customer_tl_temp_rec.create_datetime := SYSDATE;
               L_wf_customer_tl_temp_rec.create_id := GET_USER;
               L_wf_customer_tl_temp_rec.last_update_datetime := SYSDATE;
               L_wf_customer_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_wf_customer_tab(i).action = action_new then
                  L_wf_customer_tl_ins_tab.extend;
                  L_wf_customer_tl_ins_tab(L_wf_customer_tl_ins_tab.count()) := L_wf_customer_tl_temp_rec;
               end if;

               if L_svc_wf_customer_tab(i).action = action_mod then
                  L_wf_customer_tl_upd_tab.extend;
                  L_wf_customer_tl_upd_tab(L_wf_customer_tl_upd_tab.count()) := L_wf_customer_tl_temp_rec;
                  L_wf_customer_tl_upd_rst(L_wf_customer_tl_upd_tab.count()) := L_svc_wf_customer_tab(i).row_seq;
               end if;

               if L_svc_wf_customer_tab(i).action = action_del then
                  L_wf_customer_tl_del_tab.extend;
                  L_wf_customer_tl_del_tab(L_wf_customer_tl_del_tab.count()) := L_wf_customer_tl_temp_rec;
                  L_wf_customer_tl_del_rst(L_wf_customer_tl_del_tab.count()) := L_svc_wf_customer_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_WF_CUSTOMER_TL%NOTFOUND;
   END LOOP;
   close C_SVC_WF_CUSTOMER_TL;

   if EXEC_WF_CUST_TL_INS(O_error_message,
                          L_wf_customer_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_WF_CUST_TL_UPD(O_error_message,
                          L_wf_customer_tl_upd_tab,
                          L_wf_customer_tl_upd_rst,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_WF_CUST_TL_DEL(O_error_message,
                          L_wf_customer_tl_del_tab,
                          L_wf_customer_tl_del_rst,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_WF_CUSTOMER_TL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_WF_CUST_GROUP_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id      IN       SVC_WF_CUSTOMER_GROUP_TL.PROCESS_ID%TYPE,
                                  I_chunk_id        IN       SVC_WF_CUSTOMER_GROUP_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_WF_CUSTOMER.PROCESS_WF_CUST_GROUP_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WF_CUSTOMER_GROUP_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WF_CUSTOMER_GROUP';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WF_CUSTOMER_GROUP_TL';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_wf_custg_tl_temp_rec    WF_CUSTOMER_GROUP_TL%ROWTYPE;
   L_wf_custg_tl_upd_rst     ROW_SEQ_TAB;
   L_wf_custg_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_WF_CUSTOMER_GROUP_TL(I_process_id NUMBER,
                                     I_chunk_id NUMBER) is
      select pk_wf_customer_group_tl.rowid  as pk_wf_customer_group_tl_rid,
             fk_wf_customer_group.rowid     as fk_wf_customer_group_rid,
             fk_lang.rowid                  as fk_lang_rid,
             st.lang,
             st.wf_customer_group_id,
             st.wf_customer_group_name,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_wf_customer_group_tl  st,
             wf_customer_group         fk_wf_customer_group,
             wf_customer_group_tl      pk_wf_customer_group_tl,
             lang          fk_lang
       where st.process_id            =  I_process_id
         and st.chunk_id              =  I_chunk_id
         and st.wf_customer_group_id  =  fk_wf_customer_group.wf_customer_group_id (+)
         and st.lang                  =  pk_wf_customer_group_tl.lang (+)
         and st.wf_customer_group_id  =  pk_wf_customer_group_tl.wf_customer_group_id (+)
         and st.lang                  =  fk_lang.lang (+);

   TYPE SVC_WF_CUSTOMER_GROUP_TL is TABLE OF C_SVC_WF_CUSTOMER_GROUP_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_wf_customer_group_tab        SVC_WF_CUSTOMER_GROUP_TL;

   L_wf_customer_group_tl_ins_tab         WF_CUSTOMER_GROUP_TAB         := NEW WF_CUSTOMER_GROUP_TAB();
   L_wf_customer_group_tl_upd_tab         WF_CUSTOMER_GROUP_TAB         := NEW WF_CUSTOMER_GROUP_TAB();
   L_wf_customer_group_tl_del_tab         WF_CUSTOMER_GROUP_TAB         := NEW WF_CUSTOMER_GROUP_TAB();

BEGIN
   if C_SVC_WF_CUSTOMER_GROUP_TL%ISOPEN then
      close C_SVC_WF_CUSTOMER_GROUP_TL;
   end if;

   open C_SVC_WF_CUSTOMER_GROUP_TL(I_process_id,
                                   I_chunk_id);
   LOOP
      fetch C_SVC_WF_CUSTOMER_GROUP_TL bulk collect into L_svc_wf_customer_group_tab limit LP_bulk_fetch_limit;
      if L_svc_wf_customer_group_tab.COUNT > 0 then
         FOR i in L_svc_wf_customer_group_tab.FIRST..L_svc_wf_customer_group_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_wf_customer_group_tab(i).action is NULL
               or L_svc_wf_customer_group_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wf_customer_group_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_wf_customer_group_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wf_customer_group_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_wf_customer_group_tab(i).action = action_new
               and L_svc_wf_customer_group_tab(i).pk_wf_customer_group_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wf_customer_group_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_wf_customer_group_tab(i).action IN (action_mod, action_del)
               and L_svc_wf_customer_group_tab(i).lang is NOT NULL
               and L_svc_wf_customer_group_tab(i).wf_customer_group_id is NOT NULL
               and L_svc_wf_customer_group_tab(i).pk_wf_customer_group_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_wf_customer_group_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_wf_customer_group_tab(i).action = action_new
               and L_svc_wf_customer_group_tab(i).wf_customer_group_id is NOT NULL
               and L_svc_wf_customer_group_tab(i).fk_wf_customer_group_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_wf_customer_group_tab(i).row_seq,
                            NULL,
                            'INV_CUST_GROUP');
               L_error :=TRUE;
            end if;

            if L_svc_wf_customer_group_tab(i).action = action_new
               and L_svc_wf_customer_group_tab(i).lang is NOT NULL
               and L_svc_wf_customer_group_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wf_customer_group_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_wf_customer_group_tab(i).action in (action_new, action_mod) then
               if L_svc_wf_customer_group_tab(i).wf_customer_group_name is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_wf_customer_group_tab(i).row_seq,
                              'WF_CUSTOMER_GROUP_NAME',
                              'FCUST_GRP_NAME_REQD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_wf_customer_group_tab(i).wf_customer_group_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wf_customer_group_tab(i).row_seq,
                           'WF_CUSTOMER_GROUP_ID',
                           'FCUST_GRP_ID_REQD');
               L_error :=TRUE;
            end if;

            if L_svc_wf_customer_group_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wf_customer_group_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_wf_custg_tl_temp_rec.lang := L_svc_wf_customer_group_tab(i).lang;
               L_wf_custg_tl_temp_rec.wf_customer_group_id := L_svc_wf_customer_group_tab(i).wf_customer_group_id;
               L_wf_custg_tl_temp_rec.wf_customer_group_name := L_svc_wf_customer_group_tab(i).wf_customer_group_name;
               L_wf_custg_tl_temp_rec.create_datetime := SYSDATE;
               L_wf_custg_tl_temp_rec.create_id := GET_USER;
               L_wf_custg_tl_temp_rec.last_update_datetime := SYSDATE;
               L_wf_custg_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_wf_customer_group_tab(i).action = action_new then
                  L_wf_customer_group_tl_ins_tab.extend;
                  L_wf_customer_group_tl_ins_tab(L_wf_customer_group_tl_ins_tab.count()) := L_wf_custg_tl_temp_rec;
               end if;

               if L_svc_wf_customer_group_tab(i).action = action_mod then
                  L_wf_customer_group_tl_upd_tab.extend;
                  L_wf_customer_group_tl_upd_tab(L_wf_customer_group_tl_upd_tab.count()) := L_wf_custg_tl_temp_rec;
                  L_wf_custg_tl_upd_rst(L_wf_customer_group_tl_upd_tab.count()) := L_svc_wf_customer_group_tab(i).row_seq;
               end if;

               if L_svc_wf_customer_group_tab(i).action = action_del then
                  L_wf_customer_group_tl_del_tab.extend;
                  L_wf_customer_group_tl_del_tab(L_wf_customer_group_tl_del_tab.count()) := L_wf_custg_tl_temp_rec;
                  L_wf_custg_tl_del_rst(L_wf_customer_group_tl_del_tab.count()) := L_svc_wf_customer_group_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_WF_CUSTOMER_GROUP_TL%NOTFOUND;
   END LOOP;
   close C_SVC_WF_CUSTOMER_GROUP_TL;

   if EXEC_WFCUSTG_TL_INS(O_error_message,
                          L_wf_customer_group_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_WFCUSTG_TL_UPD(O_error_message,
                          L_wf_customer_group_tl_upd_tab,
                          L_wf_custg_tl_upd_rst,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_WFCUSTG_TL_DEL(O_error_message,
                          L_wf_customer_group_tl_del_tab,
                          L_wf_custg_tl_del_rst,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_WF_CUST_GROUP_TL;
------------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete
     from svc_wf_customer_tl
    where process_id = I_process_id;

   delete
     from svc_wf_cust
    where process_id = I_process_id;
    
   delete
     from svc_wf_customer_group_tl
    where process_id = I_process_id;

   delete
     from svc_wf_cust_grp
    where process_id = I_process_id;
END;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count   IN OUT NUMBER,
                  I_process_id    IN     NUMBER,
                  I_chunk_id      IN     NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    :='CORESVC_WF_CUSTOMER.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_WFCUSTG(O_error_message,
                      I_process_id,
                      I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_WF_CUST_GROUP_TL(O_error_message,
                               I_process_id,
                               I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_WF_CUST(O_error_message,
                      I_process_id,
                      I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_WF_CUSTOMER_TL(O_error_message,
                             I_process_id,
                             I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_WFCUSTG_DEL(O_error_message,
                          I_process_id,
                          I_chunk_id)=FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
          action_date = SYSDATE
   where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
------------------------------------------------------------------------------------------------------
END CORESVC_WF_CUSTOMER;
/