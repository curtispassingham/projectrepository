CREATE OR REPLACE PACKAGE BODY WF_TRANSFER_SQL AS

   LP_sdate                 PERIOD.VDATE%TYPE   := SYSDATE;
   LP_vdate                 PERIOD.VDATE%TYPE   := GET_VDATE;
   LP_user                  VARCHAR2(50)        := GET_USER;
   LP_table                 VARCHAR2(30);
   
   cursor C_LOCK_TSFHEAD_CFA_EXT (CV_tsf_no TSFHEAD.TSF_NO%TYPE) is
      select 'x'
        from tsfhead_cfa_ext
       where tsf_no   = CV_tsf_no
         for update nowait;

   cursor C_LOCK_TSFHEAD (CV_tsf_no TSFHEAD.TSF_NO%TYPE) is
      select 'x'
        from tsfhead
       where tsf_no   = CV_tsf_no
         for update nowait;

   cursor C_LOCK_TSFDETAIL (CV_tsf_no TSFHEAD.TSF_NO%TYPE) is
      select 'x'
        from tsfdetail
       where tsf_no   = CV_tsf_no
         for update nowait;
         
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked, -54);
--------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
--------------------------------------------------------------------------------------------------
FUNCTION WF_CREATE_TSF_HEAD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tsf_no          IN OUT   TSFHEAD.TSF_NO%TYPE,
                             I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                             I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                             I_to_loc_type     IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                             I_to_loc          IN       TSFHEAD.TO_LOC%TYPE,
                             I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE,
                             I_need_date       IN       TSFHEAD.DELIVERY_DATE%TYPE,
                             I_wf_order_no     IN       TSFHEAD.WF_ORDER_NO%TYPE,
                             I_rma_no          IN       TSFHEAD.RMA_NO%TYPE,
                             I_status          IN       TSFHEAD.STATUS%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)  := 'WF_TRANSFER_SQL.WF_CREATE_TSF_HEAD';
   L_inv_parm             VARCHAR2(30)  := NULL;
   L_valid                BOOLEAN;
   L_wh_name              WH.WH_NAME%TYPE;
   L_store_name           STORE.STORE_NAME%TYPE;
   L_exist_store          BOOLEAN;
   L_store_row            STORE%ROWTYPE;
   L_return_code          VARCHAR2(5);
   L_delivery_date        TSFHEAD.DELIVERY_DATE%TYPE DEFAULT NULL;
   L_comp_loc_type        WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_not_after_date       TSFHEAD.NOT_AFTER_DATE%TYPE;
   
   cursor C_GET_ORD_DETAILS IS
      select wod.not_after_date
        from wf_order_detail wod
       where wod.wf_order_no=I_wf_order_no;
   

BEGIN

   if I_from_loc_type is NULL then
      L_inv_parm := 'I_from_loc_type';
   elsif I_from_loc is NULL then
      L_inv_parm := 'I_from_loc';
   elsif I_to_loc_type is NULL then
      L_inv_parm := 'I_to_loc_type';
   elsif I_to_loc is NULL then
      L_inv_parm := 'I_to_loc';
   elsif I_tsf_type is NULL then
      L_inv_parm := 'I_tsf_type';
   elsif I_status is NULL then
      L_inv_parm := 'I_status';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   
   if I_wf_order_no is NULL and I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_EITHER',
                                             'I_wf_order_no',
                                             'I_rma_no',
                                             L_program);
      return FALSE;
   end if;

   if I_tsf_type not in ('FO', 'FR') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER');
      return FALSE;
   end if;

   if I_tsf_type = 'FO' then
      if I_from_loc_type = 'WH' then
         if WH_ATTRIB_SQL.GET_NAME(O_error_message,
                                   I_from_loc,
                                   L_wh_name) = FALSE then
            return FALSE;
         end if;
      elsif I_from_loc_type = 'ST' then
         if STORE_ATTRIB_SQL.GET_NAME(O_error_message,
                                      I_from_loc,
                                      L_store_name) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                  L_exist_store,
                                  L_store_row,
                                  I_to_loc) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exist_store = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      -- Check if the input store is a valid F store.
      elsif L_store_row.store_type != 'F' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_WF_STORE');
         return FALSE;
      end if;   
   elsif I_tsf_type = 'FR' then
      if I_from_loc_type = 'WH' then
         if WH_ATTRIB_SQL.GET_NAME(O_error_message,
                                   I_to_loc,
                                   L_wh_name) = FALSE then
            return FALSE;
         end if;
      elsif I_from_loc_type = 'ST' then
         if STORE_ATTRIB_SQL.GET_NAME(O_error_message,
                                      I_to_loc,
                                      L_store_name) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                  L_exist_store,
                                  L_store_row,
                                  I_from_loc) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exist_store = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      -- Check if the input store is a valid F store.
      elsif L_store_row.store_type != 'F' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_WF_STORE');
         return FALSE;
      end if;   
   end if;

   if I_tsf_type = 'FO' then 
      if I_from_loc_type = 'S' then
         L_comp_loc_type := 'ST';
      else
         L_comp_loc_type := 'WH';
      end if;
	
      if NOT WF_ORDER_SQL.WF_ORDER_EXISTS(O_error_message,
                                          L_valid,
                                          I_to_loc,
                                          L_comp_loc_type,
                                          I_from_loc,
                                          I_need_date) or NOT L_valid then
         O_error_message := SQL_LIB.CREATE_MSG('INV_NEED_DATE');
         return FALSE;
      end if;
      if NOT WF_ORDER_SQL.WF_ORDER_EXISTS(O_error_message,
                                          L_valid,
                                          I_wf_order_no) or NOT L_valid then
         O_error_message := SQL_LIB.CREATE_MSG('INV_EXT_REF_NO');
         return FALSE;
      end if;
      L_delivery_date := I_need_date;
   end if;

   if I_tsf_type = 'FR' then
      if NOT WF_RETURN_SQL.VALIDATE_RMA_NUMBER(O_error_message,
                                               L_valid,
                                               I_rma_no) or NOT L_valid then
         O_error_message := SQL_LIB.CREATE_MSG('INV_EXT_REF_NO');
         return FALSE;
      end if;
   end if;
   
    open C_GET_ORD_DETAILS;
    fetch C_GET_ORD_DETAILS into L_not_after_date;
    close C_GET_ORD_DETAILS;

   NEXT_TRANSFER_NUMBER(O_tsf_no,
                        L_return_code,
                        O_error_message);

   insert into tsfhead(tsf_no,
                       tsf_parent_no,
                       from_loc_type,
                       from_loc,
                       to_loc_type,
                       to_loc,
                       exp_dc_date,
                       dept,
                       tsf_type,
                       status,
                       freight_code,
                       routing_code,
                       create_date,
                       create_id,
                       approval_date,
                       approval_id,
                       delivery_date,
                       close_date,
                       wf_order_no,
                       rma_no,
                       repl_tsf_approve_ind,
                       comment_desc,
                       inventory_type,
                       wf_need_date,
                       delivery_slot_id,
		       not_after_date)
                values(O_tsf_no,
                       NULL,
                       I_from_loc_type,
                       I_from_loc,
                       I_to_loc_type,
                       I_to_loc,
                       NULL,
                       NULL,
                       I_tsf_type,
                       I_status,
                       'N',
                       NULL,
                       sysdate,
                       LP_user,
                       sysdate,
                       LP_user,
                       L_delivery_date,
                       NULL,
                       I_wf_order_no,
                       I_rma_no,
                       'N',
                       NULL,
                       'A',
                       L_delivery_date,
                       NULL,
		       L_not_after_date);
                       
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END WF_CREATE_TSF_HEAD;
-----------------------------------------------------------------------------------
FUNCTION WF_CREATE_TSF_DETAIL (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE,
                               I_qty             IN       TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64)  := 'WF_TRANSFER_SQL.WF_CREATE_TSF_DETAIL';
   L_inv_parm           VARCHAR2(30)  := NULL;
   L_status             TSFHEAD.STATUS%TYPE;
   L_from_loc           TSFHEAD.FROM_LOC%TYPE;
   L_from_loc_type      TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_to_loc             TSFHEAD.TO_LOC%TYPE;
   L_to_loc_type        TSFHEAD.TO_LOC_TYPE%TYPE;
   L_exist              VARCHAR2(1) DEFAULT 'N';
   L_supp_pack_size     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_tsf_seq_no         TSFDETAIL.TSF_SEQ_NO%TYPE;

   cursor C_GET_TRANSFER is
      select status, from_loc, from_loc_type, to_loc, to_loc_type
        from tsfhead
       where tsf_no = I_tsf_no
         and status in ('A','S','P','L','I','B');

   cursor C_MAX_SEQ is
      select NVL(MAX(tsf_seq_no),0) + 1
        from tsfdetail
       where tsf_no = I_tsf_no;

BEGIN

   if I_tsf_no is NULL then
      L_inv_parm := 'I_tsf_no';
   elsif I_item is NULL then
      L_inv_parm := 'I_item';
   elsif I_qty is NULL then
      L_inv_parm := 'I_qty';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_GET_TRANSFER;
   fetch C_GET_TRANSFER into L_status, L_from_loc, L_from_loc_type, L_to_loc, L_to_loc_type;
   if C_GET_TRANSFER%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER');
      close C_GET_TRANSFER;
      return FALSE;
   end if;
   close C_GET_TRANSFER;

   if I_qty <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('QTY_GREATER_ZERO');
      return FALSE;
   end if;

   if SUPP_ITEM_ATTRIB_SQL.GET_SUPP_PACK_SIZE(O_error_message,
                                              L_supp_pack_size,
                                              I_item,
                                              NULL,
                                              NULL) = FALSE then
      return FALSE;
   end if;

   open C_MAX_SEQ;
   fetch C_MAX_SEQ into L_tsf_seq_no;
   close C_MAX_SEQ;

   insert into tsfdetail(tsf_no,
                         tsf_seq_no,
                         item,
                         inv_status,
                         tsf_price,
                         tsf_qty,
                         fill_qty,
                         ship_qty,
                         received_qty,
                         reconciled_qty,
                         distro_qty,
                         selected_qty,
                         cancelled_qty,
                         supp_pack_size,
                         tsf_po_link_no,
                         mbr_processed_ind,
                         publish_ind,
                         updated_by_rms_ind)
                  values(I_tsf_no,
                         L_tsf_seq_no,
                         I_item,
                         NULL,
                         NULL,
                         I_qty,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         L_supp_pack_size,
                         NULL,
                         NULL,
                         'N',
                         'Y');

   -- If the transfer is in 'A'pproved status, update item_loc_soh table for the reserved_qty and expected_qty.
   if L_status = 'A' then
      if WF_TRANSFER_SQL.WF_UPD_ITEM_RESV(O_error_message,
                                          I_item,
                                          I_qty,
                                          L_from_loc,
                                          L_from_loc_type) = FALSE then
         return FALSE;
      end if;
      
      if WF_TRANSFER_SQL.WF_UPD_ITEM_EXP(O_error_message,
                                          I_item,
                                          I_qty,
                                          L_to_loc,
                                          L_to_loc_type) = FALSE then
         return FALSE;
      end if;
      
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_TRANSFER%ISOPEN then
         close C_GET_TRANSFER;
      end if;        
      if C_MAX_SEQ%ISOPEN then
         close C_MAX_SEQ;
      end if;        
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END WF_CREATE_TSF_DETAIL;
-----------------------------------------------------------------------------------
FUNCTION WF_UPDATE_TSF_HEAD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                             I_status          IN       TSFHEAD.STATUS%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64)  := 'WF_TRANSFER_SQL.WF_UPDATE_TSF_HEAD';
   L_inv_parm           VARCHAR2(30)  := NULL;
   L_exist              VARCHAR2(1) DEFAULT 'N';

   cursor C_VALID_TRANSFER is
      select 'Y'
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_GET_TSF_DETAILS is
      select th.from_loc,
             th.from_loc_type,
             th.to_loc,
             th.to_loc_type,
             td.item,
             td.tsf_qty
        from tsfhead th,
             tsfdetail td
       where th.tsf_no = I_tsf_no
         and td.tsf_no = th.tsf_no;

   TYPE ils_tsf_TBL is TABLE of C_GET_TSF_DETAILS%ROWTYPE INDEX BY BINARY_INTEGER;   
   L_ils_tsf_tbl      ils_tsf_TBL;
          
BEGIN

   if I_tsf_no is NULL then
      L_inv_parm := 'I_tsf_no';
   elsif I_status is NULL then
      L_inv_parm := 'I_status';
   end if;
      
   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'L_inv_parm',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   
   open C_VALID_TRANSFER;
   fetch C_VALID_TRANSFER into L_exist;
   close C_VALID_TRANSFER;
   
   if L_exist != 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER');
      return FALSE;
   end if;

   LP_table := 'tsfhead';
   
   open C_LOCK_TSFHEAD(I_tsf_no);
   close C_LOCK_TSFHEAD;
   
   update tsfhead
      set status = I_status,
          approval_date = DECODE(I_status,'A',sysdate,approval_date),
          approval_id = DECODE(I_status,'A',LP_user,approval_id)
    where tsf_no = I_tsf_no;
    
   if I_status = 'A' then
     
      open C_GET_TSF_DETAILS;
      fetch C_GET_TSF_DETAILS BULK COLLECT into L_ils_tsf_tbl;
      close C_GET_TSF_DETAILS;
     
      if L_ils_tsf_tbl is not NULL and L_ils_tsf_tbl.COUNT > 0 then   
         FOR i in L_ils_tsf_tbl.first..L_ils_tsf_tbl.last LOOP
            if WF_TRANSFER_SQL.WF_UPD_ITEM_RESV(O_error_message,
                                                L_ils_tsf_tbl(i).item,
                                                L_ils_tsf_tbl(i).tsf_qty,
                                                L_ils_tsf_tbl(i).from_loc,
                                                L_ils_tsf_tbl(i).from_loc_type) = FALSE then
               return FALSE;
            end if;
            
            if WF_TRANSFER_SQL.WF_UPD_ITEM_EXP(O_error_message,
                                                L_ils_tsf_tbl(i).item,
                                                L_ils_tsf_tbl(i).tsf_qty,
                                                L_ils_tsf_tbl(i).to_loc,
                                                L_ils_tsf_tbl(i).to_loc_type) = FALSE then
               return FALSE;
            end if;
         END LOOP;
      end if;
   
   end if;
   
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_VALID_TRANSFER%ISOPEN then
         close C_VALID_TRANSFER;
      end if;      
      if C_GET_TSF_DETAILS%ISOPEN then
         close C_GET_TSF_DETAILS;
      end if;      
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_tsf_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      if C_VALID_TRANSFER%ISOPEN then
         close C_VALID_TRANSFER;
      end if;      
      if C_GET_TSF_DETAILS%ISOPEN then
         close C_GET_TSF_DETAILS;
      end if;      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END WF_UPDATE_TSF_HEAD;
-----------------------------------------------------------------------------------
FUNCTION WF_UPDATE_TSF_DETAIL (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE,
                               I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64)  := 'WF_TRANSFER_SQL.WF_UPDATE_TSF_DETAIL';
   L_inv_parm           VARCHAR2(30)  := NULL;
   L_exist_item         VARCHAR2(1) DEFAULT 'N';
   L_status             TSFHEAD.STATUS%TYPE := '';
   L_from_loc           TSFHEAD.FROM_LOC%TYPE;
   L_from_loc_type      TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_to_loc             TSFHEAD.TO_LOC%TYPE;
   L_to_loc_type        TSFHEAD.TO_LOC_TYPE%TYPE;
   L_old_tsf_qty        TSFDETAIL.TSF_QTY%TYPE := 0;
   L_qty_to_resv        TSFDETAIL.TSF_QTY%TYPE := 0;
   
   cursor C_GET_TRANSFER is
      select status, 
             from_loc, 
             from_loc_type, 
             to_loc, 
             to_loc_type
        from tsfhead
       where tsf_no = I_tsf_no;
       
   cursor C_VALID_TRANSFER_ITEM is
      select 'Y', 
             tsf_qty
        from tsfdetail
       where tsf_no = I_tsf_no
         and item = I_item;
    
BEGIN

   if I_tsf_no is NULL then
      L_inv_parm := 'I_tsf_no';
   elsif I_item is NULL then
      L_inv_parm := 'I_item';
   elsif I_tsf_qty is NULL then
      L_inv_parm := 'I_tsf_qty';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   
   open C_GET_TRANSFER;
   fetch C_GET_TRANSFER into L_status, L_from_loc, L_from_loc_type, L_to_loc, L_to_loc_type;
   close C_GET_TRANSFER;
   
   if L_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER');
      return FALSE;
   end if;
   
   open C_VALID_TRANSFER_ITEM;
   fetch C_VALID_TRANSFER_ITEM into L_exist_item, L_old_tsf_qty;
   close C_VALID_TRANSFER_ITEM;
   
   if L_exist_item != 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_ITEM');
      return FALSE;
   end if;
   
   LP_table := 'tsfdetail';
   open C_LOCK_TSFDETAIL(I_tsf_no);
   close C_LOCK_TSFDETAIL;
   update tsfdetail
      set tsf_qty = I_tsf_qty,
          updated_by_rms_ind = 'Y'
    where tsf_no  = I_tsf_no
      and item    = I_item;

   if L_status in ('A','P','S','L') then
      L_qty_to_resv := I_tsf_qty - L_old_tsf_qty;
      if WF_TRANSFER_SQL.WF_UPD_ITEM_RESV (O_error_message,
                                           I_item,   
                                           L_qty_to_resv,   
                                           L_from_loc,
                                           L_from_loc_type) = FALSE then
         return FALSE;
      end if;

      if WF_TRANSFER_SQL.WF_UPD_ITEM_EXP (O_error_message,
                                          I_item,
                                          L_qty_to_resv,   
                                          L_to_loc,
                                          L_to_loc_type) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_GET_TRANSFER%ISOPEN then
         close C_GET_TRANSFER;
      end if;        
      if C_VALID_TRANSFER_ITEM%ISOPEN then
         close C_VALID_TRANSFER_ITEM;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_tsf_no),
                                            (I_item));
      return FALSE;
   when OTHERS then
      if C_GET_TRANSFER%ISOPEN then
         close C_GET_TRANSFER;
      end if;        
      if C_VALID_TRANSFER_ITEM%ISOPEN then
         close C_VALID_TRANSFER_ITEM;
      end if;       
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END WF_UPDATE_TSF_DETAIL;
-----------------------------------------------------------------------------------
FUNCTION WF_DELETE_TSF_HEAD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)

RETURN BOOLEAN IS

   L_program            VARCHAR2(64)  := 'WF_TRANSFER_SQL.WF_DELETE_TSF_HEAD';
   L_inv_parm           VARCHAR2(30)  := NULL;
   L_exist              VARCHAR2(1) DEFAULT 'N';

   cursor C_VALID_TRANSFER is
      select 'Y'
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_GET_TRANSFER is
      select status, 
             from_loc, 
             from_loc_type, 
             to_loc, 
             to_loc_type, 
             tsf_qty, 
             item
        from tsfhead th, 
             tsfdetail td
       where th.tsf_no = I_tsf_no
         and th.tsf_no = td.tsf_no;

   TYPE tsf_TBL is TABLE of C_GET_TRANSFER%ROWTYPE INDEX BY BINARY_INTEGER;   
   L_tsf_tbl      tsf_TBL;
   
BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   
   open C_VALID_TRANSFER;
   fetch C_VALID_TRANSFER into L_exist;
   close C_VALID_TRANSFER;

   if L_exist != 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER');
      return FALSE;
   end if;

   open C_GET_TRANSFER;
   fetch C_GET_TRANSFER BULK COLLECT into L_tsf_tbl;
   close C_GET_TRANSFER;

   if L_tsf_tbl is not NULL and L_tsf_tbl.COUNT > 0 then   
      if L_tsf_tbl(1).status in ('A','P','S','L') then
         FOR i in L_tsf_tbl.first..L_tsf_tbl.last LOOP
            if WF_TRANSFER_SQL.WF_UPD_ITEM_RESV (O_error_message,
                                                 L_tsf_tbl(i).item,   
                                                 (L_tsf_tbl(i).tsf_qty * -1),   
                                                 L_tsf_tbl(i).from_loc,
                                                 L_tsf_tbl(i).from_loc_type) = FALSE then
               return FALSE;
            end if;

            if WF_TRANSFER_SQL.WF_UPD_ITEM_EXP (O_error_message,
                                                 L_tsf_tbl(i).item,   
                                                 (L_tsf_tbl(i).tsf_qty * -1),   
                                                 L_tsf_tbl(i).to_loc,
                                                 L_tsf_tbl(i).to_loc_type) = FALSE then
               return FALSE;
            end if;
         END LOOP;
      end if;
   end if;
   
   LP_table := 'tsfdetail';
   
   open C_LOCK_TSFDETAIL(I_tsf_no);
   close C_LOCK_TSFDETAIL;
   
   delete from tsfdetail
    where tsf_no = I_tsf_no;

   LP_table := 'tsfhead_cfa_ext';
   
   open C_LOCK_TSFHEAD_CFA_EXT(I_tsf_no);
   close C_LOCK_TSFHEAD_CFA_EXT;
   
   delete from tsfhead_cfa_ext
    where tsf_no = I_tsf_no;

   LP_table := 'tsfhead';
   
   open C_LOCK_TSFHEAD(I_tsf_no);
   close C_LOCK_TSFHEAD;
   
   delete from tsfhead
    where tsf_no = I_tsf_no;       

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_VALID_TRANSFER%ISOPEN then
         close C_VALID_TRANSFER;
      end if;    
      if C_GET_TRANSFER%ISOPEN then
         close C_GET_TRANSFER;
      end if;    
      if C_LOCK_TSFDETAIL%ISOPEN then
         close C_LOCK_TSFDETAIL;
      end if;    
      if C_LOCK_TSFHEAD_CFA_EXT%ISOPEN then
         close C_LOCK_TSFHEAD_CFA_EXT;
      end if;    
      if C_LOCK_TSFHEAD%ISOPEN then
         close C_LOCK_TSFHEAD;
      end if;    
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_tsf_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      if C_VALID_TRANSFER%ISOPEN then
         close C_VALID_TRANSFER;
      end if;   
      if C_GET_TRANSFER%ISOPEN then
         close C_GET_TRANSFER;
      end if;    
      if C_LOCK_TSFDETAIL%ISOPEN then
         close C_LOCK_TSFDETAIL;
      end if;    
      if C_LOCK_TSFHEAD_CFA_EXT%ISOPEN then
         close C_LOCK_TSFHEAD_CFA_EXT;
      end if;    
      if C_LOCK_TSFHEAD%ISOPEN then
         close C_LOCK_TSFHEAD;
      end if;    
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END WF_DELETE_TSF_HEAD;
-----------------------------------------------------------------------------------------
FUNCTION WF_DELETE_TSF_DETAIL (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE)                              

RETURN BOOLEAN IS

   L_program            VARCHAR2(64)  := 'WF_TRANSFER_SQL.WF_DELETE_TSF_DETAIL';
   L_inv_parm           VARCHAR2(30)  := NULL;
   L_exist              VARCHAR2(1) DEFAULT 'N';
   L_exist_item         VARCHAR2(1) DEFAULT 'N';
   L_tsfdet_exist       VARCHAR2(1) DEFAULT 'N';
   L_status             TSFHEAD.STATUS%TYPE := '';
   L_from_loc           TSFHEAD.FROM_LOC%TYPE;
   L_from_loc_type      TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_to_loc             TSFHEAD.TO_LOC%TYPE;
   L_to_loc_type        TSFHEAD.TO_LOC_TYPE%TYPE;
   L_tsf_qty            TSFDETAIL.TSF_QTY%TYPE;
 
   cursor C_VALID_TRANSFER is
      select status, 
             from_loc, 
             from_loc_type, 
             to_loc, 
             to_loc_type
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_VALID_TRANSFER_ITEM is
      select 'Y', 
             tsf_qty qty
        from tsfdetail
       where tsf_no = I_tsf_no
         and item = I_item;

   cursor C_TSF_DETAIL_EXIST is
      select 'Y'
        from tsfdetail
       where tsf_no = I_tsf_no;
   
BEGIN

   if I_tsf_no is NULL then
      L_inv_parm := 'I_tsf_no';
   elsif I_item is NULL then
      L_inv_parm := 'I_item';
   end if;
   
   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   
   open C_VALID_TRANSFER;
   fetch C_VALID_TRANSFER into L_status, 
                               L_from_loc, 
                               L_from_loc_type, 
                               L_to_loc, 
                               L_to_loc_type;
   close C_VALID_TRANSFER;

   if L_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER');
      return FALSE;
   end if;

   open C_VALID_TRANSFER_ITEM;
   fetch C_VALID_TRANSFER_ITEM into L_exist_item, L_tsf_qty;
   close C_VALID_TRANSFER_ITEM;
   
   if L_exist_item != 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_ITEM');
         return FALSE;
   end if;
   
   if L_status in ('A','P','S','L') then
      if WF_TRANSFER_SQL.WF_UPD_ITEM_RESV (O_error_message,
                                           I_item,   
                                           L_tsf_qty * -1,   
                                           L_from_loc,
                                           L_from_loc_type) = FALSE then
         return FALSE;
      end if;

      if WF_TRANSFER_SQL.WF_UPD_ITEM_EXP (O_error_message,
                                          I_item,
                                          L_tsf_qty * -1,   
                                          L_to_loc,
                                          L_to_loc_type) = FALSE then
         return FALSE;
      end if;
   end if;

   LP_table := 'tsfdetail';
   
   open C_LOCK_TSFDETAIL(I_tsf_no);
   close C_LOCK_TSFDETAIL;

   delete from tsfdetail
    where tsf_no = I_tsf_no
      and item = I_item;

   open C_TSF_DETAIL_EXIST;
   fetch C_TSF_DETAIL_EXIST into L_tsfdet_exist;
   close C_TSF_DETAIL_EXIST;

   if L_tsfdet_exist != 'Y' then
      LP_table := 'tsfhead_cfa_ext';
      
      open C_LOCK_TSFHEAD_CFA_EXT(I_tsf_no);
      close C_LOCK_TSFHEAD_CFA_EXT;
      
      delete from tsfhead_cfa_ext
       where tsf_no = I_tsf_no;

      LP_table := 'tsfhead';
      
      open C_LOCK_TSFHEAD(I_tsf_no);
      close C_LOCK_TSFHEAD;
      
      delete from tsfhead
       where tsf_no = I_tsf_no;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_VALID_TRANSFER%ISOPEN then
         close C_VALID_TRANSFER;
      end if;        
      if C_VALID_TRANSFER_ITEM%ISOPEN then
         close C_VALID_TRANSFER_ITEM;
      end if;        
      if C_TSF_DETAIL_EXIST%ISOPEN then
         close C_TSF_DETAIL_EXIST;
      end if;        
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_tsf_no),
                                            I_item);
      return FALSE;
   when OTHERS then
      if C_VALID_TRANSFER%ISOPEN then
         close C_VALID_TRANSFER;
      end if;        
      if C_VALID_TRANSFER_ITEM%ISOPEN then
         close C_VALID_TRANSFER_ITEM;
      end if;        
      if C_TSF_DETAIL_EXIST%ISOPEN then
         close C_TSF_DETAIL_EXIST;
      end if;        
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END WF_DELETE_TSF_DETAIL;
-----------------------------------------------------------------------------------------
-- Internal Function called by WF_UPD_ITEM_RESV
-----------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_RESV(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE,
                       I_upd_comp_ind    IN       ITEM_MASTER.PACK_IND%TYPE,
                       I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                       I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                       I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)   := 'WF_TRANSFER_SQL.UPD_ITEM_RESV';

   cursor C_LOCK_ITEM_LOC_SOH is
      select 'x'
        from item_loc_soh
       where loc      = I_from_loc
         and loc_type = I_from_loc_type
         and item     = I_item
         for update nowait;

BEGIN

   LP_table := 'item_loc_soh';
   open C_LOCK_ITEM_LOC_SOH;
   close C_LOCK_ITEM_LOC_SOH;

   update item_loc_soh
      set tsf_reserved_qty     = decode(I_upd_comp_ind,'Y',
                                        tsf_reserved_qty,
                                        GREATEST(tsf_reserved_qty + I_tsf_qty,0)),
          pack_comp_resv       = decode(I_upd_comp_ind,'Y',
                                        GREATEST(pack_comp_resv + I_tsf_qty,0),
                                        pack_comp_resv),
          last_update_datetime = sysdate,
          last_update_id       = LP_user
    where loc                  = I_from_loc
      and loc_type             = I_from_loc_type
      and item                 = I_item
      and not exists(select 1
                       from store
                      where store = I_from_loc
                        and store_type = 'F'
                        and stockholding_ind = 'N');
 
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_ITEM_LOC_SOH%ISOPEN then
         close C_LOCK_ITEM_LOC_SOH;
      end if;        
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_from_loc),
                                            (I_item));
      return FALSE;
   when OTHERS then
      if C_LOCK_ITEM_LOC_SOH%ISOPEN then
         close C_LOCK_ITEM_LOC_SOH;
      end if;        
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPD_ITEM_RESV;
-----------------------------------------------------------------------------------------
FUNCTION WF_UPD_ITEM_RESV(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item            IN       ITEM_MASTER.ITEM%TYPE,
                          I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE,
                          I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                          I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64)  := 'WF_TRANSFER_SQL.WF_UPD_ITEM_RESV';

   L_item_qty         TSFDETAIL.TSF_QTY%TYPE;
   L_item_record      ITEM_MASTER%ROWTYPE;
   L_upd_comp_ind     VARCHAR2(1) := 'N';

   L_weight_cuom      ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_cuom             ITEM_SUPP_COUNTRY.COST_UOM%TYPE;
   L_delta_qty        TSFDETAIL.TSF_QTY%TYPE;
   
   cursor C_ITEMS_IN_PACK is
      select v.item,
             v.qty
        from v_packsku_qty v,
             item_master im
       where v.pack_no        = I_item
         and im.item          = v.item
         and im.inventory_ind = 'Y';

BEGIN

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_record,
                                      I_item) = FALSE then
      return FALSE;
   end if;
   
   if L_item_record.inventory_ind = 'N' then
      return TRUE;
   end if;

   if L_item_record.pack_ind = 'N' then
      if UPD_ITEM_RESV(O_error_message,
                       I_item,
                       'N',
                       I_from_loc,
                       I_from_loc_type,
                       I_tsf_qty) = FALSE then
         return FALSE;
      end if;
   else
      if L_item_record.simple_pack_ind = 'Y' and L_item_record.catch_weight_ind = 'Y' then
         if NOT CATCH_WEIGHT_SQL.PRORATE_WEIGHT(O_error_message,
                                                L_weight_cuom,
                                                L_cuom,
                                                I_item,
                                                I_from_loc,
                                                I_from_loc_type,
                                                NULL,
                                                NULL,
                                                I_tsf_qty,
                                                I_tsf_qty) then
            return FALSE;
         end if;
      end if;
    
      if I_from_loc_type = 'W' then  
         if UPD_ITEM_RESV(O_error_message,
                          I_item,
                          'N',
                          I_from_loc,
                          I_from_loc_type,
                          I_tsf_qty) = FALSE then
            return FALSE;
         end if;
      end if;      
      ---
      FOR rec IN C_ITEMS_IN_PACK LOOP
         
         if L_weight_cuom is NOT NULL and L_cuom is NOT NULL then
            if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                                     L_delta_qty,
                                                     rec.item,
                                                     rec.qty,
                                                     L_weight_cuom,
                                                     L_cuom) = FALSE then
               return FALSE;
            end if;
            L_item_qty := L_delta_qty;
         else
            L_item_qty := I_tsf_qty * rec.qty;
         end if;

         if I_from_loc_type = 'W' then
            L_upd_comp_ind := 'Y';
         else
            L_upd_comp_ind := 'N';
         end if;

         if UPD_ITEM_RESV(O_error_message,
                          rec.item,
                          L_upd_comp_ind,
                          I_from_loc,
                          I_from_loc_type,
                          L_item_qty) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ITEMS_IN_PACK%ISOPEN then
         close C_ITEMS_IN_PACK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END WF_UPD_ITEM_RESV;
-----------------------------------------------------------------------------------------
-- Internal Function called by WF_UPD_ITEM_EXP
-----------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_EXP( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE,
                       I_upd_comp_ind    IN       ITEM_MASTER.PACK_IND%TYPE,
                       I_to_loc          IN       TSFHEAD.TO_LOC%TYPE,
                       I_to_loc_type     IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                       I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)   := 'WF_TRANSFER_SQL.UPD_ITEM_EXP';

   cursor C_LOCK_ITEM_LOC_SOH is
      select 'x'
        from item_loc_soh
       where loc      = I_to_loc
         and loc_type = I_to_loc_type
         and item     = I_item
         for update nowait;

BEGIN

   LP_table := 'item_loc_soh';
   open C_LOCK_ITEM_LOC_SOH;
   close C_LOCK_ITEM_LOC_SOH;

   update item_loc_soh
      set tsf_expected_qty     = decode(I_upd_comp_ind,'Y',
                                        tsf_expected_qty,
                                        GREATEST(tsf_expected_qty + I_tsf_qty,0)),
          pack_comp_exp        = decode(I_upd_comp_ind,'Y',
                                        GREATEST(pack_comp_exp + I_tsf_qty,0),
                                        pack_comp_exp),
          last_update_datetime = sysdate,
          last_update_id       = LP_user
    where loc                  = I_to_loc
      and loc_type             = I_to_loc_type
      and item                 = I_item
      and not exists(select 1
                       from store
                      where store = I_to_loc
                        and store_type = 'F'
                        and stockholding_ind = 'N');

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_ITEM_LOC_SOH%ISOPEN then
         close C_LOCK_ITEM_LOC_SOH;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_to_loc),
                                            (I_item));
      return FALSE;
   when OTHERS then
      if C_LOCK_ITEM_LOC_SOH%ISOPEN then
         close C_LOCK_ITEM_LOC_SOH;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPD_ITEM_EXP;
-----------------------------------------------------------------------------------------
FUNCTION WF_UPD_ITEM_EXP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item            IN       ITEM_MASTER.ITEM%TYPE,
                         I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE,
                         I_to_loc          IN       TSFHEAD.TO_LOC%TYPE,
                         I_to_loc_type     IN       TSFHEAD.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64)  := 'WF_TRANSFER_SQL.WF_UPD_ITEM_EXP';

   L_item_qty         TSFDETAIL.TSF_QTY%TYPE;
   L_item_record      ITEM_MASTER%ROWTYPE;
   L_upd_comp_ind     VARCHAR2(1) := 'N';

   L_weight_cuom      ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_cuom             ITEM_SUPP_COUNTRY.COST_UOM%TYPE;
   L_delta_qty        TSFDETAIL.TSF_QTY%TYPE;   

   cursor C_ITEMS_IN_PACK is
      select v.item,
             v.qty
        from v_packsku_qty v,
             item_master im
       where v.pack_no        = I_item
         and im.item          = v.item
         and im.inventory_ind = 'Y';

BEGIN

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_record,
                                      I_item) = FALSE then
      return FALSE;
   end if;
   
   if L_item_record.inventory_ind = 'N' then
      return TRUE;
   end if;
   
   if L_item_record.pack_ind = 'N' then
      if UPD_ITEM_EXP(O_error_message,
                      I_item,
                      'N',     --update_comp_ind
                      I_to_loc,
                      I_to_loc_type,
                      I_tsf_qty) = FALSE then
         return FALSE;
      end if;
   else
      if L_item_record.simple_pack_ind = 'Y' and L_item_record.catch_weight_ind = 'Y' then
         if NOT CATCH_WEIGHT_SQL.PRORATE_WEIGHT(O_error_message,
                                                L_weight_cuom,
                                                L_cuom,
                                                I_item,
                                                I_to_loc,
                                                I_to_loc_type,
                                                NULL,
                                                NULL,
                                                I_tsf_qty,
                                                I_tsf_qty) then
            return FALSE;
         end if;
      end if;
  
      --update pack's inventory at warehouse
      if I_to_loc_type = 'W' then
         if UPD_ITEM_EXP(O_error_message,
                         I_item,
                         'N',     --update_comp_ind
                         I_to_loc,
                         I_to_loc_type,
                         I_tsf_qty) = FALSE then
            return FALSE;
         end if;
      end if;
      
      ---
      FOR rec IN C_ITEMS_IN_PACK LOOP
         if L_weight_cuom is NOT NULL and L_cuom is NOT NULL then
            if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                                     L_delta_qty,
                                                     rec.item,
                                                     rec.qty,
                                                     L_weight_cuom,
                                                     L_cuom) = FALSE then
               return FALSE;
            end if;
            L_item_qty := L_delta_qty;
         else
            L_item_qty := I_tsf_qty * rec.qty;
         end if;

         if I_to_loc_type = 'W' then
            L_upd_comp_ind := 'Y';
         else
            L_upd_comp_ind := 'N';
         end if;

         if UPD_ITEM_EXP(O_error_message,
                         rec.item,
                         L_upd_comp_ind,
                         I_to_loc,
                         I_to_loc_type,
                         L_item_qty) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ITEMS_IN_PACK%ISOPEN then
         close C_ITEMS_IN_PACK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END WF_UPD_ITEM_EXP;
-------------------------------------------------------------------------
FUNCTION STORE_ORDER_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists         IN OUT  BOOLEAN,
                            O_processed      IN OUT  BOOLEAN,
                            I_item           IN      STORE_ORDERS.ITEM%TYPE,
                            I_store          IN      STORE_ORDERS.STORE%TYPE,
                            I_need_date      IN      WF_ORDER_DETAIL.NEED_DATE%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)  := 'WF_TRANSFER_SQL.STORE_ORDER_EXISTS';
   L_exists    VARCHAR2(1)   := 'N';

   cursor C_STORE_ORDER_EXISTS IS
      select NVL2(processed_date,'P','Y')
        from store_orders
       where item = I_item
         and store = I_store
         and need_date = I_need_date
         and rownum = 1;

BEGIN
   O_exists    := FALSE;
   O_processed := FALSE;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_need_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_need_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_STORE_ORDER_EXISTS;
   fetch C_STORE_ORDER_EXISTS into L_exists;
   close C_STORE_ORDER_EXISTS;

   if L_exists in ('Y','P') then
      O_exists    := TRUE;
      if L_exists = 'P' then
         O_processed := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_STORE_ORDER_EXISTS%ISOPEN then
         close C_STORE_ORDER_EXISTS;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END STORE_ORDER_EXISTS;
-------------------------------------------------------------------------
FUNCTION STORE_ORDER_EXISTS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists           IN OUT  BOOLEAN,
                            O_processed        IN OUT  BOOLEAN,
                            I_wf_order_no      IN      STORE_ORDERS.WF_ORDER_NO%TYPE,
                            I_wf_order_line_no IN      STORE_ORDERS.WF_ORDER_LINE_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)  := 'WF_TRANSFER_SQL.STORE_ORDER_EXISTS';
   L_exists    VARCHAR2(1)   := 'N';

   cursor C_STORE_ORDER_EXISTS IS
      select NVL2(processed_date,'P','Y')
        from store_orders
       where wf_order_no = I_wf_order_no
         and wf_order_line_no = I_wf_order_line_no
         and rownum = 1;

BEGIN
   O_exists    := FALSE;
   O_processed := FALSE;

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_wf_order_line_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_line_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_STORE_ORDER_EXISTS;
   fetch C_STORE_ORDER_EXISTS into L_exists;
   close C_STORE_ORDER_EXISTS;

   if L_exists in ('Y','P') then
      O_exists    := TRUE;
      if L_exists = 'P' then
         O_processed := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_STORE_ORDER_EXISTS%ISOPEN then
         close C_STORE_ORDER_EXISTS;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END STORE_ORDER_EXISTS;
-------------------------------------------------------------------------
FUNCTION DELETE_STORE_ORDER(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_wf_order_no       IN      WF_ORDER_DETAIL.WF_ORDER_NO%TYPE,
                            I_wf_order_line_no  IN      WF_ORDER_DETAIL.WF_ORDER_LINE_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)  := 'WF_TRANSFER_SQL.DELETE_STORE_ORDER';

   cursor C_LOCK_STORE_ORDERS IS
      select 'x'
        from store_orders
       where wf_order_no  = I_wf_order_no
         and wf_order_line_no = I_wf_order_line_no
         for update nowait;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_wf_order_line_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_line_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   LP_table := ' STORE_ORDERS';
   
   open C_LOCK_STORE_ORDERS;
   close C_LOCK_STORE_ORDERS;

   delete from store_orders
    where wf_order_no  = I_wf_order_no
      and wf_order_line_no = I_wf_order_line_no;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_STORE_ORDERS%ISOPEN then
         close C_LOCK_STORE_ORDERS;
      end if;      
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_wf_order_no),
                                            to_char(I_wf_order_line_no));
      return FALSE;
   when OTHERS then
      if C_LOCK_STORE_ORDERS%ISOPEN then
         close C_LOCK_STORE_ORDERS;
      end if;     
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
     return FALSE;
END DELETE_STORE_ORDER;
------------------------------------------------------------------------
FUNCTION CREATE_STORE_ORDER(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item             IN       STORE_ORDERS.ITEM%TYPE,
                            I_store            IN       STORE_ORDERS.STORE%TYPE,
                            I_need_date        IN       WF_ORDER_DETAIL.NEED_DATE%TYPE,
                            I_qty              IN       TSFDETAIL.TSF_QTY%TYPE,
                            I_wf_order_no      IN       STORE_ORDERS.WF_ORDER_NO%TYPE,
                            I_wf_order_line_no IN       STORE_ORDERS.WF_ORDER_LINE_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)  := 'WF_TRANSFER_SQL.CREATE_STORE_ORDER';
   L_exists    BOOLEAN;
   L_processed BOOLEAN;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_need_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_need_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_qty',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_wf_order_line_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_line_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if WF_TRANSFER_SQL.STORE_ORDER_EXISTS (O_error_message,
                                          L_exists,
                                          L_processed,
                                          I_item,
                                          I_store,
                                          I_need_date) =FALSE then
      return FALSE;
   end if;

   if L_exists = TRUE then
      O_error_message := SQL_LIB.CREATE_MSG('STORE_ORDER_EXISTS',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;

   insert into store_orders(item,
                            store,
                            need_date,
                            need_qty,
                            processed_date,
                            wf_order_no,
                            delivery_slot_id,
                            roq_extracted_ind,
                            wf_order_line_no)
                     values(I_item,
                            I_store,
                            I_need_date,
                            I_qty,
                            NULL,
                            I_wf_order_no,
                            NULL,
                            'N',
                            I_wf_order_line_no);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END CREATE_STORE_ORDER;
------------------------------------------------------------------------
FUNCTION UPDATE_STORE_ORDER(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_wf_order_no      IN      STORE_ORDERS.WF_ORDER_NO%TYPE,
                            I_wf_order_line_no IN      STORE_ORDERS.WF_ORDER_LINE_NO%TYPE,
                            I_qty              IN      TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)  := 'WF_TRANSFER_SQL.UPDATE_STORE_ORDER';

   cursor C_LOCK_STORE_ORDER IS
      select 'x'
        from store_orders
       where wf_order_no      = I_wf_order_no
         and wf_order_line_no = I_wf_order_line_no
         for update nowait;

BEGIN

   if I_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_wf_order_line_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_order_line_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   LP_table := 'STORE_ORDERS';
   
   open C_LOCK_STORE_ORDER;
   close C_LOCK_STORE_ORDER;

   update store_orders
      set need_qty = I_qty
    where wf_order_no      = I_wf_order_no
      and wf_order_line_no = I_wf_order_line_no;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_STORE_ORDER%ISOPEN then
         close C_LOCK_STORE_ORDER;
      end if;      
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_wf_order_no),
                                            to_char(I_wf_order_line_no));
      return FALSE;
   when OTHERS then
      if C_LOCK_STORE_ORDER%ISOPEN then
         close C_LOCK_STORE_ORDER;
      end if;      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END UPDATE_STORE_ORDER;
------------------------------------------------------------------------
FUNCTION BUILD_TSF_STORE_ORDER(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_wf_order_no          IN      WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                               I_status               IN      WF_ORDER_HEAD.STATUS%TYPE,                               
                               I_transfer_temp_tbl    IN      OBJ_F_TRANSFER_TBL,
                               I_store_order_temp_tbl IN      OBJ_F_STORE_ORDER_TBL)
RETURN BOOLEAN IS

   L_program              VARCHAR2(50)  := 'WF_TRANSFER_SQL.BUILD_TSF_STORE_ORDER';
   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_cust_loc             WF_ORDER_DETAIL.CUSTOMER_LOC%TYPE;
   L_source_loc_id        WF_ORDER_DETAIL.SOURCE_LOC_ID%TYPE;
   L_source_loc_type      TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_need_date            WF_ORDER_DETAIL.NEED_DATE%TYPE;
   L_tsf_no               TSFHEAD.TSF_NO%TYPE;
   L_tsf_exists           VARCHAR2(1) ;
   L_valid                BOOLEAN;
   
   cursor C_TSF_EXISTS is
      select tsf_no
        from tsfhead
       where wf_order_no = I_wf_order_no
         and from_loc = L_source_loc_id
         and to_loc = L_cust_loc
         and wf_need_date = L_need_date
         and rownum=1;
       
BEGIN

   if NOT WF_ORDER_SQL.WF_ORDER_EXISTS(O_error_message,
                                       L_valid,
                                       I_wf_order_no) or NOT L_valid then
      O_error_message := SQL_LIB.CREATE_MSG('INV_EXT_REF_NO');
      return FALSE;
   end if;
   
   if I_status ='A' then
      if I_transfer_temp_tbl.COUNT > 0 then
         FOR tsf_temp_ctr IN I_transfer_temp_tbl.FIRST .. I_transfer_temp_tbl.LAST LOOP
            L_item := I_transfer_temp_tbl(tsf_temp_ctr).item;
            L_source_loc_id := I_transfer_temp_tbl(tsf_temp_ctr).company_loc;
            L_cust_loc := I_transfer_temp_tbl(tsf_temp_ctr).customer_loc;
            L_need_date := I_transfer_temp_tbl(tsf_temp_ctr).need_date;
            L_tsf_exists := 'Y';
            L_tsf_no := -999;

            SQL_LIB.SET_MARK('OPEN',
                             'C_TSF_EXISTS',
                             'TSFHEAD',
                             NULL);
            open C_TSF_EXISTS;

            SQL_LIB.SET_MARK('FETCH',
                             'C_TSF_EXISTS',
                             'TSFHEAD',
                             NULL);
            fetch C_TSF_EXISTS into L_tsf_no;

            if C_TSF_EXISTS%NOTFOUND then
               L_tsf_exists :='N';
            end if;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_TSF_EXISTS',
                             'TSFHEAD',
                             NULL);
            close C_TSF_EXISTS;           

            if L_tsf_exists ='N' then
               if I_transfer_temp_tbl(tsf_temp_ctr).company_loc_type = 'ST' then
                  L_source_loc_type := 'S';
               elsif I_transfer_temp_tbl(tsf_temp_ctr).company_loc_type = 'WH' then
                  L_source_loc_type := 'W';
               end if;
               if WF_TRANSFER_SQL.WF_CREATE_TSF_HEAD (O_error_message,
                                                      L_tsf_no,
                                                      L_source_loc_type,
                                                      L_source_loc_id,
                                                      I_transfer_temp_tbl(tsf_temp_ctr).customer_loc_type,
                                                      L_cust_loc,
                                                      I_transfer_temp_tbl(tsf_temp_ctr).tsf_type,
                                                      L_need_date,
                                                      I_transfer_temp_tbl(tsf_temp_ctr).wf_order_no,
                                                      NULL,       -- RMA_NO
                                                      I_status) =  FALSE then 
                  return FALSE;       
               end if;
            end if;

            if WF_TRANSFER_SQL.WF_CREATE_TSF_DETAIL(O_error_message,
                                                    L_tsf_no,
                                                    L_item,
                                                    I_transfer_temp_tbl(tsf_temp_ctr).qty)= FALSE then
               return FALSE;
            end if;

         END LOOP;
      end if;

      if I_store_order_temp_tbl.COUNT > 0 then
          FOR store_order_temp_ctr IN I_store_order_temp_tbl.FIRST .. I_store_order_temp_tbl.LAST LOOP
             if CREATE_STORE_ORDER(O_error_message,
                                   I_store_order_temp_tbl(store_order_temp_ctr).item,
                                   I_store_order_temp_tbl(store_order_temp_ctr).customer_loc,
                                   I_store_order_temp_tbl(store_order_temp_ctr).need_date,
                                   I_store_order_temp_tbl(store_order_temp_ctr).qty,
                                   I_store_order_temp_tbl(store_order_temp_ctr).wf_order_no,
                                   I_store_order_temp_tbl(store_order_temp_ctr).wf_order_line_no) = FALSE then
                return FALSE;
             end if;
          END LOOP;
       end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('WF_ORDER_REJECT','I_wf_order_no',L_program,NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      if C_TSF_EXISTS%ISOPEN then
         close C_TSF_EXISTS;
      end if;      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
   RETURN FALSE;
END BUILD_TSF_STORE_ORDER;
------------------------------------------------------------------------
FUNCTION DELETE_FRANCHISE_TSF ( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_wf_order_no     IN       TSFHEAD.WF_ORDER_NO%TYPE,
                                I_rma_no          IN       TSFHEAD.RMA_NO%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)  := 'WF_TRANSFER_SQL.DELETE_FRANCHISE_TSF';
   L_valid                BOOLEAN;
   L_tsf_exists           NUMBER;
   L_inv_parm             VARCHAR2(30)  := NULL;

   cursor C_GET_INVALID_TRANSFERS is
      select 1
        from tsfhead
       where decode(I_wf_order_no, null, rma_no, wf_order_no) = decode(I_wf_order_no, null,I_rma_no,I_wf_order_no)
         and status <> 'I'
         and rownum = 1;
 
   cursor C_GET_LOCK_TRANSFERS is
      select tsf_no
        from tsfhead
       where decode(I_wf_order_no, null, rma_no, wf_order_no) = decode(I_wf_order_no, null,I_rma_no,I_wf_order_no)
         and status = 'I'
         for update nowait;
         
   cursor C_LOCK_TSFDETAIL is
      select td.tsf_no
        from tsfhead th, tsfdetail td
       where th.tsf_no = td.tsf_no
         and decode(I_wf_order_no, null, rma_no, wf_order_no) = decode(I_wf_order_no, null,I_rma_no,I_wf_order_no)
         and th.status = 'I'
         for update of td.tsf_no nowait;

   TYPE tsf_TBL is TABLE of C_GET_LOCK_TRANSFERS%ROWTYPE INDEX BY BINARY_INTEGER;   
   L_tsf_tbl      tsf_TBL;
   
BEGIN

   if I_wf_order_no is NULL and I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_EITHER',
                                             'I_wf_order_no',
                                             'I_rma_no',
                                             L_program);
      return FALSE;
   end if;

   if I_wf_order_no is not NULL then
      if NOT WF_ORDER_SQL.WF_ORDER_EXISTS(O_error_message,
                                          L_valid,
                                          I_wf_order_no) or NOT L_valid then
         O_error_message := SQL_LIB.CREATE_MSG('INV_EXT_REF_NO');
         return FALSE;
      end if;
   end if;

   if I_rma_no is not NULL then
      if NOT WF_RETURN_SQL.VALIDATE_RMA_NUMBER(O_error_message,
                                               L_valid,
                                               I_rma_no) or NOT L_valid then
         O_error_message := SQL_LIB.CREATE_MSG('INV_EXT_REF_NO');
         return FALSE;
      end if;
   end if;

   -- If the transfers are in 'I'nput status only, they can be deleted. Even if there is one transfer that is not in
   -- 'I'nput status, none of the transfers can be deleted.
   -- As transfers in 'I'nput status only can be deleted, item_loc_soh adjustments are not required.
   
   open C_GET_INVALID_TRANSFERS;
   fetch C_GET_INVALID_TRANSFERS into L_tsf_exists;
   close C_GET_INVALID_TRANSFERS;

   if L_tsf_exists = 1 then
      O_error_message := SQL_LIB.CREATE_MSG('WF_NO_INPUT_TSF',I_wf_order_no);
      return TRUE; -- If there are no transfers in Input, that is not an error. So return TRUE
   end if;
   
   open C_GET_LOCK_TRANSFERS;
   fetch C_GET_LOCK_TRANSFERS BULK COLLECT into L_tsf_tbl;
   close C_GET_LOCK_TRANSFERS;

   if L_tsf_tbl is not NULL and L_tsf_tbl.COUNT > 0 then   
      LP_table := 'TSFDETAIL';
      
      open C_LOCK_TSFDETAIL;
      close C_LOCK_TSFDETAIL;
         
      FORALL i in L_tsf_tbl.first..L_tsf_tbl.last
         delete from tsfdetail
          where tsf_no = L_tsf_tbl(i).tsf_no;
      
      LP_table := 'TSFHEAD';
      
      FORALL i in L_tsf_tbl.first..L_tsf_tbl.last
         delete from tsfhead
          where tsf_no = L_tsf_tbl(i).tsf_no;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_GET_INVALID_TRANSFERS%ISOPEN then
         close C_GET_INVALID_TRANSFERS;
      end if;
      if C_GET_LOCK_TRANSFERS%ISOPEN then
         close C_GET_LOCK_TRANSFERS;
      end if;
      if C_LOCK_TSFDETAIL%ISOPEN then
         close C_LOCK_TSFDETAIL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_wf_order_no),
                                            to_char(I_rma_no));
      return FALSE;
   when OTHERS then
      if C_GET_INVALID_TRANSFERS%ISOPEN then
         close C_GET_INVALID_TRANSFERS;
      end if;
      if C_GET_LOCK_TRANSFERS%ISOPEN then
         close C_GET_LOCK_TRANSFERS;
      end if;
      if C_LOCK_TSFDETAIL%ISOPEN then
         close C_LOCK_TSFDETAIL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_FRANCHISE_TSF;
--------------------------------------------------------------------------------------
FUNCTION DELETE_FRANCHISE_TSF_DETAIL ( O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_wf_order_no        IN       TSFHEAD.WF_ORDER_NO%TYPE,
                                       I_rma_no             IN       TSFHEAD.RMA_NO%TYPE,
                                       I_item               IN       TSFDETAIL.ITEM%TYPE,
                                       I_from_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                                       I_to_loc             IN       TSFHEAD.TO_LOC%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)  := 'WF_TRANSFER_SQL.DELETE_FRANCHISE_TSF_DETAIL';
   L_valid                BOOLEAN;
   L_tsf_no               TSFHEAD.TSF_NO%TYPE := '';
   L_status               TSFHEAD.STATUS%TYPE := '';
   L_inv_parm             VARCHAR2(30)  := NULL;

   cursor C_GET_TSF_NO is
      select th.tsf_no, th.status
        from tsfhead th,
             tsfdetail td
       where th.tsf_no = td.tsf_no
         and decode(I_wf_order_no, null,rma_no,wf_order_no) = decode(I_wf_order_no, null,I_rma_no,I_wf_order_no)
         and from_loc = NVL(I_from_loc, from_loc)
         and to_loc = NVL(I_to_loc, to_loc)
         and item = I_item; 
           
BEGIN

   if I_wf_order_no is NULL and I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_EITHER',
                                             'I_wf_order_no',
                                             'I_rma_no',
                                             L_program);
      return FALSE;
   end if;

   if I_wf_order_no is not NULL then
      if NOT WF_ORDER_SQL.WF_ORDER_EXISTS(O_error_message,
                                          L_valid,
                                          I_wf_order_no) or NOT L_valid then
         O_error_message := SQL_LIB.CREATE_MSG('INV_EXT_REF_NO');
         return FALSE;
      end if;
   end if;

   if I_rma_no is not NULL then
      if NOT WF_RETURN_SQL.VALIDATE_RMA_NUMBER(O_error_message,
                                               L_valid,
                                               I_rma_no) or NOT L_valid then
         O_error_message := SQL_LIB.CREATE_MSG('INV_EXT_REF_NO');
         return FALSE;
      end if;
   end if;
   
   open C_GET_TSF_NO;
   fetch C_GET_TSF_NO into L_tsf_no, L_status;
   close C_GET_TSF_NO;

   if L_tsf_no is not NULL and L_status = 'I' then
      LP_table := 'TSFDETAIL';
      
      open C_LOCK_TSFDETAIL(L_tsf_no);
      close C_LOCK_TSFDETAIL;
            
      delete from tsfdetail
       where tsf_no = L_tsf_no
         and item = I_item;
   elsif L_status <> 'I' then
      O_error_message := SQL_LIB.CREATE_MSG('WF_NO_INPUT_TSF');
      return FALSE;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_GET_TSF_NO%ISOPEN then
         close C_GET_TSF_NO;
      end if;
      if C_LOCK_TSFDETAIL%ISOPEN then
         close C_LOCK_TSFDETAIL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(L_tsf_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      if C_GET_TSF_NO%ISOPEN then
         close C_GET_TSF_NO;
      end if;
      if C_LOCK_TSFDETAIL%ISOPEN then
         close C_LOCK_TSFDETAIL;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_FRANCHISE_TSF_DETAIL;
--------------------------------------------------------------------------------------
FUNCTION CANCEL_FRANCHISE_TSF ( O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_wf_order_no        IN       TSFHEAD.WF_ORDER_NO%TYPE,
                                I_rma_no             IN       TSFHEAD.RMA_NO%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)  := 'WF_TRANSFER_SQL.CANCEL_FRANCHISE_TSF';

   cursor C_GET_TSF is
      select tsf_no, 
             from_loc, 
             from_loc_type, 
             to_loc, 
             to_loc_type 
        from tsfhead
       where decode(I_wf_order_no, NULL, rma_no, wf_order_no) = decode(I_wf_order_no, NULL, I_rma_no, I_wf_order_no)
         and from_loc_type = 'S'
         for update nowait;         

   -- Retrieve the outstanding quantity to be cancelled.
   cursor C_GET_TSF_DETAIL (L_tsf_no TSFHEAD.TSF_NO%TYPE) is
      select item, 
             (tsf_qty - GREATEST(NVL(ship_qty,0),
                                 NVL(distro_qty,0),
                                 NVL(selected_qty,0))) qty_for_cancellation
        from tsfdetail
       where tsf_no = L_tsf_no
         and (tsf_qty - GREATEST(NVL(ship_qty,0),
                                 NVL(distro_qty,0),
                                 NVL(selected_qty,0))) > 0;

   TYPE tsf_TBL is TABLE of C_GET_TSF%ROWTYPE INDEX BY BINARY_INTEGER;   
   L_tsf_tbl      tsf_TBL;
   
   TYPE tsf_dtl_TBL is TABLE of C_GET_TSF_DETAIL%ROWTYPE INDEX BY BINARY_INTEGER;   
   L_tsf_dtl_tbl  tsf_dtl_TBL;
   
BEGIN
   
   if I_wf_order_no is NULL and I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_EITHER',
                                             'I_wf_order_no',
                                             'I_rma_no',
                                             L_program);
      return FALSE;
   end if;

   open C_GET_TSF;
   fetch C_GET_TSF BULK COLLECT into L_tsf_tbl;
   close C_GET_TSF;

   if L_tsf_tbl is not NULL and L_tsf_tbl.COUNT > 0 then   
      FOR i in L_tsf_tbl.first..L_tsf_tbl.last LOOP

         open C_GET_TSF_DETAIL(L_tsf_tbl(i).tsf_no);
         fetch C_GET_TSF_DETAIL BULK COLLECT into L_tsf_dtl_tbl;
         close C_GET_TSF_DETAIL;

         if L_tsf_dtl_tbl is not NULL and L_tsf_dtl_tbl.COUNT > 0 then   
            LP_table := 'TSFDETAIL';

            open C_LOCK_TSFDETAIL(L_tsf_tbl(i).tsf_no);
            close C_LOCK_TSFDETAIL;         
            FOR j in L_tsf_dtl_tbl.first..L_tsf_dtl_tbl.last LOOP

               update tsfdetail
                  set tsf_qty = tsf_qty - L_tsf_dtl_tbl(j).qty_for_cancellation,
                      updated_by_rms_ind = 'Y'
                where tsf_no = L_tsf_tbl(i).tsf_no
                  and item = L_tsf_dtl_tbl(j).item;

               if WF_TRANSFER_SQL.WF_UPD_ITEM_RESV (O_error_message,
                                                    L_tsf_dtl_tbl(j).item,   
                                                    (L_tsf_dtl_tbl(j).qty_for_cancellation * -1),   
                                                    L_tsf_tbl(i).from_loc,
                                                    L_tsf_tbl(i).from_loc_type) = FALSE then
                  return FALSE;
               end if;

               if WF_TRANSFER_SQL.WF_UPD_ITEM_EXP (O_error_message,
                                                   L_tsf_dtl_tbl(j).item,   
                                                   (L_tsf_dtl_tbl(j).qty_for_cancellation * -1),   
                                                   L_tsf_tbl(i).to_loc,
                                                   L_tsf_tbl(i).to_loc_type) = FALSE then
                  return FALSE;
               end if;
            END LOOP;
         end if;
      END LOOP;

      LP_table := 'TSFHEAD';
      
      FORALL i in L_tsf_tbl.first..L_tsf_tbl.last
         update tsfhead
            set status = 'D'
          where tsf_no = L_tsf_tbl(i).tsf_no;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_GET_TSF%ISOPEN then
         close C_GET_TSF;
      end if;
      if C_GET_TSF_DETAIL%ISOPEN then
         close C_GET_TSF_DETAIL;
      end if;
      if C_LOCK_TSFDETAIL%ISOPEN then
         close C_LOCK_TSFDETAIL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      if C_GET_TSF%ISOPEN then
         close C_GET_TSF;
      end if;
      if C_GET_TSF_DETAIL%ISOPEN then
         close C_GET_TSF_DETAIL;
      end if;
      if C_LOCK_TSFDETAIL%ISOPEN then
         close C_LOCK_TSFDETAIL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CANCEL_FRANCHISE_TSF;
--------------------------------------------------------------------------------------
FUNCTION MODIFY_FRANCHISE_TSF ( O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_wf_order_no        IN       TSFHEAD.WF_ORDER_NO%TYPE,
                                I_rma_no             IN       TSFHEAD.RMA_NO%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)  := 'WF_TRANSFER_SQL.MODIFY_FRANCHISE_TSF';
   
   cursor C_UPD_ITEM_LOC_SOH is
      select wod1.item,
             th1.from_loc,
             th1.from_loc_type,
             th1.to_loc,
             th1.to_loc_type,
             (wod1.requested_qty - td1.tsf_qty) resv_exp_qty
        from wf_order_detail wod1,
             wf_order_head woh1,
             tsfhead th1,
             tsfdetail td1
       where wod1.wf_order_no = NVL(I_wf_order_no, -999)
         and woh1.wf_order_no = wod1.wf_order_no
         and woh1.status in ('A', 'P')
         and th1.wf_order_no = wod1.wf_order_no
         and th1.from_loc_type = DECODE(wod1.source_loc_type,'ST','S')
         and th1.from_loc = wod1.source_loc_id
         and th1.to_loc = wod1.customer_loc
         and th1.tsf_no = td1.tsf_no
         and td1.item = wod1.item
         and (wod1.requested_qty - td1.tsf_qty) <> 0
      UNION ALL
      select wrd1.item,
             th1.from_loc,
             th1.from_loc_type,
             th1.to_loc,
             th1.to_loc_type,
             (wrd1.returned_qty - td1.tsf_qty) resv_exp_qty
        from wf_return_detail wrd1,
             wf_return_head wrh1,
             tsfhead th1,
             tsfdetail td1
       where wrd1.rma_no = NVL(I_rma_no, -999)
         and wrh1.rma_no = wrd1.rma_no
         and wrh1.status in ('A', 'P')
         and th1.rma_no = wrd1.rma_no
         and th1.from_loc = wrh1.customer_loc
         and th1.to_loc_type = wrh1.return_loc_type
         and th1.to_loc = wrh1.return_loc_id
         and th1.tsf_no = td1.tsf_no
         and td1.item = wrd1.item
         and (wrd1.returned_qty - td1.tsf_qty) <> 0;      
   
   TYPE ils_upd_TBL is TABLE of C_UPD_ITEM_LOC_SOH%ROWTYPE INDEX BY BINARY_INTEGER;   
   L_ils_upd_TBL      ils_upd_TBL;

BEGIN
   
   if I_wf_order_no is NULL and I_rma_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_EITHER',
                                             'I_wf_order_no',
                                             'I_rma_no',
                                             L_program);
      return FALSE;
   end if;
   
   -- Post merge the transfer tables will be updated. 
   -- So, fecth the item,loc and quantity to be updated in item_loc_soh table before merge. 
   -- Once tsfdetail table is merged, update item_loc_soh using the below collection.
   
   open C_UPD_ITEM_LOC_SOH;
   fetch C_UPD_ITEM_LOC_SOH BULK COLLECT into L_ils_upd_TBL;
   close C_UPD_ITEM_LOC_SOH;

   -- Update tsf_qty to match with requested_qty.
   
   if I_wf_order_no is not NULL then
      merge into tsfdetail td_out
      using (select wod1.wf_order_no,
                    wod1.requested_qty,
                    wod1.item,
                    th1.tsf_no
               from wf_order_detail wod1,
                    wf_order_head woh1,
                    tsfhead th1,
                    tsfdetail td1
              where woh1.wf_order_no = I_wf_order_no
                and woh1.wf_order_no = wod1.wf_order_no
                and th1.wf_order_no = woh1.wf_order_no
                and th1.from_loc_type = DECODE(wod1.source_loc_type,'ST','S')
                and th1.from_loc = wod1.source_loc_id
                and th1.to_loc = wod1.customer_loc
                and th1.tsf_no = td1.tsf_no
                and td1.item = wod1.item
                and td1.tsf_qty <> wod1.requested_qty) wod
         on   (     td_out.tsf_no = wod.tsf_no
                and td_out.item = wod.item) 
      when matched then
              update set td_out.tsf_qty  = wod.requested_qty,
                         td_out.updated_by_rms_ind = 'Y';
   elsif I_rma_no is not NULL then
      merge into tsfdetail td_out
      using (select wrd1.rma_no,
                    wrd1.returned_qty,
                    wrd1.item,
                    th1.tsf_no
               from wf_return_detail wrd1,
                    wf_return_head wrh1,
                    tsfhead th1,
                    tsfdetail td1
              where wrh1.rma_no = I_rma_no
                and wrh1.rma_no = wrd1.rma_no
                and th1.rma_no = wrd1.rma_no
                and th1.from_loc = wrh1.customer_loc
                and th1.to_loc_type = wrh1.return_loc_type
                and th1.to_loc = wrh1.return_loc_id
                and th1.tsf_no = td1.tsf_no
                and td1.item = wrd1.item
                and td1.tsf_qty <> wrd1.returned_qty) wod
         on   (     td_out.tsf_no = wod.tsf_no
                and td_out.item = wod.item) 
      when matched then
              update set td_out.tsf_qty  = wod.returned_qty,
                         td_out.updated_by_rms_ind = 'Y';
   
   end if;

   if L_ils_upd_TBL is not NULL and L_ils_upd_TBL.COUNT > 0 then   
      FOR i in L_ils_upd_TBL.first..L_ils_upd_TBL.last LOOP

         if WF_TRANSFER_SQL.WF_UPD_ITEM_RESV (O_error_message, 
                                              L_ils_upd_TBL(i).item,
                                              L_ils_upd_TBL(i).resv_exp_qty,
                                              L_ils_upd_TBL(i).from_loc,
                                              L_ils_upd_TBL(i).from_loc_type) = FALSE then
                              
            return FALSE;
         end if;

         if WF_TRANSFER_SQL.WF_UPD_ITEM_EXP (O_error_message, 
                                             L_ils_upd_TBL(i).item,
                                             L_ils_upd_TBL(i).resv_exp_qty,
                                             L_ils_upd_TBL(i).to_loc,
                                             L_ils_upd_TBL(i).to_loc_type) = FALSE then
                             
            return FALSE;
         end if;
      END LOOP;
    end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_UPD_ITEM_LOC_SOH%ISOPEN then
         close C_UPD_ITEM_LOC_SOH;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_FRANCHISE_TSF;
--------------------------------------------------------------------------------------
FUNCTION DETERMINE_F_ORDRET_STATUS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_tsf_status      IN OUT   TSFHEAD.STATUS%TYPE,
                                    O_f_order_status  IN OUT   WF_ORDER_HEAD.STATUS%TYPE,
                                    O_f_return_status IN OUT   WF_RETURN_HEAD.STATUS%TYPE,
                                    I_from_loc        IN       ITEM_LOC.LOC%TYPE,
                                    I_from_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                                    I_to_loc          IN       ITEM_LOC.LOC%TYPE,
                                    I_to_loc_type     IN       ITEM_LOC.LOC_TYPE%TYPE,
                                    I_tsf_status      IN       TSFHEAD.STATUS%TYPE,
                                    I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64)                 := 'WF_TRANSFER_SQL.DETERMINE_F_ORDRET_STATUS';
   L_inv_parm         VARCHAR2(30)                 := NULL;
   L_action_type      VARCHAR2(1)                  := NULL;
   L_credit_ind       WF_CUSTOMER.CREDIT_IND%TYPE  := NULL;
   
BEGIN
   O_tsf_status := I_tsf_status;  -- Initialize out transfer status same as Input.

   if I_from_loc is NULL then
      L_inv_parm := 'I_from_loc';
   elsif I_from_loc_type is NULL then
      L_inv_parm := 'I_from_loc_type';
   elsif I_to_loc is NULL then
      L_inv_parm := 'I_to_loc';
   elsif I_to_loc_type is NULL then
      L_inv_parm := 'I_to_loc_type';
   elsif I_tsf_status is NULL then
      L_inv_parm := 'I_tsf_status';
   elsif I_tsf_type is NULL then
      L_inv_parm := 'I_tsf_type';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                L_action_type,
                                                I_from_loc,
                                                I_from_loc_type,
                                                I_to_loc,
                                                I_to_loc_type) = FALSE then
      return FALSE;
   end if;
   
   if L_action_type = ACTION_CREATE_WF_NONE then
      -- If there is not company to/from Franchise location movement, return O_f_order_status and O_f_return_status as NULL.
      O_f_order_status := NULL;
      O_f_return_status := NULL;

   elsif L_action_type = ACTION_CREATE_WF_RETURN and I_tsf_status in ('I', 'B') then
      O_f_return_status := WF_STATUS_INPUT;

   elsif L_action_type = ACTION_CREATE_WF_RETURN and I_tsf_status = 'A' then
      O_f_return_status := WF_STATUS_APPROVE;                              -- Credit check is not required for franchise return

   elsif L_action_type = ACTION_CREATE_WF_ORDER and I_tsf_status in ('I', 'B') then      
      O_f_order_status := WF_STATUS_INPUT;          

   elsif L_action_type = ACTION_CREATE_WF_ORDER and I_tsf_type IN ('EG', 'SIM', 'CO') and I_tsf_status = 'A' then 
      O_f_order_status := WF_STATUS_APPROVE; -- No credit check required for EG/SIM/CO. Also, these transfer should always in interfaced in Approved Status. 

   elsif L_action_type = ACTION_CREATE_WF_ORDER and I_tsf_status = 'A' then      -- Other Transfer types. Do credit check. 
      if WF_CUSTOMER_SQL.CHECK_WF_CUST_LOC_CREDIT (O_error_message,
                                                   L_credit_ind,
                                                   I_to_loc) = FALSE then
         return FALSE;
      end if;

      if L_credit_ind = 'Y' then
         O_f_order_status := WF_STATUS_APPROVE;
      else  -- Fails credit check. Do not approve the transfer and keep Franchise order in require credit approval status. 
         O_tsf_status := 'I';
         O_f_order_status := WF_STATUS_REQ_CREDIT;
      end if;

   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_F_TSF_STATUS',
                                            I_tsf_type,
                                            I_tsf_status,
                                            NULL);                          
      return FALSE;         
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DETERMINE_F_ORDRET_STATUS;
-----------------------------------------------------------------------------------------
FUNCTION IS_FRANCHISE_ORDER_RETURN (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_action_type     IN OUT   VARCHAR2,
                                    I_from_loc        IN       ITEM_LOC.LOC%TYPE,
                                    I_from_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                                    I_to_loc          IN       ITEM_LOC.LOC%TYPE,
                                    I_to_loc_type     IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)                  := 'WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN';
   L_inv_parm                 VARCHAR2(30)                  := NULL;
   L_loc                      STORE.STORE%TYPE              := NULL;
   L_from_type                STORE.STORE_TYPE%TYPE         := NULL;
   L_to_type                  STORE.STORE_TYPE%TYPE         := NULL;
   L_from_stockholding_ind    STORE.STOCKHOLDING_IND%TYPE   := NULL;
   L_to_stockholding_ind      STORE.STOCKHOLDING_IND%TYPE   := NULL;
   L_from_finisher_ind        WH.FINISHER_IND%TYPE          := NULL;
   L_to_finisher_ind          WH.FINISHER_IND%TYPE          := NULL;
   L_table                    VARCHAR2(50)                  := NULL;
   
   cursor C_GET_STORETYPE is
      select store_type,
             stockholding_ind
        from store
       where store = L_loc;

   cursor C_GET_WH_FINISHER_IND is
      select finisher_ind
        from wh
       where wh = L_loc;

BEGIN

   O_action_type := NULL;

   -- Validate the input parameters are populated and correct. 
   if I_from_loc is NULL then
      L_inv_parm := 'I_from_loc';
   elsif I_from_loc_type is NULL then
      L_inv_parm := 'I_from_loc_type';
   elsif I_to_loc is NULL then
      L_inv_parm := 'I_to_loc';
   elsif I_to_loc_type is NULL then
      L_inv_parm := 'I_to_loc_type';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
      
   if I_from_loc_type not in ('S', 'W') or I_to_loc_type not in ('S', 'W') then
      O_action_type := ACTION_CREATE_WF_NONE;
      return TRUE;
   End if;
   
   -- Validate the from location and get the location details for identifying F order/return
   L_loc := I_from_loc;
   if I_from_loc_type = 'S' then
      L_table := 'STORE';
      SQL_LIB.SET_MARK('OPEN', 'C_GET_STORETYPE', L_table, L_loc);      
      open C_GET_STORETYPE;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_STORETYPE', L_table, L_loc);      
      fetch C_GET_STORETYPE into L_from_type, L_from_stockholding_ind;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_STORETYPE', L_table, L_loc);      
      close C_GET_STORETYPE;
      
      if L_from_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_WH',
                                               I_from_loc,
                                               NULL,
                                               NULL);

         return FALSE;
      end if;   
   else
      L_table := 'WH';
      SQL_LIB.SET_MARK('OPEN', 'C_GET_WH_FINISHER_IND', L_table, L_loc);      
      open C_GET_WH_FINISHER_IND;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_WH_FINISHER_IND', L_table, L_loc);      
      fetch C_GET_WH_FINISHER_IND into L_from_finisher_ind;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_WH_FINISHER_IND', L_table, L_loc);      
      close C_GET_WH_FINISHER_IND;

      if L_from_finisher_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_WH',
                                               I_from_loc,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   -- Validate the to location and get the location details for identifying F order/return
   L_loc := I_to_loc;
   if I_to_loc_type = 'S' then
      L_table := 'STORE';
      SQL_LIB.SET_MARK('OPEN', 'C_GET_STORETYPE', L_table, L_loc);      
      open C_GET_STORETYPE;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_STORETYPE', L_table, L_loc);      
      fetch C_GET_STORETYPE into L_to_type, L_to_stockholding_ind;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_STORETYPE', L_table, L_loc);      
      close C_GET_STORETYPE;

      if L_to_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_WH',
                                               I_to_loc,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   else
      L_table := 'WH';
      SQL_LIB.SET_MARK('OPEN', 'C_GET_WH_FINISHER_IND', L_table, L_loc);      
      open C_GET_WH_FINISHER_IND;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_WH_FINISHER_IND', L_table, L_loc);      
      fetch C_GET_WH_FINISHER_IND into L_to_finisher_ind;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_WH_FINISHER_IND', L_table, L_loc);      
      close C_GET_WH_FINISHER_IND;

      if L_to_finisher_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_WH',
                                               I_to_loc,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;    
   end if;
   
   if NVL(L_from_type,'C') = 'C' and NVL(L_to_type,'C') = 'F' then   -- Company location to Franchise store
      -- Transfer between company store to non-stockholding franchise location is not allowed. 
      -- Also transfer from a Finisher warehouse to franchise location is not allowed. 
      if I_from_loc_type = 'S' and L_to_stockholding_ind = 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_F_NONSSTOCK_STORE',
                                               I_from_loc,
                                               I_to_loc,
                                               NULL);
         
         return FALSE;
      end if;
      if I_from_loc_type = 'W' and L_from_finisher_ind = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_F_FINISHER_LOCATION',
                                               I_from_loc,
                                               I_to_loc,
                                               NULL);
         
         return FALSE;
      end if;
      O_action_type := ACTION_CREATE_WF_ORDER;  -- Franchise Order    
   elsif NVL(L_from_type,'C') = 'F' and NVL(L_to_type,'C') = 'C' then   -- Franchise Store to company location
      -- Transfer non-stockholding franchise location to company store is not allowed. 
      -- Also return to finisher warehouse is not allowed.
      if I_to_loc_type = 'S' and L_from_stockholding_ind = 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_F_NONSSTOCK_STORE',
                                               I_to_loc,
                                               I_from_loc,
                                               NULL);
         
         return FALSE;
      end if;
      if I_to_loc_type = 'W' and L_to_finisher_ind = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_F_FINISHER_LOCATION',
                                               I_to_loc,
                                               I_from_loc,
                                               NULL);
         
         return FALSE;
      end if;      
      O_action_type := ACTION_CREATE_WF_RETURN;  -- Franchise Return 
   else  -- between two company or two franchise location. 
      O_action_type := ACTION_CREATE_WF_NONE;
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      if C_GET_STORETYPE%ISOPEN then
         close C_GET_STORETYPE;
      end if;
      
      if C_GET_WH_FINISHER_IND%ISOPEN then
         close C_GET_WH_FINISHER_IND;
      end if;
   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END IS_FRANCHISE_ORDER_RETURN;
-----------------------------------------------------------------------------------------
FUNCTION SYNC_F_ORDER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_wf_order_no     IN OUT   WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                       O_tsf_status      IN OUT   TSFHEAD.STATUS%TYPE,
                       I_action_type     IN       VARCHAR2,
                       I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                       I_tsf_status      IN       TSFHEAD.STATUS%TYPE,
                       I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64)                   := 'WF_TRANSFER_SQL.SYNC_F_ORDER';
   L_inv_parm                 VARCHAR2(30)                   := NULL;
   L_tsf_status               TSFHEAD.STATUS%TYPE            := NULL;
   L_input_tsf_status         TSFHEAD.STATUS%TYPE            := NULL;
   L_dummy_status             TSFHEAD.STATUS%TYPE            := NULL;
   L_tsf_type                 TSFHEAD.TSF_TYPE%TYPE          := NULL;
   L_wf_order_type            WF_ORDER_HEAD.ORDER_TYPE%TYPE  := NULL;
   L_f_order_status           WF_ORDER_HEAD.STATUS%TYPE      := NULL;
   L_action_type              VARCHAR2(10)                   := I_action_type;
   L_f_order_head_rec         OBJ_F_ORDER_HEAD_REC           := OBJ_F_ORDER_HEAD_REC();
   L_f_order_detail_tbl       OBJ_F_ORDER_DETAIL_TBL         := OBJ_F_ORDER_DETAIL_TBL();
   L_from_loc                 ITEM_LOC.LOC%TYPE              := NULL;
   L_from_loc_type            ITEM_LOC.LOC_TYPE%TYPE         := NULL;
   L_to_loc                   ITEM_LOC.LOC%TYPE              := NULL;
   L_to_loc_type              ITEM_LOC.LOC_TYPE%TYPE         := NULL;
   L_wf_order_no              WF_ORDER_HEAD.WF_ORDER_NO%TYPE := NULL;
   L_table                    VARCHAR2(50)                   := NULL;
   L_ship_tsf_ind             VARCHAR2(1)                    := 'N';
    
   cursor C_GET_TSF_INFO is
      select status,
             from_loc,
             from_loc_type,
             to_loc,
             to_loc_type,
             tsf_type,
             wf_order_no
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_GET_WF_STATUS is
      select status,
             order_type
        from wf_order_head
       where wf_order_no = L_wf_order_no;
       
   cursor C_GET_TSF_HEAD is
      select OBJ_F_ORDER_HEAD_REC(wf_order_no,                                                     -- WF_ORDER_NO
                                  NVL(L_wf_order_type, DECODE(tsf_type,'EG', 'X',                  -- ORDER TYPE
                                                                       'SIM', 'X',                 -- Externally generated for SIM/EG,
																	   'CO', 'X',				   -- Externally generated for CO
                                                                       'A')),                      -- Else Auto.                       
                                  NULL,                                                            -- CURRENCY CODE
                                  L_f_order_status,                                                -- STATUS
                                  NULL,                                                            -- CANCEL REASON
                                  NULL,                                                            -- COMMENTS
                                  L_f_order_detail_tbl)                                            -- F_ORDER_DETAILS
      from tsfhead   
      where tsf_no = I_tsf_no;

   cursor C_GET_TSF_DETAIL is
      select OBJ_F_ORDER_DETAIL_REC( wf_order_no,                                                  -- WF_ORDER_NO
                                     td.item,                                                      -- ITEM
                                     DECODE(th.from_loc_type,'S','ST','W','WH'),                   -- SOURCE_LOC_TYPE
                                     th.from_loc,                                                  -- SOURCE_LOC_ID
                                     th.to_loc,                                                    -- CUSTOMER_LOC
                                     DECODE(L_ship_tsf_ind, 'N', NVL(td.tsf_qty,0), 0),            -- REQUESTED_QTY
                                     NULL,                                                         -- CANCEL_REASON
                                     GREATEST(NVL(th.delivery_date, get_vdate),get_vdate),                             -- NEED DATE
                                     GREATEST(NVL(th.not_after_date, 
                                                   NVL(th.delivery_date, get_vdate) + 1),get_vdate + 1))               -- NOT_AFTER_DATE
      from  tsfhead th,
            tsfdetail td
      where th.tsf_no = td.tsf_no
        and th.tsf_no = I_tsf_no;

BEGIN
   L_input_tsf_status := I_tsf_status;
      
   if I_tsf_no is NULL then
      L_inv_parm := 'I_tsf_no';
   elsif I_action_type is NULL then
      L_inv_parm := 'I_action_type';
   elsif I_tsf_status is NULL and I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE then
      L_inv_parm := 'I_tsf_status';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   
   if I_action_type not in (RMS_CONSTANTS.WF_ACTION_CREATE, 
                            RMS_CONSTANTS.WF_ACTION_UPDATE, 
                            RMS_CONSTANTS.WF_ACTION_SHIPPED) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ACTION_TYPE',
                                            I_action_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   -- Get existing transfer information. 
   L_table := 'TSFHEAD';
   SQL_LIB.SET_MARK('OPEN', 'C_GET_TSF_INFO', L_table, I_tsf_no);      
   open C_GET_TSF_INFO;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_TSF_INFO', L_table, I_tsf_no);      
   fetch C_GET_TSF_INFO into L_tsf_status,
                             L_from_loc,
                             L_from_loc_type,
                             L_to_loc,
                             L_to_loc_type,
                             L_tsf_type,
                             L_wf_order_no;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_TSF_INFO', L_table, I_tsf_no);      
   close C_GET_TSF_INFO;
   
   -- If there are no transfer records error out. 
   if L_tsf_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF',
                                            I_tsf_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   -- If the action type is Shipped and no linked wf_order_no exists in tsfhead table, 
   -- this is a case of shipment created for no existing RMS transfer. Create the franchise
   -- order. The franchise order will be created with zero quantities to indicate this 
   -- is a post approval change in quantities.
   if L_wf_order_no is NULL and I_action_type = RMS_CONSTANTS.WF_ACTION_SHIPPED then
      L_action_type := RMS_CONSTANTS.WF_ACTION_CREATE;
      L_input_tsf_status := 'A';
      L_ship_tsf_ind     := 'Y';
   elsif I_action_type = RMS_CONSTANTS.WF_ACTION_SHIPPED then        -- Treat shipment as regular update
      L_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;
   end if;   
   
   -- Do the credit check validation for a new transfer create or for transfer status update from Input to Approved.
   if L_action_type = RMS_CONSTANTS.WF_ACTION_CREATE 
      or (L_action_type = RMS_CONSTANTS.WF_ACTION_UPDATE and L_tsf_status in ('I','B') and L_input_tsf_status = 'A' and L_wf_order_no is NOT NULL) then

      -- WF_TRANSFER_SQL.DETERMINE_F_ORDRET_STATUS will return back L_f_order_status as NOT NULL is the from/to loc
      -- corresponds to a franchise order scenario. 
      if WF_TRANSFER_SQL.DETERMINE_F_ORDRET_STATUS(O_error_message,
                                                   O_tsf_status,
                                                   L_f_order_status,          -- F order status
                                                   L_dummy_status,            
                                                   L_from_loc,
                                                   L_from_loc_type,
                                                   L_to_loc,
                                                   L_to_loc_type,
                                                   L_input_tsf_status,
                                                   NVL(I_tsf_type,L_tsf_type)) = FALSE then
         return FALSE;
      end if;
   elsif L_wf_order_no is NOT NULL then
      L_table := 'WF_ORDER_HEAD';
      SQL_LIB.SET_MARK('OPEN', 'C_GET_WF_STATUS', L_table, L_wf_order_no);      
      open C_GET_WF_STATUS;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_WF_STATUS', L_table, L_wf_order_no);      
      fetch C_GET_WF_STATUS into L_f_order_status, L_wf_order_type;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_WF_STATUS', L_table, L_wf_order_no);      
      close C_GET_WF_STATUS;
      
      O_tsf_status := NVL(L_input_tsf_status, L_tsf_status);
   end if;

   -- if WF_TRANSFER_SQL.DETERMINE_F_ORDRET_STATUS is called for a non franchise transfer, L_f_order_status will be null.
   -- Return back as no sync is required. 
   if L_f_order_status is NULL then
      return TRUE;
   end if;

   L_table := 'TSFHEAD, TSFDETAIL';
   SQL_LIB.SET_MARK('OPEN', 'C_GET_TSF_DETAIL', L_table, I_tsf_no);      
   open C_GET_TSF_DETAIL;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_TSF_DETAIL', L_table, I_tsf_no);      
   fetch C_GET_TSF_DETAIL BULK COLLECT into L_f_order_detail_tbl;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_TSF_DETAIL', L_table, I_tsf_no);      
   close C_GET_TSF_DETAIL;

   L_table := 'TSFHEAD';
   SQL_LIB.SET_MARK('OPEN', 'C_GET_TSF_HEAD', L_table, I_tsf_no);      
   open C_GET_TSF_HEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_TSF_HEAD', L_table, I_tsf_no);      
   fetch C_GET_TSF_HEAD into L_f_order_head_rec;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_TSF_HEAD', L_table, I_tsf_no);      
   close C_GET_TSF_HEAD;

   if WF_CREATE_ORDER_SQL.PROCESS_F_ORDER(O_error_message,
                                          O_wf_order_no,
                                          L_f_order_head_rec,
                                          L_action_type,
                                          I_tsf_no) = FALSE then
      return FALSE;
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SYNC_F_ORDER;
-----------------------------------------------------------------------------------------
FUNCTION SYNC_F_RETURN (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_rma_no          IN OUT   WF_RETURN_HEAD.RMA_NO%TYPE,
                        I_action_type     IN       VARCHAR2,
                        I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                        I_tsf_status      IN       TSFHEAD.STATUS%TYPE,
                        I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64)                           := 'WF_TRANSFER_SQL.SYNC_F_RETURN';
   L_inv_parm                 VARCHAR2(30)                           := NULL;
   L_dummy_status             TSFHEAD.STATUS%TYPE                    := NULL;
   L_input_tsf_status         TSFHEAD.STATUS%TYPE                    := NULL;
   L_f_return_head_rec        OBJ_F_RETURN_HEAD_REC                  := OBJ_F_RETURN_HEAD_REC();
   L_f_return_detail_tbl      OBJ_F_RETURN_DETAIL_TBL                := OBJ_F_RETURN_DETAIL_TBL();
   L_action_type              VARCHAR2(10);
   L_from_loc                 ITEM_LOC.LOC%TYPE                      := NULL;
   L_from_loc_type            ITEM_LOC.LOC_TYPE%TYPE                 := NULL;
   L_to_loc                   ITEM_LOC.LOC%TYPE                      := NULL;
   L_to_loc_type              ITEM_LOC.LOC_TYPE%TYPE                 := NULL;
   L_rma_no                   WF_RETURN_HEAD.RMA_NO%TYPE             := NULL;
   L_f_return_status          WF_RETURN_HEAD.STATUS%TYPE             := NULL;
   L_wf_return_type           WF_RETURN_HEAD.RETURN_TYPE%TYPE        := NULL;
   L_tsf_status               TSFHEAD.STATUS%TYPE                    := NULL;
   L_table                    VARCHAR2(50)                           := NULL;
   L_ship_tsf_ind             VARCHAR2(1)                            := 'N';
   
   cursor C_GET_TSF_INFO is
      select status,
             from_loc,
             from_loc_type,
             to_loc,
             to_loc_type,
             rma_no
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_GET_WF_RETURN is
      select status,
             return_type
        from wf_return_head
       where rma_no = L_rma_no;
       
   cursor C_GET_TSF_HEAD is
      select OBJ_F_RETURN_HEAD_REC(rma_no,                                            -- RMA_NO
                                   NULL,                                              -- CUST_RET_REF_NO
                                   NVL(L_wf_return_type, DECODE(tsf_type,'EG', 'X',   -- RETURN TYPE
                                                                         'SIM','X',   -- Externally generated for SIM/EG, 
																		 'CO', 'X',   -- Externally generated for CO
                                                                         'A')),       -- Else Auto.                                        
                                   from_loc,                                          -- CUSTOMER_LOC
                                   'R',                                               -- RETURN_METHOD
                                   to_loc_type,                                       -- RETURN_LOC_TYPE
                                   to_loc,                                            -- RETURN_LOC_ID
                                   NULL,                                              -- CURRENCY_CODE
                                   L_f_return_status,                                 -- STATUS
                                   NULL,                                              -- CANCEL REASON
                                   NULL,                                              -- COMMENTS
                                   L_f_return_detail_tbl)                             -- F_RETURN_DETAILS
      from tsfhead   
      where tsf_no = I_tsf_no;

   cursor C_GET_TSF_DETAIL is
      select OBJ_F_RETURN_DETAIL_REC( th.rma_no,                                      -- RMA_NO        
                                      td.item,                                        -- ITEM
                                      NULL,                                           -- WF_ORDER_NO
                                      DECODE(L_ship_tsf_ind,'N',NVL(td.tsf_qty,0),0), -- RETURNED_QTY
                                      'W',                                            -- RETURN_REASON  -- Externally Initiated RTV
                                      NULL,                                           -- RETURN_UNIT_COST
                                      NULL,                                           -- CANCEL_REASON
                                      NULL,                                           -- RESTOCK_TYPE
                                      NULL)                                           -- UNIT_RESTOCK_FEE
      from  tsfhead th,
            tsfdetail td
      where th.tsf_no = td.tsf_no
        and th.tsf_no = I_tsf_no;
       
BEGIN
   L_input_tsf_status := I_tsf_status;
   if I_tsf_no is NULL then
      L_inv_parm := 'I_tsf_no';
   elsif I_action_type is NULL then
      L_inv_parm := 'I_action_type';
   elsif I_tsf_status is NULL and I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE then
      L_inv_parm := 'I_tsf_status';
   elsif I_tsf_type is NULL and I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE then
      L_inv_parm := 'I_tsf_type';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_action_type not in (RMS_CONSTANTS.WF_ACTION_CREATE, RMS_CONSTANTS.WF_ACTION_UPDATE, RMS_CONSTANTS.WF_ACTION_SHIPPED)then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ACTION_TYPE',
                                            I_action_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   -- Get existing transfer status. Check its a valid transfer and also RMA number is populated for update. 
   L_table := 'TSFHEAD';
   SQL_LIB.SET_MARK('OPEN', 'C_GET_TSF_INFO', L_table, I_tsf_no);      
   open C_GET_TSF_INFO;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_TSF_INFO', L_table, I_tsf_no);      
   fetch C_GET_TSF_INFO into L_tsf_status,
                             L_from_loc,
                             L_from_loc_type,
                             L_to_loc,
                             L_to_loc_type,
                             L_rma_no;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_TSF_INFO', L_table, I_tsf_no);      
   close C_GET_TSF_INFO;
   
   if L_tsf_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF',
                                            I_tsf_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- If the action type is Shipped and no linked rma_no exists in tsfhead table, 
   -- this is a case of shipment created for no existing RMS transfer. Create the franchise
   -- return. The franchise return will be created with zero quantities to indicate this 
   -- is a post approval change in quantities.
   if L_rma_no is NULL and I_action_type = RMS_CONSTANTS.WF_ACTION_SHIPPED then
      L_action_type := RMS_CONSTANTS.WF_ACTION_CREATE;
      L_input_tsf_status := 'A';
      L_ship_tsf_ind     := 'Y';            
   elsif I_action_type = RMS_CONSTANTS.WF_ACTION_SHIPPED then        -- Treat shipment as regular update
      L_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;
   else
      L_action_type := I_action_type;
   end if;   
   
   -- Validate that for new return creation, the from-loc to-loc combination are correct. 
   if L_action_type = RMS_CONSTANTS.WF_ACTION_CREATE then 
      if WF_TRANSFER_SQL.DETERMINE_F_ORDRET_STATUS(O_error_message,
                                                   L_dummy_status,               
                                                   L_dummy_status,               
                                                   L_f_return_status,            -- F return status will be populated for franchise return.
                                                   L_from_loc,
                                                   L_from_loc_type,
                                                   L_to_loc,
                                                   L_to_loc_type,
                                                   L_input_tsf_status,
                                                   I_tsf_type) = FALSE then
         return FALSE;
      end if;
   end if;
      
   -- If a new transfer is created or an existing transfer with linked return is processed, go through the
   -- logic for franchise return maintenance. Else for a non franchise return do nothing and return success.
   if L_f_return_status is NOT NULL or L_rma_no is NOT NULL then

      -- Prepare the OBJ_F_RETURN_HEAD_REC.OBJ_F_RETURN_DETAIL_TBL object. 
      L_table := 'TSFHEAD, TSFDETAIL';
      SQL_LIB.SET_MARK('OPEN', 'C_GET_TSF_DETAIL', L_table, I_tsf_no);      
      open C_GET_TSF_DETAIL;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_TSF_DETAIL', L_table, I_tsf_no);      
      fetch C_GET_TSF_DETAIL BULK COLLECT into L_f_return_detail_tbl;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_TSF_DETAIL', L_table, I_tsf_no);      
      close C_GET_TSF_DETAIL;
      
      if L_action_type = RMS_CONSTANTS.WF_ACTION_UPDATE then
         L_table := 'WF_RETURN_HEAD';
         SQL_LIB.SET_MARK('OPEN', 'C_GET_WF_RETURN', L_table, L_rma_no);      
         open C_GET_WF_RETURN;
         SQL_LIB.SET_MARK('FETCH', 'C_GET_WF_RETURN', L_table, L_rma_no);      
         fetch C_GET_WF_RETURN into L_f_return_status, L_wf_return_type;
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_WF_RETURN', L_table, L_rma_no);      
         close C_GET_WF_RETURN;

         -- The franchise return status change will happen for 
         --   If franchaise return status is 'I' and input (intended) transfer status is 'A', approve franchise return.
         if L_input_tsf_status = 'A' and L_f_return_status = WF_STATUS_INPUT then
            L_f_return_status := WF_STATUS_APPROVE;
         end if;      
      end if;

      L_table := 'TSFHEAD';
      SQL_LIB.SET_MARK('OPEN', 'C_GET_TSF_HEAD', L_table, I_tsf_no);      
      open C_GET_TSF_HEAD;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_TSF_HEAD', L_table, I_tsf_no);      
      fetch C_GET_TSF_HEAD into L_f_return_head_rec;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_TSF_HEAD', L_table, I_tsf_no);      
      close C_GET_TSF_HEAD;

      if WF_CREATE_RETURN_SQL.PROCESS_F_RETURN(O_error_message,
                                               O_rma_no,
                                               L_f_return_head_rec,
                                               L_action_type) = FALSE then
         return FALSE;
      end if;

   end if; -- L_f_return_status and L_rma_no are not null
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SYNC_F_RETURN;
-----------------------------------------------------------------------------------------
FUNCTION CANCEL_F_ORDER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64)                  := 'WF_TRANSFER_SQL.CANCEL_F_ORDER';
   L_dummy_order_no           WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_f_order_head_rec         OBJ_F_ORDER_HEAD_REC          := OBJ_F_ORDER_HEAD_REC();
   L_table                    VARCHAR2(50)                  := NULL;

   cursor C_GET_TSF_HEAD is
      select OBJ_F_ORDER_HEAD_REC(wf_order_no,             -- wf_order_no
                                  NULL,                    -- Order_type
                                  NULL,                    -- Currency Code
                                  NULL,                    -- Status
                                  'ED',                    -- Cancel Reason  Externally Deleted. 
                                  NULL,                    -- Comments
                                  NULL)                    -- F_ORDER_DETAILS
        from tsfhead   
       where tsf_no = I_tsf_no
         and wf_order_no is NOT NULL;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   
   L_table := 'TSFHEAD';
   SQL_LIB.SET_MARK('OPEN', 'C_GET_TSF_HEAD', L_table, I_tsf_no);      
   open C_GET_TSF_HEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_TSF_HEAD', L_table, I_tsf_no);      
   fetch C_GET_TSF_HEAD into L_f_order_head_rec;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_TSF_HEAD', L_table, I_tsf_no);      
   close C_GET_TSF_HEAD;

   -- if the input transfer is linked to a franchaise order, cancel the linked franchise order.
   if L_f_order_head_rec is NOT NULL then
      if WF_CREATE_ORDER_SQL.PROCESS_F_ORDER(O_error_message,
                                             L_dummy_order_no, 
                                             L_f_order_head_rec,
                                             RMS_CONSTANTS.WF_ACTION_CANCEL) = FALSE then
         return FALSE;
      end if;
   end if;
         
   return TRUE;
   
EXCEPTION
   when OTHERS then
      if C_GET_TSF_HEAD%ISOPEN then
         close C_GET_TSF_HEAD;
      end if;
   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CANCEL_F_ORDER;
-----------------------------------------------------------------------------------
FUNCTION CANCEL_F_RETURN (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64)                   := 'WF_TRANSFER_SQL.CANCEL_F_RETURN';
   L_dummy_rma_no             WF_RETURN_HEAD.RMA_NO%TYPE;
   L_f_return_head_rec        OBJ_F_RETURN_HEAD_REC          := OBJ_F_RETURN_HEAD_REC();
   L_table                    VARCHAR2(50)                   := NULL;
  
   cursor C_GET_TSF_HEAD is
      select OBJ_F_RETURN_HEAD_REC(rma_no,                  -- RMA_NO
                                   NULL,                    -- CUST_RET_REF_NO
                                   NULL,                    -- RETURN_TYPE   
                                   NULL,                    -- CUSTOMER_LOC
                                   NULL,                    -- RETURN_METHOD
                                   NULL,                    -- RETURN_LOC_TYPE
                                   NULL,                    -- RETURN_LOC_ID
                                   NULL,                    -- CURRENCY_CODE
                                   NULL,                    -- STATUS
                                   'ED',                    -- CANCEL REASON  Externally Deleted.
                                   NULL,                    -- COMMENTS
                                   NULL)                    -- F_RETURN_DETAILS
        from tsfhead   
       where tsf_no = I_tsf_no
         and rma_no is NOT NULL;
        
BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   L_table := 'TSFHEAD';
   SQL_LIB.SET_MARK('OPEN', 'C_GET_TSF_HEAD', L_table, I_tsf_no);      
   open C_GET_TSF_HEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_TSF_HEAD', L_table, I_tsf_no);      
   fetch C_GET_TSF_HEAD into L_f_return_head_rec;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_TSF_HEAD', L_table, I_tsf_no);      
   close C_GET_TSF_HEAD;
   
   -- if the input transfer is linked to a franchise return, cancel the linked franchise return.
   if L_f_return_head_rec is NOT NULL then
      if WF_CREATE_RETURN_SQL.PROCESS_F_RETURN(O_error_message,
                                               L_dummy_rma_no,
                                               L_f_return_head_rec,
                                               RMS_CONSTANTS.WF_ACTION_CANCEL) = FALSE then
         return FALSE;
      end if;
   end if;
   return TRUE;
   
EXCEPTION
   when OTHERS then
      if C_GET_TSF_HEAD%ISOPEN then
         close C_GET_TSF_HEAD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CANCEL_F_RETURN;
-----------------------------------------------------------------------------------
FUNCTION GET_WF_ORDER_RMA_NO(O_error_message        IN OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                             O_wf_order_no          IN OUT      WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                             O_rma_no               IN OUT      WF_RETURN_HEAD.RMA_NO%TYPE,
                             I_distro_type          IN          SHIPSKU.DISTRO_TYPE%TYPE,
                             I_distro_no            IN          SHIPSKU.DISTRO_NO%TYPE,
                             I_to_loc               IN          TSFHEAD.TO_LOC%TYPE,
                             I_to_loc_type          IN          TSFHEAD.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(61) := 'WF_TRANSFER_SQL.GET_WF_ORDER_RMA_NO';

   cursor C_GET_ORD_RMA_NO is
      select th.wf_order_no,
             th.rma_no
        from tsfhead th
       where th.tsf_no = I_distro_no
         and I_distro_type = 'T'
      union all
      select ad.wf_order_no,
             NULL rma_no
        from alloc_detail ad
       where ad.alloc_no = I_distro_no
         and I_distro_type = 'A'
         and ad.to_loc = I_to_loc
         and ad.to_loc_type = I_to_loc_type;
BEGIN
   if I_distro_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_distro_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_distro_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_distro_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_distro_type NOT IN ('A', 'T') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO_TYPE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_GET_ORD_RMA_NO;
   fetch C_GET_ORD_RMA_NO into O_wf_order_no,
                               O_rma_no;
   ---
   if C_GET_ORD_RMA_NO%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_FO_FR_EXISTS',
                                            I_distro_no,
                                            I_distro_type,
                                            NULL);
      close C_GET_ORD_RMA_NO;                                      
      return FALSE;
   end if;
   ---
   close C_GET_ORD_RMA_NO;   

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_WF_ORDER_RMA_NO;
-----------------------------------------------------------------------------------
FUNCTION BULK_SYNC_F_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_action_type     IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'WF_TRANSFER_SQL.BULK_SYNC_F_ORDER';

   TYPE tsf_wf_order_typ IS RECORD (TSF_NO        TSFHEAD.TSF_NO%TYPE,
                                    TSF_STATUS    TSFHEAD.STATUS%TYPE,
                                    WF_ORDER_NO   WF_ORDER_HEAD.WF_ORDER_NO%TYPE);

   TYPE tsf_no_tbl     IS TABLE OF TSFHEAD.TSF_NO%TYPE;
   TYPE tsf_status_tbl IS TABLE OF TSFHEAD.STATUS%TYPE;
   TYPE tsf_info_tbl   IS TABLE OF TSF_WF_ORDER_TYP INDEX BY BINARY_INTEGER;

   L_tsf_info_tbl     TSF_INFO_TBL;
   L_tsf_no_tbl       TSF_NO_TBL;
   L_tsf_status_tbl   TSF_STATUS_TBL;
   L_action_type      VARCHAR2(10);
   L_tsf_status_out   TSFHEAD.STATUS%TYPE   := NULL;

   -- Get the transfer# created by the replenishment batch to create/update the franchise order. 
   -- Incase the action type is create and frachise order already exists with order type not in
   -- Manual or EDI, filter the record. 
   cursor C_GET_TRANSFERS is
      select th.tsf_no,
             th.status,
             th.wf_order_no
        from tsfhead th,
             store s
       where th.repl_tsf_approve_ind = 'N'
         and th.status      in ('A','B')
         and th.create_id   = 'BATCH'
         and th.to_loc      = s.store
         and s.store_type   = 'F'
         and not exists (select 'x'
                           from wf_order_head woh
                          where th.wf_order_no = woh.wf_order_no
                            and woh.order_type not in ('M','E')
                            and I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE)
         for update of th.tsf_no nowait;

BEGIN

   if I_action_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_action_type not in (RMS_CONSTANTS.WF_ACTION_CREATE,
                            RMS_CONSTANTS.WF_ACTION_UPDATE,
                            RMS_CONSTANTS.WF_ACTION_SHIPPED) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ACTION_TYPE',
                                            I_action_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   LP_table := 'TSFHEAD';
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TRANSFERS',
                    LP_table,
                    NULL);
   open C_GET_TRANSFERS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TRANSFERS',
                    LP_table,
                    NULL);
   fetch C_GET_TRANSFERS BULK COLLECT into L_tsf_info_tbl;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TRANSFERS',
                    LP_table,
                    NULL);
   close C_GET_TRANSFERS;

   -- Loop through the fetched collection and call SYNC_F_ORDER.
   if L_tsf_info_tbl is NOT NULL and L_tsf_info_tbl.COUNT > 0 then
      for i in L_tsf_info_tbl.FIRST..L_tsf_info_tbl.LAST LOOP
      
         -- Manual or EDI franchise order can initiate a store order request. The transfer created because
         -- of this store order should sync the original franchise and not create a new franchise order.
         if I_action_type = RMS_CONSTANTS.WF_ACTION_CREATE and L_tsf_info_tbl(i).wf_order_no is not NULL then
            L_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;
         else
            L_action_type := I_action_type;
         end if;
         
         if WF_TRANSFER_SQL.SYNC_F_ORDER (O_error_message,
                                          L_tsf_info_tbl(i).wf_order_no,
                                          L_tsf_status_out,
                                          L_action_type,
                                          L_tsf_info_tbl(i).tsf_no,
                                          L_tsf_info_tbl(i).tsf_status,
                                          'FO') = FALSE then
            return FALSE;
         end if;
      end LOOP;

      -- Update tsfhead
      FORALL i in L_tsf_info_tbl.FIRST..L_tsf_info_tbl.LAST
         update tsfhead
            set wf_order_no = L_tsf_info_tbl(i).wf_order_no
          where tsf_no      = L_tsf_info_tbl(i).tsf_no
            and wf_order_no is NULL;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BULK_SYNC_F_ORDER;
-----------------------------------------------------------------------------------
END WF_TRANSFER_SQL;
/