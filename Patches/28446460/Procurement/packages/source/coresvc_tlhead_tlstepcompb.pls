create or replace PACKAGE BODY CORESVC_TLHEAD_TLSTEPCOMP AS
   cursor C_SVC_TIMELINE_STEPS(I_process_id NUMBER,
                               I_chunk_id NUMBER) is
      select t.pk_timeline_steps_rid,
             t.tst_tlh_fk_rid,
             t.tst_tsc_fk_rid,
             t.stts_rid,
             t.cd_timetype_rid,
             t.cd_timebase_rid,
             t.cd_timetypes_rid,
             t.stts_display_seq,
             t.stts_days_completed,
             UPPER(t.stts_timeline_type) as stts_timeline_type,
             t.stts_step_no,
             t.stts_timeline_no,
             t.stts_process_id,
             t.stts_row_seq,
             t.stts_chunk_id,
             t.stts_action,
             t.stts_process$status,
             LEAD(t.stth_timeline_no, 1, 0) over (order by t.stth_timeline_no) as next_stth_timeline_no,
             ROW_NUMBER() over (partition by t.stth_timeline_no
                                order by t.stth_timeline_no)                   as th_rank,
             t.stth_rid,
             t.stth_timeline_no,
             t.stth_timeline_base,
             UPPER(t.stth_timeline_type) as stth_timeline_type,
             t.old_timeline_type,
             t.stth_timeline_desc,
             t.stth_process_id,
             t.stth_row_seq,
             t.stth_chunk_id,
             t.stth_action,
             t.stth_process$status
        from
           (select pk_timeline_steps.rowid      as pk_timeline_steps_rid,
                   stts.rowid                   as stts_rid,
                   stth.rowid                   as stth_rid,
                   tst_tlh_fk.rowid             as tst_tlh_fk_rid,
                   tst_tsc_fk.rowid             as tst_tsc_fk_rid,
                   cd_timetype.rowid            as cd_timetype_rid,
                   cd_timebase.rowid            as cd_timebase_rid,
                   cd_timetype_step.rowid       as cd_timetypes_rid,
                   stts.display_seq             as stts_display_seq,
                   stts.days_completed          as stts_days_completed,
                   UPPER(stts.timeline_type)    as stts_timeline_type,
                   stts.step_no                 as stts_step_no,
                   stts.timeline_no             as stts_timeline_no,
                   stts.process_id              as stts_process_id,
                   stts.row_seq                 as stts_row_seq,
                   stts.chunk_id                as stts_chunk_id,
                   UPPER(stts.action)           as stts_action,
                   stts.process$status          as stts_process$status,
                   stth.timeline_no             as stth_timeline_no,
                   stth.timeline_base           as stth_timeline_base,
                   UPPER(stth.timeline_type)    as stth_timeline_type,
                   tst_tlh_fk.timeline_type     as old_timeline_type,
                   stth.timeline_desc           as stth_timeline_desc,
                   stth.process_id              as stth_process_id,
                   stth.row_seq                 as stth_row_seq,
                   stth.chunk_id                as stth_chunk_id,
                   UPPER(stth.action)           as stth_action,
                   stth.process$status          as stth_process$status
              from svc_timeline_head stth,
                   svc_timeline_steps stts,
                   timeline_steps pk_timeline_steps,
                   timeline_head tst_tlh_fk,
                   timeline_step_comp tst_tsc_fk,
                   code_detail  cd_timetype,
                   code_detail  cd_timebase,
                   code_detail  cd_timetype_step
             where stth.process_id            = I_process_id
               and stth.chunk_id              = I_chunk_id
               and stth.process_id            = stts.process_id (+)
               and stth.chunk_id              = stts.chunk_id (+)
               and stth.timeline_no           = stts.timeline_no(+)
               and stth.timeline_type         = stts.timeline_type(+)
               and stth.timeline_no           = tst_tlh_fk.timeline_no (+)
               and UPPER(stts.timeline_type)  = pk_timeline_steps.timeline_type (+)
               and stts.step_no               = pk_timeline_steps.step_no (+)
               and stts.timeline_no           = pk_timeline_steps.timeline_no (+)
               and stts.step_no               = tst_tsc_fk.step_no (+)
               and UPPER(stts.timeline_type)  = tst_tsc_fk.timeline_type (+)
               and stth.timeline_type         = cd_timetype.code (+)
               and cd_timetype.code_type (+)  = 'TMLN'
               and stth.timeline_base         = cd_timebase.code (+)
               and cd_timebase.code_type (+)  = 'TLBC'
               and stts.timeline_type         = cd_timetype_step.code (+)
               and cd_timetype_step.code_type (+)   = 'TMLN'
               and stth.timeline_type is not NULL
         UNION ALL
            select pk_timeline_steps.rowid      as pk_timeline_steps_rid,
                   stts.rowid                   as stts_rid,
                   stth.rowid                   as stth_rid,
                   tst_tlh_fk.rowid             as tst_tlh_fk_rid,
                   tst_tsc_fk.rowid             as tst_tsc_fk_rid,
                   cd_timetype.rowid            as cd_timetype_rid,
                   cd_timebase.rowid            as cd_timebase_rid,
                   cd_timetype_step.rowid       as cd_timetypes_rid,
                   stts.display_seq             as stts_display_seq,
                   stts.days_completed          as stts_days_completed,
                   UPPER(stts.timeline_type)    as stts_timeline_type,
                   stts.step_no                 as stts_step_no,
                   stts.timeline_no             as stts_timeline_no,
                   stts.process_id              as stts_process_id,
                   stts.row_seq                 as stts_row_seq,
                   stts.chunk_id                as stts_chunk_id,
                   UPPER(stts.action)           as stts_action,
                   stts.process$status          as stts_process$status,
                   stth.timeline_no             as stth_timeline_no,
                   stth.timeline_base           as stth_timeline_base,
                   NVL(UPPER(stth.timeline_type),tst_tlh_fk.timeline_type)    as stth_timeline_type,
                   tst_tlh_fk.timeline_type     as old_timeline_type,
                   stth.timeline_desc           as stth_timeline_desc,
                   stth.process_id              as stth_process_id,
                   stth.row_seq                 as stth_row_seq,
                   stth.chunk_id                as stth_chunk_id,
                   UPPER(stth.action)           as stth_action,
                   stth.process$status          as stth_process$status
              from svc_timeline_head stth,
                   svc_timeline_steps stts,
                   timeline_steps pk_timeline_steps,
                   timeline_head tst_tlh_fk,
                   timeline_step_comp tst_tsc_fk,
                   code_detail  cd_timetype,
                   code_detail  cd_timebase,
                   code_detail  cd_timetype_step
             where stth.process_id            = I_process_id
               and stth.chunk_id              = I_chunk_id
               and stth.process_id            = stts.process_id (+)
               and stth.chunk_id              = stts.chunk_id (+)
               and stth.timeline_no           = stts.timeline_no(+)
               and stth.timeline_no           = tst_tlh_fk.timeline_no (+)
               and UPPER(stts.timeline_type)  = pk_timeline_steps.timeline_type (+)
               and stts.step_no               = pk_timeline_steps.step_no (+)
               and stts.timeline_no           = pk_timeline_steps.timeline_no (+)
               and stts.step_no               = tst_tsc_fk.step_no (+)
               and UPPER(stts.timeline_type)  = tst_tsc_fk.timeline_type (+)
               and stth.timeline_type         = cd_timetype.code (+)
               and cd_timetype.code_type (+)  = 'TMLN'
               and stth.timeline_base         = cd_timebase.code (+)
               and cd_timebase.code_type (+)  = 'TLBC'
               and stts.timeline_type         = cd_timetype_step.code (+)
               and cd_timetype_step.code_type (+)   = 'TMLN'
               and stth.timeline_type is NULL
         UNION ALL
            select pk_timeline_steps.rowid      as pk_timeline_steps_rid,
                   stts.rowid                   as stts_rid,
                   stth.rowid                   as stth_rid,
                   tst_tlh_fk.rowid             as tst_tlh_fk_rid,
                   tst_tsc_fk.rowid             as tst_tsc_fk_rid,
                   cd_timetype.rowid            as cd_timetype_rid,
                   cd_timebase.rowid            as cd_timebase_rid,
                   cd_timetype_step.rowid       as cd_timetypes_rid,
                   stts.display_seq             as stts_display_seq,
                   stts.days_completed          as stts_days_completed,
                   UPPER(stts.timeline_type)    as stts_timeline_type,
                   stts.step_no                 as stts_step_no,
                   stts.timeline_no             as stts_timeline_no,
                   stts.process_id              as stts_process_id,
                   stts.row_seq                 as stts_row_seq,
                   stts.chunk_id                as stts_chunk_id,
                   UPPER(stts.action)           as stts_action,
                   stts.process$status          as stts_process$status,
                   NVL(stth.timeline_no, tst_tlh_fk.timeline_no)             as stth_timeline_no,
                   stth.timeline_base           as stth_timeline_base,
                   UPPER(stth.timeline_type)    as stth_timeline_type,
                   tst_tlh_fk.timeline_type     as old_timeline_type,
                   stth.timeline_desc           as stth_timeline_desc,
                   stth.process_id              as stth_process_id,
                   stth.row_seq                 as stth_row_seq,
                   stth.chunk_id                as stth_chunk_id,
                   UPPER(stth.action)           as stth_action,
                   stth.process$status          as stth_process$status
              from svc_timeline_steps stts,
                   svc_timeline_head stth,
                   timeline_steps pk_timeline_steps,
                   timeline_head tst_tlh_fk,
                   timeline_step_comp tst_tsc_fk,
                   code_detail  cd_timetype,
                   code_detail  cd_timebase,
                   code_detail  cd_timetype_step
             where stts.process_id           = I_process_id
               and stts.chunk_id             = I_chunk_id
               and stts.process_id           = stth.process_id (+)
               and stts.chunk_id             = stth.chunk_id (+)
               and stts.timeline_no          = stth.timeline_no (+)
               and stts.timeline_no          = tst_tlh_fk.timeline_no (+)
               and UPPER(stts.timeline_type) = pk_timeline_steps.timeline_type (+)
               and stts.step_no              = pk_timeline_steps.step_no (+)
               and stts.timeline_no          = pk_timeline_steps.timeline_no (+)
               and stts.step_no              = tst_tsc_fk.step_no (+)
               and UPPER(stts.timeline_type) = tst_tsc_fk.timeline_type (+)
               and stth.timeline_type         = cd_timetype.code (+)
               and cd_timetype.code_type (+)  = 'TMLN'
               and stth.timeline_base         = cd_timebase.code (+)
               and cd_timebase.code_type (+)  = 'TLBC'
               and stts.timeline_type         = cd_timetype_step.code (+)
               and cd_timetype_step.code_type (+)   = 'TMLN'
               and stth.timeline_no is NULL
           ) t;
           
   cursor C_SVC_TIMELINE_STEP_COMP(I_process_id NUMBER,
                                   I_chunk_id   NUMBER) is
      select pk_timeline_step_comp.rowid  as pk_timeline_step_comp_rid,
             st.rowid                     as st_rid,
             st.step_desc,
             st.step_no,
             st.timeline_type,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)             as action,
             st.process$status
        from svc_timeline_step_comp st,
             timeline_step_comp pk_timeline_step_comp,
             dual
       where st.process_id    = I_process_id
         and st.chunk_id      = I_chunk_id
         and st.step_no       = pk_timeline_step_comp.step_no (+)
         and st.timeline_type = pk_timeline_step_comp.timeline_type (+);

   TYPE errors_tab_typ is TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ is TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;

   TYPE TIMELINE_HEAD_TAB is TABLE OF TIMELINE_HEAD_TL%ROWTYPE;
   TYPE TIMELINE_STEP_COMP_TAB is TABLE OF TIMELINE_STEP_COMP_TL%ROWTYPE;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;

   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.EXISTS(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := S9T_ERRORS_SEQ.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   NUMBER) IS
   L_sheets                  S9T_PKG.names_map_typ;
   TIMELINE_STEPS_cols       S9T_PKG.names_map_typ;
   TIMELINE_HEAD_cols        S9T_PKG.names_map_typ;
   TIMELINE_HEAD_TL_cols     S9T_PKG.names_map_typ;
   TIMELINE_STEP_COMP_cols   S9T_PKG.NAMES_MAP_TYP;
   TSTEP_COMP_TL_cols        S9T_PKG.NAMES_MAP_TYP;
BEGIN
   L_sheets                         := S9T_PKG.GET_SHEET_NAMES(I_file_id);   
   TIMELINE_STEPS_cols              := S9T_PKG.GET_COL_NAMES(I_file_id,TIMELINE_STEPS_sheet);
   TIMELINE_STEPS$Action            := TIMELINE_STEPS_cols('ACTION');
   TIMELINE_STEPS$DISPLAY_SEQ       := TIMELINE_STEPS_cols('DISPLAY_SEQ');
   TIMELINE_STEPS$DAYS_COMPLETED    := TIMELINE_STEPS_cols('DAYS_COMPLETED');
   TIMELINE_STEPS$TIMELINE_TYPE     := TIMELINE_STEPS_cols('TIMELINE_TYPE');
   TIMELINE_STEPS$STEP_NO           := TIMELINE_STEPS_cols('STEP_NO');
   TIMELINE_STEPS$TIMELINE_NO       := TIMELINE_STEPS_cols('TIMELINE_NO');

   TIMELINE_HEAD_cols               := S9T_PKG.GET_COL_NAMES(I_file_id,TIMELINE_HEAD_sheet);
   TIMELINE_HEAD$Action             := TIMELINE_HEAD_cols('ACTION');
   TIMELINE_HEAD$TIMELINE_NO        := TIMELINE_HEAD_cols('TIMELINE_NO');
   TIMELINE_HEAD$TIMELINE_BASE      := TIMELINE_HEAD_cols('TIMELINE_BASE');
   TIMELINE_HEAD$TIMELINE_TYPE      := TIMELINE_HEAD_cols('TIMELINE_TYPE');
   TIMELINE_HEAD$TIMELINE_DESC      := TIMELINE_HEAD_cols('TIMELINE_DESC');

   TIMELINE_HEAD_TL_cols            := S9T_PKG.GET_COL_NAMES(I_file_id,TIMELINE_HEAD_TL_sheet);
   TIMELINE_HEAD_TL$Action          := TIMELINE_HEAD_TL_cols('ACTION');
   TIMELINE_HEAD_TL$LANG            := TIMELINE_HEAD_TL_cols('LANG');
   TIMELINE_HEAD_TL$TIMELINE_NO     := TIMELINE_HEAD_TL_cols('TIMELINE_NO');
   TIMELINE_HEAD_TL$TIMELINE_DESC   := TIMELINE_HEAD_TL_cols('TIMELINE_DESC');
   
   TIMELINE_STEP_COMP_cols          := S9T_PKG.GET_COL_NAMES(I_file_id,TIMELINE_STEP_COMP_sheet);
   TIMELINE_STEP_COMP$Action        := TIMELINE_STEP_COMP_cols('ACTION');
   TIMELINE_STEP_COMP$STEP_DESC     := TIMELINE_STEP_COMP_cols('STEP_DESC');
   TIMELINE_STEP_COMP$STEP_NO       := TIMELINE_STEP_COMP_cols('STEP_NO');
   TSTEP_COMP$TIMELINE_TYPE         := TIMELINE_STEP_COMP_cols('TIMELINE_TYPE');

   TSTEP_COMP_TL_cols               := S9T_PKG.GET_COL_NAMES(I_file_id,TSTEP_COMP_TL_sheet);
   TSTEP_COMP_TL$Action             := TSTEP_COMP_TL_cols('ACTION');
   TSTEP_COMP_TL$LANG               := TSTEP_COMP_TL_cols('LANG');
   TSTEP_COMP_TL$STEP_DESC          := TSTEP_COMP_TL_cols('STEP_DESC');
   TSTEP_COMP_TL$STEP_NO            := TSTEP_COMP_TL_cols('STEP_NO');
   TSTEP_COMP_TL$TIMELINE_TYPE      := TSTEP_COMP_TL_cols('TIMELINE_TYPE');
END POPULATE_NAMES;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_TIMELINE_STEPS(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = TIMELINE_STEPS_sheet)
   select s9t_row(s9t_cells(CORESVC_TLHEAD_TLSTEPCOMP.action_mod,
                            timeline_no,
                            step_no,
                            timeline_type,
                            days_completed,
                            display_seq))
     from timeline_steps;
END POPULATE_TIMELINE_STEPS;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_TIMELINE_HEAD(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = TIMELINE_HEAD_sheet)
   select s9t_row(s9t_cells(CORESVC_TLHEAD_TLSTEPCOMP.action_mod,
                            timeline_no,
                            timeline_desc,
                            timeline_type,
                            timeline_base))
     from timeline_head;
END POPULATE_TIMELINE_HEAD;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_TIMELINE_HEAD_TL(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = TIMELINE_HEAD_TL_sheet)
   select s9t_row(s9t_cells(CORESVC_TLHEAD_TLSTEPCOMP.action_mod,
                            lang,
                            timeline_no,
                            timeline_desc))
     from timeline_head_tl;
END POPULATE_TIMELINE_HEAD_TL;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_TIMELINE_STEP_COMP(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = TIMELINE_STEP_COMP_sheet)
   select s9t_row(s9t_cells(CORESVC_TLHEAD_TLSTEPCOMP.action_mod,
                            timeline_type,
                            step_no,
                            step_desc))
     from timeline_step_comp;
END POPULATE_TIMELINE_STEP_COMP;
--------------------------------------------------------------------------------------
PROCEDURE POPULATE_TSTEP_COMP_TL(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = TSTEP_COMP_TL_sheet)
   select s9t_row(s9t_cells(CORESVC_TLHEAD_TLSTEPCOMP.action_mod,
                            lang,
                            timeline_type,
                            step_no,
                            step_desc))
     from timeline_step_comp_tl;
END POPULATE_TSTEP_COMP_TL;
--------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file        S9T_FILE;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;

   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(TIMELINE_STEPS_sheet);
   L_file.sheets(l_file.get_sheet_index(TIMELINE_STEPS_sheet)).column_headers := s9t_cells('ACTION',
                                                                                           'TIMELINE_NO',
                                                                                           'STEP_NO',
                                                                                           'TIMELINE_TYPE',
                                                                                           'DAYS_COMPLETED',
                                                                                           'DISPLAY_SEQ');

   L_file.add_sheet(TIMELINE_HEAD_sheet);
   L_file.sheets(l_file.get_sheet_index(TIMELINE_HEAD_sheet)).column_headers := s9t_cells('ACTION',
                                                                                          'TIMELINE_NO',
                                                                                          'TIMELINE_DESC',
                                                                                          'TIMELINE_TYPE',
                                                                                          'TIMELINE_BASE');

   L_file.add_sheet(TIMELINE_HEAD_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(TIMELINE_HEAD_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                             'LANG',
                                                                                             'TIMELINE_NO',
                                                                                             'TIMELINE_DESC');                                                                                         

   L_file.add_sheet(TIMELINE_STEP_COMP_sheet);
   L_file.sheets(l_file.get_sheet_index(TIMELINE_STEP_COMP_sheet)).column_headers := s9t_cells('ACTION',
                                                                                               'TIMELINE_TYPE',
                                                                                               'STEP_NO',
                                                                                               'STEP_DESC');

   L_file.add_sheet(TSTEP_COMP_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(TSTEP_COMP_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                          'LANG',
                                                                                          'TIMELINE_TYPE',
                                                                                          'STEP_NO',
                                                                                          'STEP_DESC');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_TLHEAD_TLSTEPCOMP.CREATE_S9T';
   L_file      S9T_FILE;

BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_TIMELINE_STEPS(O_file_id);
      POPULATE_TIMELINE_HEAD(O_file_id);
      POPULATE_TIMELINE_HEAD_TL(O_file_id);
      POPULATE_TIMELINE_STEP_COMP(O_file_id);
      POPULATE_TSTEP_COMP_TL(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TIMELINE_STEPS(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                     I_process_id   IN   SVC_TIMELINE_STEPS.PROCESS_ID%TYPE) IS

   TYPE svc_TIMELINE_STEPS_col_typ is TABLE OF SVC_TIMELINE_STEPS%ROWTYPE;
   L_temp_rec               SVC_TIMELINE_STEPS%ROWTYPE;
   svc_TIMELINE_STEPS_col   svc_TIMELINE_STEPS_col_typ := NEW svc_TIMELINE_STEPS_col_typ();
   L_process_id             SVC_TIMELINE_STEPS.PROCESS_ID%TYPE;
   L_error                  BOOLEAN                    := FALSE;
   L_default_rec            SVC_TIMELINE_STEPS%ROWTYPE;

   cursor C_MANDATORY_IND is
      select DISPLAY_SEQ_mi,
             DAYS_COMPLETED_mi,
             TIMELINE_TYPE_mi,
             STEP_NO_mi,
             TIMELINE_NO_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key              = template_key
                 and wksht_key                 = 'TIMELINE_STEPS')
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('DISPLAY_SEQ'    as DISPLAY_SEQ,
                                                                'DAYS_COMPLETED' as DAYS_COMPLETED,
                                                                'TIMELINE_TYPE'  as TIMELINE_TYPE,
                                                                'STEP_NO'        as STEP_NO,
                                                                'TIMELINE_NO'    as TIMELINE_NO));
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)   := 'SVC_TIMELINE_STEPS';
   L_pk_columns   VARCHAR2(255)  := 'Timeline Number,Step Number,Timeline Type';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   --Get default values.
   FOR rec IN (select DISPLAY_SEQ_dv,
                      DAYS_COMPLETED_dv,
                      TIMELINE_TYPE_dv,
                      STEP_NO_dv,
                      TIMELINE_NO_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key     = template_key
                          and wksht_key        = 'TIMELINE_STEPS')
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('DISPLAY_SEQ'    as DISPLAY_SEQ,
                                                                             'DAYS_COMPLETED' as DAYS_COMPLETED,
                                                                             'TIMELINE_TYPE'  as TIMELINE_TYPE,
                                                                             'STEP_NO'        as STEP_NO,
                                                                             'TIMELINE_NO'    as TIMELINE_NO)))
   LOOP
      BEGIN
         L_default_rec.display_seq := rec.display_seq_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TIMELINE_STEPS',
                            NULL,
                            'DISPLAY_SEQ',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.days_completed := rec.days_completed_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TIMELINE_STEPS',
                            NULL,
                            'DAYS_COMPLETED',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.timeline_type := rec.timeline_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TIMELINE_STEPS',
                            NULL,
                            'TIMELINE_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.step_no := rec.step_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TIMELINE_STEPS',
                            NULL,
                            'STEP_NO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.timeline_no := rec.timeline_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TIMELINE_STEPS',
                            NULL,
                            'TIMELINE_NO',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(TIMELINE_STEPS$Action)                 as Action,
                      r.get_cell(TIMELINE_STEPS$DISPLAY_SEQ)            as DISPLAY_SEQ,
                      r.get_cell(TIMELINE_STEPS$DAYS_COMPLETED)         as DAYS_COMPLETED,
                      UPPER(r.get_cell(TIMELINE_STEPS$TIMELINE_TYPE))   as TIMELINE_TYPE,
                      r.get_cell(TIMELINE_STEPS$STEP_NO)                as STEP_NO,
                      r.get_cell(TIMELINE_STEPS$TIMELINE_NO)            as TIMELINE_NO,
                      r.get_row_seq()                                   as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id    = I_file_id
                  and ss.sheet_name = sheet_name_trans(TIMELINE_STEPS_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_STEPS_sheet,
                               rec.row_seq,
                               action_column,
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.display_seq := rec.display_seq;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_STEPS_sheet,
                               rec.row_seq,
                               'DISPLAY_SEQ',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.days_completed := rec.days_completed;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_STEPS_sheet,
                               rec.row_seq,
                               'DAYS_COMPLETED',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.timeline_type := rec.timeline_type;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_STEPS_sheet,
                               rec.row_seq,
                               'TIMELINE_TYPE',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.step_no := rec.step_no;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_STEPS_sheet,
                               rec.row_seq,
                               'STEP_NO',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.timeline_no := rec.timeline_no;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_STEPS_sheet,
                               rec.row_seq,
                               'TIMELINE_NO',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;

      if rec.action = CORESVC_TLHEAD_TLSTEPCOMP.action_new then
         L_temp_rec.display_seq    := NVL(L_temp_rec.display_seq,L_default_rec.display_seq);
         L_temp_rec.days_completed := NVL(L_temp_rec.days_completed,L_default_rec.days_completed);
         L_temp_rec.timeline_type  := NVL(L_temp_rec.timeline_type,L_default_rec.timeline_type);
         L_temp_rec.step_no        := NVL(L_temp_rec.step_no,L_default_rec.step_no);
         L_temp_rec.timeline_no    := NVL(L_temp_rec.timeline_no,L_default_rec.timeline_no);
      end if;
      if rec.action in (CORESVC_TLHEAD_TLSTEPCOMP.action_mod,CORESVC_TLHEAD_TLSTEPCOMP.action_del) then
         if NOT (L_temp_rec.timeline_type is NOT NULL and
                 L_temp_rec.step_no       is NOT NULL and
                 L_temp_rec.timeline_no   is NOT NULL) then
            WRITE_S9T_ERROR(I_file_id,
                            TIMELINE_STEPS_sheet,
                            rec.row_seq,
                            NULL,
                            NULL,
                            SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
            L_error := TRUE;
         end if;
      end if;
      if NOT L_error then
         svc_TIMELINE_STEPS_col.extend();
         svc_TIMELINE_STEPS_col(svc_TIMELINE_STEPS_col.COUNT()) := L_temp_rec;
      end if;

   END LOOP;
   BEGIN
      FORALL i IN 1..svc_TIMELINE_STEPS_col.COUNT SAVE EXCEPTIONS
      merge into svc_timeline_steps st
      using(select
                  (case
                   when L_mi_rec.DISPLAY_SEQ_mi    = 'N'
                    and svc_TIMELINE_STEPS_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.DISPLAY_SEQ IS NULL
                   then mt.DISPLAY_SEQ
                   else s1.DISPLAY_SEQ
                   end) as DISPLAY_SEQ,
                  (case
                   when L_mi_rec.DAYS_COMPLETED_mi    = 'N'
                    and svc_TIMELINE_STEPS_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.DAYS_COMPLETED IS NULL
                   then mt.DAYS_COMPLETED
                   else s1.DAYS_COMPLETED
                   end) as DAYS_COMPLETED,
                  (case
                   when L_mi_rec.TIMELINE_TYPE_mi    = 'N'
                    and svc_TIMELINE_STEPS_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.TIMELINE_TYPE IS NULL
                   then mt.TIMELINE_TYPE
                   else s1.TIMELINE_TYPE
                   end) as TIMELINE_TYPE,
                  (case
                   when L_mi_rec.STEP_NO_mi    = 'N'
                    and svc_TIMELINE_STEPS_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.STEP_NO IS NULL
                   then mt.STEP_NO
                   else s1.STEP_NO
                   end) as STEP_NO,
                  (case
                   when L_mi_rec.TIMELINE_NO_mi    = 'N'
                    and svc_TIMELINE_STEPS_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.TIMELINE_NO IS NULL
                   then mt.TIMELINE_NO
                   else s1.TIMELINE_NO
                   end) as TIMELINE_NO
              from (select svc_TIMELINE_STEPS_col(i).DISPLAY_SEQ    as DISPLAY_SEQ,
                           svc_TIMELINE_STEPS_col(i).DAYS_COMPLETED as DAYS_COMPLETED,
                           svc_TIMELINE_STEPS_col(i).TIMELINE_TYPE  as TIMELINE_TYPE,
                           svc_TIMELINE_STEPS_col(i).STEP_NO        as STEP_NO,
                           svc_TIMELINE_STEPS_col(i).TIMELINE_NO    as TIMELINE_NO
                      from dual)         s1,
                   timeline_steps mt
             where mt.TIMELINE_TYPE (+) = s1.TIMELINE_TYPE  and
                   mt.STEP_NO (+)       = s1.STEP_NO        and
                   mt.TIMELINE_NO (+)   = s1.TIMELINE_NO)sq
                on ( st.TIMELINE_TYPE   = sq.TIMELINE_TYPE  and
                     st.STEP_NO         = sq.STEP_NO        and
                     st.TIMELINE_NO     = sq.TIMELINE_NO    and
                     svc_TIMELINE_STEPS_col(i).ACTION IN (CORESVC_TLHEAD_TLSTEPCOMP.action_mod,CORESVC_TLHEAD_TLSTEPCOMP.action_del))
      when matched then
         update
            set process_id      = svc_TIMELINE_STEPS_col(i).process_id,
                chunk_id        = svc_TIMELINE_STEPS_col(i).chunk_id,
                row_seq         = svc_TIMELINE_STEPS_col(i).row_seq,
                action          = svc_TIMELINE_STEPS_col(i).action,
                process$status  = svc_TIMELINE_STEPS_col(i).process$status,
                days_completed  = sq.days_completed,
                display_seq     = sq.display_seq,
                create_id       = svc_TIMELINE_STEPS_col(i).create_id,
                create_datetime = svc_TIMELINE_STEPS_col(i).create_datetime,
                last_upd_id     = svc_TIMELINE_STEPS_col(i).last_upd_id,
                last_upd_datetime = svc_TIMELINE_STEPS_col(i).last_upd_datetime
      when NOT matched then
         insert(process_id,
                chunk_id,
                row_seq,
                action,
                process$status,
                display_seq,
                days_completed,
                timeline_type,
                step_no,
                timeline_no,
                create_id,
                create_datetime,
                last_upd_id,
                last_upd_datetime)
         values(svc_TIMELINE_STEPS_col(i).process_id,
                svc_TIMELINE_STEPS_col(i).chunk_id,
                svc_TIMELINE_STEPS_col(i).row_seq,
                svc_TIMELINE_STEPS_col(i).action,
                svc_TIMELINE_STEPS_col(i).process$status,
                sq.display_seq,
                sq.days_completed,
                sq.timeline_type,
                sq.step_no,
                sq.timeline_no,
                svc_TIMELINE_STEPS_col(i).create_id,
                svc_TIMELINE_STEPS_col(i).create_datetime,
                svc_TIMELINE_STEPS_col(i).last_upd_id,
                svc_TIMELINE_STEPS_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            TIMELINE_STEPS_sheet,
                            svc_TIMELINE_STEPS_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
      when OTHERS then
         if C_MANDATORY_IND%ISOPEN then
            close C_MANDATORY_IND;
         end if;
         ROLLBACK;
   END;
END PROCESS_S9T_TIMELINE_STEPS;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TIMELINE_HEAD(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                    I_process_id   IN   SVC_TIMELINE_HEAD.PROCESS_ID%TYPE) IS

   TYPE svc_TIMELINE_HEAD_col_typ is TABLE OF SVC_TIMELINE_HEAD%ROWTYPE;
   L_temp_rec              SVC_TIMELINE_HEAD%ROWTYPE;
   svc_TIMELINE_HEAD_col   svc_TIMELINE_HEAD_col_typ := NEW svc_TIMELINE_HEAD_col_typ();
   L_process_id            SVC_TIMELINE_HEAD.PROCESS_ID%TYPE;
   L_error                 BOOLEAN                   := FALSE;
   L_default_rec           SVC_TIMELINE_HEAD%ROWTYPE;

   cursor C_MANDATORY_IND is
      select TIMELINE_NO_mi,
             TIMELINE_BASE_mi,
             TIMELINE_TYPE_mi,
             TIMELINE_DESC_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key         = template_key
                 and wksht_key            = 'TIMELINE_HEAD')
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('TIMELINE_NO'   as TIMELINE_NO,
                                                                'TIMELINE_BASE' as TIMELINE_BASE,
                                                                'TIMELINE_TYPE' as TIMELINE_TYPE,
                                                                'TIMELINE_DESC' as TIMELINE_DESC));
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)  := 'SVC_TIMELINE_HEAD';
   L_pk_columns   VARCHAR2(255) := 'Timeline Number';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   --Get default values.
   FOR rec IN (select TIMELINE_NO_dv,
                      TIMELINE_BASE_dv,
                      TIMELINE_TYPE_dv,
                      TIMELINE_DESC_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                     = template_key
                          and wksht_key                        = 'TIMELINE_HEAD')
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('TIMELINE_NO'   as TIMELINE_NO,
                                                                             'TIMELINE_BASE' as TIMELINE_BASE,
                                                                             'TIMELINE_TYPE' as TIMELINE_TYPE,
                                                                             'TIMELINE_DESC' as TIMELINE_DESC)))
   LOOP
      BEGIN
         L_default_rec.timeline_no := rec.timeline_no_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'TIMELINE_HEAD',
                               NULL,
                               'TIMELINE_NO',
                               NULL,
                               'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.timeline_base := rec.timeline_base_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'TIMELINE_HEAD',
                               NULL,
                               'TIMELINE_BASE',
                               NULL,
                               'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.timeline_type := rec.timeline_type_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'TIMELINE_HEAD',
                               NULL,
                               'TIMELINE_TYPE',
                               NULL,
                               'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.timeline_desc := rec.timeline_desc_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'TIMELINE_HEAD',
                               NULL,
                               'TIMELINE_DESC',
                               NULL,
                               'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(TIMELINE_HEAD$Action)                  as Action,
                      r.get_cell(TIMELINE_HEAD$TIMELINE_NO)             as TIMELINE_NO,
                      r.get_cell(TIMELINE_HEAD$TIMELINE_BASE)           as TIMELINE_BASE,
                      UPPER(r.get_cell(TIMELINE_HEAD$TIMELINE_TYPE))    as TIMELINE_TYPE,
                      r.get_cell(TIMELINE_HEAD$TIMELINE_DESC)           as TIMELINE_DESC,
                      r.get_row_seq()                                   as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(TIMELINE_HEAD_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                       := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_HEAD_sheet,
                               rec.row_seq,
                               action_column,
                               SQLCODE,
                               SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.timeline_no := rec.timeline_no;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_HEAD_sheet,
                               rec.row_seq,
                               'TIMELINE_NO',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.timeline_base := rec.timeline_base;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_HEAD_sheet,
                               rec.row_seq,
                               'TIMELINE_BASE',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.timeline_type := rec.timeline_type;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_HEAD_sheet,
                               rec.row_seq,
                               'TIMELINE_TYPE',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.timeline_desc := rec.timeline_desc;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_HEAD_sheet,
                               rec.row_seq,
                               'TIMELINE_DESC',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;

      if rec.action = CORESVC_TLHEAD_TLSTEPCOMP.action_new then
         L_temp_rec.timeline_no   := NVL(L_temp_rec.timeline_no,L_default_rec.timeline_no);
         L_temp_rec.timeline_base := NVL(L_temp_rec.timeline_base,L_default_rec.timeline_base);
         L_temp_rec.timeline_type := NVL(L_temp_rec.timeline_type,L_default_rec.timeline_type);
         L_temp_rec.timeline_desc := NVL(L_temp_rec.timeline_desc,L_default_rec.timeline_desc);
      end if;
      if rec.action in (CORESVC_TLHEAD_TLSTEPCOMP.action_mod,CORESVC_TLHEAD_TLSTEPCOMP.action_del) then
         if NOT (L_temp_rec.timeline_no is NOT NULL)then
            WRITE_S9T_ERROR(I_file_id,
                            TIMELINE_HEAD_sheet,
                            rec.row_seq,
                            NULL,
                            NULL,
                            SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
            L_error := TRUE;
         end if;
      end if;
      if NOT L_error then
         svc_TIMELINE_HEAD_col.extend();
         svc_TIMELINE_HEAD_col(svc_TIMELINE_HEAD_col.COUNT()) := L_temp_rec;
      end if;

   END LOOP;
   BEGIN
      FORALL i IN 1..svc_TIMELINE_HEAD_col.COUNT SAVE EXCEPTIONS
      merge into svc_timeline_head st
      using(select
                  (case
                   when L_mi_rec.TIMELINE_NO_mi    = 'N'
                    and svc_TIMELINE_HEAD_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.TIMELINE_NO IS NULL
                   then mt.TIMELINE_NO
                   else s1.TIMELINE_NO
                   end) as TIMELINE_NO,
                  (case
                   when L_mi_rec.TIMELINE_BASE_mi    = 'N'
                    and svc_TIMELINE_HEAD_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.TIMELINE_BASE IS NULL
                   then mt.TIMELINE_BASE
                   else s1.TIMELINE_BASE
                   end) as TIMELINE_BASE,
                  (case
                   when L_mi_rec.TIMELINE_TYPE_mi    = 'N'
                    and svc_TIMELINE_HEAD_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.TIMELINE_TYPE IS NULL
                   then mt.TIMELINE_TYPE
                   else s1.TIMELINE_TYPE
                   end) as TIMELINE_TYPE,
                  (case
                   when L_mi_rec.TIMELINE_DESC_mi    = 'N'
                    and svc_TIMELINE_HEAD_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.TIMELINE_DESC IS NULL
                   then mt.TIMELINE_DESC
                   else s1.TIMELINE_DESC
                   end) as TIMELINE_DESC
              from (select svc_TIMELINE_HEAD_col(i).TIMELINE_NO   as TIMELINE_NO,
                           svc_TIMELINE_HEAD_col(i).TIMELINE_BASE as TIMELINE_BASE,
                           svc_TIMELINE_HEAD_col(i).TIMELINE_TYPE as TIMELINE_TYPE,
                           svc_TIMELINE_HEAD_col(i).TIMELINE_DESC as TIMELINE_DESC
                      from dual) s1,
                   timeline_head mt
             where mt.TIMELINE_NO (+) = s1.TIMELINE_NO)sq
                on (st.TIMELINE_NO   = sq.TIMELINE_NO
                and svc_TIMELINE_HEAD_col(i).ACTION IN (CORESVC_TLHEAD_TLSTEPCOMP.action_mod,CORESVC_TLHEAD_TLSTEPCOMP.action_del))
      when matched then
         update
            set process_id      = svc_TIMELINE_HEAD_col(i).process_id,
                chunk_id        = svc_TIMELINE_HEAD_col(i).chunk_id,
                row_seq         = svc_TIMELINE_HEAD_col(i).row_seq,
                action          = svc_TIMELINE_HEAD_col(i).action,
                process$status  = svc_TIMELINE_HEAD_col(i).process$status,
                timeline_desc   = sq.timeline_desc,
                timeline_base   = sq.timeline_base,
                timeline_type   = sq.timeline_type,
                create_id       = svc_TIMELINE_HEAD_col(i).create_id,
                create_datetime = svc_TIMELINE_HEAD_col(i).create_datetime,
                last_upd_id     = svc_TIMELINE_HEAD_col(i).last_upd_id,
                last_upd_datetime = svc_TIMELINE_HEAD_col(i).last_upd_datetime
      when NOT matched then
         insert(process_id,
                chunk_id,
                row_seq,
                action,
                process$status,
                timeline_no,
                timeline_base,
                timeline_type,
                timeline_desc,
                create_id,
                create_datetime,
                last_upd_id,
                last_upd_datetime)
         values(svc_TIMELINE_HEAD_col(i).process_id,
                svc_TIMELINE_HEAD_col(i).chunk_id,
                svc_TIMELINE_HEAD_col(i).row_seq,
                svc_TIMELINE_HEAD_col(i).action,
                svc_TIMELINE_HEAD_col(i).process$status,
                sq.timeline_no,
                sq.timeline_base,
                sq.timeline_type,
                sq.timeline_desc,
                svc_TIMELINE_HEAD_col(i).create_id,
                svc_TIMELINE_HEAD_col(i).create_datetime,
                svc_TIMELINE_HEAD_col(i).last_upd_id,
                svc_TIMELINE_HEAD_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            TIMELINE_HEAD_sheet,
                            svc_TIMELINE_HEAD_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
      when OTHERS then
         if C_MANDATORY_IND%ISOPEN then
            close C_MANDATORY_IND;
         end if;
         ROLLBACK;
   END;
END PROCESS_S9T_TIMELINE_HEAD;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TIMELINE_HEAD_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                       I_process_id   IN   SVC_TIMELINE_HEAD_TL.PROCESS_ID%TYPE) IS

   TYPE svc_timeline_head_tl_col_TYP is TABLE OF SVC_TIMELINE_HEAD_TL%ROWTYPE;
   L_temp_rec                 SVC_TIMELINE_HEAD_TL%ROWTYPE;
   svc_timeline_head_tl_col   svc_timeline_head_tl_col_typ := NEW svc_timeline_head_tl_col_typ();
   L_process_id               SVC_TIMELINE_HEAD_TL.PROCESS_ID%TYPE;
   L_error                    BOOLEAN := FALSE;
   L_default_rec              SVC_TIMELINE_HEAD_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select timeline_desc_mi,
             timeline_no_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'TIMELINE_HEAD_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('TIMELINE_DESC' as timeline_desc,
                                       'TIMELINE_NO'    as timeline_no,
                                       'LANG'          as lang));

   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)   := 'SVC_TIMELINE_HEAD_TL';
   L_pk_columns   VARCHAR2(255)  := 'Timeline No., Lang';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   --Get default values.
   FOR rec IN (select timeline_desc_dv,
                      timeline_no_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'TIMELINE_HEAD_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('TIMELINE_DESC' as timeline_desc,
                                                'TIMELINE_NO'    as timeline_no,
                                                'LANG'          as lang)))
   LOOP
      BEGIN
         L_default_rec.timeline_desc := rec.timeline_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TIMELINE_HEAD_TL_SHEET,
                            NULL,
                            'TIMELINE_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.timeline_no := rec.timeline_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TIMELINE_HEAD_TL_SHEET,
                            NULL,
                            'TIMELINE_NO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TIMELINE_HEAD_TL_SHEET,
                            NULL,
                            'LANG',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(timeline_head_tl$action))    as action,
                      r.get_cell(timeline_head_tl$timeline_desc)    as timeline_desc,
                      r.get_cell(timeline_head_tl$timeline_no)      as timeline_no,
                      r.get_cell(timeline_head_tl$lang)             as lang,
                      r.get_row_seq()                               as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(TIMELINE_HEAD_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            timeline_head_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.timeline_desc := rec.timeline_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TIMELINE_HEAD_TL_SHEET,
                            rec.row_seq,
                            'TIMELINE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.timeline_no := rec.timeline_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TIMELINE_HEAD_TL_SHEET,
                            rec.row_seq,
                            'TIMELINE_NO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TIMELINE_HEAD_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TLHEAD_TLSTEPCOMP.action_new then
         L_temp_rec.timeline_desc := NVL(L_temp_rec.timeline_desc,L_default_rec.timeline_desc);
         L_temp_rec.timeline_no   := NVL(L_temp_rec.timeline_no,L_default_rec.timeline_no);
         L_temp_rec.lang          := NVL(L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.timeline_no is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         TIMELINE_HEAD_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_timeline_head_tl_col.extend();
         svc_timeline_head_tl_col(svc_timeline_head_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      FORALL i IN 1..svc_timeline_head_tl_col.COUNT SAVE EXCEPTIONS
      merge into svc_timeline_head_tl st
      using(select
                  (case
                   when L_mi_rec.timeline_desc_mi = 'N'
                    and svc_timeline_head_tl_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.timeline_desc is NULL then
                        mt.timeline_desc
                   else s1.timeline_desc
                   end) as timeline_desc,
                  (case
                   when L_mi_rec.lang_mi = 'N'
                    and svc_timeline_head_tl_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.lang is NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when L_mi_rec.timeline_no_mi = 'N'
                    and svc_timeline_head_tl_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.timeline_no is NULL then
                        mt.timeline_no
                   else s1.timeline_no
                   end) as timeline_no
              from (select svc_timeline_head_tl_col(i).timeline_desc as timeline_desc,
                           svc_timeline_head_tl_col(i).timeline_no        as timeline_no,
                           svc_timeline_head_tl_col(i).lang              as lang
                      from dual) s1,
                   timeline_head_tl mt
             where mt.timeline_no (+) = s1.timeline_no
               and mt.lang (+)       = s1.lang) sq
                on (st.timeline_no = sq.timeline_no and
                    st.lang = sq.lang and
                    svc_timeline_head_tl_col(i).ACTION IN (CORESVC_TLHEAD_TLSTEPCOMP.action_mod,CORESVC_TLHEAD_TLSTEPCOMP.action_del))
      when matched then
         update
            set process_id     = svc_timeline_head_tl_col(i).process_id,
                chunk_id       = svc_timeline_head_tl_col(i).chunk_id,
                row_seq        = svc_timeline_head_tl_col(i).row_seq,
                action         = svc_timeline_head_tl_col(i).action,
                process$status = svc_timeline_head_tl_col(i).process$status,
                timeline_desc  = sq.timeline_desc
      when NOT matched then
         insert(process_id,
                chunk_id,
                row_seq,
                action,
                process$status,
                timeline_desc,
                timeline_no,
                lang)
         values(svc_timeline_head_tl_col(i).process_id,
                svc_timeline_head_tl_col(i).chunk_id,
                svc_timeline_head_tl_col(i).row_seq,
                svc_timeline_head_tl_col(i).action,
                svc_timeline_head_tl_col(i).process$status,
                sq.timeline_desc,
                sq.timeline_no,
                sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            TIMELINE_HEAD_TL_SHEET,
                            svc_timeline_head_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TIMELINE_HEAD_TL;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TIMELINE_STEP_COMP(I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                         I_process_id IN   SVC_TIMELINE_STEP_COMP.PROCESS_ID%TYPE) IS

   TYPE svc_TIMELINE_STEP_COMP_col_typ is TABLE OF SVC_TIMELINE_STEP_COMP%ROWTYPE;
   L_temp_rec                   SVC_TIMELINE_STEP_COMP%ROWTYPE;
   svc_TIMELINE_STEP_COMP_col   svc_TIMELINE_STEP_COMP_col_typ := NEW SVC_TIMELINE_STEP_COMP_COL_TYP();
   L_process_id                 SVC_TIMELINE_STEP_COMP.PROCESS_ID%TYPE;
   L_error                      BOOLEAN := FALSE;
   L_default_rec                SVC_TIMELINE_STEP_COMP%ROWTYPE;

   cursor C_MANDATORY_IND is
      select STEP_DESC_mi,
             STEP_NO_mi,
             TIMELINE_TYPE_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key      = template_key
                 and wksht_key         = 'TIMELINE_STEP_COMP'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN (
                                            'STEP_DESC'     as STEP_DESC,
                                            'STEP_NO'       as STEP_NO,
                                            'TIMELINE_TYPE' as TIMELINE_TYPE));
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)  := 'SVC_TIMELINE_STEP_COMP';
   L_pk_columns   VARCHAR2(255) := 'Timeline Type, Step Number';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   --Get default values.
   FOR rec IN (select STEP_DESC_dv,
                      STEP_NO_dv,
                      TIMELINE_TYPE_dv
                from (select column_key,
                             default_value
                        from s9t_tmpl_cols_def
                       where template_key      = template_key
                         and wksht_key         = 'TIMELINE_STEP_COMP'
                      ) PIVOT (MAX(default_value) as dv
                               FOR (column_key) IN ('STEP_DESC'     as STEP_DESC,
                                                    'STEP_NO'       as STEP_NO,
                                                    'TIMELINE_TYPE' as TIMELINE_TYPE)))
   LOOP
      BEGIN
         L_default_rec.step_desc := rec.step_desc_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'TIMELINE_STEP_COMP',
                               NULL,
                               'STEP_DESC',
                               NULL,
                               'INV_DEFAULT');
         END;
         BEGIN
            L_default_rec.step_no := rec.step_no_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'TIMELINE_STEP_COMP',
                               NULL,
                               'STEP_NO',
                               NULL,
                               'INV_DEFAULT');
         END;
         BEGIN
            L_default_rec.timeline_type := rec.timeline_type_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'TIMELINE_STEP_COMP',
                               NULL,
                               'TIMELINE_TYPE',
                               NULL,
                               'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(TIMELINE_STEP_COMP$Action)      as Action,
                      r.get_cell(TIMELINE_STEP_COMP$STEP_DESC)   as STEP_DESC,
                      r.get_cell(TIMELINE_STEP_COMP$STEP_NO)     as STEP_NO,
                      r.get_cell(TSTEP_COMP$TIMELINE_TYPE)       as TIMELINE_TYPE,
                      r.get_row_seq()                            as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(TIMELINE_STEP_COMP_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_STEP_COMP_sheet,
                               rec.row_seq,
                               action_column,
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.step_desc := rec.step_desc;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_STEP_COMP_sheet,
                               rec.row_seq,
                               'STEP_DESC',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.step_no := rec.step_no;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_STEP_COMP_sheet,
                               rec.row_seq,
                               'STEP_NO',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.timeline_type := rec.timeline_type;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               TIMELINE_STEP_COMP_sheet,
                               rec.row_seq,
                               'TIMELINE_TYPE',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      if rec.action = CORESVC_TLHEAD_TLSTEPCOMP.action_new then
         L_temp_rec.step_desc     := NVL(L_temp_rec.step_desc,L_default_rec.step_desc);
         L_temp_rec.step_no       := NVL(L_temp_rec.step_no,L_default_rec.step_no);
         L_temp_rec.timeline_type := NVL(L_temp_rec.timeline_type,L_default_rec.timeline_type);
      end if;
      if NOT (L_temp_rec.step_no is NOT NULL and
              L_temp_rec.timeline_type is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         TIMELINE_STEP_COMP_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_TIMELINE_STEP_COMP_col.extend();
         svc_TIMELINE_STEP_COMP_col(svc_TIMELINE_STEP_COMP_col.COUNT()) := L_temp_rec;
      end if;

   END LOOP;
   BEGIN
      FORALL i IN 1..svc_TIMELINE_STEP_COMP_col.COUNT SAVE EXCEPTIONS
      merge into svc_timeline_step_comp st
      using(select
                  (case
                   when L_mi_rec.STEP_DESC_mi    = 'N'
                    and svc_TIMELINE_STEP_COMP_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.STEP_DESC is NULL
                   then mt.STEP_DESC
                   else s1.STEP_DESC
                   end) as STEP_DESC,
                  (case
                   when L_mi_rec.STEP_NO_mi    = 'N'
                    and svc_TIMELINE_STEP_COMP_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.STEP_NO is NULL
                   then mt.STEP_NO
                   else s1.STEP_NO
                   end) as STEP_NO,
                  (case
                   when L_mi_rec.TIMELINE_TYPE_mi    = 'N'
                    and svc_TIMELINE_STEP_COMP_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.TIMELINE_TYPE is NULL
                   then mt.TIMELINE_TYPE
                   else s1.TIMELINE_TYPE
                   end) as TIMELINE_TYPE
              from (select
                          svc_TIMELINE_STEP_COMP_col(i).STEP_DESC     as STEP_DESC,
                          svc_TIMELINE_STEP_COMP_col(i).STEP_NO       as STEP_NO,
                          svc_TIMELINE_STEP_COMP_col(i).TIMELINE_TYPE as TIMELINE_TYPE
                      from dual) s1,
                   timeline_step_comp mt
             where mt.STEP_NO (+)       = s1.STEP_NO   and
                   mt.TIMELINE_TYPE (+) = s1.TIMELINE_TYPE)sq
                on (
                    st.STEP_NO        = sq.STEP_NO and
                    st.TIMELINE_TYPE  = sq.TIMELINE_TYPE and
                    svc_TIMELINE_STEP_COMP_col(i).ACTION IN (CORESVC_TLHEAD_TLSTEPCOMP.action_mod,CORESVC_TLHEAD_TLSTEPCOMP.action_del))
      when matched then
         update
            set process_id      = svc_TIMELINE_STEP_COMP_col(i).process_id,
                chunk_id        = svc_TIMELINE_STEP_COMP_col(i).chunk_id,
                row_seq         = svc_TIMELINE_STEP_COMP_col(i).row_seq,
                action          = svc_TIMELINE_STEP_COMP_col(i).action,
                process$status  = svc_TIMELINE_STEP_COMP_col(i).process$status,
                step_desc       = sq.step_desc,
                create_id       = svc_TIMELINE_STEP_COMP_col(i).create_id,
                create_datetime = svc_TIMELINE_STEP_COMP_col(i).create_datetime,
                last_upd_id     = svc_TIMELINE_STEP_COMP_col(i).last_upd_id,
                last_upd_datetime = svc_TIMELINE_STEP_COMP_col(i).last_upd_datetime
      when NOT matched then
         insert(process_id,
                chunk_id,
                row_seq,
                action,
                process$status,
                step_desc,
                step_no,
                timeline_type,
                create_id,
                create_datetime,
                last_upd_id,
                last_upd_datetime)
         values(svc_TIMELINE_STEP_COMP_col(i).process_id,
                svc_TIMELINE_STEP_COMP_col(i).chunk_id,
                svc_TIMELINE_STEP_COMP_col(i).row_seq,
                svc_TIMELINE_STEP_COMP_col(i).action,
                svc_TIMELINE_STEP_COMP_col(i).process$status,
                sq.step_desc,
                sq.step_no,
                sq.timeline_type,
                svc_TIMELINE_STEP_COMP_col(i).create_id,
                svc_TIMELINE_STEP_COMP_col(i).create_datetime,
                svc_TIMELINE_STEP_COMP_col(i).last_upd_id,
                svc_TIMELINE_STEP_COMP_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            TIMELINE_STEP_COMP_sheet,
                            svc_TIMELINE_STEP_COMP_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TIMELINE_STEP_COMP;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TSTEP_COMP_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                    I_process_id   IN   SVC_TIMELINE_STEP_COMP_TL.PROCESS_ID%TYPE) IS

   TYPE svc_tstep_comp_tl_col_TYP is TABLE OF SVC_TIMELINE_STEP_COMP_TL%ROWTYPE;
   L_temp_rec              SVC_TIMELINE_STEP_COMP_TL%ROWTYPE;
   svc_tstep_comp_tl_col   svc_tstep_comp_tl_col_TYP := NEW svc_tstep_comp_tl_col_TYP();
   L_process_id            SVC_TIMELINE_STEP_COMP_TL.PROCESS_ID%TYPE;
   L_error                 BOOLEAN := FALSE;
   L_default_rec           SVC_TIMELINE_STEP_COMP_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select step_desc_mi,
             step_no_mi,
             timeline_type_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'TIMELINE_STEP_COMP_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('STEP_DESC'      as step_desc,
                                       'STEP_NO'        as step_no,
                                       'TIMELINE_TYPE'  as timeline_type,
                                       'LANG'            as lang));

   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)   := 'SVC_TIMELINE_STEP_COMP_TL';
   L_pk_columns   VARCHAR2(255)  := 'Timeline Type, Step Number, Lang';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   --Get default values.
   FOR rec IN (select step_desc_dv,
                      step_no_dv,
                      timeline_type_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'TIMELINE_STEP_COMP_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('STEP_DESC'      as step_desc,
                                                'STEP_NO'        as step_no,
                                                'TIMELINE_TYPE'  as timeline_type,
                                                'LANG'            as lang)))
   LOOP
      BEGIN
         L_default_rec.step_desc := rec.step_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSTEP_COMP_TL_SHEET,
                            NULL,
                            'STEP_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.step_no := rec.step_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSTEP_COMP_TL_SHEET,
                            NULL,
                            'STEP_NO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.timeline_type := rec.timeline_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSTEP_COMP_TL_SHEET,
                            NULL,
                            'TIMELINE_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSTEP_COMP_TL_SHEET,
                            NULL,
                            'LANG',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(tstep_comp_tl$action))    as action,
                      r.get_cell(tstep_comp_tl$step_desc)        as step_desc,
                      r.get_cell(tstep_comp_tl$step_no)          as step_no,
                      r.get_cell(tstep_comp_tl$timeline_type)    as timeline_type,
                      r.get_cell(tstep_comp_tl$lang)             as lang,
                      r.get_row_seq()                            as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(TSTEP_COMP_TL_SHEET))
   LOOP
      L_temp_rec                := NULL;
      L_temp_rec.process_id     := I_process_id;
      L_temp_rec.chunk_id       := 1;
      L_temp_rec.row_seq        := rec.row_seq;
      L_temp_rec.process$status := 'N';
      L_error                   := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            timeline_step_comp_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.step_desc := rec.step_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSTEP_COMP_TL_SHEET,
                            rec.row_seq,
                            'STEP_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.step_no := rec.step_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSTEP_COMP_TL_SHEET,
                            rec.row_seq,
                            'STEP_NO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.timeline_type := rec.timeline_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSTEP_COMP_TL_SHEET,
                            rec.row_seq,
                            'TIMELINE_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSTEP_COMP_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TLHEAD_TLSTEPCOMP.action_new then
         L_temp_rec.step_desc     := NVL(L_temp_rec.step_desc,L_default_rec.step_desc);
         L_temp_rec.step_no       := NVL(L_temp_rec.step_no,L_default_rec.step_no);
         L_temp_rec.timeline_type := NVL(L_temp_rec.timeline_type,L_default_rec.timeline_type);
         L_temp_rec.lang          := NVL(L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.timeline_type is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         TSTEP_COMP_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_tstep_comp_tl_col.extend();
         svc_tstep_comp_tl_col(svc_tstep_comp_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      FORALL i IN 1..svc_tstep_comp_tl_col.COUNT SAVE EXCEPTIONS
      merge into svc_timeline_step_comp_tl st
      using(select
                  (case
                   when L_mi_rec.step_desc_mi = 'N'
                    and svc_tstep_comp_tl_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.step_desc is NULL then
                        mt.step_desc
                   else s1.step_desc
                   end) as step_desc,
                  (case
                   when L_mi_rec.step_no_mi = 'N'
                    and svc_tstep_comp_tl_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.step_no is NULL then
                        mt.step_no
                   else s1.step_no
                   end) as step_no,
                  (case
                   when L_mi_rec.lang_mi = 'N'
                    and svc_tstep_comp_tl_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.lang is NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when L_mi_rec.timeline_type_mi = 'N'
                    and svc_tstep_comp_tl_col(i).action = CORESVC_TLHEAD_TLSTEPCOMP.action_mod
                    and s1.timeline_type is NULL then
                        mt.timeline_type
                   else s1.timeline_type
                   end) as timeline_type
              from (select svc_tstep_comp_tl_col(i).step_desc as step_desc,
                           svc_tstep_comp_tl_col(i).step_no as step_no,
                           svc_tstep_comp_tl_col(i).timeline_type as timeline_type,
                           svc_tstep_comp_tl_col(i).lang as lang
                      from dual) s1,
                   timeline_step_comp_tl mt
             where mt.timeline_type (+) = s1.timeline_type
               and mt.step_no (+)       = s1.step_no
               and mt.lang (+)          = s1.lang) sq
                on (st.timeline_type = sq.timeline_type and
                    st.step_no = sq.step_no and
                    st.lang = sq.lang and
                    svc_tstep_comp_tl_col(i).ACTION IN (CORESVC_TLHEAD_TLSTEPCOMP.action_mod,CORESVC_TLHEAD_TLSTEPCOMP.action_del))
      when matched then
         update
            set process_id        = svc_tstep_comp_tl_col(i).process_id,
                chunk_id          = svc_tstep_comp_tl_col(i).chunk_id,
                row_seq           = svc_tstep_comp_tl_col(i).row_seq,
                action            = svc_tstep_comp_tl_col(i).action,
                process$status    = svc_tstep_comp_tl_col(i).process$status,
                step_desc = sq.step_desc
      when NOT matched then
         insert(process_id,
                chunk_id,
                row_seq,
                action,
                process$status,
                step_desc,
                step_no,
                timeline_type,
                lang)
         values(svc_tstep_comp_tl_col(i).process_id,
                svc_tstep_comp_tl_col(i).chunk_id,
                svc_tstep_comp_tl_col(i).row_seq,
                svc_tstep_comp_tl_col(i).action,
                svc_tstep_comp_tl_col(i).process$status,
                sq.step_desc,
                sq.step_no,
                sq.timeline_type,
                sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            TSTEP_COMP_TL_SHEET,
                            svc_tstep_comp_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TSTEP_COMP_TL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_TLHEAD_TLSTEPCOMP.PROCESS_S9T';
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.NAMES_MAP_TYP;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	          EXCEPTION;
   PRAGMA	          EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_TIMELINE_STEPS(I_file_id,I_process_id);
      PROCESS_S9T_TIMELINE_HEAD(I_file_id,I_process_id);
      PROCESS_S9T_TIMELINE_HEAD_TL(I_file_id,I_process_id);      
      PROCESS_S9T_TIMELINE_STEP_COMP(I_file_id,I_process_id);
      PROCESS_S9T_TSTEP_COMP_TL(I_file_id,I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into s9t_errors
         values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      FORALL i IN 1..O_error_count
         insert into s9t_errors
            values LP_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.count();
      FORALL i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

         update svc_process_tracker
            set status = 'PE',
                file_id  = I_file_id
          where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_TIMELINE_STEPS_VAL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error          IN OUT  BOOLEAN,
                                    I_rec            IN      C_SVC_TIMELINE_STEPS%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.PROCESS_TIMELINE_STEPS_VAL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TIMELINE_STEPS';
   L_tmln_exist   VARCHAR2(1)                       := NULL;

   cursor C_TMLN_EXIST is
      select 'Y'
        from timeline
       where timeline_no = I_rec.stts_timeline_no
         and step_no     = I_rec.stts_step_no;
BEGIN
   if I_rec.stts_action is NOT NULL then
      --null check for step_no
      if NOT (I_rec.stts_step_no is NOT NULL) then
         WRITE_ERROR(I_rec.stts_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.stts_chunk_id,
                     L_table,
                     I_rec.stts_row_seq,
                     'STEP_NO',
                     'ENTER_STEP_NO');
         O_error := TRUE;
      end if;
      --null check for days_completed
      if I_rec.stts_action IN (action_new,action_mod) then
         if NOT (I_rec.stts_days_completed is NOT NULL) then
            WRITE_ERROR(I_rec.stts_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stts_chunk_id,
                        L_table,
                        I_rec.stts_row_seq,
                        'DAYS_COMPLETED',
                        'DAYS_NOT_SPECIFIED');
            O_error := TRUE;
         end if;
         --null check for display_seq
         if NOT (I_rec.stts_display_seq is NOT NULL) then
            WRITE_ERROR(I_rec.stts_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stts_chunk_id,
                        L_table,
                        I_rec.stts_row_seq,
                        'DISPLAY_SEQ',
                        'ENT_SEQ_NUMB');
            O_error := TRUE;
         end if;

         if I_rec.stts_display_seq < 0 then
            WRITE_ERROR(I_rec.stts_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.stts_chunk_id,
                        L_table,
                        I_rec.stts_row_seq,
                        'DISPLAY_SEQ',
                        'ISC_BETWEEN_ZERO_9999');
            O_error := TRUE;
         end if;
      end if;  --I_rec.stts_action IN (action_new,action_mod)
   end if;
   --check if the TIMELINE_HEAD record is referred in TIMELINE table when action is DEL
   if I_rec.stts_action = action_del then
      open  C_TMLN_EXIST;
      fetch C_TMLN_EXIST into L_tmln_exist;
      close C_TMLN_EXIST;
      if L_tmln_exist = 'Y' then
         WRITE_ERROR(I_rec.stts_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.stts_chunk_id,
                     L_table,
                     I_rec.stts_row_seq,
                     'Timeline Number, Step Number',
                     'CANNOT_DEL_ORD_TMLN');
         O_error := TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TIMELINE_STEPS_VAL;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_STEPS_INS(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_timeline_steps_temp_rec   IN       TIMELINE_STEPS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_STEPS_INS';
BEGIN
   insert into timeline_steps
      values I_timeline_steps_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_STEPS_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_STEPS_UPD(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_timeline_steps_temp_rec   IN       TIMELINE_STEPS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_STEPS_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_STEPS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TMLN_STEPS_UPD is
      select 'x'
        from timeline_steps
       where timeline_type = I_timeline_steps_temp_rec.timeline_type
         and step_no       = I_timeline_steps_temp_rec.step_no
         and timeline_no   = I_timeline_steps_temp_rec.timeline_no
         for update nowait;

BEGIN
   open  C_LOCK_TMLN_STEPS_UPD;
   close C_LOCK_TMLN_STEPS_UPD;

   update timeline_steps
      set row = I_timeline_steps_temp_rec
    where timeline_type = I_timeline_steps_temp_rec.timeline_type
      and step_no       = I_timeline_steps_temp_rec.step_no
      and timeline_no   = I_timeline_steps_temp_rec.timeline_no;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_timeline_steps_temp_rec.timeline_type,
                                             I_timeline_steps_temp_rec.timeline_no);
      return FALSE;
   when OTHERS then
      if C_LOCK_TMLN_STEPS_UPD%ISOPEN then
            close C_LOCK_TMLN_STEPS_UPD;
      end if;
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_STEPS_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_STEPS_DEL(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_timeline_steps_temp_rec   IN       TIMELINE_STEPS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_STEPS_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_STEPS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TMLN_STEPS_DEL is
      select 'x'
        from timeline_steps
       where timeline_type = I_timeline_steps_temp_rec.timeline_type
         and step_no       = I_timeline_steps_temp_rec.step_no
         and timeline_no   = I_timeline_steps_temp_rec.timeline_no
         for update nowait;

BEGIN
   open  C_LOCK_TMLN_STEPS_DEL;
   close C_LOCK_TMLN_STEPS_DEL;

   delete from timeline_steps
    where timeline_type = I_timeline_steps_temp_rec.timeline_type
      and step_no       = I_timeline_steps_temp_rec.step_no
      and timeline_no   = I_timeline_steps_temp_rec.timeline_no;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_timeline_steps_temp_rec.timeline_type,
                                             I_timeline_steps_temp_rec.timeline_no);
      return FALSE;

   when OTHERS then
      if C_LOCK_TMLN_STEPS_DEL%ISOPEN then
         close C_LOCK_TMLN_STEPS_DEL;
      end if;
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_STEPS_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_TIMELINE_HEAD_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_error           IN OUT   BOOLEAN,
                                   I_rec             IN       C_SVC_TIMELINE_STEPS%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.PROCESS_TIMELINE_HEAD_VAL';
   L_tmln_exist   VARCHAR2(1)                       :=  NULL;
   L_head_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TIMELINE_HEAD';

   cursor C_TMLN_EXIST is
      select 'Y'
        from timeline
       where timeline_no = I_rec.stth_timeline_no;

BEGIN
   --check if the TIMELINE_HEAD record is referred in TIMELINE table when action is DEL
   if I_rec.stth_action = action_del then
      open C_TMLN_EXIST;
      fetch C_TMLN_EXIST into L_tmln_exist;
      close C_TMLN_EXIST;
      if L_tmln_exist = 'Y' then
         WRITE_ERROR(I_rec.stth_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.stth_chunk_id,
                     L_head_table,
                     I_rec.stth_row_seq,
                     'TIMELINE_NO',
                     'CANNOT_DEL_ORD_TMLN_REC');
         O_error := TRUE;
      end if;
   end if;

   if I_rec.stth_action = action_mod
      and I_rec.stth_timeline_type <> I_rec.old_timeline_type then
     WRITE_ERROR(I_rec.stth_process_id,
                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                 I_rec.stth_chunk_id,
                 L_head_table,
                 I_rec.stth_row_seq,
                 'TIMELINE_TYPE',
                 'NO_MOD_TIMELINE_TYPE');
         O_error := TRUE;
      end if;
   return TRUE;
EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TIMELINE_HEAD_VAL;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_HEAD_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_timeline_head_temp_rec   IN       TIMELINE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_HEAD_INS';
BEGIN
   insert into timeline_head
      values I_timeline_head_temp_rec;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_HEAD_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_HEAD_UPD(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_timeline_head_temp_rec   IN       TIMELINE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_HEAD_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_HEAD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TMLN_HEAD_UPD is
      select 'x'
        from timeline_head
       where timeline_no    = I_timeline_head_temp_rec.timeline_no
         for update nowait;

BEGIN
   open  C_LOCK_TMLN_HEAD_UPD;
   close C_LOCK_TMLN_HEAD_UPD;

   update timeline_head
      set row = I_timeline_head_temp_rec
    where timeline_no = I_timeline_head_temp_rec.timeline_no;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_timeline_head_temp_rec.timeline_no,
                                             NULL);
      return FALSE;

   when OTHERS then
      if C_LOCK_TMLN_HEAD_UPD%ISOPEN then
            close C_LOCK_TMLN_HEAD_UPD;
      end if;
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_HEAD_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_HEAD_PRE_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_timeline_head_temp_rec   IN       TIMELINE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_HEAD_PRE_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_STEPS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TMLN_STEPS is
      select 'x'
        from timeline_steps
       where timeline_no = I_timeline_head_temp_rec.timeline_no
         for update nowait;
BEGIN
   open  C_LOCK_TMLN_STEPS;
   close C_LOCK_TMLN_STEPS;
   
   delete from timeline_steps
    where timeline_no = I_timeline_head_temp_rec.timeline_no;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_timeline_head_temp_rec.timeline_no,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_HEAD_PRE_DEL;
--------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_HEAD_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rec                      IN       C_SVC_TIMELINE_STEPS%ROWTYPE,
                                I_timeline_head_temp_rec   IN       TIMELINE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_HEAD_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_HEAD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TMLN_HEAD_TL_DEL is
      select 'x'
        from timeline_head_tl
       where timeline_no    = I_timeline_head_temp_rec.timeline_no
         for update nowait;

   cursor C_LOCK_TMLN_HEAD_DEL is
      select 'x'
        from timeline_head
       where timeline_no    = I_timeline_head_temp_rec.timeline_no
         for update nowait;

BEGIN
   L_table := 'TIMELINE_HEAD_TL';
   open  C_LOCK_TMLN_HEAD_TL_DEL;
   close C_LOCK_TMLN_HEAD_TL_DEL;

   L_table := 'TIMELINE_HEAD';
   open  C_LOCK_TMLN_HEAD_DEL;
   close C_LOCK_TMLN_HEAD_DEL;

   --first delete all the child records in TIMELINE_STEPS
   L_table := 'TIMELINE_STEPS';
   if EXEC_TIMELINE_HEAD_PRE_DEL(O_error_message,
                                 I_timeline_head_temp_rec) = FALSE then
      WRITE_ERROR(I_rec.stth_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.stth_chunk_id,
                  L_table,
                  I_rec.stth_row_seq,
                  NULL,
                  O_error_message);
      return FALSE;
   end if;

   delete from timeline_head_tl
    where timeline_no = I_timeline_head_temp_rec.timeline_no;

   delete from timeline_head
    where timeline_no = I_timeline_head_temp_rec.timeline_no;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_timeline_head_temp_rec.timeline_no,
                                            NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_TMLN_HEAD_TL_DEL%ISOPEN then
         close C_LOCK_TMLN_HEAD_TL_DEL;
      end if;
      if C_LOCK_TMLN_HEAD_DEL%ISOPEN then
         close C_LOCK_TMLN_HEAD_DEL;
      end if;
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_HEAD_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_HEAD_TL_INS(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_timeline_head_tl_ins_tab    IN       TIMELINE_HEAD_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_HEAD_TL_INS';

BEGIN
   if I_timeline_head_tl_ins_tab is NOT NULL and I_timeline_head_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_timeline_head_tl_ins_tab.COUNT()
         insert into timeline_head_tl
              values I_timeline_head_tl_ins_tab(i);
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_HEAD_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_HEAD_TL_UPD(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_timeline_head_tl_upd_tab   IN       TIMELINE_HEAD_TAB,
                                   I_timeline_head_tl_upd_rst   IN       ROW_SEQ_TAB,
                                   I_process_id                 IN       SVC_TIMELINE_HEAD_TL.PROCESS_ID%TYPE,
                                   I_chunk_id                   IN       SVC_TIMELINE_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_HEAD_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

   cursor C_LOCK_TIMELINE_HEAD_TL_UPD(I_timeline_head  TIMELINE_HEAD_TL.TIMELINE_NO%TYPE,
                                      I_lang           TIMELINE_HEAD_TL.LANG%TYPE) is
      select 'x'
        from timeline_head_tl
       where timeline_no = I_timeline_head
         and lang = I_lang
         for update nowait;

BEGIN
   if I_timeline_head_tl_upd_tab is NOT NULL and I_timeline_head_tl_upd_tab.count > 0 then
      FOR i in I_timeline_head_tl_upd_tab.FIRST..I_timeline_head_tl_upd_tab.LAST LOOP
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_timeline_head_tl_upd_tab(i).lang);
            L_key_val2 := 'Timeline No.: '||to_char(I_timeline_head_tl_upd_tab(i).timeline_no);
            open C_LOCK_TIMELINE_HEAD_TL_UPD(I_timeline_head_tl_upd_tab(i).timeline_no,
                                             I_timeline_head_tl_upd_tab(i).lang);
            close C_LOCK_TIMELINE_HEAD_TL_UPD;
            
            update timeline_head_tl
               set timeline_desc = I_timeline_head_tl_upd_tab(i).timeline_desc,
                   last_update_id = I_timeline_head_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_timeline_head_tl_upd_tab(i).last_update_datetime
             where lang = I_timeline_head_tl_upd_tab(i).lang
               and timeline_no = I_timeline_head_tl_upd_tab(i).timeline_no;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                     L_table,
                                                     L_key_val1,
                                                     L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           I_timeline_head_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      END LOOP;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_LOCK_TIMELINE_HEAD_TL_UPD%ISOPEN then
         close C_LOCK_TIMELINE_HEAD_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_HEAD_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_HEAD_TL_DEL(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_timeline_head_tl_del_tab   IN       TIMELINE_HEAD_TAB,
                                   I_timeline_head_tl_del_rst   IN       ROW_SEQ_TAB,
                                   I_process_id                 IN       SVC_TIMELINE_HEAD_TL.PROCESS_ID%TYPE,
                                   I_chunk_id                   IN       SVC_TIMELINE_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_HEAD_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

   cursor C_LOCK_TIMELINE_HEAD_TL_DEL(I_timeline_no  TIMELINE_HEAD_TL.TIMELINE_NO%TYPE,
                                      I_lang         TIMELINE_HEAD_TL.LANG%TYPE) is
      select 'x'
        from timeline_head_tl
       where timeline_no = I_timeline_no
         and lang = I_lang
         for update nowait;

BEGIN
   if I_timeline_head_tl_del_tab is NOT NULL and I_timeline_head_tl_del_tab.count > 0 then
      FOR i in I_timeline_head_tl_del_tab.FIRST..I_timeline_head_tl_del_tab.LAST LOOP
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_timeline_head_tl_del_tab(i).lang);
            L_key_val2 := 'Timeline No.: '||to_char(I_timeline_head_tl_del_tab(i).timeline_no);
            open C_LOCK_TIMELINE_HEAD_TL_DEL(I_timeline_head_tl_del_tab(i).timeline_no,
                                             I_timeline_head_tl_del_tab(i).lang);
            close C_LOCK_TIMELINE_HEAD_TL_DEL;
            
            delete timeline_head_tl
             where lang = I_timeline_head_tl_del_tab(i).lang
               and timeline_no = I_timeline_head_tl_del_tab(i).timeline_no;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                     L_table,
                                                     L_key_val1,
                                                     L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           I_timeline_head_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      END LOOP;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_LOCK_TIMELINE_HEAD_TL_DEL%ISOPEN then
         close C_LOCK_TIMELINE_HEAD_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_HEAD_TL_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_TIMELINE_HEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_TIMELINE_HEAD.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_TIMELINE_HEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.PROCESS_TIMELINE_HEAD';
   L_head_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TIMELINE_HEAD';
   L_steps_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TIMELINE_STEPS';
   L_head_error               BOOLEAN;
   L_head_process_error       BOOLEAN                           := FALSE;
   L_timeline_head_temp_rec   TIMELINE_HEAD%ROWTYPE;
   count_steps                NUMBER;
   L_steps_error              BOOLEAN                           := FALSE;
   L_steps_process_error      BOOLEAN                           := FALSE;
   L_timeline_steps_temp_rec  TIMELINE_STEPS%ROWTYPE;
   L_timeline_no              TIMELINE.TIMELINE_NO%TYPE         := NULL;
   L_exists                   VARCHAR2(1)                       := 'N';
   L_rowseq_count             NUMBER;
   c                          NUMBER                            := 1;

   TYPE L_row_seq_tab_type is TABLE OF SVC_UDA_VALUES.ROW_SEQ%TYPE;
   L_row_seq_tab   L_row_seq_tab_type;

   --check that at least one detail exists
   cursor C_STEPS_ATLEAST1(L_tmln_no   TIMELINE.TIMELINE_NO%TYPE) is
      select count(STEP_NO) into count_steps
        from TIMELINE_STEPS
       where TIMELINE_NO = L_tmln_no;

   --check if the TIMELINE_NO is valid in TIMELINE_STEPS table
   cursor C_TIMELINE_NO(L_tmln_no   TIMELINE.TIMELINE_NO%TYPE) is
      select 'Y'
        from timeline_head
       where timeline_no = L_tmln_no;

BEGIN
   L_row_seq_tab := L_row_seq_tab_type();
   FOR rec IN C_SVC_TIMELINE_STEPS(I_process_id,
                                   I_chunk_id) LOOP
	  L_steps_error         := FALSE;
      L_head_process_error  := FALSE;
      L_steps_process_error := FALSE;

      --validate TIMELINE_HEAD only if rank is 1
      if rec.th_rank = 1 then
         L_head_error := FALSE;
         L_row_seq_tab.DELETE;
         c:=1;
         if (rec.stth_action is NULL and rec.stth_rid is NOT NULL)
            or rec.stth_action NOT IN (action_new,action_mod,action_del) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_head_table,
                        rec.stth_row_seq,
                        'ACTION',
                        'INV_ACT');
            L_head_error := TRUE;
         end if;

         if rec.stth_action IN (action_mod,action_del)
            and rec.tst_tlh_fk_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_head_table,
                        rec.stth_row_seq,
                        'TIMELINE_NO',
                        'TIMELINE_HEAD_MISSING');
            L_head_error := TRUE;
         end if;

         if rec.stth_action IN (action_new,action_mod) then
            if NOT(rec.stth_timeline_desc is NOT NULL) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_head_table,
                           rec.stth_row_seq,
                           'TIMELINE_DESC',
                           'ENTER_TMLN_DESC');
               L_head_error := TRUE;
            end if;
            if NOT(rec.stth_timeline_type is NOT NULL) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_head_table,
                           rec.stth_row_seq,
                           'TIMELINE_TYPE',
                           'ENTER_TMLN_TYPE');
               L_head_error := TRUE;
            end if;
            if rec.stth_timeline_type IN ('PO', 'POIT', 'LOPI')
		       and rec.stth_timeline_type is NOT NULL
		       and rec.cd_timebase_rid is NULL then
			   WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_head_table,
                           rec.stth_row_seq,
                           'TIMELINE_BASE',
                           'TLH_TIMELINE_BASE_INV');
               L_head_error := TRUE;
            end if;

		    if rec.stth_timeline_type NOT IN ('PO', 'POIT', 'LOPI')
		       and rec.stth_timeline_base is NOT NULL then
			   WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_head_table,
                           rec.stth_row_seq,
                           'Timeline Type, Timeline Base',
                           'TLH_TYPE_BASE_MISMATCH');
               L_head_error := TRUE;
            end if;

             if rec.stth_timeline_type is NOT NULL
                and rec.cd_timetype_rid is NULL then
                WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_head_table,
                           rec.stth_row_seq,
                           'TIMELINE_TYPE',
                           'TLH_TIMELINE_TYPE_INV');
               L_head_error := TRUE;
            end if;
         end if;  --if rec.stth_action IN (action_new,action_mod)

         if PROCESS_TIMELINE_HEAD_VAL(O_error_message,
                                      L_head_error,
                                      rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_head_table,
                        rec.stth_row_seq,
                        NULL,
                        O_error_message);
            L_head_error := TRUE;
         end if;
      end if; --if rank=1 then only validate head

      --validations for TIMELINE_STEPS table
      if rec.stts_rid is NOT NULL and (NVL(rec.stth_action, '-1') <> action_del or rec.stth_action is NULL) then
         if rec.stts_action NOT IN (action_new,action_mod,action_del)
            or (rec.stts_action is NULL and rec.stts_rid is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_steps_table,
                        rec.stts_row_seq,
                        'ACTION',
                        'INV_ACT');
            L_steps_error := TRUE;
         end if;
         if (rec.stth_action <> action_new or rec.stth_action is NULL)
   	        and rec.stts_action = action_new
            and rec.pk_timeline_steps_rid is NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_steps_table,
                        rec.stts_row_seq,
                        'Timeline Number,Step Number,Timeline Type',
                        'TIMELINE_STEPS_DUP');
            L_steps_error := TRUE;
         end if;
         if rec.stts_action IN (action_mod,action_del)
            and rec.pk_timeline_steps_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_steps_table,
                        rec.stts_row_seq,
                        'Timeline Number,Step Number,Timeline Type',
                        'TIMELINE_STEPS_MISSING');
            L_steps_error := TRUE;
         end if;

         if rec.stts_action IN (action_mod,action_del,action_new) then
            if rec.tst_tsc_fk_rid is NULL and rec.cd_timetypes_rid is NOT NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_steps_table,
                           rec.stts_row_seq,
                           'Timeline Type, Step Number',
                           'INV_STEP_NO');
               L_steps_error := TRUE;
            end if;

   	        if rec.cd_timetypes_rid is NULL then
   		       WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_steps_table,
                           rec.stts_row_seq,
                           'TIMELINE_TYPE',
                           'TLH_TIMELINE_TYPE_INV');
         	   L_steps_error := TRUE;
            end if;
         end if;
         if (rec.stts_action = action_new
            and (rec.stth_action <> action_new or rec.stth_action is NULL))
            and rec.tst_tlh_fk_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_steps_table,
                        rec.stts_row_seq,
                        'Timeline Number, Timeline Type',
                        'INV_TMLN_NO');
            L_steps_error := TRUE;
         end if;

         if PROCESS_TIMELINE_STEPS_VAL(O_error_message,
                                       L_steps_error,
                                       rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_steps_table,
                        rec.stts_row_seq,
                        NULL,
                        O_error_message);
            L_steps_error := TRUE;
         end if;
      end if;

      if L_head_error then
		 if rec.stts_row_seq is NOT NULL then
		    WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_steps_table,
                        rec.stts_row_seq,
                        'TIMELINE_NO',
                        'ERR_IN_TMLN_HEAD');
         end if;
      end if;

	  if rec.th_rank = 1 then
         SAVEPOINT do_process;
      end if;
      -------------------------------------
      --begin SVC_TIMELINE_HEAD processing
      -------------------------------------
      if NOT L_head_error then
         --process only the first header record for a given TIMELINE_NO
         if rec.stth_action is NOT NULL and rec.th_rank = 1 then
            L_timeline_head_temp_rec.timeline_desc := rec.stth_timeline_desc;
            L_timeline_head_temp_rec.timeline_type := rec.stth_timeline_type;
            L_timeline_head_temp_rec.timeline_base := rec.stth_timeline_base;
            L_timeline_head_temp_rec.timeline_no   := rec.stth_timeline_no;
            if rec.stth_action = action_new then
               --get the next TIMELINE_NO when action is NEW
               L_timeline_no := NULL;
               if TIMELINE_SQL.NEXT_TMLN_NO(L_timeline_no,
                                            O_error_message) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_head_table,
                              rec.stth_row_seq,
                              NULL,
                              O_error_message);
                  L_head_process_error := TRUE;
               else
                  L_timeline_head_temp_rec.timeline_no := L_timeline_no;                                         
                  if EXEC_TIMELINE_HEAD_INS(O_error_message,
                                            L_timeline_head_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_head_table,
                                 rec.stth_row_seq,
                                 NULL,
                                 O_error_message);
                     L_head_process_error := TRUE;
                  else
                     --update timeline_no of matching record in SVC_TIMELINE_HEAD_TL
                     update svc_timeline_head_tl
                        set timeline_no = L_timeline_no
                      where timeline_no = rec.stth_timeline_no
                        and process_id = I_process_id
                        and chunk_id = I_chunk_id
                        and action = action_new;
                  end if;
               end if;
            end if;
            if rec.stth_action = action_mod then
               if EXEC_TIMELINE_HEAD_UPD(O_error_message,
                                         L_timeline_head_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_head_table,
                              rec.stth_row_seq,
                              NULL,
                              O_error_message);
                  L_head_process_error := TRUE;
               end if;
            end if;
            if rec.stth_action = action_del then
               if EXEC_TIMELINE_HEAD_DEL(O_error_message,
                                         rec,
                                         L_timeline_head_temp_rec)= FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_head_table,
                              rec.stth_row_seq,
                              NULL,
                              O_error_message);
                  L_head_process_error := TRUE;
               end if;
            end if;
            if L_head_process_error then
               if rec.stts_row_seq is NOT NULL then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_steps_table,
                              rec.stts_row_seq,
                              'TIMELINE_NO',
                              'ERR_IN_TMLN_HEAD');
               end if;
            end if;
         end if;
         --steps processing
         if rec.stth_action IN (action_new, action_mod) or rec.stth_action is NULL then
      	    if rec.stts_action is NOT NULL
      			   and NOT L_steps_error
      			   and NOT L_head_process_error then
               if rec.stth_action is NOT NULL
                  and rec.stth_action = action_new then
                  L_timeline_steps_temp_rec.timeline_no  := L_timeline_head_temp_rec.timeline_no;
               else
                  L_timeline_steps_temp_rec.timeline_no  := rec.stts_timeline_no;
               end if;
               L_timeline_steps_temp_rec.step_no         := rec.stts_step_no;
               L_timeline_steps_temp_rec.timeline_type   := rec.stts_timeline_type;
               L_timeline_steps_temp_rec.days_completed  := rec.stts_days_completed;
               L_timeline_steps_temp_rec.display_seq     := rec.stts_display_seq;

               --validation for invalid timeline number has to be done after processing head
               if L_timeline_steps_temp_rec.timeline_no is NOT NULL then
                  open C_TIMELINE_NO(L_timeline_steps_temp_rec.timeline_no);
                  fetch C_TIMELINE_NO into L_exists;
                  close C_TIMELINE_NO;

                  if L_exists = 'N' then
               	     WRITE_ERROR(rec.stts_process_id,
               	  	 		     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
               	  	 		     rec.stts_chunk_id,
               	  	 		     L_steps_table,
               	  	 		     rec.stts_row_seq,
               	  	 		     'TIMELINE_NO',
               	  	 		     'INV_TMLN_NO');
               	     L_steps_process_error := TRUE;
                  end if;
               end if;

               if rec.stts_action = action_new then
                  if EXEC_TIMELINE_STEPS_INS(O_error_message,
                                             L_timeline_steps_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_steps_table,
                                 rec.stts_row_seq,
                                 NULL,
                                 O_error_message);
                     L_steps_process_error := TRUE;
                  end if;
               end if;

               if rec.stts_action = action_mod then
                  if EXEC_TIMELINE_STEPS_UPD(O_error_message,
                  							 L_timeline_steps_temp_rec) = FALSE then
               	     WRITE_ERROR(I_process_id,
               			         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_steps_table,
                                 rec.stts_row_seq,
                                 NULL,
                                 O_error_message);
               	     L_steps_process_error := TRUE;
                  end if;
               end if;

               if rec.stts_action = action_del then
                  if EXEC_TIMELINE_STEPS_DEL(O_error_message,
                                             L_timeline_steps_temp_rec) = FALSE then
               	     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_steps_table,
                                 rec.stts_row_seq,
                                 NULL,
                                 O_error_message);
               	     L_steps_process_error := TRUE;
                  end if;
               end if;
               if NOT L_steps_process_error then
                  L_row_seq_tab.extend();
                  L_row_seq_tab(c) := rec.stts_row_seq;
                  c := c+1;
               end if;
            end if; --if rec.stth_action NOT IN (action_del)
      	 else
      	    if rec.stts_rid is NOT NULL and rec.stts_action not in (action_del) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_steps_table,
                           rec.stts_row_seq,
                           'Timeline Number',
                           'TIMELINE_HEAD_REC_DEL');
               L_steps_process_error := TRUE;
            end if;
         end if; --end of steps processing
      end if; --end of NOT L_head_error
         ---------------------------------------------------------------
         --Rollback if no TIMELINE_STEPS exists for a TIMELINE_HEAD or
         --if trying to delete the last TIMELINE_STEPS record
         ---------------------------------------------------------------

      if NOT L_head_process_error and NOT L_head_error then
         if NVL(rec.stth_timeline_no,-1) <> NVL(rec.next_stth_timeline_no,-1) then

            --check if code_head action is del
            if rec.stth_action <> action_del or rec.stth_action is NULL then
               if rec.stth_action is NULL then
                  open C_STEPS_ATLEAST1(L_timeline_steps_temp_rec.timeline_no);
               else
                  open C_STEPS_ATLEAST1(L_timeline_head_temp_rec.timeline_no);
               end if;
               fetch C_STEPS_ATLEAST1 into count_steps;
               close C_STEPS_ATLEAST1;
               if count_steps < 1 then
                  --for head action new and no record in detail
                  if rec.stth_action is NOT NULL then
                     if rec.stth_action = action_new then
                        WRITE_ERROR(I_process_id,
                                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                    I_chunk_id,
                                    L_head_table,
                                    rec.stth_row_seq,
                                    'TIMELINE_NO',
                                    'ONE_TIMELINE_STEP_REQ');
                        ROLLBACK TO do_process;
                     else
						WRITE_ERROR(I_process_id,
                                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                    I_chunk_id,
                                    L_head_table,
                                    rec.stth_row_seq,
                                    'TIMELINE_NO',
                                    'DETAIL_PROC_ER');
                        ROLLBACK TO do_process;
                     end if;
                  end if;

                  L_rowseq_count := L_row_seq_tab.count();
                  if L_rowseq_count > 0 then
                     FOR i in 1..L_rowseq_count
                     LOOP
                        if rec.stts_action is NOT NULL and rec.stts_action = action_del then
                        --for TIMELINE_STEPS action is del and it is the last record in TIMELINE_STEPS
                           WRITE_ERROR(I_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                       I_chunk_id,
                                       L_steps_table,
                                       L_row_seq_tab(i),
                                       'Timeline Number,Step Number,Timeline Type',
                                       'ONE_TIMELINE_STEP_REQ');
                           ROLLBACK TO do_process;
                        end if;
                     END LOOP;
                  end if; --L_rowseq_count > 0
               end if; --count_steps < 1
            end if;
         end if; --end of ROLLBACK condition
      end if; --if NOT L_head_process_error and NOT L_head_error then
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_STEPS_ATLEAST1%ISOPEN then
         close C_STEPS_ATLEAST1;
      end if;
      if C_TIMELINE_NO%ISOPEN then
         close C_TIMELINE_NO;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TIMELINE_HEAD;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TIMELINE_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id      IN       SVC_TIMELINE_HEAD_TL.PROCESS_ID%TYPE,
                                  I_chunk_id        IN       SVC_TIMELINE_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                      VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.PROCESS_TIMELINE_HEAD_TL';
   L_error_message                RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_HEAD_TL';
   L_base_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_HEAD';
   L_table                        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TIMELINE_HEAD_TL';
   L_error                        BOOLEAN := FALSE;
   L_process_error                BOOLEAN := FALSE;
   L_timeline_head_TL_temp_rec    TIMELINE_HEAD_TL%ROWTYPE;
   L_timeline_head_TL_upd_rst     ROW_SEQ_TAB;
   L_timeline_head_TL_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_TIMELINE_HEAD_TL(I_process_id NUMBER,
                                 I_chunk_id NUMBER) is
      select pk_timeline_head_tl.rowid  as pk_timeline_head_tl_rid,
             fk_timeline_head.rowid     as fk_timeline_head_rid,
             fk_lang.rowid              as fk_lang_rid,
             st.lang,
             st.timeline_no,
             st.timeline_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_timeline_head_tl  st,
             timeline_head      fk_timeline_head,
             timeline_head_tl   pk_timeline_head_tl,
             lang               fk_lang
       where st.process_id   =  I_process_id
         and st.chunk_id     =  I_chunk_id
         and st.timeline_no  =  fk_timeline_head.timeline_no (+)
         and st.lang         =  pk_timeline_head_tl.lang (+)
         and st.timeline_no  =  pk_timeline_head_tl.timeline_no (+)
         and st.lang         =  fk_lang.lang (+);

   TYPE SVC_TIMELINE_HEAD_TL is TABLE OF C_SVC_TIMELINE_HEAD_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_timeline_head_tab        SVC_TIMELINE_HEAD_TL;

   L_timeline_head_TL_ins_tab         timeline_head_tab         := NEW timeline_head_tab();
   L_timeline_head_TL_upd_tab         timeline_head_tab         := NEW timeline_head_tab();
   L_timeline_head_TL_del_tab         timeline_head_tab         := NEW timeline_head_tab();

BEGIN
   if C_SVC_TIMELINE_HEAD_TL%ISOPEN then
      close C_SVC_TIMELINE_HEAD_TL;
   end if;

   open C_SVC_TIMELINE_HEAD_TL(I_process_id,
                               I_chunk_id);
   LOOP
      fetch C_SVC_TIMELINE_HEAD_TL bulk collect into L_svc_timeline_head_tab limit LP_bulk_fetch_limit;
      if L_svc_timeline_head_tab.COUNT > 0 then
         FOR i in L_svc_timeline_head_tab.FIRST..L_svc_timeline_head_tab.LAST LOOP
            L_error := FALSE;

            --check if action is valid
            if L_svc_timeline_head_tab(i).action is NULL
               or L_svc_timeline_head_tab(i).action NOT IN (action_new, action_mod, action_del) then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_head_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error := TRUE;
            end if;

            --check for primary_lang
            if L_svc_timeline_head_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_head_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            --check if primary key values already exist
            if L_svc_timeline_head_tab(i).action = action_new
               and L_svc_timeline_head_tab(i).pk_timeline_head_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_head_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_timeline_head_tab(i).action IN (action_mod, action_del)
               and L_svc_timeline_head_tab(i).lang is NOT NULL
               and L_svc_timeline_head_tab(i).timeline_no is NOT NULL
               and L_svc_timeline_head_tab(i).pk_timeline_head_tl_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_head_tab(i).row_seq,
                           NULL,
                           'NO_RECORD_UPD_DEL');
               L_error := TRUE;
            end if;

            --check for FK
            if L_svc_timeline_head_tab(i).action = action_new
               and L_svc_timeline_head_tab(i).timeline_no is NOT NULL
               and L_svc_timeline_head_tab(i).fk_timeline_head_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_head_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_timeline_head_tab(i).action = action_new
               and L_svc_timeline_head_tab(i).lang is NOT NULL
               and L_svc_timeline_head_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_head_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error := TRUE;
            end if;

            --check for required fields
            if L_svc_timeline_head_tab(i).action in (action_new, action_mod) then
               if L_svc_timeline_head_tab(i).timeline_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_timeline_head_tab(i).row_seq,
                              'TIMELINE_DESC',
                              'MUST_ENTER_FIELD');
                  L_error := TRUE;
               end if;
            end if;

            if L_svc_timeline_head_tab(i).timeline_no is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_head_tab(i).row_seq,
                           'TIMELINE_NO',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_timeline_head_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_head_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if NOT L_error then
               L_timeline_head_TL_temp_rec.lang                 := L_svc_timeline_head_tab(i).lang;
               L_timeline_head_TL_temp_rec.timeline_no          := L_svc_timeline_head_tab(i).timeline_no;
               L_timeline_head_TL_temp_rec.timeline_desc        := L_svc_timeline_head_tab(i).timeline_desc;
               L_timeline_head_TL_temp_rec.create_datetime      := SYSDATE;
               L_timeline_head_TL_temp_rec.create_id            := GET_USER;
               L_timeline_head_TL_temp_rec.last_update_datetime := SYSDATE;
               L_timeline_head_TL_temp_rec.last_update_id       := GET_USER;

               if L_svc_timeline_head_tab(i).action = action_new then
                  L_timeline_head_TL_ins_tab.extend;
                  L_timeline_head_TL_ins_tab(L_timeline_head_TL_ins_tab.count()) := L_timeline_head_TL_temp_rec;
               end if;

               if L_svc_timeline_head_tab(i).action = action_mod then
                  L_timeline_head_TL_upd_tab.extend;
                  L_timeline_head_TL_upd_tab(L_timeline_head_TL_upd_tab.count()) := L_timeline_head_TL_temp_rec;
                  L_timeline_head_TL_upd_rst(L_timeline_head_TL_upd_tab.count()) := L_svc_timeline_head_tab(i).row_seq;
               end if;

               if L_svc_timeline_head_tab(i).action = action_del then
                  L_timeline_head_TL_del_tab.extend;
                  L_timeline_head_TL_del_tab(L_timeline_head_TL_del_tab.count()) := L_timeline_head_TL_temp_rec;
                  L_timeline_head_TL_del_rst(L_timeline_head_TL_del_tab.count()) := L_svc_timeline_head_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_TIMELINE_HEAD_TL%NOTFOUND;
   END LOOP;
   close C_SVC_TIMELINE_HEAD_TL;

   if EXEC_TIMELINE_HEAD_TL_INS(O_error_message,
                                L_timeline_head_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_TIMELINE_HEAD_TL_UPD(O_error_message,
                                L_timeline_head_tl_upd_tab,
                                L_timeline_head_tl_upd_rst,
                                I_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_TIMELINE_HEAD_TL_DEL(O_error_message,
                                L_timeline_head_tl_del_tab,
                                L_timeline_head_tl_del_rst,
                                I_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TIMELINE_HEAD_TL;
--------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_STEP_COMP_INS(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_timeline_step_comp_temp_rec   IN       TIMELINE_STEP_COMP%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_STEP_COMP_INS';
BEGIN
   insert into timeline_step_comp
      values I_timeline_step_comp_temp_rec;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_STEP_COMP_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_STEP_COMP_UPD(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_timeline_step_comp_temp_rec   IN       TIMELINE_STEP_COMP%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_STEP_COMP_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_STEP_COMP';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TSTEP_COMP_UPD is
      select 'x'
        from timeline_step_comp
       where step_no       = I_timeline_step_comp_temp_rec.step_no
         and timeline_type = I_timeline_step_comp_temp_rec.timeline_type
         for update nowait;
BEGIN
   open  C_LOCK_TSTEP_COMP_UPD;
   close C_LOCK_TSTEP_COMP_UPD;

   update timeline_step_comp
      set row           = I_timeline_step_comp_temp_rec
    where step_no       = I_timeline_step_comp_temp_rec.step_no
      and timeline_type = I_timeline_step_comp_temp_rec.timeline_type;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_timeline_step_comp_temp_rec.step_no,
                                            I_timeline_step_comp_temp_rec.timeline_type);
      return FALSE;
   when OTHERS then
      if C_LOCK_TSTEP_COMP_UPD%ISOPEN then
         close C_LOCK_TSTEP_COMP_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_STEP_COMP_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TIMELINE_STEP_COMP_DEL(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_process_error                 IN OUT   BOOLEAN,
                                     I_timeline_step_comp_temp_rec   IN       TIMELINE_STEP_COMP%ROWTYPE,
                                     I_rec                           IN       C_SVC_TIMELINE_STEP_COMP%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TIMELINE_STEP_COMP_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_STEP_COMP';
   L_exists        BOOLEAN                           := NULL;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TSTEP_COMP_TL_DEL is
      select 'x'
        from timeline_step_comp_tl
       where step_no       = I_timeline_step_comp_temp_rec.step_no
         and timeline_type = I_timeline_step_comp_temp_rec.timeline_type
         for update nowait;

   cursor C_LOCK_TSTEP_COMP_DEL is
      select 'x'
        from timeline_step_comp
       where step_no       = I_timeline_step_comp_temp_rec.step_no
         and timeline_type = I_timeline_step_comp_temp_rec.timeline_type
         for update nowait;

BEGIN
   --This function determines which timeline type the step number belongs.
   --If the step_no has an entry in TIMELINE table, then the record cannot be deleted.
   if TIMELINE_SQL.VALIDATE_STEP_TYPE(O_error_message,
                                      L_exists,
                                      I_timeline_step_comp_temp_rec.timeline_type,
                                      I_timeline_step_comp_temp_rec.step_no) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'Timeline Type,Step No',
                  O_error_message);
      O_process_error := TRUE;
   else
      if L_exists then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'STEP_NO',
                     'CANNOT_DEL_STEP_NO');
         O_process_error := TRUE;
      else
         L_table := 'TIMELINE_STEP_COMP_TL';
         open  C_LOCK_TSTEP_COMP_TL_DEL;
         close C_LOCK_TSTEP_COMP_TL_DEL;
         delete from timeline_step_comp_tl
          where step_no = I_timeline_step_comp_temp_rec.step_no
            and timeline_type = I_timeline_step_comp_temp_rec.timeline_type;

         L_table := 'TIMELINE_STEP_COMP';
         open  C_LOCK_TSTEP_COMP_DEL;
         close C_LOCK_TSTEP_COMP_DEL;
         delete from timeline_step_comp
          where step_no = I_timeline_step_comp_temp_rec.step_no
            and timeline_type = I_timeline_step_comp_temp_rec.timeline_type;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_timeline_step_comp_temp_rec.step_no,
                                            I_timeline_step_comp_temp_rec.timeline_type);
      return FALSE;
   when OTHERS then
      if C_LOCK_TSTEP_COMP_TL_DEL%ISOPEN then
         close C_LOCK_TSTEP_COMP_TL_DEL;
      end if;
      if C_LOCK_TSTEP_COMP_DEL%ISOPEN then
         close C_LOCK_TSTEP_COMP_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TIMELINE_STEP_COMP_DEL;
---------------------------------------------------------------------------------------------
FUNCTION EXEC_TSTEP_COMP_TL_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tstep_comp_tl_ins_tab    IN       TIMELINE_STEP_COMP_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TSTEP_COMP_TL_INS';

BEGIN
   if I_tstep_comp_tl_ins_tab is NOT NULL and I_tstep_comp_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_tstep_comp_tl_ins_tab.COUNT()
         insert into timeline_step_comp_tl
              values I_tstep_comp_tl_ins_tab(i);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_TSTEP_COMP_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TSTEP_COMP_TL_UPD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tstep_comp_tl_upd_tab   IN       TIMELINE_STEP_COMP_TAB,
                                I_tstep_comp_tl_upd_rst   IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_TIMELINE_STEP_COMP_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_TIMELINE_STEP_COMP_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TSTEP_COMP_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_STEP_COMP_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

   cursor C_LOCK_TSTEP_COMP_TL_UPD(I_timeline_type  TIMELINE_STEP_COMP_TL.TIMELINE_TYPE%TYPE,
                                   I_step_no        TIMELINE_STEP_COMP_TL.STEP_NO%TYPE,
                                   I_lang           TIMELINE_STEP_COMP_TL.LANG%TYPE) is
      select 'x'
        from timeline_step_comp_tl
       where timeline_type = I_timeline_type
         and step_no = I_step_no
         and lang = I_lang
         for update nowait;

BEGIN
   if I_tstep_comp_tl_upd_tab is NOT NULL and I_tstep_comp_tl_upd_tab.count > 0 then
      FOR i in I_tstep_comp_tl_upd_tab.FIRST..I_tstep_comp_tl_upd_tab.LAST LOOP
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_tstep_comp_tl_upd_tab(i).lang);
            L_key_val2 := 'Timeline Type: '||to_char(I_tstep_comp_tl_upd_tab(i).timeline_type);
            open C_LOCK_TSTEP_COMP_TL_UPD(I_tstep_comp_tl_upd_tab(i).timeline_type,
                                          I_tstep_comp_tl_upd_tab(i).step_no,
                                          I_tstep_comp_tl_upd_tab(i).lang);
            close C_LOCK_TSTEP_COMP_TL_UPD;
            
            update timeline_step_comp_tl
               set step_desc = i_tstep_comp_tl_upd_tab(i).step_desc,
                   last_update_id = I_tstep_comp_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_tstep_comp_tl_upd_tab(i).last_update_datetime
             where lang = I_tstep_comp_tl_upd_tab(i).lang
               and step_no = I_tstep_comp_tl_upd_tab(i).step_no
               and timeline_type = I_tstep_comp_tl_upd_tab(i).timeline_type;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                     L_table,
                                                     L_key_val1,
                                                     L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           I_tstep_comp_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      END LOOP;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_LOCK_TSTEP_COMP_TL_UPD%ISOPEN then
         close C_LOCK_TSTEP_COMP_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_TSTEP_COMP_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TSTEP_COMP_TL_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tstep_comp_tl_del_tab   IN       TIMELINE_STEP_COMP_TAB,
                                I_tstep_comp_tl_del_rst   IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_TIMELINE_STEP_COMP_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_TIMELINE_STEP_COMP_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.EXEC_TSTEP_COMP_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_STEP_COMP_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

   cursor C_LOCK_TSTEP_COMP_TL_DEL(I_timeline_type  TIMELINE_STEP_COMP_TL.TIMELINE_TYPE%TYPE,
                                   I_step_no        TIMELINE_STEP_COMP_TL.STEP_NO%TYPE,
                                   I_lang           TIMELINE_STEP_COMP_TL.LANG%TYPE) is
      select 'x'
        from timeline_step_comp_tl
       where timeline_type = I_timeline_type
         and step_no = I_step_no
         and lang = I_lang
         for update nowait;

BEGIN
   if I_tstep_comp_tl_del_tab is NOT NULL and I_tstep_comp_tl_del_tab.count > 0 then
      FOR i in I_tstep_comp_tl_del_tab.FIRST..I_tstep_comp_tl_del_tab.LAST LOOP
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_tstep_comp_tl_del_tab(i).lang);
            L_key_val2 := 'Timeline Type: '||to_char(I_tstep_comp_tl_del_tab(i).timeline_type);
            open C_LOCK_TSTEP_COMP_TL_DEL(I_tstep_comp_tl_del_tab(i).timeline_type,
                                          I_tstep_comp_tl_del_tab(i).step_no,
                                      I_tstep_comp_tl_del_tab(i).lang);
            close C_LOCK_TSTEP_COMP_TL_DEL;
            
            delete timeline_step_comp_tl
             where lang = I_tstep_comp_tl_del_tab(i).lang
               and step_no = I_tstep_comp_tl_del_tab(i).step_no
               and timeline_type = I_tstep_comp_tl_del_tab(i).timeline_type;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                     L_table,
                                                     L_key_val1,
                                                     L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           I_tstep_comp_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      END LOOP;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_LOCK_TSTEP_COMP_TL_DEL%ISOPEN then
         close C_LOCK_TSTEP_COMP_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_TSTEP_COMP_TL_DEL;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_TIMELINE_STEP_COMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_process_id      IN       SVC_TIMELINE_STEP_COMP.PROCESS_ID%TYPE,
                                    I_chunk_id        IN       SVC_TIMELINE_STEP_COMP.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                       VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.PROCESS_TIMELINE_STEP_COMP';
   L_table                         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TIMELINE_STEP_COMP';
   L_process_error                 BOOLEAN                           := FALSE;
   L_error                         BOOLEAN;
   L_timeline_step_comp_temp_rec   TIMELINE_STEP_COMP%ROWTYPE;
   L_step_no                       TIMELINE_STEP_COMP.STEP_NO%TYPE;

BEGIN
   FOR rec IN C_SVC_TIMELINE_STEP_COMP(I_process_id,
                                       I_chunk_id) LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.PK_TIMELINE_STEP_COMP_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Timeline Type,Step No',
                     'TMLN_STEP_COMP_DUP');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_TIMELINE_STEP_COMP_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Timeline Type,Step No',
                     'TMLN_STEP_COMP_MISSING');
         L_error := TRUE;
      end if;

      if rec.action IN (action_new,action_mod) then
         if NOT(rec.timeline_type IN ('PO','POIT','IT','CE','TR','TRBL','TRCO','TRPI','TRCI','TRPOBL')) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'TIMELINE_TYPE',
                        'TMLN_TYPE_INV');
            L_error := TRUE;
         end if;
         if NOT(rec.step_desc is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'STEP_DESC',
                        'TMLN_STEP_DESC_IS_NULL');
            L_error := TRUE;
         end if;
      end if;

      if NOT L_error then
         L_timeline_step_comp_temp_rec.timeline_type := rec.timeline_type;
         L_timeline_step_comp_temp_rec.step_no       := rec.step_no;
         L_timeline_step_comp_temp_rec.step_desc     := rec.step_desc;
         if rec.action = action_new then
            if TIMELINE_SQL.NEXT_STEP_NO(L_step_no,
                                         O_error_message,
                                         rec.timeline_type) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'STEP_NO',
                           O_error_message);
               L_process_error := TRUE;
            else
               L_timeline_step_comp_temp_rec.step_no := L_step_no;
               if EXEC_TIMELINE_STEP_COMP_INS(O_error_message,
                                              L_timeline_step_comp_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              NULL,
                              O_error_message);
                  L_process_error := TRUE;
               else
                  --update step_no of matching record in SVC_TIMELINE_STEP_COMP_TL
                  update svc_timeline_step_comp_tl
                     set step_no = L_step_no
                   where step_no = rec.step_no
                     and timeline_type = rec.timeline_type
                     and process_id = I_process_id
                     and chunk_id = I_chunk_id
                     and action = action_new;
               end if;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_TIMELINE_STEP_COMP_UPD(O_error_message,
                                           L_timeline_step_comp_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_TIMELINE_STEP_COMP_DEL(O_error_message,
                                           L_process_error,
                                           L_timeline_step_comp_temp_rec,
                                           rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TIMELINE_STEP_COMP;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TSTEP_COMP_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_TIMELINE_STEP_COMP_TL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_TIMELINE_STEP_COMP_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64)                      := 'CORESVC_TLHEAD_TLSTEPCOMP.PROCESS_TSTEP_COMP_TL';
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_STEP_COMP_TL';
   L_base_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TIMELINE_STEP_COMP';
   L_table                    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TIMELINE_STEP_COMP_TL';
   L_error                    BOOLEAN := FALSE;
   L_process_error            BOOLEAN := FALSE;
   L_tstep_comp_tl_temp_rec   TIMELINE_STEP_COMP_TL%ROWTYPE;
   L_tstep_comp_tl_upd_rst    ROW_SEQ_TAB;
   L_tstep_comp_tl_del_rst    ROW_SEQ_TAB;

   cursor C_SVC_TIMELINE_STEP_COMP_TL(I_process_id   NUMBER,
                                      I_chunk_id     NUMBER) is
      select pk_timeline_step_comp_tl.rowid  as pk_timeline_step_comp_tl_rid,
             fk_timeline_step_comp.rowid     as fk_timeline_step_comp_rid,
             fk_lang.rowid                   as fk_lang_rid,
             st.lang,
             st.timeline_type,
             st.step_no,
             st.step_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)                as action,
             st.process$status
        from svc_timeline_step_comp_tl  st,
             timeline_step_comp         fk_timeline_step_comp,
             timeline_step_comp_tl      pk_timeline_step_comp_tl,
             lang          fk_lang
       where st.process_id     =  I_process_id
         and st.chunk_id       =  I_chunk_id
         and st.timeline_type  =  fk_timeline_step_comp.timeline_type (+)
         and st.step_no        =  fk_timeline_step_comp.step_no (+)
         and st.lang           =  pk_timeline_step_comp_tl.lang (+)
         and st.timeline_type  =  pk_timeline_step_comp_tl.timeline_type (+)
         and st.step_no        =  pk_timeline_step_comp_tl.step_no (+)
         and st.lang           =  fk_lang.lang (+);

   TYPE SVC_TIMELINE_STEP_COMP_TL is TABLE OF C_SVC_TIMELINE_STEP_COMP_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_timeline_step_comp_tab   SVC_TIMELINE_STEP_COMP_TL;

   L_tstep_comp_tl_ins_tab   TIMELINE_STEP_COMP_TAB := NEW TIMELINE_STEP_COMP_TAB();
   L_tstep_comp_tl_upd_tab   TIMELINE_STEP_COMP_TAB := NEW TIMELINE_STEP_COMP_TAB();
   L_tstep_comp_tl_del_tab   TIMELINE_STEP_COMP_TAB := NEW TIMELINE_STEP_COMP_TAB();

BEGIN
   if C_SVC_TIMELINE_STEP_COMP_TL%ISOPEN then
      close C_SVC_TIMELINE_STEP_COMP_TL;
   end if;

   open C_SVC_TIMELINE_STEP_COMP_TL(I_process_id,
                                    I_chunk_id);
   LOOP
      fetch C_SVC_TIMELINE_STEP_COMP_TL bulk collect into L_svc_timeline_step_comp_tab limit LP_bulk_fetch_limit;
      if L_svc_timeline_step_comp_tab.COUNT > 0 then
         FOR i in L_svc_timeline_step_comp_tab.FIRST..L_svc_timeline_step_comp_tab.LAST LOOP
            L_error := FALSE;

            --check if action is valid
            if L_svc_timeline_step_comp_tab(i).action is NULL
               or L_svc_timeline_step_comp_tab(i).action NOT IN (action_new, action_mod, action_del) then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_step_comp_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error := TRUE;
            end if;

            --check for primary_lang
            if L_svc_timeline_step_comp_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_step_comp_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            --check if primary key values already exist
            if L_svc_timeline_step_comp_tab(i).action = action_new
               and L_svc_timeline_step_comp_tab(i).pk_timeline_step_comp_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_step_comp_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_timeline_step_comp_tab(i).action IN (action_mod, action_del)
               and L_svc_timeline_step_comp_tab(i).lang is NOT NULL
               and L_svc_timeline_step_comp_tab(i).step_no is NOT NULL
               and L_svc_timeline_step_comp_tab(i).timeline_type is NOT NULL
               and L_svc_timeline_step_comp_tab(i).pk_timeline_step_comp_tl_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_step_comp_tab(i).row_seq,
                           NULL,
                           'NO_RECORD_UPD_DEL');
               L_error := TRUE;
            end if;

            --check for FK
            if L_svc_timeline_step_comp_tab(i).action = action_new
               and L_svc_timeline_step_comp_tab(i).step_no is NOT NULL
               and L_svc_timeline_step_comp_tab(i).timeline_type is NOT NULL
               and L_svc_timeline_step_comp_tab(i).fk_timeline_step_comp_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_step_comp_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_timeline_step_comp_tab(i).action = action_new
               and L_svc_timeline_step_comp_tab(i).lang is NOT NULL
               and L_svc_timeline_step_comp_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_step_comp_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error := TRUE;
            end if;

            --check for required fields
            if L_svc_timeline_step_comp_tab(i).action in (action_new, action_mod) then
               if L_svc_timeline_step_comp_tab(i).step_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_timeline_step_comp_tab(i).row_seq,
                              'STEP_DESC',
                              'MUST_ENTER_FIELD');
                  L_error := TRUE;
               end if;
            end if;

            if L_svc_timeline_step_comp_tab(i).step_no is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_step_comp_tab(i).row_seq,
                           'STEP_NO',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_timeline_step_comp_tab(i).timeline_type is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_step_comp_tab(i).row_seq,
                           'TIMELINE_TYPE',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_timeline_step_comp_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_timeline_step_comp_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error := TRUE;
            end if;

            if NOT L_error then
               L_tstep_comp_tl_temp_rec.lang                 := L_svc_timeline_step_comp_tab(i).lang;
               L_tstep_comp_tl_temp_rec.timeline_type        := L_svc_timeline_step_comp_tab(i).timeline_type;
               L_tstep_comp_tl_temp_rec.step_no              := L_svc_timeline_step_comp_tab(i).step_no;
               L_tstep_comp_tl_temp_rec.step_desc            := L_svc_timeline_step_comp_tab(i).step_desc;
               L_tstep_comp_tl_temp_rec.create_datetime      := SYSDATE;
               L_tstep_comp_tl_temp_rec.create_id            := GET_USER;
               L_tstep_comp_tl_temp_rec.last_update_datetime := SYSDATE;
               L_tstep_comp_tl_temp_rec.last_update_id       := GET_USER;

               if L_svc_timeline_step_comp_tab(i).action = action_new then
                  L_tstep_comp_tl_ins_tab.extend;
                  L_tstep_comp_tl_ins_tab(L_tstep_comp_tl_ins_tab.count()) := L_tstep_comp_tl_temp_rec;
               end if;

               if L_svc_timeline_step_comp_tab(i).action = action_mod then
                  L_tstep_comp_tl_upd_tab.extend;
                  L_tstep_comp_tl_upd_tab(L_tstep_comp_tl_upd_tab.count()) := L_tstep_comp_tl_temp_rec;
                  L_tstep_comp_tl_upd_rst(L_tstep_comp_tl_upd_tab.count()) := L_svc_timeline_step_comp_tab(i).row_seq;
               end if;

               if L_svc_timeline_step_comp_tab(i).action = action_del then
                  L_tstep_comp_tl_del_tab.extend;
                  L_tstep_comp_tl_del_tab(L_tstep_comp_tl_del_tab.count()) := L_tstep_comp_tl_temp_rec;
                  L_tstep_comp_tl_del_rst(L_tstep_comp_tl_del_tab.count()) := L_svc_timeline_step_comp_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_TIMELINE_STEP_COMP_TL%NOTFOUND;
   END LOOP;
   close C_SVC_TIMELINE_STEP_COMP_TL;

   if EXEC_TSTEP_COMP_TL_INS(O_error_message,
                             L_tstep_comp_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_TSTEP_COMP_TL_UPD(O_error_message,
                             L_tstep_comp_tl_upd_tab,
                             L_tstep_comp_tl_upd_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_TSTEP_COMP_TL_DEL(O_error_message,
                             L_tstep_comp_tl_del_tab,
                             L_tstep_comp_tl_del_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TSTEP_COMP_TL;
------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS
BEGIN
   delete from svc_timeline_head_tl
    where process_id = i_process_id;

   delete from svc_timeline_head
    where process_id = i_process_id;

   delete from svc_timeline_steps
    where process_id = I_process_id;
    
   delete from svc_timeline_step_comp_tl
    where process_id = I_process_id;

   delete from svc_timeline_step_comp
    where process_id = I_process_id;
END;
--------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    := 'CORESVC_TLHEAD_TLSTEPCOMP.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_TIMELINE_HEAD(O_error_message,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_TIMELINE_HEAD_TL(O_error_message,
                               I_process_id,
                               I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_TIMELINE_STEP_COMP(O_error_message,
                                 I_process_id,
                                 I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_TSTEP_COMP_TL(O_error_message,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  C_GET_ERR_COUNT;
   fetch C_GET_ERR_COUNT into L_err_count;
   close C_GET_ERR_COUNT;

   open  C_GET_WARN_COUNT;
   fetch C_GET_WARN_COUNT into L_warn_count;
   close C_GET_WARN_COUNT;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;
   --clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------
END CORESVC_TLHEAD_TLSTEPCOMP;
/