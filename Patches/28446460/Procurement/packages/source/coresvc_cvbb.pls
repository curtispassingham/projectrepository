create or replace PACKAGE BODY CORESVC_CVB AS
   cursor C_SVC_CVB_HEAD(I_process_id NUMBER,
                         I_chunk_id NUMBER) is
      select pk_cvb_head.rowid    as pk_cvb_head_rid,
             st.rowid             as st_rid,
             UPPER(st.cvb_code)   as cvb_code,
             st.cvb_desc,
             UPPER(st.nom_flag_1) as nom_flag_1,
             UPPER(st.nom_flag_2) as nom_flag_2,
             UPPER(st.nom_flag_3) as nom_flag_3,
             UPPER(st.nom_flag_4) as nom_flag_4,
             UPPER(st.nom_flag_5) as nom_flag_5,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)     as action,
             st.process$status
        from svc_cvb_head st,
             cvb_head pk_cvb_head
       where st.process_id          = I_process_id
         and st.chunk_id            = I_chunk_id
         and UPPER(st.cvb_code)     = pk_cvb_head.cvb_code(+);

   -- CVB_DETAIL CURSOR
   cursor C_SVC_CVB_DETAIL(I_process_id NUMBER,I_chunk_id NUMBER) is
      select pk_cvb_detail.rowid  as pk_cvb_detail_rid,
             cvd_cvb_fk.rowid     as cvd_cvb_fk_rid,
             cvd_elc_fk.rowid     as cvd_elc_fk_rid,
             st.rowid             as st_rid,
             UPPER(st.cvb_code)   as cvb_code,
             UPPER(st.comp_id)    as comp_id,
             st.combo_oper,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)     as action,
             st.process$status,
             row_number() over (partition BY st.cvb_code order by st.cvb_code) as cvb_code_rank
        from svc_cvb_detail st,
             cvb_detail pk_cvb_detail,
             cvb_head cvd_cvb_fk,
             elc_comp cvd_elc_fk
       where st.process_id              = I_process_id
         and st.chunk_id                = I_chunk_id
         and UPPER(st.comp_id)          = pk_cvb_detail.comp_id (+)
         and UPPER(st.cvb_code)         = pk_cvb_detail.cvb_code (+)
         and UPPER(st.comp_id)          = cvd_elc_fk.comp_id (+)
         and UPPER(st.cvb_code)         = cvd_cvb_fk.cvb_code (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab     errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;

   Type CVB_HEAD_TL_TAB IS TABLE OF CVB_HEAD_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
-----------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if SHEET_NAME_TRANS.EXISTS(I_sheet_name) then
      return SHEET_NAME_TRANS(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
-----------------------------------------------------------------------------------------

PROCEDURE WRITE_S9T_ERROR(I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet   IN VARCHAR2,
                          I_row_seq IN NUMBER,
                          I_col     IN VARCHAR2,
                          I_sqlcode IN NUMBER,
                          I_sqlerrm IN VARCHAR2) IS
BEGIN
   Lp_s9t_errors_tab.EXTEND();
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).file_id              := I_file_id;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).error_seq_no         := s9t_errors_seq.NEXTVAL;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).template_key         := template_key;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).wksht_key            := I_sheet;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).column_key           := I_col;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).row_seq              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).error_key            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).create_id            := GET_USER;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).create_datetime      := SYSDATE;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).last_update_id       := GET_USER;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).last_update_datetime := SYSDATE;
END WRITE_S9T_ERROR;

-----------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
-----------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets           S9T_PKG.NAMES_MAP_TYP;
   CVB_HEAD_cols      S9T_PKG.NAMES_MAP_TYP;
   CVB_DETAIL_cols    S9T_PKG.NAMES_MAP_TYP;
   CVB_HEAD_TL_cols   S9T_PKG.NAMES_MAP_TYP;
BEGIN
   L_sheets                         :=s9t_pkg.get_sheet_names(I_file_id);

   --CVB HEAD columns
   CVB_HEAD_cols                    :=s9t_pkg.get_col_names(I_file_id,CVB_HEAD_sheet);
   CVB_HEAD$Action                  := CVB_HEAD_cols('ACTION');
   CVB_HEAD$CVB_CODE                := CVB_HEAD_cols('CVB_CODE');
   CVB_HEAD$CVB_DESC                := CVB_HEAD_cols('CVB_DESC');
   CVB_HEAD$NOM_FLAG_1              := CVB_HEAD_cols('NOM_FLAG_1');
   CVB_HEAD$NOM_FLAG_2              := CVB_HEAD_cols('NOM_FLAG_2');
   CVB_HEAD$NOM_FLAG_3              := CVB_HEAD_cols('NOM_FLAG_3');
   CVB_HEAD$NOM_FLAG_4              := CVB_HEAD_cols('NOM_FLAG_4');
   CVB_HEAD$NOM_FLAG_5              := CVB_HEAD_cols('NOM_FLAG_5');

   -- CVB_DETAIL columns
   CVB_DETAIL_cols                  :=s9t_pkg.get_col_names(I_file_id,CVB_DETAIL_sheet);
   CVB_DETAIL$Action                := CVB_DETAIL_cols('ACTION');
   CVB_DETAIL$CVB_CODE              := CVB_DETAIL_cols('CVB_CODE');
   CVB_DETAIL$COMP_ID               := CVB_DETAIL_cols('COMP_ID');
   CVB_DETAIL$COMBO_OPER            := CVB_DETAIL_cols('COMBO_OPER');

   --CVB HEAD_TL columns
   CVB_HEAD_TL_cols                 :=s9t_pkg.get_col_names(I_file_id,CVB_HEAD_TL_sheet);
   CVB_HEAD_TL$Action               := CVB_HEAD_TL_cols('ACTION');
   CVB_HEAD_TL$LANG                 := CVB_HEAD_TL_cols('LANG');
   CVB_HEAD_TL$CVB_CODE             := CVB_HEAD_TL_cols('CVB_CODE');
   CVB_HEAD_TL$CVB_DESC             := CVB_HEAD_TL_cols('CVB_DESC');

END POPULATE_NAMES;
-------------------------------------------------------------------------------------------
PROCEDURE POPULATE_CVB_HEAD(I_file_id IN NUMBER) IS
BEGIN
   insert into table
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id  = I_file_id
          and ss.sheet_name = CVB_HEAD_sheet)
       select s9t_row(s9t_cells( NULL,
                                 CVB_CODE,
                                 CVB_DESC,
                                 NOM_FLAG_1,
                                 NOM_FLAG_2,
                                 NOM_FLAG_3,
                                 NOM_FLAG_4,
                                 NOM_FLAG_5))
         from CVB_HEAD ;
END POPULATE_CVB_HEAD;
-------------------------------------------------------------------------------------------
PROCEDURE POPULATE_CVB_DETAIL(I_file_id IN NUMBER) IS
BEGIN
   insert into table
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id  = I_file_id
          and ss.sheet_name = CVB_DETAIL_sheet)
       select s9t_row(s9t_cells( NULL,
                                 CVB_CODE,
                                 COMP_ID,
                                 COMBO_OPER))
         from CVB_DETAIL ;
END POPULATE_CVB_DETAIL;
-------------------------------------------------------------------------------------------
PROCEDURE POPULATE_CVB_HEAD_TL(I_file_id IN NUMBER) IS
BEGIN
   insert into table
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id  = I_file_id
          and ss.sheet_name = CVB_HEAD_TL_sheet)
       select s9t_row(s9t_cells( CORESVC_CVB.action_mod,
                                 LANG,
                                 CVB_CODE,
                                 CVB_DESC))
         from CVB_HEAD_TL ;
END POPULATE_CVB_HEAD_TL;
------------------------------------------------------------------------------------------

PROCEDURE INIT_S9T(O_file_id IN OUT NUMBER) IS
   L_file      S9T_FILE;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
BEGIN

   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;

   L_file.user_lang    := GET_USER_LANG;

   --CVB_HEAD SHEET
   L_file.add_sheet(CVB_HEAD_sheet);
   L_file.sheets(L_file.GET_SHEET_INDEX(CVB_HEAD_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                      'CVB_CODE',
                                                                                      'CVB_DESC',
                                                                                      'NOM_FLAG_1',
                                                                                      'NOM_FLAG_2',
                                                                                      'NOM_FLAG_3',
                                                                                      'NOM_FLAG_4',
                                                                                      'NOM_FLAG_5');
   -- CVB DETAIL sheet
   L_file.add_sheet(CVB_DETAIL_sheet);
   L_file.sheets(L_file.GET_SHEET_INDEX(CVB_DETAIL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                        'CVB_CODE',
                                                                                        'COMP_ID',
                                                                                        'COMBO_OPER');

   --CVB_HEAD_TL SHEET
   L_file.add_sheet(CVB_HEAD_TL_sheet);
   L_file.sheets(L_file.GET_SHEET_INDEX(CVB_HEAD_TL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                         'LANG',
                                                                                         'CVB_CODE',
                                                                                         'CVB_DESC');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
-------------------------------------------------------------------------------------------

FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind IN     CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64) :='CORESVC_CVB.CREATE_S9T';
   L_file    S9T_FILE;
BEGIN

   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;


   if I_template_only_ind = 'N' then
      POPULATE_CVB_HEAD(O_file_id);
      POPULATE_CVB_DETAIL(O_file_id);
      POPULATE_CVB_HEAD_TL(O_file_id);
      COMMIT;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,template_key);
   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
----------------------------------------------------------------------------------------------------

PROCEDURE PROCESS_S9T_CVB_HEAD(I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                               I_process_id IN SVC_CVB_HEAD.PROCESS_ID%TYPE) IS
   TYPE svc_CVB_HEAD_col_typ is TABLE OF SVC_CVB_HEAD%ROWTYPE;
   L_temp_rec                            SVC_CVB_HEAD%ROWTYPE;
   svc_CVB_HEAD_col                      svc_CVB_HEAD_col_typ            :=NEW svc_CVB_HEAD_col_typ();
   L_process_id                          SVC_CVB_HEAD.PROCESS_ID%TYPE;
   L_error                               BOOLEAN                         :=FALSE;
   L_default_rec                         SVC_CVB_HEAD%ROWTYPE;
   cursor C_MANDATORY_IND is
      select CVB_CODE_mi,
             CVB_DESC_mi,
             NOM_FLAG_1_mi,
             NOM_FLAG_2_mi,
             NOM_FLAG_3_mi,
             NOM_FLAG_4_mi,
             NOM_FLAG_5_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                                = CORESVC_CVB.template_key
                 and wksht_key                                   = 'CVB_HEAD'
             ) PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('CVB_CODE'   as CVB_CODE,
                                                                'CVB_DESC'   as CVB_DESC,
                                                                'NOM_FLAG_1' as NOM_FLAG_1,
                                                                'NOM_FLAG_2' as NOM_FLAG_2,
                                                                'NOM_FLAG_3' as NOM_FLAG_3,
                                                                'NOM_FLAG_4' as NOM_FLAG_4,
                                                                'NOM_FLAG_5' as NOM_FLAG_5,
                                                                NULL         as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CVB_HEAD';
   L_pk_columns    VARCHAR2(255)  := 'Computation Value Base';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN
   (select CVB_CODE_dv,
           CVB_DESC_dv,
           NOM_FLAG_1_dv,
           NOM_FLAG_2_dv,
           NOM_FLAG_3_dv,
           NOM_FLAG_4_dv,
           NOM_FLAG_5_dv,
           null as dummy
      from (select column_key,
                   default_value
              from s9t_tmpl_cols_def
             where template_key                                    = CORESVC_CVB.template_key
               and wksht_key                                       = 'CVB_HEAD' )
             PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('CVB_CODE'   as CVB_CODE,
                                                                  'CVB_DESC'   as CVB_DESC,
                                                                  'NOM_FLAG_1' as NOM_FLAG_1,
                                                                  'NOM_FLAG_2' as NOM_FLAG_2,
                                                                  'NOM_FLAG_3' as NOM_FLAG_3,
                                                                  'NOM_FLAG_4' as NOM_FLAG_4,
                                                                  'NOM_FLAG_5' as NOM_FLAG_5,
                                                                   NULL        as dummy))) LOOP


      BEGIN
         L_default_rec.CVB_CODE := rec.CVB_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'CVB_HEAD',
                             NULL,
                            'CVB_CODE',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CVB_DESC := rec.CVB_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'CVB_HEAD',
                             NULL,
                            'CVB_DESC',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.NOM_FLAG_1 := rec.NOM_FLAG_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'CVB_HEAD',
                             NULL,
                            'NOM_FLAG_1',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.NOM_FLAG_2 := rec.NOM_FLAG_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'CVB_HEAD',
                             NULL,
                            'NOM_FLAG_2',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.NOM_FLAG_3 := rec.NOM_FLAG_3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'CVB_HEAD',
                             NULL,
                            'NOM_FLAG_3',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.NOM_FLAG_4 := rec.NOM_FLAG_4_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'CVB_HEAD',
                             NULL,
                            'NOM_FLAG_4',
                            'INV_DEFAULT',
                             SQLERRM);
      END;
      BEGIN
         L_default_rec.NOM_FLAG_5 := rec.NOM_FLAG_5_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'CVB_HEAD',
                             NULL,
                            'NOM_FLAG_5',
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND
    into L_mi_rec;
   CLOSE C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(CVB_HEAD$Action))     as ACTION,
                      UPPER(r.get_cell(CVB_HEAD$CVB_CODE))   as CVB_CODE,
                      r.get_cell(CVB_HEAD$CVB_DESC)          as CVB_DESC,
                      UPPER(r.get_cell(CVB_HEAD$NOM_FLAG_1)) as NOM_FLAG_1,
                      UPPER(r.get_cell(CVB_HEAD$NOM_FLAG_2)) as NOM_FLAG_2,
                      UPPER(r.get_cell(CVB_HEAD$NOM_FLAG_3)) as NOM_FLAG_3,
                      UPPER(r.get_cell(CVB_HEAD$NOM_FLAG_4)) as NOM_FLAG_4,
                      UPPER(r.get_cell(CVB_HEAD$NOM_FLAG_5)) as NOM_FLAG_5,
                      r.get_row_seq()                        as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(CVB_HEAD_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.ACTION := rec.ACTION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_HEAD_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CVB_CODE := rec.CVB_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_HEAD_sheet,
                            rec.row_seq,
                            'CVB_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CVB_DESC := rec.CVB_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_HEAD_sheet,
                            rec.row_seq,
                            'CVB_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.NOM_FLAG_1 := rec.NOM_FLAG_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_HEAD_sheet,
                            rec.row_seq,
                            'NOM_FLAG_1',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.NOM_FLAG_2 := rec.NOM_FLAG_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_HEAD_sheet,
                            rec.row_seq,
                            'NOM_FLAG_2',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.NOM_FLAG_3 := rec.NOM_FLAG_3;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_HEAD_sheet,
                            rec.row_seq,
                            'NOM_FLAG_3',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.NOM_FLAG_4 := rec.NOM_FLAG_4;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_HEAD_sheet,
                            rec.row_seq,
                            'NOM_FLAG_4',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.NOM_FLAG_5 := rec.NOM_FLAG_5;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_HEAD_sheet,
                            rec.row_seq,
                           'NOM_FLAG_5',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_CVB.action_new then
         L_temp_rec.CVB_CODE   := NVL( L_temp_rec.CVB_CODE,
                                       L_default_rec.CVB_CODE);
         L_temp_rec.CVB_DESC   := NVL( L_temp_rec.CVB_DESC,
                                       L_default_rec.CVB_DESC);
         L_temp_rec.NOM_FLAG_1 := NVL( L_temp_rec.NOM_FLAG_1,
                                       L_default_rec.NOM_FLAG_1);
         L_temp_rec.NOM_FLAG_2 := NVL( L_temp_rec.NOM_FLAG_2,
                                       L_default_rec.NOM_FLAG_2);
         L_temp_rec.NOM_FLAG_3 := NVL( L_temp_rec.NOM_FLAG_3,
                                       L_default_rec.NOM_FLAG_3);
         L_temp_rec.NOM_FLAG_4 := NVL( L_temp_rec.NOM_FLAG_4,
                                       L_default_rec.NOM_FLAG_4);
         L_temp_rec.NOM_FLAG_5 := NVL( L_temp_rec.NOM_FLAG_5,
                                       L_default_rec.NOM_FLAG_5);
      end if;
      if NOT (L_temp_rec.cvb_code is NOT NULL ) then
         WRITE_S9T_ERROR( I_file_id,
                          CVB_HEAD_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_CVB_HEAD_col.EXTEND();
         svc_CVB_HEAD_col(svc_CVB_HEAD_col.COUNT()):=L_temp_rec;
      end if;

   END LOOP;
BEGIN
   FORALL i IN 1..svc_CVB_HEAD_col.COUNT
   SAVE EXCEPTIONS merge into SVC_CVB_HEAD st using
   (select (case
               when L_mi_rec.CVB_CODE_mi       = 'N'
                and svc_CVB_HEAD_col(i).action = CORESVC_CVB.action_mod
                and s1.CVB_CODE             is NULL
               then mt.CVB_CODE
               else s1.CVB_CODE
               end) as CVB_CODE,
           (case
               when L_mi_rec.CVB_DESC_mi       = 'N'
                and svc_CVB_HEAD_col(i).action = CORESVC_CVB.action_mod
                and s1.CVB_DESC             is NULL
               then mt.CVB_DESC
               else s1.CVB_DESC
               end) as CVB_DESC,
           (case
               when L_mi_rec.NOM_FLAG_1_mi     = 'N'
                and svc_CVB_HEAD_col(i).action = CORESVC_CVB.action_mod
                and s1.NOM_FLAG_1             is NULL
               then mt.NOM_FLAG_1
               else s1.NOM_FLAG_1
               end) as NOM_FLAG_1,
           (case
               when L_mi_rec.NOM_FLAG_2_mi     = 'N'
                and svc_CVB_HEAD_col(i).action = CORESVC_CVB.action_mod
                and s1.NOM_FLAG_2             is NULL
               then mt.NOM_FLAG_2
               else s1.NOM_FLAG_2
               end) as NOM_FLAG_2,
           (case
               when L_mi_rec.NOM_FLAG_3_mi     = 'N'
                and svc_CVB_HEAD_col(i).action = CORESVC_CVB.action_mod
                and s1.NOM_FLAG_3             is NULL
               then mt.NOM_FLAG_3
               else s1.NOM_FLAG_3
               end) as NOM_FLAG_3,
           (case
               when L_mi_rec.NOM_FLAG_4_mi    = 'N'
                and svc_CVB_HEAD_col(i).action = CORESVC_CVB.action_mod
                and s1.NOM_FLAG_4             is NULL
               then mt.NOM_FLAG_4
               else s1.NOM_FLAG_4
               end) as NOM_FLAG_4,
           (case
               when L_mi_rec.NOM_FLAG_5_mi     = 'N'
                and svc_CVB_HEAD_col(i).action = CORESVC_CVB.action_mod
                and s1.NOM_FLAG_5             is NULL
               then mt.NOM_FLAG_5
               else s1.NOM_FLAG_5
               end) as NOM_FLAG_5,
               null as dummy
      from (select svc_CVB_HEAD_col(i).CVB_CODE   as CVB_CODE,
                   svc_CVB_HEAD_col(i).CVB_DESC   as CVB_DESC,
                   svc_CVB_HEAD_col(i).NOM_FLAG_1 as NOM_FLAG_1,
                   svc_CVB_HEAD_col(i).NOM_FLAG_2 as NOM_FLAG_2,
                   svc_CVB_HEAD_col(i).NOM_FLAG_3 as NOM_FLAG_3,
                   svc_CVB_HEAD_col(i).NOM_FLAG_4 as NOM_FLAG_4,
                   svc_CVB_HEAD_col(i).NOM_FLAG_5 as NOM_FLAG_5,
                   null as dummy
              from dual) s1,
                   CVB_HEAD mt
             where mt.CVB_CODE (+)     = s1.CVB_CODE
               and 1 = 1 ) sq ON ( st.CVB_CODE      = sq.CVB_CODE and
                                   svc_CVB_HEAD_col(i).action IN ( CORESVC_CVB.action_mod, CORESVC_CVB.action_del ))
              when matched then update
                                   set PROCESS_ID              = svc_CVB_HEAD_col(i).PROCESS_ID ,
                                       CHUNK_ID                = svc_CVB_HEAD_col(i).CHUNK_ID ,
                                       ROW_SEQ                 = svc_CVB_HEAD_col(i).ROW_SEQ ,
                                       ACTION                  = svc_CVB_HEAD_col(i).ACTION,
                                       PROCESS$STATUS          = svc_CVB_HEAD_col(i).PROCESS$STATUS ,
                                       NOM_FLAG_2              = sq.NOM_FLAG_2 ,
                                       NOM_FLAG_1              = sq.NOM_FLAG_1 ,
                                       NOM_FLAG_4              = sq.NOM_FLAG_4 ,
                                       NOM_FLAG_5              = sq.NOM_FLAG_5 ,
                                       NOM_FLAG_3              = sq.NOM_FLAG_3 ,
                                       CVB_DESC                = sq.CVB_DESC ,
                                       CREATE_ID               = svc_CVB_HEAD_col(i).CREATE_ID ,
                                       CREATE_DATETIME         = svc_CVB_HEAD_col(i).CREATE_DATETIME ,
                                       LAST_UPD_ID             = svc_CVB_HEAD_col(i).LAST_UPD_ID ,
                                       LAST_UPD_DATETIME       = svc_CVB_HEAD_col(i).LAST_UPD_DATETIME WHEN NOT matched then
                                insert (PROCESS_ID ,
                                        CHUNK_ID ,
                                        ROW_SEQ ,
                                        ACTION ,
                                        PROCESS$STATUS ,
                                        CVB_CODE ,
                                        CVB_DESC ,
                                        NOM_FLAG_1 ,
                                        NOM_FLAG_2 ,
                                        NOM_FLAG_3 ,
                                        NOM_FLAG_4 ,
                                        NOM_FLAG_5 ,
                                        CREATE_ID ,
                                        CREATE_DATETIME ,
                                        LAST_UPD_ID ,
                                        LAST_UPD_DATETIME)
                                values (svc_CVB_HEAD_col(i).PROCESS_ID ,
                                        svc_CVB_HEAD_col(i).CHUNK_ID ,
                                        svc_CVB_HEAD_col(i).ROW_SEQ ,
                                        svc_CVB_HEAD_col(i).ACTION ,
                                        svc_CVB_HEAD_col(i).PROCESS$STATUS ,
                                        sq.CVB_CODE ,
                                        sq.CVB_DESC ,
                                        sq.NOM_FLAG_1 ,
                                        sq.NOM_FLAG_2 ,
                                        sq.NOM_FLAG_3 ,
                                        sq.NOM_FLAG_4 ,
                                        sq.NOM_FLAG_5 ,
                                        svc_CVB_HEAD_col(i).CREATE_ID ,
                                        svc_CVB_HEAD_col(i).CREATE_DATETIME ,
                                        svc_CVB_HEAD_col(i).LAST_UPD_ID ,
                                        svc_CVB_HEAD_col(i).LAST_UPD_DATETIME);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          CVB_HEAD_sheet,
                          svc_CVB_HEAD_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_CVB_HEAD;
----------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CVB_HEAD_TL(I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                                  I_process_id IN SVC_CVB_HEAD_TL.PROCESS_ID%TYPE) IS
   TYPE SVC_CVB_HEAD_TL_COL_TYP is TABLE OF SVC_CVB_HEAD_TL%ROWTYPE;
   L_temp_rec                            SVC_CVB_HEAD_TL%ROWTYPE;
   svc_cvb_head_tl_col                   SVC_CVB_HEAD_TL_COL_TYP            :=NEW SVC_CVB_HEAD_TL_COL_TYP();
   L_process_id                          SVC_CVB_HEAD_TL.PROCESS_ID%TYPE;
   L_error                               BOOLEAN                         :=FALSE;
   L_default_rec                         SVC_CVB_HEAD_TL%ROWTYPE;
   cursor C_MANDATORY_IND is
      select CVB_CODE_mi,
             CVB_DESC_mi,
             LANG_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                                = CORESVC_CVB.template_key
                 and wksht_key                                   = 'CVB_HEAD_TL'
             ) PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('CVB_CODE'   as CVB_CODE,
                                                                'CVB_DESC'   as CVB_DESC,
                                                                'LANG'       as LANG));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CVB_HEAD_TL';
   L_pk_columns    VARCHAR2(255)  := 'Computation Value Base, Language';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN
   (select CVB_CODE_dv,
           CVB_DESC_dv,
           LANG_dv
      from (select column_key,
                   default_value
              from s9t_tmpl_cols_def
             where template_key                                    = CORESVC_CVB.template_key
               and wksht_key                                       = 'CVB_HEAD_TL' )
             PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('CVB_CODE'   as CVB_CODE,
                                                                  'CVB_DESC'   as CVB_DESC,
                                                                  'LANG'       as LANG))) LOOP


      BEGIN
         L_default_rec.CVB_CODE := rec.CVB_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             CVB_HEAD_TL_sheet,
                             NULL,
                            'CVB_CODE',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CVB_DESC := rec.CVB_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             CVB_HEAD_TL_sheet,
                             NULL,
                            'CVB_DESC',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             CVB_HEAD_TL_sheet,
                             NULL,
                            'LANG',
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND
    into L_mi_rec;
   CLOSE C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(CVB_HEAD_TL$Action))     as ACTION,
                      r.get_cell(CVB_HEAD_TL$LANG)              as LANG,
                      UPPER(r.get_cell(CVB_HEAD_TL$CVB_CODE))   as CVB_CODE,
                      r.get_cell(CVB_HEAD_TL$CVB_DESC)          as CVB_DESC,
                      r.get_row_seq()                           as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(CVB_HEAD_TL_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.ACTION := rec.ACTION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_HEAD_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CVB_CODE := rec.CVB_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_HEAD_TL_sheet,
                            rec.row_seq,
                            'CVB_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CVB_DESC := rec.CVB_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_HEAD_TL_sheet,
                            rec.row_seq,
                            'CVB_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_HEAD_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_CVB.action_new then
         L_temp_rec.CVB_CODE   := NVL( L_temp_rec.CVB_CODE,
                                       L_default_rec.CVB_CODE);
         L_temp_rec.CVB_DESC   := NVL( L_temp_rec.CVB_DESC,
                                       L_default_rec.CVB_DESC);
         L_temp_rec.LANG       := NVL( L_temp_rec.LANG,
                                       L_default_rec.LANG);
      end if;
      if NOT (L_temp_rec.cvb_code is NOT NULL and L_temp_rec.lang is NOT NULL ) then
         WRITE_S9T_ERROR( I_file_id,
                          CVB_HEAD_TL_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_CVB_HEAD_TL_col.EXTEND();
         svc_CVB_HEAD_TL_col(svc_CVB_HEAD_TL_col.COUNT()):=L_temp_rec;
      end if;

   END LOOP;
BEGIN
   FORALL i IN 1..svc_CVB_HEAD_TL_col.COUNT
   SAVE EXCEPTIONS merge into SVC_CVB_HEAD_TL st using
   (select (case
               when L_mi_rec.CVB_CODE_mi       = 'N'
                and svc_CVB_HEAD_TL_col(i).action = CORESVC_CVB.action_mod
                and s1.CVB_CODE             is NULL
               then mt.CVB_CODE
               else s1.CVB_CODE
               end) as CVB_CODE,
           (case
               when L_mi_rec.CVB_DESC_mi       = 'N'
                and svc_CVB_HEAD_TL_col(i).action = CORESVC_CVB.action_mod
                and s1.CVB_DESC             is NULL
               then mt.CVB_DESC
               else s1.CVB_DESC
               end) as CVB_DESC,
           (case
               when L_mi_rec.LANG_mi     = 'N'
                and svc_CVB_HEAD_TL_col(i).action = CORESVC_CVB.action_mod
                and s1.LANG             is NULL
               then mt.LANG
               else s1.LANG
               end) as LANG
      from (select svc_CVB_HEAD_TL_col(i).CVB_CODE   as CVB_CODE,
                   svc_CVB_HEAD_TL_col(i).CVB_DESC   as CVB_DESC,
                   svc_CVB_HEAD_TL_col(i).LANG       as LANG
              from dual) s1,
                   CVB_HEAD_TL mt
             where mt.LANG (+)     = s1.LANG
               and mt.CVB_CODE (+) = s1.CVB_CODE  ) sq
               ON ( st.LANG        = sq.LANG and
                    st.CVB_CODE    = sq.CVB_CODE and
                    svc_CVB_HEAD_TL_col(i).action IN ( CORESVC_CVB.action_mod, CORESVC_CVB.action_del ))
              when matched then update
                                   set PROCESS_ID              = svc_CVB_HEAD_TL_col(i).PROCESS_ID ,
                                       CHUNK_ID                = svc_CVB_HEAD_TL_col(i).CHUNK_ID ,
                                       ROW_SEQ                 = svc_CVB_HEAD_TL_col(i).ROW_SEQ ,
                                       ACTION                  = svc_CVB_HEAD_TL_col(i).ACTION,
                                       PROCESS$STATUS          = svc_CVB_HEAD_TL_col(i).PROCESS$STATUS ,
                                       CVB_DESC                = sq.CVB_DESC
              WHEN NOT matched then
                                insert (PROCESS_ID ,
                                        CHUNK_ID ,
                                        ROW_SEQ ,
                                        ACTION ,
                                        PROCESS$STATUS ,
                                        CVB_CODE ,
                                        CVB_DESC ,
                                        LANG )
                                values (svc_CVB_HEAD_TL_col(i).PROCESS_ID ,
                                        svc_CVB_HEAD_TL_col(i).CHUNK_ID ,
                                        svc_CVB_HEAD_TL_col(i).ROW_SEQ ,
                                        svc_CVB_HEAD_TL_col(i).ACTION ,
                                        svc_CVB_HEAD_TL_col(i).PROCESS$STATUS ,
                                        sq.CVB_CODE ,
                                        sq.CVB_DESC ,
                                        sq.LANG);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          CVB_HEAD_TL_sheet,
                          svc_CVB_HEAD_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_CVB_HEAD_TL;
-------------------------------------------------------------------------------------------------
--Process s9t function of detail table
-------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CVB_DETAIL(I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id IN SVC_CVB_DETAIL.PROCESS_ID%TYPE) IS

   TYPE svc_CVB_DETAIL_col_typ IS TABLE OF SVC_CVB_DETAIL%ROWTYPE;
   L_temp_rec                              SVC_CVB_DETAIL%ROWTYPE;
   svc_CVB_DETAIL_col                      svc_CVB_DETAIL_col_typ :=NEW svc_CVB_DETAIL_col_typ();
   L_process_id                            SVC_CVB_DETAIL.PROCESS_ID%TYPE;
   L_error                                 BOOLEAN                :=FALSE;
   L_default_rec                           SVC_CVB_DETAIL%ROWTYPE;
   cursor C_MANDATORY_IND is
      select CVB_CODE_mi,
             COMP_ID_mi,
             COMBO_OPER_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key   = CORESVC_CVB.template_key
                 and wksht_key      = 'CVB_DETAIL'
              ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ('CVB_CODE'   as CVB_CODE,
                                                                 'COMP_ID'    as COMP_ID,
                                                                 'COMBO_OPER' as COMBO_OPER,
                                                                  NULL        as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CVB_DETAIL';
   L_pk_columns    VARCHAR2(255)  := 'Computation Value Base, Component';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
   -- Get default values.
   FOR rec IN
   (select CVB_CODE_dv,
           COMP_ID_dv,
           COMBO_OPER_dv,
           NULL as dummy
      from (select column_key,
                   default_value
              from s9t_tmpl_cols_def
             where template_key     = CORESVC_CVB.template_key
               and wksht_key        = 'CVB_DETAIL'
            ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('CVB_CODE'   as CVB_CODE,
                                                                   'COMP_ID'    as COMP_ID,
                                                                   'COMBO_OPER' as COMBO_OPER,
                                                                    NULL        as dummy))) LOOP


      BEGIN
         L_default_rec.CVB_CODE := rec.CVB_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'CVB_DETAIL',
                             NULL,
                            'CVB_CODE',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.COMP_ID := rec.COMP_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'CVB_DETAIL',
                             NULL,
                            'COMP_ID',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.COMBO_OPER := rec.COMBO_OPER_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'CVB_DETAIL',
                             NULL,
                            'COMBO_OPER',
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND
    into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(CVB_DETAIL$Action))    as ACTION,
                      UPPER(r.get_cell(CVB_DETAIL$CVB_CODE))  as CVB_CODE,
                      UPPER(r.get_cell(CVB_DETAIL$COMP_ID))   as COMP_ID,
                      r.get_cell(CVB_DETAIL$COMBO_OPER)       as COMBO_OPER,
                      r.get_row_seq()                         as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(CVB_DETAIL_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.ACTION := rec.ACTION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_DETAIL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
         END;
      BEGIN
         L_temp_rec.CVB_CODE := rec.CVB_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_DETAIL_sheet,
                            rec.row_seq,
                            'CVB_CODE',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COMP_ID := rec.COMP_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_DETAIL_sheet,
                            rec.row_seq,
                            'COMP_ID',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COMBO_OPER := rec.COMBO_OPER;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CVB_DETAIL_sheet,
                            rec.row_seq,
                            'COMBO_OPER',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      if rec.action = CORESVC_CVB.action_new then
         L_temp_rec.CVB_CODE   := NVL( L_temp_rec.CVB_CODE,
                                       L_default_rec.CVB_CODE);
         L_temp_rec.COMP_ID    := NVL( L_temp_rec.COMP_ID,
                                       L_default_rec.COMP_ID);
         L_temp_rec.COMBO_OPER := NVL( L_temp_rec.COMBO_OPER,
                                       L_default_rec.COMBO_OPER);
      end if;
      if NOT (L_temp_rec.comp_id is NOT NULL
          and L_temp_rec.cvb_code is NOT NULL and 1 = 1) then
         WRITE_S9T_ERROR( I_file_id,
                          CVB_DETAIL_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_CVB_DETAIL_col.EXTEND();
         svc_CVB_DETAIL_col(svc_CVB_DETAIL_col.COUNT()):=L_temp_rec;
      end if;
   end loop;
BEGIN
   FORALL i IN 1..svc_CVB_DETAIL_col.COUNT
   SAVE EXCEPTIONS merge into SVC_CVB_DETAIL st
   using
   (select (case
               when L_mi_rec.CVB_CODE_mi    = 'N'
               and svc_CVB_DETAIL_col(i).action = CORESVC_CVB.action_mod
               and s1.CVB_CODE            is NULL
               then mt.CVB_CODE
               else s1.cvb_code
               end) as CVB_CODE,
           (case
               when L_mi_rec.COMP_ID_mi    = 'N'
               and svc_CVB_DETAIL_col(i).action = CORESVC_CVB.action_mod
               and s1.COMP_ID             is NULL
               then mt.COMP_ID
               else s1.COMP_ID
               end) as COMP_ID,
           (case
               when L_mi_rec.COMBO_OPER_mi    = 'N'
               and svc_CVB_DETAIL_col(i).action = CORESVC_CVB.action_mod
               and s1.COMBO_OPER          is NULL
               then mt.COMBO_OPER
               else s1.COMBO_OPER
               end) as COMBO_OPER,
               NULL as dummy
      from (select svc_CVB_DETAIL_col(i).CVB_CODE   as CVB_CODE,
                   svc_CVB_DETAIL_col(i).COMP_ID    as COMP_ID,
                   svc_CVB_DETAIL_col(i).COMBO_OPER as COMBO_OPER,
                   NULL as dummy
              from dual) s1,
                   CVB_DETAIL mt
             where mt.COMP_ID (+)     = s1.COMP_ID
               and mt.CVB_CODE (+)     = s1.CVB_CODE
               and 1 = 1) sq ON (st.COMP_ID      = sq.COMP_ID   and
                                 st.CVB_CODE      = sq.CVB_CODE and
                                 svc_CVB_DETAIL_col(i).action IN( CORESVC_CVB.action_mod, CORESVC_CVB.action_del))
              when matched then update
                                   set PROCESS_ID        = svc_CVB_DETAIL_col(i).PROCESS_ID ,
                                       CHUNK_ID          = svc_CVB_DETAIL_col(i).CHUNK_ID ,
                                       ROW_SEQ           = svc_CVB_DETAIL_col(i).ROW_SEQ ,
                                       ACTION            = svc_CVB_DETAIL_col(i).ACTION ,
                                       PROCESS$STATUS    = svc_CVB_DETAIL_col(i).PROCESS$STATUS ,
                                       COMBO_OPER        = sq.COMBO_OPER ,
                                       CREATE_ID         = svc_CVB_DETAIL_col(i).CREATE_ID ,
                                       CREATE_DATETIME   = svc_CVB_DETAIL_col(i).CREATE_DATETIME ,
                                       LAST_UPD_ID       = svc_CVB_DETAIL_col(i).LAST_UPD_ID ,
                                       LAST_UPD_DATETIME = svc_CVB_DETAIL_col(i).LAST_UPD_DATETIME WHEN NOT matched then
                                insert (PROCESS_ID ,
                                        CHUNK_ID ,
                                        ROW_SEQ ,
                                        ACTION ,
                                        PROCESS$STATUS ,
                                        CVB_CODE ,
                                        COMP_ID ,
                                        COMBO_OPER ,
                                        CREATE_ID ,
                                        CREATE_DATETIME ,
                                        LAST_UPD_ID ,
                                        LAST_UPD_DATETIME)
                                values (svc_CVB_DETAIL_col(i).PROCESS_ID ,
                                        svc_CVB_DETAIL_col(i).CHUNK_ID ,
                                        svc_CVB_DETAIL_col(i).ROW_SEQ ,
                                        svc_CVB_DETAIL_col(i).ACTION ,
                                        svc_CVB_DETAIL_col(i).PROCESS$STATUS ,
                                        sq.CVB_CODE ,
                                        sq.COMP_ID ,
                                        sq.COMBO_OPER ,
                                        svc_CVB_DETAIL_col(i).CREATE_ID ,
                                        svc_CVB_DETAIL_col(i).CREATE_DATETIME ,
                                        svc_CVB_DETAIL_col(i).LAST_UPD_ID ,
                                        svc_CVB_DETAIL_col(i).LAST_UPD_DATETIME);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          CVB_DETAIL_sheet,
                          svc_CVB_DETAIL_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
      when OTHERS then
      if C_MANDATORY_IND%ISOPEN then
         close C_MANDATORY_IND;
      end if;
      ROLLBACK;
   END;
END PROCESS_S9T_CVB_DETAIL;

--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count   IN OUT NUMBER,
                     I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id    IN     NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64) :='CORESVC_CVB.PROCESS_S9T';
   L_file           S9T_FILE;
   L_sheets         S9T_PKG.NAMES_MAP_TYP;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR     EXCEPTION;
   PRAGMA       EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;--to ensure that the record in s9t_folder is commited
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(l_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_CVB_HEAD(I_file_id,
                           I_process_id);
      PROCESS_S9T_CVB_DETAIL(I_file_id,
                             I_process_id);
      PROCESS_S9T_CVB_HEAD_TL(I_file_id,
                             I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 THEN
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   commit;
   return TRUE;

EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
------------------------------------------------------------------------------------------
FUNCTION EXEC_CVB_HEAD_INS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cvb_head_temp_rec IN     CVB_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)   :='CORESVC_CVB.EXEC_CVB_HEAD_INS';
BEGIN
   insert into cvb_head
        values I_cvb_head_temp_rec;

   return TRUE;
EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('CVB_HEAD_EXISTS',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CVB_HEAD_INS;
-----------------------------------------------------------------------------------------
FUNCTION EXEC_CVB_DETAIL_INS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cvb_detail_temp_rec  IN     CVB_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)   :='CORESVC_CVB.EXEC_CVB_DETAIL_INS';
BEGIN
   insert into cvb_detail
        values I_cvb_detail_temp_rec;

   return TRUE;
EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('CVB_DETAIL_EXISTS',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CVB_DETAIL_INS;
-----------------------------------------------------------------------------------------
FUNCTION EXEC_CVB_DETAIL_UPD(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cvb_detail_temp_rec  IN     CVB_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      :='CORESVC_CVB.EXEC_CVB_DETAIL_UPD';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_CVB_DETAIL';

   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
   cursor C_CVB_DETAIL_LOCK is
      select 'X'
        from cvb_detail
       where cvb_code = I_cvb_detail_temp_rec.cvb_code
         for update nowait;
BEGIN
   open  C_CVB_DETAIL_LOCK;
   close C_CVB_DETAIL_LOCK;

   update cvb_detail
      set row      = I_cvb_detail_temp_rec
    where comp_id  = I_cvb_detail_temp_rec.comp_id
      and cvb_code = I_cvb_detail_temp_rec.cvb_code;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                L_table,
                                                                I_cvb_detail_temp_rec.comp_id,
                                                                I_cvb_detail_temp_rec.cvb_code);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_CVB_DETAIL_LOCK%ISOPEN   then
         close C_CVB_DETAIL_LOCK;
      end if;
      return FALSE;
END EXEC_CVB_DETAIL_UPD;

--------------------------------------------------------------------------------
FUNCTION EXEC_CVB_HEAD_PRE_DEL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cvb_head_temp_rec  IN     CVB_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      :='CORESVC_CVB.EXEC_CVB_HEAD_PRE_DEL';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_CVB_DETAIL';

   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
   cursor C_CVB_DETAIL_LOCK is
      select 'X'
        from cvb_detail
       where cvb_code = I_cvb_head_temp_rec.cvb_code
         for update nowait;
BEGIN
   open  C_CVB_DETAIL_LOCK;
   close C_CVB_DETAIL_LOCK;

   delete from cvb_detail
      where cvb_code = I_cvb_head_temp_rec.cvb_code;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                L_table,
                                                                I_cvb_head_temp_rec.cvb_code,
                                                                NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_CVB_DETAIL_LOCK%ISOPEN   then
         close C_CVB_DETAIL_LOCK;
      end if;
      return FALSE;
END EXEC_CVB_HEAD_PRE_DEL;
------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_CVB_HEAD_DEL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cvb_head_temp_rec  IN     CVB_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64)                      :='CORESVC_CVB.EXEC_CVB_HEAD_DEL';
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_CVB_HEAD';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_CVB_HEAD_LOCK is
      select 'X'
        from CVB_HEAD
       where cvb_code = I_cvb_head_temp_rec.cvb_code
         for update nowait;


   cursor C_CVB_HEAD_TL_LOCK is
      select 'X'
        from CVB_HEAD_TL
       where cvb_code = I_cvb_head_temp_rec.cvb_code
         for update nowait;
BEGIN
   open  C_CVB_HEAD_TL_LOCK;
   close C_CVB_HEAD_TL_LOCK;

   open  C_CVB_HEAD_LOCK;
   close C_CVB_HEAD_LOCK;

  --delete from cvb_detail where cvb_code = I_rec.cvb_code;
   if EXEC_CVB_HEAD_PRE_DEL(O_error_message,
                            I_cvb_head_temp_rec) = FALSE then
      return FALSE;
   end if;

   delete from cvb_head_tl
      where cvb_code = I_cvb_head_temp_rec.cvb_code;

   delete from cvb_head
      where cvb_code = I_cvb_head_temp_rec.cvb_code;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                L_table,
                                                                I_cvb_head_temp_rec.cvb_code,
                                                                NULL);
      return FALSE;

   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_CVB_HEAD_LOCK%ISOPEN   then
         close C_CVB_HEAD_LOCK;
      end if;

      if C_CVB_HEAD_TL_LOCK%ISOPEN   then
         close C_CVB_HEAD_TL_LOCK;
      end if;
      return FALSE;
END EXEC_CVB_HEAD_DEL;
-----------------------------------------------------------------------------------
FUNCTION EXEC_CVB_DETAIL_DEL(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cvb_detail_temp_rec  IN     CVB_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      :='CORESVC_CVB.EXEC_CVB_DETAIL_DEL';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_CVB_DETAIL';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
   cursor C_CVB_DETAIL_LOCK is
      select 'X'
        from cvb_detail
       where cvb_code = I_cvb_detail_temp_rec.cvb_code
         for update nowait;

BEGIN
   open  C_CVB_DETAIL_LOCK;
   close C_CVB_DETAIL_LOCK;

   delete from cvb_detail
      where comp_id  = I_cvb_detail_temp_rec.comp_id
        and cvb_code = I_cvb_detail_temp_rec.cvb_code;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                                L_table,
                                                                I_cvb_detail_temp_rec.comp_id,
                                                                I_cvb_detail_temp_rec.cvb_code);
      close C_CVB_DETAIL_LOCK;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_CVB_DETAIL_LOCK%ISOPEN   then
         close C_CVB_DETAIL_LOCK;
      end if;
      return FALSE;
END EXEC_CVB_DETAIL_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CVB_HEAD_TL_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cvb_head_tl_ins_tab      IN       CVB_HEAD_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_CVB.EXEC_CVB_HEAD_TL_INS';

BEGIN
   if I_cvb_head_tl_ins_tab is NOT NULL and I_cvb_head_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_cvb_head_tl_ins_tab.COUNT()
         insert into cvb_head_tl
              values I_cvb_head_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CVB_HEAD_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CVB_HEAD_TL_UPD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cvb_head_tl_upd_tab     IN       CVB_HEAD_TL_TAB,
                              I_cvb_head_tl_upd_rst     IN       ROW_SEQ_TAB,
                              I_process_id              IN       SVC_CVB_HEAD_TL.PROCESS_ID%TYPE,
                              I_chunk_id                IN       SVC_CVB_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_CVB.EXEC_CVB_HEAD_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CVB_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CVB_HEAD_TL_UPD(I_cvb_code    CVB_HEAD_TL.CVB_CODE%TYPE,
                                 I_lang        CVB_HEAD_TL.LANG%TYPE) is
      select 'x'
        from cvb_head_tl
       where cvb_code = I_cvb_code
         and lang = I_lang
         for update nowait;

BEGIN
   if I_cvb_head_tl_upd_tab is NOT NULL and I_cvb_head_tl_upd_tab.count > 0 then
      for i in I_cvb_head_tl_upd_tab.FIRST..I_cvb_head_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_cvb_head_tl_upd_tab(i).lang);
            L_key_val2 := 'CVB Code: '||to_char(I_cvb_head_tl_upd_tab(i).cvb_code);
            open C_LOCK_CVB_HEAD_TL_UPD(I_cvb_head_tl_upd_tab(i).cvb_code,
                                        I_cvb_head_tl_upd_tab(i).lang);
            close C_LOCK_CVB_HEAD_TL_UPD;
            
            update cvb_head_tl
               set cvb_desc = I_cvb_head_tl_upd_tab(i).cvb_desc,
                   last_update_id = I_cvb_head_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_cvb_head_tl_upd_tab(i).last_update_datetime
             where lang = I_cvb_head_tl_upd_tab(i).lang
               and cvb_code = I_cvb_head_tl_upd_tab(i).cvb_code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_CVB_HEAD_TL',
                           I_cvb_head_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CVB_HEAD_TL_UPD%ISOPEN then
         close C_LOCK_CVB_HEAD_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CVB_HEAD_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CVB_HEAD_TL_DEL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cvb_head_tl_del_tab   IN       CVB_HEAD_TL_TAB,
                              I_cvb_head_tl_del_rst   IN       ROW_SEQ_TAB,
                              I_process_id            IN       SVC_CVB_HEAD_TL.PROCESS_ID%TYPE,
                              I_chunk_id              IN       SVC_CVB_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_CVB.EXEC_CVB_HEAD_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CVB_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CVB_HEAD_TL_DEL(I_cvb_code  CVB_HEAD_TL.CVB_CODE%TYPE,
                                 I_lang      CVB_HEAD_TL.LANG%TYPE) is
      select 'x'
        from cvb_head_tl
       where cvb_code = I_cvb_code
         and lang = I_lang
         for update nowait;

BEGIN
   if I_cvb_head_tl_del_tab is NOT NULL and I_cvb_head_tl_del_tab.count > 0 then
      for i in I_cvb_head_tl_del_tab.FIRST..I_cvb_head_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_cvb_head_tl_del_tab(i).lang);
            L_key_val2 := 'CVB Code: '||to_char(I_cvb_head_tl_del_tab(i).cvb_code);
            open C_LOCK_CVB_HEAD_TL_DEL(I_cvb_head_tl_del_tab(i).cvb_code,
                                        I_cvb_head_tl_del_tab(i).lang);
            close C_LOCK_CVB_HEAD_TL_DEL;
            
            delete cvb_head_tl
             where lang = I_cvb_head_tl_del_tab(i).lang
               and cvb_code = I_cvb_head_tl_del_tab(i).cvb_code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_CVB_HEAD_TL',
                           I_cvb_head_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CVB_HEAD_TL_DEL%ISOPEN then
         close C_LOCK_CVB_HEAD_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CVB_HEAD_TL_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_CVB_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error         IN OUT BOOLEAN,
                              I_rec           IN     C_SVC_CVB_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_table    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CVB_HEAD';
   L_exists   BOOLEAN;
BEGIN
   if I_rec.action = action_del then
      if ELC_SQL.CHECK_DELETE_CVB(O_error_message,
                                  L_exists,
                                  I_rec.cvb_code) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;
      if L_exists = TRUE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_table,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_CVB_HEAD;
--------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_CVB_DETAIL(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error           IN OUT BOOLEAN,
                                I_rec             IN     C_SVC_CVB_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_table  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CVB_DETAIL';
   L_exists BOOLEAN;
BEGIN
   if I_rec.action = action_new then
      if ELC_SQL.VALIDATE_CVB_COMP(O_error_message,
                                   L_exists,
                                   I_rec.cvb_code,
                                   I_rec.comp_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Computation Value Base,Component',
                     O_error_message);
         O_error := TRUE;
      end if;
      if L_exists = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Computation Value Base,Component',
                     O_error_message);
         O_error := TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_table,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_CVB_DETAIL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CVB_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id    IN     SVC_CVB_HEAD.PROCESS_ID%TYPE,
                          I_chunk_id      IN     SVC_CVB_HEAD.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program           VARCHAR2(64)                      :='CORESVC_CVB.PROCESS_CVB_HEAD';
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_CVB_HEAD';
   L_process_error     BOOLEAN                           := FALSE;
   L_error             BOOLEAN;
   L_cvb_head_temp_rec CVB_HEAD%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_CVB_HEAD(I_process_id,I_chunk_id)
   LOOP

      L_error         := FALSE;
      L_process_error := FALSE;
      SAVEPOINT successful_cvb;

      if rec.action is NOT NULL
         and rec.action NOT IN ( action_new,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;


      if rec.action = action_new
         and rec.pk_cvb_head_rid is NOT NULL
         and rec.cvb_code is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CVB_CODE',
                     'CVB_EXISTS');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_del)
         and rec.pk_cvb_head_rid is NULL
         and rec.cvb_code is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CVB_CODE',
                     'INV_CVB');
         L_error :=TRUE;
      end if;

      if rec.action = action_new then
         if rec.nom_flag_5 is NULL
            or NOT( rec.nom_flag_5 IN  ( 'Y','N' )  ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NOM_FLAG_5',
                        'INV_Y_N_IND');
            L_error :=TRUE;
         end if;

         if rec.nom_flag_4 is NULL
            or NOT( rec.nom_flag_4 IN  ( 'Y','N' )  ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NOM_FLAG_4',
                        'INV_Y_N_IND');
            L_error :=TRUE;
         end if;

         if rec.nom_flag_3 is NULL
            or NOT( rec.nom_flag_3 IN  ( 'Y','N' )  ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NOM_FLAG_3',
                        'INV_Y_N_IND');
            L_error :=TRUE;
         end if;

         if rec.nom_flag_2 is NULL
            or NOT( rec.nom_flag_2 IN  ( 'Y','N' )  ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NOM_FLAG_2',
                        'INV_Y_N_IND');
            L_error :=TRUE;
         end if;

         if rec.nom_flag_1 is NULL
            or NOT( rec.nom_flag_1 IN  ( 'Y','N' )  ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NOM_FLAG_1',
                        'INV_Y_N_IND');
            L_error :=TRUE;
         end if;

         if rec.cvb_desc is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CVB_DESC',
                        'ENTER_CVB_DESC');
            L_error :=TRUE;
         end if;
      end if;

      -- Other validations
      if PROCESS_VAL_CVB_HEAD(O_error_message,
                              L_error,
                              rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_cvb_head_temp_rec.cvb_code      := rec.cvb_code;
         L_cvb_head_temp_rec.cvb_desc      := rec.cvb_desc;
         L_cvb_head_temp_rec.nom_flag_1    := rec.nom_flag_1;
         L_cvb_head_temp_rec.nom_flag_2    := rec.nom_flag_2;
         L_cvb_head_temp_rec.nom_flag_3    := rec.nom_flag_3;
         L_cvb_head_temp_rec.nom_flag_4    := rec.nom_flag_4;
         L_cvb_head_temp_rec.nom_flag_5    := rec.nom_flag_5;

         if rec.action = action_new then
            if EXEC_CVB_HEAD_INS(O_error_message,
                                 L_cvb_head_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_CVB_HEAD_DEL(O_error_message,
                                 L_cvb_head_temp_rec) = FALSE then
               ROLLBACK TO successful_cvb;
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;

   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CVB_HEAD;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CVB_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_CVB_HEAD_TL.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_CVB_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                   VARCHAR2(64) := 'CORESVC_CVB.PROCESS_CVB_HEAD_TL';
   L_table                     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CVB_HEAD_TL';
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CVB_HEAD_TL';
   L_error                     BOOLEAN := FALSE;
   L_process_error             BOOLEAN := FALSE;
   L_cvb_head_tl_temp_rec      CVB_HEAD_TL%ROWTYPE;
   L_cvb_head_tl_upd_rst       ROW_SEQ_TAB;
   L_cvb_head_tl_del_rst       ROW_SEQ_TAB;

   cursor C_SVC_CVB_HEAD_TL(I_process_id NUMBER,
                            I_chunk_id   NUMBER) is
      select pk_cvb_head_tl.rowid  as pk_cvb_head_tl_rid,
             fk_cvb_head.rowid     as fk_cvb_head_rid,
             fk_lang.rowid         as fk_lang_rid,
             st.lang,
             st.cvb_code,
             st.cvb_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             upper(st.action)        as action,
             st.process$status
        from svc_cvb_head_tl  st,
             cvb_head         fk_cvb_head,
             cvb_head_tl      pk_cvb_head_tl,
             lang             fk_lang
       where st.process_id       =  I_process_id
         and st.chunk_id         =  I_chunk_id
         and st.cvb_code         =  fk_cvb_head.cvb_code (+)
         and st.lang             =  pk_cvb_head_tl.lang (+)
         and st.cvb_code         =  pk_cvb_head_tl.cvb_code (+)
         and st.lang             =  fk_lang.lang (+);

   TYPE SVC_CVB_HEAD_TL is TABLE OF C_SVC_CVB_HEAD_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_cvb_head_tl_tab        SVC_CVB_HEAD_TL;

   L_cvb_head_tl_ins_tab         CVB_HEAD_TL_TAB         := NEW CVB_HEAD_TL_TAB();
   L_cvb_head_tl_upd_tab         CVB_HEAD_TL_TAB         := NEW CVB_HEAD_TL_TAB();
   L_cvb_head_tl_del_tab         CVB_HEAD_TL_TAB         := NEW CVB_HEAD_TL_TAB();

BEGIN
   if C_SVC_CVB_HEAD_TL%ISOPEN then
      close C_SVC_CVB_HEAD_TL;
   end if;

   open C_SVC_CVB_HEAD_TL(I_process_id,
                          I_chunk_id);
   LOOP
      fetch C_SVC_CVB_HEAD_TL bulk collect into L_svc_cvb_head_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_cvb_head_tl_tab.COUNT > 0 then
         FOR i in L_svc_cvb_head_tl_tab.FIRST..L_svc_cvb_head_tl_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_cvb_head_tl_tab(i).action is NULL
               or L_svc_cvb_head_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cvb_head_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_cvb_head_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cvb_head_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_cvb_head_tl_tab(i).action = action_new
               and L_svc_cvb_head_tl_tab(i).pk_cvb_head_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cvb_head_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_cvb_head_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_cvb_head_tl_tab(i).lang is NOT NULL
               and L_svc_cvb_head_tl_tab(i).cvb_code is NOT NULL
               and L_svc_cvb_head_tl_tab(i).pk_cvb_head_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_cvb_head_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_cvb_head_tl_tab(i).action = action_new
               and L_svc_cvb_head_tl_tab(i).cvb_code is NOT NULL
               and L_svc_cvb_head_tl_tab(i).fk_cvb_head_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_cvb_head_tl_tab(i).row_seq,
                            'CVB_CODE',
                            'CVB_NOT_EXIST');
               L_error :=TRUE;
            end if;

            if L_svc_cvb_head_tl_tab(i).action = action_new
               and L_svc_cvb_head_tl_tab(i).lang is NOT NULL
               and L_svc_cvb_head_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cvb_head_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_cvb_head_tl_tab(i).cvb_code is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cvb_head_tl_tab(i).row_seq,
                           'CVB_CODE',
                           'ENTER_CVB');
               L_error :=TRUE;
            end if;

            if L_svc_cvb_head_tl_tab(i).action in (action_new, action_mod) and L_svc_cvb_head_tl_tab(i).cvb_desc is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cvb_head_tl_tab(i).row_seq,
                           'CVB_DESC',
                           'ENTER_CVB_DESC');
               L_error :=TRUE;
            end if;

            if L_svc_cvb_head_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cvb_head_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_cvb_head_tl_temp_rec.lang := L_svc_cvb_head_tl_tab(i).lang;
               L_cvb_head_tl_temp_rec.cvb_code := L_svc_cvb_head_tl_tab(i).cvb_code;
               L_cvb_head_tl_temp_rec.cvb_desc := L_svc_cvb_head_tl_tab(i).cvb_desc;
               L_cvb_head_tl_temp_rec.create_datetime := SYSDATE;
               L_cvb_head_tl_temp_rec.create_id := GET_USER;
               L_cvb_head_tl_temp_rec.last_update_datetime := SYSDATE;
               L_cvb_head_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_cvb_head_tl_tab(i).action = action_new then
                  L_cvb_head_tl_ins_tab.extend;
                  L_cvb_head_tl_ins_tab(L_cvb_head_tl_ins_tab.count()) := L_cvb_head_tl_temp_rec;
               end if;

               if L_svc_cvb_head_tl_tab(i).action = action_mod then
                  L_cvb_head_tl_upd_tab.extend;
                  L_cvb_head_tl_upd_tab(L_cvb_head_tl_upd_tab.count()) := L_cvb_head_tl_temp_rec;
                  L_cvb_head_tl_upd_rst(L_cvb_head_tl_upd_tab.count()) := L_svc_cvb_head_tl_tab(i).row_seq;
               end if;

               if L_svc_cvb_head_tl_tab(i).action = action_del then
                  L_cvb_head_tl_del_tab.extend;
                  L_cvb_head_tl_del_tab(L_cvb_head_tl_del_tab.count()) := L_cvb_head_tl_temp_rec;
                  L_cvb_head_tl_del_rst(L_cvb_head_tl_del_tab.count()) := L_svc_cvb_head_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_CVB_HEAD_TL%NOTFOUND;
   END LOOP;
   close C_SVC_CVB_HEAD_TL;

   if EXEC_CVB_HEAD_TL_INS(O_error_message,
                           L_cvb_head_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_CVB_HEAD_TL_UPD(O_error_message,
                           L_cvb_head_tl_upd_tab,
                           L_cvb_head_tl_upd_rst,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_CVB_HEAD_TL_DEL(O_error_message,
                           L_cvb_head_tl_del_tab,
                           L_cvb_head_tl_del_rst,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CVB_HEAD_TL;
-------------------------------------------------------------------------------------

FUNCTION CHECK_FLAG(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error         IN OUT BOOLEAN,
                    O_flag          IN OUT VARCHAR2,
                    I_cvb_code      IN     CVB_DETAIL.CVB_CODE%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR(64) := 'CORESVC_CVB.CHECK_FLAG';
   cursor C_CVB_DETAIL is
      select 'Y'
        from cvb_detail
       where cvb_code = I_cvb_code;
BEGIN
   open C_CVB_DETAIL;
   fetch C_CVB_DETAIL into O_flag;
   close C_CVB_DETAIL;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_CVB_DETAIL%ISOPEN then
         close C_CVB_DETAIL;
      end if;
      ROLLBACK;
      return FALSE;
END CHECK_FLAG;
----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CVB_DETAIL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id    IN     SVC_CVB_DETAIL.PROCESS_ID%TYPE,
                            I_chunk_id      IN     SVC_CVB_DETAIL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                    VARCHAR2(64)                      :='CORESVC_CVB.PROCESS_CVB_DETAIL';
   L_table                      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_CVB_DETAIL';
   L_flag                       VARCHAR2(1)                       :='N';
   L_error                      BOOLEAN;
   L_process_error              BOOLEAN;
   L_process_error_with_warning BOOLEAN;
   L_cvb_detail_temp_rec        CVB_DETAIL%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_CVB_DETAIL(I_process_id,I_chunk_id)
   LOOP
      L_error                      := FALSE;
      L_process_error              := FALSE;
      L_process_error_with_warning := FALSE;

      if rec.cvb_code_rank = 1 then
         L_flag :='N';
         if CHECK_FLAG(O_error_message,
                       L_error,
                       L_flag,
                       rec.cvb_code) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_error :=TRUE;
         end if;
      end if;

      if rec.action is NOT NULL
         and rec.action NOT IN (action_new,action_del)   then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.pk_cvb_detail_rid is NOT NULL
         and rec.cvb_code is NOT NULL
         and rec.comp_id is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Computation Value Base,Component',
                     'DUP_COMP');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_del)
         and rec.cvb_code is NOT NULL
         and rec.comp_id is NOT NULL
         and rec.pk_cvb_detail_rid IS NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Computation Value Base,Component',
                     'CVD_MISSING');
         L_error :=TRUE;
      end if;

      if rec.cvd_cvb_fk_rid is NULL
         and rec.cvb_code is NOT NULL
         and rec.action IN ( action_new )then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CVB_CODE',
                     'INV_CVB');
         L_error :=TRUE;
      end if;

      if rec.cvd_elc_fk_rid is NULL
         and rec.comp_id is NOT NULL
         and rec.action IN (action_new) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COMP_ID',
                     'CVD_ELC_INV');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_new ) then
         if rec.combo_oper is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COMBO_OPER',
                        'NO_COMBO_OPER');
            L_error :=TRUE;
         end if;
      end if;

      if rec.action IN (action_new ) then
         if rec.combo_oper NOT IN ('+','-')
            and rec.combo_oper is NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                       'COMBO_OPER',
                       'CVD_CHK_COMBO_OPER');
            L_error :=TRUE;
         end if;
      end if;

      --Other validations
      if PROCESS_VAL_CVB_DETAIL(O_error_message,
                                L_error,
                                rec)=FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_cvb_detail_temp_rec.cvb_code    := rec.cvb_code;
         L_cvb_detail_temp_rec.comp_id     := rec.comp_id;
         L_cvb_detail_temp_rec.combo_oper  := rec.combo_oper;

         if rec.action = action_new then
            if EXEC_CVB_DETAIL_INS(O_error_message,
                                   L_cvb_detail_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_CVB_DETAIL_DEL(O_error_message,
                                   L_cvb_detail_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if NOT L_process_error then
            if (rec.action = action_new and L_flag = 'Y')
                or rec.action IN (action_del) then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'Computation Value Base,Component',
                              'REQ_CALC',
                              'W');
            end if;
         end if;
      end if;


   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CVB_DETAIL;
----------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete
     from svc_cvb_head_tl
    where process_id = i_process_id;

   delete
     from svc_cvb_head
    where process_id = i_process_id;

   delete
     from svc_cvb_detail
    where process_id = i_process_id;
END;
----------------------------------------------------------------------------------------------------

FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count   IN OUT NUMBER,
                 I_process_id    IN     NUMBER,
                 I_chunk_id      IN     NUMBER)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)                    :='CORESVC_CVB.PROCESS';
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   Lp_errors_tab := NEW errors_tab_typ();
   if PROCESS_CVB_HEAD(O_error_message,
                       I_process_id,
                       I_chunk_id)= FALSE then
      return FALSE;
   end if;

   if PROCESS_CVB_DETAIL(O_error_message,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_CVB_HEAD_TL(O_error_message,
                          I_process_id,
                          I_chunk_id)= FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;


   update svc_process_tracker
      set status = (CASE
                      when status = 'PE'
                          then 'PE'
                    else L_process_status
                    END),
              action_date = SYSDATE
        where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
------------------------------------------------------------------------------
END CORESVC_CVB;
/