create or replace PACKAGE BODY ORDER_SETUP_SQL AS
-------------------------------------------------------------------------------
FUNCTION LOCK_ORDHEAD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ORDHEAD is
      select 'x'
        from ordhead_lock
       where order_no = I_order_no
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDHEAD','ORDHEAD_LOCK', 'order_no: '||to_char(I_order_no));
   open C_LOCK_ORDHEAD;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDHEAD','ORDHEAD_LOCK', 'order_no: '||to_char(I_order_no));
   close C_LOCK_ORDHEAD;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ORDER_LOCKED',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.LOCK_ORDHEAD',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_ORDHEAD;
-------------------------------------------------------------------------------
FUNCTION APPLY_ITEM_LIST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_ordsku_exists    IN OUT   VARCHAR2,
                         O_wksht_exists     IN OUT   VARCHAR2,
                         I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                         I_skulist          IN       SKULIST_HEAD.SKULIST%TYPE,
                         I_supplier         IN       ORDHEAD.SUPPLIER%TYPE,
                         I_dept             IN       ORDHEAD.DEPT%TYPE,
                         I_wksht_qty        IN       ORDLOC_WKSHT.ACT_QTY%TYPE,
                         I_loc              IN       ORDLOC_WKSHT.LOCATION%TYPE,
                         I_loc_type         IN       ORDLOC_WKSHT.LOC_TYPE%TYPE,
                         I_origin_country   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                         I_uop_type         IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'ORDER_SETUP_SQL.APPLY_ITEM_LIST';

   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_item_parent          ITEM_MASTER.ITEM_PARENT%TYPE;
   L_diff1                ITEM_MASTER.DIFF_1%TYPE;
   L_diff2                ITEM_MASTER.DIFF_2%TYPE;
   L_diff3                ITEM_MASTER.DIFF_3%TYPE;
   L_diff4                ITEM_MASTER.DIFF_4%TYPE;
   L_uop                  ORDLOC_WKSHT.UOP%TYPE;
   L_origin_country       ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_supp_pack_size       ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;
   L_multiple             ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;
   L_multiple2            ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;
   L_standard_uom         ORDLOC_WKSHT.STANDARD_UOM%TYPE;
   L_dummy                VARCHAR2(1);
   L_insert               VARCHAR2(1);
   L_today_date           ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_order_type           ORDHEAD.ORDER_TYPE%TYPE;
   L_customer_order_ind   VARCHAR2(1) := 'N';
   L_stockholding_ind     STORE.STOCKHOLDING_IND%TYPE;
   L_virtual_store        VARCHAR2(1) := 'N';

   /* The C_SKULIST cursor selects all SKUS in itemlist I_skulist supplied                 */
   /* by I_supplier and I_origin_country that don't already exist on table ordloc_wksht.   */
   /* If I_dept is passed, it will select only SKUs that exist in that department          */
   /* If the item is a pack, will only continue if packhead.orderable_ind = 'Y'            */
   /* If I_wksht_qty is passed, it will insert that qty as well as calculating the         */
   /* actual qty and insert that into ordloc_wksht for each item in the itemlist.          */

   cursor C_SKULIST is
      select /*+ ORDERED */ distinct im.item,
             im.item_parent,
             isc.supp_pack_size,
             d1.diff_id diff_1,
             d2.diff_id diff_2,
             d3.diff_id diff_3,
             d4.diff_id diff_4,
             im.standard_uom,
             CASE
                WHEN I_uop_type = 'P' OR (I_uop_type = 'D' AND isc.default_uop = 'P') THEN
                   (isc.ti * isc.hi * isc.supp_pack_size)
                WHEN I_uop_type = 'C' OR (I_uop_type = 'D' AND isc.default_uop = 'C') THEN
                   isc.supp_pack_size
                ELSE
                   1
             END multiple,
             CASE
                WHEN I_uop_type = 'P' OR (I_uop_type = 'D' AND isc.default_uop = 'P') THEN
                   ((isc.ti * isc.hi * isc.supp_pack_size) / isc.supp_pack_size)
                ELSE
                   1
             END multiple2,
             CASE
                WHEN I_uop_type = 'S' OR (I_uop_type = 'D' AND isc.default_uop NOT IN ('P','C'))  THEN
                   im.standard_uom
                ELSE
                   its.case_name
             END uop,
             isc.origin_country_id
        from skulist_detail sd,
             item_master im,
             item_supp_country isc,
             item_supplier its,
             diff_ids d1,
             diff_ids d2,
             diff_ids d3,
             diff_ids d4
       where sd.skulist = I_skulist
         and sd.item = im.item
         and (im.dept = I_dept
              or I_dept is NULL)
         and sd.item = isc.item
         and im.diff_1 = d1.diff_id (+)
         and im.diff_2 = d2.diff_id (+)
         and im.diff_3 = d3.diff_id (+)
         and im.diff_4 = d4.diff_id (+)
         and isc.supplier = I_supplier
         and isc.supplier = its.supplier
         and isc.item = its.item
         and (isc.origin_country_id = I_origin_country
          or (I_origin_country is NULL and isc.primary_country_ind = 'Y'))
         and NOT EXISTS (select 'x'
                           from ordsku
                          where order_no = I_order_no
                            and item     =  sd.item)
         and im.status = 'A'
         and sd.item_level = sd.tran_level
         and NVL(im.deposit_item_type, 'X') != 'A'
         and im.orderable_ind = 'Y'
         and im.inventory_ind = 'Y'
         and (L_customer_order_ind = 'N'
          or (L_customer_order_ind = 'Y' and im.sellable_ind = 'Y' and im.catch_weight_ind = 'N' and
              NVL(im.deposit_item_type, 'X') != 'Z' and  EXISTS (select sd.item
                                                                   from item_loc il,
                                                                        ordhead oh
                                                                  where il.item(+) = sd.item
                                                                    and il.loc(+) = oh.location
                                                                    and oh.location = I_loc
                                                                    and (il.status = 'A' or il.status is NULL))))
         and (L_virtual_store = 'N'
          or (L_virtual_store = 'Y' and its.direct_ship_ind = 'Y'));

   cursor C_SKULIST_PARENT is
      select /*+ ORDERED */ distinct im.item,
             d1.diff_id diff_1,
             d2.diff_id diff_2,
             d3.diff_id diff_3,
             d4.diff_id diff_4,
             isc.supp_pack_size,
             im.standard_uom,
             CASE
                WHEN I_uop_type = 'P' OR (I_uop_type = 'D' AND isc.default_uop = 'P') THEN
                   (isc.ti * isc.hi * isc.supp_pack_size)
                WHEN I_uop_type = 'C' OR (I_uop_type = 'D' AND isc.default_uop = 'C') THEN
                   isc.supp_pack_size
                ELSE
                   1
             END multiple,
             CASE
                WHEN I_uop_type = 'P' OR (I_uop_type = 'D' AND isc.default_uop = 'P') THEN
                   ((isc.ti * isc.hi * isc.supp_pack_size) / isc.supp_pack_size)
                ELSE
                   1
             END multiple2,
             CASE
                WHEN I_uop_type = 'S' OR (I_uop_type = 'D' AND isc.default_uop NOT IN ('P','C'))  THEN
                   im.standard_uom
                ELSE
                   its.case_name
             END uop,
             isc.origin_country_id
        from skulist_detail sd,
             item_master im,
             item_supp_country isc,
             item_supplier its,
             diff_ids d1,
             diff_ids d2,
             diff_ids d3,
             diff_ids d4,
             v_diff_id_group_type vdigt
       where sd.skulist = I_skulist
         and sd.item = im.item
         and (im.dept = I_dept
              or I_dept is NULL)
         and sd.item = isc.item
         and im.item = its.item
         and im.diff_1 = d1.diff_id(+)
         and im.diff_2 = d2.diff_id(+)
         and im.diff_3 = d3.diff_id(+)
         and im.diff_4 = d4.diff_id(+)
         and ((im.diff_1 is not NULL
               and (im.diff_1 = vdigt.id_group
                    and vdigt.id_group_ind = 'GROUP'))
              or (im.diff_2 is not NULL
                  and (im.diff_2 = vdigt.id_group
                       and vdigt.id_group_ind = 'GROUP'))
              or (im.diff_3 is not NULL
                  and (im.diff_3 = vdigt.id_group
                       and vdigt.id_group_ind = 'GROUP'))
              or (im.diff_4 is not NULL
                  and (im.diff_4 = vdigt.id_group
                       and vdigt.id_group_ind = 'GROUP')))
         and isc.supplier = I_supplier
         and its.supplier = isc.supplier
         and (isc.origin_country_id = I_origin_country
          or (I_origin_country is NULL and isc.primary_country_ind = 'Y'))
         and NOT EXISTS (select 'x'
                           from ordsku
                          where order_no = I_order_no
                            and item     =  sd.item)
         and im.status = 'A'
         and sd.item_level + 1 = sd.tran_level
         and NVL(im.deposit_item_type, 'X') != 'A'
         and im.orderable_ind = 'Y'
         and im.inventory_ind = 'Y'
         and (L_customer_order_ind = 'N'
          or (L_customer_order_ind = 'Y' and im.sellable_ind = 'Y' and im.catch_weight_ind = 'N' and
              NVL(im.deposit_item_type, 'X') != 'Z' and  EXISTS (select sd.item
                                                                   from item_loc il,
                                                                        ordhead oh
                                                                  where il.item(+) = sd.item
                                                                    and il.loc(+) = oh.location
                                                                    and oh.location = I_loc
                                                                    and (il.status = 'A' or il.status is NULL))))
         and (L_virtual_store = 'N'
          or (L_virtual_store = 'Y' and its.direct_ship_ind = 'Y'));

   cursor C_SKULIST_ORDSKU is
      select distinct im.item,
             im.item_parent,
             os.supp_pack_size,
             d1.diff_id diff_1,
             d2.diff_id diff_2,
             d3.diff_id diff_3,
             d4.diff_id diff_4,
             CASE
                WHEN I_uop_type = 'P' OR (I_uop_type = 'D' AND isc.default_uop = 'P') THEN
                   (isc.ti * isc.hi * isc.supp_pack_size)
                WHEN I_uop_type = 'C' OR (I_uop_type = 'D' AND isc.default_uop = 'C') THEN
                   os.supp_pack_size
                ELSE
                   1
             END multiple,
             CASE
                WHEN I_uop_type = 'P' OR (I_uop_type = 'D' AND isc.default_uop = 'P') THEN
                   ((isc.ti * isc.hi * isc.supp_pack_size) / os.supp_pack_size)
                ELSE
                   1
             END multiple2,
             im.standard_uom,
             CASE
                WHEN I_uop_type = 'S' OR (I_uop_type = 'D' AND isc.default_uop NOT IN ('P','C'))  THEN
                   im.standard_uom
                ELSE
                   its.case_name
                END uop,
             os.origin_country_id
        from skulist_detail sd,
             item_master im,
             ordsku os,
             item_supp_country isc,
             item_supplier its,
             diff_ids d1,
             diff_ids d2,
             diff_ids d3,
             diff_ids d4
       where sd.skulist  = I_skulist
         and os.order_no = I_order_no
         and sd.item = os.item
         and sd.item = its.item
         and sd.item = im.item
         and isc.supplier = I_supplier
         and its.supplier = isc.supplier
         and im.diff_1 = d1.diff_id (+)
         and im.diff_2 = d2.diff_id (+)
         and im.diff_3 = d3.diff_id (+)
         and im.diff_4 = d4.diff_id (+)
         and os.origin_country_id   = isc.origin_country_id
         and isc.item  = sd.item
         and im.status = 'A'
         and sd.item_level = sd.tran_level
         and NVL(im.deposit_item_type, 'X') != 'A'
         and im.orderable_ind = 'Y'
         and im.inventory_ind = 'Y'
         and (L_customer_order_ind = 'N'
          or (L_customer_order_ind = 'Y' and im.sellable_ind = 'Y' and im.catch_weight_ind = 'N' and
              NVL(im.deposit_item_type, 'X') != 'Z' and  EXISTS (select sd.item
                                                                   from item_loc il,
                                                                        ordhead oh
                                                                  where il.item(+) = sd.item
                                                                    and il.loc(+) = oh.location
                                                                    and oh.location = I_loc
                                                                    and (il.status = 'A' or il.status is NULL))))
         and (L_virtual_store = 'N'
          or (L_virtual_store = 'Y' and its.direct_ship_ind = 'Y'));

   cursor C_GET_ORDER_TYPE is
      select order_type
        from ordhead
       where order_no = I_order_no;

   cursor C_CHECK_STOCKHOLDING_STORE is
      select stockholding_ind
        from store
       where store_type = 'C'
         and store = I_loc;

BEGIN
   O_wksht_exists := 'N';
   O_ordsku_exists := 'N';
   L_today_date := GET_VDATE;

   -- Retrieve Order Type of the Order.
   -- This will be used in retrieving valid items for customer orders.

   open C_GET_ORDER_TYPE;
   fetch C_GET_ORDER_TYPE into L_order_type;
   close C_GET_ORDER_TYPE;

   if L_order_type = 'CO' then

      L_customer_order_ind := 'Y';

      -- Need to check if the Customer Order is a Drop Ship Order.
      -- Drop Ship Orders will have Virtual Stores - non_stockholding_ind = 'N'.

      open C_CHECK_STOCKHOLDING_STORE;
      fetch C_CHECK_STOCKHOLDING_STORE into L_stockholding_ind;
      close C_CHECK_STOCKHOLDING_STORE;

      if L_stockholding_ind = 'N' then
         L_virtual_store := 'Y';
      else
         L_virtual_store := 'N';
      end if;
   else
      L_customer_order_ind := 'N';
      L_virtual_store      := 'N';
   end if;

   --Insert all valid items from item list that do not already exist on ordsku.
   FOR rec in C_SKULIST LOOP
      L_item              := rec.item;
      L_item_parent       := rec.item_parent;
      L_supp_pack_size    := rec.supp_pack_size;
      L_diff1             := rec.diff_1;
      L_diff2             := rec.diff_2;
      L_diff3             := rec.diff_3;
      L_diff4             := rec.diff_4;
      L_multiple          := rec.multiple;
      L_multiple2         := rec.multiple2;
      L_standard_uom      := rec.standard_uom;
      L_uop               := rec.uop;
      L_origin_country    := rec.origin_country_id;

      ---
      if NOT ORDER_SETUP_SQL.ORDLOC_WKS_INSERT(O_error_message,
                                               O_wksht_exists,
                                               I_order_no,
                                               L_item,
                                               L_item_parent,
                                               L_diff1,
                                               L_diff2,
                                               L_diff3,
                                               L_diff4,
                                               I_loc_type,
                                               I_loc,
                                               I_wksht_qty * L_multiple,
                                               L_origin_country,
                                               L_standard_uom,
                                               I_wksht_qty * L_multiple2,
                                               L_uop,
                                               L_supp_pack_size) then
         return FALSE;
      end if;
      ---
   END LOOP;

   --Insert all valid items that are above the tran level from item list
   --that do not already exist on ordsku.
   FOR rec in C_SKULIST_PARENT LOOP
      L_item              := rec.item;
      L_diff1             := rec.diff_1;
      L_diff2             := rec.diff_2;
      L_diff3             := rec.diff_3;
      L_diff4             := rec.diff_4;
      L_supp_pack_size    := rec.supp_pack_size;
      L_multiple          := rec.multiple;
      L_multiple2         := rec.multiple2;
      L_standard_uom      := rec.standard_uom;
      L_uop               := rec.uop;
      L_origin_country    := rec.origin_country_id;
      ---
      if NOT ORDER_SETUP_SQL.ORDLOC_WKS_INSERT(O_error_message,
                                               O_wksht_exists,
                                               I_order_no,
                                               NULL,
                                               L_item,
                                               L_diff1,
                                               L_diff2,
                                               L_diff3,
                                               L_diff4,
                                               I_loc_type,
                                               I_loc,
                                               I_wksht_qty * L_multiple,
                                               L_origin_country,
                                               L_standard_uom,
                                               I_wksht_qty * L_multiple2,
                                               L_uop,
                                               L_supp_pack_size) then
         return FALSE;
      end if;
      ---
   END LOOP;

   --
   ---Insert all valid items from item list that do already exist on ordsku using
   ---the country of origin(s) on ordsku.
   --
   O_ordsku_exists := 'N';

   FOR rec in C_SKULIST_ORDSKU LOOP
      L_item              := rec.item;
      L_item_parent       := rec.item_parent;
      L_supp_pack_size    := rec.supp_pack_size;
      L_diff1             := rec.diff_1;
      L_diff2             := rec.diff_2;
      L_diff3             := rec.diff_3;
      L_diff4             := rec.diff_4;
      L_multiple          := rec.multiple;
      L_multiple2         := rec.multiple2;
      L_standard_uom      := rec.standard_uom;
      L_uop               := rec.uop;
      L_origin_country    := rec.origin_country_id;

      if L_item is NOT NULL then
         O_ordsku_exists := 'Y';
      end if;
      ---
      if NOT ORDER_SETUP_SQL.ORDLOC_WKS_INSERT(O_error_message,
                                               O_wksht_exists,
                                               I_order_no,
                                               L_item,
                                               L_item_parent,
                                               L_diff1,
                                               L_diff2,
                                               L_diff3,
                                               L_diff4,
                                               I_loc_type,
                                               I_loc,
                                               I_wksht_qty * L_multiple,
                                               L_origin_country,
                                               L_standard_uom,
                                               I_wksht_qty * L_multiple2,
                                               L_uop,
                                               L_supp_pack_size) then
         return FALSE;
      end if;
   ---
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END APPLY_ITEM_LIST;
-------------------------------------------------------------------------------
FUNCTION ORDLOC_WKS_INSERT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists           IN OUT   VARCHAR2,
                           I_order            IN       ORDHEAD.ORDER_NO%TYPE,
                           I_item             IN       ORDLOC_WKSHT.ITEM%TYPE,
                           I_item_parent      IN       ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                           I_diff1            IN       ORDLOC_WKSHT.DIFF_1%TYPE,
                           I_diff2            IN       ORDLOC_WKSHT.DIFF_2%TYPE,
                           I_diff3            IN       ORDLOC_WKSHT.DIFF_3%TYPE,
                           I_diff4            IN       ORDLOC_WKSHT.DIFF_4%TYPE,
                           I_loc_type         IN       ORDLOC_WKSHT.LOC_TYPE%TYPE,
                           I_location         IN       ORDLOC_WKSHT.LOCATION%TYPE,
                           I_act_qty          IN       ORDLOC_WKSHT.ACT_QTY%TYPE,
                           I_orig_country     IN       ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE,
                           I_standard_uom     IN       ORDLOC_WKSHT.STANDARD_UOM%TYPE,
                           I_wksht_qty        IN       ORDLOC_WKSHT.WKSHT_QTY%TYPE,
                           I_uop              IN       ORDLOC_WKSHT.UOP%TYPE,
                           I_supp_pack_size   IN       ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE)

   RETURN BOOLEAN IS

BEGIN

   O_exists := 'N';

   insert into ordloc_wksht (order_no,
                             item,
                             item_parent,
                             diff_1,
                             diff_2,
                             diff_3,
                             diff_4,
                             loc_type,
                             location,
                             act_qty,
                             origin_country_id,
                             standard_uom,
                             wksht_qty,
                             uop,
                             supp_pack_size)
                     values (I_order,
                             I_item,
                             I_item_parent,
                             I_diff1,
                             I_diff2,
                             I_diff3,
                             I_diff4,
                             DECODE(I_location, NULL, NULL, I_loc_type),
                             I_location,
                             I_act_qty,
                             I_orig_country,
                             I_standard_uom,
                             I_wksht_qty,
                             I_uop,
                             I_supp_pack_size);

   return TRUE;

EXCEPTION
   WHEN DUP_VAL_ON_INDEX THEN
      O_exists := 'Y';
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.ORDLOC_WKS_INSERT',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ORDLOC_WKS_INSERT;
-------------------------------------------------------------------------------
FUNCTION EXPAND_PARENT_PACK(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                            I_item             IN       ITEM_MASTER.ITEM%TYPE,
                            I_act_qty          IN       ORDLOC_WKSHT.ACT_QTY%TYPE,
                            I_loc              IN       ITEM_LOC.LOC%TYPE,
                            I_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                            I_supplier         IN       ORDHEAD.SUPPLIER%TYPE,
                            I_origin_country   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                            I_supp_pack_size   IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                            I_uop              IN       ORDLOC_WKSHT.UOP%TYPE,
                            I_uop_type         IN       VARCHAR2,
                            I_suom             IN       ORDLOC_WKSHT.STANDARD_UOM%TYPE,
                            I_wksht_qty        IN       ORDLOC_WKSHT.WKSHT_QTY%TYPE,
                            I_override_ind     IN       VARCHAR2,
                            I_contract_no      IN       CONTRACT_HEADER.CONTRACT_NO%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_inner_pack_ind ITEM_MASTER.CONTAINS_INNER_IND%TYPE;
   L_item           ITEM_MASTER.ITEM%TYPE;
   L_template_sum   ORDLOC_WKSHT.ACT_QTY%TYPE;
   L_type           ITEM_MASTER.PACK_TYPE%TYPE;
   L_dummy          ITEM_MASTER.ITEM%TYPE;

   L_pack_ind          ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind      ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind     ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type         ITEM_MASTER.PACK_TYPE%TYPE;
   L_act_qty           ORDLOC_WKSHT.ACT_QTY%TYPE;
   L_origin_country    ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_standard_uom      ITEM_MASTER.STANDARD_UOM%TYPE;
   L_wksht_qty         ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_supp_pack_size    ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;


   cursor C_INNER_PACK_IND is
      select contains_inner_ind
        from item_master
       where item = I_item;

   cursor C_INNER_ORDERABLE is
      select pi.pack_no
        from item_master im,
             packitem pi
       where im.item          = pi.pack_no
         and pi.pack_no       = I_item
         and im.orderable_ind = 'N';

   cursor C_DEPT_CHECK_INNER is
      select packitem.item
        from packitem,
             item_master,
             ordhead
       where packitem.pack_no  = I_item
         and packitem.item     = item_master.item
         and ordhead.order_no  = I_order_no
         and item_master.dept != ordhead.dept;

   cursor C_DEPT_CHECK is
      select v_packsku_qty.item
        from v_packsku_qty,
             item_master,
             ordhead
       where v_packsku_qty.pack_no = I_item
         and v_packsku_qty.item    = item_master.item
         and ordhead.order_no      = I_order_no
         and item_master.dept     != ordhead.dept;

BEGIN
   if I_uop_type = 'C' then
      if I_supp_pack_size is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_supp_pack_size',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                    L_pack_ind,
                                    L_sellable_ind,
                                    L_orderable_ind,
                                    L_pack_type,
                                    I_item) = FALSE then
      return FALSE;
   end if;
   ---
   if I_contract_no is NULL then
      if L_pack_ind = 'N' then
         -- Insert a record for each approved transaction level item associated
         -- with the passed in item whose supplier/origin country relationship
         -- uses the order's supplier and specified origin country and where the
         -- the item does not already exist on ordsku.
         ---
         insert into ordloc_wksht (order_no,
                                   item,
                                   item_parent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   loc_type,
                                   location,
                                   calc_qty,
                                   act_qty,
                                   origin_country_id,
                                   standard_uom,
                                   wksht_qty,
                                   uop,
                                   supp_pack_size)
         select distinct I_order_no,
                im.item,
                im.item_parent,
                im.diff_1,
                im.diff_2,
                im.diff_3,
                im.diff_4,
                I_loc_type,
                I_loc,
                decode(I_uop_type, 'P', (((isc.ti * isc.hi * isc.supp_pack_size) / isc.supp_pack_size) * I_wksht_qty),
                                    I_wksht_qty),
                decode(I_uop_type, 'P', ((isc.ti * isc.hi * isc.supp_pack_size) *  I_wksht_qty),
                                   'C', (I_supp_pack_size * I_wksht_qty),
                                    I_wksht_qty),
                I_origin_country,
                I_suom,
                decode(I_uop_type, 'P', (((isc.ti * isc.hi * isc.supp_pack_size) / isc.supp_pack_size) * I_wksht_qty),
                                    I_wksht_qty),
                decode(I_uop_type, 'P', its.case_name,I_uop),
                decode(I_override_ind, 'Y', I_supp_pack_size, isc.supp_pack_size)
           from item_master im,
                item_supp_country isc,
                item_supplier its
          where (im.item_parent         = I_item
                 or im.item_grandparent = I_item)
            and isc.item              = im.item
            and its.item              = im.item
            and isc.origin_country_id = I_origin_country
            and isc.supplier          = I_supplier
            and not exists (select 'x'
                              from ordsku
                             where order_no = I_order_no
                               and item     = im.item)
            and not exists (select 'x'
                              from ordloc_wksht ow
                             where ow.order_no              = I_order_no
                               and ow.item                  = im.item
                               and (ow.location             is NULL
                                    or ow.location          = I_loc)
                               and ow.store_grade          is NULL
                               and ow.store_grade_group_id is NULL)
            and im.status     = 'A'
            and im.tran_level = im.item_level;
         ---
         -- Insert all skus in the style that already exist on ordsku using the
         -- origin country and supplier pack size of the SKU on ordsku.
         ---
         insert into ordloc_wksht (order_no,
                                   item,
                                   item_parent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   loc_type,
                                   location,
                                   calc_qty,
                                   act_qty,
                                   origin_country_id,
                                   standard_uom,
                                   wksht_qty,
                                   uop,
                                   supp_pack_size)
         select distinct I_order_no,
                im.item,
                im.item_parent,
                im.diff_1,
                im.diff_2,
                im.diff_3,
                im.diff_4,
                I_loc_type,
                I_loc,
                decode(I_uop_type, 'P', (TRUNC(((isc.ti * isc.hi * isc.supp_pack_size) / os.supp_pack_size) * I_wksht_qty)),
                                    I_wksht_qty),
                decode(I_uop_type, 'P', ((isc.ti * isc.hi * isc.supp_pack_size) *  I_wksht_qty),
                                   'C', (os.supp_pack_size * I_wksht_qty),
                                    I_wksht_qty),
                os.origin_country_id,
                im.standard_uom,
                decode(I_uop_type, 'P', (TRUNC(((isc.ti * isc.hi * isc.supp_pack_size) / os.supp_pack_size) * I_wksht_qty)),
                                    I_wksht_qty),
                decode(I_uop_type, 'P', its.case_name,I_uop),
                os.supp_pack_size
          from item_master       im,
               item_supp_country isc,
               ordsku            os,
               item_supplier     its
         where (im.item_parent         = I_item
                or im.item_grandparent = I_item)
           and os.order_no           = I_order_no
           and os.item               = im.item
           and its.item              = im.item
           and isc.item              = im.item
           and isc.origin_country_id = I_origin_country
           and isc.supplier          = I_supplier
           and not exists (select 'x'
                             from ordloc_wksht ow
                            where ow.order_no              = I_order_no
                              and ow.item                  = im.item
                              and (ow.location             is NULL
                                   or ow.location          = I_loc)
                              and ow.store_grade          is NULL
                              and ow.store_grade_group_id is NULL)
           and im.status     = 'A'
           and im.tran_level = im.item_level;
      elsif L_pack_ind = 'Y' then
         ---
         if L_pack_type = 'V' then
            O_error_message := SQL_LIB.CREATE_MSG('VENDOR_PACK', I_item);
            return FALSE;
         end if;
         ---
         open C_INNER_PACK_IND;
         fetch C_INNER_PACK_IND into L_inner_pack_ind;
         close C_INNER_PACK_IND;
         ---
         if L_inner_pack_ind = 'Y' then
            open C_DEPT_CHECK_INNER;
            fetch C_DEPT_CHECK_INNER into L_dummy;
            if C_DEPT_CHECK_INNER%FOUND then
               O_error_message := SQL_LIB.CREATE_MSG('ORDER_PACK_DEPT', NULL, NULL, NULL);
               close C_DEPT_CHECK_INNER;
               return FALSE;
            end if;
            close C_DEPT_CHECK_INNER;
            ---
            open C_INNER_ORDERABLE;
            fetch C_INNER_ORDERABLE into L_dummy;
            if C_INNER_ORDERABLE%FOUND then
               O_error_message := SQL_LIB.CREATE_MSG('INNER_ORDERABLE', NULL, NULL, NULL);
               close C_INNER_ORDERABLE;
               return FALSE;
            end if;
            close C_INNER_ORDERABLE;
         elsif L_inner_pack_ind = 'N' then
            open C_DEPT_CHECK;
            fetch C_DEPT_CHECK into L_dummy;
            if C_DEPT_CHECK%FOUND then
               O_error_message := SQL_LIB.CREATE_MSG('ORDER_PACK_DEPT', NULL, NULL, NULL);
               close C_DEPT_CHECK;
               return FALSE;
            end if;
            close C_DEPT_CHECK;
         end if;
         ---
         -- Insert all skus in the pack and any inner packs that do not
         -- already exist on ordsku.
         ---
         insert into ordloc_wksht (order_no,
                                   item,
                                   item_parent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   loc_type,
                                   location,
                                   calc_qty,
                                   act_qty,
                                   origin_country_id,
                                   standard_uom,
                                   wksht_qty,
                                   uop,
                                   supp_pack_size)
         select distinct I_order_no,
                im.item,
                im.item_parent,
                im.diff_1,
                im.diff_2,
                im.diff_3,
                im.diff_4,
                I_loc_type,
                I_loc,
                decode(I_uop_type, 'P', (v.qty * (((isc.ti* isc.hi) / isc.supp_pack_size) * I_wksht_qty)),
                                                                                  (v.qty * I_wksht_qty)),
                decode(I_uop_type, 'P', (v.qty * ((isc.ti * isc.hi * isc.supp_pack_size) *  I_wksht_qty)),
                                   'C', (v.qty * (I_supp_pack_size * I_wksht_qty)),
                                                            (v.qty * I_wksht_qty)),
                I_origin_country,
                im.standard_uom,
                decode(I_uop_type, 'P', (v.qty * (((isc.ti* isc.hi) / isc.supp_pack_size) * I_wksht_qty)),
                                                                                  (v.qty * I_wksht_qty)),
                decode(I_uop_type, 'P', its.case_name,I_uop),
                decode(I_override_ind, 'Y', I_supp_pack_size, isc.supp_pack_size)
           from item_master im,
                v_packsku_qty v,
                item_supp_country isc,
                item_supplier its
          where v.pack_no =  I_item
            and v.item     = isc.item
            and v.item = its.item
            and isc.origin_country_id = I_origin_country
            and isc.supplier          = I_supplier
            and not exists (select 'x'
                              from ordsku
                             where order_no = I_order_no
                               and item = v.item)
            and v.item = im.item(+)
            and not exists (select 'x'
                              from ordloc_wksht ow
                             where ow.order_no = I_order_no
                               and ow.item = v.item
                               and (ow.location is NULL
                                    or ow.location = I_loc)
                               and ow.store_grade is NULL
                               and ow.store_grade_group_id is NULL)
            and im.status = 'A'
            and im.tran_level = im.item_level;
         ---
         -- Insert all skus in the pack and any inner packs that already
         -- exist on ordsku using the ordsku origin_country_id and supplier pack size.
         ---
         insert into ordloc_wksht (order_no,
                                   item,
                                   item_parent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   loc_type,
                                   location,
                                   calc_qty,
                                   act_qty,
                                   origin_country_id,
                                   standard_uom,
                                   wksht_qty,
                                   uop,
                                   supp_pack_size)
         select distinct I_order_no,
                im.item,
                im.item_parent,
                im.diff_1,
                im.diff_2,
                im.diff_3,
                im.diff_4,
                I_loc_type,
                I_loc,
                decode(I_uop_type, 'P', (TRUNC(v.qty * (((isc.hi * isc.ti) / os.supp_pack_size) * I_wksht_qty))),
                                                                                        (v.qty * I_wksht_qty)),
                decode(I_uop_type, 'P', (v.qty * ((isc.ti * isc.hi * isc.supp_pack_size) *  I_wksht_qty)),
                                   'C', (v.qty * (os.supp_pack_size * I_wksht_qty)),
                                                             (v.qty * I_wksht_qty)),
                os.origin_country_id,
                im.standard_uom,
                decode(I_uop_type, 'P', (TRUNC(v.qty * (((isc.hi * isc.ti) / os.supp_pack_size) * I_wksht_qty))),
                                                                                        (v.qty * I_wksht_qty)),
                decode(I_uop_type, 'P', its.case_name,I_uop),
                os.supp_pack_size
         from  item_master im,
               v_packsku_qty v,
               ordsku os,
               item_supp_country isc,
               item_supplier its
         where v.pack_no     =  I_item
           and os.order_no   =  I_order_no
           and os.item       =  v.item
           and isc.item      = v.item
           and its.item      = v.item
           and isc.supplier  = I_supplier
           and isc.origin_country_id  = I_origin_country
           and os.origin_country_id   = I_origin_country
           and v.item        =  im.item(+)
           and im.tran_level = im.item_level
           and not exists (select 'x'
                             from ordloc_wksht ow
                            where ow.order_no = I_order_no
                              and ow.item      = v.item
                              and (ow.location is NULL
                                   or ow.location = I_loc)
                              and ow.store_grade is NULL
                              and ow.store_grade_group_id is NULL)
           and im.tran_level = im.item_level;
      end if; -- L_pack_ind in ('N', 'Y')
   else -- I_contract_no is not NULL
      insert into ordloc_wksht (order_no,
                                item,
                                item_parent,
                                diff_1,
                                diff_2,
                                diff_3,
                                diff_4,
                                loc_type,
                                location,
                                calc_qty,
                                act_qty,
                                origin_country_id,
                                standard_uom,
                                wksht_qty,
                                uop,
                                supp_pack_size)
      with v_contract_item as (
      select distinct im.item item
        from contract_detail co,
             v_item_master im
       where co.contract_no = I_contract_no
         and im.status = 'A'
         and im.orderable_ind = 'Y'
         and nvl(im.deposit_item_type, 'X') != 'A'
         and im.sellable_ind = 'Y'
         and ((co.item_level_index = 1
             and co.item = im.item)
          or (co.item_level_index = 2
             and co.diff_1 = im.diff_1
             and (co.item_parent = im.item_parent
                  or co.item_grandparent = im.item_grandparent))
          or (co.item_level_index = 3
             and co.diff_2 = im.diff_2
             and (co.item_parent = im.item_parent
                  or co.item_grandparent = im.item_grandparent))
          or (co.item_level_index = 4
             and co.diff_3 = im.diff_3
             and (co.item_parent = im.item_parent
                  or co.item_grandparent = im.item_grandparent))
          or (co.item_level_index = 5
             and co.diff_4 = im.diff_4
             and (co.item_parent = im.item_parent
                  or co.item_grandparent = im.item_grandparent))
          or (co.item_level_index = 6
             and (co.item_parent = im.item_parent
                  or co.item_grandparent = im.item_grandparent))))
      select distinct I_order_no,
             im.item,
             im.item_parent,
             im.diff_1,
             im.diff_2,
             im.diff_3,
             im.diff_4,
             I_loc_type,
             I_loc,
             decode(I_uop_type, 'P', (((isc.ti * isc.hi * isc.supp_pack_size) / isc.supp_pack_size) * I_wksht_qty),
                                 I_wksht_qty),
             decode(I_uop_type, 'P', ((isc.ti * isc.hi * isc.supp_pack_size) *  I_wksht_qty),
                                'C', (I_supp_pack_size * I_wksht_qty),
                                 I_wksht_qty),
             I_origin_country,
             I_suom,
             decode(I_uop_type, 'P', (((isc.ti * isc.hi * isc.supp_pack_size) / isc.supp_pack_size) * I_wksht_qty),
                                 I_wksht_qty),
             decode(I_uop_type, 'P', its.case_name,I_uop),
             decode(I_override_ind, 'Y', I_supp_pack_size, isc.supp_pack_size)
        from item_master im,
             item_supp_country isc,
             item_supplier its,
             v_contract_item vci
       where (im.item_parent         = I_item
              or im.item_grandparent = I_item)
         and isc.item              = im.item
         and its.item              = im.item
         and isc.origin_country_id = I_origin_country
         and isc.supplier          = I_supplier
         and not exists (select 'x'
                           from ordsku
                          where order_no = I_order_no
                            and item     = im.item)
         and not exists (select 'x'
                           from ordloc_wksht ow
                          where ow.order_no              = I_order_no
                            and ow.item                  = im.item
                            and (ow.location             is NULL
                                 or ow.location          = I_loc)
                            and ow.store_grade          is NULL
                            and ow.store_grade_group_id is NULL)
         and im.status     = 'A'
         and im.tran_level = im.item_level
         and vci.item      = im.item
       union all
       select distinct I_order_no,
              im.item,
              im.item_parent,
              im.diff_1,
              im.diff_2,
              im.diff_3,
              im.diff_4,
              I_loc_type,
              I_loc,
              decode(I_uop_type, 'P', (TRUNC(((isc.ti * isc.hi * isc.supp_pack_size) / os.supp_pack_size) * I_wksht_qty)),
                                  I_wksht_qty),
              decode(I_uop_type, 'P', ((isc.ti * isc.hi * isc.supp_pack_size) *  I_wksht_qty),
                                 'C', (os.supp_pack_size * I_wksht_qty),
                                  I_wksht_qty),
              os.origin_country_id,
              im.standard_uom,
              decode(I_uop_type, 'P', (TRUNC(((isc.ti * isc.hi * isc.supp_pack_size) / os.supp_pack_size) * I_wksht_qty)),
                                  I_wksht_qty),
              decode(I_uop_type, 'P', its.case_name,I_uop),
              os.supp_pack_size
         from item_master       im,
              item_supp_country isc,
              ordsku            os,
              item_supplier     its,
              v_contract_item   vci
        where (im.item_parent         = I_item
               or im.item_grandparent = I_item)
          and os.order_no           = I_order_no
          and os.item               = im.item
          and its.item              = im.item
          and isc.item              = im.item
          and isc.origin_country_id = I_origin_country
          and isc.supplier          = I_supplier
          and not exists (select 'x'
                            from ordloc_wksht ow
                           where ow.order_no              = I_order_no
                             and ow.item                  = im.item
                             and (ow.location             is NULL
                                  or ow.location          = I_loc)
                             and ow.store_grade          is NULL
                             and ow.store_grade_group_id is NULL)
          and im.status     = 'A'
          and im.tran_level = im.item_level
          and vci.item      = im.item;
   end if; -- I_contract_no NULL, NOT NULL

   ---
   if I_loc is null then
      delete from ordloc_wksht
            where order_no    = I_order_no
              and item_parent = I_item
              and item is null
              and location is null
              and origin_country_id = I_origin_country;
   else
      delete from ordloc_wksht
            where order_no    = I_order_no
              and item_parent = I_item
              and item is null
              and loc_type    = I_loc_type
              and location    = I_loc
              and origin_country_id = I_origin_country;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.EXPAND_PARENT_PACK',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXPAND_PARENT_PACK;
-------------------------------------------------------------------------------
FUNCTION POP_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                   I_supplier        IN       ORDHEAD.SUPPLIER%TYPE,
                   I_source          IN       VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN IS

   L_earliest_ship_date  ORDSKU.EARLIEST_SHIP_DATE%TYPE;
   L_latest_ship_date    ORDSKU.LATEST_SHIP_DATE%TYPE;
   L_pickup_loc          ORDHEAD.PICKUP_LOC%TYPE;
   L_pickup_no           ORDHEAD.PICKUP_NO%TYPE;
   L_elc_ind             SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_ord_import_ind      ORDHEAD.IMPORT_ORDER_IND%TYPE;
   L_ord_currency        ORDHEAD.CURRENCY_CODE%TYPE;
   L_ord_exchg_rate      ORDHEAD.EXCHANGE_RATE%TYPE;
   L_supplier_currency   CURRENCIES.CURRENCY_CODE%TYPE;
   L_contract_currency   CURRENCIES.CURRENCY_CODE%TYPE;
   L_import_country_id   ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   L_origin_country_id   ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_contract_no         ORDHEAD.CONTRACT_NO%TYPE;
   L_latest_ship_days    SYSTEM_OPTIONS.LATEST_SHIP_DAYS%TYPE;
   L_item                ITEM_MASTER.ITEM%TYPE;
   L_pack_ind            ITEM_MASTER.PACK_IND%TYPE;
   L_previous_item       ITEM_MASTER.ITEM%TYPE := -1;
   L_prev_pack_ind       ITEM_MASTER.PACK_IND%TYPE;
   L_recalc_hts          VARCHAR2(1) := 'N';
   L_pack_type           ITEM_MASTER.PACK_TYPE%TYPE;
   L_sellable_ind        ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind       ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_item           ITEM_MASTER.ITEM%TYPE;
   L_supplier            ORDHEAD.SUPPLIER%TYPE := I_supplier;
   L_add_to_po_tbl       ORDER_SETUP_SQL.ADD_ITEM_TO_PO_TBLTYPE;
   L_approved            VARCHAR2(10) := NULL;
   L_table               VARCHAR2(30);
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_INFO is
      select s.elc_ind,
             s.latest_ship_days,
             o.import_order_ind,
             o.earliest_ship_date,
             o.latest_ship_date,
             o.currency_code,
             o.exchange_rate,
             o.import_country_id,
             o.contract_no,
             o.pickup_loc,
             o.pickup_no
        from system_options s,
             ordhead o
       where order_no = I_order_no;

   cursor C_ORDLOC_WKSHT_SKU is
      select ow.order_no,
             L_elc_ind,
             L_latest_ship_days,
             DECODE(L_import_country_id, ow.origin_country_id, 'N', 'Y'), --L_ord_import_ind
             L_earliest_ship_date,
             L_latest_ship_date,
             L_ord_currency,
             L_ord_exchg_rate,
             L_import_country_id,
             L_contract_no,
             L_contract_currency,
             L_pickup_loc,
             L_pickup_no,
             ow.item,
             ow.ref_item,
             im.pack_ind,
             im.item_xform_ind,
             im.sellable_ind,
             L_supplier,
             L_supplier_currency,
             ow.origin_country_id,
             ow.location,
             ow.loc_type,
             ow.supp_pack_size,
             NVL(isc.lead_time, 0) lead_time,
             ow.act_qty,
             ow.last_rounded_qty,
             ow.last_grp_rounded_qty,
             NULL, -- I_unit_cost
             NULL, -- I_supp_unit_cost
             NULL, -- I_source_type
             NULL, -- I_non_scale_ind
             NULL, -- I_tsf_po_link_no
             NVL(LAG(ow.item) over (order by ow.item), -1) prev_item,
             LAG(im.pack_ind) over (order by ow.item) prev_pack_ind,
             'ORDMTXWS' -- I_calling_form
        from ordloc_wksht ow,
             item_master im,
             item_supp_country isc
       where order_no             = I_order_no
         and ow.item              = im.item
         and im.item              = isc.item
         and isc.supplier         = I_supplier
         and ow.origin_country_id = isc.origin_country_id
         and ow.location         is NOT NULL
         and ((nvl(ow.act_qty, 0) > 0  and I_source is NULL) or
              (nvl(ow.act_qty, 0) >= 0 and upper(I_source) = 'POI'))
   UNION
      select ow.order_no,
             L_elc_ind,
             L_latest_ship_days,
             DECODE(L_import_country_id, ow.origin_country_id, 'N', 'Y'), --L_ord_import_ind
             L_earliest_ship_date,
             L_latest_ship_date,
             L_ord_currency,
             L_ord_exchg_rate,
             L_import_country_id,
             L_contract_no,
             L_contract_currency,
             L_pickup_loc,
             L_pickup_no,
             ow.item,
             ow.ref_item,
             im.pack_ind,
             im.item_xform_ind,
             im.sellable_ind,
             L_supplier,
             L_supplier_currency,
             ow.origin_country_id,
             ow.location,
             ow.loc_type,
             ow.supp_pack_size,
             NVL(isc.lead_time, 0) lead_time,
             ow.act_qty,
             ow.last_rounded_qty,
             ow.last_grp_rounded_qty,
             NULL, -- I_unit_cost
             NULL, -- I_supp_unit_cost
             NULL, -- I_source_type
             NULL, -- I_non_scale_ind
             NULL, -- I_tsf_po_link_no
             NVL(LAG(ow.item) over (order by ow.item), -1) prev_item,
             LAG(im.pack_ind) over (order by ow.item) prev_pack_ind,
             'ORDMTXWS' -- I_calling_form
        from ordloc_wksht ow,
             item_master im,
             item_supp_country isc
       where order_no             = I_order_no
         and ow.item              = im.item
         and im.item              = isc.item
         and isc.supplier         = I_supplier
         and ow.origin_country_id = isc.origin_country_id
         and ow.location         is NOT NULL
         and nvl(ow.act_qty, 0) = 0
         and L_approved='Y'
         order by item,loc_type desc;

   cursor C_ORDLOC_WKSHT_SKU_CONTENT is
      select ow.item,
             ow.location,
             ow.origin_country_id
        from ordloc_wksht ow,
             item_master im,
             item_supp_country isc
       where order_no             = I_order_no
         and ow.item              = im.item
         and im.item              = isc.item
         and isc.supplier         = I_supplier
         and ow.origin_country_id = isc.origin_country_id
         and ow.location          is NOT NULL
         and ( (nvl(ow.act_qty, 0)   > 0 and I_source is NULL) or
               (nvl(ow.act_qty, 0)   >= 0 and upper(I_source) = 'POI') )
         and im.deposit_item_type = 'E' --(Contents)
       order by ow.item;

   cursor C_LOCK_ORDLOC_WKSHT is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         for update nowait;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   open C_GET_INFO;
   fetch C_GET_INFO into L_elc_ind,
                         L_latest_ship_days,
                         L_ord_import_ind,
                         L_earliest_ship_date,
                         L_latest_ship_date,
                         L_ord_currency,
                         L_ord_exchg_rate,
                         L_import_country_id,
                         L_contract_no,
                         L_pickup_loc,
                         L_pickup_no;
   close C_GET_INFO;
   ---
   --Retrieve the currency for the supplier and use this currency in the
   --conversion of the unit cost from the supplier currency to the order currency.
   ---
   if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                I_supplier,
                                'V',
                                NULL,
                                L_supplier_currency) = FALSE then
      return FALSE;
   end if;
   ---
   if L_contract_no is NOT NULL then
      if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                   L_contract_no,
                                   'C',
                                   NULL,
                                   L_contract_currency) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if ORDER_ATTRIB_SQL.PREV_APPROVED(O_error_message,
                                     L_approved,
                                     I_order_no) = FALSE then
      return FALSE;
   end if;

   open C_ORDLOC_WKSHT_SKU;
   fetch C_ORDLOC_WKSHT_SKU bulk collect into L_add_to_po_tbl;
   close C_ORDLOC_WKSHT_SKU;

   if L_add_to_po_tbl is not NULL and L_add_to_po_tbl.count > 0 then
      if ADD_ITEM_TO_PO(O_error_message,
                        L_recalc_hts,
                        L_add_to_po_tbl) = FALSE then
         return FALSE;
      end if;
      ---
      -- contents item- insert the associated container
      FOR rec in C_ORDLOC_WKSHT_SKU_CONTENT LOOP
         if POP_SINGLE_CONTAINER_ITEM (O_error_message,
                                       I_order_no,
                                       I_supplier,
                                       rec.location,
                                       rec.item,
                                       rec.origin_country_id
                                       ) = FALSE then
            return FALSE;
         end if;
      END LOOP;
      ---

      -- Process the last item returned in the loop.
      ---
      L_item := L_add_to_po_tbl(L_add_to_po_tbl.count).I_item;
      L_origin_country_id := L_add_to_po_tbl(L_add_to_po_tbl.count).I_origin_country_id;
      if L_recalc_hts = 'Y' then
         if L_pack_ind = 'Y' then
            if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                             L_pack_ind,
                                             L_sellable_ind,
                                             L_orderable_ind,
                                             L_pack_type,
                                             L_item) = FALSE then
                return FALSE;
            end if;
         end if;
         ---
         if L_pack_ind = 'Y' and L_pack_type = 'B' then
            L_pack_item :=  L_item;
            L_item      :=  NULL;
         else
            L_item      :=  L_item;
            L_pack_item :=  NULL;
         end if;
         ---
         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PA',
                                   L_item,
                                   NULL,
                                   NULL,
                                   NULL,
                                   I_order_no,
                                   NULL,
                                   L_pack_item,
                                   NULL,
                                   NULL,
                                   L_import_country_id,
                                   L_origin_country_id,
                                   NULL,
                                   NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PE',
                                   L_item,
                                   NULL,
                                   NULL,
                                   NULL,
                                   I_order_no,
                                   NULL,
                                   L_pack_item,
                                   NULL,
                                   NULL,
                                   L_import_country_id,
                                   L_origin_country_id,
                                   NULL,
                                   NULL) = FALSE then
            return FALSE;
         end if;
      end if; -- end of L_recalc_hts = 'Y' */
   end if;

   if ORDER_SETUP_SQL.UPDATE_SHIP_DATES(O_error_message,
                                        I_order_no) = FALSE then
      return FALSE;
   end if;

   L_table := 'ORDLOC_WKSHT';
   open C_LOCK_ORDLOC_WKSHT;
   close C_LOCK_ORDLOC_WKSHT;

   if I_source is NULL then
      delete from ordloc_wksht
            where order_no      = I_order_no
              and item     is not NULL
              and location is not NULL
              and act_qty       >= 0 ;
   else
      delete from ordloc_wksht
      where order_no      = I_order_no
        and item     is not NULL
        and location is not NULL;
   end if;


   -- Round the order.
   if ROUNDING_SQL.ORDER_INTERFACE(O_error_message,
                                   I_order_no,
                                   I_supplier) = FALSE then
      return FALSE;
   end if;
   ---
   L_add_to_po_tbl.delete;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            TO_CHAR(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.POP_ITEMS',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_ITEMS;
-------------------------------------------------------------------------------
FUNCTION ADD_ITEM_TO_PO(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_recalc_hts             IN OUT   VARCHAR2,
                        I_order_no               IN       ORDHEAD.ORDER_NO%TYPE,
                        I_elc_ind                IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                        I_latest_ship_days       IN       SYSTEM_OPTIONS.LATEST_SHIP_DAYS%TYPE,
                        I_ord_import_ind         IN       ORDHEAD.IMPORT_ORDER_IND%TYPE,
                        I_earliest_ship_date     IN       ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
                        I_latest_ship_date       IN       ORDHEAD.LATEST_SHIP_DATE%TYPE,
                        I_ord_currency           IN       ORDHEAD.CURRENCY_CODE%TYPE,
                        I_ord_exchg_rate         IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                        I_import_country_id      IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                        I_contract_no            IN       ORDHEAD.CONTRACT_NO%TYPE,
                        I_contract_currency      IN       CONTRACT_HEADER.CURRENCY_CODE%TYPE,
                        I_pickup_loc             IN       ORDHEAD.PICKUP_LOC%TYPE,
                        I_pickup_no              IN       ORDHEAD.PICKUP_NO%TYPE,
                        I_item                   IN       ORDSKU.ITEM%TYPE,
                        I_ref_item               IN       ORDSKU.REF_ITEM%TYPE,
                        I_pack_ind               IN       ITEM_MASTER.PACK_IND%TYPE,
                        I_supplier               IN       ORDHEAD.SUPPLIER%TYPE,
                        I_supplier_currency      IN       SUPS.CURRENCY_CODE%TYPE,
                        I_origin_country_id      IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                        I_location               IN       ORDLOC.LOCATION%TYPE,
                        I_loc_type               IN       ORDLOC.LOC_TYPE%TYPE,
                        I_supp_pack_size         IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                        I_lead_time              IN       ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
                        I_act_qty                IN       ORDLOC.QTY_ORDERED%TYPE,
                        I_last_rounded_qty       IN       ORDLOC.LAST_ROUNDED_QTY%TYPE,
                        I_last_grp_rounded_qty   IN       ORDLOC.LAST_GRP_ROUNDED_QTY%TYPE,
                        I_unit_cost              IN       ORDLOC.UNIT_COST%TYPE,
                        I_supp_unit_cost         IN       ORDLOC.UNIT_COST%TYPE,
                        I_source_type            IN       CODE_DETAIL.CODE%TYPE,
                        I_non_scale_ind          IN       ORDLOC.NON_SCALE_IND%TYPE,
                        I_tsf_po_link_no         IN       ORDLOC.TSF_PO_LINK_NO%TYPE,
                        I_previous_item          IN       ORDSKU.ITEM%TYPE,
                        I_prev_pack_ind          IN       ITEM_MASTER.PACK_IND%TYPE,
                        I_calling_form           IN       VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   if ADD_ITEM_TO_PO(O_error_message,
                     O_recalc_hts,
                     I_order_no,
                     I_elc_ind,
                     I_latest_ship_days,
                     I_ord_import_ind,
                     I_earliest_ship_date,
                     I_latest_ship_date,
                     I_ord_currency,
                     I_ord_exchg_rate,
                     I_import_country_id,
                     I_contract_no,
                     I_contract_currency,
                     I_pickup_loc,
                     I_pickup_no,
                     I_item,
                     I_ref_item,
                     I_pack_ind,
                     NULL, -- item xform ind
                     NULL, -- sellable ind
                     I_supplier,
                     I_supplier_currency,
                     I_origin_country_id,
                     I_location,
                     I_loc_type,
                     I_supp_pack_size,
                     I_lead_time,
                     I_act_qty,
                     I_last_rounded_qty,
                     I_last_grp_rounded_qty,
                     I_unit_cost,
                     I_supp_unit_cost,
                     I_source_type,
                     I_non_scale_ind,
                     I_tsf_po_link_no,
                     I_previous_item,
                     I_prev_pack_ind,
                     I_calling_form) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.ADD_ITEM_TO_PO',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_ITEM_TO_PO;
-------------------------------------------------------------------------------
FUNCTION ADD_ITEM_TO_PO(O_error_message        IN OUT   VARCHAR2,
                        O_recalc_hts           IN OUT   VARCHAR2,
                        I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                        I_elc_ind              IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                        I_latest_ship_days     IN       SYSTEM_OPTIONS.LATEST_SHIP_DAYS%TYPE,
                        I_ord_import_ind       IN       ORDHEAD.IMPORT_ORDER_IND%TYPE,
                        I_earliest_ship_date   IN       ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
                        I_latest_ship_date     IN       ORDHEAD.LATEST_SHIP_DATE%TYPE,
                        I_ord_currency         IN       ORDHEAD.CURRENCY_CODE%TYPE,
                        I_ord_exchg_rate       IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                        I_import_country_id    IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                        I_contract_no          IN       ORDHEAD.CONTRACT_NO%TYPE,
                        I_contract_currency    IN       CONTRACT_HEADER.CURRENCY_CODE%TYPE,
                        I_pickup_loc           IN       ORDHEAD.PICKUP_LOC%TYPE,
                        I_pickup_no            IN       ORDHEAD.PICKUP_NO%TYPE,
                        I_item                 IN       ORDSKU.ITEM%TYPE,
                        I_ref_item             IN       ORDSKU.REF_ITEM%TYPE,
                        I_pack_ind             IN       ITEM_MASTER.PACK_IND%TYPE,
                        I_item_xform_ind       IN       ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                        I_sellable_ind         IN       ITEM_MASTER.SELLABLE_IND%TYPE,
                        I_supplier             IN       ORDHEAD.SUPPLIER%TYPE,
                        I_supplier_currency    IN       SUPS.CURRENCY_CODE%TYPE,
                        I_origin_country_id    IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                        I_location             IN       ORDLOC.LOCATION%TYPE,
                        I_loc_type             IN       ORDLOC.LOC_TYPE%TYPE,
                        I_supp_pack_size       IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                        I_lead_time            IN       ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
                        I_act_qty              IN       ORDLOC.QTY_ORDERED%TYPE,
                        I_last_rounded_qty     IN       ORDLOC.LAST_ROUNDED_QTY%TYPE,
                        I_last_grp_rounded_qty IN       ORDLOC.LAST_GRP_ROUNDED_QTY%TYPE,
                        I_unit_cost            IN       ORDLOC.UNIT_COST%TYPE,
                        I_supp_unit_cost       IN       ORDLOC.UNIT_COST%TYPE,
                        I_source_type          IN       CODE_DETAIL.CODE%TYPE,
                        I_non_scale_ind        IN       ORDLOC.NON_SCALE_IND%TYPE,
                        I_tsf_po_link_no       IN       ORDLOC.TSF_PO_LINK_NO%TYPE,
                        I_previous_item        IN       ORDSKU.ITEM%TYPE,
                        I_prev_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE,
                        I_calling_form         IN       VARCHAR2)
   return BOOLEAN is
   L_recalc_hts      VARCHAR2(1) := 'N';
   L_add_to_po_rec   ORDER_SETUP_SQL.ADD_ITEM_TO_PO_RECTYPE;
   L_add_to_po_tbl   ORDER_SETUP_SQL.ADD_ITEM_TO_PO_TBLTYPE;

BEGIN
   L_add_to_po_rec.I_order_no             := I_order_no            ;
   L_add_to_po_rec.I_elc_ind              := I_elc_ind             ;
   L_add_to_po_rec.I_latest_ship_days     := I_latest_ship_days    ;
   L_add_to_po_rec.I_ord_import_ind       := I_ord_import_ind      ;
   L_add_to_po_rec.I_earliest_ship_date   := I_earliest_ship_date  ;
   L_add_to_po_rec.I_latest_ship_date     := I_latest_ship_date    ;
   L_add_to_po_rec.I_ord_currency         := I_ord_currency        ;
   L_add_to_po_rec.I_ord_exchg_rate       := I_ord_exchg_rate      ;
   L_add_to_po_rec.I_import_country_id    := I_import_country_id   ;
   L_add_to_po_rec.I_contract_no          := I_contract_no         ;
   L_add_to_po_rec.I_contract_currency    := I_contract_currency   ;
   L_add_to_po_rec.I_pickup_loc           := I_pickup_loc          ;
   L_add_to_po_rec.I_pickup_no            := I_pickup_no           ;
   L_add_to_po_rec.I_item                 := I_item                ;
   L_add_to_po_rec.I_ref_item             := I_ref_item            ;
   L_add_to_po_rec.I_pack_ind             := I_pack_ind            ;
   L_add_to_po_rec.I_item_xform_ind       := I_item_xform_ind      ;
   L_add_to_po_rec.I_sellable_ind         := I_sellable_ind        ;
   L_add_to_po_rec.I_supplier             := I_supplier            ;
   L_add_to_po_rec.I_supplier_currency    := I_supplier_currency   ;
   L_add_to_po_rec.I_origin_country_id    := I_origin_country_id   ;
   L_add_to_po_rec.I_location             := I_location            ;
   L_add_to_po_rec.I_loc_type             := I_loc_type            ;
   L_add_to_po_rec.I_supp_pack_size       := I_supp_pack_size      ;
   L_add_to_po_rec.I_lead_time            := I_lead_time           ;
   L_add_to_po_rec.I_act_qty              := I_act_qty             ;
   L_add_to_po_rec.I_last_rounded_qty     := I_last_rounded_qty    ;
   L_add_to_po_rec.I_last_grp_rounded_qty := I_last_grp_rounded_qty;
   L_add_to_po_rec.I_unit_cost            := I_unit_cost           ;
   L_add_to_po_rec.I_supp_unit_cost       := I_supp_unit_cost      ;
   L_add_to_po_rec.I_source_type          := I_source_type         ;
   L_add_to_po_rec.I_non_scale_ind        := I_non_scale_ind       ;
   L_add_to_po_rec.I_tsf_po_link_no       := I_tsf_po_link_no      ;
   L_add_to_po_rec.I_previous_item        := I_previous_item       ;
   L_add_to_po_rec.I_prev_pack_ind        := I_prev_pack_ind       ;
   L_add_to_po_rec.I_calling_form         := I_calling_form        ;

   L_add_to_po_tbl(1) := L_add_to_po_rec;

   if ADD_ITEM_TO_PO(O_error_message,
                     L_recalc_hts,
                     L_add_to_po_tbl) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_SETUP_SQL.ADD_ITEM_TO_PO',
                                             to_char(SQLCODE));
      return FALSE;
END ADD_ITEM_TO_PO;
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_ITEM_TO_PO(O_error_message   IN OUT   VARCHAR2,
                        O_recalc_hts      IN OUT   VARCHAR2,
                        L_add_to_po_tbl   IN       ORDER_SETUP_SQL.ADD_ITEM_TO_PO_TBLTYPE)
   return BOOLEAN is
   L_program                   VARCHAR2(30) := 'ORDER_SETUP_SQL.ADD_ITEM_TO_PO';
   L_pack_ind                  ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind              ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind             ITEM_MASTER.ORDERABLE_IND%TYPE := NULL;
   L_pack_type                 ITEM_MASTER.PACK_TYPE%TYPE   := 'V';
   L_item                      ITEM_MASTER.ITEM%TYPE;
   L_ref_item                  ITEM_MASTER.ITEM%TYPE;
   L_pack_item                 ITEM_MASTER.ITEM%TYPE;
   L_status                    ITEM_MASTER.STATUS%TYPE;
   L_exists                    VARCHAR2(1)               := 'N';
   L_vdate                     ORDHEAD.NOT_BEFORE_DATE%TYPE      := GET_VDATE;
   L_unit_cost                 ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_av_cost                   ITEM_LOC_SOH.AV_COST%TYPE;
   L_unit_cost_loc             ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_unit_retail               ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_unit_retail       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom               ITEM_LOC.SELLING_UOM%TYPE;
   L_cost_source               ORDLOC.COST_SOURCE%TYPE;
   L_dummy                     VARCHAR2(1);
   L_non_scale_ind             ORDLOC.NON_SCALE_IND%TYPE;
   L_ord_cost_source           ORDLOC.COST_SOURCE%TYPE;
   L_ord_unit_cost             ORDLOC.UNIT_COST%TYPE;
   L_unit_cost_init            ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_item_master               ITEM_MASTER%ROWTYPE;
   L_loc_currency              CURRENCIES.CURRENCY_CODE%TYPE;
   L_system_currency           SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;
   L_loc_type                  ITEM_LOC.LOC_TYPE%TYPE := NULL;
   L_true                      BOOLEAN := FALSE;
   L_prev_approved             VARCHAR2(1);

   L_nil_input                 NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;
   L_nil_index                 NUMBER := 0;
   L_comp_item_cost_tbl        OBJ_COMP_ITEM_COST_TBL := NEW OBJ_COMP_ITEM_COST_TBL();
   L_cost_event_process_id     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_default_tax_type          SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE := NULL;
   L_estimated_instock_date    ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE := NULL;

   L_table                     VARCHAR2(30);
   L_clearing_zone_id          ORDHEAD.CLEARING_ZONE_ID%TYPE;
   RECORD_LOCKED               EXCEPTION;
   PRAGMA                      EXCEPTION_INIT(Record_Locked, -54);

   cursor C_ITEMLOC (P_item     item_loc.item%type,
                     P_location item_loc.loc%type) is
      select status
        from item_loc
       where item = P_item
         and loc  = P_location;

   cursor C_CHECK_ORDSKU (P_item     ordsku.item%type,
                          P_order_no ordsku.order_no%type) is
      select ref_item
        from ordsku
       where item     = P_item
         and order_no = P_order_no;

   cursor C_HTS_CHAPTER (P_item              ordsku_hts.item%type,
                         P_import_country_id ordsku_hts.import_country_id%type,
                         P_order_no          ordsku_hts.order_no%type) is
      select distinct h.chapter
        from hts h,
             ordsku_hts osh
       where osh.order_no          = P_order_no
         and osh.hts               = h.hts
         and osh.import_country_id = P_import_country_id
         and osh.item              = P_item
         and osh.pack_item is NULL;

   cursor C_CHECK_ORDLOC (P_order_no ordloc.order_no%type,
                          P_item     ordloc.item%type,
                          P_location ordloc.location%type) is
      select cost_source,
             unit_cost
        from ordloc
       where item     = P_item
         and order_no = P_order_no
         and location = P_location
         for update nowait;

   cursor C_IB_WH_EXISTS (P_order_no ordloc.order_no%type,
                          P_item     ordloc.item%type,
                          P_location ordloc.location%type) is
      select 'Y'
        from ordloc ol, wh w1, wh w2
       where ol.order_no = P_order_no
         and ol.item = P_item
         and ol.location = w1.wh
         and w1.ib_ind = 'Y'
         and w1.repl_wh_link = w2.repl_wh_link
         and w2.wh = P_location;

   cursor C_LOCK_ORDLOC_WH (P_order_no ordloc.order_no%type,
                            P_item     ordloc.item%type,
                            P_location ordloc.location%type) is
      select 'x'
        from ordloc ol
       where ol.order_no = P_order_no
         and ol.item = P_item
         and exists (select 'x'
                       from wh w1, wh w2
                      where w1.repl_wh_link = w2.repl_wh_link
                        and w2.wh = P_location)
         for update nowait;

   cursor C_GET_SYSTEM_CURRENCY is
      select currency_code
        from system_options;

   cursor C_TAX_TYPE is
      select default_tax_type
        from system_options;

   cursor C_ORDHEAD (P_order_no ordhead.order_no%type ) is
      select clearing_zone_id
        from ordhead
       where order_no=P_order_no ;


BEGIN

   open C_TAX_TYPE;
   fetch C_TAX_TYPE into L_default_tax_type;
   close C_TAX_TYPE;
   if L_add_to_po_tbl is not NULL and L_add_to_po_tbl.count > 0 then
       open C_ORDHEAD (L_add_to_po_tbl(1).I_order_no) ;
       fetch C_ORDHEAD into L_clearing_zone_id ;
       close C_ORDHEAD ;

       if L_add_to_po_tbl(1).I_ord_import_ind='Y' and L_clearing_zone_id is null then
          if ORDER_ATTRIB_SQL.GET_DEFAULT_CLEARING_ZONE_ID(O_error_message,
                                                           L_clearing_zone_id,
                                                           L_add_to_po_tbl(1).I_order_no,
                                                           L_add_to_po_tbl(1).I_import_country_id) = FALSE then
             return FALSE;
          end if;
          update ordhead
             set clearing_zone_id = L_clearing_zone_id
           where order_no=L_add_to_po_tbl(1).I_order_no ;
       end if;
   end if ;
   FOR idx in L_add_to_po_tbl.first..L_add_to_po_tbl.last LOOP

        if L_add_to_po_tbl(idx).I_pack_ind = 'Y' and L_add_to_po_tbl(idx).I_item != L_add_to_po_tbl(idx).I_previous_item then
               if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                                L_pack_ind,
                                                L_sellable_ind,
                                                L_orderable_ind,
                                                L_pack_type,
                                                L_add_to_po_tbl(idx).I_item) = FALSE then
                   return FALSE;
               end if;
        end if;

      ---
      L_status := NULL;
      ---
      open C_ITEMLOC(L_add_to_po_tbl(idx).I_item, L_add_to_po_tbl(idx).I_location);
      fetch C_ITEMLOC into L_status;
      close C_ITEMLOC;
      ---
      if L_status != 'A' then
         O_error_message := SQL_LIB.CREATE_MSG('ORDER_ITEM_NOT_ACTIVE',
                                               L_add_to_po_tbl(idx).I_item,
                                               to_char(L_add_to_po_tbl(idx).I_location),
                                               NULL);
         return FALSE;
      elsif (L_status is NULL or (L_status = 'A' and L_add_to_po_tbl(idx).I_pack_ind = 'Y')) then
         L_nil_index := L_nil_index + 1;
         ---
         L_nil_input(L_nil_index).item                    := L_add_to_po_tbl(idx).I_item;
         L_nil_input(L_nil_index).item_parent             := NULL;
         L_nil_input(L_nil_index).item_grandparent        := NULL;
         L_nil_input(L_nil_index).item_desc               := NULL;
         L_nil_input(L_nil_index).item_short_desc         := NULL;
         L_nil_input(L_nil_index).dept                    := NULL;
         L_nil_input(L_nil_index).item_class              := NULL;
         L_nil_input(L_nil_index).subclass                := NULL;
         L_nil_input(L_nil_index).item_level              := NULL;
         L_nil_input(L_nil_index).tran_level              := NULL;
         L_nil_input(L_nil_index).item_status             := NULL;
         L_nil_input(L_nil_index).waste_type              := NULL;
         L_nil_input(L_nil_index).sellable_ind            := NULL;
         L_nil_input(L_nil_index).orderable_ind           := NULL;
         L_nil_input(L_nil_index).pack_type               := NULL;
         L_nil_input(L_nil_index).diff_1                  := NULL;
         L_nil_input(L_nil_index).diff_2                  := NULL;
         L_nil_input(L_nil_index).diff_3                  := NULL;
         L_nil_input(L_nil_index).diff_4                  := NULL;
         L_nil_input(L_nil_index).loc                     := L_add_to_po_tbl(idx).I_location;
         L_nil_input(L_nil_index).loc_type                := L_add_to_po_tbl(idx).I_loc_type;
         L_nil_input(L_nil_index).daily_waste_pct         := NULL;
         L_nil_input(L_nil_index).unit_cost_loc           := NULL;
         L_nil_input(L_nil_index).unit_retail_loc         := NULL;
         L_nil_input(L_nil_index).selling_retail_loc      := NULL;
         L_nil_input(L_nil_index).selling_uom             := NULL;
         L_nil_input(L_nil_index).multi_units             := NULL;
         L_nil_input(L_nil_index).multi_unit_retail       := NULL;
         L_nil_input(L_nil_index).multi_selling_uom       := NULL;
         L_nil_input(L_nil_index).item_loc_status         := NULL;
         L_nil_input(L_nil_index).taxable_ind             := NULL;
         L_nil_input(L_nil_index).ti                      := NULL;
         L_nil_input(L_nil_index).hi                      := NULL;
         L_nil_input(L_nil_index).store_ord_mult          := NULL;
         L_nil_input(L_nil_index).meas_of_each            := NULL;
         L_nil_input(L_nil_index).meas_of_price           := NULL;
         L_nil_input(L_nil_index).uom_of_price            := NULL;
         L_nil_input(L_nil_index).primary_variant         := NULL;
         L_nil_input(L_nil_index).primary_supp            := NULL;
         L_nil_input(L_nil_index).primary_cntry           := NULL;
         L_nil_input(L_nil_index).local_item_desc         := NULL;
         L_nil_input(L_nil_index).local_short_desc        := NULL;
         L_nil_input(L_nil_index).primary_cost_pack       := NULL;
         L_nil_input(L_nil_index).store_price_ind         := NULL;
         L_nil_input(L_nil_index).uin_type                := NULL;
         L_nil_input(L_nil_index).uin_label               := NULL;
         L_nil_input(L_nil_index).capture_time            := NULL;
         L_nil_input(L_nil_index).ext_uin_ind             := 'N';
         L_nil_input(L_nil_index).source_method           := NULL;
         L_nil_input(L_nil_index).source_wh               := NULL;
         L_nil_input(L_nil_index).inbound_handling_days   := NULL;
         L_nil_input(L_nil_index).currency_code           := NULL;
         L_nil_input(L_nil_index).like_store              := NULL;
         L_nil_input(L_nil_index).default_to_children_ind := NULL;
         L_nil_input(L_nil_index).class_vat_ind           := NULL;
         L_nil_input(L_nil_index).hier_level              := NULL;
         L_nil_input(L_nil_index).hier_num_value          := NULL;
         L_nil_input(L_nil_index).hier_char_value         := NULL;
         L_nil_input(L_nil_index).costing_loc             := NULL;
         L_nil_input(L_nil_index).costing_loc_type        := NULL;
         L_nil_input(L_nil_index).ranged_ind              := 'Y';
         L_nil_input(L_nil_index).default_wh              := NULL;
         L_nil_input(L_nil_index).item_loc_ind            := 'Y';
         ---
         if L_pack_type = 'V' then
            L_nil_input(L_nil_index).pack_ind             := L_add_to_po_tbl(idx).I_pack_ind;
            L_nil_input(L_nil_index).receive_as_type      := 'P';
         else
            L_nil_input(L_nil_index).pack_ind             := NULL;
            L_nil_input(L_nil_index).receive_as_type      := NULL;
         end if;
      end if;
      ---
   END LOOP;

   if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message,
                                    L_nil_input) = FALSE then
      return FALSE;
   end if;

   FOR idx in L_add_to_po_tbl.first..L_add_to_po_tbl.last LOOP
      if L_add_to_po_tbl(idx).I_item != L_add_to_po_tbl(idx).I_previous_item then
         L_exists := 'N';
         open C_CHECK_ORDSKU (L_add_to_po_tbl(idx).I_item, L_add_to_po_tbl(idx).I_order_no);
--
         fetch C_CHECK_ORDSKU into L_ref_item;
         if C_CHECK_ORDSKU%FOUND then
            L_exists := 'Y';
         else
            L_exists := 'N';
         end if;
         close C_CHECK_ORDSKU;
         ---
         if L_exists = 'N' then
            insert into ordsku(order_no,
                               item,
                               ref_item,
                               origin_country_id,
                               earliest_ship_date,
                               latest_ship_date,
                               supp_pack_size,
                               non_scale_ind,
                               pickup_loc,
                               pickup_no)
                       values (L_add_to_po_tbl(idx).I_order_no,
                               L_add_to_po_tbl(idx).I_item,
                               L_add_to_po_tbl(idx).I_ref_item,
                               L_add_to_po_tbl(idx).I_origin_country_id,
                               NVL(L_add_to_po_tbl(idx).I_earliest_ship_date,(L_vdate + L_add_to_po_tbl(idx).I_lead_time)),
                               NVL(L_add_to_po_tbl(idx).I_latest_ship_date,(L_vdate + L_add_to_po_tbl(idx).I_lead_time + L_add_to_po_tbl(idx).I_latest_ship_days)),
                               L_add_to_po_tbl(idx).I_supp_pack_size,
                               'N',
                               L_add_to_po_tbl(idx).I_pickup_loc,
                               L_add_to_po_tbl(idx).I_pickup_no);
            ---
             -- Item Level Required Docs.
            ---
            if DOCUMENTS_SQL.GET_DEFAULTS (O_error_message,
                                           'IT',
                                           'POIT',
                                           L_add_to_po_tbl(idx).I_item,
                                           L_add_to_po_tbl(idx).I_order_no,
                                           NULL,
                                           L_add_to_po_tbl(idx).I_item) = FALSE then
               return FALSE;
            end if;
            ---
            -- Country Level Required Docs.
            ---
            if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                          'CTRY',
                                          'POIT',
                                          L_add_to_po_tbl(idx).I_origin_country_id,
                                          L_add_to_po_tbl(idx).I_order_no,
                                          NULL,
                                          L_add_to_po_tbl(idx).I_item) = FALSE then
               return FALSE;
            end if;
            ---
            if L_add_to_po_tbl(idx).I_elc_ind = 'Y' and L_add_to_po_tbl(idx).I_ord_import_ind = 'Y' then
               --Default HTS codes without recalculating.
               O_recalc_hts := 'Y';
               ---
               if ORDER_HTS_SQL.DEFAULT_HTS(O_error_message,
                                            L_add_to_po_tbl(idx).I_order_no,
                                            L_add_to_po_tbl(idx).I_item,
                                            NULL) = FALSE then
                  return FALSE;
               end if;
               ---
               -- Loop through all the chapters associated with the item
               -- and default HTS Chapter Docs.
               ---
               FOR C_chapter in C_HTS_CHAPTER(L_add_to_po_tbl(idx).I_item,
                                              L_add_to_po_tbl(idx).I_import_country_id,
                                              L_add_to_po_tbl(idx).I_order_no) LOOP
                  if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                                'HTSC',
                                                'POIT',
                                                C_chapter.chapter,
                                                L_add_to_po_tbl(idx).I_order_no,
                                                L_add_to_po_tbl(idx).I_import_country_id,
                                                L_add_to_po_tbl(idx).I_item) = FALSE then
                      return FALSE;
                  end if;
               END LOOP;  -- C_HTS_CHAPTER LOOP
            end if;
         elsif nvl(L_ref_item,'x') != nvl(L_add_to_po_tbl(idx).I_ref_item,'x') then
            update ordsku
               set ref_item = L_add_to_po_tbl(idx).I_ref_item
             where item     = L_add_to_po_tbl(idx).I_item
               and order_no = L_add_to_po_tbl(idx).I_order_no;
         end if;  -- L_exists = 'N'
      end if;  -- I_item != I_previous_item
   END LOOP;
   ---

   FOR idx in L_add_to_po_tbl.first..L_add_to_po_tbl.last LOOP
      /* The unit cost may be different from the supplier's unit cost if called */
      /* by the BUYERWKSHT.  Set the cost source appropriately for the insert or update. */
      if L_add_to_po_tbl(idx).I_unit_cost is NOT NULL and L_add_to_po_tbl(idx).I_unit_cost != L_add_to_po_tbl(idx).I_supp_unit_cost then
         L_cost_source := 'MANL';
      else
         L_cost_source := 'NORM';
      end if;

      -- reset the value of unit cost and cost source variable
      L_ord_unit_cost   := NULL;
      L_ord_cost_source := NULL;

      L_table := 'ORDLOC';
      open C_CHECK_ORDLOC(L_add_to_po_tbl(idx).I_order_no,
                          L_add_to_po_tbl(idx).I_item,
                          L_add_to_po_tbl(idx).I_location);
      fetch C_CHECK_ORDLOC into L_ord_cost_source,
                                L_ord_unit_cost;
      close C_CHECK_ORDLOC;
      ---
      if L_ord_unit_cost is NULL then   /* An ordloc record does not exist */
         L_unit_cost := L_add_to_po_tbl(idx).I_unit_cost;
         if L_unit_cost is NULL then
            if SUPP_ITEM_SQL.GET_COST(O_error_message,
                                      L_unit_cost,
                                      L_add_to_po_tbl(idx).I_item,
                                      L_add_to_po_tbl(idx).I_supplier,
                                      L_add_to_po_tbl(idx).I_origin_country_id,
                                      L_add_to_po_tbl(idx).I_location) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_add_to_po_tbl(idx).I_pack_ind = 'N' then
            if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                        L_add_to_po_tbl(idx).I_item,
                                                        L_add_to_po_tbl(idx).I_location,
                                                        L_add_to_po_tbl(idx).I_loc_type,
                                                        L_av_cost,
                                                        L_unit_cost_loc,
                                                        L_unit_retail,
                                                        L_selling_unit_retail,
                                                        L_selling_uom) = FALSE then
                return FALSE;
            end if;

            if L_add_to_po_tbl(idx).I_item_xform_ind IS NULL or L_add_to_po_tbl(idx).I_sellable_ind IS NULL then
               if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                                  L_item_master,
                                                  L_add_to_po_tbl(idx).I_item) = FALSE then
                  return FALSE;
               end if;
            else
               L_item_master.item_xform_ind := L_add_to_po_tbl(idx).I_item_xform_ind;
               L_item_master.sellable_ind := L_add_to_po_tbl(idx).I_sellable_ind;
            end if;

            if L_item_master.item_xform_ind = 'Y' and L_item_master.sellable_ind = 'N' then
               if ITEM_XFORM_SQL.CALCULATE_RETAIL(O_error_message,
                                                  L_add_to_po_tbl(idx).I_item,
                                                  L_add_to_po_tbl(idx).I_location,
                                                  L_unit_retail) = FALSE then
                  return FALSE;
               end if;
               -- if primary currency is not equal to location currency, convert primary currency to
               -- location currency.
               SQL_LIB.SET_MARK('OPEN',
                                'C_GET_SYSTEM_CURRENCY',
                                'SYSTEM_OPTIONS',
                                NULL);

               open C_GET_SYSTEM_CURRENCY;

               SQL_LIB.SET_MARK('FETCH',
                                'C_GET_SYSTEM_CURRENCY',
                                'SYSTEM_OPTIONS',
                                NULL);

               fetch C_GET_SYSTEM_CURRENCY into L_system_currency;

               SQL_LIB.SET_MARK('CLOSE',
                                'C_GET_SYSTEM_CURRENCY',
                                'SYSTEM_OPTIONS',
                                NULL);

               close C_GET_SYSTEM_CURRENCY;

               if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                            L_add_to_po_tbl(idx).I_location,
                                            L_add_to_po_tbl(idx).I_loc_type,
                                            NULL,
                                            L_loc_currency) = FALSE then
                  return FALSE;
               end if;

               if L_system_currency != L_loc_currency then
                  if CURRENCY_SQL.CONVERT(O_error_message,
                                          L_unit_retail,
                                          L_system_currency,
                                          L_loc_currency,
                                          L_unit_retail,
                                          'C',
                                          NULL,
                                          NULL,
                                          NULL,
                                          L_add_to_po_tbl(idx).I_ord_exchg_rate) = FALSE then
                     return FALSE;
                  end if;
               end if;

            elsif L_item_master.item_xform_ind = 'N' and L_item_master.sellable_ind = 'N' then
                L_unit_retail := nvl(L_unit_retail, 0);
            end if;
         else  -- The item is a pack_item
            if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                             L_pack_ind,
                                             L_sellable_ind,
                                             L_orderable_ind,
                                             L_pack_type,
                                             L_add_to_po_tbl(idx).I_item) = FALSE then
               return FALSE;
            end if;
            ---
            -- If it is a sellable vendor pack, retrieve the unit retail
            -- for the pack directly from the item_zone_price table
            ---
            if L_pack_type = 'V' and L_sellable_ind = 'Y' then
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                           L_add_to_po_tbl(idx).I_item,
                                                           L_add_to_po_tbl(idx).I_location,
                                                           L_add_to_po_tbl(idx).I_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost_loc,
                                                           L_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                   return FALSE;
                end if;
            else
               -- If it is not a sellable vendor pack, then sum up the unit retails of each
               -- component sku to get the unit retail for the pack
               ---
               if PRICING_ATTRIB_SQL.BUILD_PACK_RETAIL(O_error_message,
                                                       L_unit_retail,
                                                       L_add_to_po_tbl(idx).I_item,
                                                       L_add_to_po_tbl(idx).I_loc_type,
                                                       L_add_to_po_tbl(idx).I_location) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;   --- I_pack_ind = 'N'
         ---
         -- if I_contract_no is not NULL, call CONTRACT_SQL.GET_UNIT_COST
         ---
         if L_add_to_po_tbl(idx).I_contract_no is NOT NULL then
            if CONTRACT_SQL.GET_UNIT_COST(O_error_message,
                                          L_add_to_po_tbl(idx).I_item,
                                          L_add_to_po_tbl(idx).I_contract_no,
                                          L_unit_cost) = FALSE then
               return FALSE;
            end if;
            ---
            if L_add_to_po_tbl(idx).I_ord_currency != L_add_to_po_tbl(idx).I_contract_currency then
               if CURRENCY_SQL.CONVERT (O_error_message,
                                        L_unit_cost,
                                        L_add_to_po_tbl(idx).I_contract_currency,
                                        L_add_to_po_tbl(idx).I_ord_currency,
                                        L_unit_cost,
                                        'C',
                                        NULL,
                                        NULL,
                                        NULL,
                                        L_add_to_po_tbl(idx).I_ord_exchg_rate) = FALSE then
                  return FALSE;
               end if;
            end if;
         else
            if L_add_to_po_tbl(idx).I_ord_currency != L_add_to_po_tbl(idx).I_supplier_currency then
               if CURRENCY_SQL.CONVERT (O_error_message,
                                        L_unit_cost,
                                        L_add_to_po_tbl(idx).I_supplier_currency,
                                        L_add_to_po_tbl(idx).I_ord_currency,
                                        L_unit_cost,
                                        'C',
                                        NULL,
                                        NULL,
                                        NULL,
                                        L_add_to_po_tbl(idx).I_ord_exchg_rate) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
         ---
         if L_cost_source = 'NORM' and L_add_to_po_tbl(idx).I_calling_form != 'BUYERWKSHT' then
            L_unit_cost_init := L_unit_cost;
         else
            L_unit_cost_init := NULL;
         end if;
         ---
         if L_add_to_po_tbl(idx).I_calling_form = 'BUYERWKSHT' then
            /* If I_calling_form is BUYERWKSHT, the location to be added is a */
            /* warehouse and the worksheet item is not an Investment Buy line item, */
            /* check if a linked location on the order is an investment buy */
            /* warehouse.  If so, the non_scale_ind must be 'Y'. */
            L_non_scale_ind := L_add_to_po_tbl(idx).I_non_scale_ind;

            if L_add_to_po_tbl(idx).I_loc_type = 'W' then
               if L_add_to_po_tbl(idx).I_source_type != 'I' then
                  ---
                  open C_IB_WH_EXISTS(L_add_to_po_tbl(idx).I_order_no,
                                      L_add_to_po_tbl(idx).I_item,
                                      L_add_to_po_tbl(idx).I_location);
                  fetch C_IB_WH_EXISTS into L_dummy;
                  ---
                  if C_IB_WH_EXISTS%FOUND then
                     L_non_scale_ind := 'Y';
                  end if;
                  ---
                  close C_IB_WH_EXISTS;
               end if;
            end if;
         else
            /* if I_calling_form is ORDMTXWS, set the cost source appropriately */
            if L_add_to_po_tbl(idx).I_contract_no is NULL then
               L_cost_source := 'NORM';
            else
               L_cost_source := 'CONT';
            end if;
         end if;

         if ORDER_INSTOCK_DATE_SQL.GET_INSTOCK_DT(O_error_message,
                                                  L_estimated_instock_date,
                                                  L_add_to_po_tbl(idx).I_order_no,
                                                  L_add_to_po_tbl(idx).I_item,
                                                  L_add_to_po_tbl(idx).I_location,
                                                  L_add_to_po_tbl(idx).I_supplier) = FALSE then
            return FALSE;
         end if;

         insert into ordloc (order_no,
                             item,
                             location,
                             loc_type,
                             unit_retail,
                             qty_ordered,
                             qty_prescaled,
                             qty_received,
                             last_received,
                             last_rounded_qty,
                             last_grp_rounded_qty,
                             qty_cancelled,
                             cancel_code,
                             cancel_date,
                             cancel_id,
                             original_repl_qty,
                             unit_cost,
                             unit_cost_init,
                             cost_source,
                             non_scale_ind,
                             tsf_po_link_no,
                             estimated_instock_date)
                     values (L_add_to_po_tbl(idx).I_order_no,
                             L_add_to_po_tbl(idx).I_item,
                             L_add_to_po_tbl(idx).I_location,
                             L_add_to_po_tbl(idx).I_loc_type,
                             L_unit_retail,
                             L_add_to_po_tbl(idx).I_act_qty,     -- qty ordered
                             L_add_to_po_tbl(idx).I_act_qty,     -- prescaled qty
                             NULL,
                             NULL,
                             L_add_to_po_tbl(idx).I_last_rounded_qty,
                             L_add_to_po_tbl(idx).I_last_grp_rounded_qty,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             DECODE(L_add_to_po_tbl(idx).I_calling_form, 'BUYERWKSHT', L_add_to_po_tbl(idx).I_act_qty, NULL),
                             L_unit_cost,
                             L_unit_cost_init,
                             L_cost_source,
                             DECODE(L_add_to_po_tbl(idx).I_calling_form, 'BUYERWKSHT', L_non_scale_ind, 'N'),
                             L_add_to_po_tbl(idx).I_tsf_po_link_no,
                             L_estimated_instock_date);

         /* If I_calling_form is BUYERWKSHT and the worksheet line item is an */
         /* Investment Buy line item, update the non_scale_ind of the linked locations to 'Y'. */
         if L_add_to_po_tbl(idx).I_calling_form = 'BUYERWKSHT' then
            if L_add_to_po_tbl(idx).I_loc_type = 'W' then
               if L_add_to_po_tbl(idx).I_source_type = 'I' then
                  L_table := 'ORDLOC, WH';
                  open C_LOCK_ORDLOC_WH(L_add_to_po_tbl(idx).I_order_no,
                                        L_add_to_po_tbl(idx).I_item,
                                        L_add_to_po_tbl(idx).I_location);
                  close C_LOCK_ORDLOC_WH;

                  update ordloc ol
                     set non_scale_ind = 'Y'
                   where ol.order_no = L_add_to_po_tbl(idx).I_order_no
                     and ol.item = L_add_to_po_tbl(idx).I_item
                     and exists (select 'x'
                                   from wh w1, wh w2
                                  where w1.wh = ol.location
                                    and w1.repl_wh_link = w2.repl_wh_link
                                    and w2.wh = L_add_to_po_tbl(idx).I_location);
               end if;
            end if;
         end if;

         if L_add_to_po_tbl(idx).I_elc_ind = 'Y' then
            ---
            -- default expenses and recalculate
            ---
           if ORDER_EXPENSE_SQL.INSERT_COST_COMP(O_error_message     => O_error_message,
                                                 I_order_no          => L_add_to_po_tbl(idx).I_order_no,
                                                 I_item              => L_add_to_po_tbl(idx).I_item,
                                                 I_origin_country_id => NULL,
                                                 I_component_item    => NULL,  --- I_component_sku (for packs only)
                                                 I_location          => L_add_to_po_tbl(idx).I_location,
                                                 I_loc_type          => L_add_to_po_tbl(idx).I_loc_type,
                                                 I_backhaul_only_ind => 'N',
                                                 I_import_ord_ind    => L_add_to_po_tbl(idx).I_ord_import_ind) = FALSE then
               return FALSE;
            end if;
         end if;
      else   --- update ordloc
         if L_add_to_po_tbl(idx).I_calling_form = 'BUYERWKSHT' then
            L_unit_cost := L_add_to_po_tbl(idx).I_unit_cost;

            if L_add_to_po_tbl(idx).I_ord_currency != L_add_to_po_tbl(idx).I_supplier_currency then
               if CURRENCY_SQL.CONVERT (O_error_message,
                                        L_unit_cost,
                                        L_add_to_po_tbl(idx).I_supplier_currency,
                                        L_add_to_po_tbl(idx).I_ord_currency,
                                        L_unit_cost,
                                        'C',
                                        NULL,
                                        NULL,
                                        NULL,
                                        L_add_to_po_tbl(idx).I_ord_exchg_rate) = FALSE then
                  return FALSE;
               end if;
            end if;

            /* If the BUYERWKSHT's cost source is 'MANL' and the order's cost source */
            /* is 'MANL' then the lesser of the two costs takes precedence.  If the cost */
            /* source is 'MANL' and the order's cost source is not 'MANL', the order/item/loc's */
            /* cost source and unit cost will be updated to the worksheet's cost source */
            /* ('MANL') and unit cost. */
            /* If the BUYERWKSHT'S cost source is 'NORM' then the order/item/loc's cost */
            /* source and unit cost stay the same. */

            if L_cost_source = 'MANL' then
               if L_ord_cost_source = 'MANL' and L_ord_unit_cost < L_unit_cost then
                  L_unit_cost := L_ord_unit_cost;
               end if;
            else
               L_cost_source := L_ord_cost_source;
               L_unit_cost   := L_ord_unit_cost;
            end if;
         end if;
         ---
         update ordloc
            set qty_ordered   = qty_ordered + L_add_to_po_tbl(idx).I_act_qty,
                qty_prescaled = qty_prescaled + L_add_to_po_tbl(idx).I_act_qty,
                last_rounded_qty = last_rounded_qty + L_add_to_po_tbl(idx).I_last_rounded_qty,
                last_grp_rounded_qty = last_grp_rounded_qty + L_add_to_po_tbl(idx).I_last_grp_rounded_qty,
                original_repl_qty = DECODE(L_add_to_po_tbl(idx).I_calling_form, 'BUYERWKSHT', NVL(original_repl_qty + L_add_to_po_tbl(idx).I_act_qty, L_add_to_po_tbl(idx).I_act_qty), original_repl_qty),
                unit_cost = DECODE(L_add_to_po_tbl(idx).I_calling_form, 'BUYERWKSHT', L_unit_cost, unit_cost),
                cost_source = DECODE(L_add_to_po_tbl(idx).I_calling_form, 'BUYERWKSHT', L_cost_source, cost_source),
                non_scale_ind = DECODE(L_add_to_po_tbl(idx).I_source_type, 'I', 'Y', non_scale_ind)
          where order_no = L_add_to_po_tbl(idx).I_order_no
            and item     = L_add_to_po_tbl(idx).I_item
            and location = L_add_to_po_tbl(idx).I_location;

      end if;
        -- Recalculate landed cost components for HTS for previous item.
        -- Since HTS is at item level, only recalculate
        -- once all locations have been processed for the item.
        ---
       if  O_recalc_hts = 'Y' and L_add_to_po_tbl(idx).I_item != L_add_to_po_tbl(idx).I_previous_item then
         ---
         /* If the calling form is ORDMTXWS then check if the item is a */
         /* buyer pack.  If the calling form is BUYERWKSHT then the check */
         /* does not need to be performed as the BUYERWKSHT does not handle */
         /* buyer packs. */
         if L_add_to_po_tbl(idx).I_calling_form = 'ORDMTXWS' then
            if L_add_to_po_tbl(idx).I_pack_ind = 'Y' and L_pack_type = 'B' then
               L_pack_item  :=  L_add_to_po_tbl(idx).I_item;
               L_item       :=  NULL;
            else
               L_item       :=  L_add_to_po_tbl(idx).I_item;
               L_pack_item  :=  NULL;
            end if;
         else
            L_item := L_add_to_po_tbl(idx).I_item;
            L_pack_item := NULL;
         end if;
         ---
         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PA',
                                   L_item,
                                   NULL,
                                   NULL,
                                   NULL,
                                   L_add_to_po_tbl(idx).I_order_no,
                                   NULL,
                                   L_pack_item,
                                   NULL,
                                   NULL,
                                   L_add_to_po_tbl(idx).I_import_country_id,
                                   L_add_to_po_tbl(idx).I_origin_country_id,
                                   NULL,
                                   NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PE',
                                   L_item,
                                   NULL,
                                   NULL,
                                   NULL,
                                   L_add_to_po_tbl(idx).I_order_no,
                                   NULL,
                                   L_pack_item,
                                   NULL,
                                   NULL,
                                   L_add_to_po_tbl(idx).I_import_country_id,
                                   L_add_to_po_tbl(idx).I_origin_country_id,
                                   NULL,
                                   NULL) = FALSE then
            return FALSE;
         end if;
      end if;   -- recalculate HTS item
   END LOOP;

   if not ORDER_ATTRIB_SQL.PREV_APPROVED(O_error_message,
                                         L_prev_approved,
                                         L_add_to_po_tbl(1).I_order_no) then
      return FALSE;
   end if;

   if L_prev_approved = 'Y' then

      if not ORDER_SETUP_SQL.REDIST_CANCEL(O_error_message,
                                            L_add_to_po_tbl(1).I_order_no) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             TO_CHAR(L_add_to_po_tbl(1).I_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ADD_ITEM_TO_PO;
-------------------------------------------------------------------------------
FUNCTION LOCK_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                     I_item            IN       ITEM_MASTER.ITEM%TYPE,
                     I_location        IN       ORDLOC.LOCATION%TYPE,
                     I_calling_form    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_check_ordloc    NUMBER(12)        := 0;
   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);
   L_function        VARCHAR2(50)  := 'ORDER_SETUP_SQL.LOCK_DETAIL';

   cursor C_LOCK_ORDLOC_WKSHT is
      select 'x'
        from ordloc_wksht
       where order_no        = I_order_no
         and (item           = NVL(I_item, item)
              or item_parent = NVL(I_item, item_parent))
         and location        = NVL(I_location, location)
         for update nowait;

   cursor C_CHECK_ORDLOC is
      select count(*)
        from ordloc
       where order_no = I_order_no
         and item     = I_item
         and location = I_location;

   cursor C_LOCK_ALLOC_CHRG is
      select 'x'
        from alloc_chrg
       where alloc_no in (select alloc_no
                            from alloc_header ah,
                                 item_master im
                           where ah.order_no = I_order_no
                             and ah.item     = im.item
                             and (im.item                = NVL(I_item, im.item)
                                  or im.item_parent      = NVL(I_item, im.item_parent)
                                  or im.item_grandparent = NVL(I_item, im.item_grandparent))
                             and ah.wh       = NVL(I_location, wh))
         for update nowait;

   cursor C_LOCK_ALLOC_DETAIL is
      select 'x'
        from alloc_detail
       where alloc_no in (select alloc_no
                            from alloc_header ah,
                                 item_master im
                           where ah.order_no = I_order_no
                             and ah.item     = im.item
                             and (im.item                = NVL(I_item, im.item)
                                  or im.item_parent      = NVL(I_item, im.item_parent)
                                  or im.item_grandparent = NVL(I_item, im.item_grandparent))
                             and ah.wh       = NVL(I_location, wh))
         for update nowait;

   cursor C_LOCK_ALLOC_HEADER is
      select 'x'
        from alloc_header ah
       where ah.order_no = I_order_no
         and exists (select 1
                       from item_master im
                      where ah.item     = im.item
                        and (im.item                = NVL(I_item, im.item)
                             or im.item_parent      = NVL(I_item, im.item_parent)
                             or im.item_grandparent = NVL(I_item, im.item_grandparent)))
         and ah.wh       = NVL(I_location, wh)
         for update nowait;

   cursor C_LOCK_ORDLOC_REV is
      select 'x'
        from ordloc_rev
       where order_no = I_order_no
         and item     = NVL(I_item, item)
         and location = NVL(I_location, location)
         for update nowait;

   cursor C_LOCK_ORDLOC is
      select 'x'
        from ordloc ol
       where ol.order_no = I_order_no
         and exists (select 1
                       from item_master im
                      where (im.item                = NVL(I_item, im.item)
                             or im.item_parent      = NVL(I_item, im.item_parent)
                             or im.item_grandparent = NVL(I_item, im.item_grandparent))
                        and ol.item     = im.item)
         and location    = NVL(I_location, location)
         for update nowait;

   cursor C_LOCK_ORDSKU_REV is
      select 'x'
        from ordsku_rev
       where order_no = I_order_no
         and item     = NVL(I_item, item)
         for update nowait;

   cursor C_LOCK_ORDLOC_DISCOUNT is
      select 'x'
        from ordloc_discount od
       where od.order_no = I_order_no
         and exists (select 1
                       from item_master im
                      where (im.item                = NVL(I_item, im.item)
                             or im.item_parent      = NVL(I_item, im.item)
                             or im.item_grandparent = NVL(I_item, im.item))
                        and od.item     = im.item)
         for update nowait;

   cursor C_LOCK_ORDSKU is
      select 'x'
        from ordsku os
       where os.order_no = I_order_no
         and exists (select 1
                       from item_master im
                      where (im.item                = NVL(I_item, im.item)
                             or im.item_parent      = NVL(I_item, im.item_parent)
                             or im.item_grandparent = NVL(I_item, im.item_grandparent))
                        and os.item     = im.item)
         for update nowait;

   cursor C_LOCK_ORDCUST is
      select 'x'
        from ordcust
       where order_no = I_order_no
         for update nowait;

   cursor C_LOCK_ORDHEAD_REV is
      select 'x'
        from ordhead_rev
       where order_no = I_order_no
         for update nowait;

   cursor C_LOCK_REQ_DOC is
      select 'x'
         from req_doc
        where module      = 'PO'
          and key_value_1 = to_char(I_order_no)
          and key_value_2 is NULL
          for update nowait;

   cursor C_LOCK_TIMELINE is
      select 'x'
         from timeline
        where timeline_type = 'PO'
          and key_value_1   = to_char(I_order_no)
          and key_value_2 is NULL
          for update nowait;

   cursor C_LOCK_REQ_DOC_POIT is
      select 'x'
         from req_doc rd
        where module      = 'POIT'
          and key_value_1 = to_char(I_order_no)
          and exists (select 1
                        from item_master im
                       where im.item     = rd.key_value_2
                         and (im.item                = NVL(I_item, im.item)
                              or im.item_parent      = NVL(I_item, im.item_parent)
                              or im.item_grandparent = NVL(I_item, im.item_grandparent)))
          for update nowait;

   cursor C_LOCK_TIMELINE_POIT is
      select 'x'
         from timeline
        where timeline_type = 'POIT'
          and key_value_1   = to_char(I_order_no)
          and exists (select 1
                        from item_master im
                       where im.item       = key_value_2
                         and (im.item                = NVL(I_item, im.item)
                              or im.item_parent      = NVL(I_item, im.item_parent)
                              or im.item_grandparent = NVL(I_item, im.item_grandparent)))
          for update nowait;


   cursor C_LOCK_ORDLOC_EXP is
      select 'x'
         from ordloc_exp oe
        where oe.order_no = I_order_no
          and exists (select 1
                        from item_master im
                       where (im.item                = NVL(I_item, im.item)
                              or im.item_parent      = NVL(I_item, im.item_parent)
                              or im.item_grandparent = NVL(I_item, im.item_grandparent))
                         and im.item     = oe.item)
      UNION ALL
      select 'x'
        from ordloc_exp oe
       where oe.order_no  = I_order_no
         and exists (select 1
                       from item_master im
                      where (im.item                = NVL(I_item, im.item)
                             or im.item_parent      = NVL(I_item, im.item_parent)
                             or im.item_grandparent = NVL(I_item, im.item_grandparent))
                        and oe.pack_item = im.item);

   cursor C_LOCK_ORDSKU_HTS_ASSESS is
      select 'x'
         from ordsku_hts_assess
        where order_no = I_order_no
          and seq_no in (select seq_no
                           from ordsku_hts oh,
                                item_master im
                          where oh.order_no = I_order_no
                            and (im.item = NVL(I_item, im.item)
                             or im.item_parent = NVL(I_item, im.item_parent)
                             or im.item_grandparent = NVL(I_item, im.item_grandparent))
                            and oh.item = im.item
                            and oh.pack_item is NULL)
          for update nowait;

   cursor C_LOCK_ORDSKU_HTS_ASSESS_PACK is
      select 'x'
         from ordsku_hts_assess
        where order_no = I_order_no
          and seq_no in (select seq_no
                           from ordsku_hts oh,
                                item_master im
                          where order_no  = I_order_no
                            and (im.item = NVL(I_item, im.item)
                             or im.item_parent = NVL(I_item, im.item_parent)
                             or im.item_grandparent = NVL(I_item, im.item_grandparent))
                            and oh.pack_item = im.item)
          for update nowait;

   cursor C_LOCK_ORDSKU_HTS is
      select 'x'
         from ordsku_hts oh
        where oh.order_no = I_order_no
          and exists (select 1
                        from item_master im
                       where (im.item = NVL(I_item, im.item)
                              or im.item_parent = NVL(I_item, im.item_parent)
                              or im.item_grandparent = NVL(I_item, im.item_grandparent))
                         and oh.item = im.item)
          and oh.pack_item is NULL
          for update nowait;

   cursor C_LOCK_ORDSKU_HTS_PACK is
      select 'x'
        from ordsku_hts oh
       where order_no  = I_order_no
         and exists (select 1
                       from item_master im
                      where (im.item = NVL(I_item, im.item)
                             or im.item_parent = NVL(I_item, im.item_parent)
                             or im.item_grandparent = NVL(I_item, im.item_grandparent))
                        and oh.pack_item = im.item)
         for update nowait;

   cursor C_LOCK_ORDLC is
      select 'x'
        from ordlc
       where order_no = I_order_no
         for update nowait;
   ---
   cursor C_LOCK_DEAL_HEAD is
      select 'x'
        from deal_head
       where order_no = I_order_no
         for update nowait;

   cursor C_LOCK_DEAL_DETAIL is
      select 'x'
        from deal_detail
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no)
         for update nowait;

   cursor C_LOCK_DEAL_CALC_QUEUE is
      select 'x'
        from deal_calc_queue
       where order_no = I_order_no
         for update nowait;

   cursor C_LOCK_DEAL_QUEUE is
      select 'x'
        from deal_queue
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no)
         for update nowait;

   cursor C_LOCK_DEAL_THRESHOLD is
      select 'x'
        from deal_threshold
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no)
         for update nowait;

   cursor C_LOCK_DEAL_ITEMLOC_DIV_GRP is
      select 'x'
        from deal_itemloc_div_grp
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no)
         for update nowait;

   cursor C_LOCK_DEAL_ITEMLOC_DCS is
      select 'x'
        from deal_itemloc_dcs
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no)
         for update nowait;

   cursor C_LOCK_DEAL_ITEMLOC_ITEM is
      select 'x'
        from deal_itemloc_item
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no)
         for update nowait;

   cursor C_LOCK_DEAL_ITEMLOC_DIFF is
      select 'x'
        from deal_itemloc_parent_diff
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no)
         and item_parent in (select item
                               from item_master
                              where (   item_parent = I_item
                                     or item_grandparent = I_item
                                     or item = I_item))
         for update nowait;

   cursor C_LOCK_REV_ORDERS is
      select 'x'
        from rev_orders
       where order_no = I_order_no
         for update nowait;

      cursor C_LOCK_ORD_INV_MGMT is
         select 'x'
           from ord_inv_mgmt
          where order_no = I_order_no
            for update nowait;

BEGIN
    if (I_calling_form not in ('ordlist','ordloc','ordsku','ordhead','split_po',
        'ordredal','ordredit','ordmtxws','temp')) then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   if I_calling_form in ('ordloc','ordsku','split_po','ordhead', 'ordredal') then
      L_table := 'ALLOC_CHRG';
      open  C_LOCK_ALLOC_CHRG;
      close C_LOCK_ALLOC_CHRG;

      L_table := 'ALLOC_DETAIL';
      open  C_LOCK_ALLOC_DETAIL;
      close C_LOCK_ALLOC_DETAIL;

      L_table := 'ALLOC_HEADER';
      open  C_LOCK_ALLOC_HEADER;
      close C_LOCK_ALLOC_HEADER;
   end if;
   ---
   if I_calling_form = 'ordloc' then
      open  C_CHECK_ORDLOC;
      fetch C_CHECK_ORDLOC into L_check_ordloc;
      close C_CHECK_ORDLOC;
   end if;
   ---
   if L_check_ordloc = 1 or I_calling_form not in ('ordloc', 'ordredal') then
      L_table := 'ORDLOC_DISCOUNT';
      open  C_LOCK_ORDLOC_DISCOUNT;
      close C_LOCK_ORDLOC_DISCOUNT;

      if I_calling_form != 'ordredit' then
         L_table := 'ORDSKU_REV';
         open  C_LOCK_ORDSKU_REV;
         close C_LOCK_ORDSKU_REV;
      end if;

      L_table := 'REQ_DOC';
      open  C_LOCK_REQ_DOC_POIT;
      close C_LOCK_REQ_DOC_POIT;

      L_table := 'TIMELINE';
      open  C_LOCK_TIMELINE_POIT;
      close C_LOCK_TIMELINE_POIT;

      L_table := 'ORDSKU_EXP';
      open  C_LOCK_ORDLOC_EXP;
      close C_LOCK_ORDLOC_EXP;

      L_table := 'ORDSKU_HTS_ASSESS';
      open  C_LOCK_ORDSKU_HTS_ASSESS;
      close C_LOCK_ORDSKU_HTS_ASSESS;

      L_table := 'ORDSKU_HTS_ASSESS';
      open  C_LOCK_ORDSKU_HTS_ASSESS_PACK;
      close C_LOCK_ORDSKU_HTS_ASSESS_PACK;

      L_table := 'ORDSKU_HTS';
      open  C_LOCK_ORDSKU_HTS;
      close C_LOCK_ORDSKU_HTS;

      L_table := 'ORDSKU_HTS';
      open  C_LOCK_ORDSKU_HTS_PACK;
      close C_LOCK_ORDSKU_HTS_PACK;
   end if;

   if I_calling_form in ('ordloc','split_po','ordhead', 'ordredit') then
      if L_check_ordloc = 1 or I_calling_form != 'ordloc' then
         L_table := 'ORDSKU';
         open  C_LOCK_ORDSKU;
         close C_LOCK_ORDSKU;
      end if;
   end if;

   if I_calling_form in ('ordsku','split_po','ordhead', 'ordredit') then
      L_table := 'ORDLOC';
      open  C_LOCK_ORDLOC;
      close C_LOCK_ORDLOC;
   end if;

   if I_calling_form in('ordhead','split_po') then

      L_table := 'ORDLOC_REV';
      open  C_LOCK_ORDLOC_REV;
      close C_LOCK_ORDLOC_REV;

      L_table := 'ORDLOC_WKSHT';
      open  C_LOCK_ORDLOC_WKSHT;
      close C_LOCK_ORDLOC_WKSHT;

      L_table := 'ORDCUST';
      open  C_LOCK_ORDCUST;
      close C_LOCK_ORDCUST;

      L_table := 'ORDHEAD_REV';
      open  C_LOCK_ORDHEAD_REV;
      close C_LOCK_ORDHEAD_REV;

      L_table := 'REQ_DOC';
      open  C_LOCK_REQ_DOC;
      close C_LOCK_REQ_DOC;

      L_table := 'TIMELINE';
      open  C_LOCK_TIMELINE;
      close C_LOCK_TIMELINE;

      L_table := 'ORDLC';
      open  C_LOCK_ORDLC;
      close C_LOCK_ORDLC;
      ---
      L_table := 'DEAL_ITEMLOC_DIV_GRP';
      open C_LOCK_DEAL_ITEMLOC_DIV_GRP;
      close C_LOCK_DEAL_ITEMLOC_DIV_GRP;

      L_table := 'DEAL_ITEMLOC_DCS';
      open C_LOCK_DEAL_ITEMLOC_DCS;
      close C_LOCK_DEAL_ITEMLOC_DCS;

      L_table := 'DEAL_ITEMLOC_ITEM';
      open C_LOCK_DEAL_ITEMLOC_ITEM;
      close C_LOCK_DEAL_ITEMLOC_ITEM;

      L_table := 'DEAL_ITEMLOC_PARENT_DIFF';
      open C_LOCK_DEAL_ITEMLOC_DIFF;
      close C_LOCK_DEAL_ITEMLOC_DIFF;

      L_table := 'DEAL_THRESHOLD';
      open C_LOCK_DEAL_THRESHOLD;
      close C_LOCK_DEAL_THRESHOLD;

      L_table := 'DEAL_DETAIL';
      open C_LOCK_DEAL_DETAIL;
      close C_LOCK_DEAL_DETAIL;

      L_table := 'DEAL_QUEUE';
      open C_LOCK_DEAL_QUEUE;
      close C_LOCK_DEAL_QUEUE;

      -- lock deal_calc_queue
      L_table := 'DEAL_CALC_QUEUE';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_DEAL_CALC_QUEUE',
                       'DEAL_CALC_QUEUE',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));
      open C_LOCK_DEAL_CALC_QUEUE;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_DEAL_CALC_QUEUE',
                       'DEAL_CALC_QUEUE',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));
      close C_LOCK_DEAL_CALC_QUEUE;

      L_table := 'DEAL_HEAD';
      open C_LOCK_DEAL_HEAD;
      close C_LOCK_DEAL_HEAD;

      L_table := 'REV_ORDERS';
      open C_LOCK_REV_ORDERS;
      close C_LOCK_REV_ORDERS;

      -- lock ord_inv_mgmt
      L_table := 'ORD_INV_MGMT';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ORD_INV_MGMT',
                       'ORD_INV_MGMT',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));
      open C_LOCK_ORD_INV_MGMT;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ORD_INV_MGMT',
                       'ORD_INV_MGMT',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));
      close C_LOCK_ORD_INV_MGMT;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             TO_CHAR(I_order_no),
                                             I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_DETAIL;
-------------------------------------------------------------------------------
FUNCTION DELETE_ORDSKU(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_function     VARCHAR2(50) := 'ORDER_SETUP_SQL.DELETE_ORDSKU';
   L_dummy        VARCHAR2(1)  := NULL;
   L_dtl_count    NUMBER(10);
   L_fpo_exists   VARCHAR2(1);

   cursor C_ORDLOC_SKU is
      select 'x'
        from ordloc
       where item = I_item
         and order_no = I_order_no;

   cursor C_CHK_FPO_exists is
      select 'x'
        from ordhead oh,
             wf_order_head woh
       where oh.order_no = I_order_no
         and oh.wf_order_no = woh.wf_order_no;

   cursor C_CHK_LAST_FPO_DTL is
      select count(wod.item)
        from ordsku os,
             ordhead oh,
             wf_order_detail wod,
             wf_order_head woh
       where os.order_no = I_order_no
         and os.item = wod.item
         and oh.order_no = os.order_no
         and oh.wf_order_no = woh.wf_order_no
         and oh.status = 'W' and oh.orig_approval_id is NULL
         and woh.status = 'I' and woh.approval_date is NULL
         and woh.wf_order_no = wod.wf_order_no
         and oh.supplier = wod.source_loc_id
         and wod.source_loc_type = 'SU';

BEGIN

   L_fpo_exists := NULL;
   open C_CHK_FPO_exists;
   fetch C_CHK_FPO_exists into L_fpo_exists;
   close C_CHK_FPO_exists;

   if L_fpo_exists is NOT NULL then
      L_dtl_count := NULL;
      open C_CHK_LAST_FPO_DTL;
      fetch C_CHK_LAST_FPO_DTL into L_dtl_count;
      close C_CHK_LAST_FPO_DTL;
   else
      L_dtl_count := 0;
   end if;
   if L_dtl_count <> 1 then
      open C_ORDLOC_SKU;
      fetch C_ORDLOC_SKU into L_dummy;
      if C_ORDLOC_SKU%NOTFOUND then
         ---
         if DOCUMENTS_SQL.DELETE_REQ_DOCS(O_error_message,
                                          'POIT',
                                          I_order_no,
                                          I_item) = FALSE then
            return FALSE;
         end if;
         ---
         if TIMELINE_SQL.DELETE_TIMELINES(O_error_message,
                                          'POIT',
                                          I_order_no,
                                          I_item) = FALSE then
            return FALSE;
         end if;
         ---
         if DELETE_COMPS(O_error_message,
                         I_order_no,
                         I_item,
                         NULL,
                         NULL,
                         'A',
                         NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if DELETE_SINGLE_CONTAINER_ITEM(O_error_message,
                                         I_order_no,
                                         I_item,
                                         NULL, -- Location
                                         'S' -- ordsku
                                         ) = FALSE then
            return FALSE;
         end if;
         ---
         delete from ordsku_cfa_ext
          where item     = I_item
            and order_no = I_order_no;
         --
         delete from ordsku
          where item     = I_item
            and order_no = I_order_no;
      end if;
      close C_ORDLOC_SKU;
   else
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_FPO_DTL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ORDSKU;
-------------------------------------------------------------------------------
FUNCTION DELETE_ORDSKU_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                            I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_table         VARCHAR2(30);
   L_function      VARCHAR2(50)  := 'ORDER_SETUP_SQL.DELETE_ORDSKU_ITEM';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_dtl_count     NUMBER(10);
   L_fpo_exists    VARCHAR2(1);

   cursor C_LOCK_ORDSKU_CFA is
      select 'x'
        from ordloc_cfa_ext
       where item = I_item
         and order_no = I_order_no
         for update of item nowait;

   cursor C_LOCK_ORDSKU is
      select 'x'
        from ordloc
       where item = I_item
         and order_no = I_order_no
         for update of item nowait;

   cursor C_CHK_FPO_exists is
      select 'x'
        from ordhead oh,
             wf_order_head woh
       where oh.order_no = I_order_no
         and oh.wf_order_no = woh.wf_order_no;

   cursor C_CHK_LAST_FPO_DTL is
      select count(wod.item)
        from ordsku os,
             ordhead oh,
             wf_order_detail wod,
             wf_order_head woh
       where os.order_no = I_order_no
         and os.item = wod.item
         and oh.order_no = os.order_no
         and oh.wf_order_no = woh.wf_order_no
         and oh.status = 'W' and oh.orig_approval_id is NULL
         and woh.status = 'I' and woh.approval_date is NULL
         and woh.wf_order_no = wod.wf_order_no
         and oh.supplier = wod.source_loc_id
         and wod.source_loc_type = 'SU';

BEGIN

   L_fpo_exists := NULL;
   open C_CHK_FPO_exists;
   fetch C_CHK_FPO_exists into L_fpo_exists;
   close C_CHK_FPO_exists;

   if L_fpo_exists is NOT NULL then
      L_dtl_count := NULL;
      open C_CHK_LAST_FPO_DTL;
      fetch C_CHK_LAST_FPO_DTL into L_dtl_count;
      close C_CHK_LAST_FPO_DTL;
   else
      L_dtl_count := 0;
   end if;
   if L_dtl_count <> 1 then

     L_table := 'ORDSKU_CFA_EXT';
     open C_LOCK_ORDSKU_CFA;
     close C_LOCK_ORDSKU_CFA;

     delete from ordsku_cfa_ext
         where item = I_item
            and order_no = I_order_no;

      L_table := 'ORDSKU';
      open C_LOCK_ORDSKU;
      close C_LOCK_ORDSKU;

      delete from ordsku
            where item = I_item
              and order_no = I_order_no;

   else
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_FPO_DTL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             TO_CHAR(I_order_no),
                                             I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ORDSKU_ITEM;
-------------------------------------------------------------------------------
FUNCTION DELETE_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                      I_item            IN       ITEM_MASTER.ITEM%TYPE,
                      I_location        IN       ORDLOC.LOCATION%TYPE,
                      I_loc_type        IN       ORDLOC.LOC_TYPE%TYPE,
                      I_calling_form    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ORDER_SETUP_SQL.DELETE_ORDER';

   L_table           VARCHAR2(30);
   L_prev_approved   VARCHAR2(1);
   L_physical_wh     WH.PHYSICAL_WH%TYPE;
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ORD_PREISSUE is
      select 'x'
        from ord_preissue
       where order_no = I_order_no
         for update nowait;

   cursor C_lock_order_ext_lock is
      select 'x'
        from ordhead_l10n_ext
       where order_no = I_order_no
         for update of order_no nowait;

   cursor C_GET_PWH_WO is
      select physical_wh
        from wh
       where wh = I_location;

   cursor C_LOCK_ORDHEAD_CE is
      select 'X'
        from ordhead_cfa_ext
       where order_no = I_order_no
         for update nowait ;

   cursor C_LOCK_ORDSKU_CFA is
      select 'X'
        from ordsku_cfa_ext
       where order_no = I_order_no
         and item     = I_item
         for update nowait ;

   cursor C_LOCK_ORDLOC_CFA is
      select 'X'
        from ordloc_cfa_ext
       where order_no = I_order_no
         and item     = I_item
         and location = I_location
         for update nowait ;

BEGIN
   if I_calling_form NOT in ('ordlist','ordloc','ordsku','ordhead','split_po',
      'ordredal','ordredit','undo','ordmtxws','temp', 'preissue', 'ordautcl') then

      O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   if I_calling_form = 'ordloc' then

      --Delete expense components for the item location (i.e., zone).
      if DELETE_COMPS(O_error_message,
                      I_order_no,
                      I_item,
                      NULL, -- I_component_sku (for packs only)
                      I_location,
                      I_loc_type,
                      'E',
                      NULL,
                      I_calling_form) = FALSE then
         return FALSE;
      end if;
      ---
      delete from ordloc_discount
       where order_no = I_order_no
         and item = I_item
         and location = I_location;
      ---
      if I_loc_type = 'W' then
         open C_GET_PWH_WO;
         fetch C_GET_PWH_WO into L_physical_wh;
         close C_GET_PWH_WO;
         ---
         if WO_SQL.DELETE_WO(O_error_message,
                             NULL,
                             I_order_no,
                             NULL,
                             I_item,
                             L_physical_wh) = FALSE then
            return FALSE;
         end if;
      else
         if WO_SQL.DELETE_WO(O_error_message,
                             NULL,
                             I_order_no,
                             NULL,
                             I_item,
                             I_location) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_loc_type = 'W' then
         delete from alloc_chrg
          where alloc_no in (select alloc_no
                               from alloc_header
                              where order_no = I_order_no
                                and item     = I_item
                                and wh       = I_location);

         delete from alloc_detail
          where alloc_no in (select alloc_no
                               from alloc_header
                              where order_no = I_order_no
                                and item     = I_item
                                and wh       = I_location);

         delete from alloc_header
          where order_no = I_order_no
            and item     = I_item
            and wh       = I_location;

      end if;
      --
      -- delete from ordloc_cfa_ext
      SQL_LIB.SET_MARK('DELETE',  NULL,
                       'ORDLOC_CFA_EXT',
                       'ORDER_NO: ' || TO_CHAR(I_order_no) ||
                       ' ITEM: '    || I_item ||
                       ' Location: '|| I_location);
      open C_LOCK_ORDLOC_CFA;
      close C_LOCK_ORDLOC_CFA;
      delete from ordloc_cfa_ext
       where order_no = I_order_no
         and item     = I_item
         and location = I_location;
      --

   elsif I_calling_form = 'ordsku' then

      --Delete HTS assessments and expense components for the item.
      if DELETE_COMPS(O_error_message,
                      I_order_no,
                      I_item,
                      NULL, -- I_component_sku (for packs only)
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      I_calling_form) = FALSE then
         return FALSE;
      end if;
      ---
      if WO_SQL.DELETE_WO(O_error_message,
                          NULL,
                          I_order_no,
                          NULL,
                          I_item,
                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      delete from alloc_chrg
       where alloc_no in (select alloc_no
                            from alloc_header
                           where order_no = I_order_no
                             and item     = I_item);

      delete from alloc_detail
       where alloc_no in (select alloc_no
                            from alloc_header
                           where order_no = I_order_no
                             and item     = I_item);

      delete from alloc_header
       where order_no = I_order_no
         and item = I_item;
      --
      -- delete from ordloc_cfa_ext
      SQL_LIB.SET_MARK('DELETE',  NULL,
                       'ORDLOC_CFA_EXT',
                       'ORDER_NO: ' || TO_CHAR(I_order_no) ||
                       ' ITEM: '    || I_item);
      open C_LOCK_ORDLOC_CFA;
      close C_LOCK_ORDLOC_CFA;
      delete from ordloc_cfa_ext
       where order_no = I_order_no
         and item     = I_item;
      --
      -- delete from ordloc
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'ORDLOC',
                       'ORDER_NO: ' || TO_CHAR(I_order_no) ||
                       ' ITEM: ' || I_item);
      delete from ordloc
       where order_no = I_order_no
         and item = I_item;

      delete from ordloc_discount
       where order_no = I_order_no
         and item = I_item;

      delete from ord_tax_breakup
       where order_no = I_order_no
         and item = I_item;

      delete from tax_calc_event
       where order_no = I_order_no;

      --Delete required documents for the PO item.
      if DOCUMENTS_SQL.DELETE_REQ_DOCS(O_error_message,
                                       'POIT',
                                       I_order_no,
                                       I_item) = FALSE then
         return FALSE;
      end if;

      --Delete timelines for the PO item.
      if TIMELINE_SQL.DELETE_TIMELINES(O_error_message,
                                       'POIT',
                                       I_order_no,
                                       I_item) = FALSE then
         return FALSE;
      end if;
      --
      -- delete from ordsku_cfa_ext
      SQL_LIB.SET_MARK('DELETE',  NULL,
                       'ORDSKU_CFA_EXT',
                       'ORDER_NO: ' || TO_CHAR(I_order_no) ||
                       ' ITEM: '    || I_item);
      open C_LOCK_ORDSKU_CFA;
      close C_LOCK_ORDSKU_CFA;
      delete from ordsku_cfa_ext
       where order_no = I_order_no
         and item     = I_item;
      --
   elsif I_calling_form in('ordhead','split_po','ordautcl') then

      ---
      if WO_SQL.DELETE_WO(O_error_message,
                          NULL,
                          I_order_no,
                          NULL,
                          NULL,
                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      --Delete HTS assessment and expense components for all the items on the order.
      if DELETE_COMPS(O_error_message,
                      I_order_no,
                      NULL,
                      NULL, -- I_component_sku (for packs only)
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      I_calling_form) = FALSE then
         return FALSE;
      end if;
      ---
      if NOT ORDER_SETUP_SQL.DELETE_TEMP_TABLES(O_error_message,
                                                I_order_no) then
         return FALSE;
      end if;
      ---
      delete from alloc_chrg
       where alloc_no in (select alloc_no
                            from alloc_header
                           where order_no = I_order_no);

      delete from alloc_detail
       where alloc_no in (select alloc_no
                            from alloc_header
                           where order_no = I_order_no);

      delete from alloc_header
       where order_no = I_order_no;

      delete from ordloc_wksht
       where order_no = I_order_no;

      delete from ordloc_rev
       where order_no = I_order_no;
      --
      -- delete from ordloc_cfa_ext
      SQL_LIB.SET_MARK('DELETE',  NULL,
                       'ORDLOC_CFA_EXT',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));
      open C_LOCK_ORDLOC_CFA;
      close C_LOCK_ORDLOC_CFA;
      delete from ordloc_cfa_ext
       where order_no = I_order_no;
      --
      delete from ordloc
       where order_no = I_order_no;

      delete from ord_tax_breakup
       where order_no = I_order_no;

      delete from tax_calc_event
       where order_no = I_order_no;

      delete from ordloc_discount
       where order_no = I_order_no;

      --Delete all the item level required documents on the order.
      if DOCUMENTS_SQL.DELETE_REQ_DOCS(O_error_message,
                                       'POIT',
                                       I_order_no,
                                       NULL) = FALSE then
         return FALSE;
      end if;

      ---Delete all the item level timelines on the order.
      if TIMELINE_SQL.DELETE_TIMELINES(O_error_message,
                                       'POIT',
                                       I_order_no,
                                       NULL) = FALSE then
         return FALSE;
      end if;

      --Delete all the PO level required documents.
      if DOCUMENTS_SQL.DELETE_REQ_DOCS(O_error_message,
                                       'PO',
                                       I_order_no,
                                       NULL) = FALSE then
         return FALSE;
      end if;

       --Delete all the PO level timelines.
      if TIMELINE_SQL.DELETE_TIMELINES(O_error_message,
                                       'PO',
                                       I_order_no,
                                       NULL) = FALSE then
         return FALSE;
      end if;

      delete from ordsku_rev
       where order_no = I_order_no;
      --
      -- delete from ordsku_cfa_ext
      SQL_LIB.SET_MARK('DELETE',  NULL,
                       'ORDSKU_CFA_EXT',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));
      open C_LOCK_ORDSKU_CFA;
      close C_LOCK_ORDSKU_CFA;
      delete from ordsku_cfa_ext
       where order_no = I_order_no;
      --
      -- delete from ordsku
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'ORDSKU',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));

      delete from ordsku
       where order_no = I_order_no;

      -- Lock ordhead_cfa_ext
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ORDHEAD_CE', 'ORDHEAD_CFA_EXT', 'ORDER_NO: ' || I_order_no);
      open C_LOCK_ORDHEAD_CE;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDHEAD_CE', 'ORDHEAD_CFA_EXT', 'ORDER_NO: ' || I_order_no);
      close C_LOCK_ORDHEAD_CE;

      L_table := 'ORDHEAD_CFA_EXT';
      -- delete from ordhead_cfa_ext
      SQL_LIB.SET_MARK('DELETE',
                        NULL,
                       'ORDHEAD_CFA_EXT',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));

      delete from ordhead_cfa_ext
       where order_no = I_order_no;

      open C_LOCK_ORDER_EXT_LOCK;
      close C_LOCK_ORDER_EXT_LOCK;
      delete from ordhead_l10n_ext
       where order_no = I_order_no;

      delete from ordcust_pub_info
       where ordcust_no in (select ordcust_no
                              from ordcust
                             where order_no = I_order_no);

      delete from ordcust_detail
       where ordcust_no in (select ordcust_no
                              from ordcust
                             where order_no = I_order_no);

      delete from ordcust
       where order_no = I_order_no;

      delete from ordhead_rev
       where order_no = I_order_no;

      delete from ordlc
       where order_no = I_order_no;

      --- Delete all PO-specific deals
      delete from deal_comp_prom
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no);

      delete from deal_itemloc_div_grp
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no);

      delete from deal_itemloc_dcs
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no);

      delete from deal_itemloc_item
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no);

      delete from deal_itemloc_parent_diff
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no);

      delete from deal_threshold
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no);

      -- delete from deal_item_loc_explode
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'DEAL_ITEM_LOC_EXPLODE',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));
      delete from deal_item_loc_explode
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no);

      delete from deal_detail
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no);

      delete from deal_queue
       where deal_id in (select deal_id
                           from deal_head
                          where order_no = I_order_no);

      -- delete from deal_calc_queue
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'DEAL_CALC_QUEUE',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));
      delete from deal_calc_queue
       where order_no = I_order_no;

      -- delete from deal_head_cfa_ext
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'DEAL_HEAD_CFA_EXT',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));

      delete from deal_head_cfa_ext
       where deal_id in (select deal_id from deal_head
                          where order_no = I_order_no);

      delete from deal_head
       where order_no = I_order_no;

      --- Delete inventory management/replenishment records

      -- delete from ord_inv_mgmt
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'ORD_INV_MGMT',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));
      delete from ord_inv_mgmt
         where order_no = I_order_no;

      delete from rev_orders
         where order_no = I_order_no;

      -- delete from alc_comp_loc
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'ALC_COMP_LOC',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));
      delete from alc_comp_loc
       where order_no = I_order_no;

      -- delete from alc_head
      SQL_LIB.SET_MARK('DELTE',
                       NULL,
                       'ALC_HEAD',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));
      delete from alc_head
       where order_no = I_order_no;

   elsif I_calling_form in ('ordredit', 'ordredal','undo') then
      -- the following call deals with deletion of work orders
      -- at the proper level

      if NOT WO_SQL.DELETE_WO(O_error_message,
                              NULL,        -- wo_id
                              I_order_no,
                              NULL,        -- tsfalloc_no
                              I_item,
                              I_location) then
         return FALSE;
      end if;
      --
      if I_calling_form != 'ordredit' then
         delete from alloc_chrg
          where alloc_no in (select alloc_no
                               from alloc_header ah,
                                    item_master im
                              where ah.order_no = I_order_no
                                and (im.item = NVL(I_item, im.item)
                                 or item_parent = NVL(I_item, im.item_parent)
                                 or item_grandparent = NVL(I_item, im.item_grandparent))
                                and ah.item = im.item
                                and ah.wh = NVL(I_location, ah.wh));

         delete from alloc_detail
          where alloc_no in (select alloc_no
                               from alloc_header ah,
                                    item_master im
                              where ah.order_no = I_order_no
                                and (im.item = NVL(I_item, im.item)
                                 or item_parent = NVL(I_item, im.item_parent)
                                 or item_grandparent = NVL(I_item, im.item_grandparent))
                                and ah.item = im.item
                                and ah.wh = NVL(I_location, ah.wh));

         delete from alloc_header
          where order_no = I_order_no
            and wh = NVL(I_location, wh)
            and item in (select item
                           from item_master
                          where item = NVL(I_item, item)
                             or item_parent = NVL(I_item, item_parent)
                             or item_grandparent = NVL(I_item, item_grandparent));
      end if;
      --
      if I_calling_form in ('ordredit','undo') then
         -- Detemine if the order is or ever has been approved. If it
         -- it has been, then don't delete timelines, req_docs or ordsku
         -- records.
         if not ORDER_ATTRIB_SQL.PREV_APPROVED(O_error_message,
                                               L_prev_approved,
                                               I_order_no) then
            return FALSE;
         end if;
         ---
         if DELETE_COMPS (O_error_message,
                          I_order_no,
                          I_item,
                          NULL, -- I_component_sku (for packs only)
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          I_calling_form) = FALSE then
            return FALSE;
         end if;

            delete from ordloc_discount
             where order_no = I_order_no
               and item in (select item
                              from item_master
                             where item = NVL(I_item, item)
                                or item_parent = I_item
                                or item_grandparent = I_item);

            delete from ordloc_cfa_ext
             where order_no = I_order_no
               and item in (select item
                              from item_master
                             where item = NVL(I_item, item)
                                or item_parent = I_item
                                or item_grandparent = I_item);

            delete from ordloc ol
             where order_no = I_order_no
               and item in (select item
                              from item_master
                             where item = NVL(I_item, item)
                                or item_parent = I_item
                                or item_grandparent = I_item);

         delete from ord_tax_breakup ot
          where order_no = I_order_no
            and item in (select item
                           from item_master
                          where item = NVL(I_item, item)
                             or item_parent = I_item
                             or item_grandparent = I_item);

         delete from tax_calc_event ot
                   where order_no = I_order_no;

         delete from repl_results
          where order_no = I_order_no
            and item in (select item
                           from item_master
                           where item = NVL(I_item, item)
                              or item_parent = I_item
                              or item_grandparent = I_item);

         if L_prev_approved = 'N' then
            ---
            delete from req_doc
             where module = 'POIT'
               and key_value_1 = TO_CHAR(I_order_no)
               and key_value_2 in (select item
                                     from item_master
                                    where item = NVL(I_item, item)
                                       or item_parent = I_item
                                       or item_grandparent = I_item);
            ---
            delete from timeline
             where timeline_type = 'POIT'
               and key_value_1 = TO_CHAR(I_order_no)
               and key_value_2 in (select item
                                     from item_master
                                    where item = NVL(I_item, item)
                                       or item_parent = I_item
                                       or item_grandparent = I_item);
            ---
            delete from ordsku_cfa_ext
             where order_no = I_order_no
               and item in (select item
                              from item_master
                             where item = NVL(I_item, item)
                                or item_parent = I_item
                                or item_grandparent = I_item);
            --
            delete from ordsku
             where order_no = I_order_no
               and item in (select item
                              from item_master
                             where item = NVL(I_item, item)
                                or item_parent = I_item
                                or item_grandparent = I_item);
         end if;
      end if;
   else /* I_calling_form = 'preissue' */

      L_table := 'ORD_PREISSUE';
      open C_LOCK_ORD_PREISSUE;
      close C_LOCK_ORD_PREISSUE;

      delete from ord_preissue
       where order_no = I_order_no;

   end if;

   if I_calling_form in ('split_po', 'ordautcl') then

      SQL_LIB.SET_MARK('OPEN',
                             'C_lock_order_ext_lock',
                             'ORDHEAD_L10N_EXT',
                             'ORDER_NO: '||to_char(I_order_no));
      open C_LOCK_ORDER_EXT_LOCK;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_lock_order_ext_lock',
                       'ORDHEAD_L10N_EXT',
                       'ORDER_NO: '||to_char(I_order_no));
      close C_LOCK_ORDER_EXT_LOCK;

      -- delete from ordhead_l10n_ext
      SQL_LIB.SET_MARK('DELETE',
                        NULL,
                       'ORDHEAD_L10N_EXT',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));

      delete from ordhead_l10n_ext
       where order_no = I_order_no;

      -- Lock ordhead_cfa_ext
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ORDHEAD_CE', 'ORDHEAD_CFA_EXT', 'ORDER_NO: ' || I_order_no);
      open C_LOCK_ORDHEAD_CE;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDHEAD_CE', 'ORDHEAD_CFA_EXT', 'ORDER_NO: ' || I_order_no);
      close C_LOCK_ORDHEAD_CE;

      L_table := 'ORDHEAD_CFA_EXT';
      -- delete from ordhead_cfa_ext
      SQL_LIB.SET_MARK('DELETE',
                        NULL,
                       'ORDHEAD_CFA_EXT',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));

      delete from ordhead_cfa_ext
       where order_no = I_order_no;

      ---
      if LOCK_ORDHEAD(O_error_message,
                      I_order_no) = FALSE then
         return FALSE;
      end if;

      L_table := 'ORDHEAD';
      -- delete from ordhead
      SQL_LIB.SET_MARK('DELETE',
                        NULL,
                       'ORDHEAD',
                       'ORDER_NO: ' || TO_CHAR(I_order_no));
      delete from ordhead
       where order_no = I_order_no;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            TO_CHAR(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ORDER;
-------------------------------------------------------------------------------
FUNCTION DELETE_TEMP_TABLES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---

   SQL_LIB.SET_MARK('DELETE',NULL,'ordsku_temp',NULL);

   delete from ordsku_temp
    where order_no = I_order_no;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'ordloc_temp',NULL);

   delete from ordloc_temp
    where order_no = I_order_no;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'alloc_chrg_temp',NULL);

   delete from alloc_chrg_temp
    where alloc_no in (select alloc_no
                         from alloc_header_temp
                        where order_no = I_order_no);
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'alloc_detail_temp',NULL);

   delete from alloc_detail_temp
    where alloc_no in (select alloc_no
                         from alloc_header_temp
                        where order_no = I_order_no);
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'alloc_header_temp',NULL);

   delete from alloc_header_temp
    where order_no = I_order_no;

   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'ordloc_exp_temp',NULL);

   delete from ordloc_exp_temp
    where order_no = I_order_no;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'ordsku_hts_assess_temp',NULL);

   delete from ordsku_hts_assess_temp
    where order_no = I_order_no;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'ordsku_hts_temp',NULL);

   delete from ordsku_hts_temp
    where order_no = I_order_no;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'ordloc_discount_temp',NULL);

   delete from ordloc_discount_temp
    where order_no = I_order_no;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'timeline_temp',NULL);

   delete from timeline_temp
    where timeline_type = 'POIT'
      and key_value_1   = I_order_no;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'req_doc_temp',NULL);

   delete from req_doc_temp
    where module = 'POIT'
      and key_value_1 = I_order_no;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'wo_detail_temp',NULL);

   delete from wo_detail_temp
    where wo_id in (select wo_id
                      from wo_head_temp
                     where order_no = I_order_no);
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'wo_head_temp',NULL);

   delete from wo_head_temp
    where order_no = I_order_no;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'repl_results_temp',NULL);
   delete from repl_results_temp
     where order_no = I_order_no;
   ---

   -- delete from orddist_item_temp
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'orddist_item_temp',
                    NULL);
   delete from orddist_item_temp
    where order_no = I_order_no;

   -- delete from alc_head_temp
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'alc_head_temp',
                    NULL);
   delete from alc_head_temp
    where order_no = I_order_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.DELETE_TEMP_TABLES',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_TEMP_TABLES;
-------------------------------------------------------------------------------
FUNCTION EXPLODE_GRADE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_validation_flag IN OUT   VARCHAR2,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                       I_where_clause    IN       FILTER_TEMP.WHERE_CLAUSE%TYPE)
   RETURN BOOLEAN IS

   L_store_grade_recs       NUMBER(3);
   L_item                   ITEM_MASTER.ITEM%TYPE;
   L_item_parent            ITEM_MASTER.ITEM_PARENT%TYPE;
   L_diff1                  ITEM_MASTER.DIFF_1%TYPE;
   L_diff2                  ITEM_MASTER.DIFF_2%TYPE;
   L_diff3                  ITEM_MASTER.DIFF_3%TYPE;
   L_diff4                  ITEM_MASTER.DIFF_4%TYPE;
   L_store_grade            STORE_GRADE.STORE_GRADE%TYPE;
   L_store_grade_group_id   STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE;
   L_calc_qty               ORDLOC_WKSHT.CALC_QTY%TYPE;
   L_act_qty                ORDLOC_WKSHT.ACT_QTY%TYPE;
   L_origin_country         ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE;
   L_wksht_qty              ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_standard_uom           ORDLOC_WKSHT.STANDARD_UOM%TYPE;
   L_uop                    ORDLOC_WKSHT.UOP%TYPE;
   L_supp_pack_size         ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;
   L_variance_qty           ORDLOC_WKSHT.VARIANCE_QTY%TYPE;
   L_where_clause           FILTER_TEMP.WHERE_CLAUSE%TYPE;
   L_ordloc_wksht_cnt       NUMBER := 0;
   L_nbd                    ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_nad                    ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_ord_imp_cnty_id        ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   L_import_cnty_id         ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   L_status                 ORDHEAD.STATUS%TYPE;
   L_contract_no            ORDHEAD.CONTRACT_NO%TYPE;
   L_supplier               ORDHEAD.SUPPLIER%TYPE;
   L_valid_loc_count        NUMBER;
   L_valid_loc_ind          VARCHAR2(1);
   L_row_num                VARCHAR2(1);
   L_inv_flag               VARCHAR2(1) := 'N';
   L_error_type             RTK_ERRORS.RTK_TEXT%TYPE;
   L_text                   VARCHAR2(9000); /* Size must be larger than FILTER_TEMP.WHERE_CLAUSE%TYPE */
   L_multiple               ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   C_ORDLOC_WKSHT           INTEGER;
   L_dummy                  INTEGER;
   L_table                  VARCHAR2(30)  := 'ORDLOC_WKSHT';
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked, -54);

   TYPE STORE_GRADE_TEMP_TBL  IS TABLE OF STORE_GRADE_STORE.STORE%TYPE;
   L_store_grade_tbl        STORE_GRADE_TEMP_TBL;

   cursor C_LOCK_ORDLOC_WKSHT is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         for update nowait;

   cursor C_STORE_GRADE is
      select count(*)
        from store_grade_store
       where store_grade          = L_store_grade
         and store_grade_group_id = L_store_grade_group_id;

   cursor C_GET_STORE is
     select store
       from store_grade_store
      where store_grade          = L_store_grade
        and store_grade_group_id = L_store_grade_group_id
      order by store;

   cursor C_GET_ORDHEAD is
     select o.not_before_date,
            o.not_after_date,
            o.import_country_id,
            o.status,
            o.contract_no,
            o.supplier
       from ordhead o
      where order_no = I_order_no;

   cursor C_ORDLOC_WKSHT_CNT is
      select count(*)
        from ordloc_wksht
       where order_no = I_order_no
         and location is not NULL;

BEGIN

   C_ORDLOC_WKSHT := DBMS_SQL.OPEN_CURSOR;

   if I_where_clause is not NULL then
      L_where_clause := ' and '||I_where_clause;
   end if;
   L_text := 'select item, item_parent, diff_1, diff_2, diff_3, diff_4, store_grade_group_id, store_grade, ' ||
             'act_qty, origin_country_id, wksht_qty, standard_uom, uop, supp_pack_size ' ||
             'from ordloc_wksht where order_no = to_char(:order_no) '||L_where_clause;

   DBMS_SQL.PARSE(C_ORDLOC_WKSHT,L_text,dbms_sql.native);
   DBMS_SQL.BIND_VARIABLE( C_ORDLOC_WKSHT, ':order_no', I_order_no );
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 1, L_item, 25);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 2, L_item_parent, 25);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 3, L_diff1, 10);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 4, L_diff2, 10);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 5, L_diff3, 10);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 6, L_diff4, 10);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 7, L_store_grade_group_id);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 8, L_store_grade, 15);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 9, L_act_qty);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 10, L_origin_country, 3);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 11, L_wksht_qty);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 12, L_standard_uom, 4);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 13, L_uop, 6);
   DBMS_SQL.DEFINE_COLUMN (C_ORDLOC_WKSHT, 14, L_supp_pack_size);
   ---
   L_dummy := DBMS_SQL.EXECUTE(C_ORDLOC_WKSHT);
   ---

   open C_GET_ORDHEAD;
   fetch C_GET_ORDHEAD into L_nbd,
                            L_nad,
                            L_ord_imp_cnty_id,
                            L_status,
                            L_contract_no,
                            L_supplier;
   close C_GET_ORDHEAD;

   L_valid_loc_ind := 'N';

   LOOP
      if DBMS_SQL.FETCH_ROWS   (C_ORDLOC_WKSHT) > 0 then
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 1, L_item);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 2, L_item_parent);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 3, L_diff1);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 4, L_diff2);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 5, L_diff3);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 6, L_diff4);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 7, L_store_grade_group_id);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 8, L_store_grade);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 9, L_act_qty);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 10, L_origin_country);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 11, L_wksht_qty);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 12, L_standard_uom);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 13, L_uop);
         DBMS_SQL.COLUMN_VALUE (C_ORDLOC_WKSHT, 14, L_supp_pack_size);

         L_store_grade_recs := NULL;
         open C_STORE_GRADE;
         fetch C_STORE_GRADE into L_store_grade_recs;
         close C_STORE_GRADE;

         if L_store_grade_recs = 0 then
            O_error_message := SQL_LIB.CREATE_MSG('STORE_GRADE_NO_STORE',
                                                  L_store_grade,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;

         L_valid_loc_count := 0;

         if L_status != 'W' or L_ordloc_wksht_cnt > 0 then
            L_row_num := '2';
         else
            L_row_num := '1';
         end if;

         L_store_grade_tbl := NEW STORE_GRADE_TEMP_TBL();

         -- Count all valid stores
         FOR rec in C_GET_STORE LOOP
            if L_valid_loc_ind = 'N' then
               L_import_cnty_id := L_ord_imp_cnty_id;
            end if;

            if DISTRIBUTE_LOCATION_SQL.VALIDATE_LOCATION (O_error_message,
                                                          L_error_type,
                                                          L_import_cnty_id,
                                                          I_order_no,
                                                          L_row_num,
                                                          L_contract_no,
                                                          'S',
                                                          rec.store,
                                                          L_supplier,
                                                          NULL,
                                                          L_nbd,
                                                          L_nad) = FALSE then
               L_inv_flag := 'Y';
            else

               L_store_grade_tbl.EXTEND;
               L_store_grade_tbl(L_store_grade_tbl.COUNT()) := rec.store;
               L_valid_loc_ind := 'Y';
               L_valid_loc_count := L_valid_loc_count + 1;

               if L_row_num = 1 then
                  L_row_num := '2';
               end if;

            end if;
         END LOOP;

         -- Insert to ORDLOC_WKSHT table all valid stores only
         if L_valid_loc_count != 0 then
            if NVL(L_act_qty,0) > 0 then
               L_calc_qty     := L_act_qty / L_valid_loc_count;
            else
               L_calc_qty := 0;
            end if;
            --if the uop and standard uom are the same then the wksht_qty will be the same as the act_qty
            --else it will be the act_qty / the supplier pack size
            if L_uop = L_standard_uom then
               L_multiple := 1;
            else
               L_multiple := L_supp_pack_size;
            end if;

            --if the UOP is the same as the standard UOM(not eaches)then we want to round the qtys to four decimals to ensure we
            --keep the decimals
            if NVL(L_wksht_qty,0) > 0 then
               if L_uop = L_standard_uom and L_standard_uom <> 'EA' then
                  /* Round L_calc_qty to the fourth decimal */
                  L_wksht_qty :=  ROUND(L_calc_qty, 4);
                  L_act_qty      := L_wksht_qty;
                  L_variance_qty := ((L_act_qty - L_calc_qty)/L_calc_qty) * 100;
               else -- if the UOP is case or standard UOM (eaches) then we will want to truncate since they are whole numbers anyway
                  /* Truncate L_calc_qty DOWN to the next whole number */
                  L_wksht_qty :=  TRUNC((L_calc_qty / L_multiple));
                  L_act_qty      := (L_wksht_qty * L_multiple);
                  L_variance_qty := ((L_act_qty - L_calc_qty)/L_calc_qty) * 100;
               end if;
            end if;

            FORALL i in 1..L_store_grade_tbl.COUNT
            insert into ordloc_wksht (order_no,
                                      item,
                                      item_parent,
                                      diff_1,
                                      diff_2,
                                      diff_3,
                                      diff_4,
                                      store_grade_group_id,
                                      store_grade,
                                      loc_type,
                                      location,
                                      calc_qty,
                                      act_qty,
                                      variance_qty,
                                      origin_country_id,
                                      standard_uom,
                                      wksht_qty,
                                      uop,
                                      supp_pack_size)
                               values(I_order_no,
                                      L_item,
                                      L_item_parent,
                                      L_diff1,
                                      L_diff2,
                                      L_diff3,
                                      L_diff4,
                                      L_store_grade_group_id,
                                      L_store_grade,
                                      'S',
                                      L_store_grade_tbl(i),
                                      L_calc_qty,
                                      L_act_qty,
                                      L_variance_qty,
                                      L_origin_country,
                                      L_standard_uom,
                                      L_wksht_qty,
                                      L_uop,
                                      L_supp_pack_size);
         end if;
      else
         exit;
      end if;

      if L_ordloc_wksht_cnt = 0 then
         open C_ORDLOC_WKSHT_CNT;
         fetch C_ORDLOC_WKSHT_CNT into L_ordloc_wksht_cnt;
         close C_ORDLOC_WKSHT_CNT;
      end if;

   END LOOP;
   DBMS_SQL.CLOSE_CURSOR (C_ORDLOC_WKSHT);

   L_ordloc_wksht_cnt := 0;

   open C_ORDLOC_WKSHT_CNT;
   fetch C_ORDLOC_WKSHT_CNT into L_ordloc_wksht_cnt;
   close C_ORDLOC_WKSHT_CNT;


   if L_ordloc_wksht_cnt = 0 then
      O_validation_flag := 'E';
   else
      open C_LOCK_ORDLOC_WKSHT;
      close C_LOCK_ORDLOC_WKSHT;

      L_text := 'delete from ordloc_wksht where location is NULL and order_no = '||to_char(I_order_no)||L_where_clause||';';

      if EXECUTE_SQL.EXECUTE_SQL(O_error_message,
                                 L_text) = FALSE then
         return FALSE;
      end if;

      if L_inv_flag = 'Y' then
         O_validation_flag := 'W';
      else
         O_validation_flag := 'S';
      end if;
   end if;

   return TRUE;


EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             TO_CHAR(I_order_no),
                                             NULL);
      if DBMS_SQL.IS_OPEN (C_ORDLOC_WKSHT) then
         DBMS_SQL.CLOSE_CURSOR (C_ORDLOC_WKSHT);
      end if;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_SETUP_SQL.EXPLODE_GRADE',
                                             to_char(SQLCODE));
      return FALSE;
END EXPLODE_GRADE;
-------------------------------------------------------------------------------
FUNCTION CREATE_FROM_EXISTING(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_order_no        IN OUT   ORDHEAD.ORDER_NO%TYPE,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program                   VARCHAR2(64) := 'ORDER_SETUP_SQL.CREATE_FROM_EXISTING';

   L_vdate                     PERIOD.VDATE%TYPE := GET_VDATE;
   L_not_before_date           ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_not_after_date            ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_earliest_ship_date        ORDHEAD.EARLIEST_SHIP_DATE%TYPE;
   L_latest_ship_date          ORDHEAD.LATEST_SHIP_DATE%TYPE;
   L_pickup_date               ORDHEAD.PICKUP_DATE%TYPE;
   L_otb_eow_date              ORDHEAD.OTB_EOW_DATE%TYPE;
   L_supplier                  ORDHEAD.SUPPLIER%TYPE;
   L_update_nbd                VARCHAR2(1) := 'N';
   L_update_nad                VARCHAR2(1) := 'N';
   L_update_pkup               VARCHAR2(1) := 'N';
   L_update_otb                VARCHAR2(1) := 'N';
   L_latest_ship_days          SYSTEM_OPTIONS.LATEST_SHIP_DAYS%TYPE;
   L_lead_time                 ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE;
   L_dd                        NUMBER;
   L_mm                        NUMBER;
   L_yyyy                      NUMBER;
   L_dd_out                    NUMBER;
   L_mm_out                    NUMBER;
   L_yyyy_out                  NUMBER;
   L_elc_ind                   SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_import_ind                SYSTEM_OPTIONS.IMPORT_IND%TYPE;
   L_order_import_ind          ORDHEAD.IMPORT_ORDER_IND%TYPE;
   L_wo_id                     WO_HEAD.WO_ID%TYPE;
   L_ordcust_no                ORDCUST.ORDCUST_NO%TYPE;
   L_ordcust_no_seq            ORDCUST.ORDCUST_NO%TYPE;
   L_return_code               VARCHAR2(255);
   L_alloc_no                  ALLOC_HEADER.ALLOC_NO%TYPE;
   L_calc_pickup_date          ORDHEAD.PICKUP_DATE%TYPE;
   L_calc_nbd                  ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_calc_nad                  ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_calc_eow_date             ORDHEAD.OTB_EOW_DATE%TYPE;
   L_pickup_insert             ORDHEAD.PICKUP_DATE%TYPE;
   L_eow_insert                ORDHEAD.OTB_EOW_DATE%TYPE;
   L_pur_type                  ORDHEAD.PURCHASE_TYPE%TYPE;
   L_nad_ind                   VARCHAR2(1)   := 'N';
   L_eow_ind                   VARCHAR2(1)   := 'N';
   L_ord_type                  VARCHAR2(4);
   L_copy_po_curr_rate         PROCUREMENT_UNIT_OPTIONS.COPY_PO_CURR_RATE%TYPE;
   L_exchange_rate             ORDHEAD.EXCHANGE_RATE%TYPE;
   L_currency_code             ORDHEAD.CURRENCY_CODE%TYPE;
   L_old_exchange_rate         ORDHEAD.EXCHANGE_RATE%TYPE;
   L_comp_currency             ORDLOC_EXP.COMP_CURRENCY%TYPE;
   L_exchange_rate_ordexp      ORDLOC_EXP.EXCHANGE_RATE%TYPE;
   L_prev_comp_currency        ORDLOC_EXP.COMP_CURRENCY%TYPE;
   L_prev_exchange_rate        ORDLOC_EXP.EXCHANGE_RATE%TYPE;
   L_seq_no                    ORDLOC_EXP.SEQ_NO%TYPE;
   L_unitcost                  ORDLOC.UNIT_COST%TYPE;
   L_unitcost_out              ORDLOC.UNIT_COST%TYPE;
   L_unit_cost_init            ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_supp_currency             SUPS.CURRENCY_CODE%TYPE;
   L_cost_source               ORDLOC.COST_SOURCE%TYPE;

   cursor C_GET_DATES is
      select not_before_date,
             not_after_date,
             earliest_ship_date,
             latest_ship_date,
             pickup_date,
             otb_eow_date,
             supplier,
             currency_code,
             exchange_rate
        from ordhead
       where order_no = I_order_no;

   cursor C_GET_WO_ID is
      select wo_id
        from wo_head
       where order_no = I_order_no;

   cursor C_GET_ALLOC_NO is
      select alloc_no
        from alloc_header
       where order_no = I_order_no;

   cursor C_ORDCUST_EXISTS is
      select ordcust_no
        from ordcust
       where order_no = I_order_no;

   cursor C_CUST_SEQ is
      select ordcust_seq.NEXTVAL seq
        from dual;

   cursor C_ORDSKU is
      select item
        from ordsku
       where order_no = O_order_no;

   cursor C_SEQ is
      select oe.seq_no
        from ordloc_exp oe,
             ordloc ol
       where oe.order_no = I_order_no
        and  oe.order_no = ol.order_no
        and  oe.item     = ol.item
        and  oe.location = ol.location
        and  oe.loc_type = ol.loc_type
        and  ol.qty_ordered <> 0
        and  ((ol.loc_type ='S'
                and EXISTS(select 'X'
                             from v_store
                            where NVL(store_close_date, TO_DATE('12319999','MMDDYYYY')) > L_vdate
                              and store = ol.location))
             or (ol.loc_type='W'))
       order by comp_currency;

   cursor C_HTS_SEQ is
      select distinct orh.seq_no
         from ordsku_hts orh,
              ordloc ol
        where orh.order_no = I_order_no
          and orh.order_no = ol.order_no
          and NVL(orh.pack_item,orh.item)= ol.item
          and ol.qty_ordered <> 0
          and ((ol.loc_type ='S'
                    and EXISTS(select 'X'
                                 from v_store
                                where NVL(store_close_date, TO_DATE('12319999','MMDDYYYY')) > L_vdate
                                  and store = ol.location))
              or (ol.loc_type='W'));

   cursor C_ASSESS_SEQ is
      select seq_no,comp_id
         from ordsku_hts_assess orha
        where order_no = I_order_no
          and seq_no in(select orh.seq_no
                          from ordsku_hts orh,
                               ordloc ol
                         where orh.order_no   = orha.order_no
                           and orh.order_no   = ol.order_no
                           and NVL(orh.pack_item,orh.item)       = ol.item
                           and ol.qty_ordered <> 0
                           and ((ol.loc_type ='S'
                                    and EXISTS(select 'X'
                                                 from v_store
                                                where NVL(store_close_date, TO_DATE('12319999','MMDDYYYY')) > L_vdate
                                                  and store = ol.location))
                               or (ol.loc_type='W')));

   cursor C_PUR_TYPE is
      select purchase_type
        from ordhead
       where order_no = I_order_no;

   cursor C_GET_COPY_PO_CURR_RATE is
      select copy_po_curr_rate
        from procurement_unit_options;

   cursor C_CURRENCY_ORDEXP is
      select comp_currency
        from ordloc_exp
       where order_no = I_order_no
         and seq_no = L_seq_no;

   cursor C_CURRENCY_SUPP is
      select currency_code
        from sups
       where supplier=L_supplier;

   cursor C_ORDHEAD_DTLS is
      select o.item,
             o.loc_type,
             o.location,
             o.unit_retail,
             o.qty_ordered,
             o.unit_cost o_unit_cost,
             iscl.unit_cost iscl_unit_cost,
             o.unit_cost_init,
             o.cost_source
        from ordloc o,
             ordsku os,
             item_supp_country_loc iscl
       where o.order_no           = I_order_no
         and os.order_no          = O_order_no
         and iscl.supplier        = L_supplier
         and o.item               = os.item
         and o.item               = iscl.item
         and os.origin_country_id = iscl.origin_country_id
         and iscl.loc             = o.location
         and o.qty_ordered        <> 0
         and ((o.loc_type ='S'
                and EXISTS(select 'X'
                             from v_store
                            where NVL(store_close_date, TO_DATE('12319999','MMDDYYYY')) > L_vdate
                              and store = o.location))
             or (o.loc_type='W'));

BEGIN
   if SYSTEM_OPTIONS_SQL.GET_IMPORT_ELC_IND(O_error_message,
                                            L_import_ind,
                                            L_elc_ind) = FALSE then
      return FALSE;
   end if;
   ---
   if ORDER_ATTRIB_SQL.GET_IMPORT_IND(O_error_message,
                                      L_order_import_ind,
                                      I_order_no) = FALSE then
      return FALSE;
   end if;

   if ORDER_NUMBER_SQL.NEXT_ORDER_NUMBER(O_error_message,
                                         O_order_no) = FALSE then
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_LATEST_SHIP_DAYS(O_error_message,
                                              L_latest_ship_days) = FALSE then
      return FALSE;
   end if;

   ----- get dates from existing order
   SQL_LIB.SET_MARK('OPEN', 'C_GET_DATES', 'ORDHEAD', 'order_no: '||TO_CHAR(I_order_no));
   open C_GET_DATES;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_DATES', 'ORDHEAD', 'order_no: '||TO_CHAR(I_order_no));
   fetch C_GET_DATES into L_not_before_date,
                          L_not_after_date,
                          L_earliest_ship_date,
                          L_latest_ship_date,
                          L_pickup_date,
                          L_otb_eow_date,
                          L_supplier,
                          L_currency_code,
                          L_old_exchange_rate;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_DATES', 'ORDHEAD', 'order_no: '||TO_CHAR(I_order_no));
   close C_GET_DATES;

   ---- get supplier currency
   open C_CURRENCY_SUPP;
   fetch C_CURRENCY_SUPP into L_supp_currency;
   close C_CURRENCY_SUPP;

   ----- get copy_po_curr_rate from procurement_unit_options
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_COPY_PO_CURR_RATE',
                    'PROCUREMENT_UNIT_OPTIONS',
                    NULL);
   open C_GET_COPY_PO_CURR_RATE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_COPY_PO_CURR_RATE',
                    'PROCUREMENT_UNIT_OPTIONS',
                    NULL);
   fetch C_GET_COPY_PO_CURR_RATE into L_copy_po_curr_rate;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_COPY_PO_CURR_RATE',
                    'PROCUREMENT_UNIT_OPTIONS',
                    NULL);
   close C_GET_COPY_PO_CURR_RATE;

   if L_copy_po_curr_rate = 'N' then
      ---- copy latest exchange rate to the new purchase order
      if CURRENCY_SQL.GET_RATE(O_error_message,
                               L_exchange_rate,
                               L_currency_code,
                               'P',
                               L_vdate) = FALSE then
         return FALSE;
      end if;
   else
      ---- copy the exchange rate from the existing purchase order
      L_exchange_rate := L_old_exchange_rate;
   end if;

   ----- calculate new dates based on vdate
   if ORDER_CALC_SQL.CALC_HEADER_DATES(O_error_message,
                                       L_calc_pickup_date,
                                       L_calc_nbd,
                                       L_calc_nad,
                                       I_order_no,
                                       L_supplier) = FALSE then
      return FALSE;
   end if;
   -----

   -- Calculate the end of week date based on the calculated Not After Date.
   L_dd    := to_number(to_char(L_calc_nad, 'DD'), '09');
   L_mm    := to_number(to_char(L_calc_nad, 'MM'), '09');
   L_yyyy  := to_number(to_char(L_calc_nad, 'YYYY'), '0999');

   CAL_TO_454_LDOW(L_dd,
                   L_mm,
                   L_yyyy,
                   L_dd_out,
                   L_mm_out,
                   L_yyyy_out,
                   L_return_code,
                   O_error_message);
   if L_return_code = 'FALSE' then
      return FALSE;
   end if;

   L_calc_eow_date := TO_DATE((TO_CHAR(L_dd_out)||'-'||
                               TO_CHAR(L_mm_out)||'-'||
                               TO_CHAR(L_yyyy_out)), 'DD-MM-YYYY');

   ------ check if dates are after current vdate
   if L_not_before_date < L_vdate then
      L_update_nbd := 'Y';
      L_nad_ind    := 'Y';
   end if;
   ---

   if L_not_after_date < L_vdate then
      L_update_nad := 'Y';
      L_eow_ind    := 'Y';
   elsif L_nad_ind = 'Y' then
      if L_not_after_date < L_calc_nbd then
         L_update_nad := 'Y';
         L_eow_ind    := 'Y';
      end if;
   end if;
   ---
   --- determine purchase type of existing order.  if not pickup,then, do not defualt in pickup date.
   SQL_LIB.SET_MARK('OPEN', 'C_PUR_TYPE', 'ORDHEAD', 'order_no: '||TO_CHAR(I_order_no));
   open C_PUR_TYPE;
   SQL_LIB.SET_MARK('FETCH', 'C_PUR_TYPE', 'ORDHEAD', 'order_no: '||TO_CHAR(I_order_no));
   fetch C_PUR_TYPE into L_pur_type;
   SQL_LIB.SET_MARK('CLOSE', 'C_PUR_TYPE', 'ORDHEAD', 'order_no: '||TO_CHAR(I_order_no));
   close C_PUR_TYPE;

   if L_pur_type = 'PUP' then
      if L_pickup_date < L_vdate then
         L_pickup_insert := L_calc_pickup_date;
      else
         L_update_pkup := 'Y';
      end if;
   else
      L_pickup_insert := NULL;
   end if;
   ---

   if L_otb_eow_date < L_vdate then
      L_update_otb := 'Y';
      L_eow_insert := L_calc_eow_date;
   elsif L_eow_ind = 'Y' then
      L_update_otb := 'Y';
      L_eow_insert := L_calc_eow_date;
   end if;
   ---
   insert into ordhead (order_no,
                        order_type,
                        dept,
                        buyer,
                        supplier,
                        supp_add_seq_no,
                        location,
                        loc_type,
                        promotion,
                        qc_ind,
                        include_on_order_ind,
                        written_date,
                        not_before_date,
                        not_after_date,
                        otb_eow_date,
                        earliest_ship_date,
                        latest_ship_date,
                        close_date,
                        terms,
                        freight_terms,
                        orig_ind,
                        payment_method,
                        ship_method,
                        purchase_type,
                        status,
                        orig_approval_date,
                        orig_approval_id,
                        ship_pay_method,
                        fob_trans_res,
                        fob_trans_res_desc,
                        fob_title_pass,
                        fob_title_pass_desc,
                        edi_sent_ind,
                        edi_po_ind,
                        import_order_ind,
                        import_country_id,
                        po_ack_recvd_ind,
                        vendor_order_no,
                        exchange_rate,
                        factory,
                        agent,
                        discharge_port,
                        lading_port,
                        freight_contract_no,
                        po_type,
                        pre_mark_ind,
                        currency_code,
                        contract_no,
                        last_sent_rev_no,
                        pickup_loc,
                        pickup_date,
                        pickup_no,
                        app_datetime,
                        backhaul_type,
                        backhaul_allowance,
                        split_ref_ordno,
                        comment_desc,
                        clearing_zone_id,
                        import_id,
                        import_type,
                        routing_loc_id,
                        delivery_supplier,
                        triangulation_ind,
                        partner_type_1,
                        partner1,
                        partner_type_2,
                        partner2,
                        partner_type_3,
                        partner3,
                        master_po_no)
                 select O_order_no,
                        decode(order_type, 'CO', order_type, 'N/B'),
                        dept,
                        buyer,
                        supplier,
                        supp_add_seq_no,
                        location,
                        loc_type,
                        promotion,
                        qc_ind,
                        include_on_order_ind,
                        L_vdate,
                        decode(L_update_nbd, 'Y', L_calc_nbd, not_before_date),
                        decode(L_update_nad, 'Y', L_calc_nad, not_after_date),
                        decode(L_update_otb, 'Y', L_eow_insert, otb_eow_date),
                        case when earliest_ship_date < L_vdate or earliest_ship_date > decode(L_update_nbd, 'Y', L_calc_nbd, not_before_date)
                           then decode(L_update_nbd, 'Y', L_calc_nbd, not_before_date)
                           else earliest_ship_date
                        end,                 -- this will be replaced by ordsku.earliest_ship_date if item records exists
                        case when latest_ship_date < (case when earliest_ship_date < L_vdate or earliest_ship_date > decode(L_update_nbd, 'Y', L_calc_nbd, not_before_date)
                                                         then decode(L_update_nbd, 'Y', L_calc_nbd, not_before_date)
                                                         else earliest_ship_date
                                                      end)
                           then decode(L_update_nbd, 'Y', L_calc_nbd, not_before_date) + NVL(L_latest_ship_days, 0)
                           else latest_ship_date
                        end,                 -- this will be replaced by ordsku.latest_ship_date if item records exists
                        NULL,              --close date
                        terms,
                        freight_terms,
                        2,                 --orig_ind
                        payment_method,
                        ship_method,
                        purchase_type,
                        'W',               --status
                        NULL,              --orig_approval_date
                        NULL,              --orig_approval_id
                        ship_pay_method,
                        fob_trans_res,
                        fob_trans_res_desc,
                        fob_title_pass,
                        fob_title_pass_desc,
                        'N',                --edi_sent_ind
                        edi_po_ind,
                        import_order_ind,
                        import_country_id,
                        'N',                --po_ack_recvd_ind
                        vendor_order_no,
                        L_exchange_rate,
                        factory,
                        agent,
                        discharge_port,
                        lading_port,
                        freight_contract_no,
                        po_type,
                        pre_mark_ind,
                        currency_code,
                        NULL,               --contract_no
                        NULL,
                        pickup_loc,
                        decode(L_update_pkup,'Y', pickup_date, L_pickup_insert),
                        pickup_no,
                        app_datetime,
                        backhaul_type,
                        backhaul_allowance,
                        split_ref_ordno,
                        comment_desc,
                        clearing_zone_id,
                        import_id,
                        import_type,
                        routing_loc_id,
                        delivery_supplier,
                        triangulation_ind,
                        partner_type_1,
                        partner1,
                        partner_type_2,
                        partner2,
                        partner_type_3,
                        partner3,
                        master_po_no
                   from ordhead
                  where order_no = I_order_no;

   ---
   insert into ordhead_cfa_ext(order_no,
                               group_id,
                               varchar2_1,
                               varchar2_2,
                               varchar2_3,
                               varchar2_4,
                               varchar2_5,
                               varchar2_6,
                               varchar2_7,
                               varchar2_8,
                               varchar2_9,
                               varchar2_10,
                               number_11,
                               number_12,
                               number_13,
                               number_14,
                               number_15,
                               number_16,
                               number_17,
                               number_18,
                               number_19,
                               number_20,
                               date_21,
                               date_22,
                               date_23,
                               date_24,
                               date_25)
                        select O_order_no,
                               ohc.group_id,
                               ohc.varchar2_1,
                               ohc.varchar2_2,
                               ohc.varchar2_3,
                               ohc.varchar2_4,
                               ohc.varchar2_5,
                               ohc.varchar2_6,
                               ohc.varchar2_7,
                               ohc.varchar2_8,
                               ohc.varchar2_9,
                               ohc.varchar2_10,
                               ohc.number_11,
                               ohc.number_12,
                               ohc.number_13,
                               ohc.number_14,
                               ohc.number_15,
                               ohc.number_16,
                               ohc.number_17,
                               ohc.number_18,
                               ohc.number_19,
                               ohc.number_20,
                               ohc.date_21,
                               ohc.date_22,
                               ohc.date_23,
                               ohc.date_24,
                               ohc.date_25
                          from ordhead_cfa_ext ohc
                         where ohc.order_no = I_order_no;
   ---
   insert into ordsku (order_no,
                       item,
                       ref_item,
                       origin_country_id,
                       earliest_ship_date,
                       latest_ship_date,
                       supp_pack_size,
                       non_scale_ind,
                       pickup_loc,
                       pickup_no)
                select O_order_no,
                       o.item,
                       o.ref_item,
                       o.origin_country_id,
                       case when o.earliest_ship_date < L_vdate or o.earliest_ship_date > decode(L_update_nbd, 'Y', L_calc_nbd, oh.not_before_date)
                              then decode(L_update_nbd, 'Y', L_calc_nbd, oh.not_before_date)
                            else o.earliest_ship_date
                       end,  --Ealiest_ship_date
                       case when o.latest_ship_date < (case when o.earliest_ship_date < L_vdate or o.earliest_ship_date > decode(L_update_nbd, 'Y', L_calc_nbd, oh.not_before_date)
                                                         then decode(L_update_nbd, 'Y', L_calc_nbd, oh.not_before_date)
                                                         else o.earliest_ship_date
                                                      end)
                              then decode(L_update_nbd, 'Y', L_calc_nbd, oh.not_before_date) + NVL(L_latest_ship_days, 0)
                            else o.latest_ship_date
                       end,  --Latest_ship_date
                       o.supp_pack_size,
                       'N',
                       o.pickup_loc,
                       o.pickup_no
                  from ordsku o,
                       ordhead oh,
                       item_supp_country isc
                 where o.order_no = I_order_no
                   and o.item = isc.item
                   and o.order_no = oh.order_no
                   and isc.supplier = oh.supplier
                   and isc.origin_country_id = o.origin_country_id
                   and not exists(select 'x'
                                    from packitem pi,
                                         pack_tmpl_head pt
                                   where o.order_no = I_order_no
                                     and pi.pack_no = o.item
                                     and pi.pack_tmpl_id = pt.pack_tmpl_id
                                     and pt.fash_prepack_ind = 'Y')
                  and exists (select 1
                                from ordloc ol
                                where ol.order_no = o.order_no
                                and ol.item = o.item
                                and ol.qty_ordered <> 0);

   insert into ordsku_cfa_ext (order_no,
                               item,
                               group_id,
                               varchar2_1,
                               varchar2_2,
                               varchar2_3,
                               varchar2_4,
                               varchar2_5,
                               varchar2_6,
                               varchar2_7,
                               varchar2_8,
                               varchar2_9,
                               varchar2_10,
                               number_11,
                               number_12,
                               number_13,
                               number_14,
                               number_15,
                               number_16,
                               number_17,
                               number_18,
                               number_19,
                               number_20,
                               date_21,
                               date_22,
                               date_23,
                               date_24,
                               date_25)
                        select O_order_no,
                               osc.item,
                               osc.group_id,
                               osc.varchar2_1,
                               osc.varchar2_2,
                               osc.varchar2_3,
                               osc.varchar2_4,
                               osc.varchar2_5,
                               osc.varchar2_6,
                               osc.varchar2_7,
                               osc.varchar2_8,
                               osc.varchar2_9,
                               osc.varchar2_10,
                               osc.number_11,
                               osc.number_12,
                               osc.number_13,
                               osc.number_14,
                               osc.number_15,
                               osc.number_16,
                               osc.number_17,
                               osc.number_18,
                               osc.number_19,
                               osc.number_20,
                               osc.date_21,
                               osc.date_22,
                               osc.date_23,
                               osc.date_24,
                               osc.date_25
                          from ordsku_cfa_ext osc,
                               ordsku os
                         where osc.order_no = I_order_no
                           and os.order_no = O_order_no
                           and os.item = osc.item;
   ---
   --- This will update ordhead with the minimum of the earliest_ship_dates on ordsku and
   --- the maximum of the latest_ship_dates on ordsku for this order.
   if UPDATE_SHIP_DATES(O_error_message,
                        O_order_no) = FALSE then
      return FALSE;
   end if;

   FOR c_rec  in C_ORDHEAD_DTLS LOOP
      if c_rec.cost_source<>'MANL' then
         L_unitcost := c_rec.iscl_unit_cost;
         L_cost_source := 'NORM';
            if UPPER(L_supp_currency) <> UPPER(L_currency_code) then
               if NOT CURRENCY_SQL.CONVERT (O_error_message,
                                            L_unitcost,
                                            L_supp_currency,
                                            L_currency_code,
                                            L_unitcost_out,
                                            'N',
                                            NULL,
                                            NULL,
                                            NULL,
                                            L_exchange_rate)then
                  return FALSE;
               end if;
               L_unit_cost_init := L_unitcost_out;
            else
               L_unitcost_out := c_rec.iscl_unit_cost;
               L_unit_cost_init := L_unitcost_out;
            end if;
      else
          L_unitcost_out := c_rec.o_unit_cost;
          L_unit_cost_init := c_rec.unit_cost_init;
          L_cost_source :='MANL';
      end if;

      insert into ordloc (order_no,
                          item,
                          loc_type,
                          location,
                          unit_retail,
                          qty_ordered,
                          qty_prescaled,
                          qty_received,
                          qty_cancelled,
                          last_received,
                          cancel_code,
                          cancel_date,
                          cancel_id,
                          original_repl_qty,
                          unit_cost,
                          unit_cost_init,
                          cost_source,
                          non_scale_ind)
                  values (O_order_no,
                          c_rec.item,
                          c_rec.loc_type,
                          c_rec.location,
                          c_rec.unit_retail,
                          c_rec.qty_ordered,
                          c_rec.qty_ordered,  --qty_prescaled
                          NULL,               --qty_received
                          NULL,               --qty_cancelled
                          NULL,               --last_received
                          NULL,               --cancel_code
                          NULL,               --cancel_date
                          NULL,               --cancel_id
                          NULL,               --original_repl_qty
                          L_unitcost_out,
                          L_unit_cost_init,
                          L_cost_source,
                          'N');
   END LOOP;
   insert into ordloc_cfa_ext( order_no,
                               item,
                               location,
                               group_id,
                               varchar2_1,
                               varchar2_2,
                               varchar2_3,
                               varchar2_4,
                               varchar2_5,
                               varchar2_6,
                               varchar2_7,
                               varchar2_8,
                               varchar2_9,
                               varchar2_10,
                               number_11,
                               number_12,
                               number_13,
                               number_14,
                               number_15,
                               number_16,
                               number_17,
                               number_18,
                               number_19,
                               number_20,
                               date_21,
                               date_22,
                               date_23,
                               date_24,
                               date_25)
                        select O_order_no,
                               olc.item,
                               olc.location,
                               olc.group_id,
                               olc.varchar2_1,
                               olc.varchar2_2,
                               olc.varchar2_3,
                               olc.varchar2_4,
                               olc.varchar2_5,
                               olc.varchar2_6,
                               olc.varchar2_7,
                               olc.varchar2_8,
                               olc.varchar2_9,
                               olc.varchar2_10,
                               olc.number_11,
                               olc.number_12,
                               olc.number_13,
                               olc.number_14,
                               olc.number_15,
                               olc.number_16,
                               olc.number_17,
                               olc.number_18,
                               olc.number_19,
                               olc.number_20,
                               olc.date_21,
                               olc.date_22,
                               olc.date_23,
                               olc.date_24,
                               olc.date_25
                          from ordloc_cfa_ext olc,
                               ordloc ol
                         where olc.order_no = I_order_no
                           and ol.order_no = O_order_no
                           and ol.item = olc.item
                           and ol.location = olc.location;

   insert into ordlc (order_no,
                      lc_ref_id,
                      lc_group_id,
                      applicant,
                      beneficiary,
                      merch_desc,
                      transshipment_ind,
                      partial_shipment_ind,
                      lc_ind)
               select O_order_no,
                      NULL,
                      lc_group_id,
                      applicant,
                      beneficiary,
                      merch_desc,
                      transshipment_ind,
                      partial_shipment_ind,
                      'N'
                 from ordlc
                where order_no = I_order_no;

   insert into ordhead_l10n_ext (order_no,
                                 l10n_country_id,
                                 group_id,
                                 varchar2_1,
                                 varchar2_2,
                                 varchar2_3,
                                 varchar2_4,
                                 varchar2_5,
                                 varchar2_6,
                                 varchar2_7,
                                 varchar2_8,
                                 varchar2_9,
                                 varchar2_10,
                                 number_11,
                                 number_12,
                                 number_13,
                                 number_14,
                                 number_15,
                                 number_16,
                                 number_17,
                                 number_18,
                                 number_19,
                                 number_20,
                                 date_21,
                                 date_22)
                          select O_order_no,
                                 l10n_country_id,
                                 group_id,
                                 varchar2_1,
                                 varchar2_2,
                                 varchar2_3,
                                 varchar2_4,
                                 varchar2_5,
                                 varchar2_6,
                                 varchar2_7,
                                 varchar2_8,
                                 varchar2_9,
                                 varchar2_10,
                                 number_11,
                                 number_12,
                                 number_13,
                                 number_14,
                                 number_15,
                                 number_16,
                                 number_17,
                                 number_18,
                                 number_19,
                                 number_20,
                                 date_21,
                                 date_22
                            from ordhead_l10n_ext
                           where order_no = I_order_no;
   ---
   --If the elc_ind = 'Y', copy HTS assessments (if the order is an
   --import order),if the elc_ind = 'Y' copy expenses for PO item levels.
   ---
   if L_elc_ind = 'Y' then
      ---
      if L_copy_po_curr_rate = 'N' then
         FOR rec in C_SEQ LOOP
            L_seq_no := rec.seq_no;
            ---- get currency code of current seq_no from ORDLOC_EXP
            SQL_LIB.SET_MARK('OPEN',
                             'C_CURRENCY_ORDEXP',
                             'ORDLOC_EXP',
                             'ORDER_NO: '||to_char(I_order_no));
            open C_CURRENCY_ORDEXP;
            SQL_LIB.SET_MARK('FETCH',
                             'C_CURRENCY_ORDEXP',
                             'ORDLOC_EXP',
                             'ORDER_NO: '||to_char(I_order_no));
            fetch C_CURRENCY_ORDEXP into L_comp_currency;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_CURRENCY_ORDEXP',
                             'ORDLOC_EXP',
                             'ORDER_NO: '||to_char(I_order_no));
            close C_CURRENCY_ORDEXP;

            if L_comp_currency = L_currency_code then
               L_exchange_rate_ordexp := L_exchange_rate;
            elsif L_comp_currency = L_prev_comp_currency then
               L_exchange_rate_ordexp := L_prev_exchange_rate;
            else
               if CURRENCY_SQL.GET_RATE(O_error_message,
                                        L_exchange_rate_ordexp,
                                        L_comp_currency,
                                        'P',
                                        L_vdate) = FALSE then
                  return FALSE;
               end if;
            end if;

            insert into ordloc_exp (order_no,
                                    seq_no,
                                    item,
                                    pack_item,
                                    location,
                                    loc_type,
                                    comp_id,
                                    cvb_code,
                                    cost_basis,
                                    comp_rate,
                                    comp_currency,
                                    exchange_rate,
                                    per_count,
                                    per_count_uom,
                                    est_exp_value,
                                    nom_flag_1,
                                    nom_flag_2,
                                    nom_flag_3,
                                    nom_flag_4 ,
                                    nom_flag_5,
                                    origin,
                                    display_order,
                                    defaulted_from,
                                    key_value_1,
                                    key_value_2)
                             select O_order_no,
                                    seq_no,
                                    item,
                                    pack_item,
                                    location,
                                    loc_type,
                                    comp_id,
                                    cvb_code,
                                    cost_basis,
                                    comp_rate,
                                    comp_currency,
                                    L_exchange_rate_ordexp,
                                    per_count,
                                    per_count_uom,
                                    est_exp_value,
                                    nom_flag_1,
                                    nom_flag_2,
                                    nom_flag_3,
                                    nom_flag_4 ,
                                    nom_flag_5,
                                    origin,
                                    display_order,
                                    defaulted_from,
                                    key_value_1,
                                    key_value_2
                               from ordloc_exp
                              where order_no = I_order_no
                                and seq_no = rec.seq_no;

            ---- save previous currency_code and exchange rate
            L_prev_comp_currency := L_comp_currency;
            L_prev_exchange_rate := L_exchange_rate_ordexp;
         END LOOP;
      else

         FOR rec in C_SEQ LOOP
            insert into ordloc_exp (order_no,
                                    seq_no,
                                    item,
                                    pack_item,
                                    location,
                                    loc_type,
                                    comp_id,
                                    cvb_code,
                                    cost_basis,
                                    comp_rate,
                                    comp_currency,
                                    exchange_rate,
                                    per_count,
                                    per_count_uom,
                                    est_exp_value,
                                    nom_flag_1,
                                    nom_flag_2,
                                    nom_flag_3,
                                    nom_flag_4 ,
                                    nom_flag_5,
                                    origin,
                                    display_order,
                                    defaulted_from,
                                    key_value_1,
                                    key_value_2)
                             select O_order_no,
                                    seq_no,
                                    item,
                                    pack_item,
                                    location,
                                    loc_type,
                                    comp_id,
                                    cvb_code,
                                    cost_basis,
                                    comp_rate,
                                    comp_currency,
                                    exchange_rate,
                                    per_count,
                                    per_count_uom,
                                    est_exp_value,
                                    nom_flag_1,
                                    nom_flag_2,
                                    nom_flag_3,
                                    nom_flag_4 ,
                                    nom_flag_5,
                                    origin,
                                    display_order,
                                    defaulted_from,
                                    key_value_1,
                                    key_value_2
                               from ordloc_exp
                              where order_no = I_order_no
                                and seq_no = rec.seq_no;
         END LOOP;
      end if; /* L_copy_po_curr_rate = 'N' */
   end if; /*  L_elc_ind = 'Y'  */

   if L_elc_ind = 'Y' and L_import_ind = 'Y' and L_order_import_ind = 'Y' then
      ---
      FOR rec_hts in C_HTS_SEQ LOOP
         insert into ordsku_hts (order_no,
                                 seq_no,
                                 item,
                                 pack_item,
                                 hts,
                                 import_country_id,
                                 effect_from,
                                 effect_to,
                                 status,
                                 origin_country_id)
                          select O_order_no,
                                 seq_no,
                                 item,
                                 pack_item,
                                 hts,
                                 import_country_id,
                                 effect_from,
                                 effect_to,
                                 status,
                                 origin_country_id
                            from ordsku_hts
                           where order_no = I_order_no
                             and seq_no = rec_hts.seq_no;
      END LOOP;

      FOR rec_assess in C_ASSESS_SEQ LOOP
         insert into ordsku_hts_assess (order_no,
                                        seq_no,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        per_count,
                                        per_count_uom,
                                        est_assess_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order)
                                 select O_order_no,
                                        seq_no,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        per_count,
                                        per_count_uom,
                                        est_assess_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order
                                   from ordsku_hts_assess
                                  where order_no = I_order_no
                                    and seq_no = rec_assess.seq_no
                                    and comp_id = rec_assess.comp_id;

      END LOOP;
   end if;
      ---
   --Loop through PO items of existing order and for each one copy
   --documents to new PO item.

   FOR C_rec in C_ORDSKU LOOP

      if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                    'POIT',
                                    'POIT',
                                    I_order_no,
                                    O_order_no,
                                    C_rec.item,
                                    C_rec.item) = FALSE then
         return FALSE;
      end if;

   END LOOP;
   ---
   --Copy PO level documents.
   if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                 'PO',
                                 'PO',
                                 I_order_no,
                                 O_order_no,
                                 NULL,
                                 NULL) = FALSE then
       return FALSE;
   end if;
   ---
   -- Logistics Modification -- when create from existing, also create the workorders
   -- which were already associated with the original order.
   ---
   FOR rec in C_GET_WO_ID LOOP
   ---
      if WO_ATTRIB_SQL.NEXT_WO_ID(O_error_message,
                                  L_wo_id) = FALSE then
       return FALSE;
      end if;
      ---
      insert into wo_head (wo_id,
                           tsfalloc_no,
                           order_no)
                   values (L_wo_id,
                           NULL,
                           O_order_no);

      insert into wo_detail(wo_id,
                            wh,
                            item,
                            location,
                            loc_type,
                            seq_no,
                            wip_code)
                     select L_wo_id,
                            w.wh,
                            w.item,
                            w.location,
                            w.loc_type,
                            w.seq_no,
                            w.wip_code
                       from wo_detail w,
                            ordsku o
                      where w.wo_id = rec.wo_id
                        and o.order_no = O_order_no
                        and w.item = o.item;
   ---
   END LOOP;
   ---
   -- Functional Modification -- insert ordcust records associated with order
   open C_ORDCUST_EXISTS;
   fetch C_ORDCUST_EXISTS into L_ordcust_no;
   close C_ORDCUST_EXISTS;
   ---
   if L_ordcust_no is not NULL then  /* if an ordcust record exists, then insert it! */

      -- get unique ordcust_no
      open C_CUST_SEQ;
      fetch C_CUST_SEQ into L_ordcust_no_seq;
      close C_CUST_SEQ;
      ---
      insert into ordcust(ordcust_no,
                          status,
                          order_no,
                          tsf_no,
                          source_loc_type,
                          source_loc_id,
                          fulfill_loc_type,
                          fulfill_loc_id,
                          customer_no,
                          customer_order_no,
                          fulfill_order_no,
                          partial_delivery_ind,
                          delivery_type,
                          carrier_code,
                          carrier_service_code,
                          consumer_delivery_date,
                          consumer_delivery_time,
                          bill_first_name,
                          bill_phonetic_first,
                          bill_last_name,
                          bill_phonetic_last,
                          bill_preferred_name,
                          bill_company_name,
                          bill_add1,
                          bill_add2,
                          bill_add3,
                          bill_county,
                          bill_city,
                          bill_state,
                          bill_country_id,
                          bill_post,
                          bill_jurisdiction,
                          bill_phone,
                          deliver_first_name,
                          deliver_phonetic_first,
                          deliver_last_name,
                          deliver_phonetic_last,
                          deliver_preferred_name,
                          deliver_company_name,
                          deliver_add1,
                          deliver_add2,
                          deliver_add3,
                          deliver_city,
                          deliver_state,
                          deliver_country_id,
                          deliver_post,
                          deliver_county,
                          deliver_jurisdiction,
                          deliver_phone,
                          deliver_charge,
                          deliver_charge_curr,
                          comments,
                          create_datetime,
                          create_id,
                          last_update_datetime,
                          last_update_id)
                   select L_ordcust_no_seq,
                          status,
                          O_order_no,
                          NULL,
                          source_loc_type,
                          source_loc_id,
                          fulfill_loc_type,
                          fulfill_loc_id,
                          customer_no,
                          O_order_no,
                          O_order_no,
                          partial_delivery_ind,
                          delivery_type,
                          carrier_code,
                          carrier_service_code,
                          NULL,
                          NULL,
                          bill_first_name,
                          bill_phonetic_first,
                          bill_last_name,
                          bill_phonetic_last,
                          bill_preferred_name,
                          bill_company_name,
                          bill_add1,
                          bill_add2,
                          bill_add3,
                          bill_county,
                          bill_city,
                          bill_state,
                          bill_country_id,
                          bill_post,
                          bill_jurisdiction,
                          bill_phone,
                          deliver_first_name,
                          deliver_phonetic_first,
                          deliver_last_name,
                          deliver_phonetic_last,
                          deliver_preferred_name,
                          deliver_company_name,
                          deliver_add1,
                          deliver_add2,
                          deliver_add3,
                          deliver_city,
                          deliver_state,
                          deliver_country_id,
                          deliver_post,
                          deliver_county,
                          deliver_jurisdiction,
                          deliver_phone,
                          deliver_charge,
                          deliver_charge_curr,
                          NULL,
                          SYSDATE,
                          GET_USER,
                          SYSDATE,
                          GET_USER
                     from ordcust
                    where order_no = I_order_no;
      ---
      insert into ordcust_detail(ordcust_no,
                                 item,
                                 ref_item,
                                 original_item,
                                 qty_ordered_suom,
                                 qty_cancelled_suom,
                                 standard_uom,
                                 transaction_uom,
                                 substitute_allowed_ind,
                                 unit_retail,
                                 retail_currency_code,
                                 comments,
                                 create_datetime,
                                 create_id,
                                 last_update_datetime,
                                 last_update_id)
                          select L_ordcust_no_seq,
                                 item,
                                 ref_item,
                                 NULL,
                                 qty_ordered_suom,
                                 NULL,
                                 standard_uom,
                                 transaction_uom,
                                 substitute_allowed_ind,
                                 unit_retail,
                                 retail_currency_code,
                                 NULL,
                                 SYSDATE,
                                 GET_USER,
                                 SYSDATE,
                                 GET_USER
                            from ordcust_detail
                           where ordcust_no = L_ordcust_no;

   end if; /* L_ordcust_no is not null */
   ---
   -- copy over order inventory management record
   ---
   insert into ord_inv_mgmt(order_no,
                            pool_supplier,
                            file_id,
                            scale_cnstr_ind,
                            scale_cnstr_lvl,
                            scale_cnstr_obj,
                            scale_cnstr_type1,
                            scale_cnstr_uom1,
                            scale_cnstr_curr1,
                            scale_cnstr_min_val1,
                            scale_cnstr_max_val1,
                            scale_cnstr_min_tol1,
                            scale_cnstr_max_tol1,
                            scale_cnstr_type2,
                            scale_cnstr_uom2,
                            scale_cnstr_curr2,
                            scale_cnstr_min_val2,
                            scale_cnstr_max_val2,
                            scale_cnstr_min_tol2,
                            scale_cnstr_max_tol2,
                            scale_cnstr_chg_ind,
                            max_scaling_iterations,
                            truck_split_ind,
                            truck_split_method,
                            truck_cnstr_type1,
                            truck_cnstr_uom1,
                            truck_cnstr_val1,
                            truck_cnstr_tol1,
                            truck_cnstr_type2,
                            truck_cnstr_uom2,
                            truck_cnstr_val2,
                            truck_cnstr_tol2,
                            truck_split_issues,
                            ltl_approval_ind,
                            ltl_ind,
                            min_cnstr_lvl,
                            min_cnstr_conj,
                            min_cnstr_type1,
                            min_cnstr_uom1,
                            min_cnstr_curr1,
                            min_cnstr_val1,
                            min_cnstr_type2,
                            min_cnstr_uom2,
                            min_cnstr_curr2,
                            min_cnstr_val2,
                            due_ord_process_ind,
                            due_ord_ind,
                            due_ord_lvl,
                            due_ord_serv_basis,
                            mult_vehicle_ind,
                            no_vehicles,
                            single_loc_ind,
                            ord_purge_ind,
                            ord_approve_ind,
                            scale_issues)
                     select O_order_no,
                            pool_supplier,
                            NULL,
                            scale_cnstr_ind,
                            scale_cnstr_lvl,
                            scale_cnstr_obj,
                            scale_cnstr_type1,
                            scale_cnstr_uom1,
                            scale_cnstr_curr1,
                            scale_cnstr_min_val1,
                            scale_cnstr_max_val1,
                            scale_cnstr_min_tol1,
                            scale_cnstr_max_tol1,
                            scale_cnstr_type2,
                            scale_cnstr_uom2,
                            scale_cnstr_curr2,
                            scale_cnstr_min_val2,
                            scale_cnstr_max_val2,
                            scale_cnstr_min_tol2,
                            scale_cnstr_max_tol2,
                            DECODE(scale_cnstr_ind, 'Y', 'Y', 'N'),  -- scale_cnstr_chg_ind
                            max_scaling_iterations,
                            truck_split_ind,
                            truck_split_method,
                            truck_cnstr_type1,
                            truck_cnstr_uom1,
                            truck_cnstr_val1,
                            truck_cnstr_tol1,
                            truck_cnstr_type2,
                            truck_cnstr_uom2,
                            truck_cnstr_val2,
                            truck_cnstr_tol2,
                            NULL,
                            'N',
                            'N',
                            min_cnstr_lvl,
                            min_cnstr_conj,
                            min_cnstr_type1,
                            min_cnstr_uom1,
                            min_cnstr_curr1,
                            min_cnstr_val1,
                            min_cnstr_type2,
                            min_cnstr_uom2,
                            min_cnstr_curr2,
                            min_cnstr_val2,
                            'N',                -- due_ord_process_ind
                            'N',                -- due_ord_ind
                            NULL,               -- due_ord_lvl
                            NULL,           -- due_ord_serv_basis
                            mult_vehicle_ind,
                            NULL,               -- no_vehicles
                            single_loc_ind,
                            'N',                -- ord_purge_ind
                            'N',                -- order approve ind
                            NULL                -- scale_issues
                       from ord_inv_mgmt
                      where order_no = I_order_no;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_FROM_EXISTING;
-------------------------------------------------------------------------------
FUNCTION GET_CUST_LOCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_cust_store      IN OUT   ORDLOC.LOCATION%TYPE,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_location ORDLOC.LOCATION%TYPE;
   L_loc_type ORDLOC.LOC_TYPE%TYPE;
   L_xdock_wh WH.WH%TYPE;

   cursor C_LOCATION is
      select location
        from ordloc_wksht
       where order_no = I_order_no
         and location is not null;

   cursor C_ORDLOC is
      select location,
             loc_type
        from ordloc
       where order_no = I_order_no;

   cursor C_ALLOC_STORE is
      select to_loc
        from alloc_detail ad,
             alloc_header ah
       where ad.alloc_no = ah.alloc_no
         and ah.order_no = I_order_no
         and ah.wh       = L_location;

BEGIN
   open C_ORDLOC;
   fetch C_ORDLOC into L_location,
                       L_loc_type;
   if C_ORDLOC%NOTFOUND then
      open C_LOCATION;
      fetch C_LOCATION into O_cust_store;
      close C_LOCATION;
   else
      if L_loc_type = 'W' then
         open C_ALLOC_STORE;
         fetch C_ALLOC_STORE into O_cust_store;
         close C_ALLOC_STORE;
      else /* loc_type = 'S' */
         O_cust_store := L_Location;
      end if;
   end if;
   close C_ORDLOC;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.GET_CUST_LOCS',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_CUST_LOCS;
-------------------------------------------------------------------------------
FUNCTION CALC_ORDER_QTYS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_calc_qty        IN OUT   ORDLOC_WKSHT.CALC_QTY%TYPE,
                         O_act_qty         IN OUT   ORDLOC_WKSHT.ACT_QTY%TYPE,
                         O_wksht_qty       IN OUT   ORDLOC_WKSHT.WKSHT_QTY%TYPE,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                         I_where_clause    IN       FILTER_TEMP.WHERE_CLAUSE%TYPE)
   RETURN BOOLEAN IS

   C_GET_QTYS        INTEGER;
   C_GET_UOPS        INTEGER;
   L_dummy           INTEGER;
   L_where_clause    FILTER_TEMP.WHERE_CLAUSE%TYPE;
   L_text            VARCHAR2(9000); /* Size must be larger than FILTER_TEMP.WHERE_CLAUSE%TYPE */

BEGIN

   C_GET_UOPS := DBMS_SQL.OPEN_CURSOR;

   if I_where_clause is not NULL then
      L_where_clause := ' and '||I_where_clause;
   end if;

   L_text := 'select ''x'' from ordloc_wksht ' ||
             'where ordloc_wksht.order_no = to_char(:order_no)' ||
             ' and exists (select ''x'' '||
                           'from ordloc_wksht b ' ||
                          'where b.order_no = ordloc_wksht.order_no ' ||
                            'and (b.standard_uom <> ordloc_wksht.standard_uom or ' ||
                                 'b.uop <> ordloc_wksht.uop)) ' ||
             'and rownum = 1' || L_where_clause;

   DBMS_SQL.PARSE(C_GET_UOPS,L_text,dbms_sql.native);
   DBMS_SQL.BIND_VARIABLE( C_GET_UOPS, ':order_no', I_order_no );
   L_dummy := DBMS_SQL.EXECUTE(C_GET_UOPS);

   if DBMS_SQL.FETCH_ROWS   (C_GET_UOPS) > 0 then
      O_calc_qty := NULL;
      O_act_qty := NULL;
      O_wksht_qty := NULL;

      DBMS_SQL.CLOSE_CURSOR(C_GET_UOPS);

      return TRUE;
   end if;

   DBMS_SQL.CLOSE_CURSOR(C_GET_UOPS);

   C_GET_QTYS := DBMS_SQL.OPEN_CURSOR;

   L_text := 'select sum(NVL(calc_qty, 0)), sum(NVL(act_qty, 0)), sum(NVL(wksht_qty, 0)) from ordloc_wksht where order_no = to_char(:order_no) '||L_where_clause;

   DBMS_SQL.PARSE(C_GET_QTYS,L_text,dbms_sql.native);
   DBMS_SQL.BIND_VARIABLE( C_GET_QTYS, ':order_no', I_order_no );
   DBMS_SQL.DEFINE_COLUMN (C_GET_QTYS, 1, O_calc_qty);
   DBMS_SQL.DEFINE_COLUMN (C_GET_QTYS, 2, O_act_qty);
   DBMS_SQL.DEFINE_COLUMN (C_GET_QTYS, 3, O_wksht_qty);

   L_dummy := DBMS_SQL.EXECUTE(C_GET_QTYS);

   if DBMS_SQL.FETCH_ROWS   (C_GET_QTYS) > 0 then
      DBMS_SQL.COLUMN_VALUE (C_GET_QTYS, 1, O_calc_qty);
      DBMS_SQL.COLUMN_VALUE (C_GET_QTYS, 2, O_act_qty);
      DBMS_SQL.COLUMN_VALUE (C_GET_QTYS, 3, O_wksht_qty);
   end if;

   DBMS_SQL.CLOSE_CURSOR(C_GET_QTYS);

   return TRUE;

EXCEPTION
   when VALUE_ERROR then
      O_error_message := SQL_LIB.CREATE_MSG('TOTAL_TOO_LARGE',
                                            NULL,
                                            NULL,
                                            NULL);
      if DBMS_SQL.IS_OPEN (C_GET_QTYS) then
         DBMS_SQL.CLOSE_CURSOR (C_GET_QTYS);
      end if;
      ---
      if DBMS_SQL.IS_OPEN (C_GET_UOPS) then
         DBMS_SQL.CLOSE_CURSOR (C_GET_UOPS);
      end if;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.CALC_ORDER_QTYS',
                                            TO_CHAR(SQLCODE));
      if DBMS_SQL.IS_OPEN (C_GET_QTYS) then
         DBMS_SQL.CLOSE_CURSOR (C_GET_QTYS);
      end if;
      ---
      if DBMS_SQL.IS_OPEN (C_GET_UOPS) then
         DBMS_SQL.CLOSE_CURSOR (C_GET_UOPS);
      end if;
      return FALSE;
END CALC_ORDER_QTYS;
-------------------------------------------------------------------------------------------
FUNCTION CHECK_FILTER_CRITERIA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_filter_ind      IN OUT   VARCHAR2,
                               I_order_no        IN       ORDLOC_WKSHT.ORDER_NO%TYPE,
                               I_item            IN       ORDLOC_WKSHT.ITEM%TYPE,
                               I_where_clause    IN       FILTER_TEMP.WHERE_CLAUSE%TYPE,
                               I_location        IN       ORDLOC_WKSHT.LOCATION%TYPE)
   return BOOLEAN IS

   C_CHECK_ORDLOC_WKSHT        INTEGER;
   L_dummy                     INTEGER;
   L_where_clause              FILTER_TEMP.WHERE_CLAUSE%TYPE;
   L_text                      VARCHAR2(9000); /* Size must be larger than FILTER_TEMP.WHERE_CLAUSE%TYPE */

BEGIN
   O_filter_ind := 'N';

   C_CHECK_ORDLOC_WKSHT := DBMS_SQL.OPEN_CURSOR;

   if I_where_clause is not NULL then
      L_where_clause := ' and '||I_where_clause;
   end if;

   L_text := 'select ''Y'''||
              ' from ordloc_wksht'||
              ' where ((item = :item '||
                     ' and ((location is NULL and nvl(to_char(:location), NULL) is NULL)'||
                           ' or (location = nvl(to_char(:location), NULL)))'||
                     ' and store_grade is NULL'||
                     ' and store_grade_group_id is NULL)'||
                     ' or'||
                     '(item_parent = :item '||
                     ' and ((location is NULL and nvl(to_char(:location), NULL) is NULL)'||
                           ' or (location = nvl(to_char(:location), NULL)))'||
                     ' and store_grade is NULL'||
                     ' and store_grade_group_id is NULL))'||
                     ' and order_no = to_char(:order_no)'||L_where_clause;

   DBMS_SQL.PARSE(C_CHECK_ORDLOC_WKSHT,L_text,dbms_sql.native);
   DBMS_SQL.BIND_VARIABLE( C_CHECK_ORDLOC_WKSHT, ':item', I_item );
   DBMS_SQL.BIND_VARIABLE( C_CHECK_ORDLOC_WKSHT, ':order_no', I_order_no );
   DBMS_SQL.BIND_VARIABLE( C_CHECK_ORDLOC_WKSHT, ':location', I_location );
   DBMS_SQL.DEFINE_COLUMN (C_CHECK_ORDLOC_WKSHT, 1, O_filter_ind, 1);

   L_dummy := DBMS_SQL.EXECUTE(C_CHECK_ORDLOC_WKSHT);

   if DBMS_SQL.FETCH_ROWS   (C_CHECK_ORDLOC_WKSHT) > 0 then
      DBMS_SQL.COLUMN_VALUE (C_CHECK_ORDLOC_WKSHT, 1, O_filter_ind);
   end if;

   DBMS_SQL.CLOSE_CURSOR(C_CHECK_ORDLOC_WKSHT);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.CHECK_FILTER_CRITERIA',
                                            TO_CHAR(SQLCODE));
      if DBMS_SQL.IS_OPEN (C_CHECK_ORDLOC_WKSHT) then
         DBMS_SQL.CLOSE_CURSOR (C_CHECK_ORDLOC_WKSHT);
      end if;
      return FALSE;
END CHECK_FILTER_CRITERIA;
-------------------------------------------------------------------------------
FUNCTION UPDATE_SHIP_DATES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_earliest_ship_date        ORDSKU.EARLIEST_SHIP_DATE%TYPE;
   L_latest_ship_date          ORDSKU.LATEST_SHIP_DATE%TYPE;
   L_table                     VARCHAR2(30)                   :=    'ORDHEAD';
   RECORD_LOCKED               EXCEPTION;
   PRAGMA                      EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_LOCK_HEAD is
      select 'x'
        from ordhead
       where order_no = I_order_no
         and (nvl(earliest_ship_date, L_earliest_ship_date - 1) != L_earliest_ship_date or
              nvl(latest_ship_date, L_latest_ship_date - 1) != L_latest_ship_date)
         for update nowait;
   ---
   cursor C_GET_SHIP_DATES is
      select MIN(earliest_ship_date),
             MAX(latest_ship_date)
        from ordsku
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_SHIP_DATES', 'ORDSKU', 'order_no: '||to_char(I_order_no));
   open  C_GET_SHIP_DATES;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_SHIP_DATES', 'ORDSKU', 'order_no: '||to_char(I_order_no));
   fetch C_GET_SHIP_DATES into L_earliest_ship_date,
                               L_latest_ship_date;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_SHIP_DATES', 'ORDSKU', 'order_no: '||to_char(I_order_no));
   close C_GET_SHIP_DATES;
   ---
   --If skus exist on the order, update ship dates on ordhead.
   if L_earliest_ship_date is not NULL and
      L_latest_ship_date is not NULL then
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_HEAD', 'ORDHEAD', 'order_no: '||to_char(I_order_no));
      open  C_LOCK_HEAD;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_HEAD', 'ORDHEAD', 'order_no: '||to_char(I_order_no));
      close C_LOCK_HEAD;
      ---
      update ordhead
            set earliest_ship_date   = L_earliest_ship_date,
                latest_ship_date     = L_latest_ship_date,
                last_update_id       = get_user,
                last_update_datetime = sysdate
          where order_no = I_order_no
            and (nvl(earliest_ship_date, L_earliest_ship_date - 1) != L_earliest_ship_date or
                 nvl(latest_ship_date, L_latest_ship_date - 1) != L_latest_ship_date);
   end if;
      ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ORDER_LOCKED',
                                            TO_CHAR(I_order_no),
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.UPDATE_SHIP_DATES',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_SHIP_DATES;
-------------------------------------------------------------------------------
FUNCTION UPDATE_IMPORT_COUNTRY(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                               I_import_country_id   IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(60)   := 'ORDER_SETUP_SQL.UPDATE_IMPORT_COUNTRY';

   L_table         VARCHAR2(30)   :=    'ORDHEAD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_LOCK_HEAD is
      select 'x'
        from ordhead
       where order_no = I_order_no
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_HEAD', L_table, 'order_no: '||TO_CHAR(I_order_no));
   open  C_LOCK_HEAD;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_HEAD', L_table, 'order_no: '||TO_CHAR(I_order_no));
   close C_LOCK_HEAD;
   ---
   update ordhead
      set import_country_id    = I_import_country_id,
          last_update_id       = get_user,
          last_update_datetime = sysdate
    where order_no = I_order_no;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ORDER_LOCKED',
                                            TO_CHAR(I_order_no),
                                            L_program,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_IMPORT_COUNTRY;
-------------------------------------------------------------------------------
FUNCTION APPLY_CHANGES(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                       I_item_parent      IN       ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                       I_diff1            IN       ORDLOC_WKSHT.DIFF_1%TYPE,
                       I_diff2            IN       ORDLOC_WKSHT.DIFF_2%TYPE,
                       I_diff3            IN       ORDLOC_WKSHT.DIFF_3%TYPE,
                       I_diff4            IN       ORDLOC_WKSHT.DIFF_4%TYPE,
                       I_item             IN       ORDLOC_WKSHT.ITEM%TYPE,
                       I_origin_country   IN       ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE,
                       I_supplier         IN       ORDHEAD.SUPPLIER%TYPE DEFAULT NULL,
                       I_override         IN       VARCHAR2 DEFAULT 'N')
   return BOOLEAN is

   L_program              VARCHAR2(40) := 'ORDER_SETUP_SQL.APPLY_CHANGES';
   L_order_type           ORDHEAD.ORDER_TYPE%TYPE := NULL;
   L_supp_pack_size       ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE := NULL;
   L_item                 ORDLOC_WKSHT.ITEM%TYPE := NULL;
   L_inner_pack_size      ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE := NULL;
   L_pallet_desc          CODE_DETAIL.CODE_DESC%TYPE := NULL;
   L_case_desc            CODE_DETAIL.CODE_DESC%TYPE := NULL;
   L_inner_desc           CODE_DETAIL.CODE_DESC%TYPE := NULL;
   L_pallet_name          ITEM_SUPPLIER.PALLET_NAME%TYPE := NULL;
   L_case_name            ITEM_SUPPLIER.CASE_NAME%TYPE := NULL;
   L_inner_name           ITEM_SUPPLIER.INNER_NAME%TYPE := NULL;
   L_supp_pack_size_upd   ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE := NULL;

BEGIN

   if I_order_no is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_order_no',
                                             L_program,
                                             NULL);
   end if;

   if I_item is NOT NULL then
      L_item := I_item;
   else
      L_item := I_item_parent;
   end if;

   -- retrieve the L_order_type value for use later.
   if ORDER_ATTRIB_SQL.GET_ORDER_TYPE(O_error_message,
                                      L_order_type,
                                      I_order_no) = FALSE then
      return FALSE;
   end if;

   -- retrieve the pack size information for the origin country
   if SUPP_ITEM_ATTRIB_SQL.GET_PACK_SIZES(O_error_message,
                                          L_supp_pack_size,
                                          L_inner_pack_size,
                                          L_pallet_desc,
                                          L_case_desc,
                                          L_inner_desc,
                                          L_pallet_name,
                                          L_case_name,
                                          L_inner_name,
                                          L_item,
                                          I_supplier,
                                          I_origin_country) = FALSE then
      return FALSE;
   end if;

   if L_order_type = 'CO' then
      L_supp_pack_size_upd := 1;
   else
      if I_override = 'N' then
         L_supp_pack_size_upd := L_supp_pack_size;
      else
         L_supp_pack_size_upd := NULL;
      end if;
   end if;

   -- apply the changes
   if I_item is not NULL then
      ---
      update ordloc_wksht
         set origin_country_id = I_origin_country,
             supp_pack_size    = NVL(L_supp_pack_size_upd, supp_pack_size)
      where order_no           = I_order_no
         and item              = I_item;
   elsif I_item is NULL then
      ---
      update ordloc_wksht
         set origin_country_id  = I_origin_country,
             supp_pack_size     = NVL(L_supp_pack_size_upd, supp_pack_size)
       where order_no           = I_order_no
         and item_parent        = I_item_parent
         and (diff_1            = I_diff1
              or diff_1 is NULL and I_diff1 is NULL)
         and (diff_2            = I_diff2
              or diff_2 is NULL and I_diff2 is NULL)
         and (diff_3            = I_diff3
              or diff_3 is NULL and I_diff3 is NULL)
         and (diff_4            = I_diff4
              or diff_4 is NULL and I_diff4 is NULL);
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.APPLY_CHANGES',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END APPLY_CHANGES;
------------------------------------------------------------------------------
FUNCTION POP_CHECK_CHILD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_rejected   IN OUT   BOOLEAN,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_item_parent            ITEM_MASTER.ITEM_PARENT%TYPE;
   L_item                   ITEM_MASTER.ITEM%TYPE;
   L_contract_item          ITEM_MASTER.ITEM%TYPE;
   L_diff_1                 ITEM_MASTER.DIFF_1%TYPE;
   L_diff_2                 ITEM_MASTER.DIFF_2%TYPE;
   L_diff_3                 ITEM_MASTER.DIFF_3%TYPE;
   L_diff_4                 ITEM_MASTER.DIFF_4%TYPE;
   L_status                 ITEM_MASTER.STATUS%TYPE;
   L_qty                    ORDDIST_ITEM_TEMP.QTY%TYPE;
   L_origin_country_id      ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_supplier               ORDHEAD.SUPPLIER%TYPE;
   L_ordsku_exists          BOOLEAN;
   L_supp_exists            BOOLEAN;
   L_country_exists         BOOLEAN;
   L_prim_country_exists    BOOLEAN;
   L_prim_origin_country    ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_origin_country_ordsku  ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_origin_country_ordwks  ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_contract_no            ORDHEAD.CONTRACT_NO%TYPE;

   cursor C_CHECK_PARENT is
      select distinct ow.item_parent,
             ow.diff_1,
             ow.diff_2,
             ow.diff_3,
             ow.diff_4,
             ow.origin_country_id,
             ow.wksht_qty
        from ordloc_wksht ow,
             item_master im
       where ow.order_no = I_order_no
         and ow.item_parent is NOT NULL
         and ow.item is NULL
         and ow.item_parent = im.item
         and ow.diff_1 is NOT NULL
         and im.diff_1 is NOT NULL
         and ((ow.diff_2 is NOT NULL
              and im.diff_2 is NOT NULL)
           or (ow.diff_2 is NULL
               and im.diff_2 is NULL))
         and ((ow.diff_3 is NOT NULL
              and im.diff_3 is NOT NULL)
           or (ow.diff_3 is NULL
               and im.diff_3 is NULL))
         and ((ow.diff_4 is NOT NULL
              and im.diff_4 is NOT NULL)
           or (ow.diff_4 is NULL
               and im.diff_4 is NULL));

   cursor C_SUPP is
      select supplier,
             contract_no
        from ordhead
       where order_no = I_order_no;

   cursor C_CHECK_ITEM is
      select item, status
        from item_master
       where item_parent = L_item_parent
         and diff_1 = L_diff_1
         and (diff_2 = L_diff_2
          or (diff_2 is NULL and L_diff_2 is NULL))
         and (diff_3 = L_diff_3
          or (diff_3 is NULL and L_diff_3 is NULL))
         and (diff_4 = L_diff_4
          or (diff_4 is NULL and L_diff_4 is NULL));

   cursor C_CHECK_ITEM_CONTRACT is
      select distinct im.item
        from contract_detail co,
             v_item_master im
       where co.contract_no = L_contract_no
         and im.item= L_item
         and im.orderable_ind = 'Y'
         and nvl(im.deposit_item_type, 'X') != 'A'
         and im.sellable_ind = 'Y'
         and ((co.item_level_index = 1
               and co.item = im.item)
          or (co.item_level_index = 2
             and co.diff_1 = im.diff_1
             and (co.item_parent = im.item_parent
                  or co.item_grandparent = im.item_grandparent))
          or (co.item_level_index = 3
             and co.diff_2 = im.diff_2
             and (co.item_parent = im.item_parent
                  or co.item_grandparent = im.item_grandparent))
          or (co.item_level_index = 4
             and co.diff_3 = im.diff_3
             and (co.item_parent = im.item_parent
                  or co.item_grandparent = im.item_grandparent))
          or (co.item_level_index = 5
             and co.diff_4 = im.diff_4
             and (co.item_parent = im.item_parent
                  or co.item_grandparent = im.item_grandparent))
          or (co.item_level_index = 6
             and (co.item_parent = im.item_parent
                  or co.item_grandparent = im.item_grandparent)));

BEGIN

   open C_SUPP;
   fetch C_SUPP into L_supplier, L_contract_no;
   close C_SUPP;

   O_item_rejected := FALSE;

   for rec in C_CHECK_PARENT LOOP
      L_item_parent := rec.item_parent;
      L_diff_1 := rec.diff_1;
      L_diff_2 := rec.diff_2;
      L_diff_3 := rec.diff_3;
      L_diff_4 := rec.diff_4;
      L_origin_country_id := rec.origin_country_id;
      L_qty := rec.wksht_qty;
      L_item := NULL;
      L_status := NULL;
      L_contract_item := NULL;

      open C_CHECK_ITEM;
      fetch C_CHECK_ITEM into L_item, L_status;
      close C_CHECK_ITEM;

      if L_contract_no is NOT NULL THEN
         if L_item is NOT NULL then
            open C_CHECK_ITEM_CONTRACT;
            fetch C_CHECK_ITEM_CONTRACT into L_contract_item;
            close C_CHECK_ITEM_CONTRACT;
            L_item := L_contract_item;
         end if;
      end if;

      if L_item is NULL then
         insert into orddist_item_temp(order_no,
                                       item_parent,
                                       item,
                                       diff_1,
                                       diff_2,
                                       diff_3,
                                       diff_4,
                                       qty,
                                       reason)
                                values(I_order_no,
                                       L_item_parent,
                                       NULL,
                                       L_diff_1,
                                       L_diff_2,
                                       L_diff_3,
                                       L_diff_4,
                                       L_qty,
                                       'INV_ITEM_DIFF_COMBO');
         O_item_rejected := TRUE;
      else -- Item does exist
         if L_status != 'A' then
            insert into orddist_item_temp(order_no,
                                          item_parent,
                                          item,
                                          diff_1,
                                          diff_2,
                                          diff_3,
                                          diff_4,
                                          qty,
                                          reason)
                                   values(I_order_no,
                                          L_item_parent,
                                          L_item,
                                          L_diff_1,
                                          L_diff_2,
                                          L_diff_3,
                                          L_diff_4,
                                          L_qty,
                                          'ITEM_APP_ORDER');
            O_item_rejected := TRUE;
         else
            if ORDER_ITEM_ATTRIB_SQL.GET_ORIGIN_COUNTRY(O_error_message,
                                                        L_ordsku_exists,
                                                        L_origin_country_ordsku,
                                                        I_order_no,
                                                        L_item) = FALSE then
               return FALSE;
            end if;
            if L_ordsku_exists then
                  update ordloc_wksht
                     set origin_country_id = L_origin_country_ordsku,
                         item = L_item
                   where order_no = I_order_no
                     and item_parent = L_item_parent
                     and diff_1 = L_diff_1
                     and (diff_2 = L_diff_2
                      or (diff_2 is NULL and L_diff_2 is NULL))
                     and (diff_3 = L_diff_3
                      or (diff_3 is NULL and L_diff_3 is NULL))
                     and (diff_4 = L_diff_4
                      or (diff_4 is NULL and L_diff_4 is NULL));
            else
               if NOT SUPP_ITEM_SQL.EXIST(O_error_message,
                                          L_supp_exists,
                                          L_item,
                                          L_supplier) then
                  return FALSE;
               end if;

               if NOT L_supp_exists then
                  insert into orddist_item_temp(order_no,
                                                item_parent,
                                                item,
                                                diff_1,
                                                diff_2,
                                                diff_3,
                                                diff_4,
                                                qty,
                                                reason)
                                         values(I_order_no,
                                                L_item_parent,
                                                L_item,
                                                L_diff_1,
                                                L_diff_2,
                                                L_diff_3,
                                                L_diff_4,
                                                L_qty,
                                                'INV_SUPP_ORDER');
                  O_item_rejected := TRUE;
               else
                  if NOT SUPP_ITEM_SQL.ITEM_SUPP_COUNTRY_EXISTS(O_error_message,
                                                                L_country_exists,
                                                                L_item,
                                                                L_supplier,
                                                                L_origin_country_id) then
                     return FALSE;
                  end if;

                  if L_country_exists then
                     update ordloc_wksht
                        set item = L_item
                      where order_no = I_order_no
                        and item_parent = L_item_parent
                        and diff_1 = L_diff_1
                        and (diff_2 = L_diff_2
                         or (diff_2 is NULL and L_diff_2 is NULL))
                        and (diff_3 = L_diff_3
                         or (diff_3 is NULL and L_diff_3 is NULL))
                        and (diff_4 = L_diff_4
                         or (diff_4 is NULL and L_diff_4 is NULL));
                     ---
                  else ---  item/supp/country doesn't exist
                     if NOT ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                                      L_prim_country_exists,
                                                                      L_prim_origin_country,
                                                                      L_item,
                                                                      L_supplier) then
                        return FALSE;
                     end if;
                     if NOT L_prim_country_exists then
                        insert into orddist_item_temp(order_no,
                                                      item_parent,
                                                      item,
                                                      diff_1,
                                                      diff_2,
                                                      diff_3,
                                                      diff_4,
                                                      qty,
                                                      reason)
                                               values(I_order_no,
                                                      L_item_parent,
                                                      L_item,
                                                      L_diff_1,
                                                      L_diff_2,
                                                      L_diff_3,
                                                      L_diff_4,
                                                      L_qty,
                                                     'INV_SUPP_ORDER');
                        O_item_rejected := TRUE;
                     else
                        update ordloc_wksht
                           set origin_country_id = L_prim_origin_country,
                               item = L_item
                         where order_no = I_order_no
                           and item_parent = L_item_parent
                           and diff_1 = L_diff_1
                           and (diff_2 = L_diff_2
                            or (diff_2 is NULL and L_diff_2 is NULL))
                           and (diff_3 = L_diff_3
                            or (diff_3 is NULL and L_diff_3 is NULL))
                           and (diff_4 = L_diff_4
                            or (diff_4 is NULL and L_diff_4 is NULL));
                     end if;  --prim country exists
                  end if;  -- item/supp/ordloc_wksht country exists
               end if;  -- item/order supp exists
            end if;  -- item/ordsku exists
         end if;  -- item is approved.
      end if;  -- item exists
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.POP_CHECK_CHILD',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_CHECK_CHILD;
------------------------------------------------------------------------------
FUNCTION DELETE_COMPS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                      I_item            IN       ITEM_MASTER.ITEM%TYPE,
                      I_location        IN       COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                      I_loc_type        IN       COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                      I_comp_type       IN       ELC_COMP.COMP_TYPE%TYPE,
                      I_origin          IN       ORDLOC_EXP.ORIGIN%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64) := 'ORDER_SETUP_SQL.DELETE_COMPS';

BEGIN

   if NOT DELETE_COMPS(O_error_message,
                       I_order_no,
                       I_item,
                       NULL,  --I_component_sku (for Pack only)
                       I_location,
                       I_loc_type,
                       I_comp_type,
                       I_origin,
                       NULL) then    -- calling form

      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_COMPS;
------------------------------------------------------------------------------
FUNCTION DELETE_COMPS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                      I_item             IN       ITEM_MASTER.ITEM%TYPE,
                      I_component_item   IN       ITEM_MASTER.ITEM%TYPE,
                      I_location         IN       COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                      I_loc_type         IN       COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                      I_comp_type        IN       ELC_COMP.COMP_TYPE%TYPE,
                      I_origin           IN       ORDLOC_EXP.ORIGIN%TYPE,
                      I_calling_form     IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_calling_form  VARCHAR2(64) := I_calling_form;
   L_program       VARCHAR2(64) := 'ORDER_SETUP_SQL.DELETE_COMPS';
   L_pack_type     ITEM_MASTER.PACK_TYPE%TYPE;
   L_pack_ind      ITEM_MASTER.PACK_IND%TYPE;
   L_item          ITEM_MASTER.ITEM%TYPE;
   L_table         VARCHAR2(20);
   L_prev_approved VARCHAR2(1);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_PACKSKU is
      select item
        from v_packsku_qty
       where pack_no = I_item;

   cursor C_LOCK_ORDLOC_EXP is
      select 'x'
         from ordloc_exp
       where order_no = I_order_no
         for update nowait;

   cursor C_LOCK_ORDSKU_HTS_ASSESS is
      select 'x'
        from ordsku_hts_assess
       where order_no = I_order_no
         for update nowait;

   cursor C_LOCK_ORDSKU_HTS is
      select 'x'
        from ordsku_hts
       where order_no = I_order_no
         for update nowait;

   cursor C_GET_PACK_IND is
      select nvl(pack_type, 'Q')
        from item_master
       where item = I_item;

   -----------------------------------------------------------------
   FUNCTION DELETE_COMPS_LOC(IF_order_no        IN   ORDHEAD.ORDER_NO%TYPE,
                             IF_item            IN   ITEM_MASTER.ITEM%TYPE,
                             IF_pack_no         IN   ITEM_MASTER.ITEM%TYPE,
                             IF_pack_type       IN   ITEM_MASTER.PACK_TYPE%TYPE,
                             IF_location        IN   ORDLOC_EXP.LOCATION%TYPE,
                             IF_calling_form    IN   VARCHAR2,
                             IF_prev_approved   IN   VARCHAR2)
      RETURN BOOLEAN IS

   -- Internal function, called only by delete_comps of this package

      LF_program      VARCHAR2(64)          := 'ORDER_SETUP_SQL.DELETE_COMPS_LOC';
      LF_exists       VARCHAR2(1)           := 'N';
      LF_item_exists  VARCHAR2(1)           := 'N';
      LF_table        VARCHAR2(20);
      RECORD_LOCKED   EXCEPTION;
      PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);


      cursor C_ITEM_EXISTS is
         select 'Y'
           from ordloc
          where location != IF_location
            and order_no  = IF_order_no
            and ((item = IF_item     and IF_pack_type != 'B')
             or ( item = IF_pack_no and IF_pack_type = 'B'));

      cursor C_LOCK_ORDLOC_EXP is
         select 'x'
           from ordloc_exp
          where order_no      = IF_order_no
            and item          = IF_item
            and ((pack_item   is NULL      and IF_pack_type != 'B') or
                 (pack_item   = IF_pack_no and IF_pack_type = 'B'))
            and location      = IF_location
          for update nowait;

      cursor C_LOCK_ORDSKU_HTS_ASSESS is
         select 'x'
           from ordsku_hts_assess
          where order_no      = IF_order_no;

      cursor C_LOCK_ORDSKU_HTS is
         select 'x'
           from ordsku_hts
          where order_no      = IF_order_no
            and item          = IF_item
            and ((pack_item   is NULL      and IF_pack_type != 'B') or
                 (pack_item   = IF_pack_no and IF_pack_type = 'B'));
   BEGIN
      LF_table := 'ordloc_exp';
      open C_LOCK_ORDLOC_EXP;
      close C_LOCK_ORDLOC_EXP;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'ORDLOC_EXP',NULL);
      delete from ordloc_exp
       where order_no      = IF_order_no
         and item          = IF_item
         and ((pack_item   is NULL      and IF_pack_type != 'B')
          or ( pack_item   = IF_pack_no and IF_pack_type = 'B'))
         and location      = IF_location;

      -- Search ordloc to see if the item exists on ordloc for any locations
      -- other than the input location
      SQL_LIB.SET_MARK('OPEN', 'C_ITEM_EXISTS', NULL, NULL);
      open C_ITEM_EXISTS;
      SQL_LIB.SET_MARK('FETCH', 'C_ITEM_EXISTS', NULL, NULL);
      fetch C_ITEM_EXISTS into LF_item_exists;
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM_EXISTS', NULL, NULL);
      close C_ITEM_EXISTS;
      ---
      if LF_item_exists = 'N' then
         -- Delete the hts/assessment components if no other item exist for
         -- any locations other than the input location.  However,
         -- if the order has ever been approved and the calling form is
         -- order redistribution, then do not delete ordsku_hts and ordsku_hts_assess
         -- records.
         if not (lower(IF_calling_form) = 'ordredit' and IF_prev_approved = 'Y') then
            LF_table := 'ORDSKU_HTS_ASSESS';
            open C_LOCK_ORDSKU_HTS_ASSESS;
            close C_LOCK_ORDSKU_HTS_ASSESS;

            SQL_LIB.SET_MARK('DELETE',NULL,'ORDSKU_HTS_ASSESS',NULL);
            delete from ordsku_hts_assess
             where order_no = IF_order_no
               and seq_no   in (select seq_no
                                 from ordsku_hts
                                where order_no    = IF_order_no
                                  and item        = IF_item
                                  and ((pack_item is NULL      and IF_pack_type != 'B')
                                   or ( pack_item = IF_pack_no and IF_pack_type = 'B')));
            ---
            LF_table := 'ORDSKU_HTS';
            open C_LOCK_ORDSKU_HTS;
            close C_LOCK_ORDSKU_HTS;

            SQL_LIB.SET_MARK('DELETE',NULL,'ORDSKU_HTS',NULL);
            delete from ordsku_hts
             where order_no    = IF_order_no
               and item        = IF_item
               and ((pack_item is NULL      and IF_pack_type != 'B')
               or ( pack_item = IF_pack_no and IF_pack_type = 'B'));
         end if;
      end if;
      ---
      return TRUE;

   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                               LF_table,
                                               to_char(IF_order_no),
                                               IF_item);
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               LF_program,
                                               to_char(SQLCODE));
         return FALSE;
   END DELETE_COMPS_LOC;
   ---------------------------------------------------------------------------

BEGIN
   -- if no calling form passed in, populate local variable with text string
   if I_calling_form is NULL then
      L_calling_form := 'no form';
   end if;

   -- check if the order is or has ever been approved.
   if not ORDER_ATTRIB_SQL.PREV_APPROVED(O_error_message,
                                         L_prev_approved,
                                         I_order_no) then
      return FALSE;
   end if;
   ---
   if I_item is NULL then
      if I_comp_type is NULL or I_comp_type = 'E' then
         L_table := 'ORDLOC_EXP';
         open C_LOCK_ORDLOC_EXP;
         close C_LOCK_ORDLOC_EXP;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'ORDLOC_EXP',NULL);
         delete from ordloc_exp
          where order_no = I_order_no
            and ((origin = 'S' and I_origin = 'S')
                 or (I_origin is NULL));
      end if;
      ---
      if I_comp_type is NULL or I_comp_type = 'A' then

         -- if the order has ever been approved and the calling form is
         -- order redistribution, then do not delete ordsku_hts and ordsku_hts_assess
         -- records.
         if not (lower(L_calling_form) = 'ordredit' and L_prev_approved = 'Y') then
            L_table := 'ORDSKU_HTS_ASSESS';
            open C_LOCK_ORDSKU_HTS_ASSESS;
            close C_LOCK_ORDSKU_HTS_ASSESS;
            ---
            SQL_LIB.SET_MARK('DELETE',NULL,'ORDSKU_HTS_ASSESS',NULL);
            delete from ordsku_hts_assess
             where order_no = I_order_no;
            ---
            L_table := 'ORDSKU_HTS';
            open C_LOCK_ORDSKU_HTS;
            close C_LOCK_ORDSKU_HTS;
            ---
            SQL_LIB.SET_MARK('DELETE',NULL,'ORDSKU_HTS',NULL);
            delete from ordsku_hts
             where order_no = I_order_no;
         end if;
      end if;
   else -- I_item is NOT NULL
      SQL_LIB.SET_MARK('OPEN', 'C_GET_PACK_IND','ITEM_MASTER', NULL);
      open C_GET_PACK_IND;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_PACK_IND','ITEM_MASTER', NULL);
      fetch C_GET_PACK_IND into L_pack_type;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_PACK_IND','ITEM_MASTER', NULL);
      close C_GET_PACK_IND;
      ---
      if I_location is NULL then
         if I_comp_type is NULL or I_comp_type = 'E' then
            L_table := 'ORDLOC_EXP';
            open C_LOCK_ORDLOC_EXP;
            close C_LOCK_ORDLOC_EXP;
            ---
            SQL_LIB.SET_MARK('DELETE',NULL,'ORDLOC_EXP',NULL);
            delete from ordloc_exp oe
             where oe.order_no    = I_order_no
               and ((oe.item     = I_item
                      and L_pack_type != 'B' and oe.pack_item is NULL)
                or    exists (select 'x'
                                from item_master im
                               where im.item = oe.item
                                 and (im.item_parent = I_item
                                      or im.item_grandparent = I_item))
                or ( oe.pack_item = I_item and L_pack_type = 'B'
                                and I_component_item is NULL)
            or ( oe.pack_item = I_item and L_pack_type = 'B'
                                        and  oe.item = I_component_item))
                and ((oe.origin = 'S' and I_origin = 'S')
                                    or (I_origin is NULL));
         end if;
         ---
         if I_comp_type is NULL or I_comp_type = 'A' then

            -- if the order has ever been approved and the calling form is
            -- order redistribution, then do not delete ordsku_hts and ordsku_hts_assess
            -- records.
            if not (lower(L_calling_form) = 'ordredit' and L_prev_approved = 'Y') then
               L_table := 'ORDSKU_HTS_ASSESS';
               open C_LOCK_ORDSKU_HTS_ASSESS;
               close C_LOCK_ORDSKU_HTS_ASSESS;
               ---
               SQL_LIB.SET_MARK('DELETE',NULL,'ORDSKU_HTS_ASSESS',NULL);
               delete from ordsku_hts_assess
                where order_no = I_order_no
                  and seq_no   in (select seq_no
                                     from ordsku_hts oh
                                    where oh.order_no    = I_order_no
                                      and ((oh.item     = I_item
                                             and L_pack_type != 'B'
                                             and oh.pack_item is null)
                                       or    exists (select 'x'
                                                       from item_master im
                                                      where im.item = oh.item
                                                        and (im.item_parent = I_item
                                                         or im.item_grandparent = I_item))
                                       or ( oh.pack_item = I_item and L_pack_type = 'B'
                                   and I_component_item is NULL)
                       or ( oh.pack_item = I_item and L_pack_type = 'B'
                                                   and  oh.item = I_component_item)));
               ---
               L_table := 'ORDSKU_HTS';
               open C_LOCK_ORDSKU_HTS;
               close C_LOCK_ORDSKU_HTS;
               ---
               SQL_LIB.SET_MARK('DELETE',NULL,'ORDSKU_HTS',NULL);
               delete from ordsku_hts oh
                where oh.order_no    = I_order_no
                  and ((oh.item     = I_item
                         and L_pack_type != 'B'
                         and oh.pack_item is null)
                   or    exists (select 'x'
                                   from item_master im
                                  where im.item = oh.item
                                    and (im.item_parent = I_item
                                         or im.item_grandparent = I_item))
                   or ( oh.pack_item = I_item and L_pack_type = 'B'
                               and I_component_item is NULL)
                   or ( oh.pack_item = I_item and L_pack_type = 'B'
                                           and  oh.item = I_component_item));
            end if;
         end if;
      else -- I_location is not NULL
         if L_pack_type != 'B' then
            if not DELETE_COMPS_LOC(I_order_no,
                                    I_item,
                                    NULL,
                                    L_pack_type,
                                    I_location,
                                    L_calling_form,
                                    L_prev_approved) then
               return FALSE;
            end if;
         else
            FOR P1 in C_PACKSKU LOOP
               L_item := P1.item;
               if not DELETE_COMPS_LOC(I_order_no,
                                       L_item,
                                       I_item,
                                       L_pack_type,
                                       I_location,
                                       L_calling_form,
                                       L_prev_approved) then
                  return FALSE;
               end if;
            END LOOP;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            TO_CHAR(I_order_no),
                                            I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_COMPS;
-------------------------------------------------------------------------------
FUNCTION EXPLODE_NON_SCALING(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item            IN       ORDLOC.ITEM%TYPE,
                             I_order_no        IN       ORDLOC.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)       := 'ORDER_SETUP_SQL.EXPLODE_NON_SCALING';
   L_table         VARCHAR2(20);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ORDLOC is
      select 'x'
        from ordloc
       where item = I_item
         and order_no = I_order_no
         for update nowait;

   cursor C_LOCK_ALLOC_DETAIL is
      select 'x'
        from alloc_detail
       where alloc_no in (select alloc_no
                            from alloc_header
                           where item     = I_item
                             and order_no = I_order_no)
         for update nowait;

BEGIN

   L_table := 'ordloc';
   open C_LOCK_ORDLOC;
   close C_LOCK_ORDLOC;

   update ordloc
      set qty_prescaled  = qty_ordered
    where item = I_item
      and order_no = I_order_no;

   L_table := 'alloc_detail';
   open C_LOCK_ALLOC_DETAIL;
   close C_LOCK_ALLOC_DETAIL;

   update alloc_detail
      set qty_prescaled = qty_allocated
    where alloc_no in (select alloc_no
                         from alloc_header
                        where item     = I_item
                          and order_no = I_order_no);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                               L_table,
                                               TO_CHAR(I_order_no),
                                               I_item);
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
END EXPLODE_NON_SCALING;
------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_UNIT_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_unit_cost       IN OUT   ORDLOC.UNIT_COST%TYPE,
                                I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                                I_supplier        IN       ORDHEAD.SUPPLIER%TYPE,
                                I_item_no         IN       ORDLOC.ITEM%TYPE,
                                I_location        IN       ORDLOC.LOCATION%TYPE,
                                I_cost_source     IN       ORDLOC.COST_SOURCE%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'ORDER_SETUP_SQL.UPDATE_ORDER_UNIT_COST';

BEGIN
   -- Convert the unit_cost passed in to the order currency.
   -- The 'V' parameter is taking in the currency of the Supplier
   -- and the 'O' parameter will convert it to the order currency.
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       I_supplier,
                                       'V',
                                       NULL,
                                       I_order_no,
                                       'O',
                                       NULL,
                                       I_unit_cost,
                                       I_unit_cost,
                                       NULL,
                                       NULL,
                                       NULL) = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'ordloc',
                    'order_no: '||to_char(I_order_no));

   update ordloc
      set unit_cost = I_unit_cost,
          cost_source = I_cost_source
    where order_no = I_order_no
      and item = I_item_no
      and location = I_location;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END UPDATE_ORDER_UNIT_COST;
-------------------------------------------------------------------------
FUNCTION DEFAULT_ORDHEAD_DOCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_supplier ORDHEAD.SUPPLIER%TYPE;
   L_factory  ORDHEAD.FACTORY%TYPE;

   L_agent    ORDHEAD.AGENT%TYPE;

   L_partner_type_1   ORDHEAD.PARTNER_TYPE_1%TYPE;
   L_partner1         ORDHEAD.PARTNER1%TYPE;
   L_partner_type_2   ORDHEAD.PARTNER_TYPE_2%TYPE;
   L_partner2         ORDHEAD.PARTNER2%TYPE;
   L_partner_type_3   ORDHEAD.PARTNER_TYPE_3%TYPE;
   L_partner3         ORDHEAD.PARTNER3%TYPE;

   cursor C_ORDHEAD is

   /* Old cursor is being modified to select partner values from ordhead table */



      select supplier,
             factory,
             agent,
             partner_type_1,
             partner1,
             partner_type_2,
             partner2,
             partner_type_3,
             partner3
        from ordhead
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ORDHEAD', 'ORDHEAD', 'ORDER_NO: '||to_char(I_order_no));
   open  C_ORDHEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_ORDHEAD', 'ORDHEAD', 'ORDER_NO: '||to_char(I_order_no));
   fetch C_ORDHEAD into L_supplier,
                        L_factory,
                        L_agent,

                        L_partner_type_1,
                        L_partner1,
                        L_partner_type_2,
                        L_partner2,
                        L_partner_type_3,
                        L_partner3;

   SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'ORDER_NO: '||to_char(I_order_no));


   close C_ORDHEAD;

   if L_supplier is NULL then

      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',
                                             to_char(I_order_no),
                                            NULL,
                                            NULL);
      return FALSE;
   end if;


   ---
   --Default supplier level required documents for the order.
   if L_supplier is not NULL then
      if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                    'SUPP',
                                    'PO',
                                    L_supplier,
                                    I_order_no,
                                    NULL,
                                    NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   --Default factory level required documents for the order.
   if L_factory is not NULL then
      if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                    'PTNR',
                                    'PO',
                                    'FA',
                                    I_order_no,
                                    L_factory,
                                    NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   --Default agent level required documents for the order.
   if L_agent is not NULL then
      if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                    'PTNR',
                                    'PO',
                                    'AG',
                                    I_order_no,
                                    L_agent,
                                    NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---

   --Default partner1 level required documents for the order.
   if L_partner1 is not NULL then
      if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                    'PTNR',
                                    'PO',
                                    L_partner_type_1,
                                    I_order_no,
                                    L_partner1,
                                    NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   --Default partner2 level required documents for the order.
   if L_partner2 is not NULL then
      if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                    'PTNR',
                                    'PO',
                                    L_partner_type_2,
                                    I_order_no,
                                    L_partner2,
                                    NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---

   --Default partner3 level required documents for the order.
   if L_partner3 is not NULL then
      if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                    'PTNR',
                                    'PO',
                                    L_partner_type_3,
                                    I_order_no,
                                    L_partner3,
                                    NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.DEFAULT_ORDHEAD_DOCS',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DEFAULT_ORDHEAD_DOCS;
-------------------------------------------------------------------------------
FUNCTION DEFAULT_ORDSKU_DOCS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no            IN       ORDSKU.ORDER_NO%TYPE,
                             I_item                IN       ORDSKU.ITEM%TYPE,
                             I_origin_country_id   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                             I_elc_ind             IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                             I_default_hts         IN       VARCHAR2)
   RETURN BOOLEAN IS

   cursor C_HTS_CHAPTER is
      select distinct h.chapter,o.import_country_id
        from hts h,
             ordsku_hts osh,
             ordhead o
       where osh.order_no          = I_order_no
         and osh.order_no          = o.order_no
         and osh.hts               = h.hts
         and osh.import_country_id = o.import_country_id
         and osh.item              = I_item
         and osh.pack_item is NULL;

BEGIN
   ---
   --Default item level required documents to the order item.
   if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                 'IT',
                                 'POIT',
                                 I_item,
                                 I_order_no,
                                 NULL,
                                 I_item) = FALSE then
      return FALSE;
   end if;
   ---
   --Default country level required documents to the order item.
   if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                 'CTRY',
                                 'POIT',
                                 I_origin_country_id,
                                 I_order_no,
                                 NULL,
                                 I_item) = FALSE then
      return FALSE;
   end if;
   ---
   if I_elc_ind = 'Y'
      and I_default_hts = 'Y' then
      ---
      --Loop through all the HTS chapters associated with the HTS codes
      --attached to the item.
      SQL_LIB.SET_MARK('OPEN', 'C_HTS_CHAPTER', 'ORDSKU_HTS', 'ORDER_NO: '||to_char(I_order_no));

      for C_rec in C_HTS_CHAPTER
      LOOP
         if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                       'HTSC',
                                       'POIT',
                                       C_rec.chapter,
                                       I_order_no,
                                       C_rec.import_country_id,
                                       I_item) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.DEFAULT_ORDSKU_DOCS',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DEFAULT_ORDSKU_DOCS;
----------------------------------------------------------------------------------------
FUNCTION GET_LC_REF_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_lc_ref_id       IN OUT   ORDLC.LC_REF_ID%TYPE,
                       I_order_no        IN       ORDLC.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50)  :=  'ORDER_SETUP_SQL.GET_LC_IND';
   ---
   cursor C_GET_LC_REF_ID is
      select lc_ref_id
        from ordlc
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_LC_REF_ID','ORDLC','Order No :'||to_char(I_order_no));
   open  C_GET_LC_REF_ID;
   SQL_LIB.SET_MARK('FETCH','C_GET_LC_REF_ID','ORDLC','Order No :'||to_char(I_order_no));
   fetch C_GET_LC_REF_ID into O_lc_ref_id;
   if C_GET_LC_REF_ID%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_GET_LC_REF_ID','ORDLC','Order No :'||to_char(I_order_no));
      close C_GET_LC_REF_ID;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('NO_LC_REF_ID',
                                            to_char(I_order_no),
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_GET_LC_REF_ID','ORDLC','Order No :'||to_char(I_order_no));
   close C_GET_LC_REF_ID;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_LC_REF_ID;
----------------------------------------------------------------------------------------
FUNCTION DELETE_WO_WKSHT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN       ORDLOC_WKSHT.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_exists       VARCHAR2(1)   := NULL;
   L_function     VARCHAR2(50)  := 'ORDER_SETUP_SQL.DELETE_WO_WKSHT';
   L_table        VARCHAR2(30)  := 'ORDLOC_WKSHT';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);


   cursor C_LOCK_ORDLOC_WKSHT is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         for update nowait;
BEGIN
   ---
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             SQLERRM,
                                             L_function,
                                             TO_CHAR(SQLCODE));
      return FALSE;
   end if;
      ---
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ORDLOC_WKSHT', 'ORDLOC_WKSHT', NULL);
   open  C_LOCK_ORDLOC_WKSHT;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ORDLOC_WKSHT', 'ORDLOC_WKSHT', NULL);
   close C_LOCK_ORDLOC_WKSHT;
   ---
   delete from ordloc_wksht ow
    where ow.order_no = I_order_no
      and EXISTS(select 'x' from wo_sku_loc wsl, wo_head wo
                           where wo.order_no = I_order_no
                             and wo.order_no = ow.order_no
                             and wsl.item     = ow.item
                             and wsl.location = (select physical_wh
                                                   from wh
                                                  where wh = ow.location));

   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             TO_CHAR(I_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_WO_WKSHT;
-------------------------------------------------------------------------------
FUNCTION ADD_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                  I_item            IN       ORDSKU.ITEM%TYPE,
                  I_ref_item        IN       ORDSKU.REF_ITEM%TYPE,
                  I_supplier        IN       ORDHEAD.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_dummy               VARCHAR2(1);
   L_orig_country_id     ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_earliest_ship_date  ORDSKU.EARLIEST_SHIP_DATE%TYPE;
   L_latest_ship_date    ORDSKU.LATEST_SHIP_DATE%TYPE;

   cursor C_CHECK_ITEM is
      select 'x'
        from ordsku os
       where os.item = I_item
         and os.order_no = I_order_no;

   cursor C_SHIP_DATE is
      select earliest_ship_date,
             latest_ship_date
        from ordhead
       where order_no = I_order_no;
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   if I_item is NULL then
    O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                              'I_item',
                          'NULL',
                          'NOT NULL');
    RETURN FALSE;
   end if;
   if I_supplier is NULL then
    O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                          'I_supplier',
                          'NULL',
                          'NOT NULL');
    RETURN FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ITEM', 'ADD_SKU', NULL);
   open  C_CHECK_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ITEM', 'ADD_SKU', NULL);
   fetch C_CHECK_ITEM into L_dummy;
   ---
   if C_CHECK_item%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ITEM', 'ADD_SKU', NULL);
      close C_CHECK_ITEM;
      ---Get ship dates
      SQL_LIB.SET_MARK('OPEN','C_SHIP_DATE', 'ADD_SKU', NULL);
      open C_SHIP_DATE;
      SQL_LIB.SET_MARK('FETCH', 'C_SHIP_DATE', 'ADD_SKU', NULL);
      fetch C_SHIP_DATE into L_earliest_ship_date, L_latest_ship_date;
      ---
      if C_SHIP_DATE%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',
                               TO_CHAR(I_order_no),
                               'NULL',
                                   'NULL');
     SQL_LIB.SET_MARK('CLOSE', 'C_SHIP_DATE', 'ADD_SKU', NULL);
     close C_SHIP_DATE;
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_SHIP_DATE', 'ADD_SKU', NULL);
      close C_SHIP_DATE;
      ---Get Origin country
      if SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                              L_orig_country_id,
                              I_item,
                              I_supplier) = FALSE then
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('INSERT', NULL, 'ordsku', 'order no: '||to_char(I_order_no)||
                       ', item: '||I_item);
      insert into ordsku(order_no,
                         item,
                         ref_item,
                         origin_country_id,
                         earliest_ship_date,
                         latest_ship_date,
                         supp_pack_size,
                         non_scale_ind)
              values(I_order_no,
                         I_item,
                         I_ref_item,
                         L_orig_country_id,
                         L_earliest_ship_date,
                         L_latest_ship_date,
                         1,
                         'N');
      return TRUE;
   else
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ITEM' , 'ADD_SKU', NULL);
      close C_CHECK_ITEM;
      return TRUE;
   end if;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.ADD_ITEM',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_ITEM;
-----------------------------------------------------------------------------------
FUNCTION DEFAULT_ORDER_INV_MGMT_INFO(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_order_no            IN       ORDSKU.ORDER_NO%TYPE,
                                     I_supplier            IN       SUPS.SUPPLIER%TYPE,
                                     I_dept                IN       DEPS.DEPT%TYPE,
                                     I_location            IN       WH.WH%TYPE,
                                     I_currency_code       IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                     I_exchange_rate       IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                                     I_order_type          IN       VARCHAR2,
                                     I_order_approve_ind   IN       ORD_INV_MGMT.ORD_APPROVE_IND%TYPE)
   RETURN BOOLEAN IS

   L_order_currency                 ORDHEAD.CURRENCY_CODE%TYPE                := I_currency_code;
   L_order_exchange_rate            ORDHEAD.EXCHANGE_RATE%TYPE                := I_exchange_rate;
   L_max_scaling_iterations         ORD_INV_MGMT.MAX_SCALING_ITERATIONS%TYPE  := NULL;
   L_order_type                     VARCHAR2(2)                               := I_order_type;
   L_sup_inv_rec                    SUP_INV_MGMT_SQL.SUP_INV_RECTYPE;


   cursor C_GET_MAX_SCALING_ITERATIONS is
      select max_scaling_iterations
        from system_options;


   cursor C_GET_ORD_CURR is
      select currency_code,
             exchange_rate
        from ordhead
       where order_no = I_order_no;

BEGIN

   if I_order_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
         return FALSE;
   end if;
   ---
   if I_supplier IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
         return FALSE;
   end if;
   ---
   if I_order_type is NULL then
      L_order_type := 'O';   -- default to regular order (not a contract order)
   end if;
   ---
   -- fetch the max_scaling_iterations
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_MAX_SCALING_ITERATIONS',
                    'system_options',
                    NULL);
   open C_GET_MAX_SCALING_ITERATIONS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_MAX_SCALING_ITERATIONS',
                    'system_options',
                    NULL);
   fetch C_GET_MAX_SCALING_ITERATIONS into L_max_scaling_iterations;


   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_MAX_SCALING_ITERATIONS',
                    'system_options',
                    NULL);
   close C_GET_MAX_SCALING_ITERATIONS;
   ---
   -- fetch any supplier, supplier/department, supplier/department/location,
   -- or the supplier/location  specific information on sup_inv_mgmt

   if SUP_INV_MGMT_SQL.GET_INV_MGMT_DATA(O_error_message,
                                         L_sup_inv_rec,
                                         I_supplier,
                                         I_dept,
                                         I_location) = FALSE then
      return FALSE;
   end if;

      -- If either constraint is 'A'mount then the currency code
      -- needs to be fetch for comparison.  Only perform currency conversions
      -- for scaling constraints if the order is not a contract order since
      -- scaling is not available for contract orders (CB - Contract Batch,
      -- CO - Contract Online)
      if (L_sup_inv_rec.scale_cnstr_type1 = 'A' and L_order_type not in ('CB','CO')) OR
         (L_sup_inv_rec.scale_cnstr_type2 = 'A' and L_order_type not in ('CB','CO')) OR
         (L_sup_inv_rec.min_cnstr_type1 = 'A') OR
         (L_sup_inv_rec.min_cnstr_type2 = 'A') then

         if (I_currency_code is NULL) OR (I_exchange_rate is NULL) then
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_ORD_CURR',
                             'ordhead',
                             'Order: '||to_char(I_order_no));
            open C_GET_ORD_CURR;

            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_ORD_CURR',
                             'ordhead',
                             'Order: '||to_char(I_order_no));
            fetch C_GET_ORD_CURR into L_order_currency,
                                      L_order_exchange_rate;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_ORD_CURR',
                             'ordhead',
                             'Order: '||to_char(I_order_no));
            close C_GET_ORD_CURR;
         end if;
      end if;
      ---
      if (L_sup_inv_rec.scale_cnstr_type1 = 'A' and L_order_type not in ('CB','CO')) then
         if L_sup_inv_rec.scale_cnstr_curr1 != L_order_currency then
            -- convert min value if greater than zero
            if L_sup_inv_rec.scale_cnstr_min_val1 > 0 then
               if not CURRENCY_SQL.CONVERT(O_error_message,
                                           L_sup_inv_rec.scale_cnstr_min_val1,     -- in curr value
                                           L_sup_inv_rec.scale_cnstr_curr1,        -- in currency
                                           L_order_currency,                       -- out currency
                                           L_sup_inv_rec.scale_cnstr_min_val1,     -- out curr value
                                           'C',                                    -- cost or retail ind
                                           NULL,                                   -- effective date (today)
                                           'P',                                    -- exchange type (sys ops)
                                           NULL,                                   -- in exchange rate
                                           L_order_exchange_rate) then             -- out exchange rate
                  return FALSE;
               end if;
            end if;   -- end of min value > 0
            -- convert max value if greater than zero
            if L_sup_inv_rec.scale_cnstr_max_val1 > 0 then
               if not CURRENCY_SQL.CONVERT(O_error_message,
                                           L_sup_inv_rec.scale_cnstr_max_val1,      -- in curr value
                                           L_sup_inv_rec.scale_cnstr_curr1,         -- in currency
                                           L_order_currency,                        -- out currency
                                           L_sup_inv_rec.scale_cnstr_max_val1,      -- out curr value
                                           'C',                                     -- cost or retail ind
                                           NULL,                                    -- effective date (today)
                                           'P',                                     -- exchange type (sys ops)
                                           NULL,                                    -- in exchange rate
                                           L_order_exchange_rate) then              -- out exchange rate
                  return FALSE;
               end if;
            end if;  -- end of max value > 0
         end if;
      end if;

      if (L_sup_inv_rec.scale_cnstr_type2 = 'A' and L_order_type not in('CB','CO')) then
         if L_sup_inv_rec.scale_cnstr_curr2 != L_order_currency then
            if L_sup_inv_rec.scale_cnstr_min_val2 > 0 then
               -- convert min value
               if not CURRENCY_SQL.CONVERT(O_error_message,
                                           L_sup_inv_rec.scale_cnstr_min_val2,     -- in curr value
                                           L_sup_inv_rec.scale_cnstr_curr2,        -- in currency
                                           L_order_currency,                       -- out currency
                                           L_sup_inv_rec.scale_cnstr_min_val2,     -- out curr value
                                           'C',                                    -- cost or retail ind
                                           NULL,                                   -- effective date (today)
                                           'P',                                    -- exchange type (sys ops)
                                           NULL,                                   -- in exchange rate
                                           L_order_exchange_rate) then             -- out exchange rate
                  return FALSE;
               end if;
            end if;
            -- convert max value
            if L_sup_inv_rec.scale_cnstr_max_val2 > 0 then
               if not CURRENCY_SQL.CONVERT(O_error_message,
                                           L_sup_inv_rec.scale_cnstr_max_val2,     -- in curr value
                                           L_sup_inv_rec.scale_cnstr_curr2,        -- in currency
                                           L_order_currency,                       -- out currency
                                           L_sup_inv_rec.scale_cnstr_max_val2,     -- out curr value
                                           'C',                                    -- cost or retail ind
                                           NULL,                                   -- effective date (today)
                                           'P',                                    -- exchange type (sys ops)
                                           NULL,                                   -- in exchange rate
                                           L_order_exchange_rate) then             -- out exchange rate
                  return FALSE;
               end if;
            end if;
         end if;
      end if;

      -- If either minimum constraint is 'A'mount, conversion must occur
      -- if the currencies are different and the value is greater than zero.
      ---
      if L_sup_inv_rec.min_cnstr_type1 = 'A' and
         L_sup_inv_rec.min_cnstr_curr1 != L_order_currency and
         L_sup_inv_rec.min_cnstr_val1 > 0 then
         if not CURRENCY_SQL.CONVERT(O_error_message,
                                     L_sup_inv_rec.min_cnstr_val1,           -- in curr value
                                     L_sup_inv_rec.min_cnstr_curr1,          -- in currency
                                     L_order_currency,                       -- out currency
                                     L_sup_inv_rec.min_cnstr_val1,           -- out curr value
                                     'C',                                    -- cost or retail ind
                                     NULL,                                   -- effective date (today)
                                     'P',                                    -- exchange rate (sys ops)
                                     NULL,                                   -- in exchange rate
                                     L_order_exchange_rate) then             -- out exchange rate
            return FALSE;
         end if;
      end if;
      ---
      if L_sup_inv_rec.min_cnstr_type2 = 'A' and
         L_sup_inv_rec.min_cnstr_val2 > 0 and
         L_sup_inv_rec.min_cnstr_curr2 != L_order_currency then
         if not CURRENCY_SQL.CONVERT(O_error_message,
                                     L_sup_inv_rec.min_cnstr_val2,           -- in curr value
                                     L_sup_inv_rec.min_cnstr_curr2,          -- in currency
                                     L_order_currency,                       -- out currency
                                     L_sup_inv_rec.min_cnstr_val2,           -- out curr value
                                     'C',                                    -- cost or retail ind
                                     NULL,                                   -- effective date (today)
                                     'P',                                    -- exchange type (sys ops)
                                     NULL,                                   -- in exchange rate
                                     L_order_exchange_rate) then             -- out exchange rate
            return FALSE;
         end if;
      end if;
      ---

   -- Note: if there is no supplier, supplier/department, supplier/department/location,
   -- or the supplier/location  information, a default record will still be inserted.

   if L_order_type not in ('CB','CO') then  -- If order is not a contract order.

      insert into ord_inv_mgmt (order_no,
                                scale_cnstr_ind,
                                scale_cnstr_lvl,
                                scale_cnstr_obj,
                                scale_cnstr_type1,
                                scale_cnstr_uom1,
                                scale_cnstr_curr1,
                                scale_cnstr_min_val1,
                                scale_cnstr_max_val1,
                                scale_cnstr_min_tol1,
                                scale_cnstr_max_tol1,
                                scale_cnstr_type2,
                                scale_cnstr_uom2,
                                scale_cnstr_curr2,
                                scale_cnstr_min_val2,
                                scale_cnstr_max_val2,
                                scale_cnstr_min_tol2,
                                scale_cnstr_max_tol2,
                                scale_cnstr_chg_ind,
                                max_scaling_iterations,
                                min_cnstr_lvl,
                                min_cnstr_conj,
                                min_cnstr_type1,
                                min_cnstr_uom1,
                                min_cnstr_curr1,
                                min_cnstr_val1,
                                min_cnstr_type2,
                                min_cnstr_uom2,
                                min_cnstr_curr2,
                                min_cnstr_val2,
                                truck_split_ind,
                                truck_split_method,
                                truck_cnstr_type1,
                                truck_cnstr_uom1,
                                truck_cnstr_val1,
                                truck_cnstr_tol1,
                                truck_cnstr_type2,
                                truck_cnstr_uom2,
                                truck_cnstr_val2,
                                truck_cnstr_tol2,
                                due_ord_process_ind,
                                due_ord_ind,
                                due_ord_lvl,
                                due_ord_serv_basis,
                                mult_vehicle_ind,
                                no_vehicles,
                                single_loc_ind,
                                ord_purge_ind,
                                ord_approve_ind,
                                pool_supplier,
                                scale_issues,
                                file_id,
                                truck_split_issues)
                        values (I_order_no,
                                nvl(L_sup_inv_rec.scale_cnstr_ind, 'N'),
                                L_sup_inv_rec.scale_cnstr_lvl,
                                L_sup_inv_rec.scale_cnstr_obj,
                                L_sup_inv_rec.scale_cnstr_type1,
                                L_sup_inv_rec.scale_cnstr_uom1,
                                DECODE(L_sup_inv_rec.scale_cnstr_type1, 'A', L_order_currency, NULL),
                                L_sup_inv_rec.scale_cnstr_min_val1,
                                L_sup_inv_rec.scale_cnstr_max_val1,
                                L_sup_inv_rec.scale_cnstr_min_tol1,
                                L_sup_inv_rec.scale_cnstr_max_tol1,
                                L_sup_inv_rec.scale_cnstr_type2,
                                L_sup_inv_rec.scale_cnstr_uom2,
                                DECODE(L_sup_inv_rec.scale_cnstr_type2, 'A', L_order_currency, NULL),
                                L_sup_inv_rec.scale_cnstr_min_val2,
                                L_sup_inv_rec.scale_cnstr_max_val2,
                                L_sup_inv_rec.scale_cnstr_min_tol2,
                                L_sup_inv_rec.scale_cnstr_max_tol2,
                                DECODE(L_sup_inv_rec.scale_cnstr_ind,'Y','Y','N'), -- scale_cnstr_chg_ind
                                L_max_scaling_iterations,
                                L_sup_inv_rec.min_cnstr_lvl,
                                L_sup_inv_rec.min_cnstr_conj,
                                L_sup_inv_rec.min_cnstr_type1,
                                L_sup_inv_rec.min_cnstr_uom1,
                                DECODE(L_sup_inv_rec.min_cnstr_type1, 'A', L_order_currency, NULL),
                                L_sup_inv_rec.min_cnstr_val1,
                                L_sup_inv_rec.min_cnstr_type2,
                                L_sup_inv_rec.min_cnstr_uom2,
                                DECODE(L_sup_inv_rec.min_cnstr_type2, 'A', L_order_currency, NULL),
                                L_sup_inv_rec.min_cnstr_val2,
                                nvl(L_sup_inv_rec.truck_split_ind, 'N'),
                                L_sup_inv_rec.truck_split_method,
                                L_sup_inv_rec.truck_cnstr_type1,
                                L_sup_inv_rec.truck_cnstr_uom1,
                                L_sup_inv_rec.truck_cnstr_val1,
                                L_sup_inv_rec.truck_cnstr_tol1,
                                L_sup_inv_rec.truck_cnstr_type2,
                                L_sup_inv_rec.truck_cnstr_uom2,
                                L_sup_inv_rec.truck_cnstr_val2,
                                L_sup_inv_rec.truck_cnstr_tol2,
                                'N',                                         -- due_ord_process_ind
                                'N',                                         -- due ord ind
                                NULL,                                        -- due_ord_lvl
                                NULL,                                        -- due_ord_serv_basis
                                nvl(L_sup_inv_rec.mult_vehicle_ind, 'N'),
                                NULL,                                        -- no_vehicles
                                nvl(L_sup_inv_rec.single_loc_ind, 'N'),
                                'N',                                         -- ord_purge_ind
                                nvl(I_order_approve_ind, 'N'),
                                L_sup_inv_rec.pool_supplier,
                                NULL,                                        -- scale_issues
                                NULL,                                        -- file_id
                                NULL);                                       -- truck_split_issues
   else  -- Order is a contract order.  Since scaling is not available
         -- for contract orders, all scaling fields and truck splitting
         -- fields should be defaulted to 'N' or NULL.
      insert into ord_inv_mgmt (order_no,
                                scale_cnstr_ind,
                                scale_cnstr_lvl,
                                scale_cnstr_obj,
                                scale_cnstr_type1,
                                scale_cnstr_uom1,
                                scale_cnstr_curr1,
                                scale_cnstr_min_val1,
                                scale_cnstr_max_val1,
                                scale_cnstr_min_tol1,
                                scale_cnstr_max_tol1,
                                scale_cnstr_type2,
                                scale_cnstr_uom2,
                                scale_cnstr_curr2,
                                scale_cnstr_min_val2,
                                scale_cnstr_max_val2,
                                scale_cnstr_min_tol2,
                                scale_cnstr_max_tol2,
                                scale_cnstr_chg_ind,
                                max_scaling_iterations,
                                min_cnstr_lvl,
                                min_cnstr_conj,
                                min_cnstr_type1,
                                min_cnstr_uom1,
                                min_cnstr_curr1,
                                min_cnstr_val1,
                                min_cnstr_type2,
                                min_cnstr_uom2,
                                min_cnstr_curr2,
                                min_cnstr_val2,
                                truck_split_ind,
                                truck_split_method,
                                truck_cnstr_type1,
                                truck_cnstr_uom1,
                                truck_cnstr_val1,
                                truck_cnstr_tol1,
                                truck_cnstr_type2,
                                truck_cnstr_uom2,
                                truck_cnstr_val2,
                                truck_cnstr_tol2,
                                due_ord_process_ind,
                                due_ord_ind,
                                due_ord_lvl,
                                due_ord_serv_basis,
                                mult_vehicle_ind,
                                no_vehicles,
                                single_loc_ind,
                                ord_purge_ind,
                                ord_approve_ind,
                                pool_supplier,
                                scale_issues,
                                file_id,
                                truck_split_issues)
                        values (I_order_no,
                                'N',                             -- scale_cnstr_ind
                                NULL,                            -- scale_cnstr_lvl,
                                NULL,                            -- scale_cnstr_obj
                                NULL,                            -- scale_cnstr_type1
                                NULL,                            -- scale_cnstr_uom1
                                NULL,                            -- scale_cnstr_curr1
                                NULL,                            -- scale_cnstr_min_val1
                                NULL,                            -- scale_cnstr_max_val1
                                NULL,                            -- scale_cnstr_min_tol1
                                NULL,                            -- scale_cnstr_max_tol1
                                NULL,                            -- scale_cnstr_type2
                                NULL,                            -- scale_cnstr_uom2
                                NULL,                            -- scale_cnstr_curr2
                                NULL,                            -- scale_cnstr_min_val2
                                NULL,                            -- scale_cnstr_max_val2
                                NULL,                            -- scale_cnstr_min_tol2
                                NULL,                            -- scale_cnstr_max_tol2
                                'N',                             -- scale_cnstr_chg_ind
                                NULL,                            -- max_scaling_iterations
                                L_sup_inv_rec.min_cnstr_lvl,
                                L_sup_inv_rec.min_cnstr_conj,
                                L_sup_inv_rec.min_cnstr_type1,
                                L_sup_inv_rec.min_cnstr_uom1,
                                DECODE(L_sup_inv_rec.min_cnstr_type1, 'A', L_order_currency, NULL),
                                L_sup_inv_rec.min_cnstr_val1,
                                L_sup_inv_rec.min_cnstr_type2,
                                L_sup_inv_rec.min_cnstr_uom2,
                                DECODE(L_sup_inv_rec.min_cnstr_type2, 'A', L_order_currency, NULL),
                                L_sup_inv_rec.min_cnstr_val2,
                                'N',                             -- truck_split_ind
                                NULL,                            -- truck_split_method
                                NULL,                            -- truck_cnstr_type1
                                NULL,                            -- truck_cnstr_uom1
                                NULL,                            -- truck_cnstr_val1
                                NULL,                            -- truck_cnstr_tol1
                                NULL,                            -- truck_cnstr_type2
                                NULL,                            -- truck_cnstr_uom2
                                NULL,                            -- truck_cnstr_val2
                                NULL,                            -- truck_cnstr_tol2
                                'N',                             -- due_ord_process_ind
                                'N',                             -- due ord ind
                                NULL,                            -- due_ord_lvl
                                NULL,                            -- due_ord_serv_basis
                                'N',                             -- mult_vehicle_ind
                                NULL,                            -- no_vehicles
                                nvl(L_sup_inv_rec.single_loc_ind, 'N'),
                                DECODE(L_order_type, 'CO', 'N', 'CB', NVL(L_sup_inv_rec.ord_purge_ind,'N')),
                                nvl(I_order_approve_ind, 'N'),
                                NULL,                            -- pool_supplier
                                NULL,                            -- scale_issues
                                NULL,                            -- file_id
                                NULL);                           -- truck_split_issues
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.DEFAULT_ORDER_INV_MGMT_INFO',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DEFAULT_ORDER_INV_MGMT_INFO;
-----------------------------------------------------------------------------------
FUNCTION GET_CONTRACT_ORDER_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_supplier        IN OUT   CONTRACT_HEADER.SUPPLIER%TYPE,
                                 O_dept            IN OUT   CONTRACT_HEADER.DEPT%TYPE,
                                 O_currency_code   IN OUT   CONTRACT_HEADER.CURRENCY_CODE%TYPE,
                                 O_country_id      IN OUT   CONTRACT_HEADER.COUNTRY_ID%TYPE,
                                 O_start_date      IN OUT   CONTRACT_HEADER.START_DATE%TYPE,
                                 O_end_date        IN OUT   CONTRACT_HEADER.END_DATE%TYPE,
                                 I_contract_no     IN       CONTRACT_HEADER.CONTRACT_NO%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_ORDER_INFO is
    select supplier,
           dept,
           currency_code,
           country_id,
           start_date,
           end_date
      from CONTRACT_HEADER
     where contract_no = I_contract_no;
BEGIN
   if I_contract_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_contract_no',
                                            'NULL',
                                            'NOT NULL');
         return FALSE;
   end if;


   SQL_LIB.SET_MARK('OPEN', 'C_GET_ORDER_INFO', 'CONTRACT_HEADER', 'Contract: '||to_char(I_contract_no));
   open  C_GET_ORDER_INFO;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_ORDER_INFO', 'CONTRACT_HEADER', 'Contract: '||to_char(I_contract_no));
   fetch C_GET_ORDER_INFO INTO O_supplier,
                               O_dept,
                               O_currency_code,
                               O_country_id,
                               O_start_date,
                               O_end_date;

   if C_GET_ORDER_INFO%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CONTRACT_SEARCH',
                                    TO_CHAR(I_CONTRACT_NO),
                              NULL,
                              NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_ORDER_INFO', 'CONTRACT_HEADER', to_char(I_contract_no));
      close C_GET_ORDER_INFO;
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_ORDER_INFO', 'CONTRACT_HEADER', 'Contract: '||to_char(I_contract_no));
   close C_GET_ORDER_INFO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.GET_CONTRACT_ORDER_INFO',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_CONTRACT_ORDER_INFO;
-----------------------------------------------------------------------------------
FUNCTION DELETE_SINGLE_CONTAINER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_order           IN       ORDLOC.ORDER_NO%TYPE,
                                      I_contents_item   IN       ITEM_MASTER.ITEM%TYPE,
                                      I_location        IN       ORDLOC.LOCATION%TYPE,
                                      I_delete_ind      IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(45) := 'ORDER_SETUP_SQL.DELETE_SINGLE_CONTAINER_ITEM';
   L_form      VARCHAR2(6);

   cursor C_ORDLOC_ORDERS is
      select ol.item,
             ol.location,
             ol.loc_type
        from ordloc ol,
             item_master im
       where ol.item              = im.item
         and ol.order_no          = I_order
         and ol.location          = nvl(I_location,ol.location)
         and im.deposit_item_type = 'A'
         and (I_contents_item is NULL
             or (I_contents_item is not NULL
                and exists (select 'x'
                               from item_master contents
                              where contents.container_item = im.item
                                and (contents.item = I_contents_item
                                    or contents.item_parent = I_contents_item
                                    or contents.item_grandparent = I_contents_item)
                                 and rownum = 1)));
   cursor C_ORDSKU_ORDERS is
      select os.item
        from ordsku os,
             item_master im
       where os.item              = im.item
         and os.order_no          = I_order
         and im.deposit_item_type = 'A'
         and (I_contents_item is NULL
             or (I_contents_item is not NULL
                 and exists (select 'x'
                            from item_master contents
                           where contents.container_item = im.item
                             and (contents.item = I_contents_item
                                 or contents.item_parent = I_contents_item
                                 or contents.item_grandparent = I_contents_item)
                              and rownum = 1)));

BEGIN
   if I_delete_ind = 'B' then
      L_form := 'ordsku';
   elsif I_delete_ind = 'L' then
      L_form := 'ordloc';
   end if;

   if I_delete_ind in ('B','L') then -- 'B'oth, ord'L'oc

      FOR ordloc_orders_rec in C_ORDLOC_ORDERS LOOP
        if ORDER_SETUP_SQL.DELETE_ORDER (O_error_message,
                                          I_order,
                                          ordloc_orders_rec.item,
                                          ordloc_orders_rec.location,
                                          ordloc_orders_rec.loc_type,
                                          L_form) = FALSE then
            return FALSE;
         end if;
         if I_delete_ind = 'L' then
            delete from ordloc
             where order_no = I_order
               and item = ordloc_orders_rec.item
               and location = ordloc_orders_rec.location
               and loc_type = ordloc_orders_rec.loc_type;
         end if;
      END LOOP;
   end if;

   if I_delete_ind in ('B','S') then  -- 'B'oth, ord'S'ku

      FOR ordsku_orders_rec in C_ORDSKU_ORDERS LOOP
         if ORDER_SETUP_SQL.DELETE_ORDSKU (O_error_message,
                                           I_order,
                                           ordsku_orders_rec.item) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_SINGLE_CONTAINER_ITEM;
-----------------------------------------------------------------------------------
FUNCTION POP_SINGLE_CONTAINER_ITEM(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_order_no          IN       ORDHEAD.ORDER_NO%TYPE,
                                   I_supplier          IN       SUPS.SUPPLIER%TYPE,
                                   I_location          IN       ORDLOC.LOCATION%TYPE,
                                   I_contents_item     IN       ITEM_MASTER.ITEM%TYPE,
                                   I_origin_country_id IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'ORDER_SETUP_SQL.POP_SINGLE_CONTAINER_ITEM';
   L_ordhead_row            ORDHEAD%ROWTYPE;
   L_system_options_row     SYSTEM_OPTIONS%ROWTYPE;
   L_contract_currency      CURRENCIES.CURRENCY_CODE%TYPE;
   L_supplier_currency      CURRENCIES.CURRENCY_CODE%TYPE;
   L_recalc_hts             VARCHAR2(1) := 'N';
   L_previous_item          ORDSKU.ITEM%TYPE;
   L_previous_pack_ind      ITEM_MASTER.PACK_IND%TYPE;
   L_calling_form           VARCHAR2(10) := 'ORDMTXWS';
   L_item                   ITEM_MASTER.ITEM%TYPE;
   L_location               ORDLOC.LOCATION%TYPE;
   L_loc_type               ORDLOC.LOC_TYPE%TYPE;
   L_qty_ordered            ORDLOC.QTY_ORDERED%TYPE;
   L_last_rounded_qty       ORDLOC.LAST_ROUNDED_QTY%TYPE;
   L_last_grp_rounded_qty   ORDLOC.LAST_GRP_ROUNDED_QTY%TYPE;
   L_pack_ind               ITEM_MASTER.PACK_IND%TYPE;
   L_container_item         ITEM_MASTER.ITEM%TYPE;
   L_origin_country_id      ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_supp_pack_size         ORDSKU.SUPP_PACK_SIZE%TYPE;
   L_lead_time              ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE;
   L_sum_qty_ord            ORDLOC.QTY_ORDERED%TYPE;
   L_orphan_loc             ORDLOC.LOCATION%TYPE;
   L_delete_ind             VARCHAR2(1);
   L_add_to_po_rec          ORDER_SETUP_SQL.ADD_ITEM_TO_PO_RECTYPE;
   L_add_to_po_tbl          ORDER_SETUP_SQL.ADD_ITEM_TO_PO_TBLTYPE;
   L_idx                    NUMBER := 0;
   L_ord_qty                ORDLOC.QTY_ORDERED%TYPE;
   L_order_type             ORDHEAD.ORDER_TYPE%TYPE;

   cursor C_OLO is
      select olo.item,
             olo.location,
             olo.loc_type,
             olo.qty_ordered,
             olo.last_rounded_qty,
             olo.last_grp_rounded_qty,
             iem.pack_ind,
             iem.container_item,
             isc.origin_country_id,
             isc.supp_pack_size,
             nvl(isc.lead_time,0) lead_time
        from item_supp_country      isc,
             item_master            iem,
             ordloc                 olo
       where olo.order_no           = I_order_no
         and olo.item               = nvl(I_contents_item, olo.item)
         and olo.item               = iem.item
         and olo.item               = isc.item
         and olo.location           = nvl(I_location, olo.location)
         and isc.supplier           = I_supplier
         and olo.location           is not null
         and isc.origin_country_id  = nvl(I_origin_country_id, isc.origin_country_id)
         and NVL(olo.qty_ordered,0) > 0
         and iem.deposit_item_type  = 'E' --(Contents)
       order by olo.item;

   cursor C_QTY_ORD is
      select  sum(ol.qty_ordered) sum_q_ord
         from ordloc ol,
              item_master im
        where ol.item              = im.item
          and ol.order_no          = I_order_no
          and ol.location          = L_location
          and im.container_item    = L_container_item;

   cursor C_QTY_PRESCALED is
      select qty_prescaled
        from ordloc
       where item      = L_item
         and order_no  = I_order_no
         and location  = L_location;

   cursor C_GET_CONTAINER is
     select im.container_item,
            sum(o.qty_ordered) qty_ordered,
            o.location
       from item_master im,
            ordloc o
      where im.item = I_contents_item
        and im.container_item = o.item
        and o.order_no = I_order_no
        and o.location = nvl(I_location,o.location)
        group by im.container_item, o.location;

   cursor C_ORPHAN is
      select ol.item container,
             ol.location
       from ordloc ol,
            item_master im
      where ol.item = im.item
        and ol.order_no = I_order_no
        and im.deposit_item_type = 'A'
        and not exists (select 'x'
                          from ordloc o,
                               item_master i
                         where o.item = i.item
                           and o.order_no = I_order_no
                           and o.location = ol.location
                           and im.item = i.container_item);

   cursor C_ORDTYPE is
      select order_type
        from ordhead
       where order_no = I_order_no;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);

      return FALSE;
   end if;
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   if ORDER_ATTRIB_SQL.GET_ORDHEAD_ROW(O_error_message,
                                       L_ordhead_row,
                                       I_order_no) = FALSE then
      return FALSE;
   end if;
   if L_ordhead_row.contract_no is NULL then
      L_contract_currency := NULL;
   else
      if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                   L_ordhead_row.contract_no,
                                   'C',
                                   NULL,
                                   L_contract_currency) = FALSE then
         return FALSE;
      end if;
   end if;

   if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                I_supplier,
                                'V',
                                NULL,
                                L_supplier_currency) = FALSE then
      return FALSE;
   end if;
   L_previous_item := -1;
   L_previous_pack_ind := NULL;

   FOR c_olo_row in C_OLO LOOP
      L_idx := L_idx + 1;
      L_item                 := c_olo_row.item;
      L_location             := c_olo_row.location;
      L_loc_type             := c_olo_row.loc_type;
      L_qty_ordered          := c_olo_row.qty_ordered;
      L_last_rounded_qty     := c_olo_row.last_rounded_qty;
      L_last_grp_rounded_qty := c_olo_row.last_grp_rounded_qty;
      L_pack_ind             := c_olo_row.pack_ind;
      L_container_item       := c_olo_row.container_item;
      L_origin_country_id    := c_olo_row.origin_country_id;
      L_lead_time            := c_olo_row.lead_time;

      -- Container item will not be rounded as the container qty would be same as ordered contents.
      L_supp_pack_size   := 1;

      open C_QTY_PRESCALED;
      fetch C_QTY_PRESCALED into L_ord_qty;
      close C_QTY_PRESCALED;

      -- delete the containers associated with the item and readd for the new qty.
      if ORDER_SETUP_SQL.DELETE_SINGLE_CONTAINER_ITEM (O_error_message,
                                                       I_order_no,
                                                       L_item,
                                                       L_location,
                                                       'L' -- I_delete_ind
                                                       ) = FALSE then
         return FALSE;
      end if;

      if nvl(L_ord_qty,0) != 0 then -- if the total qty ordered is 0 don't call the add function
         L_add_to_po_rec.I_order_no             := I_order_no;
         L_add_to_po_rec.I_elc_ind              := L_system_options_row.elc_ind;
         L_add_to_po_rec.I_latest_ship_days     := L_system_options_row.latest_ship_days;
         L_add_to_po_rec.I_ord_import_ind       := L_ordhead_row.import_order_ind;
         L_add_to_po_rec.I_earliest_ship_date   := L_ordhead_row.earliest_ship_date;
         L_add_to_po_rec.I_latest_ship_date     := L_ordhead_row.latest_ship_date;
         L_add_to_po_rec.I_ord_currency         := L_ordhead_row.currency_code;
         L_add_to_po_rec.I_ord_exchg_rate       := L_ordhead_row.exchange_rate;
         L_add_to_po_rec.I_import_country_id    := L_ordhead_row.import_country_id;
         L_add_to_po_rec.I_contract_no          := L_ordhead_row.contract_no;
         L_add_to_po_rec.I_contract_currency    := L_contract_currency;
         L_add_to_po_rec.I_pickup_loc           := L_ordhead_row.pickup_loc;
         L_add_to_po_rec.I_pickup_no            := L_ordhead_row.pickup_no;
         L_add_to_po_rec.I_item                 := L_container_item;
         L_add_to_po_rec.I_ref_item             := NULL;
         L_add_to_po_rec.I_pack_ind             := L_pack_ind;
         L_add_to_po_rec.I_item_xform_ind       := NULL;
         L_add_to_po_rec.I_sellable_ind         := NULL;
         L_add_to_po_rec.I_supplier             := I_supplier;
         L_add_to_po_rec.I_supplier_currency    := L_supplier_currency;
         L_add_to_po_rec.I_origin_country_id    := L_origin_country_id;
         L_add_to_po_rec.I_location             := L_location;
         L_add_to_po_rec.I_loc_type             := L_loc_type;
         L_add_to_po_rec.I_supp_pack_size       := L_supp_pack_size;
         L_add_to_po_rec.I_lead_time            := L_lead_time;
         L_add_to_po_rec.I_act_qty              := L_ord_qty;
         L_add_to_po_rec.I_last_rounded_qty     := L_last_rounded_qty;
         L_add_to_po_rec.I_last_grp_rounded_qty := L_last_grp_rounded_qty;
         L_add_to_po_rec.I_unit_cost            := NULL;
         L_add_to_po_rec.I_supp_unit_cost       := NULL;
         L_add_to_po_rec.I_source_type          := NULL;
         L_add_to_po_rec.I_non_scale_ind        := NULL;
         L_add_to_po_rec.I_tsf_po_link_no       := NULL;
         L_add_to_po_rec.I_previous_item        := L_previous_item;
         L_add_to_po_rec.I_prev_pack_ind        := L_previous_pack_ind;
         L_add_to_po_rec.I_calling_form         := L_calling_form;

         L_add_to_po_tbl(L_idx) := L_add_to_po_rec;
      end if;

      L_previous_item := L_item;
      L_previous_pack_ind := L_pack_ind;
   END LOOP;

   if L_add_to_po_tbl is not NULL and L_add_to_po_tbl.count > 0 then
      if ADD_ITEM_TO_PO(O_error_message,
                        L_recalc_hts,
                        L_add_to_po_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   -- check for orphan containers
   -- if all the associated contents have been deleted for a given container, container should be deleted.
   for recs in C_ORPHAN LOOP

      if I_location is NULL then -- function called from ordsku
         L_delete_ind := 'B'; --'B'oth ordloc and ordsku
      else -- function called from ordloc
         L_delete_ind := 'L';
      end if;
      L_orphan_loc := recs.location;

      -- if I_contents was passed in, but no c_olo rows found, this call will delete the specified container
      -- if I_contents_item is null, no c_olo rows were found (across the order) this call will delete
      -- all containers
       if ORDER_SETUP_SQL.DELETE_SINGLE_CONTAINER_ITEM(O_error_message,
                                                       I_order_no,
                                                       I_contents_item, -- contents
                                                       L_orphan_loc, -- location
                                                       L_delete_ind) = FALSE then
         return FALSE;
      end if;
   end LOOP;

   open C_ORDTYPE;
   fetch C_ORDTYPE into L_order_type;
   close C_ORDTYPE;

   if L_order_type != 'DSD' then
      if ROUNDING_SQL.ORDER_INTERFACE(O_error_message,
                                      I_order_no,
                                      I_supplier,
                                      NULL,
                                      NULL) = FALSE then
         return FALSE;
      end if;
   end if;

   -- compare the sum(contents qty ordered) to the container qty ordered and
   -- then update the container qty if they are different.

   for rec in C_GET_CONTAINER LOOP  -- when I_location is not null (ie function called from ordloc)
                                    -- this loop will return one record.  when I_location is null (function
                                    -- called from ordsku) this loop will return a rec for each location
      L_container_item := rec.container_item;
      L_qty_ordered := rec.qty_ordered;
      L_location := rec.location;
      L_sum_qty_ord := 0;

      open C_QTY_ORD;
      fetch C_QTY_ORD into L_sum_qty_ord;
      close C_QTY_ORD;

      if L_sum_qty_ord != L_qty_ordered then
         update ordloc
            set qty_ordered = L_sum_qty_ord
          where item = L_container_item
            and order_no = I_order_no
            and location = L_location;
      end if;

   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END POP_SINGLE_CONTAINER_ITEM;
-------------------------------------------------------------------------------------------------
FUNCTION POP_CONTAINER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                            I_supplier        IN       SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(45) := 'ORDER_SETUP_SQL.POP_CONTAINER_ITEM';

BEGIN
   -- call overloaded function
   if ORDER_SETUP_SQL.POP_SINGLE_CONTAINER_ITEM (O_error_message,
                                                 I_order_no,
                                                 I_supplier,
                                                 NULL, -- I_location
                                                 NULL -- I_contents_item
                                                 ) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END POP_CONTAINER_ITEM;
-------------------------------------------------------------------------------------------------
FUNCTION DELETE_CONTAINER_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order           IN       ORDLOC.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(45) := 'ORDER_SETUP_SQL.DELETE_CONTAINER_ITEMS';

BEGIN
   -- call overloaded function
   if ORDER_SETUP_SQL.DELETE_SINGLE_CONTAINER_ITEM (O_error_message,
                                                    I_order,
                                                    NULL, -- I_contents_item
                                                    NULL, -- I_location,
                                                    'B' -- I_delete_ind
                                                    ) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_CONTAINER_ITEMS;
-----------------------------------------------------------------------------------
FUNCTION CANCEL_CONTAINER_ITEMS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no         IN       ORDLOC.ORDER_NO%TYPE,
                                I_container_item   IN       ORDLOC.ITEM%TYPE,
                                I_location         IN       ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(45) := 'ORDER_SETUP_SQL.CANCEL_CONTAINER_ITEMS';
   L_sum_qty_ord    ORDLOC.QTY_ORDERED%TYPE;
   L_sum_qty_can    ORDLOC.QTY_CANCELLED%TYPE;
   L_sysdate        DATE := sysdate;
   L_user           VARCHAR2(50) := get_user;
   L_cancel_code    ORDLOC.CANCEL_CODE%TYPE := 'A'; -- automatic

   cursor C_ORDLOC is
   select  sum(ol.qty_ordered) sum_q_ord,
           sum(ol.qty_cancelled) sum_q_can
      from ordloc ol,
           item_master im
     where ol.item              = im.item
       and ol.order_no          = I_order_no
       and ol.location          = I_location
       and im.container_item    = I_container_item;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_container_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_container_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ORDLOC;
   fetch C_ORDLOC into L_sum_qty_ord,
                       L_sum_qty_can;
   close C_ORDLOC;

   if L_sum_qty_can is NULL then
      L_sysdate := NULL;
      L_user := NULL;
      L_cancel_code := NULL;
   end if;

   update ordloc
      set qty_cancelled = L_sum_qty_can,
          qty_ordered   = L_sum_qty_ord,
          cancel_code = L_cancel_code,
          cancel_date = L_sysdate,
          cancel_id = L_user
    where order_no = I_order_no
      and location = I_location
      and item = I_container_item;


   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CANCEL_CONTAINER_ITEMS;
-----------------------------------------------------------------------------------------------
FUNCTION COUNT_SKULIST_RECORDS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_skulist_records   IN OUT   NUMBER,
                               I_skulist           IN       SKULIST_DETAIL.SKULIST%TYPE,
                               I_order_no          IN       ORDHEAD.ORDER_NO%TYPE,
                               I_location          IN       ORDHEAD.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_program_name VARCHAR2(60) := 'ORDER_SETUP_SQL.COUNT_SKULIST_RECORDS';

   cursor C_GET_SKULIST_RECORDS is
      select count(*)
        from skulist_detail
       where skulist = I_skulist
         and not exists (select 'x'
                           from ordloc_wksht
                          where order_no = I_order_no
                            and (location = I_location or location is NULL)
                            and item = skulist_detail.item
                            and store_grade is NULL
                            and diff_1 is NULL
                            and diff_2 is NULL
                            and diff_3 is NULL
                            and diff_4 is NULL);

BEGIN
   if I_skulist is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_skulist,
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_order_no,
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_SKULIST_RECORDS',
                    'SKULIST_DETAIL',
                    'Skulist: '||to_char(I_skulist));
   open C_GET_SKULIST_RECORDS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_SKULIST_RECORDS',
                    'SKULIST_DETAIL',
                    'Skulist: '||to_char(I_skulist));
   fetch C_GET_SKULIST_RECORDS into O_skulist_records;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_SKULIST_RECORDS',
                    'SKULIST_DETAIL',
                    'Skulist: '||to_char(I_skulist));
   close C_GET_SKULIST_RECORDS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.COUNT_SKULIST_RECORDS',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END COUNT_SKULIST_RECORDS;
-----------------------------------------------------------------------------------------------
FUNCTION UPDATE_LOCATION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64)  := 'ORDER_SETUP_SQL.UPDATE_LOCATION';

   L_location           ORDLOC.LOCATION%TYPE;
   L_loctype            ORDLOC.LOC_TYPE%TYPE;
   L_order_type         ORDHEAD.ORDER_TYPE%TYPE;
   L_stockholding_ind   WH.STOCKHOLDING_IND%TYPE;
   O_cust_store         ORDLOC.LOCATION%TYPE;
   L_dummy              VARCHAR2(1);

   cursor C_ORDHEAD is
      select location,
             loc_type
        from ordhead
       where order_no = I_order_no;

   cursor C_CUST_ORDER is
      select order_type
        from ordhead
       where order_no = I_order_no;

   cursor C_LOCK_ORDLOC_WKSHT is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         for update nowait;

BEGIN

   open C_CUST_ORDER;
   fetch C_CUST_ORDER into L_order_type ;
   close C_CUST_ORDER;

   if L_order_type = 'CO' then
      if NOT ORDER_SETUP_SQL.GET_CUST_LOCS(O_error_message,
                                           L_location,
                                           I_order_no) then
          return FALSE;
      end if;
      L_loctype := 'S';
   end if;

   if L_location is NULL then
      open  C_ORDHEAD;
      fetch C_ORDHEAD into L_location,
                           L_loctype;
      close  C_ORDHEAD;
   end if;

   if L_location is NOT NULL then
      if L_loctype = 'W' then
         if LOCATION_ATTRIB_SQL.CHECK_STOCKHOLDING(O_error_message,
                                                   L_stockholding_ind,
                                                   L_location,
                                                   L_loctype) = FALSE then
            return FALSE;
         end if;
      end if;

      if (L_loctype = 'S' or
         (L_loctype='W' and L_stockholding_ind = 'Y')) then
         open C_LOCK_ORDLOC_WKSHT;
         fetch C_LOCK_ORDLOC_WKSHT into L_dummy;
         close C_LOCK_ORDLOC_WKSHT;

         update ordloc_wksht
            set location = L_location,
                loc_type = L_loctype
          where order_no = I_order_no;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_LOCATION;
------------------------------------------------------------------------------------------------------
FUNCTION DSDORD_DUP_CHECK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_item_exists     IN OUT   BOOLEAN,
                          I_order_no        IN       ORDAUTO_TEMP.ORDER_NO%TYPE,
                          I_item            IN       ORDAUTO_TEMP.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_dummy         VARCHAR2(1)    := 'N';
   L_program       VARCHAR2(60)   := 'ORDER_SETUP_SQL.DSDORD_DUP_CHECK';

   cursor C_ORDAUTO_TEMP is
      select 'Y'
        from ordauto_temp
       where order_no = I_order_no
         and item     = I_item;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ORDAUTO_TEMP;
   fetch C_ORDAUTO_TEMP into L_dummy;
   close C_ORDAUTO_TEMP;

   if L_dummy = 'Y' then
      O_item_exists := TRUE;
   else
      O_item_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DSDORD_DUP_CHECK;
-----------------------------------------------------------------------------------------------
FUNCTION ORDAUTO_TEMP_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN       ORDAUTO_TEMP.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60)  := 'ORDER_SETUP_SQL.ORDAUTO_TEMP_DELETE';

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   delete from ordauto_temp
    where order_no = I_order_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ORDAUTO_TEMP_DELETE;
-----------------------------------------------------------------------------------------------
FUNCTION INSERT_CONTAINER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no        IN       ORDAUTO_TEMP.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60)  := 'ORDER_SETUP_SQL.INSERT_CONTAINER_ITEM';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_container_item  ITEM_MASTER.ITEM%TYPE;
   L_exists          BOOLEAN;

   cursor C_GET_ORDAUTO_TEMP is
      select ot.order_no,
             ot.item,
             ot.ref_item,
             ot.supplier,
             ot.dept,
             ot.store,
             ot.qty,
             ot.standard_uom,
             ot.unit_cost,
             ot.currency_code,
             ot.unit_retail,
             ot.origin_country_id,
             ot.cost_source,
             ot.weight_received,
             ot.weight_received_uom
        from ordauto_temp ot,
             item_master im
       where ot.order_no = I_order_no
         and ot.item = im.item
         and im.deposit_item_type = 'E';

   TYPE ordauto_temp_tbl is table of C_GET_ORDAUTO_TEMP%ROWTYPE index by binary_integer;
   L_ordauto_temp_tbl      ordauto_temp_tbl;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_ORDAUTO_TEMP;
   fetch C_GET_ORDAUTO_TEMP BULK COLLECT into L_ordauto_temp_tbl;
   close C_GET_ORDAUTO_TEMP;

   if L_ordauto_temp_tbl.first is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_REC_TO_PROCESS');
      return FALSE;
   end if;

   FOR i in L_ordauto_temp_tbl.first..L_ordauto_temp_tbl.last LOOP
      L_container_item := NULL;
      L_exists := FALSE;
      if ITEM_ATTRIB_SQL.GET_CONTAINER_ITEM(O_error_message,
                                            L_container_item,
                                            L_ordauto_temp_tbl(i).item) = FALSE then
         return FALSE;
      end if;
      ---
      if L_container_item is not NULL then
         -- we need to check if the container item has already been added to the ordauto_temp table
         -- this is in case 2 content items have the same container item.
         if ORDER_SETUP_SQL.DSDORD_DUP_CHECK(O_error_message,
                                             L_exists,
                                             I_order_no,
                                             L_container_item) = FALSE then
            return FALSE;
         end if;
         ---
         if L_exists = TRUE then
            -- if the container item has already been added, then just do an update
            update ordauto_temp
               set qty = qty + L_ordauto_temp_tbl(i).qty,
                   weight_received = weight_received + L_ordauto_temp_tbl(i).weight_received
             where order_no = I_order_no
               and item = L_container_item;
         else
            -- insert the item into the ordauto_temp table
            insert into ordauto_temp(order_no,
                                     item,
                                     ref_item,
                                     supplier,
                                     dept,
                                     store,
                                     qty,
                                     standard_uom,
                                     unit_cost,
                                     currency_code,
                                     unit_retail,
                                     origin_country_id,
                                     cost_source,
                                     weight_received,
                                     weight_received_uom)
                              values(I_order_no,
                                     L_container_item,
                                     L_ordauto_temp_tbl(i).ref_item,
                                     L_ordauto_temp_tbl(i).supplier,
                                     L_ordauto_temp_tbl(i).dept,
                                     L_ordauto_temp_tbl(i).store,
                                     L_ordauto_temp_tbl(i).qty,
                                     L_ordauto_temp_tbl(i).standard_uom,
                                     L_ordauto_temp_tbl(i).unit_cost,
                                     L_ordauto_temp_tbl(i).currency_code,
                                     0,
                                     L_ordauto_temp_tbl(i).origin_country_id,
                                     L_ordauto_temp_tbl(i).cost_source,
                                     L_ordauto_temp_tbl(i).weight_received,
                                     L_ordauto_temp_tbl(i).weight_received_uom);
         end if;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_CONTAINER_ITEM;
-----------------------------------------------------------------------------------------------
FUNCTION GET_SCALE_AIP_ORD_IND(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_scale_aip_ord_ind   IN OUT   SUPS.SCALE_AIP_ORDERS%TYPE,
                               I_supplier            IN       SUPS.SCALE_AIP_ORDERS%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64) := 'ORDER_SETUP_SQL.GET_SCALE_AIP_ORDERS_IND';

   cursor C_scale_aip_ord_ind is
      select NVL(scale_aip_orders,'N')
        from sups
       where supplier = I_supplier;

BEGIN

   open C_SCALE_AIP_ORD_IND;
   fetch C_SCALE_AIP_ORD_IND into O_scale_aip_ord_ind;
   close C_SCALE_AIP_ORD_IND;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_SCALE_AIP_ORD_IND;
-------------------------------------------------------------------------------------------------------
FUNCTION FASHPACK_INS_ITEM_LOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'ORDER_SETUP_SQL.FASHPACK_INS_ITEM_LOC';

   L_input   NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;

   cursor C_PO_ITEM_LOC is
      select o.item,
             NULL,         --item_parent,
             NULL,         --item_grandparent,
             NULL,         --short_desc,
             NULL,         --dept,
             NULL,         --class,
             NULL,         --subclass,
             NULL,         --item_level,
             NULL,         --tran_level,
             NULL,         --item_status,
             NULL,         --waste_type,
             NULL,         --sellable_ind,
             NULL,         --orderable_ind,
             'Y' pack_ind,
             NULL,         --pack_type,
             NULL,         --item_desc,
             NULL,         --diff_1,
             NULL,         --diff_2,
             NULL,         --diff_3,
             NULL,         --diff_4,
             o.location,
             o.loc_type,
             NULL,         --daily_waste_pct,
             NULL,         --unit_cost_loc,
             NULL,         --unit_retail_loc,
             NULL,         --selling_retail_loc,
             NULL,         --selling_uom,
             NULL,         --multi_units,
             NULL,         --multi_unit_retail,
             NULL,         --multi_selling_uom,
             NULL,         --item_loc_status,
             NULL,         --taxable_ind,
             NULL,         --ti,
             NULL,         --hi,
             NULL,         --store_ord_mult,
             NULL,         --meas_of_each,
             NULL,         --meas_of_price,
             NULL,         --uom_of_price,
             NULL,         --primary_variant,
             NULL,         --primary_supp,
             NULL,         --primary_cntry,
             NULL,         --local_item_desc,
             NULL,         --local_short_desc,
             NULL,         --primary_cost_pack,
             p.rec_as_type,
             NULL,         --store_price_ind,
             NULL,         --uin_type,
             NULL,         --uin_label,
             NULL,         --capture_time,
             'N',          --ext_uin_ind,
             NULL,         --source_method,
             NULL,         --source_wh,
             NULL,         --inbound_handling_days,
             NULL,         --CURRENCY_CODE,
             NULL,         --like_store,
             'N',          --default_to_children_ind,
             NULL,         --class_vat_ind,
             NULL,         --hier_level,
             NULL,         --hier_value,
             NULL,         --hier_char_value
             NULL,         --costing_loc
             NULL,         --costing_loc_type
             NULL,         --ranged_ind
             NULL,         --default_wh
             NULL          --item_loc_ind
        from ordloc_wksht o,
             pack_tmpl_head p,
             packitem_breakout pb
       where o.order_no         = I_order_no
         and o.item             = pb.pack_no
         and pb.pack_tmpl_id    = p.pack_tmpl_id
         and p.fash_prepack_ind = 'Y'
      union
      select pb.item,
             NULL,         --item_parent,
             NULL,         --item_grandparent,
             NULL,         --short_desc,
             NULL,         --dept,
             NULL,         --class,
             NULL,         --subclass,
             NULL,         --item_level,
             NULL,         --tran_level,
             NULL,         --item_status,
             NULL,         --waste_type,
             NULL,         --sellable_ind,
             NULL,         --orderable_ind,
             'N' pack_ind,
             NULL,         --pack_type,
             NULL,         --item_desc,
             NULL,         --diff_1,
             NULL,         --diff_2,
             NULL,         --diff_3,
             NULL,         --diff_4,
             o.location,
             o.loc_type,
             NULL,         --daily_waste_pct,
             NULL,         --unit_cost_loc,
             NULL,         --unit_retail_loc,
             NULL,         --selling_retail_loc,
             NULL,         --selling_uom,
             NULL,         --multi_units,
             NULL,         --multi_unit_retail,
             NULL,         --multi_selling_uom,
             NULL,         --item_loc_status,
             NULL,         --taxable_ind,
             NULL,         --ti,
             NULL,         --hi,
             NULL,         --store_ord_mult,
             NULL,         --meas_of_each,
             NULL,         --meas_of_price,
             NULL,         --uom_of_price,
             NULL,         --primary_variant,
             NULL,         --primary_supp,
             NULL,         --primary_cntry,
             NULL,         --local_item_desc,
             NULL,         --local_short_desc,
             NULL,         --primary_cost_pack,
             p.rec_as_type,
             NULL,         --store_price_ind,
             NULL,         --uin_type,
             NULL,         --uin_label,
             NULL,         --capture_time,
             'N',          --ext_uin_ind,
             NULL,         --source_method,
             NULL,         --source_wh,
             NULL,         --inbound_handling_days,
             NULL,         --CURRENCY_CODE,
             NULL,         --like_store,
             'N',          --default_to_children_ind,
             NULL,         --class_vat_ind,
             NULL,         --hier_level,
             NULL,         --hier_value,
             NULL,         --hier_char_value
             NULL,         --costing_loc
             NULL,         --costing_loc_type
             NULL,         --ranged_ind
             NULL,         --default_wh
             NULL          --item_loc_ind
        from ordloc_wksht o,
             pack_tmpl_head p,
             packitem_breakout pb
       where o.order_no         = I_order_no
         and o.item             = pb.pack_no
         and pb.pack_tmpl_id    = p.pack_tmpl_id
         and p.fash_prepack_ind = 'Y'
         and NOT EXISTS(select 'x'
                          from item_loc il
                         where il.item = pb.item
                           and il.loc = o.location);

BEGIN
   open C_PO_ITEM_LOC;
   fetch C_PO_ITEM_LOC BULK COLLECT into L_input;
   close C_PO_ITEM_LOC;
   ---
   if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message,
                                    L_input) = FALSE then
      return FALSE;

   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END FASHPACK_INS_ITEM_LOC;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_RANGED_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_ranged_loc      IN OUT   VARCHAR2,
                          I_order_no        IN       ORDLOC_WKSHT.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ORDER_SETUP_SQL.CHECK_RANGED_LOC';

   L_ordloc_wksht_exists   VARCHAR2(1) :=  NULL;
   L_ranged_exists         VARCHAR2(1) :=  NULL;
   L_ordloc_ranged_exists  VARCHAR2(1) :=  NULL;

   cursor C_ORDLOC_WKSHT_EXISTS is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         and rownum = 1;

   cursor C_CHECK_RANGED_LOC is
      select 'x'
        from ordloc_wksht ow,
             item_loc il,
             item_master im
       where ow.order_no = I_order_no
         and ow.item = im.item
         and (im.deposit_item_type is NULL
          or (im.deposit_item_type is NOT NULL
         and im.deposit_item_type != 'A'))
         and ow.item = il.item (+)
         and ow.location = il.loc(+)
         and ow.location is NOT NULL
         and (ranged_ind = 'N' or ranged_ind is NULL)
         and rownum = 1;

   cursor C_CHECK_ORDLOC_RANGED_LOC is
      select 'x'
        from ordloc o,
             item_loc i,
             item_master im
       where o.order_no = I_order_no
         and o.item = i.item
         and i.item = im.item
         and (im.deposit_item_type is NULL
          or (im.deposit_item_type is NOT NULL
         and im.deposit_item_type != 'A'))
         and o.location = i.loc
         and i.ranged_ind = 'N'
         and rownum = 1;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ORDLOC_WKSHT_EXISTS;
   fetch C_ORDLOC_WKSHT_EXISTS into L_ordloc_wksht_exists;
   close C_ORDLOC_WKSHT_EXISTS;

   if L_ordloc_wksht_exists is NOT NULL then
      open C_CHECK_RANGED_LOC;
      fetch C_CHECK_RANGED_LOC into L_ranged_exists;
      close C_CHECK_RANGED_LOC;

      if L_ranged_exists is NOT NULL then
         O_ranged_loc := 'N';
      else
         O_ranged_loc := 'Y';
      end if;
   else
      open C_CHECK_ORDLOC_RANGED_LOC;
      fetch C_CHECK_ORDLOC_RANGED_LOC into L_ordloc_ranged_exists;
      close C_CHECK_ORDLOC_RANGED_LOC;

      if L_ordloc_ranged_exists is NOT NULL then
         O_ranged_loc := 'N';
      else
         O_ranged_loc := 'Y';
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_RANGED_LOC;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_RANGED_IND(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no        IN       ORDLOC.ORDER_NO%TYPE,
                           I_item            IN       ITEM_LOC.ITEM%TYPE,
                           I_location        IN       ITEM_LOC.LOC%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ORDER_SETUP_SQL.UPDATE_RANGED_IND';

   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_LOC is
      select 'x'
        from item_loc
       where item = I_item
         and loc = I_location
         for update nowait;

BEGIN

   if I_order_no is NULL and
      I_item is NULL and
      I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_order_no is NOT NULL then
      merge into item_loc il
         using (select o.item,
                       o.location
                  from ordloc o,
                       item_master im
                 where o.order_no = I_order_no
                   and o.item = im.item
                   and (im.deposit_item_type is NULL
                    or (im.deposit_item_type is NOT NULL
                   and im.deposit_item_type != 'A'))) use_this
         on (il.item = use_this.item and
             il.loc = use_this.location)
         when matched then
            update
               set il.ranged_ind = 'Y'
             where il.ranged_ind = 'N';
   end if;

   if I_item is NOT NULL and
      I_location is NOT NULL then

      L_table := 'ITEM_LOC';

      open C_LOCK_ITEM_LOC;
      close C_LOCK_ITEM_LOC;

      update item_loc
         set ranged_ind = 'Y'
       where item = I_item
         and loc = I_location;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            TO_CHAR(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_RANGED_IND;
-------------------------------------------------------------------------------------------------------
FUNCTION POP_ORDCUST_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDLOC.ORDER_NO%TYPE)
   RETURN BOOLEAN is

   L_program          VARCHAR2(64) := 'ORDER_SETUP_SQL.POP_ORDCUST_DETAIL';

   L_ordcust_no       ORDCUST.ORDCUST_NO%TYPE := NULL;

   TYPE ordcust_detail_TBL   is table of ORDCUST_DETAIL%ROWTYPE   INDEX BY BINARY_INTEGER;

   L_ordcust_detail_TBL   ORDCUST_DETAIL_TBL;

   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_ORDCUST_DETAIL_EXISTS is
      select distinct ordcust_no
        from ordcust_detail
       where ordcust_no in (select ordcust_no
                              from ordcust
                             where order_no = I_order_no);

   cursor C_LOCK_ORDCUST_DETAIL is
      select 'x'
        from ordcust_detail
       where ordcust_no = L_ordcust_no
         for update nowait;

   cursor C_ORDCUST_DETAIL is
      select oc.ordcust_no,
             os.item,
             os.ref_item,
             NULL,             -- original_item
             ol.qty_ordered,   -- qty_ordered_suom
             NULL,             -- qty_cancelled_suom
             im.standard_uom,  -- standard_uom
             im.standard_uom,  -- transaction_uom
             'N',              -- substitute_allowed_ind
             ol.unit_retail,   -- unit_retail
             s.currency_code,  -- retail_currency_code
             NULL,             -- comments
             sysdate,          -- create_datetime
             get_user,             -- create_id
             sysdate,          -- last_update_datetime
             get_user              -- last_update_id
        from ordcust oc,
             ordloc ol,
             ordsku os,
             item_master im,
             store s
       where oc.order_no = I_order_no
         and oc.order_no = ol.order_no
         and oc.order_no = os.order_no
         and os.item     = ol.item
         and os.item     = im.item
         and ol.location = s.store
         and (im.deposit_item_type is NULL or im.deposit_item_type = 'E');

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ORDCUST_DETAIL_EXISTS;
   fetch C_ORDCUST_DETAIL_EXISTS into L_ordcust_no;
   close C_ORDCUST_DETAIL_EXISTS;

   if L_ordcust_no is NULL then
   -- No ORDCUST_DETAIL record exists. Insert a new record.

      open C_ORDCUST_DETAIL;
      fetch C_ORDCUST_DETAIL BULK COLLECT into L_ordcust_detail_TBL;
      close C_ORDCUST_DETAIL;

      FORALL i IN L_ordcust_detail_TBL.FIRST..L_ordcust_detail_TBL.LAST
         insert into ordcust_detail(ordcust_no,
                                    item,
                                    ref_item,
                                    original_item,
                                    qty_ordered_suom,
                                    qty_cancelled_suom,
                                    standard_uom,
                                    transaction_uom,
                                    substitute_allowed_ind,
                                    unit_retail,
                                    retail_currency_code,
                                    comments,
                                    create_datetime,
                                    create_id,
                                    last_update_datetime,
                                    last_update_id)
                             values(L_ordcust_detail_TBL(i).ordcust_no,
                                    L_ordcust_detail_TBL(i).item,
                                    L_ordcust_detail_TBL(i).ref_item,
                                    L_ordcust_detail_TBL(i).original_item,
                                    L_ordcust_detail_TBL(i).qty_ordered_suom,
                                    L_ordcust_detail_TBL(i).qty_cancelled_suom,
                                    L_ordcust_detail_TBL(i).standard_uom,
                                    L_ordcust_detail_TBL(i).transaction_uom,
                                    L_ordcust_detail_TBL(i).substitute_allowed_ind,
                                    L_ordcust_detail_TBL(i).unit_retail,
                                    L_ordcust_detail_TBL(i).retail_currency_code,
                                    L_ordcust_detail_TBL(i).comments,
                                    L_ordcust_detail_TBL(i).create_datetime,
                                    L_ordcust_detail_TBL(i).create_id,
                                    L_ordcust_detail_TBL(i).last_update_datetime,
                                    L_ordcust_detail_TBL(i).last_update_id);

   else
      -- ORDCUST_DETAIL record exists
      -- Delete all ORDCUST_DETAIL records for this Order and then reinsert data
      open C_LOCK_ORDCUST_DETAIL;
      close C_LOCK_ORDCUST_DETAIL;

      delete from ordcust_detail
       where ordcust_no = L_ordcust_no;

      open C_ORDCUST_DETAIL;
      fetch C_ORDCUST_DETAIL BULK COLLECT into L_ordcust_detail_TBL;
      close C_ORDCUST_DETAIL;

      FORALL i IN L_ordcust_detail_TBL.FIRST..L_ordcust_detail_TBL.LAST
         insert into ordcust_detail(ordcust_no,
                                    item,
                                    ref_item,
                                    original_item,
                                    qty_ordered_suom,
                                    qty_cancelled_suom,
                                    standard_uom,
                                    transaction_uom,
                                    substitute_allowed_ind,
                                    unit_retail,
                                    retail_currency_code,
                                    comments,
                                    create_datetime,
                                    create_id,
                                    last_update_datetime,
                                    last_update_id)
                             values(L_ordcust_detail_TBL(i).ordcust_no,
                                    L_ordcust_detail_TBL(i).item,
                                    L_ordcust_detail_TBL(i).ref_item,
                                    L_ordcust_detail_TBL(i).original_item,
                                    L_ordcust_detail_TBL(i).qty_ordered_suom,
                                    L_ordcust_detail_TBL(i).qty_cancelled_suom,
                                    L_ordcust_detail_TBL(i).standard_uom,
                                    L_ordcust_detail_TBL(i).transaction_uom,
                                    L_ordcust_detail_TBL(i).substitute_allowed_ind,
                                    L_ordcust_detail_TBL(i).unit_retail,
                                    L_ordcust_detail_TBL(i).retail_currency_code,
                                    L_ordcust_detail_TBL(i).comments,
                                    L_ordcust_detail_TBL(i).create_datetime,
                                    L_ordcust_detail_TBL(i).create_id,
                                    L_ordcust_detail_TBL(i).last_update_datetime,
                                    L_ordcust_detail_TBL(i).last_update_id);

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             TO_CHAR(I_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_ORDCUST_DETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORD_INV_MGMT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN       ORDLOC.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'ORDER_SETUP_SQL.DELETE_ORD_INV_MGMT';

   L_exists        VARCHAR2(1);
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_ORD_INV_MGMT_EXISTS is
      select 'x'
        from ord_inv_mgmt
       where order_no = I_order_no;

   cursor C_LOCK_ORD_INV_MGMT is
      select 'x'
        from ord_inv_mgmt
       where order_no = I_order_no
         for update nowait;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ORD_INV_MGMT_EXISTS;
   fetch C_ORD_INV_MGMT_EXISTS into L_exists;
   close C_ORD_INV_MGMT_EXISTS;

   if L_exists is NOT NULL then
   -- ORD_INV_MGMT record exists. Delete the record as this order is already a CO PO.

      open C_LOCK_ORD_INV_MGMT;
      close C_LOCK_ORD_INV_MGMT;

      delete from ord_inv_mgmt
       where order_no = I_order_no;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             TO_CHAR(I_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ORD_INV_MGMT;
------------------------------------------------------------------------------------------------
FUNCTION CHECK_DIRECT_SHIP_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                                I_supplier        IN       ORDHEAD.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)   := 'ORDER_SETUP_SQL.CHECK_DIRECT_SHIP_ITEM';
   L_exists     VARCHAR2(1)    := NULL;

   cursor C_NOT_DIRECT_SHIP is
      select 'x'
        from ordsku o
       where o.order_no = I_order_no
         and exists (select 'x'
                       from item_supplier i
                      where i.supplier = I_supplier
                        and i.item = o.item
                        and i.direct_ship_ind = 'N')
         and rownum = 1;

BEGIN

   open C_NOT_DIRECT_SHIP;
   fetch C_NOT_DIRECT_SHIP into L_exists;
   close C_NOT_DIRECT_SHIP;

   if L_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_NOT_DIRECT_SHIP',
                                            I_supplier,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_DIRECT_SHIP_ITEM;
------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORDCUST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no        IN       ORDSKU.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64)   := 'ORDER_SETUP_SQL.DELETE_ORDCUST';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(30);

   cursor C_LOCK_ORDCUST is
      select 'x'
        from ordcust
       where order_no = I_order_no
         for update nowait;

   cursor C_LOCK_ORDCUST_DTL is
      select 'x'
        from ordcust_detail
       where ordcust_no in (select ordcust_no
                              from ordcust
                             where order_no = I_order_no)
         for update nowait;
BEGIN

   L_table := 'ORCUST_DETAIL';
   open C_LOCK_ORDCUST_DTL;
   close C_LOCK_ORDCUST_DTL;

   delete from ordcust_detail
    where ordcust_no in (select ordcust_no
                           from ordcust
                          where order_no = I_order_no);

   L_table := 'ORDCUST';
   open C_LOCK_ORDCUST;
   close C_LOCK_ORDCUST;

   delete from ordcust
    where order_no = I_order_no;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             TO_CHAR(I_order_no),
                                             NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ORDCUST;
------------------------------------------------------------------------------------------------
FUNCTION UPD_ORDLOC_WKSHT_CTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no          IN     ORDSKU.ORDER_NO%TYPE,
                        I_item              IN     ORDSKU.ITEM%TYPE,
                        I_origin_country_id IN     ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE)
  RETURN BOOLEAN
IS

  L_program VARCHAR2(64) := 'ORDER_SETUP_SQL.UPD_ORDLOC_WKSHT_CTRY';

BEGIN

  UPDATE ordloc_wksht
     SET origin_country_id = I_origin_country_id
   WHERE order_no          = I_order_no
     AND item              = I_item;

  RETURN TRUE;

EXCEPTION

   WHEN OTHERS THEN
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
     RETURN FALSE;

END UPD_ORDLOC_WKSHT_CTRY;
------------------------------------------------------------------------------------------------
FUNCTION DEL_ORDLOC_DISCOUNT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no      IN ORDSKU.ORDER_NO%TYPE,
                      I_item          IN ORDSKU.ITEM%TYPE)
  RETURN BOOLEAN
IS

  L_program VARCHAR2(64) := 'ORDER_SETUP_SQL.DEL_ORDLOC_DISCOUNT';

  CURSOR C_LOCK_DISCOUNT
  IS
    SELECT 'x'
    FROM ordloc_discount
    WHERE order_no = I_order_no
    AND (item      = I_item
    OR pack_no     = I_item) FOR UPDATE nowait;

BEGIN

  OPEN C_LOCK_DISCOUNT;
  CLOSE C_LOCK_DISCOUNT;
  --
  DELETE
  FROM ordloc_discount
  WHERE order_no = I_order_no
  AND (item      = I_item
  OR pack_no     = I_item);
  --
  RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
     RETURN FALSE;
END DEL_ORDLOC_DISCOUNT;
------------------------------------------------------------------------------------------------
FUNCTION REDIST_CANCEL(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no          IN     ORDLOC.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'ORDER_SETUP_SQL.REDIST_CANCEL';
   L_item          ORDLOC.ITEM%TYPE;
   L_location      ORDLOC.LOCATION%TYPE;
   L_loc_type      ORDLOC.LOC_TYPE%TYPE;
   L_qty_ordered   ORDLOC.QTY_ORDERED%TYPE;
   L_redist_qty    ORDLOC.QTY_ORDERED%TYPE;
   L_reinstate_qty ORDLOC.QTY_ORDERED%TYPE;
   L_order_no      ORDLOC.ORDER_NO%TYPE;

   L_cancel_date   ORDLOC.CANCEL_DATE%TYPE := get_vdate();
   L_cancel_id     ORDLOC.CANCEL_ID%TYPE   := get_user();

   cursor C_GET_ORDLOC_TEMP is
      select order_no,
             item,
             location,
             loc_type,
             unit_retail,
             qty_ordered,
             qty_prescaled,
             qty_received,
             last_received,
             last_rounded_qty,
             last_grp_rounded_qty,
             NVL(qty_cancelled,0) qty_cancelled,
             cancel_code,
             cancel_date,
             cancel_id,
             original_repl_qty,
             unit_cost,
             unit_cost_init,
             cost_source,
             non_scale_ind,
             tsf_po_link_no
        from ordloc_temp
       where order_no = I_order_no;

   cursor C_CHECK_ORDLOC_EXISTS is
      select order_no,
             NVL(qty_ordered,0)
        from ordloc
       where order_no = I_order_no
         and item     = L_item
         and location = L_location
         and loc_type = L_loc_type;

BEGIN

   FOR c_rec in C_GET_ORDLOC_TEMP LOOP

      L_item          := c_rec.item;
      L_location      := c_rec.location;
      L_loc_type      := c_rec.loc_type;
      L_reinstate_qty := NULL;
      L_qty_ordered   := NULL;

      open C_CHECK_ORDLOC_EXISTS;
      fetch C_CHECK_ORDLOC_EXISTS into L_order_no,
                                       L_qty_ordered;
      close C_CHECK_ORDLOC_EXISTS;

      if L_order_no is NOT NULL then

         L_redist_qty := L_qty_ordered - c_rec.qty_ordered;

         if L_redist_qty > 0 then /* new redist quantity is greater, reinstate cancel qty */

            /* If original order line has cancelled quantity, reinstate cancel quantity */
            if c_rec.qty_cancelled != 0 then

               if c_rec.qty_cancelled <= L_redist_qty then

                  L_reinstate_qty := c_rec.qty_cancelled;

                  SQL_LIB.SET_MARK('UPDATE',
                                   NULL,
                                   'ordloc',
                                   'order no: '||TO_CHAR(I_order_no));

                  update ordloc
                     set qty_cancelled = 0,
                         cancel_code   = NULL,
                         cancel_date   = NULL,
                         cancel_id     = NULL
                   where order_no      = I_order_no
                     and item          = L_item
                     and location      = L_location
                     and loc_type      = L_loc_type;

               else
                  L_reinstate_qty := L_redist_qty;

                  SQL_LIB.SET_MARK('UPDATE',
                                   NULL,
                                   'ordloc',
                                   'order no: '||TO_CHAR(I_order_no));

                  update ordloc
                     set qty_cancelled = c_rec.qty_cancelled - L_redist_qty,
                         cancel_code   = 'A',
                         cancel_date   = L_cancel_date,
                         cancel_id     = L_cancel_id
                   where order_no      = I_order_no
                     and item          = L_item
                     and location      = L_location
                     and loc_type      = L_loc_type;
               end if;

            end if; /* cancelled_qty != 0 */

         elsif L_redist_qty < 0 then /* new redist quantity is less, increase cancel qty */

            L_reinstate_qty :=  ABS(L_redist_qty);

            SQL_LIB.SET_MARK('UPDATE',
                             NULL,
                             'ordloc',
                             'order no: '||TO_CHAR(I_order_no));

            update ordloc
               set qty_cancelled = c_rec.qty_cancelled - L_redist_qty,
                   cancel_code   = 'A',
                   cancel_date   = L_cancel_date,
                   cancel_id     = L_cancel_id
             where order_no      = I_order_no
               and item          = L_item
               and location      = L_location
               and loc_type      = L_loc_type;

         end if;

      /* re-insert ordloc record from ordloc_temp with cancelled quantity */
      else

         L_reinstate_qty :=  c_rec.qty_ordered;

         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'ordloc',
                          'order no: '||TO_CHAR(I_order_no));

         insert into ordloc (order_no,
                             item,
                             loc_type,
                             location,
                             unit_retail,
                             qty_ordered,
                             qty_prescaled,
                             qty_received,
                             qty_cancelled,
                             last_received,
                             cancel_code,
                             cancel_date,
                             cancel_id,
                             original_repl_qty,
                             unit_cost,
                             unit_cost_init,
                             cost_source,
                             non_scale_ind)
                     values (c_rec.order_no,
                             c_rec.item,
                             c_rec.loc_type,
                             c_rec.location,
                             c_rec.unit_retail,
                             0,
                             c_rec.qty_prescaled,
                             c_rec.qty_received,
                             NVL(c_rec.qty_cancelled,0) + c_rec.qty_ordered,
                             c_rec.last_received,
                             'A',
                             L_cancel_date,
                             L_cancel_id,
                             c_rec.original_repl_qty,
                             c_rec.unit_cost,
                             c_rec.unit_cost_init,
                             c_rec.cost_source,
                             'N');

      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END REDIST_CANCEL;
------------------------------------------------------------------------------------------------
END ORDER_SETUP_SQL;
/
