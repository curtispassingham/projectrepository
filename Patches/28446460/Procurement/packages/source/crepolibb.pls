CREATE OR REPLACE PACKAGE BODY CREATE_PO_LIB_SQL AS

   LP_import_type       ORDHEAD.IMPORT_TYPE%TYPE;
   LP_contr_ind         VARCHAR2(1);

---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ORDHEAD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordhead_rec    IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_REC)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ORD_INV_MGMT(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no                  IN     ORDHEAD.ORDER_NO%TYPE,
                             I_src_order                 IN     ORDHEAD.SPLIT_REF_ORDNO%TYPE,
                             I_pool_supp                 IN     ORD_INV_MGMT.POOL_SUPPLIER%TYPE,
                             I_file_id                   IN     ORD_INV_MGMT.FILE_ID%TYPE,
                             I_contract_ind              IN     ORDHEAD.CONTRACT_NO%TYPE,
                             I_total_order_due_indicator IN     ORD_INV_MGMT.DUE_ORD_IND%TYPE,
                             I_ltl_flag                  IN     NUMBER,
                             I_order_status              IN     ORDHEAD.STATUS%TYPE,
                             I_sup_inv_mgmt_rowid        IN     VARCHAR2,
                             I_ord_inv_mgmt_rowid        IN     VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION HEADER_DEFAULTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_EXCHANGE_RATE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exchange_rate     OUT  ORDHEAD.EXCHANGE_RATE%TYPE,
                           I_currency_code  IN      ORDHEAD.CURRENCY_CODE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_CONTRACT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_ordhead_rec    IN      ADD_LINE_ITEM_SQL.ORDHEAD_REC)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION BUILD_DATES(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_ordhead_rec         IN      ADD_LINE_ITEM_SQL.ORDHEAD_REC,
                     O_not_before_date     OUT     ORDHEAD.NOT_BEFORE_DATE%TYPE,
                     O_not_after_date      OUT     ORDHEAD.NOT_AFTER_DATE%TYPE,
                     O_earliest_ship_date  OUT     ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
                     O_latest_ship_date    OUT     ORDHEAD.LATEST_SHIP_DATE%TYPE,
                     O_pickup_date         OUT     ORDHEAD.PICKUP_DATE%TYPE,
                     O_eow_date            OUT     ORDHEAD.OTB_EOW_DATE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION ORDHEAD_DEFAULTS(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_ordhead_rec           IN      ADD_LINE_ITEM_SQL.ORDHEAD_REC,
                          O_order_type               OUT  ORDHEAD.ORDER_TYPE%TYPE,
                          O_orig_ind                 OUT  ORDHEAD.ORIG_IND%TYPE,
                          O_promotion                OUT  ORDHEAD.PROMOTION%TYPE,
                          O_ship_pay_method          OUT  ORDHEAD.SHIP_PAY_METHOD%TYPE,
                          O_fob_trans_res            OUT  ORDHEAD.FOB_TRANS_RES%TYPE,
                          O_fob_trans_res_desc       OUT  ORDHEAD.FOB_TRANS_RES_DESC%TYPE,
                          O_fob_title_pass           OUT  ORDHEAD.FOB_TITLE_PASS%TYPE,
                          O_fob_title_pass_desc      OUT  ORDHEAD.FOB_TITLE_PASS_DESC%TYPE,
                          O_include_on_order_ind     OUT  ORDHEAD.INCLUDE_ON_ORDER_IND%TYPE,
                          O_factory                  OUT  ORDHEAD.FACTORY%TYPE,
                          O_freight_contract_no      OUT  ORDHEAD.FREIGHT_CONTRACT_NO%TYPE,
                          O_po_type                  OUT  ORDHEAD.PO_TYPE%TYPE,
                          O_pickup_no                OUT  ORDHEAD.PICKUP_NO%TYPE,
                          O_comment_desc             OUT  ORDHEAD.COMMENT_DESC%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ORDSKU(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_ordsku_rec       IN      ADD_LINE_ITEM_SQL.ORDSKU_REC,
                       I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                       I_pickup_loc       IN      ORDHEAD.PICKUP_LOC%TYPE,
                       I_src_order_no     IN      ORDHEAD.SPLIT_REF_ORDNO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION BUILD_SKU_DATES(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_earliest_ship_date    OUT  ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
                         O_latest_ship_date      OUT  ORDHEAD.LATEST_SHIP_DATE%TYPE,
                         O_pickup_loc            OUT  ORDHEAD.PICKUP_LOC%TYPE,
                         O_pickup_no             OUT  ORDHEAD.PICKUP_NO%TYPE,
                         I_ordsku_rec         IN      ADD_LINE_ITEM_SQL.ORDSKU_REC,
                         I_order_no           IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_REF_ITEM(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_item           IN      ITEM_MASTER.ITEM%TYPE,
                      O_ref_item          OUT  ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_UNIT_RETAIL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_ordloc_rec     IN      ADD_LINE_ITEM_SQL.ORDLOC_REC,
                         O_unit_retail       OUT  ITEM_LOC.UNIT_RETAIL%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION BUILD_PACK_RETAIL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item           IN      ITEM_LOC.ITEM%TYPE,
                           I_loc            IN      ITEM_LOC.LOC%TYPE,
                           I_loc_type       IN      ITEM_LOC.LOC_TYPE%TYPE,
                           O_unit_retail       OUT  ITEM_LOC.UNIT_RETAIL%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_UNIT_COST(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_unit_cost             OUT  ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                       I_item               IN      ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                       I_supplier           IN      ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                       I_cntry              IN      ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                       I_loc                IN      ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION ITEM_DEFAULTS(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no           IN      ORDSKU.ORDER_NO%TYPE,
                       I_item               IN      ORDSKU.ITEM%TYPE,
                       I_ctry               IN      ORDSKU.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ORDLOC(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_ordloc_rec         IN      ADD_LINE_ITEM_SQL.ORDLOC_REC,
                       I_order_no           IN      ORDHEAD.ORDER_NO%TYPE,
                       I_src_order_no       IN      ORDHEAD.SPLIT_REF_ORDNO%TYPE,
                       I_supplier           IN      ORDHEAD.SUPPLIER%TYPE,
                       I_cntry              IN      ORDSKU.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_EST_INSTOCK_DATE(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                              I_item              IN      ORDHEAD.ITEM%TYPE,
                              I_location          IN      ORDHEAD.LOCATION%TYPE,
                              I_supplier          IN      ORDHEAD.SUPPLIER%TYPE,
                              O_est_instock_date     OUT  ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ALLOC_HEADER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_ordloc_rec     IN      ADD_LINE_ITEM_SQL.ORDLOC_REC,
                             I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                             O_alloc_no          OUT  ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_ALLOC_NO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_alloc_no          OUT  ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ALLOC_DETAIL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_ordloc_rec     IN      ADD_LINE_ITEM_SQL.ORDLOC_REC,
                             I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                             I_alloc_no       IN      ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_SOURCE_TABLE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_rr_rowid       IN      VARCHAR2,
                             I_ir_rowid       IN      VARCHAR2,
                             I_bw_rowid       IN      VARCHAR2,
                             I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                             I_alloc_no       IN      ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION ADD_COST_COMP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_ordsku_rec     IN      ADD_LINE_ITEM_SQL.ORDSKU_REC,
                       I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION VERIFY_USER_ROLL_PRIV(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_non_fatal         OUT  BOOLEAN,
                               I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION VERIFY_LC_HTS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_non_fatal           OUT  BOOLEAN,
                       I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                       I_import_order_ind IN      ORDHEAD.IMPORT_ORDER_IND%TYPE,
                       I_payment_method   IN      ORDHEAD.PAYMENT_METHOD%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION VERIFY_ITEM_MIN_MAX(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_non_fatal         OUT  BOOLEAN,
                             I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                             I_supplier       IN      ORDHEAD.SUPPLIER%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION VERIFY_VEND_MIN(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_non_fatal         OUT  BOOLEAN,
                         I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION VERIFY_OTB(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    O_non_fatal         OUT  BOOLEAN,
                    I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                    I_order_status   IN      ORDHEAD.STATUS%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION APPLY_DEALS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION SET_WORKSHEET_STATUS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                              I_reject_code    IN      ORDHEAD.REJECT_CODE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION SET_APPROVE_SUBMIT_STATUS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_order_status   IN      ORDHEAD.STATUS%TYPE,
                                   I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION VALID_SUPS_IMP_EXP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_ordhead_rec    IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_REC)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION VALID_SUPS_ROUTING_LOC(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_ordhead_rec    IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_REC)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION CREATE_F_ORDER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordhead_rec    IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_REC)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- Function: DO_INSERTS
-- Purpose : This function will take all the info in the order table types and create orders.
/*
LOOP ON ORDHEAD
  insert ordhead record
  insert ord_inv_mgmt record
  call necessary packages for new order creation
  LOOP ON ORDSKU in the current ORDHEAD record
    if the the item is orderable
      insert ordsku record
      call necessary package for new ordsku creation
      LOOP ON ORDLOC in the current ORDSKU record in the current ORDHEAD record
        if wh portion of xdock
          -- These will always come before the store portions of the
          -- xdock ensuring a allocation header will already be
          -- created when the store portions are reached because of
          -- the ordering of the driving cursor.
          insert a ordloc record (total xdock record)
          create an allocation header
          update repl_results with the order
        else if store portion of xdock
          -- the allocation header record will always be there already
          -- due to the ordering of the driving cursor
          insert an allocation detail record
          update repl_results with the order and allocation numbers
        else direct to store or direct to wh
          update repl_results with the order
        end if
      END ORDLOC LOOP
    end if
  END ORDSKU LOOP
END ORDHEAD LOOP
 */
---------------------------------------------------------------------------------------------------------
FUNCTION DO_INSERTS(I_ordhead_tbl       IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                    O_orders_processed  IN OUT  NUMBER,
                    O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(100) := 'CREATE_PO_LIB_SQL.DO_INSERTS';

   L_item_record        ITEM_MASTER%ROWTYPE;
   
   L_orderable_ind      ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_deposit_item_type  ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE;
   L_container_item     ITEM_MASTER.CONTAINER_ITEM%TYPE;
   
   L_ordsku_rec         ADD_LINE_ITEM_SQL.ORDSKU_REC;
   L_alloc_no           ALLOC_HEADER.ALLOC_NO%TYPE;
   L_ordloc_rec         ADD_LINE_ITEM_SQL.ORDLOC_REC;
   L_unit_cost          ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE;
   
   L_fc_loc             ORDLOC.LOCATION%TYPE;

   cursor C_FRANCHISE_LOC(L_order_no  ORDHEAD.ORDER_NO%TYPE) is
      select location
        from ordloc ol,
             store s
       where ol.order_no = L_order_no
         and ol.loc_type = 'S'
         and ol.location = s.store
         and s.store_type = 'F'
         and rownum   = 1;

BEGIN 
   /* LOOP on ordhead records */
   if I_ordhead_tbl is NOT NULL and I_ordhead_tbl.COUNT > 0 then 
      for i in I_ordhead_tbl.FIRST..I_ordhead_tbl.LAST
      LOOP 
          /* increment the number of orders processed for restart recovery purposes */
          O_orders_processed := O_orders_processed + 1;
          /* No order is being inserted, no reason to loop on rest of records*/
          if I_ordhead_tbl(i).write_ind = 'Y' then
             /* insert a ordhead record */
             if INSERT_ORDHEAD(O_error_message,
                               I_ordhead_tbl(i)) = FALSE then
                return FALSE;
             end if;
             /* LOOP on ORDSKU records for the current ORDHEAD record */
             if I_ordhead_tbl(i).ordsku_tbl is NOT NULL and I_ordhead_tbl(i).ordsku_tbl.COUNT > 0 then 
                for j in I_ordhead_tbl(i).ordsku_tbl.FIRST..I_ordhead_tbl(i).ordsku_tbl.LAST
                LOOP 
                    if not ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                                           L_item_record,
                                                           I_ordhead_tbl(i).ordsku_tbl(j).item) then
                       return FALSE;
                    end if;

                    L_orderable_ind      := L_item_record.orderable_ind;
                    L_deposit_item_type  := L_item_record.deposit_item_type;
                    L_container_item     := L_item_record.container_item;

                    if L_orderable_ind <> 'N' then
                       /* insert an ordsku record */
                       if INSERT_ORDSKU(O_error_message,
                                        I_ordhead_tbl(i).ordsku_tbl(j),
                                        I_ordhead_tbl(i).order_no,
                                        I_ordhead_tbl(i).sup_info.pickup_loc,
                                        I_ordhead_tbl(i).split_ref_ord_no) = FALSE then
                          return FALSE;
                       end if;

                       if L_deposit_item_type = 'E' then
                          /* insert an ordsku record for the container item*/
                          L_ordsku_rec      := I_ordhead_tbl(i).ordsku_tbl(j);
                          L_ordsku_rec.item := L_container_item;
                          if INSERT_ORDSKU(O_error_message,
                                           L_ordsku_rec,
                                           I_ordhead_tbl(i).order_no,
                                           I_ordhead_tbl(i).sup_info.pickup_loc,
                                           I_ordhead_tbl(i).split_ref_ord_no) = FALSE then
                             return FALSE;
                          end if;
                       end if; 
                       /* LOOP on ORDLOC records for the current ORDSKU record in the current ORDHEAD record */
                       if I_ordhead_tbl(i).ordsku_tbl(j).ordloc_tbl is NOT NULL and I_ordhead_tbl(i).ordsku_tbl(j).ordloc_tbl.COUNT > 0 then 
                          for k in I_ordhead_tbl(i).ordsku_tbl(j).ordloc_tbl.FIRST..I_ordhead_tbl(i).ordsku_tbl(j).ordloc_tbl.LAST
                          LOOP
                              /* this is a xdock record */
                              if I_ordhead_tbl(i).ordsku_tbl(j).ordloc_tbl(k).xdock_ind = 'Y' then
                                 /* this is the wh portion of a xdock */
                                 if I_ordhead_tbl(i).ordsku_tbl(j).ordloc_tbl(k).xdock_store = -1 then
                                    /* this is a xdock total record, need a ordloc record for the wh, and
                                       an alloc_header record for the item for this ordloc record*/
                                    if INSERT_ORDLOC(O_error_message,
                                                     I_ordhead_tbl(i).ordsku_tbl(j).ordloc_tbl(k),
                                                     I_ordhead_tbl(i).order_no,
                                                     I_ordhead_tbl(i).split_ref_ord_no,
                                                     I_ordhead_tbl(i).supplier,
                                                     I_ordhead_tbl(i).ordsku_tbl(j).ctry) = FALSE then
                                       return FALSE;
                                    end if;
                                    /* insert an alloc_header record the driving cursor will ensure
                                       that the WH record will always come first, so the detail won't
                                       be inserted before the alloc_header */
                                    if INSERT_ALLOC_HEADER(O_error_message,
                                                           I_ordhead_tbl(i).ordsku_tbl(j).ordloc_tbl(k),
                                                           I_ordhead_tbl(i).order_no,
                                                           L_alloc_no) = FALSE then
                                       return FALSE;
                                    end if;
                                 else
                                    /* this is a store portion of a xdock, need an alloc_detail record
                                       for this ordloc record */
                                    if INSERT_ALLOC_DETAIL(O_error_message,
                                                           I_ordhead_tbl(i).ordsku_tbl(j).ordloc_tbl(k),
                                                           I_ordhead_tbl(i).order_no,
                                                           L_alloc_no) = FALSE then
                                       return FALSE;
                                    end if;
                                 end if;
                              else
                                 /* this is a direct to store or warehouse not a xdock,
                                 need a ordloc record */
                                 if INSERT_ORDLOC(O_error_message,
                                                  I_ordhead_tbl(i).ordsku_tbl(j).ordloc_tbl(k),
                                                  I_ordhead_tbl(i).order_no,
                                                  I_ordhead_tbl(i).split_ref_ord_no,
                                                  I_ordhead_tbl(i).supplier,
                                                  I_ordhead_tbl(i).ordsku_tbl(j).ctry) = FALSE then
                                    return FALSE;
                                 end if;
                              end if;

                              if L_deposit_item_type = 'E' then
                                 /* process the container item */
                                 L_ordloc_rec      := I_ordhead_tbl(i).ordsku_tbl(j).ordloc_tbl(k);
                                 L_ordloc_rec.item := L_container_item;
                                 /* Get the unit cost for the container item */
                                 if GET_UNIT_COST(O_error_message,
                                                  L_unit_cost,
                                                  L_container_item,
                                                  I_ordhead_tbl(i).supplier,
                                                  I_ordhead_tbl(i).ordsku_tbl(j).ctry,
                                                  I_ordhead_tbl(i).ordsku_tbl(j).ordloc_tbl(k).loc) = FALSE then
                                    return FALSE;
                                 end if;

                                 L_ordloc_rec.unit_cost      := L_unit_cost;
                                 L_ordloc_rec.unit_cost_init := L_unit_cost;

                                 if L_ordloc_rec.xdock_ind = 'Y' then
                                    /* this is the wh portion of a xdock */
                                    if L_ordloc_rec.xdock_store = -1 then
                                       /* this is a xdock total record, need a ordloc record for the wh, and
                                          an alloc_header record for the item */
                                       if INSERT_ORDLOC(O_error_message,
                                                        L_ordloc_rec,
                                                        I_ordhead_tbl(i).order_no,
                                                        I_ordhead_tbl(i).split_ref_ord_no,
                                                        I_ordhead_tbl(i).supplier,
                                                        I_ordhead_tbl(i).ordsku_tbl(j).ctry) = FALSE then
                                          return FALSE;
                                       end if;
                                    end if;
                                 else 
                                    /* this is a direct to store or warehouse not a xdock,
                                       need a ordloc record */
                                    if INSERT_ORDLOC(O_error_message,
                                                     L_ordloc_rec,
                                                     I_ordhead_tbl(i).order_no,
                                                     I_ordhead_tbl(i).split_ref_ord_no,
                                                     I_ordhead_tbl(i).supplier,
                                                     I_ordhead_tbl(i).ordsku_tbl(j).ctry) = FALSE then
                                       return FALSE;
                                    end if;
                                 end if;
                              end if;
                          END LOOP; /* end ordloc loop */
                       end if;
                       /* this call needs to be done after all the location
                          records are inserted in order for the package to work,
                          and it should be only called for the item that has
                          been actually added to the order. */
                       if ADD_LINE_ITEM_SQL.LP_elc_ind = 'Y' and I_ordhead_tbl(i).ordsku_tbl(j).write_ind = 'Y' then
                          if ADD_COST_COMP(O_error_message,
                                           I_ordhead_tbl(i).ordsku_tbl(j),
                                           I_ordhead_tbl(i).order_no) = FALSE then
                             return FALSE;
                          end if;
                       end if;
                    end if;
                END LOOP; /*end ordsku loop */
             end if;

             SQL_LIB.SET_MARK('OPEN','C_FRANCHISE_LOC','ORDLOC, STORE','order_no:' || I_ordhead_tbl(i).order_no);
             open C_FRANCHISE_LOC(I_ordhead_tbl(i).order_no);
             SQL_LIB.SET_MARK('FETCH`','C_FRANCHISE_LOC','ORDLOC, STORE','order_no:' || I_ordhead_tbl(i).order_no);
             fetch C_FRANCHISE_LOC into L_fc_loc;
             SQL_LIB.SET_MARK('CLOSE','C_FRANCHISE_LOC','ORDLOC, STORE','order_no:' || I_ordhead_tbl(i).order_no);
             close C_FRANCHISE_LOC;

             if L_fc_loc is NOT NULL then
                I_ordhead_tbl(i).order_loc := L_fc_loc;
                if CREATE_F_ORDER(O_error_message,
                                  I_ordhead_tbl(i)) = FALSE then
                   return FALSE;
                end if;
             end if;
          end if;
      END LOOP; /* end order loop */
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END DO_INSERTS;
---------------------------------------------------------------------------------------------------------
-- Function: INSERT_ORDHEAD
-- Purpose : Determine if an ordhead record needs to be inserted.  If so,set up dates and the
--           exchange rate then insert the record. Call the necessary packages for order creation and
--           update the contract tables if a contract order.
--           Each ordhead record also gets a ord_inv_mgmt record inserted.
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ORDHEAD(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordhead_rec    IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_REC)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(100) := 'CREATE_PO_LIB_SQL.INSERT_ORDHEAD';

   L_not_before_date         ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_not_after_date          ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_earliest_ship_date      ORDHEAD.EARLIEST_SHIP_DATE%TYPE;
   L_latest_ship_date        ORDHEAD.LATEST_SHIP_DATE%TYPE;
   L_pickup_date             ORDHEAD.PICKUP_DATE%TYPE;
   L_eow_date                ORDHEAD.OTB_EOW_DATE%TYPE;

   L_exchange_rate           ORDHEAD.EXCHANGE_RATE%TYPE;

   L_order_type              ORDHEAD.ORDER_TYPE%TYPE := NULL;
   L_orig_ind                ORDHEAD.ORIG_IND%TYPE := 5;
   L_promotion               ORDHEAD.PROMOTION%TYPE := NULL;
   L_ship_pay_method         ORDHEAD.SHIP_PAY_METHOD%TYPE := NULL;
   L_fob_trans_res           ORDHEAD.FOB_TRANS_RES%TYPE := NULL;
   L_fob_trans_res_desc      ORDHEAD.FOB_TRANS_RES_DESC%TYPE := NULL;
   L_fob_title_pass          ORDHEAD.FOB_TITLE_PASS%TYPE := NULL;
   L_fob_title_pass_desc     ORDHEAD.FOB_TITLE_PASS_DESC%TYPE := NULL;
   L_include_on_order_ind    ORDHEAD.INCLUDE_ON_ORDER_IND%TYPE := NULL;
   L_factory                 ORDHEAD.FACTORY%TYPE := NULL;
   L_freight_contract_no     ORDHEAD.FREIGHT_CONTRACT_NO%TYPE := NULL;
   L_po_type                 ORDHEAD.PO_TYPE%TYPE := NULL;
   L_pickup_no               ORDHEAD.PICKUP_NO%TYPE := NULL;
   L_comment_desc            ORDHEAD.COMMENT_DESC%TYPE := NULL;

   L_import_country_id       ORDHEAD.IMPORT_COUNTRY_ID%TYPE;

   L_clear_zone_exists       BOOLEAN                  := FALSE;
   L_clearing_zone           OUTLOC.OUTLOC_ID%TYPE    := NULL;
   L_outloc_desc             OUTLOC.OUTLOC_DESC%TYPE;

BEGIN

   if BUILD_DATES(O_error_message,
                  I_ordhead_rec,
                  L_not_before_date,
                  L_not_after_date,
                  L_earliest_ship_date,
                  L_latest_ship_date,
                  L_pickup_date,
                  L_eow_date) = FALSE then
      return FALSE;
   end if;

   if GET_EXCHANGE_RATE(O_error_message,
                        L_exchange_rate,
                        I_ordhead_rec.sup_info.currency_code) = FALSE then
      return FALSE;
   end if;

   if ORDHEAD_DEFAULTS(O_error_message,
                       I_ordhead_rec,
                       L_order_type,
                       L_orig_ind,
                       L_promotion,
                       L_ship_pay_method,
                       L_fob_trans_res,
                       L_fob_trans_res_desc,
                       L_fob_title_pass,
                       L_fob_title_pass_desc,
                       L_include_on_order_ind,
                       L_factory,
                       L_freight_contract_no,
                       L_po_type,
                       L_pickup_no,
                       L_comment_desc) = FALSE then
      return FALSE;
   end if;

   /* make sure status is not approved if the order is not due */
   if I_ordhead_rec.order_status = 'A' and
      I_ordhead_rec.sup_info.due_ord_process_ind = 'Y' and
      I_ordhead_rec.sup_info.due_ord_ind = 'N' then 
      I_ordhead_rec.order_status := 'W';
   end if;

   if I_ordhead_rec.import_order = 'Y' then
      if VALID_SUPS_IMP_EXP(O_error_message,
                            I_ordhead_rec) = FALSE then
         return FALSE;
      end if;
      if VALID_SUPS_ROUTING_LOC(O_error_message,
                                I_ordhead_rec) = FALSE then
         return FALSE;
      end if;

      L_import_country_id := I_ordhead_rec.import_country_id;

      if OUTSIDE_LOCATION_SQL.GET_PRIM_CLEAR_ZONE_IMP_CTRY(O_error_message,
                                                           L_clear_zone_exists,
                                                           L_clearing_zone,
                                                           L_outloc_desc,
                                                           L_import_country_id) = FALSE then
         return FALSE;
      end if;

      if L_clear_zone_exists = TRUE then
         I_ordhead_rec.clearing_zone_id := L_clearing_zone;
      else
         I_ordhead_rec.clearing_zone_id := NULL;
      end if;
   else
      I_ordhead_rec.import_id         := NULL;
      LP_import_type                  := NULL;
      I_ordhead_rec.routing_loc_id    := NULL;
      I_ordhead_rec.clearing_zone_id  := NULL;
   end if;

   insert into ordhead(order_no,
                       order_type,
                       dept,
                       buyer,
                       supplier,
                       supp_add_seq_no,
                       loc_type,
                       location,
                       promotion,
                       qc_ind,
                       written_date,
                       not_before_date,
                       not_after_date,
                       otb_eow_date,
                       earliest_ship_date,
                       latest_ship_date,
                       close_date,
                       terms,
                       freight_terms,
                       orig_ind,
                       payment_method,
                       backhaul_type,
                       ship_method,
                       purchase_type,
                       status,
                       orig_approval_date,
                       orig_approval_id,
                       ship_pay_method,
                       fob_trans_res,
                       fob_trans_res_desc,
                       fob_title_pass,
                       fob_title_pass_desc,
                       edi_sent_ind,
                       edi_po_ind,
                       import_order_ind,
                       import_country_id,
                       po_ack_recvd_ind,
                       include_on_order_ind,
                       vendor_order_no,
                       exchange_rate,
                       agent,
                       discharge_port,
                       lading_port,
                       freight_contract_no,
                       po_type,
                       pre_mark_ind,
                       currency_code,
                       reject_code,
                       contract_no,
                       last_sent_rev_no,
                       split_ref_ordno,
                       pickup_loc,
                       pickup_no,
                       pickup_date,
                       app_datetime,
                       comment_desc,
                       partner_type_1,
                       partner1,
                       partner_type_2,
                       partner2,
                       partner_type_3,
                       partner3,
                       factory,
                       import_id,
                       import_type,
                       routing_loc_id,
                       clearing_zone_id)
               values (I_ordhead_rec.order_no,                                                            /*order_no*/
                       NVL(L_order_type,DECODE(ADD_LINE_ITEM_SQL.LP_batch_ind, 'N', 'BRB', 'ARB')),       /*order_type*/
                       DECODE(I_ordhead_rec.dept,'-1', NULL,I_ordhead_rec.dept),                          /*dept-only if dept order*/
                       DECODE(I_ordhead_rec.dept,'-1', NULL,I_ordhead_rec.sup_info.buyer),                /*buyer-only if dept order*/
                       I_ordhead_rec.supplier,                                                            /*supplier*/
                       I_ordhead_rec.sup_info.seq_no,                                                     /*supp_add_seq_no*/
                       I_ordhead_rec.order_loc_type,                                                      /*location*/
                       I_ordhead_rec.order_loc,                                                           /*loc_type*/
                       L_promotion,                                                                       /*promotion*/
                       I_ordhead_rec.sup_info.qc_ind,                                                     /*qc_ind*/
                       ADD_LINE_ITEM_SQL.LP_vdate,                                                        /*written_date*/
                       L_not_before_date,                                                                 /*not_before_date*/
                       L_not_after_date,                                                                  /*not_after_date*/
                       L_eow_date,                                                                        /*otb_eow_date*/
                       L_earliest_ship_date,                                                              /*earliest_ship_date*/
                       L_latest_ship_date,                                                                /*latest_ship_date*/
                       NULL,                                                                              /*close_date*/
                       I_ordhead_rec.sup_info.terms,                                                      /*terms*/
                       I_ordhead_rec.sup_info.freight_terms,                                              /*freight_terms*/
                       NVL(L_orig_ind,DECODE(ADD_LINE_ITEM_SQL.LP_batch_ind,'N', 3, 0)),                  /*orig_ind*/
                       I_ordhead_rec.sup_info.payment_method,                                             /*payment_method*/
                       DECODE(I_ordhead_rec.sup_info.purchase_type,'FOB', 'C', 'BACK', 'C', NULL),        /*backhaul_type*/
                       I_ordhead_rec.sup_info.ship_method,                                                /*ship_method*/
                       I_ordhead_rec.sup_info.purchase_type,                                              /*purchase_type*/
                       'W',                                                                               /*status*/
                       NULL,                                                                              /*orig_approval_date*/
                       NULL,                                                                              /*orig_approval_id*/
                       L_ship_pay_method,                                                                 /*ship_pay_method*/
                       L_fob_trans_res,                                                                   /*fob_trans_res*/
                       L_fob_trans_res_desc,                                                              /*fob_trans_rec_desc*/
                       NVL(L_fob_title_pass,                                                              /*fob_title_pass*/
                           DECODE(ADD_LINE_ITEM_SQL.LP_fob_title_pass, '-1', NULL,
                                  ADD_LINE_ITEM_SQL.LP_fob_title_pass)),
                       NVL(L_fob_title_pass_desc,                                                         /*fob_title_pass_desc*/
                           DECODE(ADD_LINE_ITEM_SQL.LP_fob_title_pass_desc, 'NONE' ,NULL, ADD_LINE_ITEM_SQL.LP_fob_title_pass_desc)),
                       'N',                                                                               /*edi_sent_ind*/
                       I_ordhead_rec.sup_info.edi_po_ind,                                                 /*edi_po_ind*/
                       I_ordhead_rec.import_order,                                                        /*import_order_ind*/
                       I_ordhead_rec.import_country_id,                                                   /*import_country_id*/
                       'N',                                                                               /*po_ack_recvd_ind*/
                       NVL(L_include_on_order_ind,'Y'),                                                   /*include_on_order_ind*/
                       NULL,                                                                              /*vendor_order_no*/
                       L_exchange_rate,                                                                   /*exchange_rate*/
                       I_ordhead_rec.sup_info.agent,                                                      /*agent*/
                       I_ordhead_rec.sup_info.discharge_port,                                             /*dischage_port*/
                       I_ordhead_rec.sup_info.lading_port,                                                /*lading_port*/
                       L_freight_contract_no,                                                             /*freight_contract_no*/
                       L_po_type,                                                                         /*po_type*/
                       I_ordhead_rec.sup_info.pre_mark_ind,                                               /*pre_mark_ind*/
                       I_ordhead_rec.sup_info.currency_code,                                              /*currency_code*/
                       NULL,                                                                              /*reject_code*/
                       DECODE(I_ordhead_rec.contract, '-1', NULL, I_ordhead_rec.contract),                   /*contract_no*/
                       NULL,                                                                              /*last_sent_rev_no*/
                       I_ordhead_rec.split_ref_ord_no,
                       I_ordhead_rec.sup_info.pickup_loc,                                                 /*pickup_loc*/
                       L_pickup_no,                                                                       /*pickup_no*/
                       L_pickup_date,                                                                     /*pickup_date*/
                       NULL,                                                                              /*app_datetime*/
                       L_comment_desc,                                                                    /*comment desc*/
                       I_ordhead_rec.sup_info.partner_type_1,                                             /*partner type1*/
                       I_ordhead_rec.sup_info.partner_1,                                                  /*partner1*/
                       I_ordhead_rec.sup_info.partner_type_2,                                             /*partner type2*/
                       I_ordhead_rec.sup_info.partner_2,                                                  /*partner2*/
                       I_ordhead_rec.sup_info.partner_type_3,                                             /*partner type3*/
                       I_ordhead_rec.sup_info.partner_3,                                                  /*partner3*/
                       I_ordhead_rec.sup_info.factory,                                                    /*factory*/
                       I_ordhead_rec.import_id,                                                           /*import_id*/
                       LP_import_type,                                                                    /*import_type*/
                       I_ordhead_rec.routing_loc_id,                                                      /*routing_loc_id*/
                       I_ordhead_rec.clearing_zone_id);                                                   /*clearing_zone_id*/


   insert into l10n_doc_details_gtt(doc_id,
                                    doc_type,
                                    country_id)
                            values (I_ordhead_rec.order_no,
                                    'PO',
                                    I_ordhead_rec.import_country_id);

   if HEADER_DEFAULTS(O_error_message,
                      I_ordhead_rec.order_no) = FALSE then
      return FALSE;
   end if;

   if UPDATE_CONTRACT(O_error_message,
                      I_ordhead_rec) = FALSE then
      return FALSE;
   end if;
   /* create the ord_inv_mgmt record for the new order */
   if INSERT_ORD_INV_MGMT(O_error_message,
                          I_ordhead_rec.order_no,
                          I_ordhead_rec.split_ref_ord_no,
                          I_ordhead_rec.pool_supp,
                          I_ordhead_rec.file_id,
                          I_ordhead_rec.contract,
                          I_ordhead_rec.sup_info.due_ord_ind,
                          I_ordhead_rec.sup_info.ltl_flag,
                          I_ordhead_rec.order_status,
                          I_ordhead_rec.sup_info.sup_inv_mgmt_rowid,
                          I_ordhead_rec.sup_info.ord_inv_mgmt_rowid) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END INSERT_ORDHEAD;
---------------------------------------------------------------------------------------------------------
-- Function: INSERT_ORD_INV_MGMT
-- Purpose : Inserts a record into ord_inv_mgmt.  Most values are taken from the sup_inv_mgmt record
--           for the supplier or supplier/dept vendor line that this order is being created for.
--           If no record exists for the supplier or supplier/dept a default row is created.
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ORD_INV_MGMT(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no                  IN     ORDHEAD.ORDER_NO%TYPE,
                             I_src_order                 IN     ORDHEAD.SPLIT_REF_ORDNO%TYPE,
                             I_pool_supp                 IN     ORD_INV_MGMT.POOL_SUPPLIER%TYPE,
                             I_file_id                   IN     ORD_INV_MGMT.FILE_ID%TYPE,
                             I_contract_ind              IN     ORDHEAD.CONTRACT_NO%TYPE,
                             I_total_order_due_indicator IN     ORD_INV_MGMT.DUE_ORD_IND%TYPE,
                             I_ltl_flag                  IN     NUMBER,
                             I_order_status              IN     ORDHEAD.STATUS%TYPE,
                             I_sup_inv_mgmt_rowid        IN     VARCHAR2,
                             I_ord_inv_mgmt_rowid        IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program      VARCHAR2(100) := 'CREATE_PO_LIB_SQL.INSERT_ORD_INV_MGMT';

BEGIN
   /* if we get a rowid do an insert select */
   if I_ord_inv_mgmt_rowid is NOT NULL then

      insert into ord_inv_mgmt(order_no,
                               scale_cnstr_ind,
                               scale_cnstr_lvl,
                               scale_cnstr_obj,
                               scale_cnstr_type1,
                               scale_cnstr_uom1,
                               scale_cnstr_curr1,
                               scale_cnstr_min_val1,
                               scale_cnstr_max_val1,
                               scale_cnstr_min_tol1,
                               scale_cnstr_max_tol1,
                               scale_cnstr_type2,
                               scale_cnstr_uom2,
                               scale_cnstr_curr2,
                               scale_cnstr_min_val2,
                               scale_cnstr_max_val2,
                               scale_cnstr_min_tol2,
                               scale_cnstr_max_tol2,
                               scale_cnstr_chg_ind,
                               max_scaling_iterations,
                               min_cnstr_lvl,
                               min_cnstr_conj,
                               min_cnstr_type1,
                               min_cnstr_uom1,
                               min_cnstr_curr1,
                               min_cnstr_val1,
                               min_cnstr_type2,
                               min_cnstr_uom2,
                               min_cnstr_curr2,
                               min_cnstr_val2,
                               truck_split_ind,
                               truck_split_method,
                               truck_cnstr_type1,
                               truck_cnstr_uom1,
                               truck_cnstr_val1,
                               truck_cnstr_tol1,
                               truck_cnstr_type2,
                               truck_cnstr_uom2,
                               truck_cnstr_val2,
                               truck_cnstr_tol2,
                               ltl_approval_ind,
                               ltl_ind,
                               due_ord_process_ind,
                               due_ord_ind,
                               due_ord_lvl,
                               due_ord_serv_basis,
                               mult_vehicle_ind,
                               no_vehicles,
                               single_loc_ind,
                               ord_purge_ind,
                               pool_supplier,
                               file_id,
                               scale_issues,
                               truck_split_issues,
                               ord_approve_ind)
                        select I_order_no,
                               scale_cnstr_ind,
                               scale_cnstr_lvl,
                               scale_cnstr_obj,
                               scale_cnstr_type1,
                               scale_cnstr_uom1,
                               scale_cnstr_curr1,
                               scale_cnstr_min_val1,
                               scale_cnstr_max_val1,
                               scale_cnstr_min_tol1,
                               scale_cnstr_max_tol1,
                               scale_cnstr_type2,
                               scale_cnstr_uom2,
                               scale_cnstr_curr2,
                               scale_cnstr_min_val2,
                               scale_cnstr_max_val2,
                               scale_cnstr_min_tol2,
                               scale_cnstr_max_tol2,
                               'N',                       /*scale_cnstr_chg_ind*/
                               max_scaling_iterations,
                               min_cnstr_lvl,
                               min_cnstr_conj,
                               min_cnstr_type1,
                               min_cnstr_uom1,
                               min_cnstr_curr1,
                               min_cnstr_val1,
                               min_cnstr_type2,
                               min_cnstr_uom2,
                               min_cnstr_curr2,
                               min_cnstr_val2,
                               truck_split_ind,
                               truck_split_method,
                               truck_cnstr_type1,
                               truck_cnstr_uom1,
                               truck_cnstr_val1,
                               truck_cnstr_tol1,
                               truck_cnstr_type2,
                               truck_cnstr_uom2,
                               truck_cnstr_val2,
                               truck_cnstr_tol2,
                               ltl_approval_ind,
                               DECODE(I_ltl_flag, 1, 'Y', 2, 'N', NULL),
                               'N',                       /*due_ord_process_ind,*/
                               'Y',                       /*due_ord_ind,*/
                               NULL,                      /*due_ord_lvl,*/
                               NULL,                      /*due_ord_serv_basis,*/
                               mult_vehicle_ind,
                               NULL,
                               single_loc_ind,
                               ord_purge_ind,
                               pool_supplier,
                               file_id,
                               NULL,                      /*scale_issues*/
                               NULL,                      /*truck_split_issues*/
                               DECODE(I_order_status,'A','Y','N')
                          from ord_inv_mgmt
                         where rowid = CHARTOROWID(I_ord_inv_mgmt_rowid);

      /* since we are splitting when we have a ord_inv_mgmt record to
      work from, we want to default the ordlc records to the new
      orders from the source order */
      insert into ordlc (order_no,
                         lc_ref_id,
                         lc_group_id,
                         applicant,
                         beneficiary,
                         merch_desc,
                         transshipment_ind,
                         partial_shipment_ind,
                         lc_ind)
                  select I_order_no,
                         NULL,
                         olc.lc_group_id,
                         olc.applicant,
                         olc.beneficiary,
                         olc.merch_desc,
                         olc.transshipment_ind,
                         olc.partial_shipment_ind,
                         'N'
                    from ordlc olc
                   where olc.order_no = I_src_order;

   elsif I_sup_inv_mgmt_rowid is NOT NULL then

         insert into ord_inv_mgmt(order_no,
                                  scale_cnstr_ind,
                                  scale_cnstr_lvl,
                                  scale_cnstr_obj,
                                  scale_cnstr_type1,
                                  scale_cnstr_uom1,
                                  scale_cnstr_curr1,
                                  scale_cnstr_min_val1,
                                  scale_cnstr_max_val1,
                                  scale_cnstr_min_tol1,
                                  scale_cnstr_max_tol1,
                                  scale_cnstr_type2,
                                  scale_cnstr_uom2,
                                  scale_cnstr_curr2,
                                  scale_cnstr_min_val2,
                                  scale_cnstr_max_val2,
                                  scale_cnstr_min_tol2,
                                  scale_cnstr_max_tol2,
                                  scale_cnstr_chg_ind,
                                  max_scaling_iterations,
                                  min_cnstr_lvl,
                                  min_cnstr_conj,
                                  min_cnstr_type1,
                                  min_cnstr_uom1,
                                  min_cnstr_curr1,
                                  min_cnstr_val1,
                                  min_cnstr_type2,
                                  min_cnstr_uom2,
                                  min_cnstr_curr2,
                                  min_cnstr_val2,
                                  truck_split_ind,
                                  truck_split_method,
                                  truck_cnstr_type1,
                                  truck_cnstr_uom1,
                                  truck_cnstr_val1,
                                  truck_cnstr_tol1,
                                  truck_cnstr_type2,
                                  truck_cnstr_uom2,
                                  truck_cnstr_val2,
                                  truck_cnstr_tol2,
                                  ltl_approval_ind,
                                  ltl_ind,
                                  due_ord_process_ind,
                                  due_ord_ind,
                                  due_ord_lvl,
                                  due_ord_serv_basis,
                                  mult_vehicle_ind,
                                  no_vehicles,
                                  single_loc_ind,
                                  ord_purge_ind,
                                  pool_supplier,
                                  file_id,
                                  scale_issues,
                                  truck_split_issues,
                                  ord_approve_ind)
                           select I_order_no,
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_ind, 'N'),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_lvl, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_obj, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_type1, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_uom1, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_curr1, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_min_val1, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_max_val1, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_min_tol1, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_max_tol1, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_type2, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_uom2, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_curr2, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_min_val2, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_max_val2, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_min_tol2, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', scale_cnstr_max_tol2, NULL),
                                  'N',              /*scale_cnstr_chg_ind*/
                                  ADD_LINE_ITEM_SQL.LP_max_scale_iterations,
                                  min_cnstr_lvl,
                                  min_cnstr_conj,
                                  min_cnstr_type1,
                                  min_cnstr_uom1,
                                  min_cnstr_curr1,
                                  min_cnstr_val1,
                                  min_cnstr_type2,
                                  min_cnstr_uom2,
                                  min_cnstr_curr2,
                                  min_cnstr_val2,
                                  truck_split_ind,
                                  truck_split_method,
                                  truck_cnstr_type1,
                                  truck_cnstr_uom1,
                                  truck_cnstr_val1,
                                  truck_cnstr_tol1,
                                  truck_cnstr_type2,
                                  truck_cnstr_uom2,
                                  truck_cnstr_val2,
                                  truck_cnstr_tol2,
                                  ltl_approval_ind,
                                  DECODE(I_ltl_flag, 1, 'Y', 2, 'N', NULL),
                                  DECODE(I_contract_ind,
                                         '-1', due_ord_process_ind, 'N'),
                                  DECODE(I_contract_ind,
                                         '-1', I_total_order_due_indicator, 'Y'),
                                  DECODE(I_contract_ind,
                                         '-1', due_ord_lvl, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', due_ord_serv_basis, NULL),
                                  DECODE(I_contract_ind,
                                         '-1', mult_vehicle_ind, 'N'),
                                  NULL,               /*no_vehicles*/
                                  DECODE(I_contract_ind,
                                         '-1', single_loc_ind, 'N'),
                                  ord_purge_ind,
                                  I_pool_supp,
                                  I_file_id,
                                  NULL,               /*scale_issues*/
                                  NULL,               /*truck_split_issues*/
                                  DECODE(I_order_status,'A','Y','N')
                             from sup_inv_mgmt
                            where rowid = CHARTOROWID(I_sup_inv_mgmt_rowid);

   else
       /* no row was found on SUP_INV_MGMT, use default values */
        insert into ord_inv_mgmt(order_no,
                                 scale_cnstr_ind,
                                 scale_cnstr_lvl,
                                 scale_cnstr_obj,
                                 scale_cnstr_type1,
                                 scale_cnstr_uom1,
                                 scale_cnstr_curr1,
                                 scale_cnstr_min_val1,
                                 scale_cnstr_max_val1,
                                 scale_cnstr_min_tol1,
                                 scale_cnstr_max_tol1,
                                 scale_cnstr_type2,
                                 scale_cnstr_uom2,
                                 scale_cnstr_curr2,
                                 scale_cnstr_min_val2,
                                 scale_cnstr_max_val2,
                                 scale_cnstr_min_tol2,
                                 scale_cnstr_max_tol2,
                                 scale_cnstr_chg_ind,
                                 max_scaling_iterations,
                                 min_cnstr_lvl,
                                 min_cnstr_conj,
                                 min_cnstr_type1,
                                 min_cnstr_uom1,
                                 min_cnstr_curr1,
                                 min_cnstr_val1,
                                 min_cnstr_type2,
                                 min_cnstr_uom2,
                                 min_cnstr_curr2,
                                 min_cnstr_val2,
                                 truck_split_ind,
                                 truck_split_method,
                                 truck_cnstr_type1,
                                 truck_cnstr_uom1,
                                 truck_cnstr_val1,
                                 truck_cnstr_tol1,
                                 truck_cnstr_type2,
                                 truck_cnstr_uom2,
                                 truck_cnstr_val2,
                                 truck_cnstr_tol2,
                                 ltl_approval_ind,
                                 ltl_ind,
                                 due_ord_process_ind,
                                 due_ord_ind,
                                 due_ord_lvl,
                                 due_ord_serv_basis,
                                 mult_vehicle_ind,
                                 no_vehicles,
                                 single_loc_ind,
                                 ord_purge_ind,
                                 pool_supplier,
                                 file_id,
                                 scale_issues,
                                 truck_split_issues,
                                 ord_approve_ind)
                          values(I_order_no,
                                 'N',                      /*scale_cnstr_ind,*/
                                 NULL,                     /*scale_cnstr_lvl,*/
                                 NULL,                     /*scale_cnstr_obj,*/
                                 NULL,                     /*scale_cnstr_type1,*/
                                 NULL,                     /*scale_cnstr_uom1,*/
                                 NULL,                     /*scale_cnstr_curr1,*/
                                 NULL,                     /*scale_cnstr_min_val1,*/
                                 NULL,                     /*scale_cnstr_max_val1,*/
                                 NULL,                     /*scale_cnstr_min_tol1,*/
                                 NULL,                     /*scale_cnstr_max_tol1,*/
                                 NULL,                     /*scale_cnstr_type2,*/
                                 NULL,                     /*scale_cnstr_uom2,*/
                                 NULL,                     /*scale_cnstr_curr2,*/
                                 NULL,                     /*scale_cnstr_min_val2,*/
                                 NULL,                     /*scale_cnstr_max_val2,*/
                                 NULL,                     /*scale_cnstr_min_tol2,*/
                                 NULL,                     /*scale_cnstr_max_tol2,*/
                                 'N',                      /*scale_cnstr_chg_ind,*/
                                 ADD_LINE_ITEM_SQL.LP_max_scale_iterations,
                                 NULL,                     /*min_cnstr_lvl,*/
                                 NULL,                     /*min_cnstr_conj,*/
                                 NULL,                     /*min_cnstr_type1,*/
                                 NULL,                     /*min_cnstr_uom1,*/
                                 NULL,                     /*min_cnstr_curr1,*/
                                 NULL,                     /*min_cnstr_val1,*/
                                 NULL,                     /*min_cnstr_type2,*/
                                 NULL,                     /*min_cnstr_uom2,*/
                                 NULL,                     /*min_cnstr_curr2,*/
                                 NULL,                     /*min_cnstr_val2,*/
                                 NULL,                     /* truck_split_ind, */
                                 NULL,                     /* truck_split_method, */
                                 NULL,                     /* truck_cnstr_type1, */
                                 NULL,                     /* truck_cnstr_uom1, */
                                 NULL,                     /* truck_cnstr_val1, */
                                 NULL,                     /* truck_cnstr_tol1, */
                                 NULL,                     /* truck_cnstr_type2, */
                                 NULL,                     /* truck_cnstr_uom2, */
                                 NULL,                     /* truck_cnstr_val2, */
                                 NULL,                     /* truck_cnstr_tol2, */
                                 'Y',                      /*ltl_approval_ind, */
                                 DECODE(I_ltl_flag, 1, 'Y', 2, 'N', NULL),
                                 'N',                      /*due_ord_process_ind,*/
                                 'Y',                      /*due_ord_ind,*/
                                 NULL,                     /*due_ord_lvl,*/
                                 NULL,                     /*due_ord_serv_basis,*/
                                 'N',                      /*mult_vehicle_ind,*/
                                 NULL,                     /*no_vehicles,*/
                                 'N',                      /*single_loc_ind,*/
                                 'N',                      /*ord_purge_ind,*/
                                 I_pool_supp,
                                 I_file_id,
                                 NULL,                     /*scale_issues*/
                                 NULL,                     /*truck_split_issues*/
                                 DECODE(I_order_status,'A','Y','N'));

   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END INSERT_ORD_INV_MGMT;
---------------------------------------------------------------------------------------------------------
-- Function: BUILD_DATES
-- Purpose : Set up not before and not after dates and the eow date for
--           the not after date, used for inserts into ordhead and ordsku
---------------------------------------------------------------------------------------------------------
FUNCTION BUILD_DATES(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_ordhead_rec          IN      ADD_LINE_ITEM_SQL.ORDHEAD_REC,
                     O_not_before_date      OUT     ORDHEAD.NOT_BEFORE_DATE%TYPE,
                     O_not_after_date       OUT     ORDHEAD.NOT_AFTER_DATE%TYPE,
                     O_earliest_ship_date   OUT     ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
                     O_latest_ship_date     OUT     ORDHEAD.LATEST_SHIP_DATE%TYPE,
                     O_pickup_date          OUT     ORDHEAD.PICKUP_DATE%TYPE,
                     O_eow_date             OUT     ORDHEAD.OTB_EOW_DATE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'CREATE_PO_LIB_SQL.BUILD_DATES';

   L_order_no  ORDHEAD.ORDER_NO%TYPE;

   cursor C_ORD_DATES is
      select oh.not_before_date,
             oh.not_after_date,
             oh.earliest_ship_date,
             oh.latest_ship_date,
             oh.pickup_date,
             oh.otb_eow_date
        from ordhead oh
       where oh.order_no = L_order_no;

   cursor C_DATES is
      select /* not before date */
             DECODE(ADD_LINE_ITEM_SQL.LP_batch_ind, 'Y',
                    ADD_LINE_ITEM_SQL.LP_vdate +
                        (1 + I_ordhead_rec.min_supp_lead_time + I_ordhead_rec.min_pickup_lead_time),
                    ADD_LINE_ITEM_SQL.LP_vdate +
                            (I_ordhead_rec.min_supp_lead_time + I_ordhead_rec.min_pickup_lead_time)),
             /* not after date */
             DECODE(ADD_LINE_ITEM_SQL.LP_batch_ind, 'Y',
                    ADD_LINE_ITEM_SQL.LP_vdate +
                        (1 + I_ordhead_rec.max_supp_lead_time + I_ordhead_rec.max_pickup_lead_time +
                             ADD_LINE_ITEM_SQL.LP_repl_order_days),
                    ADD_LINE_ITEM_SQL.LP_vdate +
                            (I_ordhead_rec.max_supp_lead_time + I_ordhead_rec.max_pickup_lead_time)),
             /* earliest ship date */
             DECODE(ADD_LINE_ITEM_SQL.LP_batch_ind, 'Y',
                    ADD_LINE_ITEM_SQL.LP_vdate + (1 + I_ordhead_rec.min_supp_lead_time),
                    ADD_LINE_ITEM_SQL.LP_vdate + (I_ordhead_rec.min_supp_lead_time)),
             /* latest ship date */
             DECODE(ADD_LINE_ITEM_SQL.LP_batch_ind, 'Y',
                    ADD_LINE_ITEM_SQL.LP_vdate +
                        (1 + I_ordhead_rec.min_supp_lead_time + ADD_LINE_ITEM_SQL.LP_latest_ship_days),
                    ADD_LINE_ITEM_SQL.LP_vdate +
                            (I_ordhead_rec.min_supp_lead_time + ADD_LINE_ITEM_SQL.LP_latest_ship_days)),
             /* pickup date */
             DECODE(ADD_LINE_ITEM_SQL.LP_batch_ind, 'Y',
                    ADD_LINE_ITEM_SQL.LP_vdate + (1 + I_ordhead_rec.min_supp_lead_time),
                    ADD_LINE_ITEM_SQL.LP_vdate + (I_ordhead_rec.min_supp_lead_time))
        from dual;

BEGIN
   /* if order is being split from an existing order, use
      the dates from the existing order */
   if I_ordhead_rec.split_ref_ord_no is NOT NULL then

      L_order_no := I_ordhead_rec.split_ref_ord_no;

      SQL_LIB.SET_MARK('OPEN','C_ORD_DATES','ORDHEAD','order_no:' || L_order_no);
       open C_ORD_DATES;
      SQL_LIB.SET_MARK('FETCH','C_ORD_DATES','ORDHEAD','order_no:' || L_order_no);
      fetch C_ORD_DATES into O_not_before_date,
                             O_not_after_date,
                             O_earliest_ship_date,
                             O_latest_ship_date,
                             O_pickup_date,
                             O_eow_date;
      SQL_LIB.SET_MARK('CLOSE','C_ORD_DATES','ORDHEAD','order_no:' || L_order_no);
      close C_ORD_DATES;

   else
      SQL_LIB.SET_MARK('OPEN','C_DATES', 'DUAL', NULL);
      open C_DATES;
      SQL_LIB.SET_MARK('FETCH','C_DATES', 'DUAL', NULL);
      fetch C_DATES into O_not_before_date,
                         O_not_after_date,
                         O_earliest_ship_date,
                         O_latest_ship_date,
                         O_pickup_date;
      SQL_LIB.SET_MARK('CLOSE','C_DATES', 'DUAL', NULL);
      close C_DATES;

      if not DATES_SQL.GET_EOW_DATE(O_error_message,
                                    O_eow_date,
                                    O_not_after_date) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END BUILD_DATES;
---------------------------------------------------------------------------------------------------------
-- Function: ORDHEAD_DEFAULTS
-- Purpose : If the order being created is a result of a split, get values
--           from the source order to use in the creation of the new order.
---------------------------------------------------------------------------------------------------------
FUNCTION ORDHEAD_DEFAULTS(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_ordhead_rec           IN      ADD_LINE_ITEM_SQL.ORDHEAD_REC,
                          O_order_type               OUT  ORDHEAD.ORDER_TYPE%TYPE,
                          O_orig_ind                 OUT  ORDHEAD.ORIG_IND%TYPE,
                          O_promotion                OUT  ORDHEAD.PROMOTION%TYPE,
                          O_ship_pay_method          OUT  ORDHEAD.SHIP_PAY_METHOD%TYPE,
                          O_fob_trans_res            OUT  ORDHEAD.FOB_TRANS_RES%TYPE,
                          O_fob_trans_res_desc       OUT  ORDHEAD.FOB_TRANS_RES_DESC%TYPE,
                          O_fob_title_pass           OUT  ORDHEAD.FOB_TITLE_PASS%TYPE,
                          O_fob_title_pass_desc      OUT  ORDHEAD.FOB_TITLE_PASS_DESC%TYPE,
                          O_include_on_order_ind     OUT  ORDHEAD.INCLUDE_ON_ORDER_IND%TYPE,
                          O_factory                  OUT  ORDHEAD.FACTORY%TYPE,
                          O_freight_contract_no      OUT  ORDHEAD.FREIGHT_CONTRACT_NO%TYPE,
                          O_po_type                  OUT  ORDHEAD.PO_TYPE%TYPE,
                          O_pickup_no                OUT  ORDHEAD.PICKUP_NO%TYPE,
                          O_comment_desc             OUT  ORDHEAD.COMMENT_DESC%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(100) := 'CREATE_PO_LIB_SQL.ORDHEAD_DEFAULTS';

   L_order_no   ORDHEAD.ORDER_NO%TYPE;

   cursor C_ORD_DEF is
      select oh.order_type,
             oh.orig_ind,
             oh.promotion,
             oh.ship_pay_method,
             oh.fob_trans_res,
             oh.fob_trans_res_desc,
             oh.fob_title_pass,
             oh.fob_title_pass_desc,
             oh.include_on_order_ind,
             oh.factory,
             oh.freight_contract_no,
             oh.po_type,
             oh.pickup_no,
             oh.comment_desc
        from ordhead oh
       where oh.order_no = L_order_no;

BEGIN

   L_order_no := I_ordhead_rec.split_ref_ord_no;

   if L_order_no is NOT NULL then

      SQL_LIB.SET_MARK('OPEN','C_ORD_DEF','ORDHEAD','order_no:' || L_order_no);
      open C_ORD_DEF;
      SQL_LIB.SET_MARK('FETCH','C_ORD_DEF','ORDHEAD','order_no:' || L_order_no);
      fetch C_ORD_DEF into O_order_type,
                           O_orig_ind,
                           O_promotion,
                           O_ship_pay_method,
                           O_fob_trans_res,
                           O_fob_trans_res_desc,
                           O_fob_title_pass,
                           O_fob_title_pass_desc,
                           O_include_on_order_ind,
                           O_factory,
                           O_freight_contract_no,
                           O_po_type,
                           O_pickup_no,
                           O_comment_desc;
      SQL_LIB.SET_MARK('CLOSE','C_ORD_DEF','ORDHEAD','order_no:' || L_order_no);
      close C_ORD_DEF;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END ORDHEAD_DEFAULTS;
---------------------------------------------------------------------------------------------------------
-- Function: HEADER_DEFAULTS
-- Purpose : ORDER_SETUP_SQL.DEFAULT_ORDHEAD_DOCS is called for each order created.
---------------------------------------------------------------------------------------------------------
FUNCTION HEADER_DEFAULTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(100) := 'CREATE_PO_LIB_SQL.HEADER_DEFAULTS';

BEGIN

   if not ORDER_SETUP_SQL.DEFAULT_ORDHEAD_DOCS(O_error_message,
                                               I_order_no) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END HEADER_DEFAULTS;
---------------------------------------------------------------------------------------------------------
-- Function: GET_EXCHANGE_RATE
-- Purpose : The package CURRENCY_SQL.GET_RATE is called to get exchange rate for the passed
--           in currency code.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_EXCHANGE_RATE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exchange_rate     OUT  ORDHEAD.EXCHANGE_RATE%TYPE,
                           I_currency_code  IN      ORDHEAD.CURRENCY_CODE%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(100) := 'CREATE_PO_LIB_SQL.GET_EXCHANGE_RATE';

BEGIN

   if not CURRENCY_SQL.GET_RATE(O_error_message,
                                O_exchange_rate,
                                I_currency_code,
                                'P',
                                NULL) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END GET_EXCHANGE_RATE;
---------------------------------------------------------------------------------------------------------
-- Function: UPDATE_CONTRACT
-- Purpose : For approved contract orders, update contract_header or contract detail
--           depending on the contract type.
---------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_CONTRACT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_ordhead_rec    IN      ADD_LINE_ITEM_SQL.ORDHEAD_REC)
RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'CREATE_PO_LIB_SQL.UPDATE_CONTRACT';

   cursor C_LOCK_CONTRACT_DETAIL is
      select 'x'
        from contract_detail
       where contract_no = I_ordhead_rec.contract
         for update nowait;

   cursor C_LOCK_CONTRACT_HEADER is
      select 'x'
        from contract_header
       where rowid = CHARTOROWID(I_ordhead_rec.contract_header_rowid)
         for update nowait;

BEGIN

   if I_ordhead_rec.contract = -1 then
      LP_contr_ind := 'N';
   else
      LP_contr_ind := 'Y';
   end if;

   /* if there is no contract, or the order is not approved, or
      the contract is not approved there is nothing to do here */
   if I_ordhead_rec.contract = -1 or
      I_ordhead_rec.order_status <> 'A' or
      I_ordhead_rec.contract_approval_ind <> 'Y' then
      return TRUE;
   end if;

   if I_ordhead_rec.contract_type = 'A' then

      update contract_header
         set last_ordered_date = ADD_LINE_ITEM_SQL.LP_vdate
       where rowid             = CHARTOROWID(I_ordhead_rec.contract_header_rowid);

      if ADD_LINE_ITEM_SQL.LP_locked_ind = 'Y' then
         SQL_LIB.SET_MARK('OPEN','C_LOCK_CONTRACT_DETAIL','CONTRACT_DETAIL','contract_no: '||I_ordhead_rec.contract);
         open C_LOCK_CONTRACT_DETAIL;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_CONTRACT_DETAIL','CONTRACT_DETAIL','contract_no: '||I_ordhead_rec.contract);
         close C_LOCK_CONTRACT_DETAIL;
      end if;

      update contract_detail
         set qty_ordered    = NVL(qty_ordered,0) + cur_repl_qty,
             cur_repl_qty   = 0
       where contract_no    = I_ordhead_rec.contract;
   else

      if ADD_LINE_ITEM_SQL.LP_locked_ind = 'Y' then
         SQL_LIB.SET_MARK('OPEN','C_LOCK_CONTRACT_HEADER','CONTRACT_HEADER','rowid: '||I_ordhead_rec.contract_header_rowid);
         open C_LOCK_CONTRACT_HEADER;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_CONTRACT_HEADER','CONTRACT_HEADER','rowid: '||I_ordhead_rec.contract_header_rowid);
         close C_LOCK_CONTRACT_HEADER;
      end if;

      update contract_header
         set outstand_cost  = NVL(outstand_cost,0) - cur_repl_cost,
             cur_repl_cost  = 0,
             last_ordered_date  = ADD_LINE_ITEM_SQL.LP_vdate
       where rowid  = CHARTOROWID(I_ordhead_rec.contract_header_rowid);
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END UPDATE_CONTRACT;
---------------------------------------------------------------------------------------------------------
-- Function: INSERT_ORDSKU
-- Purpose : Insert an ordsku record for the passed in ordsku record if needed.
--           Get the upc info for the item if an insert is going to take place.
--           Call the needed packages for a new ordsku record.
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ORDSKU(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_ordsku_rec       IN      ADD_LINE_ITEM_SQL.ORDSKU_REC,
                       I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                       I_pickup_loc       IN      ORDHEAD.PICKUP_LOC%TYPE,
                       I_src_order_no     IN      ORDHEAD.SPLIT_REF_ORDNO%TYPE)
RETURN BOOLEAN IS

   L_program                VARCHAR2(100) := 'CREATE_PO_LIB_SQL.INSERT_ORDSKU';

   L_earliest_ship_date     ORDHEAD.EARLIEST_SHIP_DATE%TYPE;
   L_latest_ship_date       ORDHEAD.LATEST_SHIP_DATE%TYPE;
   L_pickup_loc             ORDHEAD.PICKUP_LOC%TYPE;
   L_pickup_no              ORDHEAD.PICKUP_NO%TYPE;

   L_ref_item               ITEM_MASTER.ITEM%TYPE;

BEGIN

   if I_ordsku_rec.write_ind = 'N' then
      return TRUE;
   end if;

   if BUILD_SKU_DATES(O_error_message,
                      L_earliest_ship_date,
                      L_latest_ship_date,
                      L_pickup_loc,
                      L_pickup_no,
                      I_ordsku_rec,
                      I_src_order_no) = FALSE then
      return FALSE;
   end if;

   if GET_REF_ITEM(O_error_message,
                   I_ordsku_rec.item,
                   L_ref_item) = FALSE then
      return FALSE;
   end if;

   insert into ordsku(order_no,
                      item,
                      ref_item,
                      origin_country_id,
                      earliest_ship_date,
                      latest_ship_date,
                      supp_pack_size,
                      non_scale_ind,
                      pickup_loc,
                      pickup_no)
              values (I_order_no,
                      I_ordsku_rec.item,
                      L_ref_item,
                      I_ordsku_rec.ctry,
                      L_earliest_ship_date,
                      L_latest_ship_date,
                      I_ordsku_rec.pack_size,
                      I_ordsku_rec.non_scale_ind,
                      NVL(L_pickup_loc, I_pickup_loc),
                      L_pickup_no);

   if ITEM_DEFAULTS(O_error_message,
                    I_order_no,
                    I_ordsku_rec.item,
                    I_ordsku_rec.ctry) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END INSERT_ORDSKU;
---------------------------------------------------------------------------------------------------------
-- Function: BUILD_SKU_DATES
-- Purpose : Set up earliest and latest ship dates used for inserts into ordsku
---------------------------------------------------------------------------------------------------------
FUNCTION BUILD_SKU_DATES(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_earliest_ship_date    OUT  ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
                         O_latest_ship_date      OUT  ORDHEAD.LATEST_SHIP_DATE%TYPE,
                         O_pickup_loc            OUT  ORDHEAD.PICKUP_LOC%TYPE,
                         O_pickup_no             OUT  ORDHEAD.PICKUP_NO%TYPE,
                         I_ordsku_rec         IN      ADD_LINE_ITEM_SQL.ORDSKU_REC,
                         I_order_no           IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(100) := 'CREATE_PO_LIB_SQL.BUILD_SKU_DATES';

   cursor C_SKU_ORD_DATES is
      select os.earliest_ship_date,
             os.latest_ship_date,
             pickup_loc,
             pickup_no
        from ordsku os
       where os.order_no          = I_order_no
         and os.item              = I_ordsku_rec.item
         and os.origin_country_id = I_ordsku_rec.ctry;

   cursor C_SKU_DATES is
      select /* earliest ship date */
             DECODE(ADD_LINE_ITEM_SQL.LP_batch_ind, 'Y', ADD_LINE_ITEM_SQL.LP_vdate + (1 + I_ordsku_rec.min_supp_lead_time), 
                    ADD_LINE_ITEM_SQL.LP_vdate + (I_ordsku_rec.min_supp_lead_time)),
             /* latest ship date */
             DECODE(ADD_LINE_ITEM_SQL.LP_batch_ind, 'Y', ADD_LINE_ITEM_SQL.LP_vdate + (1 + I_ordsku_rec.min_supp_lead_time + ADD_LINE_ITEM_SQL.LP_latest_ship_days),
                    ADD_LINE_ITEM_SQL.LP_vdate + (I_ordsku_rec.min_supp_lead_time + ADD_LINE_ITEM_SQL.LP_latest_ship_days))
        from dual;

BEGIN
   /* if order is being split from an existing order, use
      the dates from the existing order */
   if I_order_no is NOT NULL then

      SQL_LIB.SET_MARK('OPEN','C_SKU_ORD_DATES','ORDSKU',
                       'order_no: '||I_order_no||' item: '||I_ordsku_rec.item||' origin_country_id: '||I_ordsku_rec.ctry);
      open C_SKU_ORD_DATES;
      SQL_LIB.SET_MARK('FETCH','C_SKU_ORD_DATES','ORDSKU',
                       'order_no: '||I_order_no||' item: '||I_ordsku_rec.item||' origin_country_id: '||I_ordsku_rec.ctry);
      fetch C_SKU_ORD_DATES into O_earliest_ship_date,
                                 O_latest_ship_date,
                                 O_pickup_loc,
                                 O_pickup_no;
      SQL_LIB.SET_MARK('CLOSE','C_SKU_ORD_DATES','ORDSKU',
                       'order_no: '||I_order_no||' item: '||I_ordsku_rec.item||' origin_country_id: '||I_ordsku_rec.ctry);
      close C_SKU_ORD_DATES;
   else
      SQL_LIB.SET_MARK('OPEN','C_SKU_DATES', NULL, NULL);
      open C_SKU_DATES;
      SQL_LIB.SET_MARK('FETCH','C_SKU_DATES', NULL, NULL);
      fetch C_SKU_DATES into O_earliest_ship_date,
                             O_latest_ship_date;
      SQL_LIB.SET_MARK('CLOSE','C_SKU_DATES', NULL, NULL);
      close C_SKU_DATES;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END BUILD_SKU_DATES;
---------------------------------------------------------------------------------------------------------
-- Function: GET_REF_ITEM
-- Purpose : A function for calling ITEM_ATTRIB_SQL.GET_PRIMARY_REF_ITEM which is used when 
--           inserting ordsku records.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_REF_ITEM(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_item           IN      ITEM_MASTER.ITEM%TYPE,
                      O_ref_item          OUT  ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(100) := 'CREATE_PO_LIB_SQL.GET_REF_ITEM';

   L_ref_item   ITEM_MASTER.ITEM%TYPE := NULL;
   L_exists     BOOLEAN;

BEGIN

   if not ITEM_ATTRIB_SQL.GET_PRIMARY_REF_ITEM(O_error_message,
                                               L_ref_item,
                                               L_exists,
                                               I_item) then
      return FALSE;
   else
      if L_exists = TRUE then
         O_ref_item := L_ref_item;
      end if;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END GET_REF_ITEM;
---------------------------------------------------------------------------------------------------------
-- Function: ADD_COST_COMP
-- Purpose : ORDER_EXPENSE_SQL.INSERT_COST_COMP is called for each ordsku record inserted 
--           when elc is on.  Needs to be called after the ordloc records are inserted 
--           for each item in order to work correctly.
---------------------------------------------------------------------------------------------------------
FUNCTION ADD_COST_COMP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_ordsku_rec     IN      ADD_LINE_ITEM_SQL.ORDSKU_REC,
                       I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'CREATE_PO_LIB_SQL.ADD_COST_COMP';

   L_item      ITEM_MASTER.ITEM%TYPE;

BEGIN

   L_item := I_ordsku_rec.item;

   if not ORDER_EXPENSE_SQL.INSERT_COST_COMP(O_error_message,
                                             I_order_no,
                                             L_item,
                                             NULL,
                                             NULL,
                                             NULL) then
      return FALSE;
   end if;

   if not ORDER_HTS_SQL.DEFAULT_CALC_HTS(O_error_message,
                                         I_order_no,
                                         L_item,
                                         NULL) then
      return FALSE;
   end if;

   if not ELC_CALC_SQL.CALC_COMP(O_error_message,
                                 'PE',
                                 L_item,
                                 NULL,
                                 NULL,
                                 NULL,
                                 I_order_no,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END ADD_COST_COMP;
---------------------------------------------------------------------------------------------------------
-- Function: ITEM_DEFAULTS
-- Purpose : ORDER_SETUP_SQL.DEFAULT_ORDSKU_DOCS is called for each ordsku record inserted on.
---------------------------------------------------------------------------------------------------------
FUNCTION ITEM_DEFAULTS(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no           IN      ORDSKU.ORDER_NO%TYPE,
                       I_item               IN      ORDSKU.ITEM%TYPE,
                       I_ctry               IN      ORDSKU.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'CREATE_PO_LIB_SQL.ITEM_DEFAULTS';

BEGIN

   if not ORDER_SETUP_SQL.DEFAULT_ORDSKU_DOCS(O_error_message,
                                              I_order_no,
                                              I_item,
                                              I_ctry,
                                              ADD_LINE_ITEM_SQL.LP_elc_ind,
                                              'Y') then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END ITEM_DEFAULTS;
---------------------------------------------------------------------------------------------------------
-- Function: INSERT_ALLOC_HEADER
-- Purpose : Inserts a record into alloc_header, after generating a new allocation number. 
--           This is called for the WH total portion of a xdock order.
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ALLOC_HEADER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_ordloc_rec     IN      ADD_LINE_ITEM_SQL.ORDLOC_REC,
                             I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                             O_alloc_no          OUT  ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(100) := 'CREATE_PO_LIB_SQL.INSERT_ALLOC_HEADER';

   L_release_date   ALLOC_HEADER.RELEASE_DATE%TYPE;
   L_order_type     ALLOC_HEADER.ORDER_TYPE%TYPE := 'PREDIST';

   cursor C_ALLOC_DATES is
      select NVL(oh.not_before_date, p.vdate)
        from ordhead oh,
             period p
       where oh.order_no = I_order_no;

BEGIN

   open C_ALLOC_DATES;
   fetch C_ALLOC_DATES into L_release_date;
   close C_ALLOC_DATES;

   if I_ordloc_rec.write_ind = 'N' then
      return TRUE;
   end if;

   if GET_ALLOC_NO(O_error_message,
                   O_alloc_no) = FALSE then
      return FALSE;
   end if;

   insert into alloc_header(alloc_no,
                            order_no,
                            wh,
                            item,
                            status,
                            alloc_desc,
                            po_type,
                            alloc_method,
                            release_date,
                            order_type,
                            origin_ind)
                    values (O_alloc_no,
                            I_order_no,
                            I_ordloc_rec.loc,
                            I_ordloc_rec.item,
                            'W',
                            'createordlib'||ADD_LINE_ITEM_SQL.LP_vdate,
                            NULL,
                            ADD_LINE_ITEM_SQL.LP_alloc_method,
                            L_release_date,
                            L_order_type,
                            'RMS');

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END INSERT_ALLOC_HEADER;
---------------------------------------------------------------------------------------------------------
-- Function: GET_ALLOC_NO
-- Purpose : Generate a new allocation number of the database sequence for each new
--           xdock order.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_ALLOC_NO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_alloc_no          OUT  ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(100) := 'CREATE_PO_LIB_SQL.GET_ALLOC_NO';

   L_alloc_no        ALLOC_HEADER.ALLOC_NO%TYPE := NULL;
   L_return_code     VARCHAR(5)   := NULL;

BEGIN

   NEXT_ALLOC_NO(L_alloc_no,
                 L_return_code,
                 O_error_message);

   if L_return_code = 'TRUE' then
      O_alloc_no   := L_alloc_no;
   else
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END GET_ALLOC_NO;
---------------------------------------------------------------------------------------------------------
-- Function: INSERT_ORDLOC
-- Purpose : Insert a record into ordloc, update repl_results with the order number.
--           Unit retail is retrieved before the ordloc record is populated.
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ORDLOC(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_ordloc_rec         IN      ADD_LINE_ITEM_SQL.ORDLOC_REC,
                       I_order_no           IN      ORDHEAD.ORDER_NO%TYPE,
                       I_src_order_no       IN      ORDHEAD.SPLIT_REF_ORDNO%TYPE,
                       I_supplier           IN      ORDHEAD.SUPPLIER%TYPE,
                       I_cntry              IN      ORDSKU.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(100) := 'CREATE_PO_LIB_SQL.INSERT_ORDLOC';

   L_est_instock_date   DATE;
   L_unit_retail        ITEM_LOC.UNIT_RETAIL%TYPE;
   L_unit_cost          ORDLOC.UNIT_COST%TYPE;
   L_cost_source        ORDLOC.COST_SOURCE%TYPE;
   L_location           ORDLOC.LOCATION%TYPE;
   L_store_type         STORE.STORE_TYPE%TYPE := NULL;
   L_cost_loc           ITEM_LOC.COSTING_LOC%TYPE;

   L_wh_exists          BOOLEAN   := FALSE;
   L_loc_exists         BOOLEAN   := FALSE;

   cursor C_GET_TYPE is
      select store_type
        from store
       where store = L_location;

   cursor C_GET_COSTING_LOC is
      select costing_loc
        from item_loc il
       where il.item = I_ordloc_rec.item
         and il.loc  = I_ordloc_rec.loc;

BEGIN

   if GET_EST_INSTOCK_DATE(O_error_message,
                           I_order_no,
                           I_ordloc_rec.item,
                           I_ordloc_rec.loc,
                           I_supplier,
                           L_est_instock_date) = FALSE then
      return FALSE;
   end if;

   if UPDATE_SOURCE_TABLE(O_error_message,
                          I_ordloc_rec.rr_rowid,
                          I_ordloc_rec.ir_rowid,
                          I_ordloc_rec.bw_rowid,
                          I_order_no,
                          NULL)= FALSE then
      return FALSE;
   end if;

   if I_ordloc_rec.write_ind = 'N' then
      return TRUE;
   end if;

   if GET_UNIT_RETAIL(O_error_message,
                      I_ordloc_rec,
                      L_unit_retail) = FALSE then
      return FALSE;
   end if;

   L_unit_cost    := I_ordloc_rec.unit_cost;
   L_cost_source  := I_ordloc_rec.cost_source;

   if I_ordloc_rec.bw_rowid is NULL then
      if ((I_src_order_no is NULL or ADD_LINE_ITEM_SQL.LP_batch_ind = 'Y') and (LP_contr_ind = 'N' or I_ordloc_rec.cost_source <> 'CONT')) then
         if I_ordloc_rec.unit_cost is NULL then
            if GET_UNIT_COST(O_error_message,
                             L_unit_cost,
                             I_ordloc_rec.item,
                             I_supplier,
                             I_cntry,
                             I_ordloc_rec.loc) = FALSE then
               return FALSE;
               L_cost_source := 'NORM';
            end if;
	    end if;
      end if;
   end if;

   if I_ordloc_rec.loc_type = 'S' then

      L_location := I_ordloc_rec.loc;

      SQL_LIB.SET_MARK('OPEN','C_GET_TYPE','STORE','store: '||L_location);
      open C_GET_TYPE;
      SQL_LIB.SET_MARK('FETCH','C_GET_TYPE','STORE','store: '||L_location);
      fetch C_GET_TYPE into L_store_type;
      SQL_LIB.SET_MARK('CLOSE','C_GET_TYPE','STORE','store: '||L_location);
      close C_GET_TYPE;

      if L_store_type = 'F' then

         SQL_LIB.SET_MARK('OPEN','C_GET_COSTING_LOC','ITEM_LOC','item: '||I_ordloc_rec.item||'loc: '||I_ordloc_rec.loc);
         open C_GET_COSTING_LOC;
         SQL_LIB.SET_MARK('FETCH','C_GET_COSTING_LOC','ITEM_LOC','item: '||I_ordloc_rec.item||'loc: '||I_ordloc_rec.loc);
         fetch C_GET_COSTING_LOC into L_cost_loc;
         SQL_LIB.SET_MARK('CLOSE','C_GET_COSTING_LOC','ITEM_LOC','item: '||I_ordloc_rec.item||'loc: '||I_ordloc_rec.loc);
         close C_GET_COSTING_LOC;

         if GET_UNIT_COST(O_error_message,
                          L_unit_cost,
                          I_ordloc_rec.item,
                          I_supplier,
                          I_cntry,
                          L_cost_loc) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   insert into ordloc(order_no,
                      item,
                      location,
                      loc_type,
                      unit_retail,
                      qty_ordered,
                      qty_prescaled,
                      qty_received,
                      last_received,
                      last_rounded_qty,
                      last_grp_rounded_qty,
                      qty_cancelled,
                      cancel_code,
                      cancel_date,
                      cancel_id,
                      original_repl_qty,
                      unit_cost,
                      unit_cost_init,
                      cost_source,
                      non_scale_ind,
                      tsf_po_link_no,
                      estimated_instock_date)
               values(I_order_no,                                 /*order_no*/
                      I_ordloc_rec.item,                          /*item*/
                      I_ordloc_rec.loc,                           /*location*/
                      I_ordloc_rec.loc_type,                      /*loc_type*/
                      L_unit_retail,                              /*unit_retail*/
                      I_ordloc_rec.qty,                           /*qty_ordered*/
                      I_ordloc_rec.qty,                           /*qty_prescaled*/
                      NULL,                                       /*qty_received*/
                      NULL,                                       /*last_received*/
                      DECODE(I_ordloc_rec.last_rounded_qty, 0,
                             NULL,
                             I_ordloc_rec.last_rounded_qty),
                      DECODE(I_ordloc_rec.last_grp_rounded_qty, 0,
                             NULL,
                             I_ordloc_rec.last_grp_rounded_qty),
                      NULL,                                       /*qty_canceled*/
                      NULL,                                       /*cancel_code*/
                      NULL,                                       /*canced_date*/
                      NULL,                                       /*cancel_id*/
                      I_ordloc_rec.qty,                           /*original_repl_qty*/
                      L_unit_cost,                                /*unit_cost*/
                      DECODE(I_src_order_no,                      /*unit_cost_init*/
                             NULL, I_ordloc_rec.unit_cost_init,
                             NULL),
                      L_cost_source,                              /*cost_source*/
                      I_ordloc_rec.non_scale_ind,                 /*non_scale_ind*/
                      I_ordloc_rec.tsf_po_link,                   /*tsf_po_link*/
                      L_est_instock_date                          /*estimated in stock date*/
                      );

   if ORDER_ATTRIB_SQL.MULTIPLE_LOC_TYPE_EXISTS(O_error_message,
                                                L_wh_exists,
                                                L_loc_exists,
                                                I_order_no) = FALSE then
      return FALSE;
   else

      if L_wh_exists = TRUE then
         update ordhead
            set routing_loc_id = NULL
          where order_no = I_order_no;
      end if;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END INSERT_ORDLOC;
---------------------------------------------------------------------------------------------------------
-- Function: GET_EST_INSTOCK_DATE
-- Purpose : A function for calling ORDER_INSTOCK_DATE_SQL.GET_INSTOCK_DT
--           which is used when inserting ordloc records.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_EST_INSTOCK_DATE(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                              I_item              IN      ORDHEAD.ITEM%TYPE,
                              I_location          IN      ORDHEAD.LOCATION%TYPE,
                              I_supplier          IN      ORDHEAD.SUPPLIER%TYPE,
                              O_est_instock_date     OUT  ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(100) := 'CREATE_PO_LIB_SQL.GET_EST_INSTOCK_DATE';

BEGIN

   if not ORDER_INSTOCK_DATE_SQL.GET_INSTOCK_DT(O_error_message,
                                                O_est_instock_date,
                                                I_order_no,
                                                I_item,
                                                I_location,
                                                I_supplier) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END GET_EST_INSTOCK_DATE;
---------------------------------------------------------------------------------------------------------
-- Function: GET_UNIT_RETAIL
-- Purpose : The unit retail is retrieved for a passed in item location, this
--           is called for each ordloc record that is inserted.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_UNIT_RETAIL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_ordloc_rec     IN      ADD_LINE_ITEM_SQL.ORDLOC_REC,
                         O_unit_retail       OUT  ITEM_LOC.UNIT_RETAIL%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(100) := 'CREATE_PO_LIB_SQL.GET_UNIT_RETAIL';

   L_location      ORDLOC.LOCATION%TYPE  := NULL;
   L_loc_type      ORDLOC.LOC_TYPE%TYPE  := NULL;
   L_store_type    STORE.STORE_TYPE%TYPE := NULL;

   cursor C_GET_STORE_TYPE is
      select store_type
        from store
       where store = L_location;

   cursor C_GET_COST_LOC is
      select costing_loc,
             costing_loc_type
        from item_loc il
       where il.item = I_ordloc_rec.item
         and il.loc  = I_ordloc_rec.loc;

   cursor C_IL_RETAIL is
      select NVL(il.unit_retail,0)
        from item_loc il
       where il.item = I_ordloc_rec.item
         and il.loc  = L_location;

BEGIN

   L_location :=  I_ordloc_rec.loc;
   L_loc_type :=  I_ordloc_rec.loc_type;

   if L_loc_type = 'S' then

      SQL_LIB.SET_MARK('OPEN','C_GET_STORE_TYPE','STORE','store: '||L_location);
      open C_GET_STORE_TYPE;
      SQL_LIB.SET_MARK('FETCH','C_GET_STORE_TYPE','STORE','store: '||L_location);
      fetch C_GET_STORE_TYPE into L_store_type;
      SQL_LIB.SET_MARK('CLOSE','C_GET_STORE_TYPE','STORE','store: '||L_location);
      close C_GET_STORE_TYPE;

      if L_store_type = 'F' then

         SQL_LIB.SET_MARK('OPEN','C_GET_COST_LOC','ITEM_LOC','item: '||I_ordloc_rec.item||'loc: '||I_ordloc_rec.loc);
         open C_GET_COST_LOC;
         SQL_LIB.SET_MARK('FETCH','C_GET_COST_LOC','ITEM_LOC','item: '||I_ordloc_rec.item||'loc: '||I_ordloc_rec.loc);
         fetch C_GET_COST_LOC into L_location,
                                   L_loc_type;
         SQL_LIB.SET_MARK('CLOSE','C_GET_COST_LOC','ITEM_LOC','item: '||I_ordloc_rec.item||'loc: '||I_ordloc_rec.loc);
         close C_GET_COST_LOC;
      end if;
   end if;

   /* if item is a pack, process appropriately */
   if I_ordloc_rec.pack_ind = 'P' then
      if BUILD_PACK_RETAIL(O_error_message,
                           I_ordloc_rec.item,
                           L_location,
                           L_loc_type,
                           O_unit_retail) = FALSE then
         return FALSE;
      end if;
   else /* item is not a pack */
      SQL_LIB.SET_MARK('OPEN','C_IL_RETAIL','ITEM_LOC','item: '||I_ordloc_rec.item||'loc: '||L_location);
      open C_IL_RETAIL;
      SQL_LIB.SET_MARK('FETCH','C_IL_RETAIL','ITEM_LOC','item: '||I_ordloc_rec.item||'loc: '||L_location);
      fetch C_IL_RETAIL into O_unit_retail;
      SQL_LIB.SET_MARK('CLOSE','C_IL_RETAIL','ITEM_LOC','item: '||I_ordloc_rec.item||'loc: '||L_location);
      close C_IL_RETAIL;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END GET_UNIT_RETAIL;
---------------------------------------------------------------------------------------------------------
-- Function: BUILD_PACK_RETAIL
-- Purpose : Function gets the retail value of a pack at a given location, called by get_unit_retail
---------------------------------------------------------------------------------------------------------
FUNCTION BUILD_PACK_RETAIL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item           IN      ITEM_LOC.ITEM%TYPE,
                           I_loc            IN      ITEM_LOC.LOC%TYPE,
                           I_loc_type       IN      ITEM_LOC.LOC_TYPE%TYPE,
                           O_unit_retail       OUT  ITEM_LOC.UNIT_RETAIL%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(100) := 'CREATE_PO_LIB_SQL.BUILD_PACK_RETAIL';

BEGIN

   if PRICING_ATTRIB_SQL.BUILD_PACK_RETAIL(O_error_message,
                                           O_unit_retail,
                                           I_item,
                                           I_loc_type,
                                           I_loc) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END BUILD_PACK_RETAIL;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_UNIT_COST(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_unit_cost             OUT  ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                       I_item               IN      ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                       I_supplier           IN      ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                       I_cntry              IN      ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                       I_loc                IN      ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(100) := 'CREATE_PO_LIB_SQL.GET_UNIT_COST';

   cursor C_ISCL_COST is
      select iscl.unit_cost
        from item_supp_country_loc iscl,
             addr a
       where iscl.item              = I_item
         and iscl.supplier          = I_supplier
         and iscl.origin_country_id = I_cntry
         and iscl.loc               = I_loc
         and a.module in ('ST', 'WH', 'WFST')
         and (key_value_1           = I_loc or
              key_value_1           = (select physical_wh
                                         from wh
                                        where wh = I_loc))
         and addr_type              = '01'
         and seq_no                 = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_ISCL_COST','ITEM_SUPP_COUNTRY_LOC, ADDR',
                    'item: '||I_item||'supplier: '||I_supplier||'I_origin_country_id: '||I_cntry||'I_loc: '||I_loc);
   open C_ISCL_COST;
   SQL_LIB.SET_MARK('FETCH','C_ISCL_COST','ITEM_SUPP_COUNTRY_LOC, ADDR',
                    'item: '||I_item||'supplier: '||I_supplier||'I_origin_country_id: '||I_cntry||'I_loc: '||I_loc);
   fetch C_ISCL_COST into O_unit_cost;
   SQL_LIB.SET_MARK('CLOSE','C_ISCL_COST','ITEM_SUPP_COUNTRY_LOC, ADDR',
                    'item: '||I_item||'supplier: '||I_supplier||'I_origin_country_id: '||I_cntry||'I_loc: '||I_loc);
   close C_ISCL_COST;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END GET_UNIT_COST;
---------------------------------------------------------------------------------------------------------
-- Function: INSERT_ALLOC_DETAIL
-- Purpose : Inserts a record into alloc_detail, alloc_chrg update repl_results with
--           the order_no and the alloc_no, if a pack, makes sure the pack/loc
--           relationship exist. Alloc_detail records are only created if the
--           item/loc is really due
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ALLOC_DETAIL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_ordloc_rec     IN      ADD_LINE_ITEM_SQL.ORDLOC_REC,
                             I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                             I_alloc_no       IN      ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(100) := 'CREATE_PO_LIB_SQL.INSERT_ALLOC_DETAIL';

   L_alloc_no     ALLOC_HEADER.ALLOC_NO%TYPE := NULL;
   L_from_loc     ORDLOC.LOCATION%TYPE;
   L_to_loc       NUMBER(10);
   L_item         ORDLOC.ITEM%TYPE;
   L_return       BOOLEAN;

BEGIN

   /* if the alloc_detail record is not being written, do not
   associate it's repl_results record with the allocation */
   if I_ordloc_rec.write_ind = 'N' then
      L_alloc_no := NULL;
   else
      L_alloc_no := I_alloc_no;
   end if;

   if UPDATE_SOURCE_TABLE(O_error_message,
                          I_ordloc_rec.rr_rowid,
                          I_ordloc_rec.ir_rowid,
                          I_ordloc_rec.bw_rowid,
                          I_order_no,
                          L_alloc_no) = FALSE then
      return FALSE;
   end if;

   if I_ordloc_rec.write_ind = 'N' then
      return TRUE;
   end if;

   insert into alloc_detail (alloc_no,
                             to_loc,
                             to_loc_type,
                             qty_transferred,
                             qty_allocated,
                             qty_prescaled,
                             non_scale_ind,
                             qty_distro,
                             qty_selected,
                             qty_cancelled,
                             qty_received,
                             po_rcvd_qty)
                      values(L_alloc_no,
                             I_ordloc_rec.xdock_store,
                             'S',
                             0,
                             I_ordloc_rec.qty,
                             I_ordloc_rec.qty,
                             I_ordloc_rec.non_scale_ind,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL);

   L_from_loc := I_ordloc_rec.loc;
   L_to_loc   := I_ordloc_rec.xdock_store;
   L_item     := I_ordloc_rec.item;

   L_return := ALLOC_CHARGE_SQL.DEFAULT_CHRGS(O_error_message,
                                              L_alloc_no,
                                              L_from_loc,
                                              L_to_loc,
                                              'S',
                                              L_item);

   if L_return = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END INSERT_ALLOC_DETAIL;
---------------------------------------------------------------------------------------------------------
-- Function: UPDATE_SOURCE_TABLE
-- Purpose : For each item/loc that is processed, (not wh total parts of xdock), the order
--           and allocation numbers that associated with them are linked back to
--           repl_results.  This will allow the item/loc recalc process to find the
--           starting point for all item/locs on a given order.
--           If the item/loc is linked to a ib_results record, set the status to 'P'rocessed.
---------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_SOURCE_TABLE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_rr_rowid       IN      VARCHAR2,
                             I_ir_rowid       IN      VARCHAR2,
                             I_bw_rowid       IN      VARCHAR2,
                             I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                             I_alloc_no       IN      ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(100) := 'CREATE_PO_LIB_SQL.UPDATE_SOURCE_TABLE';

BEGIN

   if I_rr_rowid is NOT NULL then

      update repl_results
         set order_no = I_order_no,
             alloc_no = I_alloc_no,
             status   = 'P'
       where rowid    = CHARTOROWID(I_rr_rowid);
   end if;

   if I_ir_rowid is NOT NULL then

      update ib_results
         set order_no = I_order_no,
             status   = 'P'
       where rowid    = CHARTOROWID(I_ir_rowid);
   end if;

   if I_bw_rowid is NOT NULL then

      update buyer_wksht_manual
         set status = 'P'
       where rowid  = CHARTOROWID(I_bw_rowid);
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END UPDATE_SOURCE_TABLE;
---------------------------------------------------------------------------------------------------------
--*                   BEGIN SUBMIT/APPROVAL LOGIC FUNCTIONS                                             *
---------------------------------------------------------------------------------------------------------
FUNCTION APPROVE_SUBMIT_PO(I_ordhead_tbl    IN      ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                           O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(100) := 'CREATE_PO_LIB_SQL.APPROVE_SUBMIT_PO';

   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE;
   L_otb_status_ind       SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE := 'N';
   L_order_no             ORDHEAD.ORDER_NO%TYPE;
   L_supplier             ORDHEAD.SUPPLIER%TYPE;
   L_reject               BOOLEAN := TRUE;
   L_non_fatal            BOOLEAN := TRUE;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            L_system_options_rec) = FALSE then
      O_error_message := L_error_message;
   end if;

   L_otb_status_ind := NVL(L_system_options_rec.otb_system_ind, 'N');

   /* LOOP on ordhead records */
   if I_ordhead_tbl is NOT NULL and I_ordhead_tbl.COUNT > 0 then
      for i in I_ordhead_tbl.FIRST..I_ordhead_tbl.LAST
      LOOP
          L_order_no := I_ordhead_tbl(i).order_no;
          L_supplier := I_ordhead_tbl(i).supplier;

          if I_ordhead_tbl(i).order_status in ('A','S') then 
             /* VERIFY user-roll privileges */ 
             if I_ordhead_tbl(i).order_status = 'A' then 
                L_reject := VERIFY_USER_ROLL_PRIV(O_error_message,
                                                  L_non_fatal,
                                                  L_order_no);
                if L_reject = FALSE then
                   return FALSE;
                end if;
             end if;

             /* VERIFY lc / hts info */
             if I_ordhead_tbl(i).order_status = 'A' then

                L_reject := VERIFY_LC_HTS(O_error_message,
                                          L_non_fatal,
                                          L_order_no,
                                          I_ordhead_tbl(i).import_order,
                                          I_ordhead_tbl(i).sup_info.payment_method);

                if L_reject = FALSE then
                   return FALSE;
                end if;
                /* Handle a non-fatal return from verify_lc_hts as a successful return */
                if L_non_fatal = FALSE then
                   L_non_fatal := TRUE; 
                end if;
             end if; 

             /* VERIFY item level mins and maxs */
             if L_non_fatal = TRUE then 

                L_reject := VERIFY_ITEM_MIN_MAX(O_error_message,
                                                L_non_fatal,
                                                L_order_no,
                                                L_supplier);
                if L_reject = FALSE then
                   return FALSE;
                end if; 
             end if; 

             /* VERIFY item supplier level mins (order or location mins) */
             if L_non_fatal = TRUE and 
                (I_ordhead_tbl(i).sup_info.min_cnstr_lvl = 'O' or
                I_ordhead_tbl(i).sup_info.min_cnstr_lvl = 'L') then

                L_reject := VERIFY_VEND_MIN(O_error_message,
                                            L_non_fatal,
                                            L_order_no);
                if L_reject = FALSE then
                   return FALSE;
                end if;
             end if; 

             /* VERIFY OTB */
             if SYSTEM_OPTIONS_SQL.GET_OTB_SYSTEM_IND(L_error_message,
                                                      L_otb_status_ind) = FALSE then
                O_error_message := L_error_message;
             end if;

             if L_non_fatal = TRUE and L_otb_status_ind = 'Y' then
                L_reject := VERIFY_OTB(O_error_message,
                                       L_non_fatal,
                                       L_order_no,
                                       I_ordhead_tbl(i).order_status);
                if L_reject = FALSE then
                   return FALSE;
                end if;
             end if; 

             /* if order is not rejected with out rejecting the order
                -- apply deals / brackets / allowances */
             if L_non_fatal = TRUE and I_ordhead_tbl(i).order_status = 'A' then 
                if APPLY_DEALS(O_error_message,
                               L_order_no) = FALSE then
                   return FALSE;
                end if;
             end if;

             if L_non_fatal = TRUE then 
                if SET_APPROVE_SUBMIT_STATUS(O_error_message,
                                             I_ordhead_tbl(i).order_status,
                                             L_order_no) = FALSE then
                   return FALSE;
                end if;
             end if;
          end if; /* end approve or submit */
      END LOOP; /* end order loop */
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END APPROVE_SUBMIT_PO;
---------------------------------------------------------------------------------------------------------
FUNCTION VERIFY_USER_ROLL_PRIV(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_non_fatal         OUT  BOOLEAN,
                               I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(100) := 'CREATE_PO_LIB_SQL.VERIFY_USER_ROLL_PRIV';

   L_ord_appr_amt     RTK_ROLE_PRIVS.ORD_APPR_AMT%TYPE;

   L_unit_cost        ORDLOC.UNIT_COST%TYPE;
   L_unit_retail      ORDLOC.UNIT_RETAIL%TYPE;

   cursor C_ROLE is
      select NVL(MAX(rrp.ord_appr_amt),0)
        from rtk_role_privs rrp,
             sec_user_role sur,
             sec_user su
       where rrp.role = sur.role
         and sur.user_seq = su.user_seq
         and get_user = DECODE(sys_context('RETAIL_CTX', 'APP_USER_ID'), NULL, su.database_user_id, su.application_user_id);

   cursor C_COST_RETAIL is
      select SUM(ol.unit_cost),
             SUM(ol.unit_retail)
        from ordloc ol
       where ol.order_no = I_order_no;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_ROLE','RTK_ROLE_PRIVS,SEC_USER_ROLE,SEC_USER',NULL);
   open C_ROLE;
   SQL_LIB.SET_MARK('FETCH','C_ROLE','RTK_ROLE_PRIVS,SEC_USER_ROLE,SEC_USER',NULL);
   fetch C_ROLE into L_ord_appr_amt;
   if C_ROLE%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_ROLE','RTK_ROLE_PRIVS,SEC_USER_ROLE,SEC_USER',NULL);
      close C_ROLE;
      if SET_WORKSHEET_STATUS(O_error_message,
                              I_order_no,
                              'UR') = FALSE then
         return FALSE;
      end if;
      O_non_fatal := FALSE;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ROLE','RTK_ROLE_PRIVS,SEC_USER_ROLE,SEC_USER',NULL);
   close C_ROLE;

   SQL_LIB.SET_MARK('OPEN','C_COST_RETAIL','ORDLOC','order_no: '||I_order_no);
   open C_COST_RETAIL;
   SQL_LIB.SET_MARK('FETCH','C_COST_RETAIL','ORDLOC','order_no: '||I_order_no);
   fetch C_COST_RETAIL into L_unit_cost, L_unit_retail;
   SQL_LIB.SET_MARK('CLOSE','C_COST_RETAIL','ORDLOC','order_no: '||I_order_no);
   close C_COST_RETAIL;

   /* make sure amt of order is less than the amt user is authorized for */
   if ADD_LINE_ITEM_SQL.LP_ord_appr_amt_code = 'R' then
      if L_ord_appr_amt < L_unit_retail then
         if SET_WORKSHEET_STATUS(O_error_message,
                                 I_order_no,
                                 'UR') = FALSE then
            return FALSE;
         end if;
         O_non_fatal := FALSE;
         return TRUE;
      end if;
   else
      if L_ord_appr_amt < L_unit_cost then
         if SET_WORKSHEET_STATUS(O_error_message,
                                 I_order_no,
                                 'UR') = FALSE then
            return FALSE;
         end if;
         O_non_fatal := FALSE;
         return TRUE;
      end if;
   end if;

   O_non_fatal := TRUE;
   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END VERIFY_USER_ROLL_PRIV;
---------------------------------------------------------------------------------------------------------
FUNCTION VERIFY_ITEM_MIN_MAX(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_non_fatal         OUT  BOOLEAN,
                             I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                             I_supplier       IN      ORDHEAD.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'CREATE_PO_LIB_SQL.VERIFY_ITEM_MIN_MAX';

   L_min_check     VARCHAR2(1);
   L_max_check     VARCHAR2(1);

   cursor C_ITEM_MIN_MAX is
      select DECODE(SIGN(NVL(SUM(ol.qty_ordered),0) -
                         NVL(isc.min_order_qty,0)),
                    -1, 'F',
                    'P'),
             DECODE(SIGN(NVL(isc.max_order_qty, NVL(SUM(ol.qty_ordered),0)) -
                         NVL(SUM(ol.qty_ordered),0)),
                    -1, 'F',
                    'P')
        from item_supp_country isc,
             ordsku os,
             ordloc ol
       where ol.order_no             = I_order_no
         and os.order_no             = ol.order_no
         and os.item                 = ol.item
         and isc.item                = os.item
         and isc.supplier            = I_supplier
         and isc.origin_country_id   = os.origin_country_id
       group by isc.min_order_qty,
                isc.max_order_qty;

BEGIN

   open C_ITEM_MIN_MAX;
   fetch C_ITEM_MIN_MAX into L_min_check, L_max_check;
   close C_ITEM_MIN_MAX;

   if L_min_check <> 'P' or L_max_check <> 'P' then
      if SET_WORKSHEET_STATUS(O_error_message,
                              I_order_no,
                              'VM') = FALSE then
         return FALSE;
      end if;
      O_non_fatal := FALSE;
      return TRUE;
   end if;
   
   O_non_fatal := TRUE;
   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END VERIFY_ITEM_MIN_MAX;
---------------------------------------------------------------------------------------------------------
FUNCTION VERIFY_VEND_MIN(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_non_fatal         OUT  BOOLEAN,
                         I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'CREATE_PO_LIB_SQL.VERIFY_VEND_MIN';

   O_min_flag        VARCHAR2(5) := NULL;
   O_min_qty         SUP_INV_MGMT.MIN_CNSTR_VAL1%TYPE := NULL;
   O_ord_qty         SUP_INV_MGMT.MIN_CNSTR_VAL1%TYPE := NULL;
   L_return_code     VARCHAR2(5) := NULL;

BEGIN

   CHECK_VEND_MIN(I_order_no,
                  -1, /* store */
                  -1, /* wh */
                  O_min_qty,
                  O_ord_qty,
                  O_min_flag,
                  L_return_code,
                  O_error_message);

   if L_return_code = 'FALSE' then     /* procedure failed */
      return FALSE;
   end if;

   if O_min_flag = 'TRUE' then         /* order less than sup minimum */
      if SET_WORKSHEET_STATUS(O_error_message,
                              I_order_no,
                              'VM') = FALSE then
         return FALSE;
      end if;
      O_non_fatal := FALSE;
      return TRUE;
   end if;

   O_non_fatal := TRUE;
   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END VERIFY_VEND_MIN;
---------------------------------------------------------------------------------------------------------
FUNCTION VERIFY_OTB(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    O_non_fatal         OUT  BOOLEAN,
                    I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                    I_order_status   IN      ORDHEAD.STATUS%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'CREATE_PO_LIB_SQL.VERIFY_OTB';

   L_subclass_list   VARCHAR2(154)  := NULL;
   L_excess_ind      VARCHAR2(5)    := NULL;

BEGIN

   if not OTB_SQL.ORD_CHECK(I_order_no,
                            L_subclass_list,
                            L_excess_ind,
                            O_error_message) then
      return FALSE;
   end if;

   if L_excess_ind = 'TRUE' then
      if SET_WORKSHEET_STATUS(O_error_message,
                              I_order_no,
                              'OTB') = FALSE then
         return FALSE;
      end if;
      O_non_fatal := FALSE;
      return TRUE;
   end if;

   if I_order_status = 'A' then
      if not OTB_SQL.ORD_APPROVE(I_order_no,
                                 O_error_message) then
         return FALSE;
      end if;
   end if;

   O_non_fatal := TRUE;
   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END VERIFY_OTB;
---------------------------------------------------------------------------------------------------------
FUNCTION VERIFY_LC_HTS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_non_fatal           OUT  BOOLEAN,
                       I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                       I_import_order_ind IN      ORDHEAD.IMPORT_ORDER_IND%TYPE,
                       I_payment_method   IN      ORDHEAD.PAYMENT_METHOD%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(100) := 'CREATE_PO_LIB_SQL.VERIFY_LC_HTS';

   L_hts_missing         VARCHAR2(1);
   L_no_approved_exists  VARCHAR2(1);

   cursor C_COMPARE_ITEMS is
      select 'x'
        from dual
       where exists(select 'x'
                      from ordsku os,
                           ordhead oh
                     where os.order_no = I_order_no
                       and oh.order_no = os.order_no
                       and oh.import_country_id != os.origin_country_id
                       and not exists (select 'x'
                                         from ordsku_hts osh
                                        where osh.order_no = os.order_no
                                          and osh.item = os.item
                                          and osh.pack_item is NULL));

   cursor C_HTS_CHECK_STATUS is
      select 'x'
        from ordsku_hts osh
       where osh.order_no = I_order_no
         and osh.pack_item is NULL
         and osh.status != 'A';

BEGIN

   if ADD_LINE_ITEM_SQL.LP_import_ind = 'Y' and I_payment_method = 'LC' then
      if SET_WORKSHEET_STATUS(O_error_message,
                              I_order_no,
                              'LC') = FALSE then
         return FALSE;
      end if;
      O_non_fatal := FALSE;
      return TRUE;
   end if;

   if ADD_LINE_ITEM_SQL.LP_elc_ind = 'Y' and ADD_LINE_ITEM_SQL.LP_import_ind = 'Y' and I_import_order_ind = 'Y' then

      open C_COMPARE_ITEMS;
      fetch C_COMPARE_ITEMS into L_hts_missing;
      close C_COMPARE_ITEMS;

      if L_hts_missing is NOT NULL then
         if SET_WORKSHEET_STATUS(O_error_message,
                                 I_order_no,
                                 'HTS') = FALSE then
            return FALSE;
         end if;
         O_non_fatal := FALSE;
         return TRUE;
      end if;

      open C_HTS_CHECK_STATUS;
      fetch C_HTS_CHECK_STATUS into L_no_approved_exists;
      close C_HTS_CHECK_STATUS;

      if L_no_approved_exists is NOT NULL then
         if SET_WORKSHEET_STATUS(O_error_message,
                                 I_order_no,
                                 'HTS') = FALSE then
            return FALSE;
         end if;
         O_non_fatal := FALSE;
         return TRUE;
      end if;
   end if;

   O_non_fatal := TRUE;
   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END VERIFY_LC_HTS;
---------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_OIM(I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                    I_ltl_flag       IN      ORD_INV_MGMT.LTL_IND%TYPE,
                    I_split_message  IN      ORD_INV_MGMT.TRUCK_SPLIT_ISSUES%TYPE,
                    O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(100) := 'CREATE_PO_LIB_SQL.UPDATE_OIM';

   cursor C_LOCK_ORD_INV_MGMT is
      select 'x'
        from ord_inv_mgmt
       where order_no = I_order_no
         for update nowait;

BEGIN

   if ADD_LINE_ITEM_SQL.LP_locked_ind = 'Y' then 
      open C_LOCK_ORD_INV_MGMT;
      close C_LOCK_ORD_INV_MGMT;
   end if;

   update ord_inv_mgmt
      set ltl_ind = NVL(I_ltl_flag, ltl_ind),
          truck_split_issues = I_split_message
    where order_no = I_order_no;

   return TRUE;
   
EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END UPDATE_OIM;
---------------------------------------------------------------------------------------------------------
FUNCTION SET_APPROVE_SUBMIT_STATUS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_order_status   IN      ORDHEAD.STATUS%TYPE,
                                   I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(100) := 'CREATE_PO_LIB_SQL.SET_APPROVE_SUBMIT_STATUS';

   IO_l10n_obj       L10N_OBJ := L10N_OBJ();
   L_wf_order        ORDHEAD.WF_ORDER_NO%TYPE := NULL;
   L_wf_order_no     ORDHEAD.WF_ORDER_NO%TYPE := NULL;
   L_order_status    ORDHEAD.STATUS%TYPE  := 'A';
   L_approval_ind    VARCHAR2(1) := 'Y';

   cursor C_GET_WF_ORDER is
      select NVL(wf_order_no,-1)
        from ordhead
       where order_no = I_order_no;

BEGIN

   if I_order_status = 'A' then 
      IO_l10n_obj.doc_id := I_order_no;
      IO_l10n_obj.doc_type := 'PO';
      IO_l10n_obj.procedure_key := 'LOAD_ORDER_TAX_OBJECT';

      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                IO_l10n_obj) = FALSE then
         return FALSE;
      end if;

      open C_GET_WF_ORDER;
      fetch C_GET_WF_ORDER into L_wf_order;
      close C_GET_WF_ORDER;

      if L_wf_order <> -1 then
         if  WF_PO_SQL.SYNC_F_ORDER(O_error_message,
                                    L_wf_order_no,
                                    L_order_status,
                                    RMS_CONSTANTS.WF_ACTION_UPDATE,
                                    I_order_no,
                                    'A') = FALSE then
             return FALSE;
         end if;
         if L_order_status = 'W' then
            L_approval_ind := 'N';
         end if;
      end if;
   end if;

   if L_approval_ind = 'Y' then
   
      update ordhead
         set status = I_order_status,
             orig_approval_id = USER,
             orig_approval_date = GET_VDATE
       where order_no = I_order_no;
   end if;

   /* Don't approve allocations since they
      won't be created by buyer orders.  */

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END SET_APPROVE_SUBMIT_STATUS;
---------------------------------------------------------------------------------------------------------
FUNCTION SET_WORKSHEET_STATUS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                              I_reject_code    IN      ORDHEAD.REJECT_CODE%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(100) := 'CREATE_PO_LIB_SQL.SET_WORKSHEET_STATUS';

BEGIN

   update ordhead
      set reject_code = I_reject_code
    where order_no = I_order_no;

   update ord_inv_mgmt
      set ord_approve_ind = 'N'
    where order_no = I_order_no;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END SET_WORKSHEET_STATUS;
---------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_GROUP_ROUNDED_QTY(I_ordhead_tbl    IN      ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                                  O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(100) := 'CREATE_PO_LIB_SQL.UPDATE_GROUP_ROUNDED_QTY';

BEGIN
   /* LOOP on ordhead records in order to update the
   appropriate group rounded qty's on ordloc */
   if I_ordhead_tbl is NOT NULL and I_ordhead_tbl.COUNT > 0 then
      for i in I_ordhead_tbl.FIRST..I_ordhead_tbl.LAST
      LOOP
          /* No order is being inserted, don't round what isn't there */
          /* Contract orders should not be rounded */
          if I_ordhead_tbl(i).write_ind = 'Y' or I_ordhead_tbl(i).contract = -1 then

             if not ROUNDING_SQL.CALC_ROUNDED_GROUP_QTYS(O_error_message,
                                                         I_ordhead_tbl(i).order_no) then
                return FALSE;
             end if;
          end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END UPDATE_GROUP_ROUNDED_QTY;
---------------------------------------------------------------------------------------------------------
FUNCTION ROUND_PO(I_ordhead_tbl    IN      ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                  O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)  
RETURN BOOLEAN IS

   L_program         VARCHAR2(100) := 'CREATE_PO_LIB_SQL.UPDATE_GROUP_ROUNDED_QTY';

   L_order_no        ORDHEAD.ORDER_NO%TYPE;
   L_wf_order        ORDHEAD.WF_ORDER_NO%TYPE;
   L_wf_order_no     ORDHEAD.WF_ORDER_NO%TYPE;
   L_order_status    ORDHEAD.STATUS%TYPE;

   cursor C_CHECK_FPO is
      select NVL(oh.wf_order_no,-1)
        from ordhead oh
       where oh.order_no = L_order_no;

BEGIN
   /* LOOP on ordhead nodes */
   if I_ordhead_tbl is NOT NULL and I_ordhead_tbl.COUNT > 0 then
      for i in I_ordhead_tbl.FIRST..I_ordhead_tbl.LAST
      LOOP
          /* No order is being inserted, don't round what isn't there */
          /* Contract orders should not be rounded */
          if I_ordhead_tbl(i).write_ind = 'Y' or I_ordhead_tbl(i).contract = -1 then

             L_order_no := I_ordhead_tbl(i).order_no;

             if not ROUNDING_SQL.ORDER_INTERFACE(O_error_message,
                                                 L_order_no,
                                                 I_ordhead_tbl(i).supplier,
                                                 'Y',
                                                 'Y') then
                return FALSE;
             end if;

             open C_CHECK_FPO;
             fetch C_CHECK_FPO into L_wf_order;
             close C_CHECK_FPO;

             if L_wf_order <> -1 then
                if  WF_PO_SQL.SYNC_F_ORDER(O_error_message,
                                           L_wf_order_no,
                                           L_order_status,
                                           RMS_CONSTANTS.WF_ACTION_UPDATE,
                                           L_order_no,
                                           NULL) = FALSE then
                    return FALSE;
                end if;
             end if;
          end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END ROUND_PO;
---------------------------------------------------------------------------------------------------------
FUNCTION APPLY_DEALS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(100) := 'CREATE_PO_LIB_SQL.APPLY_DEALS';

BEGIN

   if DEAL_ORD_LIB_SQL.EXTERNAL_SHARE_APPLY_DEALS(O_error_message,
                                                  I_order_no,
                                                  'Y',
                                                  'N',
                                                  'N') = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END APPLY_DEALS;
---------------------------------------------------------------------------------------------------------
FUNCTION VALID_SUPS_IMP_EXP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_ordhead_rec    IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_REC)
RETURN BOOLEAN IS

   L_program     VARCHAR2(100) := 'CREATE_PO_LIB_SQL.VALID_SUPS_IMP_EXP';

   L_import_id     SUPS_IMP_EXP.IMPORT_ID%TYPE    := NULL;
   L_import_type   SUPS_IMP_EXP.IMPORT_TYPE%TYPE  := NULL;
   L_exists        BOOLEAN                        := FALSE;
   L_supplier      ORDHEAD.SUPPLIER%TYPE;

BEGIN

   L_supplier     := I_ordhead_rec.supplier;
   LP_import_type := NULL;

   if not SUPP_ATTRIB_SQL.GET_IMPORT_ID(O_error_message,
                                        L_import_id,
                                        L_import_type,
                                        L_exists,
                                        L_supplier) then
      return FALSE;
   end if;

   if L_exists = TRUE then
      I_ordhead_rec.import_id          := L_import_id;
      LP_import_type                   := L_import_type;
   else
      I_ordhead_rec.import_id := NULL;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END VALID_SUPS_IMP_EXP;
---------------------------------------------------------------------------------------------------------
FUNCTION VALID_SUPS_ROUTING_LOC(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_ordhead_rec    IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_REC)
RETURN BOOLEAN IS

   L_program          VARCHAR2(100) := 'CREATE_PO_LIB_SQL.VALID_SUPS_IMP_EXP';

   L_routing_loc_id   SUPS_ROUTING_LOC.ROUTING_LOC_ID%TYPE := NULL;
   L_exists           BOOLEAN                              := FALSE;
   L_supplier         ORDHEAD.SUPPLIER%TYPE;

BEGIN

   L_supplier := I_ordhead_rec.supplier;

   if not SUPP_ATTRIB_SQL.GET_ROUTING_LOC(O_error_message,
                                          L_routing_loc_id,
                                          L_exists,
                                          L_supplier) then
      return FALSE;
   end if;

   if L_exists = TRUE then
      I_ordhead_rec.routing_loc_id := L_routing_loc_id;
   else
      I_ordhead_rec.routing_loc_id := NULL;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END VALID_SUPS_ROUTING_LOC;
---------------------------------------------------------------------------------------------------------
FUNCTION CREATE_F_ORDER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordhead_rec    IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_REC)
RETURN BOOLEAN IS

   L_program         VARCHAR2(100) := 'CREATE_PO_LIB_SQL.CREATE_F_ORDER';

   L_order_no        ORDHEAD.ORDER_NO%TYPE;
   L_location        ORDHEAD.LOCATION%TYPE;
   L_wf_order_no     ORDHEAD.WF_ORDER_NO%TYPE;
   L_order_status    ORDHEAD.STATUS%TYPE;
   L_costing_loc     ITEM_LOC.COSTING_LOC%TYPE;

   cursor C_FETCH_COSTING_LOC is
      select costing_loc
        from item_loc il,
             ordloc ol
       where ol.order_no = L_order_no
         and ol.item     = il.item
         and ol.location = il.loc
         and ol.location = L_location
         and rownum = 1;

BEGIN

   L_order_no  := I_ordhead_rec.order_no;
   L_location  := I_ordhead_rec.order_loc;

   if not WF_PO_SQL.SYNC_F_ORDER(O_error_message,
                                 L_wf_order_no,
                                 L_order_status,
                                 RMS_CONSTANTS.WF_ACTION_CREATE,
                                 L_order_no,
                                 'W') then
      return FALSE;
   end if;

   open C_FETCH_COSTING_LOC;
   fetch C_FETCH_COSTING_LOC into L_costing_loc;
   close C_FETCH_COSTING_LOC;

   update ordhead
      set wf_order_no = L_wf_order_no,
          location = L_location,
          loc_type = 'S',
          import_id = L_costing_loc,
          import_type = 'F'
    where order_no = L_order_no;

   return TRUE;

EXCEPTION
    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END CREATE_F_ORDER;
END CREATE_PO_LIB_SQL;
/