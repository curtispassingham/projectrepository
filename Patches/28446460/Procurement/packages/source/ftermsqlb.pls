CREATE OR REPLACE PACKAGE BODY FTERM_SQL AS

/* Function and Procedure Bodies */
---------------------------------------------------------------------------
FUNCTION PROCESS_TERMS (O_text           IN OUT   VARCHAR2,
                        I_fterm_record   IN       FTERM_RECORD)
RETURN BOOLEAN IS

   L_freightrec       FTERM_RECORD        := I_fterm_record;
   L_exists           VARCHAR2(1)        := 'N';

   CURSOR c_freight IS
      select 'Y'
        from freight_terms
       where freight_terms = L_freightrec.terms;

BEGIN

   if VALIDATE_TERMS(O_text,
                     L_freightrec) = FALSE then
      return FALSE;
   end if;

   open c_freight;
   fetch c_freight into L_exists;
   close c_freight;

   if L_exists = 'N' then
      if INSERT_TERMS(O_text,
                      L_freightrec) = FALSE then
         return FALSE;
      end if;
   else
      if UPDATE_TERMS(O_text,
                      L_freightrec) = FALSE then
         return FALSE;
      end if;
  end if;

  return TRUE;

EXCEPTION
   when OTHERS then
       O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                     SQLERRM,
                                     'FTERMS_SQL.PROCESS_TERMS',
                                     to_char(SQLCODE));
      return FALSE;

END PROCESS_TERMS;
---------------------------------------------------------------------------------
FUNCTION INSERT_TERMS(O_text           IN OUT   VARCHAR2,
                      I_fterm_record   IN       FTERM_RECORD)
return BOOLEAN IS


BEGIN
   insert into freight_terms(freight_terms,
                             start_date_active,
                             end_date_active,
                             enabled_flag)
                      values(I_fterm_record.terms,
                             I_fterm_record.start_date_active,
                             I_fterm_record.end_date_active,
                             I_fterm_record.enabled_flag);

   insert into freight_terms_tl(freight_terms,
                                term_desc,
                                lang,
                                orig_lang_ind,
                                reviewed_ind,
                                create_datetime,
                                create_id,
                                last_update_datetime,
                                last_update_id)
                         values(I_fterm_record.terms,
                                I_fterm_record.description,
                                GET_PRIMARY_LANG,
                                'Y',
                                'N',
                                SYSDATE,
                                GET_USER,
                                SYSDATE,
                                GET_USER
                                );
   return TRUE;

EXCEPTION
   when OTHERS then
      O_text := sql_lib.create_msg('PACKAGE_ERROR',
                                   SQLERRM,
                                   'FTERMS_SQL.SUB_TERMS.INSERT_TERMS',
                                   to_char(SQLCODE));
      return FALSE;
END INSERT_TERMS;
-----------------------------------------------------------------------------------------------
FUNCTION UPDATE_TERMS(O_text           IN OUT   VARCHAR2,
                      I_fterm_record   IN       FTERM_RECORD)
return BOOLEAN IS

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   CURSOR c_lock_terms IS
      select 'Y'
        from freight_terms
       where freight_terms = I_fterm_record.terms
         for update nowait;

   CURSOR c_lock_terms_tl IS
      select 'Y'
        from freight_terms_tl
       where freight_terms = I_fterm_record.terms
         for update nowait;
         
BEGIN
   -- Lock the terms table before updating
   open c_lock_terms;
   close c_lock_terms;
   open c_lock_terms_tl;
   close c_lock_terms_tl;
   ---
   -- When RMS is being used with Oracle Financials version 11.5.10 or later,
   -- the Freight Term data within RMS must match the data in Oracle Financials.
   -- This includes having no start date active or end date active values.  For
   -- example, when a Freight Term with an end date active in the future is updated
   -- within Oracle Financials to be active forever (end date active is NULL), this
   -- API must reflect this change in the Freight Term within RMS.

   update freight_terms
      set start_date_active = I_fterm_record.start_date_active,
          end_date_active = I_fterm_record.end_date_active,
          enabled_flag = I_fterm_record.enabled_flag
    where freight_terms = I_fterm_record.terms;

   update freight_terms_tl
      set term_desc = I_fterm_record.description,
          last_update_datetime = SYSDATE,
          last_update_id = GET_USER
    where freight_terms = I_fterm_record.terms
      and lang = GET_PRIMARY_LANG;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_text := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                   'FREIGHT_TERMS',
                                   I_fterm_record.terms,
                                   'FTERMS_SQL.UPDATE_TERMS');
      return TRUE;
   when OTHERS then
      O_text := sql_lib.create_msg('PACKAGE_ERROR',
                                   SQLERRM,
                                   'FTERMS_SQL.UPDATE_TERMS',
                                   to_char(SQLCODE));
      return FALSE;
END UPDATE_TERMS;
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_TERMS(O_text        IN OUT VARCHAR2,
                        I_fterm_record  IN     FTERM_RECORD)
return BOOLEAN IS

BEGIN

   if CHECK_NULLS(O_text,
                  I_fterm_record.terms) = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_text,
                  I_fterm_record.description) = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_text,
                  I_fterm_record.enabled_flag) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
      O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                   SQLERRM,
                                   'FTERMS_SQL.VALIDATE_TERMS',
                                   to_char(SQLCODE));
      return FALSE;

END VALIDATE_TERMS;
----------------------------------------------------------------------------------------
FUNCTION CHECK_NULLS (O_text              IN OUT   VARCHAR2,
                      I_record_variable   IN       VARCHAR2)
return BOOLEAN IS

BEGIN

   if I_record_variable is NULL then
      O_text := sql_lib.create_msg('INVALID_PARM_IN_FUNC',
                                   'I_record_variable',
                                   'NULL',
                                   'FTERMS_SQL.CHECK_NULLS');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                   SQLERRM,
                                   'FTERMS_SQL.CHECK_NULLS',
                                   to_char(SQLCODE));
      return FALSE;

END CHECK_NULLS;
----------------------------------------------------------------------------
END FTERM_SQL;
/
