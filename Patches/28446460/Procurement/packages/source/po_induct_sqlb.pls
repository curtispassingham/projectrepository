create or replace
PACKAGE BODY PO_INDUCT_SQL AS

   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab   s9t_errors_tab_typ;
   LP_error_exists     VARCHAR2(1)          := 'N';
   TYPE name_trans IS TABLE OF VARCHAR2(255) INDEX BY VARCHAR2(255);
   sheet_name_trans    S9T_PKG.trans_map_typ;
   TYPE svc_po_tab_typ IS TABLE OF SVC_PO_DEL%ROWTYPE;
   LP_svc_po_tab       svc_po_tab_typ;
   LP_format           VARCHAR2(64);

   action_column       VARCHAR2(255)        := 'ACTION';
   P_ERROR             CONSTANT VARCHAR2(4) := 'PE';
   P_WARNING           CONSTANT VARCHAR2(4) := 'PW';
   P_SUCCESS           CONSTANT VARCHAR2(4) := 'PS';

--Private sub program declaration
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_OHA(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_ORH(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_ORE(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_ORD(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_OHE(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_ODT(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_OHA(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_ORH(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_ORE(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_ORD(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_OHE(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_ODT(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id    IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet      IN   VARCHAR2,
                          I_row_seq    IN   NUMBER,
                          I_col        IN   VARCHAR2,
                          I_sqlcode    IN   NUMBER,
                          I_sqlerrm    IN   VARCHAR2,
                          I_err_type   IN   VARCHAR2 DEFAULT NULL);
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   NUMBER);
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_OHE(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                         I_process_id    IN   SVC_ORDHEAD.PROCESS_ID%TYPE);
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ODT(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_ORDDETAIL.PROCESS_ID%TYPE);
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ORD(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_ORDLC.PROCESS_ID%TYPE);
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ORE(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_ORDLOC_EXP.PROCESS_ID%TYPE);
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ORH(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_ORDSKU_HTS.PROCESS_ID%TYPE);
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_OHA(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_ORDSKU_HTS_ASSESS.PROCESS_ID%TYPE);
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T_CFA(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_file_id        IN      S9T_FOLDER.FILE_ID%TYPE,
                         I_process_id     IN      SVC_CFA_EXT.PROCESS_ID%TYPE,
                         I_template_key   IN      SVC_PROCESS_TRACKER.TEMPLATE_KEY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION IS_NUMBER(I_string IN VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_PRC_DEST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_NEW_ACTION(I_old_action   IN   VARCHAR2,
                        I_new_action   IN   VARCHAR2)
RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER);
-------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_UPLD_FROM_EXT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION EXEC_ASYNC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_rms_async_id    IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                    I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program       VARCHAR2(75) := 'PO_INDUCT_SQL.EXEC_ASYNC';
   L_error_count   NUMBER := 0;

   cursor C_PRC is
      select process_id,
             process_desc,
             file_id,
             template_key,
             action_type,
             process_source,
             process_destination,
             action_date,
             status,
             user_id,
             rms_async_id,
             file_path
        from svc_process_tracker
       where rms_async_id = I_rms_async_id
          or (I_rms_async_id is NULL
              and template_key = 'RMSSUB_XORDER'
              and process_id = I_process_id);

   c_prc_rec   C_PRC%ROWTYPE;

BEGIN
logger.log('Begin exec_async for async id: '||I_rms_async_id);
   open C_PRC;
   fetch C_PRC into c_prc_rec;
   close C_PRC;

   if c_prc_rec.action_type = 'D' then
      if PO_INDUCT_SQL.CREATE_FILE(O_error_message,
                                   c_prc_rec.file_id,
                                   c_prc_rec.process_id,
                                   c_prc_rec.action_type,
                                   c_prc_rec.process_source,
                                   c_prc_rec.template_key,
                                   c_prc_rec.file_path) = FALSE then
         return FALSE;
      end if;
   end if;

   if c_prc_rec.action_type = 'U' and c_prc_rec.process_source = 'S9T' then
logger.log('calling PO_INDUCT_SQL.PROCESS_S9T for PID: '||c_prc_rec.process_id);
     if PO_INDUCT_SQL.PROCESS_S9T(O_error_message,
                                  c_prc_rec.file_id,
                                  c_prc_rec.process_id,
                                  L_error_count) = FALSE then
        return FALSE;
     end if;
   end if;
   if c_prc_rec.action_type = 'U' and c_prc_rec.process_source = 'STG' then
logger.log('calling PO_INDUCT_SQL.PREPARE_UPLD_FROM_STG for PID: '||c_prc_rec.process_id);
     if PO_INDUCT_SQL.PREPARE_UPLD_FROM_STG(O_error_message,
                                            c_prc_rec.process_id) = FALSE then
        return FALSE;
     end if;
   elsif c_prc_rec.action_type = 'U' and c_prc_rec.process_source = 'EXT' then
     if PO_INDUCT_SQL.PREPARE_UPLD_FROM_EXT(O_error_message,
                                            c_prc_rec.process_id) = FALSE then
        return FALSE;
     end if;
   end if;

   if c_prc_rec.action_type = 'U' and c_prc_rec.process_destination = 'RMS' and
     (L_error_count = 0 or LP_error_exists = 'N') then
      if CORESVC_PO.PROCESS(O_error_message,
                            c_prc_rec.process_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END EXEC_ASYNC;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_FILE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id         OUT      SVC_PROCESS_TRACKER.FILE_ID%TYPE,
                     I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                     I_action          IN       VARCHAR2,
                     I_source          IN       VARCHAR2,
                     I_template        IN       VARCHAR2,
                     I_file_path       IN       VARCHAR2,
                     I_err_4_process   IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
   L_program             VARCHAR2(75) := 'PO_INDUCT_SQL.CREATE_FILE';
   L_template_only_ind   CHAR         := 'N';

BEGIN
   if I_action = 'DT' then
      L_template_only_ind := 'Y';
  end if;

   PO_INDUCT_SQL.template_key        := I_template;
   PO_INDUCT_SQL.file_name           := s9t_pkg.basename(I_file_path);
   PO_INDUCT_SQL.add_error_4_process := I_err_4_process;

   if ((I_action = 'D' and I_source = 'RMS') or I_action = 'DT') then
      if PO_INDUCT_SQL.CREATE_S9T(O_error_message,
                                  O_file_id,
                                  I_process_id,
                                  L_template_only_ind) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_source = 'STG' then
      if PO_INDUCT_SQL.CREATE_SVC_S9T(O_error_message,
                                      O_file_id,
                                      I_process_id) = FALSE then
         return FALSE;
      end if;
   end if;

   if PO_INDUCT_SQL.DELETE_SEARCH_POS(O_error_message,
                                      I_process_id) = FALSE then
      return FALSE;
   end if;

   update svc_process_tracker
      set file_id      = O_file_id,
          status       = 'PS'
    where process_id = I_process_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END CREATE_FILE;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_process_id          IN       NUMBER,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')

RETURN BOOLEAN IS
   L_program   VARCHAR2(255) := 'PO_INDUCT_SQL.CREATE_S9T';
   L_file      s9t_file;

BEGIN
   INIT_S9T(O_file_id);

   if I_template_only_ind = 'N' then
      POPULATE_OHA(O_file_id, I_process_id);
      POPULATE_ORH(O_file_id, I_process_id);
      POPULATE_ORE(O_file_id, I_process_id);
      POPULATE_ORD(O_file_id, I_process_id);
      POPULATE_OHE(O_file_id, I_process_id);
      POPULATE_ODT(O_file_id, I_process_id);
      commit;
   end if;

   if CORESVC_CFLEX.ADD_ORDER_CFLEX_SHEETS(O_error_message,
                                           O_file_id,
                                           I_process_id,
                                           template_key,
                                           I_template_only_ind) = FALSE then
      return FALSE;
   end if;
   
   if PO_INDUCT_SQL.POPULATE_LISTS(O_error_message,
                                   O_file_id,
                                   template_key) = FALSE THEN
      return FALSE;
   end if;
   
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   --Apply template
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := s9t_file(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        'POS9T',
                        L_file) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_SVC_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_file_id         IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program   VARCHAR2(255) := 'PO_INDUCT_SQL.CREATE_SVC_S9T';
   L_file      s9t_file;

BEGIN
   INIT_S9T(O_file_id);

   POPULATE_SVC_OHA(O_file_id,
                    I_process_id);

   POPULATE_SVC_ORH(O_file_id,
                    I_process_id);

   POPULATE_SVC_ORE(O_file_id,
                    I_process_id);

   POPULATE_SVC_ORD(O_file_id,
                    I_process_id);

   POPULATE_SVC_OHE(O_file_id,
                    I_process_id);

   POPULATE_SVC_ODT(O_file_id,
                    I_process_id);

   if CORESVC_CFLEX.ORDER_SVC2S9T(O_error_message,
                                 O_file_id,
                                 I_process_id,
                                 template_key)= FALSE THEN
      return FALSE;
   end if;
   
   commit;
   
   if PO_INDUCT_SQL.POPULATE_LISTS(O_error_message,
                                   O_file_id,
                                   template_key) = FALSE THEN
      return FALSE;
   end if;
   
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   --Apply template
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   if ADD_ERROR_4_PROCESS is NOT NULL then
      if ADD_ERROR_SHEET(O_error_message,
                         O_file_id,
                         ADD_ERROR_4_PROCESS) = FALSE then
         return FALSE;
      end if;
   end if;

   L_file := s9t_file(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message ,
                        'POS9T',
                        L_file)=FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_SVC_S9T;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_SEARCH_POS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program   VARCHAR2(75) := 'PO_INDUCT_SQL.DELETE_SEARCH_POS';

BEGIN
   delete from svc_po_search_temp
         where process_id = I_process_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END DELETE_SEARCH_POS;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_LISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_file_id         IN       NUMBER,
                        I_template_key    IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS
   L_program   VARCHAR2(75) := 'PO_INDUCT_SQL.POPULATE_LISTS';

BEGIN
   if I_template_key is not null then 
      if S9T_PKG.POPULATE_LISTS(O_error_message,
                                I_file_id,
                                'POS9T',
                                I_template_key)=FALSE then
         return FALSE;
      end if;
   else
      if S9T_PKG.POPULATE_LISTS(O_error_message,
                                I_file_id,
                               'POS9T')=FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END POPULATE_LISTS;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_OHA(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = OHA_sheet)
        select s9t_row(s9t_cells('MOD'
                                ,oha.order_no
                                ,oh.item
                                ,oh.pack_item
                                ,oh.hts
                                ,oha.comp_id
                                ,oha.cvb_code
                                ,oha.comp_rate
                                ,oha.per_count
                                ,oha.per_count_uom
                                ,oha.est_assess_value
                                ,oha.nom_flag_1
                                ,oha.nom_flag_2
                                ,oha.nom_flag_3
                                ,oha.nom_flag_4
                                ,oha.nom_flag_5))
                  from ordsku_hts_assess oha,
                       ordsku_hts oh
                 where oha.order_no = oh.order_no
                   and oha.seq_no = oh.seq_no
                   and oha.order_no in (select order_no
                                          from svc_po_search_temp
                                         where process_id = I_process_id);
END POPULATE_OHA;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_ORH(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = ORH_sheet)
        select s9t_row(s9t_cells('MOD'
                                ,order_no
                                ,item
                                ,pack_item
                                ,hts
                                ,import_country_id
                                ,effect_from
                                ,effect_to
                                ,status
                                ,origin_country_id))
          from ordsku_hts
         where order_no in (select order_no
                              from svc_po_search_temp
                             where process_id = I_process_id);
END POPULATE_ORH;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_ORE(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = ORE_sheet)
        select s9t_row(s9t_cells('MOD'
                                ,order_no
                                ,item
                                ,pack_item
                                ,location
                                ,loc_type
                                ,comp_id
                                ,cvb_code
                                ,cost_basis
                                ,comp_rate
                                ,comp_currency
                                ,exchange_rate
                                ,per_count
                                ,per_count_uom
                                ,est_exp_value
                                ,nom_flag_1
                                ,nom_flag_2
                                ,nom_flag_3
                                ,nom_flag_4
                                ,nom_flag_5
                                ,origin
                                ,defaulted_from
                                ,key_value_1
                                ,key_value_2))
          from ordloc_exp
         where order_no in (select order_no
                              from svc_po_search_temp
                             where process_id = I_process_id);
END POPULATE_ORE;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_ORD(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = ORD_sheet)
        select s9t_row(s9t_cells('MOD'
                                ,order_no
                                ,lc_ref_id
                                ,lc_group_id
                                ,applicant
                                ,beneficiary
                                ,merch_desc
                                ,transshipment_ind
                                ,partial_shipment_ind
                                ,lc_ind))
          from ordlc
         where order_no in (select order_no
                              from svc_po_search_temp
                             where process_id = I_process_id);
END POPULATE_ORD;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_OHE(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = OHE_sheet)
        select s9t_row(s9t_cells('MOD'
                                ,order_no
                                ,order_type
                                ,dept
                                ,buyer
                                ,supplier
                                ,loc_type
                                ,location
                                ,promotion
                                ,qc_ind
                                ,written_date
                                ,not_before_date
                                ,not_after_date
                                ,otb_eow_date
                                ,earliest_ship_date
                                ,latest_ship_date
                                ,NVL(tl.terms_code,oh.terms)
                                ,freight_terms
                                ,orig_ind
                                ,payment_method
                                ,backhaul_type
                                ,backhaul_allowance
                                ,ship_method
                                ,purchase_type
                                ,status
                                ,ship_pay_method
                                ,fob_trans_res
                                ,fob_trans_res_desc
                                ,fob_title_pass
                                ,fob_title_pass_desc
                                ,edi_po_ind
                                ,import_order_ind
                                ,import_country_id
                                ,include_on_order_ind
                                ,vendor_order_no
                                ,exchange_rate
                                ,factory
                                ,agent
                                ,discharge_port
                                ,lading_port
                                ,freight_contract_no
                                ,po_type
                                ,pre_mark_ind
                                ,currency_code
                                ,reject_code
                                ,contract_no
                                ,pickup_loc
                                ,pickup_no
                                ,pickup_date
                                ,comment_desc
                                ,partner_type_1
                                ,partner1
                                ,partner_type_2
                                ,partner2
                                ,partner_type_3
                                ,partner3
                                ,import_type
                                ,import_id
                                ,clearing_zone_id
                                ,routing_loc_id
                                ,NULL --,ext_ref_no
                                ,master_po_no
                                ,NULL --re_approve_ind
                                ,NULL --last_upd_id
                                ,NULL --next_upd_id
                                ,NULL)) --last_upd_datetime
          from ordhead oh,
               (select distinct terms,terms_code
                  from V_Terms_Head_Tl )tl
         where order_no in (select order_no
                              from svc_po_search_temp
                             where process_id = I_process_id)
         and oh.terms=tl.terms(+);
END POPULATE_OHE;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_ODT(I_file_id      IN   NUMBER,
                       I_process_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = ODT_sheet)
        select s9t_row(s9t_cells('MOD'                      -- action   -- ordsku records only
                                ,osk.order_no               -- order_no
                                ,osk.item                   -- item
                                ,im.item_desc               -- item_desc
                                ,isp.vpn                    -- vpn
                                ,im.item_parent             -- item_parent
                                ,osk.ref_item               -- ref_item
                                ,im.diff_1                  -- diff_1
                                ,im.diff_2                  -- diff_2
                                ,im.diff_3                  -- diff_3
                                ,im.diff_4                  -- diff_4
                                ,osk.origin_country_id      -- origin_country_id
                                ,osk.supp_pack_size         -- supp_pack_size
                                ,NULL                       -- loc_type
                                ,NULL                       -- location
                                ,NULL                       -- location_desc
                                ,NULL                       -- unit_cost
                                ,NULL                       -- qty_ordered
                                ,NULL                       -- delivery_date
                                ,NULL                       -- non_scale_ind
                                ,NULL                       -- processing_type
                                ,NULL                       -- processing_wh
                                ,NULL                       -- alloc_no
                                ,NULL                       -- qty_cancelled
                                ,NULL                       -- qty_transferred
                                ,NULL                       -- cancel_code
                                ,NULL                       -- release_date
                                ,NULL ))                    -- in_store_date
                                                      
          from ordhead oh
              ,ordsku osk
              ,item_master im
              ,item_supplier isp
         where oh.order_no = osk.order_no
           and im.item = osk.item
           and im.item = isp.item
           and isp.supplier = oh.supplier
           and NOT exists(select 'x'
                            from alloc_header ah
                           where ah.order_no = osk.order_no
                             and rownum = 1)
           and NOT exists(select 'x'
                            from ordloc ol
                           where ol.order_no = osk.order_no
                             and rownum = 1)
           and osk.order_no in (select order_no
                                  from svc_po_search_temp
                                 where process_id = I_process_id)
         union all
        select s9t_row(s9t_cells('MOD'                        -- action -- ordloc and ordsku records without allocation
                                 ,osk.order_no                -- order_no
                                 ,osk.item                    -- item
                                 ,im.item_desc                -- item_desc
                                 ,isp.vpn                     -- vpn
                                 ,im.item_parent              -- item_parent
                                 ,osk.ref_item                -- ref_item
                                 ,im.diff_1                   -- diff_1
                                 ,im.diff_2                   -- diff_2
                                 ,im.diff_3                   -- diff_3
                                 ,im.diff_4                   -- diff_4
                                 ,osk.origin_country_id       -- origin_country_id
                                 ,osk.supp_pack_size          -- supp_pack_size
                                 ,loc.loc_type                -- loc_type
                                 ,loc.location                -- location
                                 ,loc.loc_name                -- location_desc
                                 ,loc.unit_cost               -- unit_cost
                                 ,loc.qty_ordered - NVL(alloc_rec.total_qty_allocated,0)    -- qty_ordered, unallocated
                                 ,NULL                        -- delivery_date
                                 ,loc.non_scale_ind           -- non_scale_ind
                                 ,NULL                        -- processing_type
                                 ,NULL                        -- processing_wh
                                 ,NULL                        -- alloc_no
                                 ,loc.qty_cancelled           -- qty_cancelled
                                 ,NULL                        -- qty_transferred
                                 ,loc.cancel_code             -- cancel_code
                                 ,NULL                        -- release_date
                                 ,NULL ))                     -- in_store_date
                                                       
          from ordsku osk
              ,ordhead oh
              ,item_master im
              ,item_supplier isp
              ,(select wh.wh location
                      ,wh.wh_name loc_name
                      ,'W' loc_type
                      ,ordloc.order_no
                      ,ordloc.item
                      ,ordloc.unit_cost
                      ,ordloc.qty_ordered
                      ,ordloc.non_scale_ind
                      ,ordloc.qty_cancelled
                      ,ordloc.cancel_code
                  from wh,
                       ordloc
                 where ordloc.location = wh.wh
                 union all
                select store location
                      ,store_name loc_name
                      ,'S' loc_type
                      ,ordloc.order_no
                      ,ordloc.item
                      ,ordloc.unit_cost
                      ,ordloc.qty_ordered
                      ,ordloc.non_scale_ind
                      ,ordloc.qty_cancelled
                      ,ordloc.cancel_code
                  from store
                      ,ordloc
                 where ordloc.location = store.store) loc
              ,(select sum(ad.qty_allocated) total_qty_allocated,
                       ah.alloc_no,
                       ah.order_no,
                       ah.item,
                       ah.wh
                  from alloc_detail ad,
                       alloc_header ah
                 where ad.alloc_no = ah.alloc_no
                   and ah.order_no in (select order_no
                                         from svc_po_search_temp
                                        where process_id = I_process_id)
                 group by ah.alloc_no,
                       ah.order_no,
                       ah.item,
                       ah.wh)  alloc_rec
         where im.item = osk.item
           and osk.item = isp.item
           and isp.item = loc.item
           and osk.order_no = loc.order_no
           and oh.order_no = osk.order_no
           and oh.supplier = isp.supplier
           and oh.order_no = alloc_rec.order_no(+)
           and osk.item = alloc_rec.item(+)
           and loc.location = alloc_rec.wh(+)
           and (loc.qty_ordered - NVL(alloc_rec.total_qty_allocated,0) > 0
                or loc.qty_ordered = 0)
           and oh.order_no in (select order_no
                                 from svc_po_search_temp
                                where process_id = I_process_id)
         union all
         select s9t_row(s9t_cells('MOD'                         -- action -- ordloc and ordsku records WITH allocation
                                 ,osk.order_no                  -- order_no
                                 ,osk.item                      -- item
                                 ,im.item_desc                  -- item_desc
                                 ,isp.vpn                       -- vpn
                                 ,im.item_parent                -- item_parent
                                 ,osk.ref_item                  -- ref_item
                                 ,im.diff_1                     -- diff_1
                                 ,im.diff_2                     -- diff_2
                                 ,im.diff_3                     -- diff_3
                                 ,im.diff_4                     -- diff_4
                                 ,osk.origin_country_id         -- origin_country_id
                                 ,osk.supp_pack_size            -- supp_pack_size
                                 ,adt.to_loc_type               -- loc_type
                                 ,adt.to_loc                    -- location
                                 ,adt.loc_name                  -- location_desc
                                 ,ol.unit_cost                  -- unit_cost
                                 ,adt.qty_allocated             -- qty_ordered
                                 ,NULL                          -- delivery_date
                                 ,ol.non_scale_ind              -- non_scale_ind
                                 ,'Cross Dock'                  -- processing_type
                                 ,ah.wh                         -- processing_wh
                                 ,ah.alloc_no                   -- alloc_no
                                 ,adt.qty_cancelled             -- qty_cancelled
                                 ,adt.qty_transferred           -- qty_transferredred
                                 ,ol.cancel_code                -- cancel_code
                                 ,ah.release_date               -- release_date
                                 ,adt.in_store_date))           -- in_store_date
           from ordsku osk
               ,ordhead oh
               ,ordloc ol
               ,item_master im
               ,item_supplier isp
               ,alloc_header ah
               ,(select store to_loc
                       ,store_name loc_name
                       ,'S' to_loc_type
                       ,ad.alloc_no
                       ,ad.qty_allocated
                       ,ad.qty_cancelled
                       ,ad.qty_transferred
                       ,ad.in_store_date
                   from store s
                       ,alloc_detail ad
                  where ad.to_loc = s.store
              union all
                 select w.wh to_loc
                       ,w.wh_name loc_name
                       ,'W' to_loc_type
                       ,ad.alloc_no
                       ,ad.qty_allocated
                       ,ad.qty_cancelled
                       ,ad.qty_transferred
                       ,ad.in_store_date
                   from wh w
                       ,alloc_detail ad
                  where ad.to_loc = w.wh) adt
          where im.item = osk.item
            and osk.item = isp.item
            and isp.item = ol.item
            and ol.item = ah.item
            and osk.order_no = ol.order_no
            and ol.order_no = oh.order_no
            and oh.order_no = ah.order_no
            and ah.alloc_no = adt.alloc_no
            and ah.wh = ol.location
            and oh.supplier = isp.supplier
            and oh.order_no in (select order_no
                                  from svc_po_search_temp
                                 where process_id = I_process_id);
END POPULATE_ODT;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_OHA(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = OHA_sheet)
        select s9t_row(s9t_cells('MOD'
                                ,order_no
                                ,item
                                ,pack_item
                                ,hts
                                ,comp_id
                                ,cvb_code
                                ,comp_rate
                                ,per_count
                                ,per_count_uom
                                ,est_assess_value
                                ,nom_flag_1
                                ,nom_flag_2
                                ,nom_flag_3
                                ,nom_flag_4
                                ,nom_flag_5))
          from svc_ordsku_hts_assess
         where order_no in (select order_no
                              from svc_po_search_temp
                             where process_id = I_process_id);
END POPULATE_SVC_OHA;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_ORH(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = ORH_sheet)
        select s9t_row(s9t_cells('MOD'
                                ,order_no
                                ,item
                                ,pack_item
                                ,hts
                                ,import_country_id
                                ,effect_from
                                ,effect_to
                                ,status
                                ,origin_country_id))
          from svc_ordsku_hts
         where order_no in (select order_no
                              from svc_po_search_temp
                             where process_id = I_process_id);
END POPULATE_SVC_ORH;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_ORE(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = ORE_sheet)
        select s9t_row(s9t_cells('MOD'
                                ,order_no
                                ,item
                                ,pack_item
                                ,location
                                ,loc_type
                                ,comp_id
                                ,cvb_code
                                ,cost_basis
                                ,comp_rate
                                ,comp_currency
                                ,exchange_rate
                                ,per_count
                                ,per_count_uom
                                ,est_exp_value
                                ,nom_flag_1
                                ,nom_flag_2
                                ,nom_flag_3
                                ,nom_flag_4
                                ,nom_flag_5
                                ,origin
                                ,defaulted_from
                                ,key_value_1
                                ,key_value_2))
          from svc_ordloc_exp
         where order_no in (select order_no
                              from svc_po_search_temp
                             where process_id = I_process_id);
END POPULATE_SVC_ORE;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_ORD(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = ORD_sheet)
        select s9t_row(s9t_cells('MOD'
                                ,order_no
                                ,lc_ref_id
                                ,lc_group_id
                                ,applicant
                                ,beneficiary
                                ,merch_desc
                                ,transshipment_ind
                                ,partial_shipment_ind
                                ,lc_ind))
          from svc_ordlc
         where order_no in (select order_no
                              from svc_po_search_temp
                             where process_id = I_process_id);
END POPULATE_SVC_ORD;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_OHE(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = OHE_sheet)
        select s9t_row(s9t_cells('MOD'
                                ,order_no
                                ,order_type
                                ,dept
                                ,buyer
                                ,supplier
                                ,loc_type
                                ,location
                                ,promotion
                                ,qc_ind
                                ,written_date
                                ,not_before_date
                                ,not_after_date
                                ,otb_eow_date
                                ,earliest_ship_date
                                ,latest_ship_date
                                ,terms
                                ,freight_terms
                                ,orig_ind
                                ,payment_method
                                ,backhaul_type
                                ,backhaul_allowance
                                ,ship_method
                                ,purchase_type
                                ,status
                                ,ship_pay_method
                                ,fob_trans_res
                                ,fob_trans_res_desc
                                ,fob_title_pass
                                ,fob_title_pass_desc
                                ,edi_po_ind
                                ,import_order_ind
                                ,import_country_id
                                ,include_on_order_ind
                                ,vendor_order_no
                                ,exchange_rate
                                ,factory
                                ,agent
                                ,discharge_port
                                ,lading_port
                                ,freight_contract_no
                                ,po_type
                                ,pre_mark_ind
                                ,currency_code
                                ,reject_code
                                ,contract_no
                                ,pickup_loc
                                ,pickup_no
                                ,pickup_date
                                ,comment_desc
                                ,partner_type_1
                                ,partner1
                                ,partner_type_2
                                ,partner2
                                ,partner_type_3
                                ,partner3
                                ,import_type
                                ,import_id
                                ,clearing_zone_id
                                ,routing_loc_id
                                ,ext_ref_no
                                ,master_po_no
                                ,re_approve_ind
                                ,last_upd_id
                                ,next_upd_id
                                ,last_upd_datetime))
          from svc_ordhead
         where order_no in (select order_no
                              from svc_po_search_temp
                             where process_id = I_process_id);
END POPULATE_SVC_OHE;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SVC_ODT(I_file_id      IN   NUMBER,
                           I_process_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = ODT_sheet)
        select s9t_row(s9t_cells('MOD'
                                ,order_no
                                ,item
                                ,item_desc
                                ,vpn
                                ,item_parent
                                ,ref_item
                                ,diff_1
                                ,diff_2
                                ,diff_3
                                ,diff_4
                                ,origin_country_id
                                ,supp_pack_size
                                ,loc_type
                                ,location
                                ,location_desc
                                ,unit_cost
                                ,qty_ordered
                                ,delivery_date
                                ,non_scale_ind
                                ,processing_type
                                ,processing_wh
                                ,alloc_no
                                ,qty_cancelled
                                ,qty_transferred
                                ,cancel_code
                                ,release_date))
          from svc_orddetail
         where order_no in (select order_no
                              from svc_po_search_temp
                             where process_id = I_process_id);
END POPULATE_SVC_ODT;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_ERROR_SHEET(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                         I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(75)  := 'PO_INDUCT_SQL.ADD_ERROR_SHEET';
   L_err_sheet_name   VARCHAR2(255) := 'ERRORS';
   L_sheet_names      s9t_cells;
   L_cols             s9t_cells;
   PRAGMA             AUTONOMOUS_TRANSACTION;

   cursor C_SHEET is
      select wksht_name
        from v_s9t_tmpl_wksht_def
       where template_key = 'PO_IND_ERRORS'
         and wksht_key    = 'ERRORS';

   cursor C_COL(I_col_key IN VARCHAR2) is
      select column_name
        from v_s9t_tmpl_cols_def
       where template_key = 'PO_IND_ERRORS'
         and wksht_key    = 'ERRORS'
         and column_key   = I_col_key;

BEGIN

   select sf.s9t_file_obj.sheet_names
     into L_sheet_names
     from s9t_folder sf
    where file_id = I_file_id;

   L_cols := NEW s9t_cells('ORDER_NO',
                           'ITEM',
                           'ITEM_DESC_TRANS',
                           'SUPPLIER',
                           'SUP_NAME_TRANS',
                           'LOCATION',
                           'LOC_NAME_TRANS',
                           'TABLE_DESC',
                           'COLUMN_DESC',
                           'ROW_SEQ',
                           'ERROR_MSG',
                           'ERROR_TYPE');

   open C_SHEET;
   fetch C_SHEET into L_err_sheet_name;
   close C_SHEET;
   L_sheet_names.EXTEND;
   L_sheet_names(L_sheet_names.count()) := L_err_sheet_name;

   FOR i IN 1..L_cols.COUNT LOOP
      open C_COL(L_cols(i));
      fetch C_COL into L_cols(i);
      close C_COL;
   END LOOP;

   update s9t_folder sf
      set sf.s9t_file_obj.sheet_names = L_sheet_names
    where file_id                     = I_file_id;

   insert into TABLE(select sf.s9t_file_obj.sheets
                       from s9t_folder sf
                      where file_id = I_file_id)
      select s9t_sheet(L_err_sheet_name,
                       L_cols,
                       s9t_row_tab(),
                       s9t_list_val_tab())
        from dual;

   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = l_err_sheet_name)
      select s9t_row(s9t_cells(order_no,
                               item,
                               item_desc_trans,
                               supplier,
                               sup_name_trans,
                               location,
                               loc_name_trans,
                               table_desc,
                               column_desc,
                               row_seq,
                               error_msg,
                               error_type))
        from v_poind_errors_list
       where process_id = I_process_id;
   commit;

   return TRUE;

EXCEPTION
   when OTHERS then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END ADD_ERROR_SHEET;
-------------------------------------------------------------------------------------------------------
FUNCTION SEARCH_POS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_order_count     IN OUT   NUMBER,
                    I_sch_crit_rec    IN       PO_INDUCT_SCH_CRIT_TYP,
                    I_source          IN       VARCHAR2,
                    I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program   VARCHAR2(75) := 'PO_INDUCT_SQL.SEARCH_POS';

BEGIN
   if I_source = 'RMS' then
      insert into svc_po_search_temp(process_id,
                                     order_no)
                              select I_process_id,
                                     oh.order_no
                                from v_ordhead oh
                               where (I_sch_crit_rec.order_no is NULL
                                     or oh.order_no = I_sch_crit_rec.order_no)
                                 and (I_sch_crit_rec.status is NULL
                                     or oh.status = I_sch_crit_rec.status)
                                 and (I_sch_crit_rec.supplier is NULL
                                     or oh.supplier = I_sch_crit_rec.supplier)
                                 and (I_sch_crit_rec.buyer is NULL
                                     or oh.buyer = I_sch_crit_rec.buyer)
                                 and (I_sch_crit_rec.dept is NULL
                                     or oh.dept = I_sch_crit_rec.dept
                                     or exists (select 'x'
                                                  from v_ordsku os,
                                                       v_item_master im
                                                 where os.order_no = oh.order_no
                                                   and os.item = im.item
                                                   and im.dept = I_sch_crit_rec.dept))
                                 and (I_sch_crit_rec.class is NULL
                                     or exists (select 'x'
                                                  from v_ordsku os,
                                                       v_item_master im
                                                 where os.order_no = oh.order_no
                                                   and os.item = im.item
                                                   and im.dept = I_sch_crit_rec.dept
                                                   and im.class = I_sch_crit_rec.class))
                                 and (I_sch_crit_rec.subclass is NULL
                                     or exists (select 'x'
                                                  from v_ordsku os,
                                                       v_item_master im
                                                 where os.order_no = oh.order_no
                                                   and os.item = im.item
                                                   and im.dept = I_sch_crit_rec.dept
                                                   and im.class = I_sch_crit_rec.class
                                                   and im.subclass = I_sch_crit_rec.subclass))
                                 and (I_sch_crit_rec.item is NULL
                                     or exists (select 'x'
                                                  from v_ordsku os
                                                 where os.order_no = oh.order_no
                                                   and os.item = I_sch_crit_rec.item))
                                 and (I_sch_crit_rec.item_desc is NULL
                                     or exists (select 'x'
                                                  from v_ordsku os,
                                                       v_item_master im
                                                 where os.order_no = oh.order_no
                                                   and os.item = im.item
                                                   and upper(im.item_desc) LIKE '%'||upper(I_sch_crit_rec.item_desc)||'%'))
                                 and (I_sch_crit_rec.orig_ind is NULL
                                     or oh.orig_ind = I_sch_crit_rec.orig_ind)
                                 and (I_sch_crit_rec.import_order_ind is NULL
                                     or oh.import_order_ind = I_sch_crit_rec.import_order_ind)
                                 and (I_sch_crit_rec.order_type is NULL
                                     or oh.order_type = I_sch_crit_rec.order_type)
                                 and (I_sch_crit_rec.import_country_id is NULL
                                     or oh.import_country_id = I_sch_crit_rec.import_country_id)
                                 and (I_sch_crit_rec.delivery_before_date is NULL
                                     or oh.not_after_date <= I_sch_crit_rec.delivery_before_date)
                                 and (I_sch_crit_rec.delivery_after_date is NULL
                                     or oh.not_before_date >= I_sch_crit_rec.delivery_after_date)
                                 and (I_sch_crit_rec.create_before_date is NULL
                                     or oh.written_date <= I_sch_crit_rec.create_before_date)
                                 and (I_sch_crit_rec.create_after_date is NULL
                                     or oh.written_date >= I_sch_crit_rec.create_after_date)
                                 and (I_sch_crit_rec.vendor_order_no is NULL
                                     or upper(oh.vendor_order_no) = upper(I_sch_crit_rec.vendor_order_no))
                                 and (I_sch_crit_rec.master_po_no is NULL
                                     or oh.master_po_no = I_sch_crit_rec.master_po_no)
                                 and (I_sch_crit_rec.loc_type is NULL
                                     or oh.loc_type = I_sch_crit_rec.loc_type
                                     or (oh.loc_type is NULL
                                        and exists (select 'x'
                                                      from v_ordloc ol
                                                     where ol.order_no = oh.order_no
                                                       and ol.loc_type = I_sch_crit_rec.loc_type)))
                                 and (I_sch_crit_rec.location is NULL
                                     or oh.location = I_sch_crit_rec.location
                                     or (oh.location is NULL
                                        and exists (select 'x'
                                                     from v_ordloc ol
                                                    where ol.order_no = oh.order_no
                                                      and ol.location = I_sch_crit_rec.location)));
      O_order_count := SQL%ROWCOUNT;
   end if;

   if I_source = 'STG' then
      insert into svc_po_search_temp(process_id,
                                     order_no)
                                with order_table as (select order_no,
                                                            status,
                                                            supplier,
                                                            buyer,
                                                            dept,
                                                            NULL as class,
                                                            NULL as subclass,
                                                            NULL as item,
                                                            NULL as item_desc,
                                                            process_id,
                                                            orig_ind,
                                                            import_order_ind,
                                                            order_type,
                                                            import_country_id,
                                                            not_after_date,
                                                            not_before_date,
                                                            written_date,
                                                            last_upd_id,
                                                            next_upd_id,
                                                            vendor_order_no,
                                                            master_po_no,
                                                            loc_type,
                                                            location
                                                       from svc_ordhead
                                                      union all
                                                     select order_no,
                                                            NULL as status,
                                                            NULL as supplier,
                                                            NULL as buyer,
                                                            NULL as dept,
                                                            NULL as class,
                                                            NULL as subclass,
                                                            item as item,
                                                            item_desc as item_desc,
                                                            process_id,
                                                            NULL as orig_ind,
                                                            NULL as import_order_ind,
                                                            NULL as order_type,
                                                            NULL as import_country_id,
                                                            NULL as not_after_date,
                                                            NULL as not_before_date,
                                                            NULL as written_date,
                                                            NULL as last_upd_id,
                                                            NULL as next_upd_id,
                                                            NULL as vendor_order_no,
                                                            NULL as master_po_no,
                                                            loc_type as loc_type,
                                                            location as location
                                                       from svc_orddetail
                                                      union all
                                                     select order_no,
                                                            NULL as status,
                                                            NULL as supplier,
                                                            NULL as buyer,
                                                            NULL as dept,
                                                            NULL as class,
                                                            NULL as subclass,
                                                            NULL as item,
                                                            NULL as item_desc,
                                                            process_id,
                                                            NULL as orig_ind,
                                                            NULL as import_order_ind,
                                                            NULL as order_type,
                                                            NULL as import_country_id,
                                                            NULL as not_after_date,
                                                            NULL as not_before_date,
                                                            NULL as written_date,
                                                            NULL as last_upd_id,
                                                            NULL as next_upd_id,
                                                            NULL as vendor_order_no,
                                                            NULL as master_po_no,
                                                            NULL as loc_type,
                                                            NULL as location
                                                       from svc_ordlc
                                                      union all
                                                     select order_no,
                                                            NULL as status,
                                                            NULL as supplier,
                                                            NULL as buyer,
                                                            NULL as dept,
                                                            NULL as class,
                                                            NULL as subclass,
                                                            item as item,
                                                            NULL as item_desc,
                                                            process_id,
                                                            NULL as orig_ind,
                                                            NULL as import_order_ind,
                                                            NULL as order_type,
                                                            NULL as import_country_id,
                                                            NULL as not_after_date,
                                                            NULL as not_before_date,
                                                            NULL as written_date,
                                                            NULL as last_upd_id,
                                                            NULL as next_upd_id,
                                                            NULL as vendor_order_no,
                                                            NULL as master_po_no,
                                                            loc_type as loc_type,
                                                            location as location
                                                       from svc_ordloc_exp
                                                      union all
                                                     select order_no,
                                                            NULL as status,
                                                            NULL as supplier,
                                                            NULL as buyer,
                                                            NULL as dept,
                                                            NULL as class,
                                                            NULL as subclass,
                                                            item as item,
                                                            NULL as item_desc,
                                                            process_id,
                                                            NULL as orig_ind,
                                                            NULL as import_order_ind,
                                                            NULL as order_type,
                                                            NULL as import_country_id,
                                                            NULL as not_after_date,
                                                            NULL as not_before_date,
                                                            NULL as written_date,
                                                            NULL as last_upd_id,
                                                            NULL as next_upd_id,
                                                            NULL as vendor_order_no,
                                                            NULL as master_po_no,
                                                            NULL as loc_type,
                                                            NULL as location
                                                       from svc_ordsku_hts
                                                      union all
                                                     select order_no,
                                                            NULL as status,
                                                            NULL as supplier,
                                                            NULL as buyer,
                                                            NULL as dept,
                                                            NULL as class,
                                                            NULL as subclass,
                                                            NULL as item,
                                                            NULL as item_desc,
                                                            process_id,
                                                            NULL as orig_ind,
                                                            NULL as import_order_ind,
                                                            NULL as order_type,
                                                            NULL as import_country_id,
                                                            NULL as not_after_date,
                                                            NULL as not_before_date,
                                                            NULL as written_date,
                                                            NULL as last_upd_id,
                                                            NULL as next_upd_id,
                                                            NULL as vendor_order_no,
                                                            NULL as master_po_no,
                                                            NULL as loc_type,
                                                            NULL as location
                                                       from svc_ordsku_hts_assess
                                                      union all
                                                     select to_number(key_val_pairs_pkg.get_attr(keys_col,'ORDER_NO')) as order_no,
                                                            NULL as status,
                                                            NULL as supplier,
                                                            NULL as buyer,
                                                            NULL as dept,
                                                            NULL as class,
                                                            NULL as subclass,
                                                            NULL as item,
                                                            NULL as item_desc,
                                                            process_id,
                                                            NULL as orig_ind,
                                                            NULL as import_order_ind,
                                                            NULL as order_type,
                                                            NULL as import_country_id,
                                                            NULL as not_after_date,
                                                            NULL as not_before_date,
                                                            NULL as written_date,
                                                            NULL as last_upd_id,
                                                            NULL as next_upd_id,
                                                            NULL as vendor_order_no,
                                                            NULL as master_po_no,
                                                            NULL as loc_type,
                                                            NULL as location
                                                       from svc_cfa_ext)
                              select distinct I_process_id,
                                     oh.order_no
                                from order_table oh
                               where (I_sch_crit_rec.order_no is NULL
                                     or oh.order_no = I_sch_crit_rec.order_no)
                                 and (I_sch_crit_rec.status is NULL
                                     or oh.status = I_sch_crit_rec.status)
                                 and (I_sch_crit_rec.supplier is NULL
                                     or oh.supplier = I_sch_crit_rec.supplier)
                                 and (I_sch_crit_rec.buyer is NULL
                                     or oh.buyer = I_sch_crit_rec.buyer)
                                 and (I_sch_crit_rec.dept is NULL
                                     or oh.dept = I_sch_crit_rec.dept
                                     or exists (select 'x'
                                                  from v_item_master im
                                                 where oh.item = im.item
                                                   and im.dept = I_sch_crit_rec.dept))
                                 and (I_sch_crit_rec.class is NULL
                                     or exists (select 'x'
                                                  from v_item_master im
                                                 where oh.item = im.item
                                                   and im.dept = I_sch_crit_rec.dept
                                                   and im.class = I_sch_crit_rec.class))
                                 and (I_sch_crit_rec.subclass is NULL
                                     or exists (select 'x'
                                                  from v_item_master im
                                                 where oh.item = im.item
                                                   and im.dept = I_sch_crit_rec.dept
                                                   and im.class = I_sch_crit_rec.class
                                                   and im.subclass = I_sch_crit_rec.subclass))
                                 and (I_sch_crit_rec.item is NULL
                                     or exists (select 'x'
                                                  from v_item_master im
                                                 where oh.item = im.item
                                                   and oh.item = I_sch_crit_rec.item))
                                 and (I_sch_crit_rec.item_desc is NULL
                                      or I_sch_crit_rec.item is NOT NULL
                                      or (I_sch_crit_rec.item is NULL
                                          and exists (select 'x'
                                                        from v_item_master im
                                                       where oh.item = im.item
                                                         and upper(oh.item_desc) LIKE '%'||upper(I_sch_crit_rec.item_desc)||'%'))
                                     )
                                 and (I_sch_crit_rec.process_id is NULL
                                     or oh.process_id = I_sch_crit_rec.process_id)
                                 and (I_sch_crit_rec.orig_ind is NULL
                                     or oh.orig_ind = I_sch_crit_rec.orig_ind)
                                 and (I_sch_crit_rec.import_order_ind is NULL
                                     or oh.import_order_ind = I_sch_crit_rec.import_order_ind)
                                 and (I_sch_crit_rec.order_type is NULL
                                     or oh.order_type = I_sch_crit_rec.order_type)
                                 and (I_sch_crit_rec.import_country_id is NULL
                                     or oh.import_country_id = I_sch_crit_rec.import_country_id)
                                 and (I_sch_crit_rec.delivery_before_date is NULL
                                     or oh.not_after_date <= I_sch_crit_rec.delivery_before_date)
                                 and (I_sch_crit_rec.delivery_after_date is NULL
                                     or oh.not_before_date >= I_sch_crit_rec.delivery_after_date)
                                 and (I_sch_crit_rec.create_before_date is NULL
                                     or oh.written_date <= I_sch_crit_rec.create_before_date)
                                 and (I_sch_crit_rec.create_after_date is NULL
                                     or oh.written_date >= I_sch_crit_rec.create_after_date)
                                 and (I_sch_crit_rec.last_upd_id is NULL
                                     or oh.last_upd_id = I_sch_crit_rec.last_upd_id)
                                 and (I_sch_crit_rec.next_upd_id is NULL
                                     or oh.next_upd_id = I_sch_crit_rec.next_upd_id)
                                 and (I_sch_crit_rec.vendor_order_no is NULL
                                     or upper(oh.vendor_order_no) = upper(I_sch_crit_rec.vendor_order_no))
                                 and (I_sch_crit_rec.master_po_no is NULL
                                     or oh.master_po_no = I_sch_crit_rec.master_po_no)
                                 and (I_sch_crit_rec.loc_type is NULL
                                     or oh.loc_type = I_sch_crit_rec.loc_type)
                                 and (I_sch_crit_rec.location is NULL
                                     or oh.location = I_sch_crit_rec.location);

      O_order_count := SQL%ROWCOUNT;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END SEARCH_POS;
-------------------------------------------------------------------------------------------------------
FUNCTION INIT_PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                      I_process_desc    IN       SVC_PROCESS_TRACKER.PROCESS_DESC%TYPE,
                      I_template_key    IN       SVC_PROCESS_TRACKER.TEMPLATE_KEY%TYPE,
                      I_action_type     IN       SVC_PROCESS_TRACKER.ACTION_TYPE%TYPE,
                      I_source          IN       SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
                      I_destination     IN       SVC_PROCESS_TRACKER.PROCESS_DESTINATION%TYPE,
                      I_rms_async_id    IN       SVC_PROCESS_TRACKER.RMS_ASYNC_ID%TYPE,
                      I_file_id         IN       SVC_PROCESS_TRACKER.FILE_ID%TYPE,
                      I_file_path       IN       SVC_PROCESS_TRACKER.FILE_PATH%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(75) := 'PO_INDUCT_SQL.INIT_PROCESS';

BEGIN
   insert into svc_process_tracker(process_id,
                                   process_desc,
                                   template_key,
                                   action_type,
                                   process_source,
                                   process_destination,
                                   status,
                                   user_id,
                                   rms_async_id,
                                   file_id,
                                   file_path)
                            values(I_process_id,
                                   I_process_desc,
                                   I_template_key,
                                   I_action_type,
                                   I_source,
                                   I_destination,
                                   'N',
                                   get_user,
                                   I_rms_async_id,
                                   I_file_id,
                                   I_file_path);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END INIT_PROCESS;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER,
                     O_error_count     OUT      NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(255) := 'PO_INDUCT_SQL.PROCESS_S9T';
   L_file             s9t_file;
   L_sheets           s9t_pkg.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
   DML_ERRORS         EXCEPTION;
   PRAGMA             EXCEPTION_INIT(DML_ERRORS, -24381);
   L_error_code       NUMBER;

   cursor C_GET_NLS_DATE is
      select value
        from v$nls_parameters
       where parameter = 'NLS_DATE_FORMAT';

BEGIN
   commit;
   S9T_PKG.ODS2OBJ(I_file_id);
   l_file            := s9t_pkg.get_obj(I_file_id,FALSE);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   LP_svc_po_tab     := NEW svc_po_tab_typ();
   LP_error_exists   := 'N';
   template_key      := L_file.template_key;

   open C_GET_NLS_DATE;
   fetch C_GET_NLS_DATE into LP_format;
   close C_GET_NLS_DATE;
   if LP_format is NULL then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'NLS_DATE_FORMAT_REQUIRED');
   else
      if S9T_PKG.CODE2DESC(O_error_message,
                           'POS9T',
                           L_file,
                           TRUE) = FALSE then
         return FALSE;
      end if;

      S9T_PKG.SAVE_OBJ(L_file);
      if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
         WRITE_S9T_ERROR(I_file_id,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         'S9T_INVALID_TEMPLATE');
      else
         POPULATE_NAMES(I_file_id);

         sheet_name_trans := S9T_PKG.SHEET_TRANS(template_key,
                                                 L_file.user_lang);
         PROCESS_S9T_OHE(I_file_id,
                         I_process_id);

         PROCESS_S9T_ODT(I_file_id,
                         I_process_id);

         PROCESS_S9T_ORD(I_file_id,
                         I_process_id);

         PROCESS_S9T_ORE(I_file_id,
                         I_process_id);

         PROCESS_S9T_ORH(I_file_id,
                         I_process_id);

         PROCESS_S9T_OHA(I_file_id,
                         I_process_id);

         if PROCESS_S9T_CFA(O_error_message,
                            I_file_id,
                            I_process_id,
                            template_key) = FALSE then
            return FALSE;
         end if;

         if LP_svc_po_tab is NOT NULL and LP_svc_po_tab.COUNT > 0 then
            BEGIN
               FORALL i IN 1..LP_svc_po_tab.COUNT() SAVE EXCEPTIONS
                  insert into svc_po_del
                       values LP_svc_po_tab(i);
            EXCEPTION
               when DML_ERRORS then
                  FOR i in 1..SQL%BULK_EXCEPTIONS.COUNT LOOP
                     L_error_code := sql%bulk_exceptions(i).error_code;
                     if L_error_code = 1 then
                        continue;
                     else
                        WRITE_S9T_ERROR(I_file_id,
                                        NULL,
                                        NULL,
                                        NULL,
                                        L_error_code,
                                        NULL);
                     end if;
                  END LOOP;
            END;
            if PROCESS_CASCADE_DELETE(O_error_message) = FALSE then
               return FALSE;
            end if;
            LP_svc_po_tab := NEW svc_po_tab_typ();
         end if;

         FOR i in 1..s9t_pkg.LP_s9t_errors_tab.COUNT LOOP
            LP_s9t_errors_tab.EXTEND;
            LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT) :=  S9T_PKG.LP_s9t_errors_tab(i);
         END LOOP;
         O_error_count := LP_s9t_errors_tab.COUNT();
         if O_error_count = 0 or LP_error_exists = 'N' then
            if CORESVC_PO.COMPLETE_SVC_PO(O_error_message,
                                          I_process_id) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
   end if;

   O_error_count := LP_s9t_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count > 0 then
      if LP_error_exists = 'Y' then
         update svc_process_tracker
            set status     = P_ERROR,
                file_id    = I_file_id
          where process_id = I_process_id;
      else
         update svc_process_tracker
            set status     = P_WARNING,
                file_id    = I_file_id
          where process_id = I_process_id;
      end if;
   else
      update svc_process_tracker
         set status     = P_SUCCESS,
             file_id    = I_file_id
       where process_id = I_process_id
         and process_destination = 'STG';
   end if;

   return TRUE;

EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.COUNT();
      FORALL i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.COUNT();
      FORALL i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
-------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_UPLD_FROM_STG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN BOOLEAN IS
  L_program   VARCHAR2(75) := 'PO_INDUCT_SQL.PREPARE_UPLD_FROM_STG';

BEGIN
   --svc_ordhead
   merge into svc_ordhead svc
      using (select temp.process_id,
                    s.rowid   AS   rid,
                    rownum    AS   row_seq
               from svc_po_search_temp temp,
                    svc_ordhead s
              where temp.process_id = I_process_id
                and temp.order_no = s.order_no) sq
      on (svc.rowid = sq.rid)
      when matched then
         update set process_id = sq.process_id,
         process$status        = DECODE(process$status,'E','N',process$status),
         row_seq               = sq.row_seq;

logger.log('PO_INDUCT_SQL.PREPARE_UPLD_FROM_STG for PID: '||I_process_id||' SVC_ohe rows merged: '||sql%rowcount);

   --svc_orddetail
   merge into svc_orddetail svc
      using (select temp.process_id,
                    s.rowid   AS   rid,
                    rownum    AS   row_seq
               from svc_po_search_temp temp,
                    svc_orddetail s
              where temp.process_id = I_process_id
                and temp.order_no = s.order_no) sq
      on (svc.rowid = sq.rid)
      when matched then
         update set process_id = sq.process_id,
         process$status        = DECODE(process$status,'E','N',process$status),
         row_seq               = sq.row_seq;

   --svc_ordlc
   merge into svc_ordlc svc
      using (select temp.process_id,
                    s.rowid   AS   rid,
                    rownum    AS   row_seq
               from svc_po_search_temp temp,
                    svc_ordlc s
              where temp.process_id = I_process_id
                and temp.order_no = s.order_no) sq
      on (svc.rowid = sq.rid)
      when matched then
         update set process_id     = sq.process_id,
                    process$status = DECODE(process$status,'E','N',process$status),
                    row_seq        = sq.row_seq;

   --svc_ordloc_exp
   merge into svc_ordloc_exp svc
      using (select temp.process_id,
                    s.rowid   AS   rid,
                    rownum    AS   row_seq
               from svc_po_search_temp temp,
                    svc_ordloc_exp s
              where temp.process_id = I_process_id
                and temp.order_no = s.order_no) sq
      on (svc.rowid = sq.rid)
      when matched then
         update set process_id     = sq.process_id,
                    process$status = DECODE(process$status,'E','N',process$status),
                    row_seq        = sq.row_seq;

   --svc_ordsku_hts
   merge into svc_ordsku_hts svc
      using (select temp.process_id,
                    s.rowid   AS   rid,
                    rownum    AS   row_seq
               from svc_po_search_temp temp,
                    svc_ordsku_hts s
              where temp.process_id = I_process_id
                and temp.order_no = s.order_no) sq
      on (svc.rowid = sq.rid)
      when matched then
         update set process_id     = sq.process_id,
                    process$status = DECODE(process$status,'E','N',process$status),
                    row_seq        = sq.row_seq;

   --svc_ordsku_hts_assess
      merge into svc_ordsku_hts_assess svc
      using (select temp.process_id,
                    s.rowid   AS   rid,
                    rownum    AS   row_seq
               from svc_po_search_temp temp,
                    svc_ordsku_hts_assess s
              where temp.process_id = I_process_id
                and temp.order_no = s.order_no) sq
      on (svc.rowid = sq.rid)
      when matched then
         update set process_id     = sq.process_id,
                    process$status = DECODE(process$status,'E','N',process$status),
                    row_seq        = sq.row_seq;

     --svc_cfa_ext
     merge into svc_cfa_ext svc
        using (select temp.process_id,
                      s.rowid   AS   rid,
                      rownum    AS   row_seq
                 from svc_po_search_temp temp,
                      svc_cfa_ext s
                where temp.process_id = I_process_id
                  and to_char(temp.order_no) = (key_val_pairs_pkg.get_attr(s.keys_col,'ORDER_NO'))) sq
        on (svc.rowid = sq.rid)
        when matched then
           update set process_id     = sq.process_id,
                      process$status = DECODE(process$status,'E','N',process$status),
                      row_seq        = sq.row_seq;

   if CORESVC_PO.COMPLETE_SVC_PO(O_error_message,
                                 I_process_id) = FALSE then
      return FALSE;
   end if;

   if PO_INDUCT_SQL.DELETE_SEARCH_POS(O_error_message,
                                      I_process_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PREPARE_UPLD_FROM_STG;
-------------------------------------------------------------------------------------------------------
FUNCTION PREPARE_UPLD_FROM_EXT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN BOOLEAN IS
  L_program   VARCHAR2(75) := 'PO_INDUCT_SQL.PREPARE_UPLD_FROM_EXT';

BEGIN
   --svc_ordhead
   merge into svc_ordhead svc
      using (select temp.process_id,
                    s.rowid   AS   rid,
                    rownum    AS   row_seq
               from svc_po_search_temp temp,
                    svc_ordhead s
              where temp.process_id = I_process_id
                and temp.order_no = s.order_no) sq
      on (svc.rowid = sq.rid)
      when matched then
         update set process_id = sq.process_id,
         process$status        = DECODE(process$status,'E','N',process$status),
         row_seq               = sq.row_seq;

   --svc_orddetail
   merge into svc_orddetail svc
      using (select temp.process_id,
                    s.rowid   AS   rid,
                    rownum    AS   row_seq
               from svc_po_search_temp temp,
                    svc_orddetail s
              where temp.process_id = I_process_id
                and temp.order_no = s.order_no) sq
      on (svc.rowid = sq.rid)
      when matched then
         update set process_id = sq.process_id,
         process$status        = DECODE(process$status,'E','N',process$status),
         row_seq               = sq.row_seq;

   if CORESVC_PO.COMPLETE_SVC_PO(O_error_message,
                                 I_process_id) = FALSE then
      return FALSE;
   end if;

   if PO_INDUCT_SQL.DELETE_SEARCH_POS(O_error_message,
                                      I_process_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PREPARE_UPLD_FROM_EXT;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_CONFIG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_rec             IN OUT   PO_INDUCT_CONFIG%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(75) := 'PO_INDUCT_SQL.GET_CONFIG';

BEGIN
   select *
     into O_rec
     from po_induct_config;

  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END GET_CONFIG;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
  RETURN VARCHAR2 IS
BEGIN
  if SHEET_NAME_TRANS.EXISTS(I_sheet_name) then
     return sheet_name_trans(I_sheet_name);
  else
    return NULL;
  end if;
END GET_SHEET_NAME_TRANS;
-------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id    IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet      IN   VARCHAR2,
                          I_row_seq    IN   NUMBER,
                          I_col        IN   VARCHAR2,
                          I_sqlcode    IN   NUMBER,
                          I_sqlerrm    IN   VARCHAR2,
                          I_err_type   IN   VARCHAR2 DEFAULT NULL) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).file_id              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_seq_no         := s9t_errors_seq.nextval;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).template_key         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).wksht_key            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).column_key           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).row_seq              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_id            := get_user;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_datetime      := sysdate;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_id       := get_user;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_datetime := sysdate;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_key            := (case
                                                                            when I_sqlcode is NULL then
                                                                               I_sqlerrm
                                                                            else
                                                                               'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                            end);
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_type           := I_err_type;

   if LP_error_exists = 'N' then
      if I_err_type = 'E' or I_err_type is NULL then
         LP_error_exists := 'Y';
      end if;
   end if;

END WRITE_S9T_ERROR;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   NUMBER) IS
   L_sheets       S9T_PKG.NAMES_MAP_TYP;
   OHA_cols       S9T_PKG.NAMES_MAP_TYP;
   ORH_cols       S9T_PKG.NAMES_MAP_TYP;
   ORE_cols       S9T_PKG.NAMES_MAP_TYP;
   ORD_cols       S9T_PKG.NAMES_MAP_TYP;
   OHE_cols       S9T_PKG.NAMES_MAP_TYP;
   ODT_cols       S9T_PKG.NAMES_MAP_TYP;
   OHE_CFA_cols   S9T_PKG.NAMES_MAP_TYP;

   FUNCTION GET_COL_IDX(I_names     IN   S9T_PKG.NAMES_MAP_TYP,
                        I_col_key   IN   VARCHAR2)
   RETURN NUMBER IS

   BEGIN
      if I_names.exists(I_col_key) then
         return I_names(I_col_key);
      else
         return NULL;
      end if;
   END GET_COL_IDX;

BEGIN
   L_sheets := S9T_PKG.GET_SHEET_NAMES(I_file_id);
   --ordsku_hts_assess
   OHA_cols                 := S9T_PKG.GET_COL_NAMES(I_file_id,OHA_sheet);
   OHA$action               := GET_COL_IDX(OHA_cols,'ACTION');
   OHA$est_assess_value     := GET_COL_IDX(OHA_cols,'EST_ASSESS_VALUE');
   OHA$nom_flag_1           := GET_COL_IDX(OHA_cols,'NOM_FLAG_1');
   OHA$nom_flag_2           := GET_COL_IDX(OHA_cols,'NOM_FLAG_2');
   OHA$nom_flag_3           := GET_COL_IDX(OHA_cols,'NOM_FLAG_3');
   OHA$nom_flag_4           := GET_COL_IDX(OHA_cols,'NOM_FLAG_4');
   OHA$nom_flag_5           := GET_COL_IDX(OHA_cols,'NOM_FLAG_5');
   OHA$order_no             := GET_COL_IDX(OHA_cols,'ORDER_NO');
   OHA$item                 := GET_COL_IDX(OHA_cols,'ITEM');
   OHA$pack_item            := GET_COL_IDX(OHA_cols,'PACK_ITEM');
   OHA$hts                  := GET_COL_IDX(OHA_cols,'HTS');
   OHA$comp_id              := GET_COL_IDX(OHA_cols,'COMP_ID');
   OHA$cvb_code             := GET_COL_IDX(OHA_cols,'CVB_CODE');
   OHA$comp_rate            := GET_COL_IDX(OHA_cols,'COMP_RATE');
   OHA$per_count            := GET_COL_IDX(OHA_cols,'PER_COUNT');
   OHA$per_count_uom        := GET_COL_IDX(OHA_cols,'PER_COUNT_UOM');
   --ordsku_hts
   ORH_cols                 := S9T_PKG.GET_COL_NAMES(I_file_id,ORH_sheet);
   ORH$action               := GET_COL_IDX(ORH_cols,'ACTION');
   ORH$order_no             := GET_COL_IDX(ORH_cols,'ORDER_NO');
   ORH$item                 := GET_COL_IDX(ORH_cols,'ITEM');
   ORH$pack_item            := GET_COL_IDX(ORH_cols,'PACK_ITEM');
   ORH$hts                  := GET_COL_IDX(ORH_cols,'HTS');
   ORH$import_country_id    := GET_COL_IDX(ORH_cols,'IMPORT_COUNTRY_ID');
   ORH$effect_from          := GET_COL_IDX(ORH_cols,'EFFECT_FROM');
   ORH$effect_to            := GET_COL_IDX(ORH_cols,'EFFECT_TO');
   ORH$status               := GET_COL_IDX(ORH_cols,'STATUS');
   ORH$origin_country_id    := GET_COL_IDX(ORH_cols,'ORIGIN_COUNTRY_ID');
   --ordloc_exp
   ORE_cols                 := S9T_PKG.GET_COL_NAMES(I_file_id,ORE_sheet);
   ORE$action               := GET_COL_IDX(ORE_cols,'ACTION');
   ORE$order_no             := GET_COL_IDX(ORE_cols,'ORDER_NO');
   ORE$item                 := GET_COL_IDX(ORE_cols,'ITEM');
   ORE$pack_item            := GET_COL_IDX(ORE_cols,'PACK_ITEM');
   ORE$location             := GET_COL_IDX(ORE_cols,'LOCATION');
   ORE$loc_type             := GET_COL_IDX(ORE_cols,'LOC_TYPE');
   ORE$comp_id              := GET_COL_IDX(ORE_cols,'COMP_ID');
   ORE$cvb_code             := GET_COL_IDX(ORE_cols,'CVB_CODE');
   ORE$cost_basis           := GET_COL_IDX(ORE_cols,'COST_BASIS');
   ORE$comp_rate            := GET_COL_IDX(ORE_cols,'COMP_RATE');
   ORE$comp_currency        := GET_COL_IDX(ORE_cols,'COMP_CURRENCY');
   ORE$exchange_rate        := GET_COL_IDX(ORE_cols,'EXCHANGE_RATE');
   ORE$per_count            := GET_COL_IDX(ORE_cols,'PER_COUNT');
   ORE$per_count_uom        := GET_COL_IDX(ORE_cols,'PER_COUNT_UOM');
   ORE$est_exp_value        := GET_COL_IDX(ORE_cols,'EST_EXP_VALUE');
   ORE$nom_flag_1           := GET_COL_IDX(ORE_cols,'NOM_FLAG_1');
   ORE$nom_flag_2           := GET_COL_IDX(ORE_cols,'NOM_FLAG_2');
   ORE$nom_flag_3           := GET_COL_IDX(ORE_cols,'NOM_FLAG_3');
   ORE$nom_flag_4           := GET_COL_IDX(ORE_cols,'NOM_FLAG_4');
   ORE$nom_flag_5           := GET_COL_IDX(ORE_cols,'NOM_FLAG_5');
   ORE$origin               := GET_COL_IDX(ORE_cols,'ORIGIN');
   ORE$defaulted_from       := GET_COL_IDX(ORE_cols,'DEFAULTED_FROM');
   ORE$key_value_1          := GET_COL_IDX(ORE_cols,'KEY_VALUE_1');
   ORE$key_value_2          := GET_COL_IDX(ORE_cols,'KEY_VALUE_2');
   --ordlc
   ORD_cols                 := S9T_PKG.GET_COL_NAMES(I_file_id,ORD_sheet);
   ORD$action               := GET_COL_IDX(ORD_cols,'ACTION');
   ORD$order_no             := GET_COL_IDX(ORD_cols,'ORDER_NO');
   ORD$lc_ref_id            := GET_COL_IDX(ORD_cols,'LC_REF_ID');
   ORD$lc_group_id          := GET_COL_IDX(ORD_cols,'LC_GROUP_ID');
   ORD$applicant            := GET_COL_IDX(ORD_cols,'APPLICANT');
   ORD$beneficiary          := GET_COL_IDX(ORD_cols,'BENEFICIARY');
   ORD$merch_desc           := GET_COL_IDX(ORD_cols,'MERCH_DESC');
   ORD$transshipment_ind    := GET_COL_IDX(ORD_cols,'TRANSSHIPMENT_IND');
   ORD$partial_shipment_ind := GET_COL_IDX(ORD_cols,'PARTIAL_SHIPMENT_IND');
   ORD$lc_ind               := GET_COL_IDX(ORD_cols,'LC_IND');
   --ordhead
   OHE_cols                 := S9T_PKG.GET_COL_NAMES(I_file_id,OHE_sheet);
   OHE$action               := GET_COL_IDX(OHE_cols,'ACTION');
   OHE$order_no             := GET_COL_IDX(OHE_cols,'ORDER_NO');
   OHE$order_type           := GET_COL_IDX(OHE_cols,'ORDER_TYPE');
   OHE$dept                 := GET_COL_IDX(OHE_cols,'DEPT');
   OHE$buyer                := GET_COL_IDX(OHE_cols,'BUYER');
   OHE$supplier             := GET_COL_IDX(OHE_cols,'SUPPLIER');
   OHE$loc_type             := GET_COL_IDX(OHE_cols,'LOC_TYPE');
   OHE$location             := GET_COL_IDX(OHE_cols,'LOCATION');
   OHE$promotion            := GET_COL_IDX(OHE_cols,'PROMOTION');
   OHE$qc_ind               := GET_COL_IDX(OHE_cols,'QC_IND');
   OHE$written_date         := GET_COL_IDX(OHE_cols,'WRITTEN_DATE');
   OHE$not_before_date      := GET_COL_IDX(OHE_cols,'NOT_BEFORE_DATE');
   OHE$not_after_date       := GET_COL_IDX(OHE_cols,'NOT_AFTER_DATE');
   OHE$otb_eow_date         := GET_COL_IDX(OHE_cols,'OTB_EOW_DATE');
   OHE$earliest_ship_date   := GET_COL_IDX(OHE_cols,'EARLIEST_SHIP_DATE');
   OHE$latest_ship_date     := GET_COL_IDX(OHE_cols,'LATEST_SHIP_DATE');
   OHE$terms                := GET_COL_IDX(OHE_cols,'TERMS');
   OHE$freight_terms        := GET_COL_IDX(OHE_cols,'FREIGHT_TERMS');
   OHE$orig_ind             := GET_COL_IDX(OHE_cols,'ORIG_IND');
   OHE$payment_method       := GET_COL_IDX(OHE_cols,'PAYMENT_METHOD');
   OHE$backhaul_type        := GET_COL_IDX(OHE_cols,'BACKHAUL_TYPE');
   OHE$backhaul_allowance   := GET_COL_IDX(OHE_cols,'BACKHAUL_ALLOWANCE');
   OHE$ship_method          := GET_COL_IDX(OHE_cols,'SHIP_METHOD');
   OHE$purchase_type        := GET_COL_IDX(OHE_cols,'PURCHASE_TYPE');
   OHE$status               := GET_COL_IDX(OHE_cols,'STATUS');
   OHE$ship_pay_method      := GET_COL_IDX(OHE_cols,'SHIP_PAY_METHOD');
   OHE$fob_trans_res        := GET_COL_IDX(OHE_cols,'FOB_TRANS_RES');
   OHE$fob_trans_res_desc   := GET_COL_IDX(OHE_cols,'FOB_TRANS_RES_DESC');
   OHE$fob_title_pass       := GET_COL_IDX(OHE_cols,'FOB_TITLE_PASS');
   OHE$fob_title_pass_desc  := GET_COL_IDX(OHE_cols,'FOB_TITLE_PASS_DESC');
   OHE$edi_po_ind           := GET_COL_IDX(OHE_cols,'EDI_PO_IND');
   OHE$import_order_ind     := GET_COL_IDX(OHE_cols,'IMPORT_ORDER_IND');
   OHE$import_country_id    := GET_COL_IDX(OHE_cols,'IMPORT_COUNTRY_ID');
   OHE$include_on_order_ind := GET_COL_IDX(OHE_cols,'INCLUDE_ON_ORDER_IND');
   OHE$vendor_order_no      := GET_COL_IDX(OHE_cols,'VENDOR_ORDER_NO');
   OHE$exchange_rate        := GET_COL_IDX(OHE_cols,'EXCHANGE_RATE');
   OHE$factory              := GET_COL_IDX(OHE_cols,'FACTORY');
   OHE$agent                := GET_COL_IDX(OHE_cols,'AGENT');
   OHE$discharge_port       := GET_COL_IDX(OHE_cols,'DISCHARGE_PORT');
   OHE$lading_port          := GET_COL_IDX(OHE_cols,'LADING_PORT');
   OHE$freight_contract_no  := GET_COL_IDX(OHE_cols,'FREIGHT_CONTRACT_NO');
   OHE$po_type              := GET_COL_IDX(OHE_cols,'PO_TYPE');
   OHE$pre_mark_ind         := GET_COL_IDX(OHE_cols,'PRE_MARK_IND');
   OHE$currency_code        := GET_COL_IDX(OHE_cols,'CURRENCY_CODE');
   OHE$reject_code          := GET_COL_IDX(OHE_cols,'REJECT_CODE');
   OHE$contract_no          := GET_COL_IDX(OHE_cols,'CONTRACT_NO');
   OHE$pickup_loc           := GET_COL_IDX(OHE_cols,'PICKUP_LOC');
   OHE$pickup_no            := GET_COL_IDX(OHE_cols,'PICKUP_NO');
   OHE$pickup_date          := GET_COL_IDX(OHE_cols,'PICKUP_DATE');
   OHE$comment_desc         := GET_COL_IDX(OHE_cols,'COMMENT_DESC');
   OHE$partner_type_1       := GET_COL_IDX(OHE_cols,'PARTNER_TYPE_1');
   OHE$partner1             := GET_COL_IDX(OHE_cols,'PARTNER1');
   OHE$partner_type_2       := GET_COL_IDX(OHE_cols,'PARTNER_TYPE_2');
   OHE$partner2             := GET_COL_IDX(OHE_cols,'PARTNER2');
   OHE$partner_type_3       := GET_COL_IDX(OHE_cols,'PARTNER_TYPE_3');
   OHE$partner3             := GET_COL_IDX(OHE_cols,'PARTNER3');
   OHE$import_type          := GET_COL_IDX(OHE_cols,'IMPORT_TYPE');
   OHE$import_id            := GET_COL_IDX(OHE_cols,'IMPORT_ID');
   OHE$clearing_zone_id     := GET_COL_IDX(OHE_cols,'CLEARING_ZONE_ID');
   OHE$routing_loc_id       := GET_COL_IDX(OHE_cols,'ROUTING_LOC_ID');
   OHE$ext_ref_no           := GET_COL_IDX(OHE_cols,'EXT_REF_NO');
   OHE$master_po_no         := GET_COL_IDX(OHE_cols,'MASTER_PO_NO');
   OHE$re_approve_ind       := GET_COL_IDX(OHE_cols,'RE_APPROVE_IND');
   OHE$last_upd_id          := GET_COL_IDX(OHE_cols,'LAST_UPD_ID');
   OHE$next_upd_id          := GET_COL_IDX(OHE_cols,'NEXT_UPD_ID');
   OHE$last_upd_datetime    := GET_COL_IDX(OHE_cols,'LAST_UPD_DATETIME');
   --orddetail
   ODT_cols                 := S9T_PKG.GET_COL_NAMES(I_file_id,ODT_sheet);
   ODT$action               := GET_COL_IDX(ODT_cols,'ACTION');
   ODT$ORDER_NO             := GET_COL_IDX(ODT_cols,'ORDER_NO');
   ODT$ITEM                 := GET_COL_IDX(ODT_cols,'ITEM');
   ODT$ITEM_DESC            := GET_COL_IDX(ODT_cols,'ITEM_DESC');
   ODT$VPN                  := GET_COL_IDX(ODT_cols,'VPN');
   ODT$ITEM_PARENT          := GET_COL_IDX(ODT_cols,'ITEM_PARENT');
   ODT$REF_ITEM             := GET_COL_IDX(ODT_cols,'REF_ITEM');
   ODT$DIFF_1               := GET_COL_IDX(ODT_cols,'DIFF_1');
   ODT$DIFF_2               := GET_COL_IDX(ODT_cols,'DIFF_2');
   ODT$DIFF_3               := GET_COL_IDX(ODT_cols,'DIFF_3');
   ODT$DIFF_4               := GET_COL_IDX(ODT_cols,'DIFF_4');
   ODT$ORIGIN_COUNTRY_ID    := GET_COL_IDX(ODT_cols,'ORIGIN_COUNTRY_ID');
   ODT$SUPP_PACK_SIZE       := GET_COL_IDX(ODT_cols,'SUPP_PACK_SIZE');
   ODT$LOC_type             := GET_COL_IDX(ODT_cols,'LOC_TYPE');
   ODT$LOCATION             := GET_COL_IDX(ODT_cols,'LOCATION');
   ODT$LOCATION_DESC        := GET_COL_IDX(ODT_cols,'LOCATION_DESC');
   ODT$UNIT_COST            := GET_COL_IDX(ODT_cols,'UNIT_COST');
   ODT$QTY_ORDERED          := GET_COL_IDX(ODT_cols,'QTY_ORDERED');
   ODT$DELIVERY_DATE        := GET_COL_IDX(ODT_cols,'DELIVERY_DATE');
   ODT$NON_SCALE_IND        := GET_COL_IDX(ODT_cols,'NON_SCALE_IND');
   ODT$PROCESSING_type      := GET_COL_IDX(ODT_cols,'PROCESSING_TYPE');
   ODT$PROCESSING_WH        := GET_COL_IDX(ODT_cols,'PROCESSING_WH');
   ODT$ALLOC_NO             := GET_COL_IDX(ODT_cols,'ALLOC_NO');
   ODT$QTY_CANCELLED        := GET_COL_IDX(ODT_cols,'QTY_CANCELLED');
   ODT$QTY_TRANSFERRED      := GET_COL_IDX(ODT_cols,'QTY_TRANSFERRED');
   ODT$CANCEL_CODE          := GET_COL_IDX(ODT_cols,'CANCEL_CODE');
   ODT$RELEASE_DATE         := GET_COL_IDX(ODT_cols,'RELEASE_DATE');
   ODT$IN_STORE_DATE        := GET_COL_IDX(ODT_cols,'IN_STORE_DATE');
   --ordhead_cfa_ext
   OHE_CFA_cols             := S9T_PKG.GET_COL_NAMES(I_file_id,OHE_CFA_sheet);
   OHE_CFA$action           := GET_COL_IDX(OHE_CFA_cols,'ACTION');
   OHE_CFA$ORDER_NO         := GET_COL_IDX(OHE_CFA_cols,'ORDER_NO');
   OHE_CFA$GROUP_ID         := GET_COL_IDX(OHE_CFA_cols,'GROUP_ID');
   OHE_CFA$VARCHAR2_1       := GET_COL_IDX(OHE_CFA_cols,'VARCHAR2_1');
   OHE_CFA$VARCHAR2_2       := GET_COL_IDX(OHE_CFA_cols,'VARCHAR2_2');
   OHE_CFA$VARCHAR2_3       := GET_COL_IDX(OHE_CFA_cols,'VARCHAR2_3');
   OHE_CFA$VARCHAR2_4       := GET_COL_IDX(OHE_CFA_cols,'VARCHAR2_4');
   OHE_CFA$VARCHAR2_5       := GET_COL_IDX(OHE_CFA_cols,'VARCHAR2_5');
   OHE_CFA$VARCHAR2_6       := GET_COL_IDX(OHE_CFA_cols,'VARCHAR2_6');
   OHE_CFA$VARCHAR2_7       := GET_COL_IDX(OHE_CFA_cols,'VARCHAR2_7');
   OHE_CFA$VARCHAR2_8       := GET_COL_IDX(OHE_CFA_cols,'VARCHAR2_8');
   OHE_CFA$VARCHAR2_9       := GET_COL_IDX(OHE_CFA_cols,'VARCHAR2_9');
   OHE_CFA$VARCHAR2_10      := GET_COL_IDX(OHE_CFA_cols,'VARCHAR2_10');
   OHE_CFA$NUMBER_11        := GET_COL_IDX(OHE_CFA_cols,'NUMBER_11');
   OHE_CFA$NUMBER_12        := GET_COL_IDX(OHE_CFA_cols,'NUMBER_12');
   OHE_CFA$NUMBER_13        := GET_COL_IDX(OHE_CFA_cols,'NUMBER_13');
   OHE_CFA$NUMBER_14        := GET_COL_IDX(OHE_CFA_cols,'NUMBER_14');
   OHE_CFA$NUMBER_15        := GET_COL_IDX(OHE_CFA_cols,'NUMBER_15');
   OHE_CFA$NUMBER_16        := GET_COL_IDX(OHE_CFA_cols,'NUMBER_16');
   OHE_CFA$NUMBER_17        := GET_COL_IDX(OHE_CFA_cols,'NUMBER_17');
   OHE_CFA$NUMBER_18        := GET_COL_IDX(OHE_CFA_cols,'NUMBER_18');
   OHE_CFA$NUMBER_19        := GET_COL_IDX(OHE_CFA_cols,'NUMBER_19');
   OHE_CFA$NUMBER_20        := GET_COL_IDX(OHE_CFA_cols,'NUMBER_20');
   OHE_CFA$DATE_21          := GET_COL_IDX(OHE_CFA_cols,'DATE_21');
   OHE_CFA$DATE_22          := GET_COL_IDX(OHE_CFA_cols,'DATE_22');
   OHE_CFA$DATE_23          := GET_COL_IDX(OHE_CFA_cols,'DATE_23');
   OHE_CFA$DATE_24          := GET_COL_IDX(OHE_CFA_cols,'DATE_24');
   OHE_CFA$DATE_25          := GET_COL_IDX(OHE_CFA_cols,'DATE_25');

END POPULATE_NAMES;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_OHE(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_ORDHEAD.PROCESS_ID%TYPE) IS

   TYPE            SVC_OHE_COL_TYP IS TABLE OF SVC_ORDHEAD%ROWTYPE;
   L_temp_rec      SVC_ORDHEAD%ROWTYPE;
   svc_ins_col     SVC_OHE_COL_TYP := NEW SVC_OHE_COL_TYP();
   svc_upd_col     SVC_OHE_COL_TYP := NEW SVC_OHE_COL_TYP();
   L_process_id    SVC_ORDHEAD.PROCESS_ID%TYPE;
   L_error         BOOLEAN := FALSE;
   L_default_rec   SVC_ORDHEAD%ROWTYPE;
   L_table         VARCHAR2(30) := 'SVC_ORDHEAD';
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   L_stg_exists    BOOLEAN;
   L_pk_columns    VARCHAR2(255) := 'order number or external reference number';
   L_error_code    NUMBER;

   cursor C_MANDATORY_IND is
      select order_no_mi,
             order_type_mi,
             dept_mi,
             buyer_mi,
             supplier_mi,
             loc_type_mi,
             location_mi,
             promotion_mi,
             qc_ind_mi,
             written_date_mi,
             not_before_date_mi,
             not_after_date_mi,
             otb_eow_date_mi,
             earliest_ship_date_mi,
             latest_ship_date_mi,
             terms_mi,
             freight_terms_mi,
             orig_ind_mi,
             payment_method_mi,
             backhaul_type_mi,
             backhaul_allowance_mi,
             ship_method_mi,
             purchase_type_mi,
             status_mi,
             ship_pay_method_mi,
             fob_trans_res_mi,
             fob_trans_res_desc_mi,
             fob_title_pass_mi,
             fob_title_pass_desc_mi,
             edi_po_ind_mi,
             import_order_ind_mi,
             import_country_id_mi,
             include_on_order_ind_mi,
             vendor_order_no_mi,
             exchange_rate_mi,
             factory_mi,
             agent_mi,
             discharge_port_mi,
             lading_port_mi,
             freight_contract_no_mi,
             po_type_mi,
             pre_mark_ind_mi,
             currency_code_mi,
             reject_code_mi,
             contract_no_mi,
             pickup_loc_mi,
             pickup_no_mi,
             pickup_date_mi,
             comment_desc_mi,
             partner_type_1_mi,
             partner1_mi,
             partner_type_2_mi,
             partner2_mi,
             partner_type_3_mi,
             partner3_mi,
             import_type_mi,
             import_id_mi,
             clearing_zone_id_mi,
             routing_loc_id_mi,
             ext_ref_no_mi,
             master_po_no_mi,
             re_approve_ind_mi,
             last_upd_id_mi,
             next_upd_id_mi,
             last_upd_datetime_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = PO_INDUCT_SQL.TEMPLATE_KEY
                 and wksht_key    = 'ORDHEAD')
       pivot (MAX(mandatory) AS mi FOR (column_key) IN ('ORDER_NO'             AS ORDER_NO,
                                                        'ORDER_TYPE'           AS ORDER_TYPE,
                                                        'DEPT'                 AS DEPT,
                                                        'BUYER'                AS BUYER,
                                                        'SUPPLIER'             AS SUPPLIER,
                                                        'LOC_TYPE'             AS LOC_TYPE,
                                                        'LOCATION'             AS LOCATION,
                                                        'PROMOTION'            AS PROMOTION,
                                                        'QC_IND'               AS QC_IND,
                                                        'WRITTEN_DATE'         AS WRITTEN_DATE,
                                                        'NOT_BEFORE_DATE'      AS NOT_BEFORE_DATE,
                                                        'NOT_AFTER_DATE'       AS NOT_AFTER_DATE,
                                                        'OTB_EOW_DATE'         AS OTB_EOW_DATE,
                                                        'EARLIEST_SHIP_DATE'   AS EARLIEST_SHIP_DATE,
                                                        'LATEST_SHIP_DATE'     AS LATEST_SHIP_DATE,
                                                        'TERMS'                AS TERMS,
                                                        'FREIGHT_TERMS'        AS FREIGHT_TERMS,
                                                        'ORIG_IND'             AS ORIG_IND,
                                                        'PAYMENT_METHOD'       AS PAYMENT_METHOD,
                                                        'BACKHAUL_TYPE'        AS BACKHAUL_TYPE,
                                                        'BACKHAUL_ALLOWANCE'   AS BACKHAUL_ALLOWANCE,
                                                        'SHIP_METHOD'          AS SHIP_METHOD,
                                                        'PURCHASE_TYPE'        AS PURCHASE_TYPE,
                                                        'STATUS'               AS STATUS,
                                                        'SHIP_PAY_METHOD'      AS SHIP_PAY_METHOD,
                                                        'FOB_TRANS_RES'        AS FOB_TRANS_RES,
                                                        'FOB_TRANS_RES_DESC'   AS FOB_TRANS_RES_DESC,
                                                        'FOB_TITLE_PASS'       AS FOB_TITLE_PASS,
                                                        'FOB_TITLE_PASS_DESC'  AS FOB_TITLE_PASS_DESC,
                                                        'EDI_PO_IND'           AS EDI_PO_IND,
                                                        'IMPORT_ORDER_IND'     AS IMPORT_ORDER_IND,
                                                        'IMPORT_COUNTRY_ID'    AS IMPORT_COUNTRY_ID,
                                                        'INCLUDE_ON_ORDER_IND' AS INCLUDE_ON_ORDER_IND,
                                                        'VENDOR_ORDER_NO'      AS VENDOR_ORDER_NO,
                                                        'EXCHANGE_RATE'        AS EXCHANGE_RATE,
                                                        'FACTORY'              AS FACTORY,
                                                        'AGENT'                AS AGENT,
                                                        'DISCHARGE_PORT'       AS DISCHARGE_PORT,
                                                        'LADING_PORT'          AS LADING_PORT,
                                                        'FREIGHT_CONTRACT_NO'  AS FREIGHT_CONTRACT_NO,
                                                        'PO_TYPE'              AS PO_TYPE,
                                                        'PRE_MARK_IND'         AS PRE_MARK_IND,
                                                        'CURRENCY_CODE'        AS CURRENCY_CODE,
                                                        'REJECT_CODE'          AS REJECT_CODE,
                                                        'CONTRACT_NO'          AS CONTRACT_NO,
                                                        'PICKUP_LOC'           AS PICKUP_LOC,
                                                        'PICKUP_NO'            AS PICKUP_NO,
                                                        'PICKUP_DATE'          AS PICKUP_DATE,
                                                        'COMMENT_DESC'         AS COMMENT_DESC,
                                                        'PARTNER_TYPE_1'       AS PARTNER_TYPE_1,
                                                        'PARTNER1'             AS PARTNER1,
                                                        'PARTNER_TYPE_2'       AS PARTNER_TYPE_2,
                                                        'PARTNER2'             AS PARTNER2,
                                                        'PARTNER_TYPE_3'       AS PARTNER_TYPE_3,
                                                        'PARTNER3'             AS PARTNER3,
                                                        'IMPORT_TYPE'          AS IMPORT_TYPE,
                                                        'IMPORT_ID'            AS IMPORT_ID,
                                                        'CLEARING_ZONE_ID'     AS CLEARING_ZONE_ID,
                                                        'ROUTING_LOC_ID'       AS ROUTING_LOC_ID,
                                                        'EXT_REF_NO'           AS EXT_REF_NO,
                                                        'MASTER_PO_NO'         AS MASTER_PO_NO,
                                                        'RE_APPROVE_IND'       AS RE_APPROVE_IND,
                                                        'LAST_UPD_ID'          AS LAST_UPD_ID,
                                                        'NEXT_UPD_ID'          AS NEXT_UPD_ID,
                                                        'LAST_UPD_DATETIME'    AS LAST_UPD_DATETIME));
   L_mi_rec     C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS   EXCEPTION;
   PRAGMA       EXCEPTION_INIT(DML_ERRORS, -24381);

BEGIN
   --Get default values.
   FOR rec IN (select order_no_dv,
                      order_type_dv,
                      dept_dv,
                      buyer_dv,
                      supplier_dv,
                      loc_type_dv,
                      location_dv,
                      promotion_dv,
                      qc_ind_dv,
                      written_date_dv,
                      not_before_date_dv,
                      not_after_date_dv,
                      otb_eow_date_dv,
                      earliest_ship_date_dv,
                      latest_ship_date_dv,
                      terms_dv,
                      freight_terms_dv,
                      orig_ind_dv,
                      payment_method_dv,
                      backhaul_type_dv,
                      backhaul_allowance_dv,
                      ship_method_dv,
                      purchase_type_dv,
                      status_dv,
                      ship_pay_method_dv,
                      fob_trans_res_dv,
                      fob_trans_res_desc_dv,
                      fob_title_pass_dv,
                      fob_title_pass_desc_dv,
                      edi_po_ind_dv,
                      import_order_ind_dv,
                      UPPER(import_country_id_dv) as import_country_id_dv,
                      include_on_order_ind_dv,
                      vendor_order_no_dv,
                      exchange_rate_dv,
                      factory_dv,
                      agent_dv,
                      discharge_port_dv,
                      lading_port_dv,
                      freight_contract_no_dv,
                      po_type_dv,
                      pre_mark_ind_dv,
                      UPPER(currency_code_dv) as currency_code_dv,
                      reject_code_dv,
                      contract_no_dv,
                      pickup_loc_dv,
                      pickup_no_dv,
                      pickup_date_dv,
                      comment_desc_dv,
                      partner_type_1_dv,
                      partner1_dv,
                      partner_type_2_dv,
                      partner2_dv,
                      partner_type_3_dv,
                      partner3_dv,
                      import_type_dv,
                      import_id_dv,
                      clearing_zone_id_dv,
                      routing_loc_id_dv,
                      ext_ref_no_dv,
                      master_po_no_dv,
                      re_approve_ind_dv,
                      last_upd_id_dv,
                      next_upd_id_dv,
                      last_upd_datetime_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = PO_INDUCT_SQL.TEMPLATE_KEY
                          and wksht_key = 'ORDHEAD')
                pivot (MAX(default_value) AS dv FOR (column_key) IN ('ORDER_NO'             AS ORDER_NO,
                                                                     'ORDER_TYPE'           AS ORDER_TYPE,
                                                                     'DEPT'                 AS DEPT,
                                                                     'BUYER'                AS BUYER,
                                                                     'SUPPLIER'             AS SUPPLIER,
                                                                     'LOC_TYPE'             AS LOC_TYPE,
                                                                     'LOCATION'             AS LOCATION,
                                                                     'PROMOTION'            AS PROMOTION,
                                                                     'QC_IND'               AS QC_IND,
                                                                     'WRITTEN_DATE'         AS WRITTEN_DATE,
                                                                     'NOT_BEFORE_DATE'      AS NOT_BEFORE_DATE,
                                                                     'NOT_AFTER_DATE'       AS NOT_AFTER_DATE,
                                                                     'OTB_EOW_DATE'         AS OTB_EOW_DATE,
                                                                     'EARLIEST_SHIP_DATE'   AS EARLIEST_SHIP_DATE,
                                                                     'LATEST_SHIP_DATE'     AS LATEST_SHIP_DATE,
                                                                     'TERMS'                AS TERMS,
                                                                     'FREIGHT_TERMS'        AS FREIGHT_TERMS,
                                                                     'ORIG_IND'             AS ORIG_IND,
                                                                     'PAYMENT_METHOD'       AS PAYMENT_METHOD,
                                                                     'BACKHAUL_TYPE'        AS BACKHAUL_TYPE,
                                                                     'BACKHAUL_ALLOWANCE'   AS BACKHAUL_ALLOWANCE,
                                                                     'SHIP_METHOD'          AS SHIP_METHOD,
                                                                     'PURCHASE_TYPE'        AS PURCHASE_TYPE,
                                                                     'STATUS'               AS STATUS,
                                                                     'SHIP_PAY_METHOD'      AS SHIP_PAY_METHOD,
                                                                     'FOB_TRANS_RES'        AS FOB_TRANS_RES,
                                                                     'FOB_TRANS_RES_DESC'   AS FOB_TRANS_RES_DESC,
                                                                     'FOB_TITLE_PASS'       AS FOB_TITLE_PASS,
                                                                     'FOB_TITLE_PASS_DESC'  AS FOB_TITLE_PASS_DESC,
                                                                     'EDI_PO_IND'           AS EDI_PO_IND,
                                                                     'IMPORT_ORDER_IND'     AS IMPORT_ORDER_IND,
                                                                     'IMPORT_COUNTRY_ID'    AS IMPORT_COUNTRY_ID,
                                                                     'INCLUDE_ON_ORDER_IND' AS INCLUDE_ON_ORDER_IND,
                                                                     'VENDOR_ORDER_NO'      AS VENDOR_ORDER_NO,
                                                                     'EXCHANGE_RATE'        AS EXCHANGE_RATE,
                                                                     'FACTORY'              AS FACTORY,
                                                                     'AGENT'                AS AGENT,
                                                                     'DISCHARGE_PORT'       AS DISCHARGE_PORT,
                                                                     'LADING_PORT'          AS LADING_PORT,
                                                                     'FREIGHT_CONTRACT_NO'  AS FREIGHT_CONTRACT_NO,
                                                                     'PO_TYPE'              AS PO_TYPE,
                                                                     'PRE_MARK_IND'         AS PRE_MARK_IND,
                                                                     'CURRENCY_CODE'        AS CURRENCY_CODE,
                                                                     'REJECT_CODE'          AS REJECT_CODE,
                                                                     'CONTRACT_NO'          AS CONTRACT_NO,
                                                                     'PICKUP_LOC'           AS PICKUP_LOC,
                                                                     'PICKUP_NO'            AS PICKUP_NO,
                                                                     'PICKUP_DATE'          AS PICKUP_DATE,
                                                                     'COMMENT_DESC'         AS COMMENT_DESC,
                                                                     'PARTNER_TYPE_1'       AS PARTNER_TYPE_1,
                                                                     'PARTNER1'             AS PARTNER1,
                                                                     'PARTNER_TYPE_2'       AS PARTNER_TYPE_2,
                                                                     'PARTNER2'             AS PARTNER2,
                                                                     'PARTNER_TYPE_3'       AS PARTNER_TYPE_3,
                                                                     'PARTNER3'             AS PARTNER3,
                                                                     'IMPORT_TYPE'          AS IMPORT_TYPE,
                                                                     'IMPORT_ID'            AS IMPORT_ID,
                                                                     'CLEARING_ZONE_ID'     AS CLEARING_ZONE_ID,
                                                                     'ROUTING_LOC_ID'       AS ROUTING_LOC_ID,
                                                                     'EXT_REF_NO'           AS EXT_REF_NO,
                                                                     'MASTER_PO_NO'         AS MASTER_PO_NO,
                                                                     'RE_APPROVE_IND'       AS RE_APPROVE_IND,
                                                                     'LAST_UPD_ID'          AS LAST_UPD_ID,
                                                                     'NEXT_UPD_ID'          AS NEXT_UPD_ID,
                                                                     'LAST_UPD_DATETIME'    AS LAST_UPD_DATETIME)))

   LOOP
      BEGIN
         L_default_rec.order_no := rec.order_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'ORDER_NO','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.order_type := rec.order_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'ORDER_TYPE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.dept := rec.dept_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'DEPT','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.buyer := rec.buyer_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'BUYER','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.supplier := rec.supplier_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'SUPPLIER','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.loc_TYPE := rec.loc_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'LOC_TYPE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.location := rec.location_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'LOCATION','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.promotion := rec.promotion_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PROMOTION','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.qc_ind := rec.qc_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'QC_IND','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.written_date := rec.written_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'WRITTEN_DATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.not_before_date := rec.not_before_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'NOT_BEFORE_DATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.not_after_date := rec.not_after_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'NOT_AFTER_DATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.otb_eow_date := rec.otb_eow_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'OTB_EOW_DATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.earliest_ship_date := rec.earliest_ship_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'EARLIEST_SHIP_DATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.latest_ship_date := rec.latest_ship_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'LATEST_SHIP_DATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.terms := rec.terms_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'TERMS','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.freight_terms := rec.freight_terms_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'FREIGHT_TERMS','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.orig_ind := rec.orig_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'ORIG_IND','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.payment_method := rec.payment_method_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PAYMENT_METHOD','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.backhaul_type := rec.backhaul_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'BACKHAUL_TYPE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.backhaul_allowance := rec.backhaul_allowance_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'BACKHAUL_ALLOWANCE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.ship_method := rec.ship_method_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'SHIP_METHOD','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.purchase_type := rec.purchase_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PURCHASE_TYPE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.status := rec.status_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'STATUS','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.ship_pay_method := rec.ship_pay_method_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'SHIP_PAY_METHOD','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.fob_trans_res := rec.fob_trans_res_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'FOB_TRANS_RES','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.fob_trans_res_desc := rec.fob_trans_res_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'FOB_TRANS_RES_DESC','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.fob_title_pass := rec.fob_title_pass_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'FOB_TITLE_PASS','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.fob_title_pass_desc := rec.fob_title_pass_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'FOB_TITLE_PASS_DESC','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.edi_po_ind := rec.edi_po_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'EDI_PO_IND','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.import_order_ind := NVL(rec.import_order_ind_dv,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'IMPORT_ORDER_IND','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'IMPORT_COUNTRY_ID','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.include_on_order_ind := rec.include_on_order_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'INCLUDE_ON_ORDER_IND','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.vendor_order_no := rec.vendor_order_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'VENDOR_ORDER_NO','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.exchange_rate := rec.exchange_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'EXCHANGE_RATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.factory := rec.factory_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'FACTORY','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.agent := rec.agent_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'AGENT','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.discharge_port := rec.discharge_port_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'DISCHARGE_PORT','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.lading_port := rec.lading_port_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'LADING_PORT','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.freight_contract_no := rec.freight_contract_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'FREIGHT_CONTRACT_NO','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.po_TYPE := rec.po_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PO_TYPE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.pre_mark_ind := rec.pre_mark_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PRE_MARK_IND','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.currency_code := rec.currency_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'CURRENCY_CODE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.reject_code := rec.reject_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'REJECT_CODE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.contract_no := rec.contract_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'CONTRACT_NO','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.pickup_loc := rec.pickup_loc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PICKUP_LOC','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.pickup_no := rec.pickup_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PICKUP_NO','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.pickup_date := rec.pickup_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PICKUP_DATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.comment_desc := rec.comment_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'COMMENT_DESC','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.partner_type_1 := rec.partner_type_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PARTNER_TYPE_1','INV_DEFAULT',SQLERRM);
      END;

      BEGIN
         L_default_rec.partner1 := rec.partner1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PARTNER1','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.partner_type_2 := rec.partner_type_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PARTNER_TYPE_2','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.partner2 := rec.partner2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PARTNER2','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.partner_type_3 := rec.partner_type_3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PARTNER_TYPE_3','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.partner3 := rec.partner3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'PARTNER3','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.import_TYPE := rec.import_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'IMPORT_TYPE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.import_id := rec.import_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'IMPORT_ID','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.clearing_zone_id := rec.clearing_zone_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'CLEARING_ZONE_ID','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.routing_loc_id := rec.routing_loc_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'ROUTING_LOC_ID','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.master_po_no := rec.master_po_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'MASTER_PO_NO','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.re_approve_ind := NVL(rec.re_approve_ind_dv,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'RE_APPROVE_IND','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.next_upd_id := rec.next_upd_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,NULL,'NEXT_UPD_ID','INV_DEFAULT',SQLERRM);
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;

   FOR rec IN (select r.get_cell(OHE$action)               AS action,
                      r.get_cell(OHE$order_no)             AS order_no,
                      r.get_cell(OHE$order_TYPE)           AS order_type,
                      r.get_cell(OHE$dept)                 AS dept,
                      r.get_cell(OHE$buyer)                AS buyer,
                      r.get_cell(OHE$supplier)             AS supplier,
                      r.get_cell(OHE$loc_type)             AS loc_type,
                      r.get_cell(OHE$location)             AS location,
                      r.get_cell(OHE$promotion)            AS promotion,
                      r.get_cell(OHE$qc_ind)               AS qc_ind,
                      r.get_cell(OHE$written_date)         AS written_date,
                      r.get_cell(OHE$not_before_date)      AS not_before_date,
                      r.get_cell(OHE$not_after_date)       AS not_after_date,
                      r.get_cell(OHE$otb_eow_date)         AS otb_eow_date,
                      r.get_cell(OHE$earliest_ship_date)   AS earliest_ship_date,
                      r.get_cell(OHE$latest_ship_date)     AS latest_ship_date,
                      r.get_cell(OHE$terms)                AS terms,
                      r.get_cell(OHE$freight_terms)        AS freight_terms,
                      r.get_cell(OHE$orig_ind)             AS orig_ind,
                      r.get_cell(OHE$payment_method)       AS payment_method,
                      r.get_cell(OHE$backhaul_type)        AS backhaul_type,
                      r.get_cell(OHE$backhaul_allowance)   AS backhaul_allowance,
                      r.get_cell(OHE$ship_method)          AS ship_method,
                      r.get_cell(OHE$purchase_type)        AS purchase_type,
                      r.get_cell(OHE$status)               AS status,
                      r.get_cell(OHE$ship_pay_method)      AS ship_pay_method,
                      r.get_cell(OHE$fob_trans_res)        AS fob_trans_res,
                      r.get_cell(OHE$fob_trans_res_desc)   AS fob_trans_res_desc,
                      r.get_cell(OHE$fob_title_pass)       AS fob_title_pass,
                      r.get_cell(OHE$fob_title_pass_desc)  AS fob_title_pass_desc,
                      r.get_cell(OHE$edi_po_ind)           AS edi_po_ind,
                      r.get_cell(OHE$import_order_ind)     AS import_order_ind,
                      UPPER(r.get_cell(OHE$import_country_id))    AS import_country_id,
                      r.get_cell(OHE$include_on_order_ind) AS include_on_order_ind,
                      r.get_cell(OHE$vendor_order_no)      AS vendor_order_no,
                      r.get_cell(OHE$exchange_rate)        AS exchange_rate,
                      r.get_cell(OHE$factory)              AS factory,
                      r.get_cell(OHE$agent)                AS agent,
                      r.get_cell(OHE$discharge_port)       AS discharge_port,
                      r.get_cell(OHE$lading_port)          AS lading_port,
                      r.get_cell(OHE$freight_contract_no)  AS freight_contract_no,
                      r.get_cell(OHE$po_type)              AS po_type,
                      r.get_cell(OHE$pre_mark_ind)         AS pre_mark_ind,
                      UPPER(r.get_cell(OHE$currency_code)) AS currency_code,
                      r.get_cell(OHE$reject_code)          AS reject_code,
                      r.get_cell(OHE$contract_no)          AS contract_no,
                      r.get_cell(OHE$pickup_loc)           AS pickup_loc,
                      r.get_cell(OHE$pickup_no)            AS pickup_no,
                      r.get_cell(OHE$pickup_date)          AS pickup_date,
                      r.get_cell(OHE$comment_desc)         AS comment_desc,
                      r.get_cell(OHE$partner_type_1)       AS partner_type_1,
                      r.get_cell(OHE$partner1)             AS partner1,
                      r.get_cell(OHE$partner_type_2)       AS partner_type_2,
                      r.get_cell(OHE$partner2)             AS partner2,
                      r.get_cell(OHE$partner_type_3)       AS partner_type_3,
                      r.get_cell(OHE$partner3)             AS partner3,
                      r.get_cell(OHE$import_type)          AS import_type,
                      r.get_cell(OHE$import_id)            AS import_id,
                      r.get_cell(OHE$clearing_zone_id)     AS clearing_zone_id,
                      r.get_cell(OHE$routing_loc_id)       AS routing_loc_id,
                      r.get_cell(OHE$ext_ref_no)           AS ext_ref_no,
                      r.get_cell(OHE$master_po_no)         AS master_po_no,
                      r.get_cell(OHE$re_approve_ind)       AS re_approve_ind,
                      r.get_cell(OHE$last_upd_id)          AS last_upd_id,
                      r.get_cell(OHE$next_upd_id)          AS next_upd_id,
                      r.get_cell(OHE$last_upd_datetime)    AS last_upd_datetime,
                      r.get_row_seq()                      AS row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = get_sheet_name_trans(OHE_sheet))
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.create_datetime   := sysdate;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.last_upd_datetime := sysdate;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.order_no := rec.order_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'ORDER_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.order_TYPE := rec.order_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'ORDER_TYPE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.dept := rec.dept;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'DEPT',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.buyer := rec.buyer;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'BUYER',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.supplier := rec.supplier;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'SUPPLIER',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.loc_TYPE := rec.loc_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'LOC_TYPE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.location := rec.location;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'LOCATION',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.promotion := rec.promotion;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PROMOTION',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.qc_ind := NVL(rec.qc_ind,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'QC_IND',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.written_date := rec.written_date;
      EXCEPTION
         when OTHERS then
            L_error_msg := SQL_LIB.CREATE_MSG('INVALID_DATE',rec.written_date,LP_format);
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'WRITTEN_DATE',NULL,L_error_msg);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.not_before_date := rec.not_before_date;
      EXCEPTION
         when OTHERS then
            L_error_msg := SQL_LIB.CREATE_MSG('INVALID_DATE',rec.not_before_date,LP_format);
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'NOT_BEFORE_DATE',NULL,L_error_msg);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.not_after_date := rec.not_after_date;
      EXCEPTION
         when OTHERS then
            L_error_msg := SQL_LIB.CREATE_MSG('INVALID_DATE',rec.not_after_date,LP_format);
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'NOT_AFTER_DATE',NULL,L_error_msg);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.otb_eow_date := rec.otb_eow_date;
      EXCEPTION
         when OTHERS then
            L_error_msg := SQL_LIB.CREATE_MSG('INVALID_DATE',rec.otb_eow_date,LP_format);
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'OTB_EOW_DATE',NULL,L_error_msg);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.earliest_ship_date := rec.earliest_ship_date;
      EXCEPTION
         when OTHERS then
            L_error_msg := SQL_LIB.CREATE_MSG('INVALID_DATE',rec.earliest_ship_date,LP_format);
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'EARLIEST_SHIP_DATE',NULL,L_error_msg);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.latest_ship_date := rec.latest_ship_date;
      EXCEPTION
         when OTHERS then
            L_error_msg := SQL_LIB.CREATE_MSG('INVALID_DATE',rec.latest_ship_date,LP_format);
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'LATEST_SHIP_DATE',NULL,L_error_msg);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.terms := rec.terms;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'TERMS',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.freight_terms := rec.freight_terms;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'FREIGHT_TERMS',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.orig_ind := rec.orig_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'ORIG_IND',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.payment_method := rec.payment_method;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PAYMENT_METHOD',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.backhaul_TYPE := rec.backhaul_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'BACKHAUL_TYPE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.backhaul_allowance := rec.backhaul_allowance;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'BACKHAUL_ALLOWANCE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ship_method := rec.ship_method;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'SHIP_METHOD',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.purchase_type := rec.purchase_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PURCHASE_TYPE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.status := rec.status;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'STATUS',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ship_pay_method := rec.ship_pay_method;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'SHIP_PAY_METHOD',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.fob_trans_res := rec.fob_trans_res;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'FOB_TRANS_RES',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.fob_trans_res_desc := rec.fob_trans_res_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'FOB_TRANS_RES_DESC',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.fob_title_pass := rec.fob_title_pass;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'FOB_TITLE_PASS',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.fob_title_pass_desc := rec.fob_title_pass_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'FOB_TITLE_PASS_DESC',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.edi_po_ind := rec.edi_po_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'EDI_PO_IND',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_order_ind := NVL(rec.import_order_ind,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'IMPORT_ORDER_IND',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'IMPORT_COUNTRY_ID',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.include_on_order_ind := NVL(rec.include_on_order_ind,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'INCLUDE_ON_ORDER_IND',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.vendor_order_no := rec.vendor_order_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'VENDOR_ORDER_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.exchange_rate := rec.exchange_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'EXCHANGE_RATE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.factory := rec.factory;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'FACTORY',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.agent := rec.agent;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'AGENT',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.discharge_port := rec.discharge_port;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'DISCHARGE_PORT',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lading_port := rec.lading_port;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'LADING_PORT',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.freight_contract_no := rec.freight_contract_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'FREIGHT_CONTRACT_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.po_TYPE := rec.po_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PO_TYPE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.pre_mark_ind := NVL(rec.pre_mark_ind,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PRE_MARK_IND',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.currency_code := rec.currency_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'CURRENCY_CODE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.reject_code := rec.reject_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'REJECT_CODE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.contract_no := rec.contract_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'CONTRACT_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.pickup_loc := rec.pickup_loc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PICKUP_LOC',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.pickup_no := rec.pickup_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PICKUP_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.pickup_date := rec.pickup_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PICKUP_DATE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comment_desc := rec.comment_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'COMMENT_DESC',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.partner_type_1 := rec.partner_type_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PARTNER_TYPE_1',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.partner1 := rec.partner1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PARTNER1',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.partner_type_2 := rec.partner_type_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PARTNER_TYPE_2',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.partner2 := rec.partner2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PARTNER2',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.partner_type_3 := rec.partner_type_3;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PARTNER_TYPE_3',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.partner3 := rec.partner3;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'PARTNER3',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_type := rec.import_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'IMPORT_TYPE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_id := rec.import_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'IMPORT_ID',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.clearing_zone_id := rec.clearing_zone_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'CLEARING_ZONE_ID',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.routing_loc_id := rec.routing_loc_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'ROUTING_LOC_ID',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ext_ref_no := rec.ext_ref_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'EXT_REF_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.master_po_no := rec.master_po_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'MASTER_PO_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.re_approve_ind := rec.re_approve_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'RE_APPROVE_IND',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.next_upd_id := rec.next_upd_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHE_sheet,rec.row_seq,'NEXT_UPD_ID',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;

      if NOT L_error then
         DECLARE
            L_stg_rec       SVC_ORDHEAD%ROWTYPE;
            L_rms_exists    BOOLEAN;
            RECORD_LOCKED   EXCEPTION;
            PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

            cursor C_STG_REC is
               select *
                 from svc_ordhead
                where order_no = L_temp_rec.order_no;

            cursor C_RMS_REC is
               select oh.*, tl.terms_code
                from ordhead oh,
                     (select distinct terms,terms_code
                        from v_terms_head_tl ) tl
               where order_no = L_temp_rec.order_no
                 and oh.terms = tl.terms;
           
            L_rms_rec       C_RMS_REC%ROWTYPE;
           
            cursor C_LOCK_SVC_ORDHEAD is
               select 'x'
                 from svc_ordhead
                where process_id = L_stg_rec.process_id
                  and row_seq = L_stg_rec.row_seq
                  for update nowait;

         BEGIN
            open C_STG_REC;
            fetch C_STG_REC into L_stg_rec;
            if C_STG_REC%FOUND then
               L_stg_exists := TRUE;
            else
               L_stg_exists := FALSE;
            end if;
            close C_STG_REC;

            open C_RMS_REC;
            fetch C_RMS_REC into L_rms_rec;
            if C_RMS_REC%FOUND then
               L_rms_exists := TRUE;
            else
               L_rms_exists := FALSE;
            end if;
            close C_RMS_REC;

            if L_stg_exists then
               --delete record from staging if it is already uploaded to RMS.
               if (L_stg_rec.process$status = 'P' or (L_stg_rec.process$status = 'E' and L_rms_exists)) and
                  GET_PRC_DEST(L_error_msg,
                               L_stg_rec.process_id) = 'RMS' then
                     open C_LOCK_SVC_ORDHEAD;
                     close C_LOCK_SVC_ORDHEAD;
                     delete from svc_ordhead
                           where process_id = L_stg_rec.process_id
                             and row_seq = L_stg_rec.row_seq;
                     L_stg_exists := FALSE;
               end if;

               --delete record from staging if sending a DEL record for an order not in RMS
               if NOT L_rms_exists and
                  L_stg_rec.action = PO_INDUCT_SQL.ACTION_NEW and
                  L_temp_rec.action = PO_INDUCT_SQL.ACTION_MOD and
                  L_temp_rec.status = 'D' then
                  open C_LOCK_SVC_ORDHEAD;
                  close C_LOCK_SVC_ORDHEAD;
                  delete from svc_ordhead
                        where process_id = L_stg_rec.process_id
                          and row_seq = L_stg_rec.row_seq;
                  ---
                  LP_svc_po_tab.EXTEND();
                  LP_svc_po_tab(LP_svc_po_tab.COUNT()).order_no     := L_temp_rec.order_no;
                  LP_svc_po_tab(LP_svc_po_tab.COUNT()).svc_tbl_name := L_table;
                  continue;
               end if;

               if L_stg_rec.process$status = 'N' then
                  --retain a DEL record in staging if the record has not yet been processed
                  if L_stg_rec.action = PO_INDUCT_SQL.ACTION_MOD and
                     L_stg_rec.status = 'D' and
                     L_temp_rec.action in (PO_INDUCT_SQL.ACTION_MOD, PO_INDUCT_SQL.ACTION_DEL) then
                     continue;
                  end if;
               end if;
            end if; -- if L_stg_exists

            --copy values for non-mandatory columns from staging.
            if L_stg_exists then
               if L_mi_rec.order_no_mi <> 'Y' then
                  L_temp_rec.order_no := L_stg_rec.order_no;
               end if;
               if L_mi_rec.order_TYPE_mi <> 'Y' then
                  L_temp_rec.order_type := L_stg_rec.order_type;
               end if;
               if L_mi_rec.dept_mi <> 'Y' then
                  L_temp_rec.dept := L_stg_rec.dept;
               end if;
               if L_mi_rec.buyer_mi <> 'Y' then
                  L_temp_rec.buyer := L_stg_rec.buyer;
               end if;
               if L_mi_rec.supplier_mi <> 'Y' then
                  L_temp_rec.supplier := L_stg_rec.supplier;
               end if;
               if L_mi_rec.loc_type_mi <> 'Y' then
                  L_temp_rec.loc_type := L_stg_rec.loc_type;
               end if;
               if L_mi_rec.location_mi <> 'Y' then
                  L_temp_rec.location := L_stg_rec.location;
               end if;
               if L_mi_rec.promotion_mi <> 'Y' then
                  L_temp_rec.promotion := L_stg_rec.promotion;
               end if;
               if L_mi_rec.qc_ind_mi <> 'Y' then
                  L_temp_rec.qc_ind := L_stg_rec.qc_ind;
               end if;
               if L_mi_rec.written_date_mi <> 'Y' then
                  L_temp_rec.written_date := L_stg_rec.written_date;
               end if;
               if L_mi_rec.not_before_date_mi <> 'Y' then
                  L_temp_rec.not_before_date := L_stg_rec.not_before_date;
               end if;
               if L_mi_rec.not_after_date_mi <> 'Y' then
                  L_temp_rec.not_after_date := L_stg_rec.not_after_date;
               end if;
               if L_mi_rec.otb_eow_date_mi <> 'Y' then
                  L_temp_rec.otb_eow_date := L_stg_rec.otb_eow_date;
               end if;
               if L_mi_rec.earliest_ship_date_mi <> 'Y' then
                  L_temp_rec.earliest_ship_date := L_stg_rec.earliest_ship_date;
               end if;
               if L_mi_rec.latest_ship_date_mi <> 'Y' then
                  L_temp_rec.latest_ship_date := L_stg_rec.latest_ship_date;
               end if;
               if L_mi_rec.terms_mi <> 'Y' then
                  L_temp_rec.terms := L_stg_rec.terms;
               end if;
               if L_mi_rec.freight_terms_mi <> 'Y' then
                  L_temp_rec.freight_terms := L_stg_rec.freight_terms;
               end if;
               if L_mi_rec.orig_ind_mi <> 'Y' then
                  L_temp_rec.orig_ind := L_stg_rec.orig_ind;
               end if;
               if L_mi_rec.payment_method_mi <> 'Y' then
                  L_temp_rec.payment_method := L_stg_rec.payment_method;
               end if;
               if L_mi_rec.backhaul_type_mi <> 'Y' then
                  L_temp_rec.backhaul_type := L_stg_rec.backhaul_type;
               end if;
               if L_mi_rec.backhaul_allowance_mi <> 'Y' then
                  L_temp_rec.backhaul_allowance := L_stg_rec.backhaul_allowance;
               end if;
               if L_mi_rec.ship_method_mi <> 'Y' then
                  L_temp_rec.ship_method := L_stg_rec.ship_method;
               end if;
               if L_mi_rec.purchase_type_mi <> 'Y' then
                  L_temp_rec.purchase_type := L_stg_rec.purchase_type;
               end if;
               if L_mi_rec.status_mi <> 'Y' then
                  L_temp_rec.status := L_stg_rec.status;
               end if;
               if L_mi_rec.ship_pay_method_mi <> 'Y' then
                  L_temp_rec.ship_pay_method := L_stg_rec.ship_pay_method;
               end if;
               if L_mi_rec.fob_trans_res_mi <> 'Y' then
                  L_temp_rec.fob_trans_res := L_stg_rec.fob_trans_res;
               end if;
               if L_mi_rec.fob_trans_res_desc_mi <> 'Y' then
                  L_temp_rec.fob_trans_res_desc := L_stg_rec.fob_trans_res_desc;
               end if;
               if L_mi_rec.fob_title_pass_mi <> 'Y' then
                  L_temp_rec.fob_title_pass := L_stg_rec.fob_title_pass;
               end if;
               if L_mi_rec.fob_title_pass_desc_mi <> 'Y' then
                  L_temp_rec.fob_title_pass_desc := L_stg_rec.fob_title_pass_desc;
               end if;
               if L_mi_rec.edi_po_ind_mi <> 'Y' then
                  L_temp_rec.edi_po_ind := L_stg_rec.edi_po_ind;
               end if;
               if L_mi_rec.import_order_ind_mi <> 'Y' then
                  L_temp_rec.import_order_ind := L_stg_rec.import_order_ind;
               end if;
               if L_mi_rec.import_country_id_mi <> 'Y' then
                  L_temp_rec.import_country_id := L_stg_rec.import_country_id;
               end if;
               if L_mi_rec.include_on_order_ind_mi <> 'Y' then
                  L_temp_rec.include_on_order_ind := L_stg_rec.include_on_order_ind;
               end if;
               if L_mi_rec.vendor_order_no_mi <> 'Y' then
                  L_temp_rec.vendor_order_no := L_stg_rec.vendor_order_no;
               end if;
               if L_mi_rec.exchange_rate_mi <> 'Y' then
                  L_temp_rec.exchange_rate := L_stg_rec.exchange_rate;
               end if;
               if L_mi_rec.factory_mi <> 'Y' then
                  L_temp_rec.factory := L_stg_rec.factory;
               end if;
               if L_mi_rec.agent_mi <> 'Y' then
                  L_temp_rec.agent := L_stg_rec.agent;
               end if;
               if L_mi_rec.discharge_port_mi <> 'Y' then
                  L_temp_rec.discharge_port := L_stg_rec.discharge_port;
               end if;
               if L_mi_rec.lading_port_mi <> 'Y' then
                  L_temp_rec.lading_port := L_stg_rec.lading_port;
               end if;
               if L_mi_rec.freight_contract_no_mi <> 'Y' then
                  L_temp_rec.freight_contract_no := L_stg_rec.freight_contract_no;
               end if;
               if L_mi_rec.po_type_mi <> 'Y' then
                  L_temp_rec.po_type := L_stg_rec.po_type;
               end if;
               if L_mi_rec.pre_mark_ind_mi <> 'Y' then
                  L_temp_rec.pre_mark_ind := L_stg_rec.pre_mark_ind;
               end if;
               if L_mi_rec.currency_code_mi <> 'Y' then
                  L_temp_rec.currency_code := L_stg_rec.currency_code;
               end if;
               if L_mi_rec.reject_code_mi <> 'Y' then
                  L_temp_rec.reject_code := L_stg_rec.reject_code;
               end if;
               if L_mi_rec.contract_no_mi <> 'Y' then
                  L_temp_rec.contract_no := L_stg_rec.contract_no;
               end if;
               if L_mi_rec.pickup_loc_mi <> 'Y' then
                  L_temp_rec.pickup_loc := L_stg_rec.pickup_loc;
               end if;
               if L_mi_rec.pickup_no_mi <> 'Y' then
                  L_temp_rec.pickup_no := L_stg_rec.pickup_no;
               end if;
               if L_mi_rec.pickup_date_mi <> 'Y' then
                  L_temp_rec.pickup_date := L_stg_rec.pickup_date;
               end if;
               if L_mi_rec.comment_desc_mi <> 'Y' then
                  L_temp_rec.comment_desc := L_stg_rec.comment_desc;
               end if;
               if L_mi_rec.partner_type_1_mi <> 'Y' then
                  L_temp_rec.partner_type_1 := L_stg_rec.partner_type_1;
               end if;
               if L_mi_rec.partner1_mi <> 'Y' then
                  L_temp_rec.partner1 := L_stg_rec.partner1;
               end if;
               if L_mi_rec.partner_type_2_mi <> 'Y' then
                  L_temp_rec.partner_type_2 := L_stg_rec.partner_type_2;
               end if;
               if L_mi_rec.partner2_mi <> 'Y' then
                  L_temp_rec.partner2 := L_stg_rec.partner2;
               end if;
               if L_mi_rec.partner_type_3_mi <> 'Y' then
                  L_temp_rec.partner_type_3 := L_stg_rec.partner_type_3;
               end if;
               if L_mi_rec.partner3_mi <> 'Y' then
                  L_temp_rec.partner3 := L_stg_rec.partner3;
               end if;
               if L_mi_rec.import_type_mi <> 'Y' then
                  L_temp_rec.import_type := L_stg_rec.import_type;
               end if;
               if L_mi_rec.import_id_mi <> 'Y' then
                  L_temp_rec.import_id := L_stg_rec.import_id;
               end if;
               if L_mi_rec.clearing_zone_id_mi <> 'Y' then
                  L_temp_rec.clearing_zone_id := L_stg_rec.clearing_zone_id;
               end if;
               if L_mi_rec.routing_loc_id_mi <> 'Y' then
                  L_temp_rec.routing_loc_id := L_stg_rec.routing_loc_id;
               end if;
               if L_mi_rec.ext_ref_no_mi <> 'Y' then
                  L_temp_rec.ext_ref_no := L_stg_rec.ext_ref_no;
               end if;
               if L_mi_rec.master_po_no_mi <> 'Y' then
                  L_temp_rec.master_po_no := L_stg_rec.master_po_no;
               end if;
               if L_mi_rec.re_approve_ind_mi <> 'Y' then
                  L_temp_rec.re_approve_ind := L_stg_rec.re_approve_ind;
               end if;
               if L_mi_rec.next_upd_id_mi <> 'Y' then
                  L_temp_rec.next_upd_id := L_stg_rec.next_upd_id;
               end if;
            end if;
            --if record exists in RMS but not in staging, then copy values for non-mandatory columns from RMS.
            if L_rms_exists and NOT L_stg_exists then
               if L_mi_rec.order_no_mi <> 'Y' then
                  L_temp_rec.order_no := L_rms_rec.order_no;
               end if;
               if L_mi_rec.order_type_mi <> 'Y' then
                  L_temp_rec.order_type := L_rms_rec.order_type;
               end if;
               if L_mi_rec.dept_mi <> 'Y' then
                  L_temp_rec.dept := L_rms_rec.dept;
               end if;
               if L_mi_rec.buyer_mi <> 'Y' then
                  L_temp_rec.buyer := L_rms_rec.buyer;
               end if;
               if L_mi_rec.supplier_mi <> 'Y' then
                  L_temp_rec.supplier := L_rms_rec.supplier;
               end if;
               if L_mi_rec.loc_type_mi <> 'Y' then
                  L_temp_rec.loc_type := L_rms_rec.loc_type;
               end if;
               if L_mi_rec.location_mi <> 'Y' then
                  L_temp_rec.location := L_rms_rec.location;
               end if;
               if L_mi_rec.promotion_mi <> 'Y' then
                  L_temp_rec.promotion := L_rms_rec.promotion;
               end if;
               if L_mi_rec.qc_ind_mi <> 'Y' then
                  L_temp_rec.qc_ind := L_rms_rec.qc_ind;
               end if;
               if L_mi_rec.written_date_mi <> 'Y' then
                  L_temp_rec.written_date := L_rms_rec.written_date;
               end if;
               if L_mi_rec.not_before_date_mi <> 'Y' then
                  L_temp_rec.not_before_date := L_rms_rec.not_before_date;
               end if;
               if L_mi_rec.not_after_date_mi <> 'Y' then
                  L_temp_rec.not_after_date := L_rms_rec.not_after_date;
               end if;
               if L_mi_rec.otb_eow_date_mi <> 'Y' then
                  L_temp_rec.otb_eow_date := L_rms_rec.otb_eow_date;
               end if;
               if L_mi_rec.earliest_ship_date_mi <> 'Y' then
                  L_temp_rec.earliest_ship_date := L_rms_rec.earliest_ship_date;
               end if;
               if L_mi_rec.latest_ship_date_mi <> 'Y' then
                  L_temp_rec.latest_ship_date := L_rms_rec.latest_ship_date;
               end if;
               if L_mi_rec.terms_mi <> 'Y' then
                  L_temp_rec.terms := L_rms_rec.terms_code;
               end if;
               if L_mi_rec.freight_terms_mi <> 'Y' then
                  L_temp_rec.freight_terms := L_rms_rec.freight_terms;
               end if;
               if L_mi_rec.orig_ind_mi <> 'Y' then
                  L_temp_rec.orig_ind := L_rms_rec.orig_ind;
               end if;
               if L_mi_rec.payment_method_mi <> 'Y' then
                  L_temp_rec.payment_method := L_rms_rec.payment_method;
               end if;
               if L_mi_rec.backhaul_type_mi <> 'Y' then
                  L_temp_rec.backhaul_type := L_rms_rec.backhaul_type;
               end if;
               if L_mi_rec.backhaul_allowance_mi <> 'Y' then
                  L_temp_rec.backhaul_allowance := L_rms_rec.backhaul_allowance;
               end if;
               if L_mi_rec.ship_method_mi <> 'Y' then
                  L_temp_rec.ship_method := L_rms_rec.ship_method;
               end if;
               if L_mi_rec.purchase_type_mi <> 'Y' then
                  L_temp_rec.purchase_type := L_rms_rec.purchase_type;
               end if;
               if L_mi_rec.status_mi <> 'Y' then
                  L_temp_rec.status := L_rms_rec.status;
               end if;
               if L_mi_rec.ship_pay_method_mi <> 'Y' then
                  L_temp_rec.ship_pay_method := L_rms_rec.ship_pay_method;
               end if;
               if L_mi_rec.fob_trans_res_mi <> 'Y' then
                  L_temp_rec.fob_trans_res := L_rms_rec.fob_trans_res;
               end if;
               if L_mi_rec.fob_trans_res_desc_mi <> 'Y' then
                  L_temp_rec.fob_trans_res_desc := L_rms_rec.fob_trans_res_desc;
               end if;
               if L_mi_rec.fob_title_pass_mi <> 'Y' then
                  L_temp_rec.fob_title_pass := L_rms_rec.fob_title_pass;
               end if;
               if L_mi_rec.fob_title_pass_desc_mi <> 'Y' then
                  L_temp_rec.fob_title_pass_desc := L_rms_rec.fob_title_pass_desc;
               end if;
               if L_mi_rec.edi_po_ind_mi <> 'Y' then
                  L_temp_rec.edi_po_ind := L_rms_rec.edi_po_ind;
               end if;
               if L_mi_rec.import_order_ind_mi <> 'Y' then
                  L_temp_rec.import_order_ind := L_rms_rec.import_order_ind;
               end if;
               if L_mi_rec.import_country_id_mi <> 'Y' then
                  L_temp_rec.import_country_id := L_rms_rec.import_country_id;
               end if;
               if L_mi_rec.include_on_order_ind_mi <> 'Y' then
                  L_temp_rec.include_on_order_ind := L_rms_rec.include_on_order_ind;
               end if;
               if L_mi_rec.vendor_order_no_mi <> 'Y' then
                  L_temp_rec.vendor_order_no := L_rms_rec.vendor_order_no;
               end if;
               if L_mi_rec.exchange_rate_mi <> 'Y' then
                  L_temp_rec.exchange_rate := L_rms_rec.exchange_rate;
               end if;
               if L_mi_rec.factory_mi <> 'Y' then
                  L_temp_rec.factory := L_rms_rec.factory;
               end if;
               if L_mi_rec.agent_mi <> 'Y' then
                  L_temp_rec.agent := L_rms_rec.agent;
               end if;
               if L_mi_rec.discharge_port_mi <> 'Y' then
                  L_temp_rec.discharge_port := L_rms_rec.discharge_port;
               end if;
               if L_mi_rec.lading_port_mi <> 'Y' then
                  L_temp_rec.lading_port := L_rms_rec.lading_port;
               end if;
               if L_mi_rec.freight_contract_no_mi <> 'Y' then
                  L_temp_rec.freight_contract_no := L_rms_rec.freight_contract_no;
               end if;
               if L_mi_rec.po_type_mi <> 'Y' then
                  L_temp_rec.po_type := L_rms_rec.po_type;
               end if;
               if L_mi_rec.pre_mark_ind_mi <> 'Y' then
                  L_temp_rec.pre_mark_ind := L_rms_rec.pre_mark_ind;
               end if;
               if L_mi_rec.currency_code_mi <> 'Y' then
                  L_temp_rec.currency_code := L_rms_rec.currency_code;
               end if;
               if L_mi_rec.reject_code_mi <> 'Y' then
                  L_temp_rec.reject_code := L_rms_rec.reject_code;
               end if;
               if L_mi_rec.contract_no_mi <> 'Y' then
                  L_temp_rec.contract_no := L_rms_rec.contract_no;
               end if;
               if L_mi_rec.pickup_loc_mi <> 'Y' then
                  L_temp_rec.pickup_loc := L_rms_rec.pickup_loc;
               end if;
               if L_mi_rec.pickup_no_mi <> 'Y' then
                  L_temp_rec.pickup_no := L_rms_rec.pickup_no;
               end if;
               if L_mi_rec.pickup_date_mi <> 'Y' then
                  L_temp_rec.pickup_date := L_rms_rec.pickup_date;
               end if;
               if L_mi_rec.comment_desc_mi <> 'Y' then
                  L_temp_rec.comment_desc := L_rms_rec.comment_desc;
               end if;
               if L_mi_rec.partner_type_1_mi <> 'Y' then
                  L_temp_rec.partner_type_1 := L_rms_rec.partner_type_1;
               end if;
               if L_mi_rec.partner1_mi <> 'Y' then
                  L_temp_rec.partner1 := L_rms_rec.partner1;
               end if;
               if L_mi_rec.partner_type_2_mi <> 'Y' then
                  L_temp_rec.partner_type_2 := L_rms_rec.partner_type_2;
               end if;
               if L_mi_rec.partner2_mi <> 'Y' then
                  L_temp_rec.partner2 := L_rms_rec.partner2;
               end if;
               if L_mi_rec.partner_type_3_mi <> 'Y' then
                  L_temp_rec.partner_type_3 := L_rms_rec.partner_type_3;
               end if;
               if L_mi_rec.partner3_mi <> 'Y' then
                  L_temp_rec.partner3 := L_rms_rec.partner3;
               end if;
               if L_mi_rec.import_type_mi <> 'Y' then
                  L_temp_rec.import_type := L_rms_rec.import_type;
               end if;
               if L_mi_rec.import_id_mi <> 'Y' then
                  L_temp_rec.import_id := L_rms_rec.import_id;
               end if;
               if L_mi_rec.clearing_zone_id_mi <> 'Y' then
                  L_temp_rec.clearing_zone_id := L_rms_rec.clearing_zone_id;
               end if;
               if L_mi_rec.routing_loc_id_mi <> 'Y' then
                  L_temp_rec.routing_loc_id := L_rms_rec.routing_loc_id;
               end if;
               if L_mi_rec.master_po_no_mi <> 'Y' then
                  L_temp_rec.master_po_no := L_rms_rec.master_po_no;
               end if;
            end if;
            --Resolve new value for action column.
            if L_stg_exists then
               --if it is a create record and record already exists in staging, log an error
               if L_temp_rec.action = PO_INDUCT_SQL.ACTION_NEW then
                  if L_stg_rec.process$status = 'P' then
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS',L_pk_columns);
                  else
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',L_pk_columns);
                  end if;
                  WRITE_S9T_ERROR(I_file_id,OHE_sheet,L_temp_rec.row_seq,NULL,NULL,L_error_msg);
                  L_error := TRUE;
               else
                  --if record does not exist in staging then keep the user provided value
                  L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,L_temp_rec.action);
               end if;
            end if;
         EXCEPTION
            when RECORD_LOCKED then
               L_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                 L_table,
                                                 NULL,
                                                 NULL);
               WRITE_S9T_ERROR(I_file_id,OHE_sheet,L_stg_rec.row_seq,NULL,NULL,L_error_msg);
               L_error := TRUE;
         END;
      end if; --if no L_error

      -- If order_no and ext_ref_no are the same, log a warning and NULL out the ext_ref_no field
      if L_temp_rec.order_no is NOT NULL and
         L_temp_rec.order_no = L_temp_rec.ext_ref_no then
         WRITE_S9T_ERROR(I_file_id,
                         OHE_sheet,
                         L_temp_rec.row_seq,
                         'EXT_REF_NO',
                         NULL,
                         'EXT_REF_NO_DEF_NULL',
                         'W');
         L_temp_rec.ext_ref_no := NULL;
         L_default_rec.ext_ref_no := NULL;
      end if;


      --Apply defaults if it is a create record and record does not exist in staging already
      if rec.action = PO_INDUCT_SQL.ACTION_NEW and
         NOT L_error and
         NOT L_stg_exists then
         L_temp_rec.order_no             := NVL(L_temp_rec.order_no,L_temp_rec.ext_ref_no);
         L_temp_rec.order_type           := NVL(L_temp_rec.order_type,L_default_rec.order_type);
         L_temp_rec.dept                 := NVL(L_temp_rec.dept,L_default_rec.dept);
         L_temp_rec.buyer                := NVL(L_temp_rec.buyer,L_default_rec.buyer);
         L_temp_rec.supplier             := NVL(L_temp_rec.supplier,L_default_rec.supplier);
         L_temp_rec.loc_type             := NVL(L_temp_rec.loc_type,L_default_rec.loc_type);
         L_temp_rec.location             := NVL(L_temp_rec.location,L_default_rec.location);
         L_temp_rec.promotion            := NVL(L_temp_rec.promotion,L_default_rec.promotion);
         L_temp_rec.qc_ind               := NVL(L_temp_rec.qc_ind,L_default_rec.qc_ind);
         L_temp_rec.written_date         := NVL(L_temp_rec.written_date,L_default_rec.written_date);
         L_temp_rec.not_before_date      := NVL(L_temp_rec.not_before_date,L_default_rec.not_before_date);
         L_temp_rec.not_after_date       := NVL(L_temp_rec.not_after_date,L_default_rec.not_after_date);
         L_temp_rec.otb_eow_date         := NVL(L_temp_rec.otb_eow_date,L_default_rec.otb_eow_date);
         L_temp_rec.earliest_ship_date   := NVL(L_temp_rec.earliest_ship_date,NVL(L_default_rec.earliest_ship_date, L_temp_rec.not_before_date));
         L_temp_rec.latest_ship_date     := NVL(L_temp_rec.latest_ship_date,L_default_rec.latest_ship_date);
         L_temp_rec.terms                := NVL(L_temp_rec.terms,L_default_rec.terms);
         L_temp_rec.freight_terms        := NVL(L_temp_rec.freight_terms,L_default_rec.freight_terms);
         L_temp_rec.orig_ind             := NVL(L_temp_rec.orig_ind,L_default_rec.orig_ind);
         L_temp_rec.payment_method       := NVL(L_temp_rec.payment_method,L_default_rec.payment_method);
         L_temp_rec.backhaul_type        := NVL(L_temp_rec.backhaul_type,L_default_rec.backhaul_type);
         L_temp_rec.backhaul_allowance   := NVL(L_temp_rec.backhaul_allowance,L_default_rec.backhaul_allowance);
         L_temp_rec.ship_method          := NVL(L_temp_rec.ship_method,L_default_rec.ship_method);
         L_temp_rec.purchase_type        := NVL(L_temp_rec.purchase_type,L_default_rec.purchase_type);
         L_temp_rec.status               := NVL(L_temp_rec.status,L_default_rec.status);
         L_temp_rec.ship_pay_method      := NVL(L_temp_rec.ship_pay_method,L_default_rec.ship_pay_method);
         L_temp_rec.fob_trans_res        := NVL(L_temp_rec.fob_trans_res,L_default_rec.fob_trans_res);
         L_temp_rec.fob_trans_res_desc   := NVL(L_temp_rec.fob_trans_res_desc,L_default_rec.fob_trans_res_desc);
         L_temp_rec.fob_title_pass       := NVL(L_temp_rec.fob_title_pass,L_default_rec.fob_title_pass);
         L_temp_rec.fob_title_pass_desc  := NVL(L_temp_rec.fob_title_pass_desc,L_default_rec.fob_title_pass_desc);
         L_temp_rec.edi_po_ind           := NVL(L_temp_rec.edi_po_ind,L_default_rec.edi_po_ind);
         L_temp_rec.import_order_ind     := NVL(L_temp_rec.import_order_ind,L_default_rec.import_order_ind);
         L_temp_rec.import_country_id    := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.include_on_order_ind := NVL(L_temp_rec.include_on_order_ind,L_default_rec.include_on_order_ind);
         L_temp_rec.vendor_order_no      := NVL(L_temp_rec.vendor_order_no,L_default_rec.vendor_order_no);
         L_temp_rec.exchange_rate        := NVL(L_temp_rec.exchange_rate,L_default_rec.exchange_rate);
         L_temp_rec.factory              := NVL(L_temp_rec.factory,L_default_rec.factory);
         L_temp_rec.agent                := NVL(L_temp_rec.agent,L_default_rec.agent);
         L_temp_rec.discharge_port       := NVL(L_temp_rec.discharge_port,L_default_rec.discharge_port);
         L_temp_rec.lading_port          := NVL(L_temp_rec.lading_port,L_default_rec.lading_port);
         L_temp_rec.freight_contract_no  := NVL(L_temp_rec.freight_contract_no,L_default_rec.freight_contract_no);
         L_temp_rec.po_type              := NVL(L_temp_rec.po_type,L_default_rec.po_type);
         L_temp_rec.pre_mark_ind         := NVL(L_temp_rec.pre_mark_ind,L_default_rec.pre_mark_ind);
         L_temp_rec.currency_code        := NVL(L_temp_rec.currency_code,L_default_rec.currency_code);
         L_temp_rec.reject_code          := NVL(L_temp_rec.reject_code,L_default_rec.reject_code);
         L_temp_rec.contract_no          := NVL(L_temp_rec.contract_no,L_default_rec.contract_no);
         L_temp_rec.pickup_loc           := NVL(L_temp_rec.pickup_loc,L_default_rec.pickup_loc);
         L_temp_rec.pickup_no            := NVL(L_temp_rec.pickup_no,L_default_rec.pickup_no);
         L_temp_rec.pickup_date          := NVL(L_temp_rec.pickup_date,L_default_rec.pickup_date);
         L_temp_rec.comment_desc         := NVL(L_temp_rec.comment_desc,L_default_rec.comment_desc);
         L_temp_rec.partner_type_1       := NVL(L_temp_rec.partner_type_1,L_default_rec.partner_type_1);
         L_temp_rec.partner1             := NVL(L_temp_rec.partner1,L_default_rec.partner1);
         L_temp_rec.partner_type_2       := NVL(L_temp_rec.partner_type_2,L_default_rec.partner_type_2);
         L_temp_rec.partner2             := NVL(L_temp_rec.partner2,L_default_rec.partner2);
         L_temp_rec.partner_type_3       := NVL(L_temp_rec.partner_type_3,L_default_rec.partner_type_3);
         L_temp_rec.partner3             := NVL(L_temp_rec.partner3,L_default_rec.partner3);
         L_temp_rec.import_type          := NVL(L_temp_rec.import_type,L_default_rec.import_type);
         L_temp_rec.import_id            := NVL(L_temp_rec.import_id,L_default_rec.import_id);
         L_temp_rec.clearing_zone_id     := NVL(L_temp_rec.clearing_zone_id,L_default_rec.clearing_zone_id);
         L_temp_rec.routing_loc_id       := NVL(L_temp_rec.routing_loc_id,L_default_rec.routing_loc_id);
         L_temp_rec.ext_ref_no           := NVL(L_temp_rec.ext_ref_no,L_default_rec.ext_ref_no);
         L_temp_rec.master_po_no         := NVL(L_temp_rec.master_po_no,L_default_rec.master_po_no);
         L_temp_rec.re_approve_ind       := NVL(L_temp_rec.re_approve_ind,L_default_rec.re_approve_ind);
         L_temp_rec.next_upd_id          := NVL(L_temp_rec.next_upd_id,L_default_rec.next_upd_id);
      end if;
      -- Check PK cols. Log error if any PK col is null.
      if L_temp_rec.order_no is NULL and
         L_temp_rec.ext_ref_no is NULL then
         L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                           L_pk_columns);
            WRITE_S9T_ERROR(I_file_id,
                            OHE_sheet,
                            rec.row_seq,
                            NULL,
                            NULL,
                            L_error_msg);
          L_error := TRUE;
      end if;

      --if no error so far then add temp record to insert or update collection depending upon
      --whether record already exists in staging or not.
      if NOT L_error then
         if L_stg_exists then
            svc_upd_col.EXTEND();
            svc_upd_col(svc_upd_col.COUNT()) := L_temp_rec;
         else
            svc_ins_col.EXTEND();
            svc_ins_col(svc_ins_col.COUNT()) := L_temp_rec;
         end if;
      end if;
   END LOOP;

   --flush insert collection
   BEGIN
      FORALL i IN 1..svc_ins_col.COUNT SAVE EXCEPTIONS
         insert into svc_ordhead
              values svc_ins_col(i);
   EXCEPTION
      when DML_ERRORS then
         FOR i in 1..SQL%BULK_EXCEPTIONS.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                 L_pk_columns,
                                                 L_table);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            OHE_sheet,
                            svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
   --flush update collection
   FORALL i IN 1..svc_upd_col.COUNT
      update svc_ordhead
         set row = svc_upd_col(i)
       where order_no = svc_upd_col(i).order_no;

END PROCESS_S9T_OHE;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ODT(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_ORDDETAIL.PROCESS_ID%TYPE) IS

   TYPE SVC_ODT_COL_TYP IS TABLE OF SVC_ORDDETAIL%ROWTYPE;
   L_temp_rec      SVC_ORDDETAIL%ROWTYPE;
   svc_upd_col     SVC_ODT_COL_TYP := NEW SVC_ODT_COL_TYP();
   svc_ins_col     SVC_ODT_COL_TYP := NEW SVC_ODT_COL_TYP();
   L_process_id    SVC_ORDDETAIL.PROCESS_ID%TYPE;
   L_error         BOOLEAN := FALSE;
   L_default_rec   SVC_ORDDETAIL%ROWTYPE;
   L_table         VARCHAR2(30) := 'SVC_ORDDETAIL';
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   L_stg_exists    BOOLEAN;
   L_pk_columns    VARCHAR2(255) := 'order number, item, location type, location, delivery date, processing type, processing warehouse and allocation number';
   L_min_cols      VARCHAR2(255) := 'order number, item, location type and location';
   L_min_ud_cols   VARCHAR2(255) := 'order number and item';
   L_error_code    NUMBER;
   L_count_alloc   NUMBER;

   cursor C_MANDATORY_IND is
      select order_no_mi,
             item_mi,
             item_desc_mi,
             vpn_mi,
             item_parent_mi,
             ref_item_mi,
             diff_1_mi,
             diff_2_mi,
             diff_3_mi,
             diff_4_mi,
             origin_country_id_mi,
             supp_pack_size_mi,
             loc_type_mi,
             location_mi,
             location_desc_mi,
             unit_cost_mi,
             qty_transferred_mi,
             delivery_date_mi,
             non_scale_ind_mi,
             processing_TYPE_mi,
             processing_wh_mi,
             alloc_no_mi,
             qty_cancelled_mi,
             qty_ordered_mi,
             cancel_code_mi,
             release_date_mi,
             in_store_date_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = PO_INDUCT_SQL.TEMPLATE_KEY
                 and wksht_key    = 'ORDDETAIL')
       pivot (MAX(mandatory) AS mi FOR (column_key) IN ('ORDER_NO'          AS ORDER_NO,
                                                        'ITEM'              AS ITEM,
                                                        'ITEM_DESC'         AS ITEM_DESC,
                                                        'VPN'               AS VPN,
                                                        'ITEM_PARENT'       AS ITEM_PARENT,
                                                        'REF_ITEM'          AS REF_ITEM,
                                                        'DIFF_1'            AS DIFF_1,
                                                        'DIFF_2'            AS DIFF_2,
                                                        'DIFF_3'            AS DIFF_3,
                                                        'DIFF_4'            AS DIFF_4,
                                                        'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID,
                                                        'SUPP_PACK_SIZE'    AS SUPP_PACK_SIZE,
                                                        'LOC_TYPE'          AS LOC_TYPE,
                                                        'LOCATION'          AS LOCATION,
                                                        'LOCATION_DESC'     AS LOCATION_DESC,
                                                        'UNIT_COST'         AS UNIT_COST,
                                                        'QTY_ORDERED'       AS QTY_ORDERED,
                                                        'DELIVERY_DATE'     AS DELIVERY_DATE,
                                                        'NON_SCALE_IND'     AS NON_SCALE_IND,
                                                        'PROCESSING_TYPE'   AS PROCESSING_TYPE,
                                                        'PROCESSING_WH'     AS PROCESSING_WH,
                                                        'ALLOC_NO'          AS ALLOC_NO,
                                                        'QTY_CANCELLED'     AS QTY_CANCELLED,
                                                        'QTY_TRANSFERRED'   AS QTY_TRANSFERRED,
                                                        'CANCEL_CODE'       AS CANCEL_CODE,
                                                        'RELEASE_DATE'      AS RELEASE_DATE,
                                                        'IN_STORE_DATE'     AS IN_STORE_DATE));
   L_mi_rec     C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS   EXCEPTION;
   PRAGMA       EXCEPTION_INIT(DML_ERRORS, -24381);
   L_odt_idx    VARCHAR2(50);
   L_odt_idx2   VARCHAR2(50);
   TYPE ODT_TBL is TABLE OF SVC_ORDDETAIL.ORDER_NO%TYPE INDEX BY VARCHAR2(50);
   L_odt_tab    ODT_TBL;

BEGIN
   --Get default values.
   FOR rec IN (select order_no_dv,
                      item_dv,
                      item_desc_dv,
                      vpn_dv,
                      item_parent_dv,
                      ref_item_dv,
                      diff_1_dv,
                      diff_2_dv,
                      diff_3_dv,
                      diff_4_dv,
                      UPPER(origin_country_id_dv) as origin_country_id_dv,
                      supp_pack_size_dv,
                      loc_type_dv,
                      location_dv,
                      location_desc_dv,
                      unit_cost_dv,
                      qty_ordered_dv,
                      delivery_date_dv,
                      non_scale_ind_dv,
                      processing_type_dv,
                      processing_wh_dv,
                      alloc_no_dv,
                      qty_cancelled_dv,
                      qty_transferred_dv,
                      cancel_code_dv,
                      release_date_dv,
                      in_store_date_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = PO_INDUCT_SQL.TEMPLATE_KEY
                          and wksht_key = 'ORDDETAIL')
                pivot (MAX(default_value) AS dv FOR (column_key) IN ('ORDER_NO'          AS ORDER_NO,
                                                                     'ITEM'              AS ITEM,
                                                                     'ITEM_DESC'         AS ITEM_DESC,
                                                                     'VPN'               AS VPN,
                                                                     'ITEM_PARENT'       AS ITEM_PARENT,
                                                                     'REF_ITEM'          AS REF_ITEM,
                                                                     'DIFF_1'            AS DIFF_1,
                                                                     'DIFF_2'            AS DIFF_2,
                                                                     'DIFF_3'            AS DIFF_3,
                                                                     'DIFF_4'            AS DIFF_4,
                                                                     'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID,
                                                                     'SUPP_PACK_SIZE'    AS SUPP_PACK_SIZE,
                                                                     'LOC_TYPE'          AS LOC_TYPE,
                                                                     'LOCATION'          AS LOCATION,
                                                                     'LOCATION_DESC'     AS LOCATION_DESC,
                                                                     'UNIT_COST'         AS UNIT_COST,
                                                                     'QTY_ORDERED'       AS QTY_ORDERED,
                                                                     'DELIVERY_DATE'     AS DELIVERY_DATE,
                                                                     'NON_SCALE_IND'     AS NON_SCALE_IND,
                                                                     'PROCESSING_TYPE'   AS PROCESSING_TYPE,
                                                                     'PROCESSING_WH'     AS PROCESSING_WH,
                                                                     'ALLOC_NO'          AS ALLOC_NO,
                                                                     'QTY_CANCELLED'     AS QTY_CANCELLED,
                                                                     'QTY_TRANSFERRED'   AS QTY_TRANSFERRED,
                                                                     'CANCEL_CODE'       AS CANCEL_CODE,
                                                                     'RELEASE_DATE'      AS RELEASE_DATE,
                                                                     'IN_STORE_DATE'     AS IN_STORE_DATE)))
   LOOP
      BEGIN
         L_default_rec.order_no := rec.order_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'ORDER_NO','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.item := rec.item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'ITEM','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.item_desc := rec.item_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'ITEM_DESC','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.vpn := rec.vpn_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'VPN','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.item_parent := rec.item_parent_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'ITEM_PARENT','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.ref_item := rec.ref_item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'REF_ITEM','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.diff_1 := rec.diff_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'DIFF_1','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.diff_2 := rec.diff_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'DIFF_2','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.diff_3 := rec.diff_3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'DIFF_3','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.diff_4 := rec.diff_4_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'DIFF_4','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.origin_country_id := rec.origin_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'ORIGIN_COUNTRY_ID','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.supp_pack_size := rec.supp_pack_size_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'SUPP_PACK_SIZE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.loc_type := rec.loc_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'LOC_TYPE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.location := rec.location_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'LOCATION','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.location_desc := rec.location_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'LOCATION_DESC','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.unit_cost := rec.unit_cost_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'UNIT_COST','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.qty_ordered := rec.qty_ordered_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'QTY_ORDERED','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.qty_transferred := rec.qty_transferred_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'QTY_TRANSFERRED','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.delivery_date := rec.delivery_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'DELIVERY_DATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.non_scale_ind := NVL(rec.non_scale_ind_dv,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'NON_SCALE_IND','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.processing_type := rec.processing_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'PROCESSING_TYPE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.processing_wh := rec.processing_wh_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'PROCESSING_WH','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.qty_cancelled := rec.qty_cancelled_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'QTY_CANCELLED','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.cancel_code := rec.cancel_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'CANCEL_CODE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.release_date := rec.release_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'RELEASE_DATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.in_store_date := rec.in_store_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,NULL,'RELEASE_DATE','INV_DEFAULT',SQLERRM);
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;

   FOR rec IN (select r.get_cell(ODT$action)            AS action,
                      r.get_cell(ODT$order_no)          AS order_no,
                      r.get_cell(ODT$item)              AS item,
                      r.get_cell(ODT$item_desc)         AS item_desc,
                      r.get_cell(ODT$vpn)               AS vpn,
                      r.get_cell(ODT$item_parent)       AS item_parent,
                      r.get_cell(ODT$ref_item)          AS ref_item,
                      r.get_cell(ODT$diff_1)            AS diff_1,
                      r.get_cell(ODT$diff_2)            AS diff_2,
                      r.get_cell(ODT$diff_3)            AS diff_3,
                      r.get_cell(ODT$diff_4)            AS diff_4,
                      UPPER(r.get_cell(ODT$origin_country_id)) AS origin_country_id,
                      r.get_cell(ODT$supp_pack_size)    AS supp_pack_size,
                      r.get_cell(ODT$loc_type)          AS loc_type,
                      r.get_cell(ODT$location)          AS location,
                      r.get_cell(ODT$location_desc)     AS location_desc,
                      r.get_cell(ODT$unit_cost)         AS unit_cost,
                      r.get_cell(ODT$qty_ordered)       AS qty_ordered,
                      r.get_cell(ODT$delivery_date)     AS delivery_date,
                      r.get_cell(ODT$non_scale_ind)     AS non_scale_ind,
                      r.get_cell(ODT$processing_type)   AS processing_type,
                      r.get_cell(ODT$processing_wh)     AS processing_wh,
                      r.get_cell(ODT$alloc_no)          AS alloc_no,
                      r.get_cell(ODT$qty_cancelled)     AS qty_cancelled,
                      r.get_cell(ODT$qty_transferred)   AS qty_transferred,
                      r.get_cell(ODT$cancel_code)       AS cancel_code,
                      r.get_cell(ODT$release_date)      AS release_date,
                      r.get_cell(ODT$in_store_date)     AS in_store_date,
                      r.get_row_seq()                   AS row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(ODT_sheet))
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := sysdate;
      L_temp_rec.last_upd_datetime := sysdate;
      L_error                      := FALSE;

      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
             WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
             L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.order_no := rec.order_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'ORDER_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item := rec.item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item_desc := rec.item_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'ITEM_DESC',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.vpn := rec.vpn;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'VPN',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item_parent := rec.item_parent;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'ITEM_PARENT',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ref_item := rec.ref_item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'REF_ITEM',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.diff_1 := rec.diff_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'DIFF_1',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.diff_2 := rec.diff_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'DIFF_2',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.diff_3 := rec.diff_3;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'DIFF_3',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.diff_4 := rec.diff_4;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'DIFF_4',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.origin_country_id := rec.origin_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'ORIGIN_COUNTRY_ID',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.supp_pack_size := rec.supp_pack_size;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'SUPP_PACK_SIZE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.loc_type := rec.loc_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'LOC_TYPE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.location := rec.location;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'LOCATION',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.location_desc := rec.location_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'LOCATION_DESC',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.unit_cost := rec.unit_cost;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'UNIT_COST',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.qty_ordered := rec.qty_ordered;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'QTY_ORDERED',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.qty_transferred := rec.qty_transferred;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'QTY_TRANSFERRED',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.delivery_date := rec.delivery_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'DELIVERY_DATE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.non_scale_ind := NVL(rec.non_scale_ind,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'NON_SCALE_IND',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.processing_type := rec.processing_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'PROCESSING_TYPE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.processing_wh := rec.processing_wh;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'PROCESSING_WH',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.alloc_no := rec.alloc_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'ALLOC_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.qty_cancelled := rec.qty_cancelled;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'QTY_CANCELLED',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.cancel_code := rec.cancel_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'CANCEL_CODE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.release_date := rec.release_date;
      EXCEPTION
         when OTHERS then
            L_error_msg := SQL_LIB.CREATE_MSG('INVALID_DATE',rec.release_date,LP_format);
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'RELEASE_DATE',NULL,L_error_msg);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.in_store_date := rec.in_store_date;
      EXCEPTION
         when OTHERS then
            L_error_msg := SQL_LIB.CREATE_MSG('INVALID_DATE',rec.in_store_date,LP_format);
            WRITE_S9T_ERROR(I_file_id,ODT_sheet,rec.row_seq,'IN_STORE_DATE',NULL,L_error_msg);
            L_error := TRUE;
      END;

      if NOT L_error then
         DECLARE
            L_stg_rec            SVC_ORDDETAIL%ROWTYPE;
            L_rms_exists         BOOLEAN;
            L_header_order_no    SVC_ORDHEAD.ORDER_NO%TYPE;
            L_header_ext_ref_no  SVC_ORDHEAD.EXT_REF_NO%TYPE;
            RECORD_LOCKED        EXCEPTION;
            PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

            cursor C_STG_REC is
               select *
                 from svc_orddetail
                where order_no                 = L_temp_rec.order_no
                  and item                     = L_temp_rec.item
                  and (location                = L_temp_rec.location or
                       (location is NULL and L_temp_rec.location is NULL))
                  and (loc_type                = L_temp_rec.loc_type or
                       (loc_type is NULL and L_temp_rec.loc_type is NULL))
                  and (processing_type         = L_temp_rec.processing_type or
                       (processing_type is NULL and L_temp_rec.processing_type is NULL))
                  and (processing_wh           = L_temp_rec.processing_wh or
                       (processing_wh is NULL and L_temp_rec.processing_wh is NULL))
                  and (delivery_date           = L_temp_rec.delivery_date or
                       (delivery_date is NULL and L_temp_rec.delivery_date is NULL))
                  and (alloc_no                = L_temp_rec.alloc_no or
                       (alloc_no is NULL and L_temp_rec.alloc_no is NULL));

            cursor C_RMS_REC is
               -- processing_type is NULL
               select os.order_no                              as order_no,
                      os.item                                  as item,
                      NULL                                     as item_desc,   --item_desc
                      NULL                                     as vpn,         --VPN
                      NULL                                     as item_parent, --item_parent
                      os.ref_item                              as ref_item,
                      NULL                                     as diff_1,
                      NULL                                     as diff_2,
                      NULL                                     as diff_3,
                      NULL                                     as diff_4,
                      UPPER(os.origin_country_id)              as origin_country_id,
                      os.supp_pack_size                        as supp_pack_size,
                      ol.loc_type                              as loc_type,
                      ol.location                              as location,
                      NULL                                     as location_desc,
                      ol.unit_cost                             as unit_cost,
                      ol.qty_ordered                           as qty_ordered,
                      oh.not_after_date                        as not_after_date,
                      ol.non_scale_ind                         as non_scale_ind,
                      NULL                                     as processing_type,
                      NULL                                     as processing_wh,
                      NULL                                     as alloc_no,
                      NULL                                     as qty_cancelled,
                      NULL                                     as qty_transferred,
                      ol.cancel_code                           as cancel_code,
                      NULL                                     as release_date,
                      NULL                                     as in_store_date
                 from ordloc ol,
                      ordsku os,
                      ordhead oh
                where oh.order_no = os.order_no
                  and os.order_no = ol.order_no(+)
                  and NVL(os.order_no, -999) = NVL(L_temp_rec.order_no, -999)
                  and NVL(os.item, '-999')   = NVL(L_temp_rec.item, '-999')
                  and (L_temp_rec.processing_type is NULL and NVL(ol.location, -999) = NVL(L_temp_rec.location, -999)
                        or
                       (L_temp_rec.processing_type = CORESVC_PO.CONSOLIDATION and NVL(ol.location, -999) = NVL(L_temp_rec.processing_wh, -999)) )
                union all
               select os.order_no                              as order_no,
                      os.item                                  as item,
                      NULL                                     as item_desc,   --item_desc
                      NULL                                     as vpn,         --VPN
                      NULL                                     as item_parent, --item_parent
                      os.ref_item                              as ref_item,
                      NULL                                     as diff_1,
                      NULL                                     as diff_2,
                      NULL                                     as diff_3,
                      NULL                                     as diff_4,
                      UPPER(os.origin_country_id)              as origin_country_id,
                      os.supp_pack_size                        as supp_pack_size,
                      'W'                                      as loc_type,
                      ah.wh                                    as location,
                      NULL                                     as location_desc,
                      ol.unit_cost                             as unit_cost,
                      ol.qty_ordered                           as qty_ordered,
                      oh.not_after_date                        as not_after_date,
                      ol.non_scale_ind                         as non_scale_ind,
                      NULL                                     as processing_type,
                      NULL                                     as processing_wh,
                      ah.alloc_no                              as alloc_no,
                      ad.qty_cancelled                         as qty_cancelled,
                      ad.qty_transferred                       as qty_transferred,
                      ol.cancel_code                           as cancel_code,
                      ah.release_date                          as release_date,
                      ad.in_store_date                         as in_store_date
                 from ordloc ol,
                      ordsku os,
                      ordhead oh,
                      alloc_header ah,
                      alloc_detail ad
                where oh.order_no = os.order_no
                  and os.order_no = ol.order_no
                  and os.order_no = ah.order_no
                  and os.item = ah.item
                  and os.order_no = NVL(L_temp_rec.order_no, -999)
                  and os.item = NVL(L_temp_rec.item, '-999')
                  and ol.location = NVL(L_temp_rec.processing_wh, -999)
                  and ad.to_loc = NVL(L_temp_rec.location, -999)
                  and ad.to_loc_type = NVL(L_temp_rec.loc_type, '-999')
                  and NVL(L_temp_rec.processing_type, 'C') = CORESVC_PO.CROSS_DOCK;

            cursor C_TRAN_ITEM is
               select item_parent
                 from item_master
                where item = L_temp_rec.ref_item;

            cursor C_GET_ALLOC_WH is
               select ah.wh
                 from alloc_header ah
                where ah.alloc_no = L_temp_rec.alloc_no
                  and ah.order_no = L_temp_rec.order_no
                  and ah.item = L_temp_rec.item;

            cursor C_GET_ALLOC is
               select ah.alloc_no,
                      count(*) over (partition by ah.order_no, ad.to_loc, ah.wh, ah.item)
                 from alloc_header ah,
                      alloc_detail ad
                where ah.alloc_no = ad.alloc_no
                  and ah.order_no = L_temp_rec.order_no
                  and ad.to_loc = L_temp_rec.location
                  and ah.wh = L_temp_rec.processing_wh
                  and ah.item = L_temp_rec.item
                  and L_temp_rec.order_no is NOT NULL;

            cursor C_GET_HEADER_REC is
               select order_no,
                      ext_ref_no
                 from svc_ordhead
                where process_id = I_process_id;

            cursor C_LOCK_SVC_ORDDETAIL is
               select 'x'
                 from svc_orddetail
                where process_id = L_stg_rec.process_id
                  and row_seq = L_stg_rec.row_seq
                  for update nowait;

            cursor C_STG_REC_UD_COLS is
               select *
                 from svc_orddetail
                where order_no = L_temp_rec.order_no
                  and item = L_temp_rec.item
                  and (location = L_temp_rec.location or L_temp_rec.location is NULL)
                  and (loc_type = L_temp_rec.loc_type or L_temp_rec.loc_type is NULL)
                  and (processing_type = L_temp_rec.processing_type or L_temp_rec.processing_type is NULL)
                  and (processing_wh = L_temp_rec.processing_wh or L_temp_rec.processing_wh is NULL)
                  and (delivery_date = L_temp_rec.delivery_date or L_temp_rec.delivery_date is NULL)
                  and (alloc_no = L_temp_rec.alloc_no or L_temp_rec.alloc_no is NULL);

            L_rms_rec   C_RMS_REC%ROWTYPE;
            TYPE SVC_ORD_TBL is TABLE OF C_STG_REC_UD_COLS%ROWTYPE INDEX BY BINARY_INTEGER;
            L_stg_rec_ud     SVC_ORD_TBL;

         BEGIN
            --get tran-level item if only reference item is populated.
            if L_temp_rec.item is NULL and L_temp_rec.ref_item is NOT NULL then
               open C_TRAN_ITEM;
               fetch C_TRAN_ITEM into L_temp_rec.item;
               close C_TRAN_ITEM;
               if L_temp_rec.item is NOT NULL then
                  L_error_msg := SQL_LIB.CREATE_MSG('POIND_FIELDS_DEFAULTED','ITEM');
                  WRITE_S9T_ERROR(I_file_id,ODT_sheet,L_temp_rec.row_seq,'ITEM',NULL,L_error_msg,'W');
               end if;
            end if;

            -- default only if the order number is not to be auto generated
            open C_GET_HEADER_REC;
            fetch C_GET_HEADER_REC into L_header_order_no,
                                        L_header_ext_ref_no;
            close C_GET_HEADER_REC;

            if L_header_order_no is NULL or L_header_order_no <> NVL(L_header_ext_ref_no, '-999') then
               --populate the processing_wh and processing_type if only alloc_no is populated
               if L_temp_rec.alloc_no is NOT NULL and
                  L_temp_rec.processing_wh is NULL then
                  open C_GET_ALLOC_WH;
                  fetch C_GET_ALLOC_WH into L_temp_rec.processing_wh;
                  close C_GET_ALLOC_WH;
                  if L_temp_rec.processing_wh is NOT NULL then
                     L_temp_rec.processing_type := CORESVC_PO.CROSS_DOCK;
                     L_error_msg := SQL_LIB.CREATE_MSG('POIND_FIELDS_DEFAULTED','PROCESSING_WH');
                     WRITE_S9T_ERROR(I_file_id,ODT_sheet,L_temp_rec.row_seq,'PROCESSING_WH',NULL,L_error_msg,'W');
                     L_error_msg := SQL_LIB.CREATE_MSG('POIND_FIELDS_DEFAULTED','PROCESSING_TYPE');
                     WRITE_S9T_ERROR(I_file_id,ODT_sheet,L_temp_rec.row_seq,'PROCESSING_TYPE',NULL,L_error_msg,'W');
                  end if;
               end if;

               --populate the alloc_no if processing_wh and procesing_type are populated
               if L_temp_rec.alloc_no is NULL and
                  L_temp_rec.processing_type = CORESVC_PO.CROSS_DOCK and
                  L_temp_rec.processing_wh is NOT NULL then
                  open C_GET_ALLOC;
                  fetch C_GET_ALLOC into L_temp_rec.alloc_no,
                                         L_count_alloc;
                  close C_GET_ALLOC;
                  if L_count_alloc <> 1 or L_count_alloc is NULL then
                     L_temp_rec.alloc_no := NULL;
                  else
                     if L_temp_rec.alloc_no is NOT NULL then
                        L_error_msg := SQL_LIB.CREATE_MSG('POIND_FIELDS_DEFAULTED','ALLOC_NO');
                        WRITE_S9T_ERROR(I_file_id,ODT_sheet,L_temp_rec.row_seq,'ALLOC_NO',NULL,L_error_msg,'W');
                     end if;
                  end if;
               end if;
            end if;

            open C_STG_REC;
            fetch C_STG_REC into L_stg_rec;
            if C_STG_REC%FOUND then
               L_stg_exists := TRUE;
            else
               L_stg_exists := FALSE;
            end if;
            close C_STG_REC;

            open C_RMS_REC;
            fetch C_RMS_REC into L_rms_rec;
            if C_RMS_REC%FOUND then
               L_rms_exists := TRUE;
            else
               L_rms_exists := FALSE;
            end if;
            close C_RMS_REC;

            if L_stg_exists then
               --delete record from staging if it is already uploaded to RMS.
               if L_stg_rec.process$status = 'P' and
                  GET_PRC_DEST(L_error_msg,
                               L_stg_rec.process_id) = 'RMS' then
                     open C_LOCK_SVC_ORDDETAIL;
                     close C_LOCK_SVC_ORDDETAIL;
                     delete from svc_orddetail
                           where process_id = L_stg_rec.process_id
                             and row_seq    = L_stg_rec.row_seq;
                     L_stg_exists := FALSE;
               end if;

               if L_stg_rec.process$status in ('N', 'E') and
                  NOT L_rms_exists and
                  L_stg_rec.action = PO_INDUCT_SQL.ACTION_NEW and
                  L_temp_rec.action = PO_INDUCT_SQL.ACTION_DEL then
                     open C_LOCK_SVC_ORDDETAIL;
                     close C_LOCK_SVC_ORDDETAIL;
                     delete from svc_orddetail
                           where process_id = L_stg_rec.process_id
                             and row_seq    = L_stg_rec.row_seq;
                     ---
                     if L_temp_rec.location is NOT NULL then
                        L_odt_idx := L_temp_rec.order_no||'/'||L_temp_rec.item||'/'||L_temp_rec.location;
                     else
                        L_odt_idx := L_temp_rec.order_no||'/'||L_temp_rec.item;
                     end if;

                     if NOT(L_odt_tab.EXISTS(L_odt_idx)) then
                        LP_svc_po_tab.EXTEND();
                        LP_svc_po_tab(LP_svc_po_tab.COUNT()).order_no     := L_temp_rec.order_no;
                        LP_svc_po_tab(LP_svc_po_tab.COUNT()).item         := L_temp_rec.item;
                        LP_svc_po_tab(LP_svc_po_tab.COUNT()).location     := L_temp_rec.location;
                        LP_svc_po_tab(LP_svc_po_tab.COUNT()).svc_tbl_name := L_table;
                        L_odt_tab(L_odt_idx) := L_temp_rec.order_no;
                     end if;
                     continue;
               end if;
            elsif L_temp_rec.action = PO_INDUCT_SQL.ACTION_DEL then
               open C_STG_REC_UD_COLS;
               fetch C_STG_REC_UD_COLS bulk collect into L_stg_rec_ud;
               close C_STG_REC_UD_COLS;
               ---
               if L_stg_rec_ud is NOT NULL and L_stg_rec_ud.COUNT > 0 then
                  for i in L_stg_rec_ud.FIRST..L_stg_rec_ud.LAST loop
                     if L_stg_rec_ud(i).process$status in ('N', 'E') and
                        NOT L_rms_exists and
                        L_stg_rec_ud(i).action = PO_INDUCT_SQL.ACTION_NEW then
                           L_stg_rec.process_id := L_stg_rec_ud(i).process_id;
                           L_stg_rec.row_seq := L_stg_rec_ud(i).row_seq;
                           open C_LOCK_SVC_ORDDETAIL;
                           close C_LOCK_SVC_ORDDETAIL;
                           delete from svc_orddetail
                                 where process_id = L_stg_rec_ud(i).process_id
                                   and row_seq = L_stg_rec_ud(i).row_seq;
                           ---
                           if L_temp_rec.location is NOT NULL then
                              L_odt_idx := L_temp_rec.order_no||'/'||L_temp_rec.item||'/'||L_temp_rec.location;
                           else
                              L_odt_idx := L_temp_rec.order_no||'/'||L_temp_rec.item;
                           end if;
                           if NOT(L_odt_tab.EXISTS(L_odt_idx)) then
                              LP_svc_po_tab.EXTEND();
                              LP_svc_po_tab(LP_svc_po_tab.COUNT()).order_no     := L_temp_rec.order_no;
                              LP_svc_po_tab(LP_svc_po_tab.COUNT()).item         := L_temp_rec.item;
                              LP_svc_po_tab(LP_svc_po_tab.COUNT()).location     := L_temp_rec.location;
                              LP_svc_po_tab(LP_svc_po_tab.COUNT()).svc_tbl_name := L_table;
                              L_odt_tab(L_odt_idx) := L_temp_rec.order_no;
                           end if;
                     end if;
                  end loop;
                  continue;
               else
                  L_odt_idx := L_temp_rec.order_no||'/'||L_temp_rec.item||'/'||NVL(L_temp_rec.location,-999);
                  L_odt_idx2 := L_temp_rec.order_no||'/'||L_temp_rec.item;
                  if L_odt_tab.EXISTS(L_odt_idx) or L_odt_tab.EXISTS(L_odt_idx2) then
                     continue;
                  end if;
               end if;
            end if;

            --copy values for non-mandatory columns from staging.
            if L_stg_exists then
               if L_mi_rec.order_no_mi <> 'Y' then
                  L_temp_rec.order_no := L_stg_rec.order_no;
               end if;
               if L_mi_rec.item_mi <> 'Y' then
                  L_temp_rec.item := L_stg_rec.item;
               end if;
               if L_mi_rec.item_desc_mi <> 'Y' then
                  L_temp_rec.item_desc := L_stg_rec.item_desc;
               end if;
               if L_mi_rec.vpn_mi <> 'Y' then
                  L_temp_rec.vpn := L_stg_rec.vpn;
               end if;
               if L_mi_rec.item_parent_mi <> 'Y' then
                  L_temp_rec.item_parent := L_stg_rec.item_parent;
               end if;
               if L_mi_rec.ref_item_mi <> 'Y' then
                  L_temp_rec.ref_item := L_stg_rec.ref_item;
               end if;
               if L_mi_rec.diff_1_mi <> 'Y' then
                  L_temp_rec.diff_1 := L_stg_rec.diff_1;
               end if;
               if L_mi_rec.diff_2_mi <> 'Y' then
                  L_temp_rec.diff_2 := L_stg_rec.diff_2;
               end if;
               if L_mi_rec.diff_3_mi <> 'Y' then
                  L_temp_rec.diff_3 := L_stg_rec.diff_3;
               end if;
               if L_mi_rec.diff_4_mi <> 'Y' then
                  L_temp_rec.diff_4 := L_stg_rec.diff_4;
               end if;
               if L_mi_rec.origin_country_id_mi <> 'Y' then
                  L_temp_rec.origin_country_id := L_stg_rec.origin_country_id;
               end if;
               if L_mi_rec.supp_pack_size_mi <> 'Y' then
                  L_temp_rec.supp_pack_size := L_stg_rec.supp_pack_size;
               end if;
               if L_mi_rec.loc_TYPE_mi <> 'Y' then
                  L_temp_rec.loc_type := L_stg_rec.loc_type;
               end if;
               if L_mi_rec.location_mi <> 'Y' then
                  L_temp_rec.location := L_stg_rec.location;
               end if;
               if L_mi_rec.location_desc_mi <> 'Y' then
                  L_temp_rec.location_desc := L_stg_rec.location_desc;
               end if;
               if L_mi_rec.unit_cost_mi <> 'Y' then
                  L_temp_rec.unit_cost := L_stg_rec.unit_cost;
               end if;
               if L_mi_rec.qty_ordered_mi <> 'Y' then
                  L_temp_rec.qty_ordered := L_stg_rec.qty_ordered;
               end if;
               if L_mi_rec.qty_transferred_mi <> 'Y' then
                  L_temp_rec.qty_transferred := L_stg_rec.qty_transferred;
               end if;
               if L_mi_rec.delivery_date_mi <> 'Y' then
                  L_temp_rec.delivery_date := L_stg_rec.delivery_date;
               end if;
               if L_mi_rec.non_scale_ind_mi <> 'Y' then
                  L_temp_rec.non_scale_ind := L_stg_rec.non_scale_ind;
               end if;
               if L_mi_rec.processing_TYPE_mi <> 'Y' then
                  L_temp_rec.processing_type := L_stg_rec.processing_type;
               end if;
               if L_mi_rec.processing_wh_mi <> 'Y' then
                  L_temp_rec.processing_wh := L_stg_rec.processing_wh;
               end if;
               if L_mi_rec.alloc_no_mi <> 'Y' then
                  L_temp_rec.alloc_no := L_stg_rec.alloc_no;
               end if;
               if L_mi_rec.qty_cancelled_mi <> 'Y' then
                  L_temp_rec.qty_cancelled := L_stg_rec.qty_cancelled;
               end if;
               if L_mi_rec.cancel_code_mi <> 'Y' then
                  L_temp_rec.cancel_code := L_stg_rec.cancel_code;
               end if;
               if L_mi_rec.release_date_mi <> 'Y' then
                  L_temp_rec.release_date := L_stg_rec.release_date;
               end if;
               if L_mi_rec.in_store_date_mi <> 'Y' then
                  L_temp_rec.in_store_date := L_stg_rec.in_store_date;
               end if;
            end if;

            --if record exists in RMS but not in staging, then copy values for non-mandatory columns from RMS.
            if L_rms_exists and NOT L_stg_exists then
               if L_mi_rec.order_no_mi <> 'Y' then
                  L_temp_rec.order_no := L_rms_rec.order_no;
               end if;
               if L_mi_rec.item_mi <> 'Y' then
                  L_temp_rec.item := L_rms_rec.item;
               end if;
               if L_mi_rec.item_desc_mi <> 'Y' then
                  L_temp_rec.item_desc := L_rms_rec.item_desc;
               end if;
               if L_mi_rec.vpn_mi <> 'Y' then
                  L_temp_rec.vpn := L_rms_rec.vpn;
               end if;
               if L_mi_rec.item_parent_mi <> 'Y' then
                  L_temp_rec.item_parent := L_rms_rec.item_parent;
               end if;
               if L_mi_rec.ref_item_mi <> 'Y' then
                  L_temp_rec.ref_item := L_rms_rec.ref_item;
               end if;
               if L_mi_rec.diff_1_mi <> 'Y' then
                  L_temp_rec.diff_1 := L_rms_rec.diff_1;
               end if;
               if L_mi_rec.diff_2_mi <> 'Y' then
                  L_temp_rec.diff_2 := L_rms_rec.diff_2;
               end if;
               if L_mi_rec.diff_3_mi <> 'Y' then
                  L_temp_rec.diff_3 := L_rms_rec.diff_3;
               end if;
               if L_mi_rec.diff_4_mi <> 'Y' then
                  L_temp_rec.diff_4 := L_rms_rec.diff_4;
               end if;
               if L_mi_rec.origin_country_id_mi <> 'Y' then
                  L_temp_rec.origin_country_id := L_rms_rec.origin_country_id;
               end if;
               if L_mi_rec.supp_pack_size_mi <> 'Y' then
                  L_temp_rec.supp_pack_size := L_rms_rec.supp_pack_size;
               end if;
               if L_mi_rec.loc_type_mi <> 'Y' then
                  L_temp_rec.loc_type := L_rms_rec.loc_type;
               end if;
               if L_mi_rec.location_mi <> 'Y' then
                  L_temp_rec.location := L_rms_rec.location;
               end if;
               if L_mi_rec.location_desc_mi <> 'Y' then
                  L_temp_rec.location_desc := L_rms_rec.location_desc;
               end if;
               if L_mi_rec.unit_cost_mi <> 'Y' then
                  L_temp_rec.unit_cost := L_rms_rec.unit_cost;
               end if;
               if L_mi_rec.qty_ordered_mi <> 'Y' then
                  L_temp_rec.qty_ordered := L_rms_rec.qty_ordered;
               end if;
               if L_mi_rec.qty_transferred_mi <> 'Y' then
                  L_temp_rec.qty_transferred := L_rms_rec.qty_transferred;
               end if;
               if L_mi_rec.delivery_date_mi <> 'Y' then
                  L_temp_rec.delivery_date := L_rms_rec.not_after_date;
               end if;
               if L_mi_rec.non_scale_ind_mi <> 'Y' then
                  L_temp_rec.non_scale_ind := L_rms_rec.non_scale_ind;
               end if;
               if L_mi_rec.processing_TYPE_mi <> 'Y' then
                  L_temp_rec.processing_type := L_rms_rec.processing_type;
               end if;
               if L_mi_rec.processing_wh_mi <> 'Y' then
                  L_temp_rec.processing_wh := L_rms_rec.processing_wh;
               end if;
               if L_mi_rec.alloc_no_mi <> 'Y' then
                  L_temp_rec.alloc_no := L_rms_rec.alloc_no;
               end if;
               if L_mi_rec.qty_cancelled_mi <> 'Y' then
                  L_temp_rec.qty_cancelled := L_rms_rec.qty_cancelled;
               end if;
               if L_mi_rec.cancel_code_mi <> 'Y' then
                  L_temp_rec.cancel_code := L_rms_rec.cancel_code;
               end if;
               if L_mi_rec.release_date_mi <> 'Y' then
                  L_temp_rec.release_date := L_rms_rec.release_date;
               end if;
               if L_mi_rec.in_store_date_mi <> 'Y' then
                  L_temp_rec.in_store_date := L_rms_rec.in_store_date;
               end if;
                
            end if;

            --Resolve new value for action column.
            if L_stg_exists then
               --if it is a create record and record already exists in staging, log an error
               if L_temp_rec.action = PO_INDUCT_SQL.ACTION_NEW then
                  if L_stg_rec.process$status = 'P' then
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS',L_pk_columns);
                  else
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',L_pk_columns);
                  end if;
                  WRITE_S9T_ERROR(I_file_id,ODT_sheet,L_temp_rec.row_seq,NULL,NULL,L_error_msg);
                  L_error := TRUE;
               else
                  --if record does not exist in staging then keep the user provided value
                  L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,L_temp_rec.action);
               end if;
            end if;
         EXCEPTION
            when RECORD_LOCKED then
               L_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                 L_table,
                                                 NULL,
                                                 NULL);
               WRITE_S9T_ERROR(I_file_id,ODT_sheet,L_stg_rec.row_seq,NULL,NULL,L_error_msg);
               L_error := TRUE;
         END;
      end if; --if no L_error
      if rec.action = PO_INDUCT_SQL.ACTION_NEW and
         NOT L_error and
         NOT L_stg_exists then
         L_temp_rec.order_no          := NVL(L_temp_rec.order_no, L_default_rec.order_no);
         L_temp_rec.item              := NVL(L_temp_rec.item, L_default_rec.item);
         L_temp_rec.item_desc         := NVL(L_temp_rec.item_desc, L_default_rec.item_desc);
         L_temp_rec.vpn               := NVL(L_temp_rec.vpn, L_default_rec.vpn);
         L_temp_rec.item_parent       := NVL(L_temp_rec.item_parent, L_default_rec.item_parent);
         L_temp_rec.ref_item          := NVL(L_temp_rec.ref_item, L_default_rec.ref_item);
         L_temp_rec.diff_1            := NVL(L_temp_rec.diff_1, L_default_rec.diff_1);
         L_temp_rec.diff_2            := NVL(L_temp_rec.diff_2, L_default_rec.diff_2);
         L_temp_rec.diff_3            := NVL(L_temp_rec.diff_3, L_default_rec.diff_3);
         L_temp_rec.diff_4            := NVL(L_temp_rec.diff_4, L_default_rec.diff_4);
         L_temp_rec.origin_country_id := NVL(L_temp_rec.origin_country_id, L_default_rec.origin_country_id);
         L_temp_rec.supp_pack_size    := NVL(L_temp_rec.supp_pack_size, L_default_rec.supp_pack_size);
         L_temp_rec.loc_type          := NVL(L_temp_rec.loc_type, L_default_rec.loc_type);
         L_temp_rec.location          := NVL(L_temp_rec.location, L_default_rec.location);
         L_temp_rec.location_desc     := NVL(L_temp_rec.location_desc, L_default_rec.location_desc);
         L_temp_rec.unit_cost         := NVL(L_temp_rec.unit_cost, L_default_rec.unit_cost);
         L_temp_rec.qty_ordered       := NVL(L_temp_rec.qty_ordered, L_default_rec.qty_ordered);
         L_temp_rec.delivery_date     := NVL(L_temp_rec.delivery_date, L_default_rec.delivery_date);
         L_temp_rec.non_scale_ind     := NVL(L_temp_rec.non_scale_ind, L_default_rec.non_scale_ind);
         L_temp_rec.processing_type   := NVL(L_temp_rec.processing_type, L_default_rec.processing_type);
         L_temp_rec.processing_wh     := NVL(L_temp_rec.processing_wh, L_default_rec.processing_wh);
         L_temp_rec.alloc_no          := NVL(L_temp_rec.alloc_no, L_default_rec.alloc_no);
         L_temp_rec.qty_cancelled     := NVL(L_temp_rec.qty_cancelled, L_default_rec.qty_cancelled);
         L_temp_rec.qty_transferred   := NVL(L_temp_rec.qty_transferred, L_default_rec.qty_transferred);
         L_temp_rec.cancel_code       := NVL(L_temp_rec.cancel_code, L_default_rec.cancel_code);
         L_temp_rec.release_date      := NVL(L_temp_rec.release_date, L_default_rec.release_date);
         L_temp_rec.release_date      := NVL(L_temp_rec.in_store_date, L_default_rec.in_store_date);
         L_temp_rec.re_approve        := NULL;
         L_temp_rec.set_to_worksheet  := NULL;

         -- Check PK cols. Log error if any of these is null.
         if NOT (L_temp_rec.order_no is NOT NULL and
                 L_temp_rec.item is NOT NULL and
                 L_temp_rec.location is NOT NULL and
                 L_temp_rec.loc_type is NOT NULL) then
            L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                              L_min_cols);
            WRITE_S9T_ERROR(I_file_id,
                            ODT_sheet,
                            rec.row_seq,
                            NULL,
                            NULL,
                            L_error_msg);
            L_error := TRUE;
         end if;
      else
         -- Check PK cols. Log error if any of these is null.
         if NOT (L_temp_rec.order_no is NOT NULL and
                 L_temp_rec.item is NOT NULL) then
            L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                              L_min_ud_cols);
            WRITE_S9T_ERROR(I_file_id,
                            ODT_sheet,
                            rec.row_seq,
                            NULL,
                            NULL,
                            L_error_msg);
            L_error := TRUE;
         end if;
      end if;

      --if no error so far then add temp record to insert or update collection depending upon
      --whether record already exists in staging or not.
      if NOT L_error then
         if L_stg_exists then
            svc_upd_col.extend();
            svc_upd_col(svc_upd_col.count()) := L_temp_rec;
         else
            svc_ins_col.extend();
            svc_ins_col(svc_ins_col.count()) := L_temp_rec;
         end if;
      end if;
   END LOOP;

   --flush insert collection
   BEGIN
      FORALL i IN 1..svc_ins_col.COUNT SAVE EXCEPTIONS
         insert into svc_orddetail
              values svc_ins_col(i);
   EXCEPTION
      when DML_ERRORS then
         FOR i in 1..SQL%BULK_EXCEPTIONS.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                 L_pk_columns,
                                                 L_table);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            ODT_sheet,
                            svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;

   --flush update collection
   FORALL i IN 1..svc_upd_col.COUNT
      update svc_orddetail
         set row = svc_upd_col(i)
       where order_no = svc_upd_col(i).order_no
         and item = svc_upd_col(i).item
         and location = svc_upd_col(i).location
         and ((delivery_date is NULL and svc_upd_col(i).delivery_date is NULL) or
              (delivery_date = svc_upd_col(i).delivery_date))
         and NVL(processing_wh, -999) = NVL(svc_upd_col(i).processing_wh, -999)
         and NVL(processing_type, '-999') = NVL(svc_upd_col(i).processing_type, '-999')
         and NVL(alloc_no, -999) = NVL(svc_upd_col(i).alloc_no, -999);

END PROCESS_S9T_ODT;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ORD(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_ORDLC.PROCESS_ID%TYPE) IS

   TYPE SVC_ORD_COL_TYP IS TABLE OF SVC_ORDLC%ROWTYPE;
   L_temp_rec      SVC_ORDLC%ROWTYPE;
   svc_upd_col     SVC_ORD_COL_TYP := NEW SVC_ORD_COL_TYP();
   svc_ins_col     SVC_ORD_COL_TYP := NEW SVC_ORD_COL_TYP();
   L_process_id    SVC_ORDLC.PROCESS_ID%TYPE;
   L_error         BOOLEAN := FALSE;
   L_default_rec   SVC_ORDLC%ROWTYPE;
   L_table         VARCHAR2(30) := 'SVC_ORDLC';
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   L_stg_exists    BOOLEAN;
   L_pk_columns    VARCHAR2(255) := 'order number';
   L_error_code    NUMBER;

   cursor C_MANDATORY_IND is
      select order_no_mi,
             lc_ref_id_mi,
             lc_group_id_mi,
             applicant_mi,
             beneficiary_mi,
             merch_desc_mi,
             transshipment_ind_mi,
             partial_shipment_ind_mi,
             lc_ind_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = PO_INDUCT_SQL.TEMPLATE_KEY
                 and wksht_key    = 'ORDLC')
       pivot (MAX(mandatory) AS mi FOR (column_key) IN ('ORDER_NO'             AS ORDER_NO,
                                                        'LC_REF_ID'            AS LC_REF_ID,
                                                        'LC_GROUP_ID'          AS LC_GROUP_ID,
                                                        'APPLICANT'            AS APPLICANT,
                                                        'BENEFICIARY'          AS BENEFICIARY,
                                                        'MERCH_DESC'           AS MERCH_DESC,
                                                        'TRANSSHIPMENT_IND'    AS TRANSSHIPMENT_IND,
                                                        'PARTIAL_SHIPMENT_IND' AS PARTIAL_SHIPMENT_IND,
                                                        'LC_IND'               AS LC_IND,
                                                        NULL as dummy));
   L_mi_rec     C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS   EXCEPTION;
   PRAGMA       EXCEPTION_INIT(DML_ERRORS, -24381);

BEGIN
   --Get default values.
   FOR rec IN (select order_no_dv,
                      lc_ref_id_dv,
                      lc_group_id_dv,
                      applicant_dv,
                      beneficiary_dv,
                      merch_desc_dv,
                      transshipment_ind_dv,
                      partial_shipment_ind_dv,
                      lc_ind_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = PO_INDUCT_SQL.TEMPLATE_KEY
                          and wksht_key = 'ORDLC')
                pivot (MAX(default_value) AS dv FOR (column_key) IN ('ORDER_NO'             AS ORDER_NO,
                                                                     'LC_REF_ID'            AS LC_REF_ID,
                                                                     'LC_GROUP_ID'          AS LC_GROUP_ID,
                                                                     'APPLICANT'            AS APPLICANT,
                                                                     'BENEFICIARY'          AS BENEFICIARY,
                                                                     'MERCH_DESC'           AS MERCH_DESC,
                                                                     'TRANSSHIPMENT_IND'    AS TRANSSHIPMENT_IND,
                                                                     'PARTIAL_SHIPMENT_IND' AS PARTIAL_SHIPMENT_IND,
                                                                     'LC_IND' AS LC_IND,
                                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.order_no := rec.order_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,NULL,'ORDER_NO','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.lc_ref_id := rec.lc_ref_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,NULL,'LC_REF_ID','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.lc_group_id := rec.lc_group_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,NULL,'LC_GROUP_ID','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.applicant := rec.applicant_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,NULL,'APPLICANT','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.beneficiary := rec.beneficiary_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,NULL,'BENEFICIARY','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.merch_desc := rec.merch_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,NULL,'MERCH_DESC','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.transshipment_ind := NVL(rec.transshipment_ind_dv,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,NULL,'TRANSSHIPMENT_IND','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.partial_shipment_ind := NVL(rec.partial_shipment_ind_dv,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,NULL,'PARTIAL_SHIPMENT_IND','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.lc_ind := NVL(rec.lc_ind_dv,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,NULL,'LC_IND','INV_DEFAULT',SQLERRM);
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;

   FOR rec IN (select r.get_cell(ORD$action)               AS action,
                      r.get_cell(ORD$order_no)             AS order_no,
                      r.get_cell(ORD$lc_ref_id)            AS lc_ref_id,
                      r.get_cell(ORD$lc_group_id)          AS lc_group_id,
                      r.get_cell(ORD$applicant)            AS applicant,
                      r.get_cell(ORD$beneficiary)          AS beneficiary,
                      r.get_cell(ORD$merch_desc)           AS merch_desc,
                      r.get_cell(ORD$transshipment_ind)    AS transshipment_ind,
                      r.get_cell(ORD$partial_shipment_ind) AS partial_shipment_ind,
                      r.get_cell(ORD$lc_ind)               AS lc_ind,
                      r.get_row_seq()                      AS row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(ORD_sheet))
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := sysdate;
      L_temp_rec.last_upd_datetime := sysdate;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.order_no := rec.order_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,rec.row_seq,'ORDER_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lc_ref_id := rec.lc_ref_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,rec.row_seq,'LC_REF_ID',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lc_group_id := rec.lc_group_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,rec.row_seq,'LC_GROUP_ID',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.applicant := rec.applicant;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,rec.row_seq,'APPLICANT',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.beneficiary := rec.beneficiary;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,rec.row_seq,'BENEFICIARY',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.merch_desc := rec.merch_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,rec.row_seq,'MERCH_DESC',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.transshipment_ind := NVL(rec.transshipment_ind,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,rec.row_seq,'TRANSSHIPMENT_IND',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.partial_shipment_ind := NVL(rec.partial_shipment_ind,'N');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,rec.row_seq,'PARTIAL_SHIPMENT_IND',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lc_ind := rec.lc_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORD_sheet,rec.row_seq,'LC_IND',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;

      if NOT L_error then
         DECLARE
            L_stg_rec       SVC_ORDLC%ROWTYPE;
            L_rms_rec       ORDLC%ROWTYPE;
            L_rms_exists    BOOLEAN;
            RECORD_LOCKED   EXCEPTION;
            PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

            cursor C_STG_REC is
               select *
                 from svc_ordlc
                where order_no = L_temp_rec.order_no;

            cursor C_RMS_REC is
               select *
                 from ordlc
                where order_no = L_temp_rec.order_no;

            cursor C_LOCK_SVC_ORDLC is
               select 'x'
                 from svc_ordlc
                where process_id = L_stg_rec.process_id
                  and row_seq = L_stg_rec.row_seq
                  for update nowait;

         BEGIN
            open C_STG_REC;
            fetch C_STG_REC into L_stg_rec;
            if C_STG_REC%FOUND then
               L_stg_exists := TRUE;
            else
               L_stg_exists := FALSE;
            end if;
            close C_STG_REC;

            open C_RMS_REC;
            fetch C_RMS_REC into L_rms_rec;
            if C_RMS_REC%FOUND then
               L_rms_exists := TRUE;
            else
               L_rms_exists := FALSE;
            end if;
            close C_RMS_REC;

            if L_stg_exists then
               --delete record from staging if it is already uploaded to RMS.
               if L_stg_rec.process$status = 'P' and
                  GET_PRC_DEST(L_error_msg,
                               L_stg_rec.process_id) = 'RMS' then
                     open C_LOCK_SVC_ORDLC;
                     close C_LOCK_SVC_ORDLC;
                     delete from svc_ordlc
                           where process_id = L_stg_rec.process_id
                             and row_seq = L_stg_rec.row_seq;
                     L_stg_exists := FALSE;
               end if;

               if L_stg_rec.process$status in ('N', 'E') and
                  NOT L_rms_exists and
                  L_stg_rec.action = PO_INDUCT_SQL.ACTION_NEW and
                  L_temp_rec.action = PO_INDUCT_SQL.ACTION_DEL then
                     open C_LOCK_SVC_ORDLC;
                     close C_LOCK_SVC_ORDLC;
                     delete from svc_ordlc
                           where process_id = L_stg_rec.process_id
                             and row_seq = L_stg_rec.row_seq;
                     continue;
               end if;
            end if;

            --copy values for non-mandatory columns from staging.
            if L_stg_exists then
               if L_mi_rec.order_no_mi <> 'Y' then
                  L_temp_rec.order_no := L_stg_rec.order_no;
               end if;
               if L_mi_rec.lc_ref_id_mi <> 'Y' then
                  L_temp_rec.lc_ref_id := L_stg_rec.lc_ref_id;
               end if;
               if L_mi_rec.lc_group_id_mi <> 'Y' then
                  L_temp_rec.lc_group_id := L_stg_rec.lc_group_id;
               end if;
               if L_mi_rec.applicant_mi <> 'Y' then
                  L_temp_rec.applicant := L_stg_rec.applicant;
               end if;
               if L_mi_rec.beneficiary_mi <> 'Y' then
                  L_temp_rec.beneficiary := L_stg_rec.beneficiary;
               end if;
               if L_mi_rec.merch_desc_mi <> 'Y' then
                  L_temp_rec.merch_desc := L_stg_rec.merch_desc;
               end if;
               if L_mi_rec.transshipment_ind_mi <> 'Y' then
                  L_temp_rec.transshipment_ind := L_stg_rec.transshipment_ind;
               end if;
               if L_mi_rec.partial_shipment_ind_mi <> 'Y' then
                  L_temp_rec.partial_shipment_ind := L_stg_rec.partial_shipment_ind;
               end if;
               if L_mi_rec.lc_ind_mi <> 'Y' then
                  L_temp_rec.lc_ind := L_stg_rec.lc_ind;
               end if;
            end if;
            --if record exists in RMS but not in staging, then copy values for non-mandatory columns from RMS.
            if L_rms_exists and NOT L_stg_exists then
               if L_mi_rec.order_no_mi <> 'Y' then
                  L_temp_rec.order_no := L_rms_rec.order_no;
               end if;
               if L_mi_rec.lc_ref_id_mi <> 'Y' then
                  L_temp_rec.lc_ref_id := L_rms_rec.lc_ref_id;
               end if;
               if L_mi_rec.lc_group_id_mi <> 'Y' then
                  L_temp_rec.lc_group_id := L_rms_rec.lc_group_id;
               end if;
               if L_mi_rec.applicant_mi <> 'Y' then
                  L_temp_rec.applicant := L_rms_rec.applicant;
               end if;
               if L_mi_rec.beneficiary_mi <> 'Y' then
                  L_temp_rec.beneficiary := L_rms_rec.beneficiary;
               end if;
               if L_mi_rec.merch_desc_mi <> 'Y' then
                  L_temp_rec.merch_desc := L_rms_rec.merch_desc;
               end if;
               if L_mi_rec.transshipment_ind_mi <> 'Y' then
                  L_temp_rec.transshipment_ind := L_rms_rec.transshipment_ind;
               end if;
               if L_mi_rec.partial_shipment_ind_mi <> 'Y' then
                  L_temp_rec.partial_shipment_ind := L_rms_rec.partial_shipment_ind;
               end if;
               if L_mi_rec.lc_ind_mi <> 'Y' then
                  L_temp_rec.lc_ind := L_rms_rec.lc_ind;
               end if;
            end if;
            --Resolve new value for action column.
            if L_stg_exists then
               --if it is a create record and record already exists in staging, log an error
               if L_temp_rec.action = PO_INDUCT_SQL.ACTION_NEW then
                  if L_stg_rec.process$status = 'P' then
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS',L_pk_columns);
                  else
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',L_pk_columns);
                  end if;
                  WRITE_S9T_ERROR(I_file_id,ORD_sheet,L_temp_rec.row_seq,NULL,NULL,L_error_msg);
                  L_error := TRUE;
               else
                  --if record does not exist in staging then keep the user provided value
                  L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,L_temp_rec.action);
               end if;
            end if;
         EXCEPTION
            when RECORD_LOCKED then
               L_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                 L_table,
                                                 NULL,
                                                 NULL);
               WRITE_S9T_ERROR(I_file_id,ORD_sheet,L_stg_rec.row_seq,NULL,NULL,L_error_msg);
               L_error := TRUE;
         END;
      end if; --if no L_error
      --Apply defaults if it is a create record and record does not exist in staging already
      if rec.action = PO_INDUCT_SQL.ACTION_NEW and
         NOT L_error and
         NOT L_stg_exists then
         L_temp_rec.order_no             := NVL(L_temp_rec.order_no,L_default_rec.order_no);
         L_temp_rec.lc_ref_id            := NVL(L_temp_rec.lc_ref_id,L_default_rec.lc_ref_id);
         L_temp_rec.lc_group_id          := NVL(L_temp_rec.lc_group_id,L_default_rec.lc_group_id);
         L_temp_rec.applicant            := NVL(L_temp_rec.applicant,L_default_rec.applicant);
         L_temp_rec.beneficiary          := NVL(L_temp_rec.beneficiary,L_default_rec.beneficiary);
         L_temp_rec.merch_desc           := NVL(L_temp_rec.merch_desc,L_default_rec.merch_desc);
         L_temp_rec.transshipment_ind    := NVL(L_temp_rec.transshipment_ind,L_default_rec.transshipment_ind);
         L_temp_rec.partial_shipment_ind := NVL(L_temp_rec.partial_shipment_ind,L_default_rec.partial_shipment_ind);
         L_temp_rec.lc_ind               := NVL(L_temp_rec.lc_ind,L_default_rec.lc_ind);
      end if;

      -- Check PK cols. Log error if any PK col is null.
      if L_temp_rec.order_no is NULL then
         L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                           L_pk_columns);
         WRITE_S9T_ERROR(I_file_id,
                         ORD_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         L_error_msg);
         L_error := TRUE;
      end if;

      --if no error so far then add temp record to insert or update collection depending upon
      --whether record already exists in staging or not.
      if NOT L_error then
         if L_stg_exists then
            svc_upd_col.extend();
            svc_upd_col(svc_upd_col.count()) := L_temp_rec;
         else
            svc_ins_col.extend();
            svc_ins_col(svc_ins_col.count()) := L_temp_rec;
         end if;
      end if;
   END LOOP;

   --flush insert collection
   BEGIN
      FORALL i IN 1..svc_ins_col.COUNT SAVE EXCEPTIONS
         insert into svc_ordlc
         values svc_ins_col(i);
   EXCEPTION
      when DML_ERRORS then
         FOR i in 1..SQL%BULK_EXCEPTIONS.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                 L_pk_columns,
                                                 L_table);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            ORD_sheet,
                            svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;

   --flush update collection
   FORALL i IN 1..svc_upd_col.COUNT
      update svc_ordlc
         set row = svc_upd_col(i)
       where order_no = svc_upd_col(i).order_no;

END PROCESS_S9T_ORD;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ORE(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_ORDLOC_EXP.PROCESS_ID%TYPE) IS

   TYPE SVC_ORE_COL_TYP IS TABLE OF SVC_ORDLOC_EXP%ROWTYPE;
   L_temp_rec      SVC_ORDLOC_EXP%ROWTYPE;
   svc_upd_col     SVC_ORE_COL_TYP := NEW SVC_ORE_COL_TYP();
   svc_ins_col     SVC_ORE_COL_TYP := NEW SVC_ORE_COL_TYP();
   L_process_id    SVC_ORDLOC_EXP.PROCESS_ID%TYPE;
   L_error         BOOLEAN := FALSE;
   L_default_rec   SVC_ORDLOC_EXP%ROWTYPE;
   L_table         VARCHAR2(30) := 'SVC_ORDLOC_EXP';
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   L_stg_exists    BOOLEAN;
   L_pk_columns    VARCHAR2(255) := 'order number, item, pack item, location, HTS component ID';
   L_error_code    NUMBER;

   cursor C_MANDATORY_IND is
      select order_no_mi,
             item_mi,
             pack_item_mi,
             location_mi,
             loc_type_mi,
             location_desc_mi,
             comp_id_mi,
             cvb_code_mi,
             cost_basis_mi,
             comp_rate_mi,
             exchange_rate_mi,
             per_count_mi,
             per_count_uom_mi,
             est_exp_value_mi,
             nom_flag_1_mi,
             nom_flag_2_mi,
             nom_flag_3_mi,
             nom_flag_4_mi,
             nom_flag_5_mi,
             origin_mi,
             defaulted_from_mi,
             key_value_1_mi,
             key_value_2_mi,
             comp_currency_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = PO_INDUCT_SQL.TEMPLATE_KEY
                 and wksht_key    = 'ORDLOC_EXP')
       pivot (MAX(mandatory) AS mi FOR (column_key) IN ('ORDER_NO'       AS ORDER_NO,
                                                        'ITEM'           AS ITEM,
                                                        'PACK_ITEM'      AS PACK_ITEM,
                                                        'LOCATION'       AS LOCATION,
                                                        'LOC_TYPE'       AS LOC_TYPE,
                                                        'LOCATION_DESC'  AS LOCATION_DESC,
                                                        'COMP_ID'        AS COMP_ID,
                                                        'CVB_CODE'       AS CVB_CODE,
                                                        'COST_BASIS'     AS COST_BASIS,
                                                        'COMP_RATE'      AS COMP_RATE,
                                                        'EXCHANGE_RATE'  AS EXCHANGE_RATE,
                                                        'PER_COUNT'      AS PER_COUNT,
                                                        'PER_COUNT_UOM'  AS PER_COUNT_UOM,
                                                        'EST_EXP_VALUE'  AS EST_EXP_VALUE,
                                                        'NOM_FLAG_1'     AS NOM_FLAG_1,
                                                        'NOM_FLAG_2'     AS NOM_FLAG_2,
                                                        'NOM_FLAG_3'     AS NOM_FLAG_3,
                                                        'NOM_FLAG_4'     AS NOM_FLAG_4,
                                                        'NOM_FLAG_5'     AS NOM_FLAG_5,
                                                        'ORIGIN'         AS ORIGIN,
                                                        'DEFAULTED_FROM' AS DEFAULTED_FROM,
                                                        'KEY_VALUE_1'    AS KEY_VALUE_1,
                                                        'KEY_VALUE_2'    AS KEY_VALUE_2,
                                                        'COMP_CURRENCY'  AS COMP_CURRENCY,
                                                        NULL as dummy));
   L_mi_rec     C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS   EXCEPTION;
   PRAGMA       EXCEPTION_INIT(DML_ERRORS, -24381);

BEGIN
   --Get default values.
   FOR rec IN (select order_no_dv,
                      item_dv,
                      pack_item_dv,
                      location_dv,
                      loc_TYPE_dv,
                      location_desc_dv,
                      UPPER(comp_id_dv) as comp_id_dv,
                      UPPER(cvb_code_dv) as cvb_code_dv,
                      UPPER(cost_basis_dv) as cost_basis_dv,
                      comp_rate_dv,
                      exchange_rate_dv,
                      per_count_dv,
                      UPPER(per_count_uom_dv) as per_count_uom_dv,
                      est_exp_value_dv,
                      nom_flag_1_dv,
                      nom_flag_2_dv,
                      nom_flag_3_dv,
                      nom_flag_4_dv,
                      nom_flag_5_dv,
                      UPPER(origin_dv) as origin_dv,
                      UPPER(defaulted_from_dv) as defaulted_from_dv,
                      key_value_1_dv,
                      key_value_2_dv,
                      UPPER(comp_currency_dv) as comp_currency_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = PO_INDUCT_SQL.TEMPLATE_KEY
                          and wksht_key = 'ORDLOC_EXP')
                pivot (MAX(default_value) AS dv FOR (column_key) IN ('ORDER_NO'       AS ORDER_NO,
                                                                     'ITEM'           AS ITEM,
                                                                     'PACK_ITEM'      AS PACK_ITEM,
                                                                     'LOCATION'       AS LOCATION,
                                                                     'LOC_TYPE'       AS LOC_TYPE,
                                                                     'LOCATION_DESC'  AS LOCATION_DESC,
                                                                     'COMP_ID'        AS COMP_ID,
                                                                     'CVB_CODE'       AS CVB_CODE,
                                                                     'COST_BASIS'     AS COST_BASIS,
                                                                     'COMP_RATE'      AS COMP_RATE,
                                                                     'EXCHANGE_RATE'  AS EXCHANGE_RATE,
                                                                     'PER_COUNT'      AS PER_COUNT,
                                                                     'PER_COUNT_UOM'  AS PER_COUNT_UOM,
                                                                     'EST_EXP_VALUE'  AS EST_EXP_VALUE,
                                                                     'NOM_FLAG_1'     AS NOM_FLAG_1,
                                                                     'NOM_FLAG_2'     AS NOM_FLAG_2,
                                                                     'NOM_FLAG_3'     AS NOM_FLAG_3,
                                                                     'NOM_FLAG_4'     AS NOM_FLAG_4,
                                                                     'NOM_FLAG_5'     AS NOM_FLAG_5,
                                                                     'ORIGIN'         AS ORIGIN,
                                                                     'DEFAULTED_FROM' AS DEFAULTED_FROM,
                                                                     'KEY_VALUE_1'    AS KEY_VALUE_1,
                                                                     'KEY_VALUE_2'    AS KEY_VALUE_2,
                                                                     'COMP_CURRENCY'  AS COMP_CURRENCY,
                                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.order_no := rec.order_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'ORDER_NO','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.item := rec.item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'ITEM','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.pack_item := rec.pack_item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'PACK_ITEM','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.location := rec.location_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'LOCATION','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.loc_type := rec.loc_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'LOC_TYPE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.comp_id := rec.comp_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'COMP_ID','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.cvb_code := rec.cvb_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'CVB_CODE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.cost_basis := rec.cost_basis_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'COST_BASIS','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.comp_rate := rec.comp_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'COMP_RATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.exchange_rate := rec.exchange_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'EXCHANGE_RATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.per_count := rec.per_count_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'PER_COUNT','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.per_count_uom := rec.per_count_uom_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'PER_COUNT_UOM','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.est_exp_value := rec.est_exp_value_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'EST_EXP_VALUE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.nom_flag_1 := rec.nom_flag_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'NOM_FLAG_1','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.nom_flag_2 := rec.nom_flag_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'NOM_FLAG_2','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.nom_flag_3 := rec.nom_flag_3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'NOM_FLAG_3','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.nom_flag_4 := rec.nom_flag_4_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'NOM_FLAG_4','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.nom_flag_5 := rec.nom_flag_5_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'NOM_FLAG_5','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.origin := rec.origin_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'ORIGIN','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.defaulted_from := rec.defaulted_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'DEFAULTED_FROM','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.key_value_1 := rec.key_value_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'KEY_VALUE_1','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.key_value_2 := rec.key_value_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'KEY_VALUE_2','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.comp_currency := rec.comp_currency_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,NULL,'COMP_CURRENCY','INV_DEFAULT',SQLERRM);
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;

   FOR rec IN (select r.get_cell(ORE$action)         AS action,
                      r.get_cell(ORE$order_no)       AS order_no,
                      r.get_cell(ORE$item)           AS item,
                      r.get_cell(ORE$pack_item)      AS pack_item,
                      r.get_cell(ORE$location)       AS location,
                      r.get_cell(ORE$loc_TYPE)       AS loc_TYPE,
                      UPPER(r.get_cell(ORE$comp_id))   AS comp_id,
                      UPPER(r.get_cell(ORE$cvb_code))  AS cvb_code,
                      UPPER(r.get_cell(ORE$cost_basis)) AS cost_basis,
                      r.get_cell(ORE$comp_rate)      AS comp_rate,
                      r.get_cell(ORE$exchange_rate)  AS exchange_rate,
                      r.get_cell(ORE$per_count)      AS per_count,
                      UPPER(r.get_cell(ORE$per_count_uom))  AS per_count_uom,
                      r.get_cell(ORE$est_exp_value)  AS est_exp_value,
                      r.get_cell(ORE$nom_flag_1)     AS nom_flag_1,
                      r.get_cell(ORE$nom_flag_2)     AS nom_flag_2,
                      r.get_cell(ORE$nom_flag_3)     AS nom_flag_3,
                      r.get_cell(ORE$nom_flag_4)     AS nom_flag_4,
                      r.get_cell(ORE$nom_flag_5)     AS nom_flag_5,
                      UPPER(r.get_cell(ORE$origin))  AS origin,
                      UPPER(r.get_cell(ORE$defaulted_from)) AS defaulted_from,
                      r.get_cell(ORE$key_value_1)    AS key_value_1,
                      r.get_cell(ORE$key_value_2)    AS key_value_2,
                      UPPER(r.get_cell(ORE$comp_currency))  AS comp_currency,
                      r.get_row_seq()                AS row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(ORE_sheet))
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := sysdate;
      L_temp_rec.last_upd_datetime := sysdate;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.order_no := rec.order_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'ORDER_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item := rec.item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.pack_item := rec.pack_item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'PACK_ITEM',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.location := rec.location;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'LOCATION',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.loc_type := rec.loc_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'LOC_TYPE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_id := rec.comp_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'COMP_ID',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.cvb_code := rec.cvb_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'CVB_CODE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.cost_basis := rec.cost_basis;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'COST_BASIS',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_rate := rec.comp_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'COMP_RATE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.exchange_rate := rec.exchange_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'EXCHANGE_RATE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.per_count := rec.per_count;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'PER_COUNT',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.per_count_uom := rec.per_count_uom;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'PER_COUNT_UOM',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.est_exp_value := rec.est_exp_value;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'EST_EXP_VALUE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_1 := rec.nom_flag_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'NOM_FLAG_1',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_2 := rec.nom_flag_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'NOM_FLAG_2',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_3 := rec.nom_flag_3;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'NOM_FLAG_3',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_4 := rec.nom_flag_4;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'NOM_FLAG_4',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_5 := rec.nom_flag_5;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'NOM_FLAG_5',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.origin := rec.origin;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'ORIGIN',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.defaulted_from := rec.defaulted_from;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'DEFAULTED_FROM',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.key_value_1 := rec.key_value_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'KEY_VALUE_1',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.key_value_2 := rec.key_value_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'KEY_VALUE_2',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_currency := rec.comp_currency;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORE_sheet,rec.row_seq,'COMP_CURRENCY',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;

      if NOT L_error then
         DECLARE
            L_stg_rec       SVC_ORDLOC_EXP%ROWTYPE;
            L_rms_rec       ORDLOC_EXP%ROWTYPE;
            L_rms_exists    BOOLEAN;
            RECORD_LOCKED   EXCEPTION;
            PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

            cursor C_STG_REC is
               select *
                 from svc_ordloc_exp
                where order_no = L_temp_rec.order_no
                  and item = L_temp_rec.item
                  and NVL(pack_item, '-999') = NVL(L_temp_rec.pack_item, '-999')
                  and location = L_temp_rec.location
                  and comp_id = L_temp_rec.comp_id;

            cursor C_RMS_REC is
               select *
                 from ordloc_exp
                where order_no = L_temp_rec.order_no
                  and item = L_temp_rec.item
                  and NVL(pack_item, '-999') = NVL(L_temp_rec.pack_item, '-999')
                  and location = L_temp_rec.location
                  and comp_id = L_temp_rec.comp_id;

            cursor C_LOCK_SVC_ORDLOC_EXP is
               select 'x'
                 from svc_ordloc_exp
                where process_id = L_stg_rec.process_id
                  and row_seq = L_stg_rec.row_seq
                  for update nowait;

         BEGIN
            open C_STG_REC;
            fetch C_STG_REC into L_stg_rec;
            if C_STG_REC%FOUND then
               L_stg_exists := TRUE;
            else
               L_stg_exists := FALSE;
            end if;
            close C_STG_REC;

            open C_RMS_REC;
            fetch C_RMS_REC into L_rms_rec;
            if C_RMS_REC%FOUND then
               L_rms_exists := TRUE;
            else
               L_rms_exists := FALSE;
            end if;
            close C_RMS_REC;

            if L_stg_exists then
               --delete record from staging if it is already uploaded to RMS.
               if L_stg_rec.process$status = 'P' and
                  GET_PRC_DEST(L_error_msg,
                               L_stg_rec.process_id) = 'RMS' then
                     open C_LOCK_SVC_ORDLOC_EXP;
                     close C_LOCK_SVC_ORDLOC_EXP;
                     delete from svc_ordloc_exp
                           where process_id = L_stg_rec.process_id
                             and row_seq = L_stg_rec.row_seq;
                     L_stg_exists := FALSE;
               end if;

               if L_stg_rec.process$status in ('N', 'E') and
                  NOT L_rms_exists and
                  L_stg_rec.action = PO_INDUCT_SQL.ACTION_NEW and
                  L_temp_rec.action = PO_INDUCT_SQL.ACTION_DEL then
                     open C_LOCK_SVC_ORDLOC_EXP;
                     close C_LOCK_SVC_ORDLOC_EXP;
                     delete from svc_ordloc_exp
                           where process_id = L_stg_rec.process_id
                             and row_seq = L_stg_rec.row_seq;
                     continue;
               end if;
            end if;

            --copy values for non-mandatory columns from staging.
            if L_stg_exists then
               if L_mi_rec.order_no_mi <> 'Y' then
                  L_temp_rec.order_no := L_stg_rec.order_no;
               end if;
               if L_mi_rec.item_mi <> 'Y' then
                  L_temp_rec.item := L_stg_rec.item;
               end if;
               if L_mi_rec.pack_item_mi <> 'Y' then
                  L_temp_rec.pack_item := L_stg_rec.pack_item;
               end if;
               if L_mi_rec.location_mi <> 'Y' then
                  L_temp_rec.location := L_stg_rec.location;
               end if;
               if L_mi_rec.loc_type_mi <> 'Y' then
                  L_temp_rec.loc_type := L_stg_rec.loc_type;
               end if;
               if L_mi_rec.comp_id_mi <> 'Y' then
                  L_temp_rec.comp_id := L_stg_rec.comp_id;
               end if;
               if L_mi_rec.cvb_code_mi <> 'Y' then
                  L_temp_rec.cvb_code := L_stg_rec.cvb_code;
               end if;
               if L_mi_rec.cost_basis_mi <> 'Y' then
                  L_temp_rec.cost_basis := L_stg_rec.cost_basis;
               end if;
               if L_mi_rec.comp_rate_mi <> 'Y' then
                  L_temp_rec.comp_rate := L_stg_rec.comp_rate;
               end if;
               if L_mi_rec.exchange_rate_mi <> 'Y' then
                  L_temp_rec.exchange_rate := L_stg_rec.exchange_rate;
               end if;
               if L_mi_rec.per_count_mi <> 'Y' then
                  L_temp_rec.per_count := L_stg_rec.per_count;
               end if;
               if L_mi_rec.per_count_uom_mi <> 'Y' then
                  L_temp_rec.per_count_uom := L_stg_rec.per_count_uom;
               end if;
               if L_mi_rec.est_exp_value_mi <> 'Y' then
                  L_temp_rec.est_exp_value := L_stg_rec.est_exp_value;
               end if;
               if L_mi_rec.nom_flag_1_mi <> 'Y' then
                  L_temp_rec.nom_flag_1 := L_stg_rec.nom_flag_1;
               end if;
               if L_mi_rec.nom_flag_2_mi <> 'Y' then
                  L_temp_rec.nom_flag_2 := L_stg_rec.nom_flag_2;
               end if;
               if L_mi_rec.nom_flag_3_mi <> 'Y' then
                  L_temp_rec.nom_flag_3 := L_stg_rec.nom_flag_3;
               end if;
               if L_mi_rec.nom_flag_4_mi <> 'Y' then
                  L_temp_rec.nom_flag_4 := L_stg_rec.nom_flag_4;
               end if;
               if L_mi_rec.nom_flag_5_mi <> 'Y' then
                  L_temp_rec.nom_flag_5 := L_stg_rec.nom_flag_5;
               end if;
               if L_mi_rec.origin_mi <> 'Y' then
                  L_temp_rec.origin := L_stg_rec.origin;
               end if;
               if L_mi_rec.defaulted_from_mi <> 'Y' then
                  L_temp_rec.defaulted_from := L_stg_rec.defaulted_from;
               end if;
               if L_mi_rec.key_value_1_mi <> 'Y' then
                  L_temp_rec.key_value_1 := L_stg_rec.key_value_1;
               end if;
               if L_mi_rec.key_value_2_mi <> 'Y' then
                  L_temp_rec.key_value_2 := L_stg_rec.key_value_2;
               end if;
               if L_mi_rec.comp_currency_mi <> 'Y' then
                  L_temp_rec.comp_currency := L_stg_rec.comp_currency;
               end if;
            end if;
            --if record exists in RMS but not in staging, then copy values for non-mandatory columns from RMS.
            if L_rms_exists and NOT L_stg_exists then
               if L_mi_rec.order_no_mi <> 'Y' then
                  L_temp_rec.order_no := L_rms_rec.order_no;
               end if;
               if L_mi_rec.item_mi <> 'Y' then
                  L_temp_rec.item := L_rms_rec.item;
               end if;
               if L_mi_rec.pack_item_mi <> 'Y' then
                  L_temp_rec.pack_item := L_rms_rec.pack_item;
               end if;
               if L_mi_rec.location_mi <> 'Y' then
                  L_temp_rec.location := L_rms_rec.location;
               end if;
               if L_mi_rec.loc_type_mi <> 'Y' then
                  L_temp_rec.loc_type := L_rms_rec.loc_type;
               end if;
               if L_mi_rec.comp_id_mi <> 'Y' then
                  L_temp_rec.comp_id := L_rms_rec.comp_id;
               end if;
               if L_mi_rec.cvb_code_mi <> 'Y' then
                  L_temp_rec.cvb_code := L_rms_rec.cvb_code;
               end if;
               if L_mi_rec.cost_basis_mi <> 'Y' then
                  L_temp_rec.cost_basis := L_rms_rec.cost_basis;
               end if;
               if L_mi_rec.comp_rate_mi <> 'Y' then
                  L_temp_rec.comp_rate := L_rms_rec.comp_rate;
               end if;
               if L_mi_rec.exchange_rate_mi <> 'Y' then
                  L_temp_rec.exchange_rate := L_rms_rec.exchange_rate;
               end if;
               if L_mi_rec.per_count_mi <> 'Y' then
                  L_temp_rec.per_count := L_rms_rec.per_count;
               end if;
               if L_mi_rec.per_count_uom_mi <> 'Y' then
                  L_temp_rec.per_count_uom := L_rms_rec.per_count_uom;
               end if;
               if L_mi_rec.est_exp_value_mi <> 'Y' then
                  L_temp_rec.est_exp_value := L_rms_rec.est_exp_value;
               end if;
               if L_mi_rec.nom_flag_1_mi <> 'Y' then
                  L_temp_rec.nom_flag_1 := L_rms_rec.nom_flag_1;
               end if;
               if L_mi_rec.nom_flag_2_mi <> 'Y' then
                  L_temp_rec.nom_flag_2 := L_rms_rec.nom_flag_2;
               end if;
               if L_mi_rec.nom_flag_3_mi <> 'Y' then
                  L_temp_rec.nom_flag_3 := L_rms_rec.nom_flag_3;
               end if;
               if L_mi_rec.nom_flag_4_mi <> 'Y' then
                  L_temp_rec.nom_flag_4 := L_rms_rec.nom_flag_4;
               end if;
               if L_mi_rec.nom_flag_5_mi <> 'Y' then
                  L_temp_rec.nom_flag_5 := L_rms_rec.nom_flag_5;
               end if;
               if L_mi_rec.origin_mi <> 'Y' then
                  L_temp_rec.origin := L_rms_rec.origin;
               end if;
               if L_mi_rec.defaulted_from_mi <> 'Y' then
                  L_temp_rec.defaulted_from := L_rms_rec.defaulted_from;
               end if;
               if L_mi_rec.key_value_1_mi <> 'Y' then
                  L_temp_rec.key_value_1 := L_rms_rec.key_value_1;
               end if;
               if L_mi_rec.key_value_2_mi <> 'Y' then
                  L_temp_rec.key_value_2 := L_rms_rec.key_value_2;
               end if;
               if L_mi_rec.comp_currency_mi <> 'Y' then
                  L_temp_rec.comp_currency := L_rms_rec.comp_currency;
               end if;
            end if;
            --Resolve new value for action column.
            if L_stg_exists then
               --if it is a create record and record already exists in staging, log an error
               if L_temp_rec.action = PO_INDUCT_SQL.ACTION_NEW then
                  if L_stg_rec.process$status = 'P' then
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS',L_pk_columns);
                  else
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',L_pk_columns);
                  end if;
                  WRITE_S9T_ERROR(I_file_id,ORE_sheet,L_temp_rec.row_seq,NULL,NULL,L_error_msg);
                  L_error := TRUE;
               else
                  --if record does not exist in staging then keep the user provided value
                  L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,L_temp_rec.action);
               end if;
            end if;
         EXCEPTION
            when RECORD_LOCKED then
               L_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                 L_table,
                                                 NULL,
                                                 NULL);
               WRITE_S9T_ERROR(I_file_id,ORE_sheet,L_stg_rec.row_seq,NULL,NULL,L_error_msg);
               L_error := TRUE;
         END;
      end if; --if no L_error
      --Apply defaults if it is a create record and record does not exist in staging already
      if rec.action = PO_INDUCT_SQL.ACTION_NEW then
         L_temp_rec.order_no       := NVL(L_temp_rec.order_no,L_default_rec.order_no);
         L_temp_rec.item           := NVL(L_temp_rec.item,L_default_rec.item);
         L_temp_rec.pack_item      := NVL(L_temp_rec.pack_item,L_default_rec.pack_item);
         L_temp_rec.location       := NVL(L_temp_rec.location,L_default_rec.location);
         L_temp_rec.loc_type       := NVL(L_temp_rec.loc_type,L_default_rec.loc_type);
         L_temp_rec.comp_id        := NVL(L_temp_rec.comp_id,L_default_rec.comp_id);
         L_temp_rec.cvb_code       := NVL(L_temp_rec.cvb_code,L_default_rec.cvb_code);
         L_temp_rec.cost_basis     := NVL(L_temp_rec.cost_basis,L_default_rec.cost_basis);
         L_temp_rec.comp_rate      := NVL(L_temp_rec.comp_rate,L_default_rec.comp_rate);
         L_temp_rec.exchange_rate  := NVL(L_temp_rec.exchange_rate,L_default_rec.exchange_rate);
         L_temp_rec.per_count      := NVL(L_temp_rec.per_count,L_default_rec.per_count);
         L_temp_rec.per_count_uom  := NVL(L_temp_rec.per_count_uom,L_default_rec.per_count_uom);
         L_temp_rec.est_exp_value  := NVL(L_temp_rec.est_exp_value,L_default_rec.est_exp_value);
         L_temp_rec.nom_flag_1     := NVL(L_temp_rec.nom_flag_1,L_default_rec.nom_flag_1);
         L_temp_rec.nom_flag_2     := NVL(L_temp_rec.nom_flag_2,L_default_rec.nom_flag_2);
         L_temp_rec.nom_flag_3     := NVL(L_temp_rec.nom_flag_3,L_default_rec.nom_flag_3);
         L_temp_rec.nom_flag_4     := NVL(L_temp_rec.nom_flag_4,L_default_rec.nom_flag_4);
         L_temp_rec.nom_flag_5     := NVL(L_temp_rec.nom_flag_5,L_default_rec.nom_flag_5);
         L_temp_rec.origin         := NVL(L_temp_rec.origin,L_default_rec.origin);
         L_temp_rec.defaulted_from := NVL(L_temp_rec.defaulted_from,L_default_rec.defaulted_from);
         L_temp_rec.key_value_1    := NVL(L_temp_rec.key_value_1,L_default_rec.key_value_1);
         L_temp_rec.key_value_2    := NVL(L_temp_rec.key_value_2,L_default_rec.key_value_2);
         L_temp_rec.comp_currency  := NVL(L_temp_rec.comp_currency,L_default_rec.comp_currency);
      end if;

      -- Check PK cols. Log error if any PK col is null.
      if NOT (L_temp_rec.order_no is NOT NULL and
              L_temp_rec.item is NOT NULL and
              L_temp_rec.location is NOT NULL and
              L_temp_rec.comp_id is NOT NULL) then
         L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                           L_pk_columns);
         WRITE_S9T_ERROR(I_file_id,
                         ORE_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         L_error_msg);
         L_error := TRUE;
      end if;

      --if no error so far then add temp record to insert or update collection depending upon
      --whether record already exists in staging or not.
      if NOT L_error then
         if L_stg_exists then
            svc_upd_col.extend();
            svc_upd_col(svc_upd_col.count()) := L_temp_rec;
         else
            svc_ins_col.extend();
            svc_ins_col(svc_ins_col.count()) := L_temp_rec;
         end if;
      end if;
   END LOOP;

   --flush insert collection
   BEGIN
      FORALL i IN 1..svc_ins_col.COUNT SAVE EXCEPTIONS
         insert into svc_ordloc_exp
         values svc_ins_col(i);
   EXCEPTION
      when DML_ERRORS then
         FOR i in 1..SQL%BULK_EXCEPTIONS.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                 L_pk_columns,
                                                 L_table);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            ORE_sheet,
                            svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;

   --flush update collection
   FORALL i IN 1..svc_upd_col.COUNT
      update svc_ordloc_exp
         set row = svc_upd_col(i)
       where order_no = svc_upd_col(i).order_no
         and item = svc_upd_col(i).item
         and NVL(pack_item, '-999') = NVL(svc_upd_col(i).pack_item, '-999')
         and location = svc_upd_col(i).location
         and comp_id = svc_upd_col(i).comp_id;

END PROCESS_S9T_ORE;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ORH(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_ORDSKU_HTS.PROCESS_ID%TYPE) IS

   TYPE SVC_ORH_COL_TYP IS TABLE OF SVC_ORDSKU_HTS%ROWTYPE;
   L_temp_rec      SVC_ORDSKU_HTS%ROWTYPE;
   svc_upd_col     SVC_ORH_COL_TYP := NEW SVC_ORH_COL_TYP();
   svc_ins_col     SVC_ORH_COL_TYP := NEW SVC_ORH_COL_TYP();
   L_process_id    SVC_ORDSKU_HTS.PROCESS_ID%TYPE;
   L_error         BOOLEAN := FALSE;
   L_default_rec   SVC_ORDSKU_HTS%ROWTYPE;
   L_table         VARCHAR2(30) := 'SVC_ORDSKU_HTS';
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   L_stg_exists    BOOLEAN;
   L_pk_columns    VARCHAR2(255) := 'order number, item, pack item, HTS';
   L_error_code    NUMBER;

   cursor C_MANDATORY_IND is
      select order_no_mi,
             item_mi,
             pack_item_mi,
             hts_mi,
             import_country_id_mi,
             effect_from_mi,
             effect_to_mi,
             status_mi,
             origin_country_id_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = PO_INDUCT_SQL.TEMPLATE_KEY
                 and wksht_key    = 'ORDSKU_HTS')
       pivot (MAX(mandatory) AS mi FOR (column_key) IN ('ORDER_NO'          AS ORDER_NO,
                                                        'ITEM'              AS ITEM,
                                                        'PACK_ITEM'         AS PACK_ITEM,
                                                        'HTS'               AS HTS,
                                                        'IMPORT_COUNTRY_ID' AS IMPORT_COUNTRY_ID,
                                                        'EFFECT_FROM'       AS EFFECT_FROM,
                                                        'EFFECT_TO'         AS EFFECT_TO,
                                                        'STATUS'            AS STATUS,
                                                        'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID,
                                                        NULL as dummy));
   L_mi_rec     C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS   EXCEPTION;
   PRAGMA       EXCEPTION_INIT(DML_ERRORS, -24381);

BEGIN
   --Get default values.
   FOR rec IN (select order_no_dv,
                      item_dv,
                      pack_item_dv,
                      hts_dv,
                      UPPER(import_country_id_dv) as import_country_id_dv,
                      effect_from_dv,
                      effect_to_dv,
                      status_dv,
                      UPPER(origin_country_id_dv) as origin_country_id_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = PO_INDUCT_SQL.TEMPLATE_KEY
                          and wksht_key = 'ORDSKU_HTS')
                pivot (MAX(default_value) AS dv FOR (column_key) IN ('ORDER_NO'          AS ORDER_NO,
                                                                     'ITEM'              AS ITEM,
                                                                     'PACK_ITEM'         AS PACK_ITEM,
                                                                     'HTS'               AS HTS,
                                                                     'IMPORT_COUNTRY_ID' AS IMPORT_COUNTRY_ID,
                                                                     'EFFECT_FROM'       AS EFFECT_FROM,
                                                                     'EFFECT_TO'         AS EFFECT_TO,
                                                                     'STATUS'            AS STATUS,
                                                                     'ORIGIN_COUNTRY_ID' AS ORIGIN_COUNTRY_ID,
                                                                      NULL               AS dummy)))
   LOOP
      BEGIN
         L_default_rec.order_no := rec.order_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,NULL,'ORDER_NO','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.item := rec.item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,NULL,'ITEM','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.pack_item := rec.pack_item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,NULL,'PACK_ITEM','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,NULL,'HTS','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,NULL,'IMPORT_COUNTRY_ID','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,NULL,'EFFECT_FROM','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,NULL,'EFFECT_TO','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
        L_default_rec.status := rec.status_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,NULL,'STATUS','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.origin_country_id := rec.origin_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,NULL,'ORIGIN_COUNTRY_ID','INV_DEFAULT',SQLERRM);
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;

   FOR rec IN (select r.get_cell(ORH$action)            AS action,
                      r.get_cell(ORH$order_no)          AS order_no,
                      r.get_cell(ORH$item)              AS item,
                      r.get_cell(ORH$pack_item)         AS pack_item,
                      r.get_cell(ORH$hts)               AS hts,
                      UPPER(r.get_cell(ORH$import_country_id)) AS import_country_id,
                      r.get_cell(ORH$effect_from)       AS effect_from,
                      r.get_cell(ORH$effect_to)         AS effect_to,
                      r.get_cell(ORH$status)            AS status,
                      UPPER(r.get_cell(ORH$origin_country_id)) AS origin_country_id,
                      r.get_row_seq()                   AS row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(ORH_sheet))
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := sysdate;
      L_temp_rec.last_upd_datetime := sysdate;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.order_no := rec.order_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,rec.row_seq,'ORDER_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item := rec.item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.pack_item := rec.pack_item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,rec.row_seq,'PACK_ITEM',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,rec.row_seq,'HTS',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,rec.row_seq,'IMPORT_COUNTRY_ID',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_from := rec.effect_from;
      EXCEPTION
         when OTHERS then
            L_error_msg := SQL_LIB.CREATE_MSG('INVALID_DATE',rec.effect_from,LP_format);
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,rec.row_seq,'EFFECT_FROM',NULL,L_error_msg);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := rec.effect_to;
      EXCEPTION
         when OTHERS then
            L_error_msg := SQL_LIB.CREATE_MSG('INVALID_DATE',rec.effect_to,LP_format);
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,rec.row_seq,'EFFECT_TO',NULL,L_error_msg);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.status := rec.status;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,rec.row_seq,'STATUS',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.origin_country_id := rec.origin_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,ORH_sheet,rec.row_seq,'ORIGIN_COUNTRY_ID',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;

      if NOT L_error then
         DECLARE
            L_stg_rec       SVC_ORDSKU_HTS%ROWTYPE;
            L_rms_rec       ORDSKU_HTS%ROWTYPE;
            L_rms_exists    BOOLEAN;
            RECORD_LOCKED   EXCEPTION;
            PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

            cursor C_STG_REC is
               select *
                 from svc_ordsku_hts
                where order_no = L_temp_rec.order_no
                  and item = L_temp_rec.item
                  and NVL(pack_item, '-999') = NVL(L_temp_rec.pack_item, '-999')
                  and hts = L_temp_rec.hts;

            cursor C_RMS_REC is
               select *
                 from ordsku_hts
                where order_no = L_temp_rec.order_no
                  and item = L_temp_rec.item
                  and NVL(pack_item, '-999') = NVL(L_temp_rec.pack_item, '-999')
                  and hts = L_temp_rec.hts;

            cursor C_LOCK_SVC_ORDSKU_HTS is
               select 'x'
                 from svc_ordsku_hts
                where process_id = L_stg_rec.process_id
                  and row_seq = L_stg_rec.row_seq
                  for update nowait;

         BEGIN
            open C_STG_REC;
            fetch C_STG_REC into L_stg_rec;
            if C_STG_REC%FOUND then
               L_stg_exists := TRUE;
            else
               L_stg_exists := FALSE;
            end if;
            close C_STG_REC;

            open C_RMS_REC;
            fetch C_RMS_REC into L_rms_rec;
            if C_RMS_REC%FOUND then
               L_rms_exists := TRUE;
            else
               L_rms_exists := FALSE;
            end if;
            close C_RMS_REC;

            if L_stg_exists then
               --delete record from staging if it is already uploaded to RMS.
               if L_stg_rec.process$status = 'P' and
                  GET_PRC_DEST(L_error_msg,
                               L_stg_rec.process_id) = 'RMS' then
                     open C_LOCK_SVC_ORDSKU_HTS;
                     close C_LOCK_SVC_ORDSKU_HTS;
                     delete from svc_ordsku_hts
                           where process_id = L_stg_rec.process_id
                             and row_seq = L_stg_rec.row_seq;
                     L_stg_exists := FALSE;
               end if;

               if L_stg_rec.process$status in ('N', 'E') and
                  NOT L_rms_exists and
                  L_stg_rec.action = PO_INDUCT_SQL.ACTION_NEW and
                  L_temp_rec.action = PO_INDUCT_SQL.ACTION_DEL then
                     open C_LOCK_SVC_ORDSKU_HTS;
                     close C_LOCK_SVC_ORDSKU_HTS;
                     delete from svc_ordsku_hts
                           where process_id = L_stg_rec.process_id
                             and row_seq = L_stg_rec.row_seq;
                     ---
                     LP_svc_po_tab.EXTEND();
                     LP_svc_po_tab(LP_svc_po_tab.COUNT()).order_no     := L_temp_rec.order_no;
                     LP_svc_po_tab(LP_svc_po_tab.COUNT()).item         := L_temp_rec.item;
                     LP_svc_po_tab(LP_svc_po_tab.COUNT()).pack         := L_temp_rec.pack_item;
                     LP_svc_po_tab(LP_svc_po_tab.COUNT()).hts          := L_temp_rec.hts;
                     LP_svc_po_tab(LP_svc_po_tab.COUNT()).svc_tbl_name := L_table;
                     continue;
               end if;
            end if;

            --copy values for non-mandatory columns from staging.
            if L_stg_exists then
               if L_mi_rec.order_no_mi <> 'Y' then
                  L_temp_rec.order_no := L_stg_rec.order_no;
               end if;
               if L_mi_rec.item_mi <> 'Y' then
                  L_temp_rec.item := L_stg_rec.item;
               end if;
               if L_mi_rec.pack_item_mi <> 'Y' then
                  L_temp_rec.pack_item := L_stg_rec.pack_item;
               end if;
               if L_mi_rec.hts_mi <> 'Y' then
                  L_temp_rec.hts := L_stg_rec.hts;
               end if;
               if L_mi_rec.import_country_id_mi <> 'Y' then
                  L_temp_rec.import_country_id := L_stg_rec.import_country_id;
               end if;
               if L_mi_rec.effect_from_mi <> 'Y' then
                  L_temp_rec.effect_from := L_stg_rec.effect_from;
               end if;
               if L_mi_rec.effect_to_mi <> 'Y' then
                  L_temp_rec.effect_to := L_stg_rec.effect_to;
               end if;
               if L_mi_rec.status_mi <> 'Y' then
                  L_temp_rec.status := L_stg_rec.status;
               end if;
               if L_mi_rec.origin_country_id_mi <> 'Y' then
                  L_temp_rec.origin_country_id := L_stg_rec.origin_country_id;
               end if;
            end if;
            --if record exists in RMS but not in staging, then copy values for non-mandatory columns from RMS.
            if L_rms_exists and NOT L_stg_exists then
               if L_mi_rec.order_no_mi <> 'Y' then
                  L_temp_rec.order_no := L_rms_rec.order_no;
               end if;
               if L_mi_rec.item_mi <> 'Y' then
                  L_temp_rec.item := L_rms_rec.item;
               end if;
               if L_mi_rec.pack_item_mi <> 'Y' then
                  L_temp_rec.pack_item := L_rms_rec.pack_item;
               end if;
               if L_mi_rec.hts_mi <> 'Y' then
                  L_temp_rec.hts := L_rms_rec.hts;
               end if;
               if L_mi_rec.import_country_id_mi <> 'Y' then
                  L_temp_rec.import_country_id := L_rms_rec.import_country_id;
               end if;
               if L_mi_rec.effect_from_mi <> 'Y' then
                  L_temp_rec.effect_from := L_rms_rec.effect_from;
               end if;
               if L_mi_rec.effect_to_mi <> 'Y' then
                  L_temp_rec.effect_to := L_rms_rec.effect_to;
               end if;
               if L_mi_rec.status_mi <> 'Y' then
                  L_temp_rec.status := L_rms_rec.status;
               end if;
               if L_mi_rec.origin_country_id_mi <> 'Y' then
                  L_temp_rec.origin_country_id := L_rms_rec.origin_country_id;
               end if;
            end if;
            --Resolve new value for action column.
            if L_stg_exists then
               --if it is a create record and record already exists in staging, log an error
               if L_temp_rec.action = PO_INDUCT_SQL.ACTION_NEW then
                  if L_stg_rec.process$status = 'P' then
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS',L_pk_columns);
                  else
                     L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',L_pk_columns);
                  end if;
                  WRITE_S9T_ERROR(I_file_id,ORH_sheet,L_temp_rec.row_seq,NULL,NULL,L_error_msg);
                  L_error := TRUE;
               else
                  --if record does not exist in staging then keep the user provided value
                  L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,L_temp_rec.action);
               end if;
            end if;
         EXCEPTION
            when RECORD_LOCKED then
               L_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                 L_table,
                                                 NULL,
                                                 NULL);
               WRITE_S9T_ERROR(I_file_id,ORH_sheet,L_stg_rec.row_seq,NULL,NULL,L_error_msg);
               L_error := TRUE;
         END;
      end if; --if no L_error
      if rec.action = PO_INDUCT_SQL.ACTION_NEW then
         L_temp_rec.order_no          := NVL(L_temp_rec.order_no,L_default_rec.order_no);
         L_temp_rec.item              := NVL(L_temp_rec.item,L_default_rec.item);
         L_temp_rec.pack_item         := NVL(L_temp_rec.pack_item,L_default_rec.pack_item);
         L_temp_rec.hts               := NVL(L_temp_rec.hts,L_default_rec.hts);
         L_temp_rec.import_country_id := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.effect_from       := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.effect_to         := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.status            := NVL(L_temp_rec.status,L_default_rec.status);
         L_temp_rec.origin_country_id := NVL(L_temp_rec.origin_country_id,L_default_rec.origin_country_id);
      end if;

      -- Check PK cols. Log error if any PK col is null.
      if NOT (L_temp_rec.order_no is NOT NULL and
              L_temp_rec.item is NOT NULL and
              L_temp_rec.hts is NOT NULL) then
         L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                           L_pk_columns);
         WRITE_S9T_ERROR(I_file_id,
                         ORH_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         L_error_msg);
         L_error := TRUE;
      end if;

      --if no error so far then add temp record to insert or update collection depending upon
      --whether record already exists in staging or not.
      if NOT L_error then
         if L_stg_exists then
            svc_upd_col.extend();
            svc_upd_col(svc_upd_col.count()) := L_temp_rec;
         else
            svc_ins_col.extend();
            svc_ins_col(svc_ins_col.count()) := L_temp_rec;
         end if;
      end if;
   END LOOP;

   --flush insert collection
   BEGIN
      FORALL i IN 1..svc_ins_col.COUNT SAVE EXCEPTIONS
         insert into svc_ordsku_hts
         values svc_ins_col(i);
   EXCEPTION
      when DML_ERRORS then
         FOR i in 1..SQL%BULK_EXCEPTIONS.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                 L_pk_columns,
                                                 L_table);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            ORH_sheet,
                            svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;

   --flush update collection
   FORALL i IN 1..svc_upd_col.COUNT
      update svc_ordsku_hts
         set row = svc_upd_col(i)
       where order_no = svc_upd_col(i).order_no
         and item = svc_upd_col(i).item
         and NVL(pack_item, '-999') = NVL(svc_upd_col(i).pack_item, '-999')
         and hts = svc_upd_col(i).hts;

END PROCESS_S9T_ORH;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_OHA(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_ORDSKU_HTS_ASSESS.PROCESS_ID%TYPE) IS

   TYPE SVC_OHA_COL_TYP IS TABLE OF SVC_ORDSKU_HTS_ASSESS%ROWTYPE;
   L_temp_rec      SVC_ORDSKU_HTS_ASSESS%ROWTYPE;
   svc_upd_col     SVC_OHA_COL_TYP := NEW SVC_OHA_COL_TYP();
   svc_ins_col     SVC_OHA_COL_TYP := NEW SVC_OHA_COL_TYP();
   L_process_id    SVC_ORDSKU_HTS_ASSESS.PROCESS_ID%TYPE;
   L_error         BOOLEAN := FALSE;
   L_default_rec   SVC_ORDSKU_HTS_ASSESS%ROWTYPE;
   L_table         VARCHAR2(30) := 'SVC_ORDSKU_HTS_ASSESS';
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   L_stg_exists    BOOLEAN;
   L_pk_columns    VARCHAR2(255) := 'order number, item, pack item, HTS, HTS component id';
   L_error_code    NUMBER;

   cursor C_MANDATORY_IND is
      select order_no_mi,
             item_mi,
             pack_item_mi,
             hts_mi,
             comp_id_mi,
             cvb_code_mi,
             comp_rate_mi,
             per_count_mi,
             per_count_uom_mi,
             est_assess_value_mi,
             nom_flag_1_mi,
             nom_flag_2_mi,
             nom_flag_3_mi,
             nom_flag_4_mi,
             nom_flag_5_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = PO_INDUCT_SQL.TEMPLATE_KEY
                 and wksht_key    = 'ORDSKU_HTS_ASSESS')
       pivot (MAX(mandatory) AS mi FOR (column_key) IN ('ORDER_NO'         AS ORDER_NO,
                                                        'ITEM'             AS ITEM,
                                                        'PACK_ITEM'        AS PACK_ITEM,
                                                        'HTS'              AS HTS,
                                                        'COMP_ID'          AS COMP_ID,
                                                        'CVB_CODE'         AS CVB_CODE,
                                                        'COMP_RATE'        AS COMP_RATE,
                                                        'PER_COUNT'        AS PER_COUNT,
                                                        'PER_COUNT_UOM'    AS PER_COUNT_UOM,
                                                        'EST_ASSESS_VALUE' AS EST_ASSESS_VALUE,
                                                        'NOM_FLAG_1'       AS NOM_FLAG_1,
                                                        'NOM_FLAG_2'       AS NOM_FLAG_2,
                                                        'NOM_FLAG_3'       AS NOM_FLAG_3,
                                                        'NOM_FLAG_4'       AS NOM_FLAG_4,
                                                        'NOM_FLAG_5'       AS NOM_FLAG_5,
                                                        NULL as dummy));
   L_mi_rec     C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS   EXCEPTION;
   PRAGMA       EXCEPTION_INIT(DML_ERRORS, -24381);

BEGIN
   --Get default values.
   FOR rec IN (select order_no_dv,
                      item_dv,
                      pack_item_dv,
                      hts_dv,
                      UPPER(comp_id_dv) as comp_id_dv,
                      UPPER(cvb_code_dv) as cvb_code_dv,
                      comp_rate_dv,
                      per_count_dv,
                      UPPER(per_count_uom_dv) as per_count_uom_dv,
                      est_assess_value_dv,
                      nom_flag_1_dv,
                      nom_flag_2_dv,
                      nom_flag_3_dv,
                      nom_flag_4_dv,
                      nom_flag_5_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = PO_INDUCT_SQL.TEMPLATE_KEY
                          and wksht_key = 'ORDSKU_HTS_ASSESS')
                pivot (MAX(default_value) AS dv FOR (column_key) IN ('ORDER_NO' AS ORDER_NO,
                                                                     'ITEM' AS ITEM,
                                                                     'PACK_ITEM' AS PACK_ITEM,
                                                                     'HTS' AS HTS,
                                                                     'COMP_ID' AS COMP_ID,
                                                                     'CVB_CODE' AS CVB_CODE,
                                                                     'COMP_RATE' AS COMP_RATE,
                                                                     'PER_COUNT' AS PER_COUNT,
                                                                     'PER_COUNT_UOM' AS PER_COUNT_UOM,
                                                                     'EST_ASSESS_VALUE' AS EST_ASSESS_VALUE,
                                                                     'NOM_FLAG_1' AS NOM_FLAG_1,
                                                                     'NOM_FLAG_2' AS NOM_FLAG_2,
                                                                     'NOM_FLAG_3' AS NOM_FLAG_3,
                                                                     'NOM_FLAG_4' AS NOM_FLAG_4,
                                                                     'NOM_FLAG_5' AS NOM_FLAG_5,
                                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.order_no := rec.order_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'ORDER_NO','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.item := rec.item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'ITEM','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.pack_item := rec.pack_item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'PACK_ITEM','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'HTS','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.comp_id := rec.comp_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'COMP_ID','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.cvb_code := rec.cvb_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'CVB_CODE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.comp_rate := rec.comp_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'COMP_RATE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.per_count := rec.per_count_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'PER_COUNT','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.per_count_uom := rec.per_count_uom_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'PER_COUNT_UOM','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.est_assess_value := rec.est_assess_value_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'EST_ASSESS_VALUE','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.nom_flag_1 := rec.nom_flag_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'NOM_FLAG_1','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.nom_flag_2 := rec.nom_flag_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'NOM_FLAG_2','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.nom_flag_3 := rec.nom_flag_3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'NOM_FLAG_3','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.nom_flag_4 := rec.nom_flag_4_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'NOM_FLAG_4','INV_DEFAULT',SQLERRM);
      END;
      BEGIN
         L_default_rec.nom_flag_5 := rec.nom_flag_5_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,NULL,'NOM_FLAG_5','INV_DEFAULT',SQLERRM);
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;

   FOR rec IN (select r.get_cell(OHA$action)           AS action,
                      r.get_cell(OHA$order_no)         AS order_no,
                      r.get_cell(OHA$item)             AS item,
                      r.get_cell(OHA$pack_item)        AS pack_item,
                      r.get_cell(OHA$hts)              AS hts,
                      UPPER(r.get_cell(OHA$comp_id))   AS comp_id,
                      UPPER(r.get_cell(OHA$cvb_code))  AS cvb_code,
                      r.get_cell(OHA$comp_rate)        AS comp_rate,
                      r.get_cell(OHA$per_count)        AS per_count,
                      UPPER(r.get_cell(OHA$per_count_uom))    AS per_count_uom,
                      r.get_cell(OHA$est_assess_value) AS est_assess_value,
                      r.get_cell(OHA$nom_flag_1)       AS nom_flag_1,
                      r.get_cell(OHA$nom_flag_2)       AS nom_flag_2,
                      r.get_cell(OHA$nom_flag_3)       AS nom_flag_3,
                      r.get_cell(OHA$nom_flag_4)       AS nom_flag_4,
                      r.get_cell(OHA$nom_flag_5)       AS nom_flag_5,
                      r.get_row_seq()                  AS row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(OHA_sheet))
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := sysdate;
      L_temp_rec.last_upd_datetime := sysdate;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,action_column,SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.order_no := rec.order_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'ORDER_NO',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item := rec.item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'ITEM',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.pack_item := rec.pack_item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'PACK_ITEM',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'HTS',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_id := rec.comp_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'COMP_ID',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.cvb_code := rec.cvb_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'CVB_CODE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_rate := rec.comp_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'COMP_RATE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.per_count := rec.per_count;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'PER_COUNT',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.per_count_uom := rec.per_count_uom;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'PER_COUNT_UOM',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.est_assess_value := rec.est_assess_value;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'EST_ASSESS_VALUE',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_1 := rec.nom_flag_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'NOM_FLAG_1',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_2 := rec.nom_flag_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'NOM_FLAG_2',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_3 := rec.nom_flag_3;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'NOM_FLAG_3',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_4 := rec.nom_flag_4;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'NOM_FLAG_4',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_5 := rec.nom_flag_5;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,OHA_sheet,rec.row_seq,'NOM_FLAG_5',SQLCODE,SQLERRM);
            L_error := TRUE;
      END;

      if NOT L_error then
         DECLARE
            L_stg_rec       SVC_ORDSKU_HTS_ASSESS%ROWTYPE;
            L_rms_exists    BOOLEAN;
            RECORD_LOCKED   EXCEPTION;
            PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

            cursor C_STG_REC is
               select *
                 from svc_ordsku_hts_assess
                where order_no = L_temp_rec.order_no
                  and item = L_temp_rec.item
                  and NVL(pack_item, '-999') = NVL(L_temp_rec.pack_item, '-999')
                  and hts = L_temp_rec.hts
                  and comp_id = L_temp_rec.comp_id;

            cursor C_RMS_REC is
               select oha.*,
                      oh.item,
                      oh.pack_item,
                      oh.hts
                 from ordsku_hts_assess oha,
                      ordsku_hts oh
                where oha.order_no = L_temp_rec.order_no
                  and oha.comp_id = L_temp_rec.comp_id
                  and oha.order_no = oh.order_no
                  and oha.seq_no = oh.seq_no;

            L_rms_rec   C_RMS_REC%ROWTYPE;

            cursor C_LOCK_SVC_ORDSKU_HTS_ASSESS is
               select 'x'
                 from svc_ordsku_hts_assess
                where process_id = L_stg_rec.process_id
                  and row_seq = L_stg_rec.row_seq
                  for update nowait;

         BEGIN
            open C_STG_REC;
            fetch C_STG_REC into L_stg_rec;
            if C_STG_REC%FOUND then
               L_stg_exists := TRUE;
            else
               L_stg_exists := FALSE;
            end if;
            close C_STG_REC;

            open C_RMS_REC;
            fetch C_RMS_REC into L_rms_rec;
            if C_RMS_REC%FOUND then
               L_rms_exists := TRUE;
            else
               L_rms_exists := FALSE;
            end if;
            close C_RMS_REC;

            if L_stg_exists then
               --delete record from staging if it is already uploaded to RMS.
               if L_stg_rec.process$status = 'P' and
                  GET_PRC_DEST(L_error_msg,
                               L_stg_rec.process_id) = 'RMS' then
                     open C_LOCK_SVC_ORDSKU_HTS_ASSESS;
                     close C_LOCK_SVC_ORDSKU_HTS_ASSESS;
                     delete from svc_ordsku_hts_assess
                           where process_id = L_stg_rec.process_id
                             and row_seq = L_stg_rec.row_seq;
                     L_stg_exists := FALSE;
               end if;

               if L_stg_rec.process$status in ('N', 'E') and
                  NOT L_rms_exists and
                  L_stg_rec.action = PO_INDUCT_SQL.ACTION_NEW and
                  L_temp_rec.action = PO_INDUCT_SQL.ACTION_DEL then
                     open C_LOCK_SVC_ORDSKU_HTS_ASSESS;
                     close C_LOCK_SVC_ORDSKU_HTS_ASSESS;
                     delete from svc_ordsku_hts_assess
                           where process_id = L_stg_rec.process_id
                             and row_seq = L_stg_rec.row_seq;
                     continue;
               end if;
            end if;

            --copy values for non-mandatory columns from staging.
            if L_stg_exists then
               if L_mi_rec.order_no_mi <> 'Y' then
                  L_temp_rec.order_no := L_stg_rec.order_no;
               end if;
               if L_mi_rec.item_mi <> 'Y' then
                  L_temp_rec.item := L_stg_rec.item;
               end if;
               if L_mi_rec.pack_item_mi <> 'Y' then
                  L_temp_rec.pack_item := L_stg_rec.pack_item;
               end if;
               if L_mi_rec.hts_mi <> 'Y' then
                  L_temp_rec.hts := L_stg_rec.hts;
               end if;
               if L_mi_rec.comp_id_mi <> 'Y' then
                  L_temp_rec.comp_id := L_stg_rec.comp_id;
               end if;
               if L_mi_rec.cvb_code_mi <> 'Y' then
                  L_temp_rec.cvb_code := L_stg_rec.cvb_code;
               end if;
               if L_mi_rec.comp_rate_mi <> 'Y' then
                  L_temp_rec.comp_rate := L_stg_rec.comp_rate;
               end if;
               if L_mi_rec.per_count_mi <> 'Y' then
                  L_temp_rec.per_count := L_stg_rec.per_count;
               end if;
               if L_mi_rec.per_count_uom_mi <> 'Y' then
                  L_temp_rec.per_count_uom := L_stg_rec.per_count_uom;
               end if;
               if L_mi_rec.est_assess_value_mi <> 'Y' then
                  L_temp_rec.est_assess_value := L_stg_rec.est_assess_value;
               end if;
               if L_mi_rec.nom_flag_1_mi <> 'Y' then
                  L_temp_rec.nom_flag_1 := L_stg_rec.nom_flag_1;
               end if;
               if L_mi_rec.nom_flag_2_mi <> 'Y' then
                  L_temp_rec.nom_flag_2 := L_stg_rec.nom_flag_2;
               end if;
               if L_mi_rec.nom_flag_3_mi <> 'Y' then
                  L_temp_rec.nom_flag_3 := L_stg_rec.nom_flag_3;
               end if;
               if L_mi_rec.nom_flag_4_mi <> 'Y' then
                  L_temp_rec.nom_flag_4 := L_stg_rec.nom_flag_4;
               end if;
               if L_mi_rec.nom_flag_5_mi <> 'Y' then
                  L_temp_rec.nom_flag_5 := L_stg_rec.nom_flag_5;
               end if;
            end if;
            --if record exists in RMS but not in staging, then copy values for non-mandatory columns from RMS.
            if L_rms_exists and NOT L_stg_exists then
               if L_mi_rec.order_no_mi <> 'Y' then
                  L_temp_rec.order_no := L_rms_rec.order_no;
               end if;
               if L_mi_rec.item_mi <> 'Y' then
                  L_temp_rec.item := L_rms_rec.item;
               end if;
               if L_mi_rec.pack_item_mi <> 'Y' then
                  L_temp_rec.pack_item := L_rms_rec.pack_item;
               end if;
               if L_mi_rec.hts_mi <> 'Y' then
                  L_temp_rec.hts := L_rms_rec.hts;
               end if;
               if L_mi_rec.comp_id_mi <> 'Y' then
                  L_temp_rec.comp_id := L_rms_rec.comp_id;
               end if;
               if L_mi_rec.cvb_code_mi <> 'Y' then
                  L_temp_rec.cvb_code := L_rms_rec.cvb_code;
               end if;
               if L_mi_rec.comp_rate_mi <> 'Y' then
                  L_temp_rec.comp_rate := L_rms_rec.comp_rate;
               end if;
               if L_mi_rec.per_count_mi <> 'Y' then
                  L_temp_rec.per_count := L_rms_rec.per_count;
               end if;
               if L_mi_rec.per_count_uom_mi <> 'Y' then
                  L_temp_rec.per_count_uom := L_rms_rec.per_count_uom;
               end if;
               if L_mi_rec.est_assess_value_mi <> 'Y' then
                  L_temp_rec.est_assess_value := L_rms_rec.est_assess_value;
               end if;
               if L_mi_rec.nom_flag_1_mi <> 'Y' then
                  L_temp_rec.nom_flag_1 := L_rms_rec.nom_flag_1;
               end if;
               if L_mi_rec.nom_flag_2_mi <> 'Y' then
                  L_temp_rec.nom_flag_2 := L_rms_rec.nom_flag_2;
               end if;
               if L_mi_rec.nom_flag_3_mi <> 'Y' then
                  L_temp_rec.nom_flag_3 := L_rms_rec.nom_flag_3;
               end if;
               if L_mi_rec.nom_flag_4_mi <> 'Y' then
                  L_temp_rec.nom_flag_4 := L_rms_rec.nom_flag_4;
               end if;
               if L_mi_rec.nom_flag_5_mi <> 'Y' then
                  L_temp_rec.nom_flag_5 := L_rms_rec.nom_flag_5;
               end if;
            end if;
            --Resolve new value for action column.
             if L_stg_exists then
                --if it is a create record and record already exists in staging, log an error
                if L_temp_rec.action = PO_INDUCT_SQL.ACTION_NEW then
                   if L_stg_rec.process$status = 'P' then
                      L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS', L_pk_columns);
                   else
                      L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', L_pk_columns);
                   end if;
                   WRITE_S9T_ERROR( I_file_id,OHA_sheet,L_temp_rec.row_seq,NULL,NULL,L_error_msg);
                   L_error := TRUE;
                else
                   --if record does not exist in staging then keep the user provided value
                   L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,L_temp_rec.action);
                end if;
            end if;
         EXCEPTION
            when RECORD_LOCKED then
               L_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                 L_table,
                                                 NULL,
                                                 NULL);
               WRITE_S9T_ERROR(I_file_id,OHA_sheet,L_stg_rec.row_seq,NULL,NULL,L_error_msg);
               L_error := TRUE;
         END;
      end if; --if no L_error
      --Apply defaults if it is a create record and record does not exist in staging already
      if rec.action = PO_INDUCT_SQL.ACTION_NEW and
         NOT L_error and
         NOT L_stg_exists then
         L_temp_rec.order_no         := NVL(L_temp_rec.order_no,L_default_rec.order_no);
         L_temp_rec.item             := NVL(L_temp_rec.item,L_default_rec.item);
         L_temp_rec.pack_item        := NVL(L_temp_rec.pack_item,L_default_rec.pack_item);
         L_temp_rec.hts              := NVL(L_temp_rec.hts,L_default_rec.hts);
         L_temp_rec.comp_id          := NVL(L_temp_rec.comp_id,L_default_rec.comp_id);
         L_temp_rec.cvb_code         := NVL(L_temp_rec.cvb_code,L_default_rec.cvb_code);
         L_temp_rec.comp_rate        := NVL(L_temp_rec.comp_rate,L_default_rec.comp_rate);
         L_temp_rec.per_count        := NVL(L_temp_rec.per_count,L_default_rec.per_count);
         L_temp_rec.per_count_uom    := NVL(L_temp_rec.per_count_uom,L_default_rec.per_count_uom);
         L_temp_rec.est_assess_value := NVL(L_temp_rec.est_assess_value,L_default_rec.est_assess_value);
         L_temp_rec.nom_flag_1       := NVL(L_temp_rec.nom_flag_1,L_default_rec.nom_flag_1);
         L_temp_rec.nom_flag_2       := NVL(L_temp_rec.nom_flag_2,L_default_rec.nom_flag_2);
         L_temp_rec.nom_flag_3       := NVL(L_temp_rec.nom_flag_3,L_default_rec.nom_flag_3);
         L_temp_rec.nom_flag_4       := NVL(L_temp_rec.nom_flag_4,L_default_rec.nom_flag_4);
         L_temp_rec.nom_flag_5       := NVL(L_temp_rec.nom_flag_5,L_default_rec.nom_flag_5);
      end if;
      -- Check PK cols. Log error if any PK col is null.
      if NOT (L_temp_rec.order_no is NOT NULL and
              L_temp_rec.item is NOT NULL and
              L_temp_rec.hts is NOT NULL and
              L_temp_rec.comp_id is NOT NULL) then
         L_error_msg := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                           L_pk_columns);
         WRITE_S9T_ERROR(I_file_id,
                         OHA_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         L_error_msg);
         L_error := TRUE;
      end if;
      --if no error so far then add temp record to insert or update collection depending upon
      --whether record already exists in staging or not.
      if NOT L_error then
        if L_stg_exists then
           svc_upd_col.extend();
           svc_upd_col(svc_upd_col.count()) := L_temp_rec;
        else
           svc_ins_col.extend();
           svc_ins_col(svc_ins_col.count()) := L_temp_rec;
        end if;
      end if;
   END LOOP;

   --flush insert collection
   BEGIN
      FORALL i IN 1..svc_ins_col.COUNT SAVE EXCEPTIONS
         insert into svc_ordsku_hts_assess
              values svc_ins_col(i);
   EXCEPTION
      when DML_ERRORS then
         FOR i in 1..SQL%BULK_EXCEPTIONS.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                 L_pk_columns,
                                                 L_table);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            OHA_sheet,
                            svc_ins_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;

   --flush update collection
   FOR i in 1..svc_upd_col.COUNT LOOP
      update svc_ordsku_hts_assess
         set process_id         = svc_upd_col(i).process_id
            ,chunk_id           = svc_upd_col(i).chunk_id
            ,row_seq            = svc_upd_col(i).row_seq
            ,action             = svc_upd_col(i).action
            ,process$status     = svc_upd_col(i).process$status
            ,cvb_code           = svc_upd_col(i).cvb_code
            ,comp_rate          = svc_upd_col(i).comp_rate
            ,per_count          = svc_upd_col(i).per_count
            ,per_count_uom      = svc_upd_col(i).per_count_uom
            ,est_assess_value   = svc_upd_col(i).est_assess_value
            ,nom_flag_1         = svc_upd_col(i).nom_flag_1
            ,nom_flag_2         = svc_upd_col(i).nom_flag_2
            ,nom_flag_3         = svc_upd_col(i).nom_flag_3
            ,nom_flag_4         = svc_upd_col(i).nom_flag_4
            ,nom_flag_5         = svc_upd_col(i).nom_flag_5
            ,last_upd_id        = svc_upd_col(i).last_upd_id
            ,last_upd_datetime  = svc_upd_col(i).last_upd_datetime
       where order_no = svc_upd_col(i).order_no
         and item = svc_upd_col(i).item
         and NVL(pack_item, '-999') = NVL(svc_upd_col(i).pack_item, '-999')
         and hts = svc_upd_col(i).hts
         and comp_id = svc_upd_col(i).comp_id;
   END LOOP;

END PROCESS_S9T_OHA;
-------------------------------------------------------------------------------------------------------
FUNCTION IS_NUMBER(I_string IN VARCHAR2)
RETURN BOOLEAN IS
   L_num   NUMBER;
BEGIN
   L_num := TO_NUMBER(I_string);
   return TRUE;

EXCEPTION
   when VALUE_ERROR then
      return FALSE;
END IS_NUMBER;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T_CFA(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_file_id        IN      S9T_FOLDER.FILE_ID%TYPE,
                         I_process_id     IN      SVC_CFA_EXT.PROCESS_ID%TYPE,
                         I_template_key   IN      SVC_PROCESS_TRACKER.TEMPLATE_KEY%TYPE)
RETURN BOOLEAN IS
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   L_program       VARCHAR2(75) := 'PO_INDUCT_SQL.PROCESS_S9T_CFA';

   cursor C_GET_CFA_ORDER is
      select key_val_pairs_pkg.get_attr(cfa.keys_col,'ORDER_NO') as order_no,
             cfa.row_seq,
             cfa.group_set_view_name
        from svc_cfa_ext cfa
       where cfa.process_id = I_process_id
         and cfa.process$status <> 'E';

   TYPE cfa_order IS TABLE OF C_GET_CFA_ORDER%ROWTYPE INDEX BY BINARY_INTEGER;
   L_cfa_order_tab   cfa_order;

BEGIN
   if CORESVC_CFLEX.PROCESS_S9T_SHEETS(O_error_message,
                                       I_file_id,
                                       I_process_id,
                                       I_template_key) = FALSE then
      return FALSE;
   end if;

   --ensure that the key column is numeric
   open C_GET_CFA_ORDER;
   fetch C_GET_CFA_ORDER bulk collect into L_cfa_order_tab;
   close C_GET_CFA_ORDER;

   if L_cfa_order_tab is NOT NULL and L_cfa_order_tab.COUNT > 0 then
      FOR i in 1..L_cfa_order_tab.COUNT LOOP
         if NOT IS_NUMBER(L_cfa_order_tab(i).order_no) then
            L_error_msg := SQL_LIB.CREATE_MSG('CFA_KEY_NUMERIC',
                                              'ORDER_NO');
            WRITE_S9T_ERROR(I_file_id,
                            OHE_CFA_sheet,
                            L_cfa_order_tab(i).row_seq,
                            'KEYS_COL',
                            NULL,
                            L_error_msg);
         end if;
      END LOOP;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_S9T_CFA;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_PRC_DEST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN VARCHAR2 IS
   L_program   VARCHAR2(75) := 'PO_INDUCT_SQL.GET_PRC_DEST';
   O_dest   SVC_PROCESS_TRACKER.PROCESS_DESTINATION%TYPE;

   cursor C_GET_DEST is
     select process_destination
       from svc_process_tracker
      where process_id = I_process_id;

BEGIN
   open C_GET_DEST;
   fetch C_GET_DEST into O_dest;
   close C_GET_DEST;
   return O_dest;

END GET_PRC_DEST;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_NEW_ACTION(I_old_action   IN   VARCHAR2,
                        I_new_action   IN   VARCHAR2)
RETURN VARCHAR2 IS
   O_new_action   SVC_ITEM_MASTER.ACTION%TYPE;

BEGIN
   case
      when I_old_action = PO_INDUCT_SQL.ACTION_NEW and
           I_new_action = PO_INDUCT_SQL.ACTION_DEL then
         O_new_action := PO_INDUCT_SQL.ACTION_DEL;
      when I_old_action = PO_INDUCT_SQL.ACTION_NEW and
           I_new_action = PO_INDUCT_SQL.ACTION_MOD then
         O_new_action := PO_INDUCT_SQL.ACTION_NEW;
      when I_old_action = PO_INDUCT_SQL.ACTION_DEL and
           I_new_action = PO_INDUCT_SQL.ACTION_DEL then
         O_new_action := PO_INDUCT_SQL.ACTION_DEL;
      when I_old_action = PO_INDUCT_SQL.ACTION_DEL and
           I_new_action = PO_INDUCT_SQL.ACTION_MOD then
         O_new_action := PO_INDUCT_SQL.ACTION_DEL;
      when I_old_action = PO_INDUCT_SQL.ACTION_MOD and
           I_new_action = PO_INDUCT_SQL.ACTION_DEL then
         O_new_action := PO_INDUCT_SQL.ACTION_DEL;
      when I_old_action = PO_INDUCT_SQL.ACTION_MOD and
           I_new_action = PO_INDUCT_SQL.ACTION_MOD then
         O_new_action := PO_INDUCT_SQL.ACTION_MOD;
      when I_old_action = PO_INDUCT_SQL.ACTION_NEW and
           I_new_action is NULL then
         O_new_action := PO_INDUCT_SQL.ACTION_NEW;
      when I_old_action = PO_INDUCT_SQL.ACTION_MOD and
           I_new_action is NULL then
         O_new_action := PO_INDUCT_SQL.ACTION_MOD;
      when I_old_action = PO_INDUCT_SQL.ACTION_DEL and
           I_new_action is NULL then
         O_new_action := PO_INDUCT_SQL.ACTION_DEL;
      else
         O_new_action := I_new_action;
   end case;
   return O_new_action;

END GET_NEW_ACTION;
-------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file        S9T_FILE;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN

   L_file              := NEW s9t_file();
   O_file_id           := S9T_FOLDER_SEQ.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := PO_INDUCT_SQL.FILE_NAME;
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(OHA_sheet);
   L_file.sheets(L_file.get_sheet_index(OHA_sheet)).column_headers := s9t_cells('ACTION'
                                                                               ,'ORDER_NO'
                                                                               ,'ITEM'
                                                                               ,'PACK_ITEM'
                                                                               ,'HTS'
                                                                               ,'COMP_ID'
                                                                               ,'CVB_CODE'
                                                                               ,'COMP_RATE'
                                                                               ,'PER_COUNT'
                                                                               ,'PER_COUNT_UOM'
                                                                               ,'EST_ASSESS_VALUE'
                                                                               ,'NOM_FLAG_1'
                                                                               ,'NOM_FLAG_2'
                                                                               ,'NOM_FLAG_3'
                                                                               ,'NOM_FLAG_4'
                                                                               ,'NOM_FLAG_5');
   L_file.add_sheet(ORH_sheet);
   L_file.sheets(L_file.get_sheet_index(ORH_sheet)).column_headers := s9t_cells('ACTION'
                                                                               ,'ORDER_NO'
                                                                               ,'ITEM'
                                                                               ,'PACK_ITEM'
                                                                               ,'HTS'
                                                                               ,'IMPORT_COUNTRY_ID'
                                                                               ,'EFFECT_FROM'
                                                                               ,'EFFECT_TO'
                                                                               ,'STATUS'
                                                                               ,'ORIGIN_COUNTRY_ID');
   L_file.add_sheet(ORE_sheet);
   L_file.sheets(L_file.get_sheet_index(ORE_sheet)).column_headers := s9t_cells('ACTION'
                                                                               ,'ORDER_NO'
                                                                               ,'ITEM'
                                                                               ,'PACK_ITEM'
                                                                               ,'LOCATION'
                                                                               ,'LOC_TYPE'
                                                                               ,'COMP_ID'
                                                                               ,'CVB_CODE'
                                                                               ,'COST_BASIS'
                                                                               ,'COMP_RATE'
                                                                               ,'COMP_CURRENCY'
                                                                               ,'EXCHANGE_RATE'
                                                                               ,'PER_COUNT'
                                                                               ,'PER_COUNT_UOM'
                                                                               ,'EST_EXP_VALUE'
                                                                               ,'NOM_FLAG_1'
                                                                               ,'NOM_FLAG_2'
                                                                               ,'NOM_FLAG_3'
                                                                               ,'NOM_FLAG_4'
                                                                               ,'NOM_FLAG_5'
                                                                               ,'ORIGIN'
                                                                               ,'DEFAULTED_FROM'
                                                                               ,'KEY_VALUE_1'
                                                                               ,'KEY_VALUE_2');
   L_file.add_sheet(ORD_sheet);
   L_file.sheets(L_file.get_sheet_index(ORD_sheet)).column_headers := s9t_cells('ACTION'
                                                                               ,'ORDER_NO'
                                                                               ,'LC_REF_ID'
                                                                               ,'LC_GROUP_ID'
                                                                               ,'APPLICANT'
                                                                               ,'BENEFICIARY'
                                                                               ,'MERCH_DESC'
                                                                               ,'TRANSSHIPMENT_IND'
                                                                               ,'PARTIAL_SHIPMENT_IND'
                                                                               ,'LC_IND');

   L_file.add_sheet(ODT_sheet);
   L_file.sheets(L_file.get_sheet_index(ODT_sheet)).column_headers := s9t_cells('ACTION'
                                                                               ,'ORDER_NO'
                                                                               ,'ITEM'
                                                                               ,'ITEM_DESC'
                                                                               ,'VPN'
                                                                               ,'ITEM_PARENT'
                                                                               ,'REF_ITEM'
                                                                               ,'DIFF_1'
                                                                               ,'DIFF_2'
                                                                               ,'DIFF_3'
                                                                               ,'DIFF_4'
                                                                               ,'ORIGIN_COUNTRY_ID'
                                                                               ,'SUPP_PACK_SIZE'
                                                                               ,'LOC_TYPE'
                                                                               ,'LOCATION'
                                                                               ,'LOCATION_DESC'
                                                                               ,'UNIT_COST'
                                                                               ,'QTY_ORDERED'
                                                                               ,'DELIVERY_DATE'
                                                                               ,'NON_SCALE_IND'
                                                                               ,'PROCESSING_TYPE'
                                                                               ,'PROCESSING_WH'
                                                                               ,'ALLOC_NO'
                                                                               ,'QTY_CANCELLED'
                                                                               ,'QTY_TRANSFERRED'
                                                                               ,'CANCEL_CODE'
                                                                               ,'RELEASE_DATE'
                                                                               ,'IN_STORE_DATE');

   L_file.add_sheet(OHE_sheet);
   L_file.sheets(L_file.get_sheet_index(OHE_sheet)).column_headers := s9t_cells('ACTION'
                                                                               ,'ORDER_NO'
                                                                               ,'ORDER_TYPE'
                                                                               ,'DEPT'
                                                                               ,'BUYER'
                                                                               ,'SUPPLIER'
                                                                               ,'LOC_TYPE'
                                                                               ,'LOCATION'
                                                                               ,'PROMOTION'
                                                                               ,'QC_IND'
                                                                               ,'WRITTEN_DATE'
                                                                               ,'NOT_BEFORE_DATE'
                                                                               ,'NOT_AFTER_DATE'
                                                                               ,'OTB_EOW_DATE'
                                                                               ,'EARLIEST_SHIP_DATE'
                                                                               ,'LATEST_SHIP_DATE'
                                                                               ,'TERMS'
                                                                               ,'FREIGHT_TERMS'
                                                                               ,'ORIG_IND'
                                                                               ,'PAYMENT_METHOD'
                                                                               ,'BACKHAUL_TYPE'
                                                                               ,'BACKHAUL_ALLOWANCE'
                                                                               ,'SHIP_METHOD'
                                                                               ,'PURCHASE_TYPE'
                                                                               ,'STATUS'
                                                                               ,'SHIP_PAY_METHOD'
                                                                               ,'FOB_TRANS_RES'
                                                                               ,'FOB_TRANS_RES_DESC'
                                                                               ,'FOB_TITLE_PASS'
                                                                               ,'FOB_TITLE_PASS_DESC'
                                                                               ,'EDI_PO_IND'
                                                                               ,'IMPORT_ORDER_IND'
                                                                               ,'IMPORT_COUNTRY_ID'
                                                                               ,'INCLUDE_ON_ORDER_IND'
                                                                               ,'VENDOR_ORDER_NO'
                                                                               ,'EXCHANGE_RATE'
                                                                               ,'FACTORY'
                                                                               ,'AGENT'
                                                                               ,'DISCHARGE_PORT'
                                                                               ,'LADING_PORT'
                                                                               ,'FREIGHT_CONTRACT_NO'
                                                                               ,'PO_TYPE'
                                                                               ,'PRE_MARK_IND'
                                                                               ,'CURRENCY_CODE'
                                                                               ,'REJECT_CODE'
                                                                               ,'CONTRACT_NO'
                                                                               ,'PICKUP_LOC'
                                                                               ,'PICKUP_NO'
                                                                               ,'PICKUP_DATE'
                                                                               ,'COMMENT_DESC'
                                                                               ,'PARTNER_TYPE_1'
                                                                               ,'PARTNER1'
                                                                               ,'PARTNER_TYPE_2'
                                                                               ,'PARTNER2'
                                                                               ,'PARTNER_TYPE_3'
                                                                               ,'PARTNER3'
                                                                               ,'IMPORT_TYPE'
                                                                               ,'IMPORT_ID'
                                                                               ,'CLEARING_ZONE_ID'
                                                                               ,'ROUTING_LOC_ID'
                                                                               ,'EXT_REF_NO'
                                                                               ,'MASTER_PO_NO'
                                                                               ,'RE_APPROVE_IND'
                                                                               ,'LAST_UPD_ID'
                                                                               ,'NEXT_UPD_ID'
                                                                               ,'LAST_UPD_DATETIME');

   S9T_PKG.SAVE_OBJ(L_file);

END INIT_S9T;
-------------------------------------------------------------------------------------------------------
FUNCTION NEXT_UPDATE_ID_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists           IN OUT   BOOLEAN,
                               I_next_update_id   IN       SVC_ORDHEAD.NEXT_UPD_ID%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(75) := 'PO_INDUCT_SQL.NEXT_UPDATE_ID_EXISTS';
   L_exists    VARCHAR2(1);

   cursor C_NEXT_UPD_ID is
      select 'x'
        from svc_ordhead
       where next_upd_id = upper(I_next_update_id);

BEGIN
   open C_NEXT_UPD_ID;
   fetch C_NEXT_UPD_ID into L_exists;
   close C_NEXT_UPD_ID;

   if L_exists is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END NEXT_UPDATE_ID_EXISTS;
---------------------------------------------------------------------------------------
FUNCTION LAST_UPDATE_ID_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists           IN OUT   BOOLEAN,
                               I_last_update_id   IN       SVC_ORDHEAD.LAST_UPD_ID%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(75) := 'PO_INDUCT_SQL.LAST_UPDATE_ID_EXISTS';
   L_exists    VARCHAR2(1);

   cursor C_LAST_UPD_ID is
      select 'x'
        from svc_ordhead
       where last_upd_id = upper(I_last_update_id);

BEGIN
   open C_LAST_UPD_ID;
   fetch C_LAST_UPD_ID into L_exists;
   close C_LAST_UPD_ID;

   if L_exists is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LAST_UPDATE_ID_EXISTS;
---------------------------------------------------------------------------------------
FUNCTION VENDOR_NO_EXISTS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists            IN OUT   BOOLEAN,
                          I_vendor_order_no   IN       ORDHEAD.VENDOR_ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(75) := 'PO_INDUCT_SQL.VENDOR_NO_EXISTS';
   L_exists    VARCHAR2(1);

   cursor C_VENDOR_NO_EXISTS is
      select 'x'
         from ordhead
        where upper(vendor_order_no) = upper(I_vendor_order_no);

BEGIN
   open C_VENDOR_NO_EXISTS;
   fetch C_VENDOR_NO_EXISTS into L_exists;
   close C_VENDOR_NO_EXISTS;

   if L_exists is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VENDOR_NO_EXISTS;
---------------------------------------------------------------------------------------
FUNCTION DELETE_PROCESSES(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id_tab   IN       NUM_TAB)
RETURN BOOLEAN IS
   L_program         VARCHAR2(75) := 'PO_INDUCT_SQL.DELETE_PROCESSES';
   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_PROCESS_PO is
      select 'x'
        from svc_process_pos
       where process_id in (select * from TABLE(CAST(I_process_id_tab as NUM_TAB)))
         for update nowait;

   cursor C_LOCK_SVC_ORDHEAD is
      select 'x'
        from svc_ordhead
       where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB)))
         for update nowait;

   cursor C_LOCK_SVC_ORDDETAIL is
      select 'x'
        from svc_orddetail
       where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB)))
         for update nowait;

   cursor C_LOCK_SVC_ORDHEAD_CFA_EXT is
      select 'x'
        from svc_ordhead_cfa_ext
       where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB)))
         for update nowait;

   cursor C_LOCK_SVC_ORDLC is
      select 'x'
        from svc_ordlc
       where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB)))
         for update nowait;

   cursor C_LOCK_SVC_ORDLOC_EXP is
      select 'x'
        from svc_ordloc_exp
       where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB)))
         for update nowait;

   cursor C_LOCK_SVC_ORDSKU_HTS is
      select 'x'
        from svc_ordsku_hts
       where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB)))
         for update nowait;

   cursor C_LOCK_SVC_ORDSKU_HTS_ASSESS is
      select 'x'
        from svc_ordsku_hts_assess
       where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB)))
         for update nowait;

   cursor C_LOCK_SVC_CFA_EXT is
      select 'x'
        from svc_cfa_ext
       where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB)))
         for update nowait;

   cursor C_LOCK_CORESVC_PO_ERR is
      select 'x'
        from coresvc_po_err
       where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB)))
         for update nowait;

   cursor C_LOCK_S9T_ERRORS is
      select 'x'
        from s9t_errors
       where file_id in (select file_id
                           from svc_process_tracker
                          where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB))))
         for update nowait;

   cursor C_LOCK_CORESVC_PO_CHUNKS is
      select 'x'
        from coresvc_po_chunks
       where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB)))
         for update nowait;

   cursor C_LOCK_SVC_PROCESS_TRACKER is
      select 'x'
        from svc_process_tracker
       where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB)))
         for update nowait;

   cursor C_LOCK_S9T_FOLDER is
      select 'x'
        from s9t_folder
       where file_id in (select file_id
                           from svc_process_tracker
                          where process_id in (select * from TABLE(CAST(I_process_id_tab AS NUM_TAB))))
         for update nowait;

BEGIN
   L_table := 'SVC_PROCESS_POS';
   open C_LOCK_SVC_PROCESS_PO;
   close C_LOCK_SVC_PROCESS_PO;
   forall i IN 1..I_process_id_tab.COUNT
      delete from SVC_PROCESS_POS
       where process_id = I_process_id_tab(i);

   L_table := 'SVC_ORDHEAD';
   open C_LOCK_SVC_ORDHEAD;
   close C_LOCK_SVC_ORDHEAD;
   forall i IN 1..I_process_id_tab.COUNT
      delete from svc_ordhead
       where process_id = I_process_id_tab(i);

   L_table := 'SVC_ORDDETAIL';
   open C_LOCK_SVC_ORDDETAIL;
   close C_LOCK_SVC_ORDDETAIL;
   forall i IN 1..I_process_id_tab.COUNT
      delete from svc_orddetail
       where process_id = I_process_id_tab(i);

   L_table := 'SVC_ORDLC';
   open C_LOCK_SVC_ORDLC;
   close C_LOCK_SVC_ORDLC;
   forall i IN 1..I_process_id_tab.COUNT
     delete from svc_ordlc
      where process_id = I_process_id_tab(i);

   L_table := 'SVC_ORDLOC_EXP';
   open C_LOCK_SVC_ORDLOC_EXP;
   close C_LOCK_SVC_ORDLOC_EXP;
   forall i IN 1..I_process_id_tab.COUNT
     delete from svc_ordloc_exp
      where process_id = I_process_id_tab(i);

   L_table := 'SVC_ORDSKU_HTS';
   open C_LOCK_SVC_ORDSKU_HTS;
   close C_LOCK_SVC_ORDSKU_HTS;
   forall i IN 1..I_process_id_tab.COUNT
     delete from svc_ordsku_hts
      where process_id = I_process_id_tab(i);

   L_table := 'SVC_ORDSKU_HTS_ASSESS';
   open C_LOCK_SVC_ORDSKU_HTS_ASSESS;
   close C_LOCK_SVC_ORDSKU_HTS_ASSESS;
   forall i IN 1..I_process_id_tab.COUNT
     delete from svc_ordsku_hts_assess
      where process_id = I_process_id_tab(i);

   L_table := 'SVC_CFA_EXT';
   open C_LOCK_SVC_CFA_EXT;
   close C_LOCK_SVC_CFA_EXT;
   forall i IN 1..I_process_id_tab.COUNT
     delete from svc_cfa_ext
      where process_id = I_process_id_tab(i);

   L_table := 'S9T_ERRORS';
   open C_LOCK_S9T_ERRORS;
   close C_LOCK_S9T_ERRORS;
   forall i IN 1..I_process_id_tab.COUNT
      delete from s9t_errors
       where file_id = (select file_id
                          from svc_process_tracker
                         where process_id = I_process_id_tab(i));

   L_table := 'CORESVC_PO_CHUNKS';
   open C_LOCK_CORESVC_PO_CHUNKS;
   close C_LOCK_CORESVC_PO_CHUNKS;
   forall i IN 1..I_process_id_tab.COUNT
      delete from coresvc_po_chunks
       where process_id = I_process_id_tab(i);

   L_table := 'S9T_FOLDER';
   open C_LOCK_S9T_FOLDER;
   close C_LOCK_S9T_FOLDER;
   forall i IN 1..I_process_id_tab.COUNT
      delete from s9t_folder
       where file_id = (select file_id
                          from svc_process_tracker
                         where process_id = I_process_id_tab(i));

   L_table := 'SVC_PROCESS_TRACKER';
   open C_LOCK_SVC_PROCESS_TRACKER;
   close C_LOCK_SVC_PROCESS_TRACKER;
   forall i IN 1..I_process_id_tab.COUNT
      delete from svc_process_tracker
       where process_id = I_process_id_tab(i);

   L_table := 'CORESVC_PO_ERR';
   open C_LOCK_CORESVC_PO_ERR;
   close C_LOCK_CORESVC_PO_ERR;
   forall i IN 1..I_process_id_tab.COUNT
      delete from coresvc_po_err
       where process_id = I_process_id_tab(i);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('RECORDS_LOCKED',
                                            L_table);
      return FALSE;
   when OTHERS then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END DELETE_PROCESSES;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_CASCADE_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(75) := 'PO_INDUCT_SQL.PROCESS_CASCADE_DELETE';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_ORDDETAIL is
      with sub as (select spt.order_no
                     from svc_po_del spt
                    where spt.svc_tbl_name = 'SVC_ORDHEAD'
                      and not exists (select 'x'
                                        from svc_ordhead soh
                                       where soh.order_no = spt.order_no))
      select 'x'
        from svc_orddetail odt,
             sub
       where odt.order_no = sub.order_no
         for update nowait;

   cursor C_LOCK_SVC_ORDHEAD_CFA_EXT is
      with sub as (select spt.order_no
                     from svc_po_del spt
                    where spt.svc_tbl_name = 'SVC_ORDHEAD'
                      and not exists (select 'x'
                                        from svc_ordhead soh
                                       where soh.order_no = spt.order_no))
      select 'x'
        from svc_ordhead_cfa_ext oce,
             sub
       where oce.order_no = sub.order_no
         for update nowait;

   cursor C_LOCK_SVC_CFA_EXT is
      with sub as (select spt.order_no
                     from svc_po_del spt
                    where spt.svc_tbl_name = 'SVC_ORDHEAD'
                      and not exists (select 'x'
                                        from svc_ordhead soh
                                       where soh.order_no = spt.order_no))
      select 'x'
        from svc_cfa_ext cfa,
             sub
       where (key_val_pairs_pkg.get_attr(cfa.keys_col,'ORDER_NO')) = to_char(sub.order_no)
         for update nowait;

   cursor C_LOCK_SVC_ORDLC is
      with sub as (select spt.order_no
                     from svc_po_del spt
                    where spt.svc_tbl_name = 'SVC_ORDHEAD'
                      and not exists (select 'x'
                                        from svc_ordhead soh
                                       where soh.order_no = spt.order_no))
      select 'x'
        from svc_ordlc olc,
             sub
       where olc.order_no = sub.order_no
         for update nowait;

   cursor C_LOCK_SVC_ORDLOC_EXP_HDR is
      with sub as (select spt.order_no
                     from svc_po_del spt
                    where spt.svc_tbl_name = 'SVC_ORDHEAD'
                      and not exists (select 'x'
                                        from svc_ordhead soh
                                       where soh.order_no = spt.order_no))
      select 'x'
        from svc_ordloc_exp ole,
             sub
       where ole.order_no = sub.order_no
         for update nowait;

   cursor C_LOCK_SVC_ORDLOC_EXP_DTL is
      with sub as (select spt.order_no,
                          spt.item,
                          spt.location
                     from svc_po_del spt
                    where spt.svc_tbl_name = 'SVC_ORDDETAIL'
                      and not exists (select 'x'
                                        from svc_orddetail soh
                                       where soh.order_no = spt.order_no
                                         and soh.item = spt.item
                                         and NVL(soh.location, -999) = NVL(spt.location, -999)))
      select 'x'
        from svc_ordloc_exp ole,
             sub
       where ole.order_no = sub.order_no
         and ole.item = sub.item
         and (ole.location = sub.location or sub.location is NULL)
         for update nowait;

   cursor C_LOCK_SVC_ORDSKU_HTS_HDR is
      with sub as (select spt.order_no
                     from svc_po_del spt
                    where spt.svc_tbl_name = 'SVC_ORDHEAD'
                      and not exists (select 'x'
                                        from svc_ordhead soh
                                       where soh.order_no = spt.order_no))
      select 'x'
        from svc_ordsku_hts osh,
             sub
       where osh.order_no = sub.order_no
         for update nowait;

   cursor C_LOCK_SVC_ORDSKU_HTS_DTL is
      with sub as (select spt.order_no,
                          spt.item
                     from svc_po_del spt
                    where spt.svc_tbl_name = 'SVC_ORDDETAIL'
                      and not exists (select 'x'
                                        from svc_orddetail soh
                                       where soh.order_no = spt.order_no
                                         and soh.item = spt.item
                                         and soh.location = spt.location))
      select 'x'
        from svc_ordsku_hts osh,
             sub
       where osh.order_no = sub.order_no
         and osh.item = sub.item
         for update nowait;

   cursor C_LOCK_SVC_HTS_ASSESS_HDR is
      with sub as (select spt.order_no
                     from svc_po_del spt
                    where spt.svc_tbl_name = 'SVC_ORDHEAD'
                      and not exists (select 'x'
                                        from svc_ordhead soh
                                       where soh.order_no = spt.order_no))
      select 'x'
        from svc_ordsku_hts_assess oha,
             sub
       where oha.order_no = sub.order_no
         for update nowait;

   cursor C_LOCK_SVC_HTS_ASSESS_DTL is
      with sub as (select spt.order_no,
                          spt.item
                     from svc_po_del spt
                    where spt.svc_tbl_name = 'SVC_ORDDETAIL'
                      and not exists (select 'x'
                                        from svc_orddetail soh
                                       where soh.order_no = spt.order_no
                                         and soh.item = spt.item))
      select 'x'
        from svc_ordsku_hts_assess oha,
             sub
       where oha.order_no = sub.order_no
         and oha.item = sub.item
         for update nowait;

   cursor C_LOCK_SVC_ORDSKU_HTS_ASSESS is
      with sub as (select spt.order_no,
                          spt.item,
                          NVL(spt.pack, '-999') as pack_item,
                          spt.hts
                     from svc_po_del spt
                    where spt.svc_tbl_name = 'SVC_ORDSKU_HTS'
                      and not exists (select 'x'
                                        from svc_ordsku_hts soh
                                       where soh.order_no = spt.order_no
                                         and soh.item = spt.item
                                         and soh.hts = spt.hts
                                         and NVL(soh.pack_item, '-999') = NVL(spt.pack, '-999')))
      select 'x'
        from svc_ordsku_hts_assess oha,
             sub
       where oha.order_no = sub.order_no
         and oha.item = sub.item
         and oha.hts = sub.hts
         and NVL(oha.pack_item, '-999') = sub.pack_item
         for update nowait;

   cursor C_GET_OHE_DELETES is
      select spt.order_no
        from svc_po_del spt
       where spt.svc_tbl_name = 'SVC_ORDHEAD'
         and not exists (select 'x'
                           from svc_ordhead soh
                          where soh.order_no = spt.order_no);

   cursor C_GET_ODT_DELETES is
      select spt.order_no,
             spt.item,
             spt.location
        from svc_po_del spt
       where spt.svc_tbl_name = 'SVC_ORDDETAIL'
         and not exists (select 'x'
                           from svc_orddetail sodt
                          where sodt.order_no = spt.order_no
                            and sodt.item = spt.item
                            and sodt.location = spt.location);

   cursor C_GET_OHA_DELETES is
      select spt.order_no,
             spt.item,
             spt.pack,
             spt.hts
        from svc_po_del spt
       where spt.svc_tbl_name = 'SVC_ORDSKU_HTS'
         and not exists (select 'x'
                           from svc_ordsku_hts soha
                          where soha.order_no = spt.order_no
                            and soha.item = spt.item
                            and soha.pack_item = spt.pack
                            and soha.hts = spt.hts);

   TYPE SVC_OHE_TBL is TABLE OF C_GET_OHE_DELETES%ROWTYPE INDEX BY BINARY_INTEGER;
   L_get_ohe_rec   SVC_OHE_TBL;

   TYPE SVC_ODT_TBL is TABLE OF C_GET_ODT_DELETES%ROWTYPE INDEX BY BINARY_INTEGER;
   L_get_odt_rec   SVC_ODT_TBL;

   TYPE SVC_OHA_TBL is TABLE OF C_GET_OHA_DELETES%ROWTYPE INDEX BY BINARY_INTEGER;
   L_get_oha_rec   SVC_OHA_TBL;

BEGIN

   open C_GET_OHE_DELETES;
   fetch C_GET_OHE_DELETES bulk collect into L_get_ohe_rec;
   close C_GET_OHE_DELETES;

   if L_get_ohe_rec is NOT NULL and L_get_ohe_rec.COUNT > 0 then
      L_table := 'SVC_ORDDETAIL';
      open C_LOCK_SVC_ORDDETAIL;
      close C_LOCK_SVC_ORDDETAIL;
      forall i in 1..L_get_ohe_rec.COUNT
         delete from svc_orddetail
          where order_no = L_get_ohe_rec(i).order_no;
      ---
      L_table := 'SVC_ORDLC';
      open C_LOCK_SVC_ORDLC;
      close C_LOCK_SVC_ORDLC;
      forall i in 1..L_get_ohe_rec.COUNT
         delete from svc_ordlc
          where order_no = L_get_ohe_rec(i).order_no;
      ---
      L_table := 'SVC_ORDLOC_EXP';
      open C_LOCK_SVC_ORDLOC_EXP_HDR;
      close C_LOCK_SVC_ORDLOC_EXP_HDR;
      forall i in 1..L_get_ohe_rec.COUNT
         delete from svc_ordloc_exp
          where order_no = L_get_ohe_rec(i).order_no;
      ---
      L_table := 'SVC_ORDSKU_HTS';
      open C_LOCK_SVC_ORDSKU_HTS_HDR;
      close C_LOCK_SVC_ORDSKU_HTS_HDR;
      forall i in 1..L_get_ohe_rec.COUNT
         delete from svc_ordsku_hts
          where order_no = L_get_ohe_rec(i).order_no;
      ---
      L_table := 'SVC_ORDSKU_HTS_ASSESS';
      open C_LOCK_SVC_HTS_ASSESS_HDR;
      close C_LOCK_SVC_HTS_ASSESS_HDR;
      forall i in 1..L_get_ohe_rec.COUNT
         delete from svc_ordsku_hts_assess
          where order_no = L_get_ohe_rec(i).order_no;
      ---
      L_table := 'SVC_ORDHEAD_CFA_EXT';
      open C_LOCK_SVC_ORDHEAD_CFA_EXT;
      close C_LOCK_SVC_ORDHEAD_CFA_EXT;
      forall i in 1..L_get_ohe_rec.COUNT
         delete from svc_ordhead_cfa_ext
          where order_no = L_get_ohe_rec(i).order_no;
      ---
      L_table := 'SVC_CFA_EXT';
      open C_LOCK_SVC_CFA_EXT;
      close C_LOCK_SVC_CFA_EXT;
      forall i in 1..L_get_ohe_rec.COUNT
         delete from svc_cfa_ext
          where (key_val_pairs_pkg.get_attr(keys_col,'ORDER_NO')) = to_char(L_get_ohe_rec(i).order_no);
   end if;

   open C_GET_ODT_DELETES;
   fetch C_GET_ODT_DELETES bulk collect into L_get_odt_rec;
   close C_GET_ODT_DELETES;

   if L_get_odt_rec is NOT NULL and L_get_odt_rec.COUNT > 0 then
      L_table := 'SVC_ORDLOC_EXP';
      open C_LOCK_SVC_ORDLOC_EXP_DTL;
      close C_LOCK_SVC_ORDLOC_EXP_DTL;
      forall i in 1..L_get_odt_rec.COUNT
         delete from svc_ordloc_exp
          where order_no = L_get_odt_rec(i).order_no
            and item = L_get_odt_rec(i).item
            and (location = L_get_odt_rec(i).location or L_get_odt_rec(i).location is NULL);
      ---
      L_table := 'SVC_ORDSKU_HTS';
      open C_LOCK_SVC_ORDSKU_HTS_DTL;
      close C_LOCK_SVC_ORDSKU_HTS_DTL;
      forall i in 1..L_get_odt_rec.COUNT
         delete from svc_ordsku_hts osh
          where osh.order_no = L_get_odt_rec(i).order_no
            and osh.item = L_get_odt_rec(i).item
            and not exists (select 'x'
                              from svc_orddetail odt
                             where odt.order_no = osh.order_no
                               and odt.item = osh.item
                               and rownum = 1);
      ---
      L_table := 'SVC_ORDSKU_HTS_ASSESS';
      open C_LOCK_SVC_HTS_ASSESS_DTL;
      close C_LOCK_SVC_HTS_ASSESS_DTL;
      forall i in 1..L_get_odt_rec.COUNT
         delete from svc_ordsku_hts_assess oha
          where oha.order_no = L_get_odt_rec(i).order_no
            and oha.item = L_get_odt_rec(i).item
            and not exists (select 'x'
                              from svc_orddetail odt
                             where odt.order_no = oha.order_no
                               and odt.item = oha.item
                               and rownum = 1);
   end if;

   open C_GET_OHA_DELETES;
   fetch C_GET_OHA_DELETES bulk collect into L_get_oha_rec;
   close C_GET_OHA_DELETES;

   if L_get_oha_rec is NOT NULL and L_get_oha_rec.COUNT > 0 then
      L_table := 'SVC_ORDSKU_HTS_ASSESS';
      open C_LOCK_SVC_ORDSKU_HTS_ASSESS;
      close C_LOCK_SVC_ORDSKU_HTS_ASSESS;
      forall i in 1..L_get_oha_rec.COUNT
         delete from svc_ordsku_hts_assess
          where order_no = L_get_oha_rec(i).order_no
            and item = L_get_oha_rec(i).item
            and NVL(pack_item, '-999') = NVL(L_get_oha_rec(i).pack, '-999')
            and hts = L_get_oha_rec(i).hts;
   end if;

   delete from svc_po_del;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END PROCESS_CASCADE_DELETE;
---------------------------------------------------------------------------------------
FUNCTION DELETE_PROCESSES_WRP(O_error_message  IN OUT rtk_errors.rtk_text%type,
                              I_process_id_tab IN     num_tab)
RETURN NUMBER IS
  L_program VARCHAR2(255) := 'ITEM_INDUCT_SQL.DELETE_PROCESSES_WRP';
BEGIN
    if delete_processes(O_error_message,
                        I_process_id_tab) then
  RETURN 1;
  else RETURN 0;
  end if;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN 0;
END DELETE_PROCESSES_WRP;

----------------------------------------------------------------------------------------

FUNCTION SEARCH_POS_WRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_order_count     IN OUT   NUMBER,
                    I_sch_crit_rec    IN       PO_INDUCT_SCH_CRIT_TYP,
                    I_source          IN       VARCHAR2,
                    I_process_id      IN       NUMBER)
RETURN NUMBER IS
  L_program VARCHAR2(255) := 'ITEM_INDUCT_SQL.SEARCH_POS_WRP';
BEGIN
    if SEARCH_POS(O_error_message,
                    O_order_count,
                    I_sch_crit_rec,
                    I_source,
                    I_process_id ) then
  RETURN 1;
  else RETURN 0;
  end if;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN 0;
END SEARCH_POS_WRP;
----------------------------------------------------------------------------------------------
END PO_INDUCT_SQL;
/