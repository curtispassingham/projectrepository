CREATE OR REPLACE PACKAGE BODY RMSSUB_XORDER_SQL AS

   LP_approved             CONSTANT VARCHAR2(1) := 'A';
   LP_worksheet            CONSTANT VARCHAR2(1) := 'W';
   LP_closed               CONSTANT VARCHAR2(1) := 'C';

   LP_store                CONSTANT VARCHAR2(1) := 'S';
   LP_warehouse            CONSTANT VARCHAR2(1) := 'W';
   LP_store_type           STORE.STORE_TYPE%TYPE := NULL;
   LP_import_ind           VARCHAR2(1) := 'N';

   LP_prev_loc             ITEM_LOC.LOC%TYPE  := NULL;
   LP_import_ord_ind       ORDHEAD.IMPORT_ORDER_IND%TYPE;
   LP_process_id           SVC_ITEM_MASTER.PROCESS_ID%TYPE;
   LP_chunk_id             SVC_ITEM_MASTER.CHUNK_ID%TYPE;
   LP_process_status       SVC_ITEM_MASTER.PROCESS$STATUS%TYPE;
   LP_user                 VARCHAR2(30):= get_user;
   LP_vdate                DATE;
   TYPE svc_po_tab_typ IS TABLE OF SVC_PO_DEL%ROWTYPE;
   LP_svc_po_tab           svc_po_tab_typ;

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name:  POPULATE_RECORD_HEAD
   -- Purpose      : This private function should populate IO_svc_ordhead_rec rows for a
   --                header create message. It will also perform defaulting logics.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD_HEAD(O_error_message    IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_svc_ordhead_rec IN OUT NOCOPY  SVC_ORDHEAD%ROWTYPE,
                              I_message          IN             "RIB_XOrderDesc_REC",
                              I_message_type     IN             VARCHAR2,
                              I_old_ordhead_row  IN             ORDHEAD%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD_DETAIL
   -- Purpose      : This private function should populate IO_svc_orddetail_tbl record for a header create
   --                message. It will also perform defaulting logics.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD_DETAIL(O_error_message            IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_svc_orddetail_tbl       IN OUT NOCOPY ORDDTL_TAB,
                                I_message                  IN            "RIB_XOrderDesc_REC",
                                I_message_type             IN             VARCHAR2,
                                I_old_ordhead_row          IN             ORDHEAD%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_PRC_DEST(I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
   RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_NEW_ACTION(I_old_action   IN   VARCHAR2,
                        I_new_action   IN   VARCHAR2)
   RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_ORDHEAD
   -- Purpose      :  Inserts into the svc_ordhead staging table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ORDHEAD(O_error_message         OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_svc_ordhead_rec   IN OUT  SVC_ORDHEAD%ROWTYPE,
                            I_action             IN      SVC_ORDHEAD.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_ORDDETAIL
   -- Purpose      :  Inserts into the svc_orddetail staging table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ORDDETAIL(O_error_message         OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_svc_orddetail_tbl IN OUT  ORDDTL_TAB,
                              I_action             IN      SVC_ORDDETAIL.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  VALIDATE_MESSAGE
   -- Purpose      :  Performs specific XOrder validations
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_MESSAGE(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message            IN      "RIB_XOrderDesc_REC",
                          I_message_type       IN      VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_F_ORDER(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                        I_orig_order_status   IN       ORDHEAD.STATUS%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'RMSSUB_XORDER_SQL.CREATE_F_ORDER';
   L_wf_order_no   ORDHEAD.WF_ORDER_NO%TYPE;
   L_status        ORDHEAD.STATUS%TYPE;
   L_vdate         DATE   := GET_VDATE();

   cursor C_LOCK_ORDHEAD is
      select 'x'
        from ordhead
       where order_no = I_order_no
         for update nowait;

BEGIN

   if WF_PO_SQL.SYNC_F_ORDER(O_error_message,
                             L_wf_order_no,
                             L_status,
                             RMS_CONSTANTS.WF_ACTION_CREATE,
                             I_order_no,
                             I_orig_order_status) = FALSE then
      return FALSE;
   end if;

   open C_LOCK_ORDHEAD;
   close C_LOCK_ORDHEAD;

   if L_status = 'A' then
      update ordhead
         set status = 'A',
             orig_approval_id = LP_user,
             orig_approval_date = L_vdate,
             wf_order_no = L_wf_order_no,
             last_update_id = get_user,
             last_update_datetime = sysdate
       where order_no = I_order_no;
   else
      update ordhead
         set wf_order_no = L_wf_order_no,
             last_update_id = get_user,
             last_update_datetime = sysdate
       where order_no = I_order_no;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_F_ORDER;
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 IO_svc_ordhead_rec   IN OUT  SVC_ORDHEAD%ROWTYPE,
                 IO_svc_orddetail_tbl IN OUT  ORDDTL_TAB,
                 I_message_type       IN      VARCHAR2,
                 I_process_id         IN      SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)

   RETURN BOOLEAN IS

   L_program               VARCHAR2(50) := 'RMSSUB_XORDER_SQL.PERSIST';

BEGIN

   LP_process_id         := NULL;
   LP_chunk_id           := 1;
   LP_process_status     := 'N';
   LP_process_id         := I_process_id;
   LP_svc_po_tab         := NEW svc_po_tab_typ();

   if I_message_type = RMSSUB_XORDER.LP_cre_type then
      if not INSERT_SVC_ORDHEAD(O_error_message,
                                IO_svc_ordhead_rec,
                                CORESVC_PO.ACTION_NEW) then
         return FALSE;
      end if;
      if not INSERT_SVC_ORDDETAIL(O_error_message,
                                  IO_svc_orddetail_tbl,
                                  CORESVC_PO.ACTION_NEW) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XORDER.LP_dtl_cre_type then
      if not INSERT_SVC_ORDDETAIL(O_error_message,
                                  IO_svc_orddetail_tbl,
                                  CORESVC_PO.ACTION_NEW) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XORDER.LP_mod_type then
      if not INSERT_SVC_ORDHEAD(O_error_message,
                                IO_svc_ordhead_rec,
                                CORESVC_PO.ACTION_MOD) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XORDER.LP_dtl_mod_type then
      if not INSERT_SVC_ORDDETAIL(O_error_message,
                                  IO_svc_orddetail_tbl,
                                  CORESVC_PO.ACTION_MOD) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XORDER.LP_del_type then
      if not INSERT_SVC_ORDHEAD(O_error_message,
                                IO_svc_ordhead_rec,
                                CORESVC_PO.ACTION_MOD) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XORDER.LP_dtl_del_type then
      if not INSERT_SVC_ORDDETAIL(O_error_message,
                                  IO_svc_orddetail_tbl,
                                  CORESVC_PO.ACTION_DEL) then
         return FALSE;
      end if;
   end if;
   if LP_svc_po_tab is NOT NULL and LP_svc_po_tab.COUNT > 0 then
      FORALL i IN 1..LP_svc_po_tab.COUNT()
         insert into svc_po_del
              values LP_svc_po_tab(i);
      if PO_INDUCT_SQL.PROCESS_CASCADE_DELETE(O_error_message) = FALSE then
         return FALSE;
      end if;
      LP_svc_po_tab := NEW svc_po_tab_typ();
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST;
-------------------------------------------------------------------------------------------------------
FUNCTION BUILD_RECORDS(O_error_message      IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_svc_ordhead_rec   IN OUT NOCOPY  SVC_ORDHEAD%ROWTYPE,
                       IO_svc_orddetail_tbl IN OUT NOCOPY  ORDDTL_TAB,
                       I_message            IN             "RIB_XOrderDesc_REC",
                       I_message_type       IN             VARCHAR2)
   RETURN BOOLEAN IS

   L_program                   VARCHAR2(50) := 'RMSSUB_XORDER_SQL.BUILD_RECORDS';
   L_ordhead_row               ORDHEAD%ROWTYPE;

   cursor C_ROW is
      select *
        from ordhead
       where order_no = I_message.order_no;

BEGIN

   LP_vdate                   := GET_VDATE;

   if not SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) then
      return FALSE;
   end if;

   if VALIDATE_MESSAGE(O_error_message,
                       I_message,
                       I_message_type) = FALSE then
      return FALSE;
   end if;

   open C_ROW;
   fetch C_ROW into L_ordhead_row;
   close C_ROW;

   if not POPULATE_RECORD_HEAD(O_error_message,
                               IO_svc_ordhead_rec,
                               I_message,
                               I_message_type,
                               L_ordhead_row) then
      return FALSE;
   end if;

   if I_message.XOrderDtl_TBL is NOT NULL and I_message.XOrderDtl_TBL.COUNT > 0 then
      if not POPULATE_RECORD_DETAIL(O_error_message,
                                    IO_svc_orddetail_tbl,
                                    I_message,
                                    I_message_type,
                                    L_ordhead_row) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BUILD_RECORDS;
-------------------------------------------------------------------------------------------------------
FUNCTION BUILD_RECORDS(O_error_message      IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_svc_ordhead_rec   IN OUT NOCOPY  SVC_ORDHEAD%ROWTYPE,
                       IO_svc_orddetail_tbl IN OUT NOCOPY  ORDDTL_TAB,
                       I_message            IN             "RIB_XOrderRef_REC",
                       I_message_type       IN             VARCHAR2)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(50) := 'RMSSUB_XORDER_SQL.BUILD_RECORDS';
   L_loc_type              ORDLOC.LOC_TYPE%TYPE;
   L_order_status          ORDHEAD.STATUS%TYPE;

   cursor C_GET_ORDER_STATUS is
      select status
        from ordhead
       where order_no = I_message.order_no;

   cursor GET_LOC_TYPE(I_item       ORDLOC.ITEM%TYPE,
                       I_loc        ORDLOC.LOCATION%TYPE,
                       I_ref_item   ITEM_MASTER.ITEM%TYPE) is
      select loc_type
        from item_loc
       where ((item = I_item and I_item is NOT NULL)
              or (item = (select item_parent
                            from item_master
                            where item = I_ref_item
                              and tran_level < item_level)))
         and loc = I_loc;

BEGIN
   open C_GET_ORDER_STATUS;
   fetch C_GET_ORDER_STATUS into L_order_status;
   close C_GET_ORDER_STATUS;

   IO_svc_orddetail_tbl := ORDDTL_TAB();
   if I_message_type = RMSSUB_XORDER.LP_del_type then
      IO_svc_ordhead_rec.order_no       := I_message.order_no;
      IO_svc_ordhead_rec.status         := 'D';
   elsif I_message_type = RMSSUB_XORDER.LP_dtl_del_type then
      if I_message.XOrderDtlRef_TBL is NOT NULL and  I_message.XOrderDtlRef_TBL.COUNT  > 0 then
         FOR i in I_message.XOrderDtlRef_TBL.FIRST..I_message.XOrderDtlRef_TBL.LAST LOOP
            IO_svc_orddetail_tbl.EXTEND;
            IO_svc_orddetail_tbl(i).order_no       := I_message.order_no;
            IO_svc_orddetail_tbl(i).item           := I_message.XOrderDtlRef_TBL(i).item;
            IO_svc_orddetail_tbl(i).ref_item       := I_message.XOrderDtlRef_TBL(i).ref_item;
            IO_svc_orddetail_tbl(i).location       := I_message.XOrderDtlRef_TBL(i).location;
            open GET_LOC_TYPE(I_message.XOrderDtlRef_TBL(i).item,
                              I_message.XOrderDtlRef_TBL(i).location,
                              I_message.XOrderDtlRef_TBL(i).ref_item);
            fetch GET_LOC_TYPE into L_loc_type;
            close GET_LOC_TYPE;
            IO_svc_orddetail_tbl(i).loc_type       := L_loc_type;

         if I_message_type = RMSSUB_XORDER.LP_dtl_del_type then
            if L_order_status in ('S','A') then
               IO_svc_orddetail_tbl(i).re_approve := 'Y';
               IO_svc_orddetail_tbl(i).set_to_worksheet := 'Y';
            end if;
         end if;
         END LOOP;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BUILD_RECORDS;
----------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD_HEAD(O_error_message    IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_svc_ordhead_rec IN OUT NOCOPY  SVC_ORDHEAD%ROWTYPE,
                              I_message          IN             "RIB_XOrderDesc_REC",
                              I_message_type     IN             VARCHAR2,
                              I_old_ordhead_row  IN             ORDHEAD%ROWTYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'RMSSUB_XORDER_SQL.POPULATE_RECORD_HEAD';

   L_buyer                DEPS.BUYER%TYPE              := NULL;
   L_pickup_date          ORDHEAD.PICKUP_DATE%TYPE;
   L_scale_aip_ord_ind    SUPS.SCALE_AIP_ORDERS%TYPE;
   L_loc_country          ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_import_ind           VARCHAR2(1)                  := 'N';
   L_dept                 DEPS.DEPT%TYPE;
   L_not_before_date      ORDHEAD.NOT_BEFORE_DATE%TYPE := NULL;
   L_not_after_date       ORDHEAD.NOT_AFTER_DATE%TYPE  := NULL;
   L_location             ORDLOC.LOCATION%TYPE;

   cursor C_GET_DEPT IS
      select buyer
        from deps
       where dept = I_message.dept;

   cursor C_GET_LOC_COUNTRY(L_loc ITEM_LOC.LOC%TYPE) IS
      select country_id
        from addr
       where key_value_1 = TO_CHAR(L_loc)
         and module in ('ST', 'WFST', 'WH')
         and addr_type = DECODE(module,'WFST','07','01')
         and primary_addr_ind = 'Y';

   cursor C_GET_IMP_ID_TYPE is
      select import_id,
             import_type
        from sups_imp_exp
       where supplier = IO_svc_ordhead_rec.supplier;

   cursor C_GET_IMP_CNTRY_IND is
      select country_id
        from store st, addr ad
       where to_char(st.store) = ad.key_value_1
         and ad.addr_type = '01'
         and st.store = I_message.XOrderDtl_TBL(1).location
         and ad.module='ST'
      union
      select country_id
        from wh wh, addr ad
       where to_char(wh.physical_wh) = ad.key_value_1
         and ad.addr_type = '01'
         and wh.wh = I_message.XOrderDtl_TBL(1).location
         and ad.module='WH';

   cursor C_GET_STORE_TYPE(I_store  STORE.STORE%TYPE) is
      select store_type
        from store
       where store = I_store;
   cursor C_GET_PHYSICAL_WH is 
      select PHYSICAL_WH 
        from WH 
       where WH = I_message.XOrderDtl_TBL(1).location; 

BEGIN
   IO_svc_ordhead_rec.order_no                := I_message.order_no;
   if I_message.orig_ind = 6 then
      IO_svc_ordhead_rec.order_type           := 'ARB';
   else
      IO_svc_ordhead_rec.order_type           := 'N/B';
   end if;
   IO_svc_ordhead_rec.dept                    := I_message.dept;

   if I_message.dept is NOT NULL then
      open C_GET_DEPT;
      fetch C_GET_DEPT into L_buyer;
      close C_GET_DEPT;
   end if;

   IO_svc_ordhead_rec.buyer                   := L_buyer;
   IO_svc_ordhead_rec.supplier                := NVL(I_message.supplier, I_old_ordhead_row.supplier);
   IO_svc_ordhead_rec.supp_add_seq_no         := NULL;
   IO_svc_ordhead_rec.promotion               := NULL;
   IO_svc_ordhead_rec.written_date            := I_message.written_date;

   if I_message_type = RMSSUB_XORDER.LP_mod_type and (I_message.XOrderDtl_TBL is NULL or I_message.XOrderDtl_TBL.COUNT = 0) then
      if I_message.dept IS NULL THEN
         IO_svc_ordhead_rec.dept := I_old_ordhead_row.dept;
      end if;
   end if;

   if I_message_type = RMSSUB_XORDER.LP_cre_type then
      L_pickup_date                         := NULL;
      L_not_before_date                     := NULL;
      L_not_after_date                      := NULL;
      IO_svc_ordhead_rec.earliest_ship_date := NULL;
      IO_svc_ordhead_rec.latest_ship_date   := NULL;
      IO_svc_ordhead_rec.close_date         := NULL;
      IO_svc_ordhead_rec.po_type            := NULL;
      if I_message.pre_mark_ind is NOT NULL then
         IO_svc_ordhead_rec.pre_mark_ind := I_message.pre_mark_ind;
      else
         IO_svc_ordhead_rec.pre_mark_ind := 'N';
      end if;

      if SYSTEM_OPTIONS_SQL.GP_system_options_row.elc_ind = 'N' then
         IO_svc_ordhead_rec.lading_port := NULL;
         IO_svc_ordhead_rec.discharge_port := NULL;
      end if;
   else
      -- for a modify message, do not change dates if not defined in message
      L_not_before_date                     := I_message.not_before_date;
      L_not_after_date                      := I_message.not_after_date;
      L_pickup_date                         := I_old_ordhead_row.pickup_date;
      IO_svc_ordhead_rec.earliest_ship_date := I_old_ordhead_row.earliest_ship_date;
      IO_svc_ordhead_rec.latest_ship_date   := I_old_ordhead_row.latest_ship_date;
      IO_svc_ordhead_rec.close_date         := I_old_ordhead_row.close_date;
      IO_svc_ordhead_rec.po_type            := I_old_ordhead_row.po_type;
      if I_message.pre_mark_ind is NOT NULL then
          IO_svc_ordhead_rec.pre_mark_ind := I_message.pre_mark_ind;
      else
          IO_svc_ordhead_rec.pre_mark_ind := I_old_ordhead_row.pre_mark_ind;
      end if;
      IO_svc_ordhead_rec.agent                 := I_old_ordhead_row.agent;
      IO_svc_ordhead_rec.lading_port           := I_old_ordhead_row.lading_port;
      IO_svc_ordhead_rec.discharge_port        := I_old_ordhead_row.discharge_port;
      IO_svc_ordhead_rec.factory               := I_old_ordhead_row.factory;
      IO_svc_ordhead_rec.partner_type_1        := I_old_ordhead_row.partner_type_1;
      IO_svc_ordhead_rec.partner1              := I_old_ordhead_row.partner1;
      IO_svc_ordhead_rec.partner_type_2        := I_old_ordhead_row.partner_type_2;
      IO_svc_ordhead_rec.partner2              := I_old_ordhead_row.partner2;
      IO_svc_ordhead_rec.partner_type_3        := I_old_ordhead_row.partner_type_3;
      IO_svc_ordhead_rec.partner3              := I_old_ordhead_row.partner3;
      IO_svc_ordhead_rec.purchase_type         := I_old_ordhead_row.purchase_type;
      IO_svc_ordhead_rec.pickup_loc            := I_old_ordhead_row.pickup_loc;
   end if;
   IO_svc_ordhead_rec.terms                 := I_message.terms;
   IO_svc_ordhead_rec.freight_terms         := NULL;
   IO_svc_ordhead_rec.orig_ind              := NVL(I_message.orig_ind, RMSSUB_XORDER_DEFAULT.default_orig_ind);
   IO_svc_ordhead_rec.backhaul_type         := NULL;
   IO_svc_ordhead_rec.backhaul_allowance    := NULL;
   IO_svc_ordhead_rec.ship_method           := NULL;
   IO_svc_ordhead_rec.ship_pay_method       := NULL;
   IO_svc_ordhead_rec.fob_trans_res         := NULL;
   IO_svc_ordhead_rec.fob_trans_res_desc    := NULL;
   IO_svc_ordhead_rec.fob_title_pass        := SYSTEM_OPTIONS_SQL.GP_system_options_row.fob_title_pass;
   IO_svc_ordhead_rec.fob_title_pass_desc   := SYSTEM_OPTIONS_SQL.GP_system_options_row.fob_title_pass_desc;
   IO_svc_ordhead_rec.edi_po_ind            := NVL(I_message.edi_po_ind,RMSSUB_XORDER_DEFAULT.default_edi_po_ind);
   IO_svc_ordhead_rec.include_on_order_ind  := NVL(I_message.include_on_ord_ind,'Y');
   IO_svc_ordhead_rec.vendor_order_no       := NULL;
   IO_svc_ordhead_rec.currency_code         := UPPER(I_message.currency_code);
   IO_svc_ordhead_rec.freight_contract_no   := NULL;
   IO_svc_ordhead_rec.reject_code           := NULL;
   IO_svc_ordhead_rec.contract_no           := NULL;
   IO_svc_ordhead_rec.pickup_no             := NULL;
   IO_svc_ordhead_rec.qc_ind                := NULL;
   IO_svc_ordhead_rec.payment_method        := NULL;
   IO_svc_ordhead_rec.comment_desc          := I_message.comment_desc;
   IO_svc_ordhead_rec.ext_ref_no            := NULL;
   IO_svc_ordhead_rec.master_po_no          := NULL;

   if I_message.exchange_rate is NOT NULL then
      IO_svc_ordhead_rec.exchange_rate      := I_message.exchange_rate;
   else
      if I_message_type = RMSSUB_XORDER.LP_cre_type and IO_svc_ordhead_rec.currency_code is NOT NULL then
         if not CURRENCY_SQL.GET_RATE(O_error_message,
                                      IO_svc_ordhead_rec.exchange_rate,
                                      IO_svc_ordhead_rec.currency_code,
                                      'P', -- exchange_type
                                      LP_vdate) then  -- effective_date
            return FALSE;
         end if;
      else
         IO_svc_ordhead_rec.exchange_rate := I_old_ordhead_row.exchange_rate;
      end if;
   end if;

   -- Pick up date cannot be greater than NAD.
   -- When NAD is present in xorder message and if calculated pickup date is greater than NAD,
   -- then default the pickup date with NAD.
   if I_message.not_after_date is NOT NULL and L_pickup_date > I_message.not_after_date then
      IO_svc_ordhead_rec.pickup_date := I_message.not_after_date;
   elsif I_message_type = RMSSUB_XORDER.LP_mod_type then
      IO_svc_ordhead_rec.pickup_date := L_pickup_date;
   end if;

   if I_message.not_before_date is NOT NULL AND I_message.not_before_date >= LP_vdate then
      IO_svc_ordhead_rec.not_before_date := I_message.not_before_date;
   else
      IO_svc_ordhead_rec.not_before_date := L_not_before_date;
   end if;

   if I_message.not_after_date is NOT NULL then
      IO_svc_ordhead_rec.not_after_date := I_message.not_after_date;
   else
      IO_svc_ordhead_rec.not_after_date := L_not_after_date;
   end if;

   if I_message.otb_eow_date is NOT NULL then
      IO_svc_ordhead_rec.otb_eow_date   := I_message.otb_eow_date;
   else
      if IO_svc_ordhead_rec.not_after_date is NOT NULL then
         if NOT DATES_SQL.GET_EOW_DATE(O_error_message,
                                       IO_svc_ordhead_rec.otb_eow_date,
                                       IO_svc_ordhead_rec.not_after_date) then
            return FALSE;
         end if;
      end if;
   end if;

   if IO_svc_ordhead_rec.payment_method = 'LC' then
      IO_svc_ordhead_rec.status := LP_worksheet;
   elsif IO_svc_ordhead_rec.orig_ind = 6 and IO_svc_ordhead_rec.supplier is NOT NULL then
      if ORDER_SETUP_SQL.GET_SCALE_AIP_ORD_IND(O_error_message,
                                               L_scale_aip_ord_ind,
                                               IO_svc_ordhead_rec.supplier) = FALSE then
         return FALSE;
      end if;

      if L_scale_aip_ord_ind ='Y' then
         IO_svc_ordhead_rec.status  := LP_worksheet;
      else
         IO_svc_ordhead_rec.status := I_message.status;
      end if;
   else
      IO_svc_ordhead_rec.status := I_message.status;
   end if;

   if I_message.XOrderDtl_TBL is NOT NULL and I_message.XOrderDtl_TBL.COUNT > 0 then
      if I_message.XOrderDtl_TBL(1).location_type = LP_store then
         open C_GET_STORE_TYPE(I_message.XOrderDtl_TBL(1).location);
         fetch C_GET_STORE_TYPE into LP_store_type;
         close C_GET_STORE_TYPE;
      end if;

      if NVL(LP_store_type,'X') = 'F' or IO_svc_ordhead_rec.orig_ind = '7' then
         IO_svc_ordhead_rec.location   := I_message.XOrderDtl_TBL(1).location;
         IO_svc_ordhead_rec.loc_type   := I_message.XOrderDtl_TBL(1).location_type;
      end if;
      L_location := I_message.XOrderDtl_TBL(1).location;

      if I_message.XOrderDtl_TBL(1).location_type = LP_warehouse then
         open C_GET_PHYSICAL_WH;
         fetch C_GET_PHYSICAL_WH into L_location;
         close C_GET_PHYSICAL_WH;
      end if;
      open C_GET_LOC_COUNTRY(L_location);
      fetch C_GET_LOC_COUNTRY into L_loc_country;
      if C_GET_LOC_COUNTRY%NOTFOUND then
         L_loc_country := SYSTEM_OPTIONS_SQL.GP_system_options_row.base_country_id;
      end if;
      close C_GET_LOC_COUNTRY;

      if L_loc_country != I_message.XOrderDtl_TBL(1).origin_country_id then
         L_import_ind := 'Y';
      else
         L_import_ind := 'N';
      end if;
   end if;

   if L_import_ind = 'Y' then
      IO_svc_ordhead_rec.import_order_ind   := 'Y';
      IO_svc_ordhead_rec.import_country_id  := L_loc_country;

      OPEN C_GET_IMP_ID_TYPE;
      FETCH C_GET_IMP_ID_TYPE into IO_svc_ordhead_rec.import_id,
                                   IO_svc_ordhead_rec.import_type;
      CLOSE C_GET_IMP_ID_TYPE;

      IO_svc_ordhead_rec.routing_loc_id     := NULL;
      IO_svc_ordhead_rec.clearing_zone_id   := NULL;
   else
      IO_svc_ordhead_rec.import_order_ind := 'N';
      if I_message.XOrderDtl_TBL IS NOT NULL and I_message.XOrderDtl_TBL.COUNT > 0 then
         if I_message.XOrderDtl_TBL(1).location IS NOT NULL then
            open C_GET_IMP_CNTRY_IND;
            fetch C_GET_IMP_CNTRY_IND into IO_svc_ordhead_rec.import_country_id;
               if C_GET_IMP_CNTRY_IND%NOTFOUND then
                  IO_svc_ordhead_rec.import_country_id := NVL(L_loc_country,SYSTEM_OPTIONS_SQL.GP_system_options_row.base_country_id);
               end if;
            close C_GET_IMP_CNTRY_IND;
         elsif I_message.XOrderDtl_TBL(1).location IS NULL then
            IO_svc_ordhead_rec.import_country_id := NVL(L_loc_country,SYSTEM_OPTIONS_SQL.GP_system_options_row.base_country_id);
         end if;
      else
         IO_svc_ordhead_rec.import_country_id := NVL(L_loc_country,SYSTEM_OPTIONS_SQL.GP_system_options_row.base_country_id);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_RECORD_HEAD;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD_DETAIL(O_error_message            IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_svc_orddetail_tbl       IN OUT NOCOPY ORDDTL_TAB,
                                I_message                  IN            "RIB_XOrderDesc_REC",
                                I_message_type             IN             VARCHAR2,
                                I_old_ordhead_row          IN             ORDHEAD%ROWTYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(64) := 'RMSSUB_XORDER_SQL.POPULATE_RECORD_DETAIL';
   L_item_parent           ITEM_MASTER.ITEM%TYPE;
   L_ordsku_exists         BOOLEAN;
   L_old_ordsku_row        ORDSKU%ROWTYPE;
   L_ordloc_exists         BOOLEAN;
   L_old_ordloc_row        ORDLOC%ROWTYPE;

   cursor C_ITEM_PARENT(I_item   ITEM_MASTER.ITEM%TYPE) is
      select item_parent
        from item_master
       where item = I_item;
BEGIN

   IO_svc_orddetail_tbl := ORDDTL_TAB();

   for i in I_message.XOrderDtl_TBL.FIRST .. I_message.XOrderDtl_TBL.LAST LOOP

      IO_svc_orddetail_tbl.EXTEND;
      -- get the tran level item
      open C_ITEM_PARENT(I_message.XOrderDtl_TBL(i).ref_item);
      fetch C_ITEM_PARENT into L_item_parent;
      close C_ITEM_PARENT;

      if I_message.XOrderDtl_TBL(i).ref_item is NOT NULL and I_message.XOrderDtl_TBL(i).item is NULL then
         IO_svc_orddetail_tbl(i).item            := L_item_parent;
      else
         IO_svc_orddetail_tbl(i).item            := I_message.XOrderDtl_TBL(i).item;
      end if;

      if not ORDER_ATTRIB_SQL.GET_ORDSKU_ROW(O_error_message,
                                             L_ordsku_exists,
                                             L_old_ordsku_row,
                                             I_message.order_no,
                                             IO_svc_orddetail_tbl(i).item) then
         return FALSE;
      end if;

      if not ORDER_ATTRIB_SQL.GET_ORDLOC_ROW(O_error_message,
                                             L_ordloc_exists,
                                             L_old_ordloc_row,
                                             I_message.order_no,
                                             IO_svc_orddetail_tbl(i).item,
                                             I_message.XOrderDtl_TBL(i).location) then
         return FALSE;
      end if;

      IO_svc_orddetail_tbl(i).order_no           := I_message.order_no;
      IO_svc_orddetail_tbl(i).item_desc          := NULL;
      IO_svc_orddetail_tbl(i).vpn                := NULL;
      IO_svc_orddetail_tbl(i).item_parent        := NULL;
      IO_svc_orddetail_tbl(i).ref_item           := I_message.XOrderDtl_TBL(i).ref_item;
      IO_svc_orddetail_tbl(i).diff_1             := NULL;
      IO_svc_orddetail_tbl(i).diff_2             := NULL;
      IO_svc_orddetail_tbl(i).diff_3             := NULL;
      IO_svc_orddetail_tbl(i).diff_4             := NULL;
      IO_svc_orddetail_tbl(i).origin_country_id  := UPPER(I_message.XOrderDtl_TBL(i).origin_country_id);
      IO_svc_orddetail_tbl(i).loc_type           := I_message.XOrderDtl_TBL(i).location_type;
      IO_svc_orddetail_tbl(i).location           := I_message.XOrderDtl_TBL(i).location;
      IO_svc_orddetail_tbl(i).location_desc      := NULL;
      IO_svc_orddetail_tbl(i).delivery_date      := I_message.XOrderDtl_TBL(i).delivery_date;
      IO_svc_orddetail_tbl(i).non_scale_ind      := 'N';
      IO_svc_orddetail_tbl(i).processing_type    := NULL;
      IO_svc_orddetail_tbl(i).processing_wh      := NULL;
      IO_svc_orddetail_tbl(i).alloc_no           := NULL;
      IO_svc_orddetail_tbl(i).qty_transferred    := NULL;
      IO_svc_orddetail_tbl(i).qty_cancelled      := NULL;
      IO_svc_orddetail_tbl(i).cancel_code        := NULL;
      IO_svc_orddetail_tbl(i).supp_pack_size     := I_message.XOrderDtl_TBL(i).supp_pack_size;
      IO_svc_orddetail_tbl(i).unit_cost          := I_message.XOrderDtl_TBL(i).unit_cost;
      IO_svc_orddetail_tbl(i).qty_ordered        := I_message.XOrderDtl_TBL(i).qty_ordered;
      IO_svc_orddetail_tbl(i).qty_cancelled      := GREATEST(NVL(L_old_ordloc_row.qty_cancelled, 0) +
                                                            (NVL(L_old_ordloc_row.qty_ordered, 0) - NVL(I_message.XOrderDtl_TBL(i).qty_ordered,0)), 0);

      if I_message_type in (RMSSUB_XORDER.LP_dtl_mod_type) then
         -- allow modification of certain fields even if 'A' or 'S' status
         if I_old_ordhead_row.status in ('A','S') then
            if L_old_ordsku_row.supp_pack_size <> I_message.XOrderDtl_TBL(i).supp_pack_size or
               L_old_ordloc_row.unit_cost <> I_message.XOrderDtl_TBL(i).unit_cost or
               L_old_ordloc_row.qty_ordered <> NVL(I_message.XOrderDtl_TBL(i).qty_ordered, 0) then
               IO_svc_orddetail_tbl(i).set_to_worksheet := 'Y';
            end if;
         end if;

         if I_message.XOrderDtl_TBL(i).reinstate_ind = 'Y' and NVL(I_message.XOrderDtl_TBL(i).qty_ordered, 0) = 0 then
            IO_svc_orddetail_tbl(i).qty_ordered := NVL(L_old_ordloc_row.qty_ordered, 0) +
                                                   NVL(L_old_ordloc_row.qty_cancelled, 0);
            IO_svc_orddetail_tbl(i).qty_cancelled := NULL;
            IO_svc_orddetail_tbl(i).set_to_worksheet := 'Y';
         end if;

         if I_message.XOrderDtl_TBL(i).cancel_ind = 'Y' and NVL(I_message.XOrderDtl_TBL(i).qty_ordered, 0) = 0 then
            IO_svc_orddetail_tbl(i).qty_ordered := 0;
            IO_svc_orddetail_tbl(i).set_to_worksheet := 'Y';
         end if;
      elsif I_message_type in (RMSSUB_XORDER.LP_dtl_cre_type) then
         if I_old_ordhead_row.status in ('A','S') then
            IO_svc_orddetail_tbl(i).set_to_worksheet := 'Y';
         end if;
      end if;
      if IO_svc_orddetail_tbl(i).qty_cancelled = 0 then
         IO_svc_orddetail_tbl(i).qty_cancelled := NULL;
      end if;

      if I_message_type in (RMSSUB_XORDER.LP_dtl_cre_type, RMSSUB_XORDER.LP_dtl_mod_type) then
         if IO_svc_orddetail_tbl(i).set_to_worksheet = 'Y' then
            if I_old_ordhead_row.status in ('S','A') then
               IO_svc_orddetail_tbl(i).re_approve := 'Y';
            end if;
         end if;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_RECORD_DETAIL;
----------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ORDHEAD(O_error_message       OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_svc_ordhead_rec IN OUT  SVC_ORDHEAD%ROWTYPE,
                            I_action           IN      SVC_ORDHEAD.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'RMSSUB_XORDER_SQL.INSERT_SVC_ORDHEAD';

   TYPE             SVC_OHE_COL_TYP IS TABLE OF SVC_ORDHEAD%ROWTYPE;
   L_temp_rec       SVC_ORDHEAD%ROWTYPE;
   L_rms_rec        ORDHEAD%ROWTYPE;
   svc_ins_col      SVC_OHE_COL_TYP := NEW SVC_OHE_COL_TYP();
   svc_upd_col      SVC_OHE_COL_TYP := NEW SVC_OHE_COL_TYP();
   L_table          VARCHAR2(30) := 'SVC_ORDHEAD';
   L_stg_exists     BOOLEAN;
   L_rms_exists     BOOLEAN;
   L_pk_columns     VARCHAR2(255) := 'order_no or ext_ref_no';

BEGIN

   L_temp_rec.process_id              := LP_process_id;
   L_temp_rec.chunk_id                := LP_chunk_id;
   L_temp_rec.row_seq                 := 1;              -- ordhead will have row_seq 1 since there is only one order number per message
   L_temp_rec.process$status          := 'N';
   L_temp_rec.create_id               := LP_user;
   L_temp_rec.create_datetime         := SYSDATE;
   L_temp_rec.last_upd_id             := LP_user;
   L_temp_rec.last_upd_datetime       := SYSDATE;
   L_temp_rec.action                  := I_action;
   L_temp_rec.order_no                := IO_svc_ordhead_rec.order_no;
   L_temp_rec.order_type              := IO_svc_ordhead_rec.order_type;
   L_temp_rec.dept                    := IO_svc_ordhead_rec.dept;
   L_temp_rec.buyer                   := IO_svc_ordhead_rec.buyer;
   L_temp_rec.supplier                := IO_svc_ordhead_rec.supplier;
   L_temp_rec.supp_add_seq_no         := IO_svc_ordhead_rec.supp_add_seq_no;
   L_temp_rec.loc_type                := IO_svc_ordhead_rec.loc_type;
   L_temp_rec.location                := IO_svc_ordhead_rec.location;
   L_temp_rec.promotion               := IO_svc_ordhead_rec.promotion;
   L_temp_rec.qc_ind                  := NVL(IO_svc_ordhead_rec.qc_ind,'N');
   L_temp_rec.written_date            := IO_svc_ordhead_rec.written_date;
   L_temp_rec.not_before_date         := IO_svc_ordhead_rec.not_before_date;
   L_temp_rec.not_after_date          := IO_svc_ordhead_rec.not_after_date;
   L_temp_rec.otb_eow_date            := IO_svc_ordhead_rec.otb_eow_date;
   L_temp_rec.earliest_ship_date      := IO_svc_ordhead_rec.earliest_ship_date;
   L_temp_rec.latest_ship_date        := IO_svc_ordhead_rec.latest_ship_date;
   L_temp_rec.close_date              := IO_svc_ordhead_rec.close_date;
   L_temp_rec.terms                   := IO_svc_ordhead_rec.terms;
   L_temp_rec.freight_terms           := IO_svc_ordhead_rec.freight_terms;
   L_temp_rec.orig_ind                := IO_svc_ordhead_rec.orig_ind;
   L_temp_rec.payment_method          := IO_svc_ordhead_rec.payment_method;
   L_temp_rec.backhaul_TYPE           := IO_svc_ordhead_rec.backhaul_TYPE;
   L_temp_rec.backhaul_allowance      := IO_svc_ordhead_rec.backhaul_allowance;
   L_temp_rec.ship_method             := IO_svc_ordhead_rec.ship_method;
   L_temp_rec.purchase_type           := IO_svc_ordhead_rec.purchase_type;
   L_temp_rec.status                  := IO_svc_ordhead_rec.status;
   L_temp_rec.ship_pay_method         := IO_svc_ordhead_rec.ship_pay_method;
   L_temp_rec.fob_trans_res           := IO_svc_ordhead_rec.fob_trans_res;
   L_temp_rec.fob_trans_res_desc      := IO_svc_ordhead_rec.fob_trans_res_desc;
   L_temp_rec.fob_title_pass          := IO_svc_ordhead_rec.fob_title_pass;
   L_temp_rec.fob_title_pass_desc     := IO_svc_ordhead_rec.fob_title_pass_desc;
   L_temp_rec.edi_po_ind              := IO_svc_ordhead_rec.edi_po_ind;
   L_temp_rec.import_order_ind        := NVL(IO_svc_ordhead_rec.import_order_ind,'N');
   L_temp_rec.import_country_id       := UPPER(IO_svc_ordhead_rec.import_country_id);
   L_temp_rec.include_on_order_ind    := IO_svc_ordhead_rec.include_on_order_ind;
   L_temp_rec.vendor_order_no         := IO_svc_ordhead_rec.vendor_order_no;
   L_temp_rec.exchange_rate           := IO_svc_ordhead_rec.exchange_rate;
   L_temp_rec.factory                 := IO_svc_ordhead_rec.factory;
   L_temp_rec.agent                   := IO_svc_ordhead_rec.agent;
   L_temp_rec.discharge_port          := IO_svc_ordhead_rec.discharge_port;
   L_temp_rec.lading_port             := IO_svc_ordhead_rec.lading_port;
   L_temp_rec.freight_contract_no     := IO_svc_ordhead_rec.freight_contract_no;
   L_temp_rec.po_type                 := IO_svc_ordhead_rec.po_type;
   L_temp_rec.pre_mark_ind            := IO_svc_ordhead_rec.pre_mark_ind;
   L_temp_rec.currency_code           := UPPER(IO_svc_ordhead_rec.currency_code);
   L_temp_rec.reject_code             := IO_svc_ordhead_rec.reject_code;
   L_temp_rec.contract_no             := IO_svc_ordhead_rec.contract_no;
   L_temp_rec.pickup_loc              := IO_svc_ordhead_rec.pickup_loc;
   L_temp_rec.pickup_no               := IO_svc_ordhead_rec.pickup_no;
   L_temp_rec.pickup_date             := IO_svc_ordhead_rec.pickup_date;
   L_temp_rec.comment_desc            := IO_svc_ordhead_rec.comment_desc;
   L_temp_rec.partner_type_1          := IO_svc_ordhead_rec.partner_type_1;
   L_temp_rec.partner1                := IO_svc_ordhead_rec.partner1;
   L_temp_rec.partner_type_2          := IO_svc_ordhead_rec.partner_type_2;
   L_temp_rec.partner2                := IO_svc_ordhead_rec.partner2;
   L_temp_rec.partner_type_3          := IO_svc_ordhead_rec.partner_type_3;
   L_temp_rec.partner3                := IO_svc_ordhead_rec.partner3;
   L_temp_rec.import_type             := IO_svc_ordhead_rec.import_type;
   L_temp_rec.import_id               := IO_svc_ordhead_rec.import_id;
   L_temp_rec.clearing_zone_id        := IO_svc_ordhead_rec.clearing_zone_id;
   L_temp_rec.routing_loc_id          := IO_svc_ordhead_rec.routing_loc_id;
   L_temp_rec.ext_ref_no              := IO_svc_ordhead_rec.ext_ref_no;
   L_temp_rec.master_po_no            := IO_svc_ordhead_rec.master_po_no;
   L_temp_rec.re_approve_ind          := IO_svc_ordhead_rec.re_approve_ind;
   L_temp_rec.next_upd_id             := IO_svc_ordhead_rec.next_upd_id;

   DECLARE
      L_stg_rec       SVC_ORDHEAD%ROWTYPE;
      RECORD_LOCKED   EXCEPTION;
      PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

      cursor C_STG_REC is
         select *
           from svc_ordhead
          where order_no = L_temp_rec.order_no;

      cursor C_RMS_REC is
         select *
           from ordhead
          where order_no = L_temp_rec.order_no;

      cursor C_LOCK_SVC_ORDHEAD is
         select 'x'
           from svc_ordhead
          where process_id = L_stg_rec.process_id
            and row_seq = L_stg_rec.row_seq
            for update nowait;

   BEGIN
      open C_STG_REC;
      fetch C_STG_REC into L_stg_rec;
      if C_STG_REC%FOUND then
         L_stg_exists := TRUE;
      else
         L_stg_exists := FALSE;
      end if;
      close C_STG_REC;

      open C_RMS_REC;
      fetch C_RMS_REC into L_rms_rec;
      if C_RMS_REC%FOUND then
         L_rms_exists := TRUE;
      else
         L_rms_exists := FALSE;
      end if;
      close C_RMS_REC;

      if L_stg_exists then
         --delete record from staging if it is already uploaded to RMS.
         if L_stg_rec.process$status = 'P' and
            GET_PRC_DEST(L_stg_rec.process_id) = 'RMS' then
               open C_LOCK_SVC_ORDHEAD;
               close C_LOCK_SVC_ORDHEAD;
               delete from svc_ordhead
                     where process_id = L_stg_rec.process_id
                       and row_seq = L_stg_rec.row_seq;
               L_stg_exists := FALSE;
         end if;

         --delete record from staging if sending a DEL record for an order not in RMS
         if NOT L_rms_exists and
            L_stg_rec.action = PO_INDUCT_SQL.ACTION_NEW and
            L_temp_rec.action = PO_INDUCT_SQL.ACTION_MOD and
            L_temp_rec.status = 'D' then
            open C_LOCK_SVC_ORDHEAD;
            close C_LOCK_SVC_ORDHEAD;
            delete from svc_ordhead
                  where process_id = L_stg_rec.process_id
                    and row_seq = L_stg_rec.row_seq;
            ---
            LP_svc_po_tab.EXTEND();
            LP_svc_po_tab(LP_svc_po_tab.COUNT()).order_no     := L_temp_rec.order_no;
            LP_svc_po_tab(LP_svc_po_tab.COUNT()).svc_tbl_name := L_table;
            return TRUE;
         end if;

         if L_stg_rec.process$status = 'N' then
            --retain a DEL record in staging if the record has not yet been processed
            if L_stg_rec.action = PO_INDUCT_SQL.ACTION_MOD and
               L_stg_rec.status = 'D' and
               L_temp_rec.action in (PO_INDUCT_SQL.ACTION_MOD, PO_INDUCT_SQL.ACTION_DEL) then
               return TRUE;
            end if;
         end if;
      end if; -- if L_stg_exists

      --Resolve new value for action column.
      if L_stg_exists then
         --if it is a create record and record already exists in staging, log an error
         if L_temp_rec.action = PO_INDUCT_SQL.ACTION_NEW then
            if L_stg_rec.process$status = 'P' then
               O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS',L_pk_columns);
            else
               O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',L_pk_columns);
            end if;
            return FALSE;
         else
            L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,L_temp_rec.action);
         end if;
      --if record does not exist in staging then keep the user provided value
      end if;

   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                               L_table,
                                               NULL,
                                               NULL);
         return FALSE;
   END;

   -- Check PK cols. Log error if any PK col is null.
   if L_temp_rec.order_no is NULL and
      L_temp_rec.ext_ref_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',
                                            L_pk_columns);
      return FALSE;
   end if;
   --if no error so far then add temp record to insert or update collection depending upon
   --whether record already exists in staging or not.
   if L_stg_exists then
      svc_upd_col.EXTEND();
      svc_upd_col(svc_upd_col.COUNT()) := L_temp_rec;
   else
      svc_ins_col.EXTEND();
      svc_ins_col(svc_ins_col.COUNT()) := L_temp_rec;
   end if;

   --flush insert collection
   BEGIN
      FORALL i IN 1..svc_ins_col.COUNT
         insert into svc_ordhead
              values svc_ins_col(i);
   EXCEPTION
      when DUP_VAL_ON_INDEX then
         O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                               L_pk_columns,
                                               L_table,
                                               NULL);
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
   END;
   --flush update collection
   FORALL i IN 1..svc_upd_col.COUNT
      update svc_ordhead
         set process_id                = svc_upd_col(i).process_id,
             chunk_id                  = svc_upd_col(i).chunk_id,
             action                    = svc_upd_col(i).action,
             process$status            = svc_upd_col(i).process$status,
             order_no                  = svc_upd_col(i).order_no,
             order_type                = svc_upd_col(i).order_type,
             dept                      = svc_upd_col(i).dept,
             buyer                     = svc_upd_col(i).buyer,
             supplier                  = svc_upd_col(i).supplier,
             supp_add_seq_no           = svc_upd_col(i).supp_add_seq_no,
             loc_type                  = svc_upd_col(i).loc_type,
             location                  = svc_upd_col(i).location,
             promotion                 = svc_upd_col(i).promotion,
             qc_ind                    = svc_upd_col(i).qc_ind,
             written_date              = svc_upd_col(i).written_date,
             not_before_date           = svc_upd_col(i).not_before_date,
             not_after_date            = svc_upd_col(i).not_after_date,
             otb_eow_date              = svc_upd_col(i).otb_eow_date,
             earliest_ship_date        = svc_upd_col(i).earliest_ship_date,
             latest_ship_date          = svc_upd_col(i).latest_ship_date,
             close_date                = svc_upd_col(i).close_date,
             terms                     = svc_upd_col(i).terms,
             freight_terms             = svc_upd_col(i).freight_terms,
             orig_ind                  = svc_upd_col(i).orig_ind,
             payment_method            = svc_upd_col(i).payment_method,
             backhaul_type             = svc_upd_col(i).backhaul_type,
             backhaul_allowance        = svc_upd_col(i).backhaul_allowance,
             ship_method               = svc_upd_col(i).ship_method,
             purchase_type             = svc_upd_col(i).purchase_type,
             status                    = svc_upd_col(i).status,
             ship_pay_method           = svc_upd_col(i).ship_pay_method,
             fob_trans_res             = svc_upd_col(i).fob_trans_res,
             fob_trans_res_desc        = svc_upd_col(i).fob_trans_res_desc,
             fob_title_pass            = svc_upd_col(i).fob_title_pass,
             fob_title_pass_desc       = svc_upd_col(i).fob_title_pass_desc,
             edi_po_ind                = svc_upd_col(i).edi_po_ind,
             import_order_ind          = svc_upd_col(i).import_order_ind,
             import_country_id         = svc_upd_col(i).import_country_id,
             include_on_order_ind      = svc_upd_col(i).include_on_order_ind,
             vendor_order_no           = svc_upd_col(i).vendor_order_no,
             exchange_rate             = svc_upd_col(i).exchange_rate,
             factory                   = svc_upd_col(i).factory,
             agent                     = svc_upd_col(i).agent,
             discharge_port            = svc_upd_col(i).discharge_port,
             lading_port               = svc_upd_col(i).lading_port,             
             freight_contract_no       = svc_upd_col(i).freight_contract_no,
             po_type                   = svc_upd_col(i).po_type,
             pre_mark_ind              = svc_upd_col(i).pre_mark_ind,
             currency_code             = svc_upd_col(i).currency_code,
             reject_code               = svc_upd_col(i).reject_code,
             contract_no               = svc_upd_col(i).contract_no,
             pickup_loc                = svc_upd_col(i).pickup_loc,
             pickup_no                 = svc_upd_col(i).pickup_no,
             pickup_date               = svc_upd_col(i).pickup_date,
             comment_desc              = svc_upd_col(i).comment_desc,
             partner_type_1            = svc_upd_col(i).partner_type_1,
             partner1                  = svc_upd_col(i).partner1,
             partner_type_2            = svc_upd_col(i).partner_type_2,
             partner2                  = svc_upd_col(i).partner2,
             partner_type_3            = svc_upd_col(i).partner_type_3,
             partner3                  = svc_upd_col(i).partner3,
             import_type               = svc_upd_col(i).import_type,
             import_id                 = svc_upd_col(i).import_id,
             clearing_zone_id          = svc_upd_col(i).clearing_zone_id,
             routing_loc_id            = svc_upd_col(i).routing_loc_id,
             ext_ref_no                = svc_upd_col(i).ext_ref_no,
             master_po_no              = svc_upd_col(i).master_po_no,
             re_approve_ind            = svc_upd_col(i).re_approve_ind,
             last_upd_id               = svc_upd_col(i).last_upd_id,
             last_upd_datetime         = svc_upd_col(i).last_upd_datetime,
             next_upd_id               = svc_upd_col(i).next_upd_id
       where order_no = svc_upd_col(i).order_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_SVC_ORDHEAD;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_PRC_DEST(I_process_id    IN      SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN VARCHAR2 IS

   L_program   VARCHAR2(75) := 'RMSSUB_XORDER_SQL.GET_PRC_DEST';

   O_dest   SVC_PROCESS_TRACKER.PROCESS_DESTINATION%TYPE;

   cursor C_GET_DEST is
     select process_destination
       from svc_process_tracker
      where process_id = I_process_id;

BEGIN
   open C_GET_DEST;
   fetch C_GET_DEST into O_dest;
   close C_GET_DEST;
   return O_dest;

END GET_PRC_DEST;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_NEW_ACTION(I_old_action  IN  VARCHAR2,
                        I_new_action  IN  VARCHAR2)
RETURN VARCHAR2 IS
   O_new_action   SVC_ITEM_MASTER.ACTION%TYPE;

BEGIN
   case
      when I_old_action = PO_INDUCT_SQL.ACTION_NEW and
           I_new_action = PO_INDUCT_SQL.ACTION_DEL then
         O_new_action := PO_INDUCT_SQL.ACTION_DEL;
      when I_old_action = PO_INDUCT_SQL.ACTION_NEW and
           I_new_action = PO_INDUCT_SQL.ACTION_MOD then
         O_new_action := PO_INDUCT_SQL.ACTION_NEW;
      when I_old_action = PO_INDUCT_SQL.ACTION_DEL and
           I_new_action = PO_INDUCT_SQL.ACTION_DEL then
         O_new_action := PO_INDUCT_SQL.ACTION_DEL;
      when I_old_action = PO_INDUCT_SQL.ACTION_DEL and
           I_new_action = PO_INDUCT_SQL.ACTION_MOD then
         O_new_action := PO_INDUCT_SQL.ACTION_DEL;
      when I_old_action = PO_INDUCT_SQL.ACTION_MOD and
           I_new_action = PO_INDUCT_SQL.ACTION_DEL then
         O_new_action := PO_INDUCT_SQL.ACTION_DEL;
      when I_old_action = PO_INDUCT_SQL.ACTION_MOD and
           I_new_action = PO_INDUCT_SQL.ACTION_MOD then
         O_new_action := PO_INDUCT_SQL.ACTION_MOD;
      when I_old_action = PO_INDUCT_SQL.ACTION_NEW and
           I_new_action is NULL then
         O_new_action := PO_INDUCT_SQL.ACTION_NEW;
      when I_old_action = PO_INDUCT_SQL.ACTION_MOD and
           I_new_action is NULL then
         O_new_action := PO_INDUCT_SQL.ACTION_MOD;
      when I_old_action = PO_INDUCT_SQL.ACTION_DEL and
           I_new_action is NULL then
         O_new_action := PO_INDUCT_SQL.ACTION_DEL;
      else
         O_new_action := I_new_action;
   end case;
   return O_new_action;

END GET_NEW_ACTION;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ORDDETAIL(O_error_message         OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_svc_orddetail_tbl IN OUT  ORDDTL_TAB,
                              I_action             IN      SVC_ORDDETAIL.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XORDER_SQL.INSERT_SVC_ORDDETAIL';

   TYPE SVC_ODT_COL_TYP IS TABLE OF SVC_ORDDETAIL%ROWTYPE;
   L_temp_rec            SVC_ORDDETAIL%ROWTYPE;
   svc_upd_col           SVC_ODT_COL_TYP := NEW SVC_ODT_COL_TYP();
   svc_ins_col           SVC_ODT_COL_TYP := NEW SVC_ODT_COL_TYP();
   L_table               VARCHAR2(30) := 'SVC_ORDDETAIL';
   L_stg_exists          BOOLEAN;
   L_rms_exists          BOOLEAN;
   L_pk_columns          VARCHAR2(255) := 'order_no, item, loc_type, location, delivery_date';
   L_min_cols            VARCHAR2(255) := 'order_no, item, loc_type, location';
   L_min_ud_cols         VARCHAR2(255) := 'order_no and item';
   L_worksheet_all       VARCHAR2(1)   := 'N';
   L_re_approve_all      VARCHAR2(1)   := 'N';

BEGIN

   if IO_svc_orddetail_tbl is NOT NULL and IO_svc_orddetail_tbl.COUNT > 0 then
      FOR i in IO_svc_orddetail_tbl.FIRST..IO_svc_orddetail_tbl.LAST LOOP
         L_temp_rec.process_id        := LP_process_id;
         L_temp_rec.chunk_id          := LP_chunk_id;
         L_temp_rec.row_seq           := NVL(L_temp_rec.row_seq, 0) + 1;
         L_temp_rec.process$status    := 'N';
         L_temp_rec.create_id         := LP_user;
         L_temp_rec.last_upd_id       := LP_user;
         L_temp_rec.create_datetime   := SYSDATE;
         L_temp_rec.last_upd_datetime := SYSDATE;
         L_temp_rec.action            := I_action;
         L_temp_rec.order_no          := IO_svc_orddetail_tbl(i).order_no;
         L_temp_rec.item              := IO_svc_orddetail_tbl(i).item;
         L_temp_rec.item_desc         := IO_svc_orddetail_tbl(i).item_desc;
         L_temp_rec.vpn               := IO_svc_orddetail_tbl(i).vpn;
         L_temp_rec.item_parent       := IO_svc_orddetail_tbl(i).item_parent;
         L_temp_rec.ref_item          := IO_svc_orddetail_tbl(i).ref_item;
         L_temp_rec.diff_1            := IO_svc_orddetail_tbl(i).diff_1;
         L_temp_rec.diff_2            := IO_svc_orddetail_tbl(i).diff_2;
         L_temp_rec.diff_3            := IO_svc_orddetail_tbl(i).diff_3;
         L_temp_rec.diff_4            := IO_svc_orddetail_tbl(i).diff_4;
         L_temp_rec.origin_country_id := UPPER(IO_svc_orddetail_tbl(i).origin_country_id);
         L_temp_rec.supp_pack_size    := IO_svc_orddetail_tbl(i).supp_pack_size;
         L_temp_rec.loc_type          := IO_svc_orddetail_tbl(i).loc_type;
         L_temp_rec.location          := IO_svc_orddetail_tbl(i).location;
         L_temp_rec.location_desc     := IO_svc_orddetail_tbl(i).location_desc;
         L_temp_rec.unit_cost         := IO_svc_orddetail_tbl(i).unit_cost;
         L_temp_rec.qty_ordered       := IO_svc_orddetail_tbl(i).qty_ordered;
         L_temp_rec.qty_transferred   := IO_svc_orddetail_tbl(i).qty_transferred;
         L_temp_rec.delivery_date     := IO_svc_orddetail_tbl(i).delivery_date;
         L_temp_rec.non_scale_ind     := NVL(IO_svc_orddetail_tbl(i).non_scale_ind,'N');
         L_temp_rec.processing_type   := IO_svc_orddetail_tbl(i).processing_type;
         L_temp_rec.processing_wh     := IO_svc_orddetail_tbl(i).processing_wh;
         L_temp_rec.alloc_no          := IO_svc_orddetail_tbl(i).alloc_no;
         L_temp_rec.qty_cancelled     := IO_svc_orddetail_tbl(i).qty_cancelled;
         L_temp_rec.cancel_code       := IO_svc_orddetail_tbl(i).cancel_code;
         L_temp_rec.release_date      := IO_svc_orddetail_tbl(i).release_date;
         L_temp_rec.re_approve        := IO_svc_orddetail_tbl(i).re_approve;
         L_temp_rec.set_to_worksheet  := IO_svc_orddetail_tbl(i).set_to_worksheet;
         if L_temp_rec.re_approve = 'Y' then
            L_re_approve_all := 'Y';
         end if;
         if L_temp_rec.set_to_worksheet = 'Y' then
            L_worksheet_all := 'Y';
         end if;

         DECLARE
            L_stg_rec       SVC_ORDDETAIL%ROWTYPE;
            RECORD_LOCKED   EXCEPTION;
            PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

            cursor C_TRAN_ITEM is
               select item_parent
                 from item_master
                where item = L_temp_rec.ref_item;

            cursor C_STG_REC is
               select *
                 from svc_orddetail
                where order_no                 = L_temp_rec.order_no
                  and item                     = L_temp_rec.item
                  and NVL(location, -999)      = NVL(L_temp_rec.location, -999)
                  and NVL(loc_type, '-999')     = NVL(L_temp_rec.loc_type, '-999')
                  and NVL(processing_type, '-999') = NVL(L_temp_rec.processing_type, '-999')
                  and NVL(processing_wh, -999) = NVL(L_temp_rec.processing_wh, -999)
                  and (delivery_date           = L_temp_rec.delivery_date or
                       (delivery_date is NULL and L_temp_rec.delivery_date is NULL))
                  and NVL(alloc_no, -999)      = NVL(L_temp_rec.alloc_no, -999);

            cursor C_RMS_REC is
               select os.order_no          as order_no,
                      os.item              as item,
                      os.origin_country_id as origin_country_id,
                      ol.loc_type          as loc_type,
                      ol.location          as location
                 from ordloc ol,
                      ordsku os,
                      ordhead oh
                where oh.order_no = os.order_no
                  and os.order_no = ol.order_no(+)
                  and NVL(os.order_no, -999) = NVL(L_temp_rec.order_no, -999)
                  and NVL(os.item, '-999') = NVL(L_temp_rec.item, '-999')
                  and NVL(ol.location, -999) = NVL(L_temp_rec.location, -999)
                  and NVL(ol.loc_type, '-999') = NVL(L_temp_rec.loc_type, '-999')
                  and L_temp_rec.processing_type is NULL;

            cursor C_LOCK_SVC_ORDDETAIL is
               select 'x'
                 from svc_orddetail
                where process_id = L_stg_rec.process_id
                  and row_seq = L_stg_rec.row_seq
                  for update nowait;

            L_rms_rec   C_RMS_REC%ROWTYPE;

         BEGIN
            --get tran-level item if only reference item is populated.
            if L_temp_rec.item is NULL and L_temp_rec.ref_item is NOT NULL then
               open C_TRAN_ITEM;
               fetch C_TRAN_ITEM into L_temp_rec.item;
               close C_TRAN_ITEM;
            end if;

            open C_STG_REC;
            fetch C_STG_REC into L_stg_rec;
            if C_STG_REC%FOUND then
               L_stg_exists := TRUE;
            else
               L_stg_exists := FALSE;
            end if;
            close C_STG_REC;
            ---
            open C_RMS_REC;
            fetch C_RMS_REC into L_rms_rec;
            if C_RMS_REC%FOUND then
               L_rms_exists := TRUE;
            else
               L_rms_exists := FALSE;
            end if;
            close C_RMS_REC;

            if L_stg_exists then
               --delete record from staging if it is already uploaded to RMS.
               if L_stg_rec.process$status = 'P' and
                  GET_PRC_DEST(L_stg_rec.process_id) = 'RMS' then
                     open C_LOCK_SVC_ORDDETAIL;
                     close C_LOCK_SVC_ORDDETAIL;
                     delete from svc_orddetail
                           where process_id = L_stg_rec.process_id
                             and row_seq = L_stg_rec.row_seq;
                     L_stg_exists := FALSE;
               end if;

               if L_stg_rec.process$status in ('N', 'E') and NOT L_rms_exists and
                  L_stg_rec.action = PO_INDUCT_SQL.ACTION_NEW and
                  L_temp_rec.action = PO_INDUCT_SQL.ACTION_DEL then
                     open C_LOCK_SVC_ORDDETAIL;
                     close C_LOCK_SVC_ORDDETAIL;
                     delete from svc_orddetail
                           where process_id = L_stg_rec.process_id
                             and row_seq = L_stg_rec.row_seq;
                     ---
                     LP_svc_po_tab.EXTEND();
                     LP_svc_po_tab(LP_svc_po_tab.COUNT()).order_no     := L_temp_rec.order_no;
                     LP_svc_po_tab(LP_svc_po_tab.COUNT()).item         := L_temp_rec.item;
                     LP_svc_po_tab(LP_svc_po_tab.COUNT()).location     := L_temp_rec.location;
                     LP_svc_po_tab(LP_svc_po_tab.COUNT()).svc_tbl_name := L_table;
                     continue;
               end if;
            end if;

            --Resolve new value for action column.
            if L_stg_exists then
               --if it is a create record and record already exists in staging, log an error
               if L_temp_rec.action = PO_INDUCT_SQL.ACTION_NEW then
                  if L_stg_rec.process$status = 'P' then
                     O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_UPD_RMS',L_pk_columns);
                  else
                     O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',L_pk_columns);
                  end if;
                  return FALSE;
               else
                  --if record does not exist in staging then keep the user provided value
                  L_temp_rec.action := GET_NEW_ACTION(L_stg_rec.action,L_temp_rec.action);
               end if;
            end if;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                     L_table,
                                                     NULL,
                                                     NULL);
               return FALSE;
         END;

         if I_action = CORESVC_PO.ACTION_NEW then
            -- Check PK cols. Log error if any of these is null.
            if NOT (L_temp_rec.order_no is NOT NULL and
                    L_temp_rec.item is NOT NULL and
                    L_temp_rec.location is NOT NULL and
                    L_temp_rec.loc_type is NOT NULL) then
               O_error_message := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_min_cols);
               return FALSE;
            end if;
         else
            -- Check PK cols. Log error if any of these is null.
            if NOT (L_temp_rec.order_no is NOT NULL and
                    L_temp_rec.item is NOT NULL) then
               O_error_message := SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_min_ud_cols);
               return FALSE;
            end if;
         end if;
         --if no error so far then add temp record to insert or update collection depending upon
         --whether record already exists in staging or not.
         if L_stg_exists then
            svc_upd_col.extend();
            svc_upd_col(svc_upd_col.count()) := L_temp_rec;
         else
            svc_ins_col.extend();
            svc_ins_col(svc_ins_col.count()) := L_temp_rec;
         end if;
      END LOOP;
   end if;

   --flush insert collection
   BEGIN
      FORALL i IN 1..svc_ins_col.COUNT
         insert into svc_orddetail
              values svc_ins_col(i);
   EXCEPTION
      when DUP_VAL_ON_INDEX then
         O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                               L_pk_columns,
                                               L_table,
                                               NULL);
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;

   END;
   --flush update collection
   FORALL i IN 1..svc_upd_col.COUNT
      update svc_orddetail
         set process_id          = svc_upd_col(i).process_id,
             chunk_id            = svc_upd_col(i).chunk_id,
             row_seq             = svc_upd_col(i).row_seq,
             action              = svc_upd_col(i).action,
             process$status      = svc_upd_col(i).process$status,
             order_no            = svc_upd_col(i).order_no,
             item                = svc_upd_col(i).item,
             item_desc           = svc_upd_col(i).item_desc,
             vpn                 = svc_upd_col(i).vpn,
             item_parent         = svc_upd_col(i).item_parent,
             ref_item            = svc_upd_col(i).ref_item,
             diff_1              = svc_upd_col(i).diff_1,
             diff_2              = svc_upd_col(i).diff_2,
             diff_3              = svc_upd_col(i).diff_3,
             diff_4              = svc_upd_col(i).diff_4,
             origin_country_id   = svc_upd_col(i).origin_country_id,
             supp_pack_size      = svc_upd_col(i).supp_pack_size,
             loc_type            = svc_upd_col(i).loc_type,
             location            = svc_upd_col(i).location,
             location_desc       = svc_upd_col(i).location_desc,
             unit_cost           = svc_upd_col(i).unit_cost,
             qty_ordered         = svc_upd_col(i).qty_ordered,
             qty_transferred     = svc_upd_col(i).qty_transferred,
             delivery_date       = svc_upd_col(i).delivery_date,
             non_scale_ind       = svc_upd_col(i).non_scale_ind,
             processing_type     = svc_upd_col(i).processing_type,
             processing_wh       = svc_upd_col(i).processing_wh,
             alloc_no            = svc_upd_col(i).alloc_no,
             qty_cancelled       = svc_upd_col(i).qty_cancelled,
             cancel_code         = svc_upd_col(i).cancel_code,
             release_date        = svc_upd_col(i).release_date,
             last_upd_id         = svc_upd_col(i).last_upd_id,
             last_upd_datetime   = svc_upd_col(i).last_upd_datetime,
             re_approve          = svc_upd_col(i).re_approve,
             set_to_worksheet    = svc_upd_col(i).set_to_worksheet
       where order_no = svc_upd_col(i).order_no
         and item = svc_upd_col(i).item
         and location = svc_upd_col(i).location
         and (delivery_date = svc_upd_col(i).delivery_date or delivery_date is NULL and svc_upd_col(i).delivery_date is NULL)
         and processing_wh is NULL
         and processing_type is NULL
         and alloc_no is NULL;

   if L_worksheet_all = 'Y' then
      update svc_orddetail
         set re_approve = L_re_approve_all,
             set_to_worksheet = L_worksheet_all
       where process_id = LP_process_id
         and chunk_id = LP_chunk_id;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_SVC_ORDDETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_MESSAGE(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message            IN      "RIB_XOrderDesc_REC",
                          I_message_type       IN      VARCHAR2)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'RMSSUB_XORDER_SQL.VALIDATE_MESSAGE';
   L_item_parent   ITEM_MASTER.ITEM%TYPE;

   cursor C_ITEM_PARENT(I_item   ITEM_MASTER.ITEM%TYPE) is
      select item_parent
        from item_master
       where item = I_item;
BEGIN

   if I_message_type = RMSSUB_XORDER.LP_cre_type and
      I_message.status not in (LP_approved, LP_worksheet) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_STATUS_CREATE_ORDER', I_message.status, NULL, NULL);
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XORDER.LP_mod_type and
      I_message.status not in (LP_approved, LP_worksheet, LP_closed) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_STATUS_MOD_ORDER', I_message.status, NULL, NULL);
      return FALSE;
   end if;

   if I_message_type NOT IN (RMSSUB_XORDER.LP_del_type, RMSSUB_XORDER.LP_dtl_del_type) then
      if I_message.orig_ind is NOT NULL then
         if I_message.orig_ind not in (2, 6, 7, 8) then
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                  'orig_ind',
                                                  I_message.orig_ind,
                                                  '2, 6, 7, or 8');
            return FALSE;
         end if;

         if I_message.orig_ind = 6 and
            SYSTEM_OPTIONS_SQL.GP_system_options_row.aip_ind != 'Y' then
            ---
            O_error_message := SQL_LIB.CREATE_MSG('AIP_ORIG_IND',
                                                  '. Origin Ind : '|| I_message.orig_ind ,
                                                  'and aip_ind in System Options : '||SYSTEM_OPTIONS_SQL.GP_system_options_row.aip_ind,
                                                   NULL);
            return FALSE;
         end if;
      end if;
   end if;

   if I_message.XOrderDtl_TBL is NULL or I_message.XOrderDtl_TBL.COUNT = 0 then
      if I_message_type in (RMSSUB_XORDER.LP_dtl_cre_type, RMSSUB_XORDER.LP_dtl_mod_type) then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC',
                                               ' Order No : ',
                                              I_message.order_no,
                                               NULL);
         return FALSE;
      end if;
   else
      for i in I_message.XOrderDtl_TBL.first..I_message.XOrderDtl_TBL.last loop
         if I_message.XOrderDtl_TBL(i).location is NULL and I_message_type in (RMSSUB_XORDER.LP_dtl_cre_type, RMSSUB_XORDER.LP_cre_type) then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'location', NULL, NULL);
            return FALSE;
         end if;
         if I_message.XOrderDtl_TBL(i).reinstate_ind is NOT NULL and
            I_message.XOrderDtl_TBL(i).reinstate_ind NOT IN  ('Y','N') then
            if I_message.XOrderDtl_TBL(i).item is NOT NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_REINSTATE_IND',
                                                     I_message.XOrderDtl_TBL(i).item,
                                                     I_message.XOrderDtl_TBL(i).location,
                                                     NULL);
            elsif I_message.XOrderDtl_TBL(i).ref_item is NOT NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_REINSTATE_IND',
                                                     I_message.XOrderDtl_TBL(i).ref_item,
                                                     I_message.XOrderDtl_TBL(i).location,
                                                     NULL);
            end if;
            return FALSE;
         end if;
         if I_message.XOrderDtl_TBL(i).cancel_ind is NOT NULL and
            I_message.XOrderDtl_TBL(i).cancel_ind NOT IN  ('Y','N') then
            if I_message.XOrderDtl_TBL(i).item is NOT NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_CANCEL_IND',
                                                     I_message.XOrderDtl_TBL(i).item,
                                                     I_message.XOrderDtl_TBL(i).location,
                                                     NULL);
            elsif I_message.XOrderDtl_TBL(i).ref_item is NOT NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_CANCEL_IND',
                                                     I_message.XOrderDtl_TBL(i).ref_item,
                                                     I_message.XOrderDtl_TBL(i).location,
                                                     NULL);
            end if;
            return FALSE;
         end if;
         -- cancel_ind and reinstate_ind are only applicable to detail mod messages
         if I_message_type != RMSSUB_XORDER.LP_dtl_mod_type and
            (I_message.XOrderDtl_TBL(i).reinstate_ind is NOT NULL and
             I_message.XOrderDtl_TBL(i).reinstate_ind = 'Y' or
             I_message.XOrderDtl_TBL(i).cancel_ind is NOT NULL and
             I_message.XOrderDtl_TBL(i).cancel_ind = 'Y') then

            if I_message.XOrderDtl_TBL(i).item is NOT NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INV_CANCEL_REINSTATE',
                                                     I_message.XOrderDtl_TBL(i).item,
                                                     I_message.XOrderDtl_TBL(i).location,
                                                     NULL);
            elsif I_message.XOrderDtl_TBL(i).ref_item is NOT NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INV_CANCEL_REINSTATE',
                                                     I_message.XOrderDtl_TBL(i).ref_item,
                                                     I_message.XOrderDtl_TBL(i).location,
                                                     NULL);
            end if;
            return FALSE;
         end if;

         if I_message.XOrderDtl_TBL(i).item is NULL then
            -- get the tran level item
            open C_ITEM_PARENT(I_message.XOrderDtl_TBL(i).ref_item);
            fetch C_ITEM_PARENT into L_item_parent;
            close C_ITEM_PARENT;
         end if;

         -- cannot cancel qty via qty_ordered and cancel indicator at the same time
         if I_message.XOrderDtl_TBL(i).cancel_ind = 'Y' and I_message.XOrderDtl_TBL(i).qty_ordered <> 0 then
            O_error_message := SQL_LIB.CREATE_MSG('INV_MOD_QTY',
                                                  NVL(I_message.XOrderDtl_TBL(i).item, L_item_parent),
                                                  I_message.XOrderDtl_TBL(i).location,
                                                  NULL);
            return FALSE;
         end if;
         -- cannot reinstate and cancel qty at the same time
         if I_message.XOrderDtl_TBL(i).reinstate_ind = 'Y' and I_message.XOrderDtl_TBL(i).qty_ordered <> 0 then
            O_error_message := SQL_LIB.CREATE_MSG('NO_CANCEL_AND_REINSTATE',
                                                  NVL(I_message.XOrderDtl_TBL(i).item, L_item_parent),
                                                  I_message.XOrderDtl_TBL(i).location,
                                                  NULL);
            return FALSE;
         end if;
         if I_message.XOrderDtl_TBL(i).cancel_ind is NOT NULL and
            I_message.XOrderDtl_TBL(i).cancel_ind = 'Y' and
            I_message.XOrderDtl_TBL(i).reinstate_ind is NOT NULL and
            I_message.XOrderDtl_TBL(i).reinstate_ind = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_CANCEL_AND_REINSTATE',
                                                  NVL(I_message.XOrderDtl_TBL(i).item, L_item_parent),
                                                  I_message.XOrderDtl_TBL(i).location,
                                                  NULL);
            return FALSE;
         end if;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_MESSAGE;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XORDER_SQL;
/