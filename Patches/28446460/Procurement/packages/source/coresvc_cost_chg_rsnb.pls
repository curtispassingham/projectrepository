CREATE OR REPLACE PACKAGE BODY CORESVC_COST_CHG_REASON as
------------------------------------------------------------------------------------------
   cursor C_SVC_COST_CHG_RSN_TL(I_process_id   NUMBER,
                                I_chunk_id     NUMBER) is
      select pk_cost_chg_reason.rowid      as pk_cost_chg_reason_rid,
             st.rowid                      as st_rid,
             st.reason_desc,
             st.reason,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)              as action,
             st.process$status
        from svc_cost_chg_rsn_tl st,
             cost_chg_reason               pk_cost_chg_reason,
             dual
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.reason     = pk_cost_chg_reason.reason (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;

   Type cost_chg_reason_tab IS TABLE OF COST_CHG_REASON_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                         CASE
                                                                            WHEN I_sqlcode IS NULL THEN
                                                                               I_sqlerrm
                                                                            ELSE
                                                                               'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                            END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LasT_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LasT_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;
END WRITE_ERROR;
-----------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES (I_file_id   NUMBER) IS
   L_sheets                 s9t_pkg.names_map_typ;
   COST_CHG_RSN_TL_cols     s9t_pkg.names_map_typ;
   COST_CHG_RSN_LANG_cols   s9t_pkg.names_map_typ;
BEGIN
   L_sheets                      :=s9t_pkg.get_sheet_names(I_file_id);
   COST_CHG_RSN_TL_cols          :=s9t_pkg.get_coL_names(I_file_id,
                                                         COST_CHG_RSN_TL_sheet);
   COST_CHG_RSN_TL$Action        := COST_CHG_RSN_TL_cols('ACTION');
   COST_CHG_RSN_TL$REASON_DESC   := COST_CHG_RSN_TL_cols('REASON_DESC');
   COST_CHG_RSN_TL$REASON        := COST_CHG_RSN_TL_cols('REASON');

   COST_CHG_RSN_LANG_cols        :=s9t_pkg.get_coL_names(I_file_id,
                                                         COST_CHG_RSN_LANG_sheet);
   COST_CHG_RSN_LANG$Action      := COST_CHG_RSN_LANG_cols('ACTION');
   COST_CHG_RSN_LANG$LANG        := COST_CHG_RSN_LANG_cols('LANG');
   COST_CHG_RSN_LANG$REASON      := COST_CHG_RSN_LANG_cols('REASON');
   COST_CHG_RSN_LANG$REASON_DESC := COST_CHG_RSN_LANG_cols('REASON_DESC');

END POPULATE_NAMES;
-----------------------------------------------------------------------------------
PROCEDURE POPULATE_COST_CHG_RSN_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = COST_CHG_RSN_TL_sheet )
               select s9t_row(s9t_cells(CORESVC_COST_CHG_REASON.action_mod ,
                                        REASON,
                                        REASON_desc))
                 from cost_chg_reason_tl
                where lang = LP_primary_lang;
END POPULATE_COST_CHG_RSN_TL;
-----------------------------------------------------------------------------------
PROCEDURE POPULATE_COST_CHG_RSN_LANG( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = COST_CHG_RSN_LANG_sheet )
               select s9t_row(s9t_cells(CORESVC_COST_CHG_REASON.action_mod ,
                                        tl.lang,
                                        tl.reason,
                                        tl.reason_desc))
                 from cost_chg_reason_tl tl
                where tl.lang <> LP_primary_lang;
END POPULATE_COST_CHG_RSN_LANG;
-----------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS

   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(COST_CHG_RSN_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(COST_CHG_RSN_TL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                             'REASON',
                                                                                             'REASON_DESC');

   L_file.add_sheet(COST_CHG_RSN_LANG_sheet);
   L_file.sheets(L_file.get_sheet_index(COST_CHG_RSN_LANG_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                               'LANG',
                                                                                               'REASON',
                                                                                               'REASON_DESC');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
-----------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COST_CHG_REASON.CREATE_S9T';
   L_file      s9t_file;

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_COST_CHG_RSN_TL(O_file_id);
      POPULATE_COST_CHG_RSN_LANG(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-----------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_COST_CHG_RSN_TL( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                       I_process_id   IN   SVC_COST_CHG_RSN_TL.process_id%TYPE) IS

   TYPE svc_COST_CHG_RSN_TL_coL_typ IS TABLE OF SVC_COST_CHG_RSN_TL%ROWTYPE;
   L_temp_rec                SVC_COST_CHG_RSN_TL%ROWTYPE;
   svc_COST_CHG_RSN_TL_col   svc_COST_CHG_RSN_TL_coL_typ := NEW svc_COST_CHG_RSN_TL_coL_typ();
   L_process_id              SVC_COST_CHG_RSN_TL.process_id%TYPE;
   L_error                   BOOLEAN := FALSE;
   L_default_rec             SVC_COST_CHG_RSN_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select REASON_DESC_mi,
             REASON_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key = template_key
                 and wksht_key    = 'COST_CHG_REASON')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('REASON_DESC' as REASON_DESC,
                                       'REASON' as REASON,
                                        null as dummy));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COST_CHG_RSN_TL';
   L_pk_columns    VARCHAR2(255)  := 'Code';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
  -- Get default values.
   FOR rec IN (select REASON_DESC_dv,
                      REASON_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key = template_key
                          and wksht_key    = 'COST_CHG_REASON')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('REASON_DESC' as REASON_DESC,
                                                'REASON'      as REASON,
                                                NULL          as dummy)))
   LOOP
      BEGIN
         L_default_rec.REASON_DESC := rec.REASON_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_CHG_REASON',
                            NULL,
                           'REASON_DESC ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.REASON := rec.REASON_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_CHG_REASON',
                            NULL,
                           'REASON ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;

 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(COST_CHG_RSN_TL$Action)      as Action,
                      r.get_cell(COST_CHG_RSN_TL$REASON_DESC) as REASON_DESC,
                      r.get_cell(COST_CHG_RSN_TL$REASON)      as REASON,
                      r.get_row_seq()                         as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(COST_CHG_RSN_TL_sheet))
   LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_CHG_RSN_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.REASON_DESC := rec.REASON_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_CHG_RSN_TL_sheet,
                            rec.row_seq,
                            'REASON_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.REASON := rec.REASON;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_CHG_RSN_TL_sheet,
                            rec.row_seq,
                            'REASON',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_COST_CHG_REASON.action_new then
         L_temp_rec.REASON_DESC := NVL( L_temp_rec.REASON_DESC,L_default_rec.REASON_DESC);
         L_temp_rec.REASON      := NVL( L_temp_rec.REASON,L_default_rec.REASON);
      end if;
      if NOT (L_temp_rec.REASON is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         COST_CHG_RSN_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_COST_CHG_RSN_TL_col.extend();
         svc_COST_CHG_RSN_TL_col(svc_COST_CHG_RSN_TL_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_COST_CHG_RSN_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_COST_CHG_RSN_TL st
      using(select(case
                   when L_mi_rec.reason_desc_mi = 'N'
                    and svc_cost_chg_rsn_tl_col(i).action = coresvc_cost_chg_reason.action_mod
                    and s1.reason_desc is NULL then
                        mtl.reason_desc
                   else s1.reason_desc
                   end) as reason_desc,
                  (case
                   when L_mi_rec.reason_mi = 'N'
                    and svc_cost_chg_rsn_tl_col(i).action = coresvc_cost_chg_reason.action_mod
                    and s1.reason is NULL then
                        mtl.reason
                   else s1.reason
                   end) as reason,
                   null as dummy
              from (select svc_cost_chg_rsn_tl_col(i).reason_desc as reason_desc,
                           svc_cost_chg_rsn_tl_col(i).reason as reason,
                           null as dummy
                      from dual ) s1,
                    cost_chg_reason_tl mtl
             where mtl.reason (+) = s1.reason
               and mtl.lang (+) = LP_primary_lang)sq
               ON (st.reason    = sq.reason and
                   svc_cost_chg_rsn_tl_col(i).action in (coresvc_cost_chg_reason.action_mod,coresvc_cost_chg_reason.action_del))
      when matched then
      update
         set process_id      = svc_COST_CHG_RSN_TL_col(i).process_id ,
             chunk_id        = svc_COST_CHG_RSN_TL_col(i).chunk_id ,
             row_seq         = svc_COST_CHG_RSN_TL_col(i).row_seq ,
             action          = svc_COST_CHG_RSN_TL_col(i).action ,
             process$status  = svc_COST_CHG_RSN_TL_col(i).process$status ,
             REASON_desc     = sq.reason_desc ,
             create_id       = svc_COST_CHG_RSN_TL_col(i).create_id ,
             create_datetime = svc_COST_CHG_RSN_TL_col(i).create_datetime ,
             last_upd_id     = svc_COST_CHG_RSN_TL_col(i).last_upd_id ,
             last_upd_datetime = svc_COST_CHG_RSN_TL_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             reason_desc ,
             reason ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_COST_CHG_RSN_TL_col(i).process_id ,
             svc_COST_CHG_RSN_TL_col(i).chunk_id ,
             svc_COST_CHG_RSN_TL_col(i).row_seq ,
             svc_COST_CHG_RSN_TL_col(i).action ,
             svc_COST_CHG_RSN_TL_col(i).process$status ,
             sq.reason_desc ,
             sq.reason ,
             svc_COST_CHG_RSN_TL_col(i).create_id ,
             svc_COST_CHG_RSN_TL_col(i).create_datetime ,
             svc_COST_CHG_RSN_TL_col(i).last_upd_id ,
             svc_COST_CHG_RSN_TL_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP

         L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            COST_CHG_RSN_TL_sheet,
                            svc_COST_CHG_RSN_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);

         END LOOP;
   END;
END PROCESS_S9T_COST_CHG_RSN_TL;
--------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_COST_CHG_RSN_LANG(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                        I_process_id   IN   SVC_COST_CHG_REASON_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_COST_CHG_RSN_TL_COL_TYP IS TABLE OF SVC_COST_CHG_REASON_TL%ROWTYPE;
   L_temp_rec                       SVC_COST_CHG_REASON_TL%ROWTYPE;
   svc_cost_chg_rsn_tl_col          SVC_COST_CHG_RSN_TL_COL_TYP := NEW SVC_COST_CHG_RSN_TL_COL_TYP();
   L_process_id                     SVC_COST_CHG_REASON_TL.PROCESS_ID%TYPE;
   L_error                          BOOLEAN := FALSE;
   L_default_rec                    SVC_COST_CHG_REASON_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select lang_mi,
             reason_mi,
             reason_desc_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'COST_CHG_REASON_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('LANG'          as lang,
                                       'REASON'        as reason,
                                       'REASON_DESC'   as reason_desc));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COST_CHG_REASON_TL';
   L_pk_columns    VARCHAR2(255)  := 'Lang, Reason';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
  -- Get default values.
   FOR rec IN (select lang_dv,
                      reason_dv,
                      reason_desc_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key = template_key
                          and wksht_key    = 'COST_CHG_REASON_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('LANG'          as lang,
                                                'REASON'        as reason,
                                                'REASON_DESC'   as reason_desc)))
   LOOP
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_CHG_RSN_LANG_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.reason := rec.reason_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_CHG_RSN_LANG_SHEET ,
                            NULL,
                           'REASON ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.reason_desc := rec.reason_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_CHG_RSN_LANG_SHEET ,
                            NULL,
                           'REASON_DESC' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;

 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select upper(r.get_cell(COST_CHG_RSN_LANG$Action))      as action,
                      r.get_cell(COST_CHG_RSN_LANG$LANG)               as lang,
                      r.get_cell(COST_CHG_RSN_LANG$REASON)             as reason,
                      r.get_cell(COST_CHG_RSN_LANG$REASON_DESC)        as reason_desc,
                      r.get_row_seq()                                  as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(COST_CHG_RSN_LANG_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_CHG_RSN_LANG_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.reason := rec.reason;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_CHG_RSN_LANG_SHEET,
                            rec.row_seq,
                            'REASON',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.reason_desc := rec.reason_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_CHG_RSN_LANG_SHEET,
                            rec.row_seq,
                            'REASON_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_CHG_RSN_LANG_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_COST_CHG_REASON.action_new then
         L_temp_rec.lang          := NVL( L_temp_rec.lang,L_default_rec.lang);
         L_temp_rec.reason        := NVL( L_temp_rec.reason,l_default_rec.reason);
         L_temp_rec.reason_desc   := NVL( L_temp_rec.reason_desc,l_default_rec.reason_desc);

      end if;
      if NOT (L_temp_rec.lang is NOT NULL and L_temp_rec.REASON is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         COST_CHG_RSN_LANG_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_cost_chg_rsn_tl_col.extend();
         svc_cost_chg_rsn_tl_col(svc_cost_chg_rsn_tl_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_cost_chg_rsn_tl_col.COUNT SAVE EXCEPTIONS
      merge into svc_cost_chg_reason_tl st
      using(select(case
                   when L_mi_rec.lang_mi = 'N'
                    and svc_cost_chg_rsn_tl_col(i).action = CORESVC_COST_CHG_REASON.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when L_mi_rec.reason_mi = 'N'
                    and svc_cost_chg_rsn_tl_col(i).action = CORESVC_COST_CHG_REASON.action_mod
                    and s1.reason IS NULL then
                        mt.reason
                   else s1.reason
                   end) as reason,
                  (case
                   when L_mi_rec.reason_desc_mi = 'N'
                    and svc_cost_chg_rsn_tl_col(i).action = CORESVC_COST_CHG_REASON.action_mod
                    and s1.reason_desc IS NULL then
                        mt.reason_desc
                   else s1.reason_desc
                   end) as reason_desc,
                   null as dummy
              from (select svc_cost_chg_rsn_tl_col(i).lang as lang,
                           svc_cost_chg_rsn_tl_col(i).reason as reason,
                           svc_cost_chg_rsn_tl_col(i).reason_desc as reason_desc,
                           null as dummy
                      from dual ) s1,
                    cost_chg_reason_tl mt
             where mt.reason (+) = s1.reason
               and mt.lang (+) = s1.lang)sq
               ON (st.reason   = sq.reason and
                   st.lang     = sq.lang and
                   svc_cost_chg_rsn_tl_col(i).action IN (CORESVC_COST_CHG_REASON.action_mod,CORESVC_COST_CHG_REASON.action_del))
      when matched then
      update
         set process_id      = svc_COST_CHG_RSN_TL_col(i).process_id ,
             chunk_id        = svc_COST_CHG_RSN_TL_col(i).chunk_id ,
             row_seq         = svc_COST_CHG_RSN_TL_col(i).row_seq ,
             action          = svc_COST_CHG_RSN_TL_col(i).action ,
             process$status  = svc_COST_CHG_RSN_TL_col(i).process$status ,
             reason_desc     = sq.reason_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             lang ,
             reason ,
             reason_desc)
      values(svc_COST_CHG_RSN_TL_col(i).process_id ,
             svc_COST_CHG_RSN_TL_col(i).chunk_id ,
             svc_COST_CHG_RSN_TL_col(i).row_seq ,
             svc_COST_CHG_RSN_TL_col(i).action ,
             svc_COST_CHG_RSN_TL_col(i).process$status ,
             sq.lang ,
             sq.reason ,
             sq.reason_desc);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP

         L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            COST_CHG_RSN_LANG_SHEET,
                            svc_COST_CHG_RSN_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);

         END LOOP;
   END;
END PROCESS_S9T_COST_CHG_RSN_LANG;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_COST_CHG_REASON.PROCESS_S9T';
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);

   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_COST_CHG_RSN_TL(I_file_id,
                                  I_process_id);
      PROCESS_S9T_COST_CHG_RSN_LANG(I_file_id,
                                    I_process_id);
   end if;

   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   --Update process$status in svc_process_tracker
   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_COST_CHG_RSN_TL_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_error           IN OUT   BOOLEAN,
                                     I_rec             IN       C_SVC_COST_CHG_RSN_TL%ROWTYPE)

RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COST_CHG_REASON.PROCESS_COST_CHG_RSN_TL_VAL';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COST_CHG_RSN_TL';

BEGIN
   if I_rec.REASON IN (1,2,3,4,5,6,7,8,9,10)
      and I_rec.action = action_del then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'REASON',
                  'RSN_NO_DEL');
      O_error :=TRUE;
   end if;

   if I_rec.action = action_new and
      I_rec.REASON <= 0 then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'REASON',
                  'CODE_GREATER_ZERO');
      O_error :=TRUE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COST_CHG_RSN_TL_VAL;
-----------------------------------------------------------------------------------
FUNCTION EXEC_COST_CHG_RSN_TL_POST_INS( O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_process_error              IN OUT   BOOLEAN,
                                        I_rec                        IN       C_SVC_COST_CHG_RSN_TL%ROWTYPE,
                                        I_cost_chg_rsn_tl_temp_rec   IN       COST_CHG_REASON%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COST_CHG_REASON.EXEC_COST_CHG_RSN_TL_POST_INS';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COST_CHG_RSN_TL';

BEGIN
   if PC_ATTRIB_SQL.MERGE_COST_CHG_REASON_TL(O_error_message,
                                             I_cost_chg_rsn_tl_temp_rec.REASON,
                                             I_rec.REASON_desc,
                                             LP_primary_lang) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'REASON_DESC',
                  O_error_message);
      O_process_error := TRUE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COST_CHG_RSN_TL_POST_INS;
-----------------------------------------------------------------------------------
FUNCTION EXEC_COST_CHG_RSN_TL_INS( O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_process_error              IN OUT   BOOLEAN,
                                   I_rec                        IN       C_SVC_COST_CHG_RSN_TL%ROWTYPE,
                                   I_cost_chg_rsn_tl_temp_rec   IN       COST_CHG_REASON%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COST_CHG_REASON.EXEC_COST_CHG_RSN_TL_INS';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COST_CHG_RSN_TL';

BEGIN
   insert into cost_chg_reason
        values I_cost_chg_rsn_tl_temp_rec;

   if EXEC_COST_CHG_RSN_TL_POST_INS( O_error_message,
                                     O_process_error,
                                     I_rec,
                                     I_cost_chg_rsn_tl_temp_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      O_process_error := TRUE;
   end if;
   if O_process_error then
      ROLLBACK TO do_process_ins;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COST_CHG_RSN_TL_INS;
-----------------------------------------------------------------------------------
FUNCTION EXEC_COST_CHG_RSN_TL_PRE_DEL( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_process_error   IN OUT   BOOLEAN,
                                       I_rec             IN       C_SVC_COST_CHG_RSN_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COST_CHG_REASON.EXEC_COST_CHG_RSN_TL_PRE_DEL';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COST_CHG_RSN_TL';
   L_exists    VARCHAR2(1);

   cursor C_REASON_EXISTS is
      select 'x'
        from cost_susp_sup_head
       where REASON = I_rec.REASON;

BEGIN
   open C_REASON_EXISTS;
   fetch C_REASON_EXISTS into L_exists;
   if C_REASON_EXISTS%FOUND then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'REASON',
                  'INV_CODE_FRGN_CONSTRAINT');
      O_process_error := TRUE;
      close C_REASON_EXISTS;
      return FALSE;
   else
      if PC_ATTRIB_SQL.DEL_COST_CHG_REASON_TL(O_error_message,
                                              I_rec.REASON) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'REASON',
                     O_error_message);
         O_process_error := TRUE;
      end if;
   end if;
   close C_REASON_EXISTS;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_REASON_EXISTS%ISOPEN then
         close C_REASON_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COST_CHG_RSN_TL_PRE_DEL;
-----------------------------------------------------------------------------------
FUNCTION EXEC_COST_CHG_RSN_TL_DEL( O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_process_error              IN OUT   BOOLEAN,
                                   I_rec                        IN       C_SVC_COST_CHG_RSN_TL%ROWTYPE,
                                   I_cost_chg_rsn_tl_temp_rec   IN       COST_CHG_REASON%ROWTYPE )
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_COST_CHG_REASON.EXEC_COST_CHG_RSN_TL_DEL';
   L_svc_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COST_CHG_RSN_TL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COST_CHG_RSN_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_DEL is
      select 'x'
       from cost_chg_reason
      where reason = I_cost_chg_rsn_tl_temp_rec.reason
      for update nowait;

BEGIN
   if EXEC_COST_CHG_RSN_TL_PRE_DEL( O_error_message,
                                    O_process_error,
                                    I_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_svc_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      O_process_error := TRUE;
   else
      open C_LOCK_DEL;
      close C_LOCK_DEL;
      delete from cost_chg_reason
         where reason = I_cost_chg_rsn_tl_temp_rec.reason;
   end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_cost_chg_rsn_tl_temp_rec.reason,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_DEL%ISOPEN then
         close C_LOCK_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COST_CHG_RSN_TL_DEL;
-----------------------------------------------------------------------------------
FUNCTION PROCESS_COST_CHG_RSN_TL( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id      IN       SVC_COST_CHG_RSN_TL.PROCESS_ID%TYPE,
                                  I_chunk_id        IN       SVC_COST_CHG_RSN_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                    VARCHAR2(64) := 'CORESVC_COST_CHG_REASON.PROCESS_COST_CHG_RSN_TL';
   L_table                      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COST_CHG_RSN_TL';
   L_error                      BOOLEAN;
   L_process_error              BOOLEAN := FALSE;
   L_cost_chg_rsn_tl_temp_rec   COST_CHG_REASON%ROWTYPE;

BEGIN
   FOR rec IN C_SVC_COST_CHG_RSN_TL(I_process_id,
                                    I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.pk_cost_chg_reason_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'REASON',
                     'DUP_RECORD');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.pk_cost_chg_reason_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'REASON',
                     'NO_RECORD');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_new, action_mod) then
         if rec.REASON_DESC IS NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'REASON_DESC',
                        'MUST_ENTER_FIELD');
            L_error :=TRUE;
         end if;
      end if;

      if PROCESS_COST_CHG_RSN_TL_VAL(O_error_message,
                                     L_error,
                                     rec) = FALSE then
         WRITE_ERROR(rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     rec.chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_cost_chg_rsn_tl_temp_rec.REASON := rec.REASON;
         if rec.action = action_new then
            SAVEPOINT do_process_ins;
            if EXEC_COST_CHG_RSN_TL_INS( O_error_message,
                                         L_process_error,
                                         rec,
                                         L_cost_chg_rsn_tl_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if PC_ATTRIB_SQL.MERGE_COST_CHG_REASON_TL(O_error_message,
                                                      L_cost_chg_rsn_tl_temp_rec.REASON,
                                                      rec.REASON_desc,
                                                      LP_primary_lang) = FALSE then
               WRITE_ERROR(rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           rec.chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;

            -- if primary lang reason code is updated, set the reviewed_ind of the non-primary lang records to 'N'
            update cost_chg_reason_tl
               set reviewed_ind = 'N'
             where reason = L_cost_chg_rsn_tl_temp_rec.reason
               and lang <> LP_primary_lang;

         end if;

         if rec.action = action_del then
            if EXEC_COST_CHG_RSN_TL_DEL( O_error_message,
                                         L_process_error,
                                         rec,
                                         L_cost_chg_rsn_tl_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COST_CHG_RSN_TL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CCHG_REASON_TL_INS(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cchg_reason_tl_ins_tab    IN       COST_CHG_REASON_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COST_CHANGE.EXEC_CCHG_REASON_TL_INS';

BEGIN
   if I_cchg_reason_tl_ins_tab is NOT NULL and I_cchg_reason_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_cchg_reason_tl_ins_tab.COUNT()
         insert into cost_chg_reason_tl
              values I_cchg_reason_tl_ins_tab(i);


      FORALL i IN 1..I_cchg_reason_tl_ins_tab.COUNT()
         update cost_chg_reason_tl
            set reviewed_ind = 'Y'
          where reviewed_ind = 'N'
            and reason = I_cchg_reason_tl_ins_tab(i).reason
            and lang = LP_primary_lang;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CCHG_REASON_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CCHG_REASON_TL_UPD(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cchg_reason_tl_upd_tab   IN       COST_CHG_REASON_TAB,
                                 I_cchg_reason_tl_upd_rst   IN       ROW_SEQ_TAB,
                                 I_process_id               IN       SVC_COST_CHG_REASON_TL.PROCESS_ID%TYPE,
                                 I_chunk_id                 IN       SVC_COST_CHG_REASON_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_COST_CHANGE.EXEC_CCHG_REASON_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COST_CHG_REASON_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CCHG_REASON_TL_UPD(I_reason   COST_CHG_REASON_TL.REASON%TYPE,
                                    I_lang     COST_CHG_REASON_TL.LANG%TYPE) is
      select 'x'
        from cost_chg_reason_tl
       where reason = I_reason
         and lang = I_lang
         for update nowait;

BEGIN
   if I_cchg_reason_tl_upd_tab is NOT NULL and I_cchg_reason_tl_upd_tab.count > 0 then
      for i in I_cchg_reason_tl_upd_tab.FIRST..I_cchg_reason_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_cchg_reason_tl_upd_tab(i).lang);
            L_key_val2 := 'Reason: '||to_char(I_cchg_reason_tl_upd_tab(i).reason);
            open C_LOCK_CCHG_REASON_TL_UPD(I_cchg_reason_tl_upd_tab(i).reason,
                                           I_cchg_reason_tl_upd_tab(i).lang);
            close C_LOCK_CCHG_REASON_TL_UPD;
            
            update cost_chg_reason_tl
               set reason_desc = I_cchg_reason_tl_upd_tab(i).reason_desc,
                   last_update_id = I_cchg_reason_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_cchg_reason_tl_upd_tab(i).last_update_datetime
             where lang = I_cchg_reason_tl_upd_tab(i).lang
               and reason = I_cchg_reason_tl_upd_tab(i).reason;
            
            update cost_chg_reason_tl
               set reviewed_ind = 'Y'
             where reviewed_ind = 'N'
               and reason = I_cchg_reason_tl_upd_tab(i).reason
               and lang = LP_primary_lang;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_COST_CHG_REASON_TL',
                           I_cchg_reason_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CCHG_REASON_TL_UPD%ISOPEN then
         close C_LOCK_CCHG_REASON_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CCHG_REASON_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CCHG_REASON_TL_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cchg_reason_tl_del_tab   IN       COST_CHG_REASON_TAB,
                                 I_cchg_reason_tl_del_rst   IN       ROW_SEQ_TAB,
                                 I_process_id               IN       SVC_COST_CHG_REASON_TL.PROCESS_ID%TYPE,
                                 I_chunk_id                 IN       SVC_COST_CHG_REASON_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_COST_CHANGE.EXEC_CCHG_REASON_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COST_CHG_REASON_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CCHG_REASON_TL_DEL(I_reason  COST_CHG_REASON_TL.REASON%TYPE,
                                    I_lang    COST_CHG_REASON_TL.LANG%TYPE) is
      select 'x'
        from cost_chg_reason_tl
       where reason = I_reason
         and lang = I_lang
         for update nowait;

BEGIN
   if I_cchg_reason_tl_del_tab is NOT NULL and I_cchg_reason_tl_del_tab.count > 0 then
      for i in I_cchg_reason_tl_del_tab.FIRST..I_cchg_reason_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_cchg_reason_tl_del_tab(i).lang);
            L_key_val2 := 'Reason: '||to_char(I_cchg_reason_tl_del_tab(i).reason);
            open C_LOCK_CCHG_REASON_TL_DEL(I_cchg_reason_tl_del_tab(i).reason,
                                           I_cchg_reason_tl_del_tab(i).lang);
            close C_LOCK_CCHG_REASON_TL_DEL;
            
            delete cost_chg_reason_tl
             where lang = I_cchg_reason_tl_del_tab(i).lang
               and reason = I_cchg_reason_tl_del_tab(i).reason;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_COST_CHG_REASON_TL',
                           I_cchg_reason_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CCHG_REASON_TL_DEL%ISOPEN then
         close C_LOCK_CCHG_REASON_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CCHG_REASON_TL_DEL;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_COST_CHG_REASON_LANG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_process_id      IN       SVC_COST_CHG_REASON_TL.PROCESS_ID%TYPE,
                                      I_chunk_id        IN       SVC_COST_CHG_REASON_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                 VARCHAR2(64) := 'CORESVC_COST_CHG_REASON.PROCESS_COST_CHG_REASON_LANG';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COST_CHG_REASON_TL';
   L_base_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COST_CHG_REASON';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COST_CHG_REASON_TL';
   L_error                   BOOLEAN := FALSE;
   L_process_error           BOOLEAN := FALSE;
   L_cost_chg_reason_tl_temp_rec    COST_CHG_REASON_TL%ROWTYPE;
   L_cchg_reason_tl_upd_rst  ROW_SEQ_TAB;
   L_cchg_reason_tl_del_rst  ROW_SEQ_TAB;

   cursor C_SVC_COST_CHG_REASON_LANG(I_process_id NUMBER,
                                     I_chunk_id NUMBER) is
      select pk_cchg_reason_tl.rowid as pk_cchg_reason_tl_rid,
             fk_cchg_reason.rowid    as fk_cchg_reason_rid,
             fk_lang.rowid           as fk_lang_rid,
             pk_ccr_tl_prim.reason   as pk_ccr_tl_prim_rsn,
             st.lang,
             st.reason,
             st.reason_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_cost_chg_reason_tl st,
             cost_chg_reason        fk_cchg_reason,
             cost_chg_reason_tl     pk_cchg_reason_tl,
             (select reason
               from cost_chg_reason_tl
              where lang = LP_primary_lang) pk_ccr_tl_prim,
             lang                   fk_lang
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.reason     = fk_cchg_reason.reason (+)
         and st.lang       = pk_cchg_reason_tl.lang (+)
         and st.reason     = pk_cchg_reason_tl.reason (+)
         and st.reason     = pk_ccr_tl_prim.reason (+)
         and st.lang       = fk_lang.lang (+);

   TYPE SVC_COST_CHG_REASON_LANG is TABLE OF C_SVC_COST_CHG_REASON_LANG%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_cost_chg_reason_tab        SVC_COST_CHG_REASON_LANG;

   L_cchg_reason_tl_ins_tab         cost_chg_reason_tab         := NEW cost_chg_reason_tab();
   L_cchg_reason_tl_upd_tab         cost_chg_reason_tab         := NEW cost_chg_reason_tab();
   L_cchg_reason_tl_del_tab         cost_chg_reason_tab         := NEW cost_chg_reason_tab();

BEGIN
   if C_SVC_COST_CHG_REASON_LANG%ISOPEN then
      close C_SVC_COST_CHG_REASON_LANG;
   end if;

   open C_SVC_COST_CHG_REASON_LANG(I_process_id,
                                   I_chunk_id);
   LOOP
      fetch C_SVC_COST_CHG_REASON_LANG bulk collect into L_svc_cost_chg_reason_tab limit LP_bulk_fetch_limit;
      if L_svc_cost_chg_reason_tab.COUNT > 0 then
         FOR i in L_svc_cost_chg_reason_tab.FIRST..L_svc_cost_chg_reason_tab.LAST LOOP
         L_error := FALSE;
            
            --check for primary_lang
            if L_svc_cost_chg_reason_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cost_chg_reason_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;
            
            if L_svc_cost_chg_reason_tab(i).REASON IN (1,2,3,4,5,6,7,8,9,10)
              and L_svc_cost_chg_reason_tab(i).action = action_del then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_cost_chg_reason_tab(i).row_seq,
                              'REASON',
                              'RSN_NO_DEL');
                  L_error :=TRUE;
            end if;
            
            if L_svc_cost_chg_reason_tab(i).action = action_new and L_svc_cost_chg_reason_tab(i).lang <> LP_primary_lang and L_svc_cost_chg_reason_tab(i).pk_cchg_reason_tl_rid is NULL then
               if L_svc_cost_chg_reason_tab(i).pk_ccr_tl_prim_rsn is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_cost_chg_reason_tab(i).row_seq,
                              'LANG',
                              'PRIMARY_LANG_REQ');
                  L_error := TRUE;
               end if;
            end if;

            -- check if action is valid
            if L_svc_cost_chg_reason_tab(i).action is NULL
               or L_svc_cost_chg_reason_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cost_chg_reason_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_cost_chg_reason_tab(i).action = action_new
               and L_svc_cost_chg_reason_tab(i).pk_cchg_reason_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cost_chg_reason_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_cost_chg_reason_tab(i).action IN (action_mod, action_del)
               and L_svc_cost_chg_reason_tab(i).lang is NOT NULL
               and L_svc_cost_chg_reason_tab(i).reason is NOT NULL
               and L_svc_cost_chg_reason_tab(i).pk_cchg_reason_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_cost_chg_reason_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_cost_chg_reason_tab(i).action = action_new
               and L_svc_cost_chg_reason_tab(i).reason is NOT NULL
               and L_svc_cost_chg_reason_tab(i).fk_cchg_reason_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_cost_chg_reason_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_cost_chg_reason_tab(i).action = action_new
               and L_svc_cost_chg_reason_tab(i).lang is NOT NULL
               and L_svc_cost_chg_reason_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cost_chg_reason_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_cost_chg_reason_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cost_chg_reason_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if L_svc_cost_chg_reason_tab(i).reason is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cost_chg_reason_tab(i).row_seq,
                           'REASON',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_cost_chg_reason_tab(i).action in (action_new, action_mod) then
               if L_svc_cost_chg_reason_tab(i).reason_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_cost_chg_reason_tab(i).row_seq,
                              'REASON_DESC',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if NOT L_error then
               L_cost_chg_reason_tl_temp_rec.lang := L_svc_cost_chg_reason_tab(i).lang;
               L_cost_chg_reason_tl_temp_rec.reason := L_svc_cost_chg_reason_tab(i).reason;
               L_cost_chg_reason_tl_temp_rec.reason_desc := L_svc_cost_chg_reason_tab(i).reason_desc;
               L_cost_chg_reason_tl_temp_rec.orig_lang_ind := 'N';
               L_cost_chg_reason_tl_temp_rec.reviewed_ind := 'Y';
               L_cost_chg_reason_tl_temp_rec.create_datetime := SYSDATE;
               L_cost_chg_reason_tl_temp_rec.create_id := GET_USER;
               L_cost_chg_reason_tl_temp_rec.last_update_datetime := SYSDATE;
               L_cost_chg_reason_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_cost_chg_reason_tab(i).action = action_new then
                  L_cchg_reason_tl_ins_tab.extend;
                  L_cchg_reason_tl_ins_tab(L_cchg_reason_tl_ins_tab.count()) := L_cost_chg_reason_tl_temp_rec;
               end if;

               if L_svc_cost_chg_reason_tab(i).action = action_mod then
                  L_cchg_reason_tl_upd_tab.extend;
                  L_cchg_reason_tl_upd_tab(L_cchg_reason_tl_upd_tab.count()) := L_cost_chg_reason_tl_temp_rec;
                  L_cchg_reason_tl_upd_rst(L_cchg_reason_tl_upd_tab.count()) := L_svc_cost_chg_reason_tab(i).row_seq;
               end if;

               if L_svc_cost_chg_reason_tab(i).action = action_del then
                  L_cchg_reason_tl_del_tab.extend;
                  L_cchg_reason_tl_del_tab(L_cchg_reason_tl_del_tab.count()) := L_cost_chg_reason_tl_temp_rec;
                  L_cchg_reason_tl_del_rst(L_cchg_reason_tl_del_tab.count()) := L_svc_cost_chg_reason_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_COST_CHG_REASON_LANG%NOTFOUND;
   END LOOP;
   close C_SVC_COST_CHG_REASON_LANG;

   if EXEC_CCHG_REASON_TL_INS(O_error_message,
                              L_cchg_reason_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_CCHG_REASON_TL_UPD(O_error_message,
                              L_cchg_reason_tl_upd_tab,
                              L_cchg_reason_tl_upd_rst,
                              I_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_CCHG_REASON_TL_DEL(O_error_message,
                              L_cchg_reason_tl_del_tab,
                              L_cchg_reason_tl_del_rst,
                              I_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COST_CHG_REASON_LANG;
-----------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete from svc_cost_chg_reason_tl
      where process_id = I_process_id;

   delete from svc_cost_chg_rsn_tl
      where process_id = I_process_id;
END;
------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    := 'CORESVC_COST_CHG_REASON.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_COST_CHG_RSN_TL(O_error_message,
                              I_process_id,
                              I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_COST_CHG_REASON_LANG(O_error_message,
                                   I_process_id,
                                   I_chunk_id)=FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                          then 'PE'
                       else L_process_status
                    END),
          action_date = sysdate
    where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;

EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
-----------------------------------------------------------------------------------
END CORESVC_COST_CHG_REASON;
/