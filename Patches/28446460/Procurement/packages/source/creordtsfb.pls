CREATE OR REPLACE PACKAGE BODY CREATE_ORD_TSF_SQL AS

LP_dept_level_transfers   SYSTEM_OPTIONS.DEPT_LEVEL_TRANSFERS%TYPE := NULL;
LP_system_options_row     SYSTEM_OPTIONS%ROWTYPE;
LP_invreqitem_rec         "RIB_InvReqItem_REC";

-------------------------------------------------------------------------

FUNCTION ROUND_NEED_QTY (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_rounded_qty    IN OUT  TSFDETAIL.TSF_QTY%TYPE,
                         I_item           IN      TSFDETAIL.ITEM%TYPE,
                         I_from_loc       IN      TSFHEAD.FROM_LOC%TYPE,
                         I_to_loc         IN      TSFHEAD.TO_LOC%TYPE,
                         I_need_qty       IN      TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------

FUNCTION CREATE_ORD (O_error_message  IN OUT  VARCHAR2,
                     I_store          IN      ITEM_LOC.LOC%TYPE,
                     I_item_tbl       IN      ITEM_TBL_ORD)
RETURN BOOLEAN;

-------------------------------------------------------------------------

FUNCTION CREATE_TSF_DETAIL (O_error_message  IN OUT  VARCHAR2,
                            I_tsf_dtl_rec    IN      TSF_REC)
RETURN BOOLEAN;

-------------------------------------------------------------------------

FUNCTION CREATE_TSF (O_error_message  IN OUT  VARCHAR2,
                     I_store          IN      ITEM_LOC.LOC%TYPE,
                     I_item_tbl       IN      ITEM_TBL_TSF)
RETURN BOOLEAN;

-------------------------------------------------------------------------
FUNCTION CREATE_ORD (O_error_message  IN OUT  VARCHAR2,
                     I_store          IN      ITEM_LOC.LOC%TYPE,
                     I_item_tbl       IN      ITEM_TBL_ORD)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)  := 'CREATE_ORD_TSF_SQL.CREATE_ORD';

   TYPE new_ord_rec is RECORD (order_no         ORDHEAD.ORDER_NO%TYPE,
                               supplier         SUPS.SUPPLIER%TYPE,
                               dept             DEPS.DEPT%TYPE,
                               curr_code        ORDHEAD.CURRENCY_CODE%TYPE,
                               need_date        STORE_ORDERS.NEED_DATE%TYPE,
                               costing_loc      ITEM_LOC.COSTING_LOC%TYPE,
                               costing_loc_type ITEM_LOC.COSTING_LOC_TYPE%TYPE);

   TYPE new_ord_tbl is TABLE of new_ord_rec INDEX BY BINARY_INTEGER;

   L_origin_country_id    ITEM_LOC.PRIMARY_CNTRY%TYPE                       :=  NULL;
   L_currency_code        SUPS.CURRENCY_CODE%TYPE                           :=  NULL;
   L_item_record          ITEM_MASTER%ROWTYPE                               :=  NULL;
   L_order_no             ORDHEAD.ORDER_NO%TYPE                             :=  NULL;

   L_item                 ITEM_MASTER.ITEM%TYPE                             :=  NULL;
   L_supplier             SUPS.SUPPLIER%TYPE                                :=  NULL;
   L_count                NUMBER(5)                                         :=  0;
   L_found                VARCHAR2(2)                                       :=  'N';
   L_dept_level_orders    PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE   :=  NULL;
   L_store_type           STORE.STORE_TYPE%TYPE                             :=  NULL;
   L_costing_loc          ITEM_LOC.COSTING_LOC%TYPE                         :=  NULL;
   L_wf_order_no          WF_ORDER_HEAD.WF_ORDER_NO%TYPE                    :=  NULL;
   L_import_order         ORDHEAD.IMPORT_ORDER_IND%TYPE                     :=  NULL;
   L_import_id            ORDHEAD.IMPORT_ID%TYPE                            :=  NULL;
   L_import_type          ORDHEAD.IMPORT_TYPE%TYPE                          :=  NULL;
   L_f_import_id          ORDHEAD.IMPORT_ID%TYPE                            :=  NULL;
   L_f_import_type        ORDHEAD.IMPORT_TYPE%TYPE                          :=  NULL;
   L_order_status         ORDHEAD.STATUS%TYPE                               :=  NULL;
   L_new_ord_tbl          NEW_ORD_TBL;
   L_user                 ORDHEAD.ORIG_APPROVAL_ID%TYPE                     := GET_USER;
   L_sysdate              ORDHEAD.ORIG_APPROVAL_DATE%TYPE                   := SYSDATE;
   L_valid                BOOLEAN;

   L_l10n_obj             L10N_OBJ := L10N_OBJ();

   cursor C_GET_ORIGIN_COUNTRY_ID is
      select origin_country_id
        from repl_item_loc
       where item     = L_item
         and location = I_store
         and loc_type = 'S';

   cursor C_GET_PRIMARY_COUNTRY is
      select primary_cntry
        from item_loc
       where item     = L_item
         and loc      = I_store
         and loc_type = 'S';

   cursor C_GET_SUPPLIER_INFO is
      select currency_code
        from sups
       where supplier = L_supplier;

   cursor C_DEPT_LEVEL_ORDERS is
      select dept_level_orders
        from procurement_unit_options;

BEGIN
   -- initialize create_order_sql for this run...
   if CREATE_ORDER_SQL.INIT(O_error_message) = FALSE then
      return FALSE;
   end if;

   -- get dept_level_orders to pass around as needed
   open C_DEPT_LEVEL_ORDERS;
   fetch C_DEPT_LEVEL_ORDERS into L_dept_level_orders;
   close C_DEPT_LEVEL_ORDERS;

   select store_type
     into L_store_type
     from store
    where store = I_store;

   -- loop through all the records in the item table passed in.
   FOR i in 1..I_item_tbl.count LOOP

      -- set up some needed information

      L_item              := I_item_tbl(i).item;
      L_supplier          := I_item_tbl(i).supplier;
      L_found             := 'N';
      L_origin_country_id := NULL;
      L_currency_code     := NULL;
      L_costing_loc       := I_item_tbl(i).costing_loc;
      L_valid             := TRUE;

      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_item_record,
                                         I_item_tbl(i).item) = FALSE then
         L_valid := FALSE;
      else
         --- get origin country id from repl_item_loc. If null there, get from item_loc.
         open C_GET_ORIGIN_COUNTRY_ID;
         fetch C_GET_ORIGIN_COUNTRY_ID into L_origin_country_id;

         if L_origin_country_id is NULL then
            open C_GET_PRIMARY_COUNTRY;
            fetch C_GET_PRIMARY_COUNTRY into L_origin_country_id;
            close C_GET_PRIMARY_COUNTRY;
         end if;

         close C_GET_ORIGIN_COUNTRY_ID;

         --- Make sure supplier and origin county are not NULL before call PROCESS_DETAIL
         --- to create the order
         if L_supplier is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','L_supplier',L_program,NULL);
            L_valid := FALSE;
         elsif L_origin_country_id is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','L_origin_country_id',L_program,NULL);
            L_valid := FALSE;
         end if;
      end if;
      ---
      if L_valid = TRUE then
         --- no errors, continue processing

         open C_GET_SUPPLIER_INFO;
         fetch C_GET_SUPPLIER_INFO into L_currency_code;
         close C_GET_SUPPLIER_INFO;

         L_count := L_new_ord_tbl.count;

         -- Look for an existing order that this item can be added to
         if L_count > 0 then
            if L_store_type = 'C' then
               FOR j in 1..L_new_ord_tbl.count LOOP
                  if L_supplier = L_new_ord_tbl(j).supplier and
                     L_currency_code = L_new_ord_tbl(j).curr_code and
                     (L_dept_level_orders = 'N' or
                      L_item_record.dept = L_new_ord_tbl(j).dept) then

                     L_order_no := L_new_ord_tbl(j).order_no;

                     -- add this item to the found order
                     if CREATE_ORDER_SQL.PROCESS_DETAIL(O_error_message,
                                                        L_order_no,
                                                        I_item_tbl(i).item,
                                                        I_item_tbl(i).need_qty,
                                                        I_store,
                                                        I_item_tbl(i).supplier,
                                                        L_item_record.dept,
                                                        L_origin_country_id,
                                                        L_currency_code,
                                                        I_item_tbl(i).item_loc_status,
                                                        null,
                                                        null) = FALSE then
                        return FALSE;
                     end if;

                     L_found := 'Y';

                     EXIT;
                  end if;
               END LOOP;
            else -- franchise store
               FOR j in 1..L_new_ord_tbl.count LOOP
                  if L_supplier = L_new_ord_tbl(j).supplier and
                     L_currency_code = L_new_ord_tbl(j).curr_code and
                     L_costing_loc = L_new_ord_tbl(j).costing_loc and
                     (L_dept_level_orders = 'N' or
                      L_item_record.dept = L_new_ord_tbl(j).dept) then

                     L_order_no := L_new_ord_tbl(j).order_no;

                     -- add this item to the found order
                     if CREATE_ORDER_SQL.PROCESS_DETAIL(O_error_message,
                                                        L_order_no,
                                                        I_item_tbl(i).item,
                                                        I_item_tbl(i).need_qty,
                                                        I_store,
                                                        I_item_tbl(i).supplier,
                                                        L_item_record.dept,
                                                        L_origin_country_id,
                                                        L_currency_code,
                                                        I_item_tbl(i).item_loc_status,
                                                        null,
                                                        null) = FALSE then
                        return FALSE;
                     end if;

                     L_found := 'Y';

                     EXIT;
                  end if;
               END LOOP;
            end if;
         end if; -- L_count > 0

         -- if no order was found that would fit, create order header/details. If there are errors
         -- add to error package and process the next item.
         if L_found = 'N' then
            -- create the head info
            if CREATE_ORDER_SQL.PROCESS_HEAD(O_error_message,
                                             L_order_no,
                                             I_store,
                                             I_item_tbl(i).supplier,
                                             L_item_record.dept,
                                             L_origin_country_id,
                                             L_currency_code,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             'N/B',
                                             L_dept_level_orders) = FALSE then
               return FALSE;
            end if;
            -- create detail/item info
            if CREATE_ORDER_SQL.PROCESS_DETAIL(O_error_message,
                                               L_order_no,
                                               I_item_tbl(i).item,
                                               I_item_tbl(i).need_qty,
                                               I_store,
                                               I_item_tbl(i).supplier,
                                               L_item_record.dept,
                                               L_origin_country_id,
                                               L_currency_code,
                                               I_item_tbl(i).item_loc_status,
                                               null,
                                               null) = FALSE then
               return FALSE;
            end if;
            ---
            -- keep track of the orders created...
            L_count := L_count + 1;

            L_new_ord_tbl(L_count).order_no         := L_order_no;
            L_new_ord_tbl(L_count).supplier         := I_item_tbl(i).supplier;
            L_new_ord_tbl(L_count).dept             := L_item_record.dept;
            L_new_ord_tbl(L_count).curr_code        := L_currency_code;
            L_new_ord_tbl(L_count).need_date        := I_item_tbl(i).need_date;
            L_new_ord_tbl(L_count).costing_loc      := I_item_tbl(i).costing_loc;
            L_new_ord_tbl(L_count).costing_loc_type := I_item_tbl(i).costing_loc_type;

         end if; -- if L_found = 'N'
      else
         -- build LP_invreqitem_rec to send to add_error. Defaulting uop to 'EA' since
         -- conversion of UOP has been done in INV_REQUEST_SQL.CONVERT_NEED_QTY
         LP_invreqitem_rec.item        := I_item_tbl(i).item;
         LP_invreqitem_rec.qty_rqst    := I_item_tbl(i).need_qty;
         LP_invreqitem_rec.need_date   := I_item_tbl(i).need_date;
         LP_invreqitem_rec.uop         := 'EA';
         ---
         if RMSSUB_INVREQ_ERROR.ADD_ERROR(O_error_message,
                                          O_error_message,
                                          LP_invreqitem_rec) = FALSE then
            return FALSE;
         end if;
      end if; -- L_valid = TRUE

   END LOOP; -- end looping on I_item_tbl

   -- write the newly created orders to the db
   if CREATE_ORDER_SQL.COMPLETE_TRANSACTION(O_error_message) = FALSE then
      return FALSE;
   end if;

   -- update the status on ordhead for each order created to 'A'pproved.
   FOR i IN 1..L_new_ord_tbl.count LOOP

      if CREATE_ORDER_SQL.SET_ORDER_DATES(O_error_message,
                                          L_new_ord_tbl(i).order_no,
                                          L_new_ord_tbl(i).need_date) = FALSE then
         return FALSE;
      end if;

      L_l10n_obj.procedure_key := 'LOAD_ORDER_TAX_OBJECT';
      L_l10n_obj.doc_type := 'PO';
      L_l10n_obj.doc_id := L_new_ord_tbl(i).order_no;

      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_l10n_obj) = FALSE then
         return FALSE;
      end if;
      ---
      -- a corresponding Franchise Order needs to be created for a Franchise PO
      if L_store_type = 'F' then
         if WF_PO_SQL.SYNC_F_ORDER(O_error_message,
                                   L_wf_order_no,
                                   L_order_status,
                                   RMS_CONSTANTS.WF_ACTION_CREATE,
                                   L_new_ord_tbl(i).order_no,
                                   'A') = FALSE then
            return FALSE;
         end if;

         select import_order_ind,
                import_id,
                import_type
           into L_import_order,
                L_import_id,
                L_import_type
           from ordhead
          where order_no = L_new_ord_tbl(i).order_no;

         if L_import_order = 'Y' then
            if L_import_type is NULL or L_import_type != 'M' then
               L_f_import_id := L_new_ord_tbl(i).costing_loc;
               L_f_import_type := 'F';
            else -- import_type = 'M'
               L_f_import_id := L_import_id;
               L_f_import_type := L_import_type;
            end if;
         else
            L_f_import_id := L_new_ord_tbl(i).costing_loc;
            L_f_import_type := 'F';
         end if;
         ---
         update ordhead
            set loc_type = 'S',
                location = I_store,
                import_id = L_f_import_id,
                import_type = L_f_import_type,
                wf_order_no = L_wf_order_no,
                last_update_id = L_user,
                last_update_datetime = L_sysdate
          where order_no = L_new_ord_tbl(i).order_no;
      end if;  -- franchise store
      ---
      if L_order_status is NULL or L_order_status = 'A' then
         update ordhead
            set status = 'A',
                orig_approval_id   = L_user,
                orig_approval_date = L_sysdate,
                last_update_id = L_user,
                last_update_datetime = L_sysdate
          where order_no = L_new_ord_tbl(i).order_no;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_ORD;
-------------------------------------------------------------------------
FUNCTION CREATE_TSF_DETAIL (O_error_message  IN OUT  VARCHAR2,
                            I_tsf_dtl_rec    IN      TSF_REC)
RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'CREATE_ORD_TSF_SQL.CREATE_TSF_DETAIL';
   L_tsf_seq           TSFDETAIL.TSF_SEQ_NO%TYPE;
   L_pack_ind          ITEM_MASTER.PACK_IND%TYPE;
   L_receive_as_type   ITEM_LOC.RECEIVE_AS_TYPE%TYPE;
   L_tsf_seq_no        TSFDETAIL.TSF_SEQ_NO%TYPE;
   L_supp_pack_size    ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;

   cursor C_VALID_ITEM is
      select pack_ind
        from item_master
       where item = I_tsf_dtl_rec.item
         and item_level = tran_level
         and status = 'A';

   cursor C_MAX_SEQ is
   select NVL(MAX(td.tsf_seq_no), 0) + 1
     from tsfdetail td
    where td.tsf_no = I_tsf_dtl_rec.tsf_no;

BEGIN
   ---
   if I_tsf_dtl_rec.tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','tsf_no',L_program,NULL);
      return FALSE;
   end if;
   if I_tsf_dtl_rec.item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','item',L_program,NULL);
      return FALSE;
   end if;
   if I_tsf_dtl_rec.need_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','need_qty',L_program,NULL);
      return FALSE;
   end if;
   if I_tsf_dtl_rec.need_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','need_date',L_program,NULL);
      return FALSE;
   end if;
   ---
   open C_VALID_ITEM;
   fetch C_VALID_ITEM into L_pack_ind;
   if C_VALID_ITEM%NOTFOUND then
      close C_VALID_ITEM;
      O_error_message := SQL_LIB.CREATE_MSG('ERR_TSF_ITEM',I_tsf_dtl_rec.item,I_tsf_dtl_rec.from_loc,I_tsf_dtl_rec.to_loc);
      return FALSE;
   end if;
   close C_VALID_ITEM;
   ---
   if L_pack_ind = 'Y' then
      if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                L_receive_as_type,
                                                I_tsf_dtl_rec.item,
                                                I_tsf_dtl_rec.from_loc) = FALSE then
         return FALSE;
      end if;
      if nvl(L_receive_as_type,'P') = 'E' then
         O_error_message := SQL_LIB.CREATE_MSG('ERR_TSF_ITEM',I_tsf_dtl_rec.item,I_tsf_dtl_rec.from_loc,I_tsf_dtl_rec.to_loc);
         return FALSE;
      end if;
   end if;
   ---
   if SUPP_ITEM_ATTRIB_SQL.GET_SUPP_PACK_SIZE(O_error_message,
                                              L_supp_pack_size,
                                              I_tsf_dtl_rec.item,
                                              NULL,
                                              NULL) = FALSE then
      return FALSE;
   end if;

   L_tsf_seq_no := 0;
   open C_MAX_SEQ;
   fetch C_MAX_SEQ into L_tsf_seq_no;
   close C_MAX_SEQ;

   insert into tsfdetail (tsf_no,
                          tsf_seq_no,
                          item,
                          inv_status,
                          tsf_qty,
                          fill_qty,
                          ship_qty,
                          received_qty,
                          distro_qty,
                          selected_qty,
                          cancelled_qty,
                          supp_pack_size,
                          tsf_po_link_no,
                          mbr_processed_ind,
                          publish_ind,
                          updated_by_rms_ind)
                   values(I_tsf_dtl_rec.tsf_no,              -- TSF_NO
                          L_tsf_seq_no,                      -- TSF_SEQ_NO
                          I_tsf_dtl_rec.item,                -- ITEM
                          NULL,                              -- INV_STATUS
                          I_tsf_dtl_rec.need_qty,            -- TSF_QTY
                          NULL,                              -- FILL_QTY
                          NULL,                              -- SHIP_QTY
                          NULL,                              -- RECEIVED_QTY
                          NULL,                              -- DISTRO_QTY
                          NULL,                              -- SELECTED_QTY
                          NULL,                              -- CANCELLED_QTY
                          L_supp_pack_size,                  -- SUPP_PACK_SIZE
                          NULL,                              -- TSF_PO_LINK_NO
                          NULL,                              -- MBR_PROCESSED_IND
                          'N',                               -- PUBLISH_IND
                          'Y');                              -- UPDATED_BY_RMS_IND

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_TSF_DETAIL;

-------------------------------------------------------------------------

FUNCTION CREATE_TSF_DETAIL (O_error_message  IN OUT  VARCHAR2,
                            I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                            I_sg_tsf_ind     IN      VARCHAR2,
                            I_tsf_dtl_rec    IN      TSF_REC)
RETURN BOOLEAN IS

   L_program                     VARCHAR2(61) := 'CREATE_ORD_TSF_SQL.CREATE_TSF_DETAIL';
   L_tsf_seq                     TSFDETAIL.TSF_SEQ_NO%TYPE;
   L_pack_ind                    ITEM_MASTER.PACK_IND%TYPE;
   L_receive_as_type             ITEM_LOC.RECEIVE_AS_TYPE%TYPE;
   L_tsf_seq_no                  TSFDETAIL.TSF_SEQ_NO%TYPE;
   L_supp_pack_size              ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;

   L_currency_code               ORDHEAD.CURRENCY_CODE%TYPE;
   L_import_country_id           ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   L_total_cost_ord              ORDLOC.UNIT_COST%TYPE;
   L_total_cost_prim             ORDLOC.UNIT_COST%TYPE;
   L_total_retail_incl_vat_ord   ORDLOC.UNIT_RETAIL%TYPE;
   L_total_retail_incl_vat_prim  ORDLOC.UNIT_RETAIL%TYPE;
   L_total_retail_excl_vat_ord   ORDLOC.UNIT_RETAIL%TYPE;
   L_total_retail_excl_vat_prim  ORDLOC.UNIT_RETAIL%TYPE;
   L_total_landed_cost_ord       NUMBER(20,4);
   L_total_landed_cost_prim      NUMBER(20,4);
   L_total_expense_ord           NUMBER(20,4);
   L_total_expense_prim          NUMBER(20,4);
   L_total_duty_ord              NUMBER(20,4);
   L_total_duty_prim             NUMBER(20,4);
   L_qty                         ORDLOC.QTY_ORDERED%TYPE;
   L_exchange_rate               CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_tsfprice                    TSFDETAIL.TSF_PRICE%TYPE;
   L_loc_type                    ITEM_LOC.LOC_TYPE%TYPE;
   L_franchise_loc               ORDHEAD.LOCATION%TYPE;
   L_wf_order_no                 ORDHEAD.WF_ORDER_NO%TYPE   := NULL;
   L_to_loc                      TSFHEAD.TO_LOC%TYPE;

   cursor C_MAX_SEQ is
   select NVL(MAX(td.tsf_seq_no), 0) + 1
     from tsfdetail td
    where td.tsf_no = I_tsf_dtl_rec.tsf_no;

   cursor C_PO_DETAIL is
   select currency_code,
          import_country_id,
          NVL(location, '-1'),
          wf_order_no
     from ordhead
    where order_no = I_order_no;

BEGIN
   ---
   if I_tsf_dtl_rec.tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_tsf_dtl_rec.item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_tsf_dtl_rec.need_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'need_qty',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_tsf_dtl_rec.need_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'need_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if SUPP_ITEM_ATTRIB_SQL.GET_SUPP_PACK_SIZE(O_error_message,
                                              L_supp_pack_size,
                                              I_tsf_dtl_rec.item,
                                              NULL,
                                              NULL) = FALSE then
      return FALSE;
   end if;
   ---
   if I_SG_TSF_IND = 'Y' then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_PO_DETAIL',
                       'ORDHEAD',
                       'Order No: '||to_char(I_order_no));
      open C_PO_DETAIL;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_PO_DETAIL',
                       'ORDHEAD',
                       'Order No: '||to_char(I_order_no));
      fetch C_PO_DETAIL into L_currency_code,
                             L_import_country_id,
                             L_franchise_loc,
                             L_wf_order_no;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_PO_DETAIL',
                       'ORDHEAD',
                       'Order No: '||to_char(I_order_no));
      close C_PO_DETAIL;
      ---
      if CURRENCY_SQL.GET_RATE(O_error_message,
                               L_exchange_rate,
                               L_currency_code,
                               'P',
                               NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if L_wf_order_no is not NULL then
         L_to_loc := L_franchise_loc;
      else
         L_to_loc := I_tsf_dtl_rec.to_loc;
      end if;
      ---
      if ORDER_CALC_SQL.ITEMLOC_COST_RETAIL(O_error_message,
                                            L_total_cost_ord,
                                            L_total_cost_prim,
                                            L_total_retail_incl_vat_ord,
                                            L_total_retail_incl_vat_prim,
                                            L_total_retail_excl_vat_ord,
                                            L_total_retail_excl_vat_prim,
                                            L_total_landed_cost_ord,
                                            L_total_landed_cost_prim,
                                            L_total_expense_ord,
                                            L_total_expense_prim,
                                            L_total_duty_ord,
                                            L_total_duty_prim,
                                            I_order_no,
                                            L_to_loc,
                                            I_tsf_dtl_rec.item,
                                            I_tsf_dtl_rec.need_qty,
                                            L_exchange_rate,
                                            L_currency_code,
                                            L_import_country_id) = FALSE then
         return FALSE;
      end if;
      ---
      L_tsfprice := L_total_landed_cost_prim/I_tsf_dtl_rec.need_qty;
      ---
      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                      L_loc_type,
                                      I_tsf_dtl_rec.from_loc) = FALSE then
         return FALSE;
      end if;
      ---
      if L_loc_type in ('M','X') then
         L_loc_type := 'W';
      end if;
      --Convert price from Primary to From locations currency
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_tsf_dtl_rec.from_loc,
                                          L_loc_type,
                                          NULL,
                                          L_tsfprice,
                                          L_tsfprice,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   L_tsf_seq_no := 0;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_MAX_SEQ',
                    'TSFDETAIL',
                    'Transfer No: '||I_tsf_dtl_rec.tsf_no);
   open C_MAX_SEQ;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_MAX_SEQ',
                    'TSFDETAIL',
                    'Transfer No: '||I_tsf_dtl_rec.tsf_no);
   fetch C_MAX_SEQ into L_tsf_seq_no;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_MAX_SEQ',
                    'TSFDETAIL',
                    'Transfer No: '||I_tsf_dtl_rec.tsf_no);
   close C_MAX_SEQ;
   ---


   merge into tsfdetail target
   using (select I_tsf_dtl_rec.tsf_no tsf_no,
                 I_tsf_dtl_rec.item item
        from dual) inner
      on (    target.tsf_no = inner.tsf_no
          and target.item = inner.item )
    when matched then
         update set target.tsf_qty = target.tsf_qty + I_tsf_dtl_rec.need_qty,
                    target.ship_qty = target.ship_qty + I_tsf_dtl_rec.need_qty,
                    target.received_qty = target.received_qty + I_tsf_dtl_rec.need_qty,
                    target.supp_pack_size = L_supp_pack_size
    when not matched then
         insert (tsf_no,
                 tsf_seq_no,
                 item,
                 inv_status,
                 tsf_qty,
                 fill_qty,
                 ship_qty,
                 received_qty,
                 distro_qty,
                 selected_qty,
                 cancelled_qty,
                 supp_pack_size,
                 tsf_po_link_no,
                 mbr_processed_ind,
                 publish_ind,
                 tsf_price,
                 updated_by_rms_ind,
                 tsf_cost)
         values (I_tsf_dtl_rec.tsf_no,              -- TSF_NO
                 L_tsf_seq_no,                      -- TSF_SEQ_NO
                 I_tsf_dtl_rec.item,                -- ITEM
                 NULL,                              -- INV_STATUS
                 I_tsf_dtl_rec.need_qty,            -- TSF_QTY
                 NULL,                              -- FILL_QTY
                 I_tsf_dtl_rec.need_qty,            -- SHIP_QTY
                 I_tsf_dtl_rec.need_qty,            -- RECEIVED_QTY
                 NULL,                              -- DISTRO_QTY
                 NULL,                              -- SELECTED_QTY
                 NULL,                              -- CANCELLED_QTY
                 L_supp_pack_size,                  -- SUPP_PACK_SIZE
                 NULL,                              -- TSF_PO_LINK_NO
                 NULL,                              -- MBR_PROCESSED_IND
                 'N',                               -- PUBLISH_IND
                 L_tsfprice,                        -- TSFPRICE
                 'Y',                               -- UPDATED_BY_RMS_IND
                 L_tsfprice);                       -- TSF_COST

   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_TSF_DETAIL;

-------------------------------------------------------------------------

FUNCTION CREATE_TSF (O_error_message  IN OUT  VARCHAR2,
                     I_store          IN      ITEM_LOC.LOC%TYPE,
                     I_item_tbl       IN      ITEM_TBL_TSF)
RETURN BOOLEAN IS

   TYPE new_tsf_tbl is TABLE of tsf_rec INDEX BY BINARY_INTEGER;

   L_program        VARCHAR2(61)           := 'CREATE_ORD_TSF_SQL.CREATE_TSF';
   L_return_code    VARCHAR2(5)            := 'TRUE';
   L_count          NUMBER(5)              := 0;
   L_found          VARCHAR2(1)            := 'N';
   L_tsf_no         TSFHEAD.TSF_NO%TYPE;
   L_tsf_type       TSFHEAD.TSF_TYPE%TYPE;
   L_store_type     STORE.STORE_TYPE%TYPE;
   L_status         TSFHEAD.STATUS%TYPE;
   L_tsf_status     TSFHEAD.STATUS%TYPE;
   L_wf_order_no    TSFHEAD.WF_ORDER_NO%TYPE;
   L_action_type    VARCHAR2(30)           := RMS_CONSTANTS.WF_ACTION_CREATE;
   ---
   L_sob_desc_to_loc           FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE;
   L_sob_id_to_loc             FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE;
   L_sob_desc_from_loc         FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE;
   L_sob_id_from_loc           FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE;
   L_tsf_entity_id_to_loc      TSF_ENTITY.TSF_ENTITY_ID%TYPE;
   L_tsf_entity_desc_to_loc    TSF_ENTITY.TSF_ENTITY_DESC%TYPE;
   L_tsf_entity_id_from_loc    TSF_ENTITY.TSF_ENTITY_ID%TYPE;
   L_tsf_entity_desc_from_loc  TSF_ENTITY.TSF_ENTITY_DESC%TYPE;
   ---
   L_new_tsf_tbl    NEW_TSF_TBL;
   L_tsf_dtl_rec    TSF_REC;
   L_add_1          ADDR.ADD_1%TYPE;
   L_add_2          ADDR.ADD_2%TYPE;
   L_add_3          ADDR.ADD_3%TYPE;
   L_city           ADDR.CITY%TYPE;
   L_state          ADDR.STATE%TYPE;
   L_post           ADDR.POST%TYPE;
   L_module         ADDR.MODULE%TYPE;
   L_key_value_1    ADDR.KEY_VALUE_1%TYPE;
   L_country_id     COUNTRY.COUNTRY_ID%TYPE;
   L_l10n_obj       L10N_OBJ               := L10N_OBJ();
   cursor C_STORE_TYPE IS
     select store_type
       from store
      where store = I_store;

BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;

   FOR i in 1..I_item_tbl.count LOOP
      L_found := 'N';
      L_count := L_new_tsf_tbl.count;

      L_tsf_dtl_rec.item      := I_item_tbl(i).item;
      L_tsf_dtl_rec.dept      := I_item_tbl(i).dept;
      L_tsf_dtl_rec.from_loc  := I_item_tbl(i).from_loc;
      L_tsf_dtl_rec.to_loc    := I_item_tbl(i).to_loc;
      L_tsf_dtl_rec.need_qty  := I_item_tbl(i).need_qty;
      L_tsf_dtl_rec.need_date := I_item_tbl(i).need_date;
      L_tsf_dtl_rec.appr_ind  := I_item_tbl(i).appr_ind;

      if L_count > 0 then
         ---
         FOR j in 1..L_new_tsf_tbl.count LOOP
            if (L_tsf_dtl_rec.from_loc = L_new_tsf_tbl(j).from_loc and
                L_tsf_dtl_rec.appr_ind = L_new_tsf_tbl(j).appr_ind and
                (L_tsf_dtl_rec.dept      = L_new_tsf_tbl(j).dept or
                 LP_dept_level_transfers = 'N')) then
               ---
               L_tsf_dtl_rec.tsf_no := L_new_tsf_tbl(j).tsf_no;

               if CREATE_TSF_DETAIL(O_error_message,
                                    L_tsf_dtl_rec) = FALSE then
                  return FALSE;
               end if;

               L_found := 'Y';
               EXIT;
               ---
            end if;
         END LOOP;
         ---
      end if;

      if L_found != 'Y' then
         ---
         L_tsf_type := 'MR';
         -- need to determine the tsf_type (MR or IC)
         if LP_system_options_row.intercompany_transfer_basis = 'B' then
            if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                            L_sob_desc_to_loc,
                                            L_sob_id_to_loc,
                                            I_store,
                                            'S') = FALSE then
               return FALSE;
            end if;
            ---
            if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                            L_sob_desc_from_loc,
                                            L_sob_id_from_loc,
                                            L_tsf_dtl_rec.from_loc,
                                            'W') = FALSE then
               return FALSE;
            end if;
            ---
            if L_sob_id_to_loc != L_sob_id_from_loc then
               -- the transfer is InterCompany
               L_tsf_type := 'IC';
            end if;
         elsif LP_system_options_row.intercompany_transfer_basis = 'T' then
            if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                              L_tsf_entity_id_to_loc,
                                              L_tsf_entity_desc_to_loc,
                                              I_store,
                                              'S') = FALSE then
               return FALSE;
            end if;
            ---
            if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                              L_tsf_entity_id_from_loc,
                                              L_tsf_entity_desc_from_loc,
                                              L_tsf_dtl_rec.from_loc,
                                              'W') = FALSE then
               return FALSE;
            end if;
            ---
            if L_tsf_entity_id_to_loc != L_tsf_entity_id_from_loc then
               -- InterCompany transfer
               L_tsf_type := 'IC';
            end if;
         end if; -- transfer basis
         ---
         NEXT_TRANSFER_NUMBER(L_tsf_no,
                              L_return_code,
                              O_error_message);

         if (L_return_code <> 'TRUE') then
            return FALSE;
         end if;
         ---
         insert into tsfhead (tsf_no,
                              from_loc_type,
                              from_loc,
                              to_loc_type,
                              to_loc,
                              dept,
                              inventory_type,
                              tsf_type,
                              status,
                              freight_code,
                              routing_code,
                              create_date,
                              create_id,
                              approval_date,
                              approval_id,
                              delivery_date,
                              close_date,
                              ext_ref_no,
                              repl_tsf_approve_ind,
                              comment_desc)
                      values (L_tsf_no,                                                        -- Transfer Number
                              'W',                                                             -- From Loc Type
                              L_tsf_dtl_rec.from_loc,                                          -- Source Warehouse
                              'S',                                                             -- To Loc Type
                              I_store,                                                         -- Store
                              decode(LP_dept_level_transfers, 'Y', L_tsf_dtl_rec.dept, NULL),  -- Dept
                              'A',                                                             -- Inventory Type
                              L_tsf_type,                                                      -- Transfer Type
                              'I',                                                             -- Status
                              'N',                                                             -- Freight Code
                              NULL,                                                            -- Routing Code
                              sysdate,                                                         -- Create Date
                              'EXTERNAL',                                                      -- Create ID
                              NULL,                                                            -- Approval Date
                              NULL,                                                            -- Approval ID
                              L_tsf_dtl_rec.need_date,                                         -- Delivery_date
                              NULL,                                                            -- Close Date
                              NULL,                                                            -- External Reference Number
                              'N',                                                             -- Repl Transfer Approve Indicator
                              NULL);                                                           -- Comment Description

         L_tsf_dtl_rec.tsf_no   := L_tsf_no;

         L_module := 'WH'; -- for from location type
         if ADDRESS_SQL.GET_PRIM_ADDR(O_error_message,
                                      L_add_1,
                                      L_add_2,
                                      L_add_3,
                                      L_city,
                                      L_state,
                                      L_country_id,
                                      L_post,
                                      L_module,
                                      L_tsf_dtl_rec.from_loc,
                                      NULL) = FALSE then
            return FALSE;
         end if;

         if LP_system_options_row.default_tax_type = 'GTAX' then
            L_l10n_obj.procedure_key := 'CREATE_TRAN_UTIL_CODE';
            L_l10n_obj.doc_type      := 'TSF';
            L_l10n_obj.doc_id        := L_tsf_no;
            L_l10n_obj.source_entity := 'LOC';
            L_l10n_obj.source_id     := L_tsf_dtl_rec.from_loc;

            -- get the default utilization code for the transfer created
            if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                      L_l10n_obj) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if CREATE_TSF_DETAIL(O_error_message,
                              L_tsf_dtl_rec) = FALSE then
            return FALSE;
         end if;
         ---
         L_count := L_count + 1;
         L_new_tsf_tbl(L_count).tsf_no   := L_tsf_dtl_rec.tsf_no;
         L_new_tsf_tbl(L_count).dept     := L_tsf_dtl_rec.dept;
         L_new_tsf_tbl(L_count).from_loc := L_tsf_dtl_rec.from_loc;
         L_new_tsf_tbl(L_count).appr_ind := L_tsf_dtl_rec.appr_ind;
      end if;
      ---
   END LOOP;

   open C_STORE_TYPE;
  fetch C_STORE_TYPE into L_store_type;
  close C_STORE_TYPE;

   FOR j in 1..L_new_tsf_tbl.count LOOP
      if L_new_tsf_tbl(j).appr_ind = 'Y' then
         L_status :='A';
      else
         L_status :='I';
      end if;
      L_wf_order_no := NULL;

      if L_store_type ='F' then
         if WF_TRANSFER_SQL.SYNC_F_ORDER(O_error_message,
                                         L_wf_order_no,
                                         L_tsf_status,  --This is the tsf_status reflecting credit check
                                         L_action_type,
                                         L_new_tsf_tbl(j).tsf_no,
                                         L_status,      --This is the intended tsf_status before credit check
                                         L_tsf_type) = FALSE then
            return FALSE;
         end if;
      else
         L_tsf_status := L_status;
      end if;

      update tsfhead
         set status = L_tsf_status,
             wf_order_no = L_wf_order_no,
             approval_date = decode(L_tsf_status, 'A', sysdate),
             approval_id   = decode (L_tsf_status, 'A', 'EXTERNAL')
       where tsf_no = L_new_tsf_tbl(j).tsf_no;

      if L_tsf_status = 'A' then
         if TRANSFER_SQL.UPD_TSF_RESV_EXP(O_error_message,
                                          L_new_tsf_tbl(j).tsf_no,
                                          'A',                             -- I_add_delete_ind
                                          FALSE) = FALSE then              -- I_appr_second_leg
            return FALSE;
         end if;

      end if;
   END LOOP;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_TSF;

-------------------------------------------------------------------------

FUNCTION CREATE_TSF_IMP_EXP (O_error_message      IN OUT   VARCHAR2,
                             O_tsf_no             IN OUT   TSFHEAD.TSF_NO%TYPE,
                             I_create_tsf         IN OUT   VARCHAR2,
                             I_order_no           IN       ORDHEAD.ORDER_NO%TYPE,
                             I_item_tbl           IN       ITEM_TBL_TSF,
                             O_total_chrgs_prim   IN OUT   ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE)
RETURN BOOLEAN IS

   TYPE new_tsf_tbl is TABLE of tsf_rec INDEX BY BINARY_INTEGER;

   L_program              VARCHAR2(61)           := 'CREATE_ORD_TSF_SQL.CREATE_TSF_IMP_EXP';
   L_return_code          VARCHAR2(5)            := 'TRUE';
   L_count                NUMBER(5)              := 0;
   L_found                VARCHAR2(1)            := 'N';
   L_tsf_dtl_rec          TSF_REC;
   L_tsf_no               TSFHEAD.TSF_NO%TYPE;
   L_import_id            ORDHEAD.IMPORT_ID%TYPE;
   L_import_type          ORDHEAD.IMPORT_TYPE%TYPE;
   L_to_loc               TSFHEAD.TO_LOC%TYPE;
   L_to_loc_type          ORDLOC.LOC_TYPE%TYPE;
   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_buyer_pack           ITEM_MASTER.PACK_TYPE%TYPE := 'N';
   L_from_loc_type        ORDLOC.LOC_TYPE%TYPE;
   L_create_tsf           VARCHAR2(5) := I_create_tsf;
   L_tsf_seq_no           TSFDETAIL.TSF_SEQ_NO%TYPE := 0;
   L_l10n_obj             L10n_obj    := L10n_obj();
   L_profit_chrgs         ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE := 0;
   L_exp_chrgs            ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE := 0;
   L_il_exists            VARCHAR2(1);
   L_franchise_loc        ORDHEAD.LOCATION%TYPE;
   L_wf_order_po          ORDHEAD.WF_ORDER_NO%TYPE;

   cursor C_GET_DETAILS is
   select h.tsf_type,
          h.from_loc,
          h.from_loc_type,
          h.to_loc,
          h.to_loc_type,
          d.tsf_seq_no,
          d.item
     from tsfhead h,
          tsfdetail d
    where h.tsf_no = d.tsf_no
      and h.tsf_no = O_tsf_no
      and d.item   = L_item;

   cursor C_MAX_SEQ is
      select NVL(MAX(td.tsf_seq_no), 1)
        from tsfdetail td
    where td.tsf_no = O_tsf_no;

   cursor C_GET_FPO_DETAILS is
      select oh.wf_order_no,
             oh.location
        from ordhead oh
       where oh.order_no = I_order_no
         and oh.wf_order_no is NOT NULL;

   cursor C_ITEMLOC_EXISTS is
      select 'x'
        from item_loc
       where item = L_item
         and loc  = L_import_id;

BEGIN

   if ORDER_SQL.GET_DEFAULT_IMP_EXP(O_error_message,
                                    L_import_id,
                                    L_import_type,
                                    I_order_no) = FALSE then
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;

   ---
   open C_GET_FPO_DETAILS;
   fetch C_GET_FPO_DETAILS into L_wf_order_po,
                                L_franchise_loc;
   close C_GET_FPO_DETAILS;
   ---
   -- For a Franchise PO, a transfer will be generated even if no importer is specified.
   if (L_import_id is NULL or
      (L_import_id = I_item_tbl(1).to_loc)) and L_wf_order_po is NULL then
      return TRUE;
   end if;

   -- For Franchise PO generated transfers the To Location is the Franchise Location
   if L_wf_order_po is NOT NULL then
      L_to_loc := L_franchise_loc;
   else
      L_to_loc    := I_item_tbl(1).to_loc;
   end if;

   L_tsf_dtl_rec.item      := I_item_tbl(1).item;
   L_tsf_dtl_rec.dept      := I_item_tbl(1).dept;
   L_tsf_dtl_rec.from_loc  := L_import_id;
   L_tsf_dtl_rec.to_loc    := L_to_loc;
   L_tsf_dtl_rec.need_qty  := I_item_tbl(1).need_qty;
   L_tsf_dtl_rec.need_date := I_item_tbl(1).need_date;
   L_tsf_dtl_rec.appr_ind  := I_item_tbl(1).appr_ind;
   L_item                  := L_tsf_dtl_rec.item;

   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_to_loc_type,
                                   L_tsf_dtl_rec.to_loc) = FALSE then
      return FALSE;
   end if;
   ---
   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_from_loc_type,
                                   L_import_id) = FALSE then
      return FALSE;
   end if;
   ---
   if L_from_loc_type in ('M','X') then
      L_from_loc_type := 'W';
   end if;
   ---
   open C_ITEMLOC_EXISTS;
   fetch C_ITEMLOC_EXISTS into L_il_exists;
   close C_ITEMLOC_EXISTS;
   ---
   -- This will create a new item loc record for the item/import location if not yet exists.
   if L_il_exists is NULL and L_wf_order_po is NULL then
      if NEW_ITEM_LOC(O_error_message,
                      L_item,
                      L_import_id,
                      NULL,
                      NULL,
                      L_from_loc_type,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      FALSE,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'Y') = FALSE then   -- The ranging flag is set to 'Y'
         return FALSE;
      end if;
   end if;
   ---
   if L_create_tsf = 'Y' then
      NEXT_TRANSFER_NUMBER(O_tsf_no,
                           L_return_code,
                           O_error_message);

      if (L_return_code <> 'TRUE') then
         return FALSE;
      end if;
      ---
      insert into tsfhead (tsf_no,
                           from_loc_type,
                           from_loc,
                           to_loc_type,
                           to_loc,
                           dept,
                           inventory_type,
                           tsf_type,
                           status,
                           freight_code,
                           routing_code,
                           create_date,
                           create_id,
                           approval_date,
                           approval_id,
                           delivery_date,
                           close_date,
                           ext_ref_no,
                           repl_tsf_approve_ind,
                           comment_desc,
                           order_no)
                   values (O_tsf_no,                                                        -- Transfer Number
                           L_from_loc_type,                                                 -- From Loc Type
                           L_import_id,                                                     -- Source Warehouse
                           L_to_loc_type,                                                   -- To Loc Type
                           L_tsf_dtl_rec.to_loc,                                            -- To Loc
                           decode(LP_dept_level_transfers, 'Y', L_tsf_dtl_rec.dept, NULL),  -- Dept
                           'A',                                                             -- Inventory Type
                           'SG',                                                            -- Transfer Type
                           'C',                                                             -- Status
                           'N',                                                             -- Freight Code
                           NULL,                                                            -- Routing Code
                           sysdate,                                                         -- Create Date
                           'EXTERNAL',                                                      -- Create ID
                           NULL,                                                            -- Approval Date
                           NULL,                                                            -- Approval ID
                           L_tsf_dtl_rec.need_date,                                         -- Delivery_date
                           NULL,                                                            -- Close Date
                           NULL,                                                            -- External Reference Number
                           'N',                                                             -- Repl Transfer Approve Indicator
                           NULL,                                                            -- Comment Description
                           I_order_no);                                                     -- ORDER NO
      ---
      L_l10n_obj.procedure_key := 'CREATE_TRAN_UTIL_CODE';
      L_l10n_obj.doc_type      := 'TSF';
      L_l10n_obj.doc_id        :=  O_tsf_no;
      L_l10n_obj.source_entity := 'LOC';
      L_l10n_obj.source_id     := L_import_id;

      -- get and insert the default utilization code for transfer created into
      -- tsfhead_l10n_ext table id from loc country is localised.

      if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                 L_l10n_obj) = FALSE then
            return FALSE;
      end if;
      ---
      L_tsf_dtl_rec.tsf_no := O_tsf_no;
      L_create_tsf := 'N';
      ---
      if CREATE_TSF_DETAIL(O_error_message,
                           I_order_no,
                           'Y',
                           L_tsf_dtl_rec) = FALSE then
         return FALSE;
      end if;
      ---
   else
      L_tsf_dtl_rec.tsf_no := O_tsf_no;
      if CREATE_TSF_DETAIL(O_error_message,
                           I_order_no,
                           'Y',
                           L_tsf_dtl_rec) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_MAX_SEQ',
                    'TSFDETAIL',
                    'Transfer No: '||O_tsf_no);
   open C_MAX_SEQ;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_MAX_SEQ',
                    'TSFDETAIL',
                    'Transfer No: '||O_tsf_no);
   fetch C_MAX_SEQ into L_tsf_seq_no;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_MAX_SEQ',
                    'TSFDETAIL',
                    'Transfer No: '||O_tsf_no);
   close C_MAX_SEQ;
   ---

   -- Franchise PO generated transfers will not process charges. Transfers are generated for reference.
   if L_wf_order_po is NULL then
      if TRANSFER_CHARGE_SQL.DEFAULT_CHRGS(O_error_message,
                                           O_tsf_no,
                                           'SG',
                                           L_tsf_seq_no,
                                           NULL,
                                           NULL,
                                           L_import_id,
                                           L_from_loc_type,
                                           L_tsf_dtl_rec.to_loc,
                                           L_to_loc_type,
                                           L_item) = FALSE then
         return FALSE ;
      end if;
      ---
      if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_CHRGS(O_error_message,
                                                 O_total_chrgs_prim,
                                                 L_profit_chrgs,
                                                 L_exp_chrgs,
                                                 'T',
                                                 O_tsf_no,
                                                 'SG',
                                                 L_tsf_seq_no,
                                                 L_item,
                                                 NULL,  --pack_no
                                                 L_import_id,
                                                 L_from_loc_type,
                                                 L_tsf_dtl_rec.to_loc,
                                                 L_to_loc_type) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_TSF_IMP_EXP;

-------------------------------------------------------------------------

FUNCTION CREATE_ORD_TSF (O_error_message  IN OUT  VARCHAR2,
                         I_store          IN      ITEM_LOC.LOC%TYPE,
                         I_item_tbl       IN      INV_REQUEST_SQL.ITEM_TBL)
RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'CREATE_ORD_TSF_SQL.CREATE_ORD_TSF';
   L_supplier          SUPS.SUPPLIER%TYPE;
   L_source_wh         REPL_ITEM_LOC.SOURCE_WH%TYPE;
   L_stock_cat         REPL_ITEM_LOC.STOCK_CAT%TYPE;
   L_method            ITEM_LOC.SOURCE_METHOD%TYPE;
   L_dsd_ind           SUPS.DSD_IND%TYPE;
   L_repl_order_ctrl   REPL_ITEM_LOC.REPL_ORDER_CTRL%TYPE;
   L_soh               ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_item_loc_status   ITEM_LOC.STATUS%TYPE;
   L_valid             BOOLEAN;


   L_item              ITEM_LOC.ITEM%TYPE;
   L_dept              ITEM_MASTER.DEPT%TYPE;
   L_need_qty          STORE_ORDERS.NEED_QTY%TYPE;
   L_rounded_need_qty  STORE_ORDERS.NEED_QTY%TYPE;
   L_need_date         STORE_ORDERS.NEED_DATE%TYPE;
   L_origin_country_id REPL_ITEM_LOC.ORIGIN_COUNTRY_ID%TYPE;
   L_costing_loc       ITEM_LOC.COSTING_LOC%TYPE;
   L_costing_loc_type  ITEM_LOC.COSTING_LOC_TYPE%TYPE;
   L_record_exists     BOOLEAN := FALSE;

   L_count             NUMBER(10)  := 0;

   L_item_tbl_tsf      ITEM_TBL_TSF;
   L_item_tbl_ord      ITEM_TBL_ORD;
   L_unit_of_work      IF_ERRORS.UNIT_OF_WORK%TYPE;

   cursor C_REPL_ITEM_LOC is
     select  ril.primary_repl_supplier,
             ril.source_wh,
             ril.stock_cat,
             ril.repl_order_ctrl,
             NVL(ril.dept, im.dept),
             s.dsd_ind,
             il.status,
             ril.origin_country_id,
             NVL(il.costing_loc, -1),
             NVL(il.costing_loc_type, 'X')
        from repl_item_loc ril,
             sups s,
             item_loc il,
             item_master im
       where ril.item      = L_item
         and ril.location  = I_store
         and s.supplier(+) = ril.primary_repl_supplier
         and il.item       = ril.item
         and il.loc        = ril.location
         and il.item       = im.item
         and ril.stock_cat != 'W'
    UNION ALL -- when replenishment stock category is Warehouse Stocked primary supplier and country are not populated in replenishment attributes
      select il.primary_supp,
             ril.source_wh,
             ril.stock_cat,
             ril.repl_order_ctrl,
             nvl(ril.dept, im.dept),
             s.dsd_ind,
             il.status,
             il.primary_cntry,
             NVL(il.costing_loc, -1),
             NVL(il.costing_loc_type, 'X')
        from repl_item_loc ril,
             sups s,
             item_loc il,
             item_master im
       where ril.item      = L_item
         and ril.location  = I_store
         and s.supplier(+) = il.primary_supp
         and il.item       = ril.item
         and il.loc        = ril.location
         and il.item       = im.item
         and ril.stock_cat = 'W';

   cursor C_ITEM_LOC is
      select il.primary_supp,
             il.source_wh,
             il.source_method,
             im.dept,
             s.dsd_ind,
             il.status,
             il.primary_cntry,
             NVL(il.costing_loc, -1),
             NVL(il.costing_loc_type, 'X')
        from item_loc il,
             item_master im,
             sups s
       where il.item       = L_item
         and il.loc        = I_store
         and il.item       = im.item
         and s.supplier(+) = il.primary_supp;

BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   -- initialize record
   LP_invreqitem_rec :=   "RIB_InvReqItem_REC"(null,    -- rib_oid
                                             null,    -- item
                                             null,    -- qty_rqst
                                             null,    -- uop
                                             null,   -- need_date
                                             null);  -- delivery_slot_ind

   if I_item_tbl(1).item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_item', L_program, NULL);
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_DEPT_LEVEL_TSF(O_error_message,
                                            LP_dept_level_transfers) = FALSE then
      return FALSE;
   end if;
   ---
   FOR i in 1..I_item_tbl.count LOOP
      -- initialize values since they change for each item detail processed
      L_item              := I_item_tbl(i).item;
      L_need_qty          := I_item_tbl(i).need_qty;
      L_rounded_need_qty  := NULL;
      L_need_date         := I_item_tbl(i).need_date;
      L_valid             := TRUE;
      L_supplier          := NULL;
      L_item_loc_status   := NULL;
      L_stock_cat         := NULL;
      L_source_wh         := NULL;
      L_repl_order_ctrl   := NULL;
      L_dsd_ind           := NULL;
      L_method            := NULL;
      L_dept              := NULL;
      L_soh               := NULL;
      L_origin_country_id := NULL;
      L_record_exists     := FALSE;

      open C_REPL_ITEM_LOC;
      fetch C_REPL_ITEM_LOC into L_supplier,
                                 L_source_wh,
                                 L_stock_cat,
                                 L_repl_order_ctrl,
                                 L_dept,
                                 L_dsd_ind,
                                 L_item_loc_status,
                                 L_origin_country_id,
                                 L_costing_loc,
                                 L_costing_loc_type;
      if C_REPL_ITEM_LOC%NOTFOUND then
         close C_REPL_ITEM_LOC;

         open C_ITEM_LOC;
         fetch C_ITEM_LOC into L_supplier,
                               L_source_wh,
                               L_method,
                               L_dept,
                               L_dsd_ind,
                               L_item_loc_status,
                               L_origin_country_id,
                               L_costing_loc,
                               L_costing_loc_type;

         if C_ITEM_LOC%NOTFOUND then

            close C_ITEM_LOC;
            -- if the item location relationship was not found, create it and then requery
            if NEW_ITEM_LOC(O_error_message,
                            L_item,
                            I_store,
                            NULL,
                            NULL,
                            'S',                 /* store */
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            'A',                 /* make item/loc record Active */
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL) = FALSE then
               L_valid := FALSE;
            end if;

            open C_ITEM_LOC;
            fetch C_ITEM_LOC into L_supplier,
                                  L_source_wh,
                                  L_method,
                                  L_dept,
                                  L_dsd_ind,
                                  L_item_loc_status,
                                  L_origin_country_id,
                                  L_costing_loc,
                                  L_costing_loc_type;
            close C_ITEM_LOC;
         else
            close C_ITEM_LOC;
         end if;
      else
         close C_REPL_ITEM_LOC;
      end if;
      ---
      -- if we have a source wh, make sure that there is an item_loc record
      -- for that wh and this item
      ---

      if L_source_wh is not NULL then
         if NEW_ITEM_LOC(O_error_message,
                         L_item,
                         L_source_wh,
                         NULL,
                         NULL,
                         'W',                 /* wh */
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         'A',                 /* make item/loc record Active */
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL) = FALSE then
            L_valid := FALSE;
         end if;
      end if;
      ---
      if L_method = 'W' or
         L_stock_cat = 'W' then
         if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(O_error_message,
                                                       L_soh, -- available qty
                                                       L_item,
                                                       L_source_wh,
                                                       'W') = FALSE then
            return FALSE;
         end if;
      end if;
      ---

      if L_valid = TRUE then

         if ( ( NVL(L_stock_cat, ' ') = 'D' ) or
              ( NVL(L_stock_cat, ' ') = 'W' and NVL(L_soh, 0) < L_need_qty and L_dsd_ind = 'Y') or
              ( L_method = 'S' ) or
              ( L_method = 'W' and NVL(L_soh, 0) < L_need_qty and L_dsd_ind = 'Y' ) ) then

            if L_item_loc_status != 'A' then
               LP_invreqitem_rec.item                   := L_item;
               LP_invreqitem_rec.qty_rqst               := L_need_qty;
               LP_invreqitem_rec.need_date              := L_need_date;

               L_unit_of_work := 'Item '||L_item|| ', Supplier '||to_char(L_supplier)||', Need Qty '||L_need_qty|| ', Need Date '||L_need_date|| ', Location '||I_store ;

               if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                       SQL_LIB.GET_MESSAGE_TEXT('ORDER_ITEM_NOT_ACTIVE',
                                                                                I_item_tbl(1).item,
                                                                                I_store,
                                                                                NULL),
                                                       L_program,
                                                       L_unit_of_work) = FALSE then
                  return FALSE;
               end if;
            else

               --- build the order array
               if L_origin_country_id is NULL then
                  O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                        'L_origin_country_id',
                                                        L_program,
                                                        NULL);

                  -- build LP_invreqitem_rec to send to add_error. Defaulting uop to 'EA' since conversion of
                  -- UOP has been done in INV_REQUEST_SQL.CONVERT_NEED_QTY
                  LP_invreqitem_rec.item        := L_item;
                  LP_invreqitem_rec.qty_rqst    := L_need_qty;
                  LP_invreqitem_rec.need_date   := L_need_date;
                  LP_invreqitem_rec.uop         := 'EA';
                  ---
                  if RMSSUB_INVREQ_ERROR.ADD_ERROR(O_error_message,
                                                   O_error_message,
                                                   LP_invreqitem_rec) = FALSE then
                     return FALSE;
                  end if;
               elsif ITEM_SUPP_COUNTRY_LOC_SQL.LOC_EXISTS(O_error_message,
                                                          L_record_exists,
                                                          L_item,
                                                          L_supplier,
                                                          L_origin_country_id,
                                                          I_store)  = FALSE then
                  return FALSE;
               end if;
               ---
               if not L_record_exists then
                  O_error_message := SQL_LIB.CREATE_MSG('SUP_COST_NOT_FOUND',
                                                        L_supplier,
                                                        L_item,
                                                        NULL);

                  -- build LP_invreqitem_rec to send to add_error. Defaulting uop to 'EA' since conversion of
                  -- UOP has been done in INV_REQUEST_SQL.CONVERT_NEED_QTY
                  LP_invreqitem_rec.item        := L_item;
                  LP_invreqitem_rec.qty_rqst    := L_need_qty;
                  LP_invreqitem_rec.need_date   := L_need_date;
                  LP_invreqitem_rec.uop         := 'EA';
                  ---
                  if RMSSUB_INVREQ_ERROR.ADD_ERROR(O_error_message,
                                                   O_error_message,
                                                   LP_invreqitem_rec) = FALSE then
                     return FALSE;
                  end if;
               else
                  L_count := L_item_tbl_ord.count + 1;
                  ---
                  L_item_tbl_ord(L_count).supplier         := L_supplier;
                  L_item_tbl_ord(L_count).item             := L_item;
                  L_item_tbl_ord(L_count).need_qty         := L_need_qty;
                  L_item_tbl_ord(L_count).need_date        := L_need_date;
                  L_item_tbl_ord(L_count).item_loc_status  := L_item_loc_status;
                  L_item_tbl_ord(L_count).costing_loc      := L_costing_loc;
                  L_item_tbl_ord(L_count).costing_loc_type := L_costing_loc_type;
               end if;
            end if;
         else
            -- build the transfer array
            if L_source_wh is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','L_source_wh',L_program,NULL);

               -- build LP_invreqitem_rec to send to add_error. Defaulting uop to 'EA' since conversion of
               -- UOP has been done in INV_REQUEST_SQL.CONVERT_NEED_QTY
               LP_invreqitem_rec.item        := L_item;
               LP_invreqitem_rec.qty_rqst    := L_need_qty;
               LP_invreqitem_rec.need_date   := L_need_date;
               LP_invreqitem_rec.uop         := 'EA';
               ---
               if RMSSUB_INVREQ_ERROR.ADD_ERROR(O_error_message,
                                                O_error_message,
                                                LP_invreqitem_rec) = FALSE then
                  return FALSE;
               end if;
            elsif ROUND_NEED_QTY(O_error_message,
                                 L_rounded_need_qty,
                                 L_item,
                                 L_source_wh,
                                 I_store,
                                 L_need_qty) = FALSE then
               -- build LP_invreqitem_rec to send to add_error. Defaulting uop to 'EA' since conversion of
               -- UOP has been done in INV_REQUEST_SQL.CONVERT_NEED_QTY
               LP_invreqitem_rec.item        := L_item;
               LP_invreqitem_rec.qty_rqst    := L_need_qty;
               LP_invreqitem_rec.need_date   := L_need_date;
               LP_invreqitem_rec.uop         := 'EA';
               ---
               if RMSSUB_INVREQ_ERROR.ADD_ERROR(O_error_message,
                                                O_error_message,
                                                LP_invreqitem_rec) = FALSE then
                  return FALSE;
               end if;
            else
               ---
               L_count := L_item_tbl_tsf.count + 1;
               ---
               L_item_tbl_tsf(L_count).item      := L_item;
               L_item_tbl_tsf(L_count).dept      := L_dept;
               L_item_tbl_tsf(L_count).from_loc  := L_source_wh;
               L_item_tbl_tsf(L_count).to_loc    := I_store;
               L_item_tbl_tsf(L_count).need_qty  := L_rounded_need_qty;
               L_item_tbl_tsf(L_count).need_date := L_need_date;
               ---
               if (nvl(L_repl_order_ctrl, ' ') = 'A' or
                   L_repl_order_ctrl is  NULL) and
                   L_soh >= L_need_qty then
                   L_item_tbl_tsf(L_count).appr_ind := 'Y';
               else
                  L_item_tbl_tsf(L_count).appr_ind := 'N';
               end if;
            end if; -- L_source_wh is not null
         end if; -- check if order or transfer should be created
      else -- L_valid is FALSE
         -- build LP_invreqitem_rec to send to add_error. Defaulting uop to 'EA' since conversion of
         -- UOP has been done in INV_REQUEST_SQL.CONVERT_NEED_QTY
         LP_invreqitem_rec.item        := L_item;
         LP_invreqitem_rec.qty_rqst    := L_need_qty;
         LP_invreqitem_rec.need_date   := L_need_date;
         LP_invreqitem_rec.uop         := 'EA';
         ---
         if RMSSUB_INVREQ_ERROR.ADD_ERROR(O_error_message,
                                          O_error_message,
                                          LP_invreqitem_rec) = FALSE then
            return FALSE;
         end if;
      end if;  -- L_valid = TRUE
   END LOOP;
   ---
   if L_item_tbl_ord.count > 0 then
      if CREATE_ORD(O_error_message,
                    I_store,
                    L_item_tbl_ord) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if L_item_tbl_tsf.count > 0 then
      if CREATE_TSF(O_error_message,
                    I_store,
                    L_item_tbl_tsf) = FALSE then
         return FALSE;
      end if;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_ORD_TSF;
-------------------------------------------------------------------------
FUNCTION ROUND_NEED_QTY (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_rounded_qty    IN OUT  TSFDETAIL.TSF_QTY%TYPE,
                         I_item           IN      TSFDETAIL.ITEM%TYPE,
                         I_from_loc       IN      TSFHEAD.FROM_LOC%TYPE,
                         I_to_loc         IN      TSFHEAD.TO_LOC%TYPE,
                         I_need_qty       IN      TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'CREATE_ORD_TSF_SQL.ROUND_NEED_QTY';
   L_inv_parm          VARCHAR2(50) := NULL;
   L_store_ord_mult    ITEM_MASTER.STORE_ORD_MULT%TYPE;
   L_store_pack_size   ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_break_pack_ind    WH.BREAK_PACK_IND%TYPE;


BEGIN
   if I_item is NULL then
      L_inv_parm := 'I_item';
   elsif I_from_loc is NULL then
      L_inv_parm := 'I_from_loc';
   elsif I_to_loc is NULL then
      L_inv_parm := 'I_to_loc';
   elsif I_need_qty is NULL or I_need_qty < 0 then
      L_inv_parm := 'I_need_qty';
   end if;
   ---
   if L_inv_parm is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if ROUNDING_SQL.GET_PACKSIZE(O_error_message,
                                L_store_pack_size,
                                L_store_ord_mult,
                                L_break_pack_ind,
                                I_item,
                                I_from_loc,
                                NULL,
                                NULL,
                                I_to_loc) = FALSE then
      return FALSE;
   end if;
   ---
   if ROUNDING_SQL.TO_INNER_CASE(O_error_message,
                                 O_rounded_qty,
                                 I_item,
                                 NULL,
                                 NULL,
                                 I_to_loc,
                                 I_need_qty,
                                 NULL,
                                 L_store_pack_size,
                                 L_store_ord_mult,
                                 L_break_pack_ind) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ROUND_NEED_QTY;
-------------------------------------------------------------------------
FUNCTION BUILD_TRAN_DATA_SG (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_cost            IN       TRAN_DATA.TOTAL_COST%TYPE,
                             I_retail          IN       TRAN_DATA.TOTAL_RETAIL%TYPE,
                             I_qty             IN       TRAN_DATA.UNITS%TYPE,
                             I_distro_no       IN       TSFHEAD.TSF_NO%TYPE,
                             I_ref_pack_no     IN       TRAN_DATA.REF_PACK_NO%TYPE,
                                           I_tran_date       IN       DATE)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(61) := 'CREATE_ORD_TSF_SQL.BUILD_TRAN_DATA_SG';
   L_function                   VARCHAR2(40) := 'ORDER_RCV_SQL.STOCKLEDGER_INFO';
   L_item_record                ITEM_MASTER%ROWTYPE;
   ---
   L_tran_code                  TRAN_DATA.TRAN_CODE%TYPE;
   --- 
   L_tran_date                  PERIOD.VDATE%TYPE           := I_tran_date;
   L_extended_cost              TRAN_DATA.TOTAL_COST%TYPE   := 0.00;
   L_extended_retail            TRAN_DATA.TOTAL_RETAIL%TYPE := 0.00;
   L_source_retail              TRAN_DATA.TOTAL_RETAIL%TYPE;
   ---
   L_to_loc                     TSFHEAD.TO_LOC%TYPE;
   L_to_loc_type                TSFHEAD.TO_LOC_TYPE%TYPE;
   ---
   L_dept                       ITEM_MASTER.DEPT%TYPE;
   L_class                      ITEM_MASTER.CLASS%TYPE;
   L_subclass                   ITEM_MASTER.SUBCLASS%TYPE;
   L_import_id                  ORDHEAD.IMPORT_ID%TYPE;
   L_import_type                ORDHEAD.IMPORT_TYPE%TYPE;
   ---
   L_to_loc_currency            ORDHEAD.CURRENCY_CODE%TYPE;
   L_imp_exp_currency           ORDHEAD.CURRENCY_CODE%TYPE;
   ---
   L_profit                     NUMBER                      := 0;
   L_total_profit               NUMBER                      := 0;
   L_total_expense              NUMBER                      := 0;
   ---
   L_tsf_entity_id_to_loc       TSF_ENTITY.TSF_ENTITY_ID%TYPE;
   L_tsf_entity_id_imp          TSF_ENTITY.TSF_ENTITY_ID%TYPE;
   L_tsf_entity_desc_to_loc     TSF_ENTITY.TSF_ENTITY_DESC%TYPE;
   L_tsf_entity_desc_imp        TSF_ENTITY.TSF_ENTITY_DESC%TYPE;
   L_sob_id_to_loc              MV_LOC_SOB.SET_OF_BOOKS_ID%TYPE;
   L_sob_id_imp                 MV_LOC_SOB.SET_OF_BOOKS_ID%TYPE;
   L_sob_desc_to_loc            MV_LOC_SOB.SET_OF_BOOKS_DESC%TYPE;
   L_sob_desc_imp               MV_LOC_SOB.SET_OF_BOOKS_DESC%TYPE;
   L_physical_wh_to_loc         WH.PHYSICAL_WH%TYPE;
   L_physical_wh_imp            WH.PHYSICAL_WH%TYPE;
   L_tran_code_to_loc           TRAN_DATA.TRAN_CODE%TYPE;
   L_tran_code_imp              TRAN_DATA.TRAN_CODE%TYPE;
   L_tsf_source_location        TRAN_DATA.LOCATION%TYPE;
   L_tsf_source_loc_type        TRAN_DATA.LOC_TYPE%TYPE;
   L_from_loc_type              TSFHEAD.FROM_LOC_TYPE%TYPE;
   --------------
   L_profit_chrgs_to_loc        NUMBER                      := 0;
   L_total_chrgs_prim           ITEM_LOC.UNIT_RETAIL%TYPE   := 0;
   L_exp_chrgs_to_loc           NUMBER                      := 0;
   L_tsf_seq_no                 TSFDETAIL.TSF_SEQ_NO%TYPE;
   --------------
   L_stkldgr_vat_incl_retl_ind  SYSTEM_OPTIONS.STKLDGR_VAT_INCL_RETL_IND%TYPE;
   L_class_level_vat_ind        SYSTEM_OPTIONS.CLASS_LEVEL_VAT_IND%TYPE;
   L_class_vat_ind              CLASS.CLASS_VAT_IND%TYPE;
   L_from_loc                   TSFHEAD.FROM_LOC%TYPE;
   L_tax_rate_retail            VAT_CODE_RATES.VAT_RATE%TYPE;
   L_tax_code_retail            VAT_CODES.VAT_CODE%TYPE;   
   L_exempt_ind                 VARCHAR2(1)                 :='N';    
   L_vat_amount_rtl             TRAN_DATA.TOTAL_RETAIL%TYPE := 0;     
   L_extended_retail_src        TRAN_DATA.TOTAL_RETAIL%TYPE := 0;
   L_markup_markdown            TRAN_DATA.TOTAL_RETAIL%TYPE := 0;   
   L_shipment                   SHIPMENT.SHIPMENT%TYPE;
   L_gl_ref_no                  TRAN_DATA.GL_REF_NO%TYPE    := NULL;
   
   L_unit_cost                  ITEM_LOC_SOH.UNIT_COST%TYPE;   
   L_tran_loc                   TRAN_DATA.LOCATION%TYPE;
   L_tran_loc_type              TRAN_DATA.LOC_TYPE%TYPE;
   L_total_cost                 TRAN_DATA.TOTAL_COST%TYPE;
   L_total_retail               TRAN_DATA.TOTAL_RETAIL%TYPE;
   L_ref_no_1                   TRAN_DATA.REF_NO_1%TYPE;
   L_ref_no_2                   TRAN_DATA.REF_NO_2%TYPE := NULL;
   L_ref_pack_no                TRAN_DATA.REF_PACK_NO%TYPE;      
   L_l10n_fin_rec               L10N_FIN_REC  := L10N_FIN_REC();
   L_tran_data_rec              TRAN_DATA_REC := TRAN_DATA_REC();   
   
   L_tsf_price                  TSFDETAIL.TSF_PRICE%TYPE    := NULL;
   L_tsf_price_curr             TSFDETAIL.TSF_PRICE%TYPE    := NULL;
   L_tax_rate_cost              VAT_CODE_RATES.VAT_RATE%TYPE;
   L_tax_code_cost              VAT_CODES.VAT_CODE%TYPE;
   L_exem_ind                   VARCHAR2(1)                 :='N';  
  
   cursor C_GET_TSF_SG is
      select th.to_loc,
             th.to_loc_type,
             th.from_loc_type,
             oh.import_id,
             oh.import_type
        from tsfhead th,
             tsfdetail td,
             ordhead oh
       where th.tsf_no = td.tsf_no
         and th.order_no = oh.order_no
         and th.tsf_type = 'SG'
         and th.tsf_no = I_distro_no
         and th.order_no = I_order_no
         and td.item = NVL(I_ref_pack_no, I_item);

   cursor C_GET_TSF_SEQ is
         select tsf_seq_no,tsf_price
           from tsfdetail
          where tsf_no = I_distro_no
            and item = NVL(I_ref_pack_no, I_item);

   cursor C_GET_SOURCE_RETAIL is
      select unit_retail
        from item_loc
       where item = I_item
         and loc = L_import_id
         and loc_type = L_from_loc_type;
         
   cursor C_GET_CLASS_VAT_IND(p_dept  deps.dept%TYPE,
                              p_class class.class%TYPE) is
      select class_vat_ind
        from class
       where dept = p_dept
         and class = p_class;
         
   cursor C_GET_SHIPMENT is
      select shipment
        from shipment
       where order_no = I_order_no;         

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TSF_SG',
                    'TSFHEAD'||'TSFDETAIL'||'ORDHEAD',
                    'Transfer No: '||to_char(I_distro_no)||
                    'Order No: '||to_char(I_order_no)||
                    'Item No: '||I_item);
   open C_GET_TSF_SG;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TSF_SG',
                    'TSFHEAD'||'TSFDETAIL'||'ORDHEAD',
                    'Transfer No: '||to_char(I_distro_no)||
                    'Order No: '||to_char(I_order_no)||
                    'Item No: '||I_item);
   fetch C_GET_TSF_SG into L_to_loc,
                           L_to_loc_type,
                           L_from_loc_type,
                           L_import_id,
                           L_import_type; 
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TSF_SG',
                    'TSFHEAD'||'TSFDETAIL'||'ORDHEAD',
                    'Transfer No: '||to_char(I_distro_no)||
                    'Order No: '||to_char(I_order_no)||
                    'Item No: '||I_item);
   close C_GET_TSF_SG;
   ---
   if L_import_id is NULL or
     (L_import_id = L_to_loc) then
      return TRUE;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_record,
                                      I_item) = FALSE then
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---
   if L_to_loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_physical_wh_to_loc,
                                       L_to_loc) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if L_from_loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_physical_wh_imp,
                                       L_import_id) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   L_dept     := L_item_record.dept;
   L_class    := L_item_record.class;
   L_subclass := L_item_record.subclass;
   ---
   L_extended_retail   := I_retail;
   L_extended_cost     := I_cost;
   ---
   if LP_system_options_row.intercompany_transfer_basis = 'B' then
      if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                      L_sob_desc_to_loc,
                                      L_sob_id_to_loc,
                                      L_to_loc,
                                      L_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                      L_sob_desc_imp,
                                      L_sob_id_imp,
                                      L_import_id,
                                      L_from_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if L_sob_id_to_loc  <> L_sob_id_imp then
         L_tran_code_to_loc := 37;
         L_tran_code_imp    := 38;
      else
         if L_to_loc_type = 'W' and L_physical_wh_to_loc = L_physical_wh_imp then
            L_tran_code_to_loc := 31;
            L_tran_code_imp    := 33;
         else
            L_tran_code_to_loc := 30;
            L_tran_code_imp    := 32;
         end if;
      end if;
   elsif LP_system_options_row.intercompany_transfer_basis = 'T' then
      if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                        L_tsf_entity_id_to_loc,
                                        L_tsf_entity_desc_to_loc,
                                        L_to_loc,
                                        L_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                        L_tsf_entity_id_imp,
                                        L_tsf_entity_desc_imp,
                                        L_import_id,
                                        L_from_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if L_tsf_entity_id_to_loc <> L_tsf_entity_id_imp then
         L_tran_code_to_loc := 37;
         L_tran_code_imp    := 38;
      else
         if L_to_loc_type = 'W' and L_physical_wh_to_loc = L_physical_wh_imp then
            L_tran_code_to_loc := 31;
            L_tran_code_imp    := 33;
         else
            L_tran_code_to_loc := 30;
            L_tran_code_imp    := 32;
         end if;
      end if;
   end if;
   ---
   if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                L_to_loc,
                                L_to_loc_type,
                                NULL,
                                L_to_loc_currency)= FALSE then
      return FALSE;
   end if;
   ---
   if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                L_import_id,
                                L_from_loc_type,
                                NULL,
                                L_imp_exp_currency)= FALSE then
      return FALSE;
   end if;
   ---
   open C_GET_SOURCE_RETAIL;
   fetch C_GET_SOURCE_RETAIL into L_source_retail;
   close C_GET_SOURCE_RETAIL;
   
   open C_GET_TSF_SEQ;
   fetch C_GET_TSF_SEQ into L_tsf_seq_no,L_tsf_price;
   close C_GET_TSF_SEQ;

   if L_tran_code_to_loc in (30, 31) then
      L_tsf_source_location := L_import_id;
      L_tsf_source_loc_type := L_from_loc_type;
      ---
      if L_source_retail is not NULL then
         L_extended_retail := L_source_retail * I_qty;
      else
         L_source_retail := L_extended_retail/I_qty;
         ---
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_source_retail,
                                 L_to_loc_currency,
                                 L_imp_exp_currency,
                                 L_source_retail,
                                 'R',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
         ---
         L_extended_retail := L_source_retail * I_qty;
      end if;
      ---
      if L_extended_cost is not NULL then
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_extended_cost,
                                 L_to_loc_currency,
                                 L_imp_exp_currency,
                                 L_extended_cost,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
      end if;
   elsif L_tran_code_to_loc in (37) then   

      L_stkldgr_vat_incl_retl_ind    :=  LP_system_options_row.stkldgr_vat_incl_retl_ind;
      L_class_level_vat_ind          :=  LP_system_options_row.class_level_vat_ind;

       open C_GET_CLASS_VAT_IND(L_dept,L_class);
      fetch C_GET_CLASS_VAT_IND into L_class_vat_ind;
      close C_GET_CLASS_VAT_IND;

      if TAX_SQL.GET_TAX_RATE_INFO (O_error_message,
                                    L_tax_rate_retail,
                                    L_tax_code_retail,
                                    L_exem_ind,
                                    I_item,
                                    L_to_loc_type,
                                    L_to_loc,
                                    L_from_loc_type,
                                    L_import_id,
                                    'R') = FALSE then
         return FALSE;
      end if;
   
      if L_exem_ind = 'N' then    
         if L_stkldgr_vat_incl_retl_ind = 'Y' and L_class_level_vat_ind = 'Y' and L_class_vat_ind = 'N' then
            L_extended_retail := L_extended_retail + L_extended_retail * (NVL(L_tax_rate_retail,0)/100);
         elsif ((L_stkldgr_vat_incl_retl_ind = 'N' and L_class_level_vat_ind='Y' and L_class_vat_ind = 'Y') or
               (L_stkldgr_vat_incl_retl_ind = 'N' and L_class_level_vat_ind = 'N')) then
            L_extended_retail := L_extended_retail/(1+(NVL(L_tax_rate_retail,0)/100));
         end if;            
      end if;  
   end if;
 
   if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                          I_item,
                                          L_dept,
                                          L_class,
                                          L_subclass,
                                          L_to_loc,
                                          L_to_loc_type,
                                          L_tran_date,
                                          L_tran_code_to_loc,
                                          NULL,
                                          I_qty,
                                          L_extended_cost,
                                          L_extended_retail,
                                          I_distro_no,
                                          NULL,
                                          L_tsf_source_location,
                                          L_tsf_source_loc_type,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          L_function,
                                          L_import_id,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_ref_pack_no) = FALSE then
      return FALSE;
   end if;
   ---
   if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS( O_error_message,
                                                   L_total_chrgs_prim,
                                                   L_profit_chrgs_to_loc,
                                                   L_exp_chrgs_to_loc,
                                                   'T',
                                                   I_distro_no,
                                                   L_tsf_seq_no,
                                                   NULL,
                                                   NULL,
                                                   I_item,      --item
                                                   NULL,        --pack no
                                                   L_import_id,
                                                   'W',
                                                   L_to_loc,
                                                   L_to_loc_type) = FALSE then
            return FALSE;
   end if;

   L_total_profit := I_qty * L_profit_chrgs_to_loc;

   if L_total_profit != 0 and
      L_total_profit IS NOT NULL then
      L_tran_code := 28;
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             L_dept,
                                             L_class,
                                             L_subclass,
                                             L_to_loc,
                                             L_to_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,
                                             I_qty,
                                             L_total_profit,
                                             0,
                                             NULL,
                                             I_distro_no,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_ref_pack_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   L_total_expense := I_qty * L_exp_chrgs_to_loc;

   if L_total_expense != 0 and
      L_total_expense IS NOT NULL then
      L_tran_code := 29;

      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             L_dept,
                                             L_class,
                                             L_subclass,
                                             L_to_loc,
                                             L_to_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,
                                             I_qty,
                                             L_total_expense,
                                             0,
                                             NULL,
                                             I_distro_no,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_ref_pack_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if L_tran_code_to_loc = 37 then

      L_from_loc   := L_import_id;
       
      if TAX_SQL.GET_TAX_RATE_INFO(O_error_message,
                                   L_tax_rate_retail,
                                   L_tax_code_retail,
                                   L_exempt_ind,
                                   I_item,
                                   L_from_loc_type,
                                   L_from_loc,
                                   L_to_loc_type,
                                   L_to_loc,
                                   'R') = FALSE then
         return FALSE;
      end if;
      
      L_extended_retail  :=  L_tsf_price * I_qty;
      
      if L_stkldgr_vat_incl_retl_ind = 'Y' and L_exempt_ind = 'N' then
         L_vat_amount_rtl    := L_extended_retail * (NVL(L_tax_rate_retail,0)/100);
         L_extended_retail   := L_extended_retail + L_vat_amount_rtl;
      end if;     
      
      L_extended_retail_src := L_extended_retail/I_qty;
    
      if ((L_stkldgr_vat_incl_retl_ind = 'Y' and L_class_level_vat_ind = 'Y' and L_class_vat_ind = 'Y') or
          (L_stkldgr_vat_incl_retl_ind = 'Y' and L_class_level_vat_ind = 'N') or
          (L_stkldgr_vat_incl_retl_ind = 'N' and L_class_level_vat_ind = 'Y' and L_class_vat_ind = 'N')) then
         
         if L_extended_retail_src = L_source_retail then
            L_markup_markdown := 0;
         elsif L_extended_retail_src < L_source_retail then
            L_markup_markdown := (L_source_retail - L_extended_retail_src) * I_qty;
            L_tran_code       := 18;
         elsif L_extended_retail_src > L_source_retail then
            L_markup_markdown := (L_extended_retail_src - L_source_retail) * I_qty;
            L_tran_code       := 17;
         end if;
      elsif (L_stkldgr_vat_incl_retl_ind = 'Y' and L_class_level_vat_ind = 'Y' and L_class_vat_ind = 'N') then
      
         L_vat_amount_rtl     := L_source_retail * (NVL(L_tax_rate_retail,0)/100);
         L_source_retail      := L_source_retail + L_vat_amount_rtl;
          
         if L_extended_retail_src = L_source_retail then
            L_markup_markdown := 0;
         elsif L_extended_retail_src < L_source_retail then
            L_markup_markdown := (L_source_retail - L_extended_retail_src) * I_qty;
            L_tran_code       := 18;
         elsif L_extended_retail_src > L_source_retail then
            L_markup_markdown := (L_extended_retail_src - L_source_retail) * I_qty;
            L_tran_code       := 17;
         end if;
      elsif ((L_stkldgr_vat_incl_retl_ind = 'N' and L_class_level_vat_ind = 'Y' and L_class_vat_ind = 'Y') or 
             (L_stkldgr_vat_incl_retl_ind = 'N' and L_class_level_vat_ind = 'N')) then
              
         L_source_retail := L_source_retail/(1+(NVL(L_tax_rate_retail,0)/100));
         
         if L_extended_retail_src = L_source_retail then
            L_markup_markdown := 0;
         elsif L_extended_retail_src < L_source_retail then
            L_markup_markdown := (L_source_retail - L_extended_retail_src) * I_qty;
            L_tran_code       := 18;
         elsif L_extended_retail_src > L_source_retail then
            L_markup_markdown := (L_extended_retail_src - L_source_retail) * I_qty;
            L_tran_code       := 17;
         end if;
      end if;--L_stkldgr_vat_incl_retl_ind = 'Y' and L_class_level_vat_ind='Y'

      if L_markup_markdown != 0 then
         -- Since total_cost is in/out, can't simply use NULL.  Instead use
         -- L_unit_cost since it is otherwise not used.
         L_unit_cost      := NULL;
         L_gl_ref_no      := NULL;
         
          open C_GET_SHIPMENT;
         fetch C_GET_SHIPMENT into L_shipment;
         close C_GET_SHIPMENT;
         
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                I_item,
                                                L_dept,
                                                L_class,
                                                L_subclass,
                                                L_from_loc,
                                                L_from_loc_type,
                                                L_tran_date,
                                                L_tran_code,
                                                NULL,                    -- adj_code
                                                I_qty,                   -- units
                                                L_unit_cost,             -- NULL value (total_cost)
                                                L_markup_markdown,       -- total_retail
                                                I_distro_no,             -- ref_no_1
                                                L_shipment,              -- ref_no_2
                                                NULL,                    -- tsf_source_location
                                                NULL,                    -- tsf_source_loc_type
                                                NULL,                    -- old_unit_retail
                                                NULL,                    -- new_unit_retail
                                                NULL,                    -- source_dept
                                                NULL,                    -- source_class
                                                NULL,                    -- source_subclass
                                                L_function,              -- pgm_name
                                                L_gl_ref_no,             -- gl_ref_no
                                                NULL,                    -- pack_ind
                                                NULL,                    -- sellable_ind
                                                NULL,                    -- orderable_ind
                                                NULL,                    -- pack_type
                                                NULL,                    -- vat_rate
                                                NULL,                    -- class_vat_ind
                                                NULL,                    -- ref_pack_no
                                                NULL,                    -- total_cost_excl_elc
                                                NULL) = FALSE then       -- tax_value
            return FALSE;
         end if;
      end if;
      
      if (L_exempt_ind = 'N' and L_tax_rate_retail is not null) then
     
         ---objects for L10N_FIN_REC
         L_l10n_fin_rec.procedure_key          := 'POST_VAT';
         L_l10n_fin_rec.country_id             := NULL;
         L_l10n_fin_rec.source_entity          := 'LOC';
         L_l10n_fin_rec.item                   := I_item;
         L_l10n_fin_rec.dept                   := L_dept;
         L_l10n_fin_rec.class                  := L_class;
         L_l10n_fin_rec.subclass               := L_subclass;
      
         L_tran_loc       := L_from_loc;
         L_tran_loc_type  := L_from_loc_type;
         L_tran_code      := 88;
         L_total_cost     := NULL;
         L_total_retail   := L_tsf_price * I_qty * (NVL(L_tax_rate_retail,0)/100);
         L_ref_no_1       := I_distro_no;
         L_ref_no_2       := L_shipment;
         L_ref_pack_no    := NULL;
         L_gl_ref_no      := L_tax_code_retail;             
         
         L_l10n_fin_rec.source_id  := L_from_loc;         
      
         L_tran_data_rec := TRAN_DATA_REC(I_item,
                                          L_dept,
                                          L_class,
                                          L_subclass,
                                          NULL,
                                          L_tran_loc_type,
                                          L_tran_loc,
                                          L_tran_date,
                                          L_tran_code,
                                          NULL,
                                          I_qty,
                                          L_total_cost,
                                          L_total_retail,
                                          L_ref_no_1,
                                          L_ref_no_2,
                                          L_gl_ref_no,
                                          NULL,
                                          NULL,
                                          L_function,
                                          NULL,
                                          NULL,
                                          NULL,
                                          SYSDATE,
                                          L_ref_pack_no,
                                          NULL);
      
         L_l10n_fin_rec.tran_data   := L_tran_data_rec;

         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec)= FALSE then
            return FALSE;
         end if;
         
         if TAX_SQL.GET_TAX_RATE_INFO (O_error_message,
                                       L_tax_rate_cost,
                                       L_tax_code_cost,
                                       L_exem_ind,
                                       I_item,
                                       L_to_loc_type,
                                       L_to_loc,
                                       L_from_loc_type,
                                       L_from_loc,
                                       'C') = FALSE then
             return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_tsf_price,
                                 L_imp_exp_currency,
                                 L_to_loc_currency,
                                 L_tsf_price_curr,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;

         L_tran_loc        := L_to_loc;
         L_tran_loc_type   := L_to_loc_type;
         L_tran_code       := 87;
         L_total_cost      := L_tsf_price_curr * I_qty * (NVL(L_tax_rate_cost,0)/100);
         L_total_retail    := NULL;
         L_ref_no_1        := I_distro_no;
         L_ref_no_2        := L_shipment;
         L_ref_pack_no     := NULL;
         L_gl_ref_no       := L_tax_code_cost;
             
         L_l10n_fin_rec.source_id  := L_to_loc;      
      
         L_tran_data_rec := TRAN_DATA_REC(I_item,
                                          L_dept,
                                          L_class,
                                          L_subclass,
                                          NULL,
                                          L_tran_loc_type,
                                          L_tran_loc,
                                          L_tran_date,
                                          L_tran_code,
                                          NULL,
                                          I_qty,
                                          L_total_cost,
                                          L_total_retail,
                                          L_ref_no_1,
                                          L_ref_no_2,
                                          L_gl_ref_no,
                                          NULL,
                                          NULL,
                                          L_function,
                                          NULL,
                                          NULL,
                                          NULL,
                                          SYSDATE,
                                          L_ref_pack_no,
                                          NULL);
      
         L_l10n_fin_rec.tran_data   := L_tran_data_rec;

         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec)= FALSE then
            return FALSE;
         end if;
      end if;
      
      L_extended_cost     := I_cost;
      
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_extended_cost,
                              L_to_loc_currency,
                              L_imp_exp_currency,
                              L_extended_cost,
                              'C',
                              NULL,
                              NULL,
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             L_dept,
                                             L_class,
                                             L_subclass,
                                             L_from_loc,
                                             L_from_loc_type,
                                             L_tran_date,
                                             L_tran_code_imp,
                                             NULL,
                                             I_qty,
                                             L_extended_cost,
                                             L_extended_retail,
                                             I_distro_no,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function,
                                             L_to_loc,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_ref_pack_no) = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_TRAN_DATA_SG;
-------------------------------------------------------------------------
FUNCTION BUILD_ADJ_TRAN_DATA_SG (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                                 I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                 I_cost            IN       TRAN_DATA.TOTAL_COST%TYPE,
                                 I_retail          IN       TRAN_DATA.TOTAL_RETAIL%TYPE,
                                 I_qty             IN       TRAN_DATA.UNITS%TYPE,
                                 I_ref_pack_no     IN       TRAN_DATA.REF_PACK_NO%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(61) := 'CREATE_ORD_TSF_SQL.BUILD_ADJ_TRAN_DATA_SG';
   L_function              VARCHAR2(40) := 'REC_COST_ADJ_SQL.ITEM';
   L_item_record           ITEM_MASTER%ROWTYPE;
   ---
   L_tran_code             TRAN_DATA.TRAN_CODE%TYPE;
   ---
   L_tran_date             PERIOD.VDATE%TYPE           := GET_VDATE;
   L_extended_cost         TRAN_DATA.TOTAL_COST%TYPE   := 0.00;
   L_extended_retail       TRAN_DATA.TOTAL_RETAIL%TYPE := 0.00;
   L_source_retail         TRAN_DATA.TOTAL_RETAIL%TYPE;
   ---
   L_tsf_no                TSFDETAIL.TSF_NO%TYPE;
   L_to_loc                TSFHEAD.TO_LOC%TYPE;
   L_to_loc_type           TSFHEAD.TO_LOC_TYPE%TYPE;
   ---
   L_dept                  ITEM_MASTER.DEPT%TYPE;
   L_class                 ITEM_MASTER.CLASS%TYPE;
   L_subclass              ITEM_MASTER.SUBCLASS%TYPE;
   L_import_id             ORDHEAD.IMPORT_ID%TYPE;
   L_import_type           ORDHEAD.IMPORT_TYPE%TYPE;
   ---
   L_to_loc_currency       ORDHEAD.CURRENCY_CODE%TYPE;
   L_imp_exp_currency      ORDHEAD.CURRENCY_CODE%TYPE;
   ---
   L_profit                NUMBER                      := 0;
   L_total_profit          NUMBER                      := 0;
   L_total_expense         NUMBER                      := 0;
   ---
   L_tsf_entity_id_to_loc    TSF_ENTITY.TSF_ENTITY_ID%TYPE;
   L_tsf_entity_id_imp       TSF_ENTITY.TSF_ENTITY_ID%TYPE;
   L_tsf_entity_desc_to_loc  TSF_ENTITY.TSF_ENTITY_DESC%TYPE;
   L_tsf_entity_desc_imp     TSF_ENTITY.TSF_ENTITY_DESC%TYPE;
   L_sob_id_to_loc           MV_LOC_SOB.SET_OF_BOOKS_ID%TYPE;
   L_sob_id_imp              MV_LOC_SOB.SET_OF_BOOKS_ID%TYPE;
   L_sob_desc_to_loc         MV_LOC_SOB.SET_OF_BOOKS_DESC%TYPE;
   L_sob_desc_imp            MV_LOC_SOB.SET_OF_BOOKS_DESC%TYPE;
   L_physical_wh_to_loc      WH.PHYSICAL_WH%TYPE;
   L_physical_wh_imp         WH.PHYSICAL_WH%TYPE;
   L_tran_code_to_loc        TRAN_DATA.TRAN_CODE%TYPE;
   L_tran_code_imp           TRAN_DATA.TRAN_CODE%TYPE;
   L_tsf_source_location     TRAN_DATA.LOCATION%TYPE;
   L_tsf_source_loc_type     TRAN_DATA.LOC_TYPE%TYPE;
   L_from_loc_type           TSFHEAD.FROM_LOC_TYPE%TYPE;
   --------------
   L_profit_chrgs_to_loc   NUMBER                      := 0;
   L_total_chrgs_prim      ITEM_LOC.UNIT_RETAIL%TYPE   := 0;
   L_exp_chrgs_to_loc      NUMBER                      := 0;
   L_tsf_seq_no            TSFDETAIL.TSF_SEQ_NO%TYPE;
   --------------

   cursor C_GET_TSF_SG is
      select th.to_loc,
             th.to_loc_type,
             th.from_loc_type,
             th.tsf_no,
             oh.import_id,
             oh.import_type
        from tsfhead th,
             tsfdetail td,
             ordhead oh
       where th.tsf_no = td.tsf_no
         and th.order_no = oh.order_no
         and th.tsf_type = 'SG'
         and th.order_no = I_order_no
         and td.item = NVL(I_ref_pack_no, I_item);

   cursor C_GET_TSF_SEQ is
         select tsf_seq_no
           from tsfdetail
          where tsf_no = L_tsf_no
            and item = NVL(I_ref_pack_no, I_item);

   cursor C_GET_SOURCE_RETAIL is
      select unit_retail * I_qty
        from item_loc
       where item = I_item
         and loc = L_tsf_source_location
         and loc_type = L_tsf_source_loc_type;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TSF_SG',
                    'TSFHEAD'||'TSFDETAIL'||'ORDHEAD',
                    'Order No: '||to_char(I_order_no)||
                    'Item No: '||I_item);
   open C_GET_TSF_SG;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TSF_SG',
                    'TSFHEAD'||'TSFDETAIL'||'ORDHEAD',
                    'Order No: '||to_char(I_order_no)||
                    'Item No: '||I_item);
   fetch C_GET_TSF_SG into L_to_loc,
                           L_to_loc_type,
                           L_from_loc_type,
                           L_tsf_no,
                           L_import_id,
                           L_import_type;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TSF_SG',
                    'TSFHEAD'||'TSFDETAIL'||'ORDHEAD',
                    'Order No: '||to_char(I_order_no)||
                    'Item No: '||I_item);
   close C_GET_TSF_SG;
   ---
   if L_import_id is NULL or
     (L_import_id = L_to_loc) then
      return TRUE;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_record,
                                      I_item) = FALSE then
      return FALSE;
   end if;
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---
   if L_to_loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_physical_wh_to_loc,
                                       L_to_loc) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if L_from_loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_physical_wh_imp,
                                       L_import_id) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   L_dept     := L_item_record.dept;
   L_class    := L_item_record.class;
   L_subclass := L_item_record.subclass;
   ---
   L_extended_retail   := I_retail;
   L_extended_cost     := I_cost;

   if LP_system_options_row.intercompany_transfer_basis = 'B' then
      if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                      L_sob_desc_to_loc,
                                      L_sob_id_to_loc,
                                      L_to_loc,
                                       L_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                      L_sob_desc_imp,
                                      L_sob_id_imp,
                                      L_import_id,
                                      L_from_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if L_sob_id_to_loc  <> L_sob_id_imp then
         L_tran_code_to_loc := 37;
         L_tran_code_imp    := 38;
      else
         if L_to_loc_type = 'W' and L_physical_wh_to_loc = L_physical_wh_imp then
            L_tran_code_to_loc := 31;
            L_tran_code_imp    := 33;
         else
            L_tran_code_to_loc := 30;
            L_tran_code_imp    := 32;
         end if;
      end if;
   elsif LP_system_options_row.intercompany_transfer_basis = 'T' then
      if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                        L_tsf_entity_id_to_loc,
                                        L_tsf_entity_desc_to_loc,
                                        L_to_loc,
                                        L_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                        L_tsf_entity_id_imp,
                                        L_tsf_entity_desc_imp,
                                        L_import_id,
                                        L_from_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if L_tsf_entity_id_to_loc <> L_tsf_entity_id_imp then
         L_tran_code_to_loc := 37;
         L_tran_code_imp    := 38;
      else
         if L_to_loc_type = 'W' and L_physical_wh_to_loc = L_physical_wh_imp then
            L_tran_code_to_loc := 31;
            L_tran_code_imp    := 33;
         else
            L_tran_code_to_loc := 30;
            L_tran_code_imp    := 32;
         end if;
      end if;
   end if;

   if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                L_to_loc,
                                L_to_loc_type,
                                NULL,
                                L_to_loc_currency)= FALSE then
      return FALSE;
   end if;
   ---
   if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                L_import_id,
                                L_from_loc_type,
                                NULL,
                                L_imp_exp_currency)= FALSE then
      return FALSE;
   end if;
   ---
   if L_tran_code_to_loc in (30, 31) then
      L_tsf_source_location := L_import_id;
      L_tsf_source_loc_type := L_from_loc_type;
      ---
      if L_extended_cost is not NULL then
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_extended_cost,
                                 L_to_loc_currency,
                                 L_imp_exp_currency,
                                 L_extended_cost,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                          I_item,
                                          L_dept,
                                          L_class,
                                          L_subclass,
                                          L_to_loc,
                                          L_to_loc_type,
                                          L_tran_date,
                                          L_tran_code_to_loc,
                                          NULL,
                                          I_qty,
                                          L_extended_cost,
                                          L_extended_retail,
                                          L_tsf_no,
                                          NULL,
                                          L_tsf_source_location,
                                          L_tsf_source_loc_type,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          L_function,
                                          L_import_id) = FALSE then
      return FALSE;
   end if;
   ---
   open C_GET_TSF_SEQ;
   fetch C_GET_TSF_SEQ into L_tsf_seq_no;
   close C_GET_TSF_SEQ;

   if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS( O_error_message,
                                                   L_total_chrgs_prim,
                                                   L_profit_chrgs_to_loc,
                                                   L_exp_chrgs_to_loc,
                                                   'T',
                                                   L_tsf_no,
                                                   L_tsf_seq_no,
                                                   NULL,
                                                   NULL,
                                                   I_item,      --item
                                                   NULL,        --pack no
                                                   L_import_id,
                                                   'W',
                                                   L_to_loc,
                                                   L_to_loc_type) = FALSE then
            return FALSE;
   end if;

   L_total_profit := I_qty * L_profit_chrgs_to_loc;

   if L_total_profit != 0 and
      L_total_profit IS NOT NULL then
      L_tran_code := 28;
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             L_dept,
                                             L_class,
                                             L_subclass,
                                             L_to_loc,
                                             L_to_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,
                                             I_qty,
                                             L_total_profit,
                                             0,
                                             NULL,
                                             L_tsf_no,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   L_total_expense := I_qty * L_exp_chrgs_to_loc;

   if L_total_expense != 0 and
      L_total_expense IS NOT NULL then
      L_tran_code := 29;
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             L_dept,
                                             L_class,
                                             L_subclass,
                                             L_to_loc,
                                             L_to_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,
                                             I_qty,
                                             L_total_expense,
                                             0,
                                             NULL,
                                             L_tsf_no,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if L_tran_code_to_loc = 37 then
      if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_extended_cost,
                                 L_to_loc_currency,
                                 L_imp_exp_currency,
                                 L_extended_cost,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
      end if;
      ---
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             L_dept,
                                             L_class,
                                             L_subclass,
                                             L_import_id,
                                             L_from_loc_type,
                                             L_tran_date,
                                             L_tran_code_imp,
                                             NULL,
                                             I_qty,
                                             L_extended_cost,
                                             L_extended_cost,
                                             L_tsf_no,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function,
                                             L_to_loc) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT (O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_ADJ_TRAN_DATA_SG;
-------------------------------------------------------------------------
FUNCTION UPDATE_SG_TSF_DETAIL (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no       IN      TSFHEAD.ORDER_NO%TYPE,
                               I_location       IN      TSFHEAD.TO_LOC%TYPE,
                               I_loc_type       IN      TSFHEAD.TO_LOC_TYPE%TYPE,
                               I_item           IN      TSFDETAIL.ITEM%TYPE,
                               I_new_cost       IN      TSFDETAIL.TSF_PRICE%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(61) := 'CREATE_ORD_TSF_SQL.UPDATE_SG_TSF_DETAIL';
   L_tsf_price   TSFDETAIL.TSF_PRICE%TYPE :=   0;
   L_import_id   ORDHEAD.IMPORT_ID%TYPE   :=   NULL;
   L_loc_type    TSFHEAD.FROM_LOC_TYPE%TYPE;

   cursor C_GET_SG_DETAILS is
      select import_id
        from ordhead
       where order_no = I_order_no;

BEGIN
   ---
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_GET_SG_DETAILS;
   fetch C_GET_SG_DETAILS into L_import_id;
   close C_GET_SG_DETAILS;

   if L_import_id is NULL or
     (L_import_id = I_location) then
      return TRUE;
   end if;

   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_loc_type,
                                   L_import_id) = FALSE then
      return FALSE;
   end if;

   if L_loc_type in ('M','X') then
      L_loc_type := 'W';
   end if;

   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       I_location,
                                       I_loc_type,
                                       NULL,
                                       L_import_id,
                                       L_loc_type,
                                       NULL,
                                       I_new_cost,
                                       L_tsf_price,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
      return FALSE;
   end if;

   update tsfdetail set tsf_price = L_tsf_price
      where item = I_item
        and tsf_seq_no in (select tsf_seq_no
                             from tsfhead
                            where order_no = I_order_no
                              and to_loc = I_location);
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_SG_TSF_DETAIL;
-------------------------------------------------------------------------
END CREATE_ORD_TSF_SQL;
/