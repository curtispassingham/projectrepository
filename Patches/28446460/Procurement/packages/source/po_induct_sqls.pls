create or replace PACKAGE PO_INDUCT_SQL AUTHID CURRENT_USER AS
   TEMPLATE_KEY              VARCHAR2(255)        := 'PURCHASE_ORDER_DATA';
   FILE_NAME                 VARCHAR2(255);
   ADD_ERROR_4_PROCESS       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE;
   ACTION_NEW                CONSTANT VARCHAR2(3) := 'NEW';
   ACTION_MOD                CONSTANT VARCHAR2(3) := 'MOD';
   ACTION_DEL                CONSTANT VARCHAR2(3) := 'DEL';

   OHA_sheet                 VARCHAR2(255)        := 'ORDSKU_HTS_ASSESS';
   OHA$action                NUMBER               := 1;
   OHA$ORDER_NO              NUMBER               := 2;
   OHA$ITEM                  NUMBER               := 3;
   OHA$PACK_ITEM             NUMBER               := 4;
   OHA$HTS                   NUMBER               := 5;
   OHA$SEQ_NO                NUMBER               := 6;
   OHA$COMP_ID               NUMBER               := 7;
   OHA$CVB_CODE              NUMBER               := 8;
   OHA$COMP_RATE             NUMBER               := 9;
   OHA$PER_COUNT             NUMBER               := 10;
   OHA$PER_COUNT_UOM         NUMBER               := 11;
   OHA$EST_ASSESS_VALUE      NUMBER               := 12;
   OHA$NOM_FLAG_1            NUMBER               := 13;
   OHA$NOM_FLAG_2            NUMBER               := 14;
   OHA$NOM_FLAG_3            NUMBER               := 15;
   OHA$NOM_FLAG_4            NUMBER               := 16;
   OHA$NOM_FLAG_5            NUMBER               := 17;
   OHA$DISPLAY_ORDER         NUMBER               := 18;

   ORH_sheet                 VARCHAR2(255)        := 'ORDSKU_HTS';
   ORH$action                NUMBER               := 1;
   ORH$ORDER_NO              NUMBER               := 2;
   ORH$SEQ_NO                NUMBER               := 3;
   ORH$ITEM                  NUMBER               := 4;
   ORH$PACK_ITEM             NUMBER               := 5;
   ORH$HTS                   NUMBER               := 6;
   ORH$IMPORT_COUNTRY_ID     NUMBER               := 7;
   ORH$EFFECT_FROM           NUMBER               := 8;
   ORH$EFFECT_TO             NUMBER               := 9;
   ORH$STATUS                NUMBER               := 10;
   ORH$ORIGIN_COUNTRY_ID     NUMBER               := 11;

   ORE_sheet                 VARCHAR2(255)        := 'ORDLOC_EXP';
   ORE$action                NUMBER               := 1;
   ORE$ORDER_NO              NUMBER               := 2;
   ORE$SEQ_NO                NUMBER               := 3;
   ORE$ITEM                  NUMBER               := 4;
   ORE$PACK_ITEM             NUMBER               := 5;
   ORE$LOCATION              NUMBER               := 6;
   ORE$LOC_TYPE              NUMBER               := 7;
   ORE$COMP_ID               NUMBER               := 8;
   ORE$CVB_CODE              NUMBER               := 9;
   ORE$COST_BASIS            NUMBER               := 10;
   ORE$COMP_RATE             NUMBER               := 11;
   ORE$COMP_CURRENCY         NUMBER               := 12;
   ORE$EXCHANGE_RATE         NUMBER               := 13;
   ORE$PER_COUNT             NUMBER               := 14;
   ORE$PER_COUNT_UOM         NUMBER               := 15;
   ORE$EST_EXP_VALUE         NUMBER               := 16;
   ORE$NOM_FLAG_1            NUMBER               := 17;
   ORE$NOM_FLAG_2            NUMBER               := 18;
   ORE$NOM_FLAG_3            NUMBER               := 19;
   ORE$NOM_FLAG_4            NUMBER               := 20;
   ORE$NOM_FLAG_5            NUMBER               := 21;
   ORE$ORIGIN                NUMBER               := 22;
   ORE$DISPLAY_ORDER         NUMBER               := 23;
   ORE$DEFAULTED_FROM        NUMBER               := 24;
   ORE$KEY_VALUE_1           NUMBER               := 25;
   ORE$KEY_VALUE_2           NUMBER               := 26;

   ORD_sheet                 VARCHAR2(255)        := 'ORDLC';
   ORD$action                NUMBER               := 1;
   ORD$ORDER_NO              NUMBER               := 2;
   ORD$LC_REF_ID             NUMBER               := 3;
   ORD$LC_GROUP_ID           NUMBER               := 4;
   ORD$APPLICANT             NUMBER               := 5;
   ORD$BENEFICIARY           NUMBER               := 6;
   ORD$MERCH_DESC            NUMBER               := 7;
   ORD$TRANSSHIPMENT_IND     NUMBER               := 8;
   ORD$PARTIAL_SHIPMENT_IND  NUMBER               := 9;
   ORD$LC_IND                NUMBER               := 10;

   OHE_sheet                 VARCHAR2(255)        := 'ORDHEAD';
   OHE$action                NUMBER               := 1;
   OHE$ORDER_NO              NUMBER               := 2;
   OHE$ORDER_TYPE            NUMBER               := 3;
   OHE$DEPT                  NUMBER               := 4;
   OHE$BUYER                 NUMBER               := 5;
   OHE$SUPPLIER              NUMBER               := 6;
   OHE$SUPP_ADD_SEQ_NO       NUMBER               := 7;
   OHE$LOC_TYPE              NUMBER               := 8;
   OHE$LOCATION              NUMBER               := 9;
   OHE$PROMOTION             NUMBER               := 10;
   OHE$QC_IND                NUMBER               := 11;
   OHE$WRITTEN_DATE          NUMBER               := 12;
   OHE$NOT_BEFORE_DATE       NUMBER               := 13;
   OHE$NOT_AFTER_DATE        NUMBER               := 14;
   OHE$OTB_EOW_DATE          NUMBER               := 15;
   OHE$EARLIEST_SHIP_DATE    NUMBER               := 16;
   OHE$LATEST_SHIP_DATE      NUMBER               := 17;
   OHE$CLOSE_DATE            NUMBER               := 18;
   OHE$TERMS                 NUMBER               := 19;
   OHE$FREIGHT_TERMS         NUMBER               := 20;
   OHE$ORIG_IND              NUMBER               := 21;
   OHE$PAYMENT_METHOD        NUMBER               := 22;
   OHE$BACKHAUL_TYPE         NUMBER               := 23;
   OHE$BACKHAUL_ALLOWANCE    NUMBER               := 24;
   OHE$SHIP_METHOD           NUMBER               := 25;
   OHE$PURCHASE_TYPE         NUMBER               := 26;
   OHE$STATUS                NUMBER               := 27;
   OHE$ORIG_APPROVAL_DATE    NUMBER               := 28;
   OHE$ORIG_APPROVAL_ID      NUMBER               := 29;
   OHE$SHIP_PAY_METHOD       NUMBER               := 30;
   OHE$FOB_TRANS_RES         NUMBER               := 31;
   OHE$FOB_TRANS_RES_DESC    NUMBER               := 32;
   OHE$FOB_TITLE_PASS        NUMBER               := 33;
   OHE$FOB_TITLE_PASS_DESC   NUMBER               := 34;
   OHE$EDI_SENT_IND          NUMBER               := 35;
   OHE$EDI_PO_IND            NUMBER               := 36;
   OHE$IMPORT_ORDER_IND      NUMBER               := 37;
   OHE$IMPORT_COUNTRY_ID     NUMBER               := 38;
   OHE$PO_ACK_RECVD_IND      NUMBER               := 39;
   OHE$INCLUDE_ON_ORDER_IND  NUMBER               := 40;
   OHE$VENDOR_ORDER_NO       NUMBER               := 41;
   OHE$EXCHANGE_RATE         NUMBER               := 42;
   OHE$FACTORY               NUMBER               := 43;
   OHE$AGENT                 NUMBER               := 44;
   OHE$DISCHARGE_PORT        NUMBER               := 45;
   OHE$LADING_PORT           NUMBER               := 46;
   OHE$FREIGHT_CONTRACT_NO   NUMBER               := 47;
   OHE$PO_TYPE               NUMBER               := 48;
   OHE$PRE_MARK_IND          NUMBER               := 49;
   OHE$CURRENCY_CODE         NUMBER               := 50;
   OHE$REJECT_CODE           NUMBER               := 51;
   OHE$CONTRACT_NO           NUMBER               := 52;
   OHE$LAST_SENT_REV_NO      NUMBER               := 53;
   OHE$SPLIT_REF_ORDNO       NUMBER               := 54;
   OHE$PICKUP_LOC            NUMBER               := 55;
   OHE$PICKUP_NO             NUMBER               := 56;
   OHE$PICKUP_DATE           NUMBER               := 57;
   OHE$APP_DATETIME          NUMBER               := 58;
   OHE$COMMENT_DESC          NUMBER               := 59;
   OHE$PARTNER_TYPE_1        NUMBER               := 60;
   OHE$PARTNER1              NUMBER               := 61;
   OHE$PARTNER_TYPE_2        NUMBER               := 62;
   OHE$PARTNER2              NUMBER               := 63;
   OHE$PARTNER_TYPE_3        NUMBER               := 64;
   OHE$PARTNER3              NUMBER               := 65;
   OHE$ITEM                  NUMBER               := 66;
   OHE$IMPORT_TYPE           NUMBER               := 67;
   OHE$IMPORT_ID             NUMBER               := 68;
   OHE$CLEARING_ZONE_ID      NUMBER               := 69;
   OHE$ROUTING_LOC_ID        NUMBER               := 70;
   OHE$EXT_REF_NO            NUMBER               := 71;
   OHE$MASTER_PO_NO          NUMBER               := 72;
   OHE$RE_APPROVE_IND        NUMBER               := 73;
   OHE$LAST_UPD_ID           NUMBER               := 74;
   OHE$NEXT_UPD_ID           NUMBER               := 75;
   OHE$LAST_UPD_DATETIME     NUMBER               := 76;

   OHE_CFA_sheet             VARCHAR2(255)        := 'ORDHEAD_CFA_EXT';
   OHE_CFA$action            NUMBER               := 1;
   OHE_CFA$ORDER_NO          NUMBER               := 2;
   OHE_CFA$GROUP_ID          NUMBER               := 3;
   OHE_CFA$VARCHAR2_1        NUMBER               := 4;
   OHE_CFA$VARCHAR2_2        NUMBER               := 5;
   OHE_CFA$VARCHAR2_3        NUMBER               := 6;
   OHE_CFA$VARCHAR2_4        NUMBER               := 7;
   OHE_CFA$VARCHAR2_5        NUMBER               := 8;
   OHE_CFA$VARCHAR2_6        NUMBER               := 9;
   OHE_CFA$VARCHAR2_7        NUMBER               := 10;
   OHE_CFA$VARCHAR2_8        NUMBER               := 11;
   OHE_CFA$VARCHAR2_9        NUMBER               := 12;
   OHE_CFA$VARCHAR2_10       NUMBER               := 13;
   OHE_CFA$NUMBER_11         NUMBER               := 14;
   OHE_CFA$NUMBER_12         NUMBER               := 15;
   OHE_CFA$NUMBER_13         NUMBER               := 16;
   OHE_CFA$NUMBER_14         NUMBER               := 17;
   OHE_CFA$NUMBER_15         NUMBER               := 18;
   OHE_CFA$NUMBER_16         NUMBER               := 19;
   OHE_CFA$NUMBER_17         NUMBER               := 20;
   OHE_CFA$NUMBER_18         NUMBER               := 21;
   OHE_CFA$NUMBER_19         NUMBER               := 22;
   OHE_CFA$NUMBER_20         NUMBER               := 23;
   OHE_CFA$DATE_21           NUMBER               := 24;
   OHE_CFA$DATE_22           NUMBER               := 25;
   OHE_CFA$DATE_23           NUMBER               := 26;
   OHE_CFA$DATE_24           NUMBER               := 27;
   OHE_CFA$DATE_25           NUMBER               := 28;

   ODT_sheet                 VARCHAR2(255)        := 'ORDDETAIL';
   ODT$action                NUMBER               := 1;
   ODT$ORDER_NO              NUMBER               := 2;
   ODT$ITEM                  NUMBER               := 3;
   ODT$ITEM_DESC             NUMBER               := 4;
   ODT$VPN                   NUMBER               := 5;
   ODT$ITEM_PARENT           NUMBER               := 6;
   ODT$REF_ITEM              NUMBER               := 7;
   ODT$DIFF_1                NUMBER               := 8;
   ODT$DIFF_2                NUMBER               := 9;
   ODT$DIFF_3                NUMBER               := 10;
   ODT$DIFF_4                NUMBER               := 11;
   ODT$ORIGIN_COUNTRY_ID     NUMBER               := 12;
   ODT$SUPP_PACK_SIZE        NUMBER               := 13;
   ODT$LOC_TYPE              NUMBER               := 14;
   ODT$LOCATION              NUMBER               := 15;
   ODT$LOCATION_DESC         NUMBER               := 16;
   ODT$UNIT_COST             NUMBER               := 17;
   ODT$QTY_ORDERED           NUMBER               := 18;
   ODT$DELIVERY_DATE         NUMBER               := 19;
   ODT$NON_SCALE_IND         NUMBER               := 20;
   ODT$PROCESSING_TYPE       NUMBER               := 21;
   ODT$PROCESSING_WH         NUMBER               := 22;
   ODT$ALLOC_NO              NUMBER               := 23;
   ODT$QTY_CANCELLED         NUMBER               := 24;
   ODT$QTY_TRANSFERRED       NUMBER               := 25;
   ODT$CANCEL_CODE           NUMBER               := 26;
   ODT$RELEASE_DATE          NUMBER               := 27;
   ODT$IN_STORE_DATE         NUMBER               := 28;

---------------------------------------------------------------------------------------------------
FUNCTION EXEC_ASYNC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_rms_async_id    IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                    I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CREATE_FILE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id         OUT      SVC_PROCESS_TRACKER.FILE_ID%TYPE,
                     I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                     I_action          IN       VARCHAR2,
                     I_source          IN       VARCHAR2,
                     I_template        IN       VARCHAR2,
                     I_file_path       IN       VARCHAR2,
                     I_err_4_process   IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_process_id          IN       NUMBER,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CREATE_SVC_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_file_id         IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_SEARCH_POS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION POPULATE_LISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_file_id         IN       NUMBER,
                        I_template_key    IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION ADD_ERROR_SHEET(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                         I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION SEARCH_POS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_order_count     IN OUT   NUMBER,
                    I_sch_crit_rec    IN       PO_INDUCT_SCH_CRIT_TYP,
                    I_source          IN       VARCHAR2,
                    I_process_id      IN       NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION INIT_PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                      I_process_desc    IN       SVC_PROCESS_TRACKER.PROCESS_DESC%TYPE,
                      I_template_key    IN       SVC_PROCESS_TRACKER.TEMPLATE_KEY%TYPE,
                      I_action_type     IN       SVC_PROCESS_TRACKER.ACTION_TYPE%TYPE,
                      I_source          IN       SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
                      I_destination     IN       SVC_PROCESS_TRACKER.PROCESS_DESTINATION%TYPE,
                      I_rms_async_id    IN       SVC_PROCESS_TRACKER.RMS_ASYNC_ID%TYPE,
                      I_file_id         IN       SVC_PROCESS_TRACKER.FILE_ID%TYPE,
                      I_file_path       IN       SVC_PROCESS_TRACKER.FILE_PATH%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER,
                     O_error_count     OUT      NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION PREPARE_UPLD_FROM_STG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_CONFIG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_rec             IN OUT   PO_INDUCT_CONFIG%ROWTYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION NEXT_UPDATE_ID_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists           IN OUT   BOOLEAN,
                               I_next_update_id   IN       SVC_ORDHEAD.NEXT_UPD_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
FUNCTION LAST_UPDATE_ID_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists           IN OUT   BOOLEAN,
                               I_last_update_id   IN       SVC_ORDHEAD.LAST_UPD_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
FUNCTION VENDOR_NO_EXISTS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists            IN OUT   BOOLEAN,
                          I_vendor_order_no   IN       ORDHEAD.VENDOR_ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
FUNCTION DELETE_PROCESSES(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id_tab   IN       NUM_TAB)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CASCADE_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
FUNCTION DELETE_PROCESSES_WRP(
      O_error_message  IN OUT rtk_errors.rtk_text%type,
      I_process_id_tab IN num_tab)
RETURN NUMBER;
--------------------------------------------------------------------------------------  
FUNCTION SEARCH_POS_WRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_order_count     IN OUT   NUMBER,
                    I_sch_crit_rec    IN       PO_INDUCT_SCH_CRIT_TYP,
                    I_source          IN       VARCHAR2,
                    I_process_id      IN       NUMBER)
RETURN NUMBER;
-------------------------------------------------------------------------------------- 
END PO_INDUCT_SQL;
/