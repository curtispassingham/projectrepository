CREATE OR REPLACE PROCEDURE NOTIFY_COST_EVENT_THREAD(context raw,
                                                     reginfo sys.aq$_reg_info,
                                                     descr sys.aq$_descriptor,
                                                     payload raw,
                                                     payloadl number) AUTHID CURRENT_USER
AS
   L_dequeue_options dbms_aq.dequeue_options_t;
   L_message_properties dbms_aq.message_properties_t;
   L_message_handle RAW(16);
   L_message cost_event_thread_msg;
  
BEGIN
   
   L_dequeue_options.msgid := descr.msg_id;
   L_dequeue_options.consumer_name := descr.consumer_name;
   L_dequeue_options.visibility := DBMS_AQ.IMMEDIATE; 
   L_dequeue_options.wait := DBMS_AQ.NO_WAIT;
   
   DBMS_AQ.DEQUEUE(QUEUE_NAME => descr.queue_name,
                   DEQUEUE_OPTIONS => L_dequeue_options,
                   MESSAGE_PROPERTIES => L_message_properties,
                   PAYLOAD => L_message,
                   MSGID => L_message_handle);

   FUTURE_COST_SQL.PROCESS_COST_EVENTS(L_message.cost_event_process_id,L_message.thread_id); 
 
END;
/