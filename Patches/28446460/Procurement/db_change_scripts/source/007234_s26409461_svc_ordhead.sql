--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_ORDHEAD'
ALTER TABLE SVC_ORDHEAD MODIFY TERMS VARCHAR (50 )
/

COMMENT ON COLUMN SVC_ORDHEAD.TERMS is 'Indicator identifying the sales terms for the order. These terms specify when payment is due and if any discounts exist for early payment.'
/

