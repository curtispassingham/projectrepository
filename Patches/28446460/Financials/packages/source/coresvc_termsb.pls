CREATE OR REPLACE PACKAGE BODY CORESVC_TERMS AS
   LP_primary_lang    LANG.LANG%TYPE :=  LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;

   cursor C_SVC_FREIGHT_TERMS(I_process_id NUMBER,
                              I_chunk_id NUMBER) is
      select pk_freight_terms.rowid  AS pk_freight_terms_rid,
             st.rowid AS st_rid,
             st.enabled_flag,
             st.end_date_active,
             st.start_date_active,
             st.freight_terms,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status,
             st.term_desc
        from svc_freight_terms st,
             freight_terms pk_freight_terms
       where st.process_id      = I_process_id
         and st.chunk_id        = I_chunk_id
         and st.freight_terms   = pk_freight_terms.freight_terms (+);

   cursor C_SVC_TERMS_HEAD(I_process_id NUMBER,
                           I_chunk_id   NUMBER) is
      select
             pk_terms_head.rowid  AS pk_terms_head_rid,
             st.rowid AS st_rid,
             st.rank,
             st.terms,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status,
             st.terms_code,
             st.terms_desc,
             (select count(1)
                from svc_terms_detail svd
               where svd.terms = st.terms
                 and svd.action != action_del) cnt_details,
             (select count(1)
                from svc_terms_detail svd
               where svd.terms = st.terms
                 and svd.action != action_new) cnt_details1
        from svc_terms_head st,
             terms_head pk_terms_head
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.terms      = pk_terms_head.terms (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;

   LP_errors_tab errors_tab_typ;

   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;

   LP_s9t_errors_tab s9t_errors_tab_typ;

   TYPE FREIGHT_TERMS_TL_tab IS TABLE OF FREIGHT_TERMS_TL%ROWTYPE;

   TYPE TERMS_HEAD_TL_tab is TABLE OF TERMS_HEAD_TL%ROWTYPE;

   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

---------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE,
                       I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   LP_errors_tab(LP_errors_tab.COUNT()).error_type  := I_error_type;
END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets                s9t_pkg.names_map_typ;
   TERMS_HEAD_cols         s9t_pkg.names_map_typ;
   FREIGHT_TERMS_cols      s9t_pkg.names_map_typ;
   FREIGHT_TERMS_TL_cols   s9t_pkg.names_map_typ;  
   TERMS_DETAIL_cols       s9t_pkg.names_map_typ; 
   TERMS_HEAD_TL_cols      s9t_pkg.names_map_typ;
BEGIN
   L_sheets                         := s9t_pkg.get_sheet_names(I_file_id);
   
   FREIGHT_TERMS_cols               := s9t_pkg.get_col_names(I_file_id,
                                                             FREIGHT_TERMS_sheet);
   FREIGHT_TERMS$Action             := FREIGHT_TERMS_cols('ACTION');
   FREIGHT_TERMS$ENABLED_FLAG       := FREIGHT_TERMS_cols('ENABLED_FLAG');
   FREIGHT_TERMS$END_DATE_ACTIVE    := FREIGHT_TERMS_cols('END_DATE_ACTIVE');
   FREIGHT_TERMS$START_DT_ACTIVE    := FREIGHT_TERMS_cols('START_DATE_ACTIVE');
   FREIGHT_TERMS$FREIGHT_TERMS      := FREIGHT_TERMS_cols('FREIGHT_TERMS');
   FREIGHT_TERMS$TERM_DESC          := FREIGHT_TERMS_cols('TERM_DESC');
   
   FREIGHT_TERMS_TL_cols            := s9t_pkg.get_col_names(I_file_id,
                                                             FREIGHT_TERMS_TL_sheet);
   FREIGHT_TERMS_TL$Action          := FREIGHT_TERMS_TL_cols('ACTION');
   FREIGHT_TERMS_TL$TERM_DESC       := FREIGHT_TERMS_TL_cols('TERM_DESC');
   FREIGHT_TERMS_TL$LANG            := FREIGHT_TERMS_TL_cols('LANG');
   FREIGHT_TERMS_TL$FREIGHT_TERMS   := FREIGHT_TERMS_TL_cols('FREIGHT_TERMS');
   
   TERMS_HEAD_cols                  :=s9t_pkg.get_col_names(I_file_id,
                                                            TERMS_HEAD_sheet);
   TERMS_HEAD$Action                := TERMS_HEAD_cols('ACTION');
   TERMS_HEAD$RANK                  := TERMS_HEAD_cols('RANK');
   TERMS_HEAD$TERMS                 := TERMS_HEAD_cols('TERMS');
   TERMS_HEAD$TERMS_CODE            := TERMS_HEAD_cols('TERMS_CODE');
   TERMS_HEAD$TERMS_DESC            := TERMS_HEAD_cols('TERMS_DESC');
   
   TERMS_DETAIL_cols                :=s9t_pkg.get_col_names(I_file_id,
                                                            TERMS_DETAIL_sheet);
   TERMS_DETAIL$Action              := TERMS_DETAIL_cols('ACTION');
   TERMS_DETAIL$CUTOFF_DAY          := TERMS_DETAIL_cols('CUTOFF_DAY');
   TERMS_DETAIL$END_DATE_ACTIVE     := TERMS_DETAIL_cols('END_DATE_ACTIVE');
   TERMS_DETAIL$START_DATE_ACTIVE   := TERMS_DETAIL_cols('START_DATE_ACTIVE');
   TERMS_DETAIL$ENABLED_FLAG        := TERMS_DETAIL_cols('ENABLED_FLAG');
   TERMS_DETAIL$FIXED_DATE          := TERMS_DETAIL_cols('FIXED_DATE');
   TERMS_DETAIL$DISC_MM_FWD         := TERMS_DETAIL_cols('DISC_MM_FWD');
   TERMS_DETAIL$DISC_DOM            := TERMS_DETAIL_cols('DISC_DOM');
   TERMS_DETAIL$PERCENT             := TERMS_DETAIL_cols('PERCENT');
   TERMS_DETAIL$DISCDAYS            := TERMS_DETAIL_cols('DISCDAYS');
   TERMS_DETAIL$DUE_MM_FWD          := TERMS_DETAIL_cols('DUE_MM_FWD');
   TERMS_DETAIL$DUE_DOM             := TERMS_DETAIL_cols('DUE_DOM');
   TERMS_DETAIL$DUE_MAX_AMOUNT      := TERMS_DETAIL_cols('DUE_MAX_AMOUNT');
   TERMS_DETAIL$DUEDAYS             := TERMS_DETAIL_cols('DUEDAYS');
   TERMS_DETAIL$TERMS_SEQ           := TERMS_DETAIL_cols('TERMS_SEQ');
   TERMS_DETAIL$TERMS               := TERMS_DETAIL_cols('TERMS');
   
   TERMS_HEAD_TL_cols               := s9t_pkg.get_col_names(I_file_id,
                                                             TERMS_HEAD_TL_sheet);
   TERMS_HEAD_TL$Action             := TERMS_HEAD_TL_cols('ACTION');
   TERMS_HEAD_TL$TERMS_DESC         := TERMS_HEAD_TL_cols('TERMS_DESC');
   TERMS_HEAD_TL$TERMS_CODE         := TERMS_HEAD_TL_cols('TERMS_CODE');
   TERMS_HEAD_TL$LANG               := TERMS_HEAD_TL_cols('LANG');
   TERMS_HEAD_TL$TERMS              := TERMS_HEAD_TL_cols('TERMS');
   
   
END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_FREIGHT_TERMS( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = FREIGHT_TERMS_sheet )
   select s9t_row(s9t_cells(CORESVC_TERMS.action_mod ,
                            t.freight_terms,
                            tl.term_desc,
                            t.start_date_active,
                            t.end_date_active,
                            t.enabled_flag
                           ))
     from freight_terms t, freight_terms_tl tl
    where t.freight_terms = tl.freight_terms
      and tl.lang = LP_primary_lang;
END POPULATE_FREIGHT_TERMS;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_FREIGHT_TERMS_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = FREIGHT_TERMS_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_TERMS.action_mod ,
                            freight_terms,
                            lang,
                            term_desc
                           ))
     from freight_terms_tl
    where lang != LP_primary_lang;
END POPULATE_FREIGHT_TERMS_TL;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_TERMS_HEAD( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = TERMS_HEAD_sheet )
   select s9t_row(s9t_cells(CORESVC_TERMS.action_mod ,
                           t.terms,
                           t.rank,
                           tl.terms_code,
                           tl.terms_desc
                           ))
     from terms_head t, terms_head_tl tl
    where t.terms = tl.terms
      and tl.lang = LP_primary_lang;
END POPULATE_TERMS_HEAD;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_TERMS_DETAIL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = TERMS_DETAIL_sheet )
   select s9t_row(s9t_cells(CORESVC_TERMS.action_mod ,
                            terms,
                            terms_seq,
                            duedays,
                            due_max_amount,
                            due_dom,
                            due_mm_fwd,
                            discdays,
                            percent,
                            disc_dom,
                            disc_mm_fwd,
                            fixed_date,
                            enabled_flag,
                            start_date_active,
                            end_date_active,
                            cutoff_day
                           ))
     from terms_detail ;
END POPULATE_TERMS_DETAIL;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_TERMS_HEAD_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = TERMS_HEAD_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_TERMS.action_mod ,
                           terms,
                           lang,
                           terms_code,
                           terms_desc
                           ))
     from terms_head_tl
    where lang != LP_primary_lang;
END POPULATE_TERMS_HEAD_TL;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   
   L_file.add_sheet(FREIGHT_TERMS_sheet);
   L_file.sheets(l_file.get_sheet_index(FREIGHT_TERMS_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                           'FREIGHT_TERMS',
                                                                                           'TERM_DESC',
                                                                                           'START_DATE_ACTIVE',
                                                                                           'END_DATE_ACTIVE',
                                                                                           'ENABLED_FLAG'); 

   L_file.add_sheet(FREIGHT_TERMS_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(FREIGHT_TERMS_TL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                              'FREIGHT_TERMS',
                                                                                              'LANG',
                                                                                              'TERM_DESC');

   L_file.add_sheet(TERMS_HEAD_sheet);
   L_file.sheets(l_file.get_sheet_index(TERMS_HEAD_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                        'TERMS',
                                                                                        'RANK',
                                                                                        'TERMS_CODE',
                                                                                        'TERMS_DESC');

   L_file.add_sheet(TERMS_DETAIL_sheet);
   L_file.sheets(l_file.get_sheet_index(TERMS_DETAIL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                          'TERMS',
                                                                                          'TERMS_SEQ',
                                                                                          'DUEDAYS',
                                                                                          'DUE_MAX_AMOUNT',
                                                                                          'DUE_DOM',
                                                                                          'DUE_MM_FWD',
                                                                                          'DISCDAYS',
                                                                                          'PERCENT',
                                                                                          'DISC_DOM',
                                                                                          'DISC_MM_FWD',
                                                                                          'FIXED_DATE',
                                                                                          'ENABLED_FLAG',
                                                                                          'START_DATE_ACTIVE',
                                                                                          'END_DATE_ACTIVE',
                                                                                          'CUTOFF_DAY');

   L_file.add_sheet(TERMS_HEAD_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(TERMS_HEAD_TL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                           'TERMS',
                                                                                           'LANG',
                                                                                           'TERMS_CODE',
                                                                                           'TERMS_DESC');
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file      s9t_file;
   L_program   VARCHAR2(64):='CORESVC_TERMS.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   
   if I_template_only_ind = 'N' then
      POPULATE_FREIGHT_TERMS(O_file_id);
      POPULATE_FREIGHT_TERMS_TL(O_file_id);
      POPULATE_TERMS_HEAD(O_file_id);
      POPULATE_TERMS_HEAD_TL(O_file_id);
      POPULATE_TERMS_DETAIL(O_file_id);
      COMMIT;
   end if;
   
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if; 	
   
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_FREIGHT_TERMS( I_file_id    IN   s9t_folder.file_id%TYPE,
                                     I_process_id IN   SVC_FREIGHT_TERMS.process_id%TYPE) IS

   TYPE svc_FREIGHT_TERMS_col_typ IS TABLE OF SVC_FREIGHT_TERMS%ROWTYPE;
   L_temp_rec              SVC_FREIGHT_TERMS%ROWTYPE;
   svc_FREIGHT_TERMS_col   svc_FREIGHT_TERMS_col_typ :=NEW svc_FREIGHT_TERMS_col_typ();
   L_process_id            SVC_FREIGHT_TERMS.process_id%TYPE;
   L_error                 BOOLEAN:=FALSE;
   L_default_rec           SVC_FREIGHT_TERMS%ROWTYPE;

   cursor C_MANDATORY_IND is
      select
             ENABLED_FLAG_mi,
             END_DATE_ACTIVE_mi,
             START_DATE_ACTIVE_mi,
             FREIGHT_TERMS_mi,
             TERM_DESC_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'FREIGHT_TERMS'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'ENABLED_FLAG' AS ENABLED_FLAG,
                                         'END_DATE_ACTIVE' AS END_DATE_ACTIVE,
                                         'START_DATE_ACTIVE' AS START_DATE_ACTIVE,
                                         'FREIGHT_TERMS' AS FREIGHT_TERMS,
                                         'TERM_DESC' AS TERM_DESC, 
                                         null as dummy));

      l_mi_rec     c_mandatory_ind%ROWTYPE;
      dml_errors   EXCEPTION;

      PRAGMA exception_init(dml_errors, -24381);

      L_pk_columns    VARCHAR2(255)  := 'Freight Terms';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  -- Get default values.
   FOR rec IN (select
                       ENABLED_FLAG_dv,
                       END_DATE_ACTIVE_dv,
                       START_DATE_ACTIVE_dv,
                       FREIGHT_TERMS_dv,
                       TERM_DESC_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'FREIGHT_TERMS'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'ENABLED_FLAG' AS ENABLED_FLAG,
                                                      'END_DATE_ACTIVE' AS END_DATE_ACTIVE,
                                                      'START_DATE_ACTIVE' AS START_DATE_ACTIVE,
                                                      'FREIGHT_TERMS' AS FREIGHT_TERMS,
                                                      'TERM_DESC' AS TERM_DESC,
                                                      NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.ENABLED_FLAG := rec.ENABLED_FLAG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'FREIGHT_TERMS ' ,
                            NULL,
                            'ENABLED_FLAG ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.END_DATE_ACTIVE := rec.END_DATE_ACTIVE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'FREIGHT_TERMS ' ,
                            NULL,
                            'END_DATE_ACTIVE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.START_DATE_ACTIVE := rec.START_DATE_ACTIVE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'FREIGHT_TERMS ' ,
                            NULL,
                            'START_DATE_ACTIVE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FREIGHT_TERMS := rec.FREIGHT_TERMS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'FREIGHT_TERMS ' ,
                            NULL,
                            'FREIGHT_TERMS ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TERM_DESC := rec.TERM_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'FREIGHT_TERMS ' ,
                            NULL,
                            'TERM_DESC ' ,
                            NULL,
                            'INV_DEFAULT');
      END;  
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(FREIGHT_TERMS$Action)            AS Action,
          r.get_cell(FREIGHT_TERMS$ENABLED_FLAG)      AS ENABLED_FLAG,
          r.get_cell(FREIGHT_TERMS$END_DATE_ACTIVE)   AS END_DATE_ACTIVE,
          r.get_cell(FREIGHT_TERMS$START_DT_ACTIVE)   AS START_DATE_ACTIVE,
          r.get_cell(FREIGHT_TERMS$FREIGHT_TERMS)     AS FREIGHT_TERMS,
          r.get_cell(FREIGHT_TERMS$TERM_DESC)         AS TERM_DESC,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(FREIGHT_TERMS_sheet)
  )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TERMS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ENABLED_FLAG := rec.ENABLED_FLAG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TERMS_sheet,
                            rec.row_seq,
                            'ENABLED_FLAG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.END_DATE_ACTIVE := rec.END_DATE_ACTIVE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TERMS_sheet,
                            rec.row_seq,
                            'END_DATE_ACTIVE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.START_DATE_ACTIVE := rec.START_DATE_ACTIVE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TERMS_sheet,
                            rec.row_seq,
                            'START_DATE_ACTIVE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FREIGHT_TERMS := rec.FREIGHT_TERMS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TERMS_sheet,
                            rec.row_seq,
                            'FREIGHT_TERMS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TERM_DESC := rec.TERM_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TERMS_sheet,
                            rec.row_seq,
                            'TERM_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TERMS.action_new then
         L_temp_rec.ENABLED_FLAG        := NVL( L_temp_rec.ENABLED_FLAG,L_default_rec.ENABLED_FLAG);
         L_temp_rec.END_DATE_ACTIVE     := NVL( L_temp_rec.END_DATE_ACTIVE,L_default_rec.END_DATE_ACTIVE);
         L_temp_rec.START_DATE_ACTIVE   := NVL( L_temp_rec.START_DATE_ACTIVE,L_default_rec.START_DATE_ACTIVE);
         L_temp_rec.FREIGHT_TERMS       := NVL( L_temp_rec.FREIGHT_TERMS,L_default_rec.FREIGHT_TERMS);
         L_temp_rec.TERM_DESC           := NVL( L_temp_rec.TERM_DESC,L_default_rec.TERM_DESC);
      end if;
      if not (
            L_temp_rec.FREIGHT_TERMS is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
                         FREIGHT_TERMS_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_FREIGHT_TERMS_col.extend();
         svc_FREIGHT_TERMS_col(svc_FREIGHT_TERMS_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_FREIGHT_TERMS_col.COUNT SAVE EXCEPTIONS
      merge into SVC_FREIGHT_TERMS st
      using(select
                  (case
                   when l_mi_rec.ENABLED_FLAG_mi    = 'N'
                    and svc_FREIGHT_TERMS_col(i).action = CORESVC_TERMS.action_mod
                    and s1.ENABLED_FLAG IS NULL
                   then mt.ENABLED_FLAG
                   else s1.ENABLED_FLAG
                   end) AS ENABLED_FLAG,
                  (case
                   when l_mi_rec.END_DATE_ACTIVE_mi    = 'N'
                    and svc_FREIGHT_TERMS_col(i).action = CORESVC_TERMS.action_mod
                    and s1.END_DATE_ACTIVE IS NULL
                   then mt.END_DATE_ACTIVE
                   else s1.END_DATE_ACTIVE
                   end) AS END_DATE_ACTIVE,
                  (case
                   when l_mi_rec.START_DATE_ACTIVE_mi    = 'N'
                    and svc_FREIGHT_TERMS_col(i).action = CORESVC_TERMS.action_mod
                    and s1.START_DATE_ACTIVE IS NULL
                   then mt.START_DATE_ACTIVE
                   else s1.START_DATE_ACTIVE
                   end) AS START_DATE_ACTIVE,
                  (case
                   when l_mi_rec.FREIGHT_TERMS_mi    = 'N'
                    and svc_FREIGHT_TERMS_col(i).action = CORESVC_TERMS.action_mod
                    and s1.FREIGHT_TERMS IS NULL
                   then mt.FREIGHT_TERMS
                   else s1.FREIGHT_TERMS
                   end) AS FREIGHT_TERMS,
                  (case
                   when l_mi_rec.TERM_DESC_mi    = 'N'
                    and svc_FREIGHT_TERMS_col(i).action = CORESVC_TERMS.action_mod
                    and s1.TERM_DESC IS NULL
                   then mt.TERM_DESC
                   else s1.TERM_DESC
                   end) AS TERM_DESC,
                  null as dummy
              from (select
                          svc_FREIGHT_TERMS_col(i).ENABLED_FLAG AS ENABLED_FLAG,
                          svc_FREIGHT_TERMS_col(i).END_DATE_ACTIVE AS END_DATE_ACTIVE,
                          svc_FREIGHT_TERMS_col(i).START_DATE_ACTIVE AS START_DATE_ACTIVE,
                          svc_FREIGHT_TERMS_col(i).FREIGHT_TERMS AS FREIGHT_TERMS,
                          svc_FREIGHT_TERMS_col(i).TERM_DESC AS TERM_DESC,
                          null as dummy
                      from dual ) s1,
            (select mto.*, mtl.term_desc 
               from FREIGHT_TERMS mto, FREIGHT_TERMS_TL mtl
              where mto.FREIGHT_TERMS = mtl.FREIGHT_TERMS
                and mtl.lang = LP_primary_lang) mt
             where
                  mt.FREIGHT_TERMS (+)     = s1.FREIGHT_TERMS   and
                  1 = 1 )sq
                on (
                    st.FREIGHT_TERMS      = sq.FREIGHT_TERMS and
                    svc_FREIGHT_TERMS_col(i).ACTION IN (CORESVC_TERMS.action_mod,CORESVC_TERMS.action_del))
      when matched then
      update
         set process_id        = svc_FREIGHT_TERMS_col(i).process_id ,
             chunk_id          = svc_FREIGHT_TERMS_col(i).chunk_id ,
             row_seq           = svc_FREIGHT_TERMS_col(i).row_seq ,
             action            = svc_FREIGHT_TERMS_col(i).action ,
             process$status    = svc_FREIGHT_TERMS_col(i).process$status ,
             end_date_active   = sq.end_date_active ,
             enabled_flag      = sq.enabled_flag ,
             start_date_active = sq.start_date_active ,
             term_desc         = sq.term_desc,
             create_id         = svc_FREIGHT_TERMS_col(i).create_id ,
             create_datetime   = svc_FREIGHT_TERMS_col(i).create_datetime ,
             last_upd_id       = svc_FREIGHT_TERMS_col(i).last_upd_id ,
             last_upd_datetime = svc_FREIGHT_TERMS_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             enabled_flag ,
             end_date_active ,
             start_date_active ,
             freight_terms ,
             term_desc,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_FREIGHT_TERMS_col(i).process_id ,
             svc_FREIGHT_TERMS_col(i).chunk_id ,
             svc_FREIGHT_TERMS_col(i).row_seq ,
             svc_FREIGHT_TERMS_col(i).action ,
             svc_FREIGHT_TERMS_col(i).process$status ,
             sq.enabled_flag ,
             sq.end_date_active ,
             sq.start_date_active ,
             sq.freight_terms ,
             sq.term_desc,
             svc_FREIGHT_TERMS_col(i).create_id ,
             svc_FREIGHT_TERMS_col(i).create_datetime ,
             svc_FREIGHT_TERMS_col(i).last_upd_id ,
             svc_FREIGHT_TERMS_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TERMS_sheet,
                            svc_FREIGHT_TERMS_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_FREIGHT_TERMS;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_FREIGHT_TERMS_TL( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_FREIGHT_TERMS_TL.process_id%TYPE) IS
   
   TYPE svc_FREIGHT_TERMS_TL_col_typ IS TABLE OF SVC_FREIGHT_TERMS_TL%ROWTYPE;
   L_temp_rec SVC_FREIGHT_TERMS_TL%ROWTYPE;
   svc_FREIGHT_TERMS_TL_col svc_FREIGHT_TERMS_TL_col_typ :=NEW svc_FREIGHT_TERMS_TL_col_typ();
   L_process_id SVC_FREIGHT_TERMS_TL.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_FREIGHT_TERMS_TL%ROWTYPE;
   
   cursor C_MANDATORY_IND is
      select
             TERM_DESC_mi,
             LANG_mi,
             FREIGHT_TERMS_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'FREIGHT_TERMS_TL'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'TERM_DESC' AS TERM_DESC,
                                         'LANG' AS LANG,
                                         'FREIGHT_TERMS' AS FREIGHT_TERMS,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);

      L_pk_columns    VARCHAR2(255)  := 'Freight Terms, Lang';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  -- Get default values.
   FOR rec IN (select
                       TERM_DESC_dv,
                       LANG_dv,
                       FREIGHT_TERMS_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'FREIGHT_TERMS_TL'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'TERM_DESC' AS TERM_DESC,
                                                      'LANG' AS LANG,
                                                      'FREIGHT_TERMS' AS FREIGHT_TERMS,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.TERM_DESC := rec.TERM_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'FREIGHT_TERMS_TL ' ,
                            NULL,
                            'TERM_DESC ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FREIGHT_TERMS_TL ' ,
                            NULL,
                           'LANG ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FREIGHT_TERMS := rec.FREIGHT_TERMS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'FREIGHT_TERMS_TL ' ,
                            NULL,
                            'FREIGHT_TERMS ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(FREIGHT_TERMS_TL$Action)          AS Action,
          r.get_cell(FREIGHT_TERMS_TL$TERM_DESC)       AS TERM_DESC,
          r.get_cell(FREIGHT_TERMS_TL$LANG)            AS LANG,
          r.get_cell(FREIGHT_TERMS_TL$FREIGHT_TERMS)   AS FREIGHT_TERMS,
          r.get_row_seq()                              AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(FREIGHT_TERMS_TL_sheet)
  )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TERMS_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TERM_DESC := rec.TERM_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TERMS_TL_sheet,
                            rec.row_seq,
                            'TERM_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TERMS_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FREIGHT_TERMS := rec.FREIGHT_TERMS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TERMS_TL_sheet,
                            rec.row_seq,
                            'FREIGHT_TERMS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TERMS.action_new then
         L_temp_rec.TERM_DESC       := NVL( L_temp_rec.TERM_DESC,L_default_rec.TERM_DESC);
         L_temp_rec.LANG            := NVL( L_temp_rec.LANG,L_default_rec.LANG);
         L_temp_rec.FREIGHT_TERMS   := NVL( L_temp_rec.FREIGHT_TERMS,L_default_rec.FREIGHT_TERMS);
      end if;
      if not (
            L_temp_rec.FREIGHT_TERMS is NOT NULL and
            L_temp_rec.LANG is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
                         FREIGHT_TERMS_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_FREIGHT_TERMS_TL_col.extend();
         svc_FREIGHT_TERMS_TL_col(svc_FREIGHT_TERMS_TL_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_FREIGHT_TERMS_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_FREIGHT_TERMS_TL st
      using(select
                  (case
                   when l_mi_rec.TERM_DESC_mi    = 'N'
                    and svc_FREIGHT_TERMS_TL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.TERM_DESC IS NULL
                   then mt.TERM_DESC
                   else s1.TERM_DESC
                   end) AS TERM_DESC,
                  (case
                   when l_mi_rec.LANG_mi    = 'N'
                    and svc_FREIGHT_TERMS_TL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.LANG IS NULL
                   then mt.LANG
                   else s1.LANG
                   end) AS LANG,
                  (case
                   when l_mi_rec.FREIGHT_TERMS_mi    = 'N'
                    and svc_FREIGHT_TERMS_TL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.FREIGHT_TERMS IS NULL
                   then mt.FREIGHT_TERMS
                   else s1.FREIGHT_TERMS
                   end) AS FREIGHT_TERMS,
                  null as dummy
              from (select
                          svc_FREIGHT_TERMS_TL_col(i).TERM_DESC AS TERM_DESC,
                          svc_FREIGHT_TERMS_TL_col(i).LANG AS LANG,
                          svc_FREIGHT_TERMS_TL_col(i).FREIGHT_TERMS AS FREIGHT_TERMS,
                          null as dummy
                      from dual ) s1,
            FREIGHT_TERMS_TL mt
             where
                  mt.FREIGHT_TERMS (+)     = s1.FREIGHT_TERMS   and
                  mt.LANG (+)     = s1.LANG   and
                  1 = 1 )sq
                on (
                    st.FREIGHT_TERMS      = sq.FREIGHT_TERMS and
                    st.LANG      = sq.LANG and
                    svc_FREIGHT_TERMS_TL_col(i).ACTION IN (CORESVC_TERMS.action_mod,CORESVC_TERMS.action_del))
      when matched then
      update
         set process_id      = svc_FREIGHT_TERMS_TL_col(i).process_id ,
             chunk_id        = svc_FREIGHT_TERMS_TL_col(i).chunk_id ,
             row_seq         = svc_FREIGHT_TERMS_TL_col(i).row_seq ,
             action          = svc_FREIGHT_TERMS_TL_col(i).action ,
             process$status  = svc_FREIGHT_TERMS_TL_col(i).process$status ,
             term_desc       = sq.term_desc ,
             create_id       = svc_FREIGHT_TERMS_TL_col(i).create_id ,
             create_datetime = svc_FREIGHT_TERMS_TL_col(i).create_datetime ,
             last_upd_id     = svc_FREIGHT_TERMS_TL_col(i).last_upd_id ,
             last_upd_datetime = svc_FREIGHT_TERMS_TL_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             term_desc ,
             lang ,
             freight_terms ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_FREIGHT_TERMS_TL_col(i).process_id ,
             svc_FREIGHT_TERMS_TL_col(i).chunk_id ,
             svc_FREIGHT_TERMS_TL_col(i).row_seq ,
             svc_FREIGHT_TERMS_TL_col(i).action ,
             svc_FREIGHT_TERMS_TL_col(i).process$status ,
             sq.term_desc ,
             sq.lang ,
             sq.freight_terms ,
             svc_FREIGHT_TERMS_TL_col(i).create_id ,
             svc_FREIGHT_TERMS_TL_col(i).create_datetime ,
             svc_FREIGHT_TERMS_TL_col(i).last_upd_id ,
             svc_FREIGHT_TERMS_TL_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TERMS_TL_sheet,
                            svc_FREIGHT_TERMS_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_FREIGHT_TERMS_TL;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TERMS_HEAD( I_file_id    IN   s9t_folder.file_id%TYPE,
                                  I_process_id IN   SVC_TERMS_HEAD.process_id%TYPE) IS
   
   TYPE svc_TERMS_HEAD_col_typ IS TABLE OF SVC_TERMS_HEAD%ROWTYPE;
   
   L_temp_rec           SVC_TERMS_HEAD%ROWTYPE;
   svc_TERMS_HEAD_col   svc_TERMS_HEAD_col_typ :=NEW svc_TERMS_HEAD_col_typ();
   L_process_id         SVC_TERMS_HEAD.process_id%TYPE;
   L_error              BOOLEAN:=FALSE;
   L_default_rec        SVC_TERMS_HEAD%ROWTYPE;
   
   cursor C_MANDATORY_IND is
      select
             RANK_mi,
             TERMS_mi,
             TERMS_CODE_mi,
             TERMS_DESC_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key   = template_key
                 and wksht_key      = 'TERMS_HEAD'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'RANK' AS RANK,
                                         'TERMS' AS TERMS,
                                         'TERMS_CODE' AS TERMS_CODE,
                                         'TERMS_DESC' AS TERMS_DESC,
                                         null as dummy));

      l_mi_rec     c_mandatory_ind%ROWTYPE;
      dml_errors   EXCEPTION;

      PRAGMA exception_init(dml_errors, -24381);

      L_pk_columns   VARCHAR2(255)  := 'Terms';
      L_error_code   NUMBER;
      L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  -- Get default values.
   FOR rec IN (select
                       RANK_dv,
                       TERMS_dv,
                       TERMS_CODE_dv,
                       TERMS_DESC_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'TERMS_HEAD'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'RANK' AS RANK,
                                                      'TERMS' AS TERMS,
                                                      'TERMS_CODE' AS TERMS_CODE,
                                                      'TERMS_DESC' AS TERMS_DESC,
                                                      NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.RANK := rec.RANK_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TERMS_HEAD ' ,
                            NULL,
                            'RANK ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TERMS := rec.TERMS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TERMS_HEAD ' ,
                            NULL,
                            'TERMS ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TERMS_CODE := rec.TERMS_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TERMS_HEAD ' ,
                            NULL,
                            'TERMS_CODE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TERMS_DESC := rec.TERMS_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TERMS_HEAD ' ,
                            NULL,
                            'TERMS_DESC ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(TERMS_HEAD$Action) AS Action,
          r.get_cell(TERMS_HEAD$RANK)   AS RANK,
          r.get_cell(TERMS_HEAD$TERMS)  AS TERMS,
          r.get_cell(TERMS_HEAD$TERMS_CODE)  AS TERMS_CODE,
          r.get_cell(TERMS_HEAD$TERMS_DESC)  AS TERMS_DESC,
          r.get_row_seq()               AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(TERMS_HEAD_sheet)
  )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_HEAD_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.RANK := rec.RANK;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_HEAD_sheet,
                            rec.row_seq,
                            'RANK',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TERMS := rec.TERMS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_HEAD_sheet,
                            rec.row_seq,
                            'TERMS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TERMS_CODE := rec.TERMS_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_HEAD_sheet,
                            rec.row_seq,
                            'TERMS_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TERMS_DESC := rec.TERMS_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_HEAD_sheet,
                            rec.row_seq,
                            'TERMS_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TERMS.action_new then
         L_temp_rec.RANK := NVL( L_temp_rec.RANK,L_default_rec.RANK);
         L_temp_rec.TERMS := NVL( L_temp_rec.TERMS,L_default_rec.TERMS);
         L_temp_rec.TERMS_CODE := NVL( L_temp_rec.TERMS_CODE,L_default_rec.TERMS_CODE);
         L_temp_rec.TERMS_DESC := NVL( L_temp_rec.TERMS_DESC,L_default_rec.TERMS_DESC);
      end if;
      if not (
            L_temp_rec.TERMS is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
                         TERMS_HEAD_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_TERMS_HEAD_col.extend();
         svc_TERMS_HEAD_col(svc_TERMS_HEAD_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_TERMS_HEAD_col.COUNT SAVE EXCEPTIONS
      merge into SVC_TERMS_HEAD st
      using(select
                  (case
                   when l_mi_rec.RANK_mi    = 'N'
                    and svc_TERMS_HEAD_col(i).action = CORESVC_TERMS.action_mod
                    and s1.RANK IS NULL
                   then mt.RANK
                   else s1.RANK
                   end) AS RANK,
                  (case
                   when l_mi_rec.TERMS_mi    = 'N'
                    and svc_TERMS_HEAD_col(i).action = CORESVC_TERMS.action_mod
                    and s1.TERMS IS NULL
                   then mt.TERMS
                   else s1.TERMS
                   end) AS TERMS,
                  (case
                   when l_mi_rec.TERMS_CODE_mi    = 'N'
                    and svc_TERMS_HEAD_col(i).action = CORESVC_TERMS.action_mod
                    and s1.TERMS_CODE IS NULL
                   then mt.TERMS_CODE
                   else s1.TERMS_CODE
                   end) AS TERMS_CODE,
                 (case
                   when l_mi_rec.TERMS_DESC_mi    = 'N'
                    and svc_TERMS_HEAD_col(i).action = CORESVC_TERMS.action_mod
                    and s1.TERMS_DESC IS NULL
                   then mt.TERMS_DESC
                   else s1.TERMS_DESC
                   end) AS TERMS_DESC,
                  null as dummy
              from (select
                          svc_TERMS_HEAD_col(i).RANK AS RANK,
                          svc_TERMS_HEAD_col(i).TERMS AS TERMS,
                          svc_TERMS_HEAD_col(i).TERMS_CODE AS TERMS_CODE,
                          svc_TERMS_HEAD_col(i).TERMS_DESC AS TERMS_DESC,
                          null as dummy
                      from dual ) s1,
             (select mto.*, mtl.terms_code,mtl.terms_desc 
                from TERMS_HEAD mto, TERMS_HEAD_TL mtl
               where mto.terms = mtl.terms
                 and mtl.lang = LP_primary_lang) mt
             where
                  mt.TERMS (+)     = s1.TERMS   and
                  1 = 1 )sq
                on (
                    st.TERMS      = sq.TERMS and
                    svc_TERMS_HEAD_col(i).ACTION IN (CORESVC_TERMS.action_mod,CORESVC_TERMS.action_del))
      when matched then
      update
         set process_id        = svc_TERMS_HEAD_col(i).process_id ,
             chunk_id          = svc_TERMS_HEAD_col(i).chunk_id ,
             row_seq           = svc_TERMS_HEAD_col(i).row_seq ,
             action            = svc_TERMS_HEAD_col(i).action ,
             process$status    = svc_TERMS_HEAD_col(i).process$status ,
             rank              = sq.rank ,
             terms_code        = sq.terms_code ,
             terms_desc        = sq.terms_desc ,
             create_id         = svc_TERMS_HEAD_col(i).create_id ,
             create_datetime   = svc_TERMS_HEAD_col(i).create_datetime ,
             last_upd_id       = svc_TERMS_HEAD_col(i).last_upd_id ,
             last_upd_datetime = svc_TERMS_HEAD_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             rank ,
             terms ,
             terms_code ,
             terms_desc ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_TERMS_HEAD_col(i).process_id ,
             svc_TERMS_HEAD_col(i).chunk_id ,
             svc_TERMS_HEAD_col(i).row_seq ,
             svc_TERMS_HEAD_col(i).action ,
             svc_TERMS_HEAD_col(i).process$status ,
             sq.rank ,
             sq.terms ,
             sq.terms_code ,
             sq.terms_desc ,
             svc_TERMS_HEAD_col(i).create_id ,
             svc_TERMS_HEAD_col(i).create_datetime ,
             svc_TERMS_HEAD_col(i).last_upd_id ,
             svc_TERMS_HEAD_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            TERMS_HEAD_sheet,
                            svc_TERMS_HEAD_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TERMS_HEAD;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TERMS_DETAIL( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_TERMS_DETAIL.process_id%TYPE) IS
   TYPE svc_TERMS_DETAIL_col_typ IS TABLE OF SVC_TERMS_DETAIL%ROWTYPE;
   L_temp_rec SVC_TERMS_DETAIL%ROWTYPE;
   svc_TERMS_DETAIL_col svc_TERMS_DETAIL_col_typ :=NEW svc_TERMS_DETAIL_col_typ();
   L_process_id SVC_TERMS_DETAIL.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_TERMS_DETAIL%ROWTYPE;
   cursor C_MANDATORY_IND is
      select
             CUTOFF_DAY_mi,
             END_DATE_ACTIVE_mi,
             START_DATE_ACTIVE_mi,
             ENABLED_FLAG_mi,
             FIXED_DATE_mi,
             DISC_MM_FWD_mi,
             DISC_DOM_mi,
             PERCENT_mi,
             DISCDAYS_mi,
             DUE_MM_FWD_mi,
             DUE_DOM_mi,
             DUE_MAX_AMOUNT_mi,
             DUEDAYS_mi,
             TERMS_SEQ_mi,
             TERMS_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'TERMS_DETAIL'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'CUTOFF_DAY' AS CUTOFF_DAY,
                                         'END_DATE_ACTIVE' AS END_DATE_ACTIVE,
                                         'START_DATE_ACTIVE' AS START_DATE_ACTIVE,
                                         'ENABLED_FLAG' AS ENABLED_FLAG,
                                         'FIXED_DATE' AS FIXED_DATE,
                                         'DISC_MM_FWD' AS DISC_MM_FWD,
                                         'DISC_DOM' AS DISC_DOM,
                                         'PERCENT' AS PERCENT,
                                         'DISCDAYS' AS DISCDAYS,
                                         'DUE_MM_FWD' AS DUE_MM_FWD,
                                         'DUE_DOM' AS DUE_DOM,
                                         'DUE_MAX_AMOUNT' AS DUE_MAX_AMOUNT,
                                         'DUEDAYS' AS DUEDAYS,
                                         'TERMS_SEQ' AS TERMS_SEQ,
                                         'TERMS' AS TERMS,
                                         null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
	  
	  L_pk_columns    VARCHAR2(255)  := 'Terms, Terms Sequence';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  -- Get default values.
   FOR rec IN (select
                       CUTOFF_DAY_dv,
                       END_DATE_ACTIVE_dv,
                       START_DATE_ACTIVE_dv,
                       ENABLED_FLAG_dv,
                       FIXED_DATE_dv,
                       DISC_MM_FWD_dv,
                       DISC_DOM_dv,
                       PERCENT_dv,
                       DISCDAYS_dv,
                       DUE_MM_FWD_dv,
                       DUE_DOM_dv,
                       DUE_MAX_AMOUNT_dv,
                       DUEDAYS_dv,
                       TERMS_SEQ_dv,
                       TERMS_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'TERMS_DETAIL'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'CUTOFF_DAY' AS CUTOFF_DAY,
                                                      'END_DATE_ACTIVE' AS END_DATE_ACTIVE,
                                                      'START_DATE_ACTIVE' AS START_DATE_ACTIVE,
                                                      'ENABLED_FLAG' AS ENABLED_FLAG,
                                                      'FIXED_DATE' AS FIXED_DATE,
                                                      'DISC_MM_FWD' AS DISC_MM_FWD,
                                                      'DISC_DOM' AS DISC_DOM,
                                                      'PERCENT' AS PERCENT,
                                                      'DISCDAYS' AS DISCDAYS,
                                                      'DUE_MM_FWD' AS DUE_MM_FWD,
                                                      'DUE_DOM' AS DUE_DOM,
                                                      'DUE_MAX_AMOUNT' AS DUE_MAX_AMOUNT,
                                                      'DUEDAYS' AS DUEDAYS,
                                                      'TERMS_SEQ' AS TERMS_SEQ,
                                                      'TERMS' AS TERMS,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.CUTOFF_DAY := rec.CUTOFF_DAY_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TERMS_DETAIL ' ,
                            NULL,
                            'CUTOFF_DAY ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.END_DATE_ACTIVE := rec.END_DATE_ACTIVE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TERMS_DETAIL ' ,
                            NULL,
                            'END_DATE_ACTIVE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.START_DATE_ACTIVE := rec.START_DATE_ACTIVE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TERMS_DETAIL ' ,
                            NULL,
                            'START_DATE_ACTIVE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ENABLED_FLAG := rec.ENABLED_FLAG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TERMS_DETAIL ' ,
                            NULL,
                            'ENABLED_FLAG ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FIXED_DATE := rec.FIXED_DATE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TERMS_DETAIL ' ,
                            NULL,
                            'FIXED_DATE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DISC_MM_FWD := rec.DISC_MM_FWD_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_DETAIL ' ,
                            NULL,
                           'DISC_MM_FWD ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DISC_DOM := rec.DISC_DOM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_DETAIL ' ,
                            NULL,
                           'DISC_DOM ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.PERCENT := rec.PERCENT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_DETAIL ' ,
                            NULL,
                           'PERCENT ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DISCDAYS := rec.DISCDAYS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_DETAIL ' ,
                            NULL,
                           'DISCDAYS ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DUE_MM_FWD := rec.DUE_MM_FWD_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_DETAIL ' ,
                            NULL,
                           'DUE_MM_FWD ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DUE_DOM := rec.DUE_DOM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_DETAIL ' ,
                            NULL,
                           'DUE_DOM ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DUE_MAX_AMOUNT := rec.DUE_MAX_AMOUNT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_DETAIL ' ,
                            NULL,
                           'DUE_MAX_AMOUNT ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DUEDAYS := rec.DUEDAYS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_DETAIL ' ,
                            NULL,
                           'DUEDAYS ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TERMS_SEQ := rec.TERMS_SEQ_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_DETAIL ' ,
                            NULL,
                           'TERMS_SEQ ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TERMS := rec.TERMS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_DETAIL ' ,
                            NULL,
                           'TERMS ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(TERMS_DETAIL$Action)            AS Action,
          r.get_cell(TERMS_DETAIL$CUTOFF_DAY)        AS CUTOFF_DAY,
          r.get_cell(TERMS_DETAIL$END_DATE_ACTIVE)   AS END_DATE_ACTIVE,
          r.get_cell(TERMS_DETAIL$START_DATE_ACTIVE) AS START_DATE_ACTIVE,
          r.get_cell(TERMS_DETAIL$ENABLED_FLAG)      AS ENABLED_FLAG,
          r.get_cell(TERMS_DETAIL$FIXED_DATE)        AS FIXED_DATE,
          r.get_cell(TERMS_DETAIL$DISC_MM_FWD)       AS DISC_MM_FWD,
          r.get_cell(TERMS_DETAIL$DISC_DOM)          AS DISC_DOM,
          r.get_cell(TERMS_DETAIL$PERCENT)           AS PERCENT,
          r.get_cell(TERMS_DETAIL$DISCDAYS)          AS DISCDAYS,
          r.get_cell(TERMS_DETAIL$DUE_MM_FWD)        AS DUE_MM_FWD,
          r.get_cell(TERMS_DETAIL$DUE_DOM)           AS DUE_DOM,
          r.get_cell(TERMS_DETAIL$DUE_MAX_AMOUNT)    AS DUE_MAX_AMOUNT,
          r.get_cell(TERMS_DETAIL$DUEDAYS)           AS DUEDAYS,
          r.get_cell(TERMS_DETAIL$TERMS_SEQ)         AS TERMS_SEQ,
          r.get_cell(TERMS_DETAIL$TERMS)             AS TERMS,
          r.get_row_seq()                            AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(TERMS_DETAIL_sheet)
  )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CUTOFF_DAY := rec.CUTOFF_DAY;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'CUTOFF_DAY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.END_DATE_ACTIVE := rec.END_DATE_ACTIVE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'END_DATE_ACTIVE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.START_DATE_ACTIVE := rec.START_DATE_ACTIVE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'START_DATE_ACTIVE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ENABLED_FLAG := rec.ENABLED_FLAG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'ENABLED_FLAG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FIXED_DATE := rec.FIXED_DATE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'FIXED_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DISC_MM_FWD := rec.DISC_MM_FWD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'DISC_MM_FWD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DISC_DOM := rec.DISC_DOM;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'DISC_DOM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.PERCENT := rec.PERCENT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'PERCENT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DISCDAYS := rec.DISCDAYS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'DISCDAYS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DUE_MM_FWD := rec.DUE_MM_FWD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'DUE_MM_FWD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DUE_DOM := rec.DUE_DOM;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'DUE_DOM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DUE_MAX_AMOUNT := rec.DUE_MAX_AMOUNT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'DUE_MAX_AMOUNT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DUEDAYS := rec.DUEDAYS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'DUEDAYS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TERMS_SEQ := rec.TERMS_SEQ;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'TERMS_SEQ',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TERMS := rec.TERMS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            rec.row_seq,
                            'TERMS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TERMS.action_new then
         L_temp_rec.CUTOFF_DAY          := NVL( L_temp_rec.CUTOFF_DAY,L_default_rec.CUTOFF_DAY);
         L_temp_rec.END_DATE_ACTIVE     := NVL( L_temp_rec.END_DATE_ACTIVE,L_default_rec.END_DATE_ACTIVE);
         L_temp_rec.START_DATE_ACTIVE   := NVL( L_temp_rec.START_DATE_ACTIVE,L_default_rec.START_DATE_ACTIVE);
         L_temp_rec.ENABLED_FLAG        := NVL( L_temp_rec.ENABLED_FLAG,L_default_rec.ENABLED_FLAG);
         L_temp_rec.FIXED_DATE          := NVL( L_temp_rec.FIXED_DATE,L_default_rec.FIXED_DATE);
         L_temp_rec.DISC_MM_FWD         := NVL( L_temp_rec.DISC_MM_FWD,L_default_rec.DISC_MM_FWD);
         L_temp_rec.DISC_DOM            := NVL( L_temp_rec.DISC_DOM,L_default_rec.DISC_DOM);
         L_temp_rec.PERCENT             := NVL( L_temp_rec.PERCENT,L_default_rec.PERCENT);
         L_temp_rec.DISCDAYS            := NVL( L_temp_rec.DISCDAYS,L_default_rec.DISCDAYS);
         L_temp_rec.DUE_MM_FWD          := NVL( L_temp_rec.DUE_MM_FWD,L_default_rec.DUE_MM_FWD);
         L_temp_rec.DUE_DOM             := NVL( L_temp_rec.DUE_DOM,L_default_rec.DUE_DOM);
         L_temp_rec.DUE_MAX_AMOUNT      := NVL( L_temp_rec.DUE_MAX_AMOUNT,L_default_rec.DUE_MAX_AMOUNT);
         L_temp_rec.DUEDAYS             := NVL( L_temp_rec.DUEDAYS,L_default_rec.DUEDAYS);
         L_temp_rec.TERMS_SEQ           := NVL( L_temp_rec.TERMS_SEQ,L_default_rec.TERMS_SEQ);
         L_temp_rec.TERMS               := NVL( L_temp_rec.TERMS,L_default_rec.TERMS);
      end if;
      if not (
            L_temp_rec.TERMS_SEQ is NOT NULL and
            L_temp_rec.TERMS is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
                         TERMS_DETAIL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_TERMS_DETAIL_col.extend();
         svc_TERMS_DETAIL_col(svc_TERMS_DETAIL_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_TERMS_DETAIL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_TERMS_DETAIL st
      using(select
                  (case
                   when l_mi_rec.CUTOFF_DAY_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.CUTOFF_DAY IS NULL
                   then mt.CUTOFF_DAY
                   else s1.CUTOFF_DAY
                   end) AS CUTOFF_DAY,
                  (case
                   when l_mi_rec.END_DATE_ACTIVE_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.END_DATE_ACTIVE IS NULL
                   then mt.END_DATE_ACTIVE
                   else s1.END_DATE_ACTIVE
                   end) AS END_DATE_ACTIVE,
                  (case
                   when l_mi_rec.START_DATE_ACTIVE_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.START_DATE_ACTIVE IS NULL
                   then mt.START_DATE_ACTIVE
                   else s1.START_DATE_ACTIVE
                   end) AS START_DATE_ACTIVE,
                  (case
                   when l_mi_rec.ENABLED_FLAG_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.ENABLED_FLAG IS NULL
                   then mt.ENABLED_FLAG
                   else s1.ENABLED_FLAG
                   end) AS ENABLED_FLAG,
                  (case
                   when l_mi_rec.FIXED_DATE_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.FIXED_DATE IS NULL
                   then mt.FIXED_DATE
                   else s1.FIXED_DATE
                   end) AS FIXED_DATE,
                  (case
                   when l_mi_rec.DISC_MM_FWD_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.DISC_MM_FWD IS NULL
                   then mt.DISC_MM_FWD
                   else s1.DISC_MM_FWD
                   end) AS DISC_MM_FWD,
                  (case
                   when l_mi_rec.DISC_DOM_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.DISC_DOM IS NULL
                   then mt.DISC_DOM
                   else s1.DISC_DOM
                   end) AS DISC_DOM,
                  (case
                   when l_mi_rec.PERCENT_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.PERCENT IS NULL
                   then mt.PERCENT
                   else s1.PERCENT
                   end) AS PERCENT,
                  (case
                   when l_mi_rec.DISCDAYS_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.DISCDAYS IS NULL
                   then mt.DISCDAYS
                   else s1.DISCDAYS
                   end) AS DISCDAYS,
                  (case
                   when l_mi_rec.DUE_MM_FWD_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.DUE_MM_FWD IS NULL
                   then mt.DUE_MM_FWD
                   else s1.DUE_MM_FWD
                   end) AS DUE_MM_FWD,
                  (case
                   when l_mi_rec.DUE_DOM_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.DUE_DOM IS NULL
                   then mt.DUE_DOM
                   else s1.DUE_DOM
                   end) AS DUE_DOM,
                  (case
                   when l_mi_rec.DUE_MAX_AMOUNT_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.DUE_MAX_AMOUNT IS NULL
                   then mt.DUE_MAX_AMOUNT
                   else s1.DUE_MAX_AMOUNT
                   end) AS DUE_MAX_AMOUNT,
                  (case
                   when l_mi_rec.DUEDAYS_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.DUEDAYS IS NULL
                   then mt.DUEDAYS
                   else s1.DUEDAYS
                   end) AS DUEDAYS,
                  (case
                   when l_mi_rec.TERMS_SEQ_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.TERMS_SEQ IS NULL
                   then mt.TERMS_SEQ
                   else s1.TERMS_SEQ
                   end) AS TERMS_SEQ,
                  (case
                   when l_mi_rec.TERMS_mi    = 'N'
                    and svc_TERMS_DETAIL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.TERMS IS NULL
                   then mt.TERMS
                   else s1.TERMS
                   end) AS TERMS,
                  null as dummy
              from (select
                          svc_TERMS_DETAIL_col(i).CUTOFF_DAY AS CUTOFF_DAY,
                          svc_TERMS_DETAIL_col(i).END_DATE_ACTIVE AS END_DATE_ACTIVE,
                          svc_TERMS_DETAIL_col(i).START_DATE_ACTIVE AS START_DATE_ACTIVE,
                          svc_TERMS_DETAIL_col(i).ENABLED_FLAG AS ENABLED_FLAG,
                          svc_TERMS_DETAIL_col(i).FIXED_DATE AS FIXED_DATE,
                          svc_TERMS_DETAIL_col(i).DISC_MM_FWD AS DISC_MM_FWD,
                          svc_TERMS_DETAIL_col(i).DISC_DOM AS DISC_DOM,
                          svc_TERMS_DETAIL_col(i).PERCENT AS PERCENT,
                          svc_TERMS_DETAIL_col(i).DISCDAYS AS DISCDAYS,
                          svc_TERMS_DETAIL_col(i).DUE_MM_FWD AS DUE_MM_FWD,
                          svc_TERMS_DETAIL_col(i).DUE_DOM AS DUE_DOM,
                          svc_TERMS_DETAIL_col(i).DUE_MAX_AMOUNT AS DUE_MAX_AMOUNT,
                          svc_TERMS_DETAIL_col(i).DUEDAYS AS DUEDAYS,
                          svc_TERMS_DETAIL_col(i).TERMS_SEQ AS TERMS_SEQ,
                          svc_TERMS_DETAIL_col(i).TERMS AS TERMS,
                          null as dummy
                      from dual ) s1,
            TERMS_DETAIL mt
             where
                  mt.TERMS_SEQ (+)     = s1.TERMS_SEQ   and
                  mt.TERMS (+)     = s1.TERMS   and
                  1 = 1 )sq
                on (
                    st.TERMS_SEQ      = sq.TERMS_SEQ and
                    st.TERMS      = sq.TERMS and
                    svc_TERMS_DETAIL_col(i).ACTION IN (CORESVC_TERMS.action_mod,CORESVC_TERMS.action_del))
      when matched then
      update
         set process_id      = svc_TERMS_DETAIL_col(i).process_id ,
             chunk_id        = svc_TERMS_DETAIL_col(i).chunk_id ,
             row_seq         = svc_TERMS_DETAIL_col(i).row_seq ,
             action          = svc_TERMS_DETAIL_col(i).action ,
             process$status  = svc_TERMS_DETAIL_col(i).process$status ,
             disc_mm_fwd     = sq.disc_mm_fwd ,
             fixed_date      = sq.fixed_date ,
             due_mm_fwd      = sq.due_mm_fwd ,
             start_date_active = sq.start_date_active ,
             cutoff_day      = sq.cutoff_day ,
             enabled_flag    = sq.enabled_flag ,
             disc_dom        = sq.disc_dom ,
             percent         = sq.percent ,
             duedays         = sq.duedays ,
             discdays        = sq.discdays ,
             end_date_active = sq.end_date_active ,
             due_dom         = sq.due_dom ,
             due_max_amount  = sq.due_max_amount ,
             create_id       = svc_TERMS_DETAIL_col(i).create_id ,
             create_datetime = svc_TERMS_DETAIL_col(i).create_datetime ,
             last_upd_id     = svc_TERMS_DETAIL_col(i).last_upd_id ,
             last_upd_datetime = svc_TERMS_DETAIL_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             cutoff_day ,
             end_date_active ,
             start_date_active ,
             enabled_flag ,
             fixed_date ,
             disc_mm_fwd ,
             disc_dom ,
             percent ,
             discdays ,
             due_mm_fwd ,
             due_dom ,
             due_max_amount ,
             duedays ,
             terms_seq ,
             terms ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_TERMS_DETAIL_col(i).process_id ,
             svc_TERMS_DETAIL_col(i).chunk_id ,
             svc_TERMS_DETAIL_col(i).row_seq ,
             svc_TERMS_DETAIL_col(i).action ,
             svc_TERMS_DETAIL_col(i).process$status ,
             sq.cutoff_day ,
             sq.end_date_active ,
             sq.start_date_active ,
             sq.enabled_flag ,
             sq.fixed_date ,
             sq.disc_mm_fwd ,
             sq.disc_dom ,
             sq.percent ,
             sq.discdays ,
             sq.due_mm_fwd ,
             sq.due_dom ,
             sq.due_max_amount ,
             sq.duedays ,
             sq.terms_seq ,
             sq.terms ,
             svc_TERMS_DETAIL_col(i).create_id ,
             svc_TERMS_DETAIL_col(i).create_datetime ,
             svc_TERMS_DETAIL_col(i).last_upd_id ,
             svc_TERMS_DETAIL_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_DETAIL_sheet,
                            svc_TERMS_DETAIL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TERMS_DETAIL;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TERMS_HEAD_TL( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_TERMS_HEAD_TL.process_id%TYPE) IS
   TYPE svc_TERMS_HEAD_TL_col_typ IS TABLE OF SVC_TERMS_HEAD_TL%ROWTYPE;
   L_temp_rec SVC_TERMS_HEAD_TL%ROWTYPE;
   svc_TERMS_HEAD_TL_col svc_TERMS_HEAD_TL_col_typ :=NEW svc_TERMS_HEAD_TL_col_typ();
   L_process_id SVC_TERMS_HEAD_TL.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_TERMS_HEAD_TL%ROWTYPE;
   cursor C_MANDATORY_IND is
      select
             TERMS_DESC_mi,
             TERMS_CODE_mi,
             LANG_mi,
             TERMS_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'TERMS_HEAD_TL'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'TERMS_DESC' AS TERMS_DESC,
                                         'TERMS_CODE' AS TERMS_CODE,
                                         'LANG' AS LANG,
                                         'TERMS' AS TERMS,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);

      L_pk_columns    VARCHAR2(255)  := 'Terms, Lang';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  -- Get default values.
   FOR rec IN (select
                       TERMS_DESC_dv,
                       TERMS_CODE_dv,
                       LANG_dv,
                       TERMS_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = template_key
                          and wksht_key     = 'TERMS_HEAD_TL'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'TERMS_DESC' AS TERMS_DESC,
                                                      'TERMS_CODE' AS TERMS_CODE,
                                                      'LANG' AS LANG,
                                                      'TERMS' AS TERMS,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.TERMS_DESC := rec.TERMS_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_HEAD_TL ' ,
                            NULL,
                           'TERMS_DESC ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TERMS_CODE := rec.TERMS_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_HEAD_TL ' ,
                            NULL,
                           'TERMS_CODE ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_HEAD_TL ' ,
                            NULL,
                           'LANG ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TERMS := rec.TERMS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TERMS_HEAD_TL ' ,
                            NULL,
                           'TERMS ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(TERMS_HEAD_TL$Action)        AS Action,
          r.get_cell(TERMS_HEAD_TL$TERMS_DESC)    AS TERMS_DESC,
          r.get_cell(TERMS_HEAD_TL$TERMS_CODE)    AS TERMS_CODE,
          r.get_cell(TERMS_HEAD_TL$LANG)          AS LANG,
          r.get_cell(TERMS_HEAD_TL$TERMS)         AS TERMS,
          r.get_row_seq()                         AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(TERMS_HEAD_TL_sheet)
  )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_HEAD_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TERMS_DESC := rec.TERMS_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_HEAD_TL_sheet,
                            rec.row_seq,
                            'TERMS_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TERMS_CODE := rec.TERMS_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_HEAD_TL_sheet,
                            rec.row_seq,
                            'TERMS_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_HEAD_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TERMS := rec.TERMS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TERMS_HEAD_TL_sheet,
                            rec.row_seq,
                            'TERMS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TERMS.action_new then
         L_temp_rec.TERMS_DESC      := NVL( L_temp_rec.TERMS_DESC,L_default_rec.TERMS_DESC);
         L_temp_rec.TERMS_CODE      := NVL( L_temp_rec.TERMS_CODE,L_default_rec.TERMS_CODE);
         L_temp_rec.LANG            := NVL( L_temp_rec.LANG,L_default_rec.LANG);
         L_temp_rec.TERMS           := NVL( L_temp_rec.TERMS,L_default_rec.TERMS);
      end if;
      if not (
            L_temp_rec.LANG is NOT NULL and
            L_temp_rec.TERMS is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
                         TERMS_HEAD_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                        'S9T_TERMS_HEAD_TL_PK');
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_TERMS_HEAD_TL_col.extend();
         svc_TERMS_HEAD_TL_col(svc_TERMS_HEAD_TL_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_TERMS_HEAD_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_TERMS_HEAD_TL st
      using(select
                  (case
                   when l_mi_rec.TERMS_DESC_mi    = 'N'
                    and svc_TERMS_HEAD_TL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.TERMS_DESC IS NULL
                   then mt.TERMS_DESC
                   else s1.TERMS_DESC
                   end) AS TERMS_DESC,
                  (case
                   when l_mi_rec.TERMS_CODE_mi    = 'N'
                    and svc_TERMS_HEAD_TL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.TERMS_CODE IS NULL
                   then mt.TERMS_CODE
                   else s1.TERMS_CODE
                   end) AS TERMS_CODE,
                  (case
                   when l_mi_rec.LANG_mi    = 'N'
                    and svc_TERMS_HEAD_TL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.LANG IS NULL
                   then mt.LANG
                   else s1.LANG
                   end) AS LANG,
                  (case
                   when l_mi_rec.TERMS_mi    = 'N'
                    and svc_TERMS_HEAD_TL_col(i).action = CORESVC_TERMS.action_mod
                    and s1.TERMS IS NULL
                   then mt.TERMS
                   else s1.TERMS
                   end) AS TERMS,
                  null as dummy
              from (select
                          svc_TERMS_HEAD_TL_col(i).TERMS_DESC AS TERMS_DESC,
                          svc_TERMS_HEAD_TL_col(i).TERMS_CODE AS TERMS_CODE,
                          svc_TERMS_HEAD_TL_col(i).LANG AS LANG,
                          svc_TERMS_HEAD_TL_col(i).TERMS AS TERMS,
                          null as dummy
                      from dual ) s1,
            TERMS_HEAD_TL mt
             where
                  mt.LANG (+)     = s1.LANG   and
                  mt.TERMS (+)     = s1.TERMS   and
                  1 = 1 )sq
                on (
                    st.LANG      = sq.LANG and
                    st.TERMS      = sq.TERMS and
                    svc_TERMS_HEAD_TL_col(i).ACTION IN (CORESVC_TERMS.action_mod,CORESVC_TERMS.action_del))
      when matched then
      update
         set process_id      = svc_TERMS_HEAD_TL_col(i).process_id ,
             chunk_id        = svc_TERMS_HEAD_TL_col(i).chunk_id ,
             row_seq         = svc_TERMS_HEAD_TL_col(i).row_seq ,
             action          = svc_TERMS_HEAD_TL_col(i).action ,
             process$status  = svc_TERMS_HEAD_TL_col(i).process$status ,
             terms_code      = sq.terms_code ,
             terms_desc      = sq.terms_desc ,
             create_id       = svc_TERMS_HEAD_TL_col(i).create_id ,
             create_datetime = svc_TERMS_HEAD_TL_col(i).create_datetime ,
             last_upd_id     = svc_TERMS_HEAD_TL_col(i).last_upd_id ,
             last_upd_datetime = svc_TERMS_HEAD_TL_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             terms_desc ,
             terms_code ,
             lang ,
             terms ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_TERMS_HEAD_TL_col(i).process_id ,
             svc_TERMS_HEAD_TL_col(i).chunk_id ,
             svc_TERMS_HEAD_TL_col(i).row_seq ,
             svc_TERMS_HEAD_TL_col(i).action ,
             svc_TERMS_HEAD_TL_col(i).process$status ,
             sq.terms_desc ,
             sq.terms_code ,
             sq.lang ,
             sq.terms ,
             svc_TERMS_HEAD_TL_col(i).create_id ,
             svc_TERMS_HEAD_TL_col(i).create_datetime ,
             svc_TERMS_HEAD_TL_col(i).last_upd_id ,
             svc_TERMS_HEAD_TL_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            TERMS_HEAD_TL_sheet,
                            svc_TERMS_HEAD_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TERMS_HEAD_TL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT        OUT   NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER )
RETURN BOOLEAN IS
   L_file             s9t_file;
   L_sheets           s9t_pkg.names_map_typ;
   L_program          VARCHAR2(64):='CORESVC_TERMS.process_s9t';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   
   s9t_pkg.save_obj(L_file);
   
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_FREIGHT_TERMS(I_file_id,I_process_id);
      PROCESS_S9T_FREIGHT_TERMS_TL(I_file_id,I_process_id);
      PROCESS_S9T_TERMS_HEAD(I_file_id,I_process_id);
      PROCESS_S9T_TERMS_HEAD_TL(I_file_id,I_process_id);
      PROCESS_S9T_TERMS_DETAIL(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.count();
      forall i in 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
-----------------------------------------------------------------------------------------------
FUNCTION MERGE_FREIGHT_TERMS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_freight_terms   IN       FREIGHT_TERMS_TL.FREIGHT_TERMS%TYPE,
                                I_terms_desc      IN       FREIGHT_TERMS_TL.TERM_DESC%TYPE,
                                I_lang            IN       FREIGHT_TERMS_TL.LANG%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(61) := 'FTERM_SQL.MERGE_FREIGHT_TERMS_TL';
   L_table         VARCHAR2(30) := 'FREIGHT_TERMS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   L_freight_terms_exists   VARCHAR2(1)  := NULL;
   L_orig_lang_ind          VARCHAR2(1)  := 'N';
   L_reviewed_ind           VARCHAR2(1)  := 'N';

   cursor C_FREIGHT_TERMS_EXIST is
      select 'x'
        from freight_terms_tl
       where freight_terms = I_freight_terms
         and rownum = 1;

   cursor C_LOCK_FREIGHT_TERMS_TL is
      select 'x'
        from freight_terms_tl
       where freight_terms = I_freight_terms
         and lang   = I_lang
         for update nowait;

BEGIN

   -- Check first if the freight terms (regardless of language) already exists in the table.
   -- If it already exists, set orig_lang_ind = 'N'. Otherwise, set orig_lang_ind = 'Y'.
   -- Reviewed_ind is only set to 'N' for entries in the original language to indicate
   -- if the original description has changed and translation should be reviewed for accuracy.
   -- Both flags are only used for inserts, not updates.

   open C_FREIGHT_TERMS_EXIST;
   fetch C_FREIGHT_TERMS_EXIST into L_freight_terms_exists;
   close C_FREIGHT_TERMS_EXIST;

   if L_freight_terms_exists is NULL then
      L_orig_lang_ind := 'Y';
      L_reviewed_ind  := 'N';
   else
      L_orig_lang_ind := 'N';
      L_reviewed_ind  := 'Y';
   end if;

   open C_LOCK_FREIGHT_TERMS_TL;
   close C_LOCK_FREIGHT_TERMS_TL;

   merge into freight_terms_tl ftl
      using (select I_freight_terms freight_terms,
                    I_lang lang,
                    I_terms_desc term_desc,
                    L_orig_lang_ind orig_lang_ind, --used for inserts only
                    L_reviewed_ind reviewed_ind,   --used for inserts only
                    user create_id,
                    sysdate create_datetime,
                    user last_update_id,
                    sysdate last_update_datetime
               from dual) use_this
         on (ftl.freight_terms = use_this.freight_terms and
             ftl.lang  = use_this.lang)
   when matched then
      update
         set ftl.term_desc = use_this.term_desc,
             ftl.reviewed_ind = decode(ftl.orig_lang_ind, 'Y', 'N', reviewed_ind), --when description is changed for the original language, set the entry for translation review
             ftl.last_update_id = use_this.last_update_id,
             ftl.last_update_datetime = use_this.last_update_datetime
   when NOT matched then
      insert (freight_terms,
              lang,
              term_desc,
              orig_lang_ind,
              reviewed_ind,
              create_id,
              create_datetime,
              last_update_id,
              last_update_datetime)
      values (use_this.freight_terms,
              use_this.lang,
              use_this.term_desc,
              use_this.orig_lang_ind,
              use_this.reviewed_ind,
              use_this.create_id,
              use_this.create_datetime,
              use_this.last_update_id,
              use_this.last_update_datetime);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_freight_terms,
                                            NULL);
      close C_LOCK_FREIGHT_TERMS_TL;
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      if C_LOCK_FREIGHT_TERMS_TL%ISOPEN then
         close C_LOCK_FREIGHT_TERMS_TL;
      end if;
      return FALSE;
END MERGE_FREIGHT_TERMS_TL;
---------------------------------------------------------------------------------------------------------
FUNCTION DEL_FREIGHT_TERMS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_freight_terms   IN       FREIGHT_TERMS_TL.FREIGHT_TERMS%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(61) := 'FTERM_SQL.DEL_FREIGHT_TERMS_TL';
   L_table         VARCHAR2(30) := 'FREIGHT_TERMS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
      
   cursor C_LOCK_FREIGHT_TERMS_TL is
      select 'x'
        from freight_terms_tl
       where freight_terms = I_freight_terms
         for update nowait;                

BEGIN

   open C_LOCK_FREIGHT_TERMS_TL;
   close C_LOCK_FREIGHT_TERMS_TL;

   delete from freight_terms_tl
    where freight_terms = I_freight_terms;

   update svc_freight_terms_tl
      set process$status = 'P'
    where freight_terms = I_freight_terms;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_freight_terms,
                                            NULL);
      close C_LOCK_FREIGHT_TERMS_TL;
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      if C_LOCK_FREIGHT_TERMS_TL%ISOPEN then
         close C_LOCK_FREIGHT_TERMS_TL;
      end if;
      return FALSE;
END DEL_FREIGHT_TERMS_TL;
---------------------------------------------------------------------------------------------
FUNCTION MERGE_TERMS_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_terms           IN       TERMS_HEAD_TL.TERMS%TYPE,
                             I_terms_desc      IN       TERMS_HEAD_TL.TERMS_DESC%TYPE,
                             I_terms_code      IN       TERMS_HEAD_TL.TERMS_CODE%TYPE,
                             I_lang            IN       TERMS_HEAD_TL.LANG%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(61) := 'TERMS_SQL.MERGE_TERMS_HEAD_TL';
   L_table         VARCHAR2(30) := 'TERMS_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   L_terms_exists    VARCHAR2(1)  := NULL;
   L_orig_lang_ind   VARCHAR2(1)  := 'N';
   L_reviewed_ind    VARCHAR2(1)  := 'N';

   cursor C_TERMS_HEAD_EXIST is
      select 'x'
        from terms_head_tl
       where terms = I_terms
         and rownum = 1;

   cursor C_LOCK_TERMS_HEAD_TL is
      select 'x'
        from terms_head_tl
       where terms = I_terms
         and lang   = I_lang
         for update nowait;

BEGIN

   -- Check first if the terms head (regardless of language) already exists in the table.
   -- If it already exists, set orig_lang_ind = 'N'. Otherwise, set orig_lang_ind = 'Y'.
   -- Reviewed_ind is only set to 'N' for entries in the original language to indicate
   -- if the original description has changed and translation should be reviewed for accuracy.
   -- Both flags are only used for inserts, not updates.

   open C_TERMS_HEAD_EXIST;
   fetch C_TERMS_HEAD_EXIST into L_terms_exists;
   close C_TERMS_HEAD_EXIST;

   if L_terms_exists is NULL then
      L_orig_lang_ind := 'Y';
      L_reviewed_ind  := 'N';
   else
      L_orig_lang_ind := 'N';
      L_reviewed_ind  := 'Y';
   end if;

   open C_LOCK_TERMS_HEAD_TL;
   close C_LOCK_TERMS_HEAD_TL;

   merge into terms_head_tl thtl
      using (select I_terms terms,
                    I_lang lang,
                    I_terms_desc terms_desc,
                    I_terms_code terms_code,
                    L_orig_lang_ind orig_lang_ind, --used for inserts only
                    L_reviewed_ind reviewed_ind,   --used for inserts only
                    user create_id,
                    sysdate create_datetime,
                    user last_update_id,
                    sysdate last_update_datetime
               from dual) use_this
         on (thtl.terms = use_this.terms and
             thtl.lang  = use_this.lang)
   when matched then
      update
         set thtl.terms_desc = use_this.terms_desc,
             thtl.terms_code = use_this.terms_code,
             thtl.reviewed_ind = decode(thtl.orig_lang_ind, 'Y', 'N', reviewed_ind), --when description is changed for the original language, set the entry for translation review
             thtl.last_update_id = use_this.last_update_id,
             thtl.last_update_datetime = use_this.last_update_datetime
   when NOT matched then
      insert (terms,
              lang,
              terms_code,
              terms_desc,
              orig_lang_ind,
              reviewed_ind,
              create_id,
              create_datetime,
              last_update_id,
              last_update_datetime)
      values (use_this.terms,
              use_this.lang,
              use_this.terms_code,
              use_this.terms_desc,
              use_this.orig_lang_ind,
              use_this.reviewed_ind,
              use_this.create_id,
              use_this.create_datetime,
              use_this.last_update_id,
              use_this.last_update_datetime);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_terms,
                                            I_LANG);
      close C_LOCK_TERMS_HEAD_TL;
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      if C_LOCK_TERMS_HEAD_TL%ISOPEN then
         close C_LOCK_TERMS_HEAD_TL;
      end if;
      return FALSE;
END MERGE_TERMS_HEAD_TL;
-------------------------------------------------------------------------------
FUNCTION DEL_TERMS_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_terms           IN       TERMS_HEAD_TL.TERMS%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(61) := 'TERMS_SQL.DEL_TERMS_HEAD_TL';
   L_table         VARCHAR2(30) := 'TERMS_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
      
   cursor C_LOCK_TERMS_HEAD_TL is
      select 'x'
        from terms_head_tl
       where terms = I_terms
         for update nowait;         

BEGIN

   open C_LOCK_TERMS_HEAD_TL;
   close C_LOCK_TERMS_HEAD_TL;

   delete from terms_head_tl
    where terms = I_terms;

   update svc_terms_head_tl
      set process$status = 'P'
    where terms = I_terms;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_terms,
                                            NULL);
      close C_LOCK_TERMS_HEAD_TL;
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      if C_LOCK_TERMS_HEAD_TL%ISOPEN then
         close C_LOCK_TERMS_HEAD_TL;
      end if;
      return FALSE;
END DEL_TERMS_HEAD_TL; 
--------------------------------------------------------------------------------------
FUNCTION PROCESS_FREIGHT_TERMS_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_error           IN OUT   BOOLEAN,
                                   I_rec             IN       C_SVC_FREIGHT_TERMS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_TERMS.PROCESS_FREIGHT_TERMS_VAL';
   L_table   VARCHAR2(64):= 'SVC_FREIGHT_TERMS';
   L_exists  VARCHAR2(1)  := 'N';

   cursor C_CHK_CHILD_EXISTS is
   select 'Y'
     from (select freight_terms from ORDHEAD
           union
           select freight_terms from SUPS
           union
           select freight_terms from INVC_HEAD
           union
           select freight_terms from ORDHEAD_REV
           union
           select freight_terms from IIF_HEAD) DEP_TBL
     where DEP_TBL.freight_terms = I_rec.freight_terms; 

BEGIN
   if I_rec.action = action_del then
       
      open C_CHK_CHILD_EXISTS;
      fetch C_CHK_CHILD_EXISTS  into L_exists;
      if C_CHK_CHILD_EXISTS%FOUND then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FREIGHT_TERMS',
                     'CHILD_EXISTS');
          O_error := TRUE;
      end if;
      close C_CHK_CHILD_EXISTS;

   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_FREIGHT_TERMS_VAL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_TERMS_HEAD_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error           IN OUT   BOOLEAN,
                                I_rec             IN       C_SVC_TERMS_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_TERMS.PROCESS_TERMS_HEAD_VAL';
   L_table   VARCHAR2(64):= 'SVC_TERMS_HEAD';
   L_exists  VARCHAR2(1)  := 'N';

   cursor C_CHK_CHILD_EXISTS is
   select 'Y'
     from dual
    where exists (select terms from ORDHEAD where terms = I_rec.terms and rownum=1
                  union all
                  select terms from PARTNER where terms = I_rec.terms and rownum=1
                  union all
                  select terms from SUPS where terms = I_rec.terms and rownum=1
                  union all
                  select terms from INVC_HEAD where terms = I_rec.terms and rownum=1
                  union all
                  select terms from CONTRACT_HEADER where terms = I_rec.terms and rownum=1
                  union all
                  select terms from FIF_INVC_HEADERS_EXPORT where terms = I_rec.terms and rownum=1
                  union all
                  select terms from IB_RESULTS where terms = I_rec.terms and rownum=1
                  union all
                  select terms from IIF_HEAD where terms = I_rec.terms and rownum=1
--Removed reference to ReIM tables so that RMS can be installed without ReIM
--                  union all
--                  select terms from IM_DOC_HEAD where terms = I_rec.terms and rownum=1
--                  union all
--                  select terms from IM_INJECT_DOC_HEAD where terms = I_rec.terms and rownum=1
                  union all
                  select terms from SIM_EXPL where terms = I_rec.terms and rownum=1);

BEGIN

   if I_rec.action = action_del then
      if I_rec.cnt_details != 0 then
         update svc_terms_detail std
            set process$status ='E'
          where std.terms = I_rec.terms;

         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'TERMS',
                     'INV_PARENT_CHILD_ACT');
           O_error := TRUE;

      end if;

      open C_CHK_CHILD_EXISTS;
      fetch C_CHK_CHILD_EXISTS  into L_exists;
      if C_CHK_CHILD_EXISTS%FOUND then

         update svc_terms_detail std
            set process$status ='E'
          where std.terms = I_rec.terms;

         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'TERMS',
                     'CHILD_EXISTS');
         O_error := TRUE;
       end if;
       close C_CHK_CHILD_EXISTS;
   end if;

   if I_rec.action = action_new then
      if I_rec.cnt_details1 != 0 then
         update svc_terms_detail std
            set process$status ='E'
          where terms = I_rec.terms;  

         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'TERMS',
                     'INV_PARENT_CHILD_ACT');
           O_error := TRUE;

      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TERMS_HEAD_VAL;
--------------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_TERMS_INS(  O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_freight_terms_temp_rec   IN       FREIGHT_TERMS%ROWTYPE,
                                  I_rec                      IN       C_SVC_FREIGHT_TERMS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_TERMS.EXEC_FREIGHT_TERMS_INS';
   L_table   VARCHAR2(64):= 'SVC_FREIGHT_TERMS';
BEGIN
   SAVEPOINT INS;

   insert
     into freight_terms
   values I_freight_terms_temp_rec;

   if MERGE_FREIGHT_TERMS_TL(O_error_message,
                             I_rec.freight_terms,
                             I_rec.term_desc,
                             LP_primary_lang) = FALSE then
      ROLLBACK TO SAVEPOINT INS;
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_FREIGHT_TERMS_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_TERMS_UPD( O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_freight_terms_temp_rec   IN       FREIGHT_TERMS%ROWTYPE,
                                 I_rec                      IN       C_SVC_FREIGHT_TERMS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64):= 'CORESVC_TERMS.EXEC_FREIGHT_TERMS_UPD';
   L_table         VARCHAR2(64):= 'SVC_FREIGHT_TERMS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   cursor C_LOCK_FREIGHT_TERMS is
      select 'X' 
        from freight_terms
       where freight_terms = I_freight_terms_temp_rec.freight_terms;
BEGIN

   open C_LOCK_FREIGHT_TERMS;
   close C_LOCK_FREIGHT_TERMS;

   if MERGE_FREIGHT_TERMS_TL(O_error_message,
                             I_rec.freight_terms,
                             I_rec.term_desc,
                             LP_primary_lang) = FALSE then
      return FALSE;
   end if;
   
   update freight_terms
      set row = I_freight_terms_temp_rec
    where 1 = 1
      and freight_terms = I_freight_terms_temp_rec.freight_terms;
      
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_freight_terms_temp_rec.freight_terms,
                                             NULL);
      if C_LOCK_FREIGHT_TERMS%ISOPEN then
         close C_LOCK_FREIGHT_TERMS;
      end if;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_FREIGHT_TERMS%ISOPEN then
         close C_LOCK_FREIGHT_TERMS;
      end if;
      return FALSE;
END EXEC_FREIGHT_TERMS_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_TERMS_DEL( O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_freight_terms_temp_rec   IN   FREIGHT_TERMS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64):= 'CORESVC_TERMS.EXEC_FREIGHT_TERMS_DEL';
   L_table         VARCHAR2(64):= 'SVC_FREIGHT_TERMS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   cursor C_LOCK_FREIGHT_TERMS_DEL is
   select 'X' 
     from freight_terms
    where freight_terms = I_freight_terms_temp_rec.freight_terms
      for update nowait;

BEGIN

   open C_LOCK_FREIGHT_TERMS_DEL;
   close C_LOCK_FREIGHT_TERMS_DEL;

   --Delete in freight_terms_tl table
   if DEL_FREIGHT_TERMS_TL(O_error_message,
                           I_freight_terms_temp_rec.freight_terms) = FALSE then
      return FALSE;
   end if;

   delete
     from freight_terms
    where 1 = 1
      and freight_terms = I_freight_terms_temp_rec.freight_terms;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_freight_terms_temp_rec.freight_terms,
                                             NULL);
      if C_LOCK_FREIGHT_TERMS_DEL%ISOPEN then
         close C_LOCK_FREIGHT_TERMS_DEL;
      end if;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_FREIGHT_TERMS_DEL%ISOPEN then
         close C_LOCK_FREIGHT_TERMS_DEL;
      end if;
      return FALSE;
END EXEC_FREIGHT_TERMS_DEL;
--------------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_TERMS_TL_INS( O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE, 
                                    I_freight_terms_tl_temp_rec   IN       FREIGHT_TERMS_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_TERMS.EXEC_FREIGHT_TERMS_TL_INS';

BEGIN
   insert into freight_terms_tl
      values I_freight_terms_tl_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_FREIGHT_TERMS_TL_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_TERMS_TL_UPD( O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_freight_terms_tl_temp_rec   IN       FREIGHT_TERMS_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64):= 'CORESVC_TERMS.EXEC_FREIGHT_TERMS_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FREIGHT_TERMS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_FREIGHT_TERMS_TL_UPD is
      select 'x'
        from freight_terms_tl
       where freight_terms = I_freight_terms_tl_temp_rec.freight_terms
         and lang = I_freight_terms_tl_temp_rec.lang
         for update nowait;

BEGIN
   open C_LOCK_FREIGHT_TERMS_TL_UPD;
   close C_LOCK_FREIGHT_TERMS_TL_UPD;

   update freight_terms_tl
      set term_desc = I_freight_terms_tl_temp_rec.term_desc,
          orig_lang_ind = I_freight_terms_tl_temp_rec.orig_lang_ind,
          reviewed_ind = I_freight_terms_tl_temp_rec.reviewed_ind,
          last_update_id = I_freight_terms_tl_temp_rec.last_update_id,
          last_update_datetime = I_freight_terms_tl_temp_rec.last_update_datetime
    where lang = I_freight_terms_tl_temp_rec.lang
      and freight_terms = I_freight_terms_tl_temp_rec.freight_terms;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_freight_terms_tl_temp_rec.freight_terms,
                                             I_freight_terms_tl_temp_rec.lang);
      close C_LOCK_FREIGHT_TERMS_TL_UPD;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_FREIGHT_TERMS_TL_UPD%ISOPEN then
         close C_LOCK_FREIGHT_TERMS_TL_UPD;
      end if;
      return FALSE;
END EXEC_FREIGHT_TERMS_TL_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_TERMS_TL_DEL( O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE, 
                                    I_freight_terms_tl_temp_rec   IN       FREIGHT_TERMS_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64):= 'CORESVC_TERMS.EXEC_FREIGHT_TERMS_TL_DEL';
   L_table         VARCHAR2(64):= 'SVC_FREIGHT_TERMS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_FREIGHT_TERMS_TL_DEL is
      select 'x'
        from freight_terms_tl
       where freight_terms = I_freight_terms_tl_temp_rec.freight_terms
         and lang = I_freight_terms_tl_temp_rec.lang
         for update nowait;
BEGIN
   open C_LOCK_FREIGHT_TERMS_TL_DEL;
   close C_LOCK_FREIGHT_TERMS_TL_DEL;

   delete freight_terms_tl
    where lang = I_freight_terms_tl_temp_rec.lang
      and freight_terms = I_freight_terms_tl_temp_rec.freight_terms;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_freight_terms_tl_temp_rec.freight_terms,
                                             I_freight_terms_tl_temp_rec.lang);
      close C_LOCK_FREIGHT_TERMS_TL_DEL;
      return FALSE;   
   when OTHERS then	  
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_FREIGHT_TERMS_TL_DEL%ISOPEN then
         close C_LOCK_FREIGHT_TERMS_TL_DEL;
      end if;
      return FALSE;
END EXEC_FREIGHT_TERMS_TL_DEL;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TERMS_HEAD_INS( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                              I_terms_head_temp_rec   IN       TERMS_HEAD%ROWTYPE,
                              I_rec                   IN       C_SVC_TERMS_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_TERMS.EXEC_TERMS_HEAD_INS';
   L_table   VARCHAR2(64):= 'SVC_TERMS_HEAD';
   
BEGIN
   SAVEPOINT HEAD_INS;
   
   insert
     into terms_head
   values I_terms_head_temp_rec;
   
   if MERGE_TERMS_HEAD_TL(O_error_message,
                          I_rec.terms,
                          I_rec.terms_desc,
                          I_rec.terms_code,
                          LP_primary_lang) = FALSE then
      ROLLBACK TO SAVEPOINT HEAD_INS;
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TERMS_HEAD_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TERMS_HEAD_UPD( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_terms_head_temp_rec   IN       TERMS_HEAD%ROWTYPE,
                              I_rec                   IN       C_SVC_TERMS_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_TERMS.EXEC_TERMS_HEAD_UPD';
   L_table   VARCHAR2(64):= 'SVC_TERMS_HEAD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);   
   
   cursor C_LOCK_TERMS_HEAD_UPD is
      select 'x'
        from terms_head
       where terms = I_terms_head_temp_rec.terms
         for update nowait;	   
BEGIN

   open C_LOCK_TERMS_HEAD_UPD;
   close C_LOCK_TERMS_HEAD_UPD;

   if MERGE_TERMS_HEAD_TL(O_error_message,
                          I_rec.terms,
                          I_rec.terms_desc,
                          I_rec.terms_code,
                          LP_primary_lang) = FALSE then
      return FALSE;
   end if;
   
   update terms_head
      set row = I_terms_head_temp_rec
    where 1 = 1
      and terms = I_terms_head_temp_rec.terms;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_terms_head_temp_rec.terms,
                                             NULL);
      if C_LOCK_TERMS_HEAD_UPD%ISOPEN then
         close C_LOCK_TERMS_HEAD_UPD;
      end if;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_TERMS_HEAD_UPD%ISOPEN then
         close C_LOCK_TERMS_HEAD_UPD;
      end if;
      return FALSE;
END EXEC_TERMS_HEAD_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TERMS_HEAD_DEL( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_terms_head_temp_rec   IN       TERMS_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_TERMS.EXEC_TERMS_HEAD_DEL';
   L_table   VARCHAR2(64):= 'SVC_TERMS_HEAD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);   
   
   cursor C_LOCK_TERMS_HEAD_DEL is
      select 'x'
        from terms_head
       where terms = I_terms_head_temp_rec.terms
         for update nowait;
 
   cursor C_LOCK_TERMS_DETAIL_DEL is
      select 'x'
        from terms_detail
       where terms = I_terms_head_temp_rec.terms
         for update nowait;
BEGIN

   open C_LOCK_TERMS_DETAIL_DEL;
   close C_LOCK_TERMS_DETAIL_DEL;
 
   open C_LOCK_TERMS_HEAD_DEL;
   close C_LOCK_TERMS_HEAD_DEL;
  
   if DEL_TERMS_HEAD_TL(O_error_message,
                        I_terms_head_temp_rec.terms) = FALSE then
      return FALSE;
   end if;
   
   
   delete 
     from terms_detail td
    where terms = I_terms_head_temp_rec.terms;

   update svc_terms_detail
      set process$status = 'P'
    where terms = I_terms_head_temp_rec.terms;  

   delete
     from terms_head th
    where 1 = 1
      and terms = I_terms_head_temp_rec.terms;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_terms_head_temp_rec.terms,
                                             NULL);
     if C_LOCK_TERMS_DETAIL_DEL%ISOPEN then
         close C_LOCK_TERMS_DETAIL_DEL;
     end if; 
     if C_LOCK_TERMS_HEAD_DEL%ISOPEN then
         close C_LOCK_TERMS_HEAD_DEL;
     end if; 
     return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
     if C_LOCK_TERMS_DETAIL_DEL%ISOPEN then
         close C_LOCK_TERMS_DETAIL_DEL;
     end if; 
     if C_LOCK_TERMS_HEAD_DEL%ISOPEN then
         close C_LOCK_TERMS_HEAD_DEL;
     end if;
     return FALSE;
END EXEC_TERMS_HEAD_DEL;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TERMS_HEAD_TL_INS( O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE, 
                                 I_terms_head_tl_temp_rec   IN       TERMS_HEAD_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)  := 'CORESVC_TERMS.EXEC_TERMS_HEAD_TL_INS';
   L_table   VARCHAR2(64)  := 'SVC_TERMS_HEAD_TL';
BEGIN

   insert into terms_head_tl
      values I_terms_head_tl_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TERMS_HEAD_TL_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TERMS_HEAD_TL_UPD( O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_terms_head_tl_temp_rec   IN       TERMS_HEAD_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64):= 'CORESVC_TERMS.EXEC_TERMS_HEAD_TL_UPD';
   L_table         VARCHAR2(64):= 'SVC_TERMS_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TERMS_HEAD_TL is
      select 'x'
        from terms_head_tl
       where terms = I_terms_head_tl_temp_rec.terms 
         and lang  = I_terms_head_tl_temp_rec.lang
       for update nowait;

BEGIN
   open C_LOCK_TERMS_HEAD_TL;
   close C_LOCK_TERMS_HEAD_TL;

   update terms_head_tl
      set terms_code = I_terms_head_tl_temp_rec.terms_code,
          terms_desc = I_terms_head_tl_temp_rec.terms_desc,
          orig_lang_ind = I_terms_head_tl_temp_rec.orig_lang_ind,
          reviewed_ind = I_terms_head_tl_temp_rec.reviewed_ind,
          last_update_datetime = sysdate,
          last_update_id = GET_USER
    where lang = I_terms_head_tl_temp_rec.lang
      and terms = I_terms_head_tl_temp_rec.terms;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_terms_head_tl_temp_rec.terms,
                                             I_terms_head_tl_temp_rec.lang);
      close C_LOCK_TERMS_HEAD_TL;
      return FALSE;
   when OTHERS then
      if C_LOCK_TERMS_HEAD_TL%ISOPEN then
         close C_LOCK_TERMS_HEAD_TL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TERMS_HEAD_TL_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TERMS_HEAD_TL_DEL( O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_terms_head_tl_temp_rec   IN       TERMS_HEAD_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_TERMS.EXEC_TERMS_HEAD_TL_DEL';
   L_table   VARCHAR2(64):= 'SVC_TERMS_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TERMS_HEAD_TL is
      select 'x'
        from terms_head_tl
       where terms = I_terms_head_tl_temp_rec.terms 
         and lang  = I_terms_head_tl_temp_rec.lang
       for update nowait;
BEGIN
   open C_LOCK_TERMS_HEAD_TL;
   close C_LOCK_TERMS_HEAD_TL;

   delete
     from terms_head_tl
    where lang = I_terms_head_tl_temp_rec.lang
      and terms = I_terms_head_tl_temp_rec.terms;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_terms_head_tl_temp_rec.terms,
                                             I_terms_head_tl_temp_rec.lang);
      close C_LOCK_TERMS_HEAD_TL;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_TERMS_HEAD_TL%ISOPEN then
         close C_LOCK_TERMS_HEAD_TL;
      end if;
      return FALSE;
END EXEC_TERMS_HEAD_TL_DEL;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TERMS_DETAIL_INS( O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE, 
                                L_terms_detail_temp_rec   IN       TERMS_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_TERMS.EXEC_TERMS_DETAIL_INS';
   L_table   VARCHAR2(64):= 'SVC_TERMS_DETAIL';
BEGIN

   insert
     into terms_detail
   values L_terms_detail_temp_rec;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TERMS_DETAIL_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TERMS_DETAIL_UPD( O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                L_terms_detail_temp_rec   IN       TERMS_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64):= 'CORESVC_TERMS.EXEC_TERMS_DETAIL_UPD';
   L_table         VARCHAR2(64):= 'SVC_TERMS_DETAIL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   cursor C_LOCK_TERM_DETAIL_UPD is
      select 'x'
        from terms_detail
       where terms_seq = L_terms_detail_temp_rec.terms_seq
         and terms = L_terms_detail_temp_rec.terms;	
BEGIN
   open C_LOCK_TERM_DETAIL_UPD;
   close C_LOCK_TERM_DETAIL_UPD;
   
   update terms_detail
      set row = L_terms_detail_temp_rec
    where 1 = 1
      and terms_seq = L_terms_detail_temp_rec.terms_seq
      and terms = L_terms_detail_temp_rec.terms;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_terms_detail_temp_rec.terms,
                                             L_terms_detail_temp_rec.terms_seq);
      if C_LOCK_TERM_DETAIL_UPD%ISOPEN then
         close C_LOCK_TERM_DETAIL_UPD;
      end if;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_TERM_DETAIL_UPD%ISOPEN then
         close C_LOCK_TERM_DETAIL_UPD;
      end if;
      return FALSE;
END EXEC_TERMS_DETAIL_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TERMS_DETAIL_DEL( O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                L_terms_detail_temp_rec   IN       TERMS_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64):= 'CORESVC_TERMS.EXEC_TERMS_DETAIL_DEL';
   L_table         VARCHAR2(64):= 'SVC_TERMS_DETAIL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   cursor C_LOCK_TERM_DETAIL_DEL is
      select 'x'
        from terms_detail
       where terms_seq = L_terms_detail_temp_rec.terms_seq
         and terms = L_terms_detail_temp_rec.terms;
BEGIN

   open C_LOCK_TERM_DETAIL_DEL;
   close C_LOCK_TERM_DETAIL_DEL;

   delete
     from terms_detail
    where 1 = 1
      and terms_seq = L_terms_detail_temp_rec.terms_seq
      and terms = L_terms_detail_temp_rec.terms;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_terms_detail_temp_rec.terms,
                                             L_terms_detail_temp_rec.terms_seq);
      if C_LOCK_TERM_DETAIL_DEL%ISOPEN then
         close C_LOCK_TERM_DETAIL_DEL;
      end if;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_TERM_DETAIL_DEL%ISOPEN then
         close C_LOCK_TERM_DETAIL_DEL;
      end if;
      return FALSE;
END EXEC_TERMS_DETAIL_DEL;
--------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   SVC_FREIGHT_TERMS.PROCESS_ID%TYPE) IS

BEGIN
   delete from svc_freight_terms_tl
         where process_id= I_process_id;

   delete from svc_freight_terms
         where process_id= I_process_id;

   delete from svc_terms_head_tl
         where process_id= I_process_id;

   delete from svc_terms_detail
         where process_id= I_process_id;

   delete from svc_terms_head
         where process_id= I_process_id;

END CLEAR_STAGING_DATA;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_FREIGHT_TERMS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN       SVC_FREIGHT_TERMS.PROCESS_ID%TYPE,
                                I_chunk_id        IN       SVC_FREIGHT_TERMS.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error                    BOOLEAN := FALSE;
   L_process_error            BOOLEAN := FALSE;
   L_program                  VARCHAR2(64) := 'CORESVC_TERMS.PROCESS_FREIGHT_TERMS';
   L_FREIGHT_TERMS_temp_rec   FREIGHT_TERMS%ROWTYPE;
   L_table                    VARCHAR2(64) := 'SVC_FREIGHT_TERMS';
   L_base_trans_table         VARCHAR2(64) := 'FREIGHT_TERMS';
   
BEGIN
   FOR rec IN c_svc_FREIGHT_TERMS(I_process_id,
                                  I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,
                               action_mod,
                               action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

      if rec.action = action_new
         and rec.FREIGHT_TERMS is NOT NULL
         and rec.PK_FREIGHT_TERMS_rid is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               L_base_trans_table,
                                               NULL,
                                               NULL);
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FREIGHT_TERMS',
                     O_error_message);
         L_error :=TRUE;
      end if;

      if NOT(  rec.FREIGHT_TERMS  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'FREIGHT_TERMS',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.FREIGHT_TERMS is NOT NULL
         and rec.PK_FREIGHT_TERMS_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FREIGHT_TERMS',
                     'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;

      -- Check if given terms description is null when action is NEW or MOD
      if rec.action IN (action_new,action_mod)   then
         if NOT( rec.TERM_DESC is NOT NULL )   then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'TERM_DESC',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;

         if NOT(  rec.ENABLED_FLAG  IS NOT NULL ) then
             WRITE_ERROR(I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                         'ENABLED_FLAG',
                         'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;

         if NOT(  rec.ENABLED_FLAG IN  ( 'Y', 'N' )  ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                       'INV_Y_N_IND');
            L_error :=TRUE;
         end if;

         if (rec.end_date_active is not null and rec.start_date_active is null) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'START_DATE_ACTIVE',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;
         
         if (rec.end_date_active is not null and  rec.start_date_active is not null and rec.start_date_active >= rec.end_date_active) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'START_DATE_ACTIVE',
                        'INV_VALUE');
            L_error :=TRUE;
         end if;
      end if;

      if PROCESS_FREIGHT_TERMS_VAL(O_error_message,
                                   L_error,
                                   rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_freight_terms_temp_rec.end_date_active     := rec.end_date_active;
         L_freight_terms_temp_rec.enabled_flag        := rec.enabled_flag;
         L_freight_terms_temp_rec.freight_terms       := rec.freight_terms;
         L_freight_terms_temp_rec.start_date_active   := rec.start_date_active;

         if rec.action = action_new then
            if EXEC_FREIGHT_TERMS_INS( O_error_message,
                                       L_freight_terms_temp_rec,
                                       rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_FREIGHT_TERMS_UPD( O_error_message,
                                       L_freight_terms_temp_rec,
                                       rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_FREIGHT_TERMS_DEL( O_error_message,
                                       L_freight_terms_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_FREIGHT_TERMS;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_FREIGHT_TERMS_TL( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id    IN     SVC_FREIGHT_TERMS_TL.PROCESS_ID%TYPE,
                                   I_chunk_id      IN     SVC_FREIGHT_TERMS_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS

   L_error                       BOOLEAN := FALSE;
   L_process_error               BOOLEAN := FALSE;
   L_program                     VARCHAR2(64):='CORESVC_TERMS.PROCESS_FREIGHT_TERMS_TL';
   L_FREIGHT_TERMS_TL_temp_rec   FREIGHT_TERMS_TL%ROWTYPE;
   L_table                       VARCHAR2(64) := 'SVC_FREIGHT_TERMS_TL';
   L_base_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FREIGHT_TERMS';
   L_base_trans_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FREIGHT_TERMS_TL';

   cursor C_SVC_FREIGHT_TERMS_TL(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select pk_freight_terms_tl.rowid  AS pk_freight_terms_tl_rid,
             st.rowid AS st_rid,
             ftl_lng_fk.rowid    AS ftl_lng_fk_rid,
             ftl_frt_fk.rowid    AS ftl_frt_fk_rid,
             freight_terms_tl_prim.rowid AS freight_terms_tl_prim_rid,
             st.reviewed_ind,
             st.orig_lang_ind,
             st.term_desc,
             st.lang,
             st.freight_terms,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_freight_terms_tl st,
             freight_terms_tl pk_freight_terms_tl,
             lang ftl_lng_fk,
             freight_terms ftl_frt_fk,
             (select freight_terms
                from freight_terms_tl
               where lang = LP_primary_lang) freight_terms_tl_prim
       where st.process_id    = I_process_id
         and st.chunk_id      = I_chunk_id
         and st.freight_terms = pk_freight_terms_tl.freight_terms (+)
         and st.lang          = pk_freight_terms_tl.lang (+)
         and st.lang          = ftl_lng_fk.lang (+)
         and st.freight_terms = ftl_frt_fk.freight_terms (+)
         and st.freight_terms = freight_terms_tl_prim.freight_terms (+);

   TYPE SVC_FREIGHT_TERMS_TL is TABLE OF C_SVC_FREIGHT_TERMS_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_freight_terms_tl_tab       SVC_FREIGHT_TERMS_TL;

   L_FREIGHT_TERMS_TL_ins_tab   FREIGHT_TERMS_TL_tab := NEW FREIGHT_TERMS_TL_tab();
   L_FREIGHT_TERMS_TL_mod_tab   FREIGHT_TERMS_TL_tab := NEW FREIGHT_TERMS_TL_tab();
   L_FREIGHT_TERMS_TL_del_tab   FREIGHT_TERMS_TL_tab := NEW FREIGHT_TERMS_TL_tab();

BEGIN
   if C_SVC_FREIGHT_TERMS_TL%ISOPEN then
      close C_SVC_FREIGHT_TERMS_TL;
   end if;

   open C_SVC_FREIGHT_TERMS_TL(I_process_id,
                               I_chunk_id);
   LOOP
      fetch C_SVC_FREIGHT_TERMS_TL bulk collect into L_freight_terms_tl_tab limit LP_bulk_fetch_limit;
      if L_freight_terms_tl_tab.COUNT > 0 then
         FOR i in L_freight_terms_tl_tab.FIRST..L_freight_terms_tl_tab.LAST LOOP
            L_error := FALSE;

            if L_freight_terms_tl_tab(i).action is NULL
               or L_freight_terms_tl_tab(i).action NOT IN (action_new,action_mod,action_del) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_freight_terms_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=true;
            end if;

            --check for primary_lang
            if L_freight_terms_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_freight_terms_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
                  continue;
            end if;

            if L_freight_terms_tl_tab(i).action = action_new
               and L_freight_terms_tl_tab(i).PK_FREIGHT_TERMS_TL_rid is NOT NULL then
               O_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_freight_terms_tl_tab(i).row_seq,
                           NULL,
                           O_error_message);
               L_error :=TRUE;
            end if;

            if L_freight_terms_tl_tab(i).action IN (action_mod,action_del)
               and L_freight_terms_tl_tab(i).FREIGHT_TERMS is NOT NULL
               and L_freight_terms_tl_tab(i).LANG is NOT NULL
               and L_freight_terms_tl_tab(i).PK_FREIGHT_TERMS_TL_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_freight_terms_tl_tab(i).row_seq,
                           NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_freight_terms_tl_tab(i).action = action_new
               and L_freight_terms_tl_tab(i).lang is NOT NULL
               and L_freight_terms_tl_tab(i).lang != LP_primary_lang
               and L_freight_terms_tl_tab(i).ftl_lng_fk_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_freight_terms_tl_tab(1).row_seq,
                           'LANG',
                           'PRIMARY_LANG_REQ');
               L_error :=TRUE;
            end if;

            if L_freight_terms_tl_tab(i).action = action_new
               and L_freight_terms_tl_tab(i).FREIGHT_TERMS is NOT NULL
               and L_freight_terms_tl_tab(i).ftl_frt_fk_rid is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_freight_terms_tl_tab(i).row_seq,
                           NULL,
                           O_error_message);

               L_error :=TRUE;
            end if;

            if L_freight_terms_tl_tab(i).action = action_new
               and L_freight_terms_tl_tab(i).lang is NOT NULL
               and L_freight_terms_tl_tab(i).ftl_lng_fk_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_freight_terms_tl_tab(1).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;
            
            if L_freight_terms_tl_tab(i).action IN (action_mod,action_new) then
               if NOT(  L_freight_terms_tl_tab(i).FREIGHT_TERMS  IS NOT NULL ) then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_freight_terms_tl_tab(i).row_seq,
                              'FREIGHT_TERMS',
                              'FIELD_NOT_NULL');
                  L_error :=TRUE;
               end if;
               
               if NOT(  L_freight_terms_tl_tab(i).LANG  IS NOT NULL ) then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_freight_terms_tl_tab(i).row_seq,
                              'LANG',
                              'FIELD_NOT_NULL');
                  L_error :=TRUE;
               end if;
               
               if NOT(  L_freight_terms_tl_tab(i).TERM_DESC  IS NOT NULL ) then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_freight_terms_tl_tab(i).row_seq,
                              'TERM_DESC',
                              'FIELD_NOT_NULL');
                  L_error :=TRUE;
               end if;
            end if;

            if NOT L_error then
               L_freight_terms_tl_temp_rec.freight_terms          := L_freight_terms_tl_tab(i).freight_terms;
               L_freight_terms_tl_temp_rec.lang                   := L_freight_terms_tl_tab(i).lang;
               L_freight_terms_tl_temp_rec.term_desc              := L_freight_terms_tl_tab(i).term_desc;
               L_freight_terms_tl_temp_rec.orig_lang_ind          := 'N';
               L_freight_terms_tl_temp_rec.reviewed_ind           := 'Y';
               L_freight_terms_tl_temp_rec.create_id              := GET_USER;
               L_freight_terms_tl_temp_rec.create_datetime        := SYSDATE;
               L_freight_terms_tl_temp_rec.last_update_id         := GET_USER;
               L_freight_terms_tl_temp_rec.last_update_datetime   := SYSDATE;

               if L_freight_terms_tl_tab(i).action = action_new then
                  if EXEC_FREIGHT_TERMS_TL_INS( O_error_message,
                                                L_freight_terms_tl_temp_rec)=FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_freight_terms_tl_tab(i).row_seq,
                                 L_freight_terms_tl_tab(i).action,
                                 O_error_message);
                     L_process_error :=TRUE;
                  end if;
               end if;

               if L_freight_terms_tl_tab(i).action = action_mod then
                  if EXEC_FREIGHT_TERMS_TL_UPD( O_error_message,
                                                L_freight_terms_tl_temp_rec)=FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_freight_terms_tl_tab(i).row_seq,
                                 L_freight_terms_tl_tab(i).action,
                                 O_error_message);
                     L_process_error :=TRUE;
                  end if;
               end if;

               if L_freight_terms_tl_tab(i).action = action_del then
                  if EXEC_FREIGHT_TERMS_TL_DEL( O_error_message,
                                                L_freight_terms_tl_temp_rec)=FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_freight_terms_tl_tab(i).row_seq,
                                 L_freight_terms_tl_tab(i).action,
                                 O_error_message);
                     L_process_error :=TRUE;
                  end if;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_FREIGHT_TERMS_TL%NOTFOUND;
   END LOOP;
   close C_SVC_FREIGHT_TERMS_TL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_SVC_FREIGHT_TERMS_TL%ISOPEN then
         close C_SVC_FREIGHT_TERMS_TL;
      end if;
      return FALSE;
END PROCESS_FREIGHT_TERMS_TL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_TERMS_HEAD( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE, 
                             I_process_id      IN       SVC_TERMS_HEAD.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_TERMS_HEAD.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN := FALSE;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(64):='CORESVC_TERMS.PROCESS_TERMS_HEAD';
   L_TERMS_HEAD_temp_rec TERMS_HEAD%ROWTYPE;
   L_table VARCHAR2(64)    :='SVC_TERMS_HEAD';
   L_base_trans_table SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TERMS_HEAD';
   L_exists VARCHAR2(1) := 'y';

   cursor C_CHK_TL_EXISTS(c_terms varchar2) is
      select 'x'
        from terms_head_tl tl
       where tl.terms = c_terms
         and tl.lang  = LP_primary_lang;

BEGIN
   FOR rec IN c_svc_TERMS_HEAD(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

      if rec.action = action_new
         and rec.PK_TERMS_HEAD_rid is NOT NULL then
         dbms_output.put_line('PK_TERMS_HEAD');
         O_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                L_base_trans_table,
                                                NULL,
                                                NULL);
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TERMS',
                     O_error_message);
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.PK_TERMS_HEAD_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TERMS',
                     'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;

      if NOT(  rec.TERMS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TERMS',
                     'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;
      if rec.ACTION in (action_mod, action_new) then
         if NOT(  rec.RANK  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'RANK',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;

         -- Check if given terms description is null when action is NEW or MOD
         if NOT( rec.TERMS_DESC is NOT NULL )   then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'TERM_DESC',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;

         if NOT( rec.TERMS_CODE is NOT NULL )   then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'TERMS_CODE',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;
      end if;

      if PROCESS_TERMS_HEAD_VAL(O_error_message,
                                L_error,
                                rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_terms_head_temp_rec.terms   := rec.terms;
         L_terms_head_temp_rec.rank    := rec.rank;
         if rec.action = action_new then
            if EXEC_TERMS_HEAD_INS( O_error_message,
                                    L_terms_head_temp_rec,
                                    rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_TERMS_HEAD_UPD( O_error_message,
                                    L_terms_head_temp_rec,
                                    rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_TERMS_HEAD_DEL( O_error_message,
                                    L_terms_head_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TERMS_HEAD;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_TERMS_HEAD_TL( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,   
                                I_process_id      IN       SVC_TERMS_HEAD_TL.PROCESS_ID%TYPE,
                                I_chunk_id        IN       SVC_TERMS_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_error                    BOOLEAN := FALSE;
   L_process_error            BOOLEAN := FALSE;
   L_program                  VARCHAR2(64) := 'CORESVC_TERMS.PROCESS_TERMS_HEAD_TL';
   L_TERMS_HEAD_TL_temp_rec   TERMS_HEAD_TL%ROWTYPE;
   L_table                    VARCHAR2(64) := 'SVC_TERMS_HEAD_TL';
   L_base_trans_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TERMS_HEAD_TL';

   cursor C_SVC_TERMS_HEAD_TL(I_process_id   NUMBER,
                              I_chunk_id     NUMBER) is
      select pk_terms_head_tl.rowid  AS pk_terms_head_tl_rid,
             st.rowid AS st_rid,
             ttl_thd_fk.rowid    AS ttl_thd_fk_rid,
             ttl_lng_fk.rowid    AS ttl_lng_fk_rid,
             st.reviewed_ind,
             st.orig_lang_ind,
             st.terms_desc,
             st.terms_code,
             st.lang,
             st.terms,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_terms_head_tl st,
             terms_head_tl pk_terms_head_tl,
             terms_head ttl_thd_fk,
             lang ttl_lng_fk
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.lang       = pk_terms_head_tl.lang(+)
         and st.terms      = pk_terms_head_tl.terms(+)
         and st.lang       = ttl_lng_fk.lang(+)
         and st.terms      = ttl_thd_fk.terms(+);

   TYPE SVC_TERMS_HEAD_TL is TABLE OF C_SVC_TERMS_HEAD_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_terms_head_tl_tab   SVC_TERMS_HEAD_TL;

   L_TERMS_HEAD_TL_ins_tab   TERMS_HEAD_TL_tab   := NEW TERMS_HEAD_TL_tab();
   L_TERMS_HEAD_TL_upd_tab   TERMS_HEAD_TL_tab   := NEW TERMS_HEAD_TL_tab();
   L_TERMS_HEAD_TL_del_tab   TERMS_HEAD_TL_tab   := NEW TERMS_HEAD_TL_tab();

BEGIN
   if C_SVC_TERMS_HEAD_TL%ISOPEN then
      close C_SVC_TERMS_HEAD_TL;
   end if;

   open C_SVC_TERMS_HEAD_TL(I_process_id,
                            I_chunk_id);
   LOOP
      fetch C_SVC_TERMS_HEAD_TL bulk collect into L_svc_terms_head_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_terms_head_tl_tab.COUNT > 0 then
         FOR i in L_svc_terms_head_tl_tab.FIRST..L_svc_terms_head_tl_tab.LAST 
         LOOP
            L_error := FALSE;
            L_error               := FALSE;
            L_process_error       := FALSE;
            
            --check for primary_lang
            if L_svc_terms_head_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_terms_head_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
                  continue;
            end if;

            if L_svc_terms_head_tl_tab(i).action is NULL
               or L_svc_terms_head_tl_tab(i).action NOT IN (action_new,action_mod,action_del) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_terms_head_tl_tab(i).row_seq,
                           NULL,
                           'INV_ACT');
               L_error :=true;
            end if;

            if L_svc_terms_head_tl_tab(i).action = action_new
               and L_svc_terms_head_tl_tab(i).PK_TERMS_HEAD_TL_rid is NOT NULL then

               O_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_terms_head_tl_tab(i).row_seq,
                           'TERMS, LANG',
                           O_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_terms_head_tl_tab(i).action IN (action_mod,action_del)
               and L_svc_terms_head_tl_tab(i).PK_TERMS_HEAD_TL_rid is NULL 
               and L_svc_terms_head_tl_tab(i).terms is NOT NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_terms_head_tl_tab(i).row_seq,
                           'TERMS, LANG',
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            if L_svc_terms_head_tl_tab(i).action = action_new then
               if L_svc_terms_head_tl_tab(i).ttl_thd_fk_rid is NULL then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_terms_head_tl_tab(i).row_seq,
                              'TERMS',
                              'INV_VALUE');
                  L_error :=TRUE;
               end if;

               if L_svc_terms_head_tl_tab(i).ttl_lng_fk_rid is NULL then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_terms_head_tl_tab(i).row_seq,
                              'LANG',
                              'LANG_EXIST');
                  L_error :=TRUE;
               end if;
            end if;

            if NOT(  L_svc_terms_head_tl_tab(i).TERMS  IS NOT NULL ) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_terms_head_tl_tab(i).row_seq,
                           'TERMS',
                           'MUST_ENTER_TERMS');
               L_error :=TRUE;
            end if;

            if NOT(  L_svc_terms_head_tl_tab(i).LANG  IS NOT NULL ) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_terms_head_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANG');
               L_error :=TRUE;
            end if;

            if L_svc_terms_head_tl_tab(i).action in (action_mod, action_new) then
               if NOT(  L_svc_terms_head_tl_tab(i).TERMS_CODE  IS NOT NULL ) then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_terms_head_tl_tab(i).row_seq,
                              'TERMS_CODE',
                              'FIELD_NOT_NULL');
                  L_error :=TRUE;
               end if;

               if NOT(  L_svc_terms_head_tl_tab(i).TERMS_DESC  IS NOT NULL ) then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_terms_head_tl_tab(i).row_seq,
                              'TERMS_DESC',
                              'FIELD_NOT_NULL');
                  L_error :=TRUE;
               end if;

            end if;

            if NOT L_error then
               L_terms_head_tl_temp_rec.terms                  := L_svc_terms_head_tl_tab(i).terms;
               L_terms_head_tl_temp_rec.lang                   := L_svc_terms_head_tl_tab(i).lang;
               L_terms_head_tl_temp_rec.terms_code             := L_svc_terms_head_tl_tab(i).terms_code;
               L_terms_head_tl_temp_rec.terms_desc             := L_svc_terms_head_tl_tab(i).terms_desc;
               L_terms_head_tl_temp_rec.orig_lang_ind          := 'N';
               L_terms_head_tl_temp_rec.reviewed_ind           := 'Y';
               L_terms_head_tl_temp_rec.create_id              := GET_USER;
               L_terms_head_tl_temp_rec.create_datetime        := SYSDATE;
               L_terms_head_tl_temp_rec.last_update_datetime   := SYSDATE;
               L_terms_head_tl_temp_rec.last_update_id         := GET_USER;

               if L_svc_terms_head_tl_tab(i).action = action_new then
                  if EXEC_TERMS_HEAD_TL_INS( O_error_message,
                                             L_terms_head_tl_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_svc_terms_head_tl_tab(i).row_seq,
                                 L_svc_terms_head_tl_tab(i).action,
                                 O_error_message);
                     L_process_error :=TRUE;
                  end if;
               end if;

               if L_svc_terms_head_tl_tab(i).action = action_mod then
                  if EXEC_TERMS_HEAD_TL_UPD( O_error_message,
                                             L_terms_head_tl_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_svc_terms_head_tl_tab(i).row_seq,
                                 L_svc_terms_head_tl_tab(i).action,
                                 O_error_message);
                     L_process_error :=TRUE;
                  end if;
               end if;

               if L_svc_terms_head_tl_tab(i).action = action_del then
                  if EXEC_TERMS_HEAD_TL_DEL( O_error_message,
                                             L_terms_head_tl_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_svc_terms_head_tl_tab(i).row_seq,
                                 L_svc_terms_head_tl_tab(i).action,
                                 O_error_message);
                     L_process_error :=TRUE;
                  end if;
               end if;

            end if;
         END LOOP;
      end if;
   EXIT WHEN C_SVC_TERMS_HEAD_TL%NOTFOUND;
   END LOOP;
   close C_SVC_TERMS_HEAD_TL;  
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TERMS_HEAD_TL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_TERMS_DETAIL( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,   
                               I_process_id      IN       SVC_TERMS_DETAIL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_TERMS_DETAIL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_error                   BOOLEAN := FALSE;
   L_process_error           BOOLEAN := FALSE;
   L_program                 VARCHAR2(64):='CORESVC_TERMS.PROCESS_TERMS_DETAIL';
   L_TERMS_DETAIL_temp_rec   TERMS_DETAIL%ROWTYPE;
   L_table                   VARCHAR2(64)    :='SVC_TERMS_DETAIL';
   L_exists                  VARCHAR2(1) := 'N';
   L_seq                     TERMS_DETAIL.TERMS_SEQ%TYPE;

   cursor C_SVC_TERMS_DETAIL(I_process_id NUMBER,
                             I_chunk_id   NUMBER) is
      select pk_terms_detail.rowid  AS pk_terms_detail_rid,
             st.rowid AS st_rid,
             fk_terms_head.rowid fk_terms_head_rid,
             st.cutoff_day,
             st.end_date_active,
             st.start_date_active,
             st.enabled_flag,
             st.fixed_date,
             st.disc_mm_fwd,
             st.disc_dom,
             st.percent,
             st.discdays,
             st.due_mm_fwd,
             st.due_dom,
             st.due_max_amount,
             st.duedays,
             st.terms_seq,
             st.terms,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_terms_detail st,
             terms_detail pk_terms_detail,
             terms_head fk_terms_head
       where st.process_id         = I_process_id
         and st.chunk_id           = I_chunk_id
         and st.terms_seq          = pk_terms_detail.terms_seq (+)
         and st.terms              = pk_terms_detail.terms (+)
         and st.terms              = fk_terms_head.terms(+)
         and nvl(st.process$status,'N')  = 'N';

   cursor C_CHK_CHILD_EXISTS(I_trms VARCHAR2) is
      select 'Y'
        from dual
       where exists (select terms from ORDHEAD where terms = I_trms and rownum=1
                     union all
                     select terms from PARTNER where terms = I_trms and rownum=1
                     union all
                     select terms from SUPS where terms = I_trms and rownum=1
                     union all
                     select terms from INVC_HEAD where terms = I_trms and rownum=1
                     union all
                     select terms from CONTRACT_HEADER where terms = I_trms and rownum=1
                     union all
                     select terms from FIF_INVC_HEADERS_EXPORT where terms = I_trms and rownum=1
                     union all
                     select terms from IB_RESULTS where terms = I_trms and rownum=1
                     union all
                     select terms from IIF_HEAD where terms = I_trms and rownum=1
--Removed reference to ReIM tables so that RMS can be installed without ReIM
--                     union all
--                     select terms from IM_DOC_HEAD where terms = I_trms and rownum=1
--                     union all
--                     select terms from IM_INJECT_DOC_HEAD where terms = I_trms and rownum=1
                     union all
                     select terms from SIM_EXPL where terms = I_trms and rownum=1);

BEGIN
   FOR rec IN c_svc_TERMS_DETAIL(I_process_id,
                                 I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

      if rec.action = action_new
         and rec.PK_TERMS_DETAIL_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TERMS, ROW_SEQ',
                     'REC_EXISTS_RMS');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.PK_TERMS_DETAIL_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                    'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;

      if rec.action = action_del then
         open C_CHK_CHILD_EXISTS(rec.terms);
         fetch C_CHK_CHILD_EXISTS  into L_exists;
         if C_CHK_CHILD_EXISTS%FOUND then
            WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           'CHILD_EXISTS');
             L_error := TRUE;
         end if;
         close C_CHK_CHILD_EXISTS;
      end if;

      if rec.action = action_new then
         if rec.fk_terms_head_rid is NULL 
            and rec.terms is NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'TERMS',
                        'INV_VALUE');
            L_error :=TRUE;
         end if;
      end if;

      if rec.action in (action_mod, action_new) then
         if NOT(  rec.TERMS  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'TERMS',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;
         
         if NOT(  rec.TERMS_SEQ  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'TERMS_SEQ',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;
         
         if NOT(  rec.DUE_MAX_AMOUNT  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DUE_MAX_AMOUNT',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;
         
         if NOT(  rec.PERCENT  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'PERCENT',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;
         
         if NOT(  rec.ENABLED_FLAG  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'ENABLED_FLAG',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;
         
         if NOT(  rec.CUTOFF_DAY  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CUTOFF_DAY',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;
         
         if NOT(  rec.DUE_DOM BETWEEN 1 AND 31 ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DUE_DOM',
                        'INV_VALUE');
            L_error :=TRUE;
         end if;
         
         if NOT(  rec.DISC_DOM BETWEEN 1 AND 31 ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DISC_DOM',
                        'INV_VALUE');
            L_error :=TRUE;
         end if;
         
         if NOT(  rec.CUTOFF_DAY BETWEEN 1 AND 31 ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CUTOFF_DAY',
                        'INV_VALUE');
            L_error :=TRUE;
         end if;
         
         if NOT(  ( rec.DUE_DOM IS NOT NULL AND rec.DUE_MM_FWD IS NOT NULL AND rec.DUEDAYS IS NULL )  OR  
                  ( rec.DUE_DOM IS NULL AND rec.DUE_MM_FWD IS NULL AND rec.DUEDAYS IS NOT NULL )  OR  
                  ( rec.DUE_DOM IS NOT NULL AND rec.DUE_MM_FWD IS NOT NULL AND rec.DUEDAYS IS NOT NULL )  ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DUE_DOM,DUE_MM_FWD,DUEDAYS',
                        'INV_VALUE');
            L_error :=TRUE;
         end if;
         
         if NOT(  ( rec.DISC_DOM IS NOT NULL AND rec.DISC_MM_FWD IS NOT NULL AND rec.DISCDAYS IS NULL )  OR  
                  ( rec.DISC_DOM IS NULL AND rec.DISC_MM_FWD IS NULL AND rec.DISCDAYS IS NOT NULL )  OR  
                  ( rec.DISC_DOM IS NULL AND rec.DISC_MM_FWD IS NULL AND rec.DISCDAYS IS NULL )  OR  
                  ( rec.DISC_DOM IS NOT NULL AND rec.DISC_MM_FWD IS NOT NULL AND rec.DISCDAYS IS NOT NULL )  ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DISC_DOM,DISC_MM_FWD,DISCDAYS',
                        'INV_DISC_DATE');
            L_error :=TRUE;
         end if;
         
         if NOT(  ( rec.ENABLED_FLAG IN ('Y','N'))  ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'ENABLED_FLAG',
                        'INV_Y_N_IND');
            L_error :=TRUE;
         end if;
      end if;

      if NOT L_error then

         if rec.action = action_new then
            L_seq := TERMS_SEQ.NEXTVAL;
         else
            L_seq := rec.terms_seq;
         end if;

         L_terms_detail_temp_rec.terms               := rec.terms;
         L_terms_detail_temp_rec.terms_seq           := L_seq;
         L_terms_detail_temp_rec.duedays             := rec.duedays;
         L_terms_detail_temp_rec.due_max_amount      := rec.due_max_amount;
         L_terms_detail_temp_rec.due_dom             := rec.due_dom;
         L_terms_detail_temp_rec.due_mm_fwd          := rec.due_mm_fwd;
         L_terms_detail_temp_rec.discdays            := rec.discdays;
         L_terms_detail_temp_rec.percent             := rec.percent;
         L_terms_detail_temp_rec.disc_dom            := rec.disc_dom;
         L_terms_detail_temp_rec.disc_mm_fwd         := rec.disc_mm_fwd;
         L_terms_detail_temp_rec.fixed_date          := rec.fixed_date;
         L_terms_detail_temp_rec.enabled_flag        := rec.enabled_flag;
         L_terms_detail_temp_rec.start_date_active   := rec.start_date_active;
         L_terms_detail_temp_rec.end_date_active     := rec.end_date_active;
         L_terms_detail_temp_rec.cutoff_day          := rec.cutoff_day;

         if rec.action = action_new then
            if EXEC_TERMS_DETAIL_INS( O_error_message,
                                      L_terms_detail_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_TERMS_DETAIL_UPD( O_error_message,
                                      L_terms_detail_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_TERMS_DETAIL_DEL( O_error_message,
                                      L_terms_detail_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TERMS_DETAIL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_TERMS.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab   := NEW errors_tab_typ();
   
   if PROCESS_FREIGHT_TERMS(O_error_message,
                            I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   if PROCESS_FREIGHT_TERMS_TL(O_error_message,
                               I_process_id,
                               I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_TERMS_HEAD(O_error_message,
                         I_process_id,
                         I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   if PROCESS_TERMS_HEAD_TL(O_error_message,
                            I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   if PROCESS_TERMS_DETAIL(O_error_message,
                           I_process_id,
                           I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   O_error_count := LP_errors_tab.COUNT();
   
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);

   LP_errors_tab := NEW errors_tab_typ();
   
   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------------
END CORESVC_TERMS;
/
