create or replace PACKAGE BODY STG_SVC_SA_FIF_GL_CROSS_REF
AS

	Type s9t_errors_tab_typ
	IS
	TABLE OF s9t_errors%rowtype;
	Lp_s9t_errors_tab s9t_errors_tab_typ;
	-----------------------------------------------
	PROCEDURE write_s9t_error(
	 I_file_id IN s9t_errors.file_id%type,
	 I_sheet   IN VARCHAR2,
	 I_row_seq IN NUMBER,
	 I_col     IN VARCHAR2,
	 I_sqlcode IN NUMBER,
	 I_sqlerrm IN VARCHAR2)
	IS
	BEGIN
	Lp_s9t_errors_tab.extend();
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).FILE_ID              := I_file_id;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_SEQ_NO         := s9t_errors_seq.nextval;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).TEMPLATE_KEY         := STG_SVC_SA_FIF_GL_CROSS_REF.template_key;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).WKSHT_KEY            := I_sheet;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).COLUMN_KEY           := I_col;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ROW_SEQ              := I_row_seq;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            := (
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_ID            := GET_USER;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_DATETIME      := sysdate;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_ID       := GET_USER;
	Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_DATETIME := sysdate;
	END write_s9t_error;
	----------------------------------------------------------------------
	FUNCTION populate_lists(
	 O_error_message IN OUT rtk_errors.rtk_text%type,
	 I_file_id       IN NUMBER)RETURN BOOLEAN;

------------------------------------------------------------------------------

	PROCEDURE populate_names
	(
	 I_file_id NUMBER
	)
	IS
	l_sheets s9t_pkg.names_map_typ;
	SA_FIF_GL_CROSS_REF_cols s9t_pkg.names_map_typ;
	BEGIN
	l_sheets               :=s9t_pkg.get_sheet_names(I_file_id);
	SA_FIF_GL_CROSS_REF_cols              :=s9t_pkg.get_col_names(I_file_id,SA_FIF_GL_CROSS_REF_sheet);
	SA_GL_CROSS$Action            := SA_FIF_GL_CROSS_REF_cols('ACTION');
	SA_GL_CROSS$CR_CCID              := SA_FIF_GL_CROSS_REF_cols('CR_CCID');
	SA_GL_CROSS$DR_SEQUENCE10              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE10');
	SA_GL_CROSS$DR_SEQUENCE9              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE9');
	SA_GL_CROSS$DR_SEQUENCE8              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE8');
	SA_GL_CROSS$DR_SEQUENCE7              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE7');
	SA_GL_CROSS$DR_SEQUENCE6              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE6');
	SA_GL_CROSS$DR_SEQUENCE5              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE5');
	SA_GL_CROSS$DR_SEQUENCE4              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE4');
	SA_GL_CROSS$DR_SEQUENCE3              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE3');
	SA_GL_CROSS$DR_SEQUENCE2              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE2');
	SA_GL_CROSS$DR_SEQUENCE1              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE1');
	SA_GL_CROSS$DR_CCID              := SA_FIF_GL_CROSS_REF_cols('DR_CCID');
	SA_GL_CROSS$SET_OF_BOOKS_ID              := SA_FIF_GL_CROSS_REF_cols('SET_OF_BOOKS_ID');
	SA_GL_CROSS$TOTAL_ID              := SA_FIF_GL_CROSS_REF_cols('TOTAL_ID');
	SA_GL_CROSS$CR_SEQUENCE20              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE20');
	SA_GL_CROSS$ROLLUP_LEVEL_3              := SA_FIF_GL_CROSS_REF_cols('ROLLUP_LEVEL_3');
	SA_GL_CROSS$ROLLUP_LEVEL_2              := SA_FIF_GL_CROSS_REF_cols('ROLLUP_LEVEL_2');
	SA_GL_CROSS$ROLLUP_LEVEL_1              := SA_FIF_GL_CROSS_REF_cols('ROLLUP_LEVEL_1');
	SA_GL_CROSS$STORE              := SA_FIF_GL_CROSS_REF_cols('STORE');
	SA_GL_CROSS$CR_SEQUENCE19              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE19');
	SA_GL_CROSS$CR_SEQUENCE18              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE18');
	SA_GL_CROSS$CR_SEQUENCE17              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE17');
	SA_GL_CROSS$CR_SEQUENCE16              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE16');
	SA_GL_CROSS$CR_SEQUENCE15              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE15');
	SA_GL_CROSS$CR_SEQUENCE14              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE14');
	SA_GL_CROSS$CR_SEQUENCE13              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE13');
	SA_GL_CROSS$CR_SEQUENCE12              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE12');
	SA_GL_CROSS$CR_SEQUENCE11              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE11');
	SA_GL_CROSS$DR_SEQUENCE20              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE20');
	SA_GL_CROSS$DR_SEQUENCE19              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE19');
	SA_GL_CROSS$DR_SEQUENCE18              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE18');
	SA_GL_CROSS$DR_SEQUENCE17              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE17');
	SA_GL_CROSS$DR_SEQUENCE16              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE16');
	SA_GL_CROSS$DR_SEQUENCE15              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE15');
	SA_GL_CROSS$DR_SEQUENCE14              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE14');
	SA_GL_CROSS$DR_SEQUENCE13              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE13');
	SA_GL_CROSS$DR_SEQUENCE12              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE12');
	SA_GL_CROSS$DR_SEQUENCE11              := SA_FIF_GL_CROSS_REF_cols('DR_SEQUENCE11');
	SA_GL_CROSS$CR_SEQUENCE10              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE10');
	SA_GL_CROSS$CR_SEQUENCE9              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE9');
	SA_GL_CROSS$CR_SEQUENCE8              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE8');
	SA_GL_CROSS$CR_SEQUENCE7              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE7');
	SA_GL_CROSS$CR_SEQUENCE6              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE6');
	SA_GL_CROSS$CR_SEQUENCE5              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE5');
	SA_GL_CROSS$CR_SEQUENCE4              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE4');
	SA_GL_CROSS$CR_SEQUENCE3              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE3');
	SA_GL_CROSS$CR_SEQUENCE2              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE2');
	SA_GL_CROSS$CR_SEQUENCE1              := SA_FIF_GL_CROSS_REF_cols('CR_SEQUENCE1');
	end populate_names;
	-------------------------------------------------------------------------------
	---  Name: populate_SA_FIF_GL_CROSS_REF
	--Purpose: Populate data from SA_FIF_GL_CROSS_REF
	-------------------------------------------------------------------------------- 
	PROCEDURE populate_SA_FIF_GL_CROSS_REF
	(
	 I_file_id IN NUMBER
	)
	IS
	BEGIN
	INSERT
	INTO TABLE
	 (select ss.s9t_rows
		from s9t_folder sf,
		  TABLE(sf.s9t_file_obj.sheets) ss
		where sf.file_id  = I_file_id
		and ss.sheet_name = SA_FIF_GL_CROSS_REF_sheet
	 )
	select s9t_row(s9t_cells( stg_svc_sa_fif_gl_cross_ref.action_mod
									 ,STORE
									 ,TOTAL_ID
									 ,ROLLUP_LEVEL_1
									 ,ROLLUP_LEVEL_2
									 ,ROLLUP_LEVEL_3
									 ,SET_OF_BOOKS_ID
									 ,DR_CCID
									 ,DR_SEQUENCE1
									 ,DR_SEQUENCE2
									 ,DR_SEQUENCE3
									 ,DR_SEQUENCE4
									 ,DR_SEQUENCE5
									 ,DR_SEQUENCE6
									 ,DR_SEQUENCE7
									 ,DR_SEQUENCE8
									 ,DR_SEQUENCE9
									 ,DR_SEQUENCE10
									 ,DR_SEQUENCE11
									 ,DR_SEQUENCE12
									 ,DR_SEQUENCE13
									 ,DR_SEQUENCE14
									 ,DR_SEQUENCE15
									 ,DR_SEQUENCE16
									 ,DR_SEQUENCE17
									 ,DR_SEQUENCE18
									 ,DR_SEQUENCE19
									 ,DR_SEQUENCE20
									 ,CR_CCID
									 ,CR_SEQUENCE1
									 ,CR_SEQUENCE2
									 ,CR_SEQUENCE3
									 ,CR_SEQUENCE4
									 ,CR_SEQUENCE5
									 ,CR_SEQUENCE6
									 ,CR_SEQUENCE7
									 ,CR_SEQUENCE8
									 ,CR_SEQUENCE9
									 ,CR_SEQUENCE10
									 ,CR_SEQUENCE11
									 ,CR_SEQUENCE12
									 ,CR_SEQUENCE13
									 ,CR_SEQUENCE14
									 ,CR_SEQUENCE15
									 ,CR_SEQUENCE16
									 ,CR_SEQUENCE17
									 ,CR_SEQUENCE18
									 ,CR_SEQUENCE19
									 ,CR_SEQUENCE20
									))
	from SA_FIF_GL_CROSS_REF ;
	end populate_SA_FIF_GL_CROSS_REF;
	-------------------------------------------------------------------------------
	---  Name: init_s9t
	-------------------------------------------------------------------------------- 
	PROCEDURE init_s9t(
	 O_file_id IN OUT NUMBER)
	IS
	l_file s9t_file;
	l_file_name s9t_folder.file_name%type;
	
  BEGIN
		l_file              := NEW s9t_file();
		O_file_id           := s9t_folder_seq.nextval;
		l_file.file_id      := O_file_id;
		l_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
		l_file.file_name    := l_file_name;
		l_file.template_key := template_key;
		l_file.user_lang    := GET_USER_LANG;
		l_file.add_sheet(SA_FIF_GL_CROSS_REF_sheet);
		l_file.sheets(l_file.get_sheet_index(SA_FIF_GL_CROSS_REF_sheet)).column_headers := s9t_cells( 'ACTION',
																																	'STORE',
																																	'TOTAL_ID',
																																	'ROLLUP_LEVEL_1',
																																	'ROLLUP_LEVEL_2',
																																	'ROLLUP_LEVEL_3',
																																	'SET_OF_BOOKS_ID',
																																	'DR_CCID',
																																	'DR_SEQUENCE1',
																																	'DR_SEQUENCE2',
																																	'DR_SEQUENCE3',
																																	'DR_SEQUENCE4',
																																	'DR_SEQUENCE5',
																																	'DR_SEQUENCE6',
																																	'DR_SEQUENCE7',
																																	'DR_SEQUENCE8',
																																	'DR_SEQUENCE9',
																																	'DR_SEQUENCE10',
																																	'DR_SEQUENCE11',
																																	'DR_SEQUENCE12',
																																	'DR_SEQUENCE13',
																																	'DR_SEQUENCE14',
																																	'DR_SEQUENCE15',
																																	'DR_SEQUENCE16',
																																	'DR_SEQUENCE17',
																																	'DR_SEQUENCE18',
																																	'DR_SEQUENCE19',
																																	'DR_SEQUENCE20',
																																	'CR_CCID',
																																	'CR_SEQUENCE1',
																																	'CR_SEQUENCE2',
																																	'CR_SEQUENCE3',
																																	'CR_SEQUENCE4',
																																	'CR_SEQUENCE5',
																																	'CR_SEQUENCE6',
																																	'CR_SEQUENCE7',
																																	'CR_SEQUENCE8',
																																	'CR_SEQUENCE9',
																																	'CR_SEQUENCE10',
																																	'CR_SEQUENCE11',
																																	'CR_SEQUENCE12',
																																	'CR_SEQUENCE13',
																																	'CR_SEQUENCE14',
																																	'CR_SEQUENCE15',
																																	'CR_SEQUENCE16',
																																	'CR_SEQUENCE17',
																																	'CR_SEQUENCE18',
																																	'CR_SEQUENCE19',
																																	'CR_SEQUENCE20'
																																 );

		s9t_pkg.save_obj(l_file);
	end init_s9t;
	-------------------------------------------------------------------------------
	---  Name: create_s9t
	--Purpose: Called from UI to generate the download excel
	-------------------------------------------------------------------------------- 
	FUNCTION create_s9t(
	 O_error_message     IN OUT rtk_errors.rtk_text%type,
	 O_file_id           IN OUT s9t_folder.file_id%type,
	 I_template_only_ind IN CHAR DEFAULT 'N') RETURN BOOLEAN
	IS
		l_file s9t_file;
		L_program VARCHAR2(255):='SA_FIF_GL_CROSS_REF.create_s9t';
	BEGIN
		init_s9t(O_file_id);
		--populate column lists
		if populate_lists(O_error_message,O_file_id)=false THEN
		 RETURN false;
		end if;
		if I_template_only_ind = 'N' THEN
		---populate data
		   populate_SA_FIF_GL_CROSS_REF(O_file_id);
		commit;  
		end if;
		s9t_pkg.translate_to_user_lang(O_file_id);
		s9t_pkg.apply_template(O_file_id,stg_svc_sa_fif_gl_cross_ref.template_key);
		l_file:=s9t_file(O_file_id);
		if s9t_pkg.code2desc(O_error_message,
		                     'RSAGLC', 
								         l_file)=FALSE then
	     return FALSE;
		end if;
		s9t_pkg.save_obj(l_file);
		s9t_pkg.update_ods(l_file);
		commit;
		RETURN true;
	EXCEPTION
		WHEN OTHERS THEN
			O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
			RETURN FALSE;
	end create_s9t;
	-------------------------------------------------------------------------------
	---  Name: process_s9t_SA_GL_CROSS_REF
	--Purpose: Load data to staging table SVC_SA_FIF_GL_CROSS_REF
	-------------------------------------------------------------------------------- 
	PROCEDURE process_s9t_SA_GL_CROSS_REF
	(
	 I_file_id    IN s9t_folder.file_id%type,
	 I_process_id IN SVC_SA_FIF_GL_CROSS_REF.process_id%type
	)
	IS
		Type svc_SA_GL_CROSS_REF_col_typ
		IS
		TABLE OF SVC_SA_FIF_GL_CROSS_REF%rowtype;
		l_temp_rec SVC_SA_FIF_GL_CROSS_REF%rowtype;
		svc_SA_FIF_GL_CROSS_REF_col svc_SA_GL_CROSS_REF_col_typ :=NEW svc_SA_GL_CROSS_REF_col_typ();
		l_process_id SVC_SA_FIF_GL_CROSS_REF.process_id%type;
		l_error BOOLEAN:=false;
		l_default_rec SVC_SA_FIF_GL_CROSS_REF%rowtype;
		CURSOR c_mandatory_ind
		IS
		 select
			CR_CCID_mi,
			DR_SEQUENCE10_mi,
			DR_SEQUENCE9_mi,
			DR_SEQUENCE8_mi,
			DR_SEQUENCE7_mi,
			DR_SEQUENCE6_mi,
			DR_SEQUENCE5_mi,
			DR_SEQUENCE4_mi,
			DR_SEQUENCE3_mi,
			DR_SEQUENCE2_mi,
			DR_SEQUENCE1_mi,
			DR_CCID_mi,
			SET_OF_BOOKS_ID_mi,
			TOTAL_ID_mi,
			CR_SEQUENCE20_mi,
			ROLLUP_LEVEL_3_mi,
			ROLLUP_LEVEL_2_mi,
			ROLLUP_LEVEL_1_mi,
			STORE_mi,
			CR_SEQUENCE19_mi,
			CR_SEQUENCE18_mi,
			CR_SEQUENCE17_mi,
			CR_SEQUENCE16_mi,
			CR_SEQUENCE15_mi,
			CR_SEQUENCE14_mi,
			CR_SEQUENCE13_mi,
			CR_SEQUENCE12_mi,
			CR_SEQUENCE11_mi,
			DR_SEQUENCE20_mi,
			DR_SEQUENCE19_mi,
			DR_SEQUENCE18_mi,
			DR_SEQUENCE17_mi,
			DR_SEQUENCE16_mi,
			DR_SEQUENCE15_mi,
			DR_SEQUENCE14_mi,
			DR_SEQUENCE13_mi,
			DR_SEQUENCE12_mi,
			DR_SEQUENCE11_mi,
			CR_SEQUENCE10_mi,
			CR_SEQUENCE9_mi,
			CR_SEQUENCE8_mi,
			CR_SEQUENCE7_mi,
			CR_SEQUENCE6_mi,
			CR_SEQUENCE5_mi,
			CR_SEQUENCE4_mi,
			CR_SEQUENCE3_mi,
			CR_SEQUENCE2_mi,
			CR_SEQUENCE1_mi,
			1 as dummy
		 from
			(select column_key,
			  mandatory
			from s9t_tmpl_cols_def
			where template_key                              = STG_SVC_SA_FIF_GL_CROSS_REF.template_key
			and wksht_key                                   = 'SA_FIF_GL_CROSS_REF'
			) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN (
									'CR_CCID' AS CR_CCID,
									'DR_SEQUENCE10' AS DR_SEQUENCE10,
									'DR_SEQUENCE9' AS DR_SEQUENCE9,
									'DR_SEQUENCE8' AS DR_SEQUENCE8,
									'DR_SEQUENCE7' AS DR_SEQUENCE7,
									'DR_SEQUENCE6' AS DR_SEQUENCE6,
									'DR_SEQUENCE5' AS DR_SEQUENCE5,
									'DR_SEQUENCE4' AS DR_SEQUENCE4,
									'DR_SEQUENCE3' AS DR_SEQUENCE3,
									'DR_SEQUENCE2' AS DR_SEQUENCE2,
									'DR_SEQUENCE1' AS DR_SEQUENCE1,
									'DR_CCID' AS DR_CCID,
									'SET_OF_BOOKS_ID' AS SET_OF_BOOKS_ID,
									'TOTAL_ID' AS TOTAL_ID,
									'CR_SEQUENCE20' AS CR_SEQUENCE20,
									'ROLLUP_LEVEL_3' AS ROLLUP_LEVEL_3,
									'ROLLUP_LEVEL_2' AS ROLLUP_LEVEL_2,
									'ROLLUP_LEVEL_1' AS ROLLUP_LEVEL_1,
									'STORE' AS STORE,
									'CR_SEQUENCE19' AS CR_SEQUENCE19,
									'CR_SEQUENCE18' AS CR_SEQUENCE18,
									'CR_SEQUENCE17' AS CR_SEQUENCE17,
									'CR_SEQUENCE16' AS CR_SEQUENCE16,
									'CR_SEQUENCE15' AS CR_SEQUENCE15,
									'CR_SEQUENCE14' AS CR_SEQUENCE14,
									'CR_SEQUENCE13' AS CR_SEQUENCE13,
									'CR_SEQUENCE12' AS CR_SEQUENCE12,
									'CR_SEQUENCE11' AS CR_SEQUENCE11,
									'DR_SEQUENCE20' AS DR_SEQUENCE20,
									'DR_SEQUENCE19' AS DR_SEQUENCE19,
									'DR_SEQUENCE18' AS DR_SEQUENCE18,
									'DR_SEQUENCE17' AS DR_SEQUENCE17,
									'DR_SEQUENCE16' AS DR_SEQUENCE16,
									'DR_SEQUENCE15' AS DR_SEQUENCE15,
									'DR_SEQUENCE14' AS DR_SEQUENCE14,
									'DR_SEQUENCE13' AS DR_SEQUENCE13,
									'DR_SEQUENCE12' AS DR_SEQUENCE12,
									'DR_SEQUENCE11' AS DR_SEQUENCE11,
									'CR_SEQUENCE10' AS CR_SEQUENCE10,
									'CR_SEQUENCE9' AS CR_SEQUENCE9,
									'CR_SEQUENCE8' AS CR_SEQUENCE8,
									'CR_SEQUENCE7' AS CR_SEQUENCE7,
									'CR_SEQUENCE6' AS CR_SEQUENCE6,
									'CR_SEQUENCE5' AS CR_SEQUENCE5,
									'CR_SEQUENCE4' AS CR_SEQUENCE4,
									'CR_SEQUENCE3' AS CR_SEQUENCE3,
									'CR_SEQUENCE2' AS CR_SEQUENCE2,
									'CR_SEQUENCE1' AS CR_SEQUENCE1,
			null as dummy));
		l_mi_rec c_mandatory_ind%rowtype;
		 dml_errors EXCEPTION;
		 PRAGMA exception_init(dml_errors, -24381);
		 l_pk_columns varchar2(255):='total_id,store,set_of_books_id,rollup_level_1,rollup_level_2,rollup_level_3';
		  l_error_msg rtk_errors.rtk_text%type;
			l_error_code number;
      L_table        VARCHAR2(30)  := 'SVC_SA_FIF_GL_CROSS_REF';
	BEGIN
	-- Get default values.
		FOR rec IN
		(select
				CR_CCID_dv,
				DR_SEQUENCE10_dv,
				DR_SEQUENCE9_dv,
				DR_SEQUENCE8_dv,
				DR_SEQUENCE7_dv,
				DR_SEQUENCE6_dv,
				DR_SEQUENCE5_dv,
				DR_SEQUENCE4_dv,
				DR_SEQUENCE3_dv,
				DR_SEQUENCE2_dv,
				DR_SEQUENCE1_dv,
				DR_CCID_dv,
				SET_OF_BOOKS_ID_dv,
			  TOTAL_ID_dv,
				CR_SEQUENCE20_dv,
				ROLLUP_LEVEL_3_dv,
				ROLLUP_LEVEL_2_dv,
				ROLLUP_LEVEL_1_dv,
				STORE_dv,
				CR_SEQUENCE19_dv,
				CR_SEQUENCE18_dv,
				CR_SEQUENCE17_dv,
				CR_SEQUENCE16_dv,
				CR_SEQUENCE15_dv,
				CR_SEQUENCE14_dv,
				CR_SEQUENCE13_dv,
				CR_SEQUENCE12_dv,
				CR_SEQUENCE11_dv,
				DR_SEQUENCE20_dv,
				DR_SEQUENCE19_dv,
				DR_SEQUENCE18_dv,
				DR_SEQUENCE17_dv,
				DR_SEQUENCE16_dv,
				DR_SEQUENCE15_dv,
				DR_SEQUENCE14_dv,
				DR_SEQUENCE13_dv,
				DR_SEQUENCE12_dv,
				DR_SEQUENCE11_dv,
				CR_SEQUENCE10_dv,
				CR_SEQUENCE9_dv,
				CR_SEQUENCE8_dv,
				CR_SEQUENCE7_dv,
				CR_SEQUENCE6_dv,
				CR_SEQUENCE5_dv,
				CR_SEQUENCE4_dv,
				CR_SEQUENCE3_dv,
				CR_SEQUENCE2_dv,
				CR_SEQUENCE1_dv,
		 null as dummy
		from
		 (select column_key,
			default_value
		 from s9t_tmpl_cols_def
		 where template_key                                  = STG_SVC_SA_FIF_GL_CROSS_REF.template_key
		 and wksht_key                                       = 'SA_FIF_GL_CROSS_REF'
		 ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN (
				 'CR_CCID' AS CR_CCID,
				 'DR_SEQUENCE10' AS DR_SEQUENCE10,
				 'DR_SEQUENCE9' AS DR_SEQUENCE9,
				 'DR_SEQUENCE8' AS DR_SEQUENCE8,
				 'DR_SEQUENCE7' AS DR_SEQUENCE7,
				 'DR_SEQUENCE6' AS DR_SEQUENCE6,
				 'DR_SEQUENCE5' AS DR_SEQUENCE5,
				 'DR_SEQUENCE4' AS DR_SEQUENCE4,
				 'DR_SEQUENCE3' AS DR_SEQUENCE3,
				 'DR_SEQUENCE2' AS DR_SEQUENCE2,
				 'DR_SEQUENCE1' AS DR_SEQUENCE1,
				 'DR_CCID' AS DR_CCID,
				 'SET_OF_BOOKS_ID' AS SET_OF_BOOKS_ID,
				 'TOTAL_ID' AS TOTAL_ID,
				 'CR_SEQUENCE20' AS CR_SEQUENCE20,
				 'ROLLUP_LEVEL_3' AS ROLLUP_LEVEL_3,
				 'ROLLUP_LEVEL_2' AS ROLLUP_LEVEL_2,
				 'ROLLUP_LEVEL_1' AS ROLLUP_LEVEL_1,
				 'STORE' AS STORE,
				 'CR_SEQUENCE19' AS CR_SEQUENCE19,
				 'CR_SEQUENCE18' AS CR_SEQUENCE18,
				 'CR_SEQUENCE17' AS CR_SEQUENCE17,
				 'CR_SEQUENCE16' AS CR_SEQUENCE16,
				 'CR_SEQUENCE15' AS CR_SEQUENCE15,
				 'CR_SEQUENCE14' AS CR_SEQUENCE14,
				 'CR_SEQUENCE13' AS CR_SEQUENCE13,
				 'CR_SEQUENCE12' AS CR_SEQUENCE12,
				 'CR_SEQUENCE11' AS CR_SEQUENCE11,
				 'DR_SEQUENCE20' AS DR_SEQUENCE20,
				 'DR_SEQUENCE19' AS DR_SEQUENCE19,
				 'DR_SEQUENCE18' AS DR_SEQUENCE18,
				 'DR_SEQUENCE17' AS DR_SEQUENCE17,
				 'DR_SEQUENCE16' AS DR_SEQUENCE16,
				 'DR_SEQUENCE15' AS DR_SEQUENCE15,
				 'DR_SEQUENCE14' AS DR_SEQUENCE14,
				 'DR_SEQUENCE13' AS DR_SEQUENCE13,
				 'DR_SEQUENCE12' AS DR_SEQUENCE12,
				 'DR_SEQUENCE11' AS DR_SEQUENCE11,
				 'CR_SEQUENCE10' AS CR_SEQUENCE10,
				 'CR_SEQUENCE9' AS CR_SEQUENCE9,
				 'CR_SEQUENCE8' AS CR_SEQUENCE8,
				 'CR_SEQUENCE7' AS CR_SEQUENCE7,
				 'CR_SEQUENCE6' AS CR_SEQUENCE6,
				 'CR_SEQUENCE5' AS CR_SEQUENCE5,
				 'CR_SEQUENCE4' AS CR_SEQUENCE4,
				 'CR_SEQUENCE3' AS CR_SEQUENCE3,
				 'CR_SEQUENCE2' AS CR_SEQUENCE2,
				 'CR_SEQUENCE1' AS CR_SEQUENCE1,
		NULL      
		AS        
		dummy))
		)

		LOOP  


		BEGIN  

		l_default_rec.CR_CCID := rec.CR_CCID_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_CCID','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE10 := rec.DR_SEQUENCE10_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE10','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE9 := rec.DR_SEQUENCE9_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE9','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE8 := rec.DR_SEQUENCE8_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE8','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE7 := rec.DR_SEQUENCE7_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE7','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE6 := rec.DR_SEQUENCE6_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE6','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE5 := rec.DR_SEQUENCE5_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE5','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE4 := rec.DR_SEQUENCE4_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE4','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE3 := rec.DR_SEQUENCE3_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE3','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE2 := rec.DR_SEQUENCE2_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE2','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE1 := rec.DR_SEQUENCE1_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE1','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_CCID := rec.DR_CCID_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_CCID','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.SET_OF_BOOKS_ID := rec.SET_OF_BOOKS_ID_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'SET_OF_BOOKS_ID','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.TOTAL_ID := rec.TOTAL_ID_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'TOTAL_ID','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE20 := rec.CR_SEQUENCE20_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE20','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.ROLLUP_LEVEL_3 := rec.ROLLUP_LEVEL_3_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'ROLLUP_LEVEL_3','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.ROLLUP_LEVEL_2 := rec.ROLLUP_LEVEL_2_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'ROLLUP_LEVEL_2','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.ROLLUP_LEVEL_1 := rec.ROLLUP_LEVEL_1_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'ROLLUP_LEVEL_1','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.STORE := rec.STORE_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'STORE','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE19 := rec.CR_SEQUENCE19_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE19','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE18 := rec.CR_SEQUENCE18_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE18','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE17 := rec.CR_SEQUENCE17_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE17','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE16 := rec.CR_SEQUENCE16_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE16','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE15 := rec.CR_SEQUENCE15_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE15','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE14 := rec.CR_SEQUENCE14_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE14','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE13 := rec.CR_SEQUENCE13_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE13','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE12 := rec.CR_SEQUENCE12_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE12','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE11 := rec.CR_SEQUENCE11_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE11','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE20 := rec.DR_SEQUENCE20_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE20','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE19 := rec.DR_SEQUENCE19_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE19','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE18 := rec.DR_SEQUENCE18_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE18','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE17 := rec.DR_SEQUENCE17_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE17','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE16 := rec.DR_SEQUENCE16_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE16','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE15 := rec.DR_SEQUENCE15_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE15','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE14 := rec.DR_SEQUENCE14_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE14','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE13 := rec.DR_SEQUENCE13_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE13','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE12 := rec.DR_SEQUENCE12_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE12','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.DR_SEQUENCE11 := rec.DR_SEQUENCE11_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'DR_SEQUENCE11','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE10 := rec.CR_SEQUENCE10_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE10','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE9 := rec.CR_SEQUENCE9_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE9','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE8 := rec.CR_SEQUENCE8_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE8','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE7 := rec.CR_SEQUENCE7_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE7','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE6 := rec.CR_SEQUENCE6_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE6','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE5 := rec.CR_SEQUENCE5_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE5','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE4 := rec.CR_SEQUENCE4_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE4','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE3 := rec.CR_SEQUENCE3_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE3','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE2 := rec.CR_SEQUENCE2_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE2','INV_DEFAULT',SQLERRM);  


		end;


		BEGIN  

		l_default_rec.CR_SEQUENCE1 := rec.CR_SEQUENCE1_dv;  


		EXCEPTION  


		WHEN OTHERS THEN  

		write_s9t_error(I_file_id,'SA_FIF_GL_CROSS_REF',NULL,'CR_SEQUENCE1','INV_DEFAULT',SQLERRM);  


		end;


		end LOOP;

		--Get mandatory indicators
		OPEN C_mandatory_ind;
		FETCH C_mandatory_ind
		INTO l_mi_rec;
		CLOSE C_mandatory_ind;
		FOR rec IN
		(select r.get_cell(SA_GL_CROSS$Action)      AS Action,
		 r.get_cell(SA_GL_CROSS$CR_CCID)              AS CR_CCID,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE10)              AS DR_SEQUENCE10,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE9)              AS DR_SEQUENCE9,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE8)              AS DR_SEQUENCE8,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE7)              AS DR_SEQUENCE7,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE6)              AS DR_SEQUENCE6,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE5)              AS DR_SEQUENCE5,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE4)              AS DR_SEQUENCE4,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE3)              AS DR_SEQUENCE3,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE2)              AS DR_SEQUENCE2,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE1)              AS DR_SEQUENCE1,
		 r.get_cell(SA_GL_CROSS$DR_CCID)              AS DR_CCID,
		 r.get_cell(SA_GL_CROSS$SET_OF_BOOKS_ID)              AS SET_OF_BOOKS_ID,
		upper( r.get_cell(SA_GL_CROSS$TOTAL_ID)  )            AS TOTAL_ID,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE20)              AS CR_SEQUENCE20,
		 r.get_cell(SA_GL_CROSS$ROLLUP_LEVEL_3)              AS ROLLUP_LEVEL_3,
		 r.get_cell(SA_GL_CROSS$ROLLUP_LEVEL_2)              AS ROLLUP_LEVEL_2,
		 r.get_cell(SA_GL_CROSS$ROLLUP_LEVEL_1)              AS ROLLUP_LEVEL_1,
		 r.get_cell(SA_GL_CROSS$STORE)              AS STORE,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE19)              AS CR_SEQUENCE19,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE18)              AS CR_SEQUENCE18,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE17)              AS CR_SEQUENCE17,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE16)              AS CR_SEQUENCE16,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE15)              AS CR_SEQUENCE15,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE14)              AS CR_SEQUENCE14,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE13)              AS CR_SEQUENCE13,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE12)              AS CR_SEQUENCE12,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE11)              AS CR_SEQUENCE11,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE20)              AS DR_SEQUENCE20,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE19)              AS DR_SEQUENCE19,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE18)              AS DR_SEQUENCE18,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE17)              AS DR_SEQUENCE17,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE16)              AS DR_SEQUENCE16,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE15)              AS DR_SEQUENCE15,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE14)              AS DR_SEQUENCE14,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE13)              AS DR_SEQUENCE13,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE12)              AS DR_SEQUENCE12,
		 r.get_cell(SA_GL_CROSS$DR_SEQUENCE11)              AS DR_SEQUENCE11,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE10)              AS CR_SEQUENCE10,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE9)              AS CR_SEQUENCE9,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE8)              AS CR_SEQUENCE8,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE7)              AS CR_SEQUENCE7,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE6)              AS CR_SEQUENCE6,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE5)              AS CR_SEQUENCE5,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE4)              AS CR_SEQUENCE4,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE3)              AS CR_SEQUENCE3,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE2)              AS CR_SEQUENCE2,
		 r.get_cell(SA_GL_CROSS$CR_SEQUENCE1)              AS CR_SEQUENCE1,
		 r.get_row_seq()                             AS row_seq
		from s9t_folder sf,
		 TABLE(sf.s9t_file_obj.sheets) ss,
		 TABLE(ss.s9t_rows) r
		where sf.file_id  = I_file_id
		and ss.sheet_name = sheet_name_trans(SA_FIF_GL_CROSS_REF_sheet)
		)
		LOOP
		 l_temp_rec.process_id        := I_process_id;
		 l_temp_rec.chunk_id          := 1;
		 l_temp_rec.row_seq           := rec.row_seq;
		 l_temp_rec.process$status    := 'N';
		 l_temp_rec.create_id         := GET_USER;
		 l_temp_rec.last_upd_id       := GET_USER;
		 l_temp_rec.create_datetime   := sysdate;
		 l_temp_rec.last_upd_datetime := sysdate;
		 l_error := false;
		 BEGIN
			l_temp_rec.Action := rec.Action;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'ACTION',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_CCID := rec.CR_CCID;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_CCID',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE10 := rec.DR_SEQUENCE10;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE10',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE9 := rec.DR_SEQUENCE9;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE9',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE8 := rec.DR_SEQUENCE8;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE8',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE7 := rec.DR_SEQUENCE7;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE7',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE6 := rec.DR_SEQUENCE6;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE6',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE5 := rec.DR_SEQUENCE5;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE5',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE4 := rec.DR_SEQUENCE4;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE4',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE3 := rec.DR_SEQUENCE3;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE3',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE2 := rec.DR_SEQUENCE2;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE2',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE1 := rec.DR_SEQUENCE1;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE1',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_CCID := rec.DR_CCID;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_CCID',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.SET_OF_BOOKS_ID := rec.SET_OF_BOOKS_ID;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'SET_OF_BOOKS_ID',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.TOTAL_ID := rec.TOTAL_ID;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'TOTAL_ID',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE20 := rec.CR_SEQUENCE20;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE20',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.ROLLUP_LEVEL_3 := rec.ROLLUP_LEVEL_3;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'ROLLUP_LEVEL_3',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.ROLLUP_LEVEL_2 := rec.ROLLUP_LEVEL_2;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'ROLLUP_LEVEL_2',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.ROLLUP_LEVEL_1 := rec.ROLLUP_LEVEL_1;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'ROLLUP_LEVEL_1',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.STORE := rec.STORE;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'STORE',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE19 := rec.CR_SEQUENCE19;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE19',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE18 := rec.CR_SEQUENCE18;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE18',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE17 := rec.CR_SEQUENCE17;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE17',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE16 := rec.CR_SEQUENCE16;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE16',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE15 := rec.CR_SEQUENCE15;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE15',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE14 := rec.CR_SEQUENCE14;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE14',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE13 := rec.CR_SEQUENCE13;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE13',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE12 := rec.CR_SEQUENCE12;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE12',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE11 := rec.CR_SEQUENCE11;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE11',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE20 := rec.DR_SEQUENCE20;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE20',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE19 := rec.DR_SEQUENCE19;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE19',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE18 := rec.DR_SEQUENCE18;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE18',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE17 := rec.DR_SEQUENCE17;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE17',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE16 := rec.DR_SEQUENCE16;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE16',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE15 := rec.DR_SEQUENCE15;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE15',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE14 := rec.DR_SEQUENCE14;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE14',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE13 := rec.DR_SEQUENCE13;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE13',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE12 := rec.DR_SEQUENCE12;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE12',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.DR_SEQUENCE11 := rec.DR_SEQUENCE11;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'DR_SEQUENCE11',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE10 := rec.CR_SEQUENCE10;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE10',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE9 := rec.CR_SEQUENCE9;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE9',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE8 := rec.CR_SEQUENCE8;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE8',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE7 := rec.CR_SEQUENCE7;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE7',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE6 := rec.CR_SEQUENCE6;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE6',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE5 := rec.CR_SEQUENCE5;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE5',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE4 := rec.CR_SEQUENCE4;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE4',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE3 := rec.CR_SEQUENCE3;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE3',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE2 := rec.CR_SEQUENCE2;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE2',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 BEGIN
			l_temp_rec.CR_SEQUENCE1 := rec.CR_SEQUENCE1;
		 EXCEPTION
		 WHEN OTHERS THEN
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,'CR_SEQUENCE1',SQLCODE,SQLERRM);
			l_error := true;
		 end;
		 if rec.action = STG_SVC_SA_FIF_GL_CROSS_REF.action_new
		 then
		 l_temp_rec.CR_CCID := NVL( l_temp_rec.CR_CCID,l_default_rec.CR_CCID);
		 l_temp_rec.DR_SEQUENCE10 := NVL( l_temp_rec.DR_SEQUENCE10,l_default_rec.DR_SEQUENCE10);
		 l_temp_rec.DR_SEQUENCE9 := NVL( l_temp_rec.DR_SEQUENCE9,l_default_rec.DR_SEQUENCE9);
		 l_temp_rec.DR_SEQUENCE8 := NVL( l_temp_rec.DR_SEQUENCE8,l_default_rec.DR_SEQUENCE8);
		 l_temp_rec.DR_SEQUENCE7 := NVL( l_temp_rec.DR_SEQUENCE7,l_default_rec.DR_SEQUENCE7);
		 l_temp_rec.DR_SEQUENCE6 := NVL( l_temp_rec.DR_SEQUENCE6,l_default_rec.DR_SEQUENCE6);
		 l_temp_rec.DR_SEQUENCE5 := NVL( l_temp_rec.DR_SEQUENCE5,l_default_rec.DR_SEQUENCE5);
		 l_temp_rec.DR_SEQUENCE4 := NVL( l_temp_rec.DR_SEQUENCE4,l_default_rec.DR_SEQUENCE4);
		 l_temp_rec.DR_SEQUENCE3 := NVL( l_temp_rec.DR_SEQUENCE3,l_default_rec.DR_SEQUENCE3);
		 l_temp_rec.DR_SEQUENCE2 := NVL( l_temp_rec.DR_SEQUENCE2,l_default_rec.DR_SEQUENCE2);
		 l_temp_rec.DR_SEQUENCE1 := NVL( l_temp_rec.DR_SEQUENCE1,l_default_rec.DR_SEQUENCE1);
		 l_temp_rec.DR_CCID := NVL( l_temp_rec.DR_CCID,l_default_rec.DR_CCID);
		 l_temp_rec.SET_OF_BOOKS_ID := NVL( l_temp_rec.SET_OF_BOOKS_ID,l_default_rec.SET_OF_BOOKS_ID);
		 l_temp_rec.TOTAL_ID := upper(NVL( l_temp_rec.TOTAL_ID,l_default_rec.TOTAL_ID));
		 l_temp_rec.CR_SEQUENCE20 := NVL( l_temp_rec.CR_SEQUENCE20,l_default_rec.CR_SEQUENCE20);
		 l_temp_rec.ROLLUP_LEVEL_3 := NVL( l_temp_rec.ROLLUP_LEVEL_3,l_default_rec.ROLLUP_LEVEL_3);
		 l_temp_rec.ROLLUP_LEVEL_2 := NVL( l_temp_rec.ROLLUP_LEVEL_2,l_default_rec.ROLLUP_LEVEL_2);
		 l_temp_rec.ROLLUP_LEVEL_1 := NVL( l_temp_rec.ROLLUP_LEVEL_1,l_default_rec.ROLLUP_LEVEL_1);
		 l_temp_rec.STORE := NVL( l_temp_rec.STORE,l_default_rec.STORE);
		 l_temp_rec.CR_SEQUENCE19 := NVL( l_temp_rec.CR_SEQUENCE19,l_default_rec.CR_SEQUENCE19);
		 l_temp_rec.CR_SEQUENCE18 := NVL( l_temp_rec.CR_SEQUENCE18,l_default_rec.CR_SEQUENCE18);
		 l_temp_rec.CR_SEQUENCE17 := NVL( l_temp_rec.CR_SEQUENCE17,l_default_rec.CR_SEQUENCE17);
		 l_temp_rec.CR_SEQUENCE16 := NVL( l_temp_rec.CR_SEQUENCE16,l_default_rec.CR_SEQUENCE16);
		 l_temp_rec.CR_SEQUENCE15 := NVL( l_temp_rec.CR_SEQUENCE15,l_default_rec.CR_SEQUENCE15);
		 l_temp_rec.CR_SEQUENCE14 := NVL( l_temp_rec.CR_SEQUENCE14,l_default_rec.CR_SEQUENCE14);
		 l_temp_rec.CR_SEQUENCE13 := NVL( l_temp_rec.CR_SEQUENCE13,l_default_rec.CR_SEQUENCE13);
		 l_temp_rec.CR_SEQUENCE12 := NVL( l_temp_rec.CR_SEQUENCE12,l_default_rec.CR_SEQUENCE12);
		 l_temp_rec.CR_SEQUENCE11 := NVL( l_temp_rec.CR_SEQUENCE11,l_default_rec.CR_SEQUENCE11);
		 l_temp_rec.DR_SEQUENCE20 := NVL( l_temp_rec.DR_SEQUENCE20,l_default_rec.DR_SEQUENCE20);
		 l_temp_rec.DR_SEQUENCE19 := NVL( l_temp_rec.DR_SEQUENCE19,l_default_rec.DR_SEQUENCE19);
		 l_temp_rec.DR_SEQUENCE18 := NVL( l_temp_rec.DR_SEQUENCE18,l_default_rec.DR_SEQUENCE18);
		 l_temp_rec.DR_SEQUENCE17 := NVL( l_temp_rec.DR_SEQUENCE17,l_default_rec.DR_SEQUENCE17);
		 l_temp_rec.DR_SEQUENCE16 := NVL( l_temp_rec.DR_SEQUENCE16,l_default_rec.DR_SEQUENCE16);
		 l_temp_rec.DR_SEQUENCE15 := NVL( l_temp_rec.DR_SEQUENCE15,l_default_rec.DR_SEQUENCE15);
		 l_temp_rec.DR_SEQUENCE14 := NVL( l_temp_rec.DR_SEQUENCE14,l_default_rec.DR_SEQUENCE14);
		 l_temp_rec.DR_SEQUENCE13 := NVL( l_temp_rec.DR_SEQUENCE13,l_default_rec.DR_SEQUENCE13);
		 l_temp_rec.DR_SEQUENCE12 := NVL( l_temp_rec.DR_SEQUENCE12,l_default_rec.DR_SEQUENCE12);
		 l_temp_rec.DR_SEQUENCE11 := NVL( l_temp_rec.DR_SEQUENCE11,l_default_rec.DR_SEQUENCE11);
		 l_temp_rec.CR_SEQUENCE10 := NVL( l_temp_rec.CR_SEQUENCE10,l_default_rec.CR_SEQUENCE10);
		 l_temp_rec.CR_SEQUENCE9 := NVL( l_temp_rec.CR_SEQUENCE9,l_default_rec.CR_SEQUENCE9);
		 l_temp_rec.CR_SEQUENCE8 := NVL( l_temp_rec.CR_SEQUENCE8,l_default_rec.CR_SEQUENCE8);
		 l_temp_rec.CR_SEQUENCE7 := NVL( l_temp_rec.CR_SEQUENCE7,l_default_rec.CR_SEQUENCE7);
		 l_temp_rec.CR_SEQUENCE6 := NVL( l_temp_rec.CR_SEQUENCE6,l_default_rec.CR_SEQUENCE6);
		 l_temp_rec.CR_SEQUENCE5 := NVL( l_temp_rec.CR_SEQUENCE5,l_default_rec.CR_SEQUENCE5);
		 l_temp_rec.CR_SEQUENCE4 := NVL( l_temp_rec.CR_SEQUENCE4,l_default_rec.CR_SEQUENCE4);
		 l_temp_rec.CR_SEQUENCE3 := NVL( l_temp_rec.CR_SEQUENCE3,l_default_rec.CR_SEQUENCE3);
		 l_temp_rec.CR_SEQUENCE2 := NVL( l_temp_rec.CR_SEQUENCE2,l_default_rec.CR_SEQUENCE2);
		 l_temp_rec.CR_SEQUENCE1 := NVL( l_temp_rec.CR_SEQUENCE1,l_default_rec.CR_SEQUENCE1);
		end if;
		 If not (
					l_temp_rec.SET_OF_BOOKS_ID is not null and
					l_temp_rec.ROLLUP_LEVEL_3 is not null and
					l_temp_rec.ROLLUP_LEVEL_2 is not null and
					l_temp_rec.ROLLUP_LEVEL_1 is not null and
					l_temp_rec.TOTAL_ID is not null and
					l_temp_rec.STORE is not null and
					1 = 1
					)
		 Then
			write_s9t_error(I_file_id,SA_FIF_GL_CROSS_REF_sheet,rec.row_seq,NULL,NULL,sql_lib.create_msg('PK_COLS_REQUIRED',l_pk_columns));
			l_error := true;
		 end if;
		 if NOT l_error THEN
			svc_SA_FIF_GL_CROSS_REF_col.extend();
			svc_SA_FIF_GL_CROSS_REF_col(svc_SA_FIF_GL_CROSS_REF_col.count()):=l_temp_rec;
		 end if;
	end LOOP;
	BEGIN
		forall i IN 1..svc_SA_FIF_GL_CROSS_REF_col.count SAVE EXCEPTIONS Merge INTO SVC_SA_FIF_GL_CROSS_REF st USING
		(select
		 (CASE
			WHEN l_mi_rec.CR_CCID_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_CCID             IS NULL
			THEN mt.CR_CCID
			ELSE s1.CR_CCID
		 end) AS CR_CCID,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE10_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE10             IS NULL
			THEN mt.DR_SEQUENCE10
			ELSE s1.DR_SEQUENCE10
		 end) AS DR_SEQUENCE10,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE9_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE9             IS NULL
			THEN mt.DR_SEQUENCE9
			ELSE s1.DR_SEQUENCE9
		 end) AS DR_SEQUENCE9,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE8_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE8             IS NULL
			THEN mt.DR_SEQUENCE8
			ELSE s1.DR_SEQUENCE8
		 end) AS DR_SEQUENCE8,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE7_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE7             IS NULL
			THEN mt.DR_SEQUENCE7
			ELSE s1.DR_SEQUENCE7
		 end) AS DR_SEQUENCE7,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE6_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE6             IS NULL
			THEN mt.DR_SEQUENCE6
			ELSE s1.DR_SEQUENCE6
		 end) AS DR_SEQUENCE6,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE5_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE5             IS NULL
			THEN mt.DR_SEQUENCE5
			ELSE s1.DR_SEQUENCE5
		 end) AS DR_SEQUENCE5,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE4_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE4             IS NULL
			THEN mt.DR_SEQUENCE4
			ELSE s1.DR_SEQUENCE4
		 end) AS DR_SEQUENCE4,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE3_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE3             IS NULL
			THEN mt.DR_SEQUENCE3
			ELSE s1.DR_SEQUENCE3
		 end) AS DR_SEQUENCE3,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE2_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE2             IS NULL
			THEN mt.DR_SEQUENCE2
			ELSE s1.DR_SEQUENCE2
		 end) AS DR_SEQUENCE2,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE1_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE1             IS NULL
			THEN mt.DR_SEQUENCE1
			ELSE s1.DR_SEQUENCE1
		 end) AS DR_SEQUENCE1,
		 (CASE
			WHEN l_mi_rec.DR_CCID_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_CCID             IS NULL
			THEN mt.DR_CCID
			ELSE s1.DR_CCID
		 end) AS DR_CCID,
		 (CASE
			WHEN l_mi_rec.SET_OF_BOOKS_ID_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.SET_OF_BOOKS_ID             IS NULL
			THEN mt.SET_OF_BOOKS_ID
			ELSE s1.SET_OF_BOOKS_ID
		 end) AS SET_OF_BOOKS_ID,
		 (CASE
			WHEN l_mi_rec.TOTAL_ID_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.TOTAL_ID             IS NULL
			THEN mt.TOTAL_ID
			ELSE s1.TOTAL_ID
		 end) AS TOTAL_ID,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE20_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE20             IS NULL
			THEN mt.CR_SEQUENCE20
			ELSE s1.CR_SEQUENCE20
		 end) AS CR_SEQUENCE20,
		 (CASE
			WHEN l_mi_rec.ROLLUP_LEVEL_3_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.ROLLUP_LEVEL_3             IS NULL
			THEN mt.ROLLUP_LEVEL_3
			ELSE s1.ROLLUP_LEVEL_3
		 end) AS ROLLUP_LEVEL_3,
		 (CASE
			WHEN l_mi_rec.ROLLUP_LEVEL_2_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.ROLLUP_LEVEL_2             IS NULL
			THEN mt.ROLLUP_LEVEL_2
			ELSE s1.ROLLUP_LEVEL_2
		 end) AS ROLLUP_LEVEL_2,
		 (CASE
			WHEN l_mi_rec.ROLLUP_LEVEL_1_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.ROLLUP_LEVEL_1             IS NULL
			THEN mt.ROLLUP_LEVEL_1
			ELSE s1.ROLLUP_LEVEL_1
		 end) AS ROLLUP_LEVEL_1,
		 (CASE
			WHEN l_mi_rec.STORE_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.STORE             IS NULL
			THEN mt.STORE
			ELSE s1.STORE
		 end) AS STORE,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE19_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE19             IS NULL
			THEN mt.CR_SEQUENCE19
			ELSE s1.CR_SEQUENCE19
		 end) AS CR_SEQUENCE19,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE18_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE18             IS NULL
			THEN mt.CR_SEQUENCE18
			ELSE s1.CR_SEQUENCE18
		 end) AS CR_SEQUENCE18,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE17_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE17             IS NULL
			THEN mt.CR_SEQUENCE17
			ELSE s1.CR_SEQUENCE17
		 end) AS CR_SEQUENCE17,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE16_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE16             IS NULL
			THEN mt.CR_SEQUENCE16
			ELSE s1.CR_SEQUENCE16
		 end) AS CR_SEQUENCE16,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE15_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE15             IS NULL
			THEN mt.CR_SEQUENCE15
			ELSE s1.CR_SEQUENCE15
		 end) AS CR_SEQUENCE15,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE14_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE14             IS NULL
			THEN mt.CR_SEQUENCE14
			ELSE s1.CR_SEQUENCE14
		 end) AS CR_SEQUENCE14,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE13_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE13             IS NULL
			THEN mt.CR_SEQUENCE13
			ELSE s1.CR_SEQUENCE13
		 end) AS CR_SEQUENCE13,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE12_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE12             IS NULL
			THEN mt.CR_SEQUENCE12
			ELSE s1.CR_SEQUENCE12
		 end) AS CR_SEQUENCE12,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE11_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE11             IS NULL
			THEN mt.CR_SEQUENCE11
			ELSE s1.CR_SEQUENCE11
		 end) AS CR_SEQUENCE11,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE20_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE20             IS NULL
			THEN mt.DR_SEQUENCE20
			ELSE s1.DR_SEQUENCE20
		 end) AS DR_SEQUENCE20,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE19_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE19             IS NULL
			THEN mt.DR_SEQUENCE19
			ELSE s1.DR_SEQUENCE19
		 end) AS DR_SEQUENCE19,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE18_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE18             IS NULL
			THEN mt.DR_SEQUENCE18
			ELSE s1.DR_SEQUENCE18
		 end) AS DR_SEQUENCE18,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE17_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE17             IS NULL
			THEN mt.DR_SEQUENCE17
			ELSE s1.DR_SEQUENCE17
		 end) AS DR_SEQUENCE17,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE16_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE16             IS NULL
			THEN mt.DR_SEQUENCE16
			ELSE s1.DR_SEQUENCE16
		 end) AS DR_SEQUENCE16,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE15_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE15             IS NULL
			THEN mt.DR_SEQUENCE15
			ELSE s1.DR_SEQUENCE15
		 end) AS DR_SEQUENCE15,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE14_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE14             IS NULL
			THEN mt.DR_SEQUENCE14
			ELSE s1.DR_SEQUENCE14
		 end) AS DR_SEQUENCE14,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE13_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE13             IS NULL
			THEN mt.DR_SEQUENCE13
			ELSE s1.DR_SEQUENCE13
		 end) AS DR_SEQUENCE13,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE12_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE12             IS NULL
			THEN mt.DR_SEQUENCE12
			ELSE s1.DR_SEQUENCE12
		 end) AS DR_SEQUENCE12,
		 (CASE
			WHEN l_mi_rec.DR_SEQUENCE11_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.DR_SEQUENCE11             IS NULL
			THEN mt.DR_SEQUENCE11
			ELSE s1.DR_SEQUENCE11
		 end) AS DR_SEQUENCE11,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE10_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE10             IS NULL
			THEN mt.CR_SEQUENCE10
			ELSE s1.CR_SEQUENCE10
		 end) AS CR_SEQUENCE10,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE9_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE9             IS NULL
			THEN mt.CR_SEQUENCE9
			ELSE s1.CR_SEQUENCE9
		 end) AS CR_SEQUENCE9,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE8_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE8             IS NULL
			THEN mt.CR_SEQUENCE8
			ELSE s1.CR_SEQUENCE8
		 end) AS CR_SEQUENCE8,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE7_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE7             IS NULL
			THEN mt.CR_SEQUENCE7
			ELSE s1.CR_SEQUENCE7
		 end) AS CR_SEQUENCE7,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE6_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE6             IS NULL
			THEN mt.CR_SEQUENCE6
			ELSE s1.CR_SEQUENCE6
		 end) AS CR_SEQUENCE6,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE5_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE5             IS NULL
			THEN mt.CR_SEQUENCE5
			ELSE s1.CR_SEQUENCE5
		 end) AS CR_SEQUENCE5,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE4_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE4             IS NULL
			THEN mt.CR_SEQUENCE4
			ELSE s1.CR_SEQUENCE4
		 end) AS CR_SEQUENCE4,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE3_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE3             IS NULL
			THEN mt.CR_SEQUENCE3
			ELSE s1.CR_SEQUENCE3
		 end) AS CR_SEQUENCE3,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE2_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE2             IS NULL
			THEN mt.CR_SEQUENCE2
			ELSE s1.CR_SEQUENCE2
		 end) AS CR_SEQUENCE2,
		 (CASE
			WHEN l_mi_rec.CR_SEQUENCE1_mi    = 'N'
			and svc_SA_FIF_GL_CROSS_REF_col(i).action = STG_SVC_SA_FIF_GL_CROSS_REF.action_mod
			and s1.CR_SEQUENCE1             IS NULL
			THEN mt.CR_SEQUENCE1
			ELSE s1.CR_SEQUENCE1
		 end) AS CR_SEQUENCE1,
		 null as dummy
		from (select
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_CCID AS CR_CCID,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE10 AS DR_SEQUENCE10,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE9 AS DR_SEQUENCE9,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE8 AS DR_SEQUENCE8,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE7 AS DR_SEQUENCE7,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE6 AS DR_SEQUENCE6,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE5 AS DR_SEQUENCE5,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE4 AS DR_SEQUENCE4,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE3 AS DR_SEQUENCE3,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE2 AS DR_SEQUENCE2,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE1 AS DR_SEQUENCE1,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_CCID AS DR_CCID,
		  svc_SA_FIF_GL_CROSS_REF_col(i).SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
		 upper( svc_SA_FIF_GL_CROSS_REF_col(i).TOTAL_ID) AS TOTAL_ID,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE20 AS CR_SEQUENCE20,
		  svc_SA_FIF_GL_CROSS_REF_col(i).ROLLUP_LEVEL_3 AS ROLLUP_LEVEL_3,
		  svc_SA_FIF_GL_CROSS_REF_col(i).ROLLUP_LEVEL_2 AS ROLLUP_LEVEL_2,
		  svc_SA_FIF_GL_CROSS_REF_col(i).ROLLUP_LEVEL_1 AS ROLLUP_LEVEL_1,
		  svc_SA_FIF_GL_CROSS_REF_col(i).STORE AS STORE,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE19 AS CR_SEQUENCE19,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE18 AS CR_SEQUENCE18,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE17 AS CR_SEQUENCE17,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE16 AS CR_SEQUENCE16,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE15 AS CR_SEQUENCE15,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE14 AS CR_SEQUENCE14,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE13 AS CR_SEQUENCE13,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE12 AS CR_SEQUENCE12,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE11 AS CR_SEQUENCE11,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE20 AS DR_SEQUENCE20,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE19 AS DR_SEQUENCE19,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE18 AS DR_SEQUENCE18,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE17 AS DR_SEQUENCE17,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE16 AS DR_SEQUENCE16,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE15 AS DR_SEQUENCE15,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE14 AS DR_SEQUENCE14,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE13 AS DR_SEQUENCE13,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE12 AS DR_SEQUENCE12,
		  svc_SA_FIF_GL_CROSS_REF_col(i).DR_SEQUENCE11 AS DR_SEQUENCE11,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE10 AS CR_SEQUENCE10,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE9 AS CR_SEQUENCE9,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE8 AS CR_SEQUENCE8,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE7 AS CR_SEQUENCE7,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE6 AS CR_SEQUENCE6,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE5 AS CR_SEQUENCE5,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE4 AS CR_SEQUENCE4,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE3 AS CR_SEQUENCE3,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE2 AS CR_SEQUENCE2,
		  svc_SA_FIF_GL_CROSS_REF_col(i).CR_SEQUENCE1 AS CR_SEQUENCE1,
		  null as dummy
		 from dual
		 ) s1,
		 SA_FIF_GL_CROSS_REF mt
		where
		mt.SET_OF_BOOKS_ID (+)     = s1.SET_OF_BOOKS_ID   and
		mt.ROLLUP_LEVEL_3 (+)     = s1.ROLLUP_LEVEL_3   and
		mt.ROLLUP_LEVEL_2 (+)     = s1.ROLLUP_LEVEL_2   and
		mt.ROLLUP_LEVEL_1 (+)     = s1.ROLLUP_LEVEL_1   and
		mt.TOTAL_ID (+)     = s1.TOTAL_ID   and
		mt.STORE (+)     = s1.STORE   and
		1 = 1
		) sq ON (
		st.SET_OF_BOOKS_ID      = sq.SET_OF_BOOKS_ID and
		st.ROLLUP_LEVEL_3      = sq.ROLLUP_LEVEL_3 and
		st.ROLLUP_LEVEL_2      = sq.ROLLUP_LEVEL_2 and
		st.ROLLUP_LEVEL_1      = sq.ROLLUP_LEVEL_1 and
		st.TOTAL_ID      = sq.TOTAL_ID and
		st.STORE      = sq.STORE and
		svc_SA_FIF_GL_CROSS_REF_col(i).ACTION IN( STG_SVC_SA_FIF_GL_CROSS_REF.action_mod,STG_SVC_SA_FIF_GL_CROSS_REF.action_del)
		)
		WHEN matched THEN
		UPDATE
		SET PROCESS_ID      = svc_SA_FIF_GL_CROSS_REF_col(i).PROCESS_ID ,
		 CHUNK_ID          = svc_SA_FIF_GL_CROSS_REF_col(i).CHUNK_ID ,
		 ROW_SEQ           = svc_SA_FIF_GL_CROSS_REF_col(i).ROW_SEQ ,
		 ACTION            =svc_SA_FIF_GL_CROSS_REF_col(i).ACTION,
		 PROCESS$STATUS    = svc_SA_FIF_GL_CROSS_REF_col(i).PROCESS$STATUS ,
		 DR_SEQUENCE8              = sq.DR_SEQUENCE8 ,
		 DR_SEQUENCE1              = sq.DR_SEQUENCE1 ,
		 CR_SEQUENCE12              = sq.CR_SEQUENCE12 ,
		 DR_SEQUENCE7              = sq.DR_SEQUENCE7 ,
		 DR_SEQUENCE10              = sq.DR_SEQUENCE10 ,
		 DR_SEQUENCE11              = sq.DR_SEQUENCE11 ,
		 CR_SEQUENCE11              = sq.CR_SEQUENCE11 ,
		 CR_SEQUENCE7              = sq.CR_SEQUENCE7 ,
		 CR_SEQUENCE4              = sq.CR_SEQUENCE4 ,
		 CR_SEQUENCE9              = sq.CR_SEQUENCE9 ,
		 DR_SEQUENCE2              = sq.DR_SEQUENCE2 ,
		 DR_SEQUENCE14              = sq.DR_SEQUENCE14 ,
		 CR_SEQUENCE18              = sq.CR_SEQUENCE18 ,
		 CR_SEQUENCE16              = sq.CR_SEQUENCE16 ,
		 DR_SEQUENCE18              = sq.DR_SEQUENCE18 ,
		 DR_SEQUENCE6              = sq.DR_SEQUENCE6 ,
		 CR_SEQUENCE14              = sq.CR_SEQUENCE14 ,
		 DR_SEQUENCE17              = sq.DR_SEQUENCE17 ,
		 CR_SEQUENCE8              = sq.CR_SEQUENCE8 ,
		 CR_SEQUENCE20              = sq.CR_SEQUENCE20 ,
		 CR_SEQUENCE3              = sq.CR_SEQUENCE3 ,
		 DR_SEQUENCE9              = sq.DR_SEQUENCE9 ,
		 DR_SEQUENCE19              = sq.DR_SEQUENCE19 ,
		 DR_SEQUENCE12              = sq.DR_SEQUENCE12 ,
		 CR_SEQUENCE13              = sq.CR_SEQUENCE13 ,
		 CR_SEQUENCE2              = sq.CR_SEQUENCE2 ,
		 DR_SEQUENCE3              = sq.DR_SEQUENCE3 ,
		 CR_SEQUENCE1              = sq.CR_SEQUENCE1 ,
		 CR_SEQUENCE6              = sq.CR_SEQUENCE6 ,
		 DR_SEQUENCE4              = sq.DR_SEQUENCE4 ,
		 DR_SEQUENCE20              = sq.DR_SEQUENCE20 ,
		 CR_SEQUENCE19              = sq.CR_SEQUENCE19 ,
		 CR_SEQUENCE5              = sq.CR_SEQUENCE5 ,
		 DR_SEQUENCE15              = sq.DR_SEQUENCE15 ,
		 DR_CCID              = sq.DR_CCID ,
		 DR_SEQUENCE13              = sq.DR_SEQUENCE13 ,
		 CR_SEQUENCE17              = sq.CR_SEQUENCE17 ,
		 DR_SEQUENCE16              = sq.DR_SEQUENCE16 ,
		 CR_CCID              = sq.CR_CCID ,
		 CR_SEQUENCE15              = sq.CR_SEQUENCE15 ,
		 CR_SEQUENCE10              = sq.CR_SEQUENCE10 ,
		 DR_SEQUENCE5              = sq.DR_SEQUENCE5 ,
		 CREATE_ID         = svc_SA_FIF_GL_CROSS_REF_col(i).CREATE_ID ,
		 CREATE_DATETIME   = svc_SA_FIF_GL_CROSS_REF_col(i).CREATE_DATETIME ,
		 LAST_UPD_ID       = svc_SA_FIF_GL_CROSS_REF_col(i).LAST_UPD_ID ,
		 LAST_UPD_DATETIME = svc_SA_FIF_GL_CROSS_REF_col(i).LAST_UPD_DATETIME WHEN NOT matched THEN
		INSERT
		 (
			PROCESS_ID ,
			CHUNK_ID ,
			ROW_SEQ ,
			ACTION ,
			PROCESS$STATUS ,
			CR_CCID ,
			DR_SEQUENCE10 ,
			DR_SEQUENCE9 ,
			DR_SEQUENCE8 ,
			DR_SEQUENCE7 ,
			DR_SEQUENCE6 ,
			DR_SEQUENCE5 ,
			DR_SEQUENCE4 ,
			DR_SEQUENCE3 ,
			DR_SEQUENCE2 ,
			DR_SEQUENCE1 ,
			DR_CCID ,
			SET_OF_BOOKS_ID ,
			TOTAL_ID ,
			CR_SEQUENCE20 ,
			ROLLUP_LEVEL_3 ,
			ROLLUP_LEVEL_2 ,
			ROLLUP_LEVEL_1 ,
			STORE ,
			CR_SEQUENCE19 ,
			CR_SEQUENCE18 ,
			CR_SEQUENCE17 ,
			CR_SEQUENCE16 ,
			CR_SEQUENCE15 ,
			CR_SEQUENCE14 ,
			CR_SEQUENCE13 ,
			CR_SEQUENCE12 ,
			CR_SEQUENCE11 ,
			DR_SEQUENCE20 ,
			DR_SEQUENCE19 ,
			DR_SEQUENCE18 ,
			DR_SEQUENCE17 ,
			DR_SEQUENCE16 ,
			DR_SEQUENCE15 ,
			DR_SEQUENCE14 ,
			DR_SEQUENCE13 ,
			DR_SEQUENCE12 ,
			DR_SEQUENCE11 ,
			CR_SEQUENCE10 ,
			CR_SEQUENCE9 ,
			CR_SEQUENCE8 ,
			CR_SEQUENCE7 ,
			CR_SEQUENCE6 ,
			CR_SEQUENCE5 ,
			CR_SEQUENCE4 ,
			CR_SEQUENCE3 ,
			CR_SEQUENCE2 ,
			CR_SEQUENCE1 ,
			CREATE_ID ,
			CREATE_DATETIME ,
			LAST_UPD_ID ,
			LAST_UPD_DATETIME
		 )
		 VALUES
		 (
			svc_SA_FIF_GL_CROSS_REF_col(i).PROCESS_ID ,
			svc_SA_FIF_GL_CROSS_REF_col(i).CHUNK_ID ,
			svc_SA_FIF_GL_CROSS_REF_col(i).ROW_SEQ ,
			svc_SA_FIF_GL_CROSS_REF_col(i).ACTION ,
			svc_SA_FIF_GL_CROSS_REF_col(i).PROCESS$STATUS ,
			sq.CR_CCID ,
			sq.DR_SEQUENCE10 ,
			sq.DR_SEQUENCE9 ,
			sq.DR_SEQUENCE8 ,
			sq.DR_SEQUENCE7 ,
			sq.DR_SEQUENCE6 ,
			sq.DR_SEQUENCE5 ,
			sq.DR_SEQUENCE4 ,
			sq.DR_SEQUENCE3 ,
			sq.DR_SEQUENCE2 ,
			sq.DR_SEQUENCE1 ,
			sq.DR_CCID ,
			sq.SET_OF_BOOKS_ID ,
		  upper( sq.TOTAL_ID) ,
			sq.CR_SEQUENCE20 ,
			sq.ROLLUP_LEVEL_3 ,
			sq.ROLLUP_LEVEL_2 ,
			sq.ROLLUP_LEVEL_1 ,
			sq.STORE ,
			sq.CR_SEQUENCE19 ,
			sq.CR_SEQUENCE18 ,
			sq.CR_SEQUENCE17 ,
			sq.CR_SEQUENCE16 ,
			sq.CR_SEQUENCE15 ,
			sq.CR_SEQUENCE14 ,
			sq.CR_SEQUENCE13 ,
			sq.CR_SEQUENCE12 ,
			sq.CR_SEQUENCE11 ,
			sq.DR_SEQUENCE20 ,
			sq.DR_SEQUENCE19 ,
			sq.DR_SEQUENCE18 ,
			sq.DR_SEQUENCE17 ,
			sq.DR_SEQUENCE16 ,
			sq.DR_SEQUENCE15 ,
			sq.DR_SEQUENCE14 ,
			sq.DR_SEQUENCE13 ,
			sq.DR_SEQUENCE12 ,
			sq.DR_SEQUENCE11 ,
			sq.CR_SEQUENCE10 ,
			sq.CR_SEQUENCE9 ,
			sq.CR_SEQUENCE8 ,
			sq.CR_SEQUENCE7 ,
			sq.CR_SEQUENCE6 ,
			sq.CR_SEQUENCE5 ,
			sq.CR_SEQUENCE4 ,
			sq.CR_SEQUENCE3 ,
			sq.CR_SEQUENCE2 ,
			sq.CR_SEQUENCE1 ,
			svc_SA_FIF_GL_CROSS_REF_col(i).CREATE_ID ,
			svc_SA_FIF_GL_CROSS_REF_col(i).CREATE_DATETIME ,
			svc_SA_FIF_GL_CROSS_REF_col(i).LAST_UPD_ID ,
			svc_SA_FIF_GL_CROSS_REF_col(i).LAST_UPD_DATETIME
		 );
		EXCEPTION
		WHEN DML_ERRORS THEN
		 FOR i IN 1..sql%bulk_exceptions.count
		 LOOP
			  l_error_code:=sql%bulk_exceptions(i).error_code;
			  if l_error_code=1 then
				  l_error_code:=null;
				  l_error_msg:=sql_lib.create_msg('DUP_REC_EXISTS',l_pk_columns,l_table);
			  end if;
			write_s9t_error
			(
			  I_file_id,SA_FIF_GL_CROSS_REF_sheet,svc_SA_FIF_GL_CROSS_REF_col(sql%bulk_exceptions(i).error_index).row_seq,NULL,l_error_code,l_error_msg
			)
			;
		 end LOOP;
	end;
	end process_s9t_SA_GL_CROSS_REF;
	-------------------------------------------------------------------------------
	---  Name: process_s9t
	--Purpose: Called from UI to uplaod data from excel to staging table
	-------------------------------------------------------------------------------- 
	FUNCTION process_s9t
	(
	 O_error_message IN OUT rtk_errors.rtk_text%type ,
	 I_file_id       IN s9t_folder.file_id%type,
	 I_process_id IN Number,
	 O_error_count OUT NUMBER
	)
	RETURN BOOLEAN
	IS
	l_file             s9t_file;
	l_sheets           s9t_pkg.names_map_typ;
	L_program          VARCHAR2(255):='SA_FIF_GL_CROSS_REF.process_s9t';
	l_process_status   svc_process_tracker.status%type;
        MAX_CHAR	   EXCEPTION;
        PRAGMA	           EXCEPTION_INIT(MAX_CHAR, -01706);

	BEGIN
		commit;--to ensure that the record in s9t_folder is commited
		s9t_pkg.ods2obj(I_file_id);
		commit;  
		L_file := s9t_pkg.get_obj(I_file_id);
		Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
		if s9t_pkg.code2desc(O_error_message,
		                     'RSAGLC',
                         l_file,
                         true)=FALSE then
       return FALSE;
		end if;
		s9t_pkg.save_obj(l_file);
    if s9t_pkg.validate_template(I_file_id) = false then

		write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'S9T_INVALID_TEMPLATE');

		else
		populate_names(I_file_id);

		sheet_name_trans := s9t_pkg.sheet_trans(l_file.template_key,l_file.user_lang);

		process_s9t_SA_GL_CROSS_REF(I_file_id,I_process_id);
		end if;
		O_error_count := Lp_s9t_errors_tab.count();
		forall i IN 1..O_error_count
		 INSERT INTO s9t_errors VALUES Lp_s9t_errors_tab
			(i
			);
		Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
		if O_error_count    = 0 THEN
		 l_process_status := 'PS';
		ELSE
		 l_process_status := 'PE';
		end if;
		UPDATE svc_process_tracker
		SET status       = l_process_status,
		 file_id        = I_file_id
		where process_id = I_process_id;
		commit;
		RETURN true;
	EXCEPTION

           when MAX_CHAR then
              ROLLBACK;
              O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
              Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
              write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
              O_error_count := Lp_s9t_errors_tab.count();
              forall i IN 1..O_error_count
	         insert into s9t_errors
	              values Lp_s9t_errors_tab(i);

              update svc_process_tracker
	         set status = 'PE',
	             file_id  = I_file_id
               where process_id = I_process_id;
      
              COMMIT;
      
              return FALSE;
	
	   WHEN OTHERS THEN
			O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
			RETURN FALSE;
        END process_s9t;
	-------------------------------------------------------------------------------
	---  Name: populate_lists
	--Purpose: To populate the column list drop down for the excel from code_detail
	-------------------------------------------------------------------------------- 

	FUNCTION populate_lists(
	 O_error_message IN OUT rtk_errors.rtk_text%type,
	 I_file_id       IN NUMBER) RETURN BOOLEAN
	IS
		L_program VARCHAR2(75) := 'STG_SVC_SA_FIF_GL_CROSS_REF.POPULATE_LISTS';
		l_s9t_action s9t_cells := NEW s9t_cells(stg_svc_sa_fif_gl_cross_ref.action_new,stg_svc_sa_fif_gl_cross_ref.action_mod,stg_svc_sa_fif_gl_cross_ref.action_del);
   BEGIN

		if S9T_PKG.populate_lists(O_error_message,
		                          I_file_id,
                              'RSAGLC')=FALSE then
       return FALSE;
		end if;
		RETURN true;
	EXCEPTION
		WHEN OTHERS THEN
			O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
			RETURN FALSE;
	end populate_lists;

end STG_SVC_SA_FIF_GL_CROSS_REF;
/ 