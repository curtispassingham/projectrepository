CREATE OR REPLACE PACKAGE CORESVC_FIF_GL_CROSS_REF AUTHID CURRENT_USER AS
   template_key                        CONSTANT VARCHAR2(255):='GL_CROSS_REF_DATA';
   action_new                          VARCHAR2(25)          := 'NEW';
   action_mod                          VARCHAR2(25)          := 'MOD';
   action_del                          VARCHAR2(25)          := 'DEL';
   FIF_GL_CROSS_REF_sheet              VARCHAR2(255)         := 'FIF_GL_CROSS_REF';
   FIF_GL_CROSS_REF$Action             NUMBER                :=1;
   FIF_GL_CROSS_REF$SET_OF_BKS_ID      NUMBER                :=3;
   FIF_GL_CROSS_REF$DEPT               NUMBER                :=4;
   FIF_GL_CROSS_REF$CLASS              NUMBER                :=5;
   FIF_GL_CROSS_REF$SUBCLASS           NUMBER                :=6;
   FIF_GL_CROSS_REF$LOCATION           NUMBER                :=7;
   FIF_GL_CROSS_REF$TRAN_CODE          NUMBER                :=8;
   FIF_GL_CROSS_REF$CST_RTL_FLG        NUMBER                :=9;
   FIF_GL_CROSS_REF$LINE_TYPE          NUMBER                :=10;
   FIF_GL_CROSS_REF$TRAN_REF_NO        NUMBER                :=11;
   FIF_GL_CROSS_REF$DR_CCID            NUMBER                :=12;
   FIF_GL_CROSS_REF$DR_SEQUENCE1       NUMBER                :=13;
   FIF_GL_CROSS_REF$DR_SEQUENCE2       NUMBER                :=14;
   FIF_GL_CROSS_REF$DR_SEQUENCE3       NUMBER                :=15;
   FIF_GL_CROSS_REF$DR_SEQUENCE4       NUMBER                :=16;
   FIF_GL_CROSS_REF$DR_SEQUENCE5       NUMBER                :=17;
   FIF_GL_CROSS_REF$DR_SEQUENCE6       NUMBER                :=18;
   FIF_GL_CROSS_REF$DR_SEQUENCE7       NUMBER                :=19;
   FIF_GL_CROSS_REF$DR_SEQUENCE8       NUMBER                :=20;
   FIF_GL_CROSS_REF$DR_SEQUENCE9       NUMBER                :=21;
   FIF_GL_CROSS_REF$DR_SEQUENCE10      NUMBER                :=22;
   FIF_GL_CROSS_REF$CR_CCID            NUMBER                :=23;
   FIF_GL_CROSS_REF$CR_SEQUENCE1       NUMBER                :=24;
   FIF_GL_CROSS_REF$CR_SEQUENCE2       NUMBER                :=25;
   FIF_GL_CROSS_REF$CR_SEQUENCE3       NUMBER                :=26;
   FIF_GL_CROSS_REF$CR_SEQUENCE4       NUMBER                :=27;
   FIF_GL_CROSS_REF$CR_SEQUENCE5       NUMBER                :=28;
   FIF_GL_CROSS_REF$CR_SEQUENCE6       NUMBER                :=29;
   FIF_GL_CROSS_REF$CR_SEQUENCE7       NUMBER                :=30;
   FIF_GL_CROSS_REF$CR_SEQUENCE8       NUMBER                :=31;
   FIF_GL_CROSS_REF$CR_SEQUENCE9       NUMBER                :=32;
   FIF_GL_CROSS_REF$CR_SEQUENCE10      NUMBER                :=33;
   FIF_GL_CROSS_REF$DR_SEQUENCE11      NUMBER                :=34;
   FIF_GL_CROSS_REF$DR_SEQUENCE12      NUMBER                :=35;
   FIF_GL_CROSS_REF$DR_SEQUENCE13      NUMBER                :=36;
   FIF_GL_CROSS_REF$DR_SEQUENCE14      NUMBER                :=37;
   FIF_GL_CROSS_REF$DR_SEQUENCE15      NUMBER                :=38;
   FIF_GL_CROSS_REF$DR_SEQUENCE16      NUMBER                :=39;
   FIF_GL_CROSS_REF$DR_SEQUENCE17      NUMBER                :=40;
   FIF_GL_CROSS_REF$DR_SEQUENCE18      NUMBER                :=41;
   FIF_GL_CROSS_REF$DR_SEQUENCE19      NUMBER                :=42;
   FIF_GL_CROSS_REF$DR_SEQUENCE20      NUMBER                :=43;
   FIF_GL_CROSS_REF$CR_SEQUENCE11      NUMBER                :=44;
   FIF_GL_CROSS_REF$CR_SEQUENCE12      NUMBER                :=45;
   FIF_GL_CROSS_REF$CR_SEQUENCE13      NUMBER                :=46;
   FIF_GL_CROSS_REF$CR_SEQUENCE14      NUMBER                :=47;
   FIF_GL_CROSS_REF$CR_SEQUENCE15      NUMBER                :=48;
   FIF_GL_CROSS_REF$CR_SEQUENCE16      NUMBER                :=49;
   FIF_GL_CROSS_REF$CR_SEQUENCE17      NUMBER                :=50;
   FIF_GL_CROSS_REF$CR_SEQUENCE18      NUMBER                :=51;
   FIF_GL_CROSS_REF$CR_SEQUENCE19      NUMBER                :=52;
   FIF_GL_CROSS_REF$CR_SEQUENCE20      NUMBER                :=53;
   

   sheet_name_trans S9T_PKG.trans_map_typ;

   action_column    VARCHAR2(255) := 'ACTION';

   template_category CODE_DETAIL.CODE%TYPE := 'RMSFIN';

   --TYPE FIF_GL_CROSS_REF_rec_tab IS TABLE OF FIF_GL_CROSS_REF%ROWTYPE;
-----------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)

   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
   ----------------------------------------------------------------------------------
END CORESVC_FIF_GL_CROSS_REF;
/