
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_REALM_SQL AS
-------------------------------------------------------------------
FUNCTION GET_NEXT_ID(O_error_message   IN OUT VARCHAR2,
                     O_realm_id        IN OUT sa_realm.realm_id%TYPE)
                     return BOOLEAN is

   L_realm_id_sequence       NUMBER;
   L_wrap_sequence_number    NUMBER;
   L_first_time              BOOLEAN := TRUE;
   L_dummy                   VARCHAR2(1);
 
   CURSOR C_REALM_ID_EXISTS(realm_id_param  VARCHAR2) IS
      select 'x'
        from sa_realm
       where sa_realm.realm_id  = realm_id_param;
 
BEGIN
   LOOP
      select sa_realm_id_sequence.NEXTVAL
        into L_realm_id_sequence
        from sys.dual;
 
      if(L_first_time = TRUE) then
        L_wrap_sequence_number := L_realm_id_sequence;
        L_first_time := FALSE;
      elsif(L_realm_id_sequence = L_wrap_sequence_number) then
        O_error_message := 'Fatal error - no available realm ids';
        return FALSE;
      else
         L_wrap_sequence_number := L_realm_id_sequence;
      end if;
 
      O_realm_id := 'REALM'||substr(to_char(L_realm_id_sequence,'0999999999'),2);
 
      open C_REALM_ID_EXISTS(O_realm_id);
      fetch C_REALM_ID_EXISTS into L_dummy;
      if (C_REALM_ID_EXISTS%NOTFOUND) then
         close C_REALM_ID_EXISTS;
         return TRUE;
      end if;
      close C_REALM_ID_EXISTS;
   END LOOP;
 
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                            'SA_REALM_SQL.GET_NEXT_ID', 
                                             to_char(SQLCODE));
   return FALSE;
END GET_NEXT_ID; 
------------------------------------------------------------------------
FUNCTION GET_MAX_REALM_ID(O_error_message     IN OUT  VARCHAR2,
                          O_new_realm_id      IN OUT  SA_REALM.REALM_ID%TYPE,
                          I_original_realm_id IN      SA_REALM.REALM_ID%TYPE)
   return BOOLEAN is
   ---
   L_realm_name  SA_REALM.PHYSICAL_NAME%TYPE;
   L_program     VARCHAR2(60)            := 'SA_REALM_SQL.GET_MAX_REALM_ID';
   ---
   cursor C_GET_REALM_NAME is
      select physical_name 
        from sa_realm
       where realm_id = I_original_realm_id;
   ---
   cursor C_GET_MAX_REALM_ID is
      select max(realm_id)
        from sa_realm
       where physical_name = L_realm_name;
BEGIN
   if I_original_realm_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   open  C_GET_REALM_NAME;
   fetch C_GET_REALM_NAME into L_realm_name;
   close C_GET_REALM_NAME;
   ---
   open  C_GET_MAX_REALM_ID;
   fetch C_GET_MAX_REALM_ID into O_new_realm_id;
   close C_GET_MAX_REALM_ID;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
   return FALSE;
END GET_MAX_REALM_ID;
------------------------------------------------------------------------
END SA_REALM_SQL;   
/
