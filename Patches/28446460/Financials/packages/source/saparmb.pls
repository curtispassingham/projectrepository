
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SA_PARM_SQL AS
------------------------------------------------------------------------
FUNCTION GET_NEXT_ID(O_error_message   IN OUT VARCHAR2,
                     O_parm_id         IN OUT sa_parm.parm_id%TYPE)
                     return BOOLEAN is

   L_parm_id_sequence       NUMBER;
   L_wrap_sequence_number    NUMBER;
   L_first_time              BOOLEAN := TRUE;
   L_dummy                   VARCHAR2(1);
 
   CURSOR C_PARM_ID_EXISTS(parm_id_param  VARCHAR2) IS
      select 'x'
        from sa_parm
       where sa_parm.parm_id  = parm_id_param;
 
BEGIN
   LOOP
      select sa_parm_id_sequence.NEXTVAL
        into L_parm_id_sequence
        from sys.dual;
 
      if(L_first_time = TRUE) then
        L_wrap_sequence_number := L_parm_id_sequence;
        L_first_time := FALSE;
      elsif(L_parm_id_sequence = L_wrap_sequence_number) then
        O_error_message := 'Fatal error - no available parm ids';
        return FALSE;
      else
         L_wrap_sequence_number := L_parm_id_sequence;
      end if;
 
      O_parm_id := 'PARM'||substr(to_char(L_parm_id_sequence,'09999999999'),2);
 
      open C_PARM_ID_EXISTS(O_parm_id);
      fetch C_PARM_ID_EXISTS into L_dummy;
      if (C_PARM_ID_EXISTS%NOTFOUND) then
         close C_PARM_ID_EXISTS;
         return TRUE;
      end if;
      close C_PARM_ID_EXISTS;
   END LOOP;
 
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                            'PARM_SQL.GET_NEXT_ID', 
                                             to_char(SQLCODE));
   return FALSE;
END GET_NEXT_ID; 
-----------------------------------------------------------------------
FUNCTION GET_REALM_ID(O_error_message IN OUT VARCHAR2,
                      O_realm_id      IN OUT sa_realm.realm_id%TYPE,
                      I_parm_id       IN     sa_parm.parm_id%TYPE)
                      return BOOLEAN is

   cursor C_GET_REALM is
      select realm_id
        from sa_parm p
       where p.parm_id = I_parm_id;

BEGIN
   open C_GET_REALM;
   fetch C_GET_REALM into O_realm_id;
   if C_GET_REALM%NOTFOUND then
      close C_GET_REALM;
      O_error_message := SQL_LIB.CREATE_MSG('ARI_INV_REALM_ID',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   close C_GET_REALM;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                            'PARM_SQL.GET_REALM_ID', 
                                             to_char(SQLCODE));
   return FALSE;
END GET_REALM_ID;
------------------------------------------------------------------------
FUNCTION GET_UPDATED_PARM_ID(O_error_message     IN OUT  VARCHAR2,
                             O_new_parm_id       IN OUT  SA_PARM.PARM_ID%TYPE,
                             I_original_parm_id  IN      SA_PARM.PARM_ID%TYPE,
                             I_original_realm_id IN      SA_REALM.REALM_ID%TYPE,
                             I_new_realm_id      IN      SA_REALM.REALM_ID%TYPE)

   return BOOLEAN is
   ---
   L_physical_name  SA_REALM.PHYSICAL_NAME%TYPE;
   L_program     VARCHAR2(60)            := 'SA_PARM_SQL.GET_UPDATED_PARM_ID';
   ---
   cursor C_GET_PHYSICAL_NAME is
      select physical_name 
        from sa_parm
       where realm_id = I_original_realm_id
         and parm_id  = I_original_parm_id;
   ---
   cursor C_GET_UPDATED_PARM_ID is
      select max(parm_id)
        from sa_parm
       where physical_name = L_physical_name
         and realm_id      = I_new_realm_id;
BEGIN
   if I_original_parm_id     is NULL or
         I_original_realm_id is NULL or
         I_new_realm_id      is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   open  C_GET_PHYSICAL_NAME;
   fetch C_GET_PHYSICAL_NAME into L_physical_name;
   close C_GET_PHYSICAL_NAME;
   ---
   open  C_GET_UPDATED_PARM_ID;
   fetch C_GET_UPDATED_PARM_ID into O_new_parm_id;
   close C_GET_UPDATED_PARM_ID;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
   return FALSE;
END GET_UPDATED_PARM_ID;
------------------------------------------------------------------------
END SA_PARM_SQL;
/

