CREATE OR REPLACE PACKAGE BODY CORESVC_SA_CONSTANTS AS
   cursor C_SVC_SA_CONSTANTS(I_process_id NUMBER,
                             I_chunk_id   NUMBER) is
      select pk_sa_constants.rowid  AS pk_sa_constants_rid,
             st.rowid AS st_rid,
             st.delete_ind,
             st.value_data_type,
             st.constant_value,
             st.constant_name,
             st.constant_id,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status,
             pk_sa_constants.delete_ind db_delete_ind
        from svc_sa_constants st,
             sa_constants pk_sa_constants
       where st.process_id = I_process_id
         and st.chunk_id = I_chunk_id
         and st.constant_id = pk_sa_constants.constant_id (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;

   LP_errors_tab errors_tab_typ;

   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;

   LP_s9t_errors_tab s9t_errors_tab_typ;

   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;

----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE,
                       I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   LP_errors_tab(LP_errors_tab.COUNT()).error_type  := I_error_type;
END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets               s9t_pkg.names_map_typ;
   SA_CONSTANTS_cols      s9t_pkg.names_map_typ;
   SA_CONSTANTS_TL_cols   s9t_pkg.names_map_typ;
BEGIN
   L_sheets                       := s9t_pkg.get_sheet_names(I_file_id);
   SA_CONSTANTS_cols              := s9t_pkg.get_col_names(I_file_id,SA_CONSTANTS_sheet);
   SA_CONSTANTS$Action            := SA_CONSTANTS_cols('ACTION');
   SA_CONSTANTS$DELETE_IND        := SA_CONSTANTS_cols('DELETE_IND');
   SA_CONSTANTS$VALUE_DATA_TYPE   := SA_CONSTANTS_cols('VALUE_DATA_TYPE');
   SA_CONSTANTS$CONSTANT_VALUE    := SA_CONSTANTS_cols('CONSTANT_VALUE');
   SA_CONSTANTS$CONSTANT_NAME     := SA_CONSTANTS_cols('CONSTANT_NAME');
   SA_CONSTANTS$CONSTANT_ID       := SA_CONSTANTS_cols('CONSTANT_ID');
   
   SA_CONSTANTS_TL_cols            := s9t_pkg.get_col_names(I_file_id,SA_CONSTANTS_TL_sheet);
   SA_CONSTANTS_TL$Action          := SA_CONSTANTS_TL_cols('ACTION');
   SA_CONSTANTS_TL$CONSTANT_NAME   := SA_CONSTANTS_TL_cols('CONSTANT_NAME');
   SA_CONSTANTS_TL$CONSTANT_ID     := SA_CONSTANTS_TL_cols('CONSTANT_ID');
   SA_CONSTANTS_TL$LANG            := SA_CONSTANTS_TL_cols('LANG');
   
END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_SA_CONSTANTS( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = SA_CONSTANTS_sheet )
   select s9t_row(s9t_cells(CORESVC_SA_CONSTANTS.action_mod ,
                            constant_id,
                            constant_name,
                            constant_value,
                            value_data_type,
                            delete_ind
                           ))
     from sa_constants ;
END POPULATE_SA_CONSTANTS;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_SA_CONSTANTS_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = SA_CONSTANTS_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_SA_CONSTANTS.action_mod ,
                            lang,
                            constant_id,
                            constant_name
                           ))
     from sa_constants_tl ;
END POPULATE_SA_CONSTANTS_TL;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(SA_CONSTANTS_sheet);
   L_file.sheets(l_file.get_sheet_index(SA_CONSTANTS_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                          'CONSTANT_ID',
                                                                                          'CONSTANT_NAME',
                                                                                          'CONSTANT_VALUE',
                                                                                          'VALUE_DATA_TYPE',
                                                                                          'DELETE_IND'
                                                                                          );
   L_file.add_sheet(SA_CONSTANTS_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(SA_CONSTANTS_TL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                             'LANG',
                                                                                             'CONSTANT_ID',
                                                                                             'CONSTANT_NAME' );
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file    s9t_file;
   L_program VARCHAR2(64):='CORESVC_SA_CONSTANTS.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   
   if I_template_only_ind = 'N' then
      POPULATE_SA_CONSTANTS(O_file_id);
      POPULATE_SA_CONSTANTS_TL(O_file_id);
      COMMIT;
   end if;
   
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SA_CONSTANTS( I_file_id      IN   s9t_folder.file_id%TYPE,
                                    I_process_id   IN   SVC_SA_CONSTANTS.process_id%TYPE) IS
   TYPE svc_SA_CONSTANTS_col_typ IS TABLE OF SVC_SA_CONSTANTS%ROWTYPE;
   L_temp_rec             SVC_SA_CONSTANTS%ROWTYPE;
   svc_SA_CONSTANTS_col   svc_SA_CONSTANTS_col_typ := NEW svc_SA_CONSTANTS_col_typ();
   L_process_id           SVC_SA_CONSTANTS.process_id%TYPE;
   L_error                BOOLEAN:=FALSE;
   L_default_rec          SVC_SA_CONSTANTS%ROWTYPE;

   cursor C_MANDATORY_IND is
      select DELETE_IND_mi,
             VALUE_DATA_TYPE_mi,
             CONSTANT_VALUE_mi,
             CONSTANT_NAME_mi,
             CONSTANT_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'SA_CONSTANTS'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'DELETE_IND' AS DELETE_IND,
                                         'VALUE_DATA_TYPE' AS VALUE_DATA_TYPE,
                                         'CONSTANT_VALUE' AS CONSTANT_VALUE,
                                         'CONSTANT_NAME' AS CONSTANT_NAME,
                                         'CONSTANT_ID' AS CONSTANT_ID,
                                         null as dummy));

      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);

      L_pk_columns    VARCHAR2(255)  := 'Constant Id';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  -- Get default values.
   FOR rec IN (select  DELETE_IND_dv,
                       VALUE_DATA_TYPE_dv,
                       CONSTANT_VALUE_dv,
                       CONSTANT_NAME_dv,
                       CONSTANT_ID_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = CORESVC_SA_CONSTANTS.template_key
                          and wksht_key    = 'SA_CONSTANTS'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'DELETE_IND' AS DELETE_IND,
                                                      'VALUE_DATA_TYPE' AS VALUE_DATA_TYPE,
                                                      'CONSTANT_VALUE' AS CONSTANT_VALUE,
                                                      'CONSTANT_NAME' AS CONSTANT_NAME,
                                                      'CONSTANT_ID' AS CONSTANT_ID,
                                                       NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.DELETE_IND := rec.DELETE_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SA_CONSTANTS ' ,
                            NULL,
                           'DELETE_IND ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.VALUE_DATA_TYPE := rec.VALUE_DATA_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SA_CONSTANTS ' ,
                            NULL,
                            'VALUE_DATA_TYPE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CONSTANT_VALUE := rec.CONSTANT_VALUE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SA_CONSTANTS ' ,
                            NULL,
                            'CONSTANT_VALUE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CONSTANT_NAME := rec.CONSTANT_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SA_CONSTANTS ' ,
                            NULL,
                            'CONSTANT_NAME ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CONSTANT_ID := rec.CONSTANT_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SA_CONSTANTS ' ,
                            NULL,
                            'CONSTANT_ID ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(SA_CONSTANTS$Action)            AS Action,
          r.get_cell(SA_CONSTANTS$DELETE_IND)        AS DELETE_IND,
          r.get_cell(SA_CONSTANTS$VALUE_DATA_TYPE)   AS VALUE_DATA_TYPE,
          r.get_cell(SA_CONSTANTS$CONSTANT_VALUE)    AS CONSTANT_VALUE,
          r.get_cell(SA_CONSTANTS$CONSTANT_NAME)     AS CONSTANT_NAME,
          r.get_cell(SA_CONSTANTS$CONSTANT_ID)       AS CONSTANT_ID,
          r.get_row_seq()                            AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = GET_SHEET_NAME_TRANS(SA_CONSTANTS_sheet)
  )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SA_CONSTANTS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DELETE_IND := rec.DELETE_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SA_CONSTANTS_sheet,
                            rec.row_seq,
                            'DELETE_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.VALUE_DATA_TYPE := rec.VALUE_DATA_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SA_CONSTANTS_sheet,
                            rec.row_seq,
                            'VALUE_DATA_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CONSTANT_VALUE := rec.CONSTANT_VALUE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SA_CONSTANTS_sheet,
                            rec.row_seq,
                            'CONSTANT_VALUE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CONSTANT_NAME := rec.CONSTANT_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SA_CONSTANTS_sheet,
                            rec.row_seq,
                            'CONSTANT_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CONSTANT_ID := rec.CONSTANT_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SA_CONSTANTS_sheet,
                            rec.row_seq,
                            'CONSTANT_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_SA_CONSTANTS.action_new then
         L_temp_rec.DELETE_IND      := NVL( L_temp_rec.DELETE_IND,L_default_rec.DELETE_IND);
         L_temp_rec.VALUE_DATA_TYPE := NVL( L_temp_rec.VALUE_DATA_TYPE,L_default_rec.VALUE_DATA_TYPE);
         L_temp_rec.CONSTANT_VALUE  := NVL( L_temp_rec.CONSTANT_VALUE,L_default_rec.CONSTANT_VALUE);
         L_temp_rec.CONSTANT_NAME   := NVL( L_temp_rec.CONSTANT_NAME,L_default_rec.CONSTANT_NAME);
         L_temp_rec.CONSTANT_ID     := NVL( L_temp_rec.CONSTANT_ID,L_default_rec.CONSTANT_ID);
      end if;
      if not (
            L_temp_rec.CONSTANT_ID is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
                         SA_CONSTANTS_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_SA_CONSTANTS_col.extend();
         svc_SA_CONSTANTS_col(svc_SA_CONSTANTS_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_SA_CONSTANTS_col.COUNT SAVE EXCEPTIONS
      merge into SVC_SA_CONSTANTS st
      using(select
                  (case
                   when l_mi_rec.DELETE_IND_mi    = 'N'
                    and svc_SA_CONSTANTS_col(i).action = CORESVC_SA_CONSTANTS.action_mod
                    and s1.DELETE_IND IS NULL
                   then mt.DELETE_IND
                   else s1.DELETE_IND
                   end) AS DELETE_IND,
                  (case
                   when l_mi_rec.VALUE_DATA_TYPE_mi    = 'N'
                    and svc_SA_CONSTANTS_col(i).action = CORESVC_SA_CONSTANTS.action_mod
                    and s1.VALUE_DATA_TYPE IS NULL
                   then mt.VALUE_DATA_TYPE
                   else s1.VALUE_DATA_TYPE
                   end) AS VALUE_DATA_TYPE,
                  (case
                   when l_mi_rec.CONSTANT_VALUE_mi    = 'N'
                    and svc_SA_CONSTANTS_col(i).action = CORESVC_SA_CONSTANTS.action_mod
                    and s1.CONSTANT_VALUE IS NULL
                   then mt.CONSTANT_VALUE
                   else s1.CONSTANT_VALUE
                   end) AS CONSTANT_VALUE,
                  (case
                   when l_mi_rec.CONSTANT_NAME_mi    = 'N'
                    and svc_SA_CONSTANTS_col(i).action = CORESVC_SA_CONSTANTS.action_mod
                    and s1.CONSTANT_NAME IS NULL
                   then mt.CONSTANT_NAME
                   else s1.CONSTANT_NAME
                   end) AS CONSTANT_NAME,
                  (case
                   when l_mi_rec.CONSTANT_ID_mi    = 'N'
                    and svc_SA_CONSTANTS_col(i).action = CORESVC_SA_CONSTANTS.action_mod
                    and s1.CONSTANT_ID IS NULL
                   then mt.CONSTANT_ID
                   else s1.CONSTANT_ID
                   end) AS CONSTANT_ID,
                  null as dummy
              from (select
                          svc_SA_CONSTANTS_col(i).DELETE_IND AS DELETE_IND,
                          svc_SA_CONSTANTS_col(i).VALUE_DATA_TYPE AS VALUE_DATA_TYPE,
                          svc_SA_CONSTANTS_col(i).CONSTANT_VALUE AS CONSTANT_VALUE,
                          svc_SA_CONSTANTS_col(i).CONSTANT_NAME AS CONSTANT_NAME,
                          svc_SA_CONSTANTS_col(i).CONSTANT_ID AS CONSTANT_ID,
                          null as dummy
                      from dual ) s1,
            SA_CONSTANTS mt
             where
                  mt.CONSTANT_ID (+)     = s1.CONSTANT_ID   and
                  1 = 1 )sq
                on (
                    st.CONSTANT_ID      = sq.CONSTANT_ID and
                    svc_SA_CONSTANTS_col(i).ACTION IN (CORESVC_SA_CONSTANTS.action_mod,CORESVC_SA_CONSTANTS.action_del))
      when matched then
      update
         set process_id        = svc_SA_CONSTANTS_col(i).process_id ,
             chunk_id          = svc_SA_CONSTANTS_col(i).chunk_id ,
             row_seq           = svc_SA_CONSTANTS_col(i).row_seq ,
             action            = svc_SA_CONSTANTS_col(i).action ,
             process$status    = svc_SA_CONSTANTS_col(i).process$status ,
             value_data_type   = sq.value_data_type ,
             constant_value    = sq.constant_value ,
             delete_ind        = sq.delete_ind ,
             constant_name     = sq.constant_name ,
             create_id         = svc_SA_CONSTANTS_col(i).create_id ,
             create_datetime   = svc_SA_CONSTANTS_col(i).create_datetime ,
             last_upd_id       = svc_SA_CONSTANTS_col(i).last_upd_id ,
             last_upd_datetime = svc_SA_CONSTANTS_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             delete_ind ,
             value_data_type ,
             constant_value ,
             constant_name ,
             constant_id ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_SA_CONSTANTS_col(i).process_id ,
             svc_SA_CONSTANTS_col(i).chunk_id ,
             svc_SA_CONSTANTS_col(i).row_seq ,
             svc_SA_CONSTANTS_col(i).action ,
             svc_SA_CONSTANTS_col(i).process$status ,
             sq.delete_ind ,
             sq.value_data_type ,
             sq.constant_value ,
             sq.constant_name ,
             sq.constant_id ,
             svc_SA_CONSTANTS_col(i).create_id ,
             svc_SA_CONSTANTS_col(i).create_datetime ,
             svc_SA_CONSTANTS_col(i).last_upd_id ,
             svc_SA_CONSTANTS_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            SA_CONSTANTS_sheet,
                            svc_SA_CONSTANTS_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_SA_CONSTANTS;
--------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SA_CONSTANTS_TL( I_file_id      IN   s9t_folder.file_id%TYPE,
                                       I_process_id   IN   SVC_SA_CONSTANTS_TL.process_id%TYPE) IS
   TYPE svc_SA_CONSTANTS_TL_col_typ IS TABLE OF SVC_SA_CONSTANTS_TL%ROWTYPE;
   L_temp_rec                SVC_SA_CONSTANTS_TL%ROWTYPE;
   svc_SA_CONSTANTS_TL_col   svc_SA_CONSTANTS_TL_col_typ :=NEW svc_SA_CONSTANTS_TL_col_typ();
   L_process_id              SVC_SA_CONSTANTS_TL.process_id%TYPE;
   L_error                   BOOLEAN:=FALSE;
   L_default_rec             SVC_SA_CONSTANTS_TL%ROWTYPE;
   
   cursor C_MANDATORY_IND is
      select
             CONSTANT_NAME_mi,
             CONSTANT_ID_mi,
             LANG_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'SA_CONSTANTS_TL'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                             'CONSTANT_NAME' AS CONSTANT_NAME,
                                             'CONSTANT_ID' AS CONSTANT_ID,
                                             'LANG' AS LANG,
                                             null as dummy));

   l_mi_rec       c_mandatory_ind%ROWTYPE;
   dml_errors     EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);

   L_pk_columns   VARCHAR2(255)  := 'Constant Id, Lang';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select
                       CONSTANT_NAME_dv,
                       CONSTANT_ID_dv,
                       LANG_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'SA_CONSTANTS_TL'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'CONSTANT_NAME' AS CONSTANT_NAME,
                                                      'CONSTANT_ID' AS CONSTANT_ID,
                                                      'LANG' AS LANG,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.CONSTANT_NAME := rec.CONSTANT_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SA_CONSTANTS_TL ' ,
                            NULL,
                            'CONSTANT_NAME ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CONSTANT_ID := rec.CONSTANT_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SA_CONSTANTS_TL ' ,
                            NULL,
                            'CONSTANT_ID ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SA_CONSTANTS_TL ' ,
                            NULL,
                            'LANG ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   
   FOR rec IN (select r.get_cell(SA_CONSTANTS_TL$Action)        AS Action,
                      r.get_cell(SA_CONSTANTS_TL$CONSTANT_NAME) AS CONSTANT_NAME,
                      r.get_cell(SA_CONSTANTS_TL$CONSTANT_ID)   AS CONSTANT_ID,
                      r.get_cell(SA_CONSTANTS_TL$LANG)          AS LANG,
                      r.get_row_seq()                           AS row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(SA_CONSTANTS_TL_sheet)
               )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SA_CONSTANTS_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CONSTANT_NAME := rec.CONSTANT_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SA_CONSTANTS_TL_sheet,
                            rec.row_seq,
                            'CONSTANT_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.CONSTANT_ID := rec.CONSTANT_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SA_CONSTANTS_TL_sheet,
                            rec.row_seq,
                            'CONSTANT_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SA_CONSTANTS_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_SA_CONSTANTS.action_new then
         L_temp_rec.CONSTANT_NAME := NVL( L_temp_rec.CONSTANT_NAME,L_default_rec.CONSTANT_NAME);
         L_temp_rec.CONSTANT_ID   := NVL( L_temp_rec.CONSTANT_ID,L_default_rec.CONSTANT_ID);
         L_temp_rec.LANG          := NVL( L_temp_rec.LANG,L_default_rec.LANG);
      end if;

      if not (
            L_temp_rec.LANG is NOT NULL and
            L_temp_rec.CONSTANT_ID is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
                         SA_CONSTANTS_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                        SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_SA_CONSTANTS_TL_col.extend();
         svc_SA_CONSTANTS_TL_col(svc_SA_CONSTANTS_TL_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   
   BEGIN
      forall i IN 1..svc_SA_CONSTANTS_TL_col.COUNT SAVE EXCEPTIONS
         merge into SVC_SA_CONSTANTS_TL st
         using(select (case
                          when l_mi_rec.CONSTANT_NAME_mi    = 'N' and
                               svc_SA_CONSTANTS_TL_col(i).action = CORESVC_SA_CONSTANTS.action_mod and
                               s1.CONSTANT_NAME IS NULL then 
                               mt.CONSTANT_NAME
                          else s1.CONSTANT_NAME
                          end) AS CONSTANT_NAME,
                      (case
                          when l_mi_rec.CONSTANT_ID_mi    = 'N' and
                               svc_SA_CONSTANTS_TL_col(i).action = CORESVC_SA_CONSTANTS.action_mod and
                               s1.CONSTANT_ID IS NULL then
                               mt.CONSTANT_ID
                          else s1.CONSTANT_ID
                          end) AS CONSTANT_ID,
                      (case
                          when l_mi_rec.LANG_mi    = 'N' and
                               svc_SA_CONSTANTS_TL_col(i).action = CORESVC_SA_CONSTANTS.action_mod and
                               s1.LANG IS NULL then 
                               mt.LANG
                          else s1.LANG
                          end) AS LANG,
                          null as dummy
                 from (select svc_SA_CONSTANTS_TL_col(i).CONSTANT_NAME AS CONSTANT_NAME,
                              svc_SA_CONSTANTS_TL_col(i).CONSTANT_ID AS CONSTANT_ID,
                              svc_SA_CONSTANTS_TL_col(i).LANG AS LANG,
                              null as dummy
                         from dual ) s1,
                              SA_CONSTANTS_TL mt
                        where mt.LANG (+) = s1.LANG   and
                              mt.CONSTANT_ID (+) = s1.CONSTANT_ID   and
                              1 = 1 )sq
                on (st.LANG = sq.LANG and
                    st.CONSTANT_ID = sq.CONSTANT_ID and
                    svc_SA_CONSTANTS_TL_col(i).ACTION IN (CORESVC_SA_CONSTANTS.action_mod,
                                                          CORESVC_SA_CONSTANTS.action_del))
      when matched then
         update
            set process_id        = svc_SA_CONSTANTS_TL_col(i).process_id ,
                chunk_id          = svc_SA_CONSTANTS_TL_col(i).chunk_id ,
                row_seq           = svc_SA_CONSTANTS_TL_col(i).row_seq ,
                action            = svc_SA_CONSTANTS_TL_col(i).action ,
                process$status    = svc_SA_CONSTANTS_TL_col(i).process$status ,
                constant_name     = sq.constant_name ,
                create_id         = svc_SA_CONSTANTS_TL_col(i).create_id ,
                create_datetime   = svc_SA_CONSTANTS_TL_col(i).create_datetime ,
                last_upd_id       = svc_SA_CONSTANTS_TL_col(i).last_upd_id ,
                last_upd_datetime = svc_SA_CONSTANTS_TL_col(i).last_upd_datetime
      when NOT matched then
         insert(process_id,
                chunk_id ,
                row_seq ,
                action ,
                process$status ,
                constant_name ,
                constant_id ,
                lang ,
                create_id ,
                create_datetime ,
                last_upd_id ,
                last_upd_datetime)
         values(svc_SA_CONSTANTS_TL_col(i).process_id ,
                svc_SA_CONSTANTS_TL_col(i).chunk_id ,
                svc_SA_CONSTANTS_TL_col(i).row_seq ,
                svc_SA_CONSTANTS_TL_col(i).action ,
                svc_SA_CONSTANTS_TL_col(i).process$status ,
                sq.constant_name ,
                sq.constant_id ,
                sq.lang ,
                svc_SA_CONSTANTS_TL_col(i).create_id ,
                svc_SA_CONSTANTS_TL_col(i).create_datetime ,
                svc_SA_CONSTANTS_TL_col(i).last_upd_id ,
                svc_SA_CONSTANTS_TL_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT 
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
         end if;

         WRITE_S9T_ERROR( I_file_id,
                          SA_CONSTANTS_TL_sheet,
                          svc_SA_CONSTANTS_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);

      END LOOP;
END;      
END PROCESS_S9T_SA_CONSTANTS_TL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT        OUT   NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_file             s9t_file;
   L_sheets           s9t_pkg.names_map_typ;
   L_program          VARCHAR2(64):='CORESVC_SA_CONSTANTS.process_s9t';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;

   s9t_pkg.save_obj(L_file);

   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_SA_CONSTANTS(I_file_id,I_process_id);
      PROCESS_S9T_SA_CONSTANTS_TL(I_file_id,I_process_id);
   end if;

   O_error_COUNT := LP_s9t_errors_tab.COUNT();

   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);

   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', 
                                            NULL, 
                                            NULL, 
                                            NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert 
           into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION EXEC_SA_CONSTANTS_INS( O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sa_constants_temp_rec   IN   SA_CONSTANTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64):= 'CORESVC_SA_CONSTANTS.EXEC_SA_CONSTANTS_INS';
   L_table     VARCHAR2(64):= 'SVC_SA_CONSTANTS';
BEGIN
   insert
     into sa_constants
   values I_sa_constants_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SA_CONSTANTS_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_SA_CONSTANTS_UPD( O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sa_constants_temp_rec   IN       SA_CONSTANTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64):= 'CORESVC_SA_CONSTANTS.EXEC_SA_CONSTANTS_UPD';
   L_table         VARCHAR2(64):= 'SVC_SA_CONSTANTS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_SA_CONSTANTS is 
      select 'x' 
        from sa_constants
       where constant_id = I_sa_constants_temp_rec.constant_id
       for update nowait;

BEGIN
   open C_LOCK_SA_CONSTANTS;
   close C_LOCK_SA_CONSTANTS;

   update sa_constants
      set row = I_sa_constants_temp_rec
    where 1 = 1
      and constant_id = I_sa_constants_temp_rec.constant_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_sa_constants_temp_rec.constant_id,
                                             null);
      close C_LOCK_SA_CONSTANTS;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_SA_CONSTANTS%ISOPEN then
         close C_LOCK_SA_CONSTANTS;
      end if;
      return FALSE;
END EXEC_SA_CONSTANTS_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_SA_CONSTANTS_DEL( O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sa_constants_temp_rec   IN       SA_CONSTANTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64):= 'CORESVC_SA_CONSTANTS.EXEC_SA_CONSTANTS_DEL';
   L_table         VARCHAR2(64):= 'SVC_SA_CONSTANTS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);   

   cursor C_LOCK_SA_CONSTANTS is 
      select 'x' 
        from sa_constants
       where constant_id = I_sa_constants_temp_rec.constant_id
         for update nowait;

   cursor C_LOCK_SA_CONSTANTS_TL is 
      select 'x' 
        from sa_constants_tl
       where constant_id = I_sa_constants_temp_rec.constant_id
       for update nowait;

BEGIN
   open C_LOCK_SA_CONSTANTS;
   close C_LOCK_SA_CONSTANTS;

   open C_LOCK_SA_CONSTANTS_TL;
   close C_LOCK_SA_CONSTANTS_TL;

   delete
     from sa_constants_tl
    where 1 = 1
      and constant_id = I_sa_constants_temp_rec.constant_id;

   delete
     from sa_constants
    where 1 = 1
      and constant_id = I_sa_constants_temp_rec.constant_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_sa_constants_temp_rec.constant_id,
                                             NULL);
      if C_LOCK_SA_CONSTANTS%ISOPEN then
         close C_LOCK_SA_CONSTANTS;
      end if;
      if C_LOCK_SA_CONSTANTS_TL%ISOPEN then
         close C_LOCK_SA_CONSTANTS_TL;
      end if;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_SA_CONSTANTS%ISOPEN then
         close C_LOCK_SA_CONSTANTS;
      end if;
      if C_LOCK_SA_CONSTANTS_TL%ISOPEN then
         close C_LOCK_SA_CONSTANTS_TL;
      end if;
      return FALSE;
END EXEC_SA_CONSTANTS_DEL;
--------------------------------------------------------------------------------
FUNCTION EXEC_SA_CONSTANTS_TL_INS( O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sa_constants_tl_temp_rec   IN       SA_CONSTANTS_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64):= 'CORESVC_SA_CONSTANTS_TL.EXEC_SA_CONSTANTS_TL_INS';
   L_table     VARCHAR2(64):= 'SVC_SA_CONSTANTS_TL';
BEGIN
   insert
     into sa_constants_tl
   values I_sa_constants_tl_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SA_CONSTANTS_TL_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_SA_CONSTANTS_TL_UPD( O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sa_constants_tl_temp_rec   IN       SA_CONSTANTS_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64):= 'CORESVC_SA_CONSTANTS_TL.EXEC_SA_CONSTANTS_TL_UPD';
   L_table         VARCHAR2(64):= 'SVC_SA_CONSTANTS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_SA_CONSTANTS_TL_UPD is
      select 'x'
        from sa_constants_tl
       where constant_id = I_sa_constants_tl_temp_rec.constant_id
         and lang = I_sa_constants_tl_temp_rec.lang
         for update nowait;

BEGIN
   open C_LOCK_SA_CONSTANTS_TL_UPD;
   close C_LOCK_SA_CONSTANTS_TL_UPD;

   update sa_constants_tl
      set constant_name = I_sa_constants_tl_temp_rec.constant_name,
          last_update_id = I_sa_constants_tl_temp_rec.last_update_id,
          last_update_datetime = I_sa_constants_tl_temp_rec.last_update_datetime
    where lang = I_sa_constants_tl_temp_rec.lang
      and constant_id = I_sa_constants_tl_temp_rec.constant_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_sa_constants_tl_temp_rec.constant_id,
                                             I_sa_constants_tl_temp_rec.lang);
      close C_LOCK_SA_CONSTANTS_TL_UPD;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_SA_CONSTANTS_TL_UPD%ISOPEN then
         close C_LOCK_SA_CONSTANTS_TL_UPD;
      end if;
      return FALSE;
END EXEC_SA_CONSTANTS_TL_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_SA_CONSTANTS_TL_DEL( O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sa_constants_tl_temp_rec   IN       SA_CONSTANTS_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64):= 'CORESVC_SA_CONSTANTS_TL.EXEC_SA_CONSTANTS_TL_DEL';
   L_table         VARCHAR2(64):= 'SVC_SA_CONSTANTS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_SA_CONSTANTS_TL_DEL is
      select 'x'
        from sa_constants_tl
       where constant_id = I_sa_constants_tl_temp_rec.constant_id
         and lang = I_sa_constants_tl_temp_rec.lang
         for update nowait;
BEGIN
   open C_LOCK_SA_CONSTANTS_TL_DEL;
   close C_LOCK_SA_CONSTANTS_TL_DEL;

   delete sa_constants_tl
    where lang = I_sa_constants_tl_temp_rec.lang
      and constant_id = I_sa_constants_tl_temp_rec.constant_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_sa_constants_tl_temp_rec.constant_id,
                                             I_sa_constants_tl_temp_rec.lang);
      close C_LOCK_SA_CONSTANTS_TL_DEL;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_SA_CONSTANTS_TL_DEL%ISOPEN then
         close C_LOCK_SA_CONSTANTS_TL_DEL;
      end if;
      return FALSE;
END EXEC_SA_CONSTANTS_TL_DEL;
--------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN

   delete from svc_sa_constants_tl
      where process_id = I_process_id;

   delete from svc_sa_constants
      where process_id = I_process_id;

END CLEAR_STAGING_DATA;
--------------------------------------------------------------------------------
FUNCTION PROCESS_SA_CONSTANTS( O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN     SVC_SA_CONSTANTS.PROCESS_ID%TYPE,
                               I_chunk_id        IN     SVC_SA_CONSTANTS.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error                   BOOLEAN;
   L_process_error           BOOLEAN := FALSE;
   L_program                 VARCHAR2(64):='CORESVC_SA_CONSTANTS.PROCESS_SA_CONSTANTS';
   L_SA_CONSTANTS_temp_rec   SA_CONSTANTS%ROWTYPE;
   L_table                   VARCHAR2(64) :=  'SVC_SA_CONSTANTS';
   L_base_trans_table        VARCHAR2(64) :=  'SA_CONSTANTS';
BEGIN
   FOR rec IN c_svc_SA_CONSTANTS(I_process_id,
                                 I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

      if rec.action = action_new
         and rec.PK_SA_CONSTANTS_rid is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               L_base_trans_table,
                                               NULL,
                                               NULL);
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CONSTANTS_ID',
                     O_error_message);
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.PK_SA_CONSTANTS_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CONSTANTS_ID',
                     'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;

      if NOT(  rec.CONSTANT_ID  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CONSTANT_ID',
                     'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;

      if rec.action in (action_mod, action_new) then
         if NOT(  rec.CONSTANT_VALUE  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CONSTANT_VALUE',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;
         
         if NOT(  rec.VALUE_DATA_TYPE  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'VALUE_DATA_TYPE',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;
         
         if NOT(  rec.DELETE_IND  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DELETE_IND',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;
         
         if NOT(  rec.VALUE_DATA_TYPE IN  ( 'C','N','D' )  ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'VALUE_DATA_TYPE',
                        'INV_VALUE');
            L_error :=TRUE;
         end if;
         
         if NOT(  rec.DELETE_IND IN  ( 'Y','N' )  ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DELETE_IND',
                        'INV_Y_N_IND');
            L_error :=TRUE;
         end if;
      end if;

      if rec.action = action_del then
         if rec.db_delete_ind = 'N' then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           'CANNOT_DEL_CONSTANT');
               L_error :=TRUE;
         end if;
      end if;

      if NOT L_error then
         L_sa_constants_temp_rec.constant_id       := rec.constant_id;
         L_sa_constants_temp_rec.constant_name     := rec.constant_name;
         L_sa_constants_temp_rec.constant_value    := rec.constant_value;
         L_sa_constants_temp_rec.value_data_type   := rec.value_data_type;
         L_sa_constants_temp_rec.delete_ind        := rec.delete_ind;
         if rec.action = action_new then
            if EXEC_SA_CONSTANTS_INS( O_error_message,
                                      L_sa_constants_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_SA_CONSTANTS_UPD( O_error_message,
                                      L_sa_constants_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if rec.db_delete_ind = 'Y' then
              if EXEC_SA_CONSTANTS_DEL( O_error_message,
                                        L_sa_constants_temp_rec)=FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              rec.action,
                              O_error_message);
                  L_process_error :=TRUE;
              end if;
           end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SA_CONSTANTS;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_SA_CONSTANTS_TL( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id      IN   SVC_SA_CONSTANTS_TL.PROCESS_ID%TYPE,
                                  I_chunk_id        IN   SVC_SA_CONSTANTS_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error                      BOOLEAN := FALSE;
   L_process_error              BOOLEAN := FALSE;
   L_program                    VARCHAR2(64):='CORESVC_SA_CONSTANTS_TL.PROCESS_SA_CONSTANTS_TL';
   L_SA_CONSTANTS_TL_temp_rec   SA_CONSTANTS_TL%ROWTYPE;
   L_table                      VARCHAR2(64) := 'SVC_SA_CONSTANTS_TL';
   L_base_trans_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SA_CONSTANTS_TL';

   cursor C_SVC_SA_CONSTANTS_TL(I_process_id NUMBER,
                                I_chunk_id NUMBER) is
      select pk_sa_constants_tl.rowid  AS pk_sa_constants_tl_rid,
             st.rowid AS st_rid,
             sct_sc_fk.rowid    AS sct_sc_fk_rid,
             sct_lang_fk.rowid  AS sct_lang_fk_rid,
             st.constant_name,
             st.constant_id,
             st.lang,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_sa_constants_tl st,
             sa_constants_tl pk_sa_constants_tl,
             sa_constants sct_sc_fk,
             lang sct_lang_fk,
             dual
       where st.process_id   = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.lang         = pk_sa_constants_tl.lang (+)
         and st.constant_id  = pk_sa_constants_tl.constant_id (+)
         and st.constant_id  = sct_sc_fk.constant_id (+)
         and st.lang         = sct_lang_fk.lang (+);   

   TYPE SVC_SA_CONSTANTS_TL is TABLE OF C_SVC_SA_CONSTANTS_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_constants_tl_tab        SVC_SA_CONSTANTS_TL;

BEGIN
   if C_SVC_SA_CONSTANTS_TL%ISOPEN then
      close C_SVC_SA_CONSTANTS_TL;
   end if;

   open C_SVC_SA_CONSTANTS_TL(I_process_id,
                              I_chunk_id);
   LOOP
      fetch C_SVC_SA_CONSTANTS_TL bulk collect into L_svc_constants_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_constants_tl_tab.COUNT > 0 then
         FOR i in L_svc_constants_tl_tab.FIRST..L_svc_constants_tl_tab.LAST LOOP
            L_error := FALSE;
            if L_svc_constants_tl_tab(i).action is NULL
               or L_svc_constants_tl_tab(i).action NOT IN (action_new,action_mod,action_del) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_constants_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=true;
            end if;

            if L_svc_constants_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_constants_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            if L_svc_constants_tl_tab(i).action = action_new
               and L_svc_constants_tl_tab(i).PK_SA_CONSTANTS_TL_rid is NOT NULL then
               O_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                      L_base_trans_table,
                                                      NULL,
                                                      NULL);

               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_constants_tl_tab(i).row_seq,
                           NULL,
                           O_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_constants_tl_tab(i).action IN (action_mod,action_del)
               and L_svc_constants_tl_tab(i).PK_SA_CONSTANTS_TL_rid is NULL
               and L_svc_constants_tl_tab(i).LANG is NOT NULL
               and L_svc_constants_tl_tab(i).constant_id is NOT NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_constants_tl_tab(i).row_seq,
                           NULL,
                          'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            if L_svc_constants_tl_tab(i).action = action_new then
               if L_svc_constants_tl_tab(i).sct_sc_fk_rid is NULL 
                  and L_svc_constants_tl_tab(i).constant_id is not null then
                  dbms_output.put_line('SCT_SC_FK');
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_constants_tl_tab(i).row_seq,
                              'CONSTANT_ID',
                              'INV_CONSTANT_ID');
                  L_error :=TRUE;
               end if;
               
               if L_svc_constants_tl_tab(i).sct_lang_fk_rid is NULL 
                  and L_svc_constants_tl_tab(i).lang is NOT NULL then
                  dbms_output.put_line('SCT_LANG_FK');
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_constants_tl_tab(i).row_seq,
                              'LANG',
                              'LANG_EXIST');
                  L_error :=TRUE;
               end if;
            end if;

            if NOT(  L_svc_constants_tl_tab(i).CONSTANT_ID  IS NOT NULL ) then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_constants_tl_tab(i).row_seq,
                              'CONSTANT_ID',
                              'FIELD_NOT_NULL');
                  L_error :=TRUE;
            end if;

            if NOT(  L_svc_constants_tl_tab(i).LANG  IS NOT NULL ) then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_constants_tl_tab(i).row_seq,
                              'LANG',
                              'FIELD_NOT_NULL');
                  L_error :=TRUE;
            end if;

            if L_svc_constants_tl_tab(i).action in (action_mod, action_new) then
               if NOT(  L_svc_constants_tl_tab(i).CONSTANT_NAME  IS NOT NULL ) then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_constants_tl_tab(i).row_seq,
                              'CONSTANT_NAME',
                              'FIELD_NOT_NULL');
                  L_error :=TRUE;
               end if;
            end if;

            if NOT L_error then
               L_sa_constants_tl_temp_rec.lang                   := L_svc_constants_tl_tab(i).lang;
               L_sa_constants_tl_temp_rec.constant_id            := L_svc_constants_tl_tab(i).constant_id;
               L_sa_constants_tl_temp_rec.constant_name          := L_svc_constants_tl_tab(i).constant_name;
               L_sa_constants_tl_temp_rec.create_id              := GET_USER;
               L_sa_constants_tl_temp_rec.create_datetime        := SYSDATE;
               L_sa_constants_tl_temp_rec.last_update_id         := GET_USER;
               L_sa_constants_tl_temp_rec.last_update_datetime   := SYSDATE;

               if L_svc_constants_tl_tab(i).action = action_new then
                  if EXEC_SA_CONSTANTS_TL_INS( O_error_message,
                                               L_sa_constants_tl_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_svc_constants_tl_tab(i).row_seq,
                                 L_svc_constants_tl_tab(i).action,
                                 O_error_message);
                     L_process_error :=TRUE;
                  end if;
               end if;

               if L_svc_constants_tl_tab(i).action = action_mod then
                  if EXEC_SA_CONSTANTS_TL_UPD( O_error_message,
                                               L_sa_constants_tl_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_svc_constants_tl_tab(i).row_seq,
                                 L_svc_constants_tl_tab(i).action,
                                 O_error_message);
                     L_process_error :=TRUE;
                  end if;
               end if;
               
               if L_svc_constants_tl_tab(i).action = action_del then
                  if EXEC_SA_CONSTANTS_TL_DEL( O_error_message,
                                               L_sa_constants_tl_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_svc_constants_tl_tab(i).row_seq,
                                 L_svc_constants_tl_tab(i).action,
                                 O_error_message);
                     L_process_error :=TRUE;
                  end if;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_SA_CONSTANTS_TL%NOTFOUND;
   END LOOP;
   close C_SVC_SA_CONSTANTS_TL;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SA_CONSTANTS_TL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER
                  )
RETURN BOOLEAN IS
   L_program          VARCHAR2(64):='CORESVC_SA_CONSTANTS.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_SA_CONSTANTS(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_SA_CONSTANTS_TL(O_error_message,
                              I_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);

   LP_errors_tab := NEW errors_tab_typ();
   
   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------------
END CORESVC_SA_CONSTANTS;
/
