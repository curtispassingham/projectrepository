CREATE OR REPLACE PACKAGE BODY STKLEDGR_SQL AS
-------------------------------------------------------
--------------------------------------------------------

LP_total_retail_38        TRAN_DATA.TOTAL_RETAIL%TYPE := 0;
LP_from_loc               TRAN_DATA.LOCATION%TYPE     := NULL;
LP_to_loc                 TRAN_DATA.LOCATION%TYPE     := NULL;
LP_vat_amount_rtl         TRAN_DATA.TOTAL_RETAIL%TYPE := NULL;
LP_vat_amount_cost        TRAN_DATA.TOTAL_COST%TYPE   := NULL;
LP_vat_amount_cost_f_curr TRAN_DATA.TOTAL_COST%TYPE   := NULL;
LP_franchise_transaction  VARCHAR2(1)                 := NULL;
LP_vat_code_cost_rtl      VAT_ITEM.VAT_CODE%TYPE      := NULL;
LP_exempt_ind             VARCHAR2(1)                 := 'N';
--------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
--------------------------------------------------------------------------
-- Function Name:PROCESS_WF_WRITE_FINANCIALS
-- Purpose: The function is called by WF_WRITE_FINANCIALS to post tran_data records
--          for all warehouse/store sourced Franchise transactions
--------------------------------------------------------------------------
FUNCTION PROCESS_WF_WRITE_FINANCIALS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_tsf_alloc_unit_cost OUT    TRAN_DATA.TOTAL_COST%TYPE,
                                     O_unit_retail         OUT    TRAN_DATA.TOTAL_RETAIL%TYPE,
                                     I_distro_no           IN     SHIPSKU.DISTRO_NO%TYPE,
                                     I_distro_type         IN     SHIPSKU.DISTRO_TYPE%TYPE,
                                     I_tran_date           IN     PERIOD.VDATE%TYPE,
                                     I_item                IN     ITEM_MASTER.ITEM%TYPE,
                                     I_pack_no             IN     ITEM_MASTER.ITEM%TYPE,
                                     I_pct_in_pack         IN     NUMBER,
                                     I_dept                IN     ITEM_MASTER.DEPT%TYPE,
                                     I_class               IN     ITEM_MASTER.CLASS%TYPE,
                                     I_subclass            IN     ITEM_MASTER.SUBCLASS%TYPE,
                                     I_qty                 IN     TSFDETAIL.TSF_QTY%TYPE,
                                     I_from_loc            IN     ITEM_LOC.LOC%TYPE,
                                     I_from_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                                     I_to_loc              IN     ITEM_LOC.LOC%TYPE,
                                     I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                                     I_weight_cuom         IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                                     I_rma_no              IN     WF_RETURN_HEAD.RMA_NO%TYPE,
                                     I_shipment            IN     SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                                     I_carton              IN     SHIPSKU.CARTON%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name:PROCESS_WF_WRITE_FINANCIALS_PO
-- Purpose: The function is called by WF_WRITE_FINANCIALS_PO to post tran_data records
--          for all supplier sourced Franchise transactions
---------------------------------------------------------------------------
FUNCTION PROCESS_WF_WRITE_FINANCIALS_PO(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                                        I_tran_date           IN     PERIOD.VDATE%TYPE,
                                        I_item                IN     ITEM_MASTER.ITEM%TYPE,
                                        I_pack_no             IN     ITEM_MASTER.ITEM%TYPE,
                                        I_pct_in_pack         IN     NUMBER,
                                        I_dept                IN     ITEM_MASTER.DEPT%TYPE,
                                        I_class               IN     ITEM_MASTER.CLASS%TYPE,
                                        I_subclass            IN     ITEM_MASTER.SUBCLASS%TYPE,
                                        I_qty                 IN     ORDLOC.QTY_ORDERED%TYPE,
                                        I_costing_loc         IN     ITEM_LOC.LOC%TYPE,
                                        I_costing_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                                        I_loc                 IN     ITEM_LOC.LOC%TYPE,
                                        I_loc_type            IN     ITEM_LOC.LOC_TYPE%TYPE,
                                        I_weight_cuom         IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION INIT_TRAN_DATA_INSERT (O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)     := 'STKLEDGR_SQL.INIT_TRAN_DATA_INSERT';

BEGIN

   P_tran_data_size := 0;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,
                                            L_program,NULL);
      RETURN FALSE;
END INIT_TRAN_DATA_INSERT;
---------------------------------------------------------------------------------
FUNCTION SET_SAVEPOINT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)     := 'STKLEDGR_SQL.SET_SAVEPOINT';

BEGIN
   P_tran_data_savepoint := P_tran_data_size;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      RETURN FALSE;
END SET_SAVEPOINT;
---------------------------------------------------------------------------------
FUNCTION ROLLBACK_TO_SAVEPOINT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)     := 'STKLEDGR_SQL.ROLLBACK_TO_SAVEPOINT';

BEGIN
   P_tran_data_size      := P_tran_data_savepoint;
   P_tran_data_savepoint := 0;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      RETURN FALSE;
END ROLLBACK_TO_SAVEPOINT;
---------------------------------------------------------------------------------
FUNCTION GET_DECODE(O_error_message IN OUT   VARCHAR2,
                    O_decode        IN OUT   TRAN_DATA_CODES.DECODE%TYPE,
                    I_code          IN       TRAN_DATA_CODES.CODE%TYPE) RETURN BOOLEAN IS

   L_program     VARCHAR2(64)  :=  'STKLEDGR_SQL.GET_DECODE';
   L_cursor      VARCHAR2(20)   := NULL;

   cursor C_DECODE is
      select decode
        from tran_data_codes
       where code = I_code;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_DECODE','TRAN_DATA_CODES', NULL);
   open C_DECODE;
   SQL_LIB.SET_MARK('FETCH','C_DECODE','TRAN_DATA_CODES', NULL);
   fetch C_DECODE into O_decode;
   ---
   if C_DECODE%NOTFOUND then
      close C_DECODE;
      L_cursor := 'C_DECODE';
      raise NO_DATA_FOUND;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_DECODE','TRAN_DATA_CODES', NULL);
   close C_DECODE;

   return TRUE;

EXCEPTION
   when NO_DATA_FOUND then
      O_error_message := sql_lib.create_msg('INV_TRAN_CODE',NULL,
                                            NULL,NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,
                                            L_program ,SQLCODE);
      return FALSE;
END GET_DECODE;
------------------------------------------------------------------------------
FUNCTION BUILD_TRAN_DATA_INSERT(O_error_message         IN OUT  VARCHAR2,
                                I_item                  IN      TRAN_DATA.ITEM%TYPE,
                                I_dept                  IN      TRAN_DATA.DEPT%TYPE,
                                I_class                 IN      TRAN_DATA.CLASS%TYPE,
                                I_subclass              IN      TRAN_DATA.SUBCLASS%TYPE,
                                I_location              IN      TRAN_DATA.LOCATION%TYPE,
                                I_loc_type              IN      TRAN_DATA.LOC_TYPE%TYPE,
                                I_tran_date             IN      TRAN_DATA.TRAN_DATE%TYPE,
                                IO_tran_code            IN OUT  TRAN_DATA.TRAN_CODE%TYPE,
                                I_adj_code              IN      TRAN_DATA.ADJ_CODE%TYPE,
                                I_units                 IN      TRAN_DATA.UNITS%TYPE,
                                IO_total_cost           IN OUT  TRAN_DATA.TOTAL_COST%TYPE,
                                I_total_retail          IN      TRAN_DATA.TOTAL_RETAIL%TYPE,
                                I_to_loc_unit_retail    IN      TRAN_DATA.TOTAL_RETAIL%TYPE,
                                I_ref_no_1              IN      TRAN_DATA.REF_NO_1%TYPE,
                                I_ref_no_2              IN      TRAN_DATA.REF_NO_2%TYPE,
                                I_tsf_source_location   IN      TRAN_DATA.LOCATION%TYPE,
                                I_tsf_source_loc_type   IN      TRAN_DATA.LOC_TYPE%TYPE,
                                I_old_unit_retail       IN      TRAN_DATA.OLD_UNIT_RETAIL%TYPE,
                                I_new_unit_retail       IN      TRAN_DATA.NEW_UNIT_RETAIL%TYPE,
                                I_source_dept           IN      TRAN_DATA.DEPT%TYPE,
                                I_source_class          IN      TRAN_DATA.CLASS%TYPE,
                                I_source_subclass       IN      TRAN_DATA.SUBCLASS%TYPE,
                                I_pgm_name              IN      TRAN_DATA.PGM_NAME%TYPE,
                                I_gl_ref_no             IN      TRAN_DATA.GL_REF_NO%TYPE DEFAULT NULL,
                                I_distro_type           IN      VARCHAR2 DEFAULT NULL,
                                I_pack_ind              IN      ITEM_MASTER.PACK_IND%TYPE DEFAULT NULL,
                                I_sellable_ind          IN      ITEM_MASTER.SELLABLE_IND%TYPE DEFAULT NULL,
                                I_orderable_ind         IN      ITEM_MASTER.ORDERABLE_IND%TYPE DEFAULT NULL,
                                I_pack_type             IN      ITEM_MASTER.PACK_TYPE%TYPE DEFAULT NULL,
                                I_vat_rate              IN      VAT_CODE_RATES.VAT_RATE%TYPE DEFAULT NULL,
                                I_class_vat_ind         IN      CLASS.CLASS_VAT_IND%TYPE DEFAULT NULL,
                                I_ref_pack_no           IN      TRAN_DATA.REF_PACK_NO%TYPE DEFAULT NULL,
                                I_total_cost_excl_elc   IN      TRAN_DATA.TOTAL_COST_EXCL_ELC%TYPE DEFAULT NULL,
                                I_tax_value             IN      GTAX_ITEM_ROLLUP.CUM_TAX_PCT%TYPE DEFAULT NULL,
                                I_from_wac              IN      ITEM_LOC_SOH.AV_COST%TYPE DEFAULT NULL,
                                I_extended_base_cost    IN      ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE DEFAULT NULL,
                                I_base_cost             IN      ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE DEFAULT NULL,
                                I_ship_date             IN      shipment.ship_date%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

-- for transfers (tran_type 30/32),
-- IO total_cost and I_total_retail use the sending location's cost/retail
-- in the sending location's currency, NOT the receiving location's.

   L_cum_markon_pct       week_data.cum_markon_pct%TYPE    := 0;
   L_calc                 VARCHAR2(1);

   L_tsf_markdown_loc     VARCHAR2(1) := NULL;

   L_qty                  v_packsku_qty.qty%TYPE;
   L_item                 tran_data.item%TYPE;
   L_dept                 tran_data.dept%TYPE;
   L_class                tran_data.class%TYPE;
   L_subclass             tran_data.subclass%TYPE;
   L_units                tran_data.units%TYPE;
   L_total_cost           tran_data.total_cost%TYPE;
   L_total_retail         tran_data.total_retail%TYPE;
   L_pack_ind             item_master.pack_ind%TYPE;
   L_sellable_ind         item_master.sellable_ind%TYPE;
   L_orderable_ind        item_master.orderable_ind%TYPE;
   L_pack_type            item_master.pack_type%TYPE;
   L_item_level           item_master.item_level%TYPE;
   L_tran_level           item_master.tran_level%TYPE;
   L_std_av_ind           system_options.std_av_ind%TYPE;
   L_to_curr_code         store.currency_code%TYPE;
   L_from_curr_code       store.currency_code%TYPE;
   L_loc_type             tran_data.loc_type%TYPE;
   L_location             tran_data.location%TYPE;
   L_return_code          VARCHAR2(5);
   L_error_message        VARCHAR2(255);
   L_cursor               VARCHAR2(25);
   L_program              VARCHAR2(64)     := 'STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT';
   L_rca_ind              VARCHAR2(1);

   cursor C_PACK_ITEM is
      select item,
             qty
        from v_packsku_qty
       where pack_no = I_item;

   cursor C_GET_ITEM_DATA is
      select dept,
             class,
             subclass
        from item_master
       where item = L_item;

   cursor C_GET_COST_RETAIL is
      select NVL(ils.av_cost * L_units, 0),
             NVL(il.unit_retail * L_units, 0)
        from item_loc il,
             item_loc_soh ils
       where il.item = L_item
         and il.item = ils.item
         and il.loc = L_location
         and il.loc = ils.loc;

   cursor C_SKU_LOCATION is
      select il.loc,
             ils.loc_type,
             ils.av_cost,
             ils.unit_cost,
             (ils.stock_on_hand + ils.pack_comp_soh + ils.in_transit_qty + ils.pack_comp_intran) stock_on_hand,
             il.unit_retail
        from item_loc il,
             item_loc_soh ils
       where il.item = I_item
         and ils.item = il.item
         and ils.loc = il.loc
         and ils.loc_type = il.loc_type
         and (ils.stock_on_hand + ils.pack_comp_soh + ils.in_transit_qty + ils.pack_comp_intran) != 0;
--------------------------------------------------------------------------
FUNCTION CALC_TAX_AMT(O_error_message       IN OUT   VARCHAR2,
                      IOF_vat_cost_rtl      IN OUT   TSFDETAIL.TSF_COST%TYPE,
                      IOF_vat_code_cost_rtl IN OUT   VAT_CODE_RATES.VAT_CODE%TYPE,
                      IF_total_cost         IN       tran_data.total_cost%TYPE,
                      IF_item               IN       tran_data.item%TYPE,
                      IF_dept               IN       tran_data.dept%TYPE,
                      IF_import_id          IN       ORDHEAD.IMPORT_ID%TYPE,
                      IF_import_type        IN       ORDHEAD.IMPORT_TYPE%TYPE,
                      IF_send_entity        IN       VARCHAR2,
                      IF_send_entity_type   IN       VARCHAR2,
                      IF_store_type         IN       STORE.STORE_TYPE%TYPE,
                      IF_rtv_supplier       IN       RTV_HEAD.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   L_tax_info_tbl                OBJ_TAX_INFO_TBL := OBJ_TAX_INFO_TBL();
   L_tax_info_rec                OBJ_TAX_INFO_REC :=OBJ_TAX_INFO_REC();
   L_supp_site                   ORDHEAD.SUPPLIER%TYPE;
   L_wf_order_no                 ORDHEAD.WF_ORDER_NO%TYPE;
   L_costing_loc                 ITEM_LOC.LOC%TYPE;
   L_costing_loc_type            VARCHAR(2);
   L_to_loc_type                 VARCHAR2(2) := NULL;

   cursor C_ORD_SUPS is
      select supplier,
             wf_order_no
        from ordhead
    where order_no = I_ref_no_1;

   cursor C_GET_COSTING_LOC is
      select distinct(il.costing_loc),
             decode(il.costing_loc_type,'S','ST','W','WH') costing_loc_type,
             oh.supplier
        from item_loc il,
             ordhead oh,
             ordloc ol
       where ol.item = il.item
         and il.item = IF_item
         and il.loc = IF_send_entity
         and oh.wf_order_no = I_ref_no_2
         and ol.location = il.loc;

   cursor C_GET_TO_LOC_TYPE is
      select 'ST'
        from store
       where store = LP_to_loc
       union
      select 'WH'
        from wh
       where wh = LP_to_loc;

BEGIN

   if IO_tran_code = 20 then
      -- I_total_cost_excl_elc is NULL if called by the sales upload process.  Use IF_total_cost.
      IOF_vat_cost_rtl := NVL(I_total_cost_excl_elc, IF_total_cost);
   elsif IO_tran_code in (24,65) then
      IOF_vat_cost_rtl := IF_total_cost;
   end if;

   L_tax_info_rec := OBJ_TAX_INFO_REC();
   L_tax_info_rec.item                  := IF_item;
   L_tax_info_rec.pack_ind              := 'N';
   L_tax_info_rec.merch_hier_value      := IF_Dept;
   L_tax_info_rec.merch_hier_level      := 4;

   -- For import orders , From entity should be Invoice Location and not
   -- the Destination location as Tran Code 20 is posted for Invoice Location
   if IF_import_id is NOT NULL and NVL(IF_import_type,'X') <> 'F' then
      L_tax_info_rec.from_entity           := IF_import_id;
      L_tax_info_rec.from_entity_type      := IF_import_type;
   else
      L_tax_info_rec.from_entity           := IF_send_entity;
      L_tax_info_rec.from_entity_type      := IF_send_entity_type;
   end if;
   if IO_tran_code = 20 then
      if NVL(IF_store_type,'x') <> 'F' then
         open C_ORD_SUPS;
         fetch C_ORD_SUPS into L_supp_site,L_wf_order_no;
         close C_ORD_SUPS;
         if L_wf_order_no is not null then
            L_tax_info_rec.to_entity           := L_supp_site;
            L_tax_info_rec.to_entity_type      := 'SU';
            L_tax_info_rec.source_entity       := L_supp_site;
            L_tax_info_rec.source_entity_type  := 'SU';
         else
            L_tax_info_rec.to_entity       := L_supp_site;
            L_tax_info_rec.to_entity_type  := 'SU';
         end if;
      elsif NVL(IF_store_type,'x') = 'F' then
            open C_GET_COSTING_LOC;
            fetch C_GET_COSTING_LOC into L_costing_loc,
                                         L_costing_loc_type,
                                         L_supp_site;
            close C_GET_COSTING_LOC;
            L_tax_info_rec.to_entity           := L_costing_loc;
            L_tax_info_rec.to_entity_type      := L_costing_loc_type;
            L_tax_info_rec.source_entity       := L_supp_site;
            L_tax_info_rec.source_entity_type  := 'SU';
      end if;
      L_tax_info_rec.tran_type          := 'PO';
   else
      if NVL(IF_store_type,'x') = 'F' then
         L_tax_info_rec.to_entity       := LP_to_loc;
         open C_GET_TO_LOC_TYPE;
         fetch C_GET_TO_LOC_TYPE into L_to_loc_type;
         close C_GET_TO_LOC_TYPE;
         L_tax_info_rec.to_entity_type  := L_to_loc_type;
      else
         L_tax_info_rec.to_entity       := IF_rtv_supplier;
         L_tax_info_rec.to_entity_type  := 'SU';
      end if;
      if IO_tran_code = 24 then
         L_tax_info_rec.tran_type          := 'RTV';
      end if;
   end if;
   L_tax_info_rec.tran_date             := I_tran_date;
   L_tax_info_rec.tran_id               := I_ref_no_1;
   L_tax_info_rec.amount                := IOF_vat_cost_rtl;
   if IO_tran_code in (20,24,65) then
      L_tax_info_rec.cost_retail_ind       := 'C';
   else
      L_tax_info_rec.cost_retail_ind       := 'R';
   end if;
   L_tax_info_rec.tax_incl_ind          := 'N';
   L_tax_info_rec.tran_code             := IO_tran_code;
   L_tax_info_rec.active_date           := I_tran_date;
   L_tax_info_rec.ref_no_1              := I_ref_no_1;
   L_tax_info_rec.ref_no_2              := I_ref_no_2;
   L_tax_info_tbl.DELETE();
   L_tax_info_tbl.EXTEND();
   L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
   ---
   if TAX_SQL.POST_TAX_AMOUNT(O_error_message,
                              L_tax_info_tbl) = FALSE then
      return FALSE;
   end if;

   IOF_vat_cost_rtl := L_tax_info_tbl(L_tax_info_tbl.COUNT).tax_amount;
   IOF_vat_code_cost_rtl := L_tax_info_tbl(L_tax_info_tbl.COUNT).tax_code;

   return TRUE;

EXCEPTION
   when NO_DATA_FOUND then
      O_error_message := sql_lib.create_msg('INV_CURSOR',
                                            L_cursor,L_program,NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,L_program,NULL);
      RETURN FALSE;
END CALC_TAX_AMT;
--------------------------------------------------------------------------
FUNCTION RECORD_INSERTS(IF_item           IN       tran_data.item%TYPE,
                        IF_dept           IN       tran_data.dept%TYPE,
                        IF_class          IN       tran_data.class%TYPE,
                        IF_subclass       IN       tran_data.subclass%TYPE,
                        IF_location       IN       tran_data.location%TYPE,
                        IF_loc_type       IN       tran_data.loc_type%TYPE,
                        IF_units          IN       tran_data.units%TYPE,
                        IOF_total_cost    IN OUT   tran_data.total_cost%TYPE,
                        IF_total_retail   IN       tran_data.total_retail%TYPE,
                        I_ship_date       IN       shipment.ship_date%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   --- working totals during calculation
   LF_inputted_total_retail     tran_data.total_retail%TYPE;
   LF_total_retail              tran_data.total_retail%TYPE;
   LF_total_cost                tran_data.total_cost%TYPE;
   LF_input_old_unit_retail     tran_data.old_unit_retail%TYPE;
   LF_input_new_unit_retail     tran_data.new_unit_retail%TYPE;
   LF_old_unit_retail           tran_data.old_unit_retail%TYPE;
   LF_new_unit_retail           tran_data.new_unit_retail%TYPE;
   --- location info
   LF_loc_type                  varchar2(1);
   LF_location                  store.store%TYPE;
   --- average, unit, and total costs for comparisons with standard accounting
   LF_std_av_cost               tran_data.total_cost%TYPE;
   LF_std_cost_to_loc           tran_data.total_cost%TYPE;
   LF_total_std_cost_loc        tran_data.total_cost%TYPE;
   LF_unit_retail               item_loc.unit_retail%TYPE;
   LF_selling_unit_retail       item_loc.selling_unit_retail%TYPE;
   LF_selling_uom               item_loc.selling_uom%TYPE;

   LF_selling_uom_loc           ITEM_LOC.SELLING_UOM%TYPE;
   LF_selling_unit_retail_loc   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   LF_multi_units_loc           ITEM_LOC.MULTI_UNITS%TYPE;
   LF_multi_selling_uom_loc     ITEM_LOC.MULTI_SELLING_UOM%TYPE;
   LF_multi_unit_retail_loc     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;

   --- difference between overall total cost and standard cost at loc
   LF_cost_diff_loc             tran_data.total_cost%TYPE;
   --- vat info

   LF_vat_incl_ind              system_options.stkldgr_vat_incl_retl_ind%TYPE;
   ---
   --- These variables are only used when tran_type = 30 or 31
   ---
   --- receiving location's unit retail in currency of sender
   LF_recv_unit_retl_send_curr   tran_data.total_retail%TYPE;
   --- receiving location's unit_retail in own currency
   LF_recv_unit_retl             tran_data.total_retail%TYPE;
   L_recv_unit_retl_for_notax    tran_data.total_retail%TYPE;
   --- sender loc's unit retail in currency of receiver
   LF_send_unit_retl_recv_curr   tran_data.total_retail%TYPE;
   --- sender loc's unit retail in own currency
   LF_send_unit_retl             tran_data.total_retail%TYPE;
   --- total retail/cost for tsf_out to post after a tran_type 30 or 31
   LF_total_retail_32_33         tran_data.total_retail%TYPE;
   LF_total_cost_32_33           tran_data.total_cost%TYPE;
   --- store and wh for posting markup/down after a transfer
   LF_mkup_location              tran_data.location%TYPE;
   LF_mkup_loc_type              tran_data.loc_type%TYPE;
   --- receiver/sender unit retails for markup/down after a transfer
   LF_mkup_recv_retl             tran_data.total_retail%TYPE;
   LF_mkup_send_retl             tran_data.total_retail%TYPE;

   ---
   LF_item_rec                   item_master%ROWTYPE;
   LF_container_rec              item_master%ROWTYPE;
   --- container item's retail cost
   LF_container_unit_cost        item_supp_country.unit_cost%TYPE;
   --- container item's unit retail
   LF_container_unit_retail      item_loc.unit_retail%TYPE;
   ---
   LF_container_total_cost       item_supp_country.unit_cost%TYPE;
   LF_container_total_retail     item_loc.unit_retail%TYPE;
   ---
   LF_tsf_markdown_loc           tran_data.location%TYPE;
   --- tsf cost for where the transaction is a transfer
   LF_tsf_cost                   tsfdetail.tsf_cost%TYPE;

   LF_vat_cost_rtl               TSFDETAIL.TSF_COST%TYPE;
   LF_vat_code_cost_rtl          VAT_CODE_RATES.VAT_CODE%TYPE := NULL;
   --Variable added for container item
   LF_container_vat_cost_rtl        TSFDETAIL.TSF_COST%TYPE;
   LF_container_vat_code_cost_rtl   VAT_CODE_RATES.VAT_CODE%TYPE := NULL;
   ---variables added for legal entities mod
   LF_import_id                  ORDHEAD.IMPORT_ID%TYPE;
   LF_import_type                ORDHEAD.IMPORT_TYPE%TYPE;


   L_tax_add_tbl                 OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();
   L_tax_add_rec                 OBJ_TAX_RETAIL_ADD_REMOVE_REC;
   L_tax_remove_tbl              OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();
   L_tax_remove_rec              OBJ_TAX_RETAIL_ADD_REMOVE_REC;
   L_vdate                       PERIOD.VDATE%TYPE             := GET_VDATE;

   --- variables for TAX_SQL
   L_send_entity_type            VARCHAR2(6);
   L_send_entity                 VARCHAR2(10);
   L_rcv_entity_type             VARCHAR2(6);
   L_rcv_entity                  VARCHAR2(10);
   L_tax_info_tbl                OBJ_TAX_INFO_TBL := OBJ_TAX_INFO_TBL();
   L_tax_info_rec                OBJ_TAX_INFO_REC :=OBJ_TAX_INFO_REC();

   L_to_loc_currency             ORDHEAD.CURRENCY_CODE%TYPE;
   L_imp_exp_currency            WH.CURRENCY_CODE%TYPE;
   L_tsf_order_no                TSFHEAD.ORDER_NO%TYPE;

   ---variables added to check vat_region of from and to entity for the PO or WF order
   LF_from_entity_vat_reg        VAT_REGION.VAT_REGION%TYPE;
   LF_to_entity_vat_reg          VAT_REGION.VAT_REGION%TYPE;
   LF_wf_from_entity             TSFHEAD.FROM_LOC%TYPE;
   LF_wf_to_entity               TSFHEAD.TO_LOC%TYPE;
   L_wf_to_type                  TSFHEAD.TO_LOC_TYPE%TYPE;
   L_wf_from_type                TSFHEAD.FROM_LOC_TYPE%TYPE;

   --variable for Wrapper Function
   L_l10n_fin_rec                L10N_FIN_REC := L10N_FIN_REC();
   L_tran_data_rec               TRAN_DATA_REC := TRAN_DATA_REC();
   L_tran_code                   TRAN_DATA.TRAN_CODE%TYPE;
   L_total_cost                  TRAN_DATA.TOTAL_COST%TYPE;
   L_total_retail                TRAN_DATA.TOTAL_RETAIL%TYPE;
   L_ref_no_1                    TRAN_DATA.REF_NO_1%TYPE;
   L_loc                         TRAN_DATA.LOCATION%TYPE;
   L_ref_no_2                    TRAN_DATA.REF_NO_2%TYPE := NULL;
   L_gl_ref_no                   TRAN_DATA.GL_REF_NO%TYPE := NULL;
   L_ref_pack_no                 TRAN_DATA.REF_PACK_NO%TYPE;
   L_tran_loc                    TRAN_DATA.LOCATION%TYPE;
   L_tran_loc_type               TRAN_DATA.LOC_TYPE%TYPE;
   L_cost_dept_exists            VARCHAR2(1) := NULL;

   L_temp_retail                 ITEM_LOC.UNIT_RETAIL%TYPE := NULL;
   LF_to_curr_code               CURRENCIES.CURRENCY_CODE%TYPE;
   L_store_type                  STORE.STORE_TYPE%TYPE   := NULL;
   L_supp_curr_code              CURRENCIES.CURRENCY_CODE%TYPE;
   L_supp_unit_cost              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;

   L_supp_site                   ORDHEAD.SUPPLIER%TYPE;
   L_wf_order_no                 ORDHEAD.WF_ORDER_NO%TYPE;
   L_to_loc_type                 VARCHAR2(2) := NULL;
   L_rtv_supplier                RTV_HEAD.SUPPLIER%TYPE;   
   L_import_loc_retail           ITEM_LOC.UNIT_RETAIL%TYPE := NULL;
   L_import_temp_retail          ITEM_LOC.UNIT_RETAIL%TYPE := NULL;
   L_costing_loc                 ITEM_LOC.LOC%TYPE;
   L_costing_loc_type            VARCHAR(2);
   --- for SVAT
   L_default_tax_type            SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;
   L_stkldgr_vat_incl_retl_ind   SYSTEM_OPTIONS.STKLDGR_VAT_INCL_RETL_IND%TYPE;
   L_class_level_vat_ind         SYSTEM_OPTIONS.CLASS_LEVEL_VAT_IND%TYPE;
   L_tsf_type                    TSFHEAD.TSF_TYPE%TYPE;
   LP_item                       ITEM_MASTER.ITEM%TYPE;
   L_rcv_qty                     SHIPITEM_INV_FLOW.RECEIVED_QTY%TYPE;
   L_intercompany                BOOLEAN;
   L_from_loc                    ITEM_LOC.LOC%TYPE;
   L_final_from_loc              ITEM_LOC.LOC%TYPE;
   L_final_from_loc_type         VARCHAR2(2);
   L_from_loc_type               ITEM_LOC.LOC_TYPE%TYPE;
   L_to_loc                      ITEM_LOC.LOC%TYPE;
   L_final_to_loc_type           VARCHAR2(2);
   L_class_vat_ind               CLASS.CLASS_VAT_IND%TYPE;
   L_phy_wh_ind                  VARCHAR2(1);
   L_tax_rate                    VAT_CODE_RATES.VAT_RATE%TYPE;
   L_tax_code                    VAT_CODE_RATES.VAT_CODE%TYPE;
   L_exempt_ind                  VARCHAR2(1);

   cursor C_CALC is
      select 'x'
        from deps
       where dept = IF_dept
         and profit_calc_type = 2;

   cursor C_GET_OLD_HIER is
      select old_dept,old_class,old_subclass
        from reclass_item_temp
       where item = IF_item;

   cursor C_RECLASS_MARKON_WK is
      select nvl(cum_markon_pct, 0)
        from week_data
       where dept = L_dept
         and class = L_class
         and subclass = L_subclass
         and currency_ind = 'L'
         and cum_markon_pct IS NOT NULL
         and eow_date <= trunc( I_tran_date )
         and location = L_location
         and loc_type = L_loc_type
       order by eow_date desc;

   cursor C_RECLASS_MARKON_MN is
      select cum_markon_pct
        from month_data
       where dept = L_dept
         and class = L_class
         and subclass = L_subclass
         and currency_ind = 'L'
         and cum_markon_pct IS NOT NULL
         and eom_date <= trunc( I_tran_date )
         and location = L_location
         and loc_type = L_loc_type
       order by eom_date desc;

    cursor C_MARKON_WK is
      select nvl(cum_markon_pct, 0)
        from week_data
       where dept = IF_dept
         and class = IF_class
         and subclass = IF_subclass
         and currency_ind = 'L'
         and cum_markon_pct IS NOT NULL
         and eow_date <= trunc( I_tran_date )
         and location = L_location
         and loc_type = L_loc_type
       order by eow_date desc;

   cursor C_MARKON_MN is
      select cum_markon_pct
        from month_data
       where dept = IF_dept
         and class = IF_class
         and subclass = IF_subclass
         and currency_ind = 'L'
         and cum_markon_pct IS NOT NULL
         and eom_date <= trunc( I_tran_date )
         and location = L_location
         and loc_type = L_loc_type
       order by eom_date desc;

   cursor C_GET_CONTAINER_COST is
      select isc.unit_cost,
             sup.currency_code
        from item_supp_country isc,
             sups sup
       where isc.primary_supp_ind    = 'Y'
         and isc.primary_country_ind = 'Y'
         and isc.supplier            = sup.supplier
         and isc.item                = LF_container_rec.item;

   cursor C_GET_TO_LOC_RETAIL is
      select unit_retail
        from item_loc
       where item = IF_item
         and loc = I_location
         and loc_type = I_loc_type;

   cursor C_ORD_SUPS_VAT_REGION is
      select nvl(sp.vat_region,0)
        from ordhead oh,
             sups sp
       where oh.order_no = I_ref_no_1
         and oh.supplier = sp.supplier;

   cursor C_RTV_SUPS_VAT_REGION is
      select nvl(sp.vat_region,0),
             rh.supplier
        from rtv_head rh,
             sups sp
       where rh.rtv_order_no = I_ref_no_1
         and rh.supplier     = sp.supplier;

   cursor C_GET_LOC_VAT_REGION is
      select nvl(vat_region,0)
        from store
       where store = IF_location
      UNION ALL
      select nvl(vat_region,0)
        from wh
       where wh = IF_location;

   cursor C_COST_DEPT is
      select 'x'
        from deps
       where dept = IF_dept
         and profit_calc_type = 1;

   cursor C_STORE_TYPE is
      select store_type
        from store
       where store = IF_location
         and IF_loc_type = 'S';

   cursor C_WF_VAT_REGION is
      select NVL(vat_region,0)
        from store
       where store = L_loc
      UNION ALL
      select NVL(vat_region,0)
        from wh
       where wh = L_loc;

   cursor C_ORD_SUPS is
      select supplier,
             wf_order_no
        from ordhead
       where order_no = I_ref_no_1;

   cursor C_GET_TO_LOC_TYPE is
      select 'ST'
        from store
       where store = LP_to_loc
       union
      select 'WH'
        from wh
       where wh = LP_to_loc;
       
   cursor C_GET_IMP_LOC_RETAIL(p_item  ITEM_LOC.ITEM%TYPE) is
      select unit_retail
        from item_loc
       where item = p_item
         and loc = LF_import_id
         and loc_type = LF_import_type;
       
   cursor C_GET_COSTING_LOC is
      select distinct(il.costing_loc),
             decode(il.costing_loc_type,'S','ST','W','WH') costing_loc_type,
             oh.supplier
        from item_loc il,
             ordhead oh,
             ordloc ol
       where ol.item = il.item
         and il.item = IF_item
         and il.loc = L_send_entity
         and oh.wf_order_no = I_ref_no_2
         and ol.location = il.loc;

   cursor C_TSF_HD is
      select tsf_type,
             from_loc,
             from_loc_type,
             to_loc,
             to_loc_type
        from tsfhead
       where tsf_no = I_ref_no_1;

   cursor C_CHK_WH is
      select 'Y'
        from wh
       where wh = L_from_loc
         and wh = physical_wh;

   cursor c_vpk_sku_qty is
      select qty
        from v_packsku_qty
       where pack_no = I_ref_pack_no
         and item = IF_item;

   cursor C_SHIPITM_INV_FLW is
      select from_loc,
             from_loc_type
        from shipitem_inv_flow
       where item = LP_item
         and tsf_no = I_ref_no_1
         and rownum=1;

   cursor C_CLASS_VAT_IND is
      select class_vat_ind
        from class
       where dept = IF_dept 
         and class = IF_class;
   


BEGIN

   LF_inputted_total_retail := IF_total_retail;
   LF_input_old_unit_retail := I_old_unit_retail;
   LF_input_new_unit_retail := I_new_unit_retail;
   ---
   L_tax_info_tbl.EXTEND();

   open  C_STORE_TYPE;
   fetch C_STORE_TYPE into L_store_type;
   close C_STORE_TYPE;

   L_tsf_markdown_loc       := 'S';
   LF_vat_incl_ind          := LP_system_options.stkldgr_vat_incl_retl_ind;
   ---
   if IO_tran_code in (30, 31) then
      L_location := I_tsf_source_location;
      L_loc_type := I_tsf_source_loc_type;
   else
      L_location := IF_location;
      L_loc_type := IF_loc_type;
   end if;
   ---
   if I_ref_no_1 is NOT NULL then
      if TRANSFER_SQL.GET_TSF_ORDER_NO (L_error_message,
                                        L_tsf_order_no,
                                        I_ref_no_1) = FALSE then
         return FALSE;
      end if;
   end if;
   --- Get VAT rate for location
   --- If tran_code is 30 or 31, this returns the sender's VAT rate.
   LF_loc_type := L_loc_type;
   LF_location := L_location;

   -- VAT region of from and to entity should match for tran code 87, 88 posting.

   if IO_tran_code = 20 and LP_system_options.default_tax_type in ('SVAT','GTAX') then
      if L_store_type = 'F' and NVL(I_adj_code,'X') != 'C' then -- RCA for Franchise store is not set in wf_write_financials
         L_Loc := LP_from_loc;   -- LP_from_loc is the company location for the franchise sale set in wf_write_financials.
         open C_WF_VAT_REGION;
         fetch C_WF_VAT_REGION into LF_from_entity_vat_reg;
         close C_WF_VAT_REGION;
      else
         open C_ORD_SUPS_VAT_REGION;
         fetch C_ORD_SUPS_VAT_REGION into  LF_from_entity_vat_reg;
         close C_ORD_SUPS_VAT_REGION;
      end if;

      open C_GET_LOC_VAT_REGION;
      fetch C_GET_LOC_VAT_REGION into LF_to_entity_vat_reg;
      close C_GET_LOC_VAT_REGION;

   elsif IO_tran_code in ('24','65') and LP_system_options.default_tax_type in ('SVAT','GTAX') then
      if L_store_type = 'F' then
         L_Loc := LP_to_loc;     -- LP_to_loc is the company location for the franchise return set in wf_write_financials.
         open C_WF_VAT_REGION;
         fetch C_WF_VAT_REGION into LF_to_entity_vat_reg;
         close C_WF_VAT_REGION;
      else
         open C_RTV_SUPS_VAT_REGION;
         fetch C_RTV_SUPS_VAT_REGION into LF_to_entity_vat_reg, L_rtv_supplier;
         close C_RTV_SUPS_VAT_REGION;
      end if;

      open C_GET_LOC_VAT_REGION;
      fetch C_GET_LOC_VAT_REGION into LF_from_entity_vat_reg;
      close C_GET_LOC_VAT_REGION;
   elsif IO_tran_code = 82 and LP_system_options.default_tax_type in ('SVAT','GTAX') then
      open C_GET_LOC_VAT_REGION;
      fetch C_GET_LOC_VAT_REGION into LF_from_entity_vat_reg;
      close C_GET_LOC_VAT_REGION;

      L_Loc := LP_to_loc;       -- LP_to_loc is the franchise location for the franchise sale set in wf_write_financials.
      open C_WF_VAT_REGION;
      fetch C_WF_VAT_REGION into LF_to_entity_vat_reg;
      close C_WF_VAT_REGION;
   elsif IO_tran_code = 83 and LP_system_options.default_tax_type in ('SVAT','GTAX') then
      open C_GET_LOC_VAT_REGION;
      fetch C_GET_LOC_VAT_REGION into LF_to_entity_vat_reg;
      close C_GET_LOC_VAT_REGION;

      L_Loc := LP_from_loc;   -- LP_from_loc is the franchise location for the franchise return set in wf_write_financials.
      open C_WF_VAT_REGION;
      fetch C_WF_VAT_REGION into LF_from_entity_vat_reg;
      close C_WF_VAT_REGION;
   else
      LF_from_entity_vat_reg := 0;
      LF_to_entity_vat_reg := 0;
  end if;

   if IO_tran_code = 20 and NVL(L_store_type,'X') <> 'F' then
      if ORDER_SQL.GET_DEFAULT_IMP_EXP(O_error_message,
                                       LF_import_id,
                                       LF_import_type,
                                       I_ref_no_1) = FALSE then
         return FALSE;
      end if;
   elsif IO_tran_code in (37,38) and L_tsf_order_no is NOT NULL then
      if ORDER_SQL.GET_DEFAULT_IMP_EXP(O_error_message,
                                       LF_import_id,
                                       LF_import_type,
                                       L_tsf_order_no) = FALSE then
         return FALSE;
      end if;
   end if;

   if LF_import_id is not NULL and LF_import_type != 'F' then
      if LF_import_type in ('M', 'X') then
         LF_import_type := 'W';
      end if;
      ---
      if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                   LF_import_id,
                                   LF_import_type,
                                   NULL,
                                   L_imp_exp_currency)= FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                   IF_location,
                                   IF_loc_type,
                                   NULL,
                                   L_to_loc_currency)= FALSE then
         return FALSE;
      end if;
   end if;

   if IO_tran_code not in (17,18,38,37,82,83,84,85,86) then

      L_send_entity := LF_location;
      L_send_entity_type := L_loc_type;

      -- before 10.1 all retails in the system except the stock ledger
      -- contained vat when vat was on.  in 10.1 this is based on the
      -- class.class_vat_ind.  a conversion is done here to assure that
      -- the old assumptions will still hold true.
      if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                 LF_inputted_total_retail,
                 IF_item, IF_dept, IF_class, LF_location, LF_loc_type, L_vdate,
                 LF_inputted_total_retail, IF_units) = FALSE then
         return FALSE;
      end if;
      if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                 LF_input_old_unit_retail,
                 IF_item, IF_dept, IF_class, LF_location, LF_loc_type, L_vdate,
                 LF_input_old_unit_retail, 1) = FALSE then
         return FALSE;
      end if;
      if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                 LF_input_new_unit_retail,
                 IF_item, IF_dept, IF_class, LF_location, LF_loc_type, L_vdate,
                 LF_input_new_unit_retail, 1) = FALSE then
         return FALSE;
      end if;
   end if;

   --- if tran_code != 30 or 31, this is the final and only calculation of retail.
   if LF_vat_incl_ind = 'Y' then

      LF_total_retail    := LF_inputted_total_retail;
      LF_old_unit_retail := LF_input_old_unit_retail;
      LF_new_unit_retail := LF_input_new_unit_retail;

   else --LF_vat_incl_ind = 'N' --only applies to SVAT, GTAX and SALES are always tax inclusive

      LF_total_retail    := IF_total_retail;
      LF_old_unit_retail := I_old_unit_retail;
      LF_new_unit_retail := I_new_unit_retail;

      -- tran_code 17,18,38 already take VAT into consideration
      if IO_tran_code not in (17,18,38,37,82,83,84,85,86) then
         -- Add LF_total_retail to L_tax_remove_tbl
         L_tax_remove_rec := OBJ_TAX_RETAIL_ADD_REMOVE_REC(
                                IF_item,                 --I_ITEM
                                IF_dept,                 --I_DEPT
                                IF_class,                --I_CLASS
                                LF_location,             --I_LOCATION
                                LF_loc_type,             --I_LOC_TYPE
                                L_vdate,                 --I_EFFECTIVE_FROM_DATE
                                LF_total_retail,         --I_AMOUNT
                                IF_units,                --I_QTY
                                NULL);                   --O_AMOUNT
         L_tax_remove_tbl.EXTEND;
         L_tax_remove_tbl(L_tax_remove_tbl.COUNT)   := L_tax_remove_rec;
         ---
         -- Add LF_old_unit_retail to L_tax_remove_tbl
         -- LF_old_unit_retail always have a quantity of 1
         L_tax_remove_rec := OBJ_TAX_RETAIL_ADD_REMOVE_REC(
                                IF_item,                 --I_ITEM
                                IF_dept,                 --I_DEPT
                                IF_class,                --I_CLASS
                                LF_location,             --I_LOCATION
                                LF_loc_type,             --I_LOC_TYPE
                                L_vdate,                 --I_EFFECTIVE_FROM_DATE
                                LF_old_unit_retail,      --I_AMOUNT
                                1,                       --I_QTY
                                NULL);                   --O_AMOUNT
         L_tax_remove_tbl.EXTEND;
         L_tax_remove_tbl(L_tax_remove_tbl.COUNT)   := L_tax_remove_rec;
         ---
         -- Add LF_new_unit_retail to L_tax_remove_tbl
         -- LF_new_unit_retail always have a quantity of 1
         L_tax_remove_rec := OBJ_TAX_RETAIL_ADD_REMOVE_REC(
                                IF_item,                 --I_ITEM
                                IF_dept,                 --I_DEPT
                                IF_class,                --I_CLASS
                                LF_location,             --I_LOCATION
                                LF_loc_type,             --I_LOC_TYPE
                                L_vdate,                 --I_EFFECTIVE_FROM_DATE
                                LF_new_unit_retail,      --I_AMOUNT
                                1,                       --I_QTY
                                NULL);                   --O_AMOUNT
         L_tax_remove_tbl.EXTEND;
         L_tax_remove_tbl(L_tax_remove_tbl.COUNT)   := L_tax_remove_rec;
         ---
         -- Call the bulk version of TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL for all three amounts
         -- This reduces the number of times that GTAX_ITEM_ROLLUP is accessed
         if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                                             L_tax_remove_tbl)= FALSE then
            return FALSE;
         end if;
         ---
         -- Match up the results from TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL with the local retail variables
         -- If matched then overwrite the local retail variable with the results in L_tax_remove_tbl.O_amount
         if L_tax_remove_tbl is NOT NULL or L_tax_remove_tbl.COUNT > 0 then
            for rec in L_tax_remove_tbl.FIRST..L_tax_remove_tbl.LAST LOOP
               ---
               if NVL(L_tax_remove_tbl(rec).I_amount, -999) = NVL(LF_total_retail, -999) then
                  LF_total_retail := L_tax_remove_tbl(rec).O_amount;
               ---
               elsif NVL(L_tax_remove_tbl(rec).I_amount, -999) = NVL(LF_old_unit_retail, -999) then
                  LF_old_unit_retail := L_tax_remove_tbl(rec).O_amount;
               ---
               elsif NVL(L_tax_remove_tbl(rec).I_amount, -999) = NVL(LF_new_unit_retail, -999) then
                  LF_new_unit_retail := L_tax_remove_tbl(rec).O_amount;
               ---
               end if;
            end LOOP;
         end if;
         ---
         L_tax_remove_tbl.DELETE;
      end if;
   end if;
   ---
   LF_total_cost := IOF_total_cost;
   ---
   if IO_tran_code = 34 then
      open C_CALC;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CALC',
                       'deps',
                       'dept '||to_char(IF_dept));
      fetch C_CALC into L_calc;
      if C_CALC%found then
         open C_GET_OLD_HIER;
         SQL_LIB.SET_MARK('FETCH',
                       'C_GET_OLD_HIER',
                       'deps',
                       'dept '||to_char(IF_dept));
         fetch C_GET_OLD_HIER into L_dept,L_class,L_subclass;

         open C_RECLASS_MARKON_WK;

         SQL_LIB.SET_MARK('FETCH',
                          'C_RECLASS_MARKON_WK',
                          'week_data',
                          NULL);
         fetch C_RECLASS_MARKON_WK into L_cum_markon_pct;

         if C_RECLASS_MARKON_WK%found then
            LF_total_cost := round(LF_total_retail * (1 - (L_cum_markon_pct/100)),4);
         else
            open C_RECLASS_MARKON_MN;
            SQL_LIB.SET_MARK('FETCH',
                             'C_RECLASS_MARKON_MN',
                             'month_data',
                             NULL);
            fetch C_RECLASS_MARKON_MN into L_cum_markon_pct;
            if C_RECLASS_MARKON_MN%found then
               LF_total_cost := round(LF_total_retail * (1 - (L_cum_markon_pct/100)),4);
            end if;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_RECLASS_MARKON_MN',
                             'month_data',
                             NULL);
            close C_RECLASS_MARKON_MN;
         end if;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_OLD_HIER',
                          'reclass_item_temp',
                          NULL);

         close C_GET_OLD_HIER;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_RECLASS_MARKON_WK',
                          'week_data',
                          NULL);

        close C_RECLASS_MARKON_WK;
      end if;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CALC',
                       'deps',
                       'dept '||to_char(IF_dept));
      close C_CALC;

   elsif IO_tran_code in (30, 31) then
      LF_loc_type := IF_loc_type;
      LF_location := IF_location;

      if I_to_loc_unit_retail is null then
         -- this case only occurs when TRAN_DATA_INSERT is called by the Customer Return
         -- or Customer Sale modules
         L_cursor := 'C_GET_TO_LOC_RETAIL';
         SQL_LIB.SET_MARK('OPEN',
                          L_cursor,
                          'item_loc',
                          'item: '||IF_item ||', loc: '||IF_location);
         open C_GET_TO_LOC_RETAIL;
         SQL_LIB.SET_MARK('FETCH',
                          L_cursor,
                          'item_loc',
                          'item: '||IF_item||', loc: '||IF_location);
         fetch C_GET_TO_LOC_RETAIL into LF_recv_unit_retl;
         SQL_LIB.SET_MARK('CLOSE',
                          L_cursor,
                          'item_loc',
                          'item: '||IF_item||', loc: '||IF_location);
         close C_GET_TO_LOC_RETAIL;

      else
         LF_recv_unit_retl := I_to_loc_unit_retail;
      end if;
      ---
      L_recv_unit_retl_for_notax := LF_recv_unit_retl;
      if LP_system_options.default_tax_type in ('SVAT','GTAX') then

         L_rcv_entity      := LF_location;
         L_rcv_entity_type := LF_loc_type;

         -- before 0.1 all retails in the system except the stock ledger
         -- contained vat when vat was on.  in 10.1 this is based on the
         -- class.class_vat_ind.  a conversion is done here to assure that
         -- the old assumptions will still hold true.
         if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                    LF_input_new_unit_retail,
                    IF_item, IF_dept, IF_class, LF_location, LF_loc_type, L_vdate,
                    LF_input_new_unit_retail, 1) = FALSE then
            return FALSE;
         end if;
         if LF_vat_incl_ind = 'Y' then
            if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                    LF_recv_unit_retl,
                    IF_item, IF_dept, IF_class, LF_location, LF_loc_type, L_vdate,
                    LF_recv_unit_retl, IF_units) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;

      --- Since total retail is in sender's currency,
      --- unit retail is that divided by units
      LF_send_unit_retl := LF_inputted_total_retail / IF_units;

      if L_rca_ind = 'Y' then
         LF_recv_unit_retl := 0;
         L_recv_unit_retl_for_notax := 0;
      end if;

      --- convert sender and receiver's unit retails into each other's currency
      if CURRENCY_SQL.CONVERT(O_error_message,
                              LF_recv_unit_retl,
                              L_to_curr_code,
                              L_from_curr_code,
                              LF_recv_unit_retl_send_curr,
                              'R',
                              I_tran_date,
                              NULL) = FALSE then
         return FALSE;
      end if;
      if CURRENCY_SQL.CONVERT(O_error_message,
                              LF_send_unit_retl,
                              L_from_curr_code,
                              L_to_curr_code,
                              LF_send_unit_retl_recv_curr,
                              'R',
                              I_tran_date,
                              NULL) = FALSE then
         return FALSE;
      end if;

      --- Intra-company transfer, determine if sending or
      --- receiving location takes the markup/down
      if STKLEDGR_SQL.GET_TSF_MARKDOWN_LOC(O_error_message,
                                           LF_tsf_markdown_loc,
                                           I_ref_no_1,  -- tsf_no
                                           I_tsf_source_location,
                                           IF_location,
                                           I_tsf_source_loc_type,
                                           IF_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if LF_tsf_markdown_loc = I_tsf_source_location then -- from_loc
         L_tsf_markdown_loc := 'S';
      elsif LF_tsf_markdown_loc = IF_location then -- to_loc
         L_tsf_markdown_loc := 'R';
      end if;

      ---
      --- Final calculation of data to be posted for transfers (tran_type 30/32,
      --  tran_type 31/33(book transfers))
      --- These values depend on whether the sender or receiver takes
      --- any markdowns, and whether the vat inclusive indicator is turned
      --- on in system_options.
      ---
      if L_tsf_markdown_loc = 'S' then
         --- sending location takes any markups/downs
         LF_mkup_location := I_tsf_source_location;
         LF_mkup_loc_type := I_tsf_source_loc_type;
         ---
         if LF_vat_incl_ind = 'Y' then
            LF_total_retail       := LF_recv_unit_retl * IF_units;
            LF_total_retail_32_33 := LF_recv_unit_retl_send_curr * IF_units;
            LF_mkup_recv_retl     := LF_recv_unit_retl_send_curr;
            LF_mkup_send_retl     := LF_send_unit_retl;
         else -- LF_vat_incl_ind = 'N'

            L_temp_retail := L_recv_unit_retl_for_notax;
            if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                          L_temp_retail,
                          IF_item, IF_dept, IF_class, L_rcv_entity, L_rcv_entity_type, L_vdate,
                          L_temp_retail, 1) = FALSE then
               return FALSE;
            end if;
            LF_total_retail       := L_temp_retail * IF_units;

            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_recv_unit_retl_for_notax,
                                    L_to_curr_code,
                                    L_from_curr_code,
                                    LF_recv_unit_retl_send_curr,
                                    'R',
                                    I_tran_date,
                                    NULL) = FALSE then
               return FALSE;
            end if;
            L_temp_retail := LF_recv_unit_retl_send_curr;
            if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                          L_temp_retail,
                          IF_item, IF_dept, IF_class, L_rcv_entity, L_rcv_entity_type, L_vdate,
                          L_temp_retail, 1) = FALSE then
               return FALSE;
            end if;
            LF_total_retail_32_33 := L_temp_retail * IF_units;
            LF_mkup_recv_retl     := L_temp_retail;

            L_temp_retail := IF_total_retail/IF_units;
            if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                          L_temp_retail,
                          IF_item, IF_dept, IF_class, L_send_entity, L_send_entity_type, L_vdate,
                          L_temp_retail, 1) = FALSE then
               return FALSE;
            end if;
            LF_mkup_send_retl     := L_temp_retail;

         end if;
      else  -- L_tsf_markdown_loc = 'R'
         --- receiving location takes any markups/downs
         LF_mkup_location := IF_location;
         LF_mkup_loc_type := IF_loc_type;
         ---
         if LF_vat_incl_ind = 'Y' then

            LF_total_retail       := LF_send_unit_retl_recv_curr * IF_units;
            LF_mkup_send_retl     := LF_send_unit_retl_recv_curr;
            LF_total_retail_32_33 := LF_inputted_total_retail;
            LF_mkup_recv_retl     := LF_recv_unit_retl;

         else -- LF_vat_incl_ind = 'N'

            L_temp_retail := IF_total_retail/IF_units;

            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_temp_retail,
                                    L_from_curr_code,
                                    L_to_curr_code,
                                    L_temp_retail,
                                    'R',
                                    I_tran_date,
                                    NULL) = FALSE then
               return FALSE;
            end if;

            if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                          L_temp_retail,
                          IF_item, IF_dept, IF_class,L_send_entity, L_send_entity_type,L_vdate,
                          L_temp_retail, 1) = FALSE then
               return FALSE;
            end if;
            LF_total_retail := L_temp_retail * IF_units;
            LF_mkup_send_retl := L_temp_retail;

            L_temp_retail := IF_total_retail;
            if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                          L_temp_retail,
                          IF_item, IF_dept, IF_class,L_send_entity, L_send_entity_type, L_vdate,
                          L_temp_retail, IF_units) = FALSE then
               return FALSE;
            end if;
            LF_total_retail_32_33 := L_temp_retail;

            L_temp_retail := L_recv_unit_retl_for_notax;
            if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                          L_temp_retail,
                          IF_item, IF_dept, IF_class,L_rcv_entity, L_rcv_entity_type, L_vdate,
                          L_temp_retail, 1) = FALSE then
               return FALSE;
            end if;
            LF_mkup_recv_retl := L_temp_retail;
         end if;
      end if;

      --- Calculate transfer cost for accounting based on retail
      open C_CALC;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CALC',
                       'deps',
                       'dept '||to_char(IF_dept));
      fetch C_CALC into L_calc;
      if C_CALC%found then
         open C_MARKON_WK;
         SQL_LIB.SET_MARK('FETCH',
                          'C_MARKON_WK',
                          'week_data',
                          NULL);
         fetch C_MARKON_WK into L_cum_markon_pct;
         if C_MARKON_WK%found then
            IOF_total_cost := round(LF_total_retail_32_33 * (1 - (L_cum_markon_pct/100)),4);
         else
            open C_MARKON_MN;
            SQL_LIB.SET_MARK('FETCH',
                             'C_MARKON_MN',
                             'month_data',
                             NULL);
            fetch C_MARKON_MN into L_cum_markon_pct;
            if C_MARKON_MN%found then
               IOF_total_cost := round(LF_total_retail_32_33 * (1 - (L_cum_markon_pct/100)),4);
            end if;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_MARKON_MN',
                             'month_data',
                             NULL);
            close C_MARKON_MN;
         end if;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_MARKON_WK',
                          'week_data',
                          NULL);
         close C_MARKON_WK;
      end if; -- end if C_CALC%FOUND
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CALC',
                       'deps',
                       'dept '||to_char(IF_dept));
      close C_CALC;

      --- calculate final cost to post for transfers (tran_type 30/32)
      if CURRENCY_SQL.CONVERT(L_error_message,
                              IOF_total_cost,
                              L_from_curr_code,
                              L_to_curr_code,
                              LF_total_cost,
                              'C',
                              I_ship_date,
                              NULL) = FALSE then
         O_error_message := L_error_message;
         return FALSE;
      end if;
      LF_total_cost_32_33 := IOF_total_cost;

   -- Get the vat amount for 20/24 for non franchise transaction for posting 87/88.
   -- Franchise vat amount is calculated in wf_write_financials, so skip calculation here.
   elsif IO_tran_code in (20, 24,65) and LP_system_options.default_tax_type in ('SVAT','GTAX') and LP_franchise_transaction is NULL then
      if CALC_TAX_AMT(O_error_message,
                      LF_vat_cost_rtl,
                      LF_vat_code_cost_rtl,
                      IOF_total_cost,
                      IF_item,
                      IF_dept,
                      LF_import_id,
                      LF_import_type,
                      L_send_entity,
                      L_send_entity_type,
                      L_store_type,
                      L_rtv_supplier)= FALSE then
         return FALSE;
      end if;
      if LF_vat_code_cost_rtl is null then
         LP_exempt_ind := 'Y';
      else
         LP_exempt_ind := 'N';
      end if;
   end if;

   --- If using standard method of accounting, check for variance
   --- (only applies to tran_type 20,22,23)
   --  (cost variance for 24,30,31 are taken care of outside of RECORD_INSERT)
   if L_std_av_ind = 'S' then
      LF_cost_diff_loc := 0;
      if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                  IF_item,
                                                  LF_location,
                                                  LF_loc_type,
                                                  LF_std_av_cost,
                                                  LF_std_cost_to_loc,
                                                  LF_unit_retail,
                                                  LF_selling_unit_retail,
                                                  LF_selling_uom) = FALSE then
         return FALSE;
      end if;

      LF_total_std_cost_loc := LF_std_cost_to_loc * IF_units;

      if IO_tran_code = 20 then
         if I_adj_code = 'C' then
            LF_cost_diff_loc := IOF_total_cost;
         else
            LF_cost_diff_loc := IOF_total_cost - LF_total_std_cost_loc;
         end if;
      elsif IO_tran_code in (22, 23) then
         LF_cost_diff_loc := LF_total_cost - LF_total_std_cost_loc;
      end if;
      if LF_cost_diff_loc != 0 then
         --insert variance values into tran_data--
         P_tran_data_size                                        := P_tran_data_size + 1;
         P_tran_data_item(P_tran_data_size)                      := IF_item;
         P_tran_data_dept(P_tran_data_size)                      := IF_dept;
         P_tran_data_class(P_tran_data_size)                     := IF_class;
         P_tran_data_subclass(P_tran_data_size)                  := IF_subclass;
         P_tran_data_pack_ind(P_tran_data_size)                  := NULL;
         P_tran_data_location(P_tran_data_size)                  := IF_location;
         P_tran_data_loc_type(P_tran_data_size)                  := IF_loc_type;
         P_tran_data_tran_date(P_tran_data_size)                 := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');
         P_tran_data_tran_code(P_tran_data_size)                 := 70;
         P_tran_data_adj_code(P_tran_data_size)                  := I_adj_code;
         P_tran_data_units(P_tran_data_size)                     := IF_units;
         P_tran_data_total_cost(P_tran_data_size)                := LF_cost_diff_loc;
         P_tran_data_total_retail(P_tran_data_size)              := NULL;
         P_tran_data_ref_no_1(P_tran_data_size)                  := I_ref_no_1;
         P_tran_data_ref_no_2(P_tran_data_size)                  := I_ref_no_2;
         P_tran_data_gl_ref_no(P_tran_data_size)                 := NULL;
         P_tran_data_old_unit_retail(P_tran_data_size)           := NULL;
         P_tran_data_new_unit_retail(P_tran_data_size)           := NULL;
         P_tran_data_pgm_name(P_tran_data_size)                  := I_pgm_name;
         P_tran_data_sales_type(P_tran_data_size)                := NULL;
         P_tran_data_vat_rate(P_tran_data_size)                  := NULL;
         P_tran_data_av_cost(P_tran_data_size)                   := NULL;
         P_tran_data_timestamp(P_tran_data_size)                 := SYSDATE;
         P_tran_data_ref_pack_no(P_tran_data_size)               := NULL;
         P_tran_data_tot_cost_excl_elc(P_tran_data_size)         := NULL;
         P_tran_data_act_location(P_tran_data_size)              := NULL;
         P_tran_data_act_loc_type(P_tran_data_size)              := NULL;
      end if;
   end if; --L_std_av_ind = 'S'
   ---

   if IO_tran_code IN (20, 22, 23, 24, 25, 30, 31, 32, 33, 37, 38, 62, 82, 83) then
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         LF_item_rec,
                                         IF_item) = FALSE then
         return FALSE;
      end if;
      --Since content and container items can have different cost and Receiver Cost Adjustment
      --will be done at content and container items separately, do NOT post for container item
      --when posting a content item for RCA.
      if LF_item_rec.container_item IS NOT NULL and NVL(I_adj_code,'X') != 'C' then
         --- call get_item_master again to get the container_item details
         if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                            LF_container_rec,
                                            LF_item_rec.container_item) = FALSE then
            return FALSE;
         end if;
         --- Fetch unit_cost and unit_retail values for container item
         L_cursor := 'C_GET_CONTAINER_COST';
         SQL_LIB.SET_MARK('OPEN',
                          L_cursor,
                          'item_supp_country',
                          'item: '||LF_container_rec.item);
         open C_GET_CONTAINER_COST;

         SQL_LIB.SET_MARK('FETCH',
                          L_cursor,
                          'item_supp_country',
                          'item: '||LF_container_rec.item);
         fetch C_GET_CONTAINER_COST into L_supp_unit_cost,
                                         L_supp_curr_code;

         SQL_LIB.SET_MARK('CLOSE',
                          L_cursor,
                          'item_supp_country',
                          'item: '||LF_container_rec.item);
         close C_GET_CONTAINER_COST;

         --- Calculate the container total cost value
         if L_supp_unit_cost IS NOT NULL then

            -- Get location currency
            if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                         IF_location,
                                         IF_loc_type,
                                         NULL,
                                         LF_to_curr_code) = FALSE then
               return FALSE;
            end if;

            -- Convert total cost from primary supplier cost to local currency
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_supp_unit_cost,
                                    L_supp_curr_code,
                                    LF_to_curr_code,
                                    LF_container_unit_cost,
                                    'C',
                                    I_tran_date,
                                    NULL) = FALSE then
               return FALSE;
            end if;

            if IO_tran_code != 25 then
               LF_container_total_cost   := IF_units * LF_container_unit_cost;
            end if;
         end if;

         if IF_loc_type in ('S','W') then
            if PRICING_ATTRIB_SQL.GET_RETAIL(O_error_message,
                                             LF_container_unit_retail,
                                             LF_selling_unit_retail_loc,
                                             LF_selling_uom_loc,
                                             LF_multi_units_loc,
                                             LF_multi_unit_retail_loc,
                                             LF_multi_selling_uom_loc,
                                             LF_item_rec.container_item,
                                             IF_loc_type,
                                             IF_location) = FALSE then
               return FALSE;
            end if;
         elsif IF_loc_type = 'E' then
            if PRICING_ATTRIB_SQL.GET_EXTERNAL_FINISHER_RETAIL(O_error_message,
                                                               LF_container_unit_retail,
                                                               LF_item_rec.container_item,
                                                               IF_location)= FALSE then
               return FALSE;
            end if;
         end if;
         --- Calculate the container total retail value
         if LF_container_unit_retail IS NOT NULL and IO_tran_code != 25 then
            LF_container_total_retail := IF_units * nvl(LF_container_unit_retail,'0');
         end if;
      end if;
   end if;

   --Calculate the container vat value for non franchise transaction for posting 87/88.
   --Franchise vat amount for the container item is processed in wf_write_financials.
   if IO_tran_code in (20, 24,65) and LP_system_options.default_tax_type in ('SVAT','GTAX') then
      if LF_item_rec.container_item IS NOT NULL and NVL(I_adj_code,'X') != 'C' and LP_franchise_transaction is NULL then
         if CALC_TAX_AMT(O_error_message,
                         LF_container_vat_cost_rtl,
                         LF_container_vat_code_cost_rtl,
                         LF_container_total_cost,
                         LF_item_rec.container_item,
                         LF_item_rec.dept,
                         LF_import_id,
                         LF_import_type,
                         L_send_entity,
                         L_send_entity_type,
                         L_store_type,
                         L_rtv_supplier)= FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   if (L_rca_ind = 'Y' and IO_tran_code in (37, 38)) or
      (IO_tran_code = 20 and I_adj_code = 'C' and LF_import_id is not NULL and LF_import_type != 'F') then
      -- For cost adjustments of LE transfers, total_retail is NULL.
      LF_total_retail := 0;
      LF_container_total_retail := 0;
   end if;

   --- MAIN INSERT ---
   ---
   if IO_tran_code = 20 then
     LF_total_cost := IOF_total_cost;
   end if;
-- Insert the logic for ICT transactions similar as of 37 posting.
   if IO_tran_code = 44 then
      L_default_tax_type           := LP_system_options.default_tax_type;
      L_stkldgr_vat_incl_retl_ind  := LP_system_options.stkldgr_vat_incl_retl_ind;
      L_class_level_vat_ind        := LP_system_options.class_level_vat_ind;

      if L_default_tax_type = 'SVAT' then
         open C_TSF_HD;
         fetch C_TSF_HD into L_tsf_type,
                             L_from_loc,
                             L_from_loc_type,
                             L_to_loc,
                             L_to_loc_type;
        close C_TSF_HD;
        if L_from_loc is not null then
           if L_from_loc_type = 'W' then
             -- chk the physical wh and get virtual wh.
              open C_CHK_WH;
              fetch C_CHK_WH into L_phy_wh_ind;
              close C_CHK_WH;
              if L_phy_wh_ind = 'Y' then
                 open c_vpk_sku_qty;
                 fetch c_vpk_sku_qty into L_rcv_qty;
                 close c_vpk_sku_qty;
                 if L_rcv_qty is not null then
                    LP_item := I_ref_pack_no;
                    L_rcv_qty := IF_units/L_rcv_qty;
                 else
                    LP_item := IF_item;
                    L_rcv_qty := IF_units;
                 end if;
                    open C_SHIPITM_INV_FLW;
                    fetch C_SHIPITM_INV_FLW into L_final_from_loc,
                                                 L_final_from_loc_type;
                    close C_SHIPITM_INV_FLW;
              else -- if from loc is wh but not the physical wh
                 L_final_from_loc := L_from_loc;
                 L_final_from_loc_type := L_from_loc_type;
              end if;
           else -- Other than wh as from loc.
               L_final_from_loc := L_from_loc;
               L_final_from_loc_type := L_from_loc_type;
           end if;
           -- Check for Intercompany
           if TRANSFER_SQL.IS_INTERCOMPANY(O_error_message,
                                           L_intercompany,
                                           'T',
                                           L_tsf_type,
                                           L_final_from_loc,
                                           L_final_from_loc_type,
                                           IF_location,
                                           IF_loc_type) = FALSE then
              return FALSE;
           end if;
           if L_intercompany then
              open C_CLASS_VAT_IND;
              fetch C_CLASS_VAT_IND into L_class_vat_ind;
              close C_CLASS_VAT_IND;
              if TAX_SQL.GET_TAX_RATE_INFO (O_error_message,
                                            L_tax_rate,
                                            L_tax_code,
                                            L_exempt_ind,
                                            IF_item,
                                            IF_loc_type,
                                            IF_location,
                                            L_final_from_loc_type,
                                            L_final_from_loc,
                                            'R') = FALSE then
                 return FALSE;
              end if;
              LF_total_retail := IF_total_retail;
              -- Adding the vat or stripping the vat based on stkldgr_vat_incl_retl_ind,class_level_vat_ind and class_vat_ind
              -- to make the retail of 44 similar to 37.
              if L_stkldgr_vat_incl_retl_ind = 'Y' and L_class_level_vat_ind = 'Y' and L_class_vat_ind = 'N' then 
                 LF_total_retail := IF_total_retail*(1+(nvl(L_tax_rate,0)/100));
              elsif (L_stkldgr_vat_incl_retl_ind = 'N' and L_class_level_vat_ind = 'Y' and L_class_vat_ind = 'Y')
                    or (L_stkldgr_vat_incl_retl_ind = 'N' and L_class_level_vat_ind = 'N')then
                    LF_total_retail := IF_total_retail/(1+(nvl(L_tax_rate,0)/100));
              end if;
           end if; -- L_from_loc_entity <> L_to_loc_entity
        end if; -- L_from_loc
      end if; -- L_default_tax_type
   end if; -- IO_tran_code = 44
 
   -- Skip and return for deposit container for non franchise tranactions. The content item
   -- process will create records for container item also.
   -- For franchise transaction, deposit container item is handled in process_wf_write_financials
   -- and hence it will not be skipped here.
   if NVL(LF_item_rec.deposit_item_type,'x') = 'A' and LP_franchise_transaction is NULL then
      return TRUE;
   end if;

   P_tran_data_size                                           := P_tran_data_size + 1;
   P_tran_data_item(P_tran_data_size)                         := IF_item;
   P_tran_data_dept(P_tran_data_size)                         := IF_dept;
   P_tran_data_class(P_tran_data_size)                        := IF_class;
   P_tran_data_subclass(P_tran_data_size)                     := IF_subclass;
   P_tran_data_pack_ind(P_tran_data_size)                     := NULL;
   P_tran_data_location(P_tran_data_size)                     := IF_location;
   P_tran_data_loc_type(P_tran_data_size)                     := IF_loc_type;
   P_tran_data_tran_date(P_tran_data_size)                    := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');
   P_tran_data_tran_code(P_tran_data_size)                    := IO_tran_code;
   P_tran_data_adj_code(P_tran_data_size)                     := I_adj_code;
   P_tran_data_units(P_tran_data_size)                        := IF_units;
   P_tran_data_total_cost(P_tran_data_size)                   := LF_total_cost;
   P_tran_data_total_retail(P_tran_data_size)                 := LF_total_retail;
   P_tran_data_ref_no_1(P_tran_data_size)                     := I_ref_no_1;
   P_tran_data_ref_no_2(P_tran_data_size)                     := I_ref_no_2;
   P_tran_data_old_unit_retail(P_tran_data_size)              := LF_old_unit_retail;
   P_tran_data_new_unit_retail(P_tran_data_size)              := LF_new_unit_retail;
   P_tran_data_pgm_name(P_tran_data_size)                     := I_pgm_name;
   P_tran_data_sales_type(P_tran_data_size)                   := NULL;
   P_tran_data_vat_rate(P_tran_data_size)                     := NULL;
   P_tran_data_av_cost(P_tran_data_size)                      := NULL;
   P_tran_data_timestamp(P_tran_data_size)                    := SYSDATE;
   P_tran_data_ref_pack_no(P_tran_data_size)                  := I_ref_pack_no;
   P_tran_data_act_location(P_tran_data_size)                 := NULL;
   P_tran_data_act_loc_type(P_tran_data_size)                 := NULL;
   ---

   if IO_tran_code in (37, 38, 22, 23, 63, 64, 87, 88) then
      P_tran_data_gl_ref_no(P_tran_data_size)                 := I_gl_ref_no;
   else
      P_tran_data_gl_ref_no(P_tran_data_size)                 := NULL;
   end if;

   if IO_tran_code in (20,73) then
      P_tran_data_tot_cost_excl_elc(P_tran_data_size) := I_total_cost_excl_elc;
   else
      P_tran_data_tot_cost_excl_elc(P_tran_data_size) := NULL;
   end if;

   if IO_tran_code in (82, 83, 84, 85) then
      P_tran_data_vat_rate(P_tran_data_size)                     := I_vat_rate ;
   else
      P_tran_data_vat_rate(P_tran_data_size)                     := NULL;
   end if;

   if IO_tran_code = 20 and LF_import_id is NOT NULL and LF_import_type != 'F' and LF_import_id != IF_location then
      -- Need to check if processing for Franchise Locations
      -- If location is a Franchise Location, need to post Tran Code 20 record for this.
      if IF_loc_type = 'S' then
         select store_type
           into L_store_type
           from store
          where store = IF_location;
      end if;

      if IF_loc_type = 'W' or L_store_type = 'C' then
         P_tran_data_location(P_tran_data_size)                  := LF_import_id;
         P_tran_data_loc_type(P_tran_data_size)                  := LF_import_type;
         
          open C_GET_IMP_LOC_RETAIL(IF_item);
         fetch C_GET_IMP_LOC_RETAIL into L_import_loc_retail;
         close C_GET_IMP_LOC_RETAIL;
        
         L_import_temp_retail     := L_import_loc_retail;

         if LF_vat_incl_ind = 'Y' then
            if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                                L_import_temp_retail,
                                                IF_item, IF_dept, IF_class, LF_import_id, LF_import_type, L_vdate,
                                                L_import_temp_retail, IF_units) = FALSE then
               return FALSE;
            end if;
         else
            if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                                                L_import_temp_retail,
                                                IF_item, IF_dept, IF_class, LF_import_id, LF_import_type, L_vdate,
                                                L_import_temp_retail, IF_units) = FALSE then
               return FALSE;
            end if;                           
         end if;      
         P_tran_data_total_retail(P_tran_data_size)  := L_import_temp_retail * IF_units;           
      end if;

      P_tran_data_act_location(P_tran_data_size)                 := IF_location;
      P_tran_data_act_loc_type(P_tran_data_size)                 := IF_loc_type;
      ---
      if CURRENCY_SQL.CONVERT(O_error_message,
                              P_tran_data_total_cost(P_tran_data_size),
                              L_to_loc_currency,
                              L_imp_exp_currency,
                              P_tran_data_total_cost(P_tran_data_size),
                              'C',
                              I_tran_date,
                              NULL) = FALSE then
         return FALSE;
      end if;             
   end if; 
   ---
   if LF_item_rec.container_item IS NOT NULL and NVL(I_adj_code,'X') != 'C' and LP_franchise_transaction is NULL then
      --- Insert tran_data record for the container item for non franchise transactions
      P_tran_data_size                                        := P_tran_data_size + 1;
      P_tran_data_item(P_tran_data_size)                      := LF_container_rec.item; /*container item*/
      P_tran_data_dept(P_tran_data_size)                      := LF_container_rec.dept; /*container dept*/
      P_tran_data_class(P_tran_data_size)                     := LF_container_rec.class; /*container class*/
      P_tran_data_subclass(P_tran_data_size)                  := LF_container_rec.subclass; /*container subclass*/
      P_tran_data_pack_ind(P_tran_data_size)                  := NULL;
      P_tran_data_location(P_tran_data_size)                  := IF_location;
      P_tran_data_loc_type(P_tran_data_size)                  := IF_loc_type;
      P_tran_data_tran_date(P_tran_data_size)                 := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');
      P_tran_data_tran_code(P_tran_data_size)                 := IO_tran_code;
      P_tran_data_adj_code(P_tran_data_size)                  := I_adj_code;
      P_tran_data_units(P_tran_data_size)                     := IF_units;
      P_tran_data_total_cost(P_tran_data_size)                := LF_container_total_cost; /*total cost*/
      P_tran_data_total_retail(P_tran_data_size)              := LF_container_total_retail; /*total retail*/
      P_tran_data_ref_no_1(P_tran_data_size)                  := I_ref_no_1;
      P_tran_data_ref_no_2(P_tran_data_size)                  := I_ref_no_2;
      P_tran_data_old_unit_retail(P_tran_data_size)           := LF_old_unit_retail;
      P_tran_data_new_unit_retail(P_tran_data_size)           := LF_new_unit_retail;
      P_tran_data_pgm_name(P_tran_data_size)                  := I_pgm_name;
      P_tran_data_sales_type(P_tran_data_size)                := NULL;
      P_tran_data_vat_rate(P_tran_data_size)                  := NULL;
      P_tran_data_av_cost(P_tran_data_size)                   := NULL;
      P_tran_data_timestamp(P_tran_data_size)                 := SYSDATE;
      P_tran_data_ref_pack_no(P_tran_data_size)               := I_ref_pack_no;
      P_tran_data_tot_cost_excl_elc(P_tran_data_size)         := NULL;
      P_tran_data_act_location(P_tran_data_size)              := NULL;
      P_tran_data_act_loc_type(P_tran_data_size)              := NULL;

      if IO_tran_code in (37, 38, 22, 23, 63, 64, 87, 88) then
         P_tran_data_gl_ref_no(P_tran_data_size)                 := I_gl_ref_no;
      else
         P_tran_data_gl_ref_no(P_tran_data_size)                 := NULL;
      end if;

      if IO_tran_code in (82, 83) then
         P_tran_data_vat_rate(P_tran_data_size)                     := I_vat_rate;
      else
         P_tran_data_vat_rate(P_tran_data_size)                     := NULL;
      end if;

      if IO_tran_code = 20 and LF_import_id is NOT NULL and LF_import_type != 'F' and LF_import_id != IF_location then
         if IF_loc_type = 'W' or L_store_type = 'C' then
            P_tran_data_location(P_tran_data_size)                     := LF_import_id;
            P_tran_data_loc_type(P_tran_data_size)                     := LF_import_type;
            
             open C_GET_IMP_LOC_RETAIL(LF_container_rec.item);
            fetch C_GET_IMP_LOC_RETAIL into L_import_loc_retail;
            close C_GET_IMP_LOC_RETAIL;
          
            L_import_temp_retail     := L_import_loc_retail;
          
            if LF_vat_incl_ind = 'Y' then
               if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                                   L_import_temp_retail,
                                                   LF_container_rec.item, LF_container_rec.dept, LF_container_rec.class, 
                                                   LF_import_id, LF_import_type, L_vdate,
                                                   L_import_temp_retail, IF_units) = FALSE then
                  return FALSE;
               end if;
            else
              if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                                                  L_import_temp_retail,
                                                  LF_container_rec.item, LF_container_rec.dept, LF_container_rec.class, 
                                                  LF_import_id, LF_import_type, L_vdate,
                                                  L_import_temp_retail, IF_units) = FALSE then
                 return FALSE;
              end if;                           
            end if;
            P_tran_data_total_retail(P_tran_data_size)  := L_import_temp_retail * IF_units;            
         end if;

         P_tran_data_act_location(P_tran_data_size)                 := IF_location;
         P_tran_data_act_loc_type(P_tran_data_size)                 := IF_loc_type;
         ---
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 P_tran_data_total_cost(P_tran_data_size),
                                 L_to_loc_currency,
                                 L_imp_exp_currency,
                                 P_tran_data_total_cost(P_tran_data_size),
                                 'C',
                                 I_tran_date,
                                 NULL) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---

   SQL_LIB.SET_MARK('OPEN',
                    'C_COST_DEPT',
                    'deps',
                    'dept '||to_char(IF_dept));
   open C_COST_DEPT;
   SQL_LIB.SET_MARK('FETCH',
                    'C_COST_DEPT',
                    'deps',
                    'dept '||to_char(IF_dept));
   fetch C_COST_DEPT into L_cost_dept_exists;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_COST_DEPT',
                    'deps',
                    'dept '||to_char(IF_dept));
   close C_COST_DEPT;

   if IO_tran_code = 30 then
      --For transfers, also post a tran_type 32 (transfer out from sender)
      P_tran_data_size                                        := P_tran_data_size + 1;
      P_tran_data_item(P_tran_data_size)                      := IF_item;
      P_tran_data_dept(P_tran_data_size)                      := IF_dept;
      P_tran_data_class(P_tran_data_size)                     := IF_class;
      P_tran_data_subclass(P_tran_data_size)                  := IF_subclass;
      P_tran_data_pack_ind(P_tran_data_size)                  := NULL;
      P_tran_data_location(P_tran_data_size)                  := I_tsf_source_location;
      P_tran_data_loc_type(P_tran_data_size)                  := I_tsf_source_loc_type;
      P_tran_data_tran_date(P_tran_data_size)                 := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');
      P_tran_data_tran_code(P_tran_data_size)                 := 32;
      P_tran_data_adj_code(P_tran_data_size)                  := I_adj_code;
      P_tran_data_units(P_tran_data_size)                     := IF_units;
      P_tran_data_total_cost(P_tran_data_size)                := LF_total_cost_32_33;
      P_tran_data_total_retail(P_tran_data_size)              := LF_total_retail_32_33;
      P_tran_data_ref_no_1(P_tran_data_size)                  := I_ref_no_1;
      P_tran_data_ref_no_2(P_tran_data_size)                  := I_ref_no_2;
      P_tran_data_gl_ref_no(P_tran_data_size)                 := NULL;
      P_tran_data_old_unit_retail(P_tran_data_size)           := NULL;
      P_tran_data_new_unit_retail(P_tran_data_size)           := NULL;
      P_tran_data_pgm_name(P_tran_data_size)                  := I_pgm_name;
      P_tran_data_sales_type(P_tran_data_size)                := NULL;
      P_tran_data_vat_rate(P_tran_data_size)                  := NULL;
      P_tran_data_av_cost(P_tran_data_size)                   := NULL;
      P_tran_data_timestamp(P_tran_data_size)                 := SYSDATE;
      P_tran_data_ref_pack_no(P_tran_data_size)               := I_ref_pack_no;
      P_tran_data_tot_cost_excl_elc(P_tran_data_size)         := NULL;
      P_tran_data_act_location(P_tran_data_size)              := NULL;
      P_tran_data_act_loc_type(P_tran_data_size)              := NULL;

      if L_tsf_order_no is not NULL then
         P_tran_data_gl_ref_no(P_tran_data_size)              := IF_location;
      end if;

      if LF_item_rec.container_item IS NOT NULL and NVL(I_adj_code,'X') != 'C' then
         --- post a tran_type 32 (transfer out from sender) for the container item
         P_tran_data_size                                     := P_tran_data_size + 1;
         P_tran_data_item(P_tran_data_size)                   := LF_container_rec.item; /*container item*/
         P_tran_data_dept(P_tran_data_size)                   := LF_container_rec.dept; /*container dept*/
         P_tran_data_class(P_tran_data_size)                  := LF_container_rec.class; /*container class*/
         P_tran_data_subclass(P_tran_data_size)               := LF_container_rec.subclass;/*container subclass*/
         P_tran_data_pack_ind(P_tran_data_size)               := NULL;
         P_tran_data_location(P_tran_data_size)               := I_tsf_source_location;
         P_tran_data_loc_type(P_tran_data_size)               := I_tsf_source_loc_type;
         P_tran_data_tran_date(P_tran_data_size)              := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');
         P_tran_data_tran_code(P_tran_data_size)              := 32;
         P_tran_data_adj_code(P_tran_data_size)               := I_adj_code;
         P_tran_data_units(P_tran_data_size)                  := IF_units;
         P_tran_data_total_cost(P_tran_data_size)             := LF_container_total_cost; /*total cost*/
         P_tran_data_total_retail(P_tran_data_size)           := LF_container_total_retail; /*total retail*/
         P_tran_data_ref_no_1(P_tran_data_size)               := I_ref_no_1;
         P_tran_data_ref_no_2(P_tran_data_size)               := I_ref_no_2;
         P_tran_data_gl_ref_no(P_tran_data_size)              := NULL;
         P_tran_data_old_unit_retail(P_tran_data_size)        := NULL;
         P_tran_data_new_unit_retail(P_tran_data_size)        := NULL;
         P_tran_data_pgm_name(P_tran_data_size)               := I_pgm_name;
         P_tran_data_sales_type(P_tran_data_size)             := NULL;
         P_tran_data_vat_rate(P_tran_data_size)               := NULL;
         P_tran_data_av_cost(P_tran_data_size)                := NULL;
         P_tran_data_timestamp(P_tran_data_size)              := SYSDATE;
         P_tran_data_ref_pack_no(P_tran_data_size)            := I_ref_pack_no;
         P_tran_data_tot_cost_excl_elc(P_tran_data_size)      := NULL;
         P_tran_data_act_location(P_tran_data_size)           := NULL;
         P_tran_data_act_loc_type(P_tran_data_size)           := NULL;

         if L_tsf_order_no is not NULL then
            P_tran_data_gl_ref_no(P_tran_data_size)              := IF_location;
         end if;

      end if;
      ---

      if L_cost_dept_exists = 'x' then
         L_l10n_fin_rec                     := L10N_FIN_REC();
         L_l10n_fin_rec.procedure_key       := 'RECOVERABLE_TAX_POSTING';
         L_l10n_fin_rec.country_id          := NULL;
         L_l10n_fin_rec.source_entity       := 'LOC';
         L_l10n_fin_rec.source_ID           := I_tsf_source_location;
         L_l10n_fin_rec.source_type         := I_tsf_source_loc_type;
         L_l10n_fin_rec.dest_entity         := 'LOC';
         L_l10n_fin_rec.dest_ID             := IF_location;
         L_l10n_fin_rec.dest_type           := IF_loc_type;
         L_l10n_fin_rec.av_cost             := I_from_wac;
         L_l10n_fin_rec.unit_Cost           := I_extended_base_cost;
         L_l10n_fin_rec.base_Cost           := I_base_cost;
         L_l10n_fin_rec.total_retail        := LF_total_retail_32_33;

         L_tran_data_rec := TRAN_DATA_REC(IF_item,
                                          IF_dept,
                                          IF_class,
                                          IF_subclass,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_tran_date,
                                          IO_tran_code,
                                          I_adj_code,
                                          IF_units,
                                          NULL,
                                          LF_total_retail,
                                          I_ref_no_1,
                                          I_ref_no_2,
                                          I_gl_ref_no,
                                          LF_old_unit_retail,
                                          LF_new_unit_retail,
                                          I_pgm_name,
                                          NULL,
                                          NULL,
                                          NULL,
                                          SYSDATE,
                                          I_ref_pack_no,
                                          NULL);
         L_l10n_fin_rec.tran_data   := L_tran_data_rec;
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec)= FALSE then
            return FALSE;
         end if;
      end if;

      if LF_mkup_recv_retl != LF_mkup_send_retl then
         --post markdown/markup when prices change during transfer
         if LF_mkup_send_retl > LF_mkup_recv_retl then
            IO_tran_code := 13;
         else
            IO_tran_code := 11;
         end if;

         P_tran_data_size                                     := P_tran_data_size + 1;
         P_tran_data_item(P_tran_data_size)                   := IF_item;
         P_tran_data_dept(P_tran_data_size)                   := IF_dept;
         P_tran_data_class(P_tran_data_size)                  := IF_class;
         P_tran_data_subclass(P_tran_data_size)               := IF_subclass;
         P_tran_data_pack_ind(P_tran_data_size)               := NULL;
         P_tran_data_location(P_tran_data_size)               := LF_mkup_location;
         P_tran_data_loc_type(P_tran_data_size)               := LF_mkup_loc_type;
         P_tran_data_tran_date(P_tran_data_size)              := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');
         P_tran_data_tran_code(P_tran_data_size)              := IO_tran_code;
         P_tran_data_adj_code(P_tran_data_size)               := I_adj_code;
         P_tran_data_units(P_tran_data_size)                  := IF_units;
         P_tran_data_total_cost(P_tran_data_size)             := NULL;
         P_tran_data_total_retail(P_tran_data_size)           := round(IF_units * abs((LF_mkup_send_retl - LF_mkup_recv_retl)),4);
         P_tran_data_ref_no_1(P_tran_data_size)               := I_ref_no_1;
         P_tran_data_ref_no_2(P_tran_data_size)               := I_ref_no_2;
         P_tran_data_gl_ref_no(P_tran_data_size)              := NULL;
         P_tran_data_old_unit_retail(P_tran_data_size)        := LF_mkup_send_retl;
         P_tran_data_new_unit_retail(P_tran_data_size)        := LF_mkup_recv_retl;
         P_tran_data_pgm_name(P_tran_data_size)               := I_pgm_name;
         P_tran_data_sales_type(P_tran_data_size)             := NULL;
         P_tran_data_vat_rate(P_tran_data_size)               := NULL;
         P_tran_data_av_cost(P_tran_data_size)                := NULL;
         P_tran_data_timestamp(P_tran_data_size)              := SYSDATE;
         P_tran_data_ref_pack_no(P_tran_data_size)            := NULL;
         P_tran_data_tot_cost_excl_elc(P_tran_data_size)      := NULL;
         P_tran_data_act_location(P_tran_data_size)           := NULL;
         P_tran_data_act_loc_type(P_tran_data_size)           := NULL;

      end if;

   elsif IO_tran_code = 31 then
      --For book transfers, also post a tran_type 33 (transfer out from sender)
      P_tran_data_size                                        := P_tran_data_size + 1;
      P_tran_data_item(P_tran_data_size)                      := IF_item;
      P_tran_data_dept(P_tran_data_size)                      := IF_dept;
      P_tran_data_class(P_tran_data_size)                     := IF_class;
      P_tran_data_subclass(P_tran_data_size)                  := IF_subclass;
      P_tran_data_pack_ind(P_tran_data_size)                  := NULL;
      P_tran_data_location(P_tran_data_size)                  := I_tsf_source_location;
      P_tran_data_loc_type(P_tran_data_size)                  := I_tsf_source_loc_type;
      P_tran_data_tran_date(P_tran_data_size)                 := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');
      P_tran_data_tran_code(P_tran_data_size)                 := 33;
      P_tran_data_adj_code(P_tran_data_size)                  := I_adj_code;
      P_tran_data_units(P_tran_data_size)                     := IF_units;
      P_tran_data_total_cost(P_tran_data_size)                := LF_total_cost_32_33;
      P_tran_data_total_retail(P_tran_data_size)              := LF_total_retail_32_33;
      P_tran_data_ref_no_1(P_tran_data_size)                  := I_ref_no_1;
      P_tran_data_ref_no_2(P_tran_data_size)                  := I_ref_no_2;
      P_tran_data_gl_ref_no(P_tran_data_size)                 := NULL;
      P_tran_data_old_unit_retail(P_tran_data_size)           := NULL;
      P_tran_data_new_unit_retail(P_tran_data_size)           := NULL;
      P_tran_data_pgm_name(P_tran_data_size)                  := I_pgm_name;
      P_tran_data_sales_type(P_tran_data_size)                := NULL;
      P_tran_data_vat_rate(P_tran_data_size)                  := NULL;
      P_tran_data_av_cost(P_tran_data_size)                   := NULL;
      P_tran_data_timestamp(P_tran_data_size)                 := SYSDATE;
      P_tran_data_ref_pack_no(P_tran_data_size)               := I_ref_pack_no;
      P_tran_data_tot_cost_excl_elc(P_tran_data_size)         := NULL;
      P_tran_data_act_location(P_tran_data_size)              := NULL;
      P_tran_data_act_loc_type(P_tran_data_size)              := NULL;

      if L_tsf_order_no is not NULL then
         P_tran_data_gl_ref_no(P_tran_data_size)              := IF_location;
      end if;

      if LF_item_rec.container_item IS NOT NULL and NVL(I_adj_code,'X') != 'C' then
         --- also post a tran_type 33 (transfer out from sender) for the container item
         P_tran_data_size                                     := P_tran_data_size + 1;
         P_tran_data_item(P_tran_data_size)                   := LF_container_rec.item; /*container item*/
         P_tran_data_dept(P_tran_data_size)                   := LF_container_rec.dept; /*container dept*/
         P_tran_data_class(P_tran_data_size)                  := LF_container_rec.class; /*container class*/
         P_tran_data_subclass(P_tran_data_size)               := LF_container_rec.subclass;/*container subclass*/
         P_tran_data_pack_ind(P_tran_data_size)               := NULL;
         P_tran_data_location(P_tran_data_size)               := I_tsf_source_location;
         P_tran_data_loc_type(P_tran_data_size)               := I_tsf_source_loc_type;
         P_tran_data_tran_date(P_tran_data_size)              := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');
         P_tran_data_tran_code(P_tran_data_size)              := 33;
         P_tran_data_adj_code(P_tran_data_size)               := I_adj_code;
         P_tran_data_units(P_tran_data_size)                  := IF_units;
         P_tran_data_total_cost(P_tran_data_size)             := LF_container_total_cost; /*total cost*/
         P_tran_data_total_retail(P_tran_data_size)           := LF_container_total_retail; /*total retail*/
         P_tran_data_ref_no_1(P_tran_data_size)               := I_ref_no_1;
         P_tran_data_ref_no_2(P_tran_data_size)               := I_ref_no_2;
         P_tran_data_gl_ref_no(P_tran_data_size)              := NULL;
         P_tran_data_old_unit_retail(P_tran_data_size)        := NULL;
         P_tran_data_new_unit_retail(P_tran_data_size)        := NULL;
         P_tran_data_pgm_name(P_tran_data_size)               := I_pgm_name;
         P_tran_data_sales_type(P_tran_data_size)             := NULL;
         P_tran_data_vat_rate(P_tran_data_size)               := NULL;
         P_tran_data_av_cost(P_tran_data_size)                := NULL;
         P_tran_data_timestamp(P_tran_data_size)              := SYSDATE;
         P_tran_data_ref_pack_no(P_tran_data_size)            := NULL;
         P_tran_data_tot_cost_excl_elc(P_tran_data_size)      := NULL;
         P_tran_data_act_location(P_tran_data_size)           := NULL;
         P_tran_data_act_loc_type(P_tran_data_size)           := NULL;

         if L_tsf_order_no is not NULL then
            P_tran_data_gl_ref_no(P_tran_data_size)              := IF_location;
         end if;

      end if;
      ---
      if LF_mkup_recv_retl != LF_mkup_send_retl then
         --post markdown/markup when prices change during transfer
         if LF_mkup_send_retl > LF_mkup_recv_retl then
            IO_tran_code := 13;
         else
            IO_tran_code := 11;
         end if;

         P_tran_data_size                                      := P_tran_data_size + 1;
         P_tran_data_item(P_tran_data_size)                    := IF_item;
         P_tran_data_dept(P_tran_data_size)                    := IF_dept;
         P_tran_data_class(P_tran_data_size)                   := IF_class;
         P_tran_data_subclass(P_tran_data_size)                := IF_subclass;
         P_tran_data_pack_ind(P_tran_data_size)                := NULL;
         P_tran_data_location(P_tran_data_size)                := LF_mkup_location;
         P_tran_data_loc_type(P_tran_data_size)                := LF_mkup_loc_type;
         P_tran_data_tran_date(P_tran_data_size)               := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');
         P_tran_data_tran_code(P_tran_data_size)               := IO_tran_code;
         P_tran_data_adj_code(P_tran_data_size)                := I_adj_code;
         P_tran_data_units(P_tran_data_size)                   := IF_units;
         P_tran_data_total_cost(P_tran_data_size)              := NULL;
         P_tran_data_total_retail(P_tran_data_size)            := abs(IF_units * (LF_mkup_send_retl - LF_mkup_recv_retl));
         P_tran_data_ref_no_1(P_tran_data_size)                := I_ref_no_1;
         P_tran_data_ref_no_2(P_tran_data_size)                := I_ref_no_2;
         P_tran_data_gl_ref_no(P_tran_data_size)               := NULL;
         P_tran_data_old_unit_retail(P_tran_data_size)         := LF_mkup_send_retl;
         P_tran_data_new_unit_retail(P_tran_data_size)         := LF_mkup_recv_retl;
         P_tran_data_pgm_name(P_tran_data_size)                := I_pgm_name;
         P_tran_data_sales_type(P_tran_data_size)              := NULL;
         P_tran_data_vat_rate(P_tran_data_size)                := NULL;
         P_tran_data_av_cost(P_tran_data_size)                 := NULL;
         P_tran_data_timestamp(P_tran_data_size)               := SYSDATE;
         P_tran_data_ref_pack_no(P_tran_data_size)             := NULL;
         P_tran_data_tot_cost_excl_elc(P_tran_data_size)       := NULL;
         P_tran_data_act_location(P_tran_data_size)            := NULL;
         P_tran_data_act_loc_type(P_tran_data_size)            := NULL;

      end if;

   elsif IO_tran_code = 34 then
      --if tran_code is 34 a 36 must also also be written for reclassification
      P_tran_data_size                                         := P_tran_data_size + 1;
      P_tran_data_item(P_tran_data_size)                       := IF_item;
      P_tran_data_dept(P_tran_data_size)                       := I_source_dept;
      P_tran_data_class(P_tran_data_size)                      := I_source_class;
      P_tran_data_subclass(P_tran_data_size)                   := I_source_subclass;
      P_tran_data_pack_ind(P_tran_data_size)                   := NULL;
      P_tran_data_location(P_tran_data_size)                   := IF_location;
      P_tran_data_loc_type(P_tran_data_size)                   := IF_loc_type;
      P_tran_data_tran_date(P_tran_data_size)                  := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');
      P_tran_data_tran_code(P_tran_data_size)                  := 36;
      P_tran_data_adj_code(P_tran_data_size)                   := NULL;
      P_tran_data_units(P_tran_data_size)                      := IF_units;
      P_tran_data_total_cost(P_tran_data_size)                 := LF_total_cost;
      P_tran_data_total_retail(P_tran_data_size)               := LF_total_retail;
      P_tran_data_ref_no_1(P_tran_data_size)                   := NULL;
      P_tran_data_ref_no_2(P_tran_data_size)                   := NULL;
      P_tran_data_gl_ref_no(P_tran_data_size)                  := NULL;
      P_tran_data_old_unit_retail(P_tran_data_size)            := NULL;
      P_tran_data_new_unit_retail(P_tran_data_size)            := NULL;
      P_tran_data_pgm_name(P_tran_data_size)                   := I_pgm_name;
      P_tran_data_sales_type(P_tran_data_size)                 := NULL;
      P_tran_data_vat_rate(P_tran_data_size)                   := NULL;
      P_tran_data_av_cost(P_tran_data_size)                    := NULL;
      P_tran_data_timestamp(P_tran_data_size)                  := SYSDATE;
      P_tran_data_ref_pack_no(P_tran_data_size)                := NULL;
      P_tran_data_tot_cost_excl_elc(P_tran_data_size)          := NULL;
      P_tran_data_act_location(P_tran_data_size)               := NULL;
      P_tran_data_act_loc_type(P_tran_data_size)               := NULL;

   elsif IO_tran_code = 37 then
      if L_cost_dept_exists = 'x' then
         L_l10n_fin_rec                     := L10N_FIN_REC();
         L_l10n_fin_rec.procedure_key       := 'RECOVERABLE_TAX_POSTING';
         L_l10n_fin_rec.country_id          := NULL;
         L_l10n_fin_rec.source_entity       := 'LOC';
         L_l10n_fin_rec.source_ID           := I_tsf_source_location;
         L_l10n_fin_rec.source_type         := I_tsf_source_loc_type;
         L_l10n_fin_rec.dest_entity         := 'LOC';
         L_l10n_fin_rec.dest_ID             := IF_location;
         L_l10n_fin_rec.dest_type           := IF_loc_type;
         L_l10n_fin_rec.av_cost             := I_from_wac;
         L_l10n_fin_rec.unit_Cost           := I_extended_base_cost;
         L_l10n_fin_rec.base_Cost           := I_base_cost;
         L_l10n_fin_rec.total_retail        := LP_total_retail_38;

         L_tran_data_rec := TRAN_DATA_REC(IF_item,
                                          IF_dept,
                                          IF_class,
                                          IF_subclass,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_tran_date,
                                          IO_tran_code,
                                          NULL,
                                          IF_units,
                                          NULL,
                                          I_total_retail,
                                          I_ref_no_1,
                                          I_ref_no_2,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL);
         L_l10n_fin_rec.tran_data   := L_tran_data_rec;
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec)= FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   if LP_system_options.default_tax_type in ('SVAT','GTAX')
      and IO_tran_code in (20,24,82,83,65)
      and NVL(LP_exempt_ind,'N') = 'N' then
      if IO_tran_code = 20   then
      --if tran_code is 20 an 87 must also be written for VAT IN
         if LF_import_id is NOT NULL and LF_import_type != 'F' then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    LF_vat_cost_rtl,
                                    L_to_loc_currency,
                                    L_imp_exp_currency,
                                    LF_vat_cost_rtl,
                                    'C',
                                    I_tran_date,
                                    NULL) = FALSE then
               return FALSE;
            end if;

            if L_store_type = 'F' then
               L_tran_loc        := IF_location;
               L_tran_loc_type   := IF_loc_type;
            else
               L_tran_loc        := LF_import_id;
               L_tran_loc_type   := LF_import_type;
            end if;
         else
            L_tran_loc        := IF_location;
            L_tran_loc_type   := IF_loc_type;
         end if;
         ---

         L_tran_code      := 87;
         if L_store_type = 'F' then
            L_total_cost   := LP_vat_amount_cost_f_curr; 
         else
            L_total_cost   := LF_vat_cost_rtl;
         end if;
         L_total_retail   := NULL;
         L_ref_no_1       := I_ref_no_1;
         L_ref_pack_no    := I_ref_pack_no;

         -- I_gl_ref_no is passed in with a tax_code for Franchise transactions.
         -- For Non-Franchise transactions, tax_code is set in this function after call to POST_TAX_AMOUNT.
         L_gl_ref_no      := NVL(I_gl_ref_no, LF_vat_code_cost_rtl);

         if LF_item_rec.container_item IS NOT NULL and NVL(I_adj_code,'X') != 'C' and LP_franchise_transaction is NULL then
            if LF_import_id is NOT NULL and LF_import_type != 'F' then
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       LF_container_vat_cost_rtl,
                                       L_to_loc_currency,
                                       L_imp_exp_currency,
                                       LF_container_vat_cost_rtl,
                                       'C',
                                       I_tran_date,
                                       NULL) = FALSE then
                  return FALSE;
               end if;
            end if;
            LF_container_total_cost := LF_container_vat_cost_rtl;
            LF_container_total_retail := NULL;
         end if;
      end if;

      if IO_tran_code in (24,65) then
      --if tran_code is 24 an 87 must also be written for VAT IN
         L_tran_loc       := IF_location;
         L_tran_loc_type  := IF_loc_type;
         L_tran_code      := 87;
         if L_store_type = 'F' then
            L_total_cost := -1 * LP_vat_amount_cost_f_curr;
         else
            L_total_cost := LF_vat_cost_rtl;
         end if;
         L_total_retail   := NULL;
         L_ref_no_1       := I_ref_no_1;
         L_ref_pack_no    := NULL;

         if LF_item_rec.container_item IS NOT NULL and NVL(I_adj_code,'X') != 'C' and LP_franchise_transaction is NULL then
            LF_container_total_cost   := LF_container_vat_cost_rtl;
            LF_container_total_retail := NULL;
         end if;
      end if;

      if IO_tran_code in (20,24,65) and L_store_type = 'F' then
         L_ref_no_2       := I_ref_no_2;
      end if;

      if IO_tran_code = 24 and L_tran_code = 87 then
         L_gl_ref_no      := NVL(I_gl_ref_no, LF_vat_code_cost_rtl);
      end if;

      if IO_tran_code = 82 then
         L_tran_loc       := IF_location;
         L_tran_loc_type  := IF_loc_type;
         L_tran_code      := 88;
         L_total_cost     := NULL;
         L_total_retail   := LP_vat_amount_rtl;
         L_ref_no_1       := I_ref_no_1;
         L_ref_no_2       := I_ref_no_2;
         L_ref_pack_no    := I_ref_pack_no;
         L_gl_ref_no      := NVL(I_gl_ref_no, LF_vat_code_cost_rtl);
      end if;
      if IO_tran_code = 83 then
         L_tran_loc       := IF_location;
         L_tran_loc_type  := IF_loc_type;
         L_tran_code      := 88;
         L_total_cost     := NULL;
         L_total_retail   := -1 * LP_vat_amount_rtl;
         L_ref_no_1       := I_ref_no_1;
         L_ref_no_2       := I_ref_no_2;
         L_ref_pack_no    := I_ref_pack_no;
         L_gl_ref_no      := NVL(I_gl_ref_no, LF_vat_code_cost_rtl);
      end if;
      ---objects for L10N_FIN_REC
      L_l10n_fin_rec.procedure_key                    := 'POST_VAT';
      L_l10n_fin_rec.country_id                       := NULL;
      L_l10n_fin_rec.source_entity                    := 'LOC';
      L_l10n_fin_rec.source_id                        := IF_location;
      L_l10n_fin_rec.item                             := IF_item;
      L_l10n_fin_rec.dept                             := IF_dept;
      L_l10n_fin_rec.class                            := IF_class;
      L_l10n_fin_rec.subclass                         := IF_subclass;

      L_tran_data_rec := TRAN_DATA_REC(IF_item,
                                       IF_dept,
                                       IF_class,
                                       IF_subclass,
                                       NULL,
                                       L_tran_loc_type,
                                       L_tran_loc,
                                       I_tran_date,
                                       L_tran_code,
                                       NULL,
                                       IF_units,
                                       L_total_cost,
                                       L_total_retail,
                                       L_ref_no_1,
                                       L_ref_no_2,
                                       L_gl_ref_no,
                                       NULL,
                                       NULL,
                                       I_pgm_name,
                                       NULL,
                                       NULL,
                                       NULL,
                                       SYSDATE,
                                       L_ref_pack_no,
                                       NULL);

      L_l10n_fin_rec.tran_data   := L_tran_data_rec;
      ---
      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_l10n_fin_rec)= FALSE then
         return FALSE;
      end if;
      ---

      -- Tran code 65 is not posted for container items and 87 will not be written for the same
      if  (IO_tran_code in (20,24) and L_tran_code = 87) and 
          (LF_item_rec.container_item IS NOT NULL and NVL(I_adj_code,'X') != 'C' and LP_franchise_transaction is NULL) then
          ---objects for L10N_FIN_REC
         L_l10n_fin_rec                                  := L10N_FIN_REC();
         L_l10n_fin_rec.procedure_key                    := 'POST_VAT';
         L_l10n_fin_rec.country_id                       := NULL;
         L_l10n_fin_rec.source_entity                    := 'LOC';
         L_l10n_fin_rec.source_id                        := IF_location;
         L_l10n_fin_rec.item                             := LF_container_rec.item;
         L_l10n_fin_rec.dept                             := LF_container_rec.dept;
         L_l10n_fin_rec.class                            := LF_container_rec.class;
         L_l10n_fin_rec.subclass                         := LF_container_rec.subclass;

         L_tran_data_rec := TRAN_DATA_REC(LF_container_rec.item,
                                          LF_container_rec.dept,
                                          LF_container_rec.class,
                                          LF_container_rec.subclass,
                                          NULL,
                                          L_tran_loc_type,
                                          L_tran_loc,
                                          I_tran_date,
                                          L_tran_code,
                                          NULL,
                                          IF_units,
                                          LF_container_total_cost,
                                          LF_container_total_retail,
                                          L_ref_no_1,
                                          L_ref_no_2,
                                          NVL(I_gl_ref_no, LF_container_vat_code_cost_rtl),
                                          NULL,
                                          NULL,
                                          I_pgm_name,
                                          NULL,
                                          NULL,
                                          NULL,
                                          SYSDATE,
                                          L_ref_pack_no,
                                          NULL);

         L_l10n_fin_rec.tran_data   := L_tran_data_rec;
         ---
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec)= FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   RETURN TRUE;

EXCEPTION
   when NO_DATA_FOUND then
      O_error_message := sql_lib.create_msg('INV_CURSOR',
                                            L_cursor,L_program,NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,L_program,NULL);
      RETURN FALSE;
END RECORD_INSERTS;

--------------------------------------------------------------------------

FUNCTION RECLASSIFY(O_error_message IN OUT VARCHAR2)
     RETURN BOOLEAN IS

   L_total_units     tran_data.units%TYPE;
   L_total_unit_cost tran_data.total_cost%TYPE;
   L_total_av_cost   tran_data.total_cost%TYPE;
   L_total_cost      tran_data.total_cost%TYPE;
   L_total_retail    tran_data.total_cost%TYPE;

BEGIN
   FOR c_sku_location_rec IN C_SKU_LOCATION LOOP
      L_total_units     := c_sku_location_rec.stock_on_hand;
      L_total_av_cost   := L_total_units * c_sku_location_rec.av_cost;
      L_total_unit_cost := L_total_units * c_sku_location_rec.unit_cost;
      L_total_retail    := L_total_units * c_sku_location_rec.unit_retail;

      if L_std_av_ind = 'S' then
         L_total_cost := L_total_unit_cost;
      else
         L_total_cost := L_total_av_cost;
      end if;
      if RECORD_INSERTS(I_item,
                        I_dept,
                        I_class,
                        I_subclass,
                        c_sku_location_rec.loc,
                        c_sku_location_rec.loc_type,
                        L_total_units,
                        L_total_cost,
                        L_total_retail) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,L_program,NULL);
      RETURN FALSE;
END RECLASSIFY;
--------------------------------------------------------------------------
--------------------------------------------------------------------------
BEGIN   -- BUILD_TRAN_DATA_INSERT


   if I_item is NOT NULL then
      if ITEM_ATTRIB_SQL.GET_LEVELS(O_error_message,
                                    L_item_level,
                                    L_tran_level,
                                    I_item) = FALSE then
         return FALSE;
      end if;
      if L_item_level < L_tran_level then --parent item
         return TRUE;
      end if;
      if I_pack_ind is null or I_sellable_ind is null or I_orderable_ind is null then
         if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                          L_pack_ind,
                                          L_sellable_ind,
                                          L_orderable_ind,
                                          L_pack_type,
                                          I_item) = FALSE then
            return FALSE;
         end if;
      else
         L_pack_ind := I_pack_ind;
         L_sellable_ind := I_sellable_ind;
         L_orderable_ind := I_orderable_ind;
         L_pack_type := I_pack_type;
      end if;
   end if;
   ---
   if IO_tran_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'IO_tran_code',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   --- Do not process a transfer of 0 units
   if IO_tran_code in (30, 31, 37, 38) and I_units = 0 then
      return TRUE;
   end if;

   --Validate input parameters:  --
   --location and loc_type--
   if ((I_location is NULL
         or I_loc_type is NULL)
         and IO_tran_code != 34)
      or (IO_tran_code in (30, 31)
         and (I_tsf_source_location is NULL
            or I_tsf_source_loc_type is NULL)) then
      O_error_message := sql_lib.create_msg('STKLEDGR_LOC',
                                            L_program,NULL,NULL);
      RETURN FALSE;
   end if;
   ---
   --total cost and total retail--
   if (IO_tran_code in (1,4,20,24,27,30,31,37,38) and (IO_total_cost IS NULL
         or (I_total_retail IS NULL and L_sellable_ind != 'N')))
      or (IO_tran_code in (26,70,80) and (IO_total_cost IS NULL
         or I_total_retail IS NOT NULL))
      or (IO_tran_code in (11,12,13,14,15,16,60,81)
         and (I_total_retail IS NULL or IO_total_cost IS NOT NULL)) then
      O_error_message := sql_lib.create_msg('STKLEDGR_PRICING',
                                            L_program,NULL,NULL);
      RETURN FALSE;
   end if;

   --total cost --
   if (IO_tran_code in (22, 23) and (IO_total_cost IS NULL)) then
      O_error_message := sql_lib.create_msg('STKLEDGR_PRICING',
                                            L_program,NULL,NULL);
   end if;

   --unit retail--
   if IO_tran_code in (11,12,13,14,15,16) and (I_old_unit_retail IS NULL
         or I_new_unit_retail IS NULL) then
      O_error_message := sql_lib.create_msg('STKLEDGR_RETAIL',
                                            L_program,NULL,NULL);
      RETURN FALSE;
   end if;

   --item--
   if I_item is NULL and IO_tran_code != 71 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   --dept--
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   --class--
   if I_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_class',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   --subclass--
   if I_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_subclass',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   --tran_date--
   if I_tran_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tran_date',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   --units--
   if I_units is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_units',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   --prg_name--
   if I_pgm_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pgm_name',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   end if;

   --- IO_tran_code
   if IO_tran_code in (37, 38, 63, 64) and
      I_gl_ref_no is NULL then
      O_error_message := sql_lib.create_msg('REQUIRED_INPUT_IS_NULL',
                                            'I_gl_ref_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- This wil be used to check for LE RCA.
   if I_pgm_name = 'REC_COST_ADJ_SQL.ITEM' then
      L_rca_ind := 'Y';
   else
      L_rca_ind := 'N';
   end if;

   -- Populate the system_options variable if it is not already populated
   if LP_system_options.std_av_ind is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options) = FALSE then
         return FALSE;
      end if;
   end if;

   L_std_av_ind  := LP_system_options.std_av_ind;
   ---
   if IO_tran_code in (30,31) then
      --- if the tran_code shows a transfer, retrieve the currency codes for
      --- the location so the markon can be calculated in the correct currency.
      ---
      L_location := I_location;
      L_loc_type := I_loc_type;
      ---
      if CURRENCY_SQL.GET_CURR_LOC(L_error_message,
                                   L_location,
                                   L_loc_type,
                                   NULL,
                                   L_to_curr_code) = FALSE then
         O_error_message := L_error_message;
         return FALSE;
      end if;

      L_loc_type := I_tsf_source_loc_type;
      L_location := I_tsf_source_location;

      if CURRENCY_SQL.GET_CURR_LOC(L_error_message,
                                   L_location,
                                   L_loc_type,
                                   NULL,
                                   L_from_curr_code) = FALSE then
         O_error_message := L_error_message;
         return FALSE;
      end if;
      ---
   elsif IO_tran_code = 34 then

      --since tran_data is kept at the transaction level, reclassification of
      --pack items will not affect the component items
      --so, exit without doing anything. Also once an item above the tran level
      --is reclassified only the tran level children need to go thru
      --the reclassification function

      if ITEM_ATTRIB_SQL.GET_LEVELS(O_error_message,
                                    L_item_level,
                                    L_tran_level,
                                    I_item) = FALSE then
         return FALSE;
      end if;
      if L_pack_ind = 'Y' or L_item_level < L_tran_level then
         return TRUE;
      end if;


      if I_dept = I_source_dept and
         I_class = I_source_class and
         I_subclass = I_source_subclass then
         O_error_message := sql_lib.create_msg('NOT_RECLASSIFIED',
                                               L_program,NULL,NULL);
         return FALSE;
      end if;
      --for tran_code 34 just call RECLASSIFY and do not continue with
      --any of the other processing so return TRUE to exit
      if RECLASSIFY(O_error_message) = FALSE then
         return FALSE;
      end if;
      return TRUE;
   end if; 
   ---
   if L_pack_ind = 'N' or L_pack_ind is NULL then
      if RECORD_INSERTS(I_item,
                        I_dept,
                        I_class,
                        I_subclass,
                        I_location,
                        I_loc_type,
                        I_units,
                        IO_total_cost,
                        I_total_retail,
                        I_ship_date) = FALSE then
         return FALSE;
      end if;
   else
      SQL_LIB.SET_MARK('LOOP','C_PACK_ITEM','V_PACKSKU_QTY','item '||I_item);
      FOR rec in C_PACK_ITEM LOOP
         L_item := rec.item;
         L_qty := rec.qty;

         L_units := I_units * L_qty;
         --- get dept/class/subclass data for the item ---
         SQL_LIB.SET_MARK('OPEN','C_GET_ITEM_DATA','ITEM_MASTER',
                          'item '||L_item);
         open C_GET_ITEM_DATA;
         SQL_LIB.SET_MARK('FETCH','C_GET_ITEM_DATA','ITEM_MASTER',
                          'item '||L_item);
         fetch C_GET_ITEM_DATA into L_dept,
                                    L_class,
                                    L_subclass;
         if C_GET_ITEM_DATA%NOTFOUND then
            SQL_LIB.SET_MARK('CLOSE','C_GET_ITEM_DATA',NULL,NULL);
            close C_GET_ITEM_DATA;
            L_cursor := 'C_GET_ITEM_DATA';
            raise NO_DATA_FOUND;
         end if;
         SQL_LIB.SET_MARK('CLOSE','C_GET_ITEM_DATA',NULL,NULL);
         close C_GET_ITEM_DATA;

         if ((IO_tran_code in (30,31)) or (I_loc_type in ('X', 'M')) and (IO_tran_code = 38)) then
            L_location := I_tsf_source_location;
            L_loc_type := I_tsf_source_loc_type;
         else
            --- no receiving locs avaiable, therefore, tran_code is not 30, 31 or 38
            L_location := I_location;
            L_loc_type := I_loc_type;
         end if;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_COST_RETAIL','ITEM_LOC',
                          'item '||L_item||', loc '||to_char(L_location));
         open C_GET_COST_RETAIL;
         SQL_LIB.SET_MARK('FETCH','C_GET_COST_RETAIL','ITEM_LOC',
                          'item '||L_item||', loc '||to_char(L_location));
         fetch C_GET_COST_RETAIL into L_total_cost,
                                      L_total_retail;
         if C_GET_COST_RETAIL%NOTFOUND then
            SQL_LIB.SET_MARK('CLOSE','C_GET_COST_RETAIL',NULL,NULL);
            close C_GET_COST_RETAIL;
            L_cursor := 'C_GET_COST_RETAIL';
            raise NO_DATA_FOUND;
         end if;
         SQL_LIB.SET_MARK('CLOSE','C_GET_COST_RETAIL',NULL,NULL);
         close C_GET_COST_RETAIL;

         if I_loc_type in ('X', 'M') then
            L_loc_type := 'W';
         else
            L_loc_type := I_loc_type;
         end if;
         ---
         if RECORD_INSERTS(L_item,
                           L_dept,
                           L_class,
                           L_subclass,
                           I_location,
                           L_loc_type,
                           L_units,
                           L_total_cost,
                           L_total_retail,
                           I_ship_date) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;
   return TRUE;

EXCEPTION
   when NO_DATA_FOUND then
      O_error_message := sql_lib.create_msg('INV_CURSOR',L_cursor,
                                            L_program,NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,
                                            L_program,NULL);
      RETURN FALSE;
END BUILD_TRAN_DATA_INSERT;
--------------------------------------------------------------------------
FUNCTION BUILD_TRAN_DATA_INSERT
                (O_error_message        IN OUT  VARCHAR2,
                 I_item                 IN      TRAN_DATA.ITEM%TYPE,
                 I_dept                 IN      TRAN_DATA.DEPT%TYPE,
                 I_class                IN      TRAN_DATA.CLASS%TYPE,
                 I_subclass             IN      TRAN_DATA.SUBCLASS%TYPE,
                 I_location             IN      TRAN_DATA.LOCATION%TYPE,
                 I_loc_type             IN      TRAN_DATA.LOC_TYPE%TYPE,
                 I_tran_date            IN      TRAN_DATA.TRAN_DATE%TYPE,
                 IO_tran_code           IN OUT  TRAN_DATA.TRAN_CODE%TYPE,
                 I_adj_code             IN      TRAN_DATA.ADJ_CODE%TYPE,
                 I_units                IN      TRAN_DATA.UNITS%TYPE,
                 IO_total_cost          IN OUT  TRAN_DATA.TOTAL_COST%TYPE,
                 I_total_retail         IN      TRAN_DATA.TOTAL_RETAIL%TYPE,
                 I_ref_no_1             IN      TRAN_DATA.REF_NO_1%TYPE,
                 I_ref_no_2             IN      TRAN_DATA.REF_NO_2%TYPE,
                 I_tsf_source_location  IN      TRAN_DATA.LOCATION%TYPE,
                 I_tsf_source_loc_type  IN      TRAN_DATA.LOC_TYPE%TYPE,
                 I_old_unit_retail      IN      TRAN_DATA.OLD_UNIT_RETAIL%TYPE,
                 I_new_unit_retail      IN      TRAN_DATA.NEW_UNIT_RETAIL%TYPE,
                 I_source_dept          IN      TRAN_DATA.DEPT%TYPE,
                 I_source_class         IN      TRAN_DATA.CLASS%TYPE,
                 I_source_subclass      IN      TRAN_DATA.SUBCLASS%TYPE,
                 I_pgm_name             IN      TRAN_DATA.PGM_NAME%TYPE,
                 I_gl_ref_no            IN      TRAN_DATA.GL_REF_NO%TYPE DEFAULT NULL,
                 I_pack_ind             IN      ITEM_MASTER.PACK_IND%TYPE DEFAULT NULL,
                 I_sellable_ind         IN      ITEM_MASTER.SELLABLE_IND%TYPE DEFAULT NULL,
                 I_orderable_ind        IN      ITEM_MASTER.ORDERABLE_IND%TYPE DEFAULT NULL,
                 I_pack_type            IN      ITEM_MASTER.PACK_TYPE%TYPE DEFAULT NULL,
                 I_vat_rate             IN      VAT_CODE_RATES.VAT_RATE%TYPE DEFAULT NULL,
                 I_class_vat_ind        IN      CLASS.CLASS_VAT_IND%TYPE DEFAULT NULL,
                 I_ref_pack_no          IN      TRAN_DATA.REF_PACK_NO%TYPE DEFAULT NULL,
                 I_total_cost_excl_elc  IN      TRAN_DATA.TOTAL_COST_EXCL_ELC%TYPE DEFAULT NULL,
                 I_tax_value            IN      GTAX_ITEM_ROLLUP.CUM_TAX_PCT%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT';
BEGIN
   if BUILD_TRAN_DATA_INSERT(O_error_message,
                             I_item,
                             I_dept,
                             I_class,
                             I_subclass,
                             I_location,
                             I_loc_type,
                             I_tran_date,
                             IO_tran_code,
                             I_adj_code,
                             I_units,
                             IO_total_cost,
                             I_total_retail,
                             NULL,       -- I_to_loc_unit_retail, for tran_code 30/32 only
                             I_ref_no_1,
                             I_ref_no_2,
                             I_tsf_source_location,
                             I_tsf_source_loc_type,
                             I_old_unit_retail,
                             I_new_unit_retail,
                             I_source_dept,
                             I_source_class,
                             I_source_subclass,
                             I_pgm_name,
                             I_gl_ref_no,
                             NULL,       -- I_distro_type
                             I_pack_ind,
                             I_sellable_ind,
                             I_orderable_ind,
                             I_pack_type,
                             I_vat_rate,
                             I_class_vat_ind,
                             I_ref_pack_no,
                             I_total_cost_excl_elc,
                             I_tax_value) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END BUILD_TRAN_DATA_INSERT;
--------------------------------------------------------------------------
FUNCTION FLUSH_TRAN_DATA_INSERT (O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)            := 'STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT';
   L_deal_data_size       NUMBER                  := 0;
   L_location             TRAN_DATA.LOCATION%TYPE := NULL;
   L_loc_type             TRAN_DATA.LOC_TYPE%TYPE := NULL;
   L_store_type           STORE.STORE_TYPE%TYPE   := NULL;

   L_to_curr_code         CURRENCIES.CURRENCY_CODE%TYPE;
   L_tot_cost_excl_elc    TRAN_DATA.TOTAL_COST_EXCL_ELC%TYPE;
   L_total_retail         TRAN_DATA.TOTAL_RETAIL%TYPE;

   L_tran_loc_tbl         LOC_TBL := LOC_TBL();

   cursor C_STORE_TYPE is
      select store_type
        from store
       where store = L_location;

   cursor C_GET_CURRENCY_DEC is
      select c.currency_cost_dec,
             c.currency_rtl_dec,
             c.currency_code,
             l.location
        from currencies c,
             (select TO_NUMBER(p.partner_id) location,
                     p.currency_code
                from partner p
               where partner_type = 'E'
               union all
              select w.wh location,
                     w.currency_code
                from wh w
               union all
              select s.store location,
                     s.currency_code
                from store s) l,
             TABLE(CAST(L_tran_loc_tbl AS "LOC_TBL")) loc
      where l.currency_code = c.currency_code
        and l.location = value(loc);

   TYPE LOC_CURR_DEC_TAB is TABLE OF C_GET_CURRENCY_DEC%ROWTYPE INDEX BY BINARY_INTEGER;
   L_currency_loc   LOC_CURR_DEC_TAB;

   L_cur_rtl_dec            OBJ_NUMERIC_ID_TABLE;
   L_cur_cost_dec           OBJ_NUMERIC_ID_TABLE;

BEGIN
   if P_tran_data_size > 0 then

      for i in 1 .. P_tran_data_location.LAST loop
         L_tran_loc_tbl.extend();
         L_tran_loc_tbl(i) := P_tran_data_location(i);
      end loop;

      open C_GET_CURRENCY_DEC;
      fetch C_GET_CURRENCY_DEC BULK COLLECT into L_currency_loc;
      close C_GET_CURRENCY_DEC;

      L_cur_rtl_dec  := new OBJ_NUMERIC_ID_TABLE();
      L_cur_cost_dec := new OBJ_NUMERIC_ID_TABLE();

      -- Reorder the currency_rtl_dec and currency_cost_dec values fetched in C_GET_CURRENCY_DEC cursor,
      -- This collection should be ordered in such a way that it will
      -- to coincide with the locations in P_tran_data_location collection.
      for i in 1 .. P_tran_data_location.LAST loop
         for j in 1 .. L_currency_loc.last loop
            if L_currency_loc(j).location = P_tran_data_location(i) then
               L_cur_cost_dec.extend();
               L_cur_rtl_dec.extend();
               L_cur_cost_dec(i):= L_currency_loc(j).currency_cost_dec;
               L_cur_rtl_dec(i) := L_currency_loc(j).currency_rtl_dec;
               exit;
            end if;
         end loop;
      end loop;

      SQL_LIB.SET_MARK('INSERT',NULL,'tran_data',NULL);
      FORALL i IN 1..P_tran_data_size
         insert into tran_data (item,
                                dept,
                                class,
                                subclass,
                                pack_ind,
                                location,
                                loc_type,
                                tran_date,
                                tran_code,
                                adj_code,
                                units,
                                total_cost,
                                total_retail,
                                ref_no_1,
                                ref_no_2,
                                gl_ref_no,
                                old_unit_retail,
                                new_unit_retail,
                                pgm_name,
                                sales_type,
                                vat_rate,
                                av_cost,
                                timestamp,
                                ref_pack_no,
                                total_cost_excl_elc)
                         values(P_tran_data_item(i),
                                P_tran_data_dept(i),
                                P_tran_data_class(i),
                                P_tran_data_subclass(i),
                                P_tran_data_pack_ind(i),
                                P_tran_data_location(i),
                                P_tran_data_loc_type(i),
                                P_tran_data_tran_date(i),
                                P_tran_data_tran_code(i),
                                P_tran_data_adj_code(i),
                                P_tran_data_units(i),
                                round (P_tran_data_total_cost(i), L_cur_cost_dec(i)),
                                round (P_tran_data_total_retail(i), L_cur_rtl_dec(i)),
                                P_tran_data_ref_no_1(i),
                                P_tran_data_ref_no_2(i),
                                P_tran_data_gl_ref_no(i),
                                P_tran_data_old_unit_retail(i),
                                P_tran_data_new_unit_retail(i),
                                P_tran_data_pgm_name(i),
                                P_tran_data_sales_type(i),
                                P_tran_data_vat_rate(i),
                                P_tran_data_av_cost(i),
                                P_tran_data_timestamp(i),
                                P_tran_data_ref_pack_no(i),
                                round (P_tran_data_tot_cost_excl_elc(i), L_cur_cost_dec(i)));
   end if;

   if P_tran_data_size > 0 then
      FOR j IN 1..P_tran_data_size LOOP
         L_location := P_tran_data_location(j);
         L_loc_type := P_tran_data_loc_type(j);
         L_tot_cost_excl_elc := NULL;
         L_total_retail      := NULL;

         if L_loc_type = 'S' then
            open  C_STORE_TYPE;
            fetch C_STORE_TYPE into L_store_type;
            close C_STORE_TYPE;
         end if;

         if P_tran_data_tran_code(j) = 20 and NVL(L_store_type,'X') <> 'F' then

            L_deal_data_size := L_deal_data_size + 1;

            P_deal_data_item(L_deal_data_size)            := P_tran_data_item(j);
            P_deal_data_dept(L_deal_data_size)            := P_tran_data_dept(j);
            P_deal_data_class(L_deal_data_size)           := P_tran_data_class(j);
            P_deal_data_subclass(L_deal_data_size)        := P_tran_data_subclass(j);
            P_deal_data_act_loc_type(L_deal_data_size)    := NVL(P_tran_data_act_loc_type(j),P_tran_data_loc_type(j));
            P_deal_data_act_location(L_deal_data_size)    := NVL(P_tran_data_act_location(j),P_tran_data_location(j));
            P_deal_data_tran_date(L_deal_data_size)       := P_tran_data_tran_date(j);
            P_deal_data_tran_code(L_deal_data_size)       := P_tran_data_tran_code(j);
            P_deal_data_adj_code(L_deal_data_size)        := P_tran_data_adj_code(j);
            P_deal_data_pgm_name(L_deal_data_size)        := P_tran_data_pgm_name(j);
            P_deal_data_units(L_deal_data_size)           := P_tran_data_units(j);

            L_tot_cost_excl_elc := P_tran_data_tot_cost_excl_elc(j);
            L_total_retail      := P_tran_data_total_retail(j);

            if P_tran_data_act_location(j) is NOT NULL then
               -- If the actual location is not null, convert P_tran_data_tot_cost_excl_elc
               -- and P_tran_data_total_retail to the actual location currency.
               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   P_tran_data_location(j),
                                                   P_tran_data_loc_type(j),
                                                   NULL,
                                                   P_tran_data_act_location(j),
                                                   P_tran_data_act_loc_type(j),
                                                   NULL,
                                                   L_tot_cost_excl_elc,
                                                   L_tot_cost_excl_elc,
                                                   'C',
                                                   P_tran_data_tran_date(j),
                                                   NULL) = FALSE then
                  return FALSE;
               end if;

               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   P_tran_data_location(j),
                                                   P_tran_data_loc_type(j),
                                                   NULL,
                                                   P_tran_data_act_location(j),
                                                   P_tran_data_act_loc_type(j),
                                                   NULL,
                                                   L_total_retail,
                                                   L_total_retail,
                                                   'R',
                                                   P_tran_data_tran_date(j),
                                                   NULL) = FALSE then
                  return FALSE;
               end if;
            end if;

            -- Get currency for actual location
            if CURRENCY_SQL.GET_CURR_LOC (O_error_message,
                                          P_deal_data_act_location(L_deal_data_size),
                                          P_deal_data_act_loc_type(L_deal_data_size),
                                          NULL,
                                          L_to_curr_code) = FALSE then
               raise PROGRAM_ERROR;
            end if;

            -- round L_tot_cost_excl_elc to the currency decimal of the deal actual location
            if CURRENCY_SQL.ROUND_CURRENCY(O_error_message,
                                           L_tot_cost_excl_elc,
                                           L_to_curr_code,
                                           'C') = FALSE then
               raise PROGRAM_ERROR;
            end if;

            -- round L_total_retail to the currency decimal of the deal actual location
            if CURRENCY_SQL.ROUND_CURRENCY(O_error_message,
                                           L_total_retail,
                                           L_to_curr_code,
                                           'R') = FALSE then
               raise PROGRAM_ERROR;
            end if;

            P_deal_data_total_cost(L_deal_data_size)      := L_tot_cost_excl_elc;
            P_deal_data_total_retail(L_deal_data_size)    := L_total_retail;
            P_deal_data_ref_no_1(L_deal_data_size)        := P_tran_data_ref_no_1(j);
            P_deal_data_vat_rate(L_deal_data_size)        := P_tran_data_vat_rate(j);
            P_deal_data_ref_pack_no(L_deal_data_size)     := P_tran_data_ref_pack_no(j);

         end if;

      END LOOP;

      if L_deal_data_size > 0 then

         SQL_LIB.SET_MARK('INSERT',NULL,'deal_perf_data_temp',NULL);
         FORALL k in 1..L_deal_data_size
            insert into deal_perf_data_temp(item,
                                            dept,
                                            class,
                                            subclass,
                                            loc_type,
                                            location,
                                            tran_date,
                                            tran_code,
                                            adj_code,
                                            pgm_name,
                                            units,
                                            total_cost,
                                            total_retail,
                                            ref_no1,
                                            vat_rate,
                                            ref_pack_no)
                                     values(P_deal_data_item(k),
                                            P_deal_data_dept(k),
                                            P_deal_data_class(k),
                                            P_deal_data_subclass(k),
                                            P_deal_data_act_loc_type(k),
                                            P_deal_data_act_location(k),
                                            P_deal_data_tran_date(k),
                                            P_deal_data_tran_code(k),
                                            P_deal_data_adj_code(k),
                                            P_deal_data_pgm_name(k),
                                            P_deal_data_units(k),
                                            P_deal_data_total_cost(k),
                                            P_deal_data_total_retail(k),
                                            P_deal_data_ref_no_1(k),
                                            P_deal_data_vat_rate(k),
                                            P_deal_data_ref_pack_no(k));
      end if;
   end if;

   P_tran_data_size := 0;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,
                                            L_program,NULL);
      RETURN FALSE;
END FLUSH_TRAN_DATA_INSERT;
--------------------------------------------------------------------------
FUNCTION TRAN_DATA_INSERT
                (O_error_message        IN OUT  VARCHAR2,
                 I_item                 IN      TRAN_DATA.ITEM%TYPE,
                 I_dept                 IN      TRAN_DATA.DEPT%TYPE,
                 I_class                IN      TRAN_DATA.CLASS%TYPE,
                 I_subclass             IN      TRAN_DATA.SUBCLASS%TYPE,
                 I_location             IN      TRAN_DATA.LOCATION%TYPE,
                 I_loc_type             IN      TRAN_DATA.LOC_TYPE%TYPE,
                 I_tran_date            IN      TRAN_DATA.TRAN_DATE%TYPE,
                 IO_tran_code           IN OUT  TRAN_DATA.TRAN_CODE%TYPE,
                 I_adj_code             IN      TRAN_DATA.ADJ_CODE%TYPE,
                 I_units                IN      TRAN_DATA.UNITS%TYPE,
                 IO_total_cost          IN OUT  TRAN_DATA.TOTAL_COST%TYPE,
                 I_total_retail         IN      TRAN_DATA.TOTAL_RETAIL%TYPE,
                 I_ref_no_1             IN      TRAN_DATA.REF_NO_1%TYPE,
                 I_ref_no_2             IN      TRAN_DATA.REF_NO_2%TYPE,
                 I_tsf_source_location  IN      TRAN_DATA.LOCATION%TYPE,
                 I_tsf_source_loc_type  IN      TRAN_DATA.LOC_TYPE%TYPE,
                 I_old_unit_retail      IN      TRAN_DATA.OLD_UNIT_RETAIL%TYPE,
                 I_new_unit_retail      IN      TRAN_DATA.NEW_UNIT_RETAIL%TYPE,
                 I_source_dept          IN      TRAN_DATA.DEPT%TYPE,
                 I_source_class         IN      TRAN_DATA.CLASS%TYPE,
                 I_source_subclass      IN      TRAN_DATA.SUBCLASS%TYPE,
                 I_pgm_name             IN      TRAN_DATA.PGM_NAME%TYPE,
                 I_gl_ref_no            IN      TRAN_DATA.GL_REF_NO%TYPE DEFAULT NULL,
                 I_ref_pack_no          IN      TRAN_DATA.REF_PACK_NO%TYPE DEFAULT NULL,
                 I_total_cost_excl_elc  IN      TRAN_DATA.TOTAL_COST_EXCL_ELC%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)     := 'STKLEDGR_SQL.TRAN_DATA_INSERT';
   L_store_row     STORE%ROWTYPE;
   L_exist_store   BOOLEAN;

BEGIN
   if I_loc_type = 'S' then
      if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                  L_exist_store,
                                  L_store_row,
                                  I_location) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exist_store = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      -- Check if the input store is a valid WF store.
      elsif L_store_row.store_type in ('W', 'F') and L_store_row.stockholding_ind = 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('NONSTOCK_WF_STORE_INV',
                                               L_program,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;
   ---
   --empty out cache of tran_data inserts
   P_tran_data_size := 0;
   /*P_tran_data_item.DELETE;
   P_tran_data_dept.DELETE;
   P_tran_data_class.DELETE;
   P_tran_data_subclass.DELETE;
   P_tran_data_pack_ind.DELETE;
   P_tran_data_location.DELETE;
   P_tran_data_loc_type.DELETE;
   P_tran_data_tran_date.DELETE;
   P_tran_data_tran_code.DELETE;
   P_tran_data_adj_code.DELETE;
   P_tran_data_units.DELETE;
   P_tran_data_total_cost.DELETE;
   P_tran_data_total_retail.DELETE;
   P_tran_data_ref_no_1.DELETE;
   P_tran_data_ref_no_2.DELETE;
   P_tran_data_gl_ref_no.DELETE;
   P_tran_data_old_unit_retail.DELETE;
   P_tran_data_new_unit_retail.DELETE;
   P_tran_data_pgm_name.DELETE;
   P_tran_data_sales_type.DELETE;
   P_tran_data_vat_rate.DELETE;
   P_tran_data_av_cost.DELETE;
   P_tran_data_timestamp.DELETE;*/

   --call build
   if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT( O_error_message,
                                           I_item,
                                           I_dept,
                                           I_class,
                                           I_subclass,
                                           I_location,
                                           I_loc_type,
                                           I_tran_date,
                                           IO_tran_code,
                                           I_adj_code,
                                           I_units,
                                           IO_total_cost,
                                           I_total_retail,
                                           I_ref_no_1,
                                           I_ref_no_2,
                                           I_tsf_source_location,
                                           I_tsf_source_loc_type,
                                           I_old_unit_retail,
                                           I_new_unit_retail,
                                           I_source_dept,
                                           I_source_class,
                                           I_source_subclass,
                                           I_pgm_name,
                                           I_gl_ref_no,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           I_ref_pack_no,
                                           I_total_cost_excl_elc) = FALSE then

      return FALSE;
   end if;
   --call flush
   if FLUSH_TRAN_DATA_INSERT (O_error_message) = FALSE then
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,L_program,NULL);
      return FALSE;
END TRAN_DATA_INSERT;
--------------------------------------------------------------------------
FUNCTION STOCK_LEDGER_INSERT (i_type_code     IN     VARCHAR2,
                              i_dept          IN     NUMBER,
                              i_class         IN     NUMBER,
                              i_subclass      IN     NUMBER,
                              i_location      IN     NUMBER,
                              I_async_ind      IN     CHAR,
                              O_error_message IN OUT VARCHAR2) RETURN BOOLEAN IS
   L_temp     VARCHAR2(1);
   L_dept     DEPS.DEPT%TYPE;
   L_class    CLASS.CLASS%TYPE;
   L_subclass SUBCLASS.SUBCLASS%TYPE;
   L_location TRAN_DATA.LOCATION%TYPE;

   L_cursor   VARCHAR2(20)   := NULL;
   L_program  VARCHAR2(50)   := 'STKLEDGR_SQL.STOCK_LEDGER_INSERT';

   L_store_row      STORE%ROWTYPE;
   L_exist_store    BOOLEAN;
   L_rms_async_id   STORE_ADD.RMS_ASYNC_ID%TYPE;

  cursor C_VAL_STORE is
      select 'x'
        from store
       where store = i_location;

   cursor C_VAL_WH is
      select 'x'
        from wh
       where wh = i_location;

   cursor C_VAL_DEPT is
      select 'x'
        from deps
       where dept = i_dept;

   cursor C_VAL_CLASS is
      select 'x'
        from class
       where dept = i_dept
         and class = i_class;

   cursor C_VAL_SUBCLASS is
      select 'x'
        from subclass
       where dept = i_dept
         and class = i_class
         and subclass = i_subclass;

   cursor C_VAL_FIN is
      select 'x'
        from partner
       where partner_id = TO_CHAR(i_location)
         and partner_type = 'E';

BEGIN
   if i_type_code = 'S' then
      /* called from store.fmb */
      if i_location is NULL then
         O_error_message := sql_lib.create_msg('STKLEDGR_LOC',L_program,NULL,NULL);
         return FALSE;
      else
         open C_VAL_STORE;
         fetch C_VAL_STORE into L_temp;
         if C_VAL_STORE%NOTFOUND then
            close C_VAL_STORE;
            L_cursor := 'C_VAL_STORE';
            raise NO_DATA_FOUND;
         else
            if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                        L_exist_store,
                                        L_store_row,
                                        I_location) = FALSE then
               return FALSE;
            end if;
            ---
            if L_exist_store = FALSE then
               O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            -- Check if the input store is a valid WF store.
            elsif L_store_row.store_type in ('W', 'F') and L_store_row.stockholding_ind = 'N' then
               O_error_message := SQL_LIB.CREATE_MSG('NONSTOCK_WF_STORE_INV',
                                                     L_program,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;
            ---
            L_location := i_location;
            L_dept     := NULL;
            L_class    := NULL;
            L_subclass := NULL;
         end if;
      end if;
   elsif i_type_code = 'W' then
      /* called from warehse.fmb */
      if i_location is NULL then
         O_error_message := sql_lib.create_msg('STKLEDGR_LOC',L_program,NULL,NULL);
         return FALSE;
      else
         open C_VAL_WH;
         fetch C_VAL_WH into L_temp;
         if C_VAL_WH%NOTFOUND then
            close C_VAL_WH;
            L_cursor := 'C_VAL_WH';
            raise NO_DATA_FOUND;
         else
            close C_VAL_WH;
            L_location := i_location;
            L_dept     := NULL;
            L_class    := NULL;
            L_subclass := NULL;
         end if;
      end if;
   elsif i_type_code = 'D' then
      /* called from dept.fmb */
      if i_dept is NULL then
         O_error_message := sql_lib.create_msg('STKLEDGR_DEPT',L_program,NULL,NULL);
         return FALSE;
      else
         open C_VAL_DEPT;
         fetch C_VAL_DEPT into L_temp;
         if C_VAL_DEPT%NOTFOUND then
            close C_VAL_DEPT;
            L_cursor := 'C_VAL_DEPT';
            raise NO_DATA_FOUND;
         else
            close C_VAL_DEPT;
            L_dept     := i_dept;
            L_location := NULL;
            L_class    := NULL;
            L_subclass := NULL;
         end if;
      end if;
   elsif i_type_code = 'C' then
      /* called from class.fmb */
      if i_dept is NULL or i_class is NULL then
         O_error_message := sql_lib.create_msg('STKLEDGR_CLASS',L_program,NULL,NULL);
         return FALSE;
      else
         open C_VAL_CLASS;
         fetch C_VAL_CLASS into L_temp;
         if C_VAL_CLASS%NOTFOUND then
            close C_VAL_CLASS;
            L_cursor := 'C_VAL_CLASS';
            raise NO_DATA_FOUND;
         else
            close C_VAL_CLASS;
            L_dept     := i_dept;
            L_class    := i_class;
            L_subclass := NULL;
            L_location := NULL;
         end if;
      end if;
    elsif i_type_code = 'B' then
      /* called from subclass.fmb */
      if i_dept is NULL or i_class is NULL or i_subclass is NULL then
         O_error_message := sql_lib.create_msg('STKLEDGR_DCS',L_program,NULL,NULL);
         return FALSE;
      else
         open C_VAL_SUBCLASS;
         fetch C_VAL_SUBCLASS into L_temp;
         if C_VAL_SUBCLASS%NOTFOUND then
            close C_VAL_SUBCLASS;
            L_cursor := 'C_VAL_SUBCLASS';
            raise NO_DATA_FOUND;
         else
            close C_VAL_SUBCLASS;
            L_dept     := i_dept;
            L_class    := i_class;
            L_subclass := i_subclass;
            L_location := NULL;
         end if;
      end if;
    elsif i_type_code = 'E' then
      /* called from partner.fmb */
      if i_location is NULL then
         O_error_message := sql_lib.create_msg('STKLEDGR_LOC',L_program,NULL,NULL);
         return FALSE;
      else
         open C_VAL_FIN;
         fetch C_VAL_FIN into L_temp;
         if C_VAL_FIN%NOTFOUND then
            close C_VAL_FIN;
            L_cursor := 'C_VAL_FIN';
            raise NO_DATA_FOUND;
         else
            close C_VAL_FIN;
            L_location := i_location;
            L_dept     := NULL;
            L_class    := NULL;
            L_subclass := NULL;
         end if;
      end if;
   else
      O_error_message := sql_lib.create_msg('STKLEDGR_CODE',L_program,NULL,NULL);
      return FALSE;
   end if;
   L_rms_async_id := RMS_ASYNC_ID_SEQ.nextval;
   INSERT INTO stock_ledger_inserts(type_code,
                                    dept,
                                    class,
                                    subclass,
                                    location,
                                    rms_async_id)
                             VALUES(i_type_code,
                                    L_dept,
                                    L_class,
                                    L_subclass,
                                    L_location,
                                    L_rms_async_id);
  IF I_async_ind                                                                  = 'Y' THEN
    IF RMS_ASYNC_PROCESS_SQL.ENQUEUE_STKLGR_INSERT(O_error_message,L_rms_async_id)=FALSE THEN
      RETURN false;
    END IF;
  ELSE
    IF CORESVC_STKLEDGR_INSERTS_SQL.PROCESS_STKLEDGR_INSERTS(O_error_message,L_rms_async_id)=FALSE THEN
      RETURN false;
    END IF;
  END IF;

  return TRUE;

EXCEPTION
   when NO_DATA_FOUND then
      O_error_message := sql_lib.create_msg('INV_CURSOR',L_program,L_cursor,NULL);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,L_program,NULL);
      return FALSE;
END STOCK_LEDGER_INSERT;
--------------------------------------------------------------------------
FUNCTION STOCK_LEDGER_INSERT
  (
    i_type_code     IN VARCHAR2,
    i_dept          IN NUMBER,
    i_class         IN NUMBER,
    i_subclass      IN NUMBER,
    i_location      IN NUMBER,
    O_error_message IN OUT VARCHAR2
  )
  RETURN BOOLEAN
IS
  L_program VARCHAR2(50) := 'STKLEDGR_SQL.STOCK_LEDGER_INSERT';
BEGIN
  IF STOCK_LEDGER_INSERT (i_type_code, i_dept, i_class, i_subclass, i_location, 'N', O_error_message) = FALSE THEN
    RETURN false;
  END IF;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,L_program,NULL);
  RETURN FALSE;
END STOCK_LEDGER_INSERT;
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_COSTS_RETAILS(O_error_message         IN OUT rtk_errors.rtk_text%TYPE,
                               O_av_cost               IN OUT item_loc_soh.av_cost%TYPE,
                               O_from_unit_retail      IN OUT item_loc.unit_retail%TYPE,
                               O_to_unit_retail        IN OUT item_loc.unit_retail%TYPE,
                               I_tsf_no                IN     tsfhead.tsf_no%type,
                               I_intercompany          IN     BOOLEAN,
                               I_from_finisher_ind     IN     VARCHAR2,
                               I_to_finisher_ind       IN     VARCHAR2,
                               I_item                  IN     item_loc.item%TYPE,
                               I_from_loc              IN     item_loc.loc%TYPE,
                               I_from_loc_type         IN     item_loc.loc_type%TYPE,
                               I_to_loc                IN     item_loc.loc%TYPE,
                               I_to_loc_type           IN     item_loc.loc_type%TYPE)

RETURN BOOLEAN IS

   L_tsf_from_loc            item_loc.loc%TYPE;
   L_tsf_from_loc_type       item_loc.loc_type%TYPE;
   L_tsf_to_loc              item_loc.loc%TYPE;
   L_tsf_to_loc_type         item_loc.loc_type%TYPE;
   L_loc_type                item_loc.loc_type%TYPE;
   L_tsf_no                  tsfhead.tsf_no%TYPE;

   L_av_cost                 item_loc_soh.av_cost%TYPE;
   L_unit_cost               item_loc_soh.unit_cost%TYPE;
   L_unit_retail             item_loc.unit_retail%TYPE;
   L_selling_unit_retail     item_loc.selling_unit_retail%TYPE;
   L_selling_uom             item_loc.selling_uom%TYPE;

   L_xform_item_exist        BOOLEAN;
   L_xform_item_ind          VARCHAR2(1) := 'N';
   L_from_item               tsf_xform_detail.from_item%TYPE;
   L_to_item                 tsf_xform_detail.to_item%TYPE;
   L_tsf_type                tsfhead.tsf_type%type;
   L_tsf_markdown_loc        item_loc.loc%TYPE;

   L_item_rec                item_master%ROWTYPE := NULL;
   L_phy_wh                  BOOLEAN := FALSE;
   L_vr_wh                   ITEM_LOC.LOC%TYPE;

   L_program                 VARCHAR2(50)   := 'STKLEDGR_SQL.GET_TSF_COSTS_RETAILS';

   CURSOR C_tsf_parent_info is
      select th1.tsf_no,
             th1.tsf_type,
             from_loc,
             from_loc_type
        from tsfhead th1
       where exists (select 'X'
                       from tsfhead th2
                      where th2.tsf_no = I_tsf_no
                        and th1.tsf_no = th2.tsf_parent_no);

   CURSOR C_tsf_child_info is
      select tsf_type,
             to_loc,
             to_loc_type
        from tsfhead
       where tsf_parent_no = I_tsf_no;

   CURSOR C_xform_item_exist is
      select xd.from_item
        from tsf_xform x,
             tsf_xform_detail xd
       where x.tsf_no = L_tsf_no
         and x.tsf_xform_id = xd.tsf_xform_id
         and xd.to_item = I_item;

   CURSOR C_VWH is
      select from_loc
        from tsfitem_inv_flow
       where from_loc_type = 'W'
         and tsf_no        = I_tsf_no
         and item          = I_item
         and ROWNUM        = 1;

BEGIN
   ---
   if STKLEDGR_SQL.GET_TSF_MARKDOWN_LOC(O_error_message,
                                        L_tsf_markdown_loc,
                                        I_tsf_no,
                                        I_from_loc,
                                        I_to_loc,
                                        I_from_loc_type,
                                        I_to_loc_type) = FALSE then
      return FALSE;
   end if;

   if ITEMLOC_ATTRIB_SQL.GET_AV_COST(O_error_message,
                                     I_item,
                                     I_from_loc,
                                     I_from_loc_type,
                                     O_av_cost) = FALSE then
      return FALSE;
   end if;
   ---
   if I_from_finisher_ind = 'Y' then    -- 2nd-leg of 2-legs transfer
      ---
      open C_tsf_parent_info;
      fetch C_tsf_parent_info into L_tsf_no,
                                   L_tsf_type,
                                   L_tsf_from_loc,
                                   L_tsf_from_loc_type;
      close C_tsf_parent_info;
      ---
      if L_tsf_type != 'CO' then
         open C_xform_item_exist;
         fetch C_xform_item_exist into L_from_item;
         ---
         if C_xform_item_exist%FOUND then
            L_xform_item_ind := 'Y';
         end if;
         ---
         close C_xform_item_exist;
         ---
         if I_intercompany then      -- 2nd-leg = ICT
            ---
            --if the item hasn't beeen transform, use it's unit retail at the original from location;
            --if the item is a transformed item, use it's unit_retail at to_location
            ---
            if L_xform_item_ind = 'Y' then  -- I_item is a transformed item
               ---
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                           L_from_item,
                                                           L_tsf_from_loc,
                                                           L_tsf_from_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
               ---
               O_from_unit_retail := L_unit_retail;
               ---
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                           I_item,
                                                           I_to_loc,
                                                           I_to_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
               ---
               O_to_unit_retail := L_unit_retail;
               ---
            else    -- I_item is an original item
               ---
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                           I_item,
                                                           L_tsf_from_loc,
                                                           L_tsf_from_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
               ---
               O_from_unit_retail := L_unit_retail;
               ---
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                           I_item,
                                                           I_to_loc,
                                                           I_to_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
               ---
               O_to_unit_retail := L_unit_retail;
               ---
            end if;
            ---
         else   -- 2nd-leg, Intra-company, both original/transformed item's retail use its retail at final to_loc
            ---
            -- if any of the two legs is Inter-company transfer, both legs' tsf_type = 'IC'

            if L_tsf_type = 'IC'
               or (L_tsf_type != 'IC' and I_from_loc = L_tsf_markdown_loc) then
               ---
               -- 2nd-leg of "1st-leg=ICT"
               -- or 2nd-leg of "both legs are intra-company transfer and sending loc takes the markdown

               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                           I_item,
                                                           I_to_loc,
                                                           I_to_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
               ---
               O_from_unit_retail := L_unit_retail;
               O_to_unit_retail := O_from_unit_retail;
               ---
            else
               -- L_tsf_type != 'IC' and receiving loc takes the markdown
               ---
               if L_xform_item_ind = 'N' then
                  L_from_item := I_item;
               end if;
               ---
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                           L_from_item,
                                                           L_tsf_from_loc,
                                                           L_tsf_from_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
               ---
               O_from_unit_retail := L_unit_retail;
               ---
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                           I_item,
                                                           I_to_loc,
                                                           I_to_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
               ---
               O_to_unit_retail := L_unit_retail;
               ---
            end if;
            ---
         end if;
         ---
      else
         O_error_message := SQL_LIB.CREATE_MSG('TSF_TYPE_NOT_SUP',
                                                L_program,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
      ---
   elsif I_to_finisher_ind = 'Y' then  -- 1st-leg of 2-legs transfer
      ---
      -- get final to_loc
      ---
      open C_tsf_child_info;
      fetch C_tsf_child_info into L_tsf_type,
                                  L_tsf_to_loc,
                                  L_tsf_to_loc_type;
      close C_tsf_child_info;
      ---
      if L_tsf_type != 'CO' then
         if ITEM_XFORM_PACK_SQL.GET_XFORM_TO_ITEM(O_error_message,
                                                  L_xform_item_exist,
                                                  L_to_item,
                                                  I_tsf_no,
                                                  I_item) = FALSE then
            return FALSE;
         end if;
         ---
         if I_intercompany then    -- 1st-leg = ICT
            ---
            if L_xform_item_exist = FALSE then
               L_to_item := I_item;
            end if;
            ---
            if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                        L_to_item,
                                                        L_tsf_to_loc,
                                                        L_tsf_to_loc_type,
                                                        L_av_cost,
                                                        L_unit_cost,
                                                        L_unit_retail,
                                                        L_selling_unit_retail,
                                                        L_selling_uom) = FALSE then
               return FALSE;
            end if;
            ---
            O_to_unit_retail := L_unit_retail;
            ---
            if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                        I_item,
                                                        I_from_loc,
                                                        I_from_loc_type,
                                                        L_av_cost,
                                                        L_unit_cost,
                                                        L_unit_retail,
                                                        L_selling_unit_retail,
                                                        L_selling_uom) = FALSE then
               return FALSE;
            end if;
            ---
            O_from_unit_retail := L_unit_retail;
            ---
         else   -- 1st-leg, Intra-company
            ---
            if L_tsf_type = 'IC' then  -- 1st-leg in a transfer with "2nd-leg=ICT"
               ---
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                           I_item,
                                                           I_from_loc,
                                                           I_from_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
               ---
               O_to_unit_retail := L_unit_retail;
               O_from_unit_retail := O_to_unit_retail;
               ---
            else  -- 1st-leg of a transfer with "both legs are intra-company"
               ---
               -- sending loc takes the markdown
               if I_from_loc = L_tsf_markdown_loc then
                  ---
                  if L_xform_item_exist = FALSE then
                     L_to_item := I_item;
                  end if;
                  ---
                  if L_tsf_type = 'EG' and  L_tsf_to_loc_type = 'W' then -- Fetch the Virtual Warehouse
                     ---
                     if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                                L_phy_wh,
                                                L_tsf_to_loc) = FALSE then
                        return FALSE;
                     end if;
                     if L_phy_wh = TRUE then
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_vwh',
                                          NULL,
                                          NULL);
                        open C_vwh;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_vwh',
                                          NULL,
                                          NULL);
                        fetch C_vwh into L_vr_wh;
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_vwh',
                                          NULL,
                                          NULL);
                        close C_vwh;
                        if L_vr_wh is NOT NULL then
                           if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                                       L_to_item,
                                                                       L_vr_wh,
                                                                       L_tsf_to_loc_type,
                                                                       L_av_cost,
                                                                       L_unit_cost,
                                                                       L_unit_retail,
                                                                       L_selling_unit_retail,
                                                                       L_selling_uom) = FALSE then
                              return FALSE;
                           end if;
                        end if;
                     end if;
                  else
                     if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                                 L_to_item,
                                                                 L_tsf_to_loc,
                                                                 L_tsf_to_loc_type,
                                                                 L_av_cost,
                                                                 L_unit_cost,
                                                                 L_unit_retail,
                                                                 L_selling_unit_retail,
                                                                 L_selling_uom) = FALSE then
                        return FALSE;
                     end if;
                  end if;
                  ---
                  O_to_unit_retail := L_unit_retail;
                  ---
                  if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                              I_item,
                                                              I_from_loc,
                                                              I_from_loc_type,
                                                              L_av_cost,
                                                              L_unit_cost,
                                                              L_unit_retail,
                                                              L_selling_unit_retail,
                                                              L_selling_uom) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  O_from_unit_retail := L_unit_retail;
                  ---
               else    -- receiving loc takes the markdown
                  ---
                  if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                              I_item,
                                                              I_from_loc,
                                                              I_from_loc_type,
                                                              L_av_cost,
                                                              L_unit_cost,
                                                              L_unit_retail,
                                                              L_selling_unit_retail,
                                                              L_selling_uom) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  O_to_unit_retail := L_unit_retail;
                  O_from_unit_retail := O_to_unit_retail;
                  ---
               end if;
               ---
            end if;
            ---
         end if;
         ---
      else
         O_error_message := SQL_LIB.CREATE_MSG('TSF_TYPE_NOT_SUP',
                                                L_program,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
      ---
   else    -- single leg transfer
      ---
      if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                  I_item,
                                                  I_from_loc,
                                                  I_from_loc_type,
                                                  L_av_cost,
                                                  L_unit_cost,
                                                  L_unit_retail,
                                                  L_selling_unit_retail,
                                                  L_selling_uom) = FALSE then
         return FALSE;
      end if;
      ---
      O_from_unit_retail := L_unit_retail;
      ---
      if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                  I_item,
                                                  I_to_loc,
                                                  I_to_loc_type,
                                                  L_av_cost,
                                                  L_unit_cost,
                                                  L_unit_retail,
                                                  L_selling_unit_retail,
                                                  L_selling_uom) = FALSE then
         return FALSE;
      end if;
      ---
      O_to_unit_retail := L_unit_retail;
      ---
   end if;

   -- For orderable but non-sellable non-pack/non-transform items, unit_retails are not defined.
   -- But total_retail is still expected for TRAN_DATA records for transfers. Default unit_retails to 0.
   if O_from_unit_retail is NULL or O_to_unit_retail is NULL then
      if NOT ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                             L_item_rec,
                                             I_item) then
         return FALSE;
      end if;

      if (L_item_rec.sellable_ind = 'N' and
          L_item_rec.orderable_ind = 'Y' and
          L_item_rec.pack_ind = 'N' and
          L_item_rec.item_xform_ind = 'N') then

          O_from_unit_retail := NVL(O_from_unit_retail, 0);
          O_to_unit_retail := NVL(O_to_unit_retail, 0);
       end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GET_COSTS_AND_RETAILS',
                                            to_char(SQLCODE));
        return FALSE;
END GET_TSF_COSTS_RETAILS;
---------------------------------------------------------------------------------------------
FUNCTION WRITE_FINANCIALS(O_error_message       IN OUT VARCHAR2,
                          I_call_type           IN     VARCHAR2,
                          I_shipment            IN     shipment.shipment%TYPE,
                          I_distro_no           IN     shipsku.distro_no%TYPE,
                          I_tran_date           IN     period.vdate%TYPE,
                          I_item                IN     item_master.item%TYPE,
                          I_pack_no             IN     item_master.item%TYPE,
                          I_pct_in_pack         IN     NUMBER,
                          I_dept                IN     item_master.dept%TYPE,
                          I_class               IN     item_master.class%TYPE,
                          I_subclass            IN     item_master.subclass%TYPE,
                          I_qty                 IN     tsfdetail.tsf_qty%TYPE,
                          I_from_loc            IN     item_loc.loc%TYPE,
                          I_from_loc_type       IN     item_loc.loc_type%TYPE,
                          I_from_finisher_ind   IN     wh.finisher_ind%TYPE,
                          I_to_loc              IN     item_loc.loc%TYPE,
                          I_to_loc_type         IN     item_loc.loc_type%TYPE,
                          I_to_finisher_ind     IN     wh.finisher_ind%TYPE,
                          I_profit_chrgs_to_loc IN     NUMBER,
                          I_exp_chrgs_to_loc    IN     NUMBER,
                          I_intercompany        IN     BOOLEAN,
                          I_ship_date           IN     shipment.ship_date%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_function              VARCHAR2(60) := 'STKLEDGR_SQL.WRITE_FINANCIALS';

   L_from_wac              item_loc_soh.av_cost%TYPE;
   L_tsf_alloc_unit_cost   item_loc_soh.av_cost%TYPE;

BEGIN

   if WRITE_FINANCIALS(O_error_message,
                       L_tsf_alloc_unit_cost, -- dummy
                       I_call_type,
                       I_shipment,
                       I_distro_no,
                       I_tran_date,
                       I_item,
                       I_pack_no,
                       I_pct_in_pack,
                       I_dept,
                       I_class,
                       I_subclass,
                       I_qty,
                       NULL,  -- weight_cuom
                       I_from_loc,
                       I_from_loc_type,
                       I_from_finisher_ind,
                       I_to_loc,
                       I_to_loc_type,
                       I_to_finisher_ind,
                       L_from_wac,
                       I_profit_chrgs_to_loc,
                       I_exp_chrgs_to_loc,
                       I_intercompany,
                       NULL,
                       NULL,
                       I_ship_date) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKLEDGR_SQL.WRITE_FINANCIALS',
                                            to_char(SQLCODE));
   return FALSE;
END WRITE_FINANCIALS;
---------------------------------------------------------------------------------------------
FUNCTION WRITE_FINANCIALS(O_error_message         IN OUT   VARCHAR2,
                          O_tsf_alloc_unit_cost   IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                          I_call_type             IN       VARCHAR2,
                          I_shipment              IN       SHIPMENT.SHIPMENT%TYPE,
                          I_distro_no             IN       SHIPSKU.DISTRO_NO%TYPE,
                          I_tran_date             IN       PERIOD.VDATE%TYPE,
                          I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                          I_pack_no               IN       ITEM_MASTER.ITEM%TYPE,
                          I_pct_in_pack           IN       NUMBER,
                          I_dept                  IN       ITEM_MASTER.DEPT%TYPE,
                          I_class                 IN       ITEM_MASTER.CLASS%TYPE,
                          I_subclass              IN       ITEM_MASTER.SUBCLASS%TYPE,
                          I_qty                   IN       TSFDETAIL.TSF_QTY%TYPE,
                          I_weight_cuom           IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                          I_from_loc              IN       ITEM_LOC.LOC%TYPE,
                          I_from_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                          I_from_finisher_ind     IN       WH.FINISHER_IND%TYPE,
                          I_to_loc                IN       ITEM_LOC.LOC%TYPE,
                          I_to_loc_type           IN       ITEM_LOC.LOC_TYPE%TYPE,
                          I_to_finisher_ind       IN       WH.FINISHER_IND%TYPE,
                          I_from_wac              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                          I_profit_chrgs_to_loc   IN       NUMBER,
                          I_exp_chrgs_to_loc      IN       NUMBER,
                          I_intercompany          IN       BOOLEAN,
                          I_extended_base_cost    IN       ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE DEFAULT NULL,
                          I_base_cost             IN       ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE DEFAULT NULL,
                          I_ship_date             IN       shipment.ship_date%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_function              VARCHAR2(60) := 'STKLEDGR_SQL.WRITE_FINANCIALS';

   L_tran_code            tran_data.tran_code%TYPE        := NULL;
   L_gl_ref_no            tran_data.gl_ref_no%TYPE        := NULL;
   L_tran_date            period.vdate%TYPE               := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');
   L_distro_no            shipsku.distro_no%TYPE          := I_distro_no;

   L_extended_cost        tran_data.total_cost%TYPE       := 0;
   L_extended_retail      tran_data.total_retail%TYPE     := 0;
   L_to_extended_retail   tran_data.total_retail%TYPE     := 0;
   L_profit_chrgs_to_loc  NUMBER                          := 0;
   L_exp_chrgs_to_loc     NUMBER                          := 0;

   L_unit_retail          item_loc.unit_retail%TYPE       := NULL;
   L_to_loc_retail        item_loc.unit_retail%TYPE       := NULL;
   -- dummy values: not used
   L_av_cost              item_loc_soh.unit_cost%TYPE;
   L_selling_unit_retail  item_loc.unit_retail%TYPE;
   L_selling_uom          item_loc.selling_uom%TYPE;
   L_unit_cost            item_loc_soh.unit_cost%TYPE;
   L_from_wac             item_loc_soh.av_cost%type;
   L_current_from_wac     item_loc_soh.av_cost%type;

   -- for calculating intercompany markup/markdown
   L_tsf_price            tsfdetail.tsf_price%TYPE       := NULL;
   L_markup_markdown      tran_data.total_retail%TYPE    := 0;

   -- for calculating intracompany cost variance
   L_tsf_cost             tsfdetail.tsf_cost%TYPE        := NULL;

   -- for writing tran data code 65 restocking fee, only for 'RV' type of transfers
   L_restock_pct           TSFDETAIL.RESTOCK_PCT%TYPE     := NULL;
   L_extended_cost_restock TRAN_DATA.TOTAL_COST%TYPE      := 0;  -- cost minus restocking fee

   -- for updating TRAN_DATA.UNITS
   L_qty                   TSFDETAIL.TSF_QTY%TYPE := NULL;
   L_cuom                  ITEM_SUPP_COUNTRY.COST_UOM%TYPE := NULL;
   --- for SVAT
   get_system_option_row         SYSTEM_OPTIONS%ROWTYPE;
   L_stkldgr_vat_incl_retl_ind   SYSTEM_OPTIONS.STKLDGR_VAT_INCL_RETL_IND%TYPE;
   L_class_lvl_vat_ind           SYSTEM_OPTIONS.CLASS_LEVEL_VAT_IND%TYPE;
   L_class_vat_ind               CLASS.CLASS_VAT_IND%TYPE;
   L_tax_rate_retail             VAT_CODE_RATES.VAT_RATE%TYPE;
   LP_tax_rate_retail            VAT_CODE_RATES.VAT_RATE%TYPE;
   LP_tax_code_retail            VAT_CODES.VAT_CODE%TYPE;
   L_tax_code_retail             VAT_CODES.VAT_CODE%TYPE;
   L_tax_rate_cost               VAT_CODE_RATES.VAT_RATE%TYPE;
   L_tax_code_cost               VAT_CODES.VAT_CODE%TYPE;
   L_exem_ind                    VARCHAR2(1)                     :='N';
   LP_vat_amount_rtl             tran_data.total_retail%TYPE     := 0;
   L_extended_retail_src         tran_data.total_retail%TYPE     := 0;
   L_extended_cost_src           tran_data.total_retail%TYPE     := 0;
   L_vat_amount_rtl              tran_data.total_retail%TYPE     := 0;
   L_l10n_fin_rec                L10N_FIN_REC := L10N_FIN_REC();
   L_tran_data_rec               TRAN_DATA_REC := TRAN_DATA_REC();
   L_tran_loc                    TRAN_DATA.LOCATION%TYPE;
   L_tran_loc_type               TRAN_DATA.LOC_TYPE%TYPE;
   L_total_cost                  TRAN_DATA.TOTAL_COST%TYPE;
   L_total_retail                TRAN_DATA.TOTAL_RETAIL%TYPE;
   L_ref_no_1                    TRAN_DATA.REF_NO_1%TYPE;
   L_ref_no_2                    TRAN_DATA.REF_NO_2%TYPE := NULL;
   L_ref_pack_no                 TRAN_DATA.REF_PACK_NO%TYPE;

   cursor C_TSF_BK_SEQ is
      select book_tsf_sequence.nextval
        from dual;

   cursor C_GET_TSF_PRICE_FIRST_LEG(IC_item IN item_master.item%TYPE) is
      select tsf_price, restock_pct
        from tsfdetail
       where tsf_no       = I_distro_no
         and item         = IC_item;

   cursor C_GET_TSF_PRICE_SECOND_LEG is
      select tsf_avg_price
        from tsf_item_cost
       where tsf_no = I_distro_no
         and item   = I_item;

   cursor C_GET_FINISHING_COSTS is
      select tiwc.avg_unit_cost,
             tiwc.activity_id,
             w.cost_type
        from tsf_item_wo_cost tiwc,
             tsf_item_cost tic,
             wo_activity w
       where tic.item              = I_item
         and tic.tsf_no            = L_distro_no
         and tiwc.activity_id      = w.activity_id
         and tiwc.tsf_item_cost_id = tic.tsf_item_cost_id
         and tiwc.avg_unit_cost != 0;

   cursor C_GET_TSF_COST(IC_item IN item_master.item%TYPE) is
      select tsf_cost, restock_pct
        from tsfdetail
       where tsf_no       = I_distro_no
         and item         = IC_item;

   cursor C_CLASS_VAT_IND(L_dept IN deps.dept%TYPE,L_class IN class.class%TYPE) is
      select class_vat_ind
        from class
       where dept  = L_dept
         and class = L_class;

BEGIN

   if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                 L_current_from_wac,
                                 I_item,
                                 I_dept,
                                 I_class,
                                 I_subclass,
                                 I_from_loc,
                                 I_from_loc_type,
                                 I_tran_date,
                                 I_to_loc,
                                 I_to_loc_type) = FALSE then
      return FALSE;
   end if;

   if I_from_wac is NULL then
      L_from_wac := L_current_from_wac;
   else
      L_from_wac := I_from_wac;
   end if;
   ---
   -- O_tsf_alloc_unit_cost returns the unit_cost used for from loc's
   -- tran_data write. Shipsku.unit_cost should be based off the same
   -- cost and in from loc's currency.
   ---
   -- Get the transfer unit retail.
   if GET_TSF_COSTS_RETAILS(O_error_message,
                            L_av_cost,
                            L_unit_retail,
                            L_to_loc_retail,
                            I_distro_no,
                            I_intercompany,
                            I_from_finisher_ind,
                            I_to_finisher_ind,
                            I_item,
                            I_from_loc,
                            I_from_loc_type,
                            I_to_loc,
                            I_to_loc_type) = FALSE then
      return FALSE;
   end if;
   ---
   -- For book transfers, if no distro_no(ie. tsf_no) is passed in, assume doing
   -- automatic book transfer and need to get next book_tsf_sequence number
   -- for ref_no_1 parameter to stkledgr_sql.tran_data_insert()
   -- otherwise, assume doing manual book transfer and pass tsf_no for
   -- ref_no_1 parameter to stkledgr_sql.tran_data_insert().
   ---
   if I_distro_no is NULL and I_call_type = 'B' then
      SQL_LIB.SET_MARK('NXTVAL',
                       'C_TSF_BK_SEQ',
                       NULL,
                       'selecting book_tsf_sequence.nextval from dual');

      open C_TSF_BK_SEQ;
      fetch C_TSF_BK_SEQ into L_distro_no;
      close C_TSF_BK_SEQ;
   end if;
   ---
   if I_tran_date is NULL then
      L_tran_date := GET_VDATE;
   end if;
   ---
   if I_weight_cuom is NULL then
      -- for a non-simple pack catch weight item, L_qty is the same as I_qty
      L_qty := I_qty;
      L_extended_cost    := L_qty * L_from_wac;
      L_extended_retail  := L_qty * L_unit_retail;
   else
      -- for a simple pack catch weight item, total cost should be based on the actual weight

      -- I_weight_cuom is expressed in the pack's catch_weight_uom. Get the CW UOM corresponding to I_weight_cuom.
      if NOT ITEM_ATTRIB_SQL.GET_CATCH_WEIGHT_UOM(O_error_message,
                                                  L_cuom,
                                                  I_pack_no) then
         return FALSE;
      end if;
      ---
      -- For a simple pack catch weight item, there are two scenarioes:
      -- 1) the component item's standard UOM is weight, then TRAN_DATA.UNITS should be updated with
      -- the actual weight converted to component item's SUOM.
      -- 2) the component item's standard UOM is Eaches, then TRAN_DATA.UNITS should be updated with
      -- I_qty, which is based on v_packsku_qty.
      ---
      if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                               L_qty,   -- output qty
                                               I_item,
                                               I_qty,   -- input qty
                                               I_weight_cuom,
                                               L_cuom) = FALSE then
         return FALSE;
      end if;

      -- calculate total cost based on weight
      if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                              L_extended_cost,
                                              I_pack_no,
                                              L_from_wac,  -- component from loc wac
                                              I_weight_cuom,
                                              L_qty) then
         return FALSE;
      end if;
      -- calculate from loc total retail based on weight
      if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_RETAIL(O_error_message,
                                                I_item,
                                                L_qty,
                                                L_unit_retail,
                                                I_from_loc,
                                                I_from_loc_type,
                                                I_weight_cuom,
                                                L_cuom,
                                                L_extended_retail,
                                                TRUE,  -- post weight variance
                                                FALSE) then  -- outbound shipment
         return FALSE;
      end if;

       -- calculate to loc total retail based on weight
      if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_RETAIL(O_error_message,
                                                I_item,
                                                L_qty,
                                                L_to_loc_retail,
                                                I_to_loc,
                                                I_to_loc_type,
                                                I_weight_cuom,
                                                L_cuom,
                                                L_to_extended_retail) then
         return FALSE;
      end if;
      L_to_loc_retail := L_to_extended_retail / L_qty;
   end if;

   O_tsf_alloc_unit_cost := L_from_wac;

   L_profit_chrgs_to_loc := L_qty * I_profit_chrgs_to_loc;
   L_exp_chrgs_to_loc    := L_qty * I_exp_chrgs_to_loc;
   ---
   if I_intercompany then -- Intercompany transfer

      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               get_system_option_row) = FALSE then
         return FALSE;
      end if;
      L_stkldgr_vat_incl_retl_ind := get_system_option_row.STKLDGR_VAT_INCL_RETL_IND;
      L_class_lvl_vat_ind         := get_system_option_row.CLASS_LEVEL_VAT_IND;
      -- Use transfer price instead of unit_retail for tran code 38 (intercompany out)
      -- If the from_loc is not a finisher, any item transformation will not have
      -- occurred yet.  Therefore, need to get tsf_price from tsfdetail and, if the
      -- item is part of a pack, prorate the value.
      if I_from_finisher_ind = 'N' then
         if I_pack_no is NOT NULL then
            open C_GET_TSF_PRICE_FIRST_LEG(I_pack_no);
            fetch C_GET_TSF_PRICE_FIRST_LEG into L_tsf_price,
                                                 L_restock_pct;
            close C_GET_TSF_PRICE_FIRST_LEG;
            L_tsf_price := L_tsf_price * I_pct_in_pack;
         else
            open C_GET_TSF_PRICE_FIRST_LEG(I_item);
            fetch C_GET_TSF_PRICE_FIRST_LEG into L_tsf_price,
                                                 L_restock_pct;
            close C_GET_TSF_PRICE_FIRST_LEG;
         end if;
      else
         -- This is the second leg of a multi-legged transfer.  Items have been
         -- transformed and the transfer price has been averaged across all
         -- pack component and bulk items and written to tsf_item_cost.
         open C_GET_TSF_PRICE_SECOND_LEG;
         fetch C_GET_TSF_PRICE_SECOND_LEG into L_tsf_price;
         close C_GET_TSF_PRICE_SECOND_LEG;

         -- Values on tsf_item_cost are held in the currency of the final
         -- receiving location.  Convert this to the finisher's currency.
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             L_tsf_price,
                                             L_tsf_price,
                                             NULL,
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      --if transfer price is not found for all types of intercompany tsf/alloc then use av_cost
      if L_tsf_price is NULL then
         L_tsf_price := L_from_wac;
      end if;

      if I_weight_cuom is NULL then
         L_extended_retail := L_qty * L_tsf_price;
      else
         -- catch weight simple pack
         if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                                 L_extended_retail,
                                                 I_pack_no,
                                                 L_tsf_price,
                                                 I_weight_cuom,
                                                 L_qty) then
            return FALSE;
         end if;
      end if;

      if TAX_SQL.GET_TAX_RATE_INFO (O_error_message,
                                    L_tax_rate_retail,
                                    L_tax_code_retail,
                                    LP_exempt_ind,
                                    I_item,
                                    I_from_loc_type,
                                    I_from_loc,
                                    I_to_loc_type,
                                    I_to_loc,
                                    'R') = FALSE then
         return FALSE;
      end if;
      if (L_stkldgr_vat_incl_retl_ind = 'Y' and LP_exempt_ind = 'N') then
         LP_vat_amount_rtl     := L_extended_retail*(nvl(L_tax_rate_retail,0)/100);
         L_extended_retail     := L_extended_retail + LP_vat_amount_rtl;
      end if;
      LP_tax_rate_retail    := L_tax_rate_retail;
      LP_tax_code_retail    := L_tax_code_retail;
      L_extended_retail_src := L_extended_retail;
      L_extended_cost_src   := L_qty*L_tsf_price;

      LP_total_retail_38    := L_extended_retail;
      L_tran_code := 38; --intercompany out
      L_gl_ref_no := I_to_loc;
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             I_dept,
                                             I_class,
                                             I_subclass,
                                             I_from_loc,
                                             I_from_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,              -- adj_code
                                             L_qty,
                                             L_extended_cost,
                                             L_extended_retail, -- tsf_price
                                             L_to_loc_retail,
                                             L_distro_no,       -- ref_no_1
                                             I_shipment,        -- ref_no_2
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function,
                                             L_gl_ref_no,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_pack_no,
                                             NULL) = FALSE then
         return FALSE;
      end if;
      ---
      -- if L_restock_pct is not null, it is a 'RV' type of transfer, write restocking fee
      -- tran data record 65
      if L_restock_pct is NOT NULL and L_restock_pct > 0 then
         L_tran_code := 65;
         L_extended_cost_restock := L_restock_pct/100 * L_extended_retail;
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                I_item,
                                                I_dept,
                                                I_class,
                                                I_subclass,
                                                I_from_loc,
                                                I_from_loc_type,
                                                L_tran_date,
                                                L_tran_code,
                                                NULL,                    -- adj_code
                                                L_qty,                   -- units
                                                L_extended_cost_restock, -- restocking fee of retail (total_cost)
                                                NULL,                    -- tsf_price (total_retail)
                                                NULL,                    -- to_loc_unit_retail
                                                L_distro_no,             -- ref_no_1
                                                I_shipment,              -- ref_no_2
                                                NULL,                    -- tsf_source_location
                                                NULL,                    -- tsf_source_loc_type
                                                NULL,                    -- old_unit_retail
                                                NULL,                    -- new_unit_retail
                                                NULL,                    -- source_dept
                                                NULL,                    -- source_class
                                                NULL,                    -- source_subclass
                                                L_function,              -- pgm_name
                                                L_gl_ref_no,             -- gl_ref_no
                                                NULL,                    -- distro_type
                                                NULL,                    -- pack_ind
                                                NULL,                    -- sellable_ind
                                                NULL,                    -- orderable_ind
                                                NULL,                    -- pack_type
                                                NULL,                    -- vat_rate
                                                NULL,                    -- class_vat_ind
                                                NULL,                    -- ref_pack_no
                                                NULL,                    -- total_cost_excl_elc
                                                NULL,                    -- tax_value
                                                NULL,                    -- from_wac
                                                NULL,                    -- extended_base_cost
                                                NULL) = FALSE then       -- base_cost

            return FALSE;
         end if;
      end if;
      ---
      -- for intercompany in, cost = tsf_price (unit_retail in the intercompany out)
      -- so convert the value to the to_loc currency and load it into the av_cost variable
      L_tran_code := 37;
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          I_from_loc,
                                          I_from_loc_type,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          L_extended_cost_src,
                                          L_extended_cost,
                                          NULL,
                                          I_ship_date,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      open C_CLASS_VAT_IND(I_dept,I_class);
      fetch C_CLASS_VAT_IND into L_class_vat_ind;
      close C_CLASS_VAT_IND;
      ---
      L_extended_retail     := L_qty * L_to_loc_retail;
      L_gl_ref_no           := I_from_loc;
      ---
      if TAX_SQL.GET_TAX_RATE_INFO (O_error_message,
                                    L_tax_rate_retail,
                                    L_tax_code_retail,
                                    L_exem_ind,
                                    I_item,
                                    I_to_loc_type,
                                    I_to_loc,
                                    I_from_loc_type,
                                    I_from_loc,
                                    'R') = FALSE then
         return FALSE;
      end if;

      if L_stkldgr_vat_incl_retl_ind = 'Y' and L_class_vat_ind = 'N' and L_class_lvl_vat_ind = 'Y' and L_exem_ind = 'N' then
         L_extended_retail := L_extended_retail + L_extended_retail*(nvl(L_tax_rate_retail,0)/100);
      elsif ((L_stkldgr_vat_incl_retl_ind = 'N' and L_class_vat_ind = 'Y' and L_class_lvl_vat_ind='Y') or
            ( L_stkldgr_vat_incl_retl_ind = 'N' and L_class_lvl_vat_ind = 'N'))
            and L_exem_ind = 'N' then
         L_extended_retail := L_extended_retail/(1+(nvl(L_tax_rate_retail,0)/100));
      end if;

      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             I_dept,
                                             I_class,
                                             I_subclass,
                                             I_to_loc,
                                             I_to_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,              -- adj_code
                                             L_qty,
                                             L_extended_cost,
                                             L_extended_retail,
                                             L_to_loc_retail,
                                             L_distro_no,       -- ref_no_1
                                             I_shipment,        -- ref_no_2
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function,
                                             L_gl_ref_no,
                                             I_call_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_pack_no,
                                             NULL,
                                             NULL,
                                             L_from_wac,
                                             I_extended_base_cost,
                                             I_base_cost) = FALSE then
         return FALSE;
      end if;

      -- The check whether to write an intercompany markup/markdown.
      -- Both the tsf_price and unit_retail are in the from_loc currency,
      -- so there is not need to convert either of them.

      -- The markup/markdown compares the from_loc unit_retail excluding VAT
      -- to the tsf_price.  Therefore, check whether VAT is being used and,
      -- if so, remove it from the from_loc unit_retail

      -- Populate the system_options variable if it is not already populated
      if LP_system_options.default_tax_type is null then
         if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                  LP_system_options) = FALSE then
            return FALSE;
         end if;
      end if;

      L_extended_retail_src := L_extended_retail_src/L_qty;

      if ((L_stkldgr_vat_incl_retl_ind = 'Y' and L_class_vat_ind='Y' and L_class_lvl_vat_ind = 'Y') or
          ( L_stkldgr_vat_incl_retl_ind = 'Y' and L_class_lvl_vat_ind='N') or
          (L_stkldgr_vat_incl_retl_ind = 'N' and L_class_vat_ind='N' and L_class_lvl_vat_ind = 'Y')) then

         if L_extended_retail_src = L_unit_retail then
            L_markup_markdown := 0;
         elsif L_extended_retail_src < L_unit_retail then
            L_markup_markdown := (L_unit_retail - L_extended_retail_src) * L_qty;
            L_tran_code       := 18;
         elsif L_unit_retail < L_extended_retail_src then
            L_markup_markdown := (L_extended_retail_src - L_unit_retail) * L_qty;
            L_tran_code       := 17;
         end if;

      elsif (L_stkldgr_vat_incl_retl_ind = 'Y' and L_class_vat_ind='N' and L_class_lvl_vat_ind = 'Y') then
         LP_vat_amount_rtl := L_unit_retail*(nvl(LP_tax_rate_retail,0)/100);
         L_unit_retail     := L_unit_retail + LP_vat_amount_rtl;

         if L_extended_retail_src = L_unit_retail then
            L_markup_markdown := 0;
         elsif L_extended_retail_src < L_unit_retail then
            L_markup_markdown := (L_unit_retail - L_extended_retail_src) * L_qty;
            L_tran_code       := 18;
         elsif L_unit_retail < L_extended_retail_src then
            L_markup_markdown := (L_extended_retail_src - L_unit_retail) * L_qty;
            L_tran_code       := 17;
         end if;
      elsif ((L_stkldgr_vat_incl_retl_ind = 'N' and L_class_vat_ind='Y' and L_class_lvl_vat_ind = 'Y') or
              (L_stkldgr_vat_incl_retl_ind = 'N' and L_class_lvl_vat_ind = 'N')) then
         L_unit_retail := L_unit_retail/(1+(nvl(LP_tax_rate_retail,0)/100));
         if L_extended_retail_src = L_unit_retail then
            L_markup_markdown := 0;
         elsif L_extended_retail_src < L_unit_retail then
            L_markup_markdown := (L_unit_retail - L_extended_retail_src) * L_qty;
            L_tran_code       := 18;
         elsif L_unit_retail < L_extended_retail_src then
            L_markup_markdown := (L_extended_retail_src - L_unit_retail) * L_qty;
            L_tran_code       := 17;
         end if;
      end if;--L_stkldgr_vat_incl_retl_ind = 'Y' and L_class_vat_ind='Y'

      if L_markup_markdown != 0 then
         -- Since total_cost is in/out, can't simply use NULL.  Instead use
         --   L_unit_cost since it is otherwise not used.
         L_unit_cost := NULL;
         L_gl_ref_no := NULL;
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                I_item,
                                                I_dept,
                                                I_class,
                                                I_subclass,
                                                I_from_loc,
                                                I_from_loc_type,
                                                L_tran_date,
                                                L_tran_code,
                                                NULL,                    -- adj_code
                                                L_qty,                   -- units
                                                L_unit_cost,             -- NULL value (total_cost)
                                                L_markup_markdown,       -- total_retail
                                                L_to_loc_retail,         -- to_loc_unit_retail
                                                L_distro_no,             -- ref_no_1
                                                I_shipment,              -- ref_no_2
                                                NULL,                    -- tsf_source_location
                                                NULL,                    -- tsf_source_loc_type
                                                NULL,                    -- old_unit_retail
                                                NULL,                    -- new_unit_retail
                                                NULL,                    -- source_dept
                                                NULL,                    -- source_class
                                                NULL,                    -- source_subclass
                                                L_function,              -- pgm_name
                                                L_gl_ref_no,             -- gl_ref_no
                                                NULL,                    -- distro_type
                                                NULL,                    -- pack_ind
                                                NULL,                    -- sellable_ind
                                                NULL,                    -- orderable_ind
                                                NULL,                    -- pack_type
                                                NULL,                    -- vat_rate
                                                NULL,                    -- class_vat_ind
                                                NULL,                    -- ref_pack_no
                                                NULL,                    -- total_cost_excl_elc
                                                NULL,                    -- tax_value
                                                NULL,                    -- from_wac
                                                NULL,                    -- extended_base_cost
                                                NULL) = FALSE then       -- base_cost) = FALSE then
            return FALSE;
         end if;
      end if;
      if (LP_exempt_ind = 'N' and LP_tax_rate_retail is not null) then

         ---objects for L10N_FIN_REC
         L_l10n_fin_rec.procedure_key                    := 'POST_VAT';
         L_l10n_fin_rec.country_id                       := NULL;
         L_l10n_fin_rec.source_entity                    := 'LOC';
         L_l10n_fin_rec.item                             := I_item;
         L_l10n_fin_rec.dept                             := I_dept;
         L_l10n_fin_rec.class                            := I_class;
         L_l10n_fin_rec.subclass                         := I_subclass;

         L_tran_loc       := I_from_loc;
         L_tran_loc_type  := I_from_loc_type;
         L_tran_code      := 88;
         L_total_cost     := NULL;
         L_total_retail   := L_tsf_price*L_qty*(nvl(LP_tax_rate_retail,0)/100);
         L_ref_no_1       := L_distro_no;
         L_ref_no_2       := I_shipment;
         L_ref_pack_no    := NULL;
         L_gl_ref_no      := LP_tax_code_retail;

         L_l10n_fin_rec.source_id   := I_from_loc;

         L_tran_data_rec := TRAN_DATA_REC(I_item,
                                          I_dept,
                                          I_class,
                                          I_subclass,
                                          NULL,
                                          L_tran_loc_type,
                                          L_tran_loc,
                                          L_tran_date,
                                          L_tran_code,
                                          NULL,
                                          L_qty,
                                          L_total_cost,
                                          L_total_retail,
                                          L_ref_no_1,
                                          L_ref_no_2,
                                          L_gl_ref_no,
                                          NULL,
                                          NULL,
                                          L_function,
                                          NULL,
                                          NULL,
                                          NULL,
                                          SYSDATE,
                                          L_ref_pack_no,
                                          NULL);

         L_l10n_fin_rec.tran_data   := L_tran_data_rec;
         ---
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec)= FALSE then
            return FALSE;
         end if;

         if TAX_SQL.GET_TAX_RATE_INFO (O_error_message,
                                       L_tax_rate_cost,
                                       L_tax_code_cost,
                                       L_exem_ind,
                                       I_item,
                                       I_to_loc_type,
                                       I_to_loc,
                                       I_from_loc_type,
                                       I_from_loc,
                                       'C') = FALSE then
             return FALSE;
         end if;
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             L_tsf_price,
                                             L_tsf_price,
                                             NULL,
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         L_tran_loc       := I_to_loc;
         L_tran_loc_type  := I_to_loc_type;
         L_tran_code      := 87;
         L_total_cost     := L_tsf_price*L_qty*(nvl(L_tax_rate_cost,0)/100);
         L_total_retail   := NULL;
         L_ref_no_1       := L_distro_no;
         L_ref_no_2       := I_shipment;
         L_ref_pack_no    := NULL;
         L_gl_ref_no      := L_tax_code_cost;

         L_l10n_fin_rec.source_id  := I_to_loc;

         L_tran_data_rec := TRAN_DATA_REC(I_item,
                                          I_dept,
                                          I_class,
                                          I_subclass,
                                          NULL,
                                          L_tran_loc_type,
                                          L_tran_loc,
                                          L_tran_date,
                                          L_tran_code,
                                          NULL,
                                          L_qty,
                                          L_total_cost,
                                          L_total_retail,
                                          L_ref_no_1,
                                          L_ref_no_2,
                                          L_gl_ref_no,
                                          NULL,
                                          NULL,
                                          L_function,
                                          NULL,
                                          NULL,
                                          NULL,
                                          SYSDATE,
                                          L_ref_pack_no,
                                          NULL);

         L_l10n_fin_rec.tran_data   := L_tran_data_rec;
         ---
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec)= FALSE then
            return FALSE;
         end if;
      end if; -- L_stkldgr_vat_incl_retl_ind = 'Y'

   else  -- intra-company
      ---
      if I_call_type = 'B' then  -- Book transfer
         ---
         L_tran_code := 31;
         L_gl_ref_no := NULL;
      else -- regular transfer, allocation, stock order
         ---
         L_tran_code := 30;
         L_gl_ref_no := NULL;
      end if;

      -- If transfer cost is defined, write cost variance between from loc's WAC
      -- and transfer cost. For a pack, tsf_cost is for the pack. Prorate to the component.
      -- This applies to both regular transfer and book transfer.
      -- Book transfer will not have shipment, i.e. NULL I_shipment for I_ref_no_2.

      if I_pack_no is NOT NULL then
         open C_GET_TSF_COST(I_pack_no);
         fetch C_GET_TSF_COST into L_tsf_cost,
                                   L_restock_pct;
         close C_GET_TSF_COST;
         L_tsf_cost := L_tsf_cost * I_pct_in_pack;
      else
         open C_GET_TSF_COST(I_item);
         fetch C_GET_TSF_COST into L_tsf_cost,
                                   L_restock_pct;
         close C_GET_TSF_COST;
      end if;

      if L_tsf_cost is NOT NULL then
         if I_weight_cuom is NULL then
            L_extended_cost := L_tsf_cost * L_qty;
         else
            -- catch weight simple pack
            if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                                    L_extended_cost,
                                                    I_pack_no,
                                                    L_tsf_cost,
                                                    I_weight_cuom,
                                                    L_qty) then
               return FALSE;
            end if;
         end if;
         O_tsf_alloc_unit_cost := L_tsf_cost;
      end if;

      -- write standard transfer records
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             I_dept,
                                             I_class,
                                             I_subclass,
                                             I_to_loc,
                                             I_to_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,
                                             L_qty,
                                             L_extended_cost,
                                             L_extended_retail,
                                             L_to_loc_retail,
                                             L_distro_no,        --I_ref_no_1
                                             I_shipment,         --I_ref_no_2
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function,
                                             L_gl_ref_no,
                                             I_call_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_pack_no,
                                             NULL,
                                             NULL,
                                             L_from_wac,
                                             I_extended_base_cost,
                                             I_base_cost,
                                             I_ship_date) = FALSE then
         return FALSE;
      end if;
      ---
      -- if L_restock_pct is not null, it is a 'RV' type of transfer, write restocking fee
      -- tran data record 65
      if L_restock_pct is NOT NULL and L_restock_pct > 0 then
         L_tran_code := 65;
         L_extended_cost_restock := L_restock_pct/100 * L_extended_cost;
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                I_item,
                                                I_dept,
                                                I_class,
                                                I_subclass,
                                                I_from_loc,
                                                I_from_loc_type,
                                                L_tran_date,
                                                L_tran_code,
                                                NULL,                    -- adj_code
                                                L_qty,                   -- units
                                                L_extended_cost_restock, -- restocking fee of cost (total_cost)
                                                NULL,                    -- tsf_price (total_retail)
                                                NULL,                    -- to_loc_unit_retail
                                                L_distro_no,             -- ref_no_1
                                                I_shipment,              -- ref_no_2
                                                NULL,                    -- tsf_source_location
                                                NULL,                    -- tsf_source_loc_type
                                                NULL,                    -- old_unit_retail
                                                NULL,                    -- new_unit_retail
                                                NULL,                    -- source_dept
                                                NULL,                    -- source_class
                                                NULL,                    -- source_subclass
                                                L_function,              -- pgm_name
                                                L_gl_ref_no,             -- gl_ref_no
                                                NULL,                    -- distro_type
                                                NULL,                    -- pack_ind
                                                NULL,                    -- sellable_ind
                                                NULL,                    -- orderable_ind
                                                NULL,                    -- pack_type
                                                NULL,                    -- vat_rate
                                                NULL,                    -- class_vat_ind
                                                NULL,                    -- ref_pack_no
                                                NULL,                    -- total_cost_excl_elc
                                                NULL,                    -- tax_value
                                                NULL,                    -- from_wac
                                                NULL,                    -- extended_base_cost
                                                NULL) = FALSE then       -- base_cost
            return FALSE;
         end if;
      end if;
      ---
      --When WRITE_FINANCIALS is called for stock reconciliation, I_from_wac contains shipsku.unit_cost,
      --which holds either tsf_cost or from-loc's wac at the time of shipment. Since shipsku.unit_cost is
      --used to write tran_data 30/32 during stock reconciliation, cost variance is posted between
      --shipsku.unit_cost (i.e. I_from_wac) and from-loc's current wac (L_current_from_wac).

      if L_tsf_cost is NOT NULL then
         if NOT POST_COST_VARIANCE(O_error_message,
                                   I_item,
                                   I_pack_no,
                                   I_dept,
                                   I_class,
                                   I_subclass,
                                   I_from_loc,
                                   I_from_loc_type,
                                   L_tsf_cost,  -- in from loc currency
                                   L_current_from_wac,
                                   L_qty,
                                   I_weight_cuom,
                                   L_distro_no,
                                   I_shipment,
                                   L_tran_date) then
            return FALSE;
         end if;
      end if;
   end if; -- inter/intra company transfer

   -- write up charge records
   if L_profit_chrgs_to_loc != 0 and
      L_profit_chrgs_to_loc IS NOT NULL then
      L_tran_code := 28;
      L_gl_ref_no := NULL;
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             I_dept,
                                             I_class,
                                             I_subclass,
                                             I_to_loc,
                                             I_to_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,
                                             L_qty,
                                             L_profit_chrgs_to_loc,
                                             0,
                                             L_to_loc_retail,
                                             L_distro_no,
                                             I_shipment,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function,
                                             L_gl_ref_no) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_exp_chrgs_to_loc != 0 and
      L_exp_chrgs_to_loc IS NOT NULL then
      L_tran_code := 29;
      L_gl_ref_no := NULL;
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             I_dept,
                                             I_class,
                                             I_subclass,
                                             I_to_loc,
                                             I_to_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,
                                             L_qty,
                                             L_exp_chrgs_to_loc,
                                             0,
                                             L_to_loc_retail,
                                             L_distro_no,
                                             I_shipment,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function,
                                             L_gl_ref_no) = FALSE then
         return FALSE;
      end if;
   end if;

   -- If the from_loc is a finisher, this is the second leg of a multi-legged transfer.
   --   Write finishing costs to tran_data for the item.
   if I_from_finisher_ind = 'Y' then
      for rec in C_GET_FINISHING_COSTS loop
         L_extended_cost    := rec.avg_unit_cost * L_qty;
         L_gl_ref_no        := rec.activity_id;

         if rec.cost_type = 'U' then
            L_tran_code := 63;
         elsif rec.cost_type = 'P' then
            L_tran_code := 64;
         end if;

         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                I_item,
                                                I_dept,
                                                I_class,
                                                I_subclass,
                                                I_to_loc,
                                                I_to_loc_type,
                                                L_tran_date,
                                                L_tran_code,
                                                NULL,                    -- adj_code
                                                L_qty,                   -- units
                                                L_extended_cost,         -- total_cost
                                                NULL,                    -- total_retail
                                                L_to_loc_retail,         -- to_loc_unit_retail
                                                L_distro_no,             -- ref_no_1
                                                I_shipment,              -- ref_no_2
                                                NULL,                    -- tsf_source_location
                                                NULL,                    -- tsf_source_loc_type
                                                NULL,                    -- old_unit_retail
                                                NULL,                    -- new_unit_retail
                                                NULL,                    -- source_dept
                                                NULL,                    -- sourece_class
                                                NULL,                    -- source_subclass
                                                L_function,              -- pgm_name
                                                L_gl_ref_no,             -- gl_ref_no
                                                NULL,                    -- distro_type
                                                NULL,                    -- pack_ind
                                                NULL,                    -- sellable_ind
                                                NULL,                    -- orderable_ind
                                                NULL,                    -- pack_type
                                                NULL,                    -- vat_rate
                                                NULL,                    -- class_vat_ind
                                                NULL,                    -- ref_pack_no
                                                NULL,                    -- total_cost_excl_elc
                                                NULL,                    -- tax_value
                                                NULL,                    -- from_wac
                                                NULL,                    -- extended_base_cost
                                                NULL) = FALSE then       -- base_cost
            return FALSE;
         end if;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
    if C_CLASS_VAT_IND%ISOPEN then
       close C_CLASS_VAT_IND;
    end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STKLEDGR_SQL.WRITE_FINANCIALS',
                                            to_char(SQLCODE));
   return FALSE;
END WRITE_FINANCIALS;
-------------------------------------------------------------------------------------------
FUNCTION PROCESS_WF_WRITE_FINANCIALS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_tsf_alloc_unit_cost OUT    TRAN_DATA.TOTAL_COST%TYPE,
                                     O_unit_retail         OUT    TRAN_DATA.TOTAL_RETAIL%TYPE,
                                     I_distro_no           IN     SHIPSKU.DISTRO_NO%TYPE,
                                     I_distro_type         IN     SHIPSKU.DISTRO_TYPE%TYPE,
                                     I_tran_date           IN     PERIOD.VDATE%TYPE,
                                     I_item                IN     ITEM_MASTER.ITEM%TYPE,
                                     I_pack_no             IN     ITEM_MASTER.ITEM%TYPE,
                                     I_pct_in_pack         IN     NUMBER,
                                     I_dept                IN     ITEM_MASTER.DEPT%TYPE,
                                     I_class               IN     ITEM_MASTER.CLASS%TYPE,
                                     I_subclass            IN     ITEM_MASTER.SUBCLASS%TYPE,
                                     I_qty                 IN     TSFDETAIL.TSF_QTY%TYPE,
                                     I_from_loc            IN     ITEM_LOC.LOC%TYPE,
                                     I_from_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                                     I_to_loc              IN     ITEM_LOC.LOC%TYPE,
                                     I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                                     I_weight_cuom         IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                                     I_rma_no              IN     WF_RETURN_HEAD.RMA_NO%TYPE,
                                     I_shipment            IN     SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                                     I_carton              IN     SHIPSKU.CARTON%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_function                     VARCHAR2(60)                        := 'STKLEDGR_SQL.PROCESS_WF_WRITE_FINANCIALS';
   L_tran_code                    TRAN_DATA.TRAN_CODE%TYPE            := NULL;
   L_tran_date                    PERIOD.VDATE%TYPE                   := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');

   L_system_options               SYSTEM_OPTIONS%ROWTYPE;
   L_extended_cost                TRAN_DATA.TOTAL_COST%TYPE           := 0;
   L_sale_price                   ITEM_LOC.UNIT_RETAIL%TYPE           := 0;
   L_sale_price_l_curr            ITEM_LOC.UNIT_RETAIL%TYPE           := 0;
   L_from_loc_retail_l_curr       ITEM_LOC.UNIT_RETAIL%TYPE           := 0;
   L_tot_customer_cost            WF_ORDER_DETAIL.CUSTOMER_COST%TYPE  := 0;
   L_tot_comp_loc_retail_vat_adj  ITEM_LOC.UNIT_RETAIL%TYPE           := 0;
   L_to_loc_retail_l_curr         ITEM_LOC.UNIT_RETAIL%TYPE           := 0;
   L_to_loc_retail_l_curr_vat_ex  ITEM_LOC.UNIT_RETAIL%TYPE           := 0;
   L_tot_to_loc_cost_l_curr       ITEM_LOC_SOH.UNIT_COST%TYPE         := 0;
   L_tot_from_loc_cost_l_curr     ITEM_LOC_SOH.UNIT_COST%TYPE         := 0;
   L_from_loc_cost_l_curr         ITEM_LOC_SOH.UNIT_COST%TYPE         := 0;
   L_total_retail                 ITEM_LOC.UNIT_RETAIL%TYPE           := 0;
   L_company_loc_wac              ITEM_LOC_SOH.UNIT_COST%TYPE         := 0;
   L_return_unit_cost             ITEM_LOC_SOH.UNIT_COST%TYPE         := 0;
   L_return_unit_cost_l_curr      ITEM_LOC_SOH.UNIT_COST%TYPE         := 0;
   L_wf_return_cost               ITEM_LOC_SOH.UNIT_COST%TYPE         := 0;
   L_restock_cost                 ITEM_LOC_SOH.UNIT_COST%TYPE         := 0;
   L_restock_cost_l_curr          ITEM_LOC_SOH.UNIT_COST%TYPE         := 0;
   L_restock_cost_f_curr          ITEM_LOC_SOH.UNIT_COST%TYPE         := 0;
   L_f_store_wac                  ITEM_LOC_SOH.UNIT_COST%TYPE         := 0;
   -- dummy variables
   L_null_cost                    TRAN_DATA.TOTAL_COST%TYPE           := NULL;
   L_std_av_cost                  ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_selling_unit_retail          ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_uom                  ITEM_LOC.SELLING_UOM%TYPE;
   L_unit_cost                    ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_std_cost_to_loc              ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_f_std_av_cost                ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_f_selling_unit_retail        ITEM_LOC.UNIT_RETAIL%TYPE;
   L_f_selling_uom                ITEM_LOC.SELLING_UOM%TYPE;
   L_f_unit_cost                  ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_f_std_cost_to_loc            ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_tax_rate_retail              VAT_CODE_RATES.VAT_RATE%TYPE       := NULL;
   L_tax_rate_cost                VAT_CODE_RATES.VAT_RATE%TYPE       := NULL;
   L_tax_code_retail              VAT_CODE_RATES.VAT_CODE%TYPE       := NULL;
   L_tax_code_cost                VAT_CODE_RATES.VAT_CODE%TYPE       := NULL;
   L_inv_status                   SHIPSKU.INV_STATUS%TYPE;
   -- for updating TRAN_DATA.UNITS
   L_qty                          TSFDETAIL.TSF_QTY%TYPE             := 0;
   L_cuom                         ITEM_SUPP_COUNTRY.COST_UOM%TYPE    := NULL;

   L_store_type                   STORE.STORE_TYPE%TYPE;
   L_stockholding_ind             STORE.STOCKHOLDING_IND%TYPE;
   L_f_store_total_retail         ITEM_LOC.UNIT_RETAIL%TYPE;
   L_f_store_retail               ITEM_LOC.UNIT_RETAIL%TYPE;
   L_reason_desc                  INV_ADJ_REASON_TL.REASON_DESC%TYPE;
   L_cogs_ind                     INV_ADJ_REASON.COGS_IND%TYPE;
   L_reason                       INV_ADJ_REASON.REASON%TYPE;
   L_restock_type                 WF_RETURN_DETAIL.RESTOCK_TYPE%TYPE := NULL;
   L_destroy_onsite               BOOLEAN := FALSE;

   L_franchise_type               VARCHAR2(2)                        := NULL;
   L_wf_order_no                  TSFHEAD.TSF_NO%TYPE                := NULL;
   L_rma_no                       TSFHEAD.TSF_NO%TYPE                := NULL;
   L_class_vat_ind                CLASS.CLASS_VAT_IND%TYPE           := NULL;

   L_return_cost                  WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE;
   L_total_variance               TRAN_DATA.TOTAL_COST%TYPE;
   L_exchange_type                CURRENCY_RATES.EXCHANGE_TYPE%TYPE;
   L_from_loc_currency            CURRENCIES.CURRENCY_CODE%TYPE;
   L_to_loc_currency              CURRENCIES.CURRENCY_CODE%TYPE;
   L_f_store_currency             CURRENCIES.CURRENCY_CODE%TYPE;
   L_order_currency               CURRENCIES.CURRENCY_CODE%TYPE;
   L_return_currency              CURRENCIES.CURRENCY_CODE%TYPE;
   L_cost_retail_ind              VARCHAR2(1);
   L_loc_type                     WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   L_inv_parm                     VARCHAR2(30)                      := NULL;
   L_dummy_cost                   ITEM_LOC_SOH.UNIT_COST%TYPE       := NULL;
   L_pgm_name                     VARCHAR2(60)                      := 'STKLEDGR_SQL.WF_WRITE_FINANCIALS';
   L_return_unit_cost_vat         ITEM_LOC_SOH.UNIT_COST%TYPE       := 0;

   cursor C_STORE_TYPE(IC_store IN store.store%TYPE) is
      select store_type,
             stockholding_ind,
             currency_code
        from store
       where store = IC_store;

   cursor C_GET_CURRENCY(I_loc ITEM_LOC.LOC%TYPE) is
      select currency_code
        from wh
       where wh = I_loc
      UNION
      select currency_code
        from store
       where store = I_loc;

   cursor C_GET_SHIPSKU_COST is
      select unit_cost * NVL(I_pct_in_pack, 1),
             NVL(inv_status, -1) inv_status
        from shipsku
       where shipment  = I_shipment
         and item      = NVL(I_pack_no, I_item)
         and distro_no = I_distro_no
         and distro_type = I_distro_type
         and NVL(carton, '-*-') = NVL(I_carton, NVL(carton, '-*-'))
         and rownum = 1
       order by inv_status;

BEGIN
   if I_dept is NULL then
      L_inv_parm := 'I_dept';
   elsif I_class is NULL then
      L_inv_parm := 'I_class';
   elsif I_subclass is NULL then
      L_inv_parm := 'I_subclass';
   end if;
   -- assigning package level variables to NULL to cleanup previous run values.
   LP_from_loc               := NULL;
   LP_to_loc                 := NULL;
   LP_vat_amount_rtl         := NULL;
   LP_vat_amount_cost        := NULL;
   LP_vat_amount_cost_f_curr := NULL;

   -- Setup package level variables to be used in RECORD_INSERTS
   LP_from_loc := I_from_loc;
   LP_to_loc   := I_to_loc;

   if L_inv_parm is NOT NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_pgm_name,
                                             NULL);
       return FALSE;
   end if;

   if I_tran_date is NULL then
      L_tran_date := GET_VDATE;
   end if;
   ---if this function is called by transfer/allocation shipment, the distro_no will be populated.
   -- For destroy at site return (possible only from non stockholding franchise store), no transfer
   -- is created (distro number will be null) but RMA_NO will be populated.
   if I_distro_no is NULL and I_rma_no is NOT NULL then
      open C_STORE_TYPE(I_from_loc);
      fetch C_STORE_TYPE into L_store_type,
                              L_stockholding_ind,
                              L_f_store_currency;
      close C_STORE_TYPE;

      if L_store_type = 'F' and L_stockholding_ind = 'N' then
         L_franchise_type := 'FR';
         L_destroy_onsite := TRUE;
         L_rma_no := I_rma_no;
      elsif L_store_type = 'F' and L_stockholding_ind = 'Y' then
         -- Destroy at site is only supported for Non stockholding franchise store
         O_error_message := SQL_LIB.CREATE_MSG('INV_DEST_SITE_F_STORE', 'I_from_loc', NULL, NULL);
         return FALSE;
      else
         -- Not a valid destroy at site transaction and hence distro number is required to proceed.
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_distro_no', L_function, NULL);
         return FALSE;
      end if;
   else
      if (NVL(I_distro_type,'X') not in ('A','T')) then
         ---Distro_type is NULL
         O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO_TYPE',I_distro_type,L_function,NULL);
         return FALSE;
      end if;

      if WF_TRANSFER_SQL.GET_WF_ORDER_RMA_NO(O_error_message,
                                             L_wf_order_no,
                                             L_rma_no,
                                             I_distro_type,
                                             I_distro_no,
                                             I_to_loc,
                                             I_to_loc_type) = FALSE then
         return FALSE;
      end if;

      if (L_wf_order_no is NOT NULL and L_rma_no is NULL) then
         L_franchise_type := 'FO';  -- Franchise Order
      elsif ( L_wf_order_no is  NULL and L_rma_no is NOT NULL) then
         L_franchise_type := 'FR';  -- Franchise Return
      end if;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      return FALSE;
   end if;

   select class_vat_ind into L_class_vat_ind
     from class
    where class = I_class
      and dept = I_dept;

   if L_franchise_type in ('FO') then
      if I_shipment is NOT NULL then
         open C_GET_SHIPSKU_COST;
         fetch C_GET_SHIPSKU_COST into L_company_loc_wac, L_inv_status;
         if C_GET_SHIPSKU_COST%NOTFOUND then
            close C_GET_SHIPSKU_COST;
            O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_UNIT_COST',I_item, I_distro_no,NULL);
            return FALSE;
         end if;
         close C_GET_SHIPSKU_COST;
      else
         -- For a franchise order during shipment, sending loc's WAC is used to write TRAN_DATA.total_cost .
         -- for the sending location it will be returned through O_tsf_alloc_unit_cost for shipsku.unit_cost update.
         if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                       L_company_loc_wac,
                                       I_item,
                                       I_dept,
                                       I_class,
                                       I_subclass,
                                       I_from_loc,
                                       I_from_loc_type,
                                       L_tran_date) = FALSE then
            return FALSE;
         end if;
      end if;

      O_tsf_alloc_unit_cost := L_company_loc_wac;

      if I_weight_cuom is NULL then
         -- for a non-simple pack catch weight item, L_qty is the same as I_qty
         L_qty := I_qty;
         L_extended_cost    := L_qty * L_company_loc_wac;
      else
         -- for a simple pack catch weight item, total cost should be based on the actual weight
         -- I_weight_cuom is expressed in the pack's cost_uom. Get the CUOM corresponding to I_weight_cuom.
         if NOT ITEM_SUPP_COUNTRY_SQL.GET_COST_UOM(O_error_message,
                                                   L_cuom,
                                                   I_pack_no) then
            return FALSE;
         end if;
         -- For a simple pack catch weight item, there are two scenarioes:
         -- 1) the component item's standard UOM is weight, then TRAN_DATA.UNITS should be updated with
         -- the actual weight converted to component item's SUOM.
         -- 2) the component item's standard UOM is Eaches, then TRAN_DATA.UNITS should be updated with
         -- I_qty, which is based on v_packsku_qty.
         ---
         if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                                  L_qty,   -- output qty
                                                  I_item,
                                                  I_qty,   -- input qty
                                                  I_weight_cuom,
                                                  L_cuom) = FALSE then
            return FALSE;
         end if;

         -- calculate total cost based on weight
         if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                                 L_extended_cost,
                                                 I_pack_no,
                                                 L_company_loc_wac,  -- component source loc wac
                                                 I_weight_cuom,
                                                 I_qty) then
            return FALSE;
         end if;
      end if;

      -- Franchise cost is used as the 'sale price' for writing source-loc's total_retail on tran_data.
      -- It is also returned through O_unit_retail for writing shipsku.unit_retail.
      if I_pack_no is NOT NULL then
         if WF_ORDER_SQL.GET_ORDER_ITEM_COST(O_error_message,
                                             L_sale_price,
                                             L_order_currency,
                                             I_distro_no,
                                             I_distro_type,
                                             I_to_loc,
                                             I_to_loc_type,
                                             I_pack_no) = FALSE then
            return FALSE;
         end if;
         L_sale_price := L_sale_price * NVL(I_pct_in_pack,1);
      else
         if WF_ORDER_SQL.GET_ORDER_ITEM_COST(O_error_message,
                                             L_sale_price,           --tax exclusive
                                             L_order_currency,
                                             I_distro_no,
                                             I_distro_type,
                                             I_to_loc,
                                             I_to_loc_type,
                                             I_item) = FALSE then
            return FALSE;
         end if;
      end if;

      open  C_GET_CURRENCY(I_from_loc);
      fetch C_GET_CURRENCY into L_from_loc_currency;
      close C_GET_CURRENCY;

      open  C_GET_CURRENCY(I_to_loc);
      fetch C_GET_CURRENCY into L_to_loc_currency;
      close C_GET_CURRENCY;

      -- Convert the unit customer cost to source location currency. This will be used for markup/markdown and total retail calculation.
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_sale_price,
                              L_order_currency,
                              L_from_loc_currency,
                              L_sale_price_l_curr,     -- Output Retail in from-location currency.
                              'C',                     -- Using 'C' as this is Customer Cost which is posted as Retail for source location
                              L_tran_date,
                              L_exchange_type) = FALSE then
         return FALSE;
      end if;
      O_unit_retail :=  L_sale_price_l_curr;       -- Tax exclusive

      -- retrieve company unit_retail for markup/markdown calculation
      if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                  I_item,
                                                  I_from_loc,
                                                  I_from_loc_type,
                                                  L_std_av_cost,
                                                  L_std_cost_to_loc,
                                                  L_from_loc_retail_l_curr,
                                                  L_selling_unit_retail,
                                                  L_selling_uom) = FALSE then
         return FALSE;
      end if;

      if I_weight_cuom is NULL then
         L_tot_customer_cost := L_qty * L_sale_price_l_curr;
      else
         -- catch weight simple pack
         if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                                 L_tot_customer_cost,
                                                 I_pack_no,
                                                 L_sale_price_l_curr,
                                                 I_weight_cuom,
                                                 I_qty) then
            return FALSE;
         end if;
      end if;

      -- Get the Retail tax rate at the source location.
      if I_from_loc_type = 'S' then
         L_loc_type := 'ST';
      elsif I_from_loc_type = 'W' then
         L_loc_type := 'WH';
      end if;

      if WF_ORDER_SQL.GET_TAX_INFO (O_error_message,
                                    L_tax_rate_cost,
                                    L_tax_code_cost,
                                    I_item,
                                    L_loc_type,
                                    I_from_loc,
                                    I_to_loc,
                                    'C',
                                    L_wf_order_no) = FALSE then
         return FALSE;
      end if;

      if NVL(L_tax_rate_cost,0) = 0 then
         LP_exempt_ind := 'Y';
      end if;
      -- Get the cost vat amount of the trasnaction. This will be used to post tran code 87 at franchise location.
      LP_vat_amount_cost := L_tot_customer_cost * nvl(L_tax_rate_cost,0)/100;

      if NVL(L_from_loc_currency,'X') <> NVL(L_to_loc_currency,'X') then
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 LP_vat_amount_cost,
                                 L_from_loc_currency,
                                 L_to_loc_currency,
                                 LP_vat_amount_cost_f_curr,     -- Output Vat Amount in franchise store currency
                                 'C',
                                 L_tran_date,
                                 L_exchange_type) = FALSE then
            return FALSE;
         end if;
      else
         LP_vat_amount_cost_f_curr := LP_vat_amount_cost;
      end if;

      if WF_ORDER_SQL.GET_TAX_INFO (O_error_message,
                                    L_tax_rate_retail,
                                    L_tax_code_retail,
                                    I_item,
                                    L_loc_type,
                                    I_from_loc,
                                    I_to_loc,
                                    'R',
                                    L_wf_order_no) = FALSE then
         return FALSE;
      end if;

      if NVL(L_tax_rate_retail,0) = 0 then
         LP_exempt_ind := 'Y';
      end if;
      -- Get the retail vat amount of the trasnaction. This will be used to post tran code 88 at company location.
      LP_vat_amount_rtl := L_tot_customer_cost * nvl(L_tax_rate_retail,0)/100;

      -- VAT postings for Franchise to be routed through Custom Tax calls based on vat_calc_type in vat_region.
      -- The value for Retail is modified to add/remove VAT based on STKLDGR_VAT_INCL_RETL_IND,CLASS_LEVEL_VAT_IND
      -- and CLASS.CLASS_VAT_IND before the value is passed to BUILD_TRAN_DATA_INSERT where it will not be changed.
      if L_system_options.stkldgr_vat_incl_retl_ind = 'Y' then
         L_tot_comp_loc_retail_vat_adj := L_tot_customer_cost * (1 + nvl(L_tax_rate_retail,0)/100);
         L_sale_price_l_curr := L_sale_price_l_curr * (1 + nvl(L_tax_rate_retail,0)/100);
         if (L_system_options.class_level_vat_ind = 'Y' and L_class_vat_ind ='N') then
            --  VAT to be added
            L_from_loc_retail_l_curr := L_from_loc_retail_l_curr * (1 + nvl(L_tax_rate_retail,0)/100);
         end if;
      else -- L_system_options.stkldgr_vat_incl_retl_ind = 'N'
         L_tot_comp_loc_retail_vat_adj := L_tot_customer_cost ;
         if L_system_options.class_level_vat_ind = 'N' or (L_system_options.class_level_vat_ind = 'Y' and L_class_vat_ind ='Y') then
            -- VAT to be removed
            L_from_loc_retail_l_curr := L_from_loc_retail_l_curr /(1 + nvl(L_tax_rate_retail,0)/100);
         end if;
      end if;
      --

      L_tran_code := 82;
      -- write standard transfer records
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             I_dept,
                                             I_class,
                                             I_subclass,
                                             I_from_loc,
                                             I_from_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,
                                             L_qty,
                                             L_extended_cost,                  --Total Cost
                                             L_tot_comp_loc_retail_vat_adj,    --Total Retail
                                             NULL,
                                             I_distro_no,                --I_ref_no_1
                                             L_wf_order_no,              --I_ref_no_2
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function,
                                             L_tax_code_retail,          -- gl_ref_no, for 82, it would be Output VAT, so use retail tax_code
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NVL(L_tax_rate_retail,0),
                                             NULL,
                                             I_pack_no,
                                             NULL) = FALSE then
         return FALSE;
      end if;



      if L_sale_price_l_curr != L_from_loc_retail_l_curr then
         if L_from_loc_retail_l_curr > L_sale_price_l_curr then
            L_tran_code := 85;
         else
            L_tran_code := 84;
         end if;

         L_total_retail := L_qty * ABS(L_from_loc_retail_l_curr - L_sale_price_l_curr);


         -- write standard transfer records
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                I_item,
                                                I_dept,
                                                I_class,
                                                I_subclass,
                                                I_from_loc,
                                                I_from_loc_type,
                                                L_tran_date,
                                                L_tran_code,
                                                NULL,
                                                L_qty,
                                                L_null_cost,         -- Total Cost
                                                L_total_retail,      -- Total Retail
                                                NULL,
                                                I_distro_no,                --I_ref_no_1
                                                L_wf_order_no,              --I_ref_no_2
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_function,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NVL(L_tax_rate_retail,0),
                                                NULL,
                                                I_pack_no,
                                                NULL) = FALSE then
            return FALSE;
         end if;
      end if;

      -- Get the stockholding indicator for the franchise location. For stockholding franchise
      -- store, tran code 20 will be posted.
      open  C_STORE_TYPE(I_to_loc);
      fetch C_STORE_TYPE into L_store_type,
                              L_stockholding_ind,
                              L_f_store_currency;
      close C_STORE_TYPE;

      if(L_store_type = 'F' and L_stockholding_ind = 'Y') then
         -- Get the Franchaise location retail amount for posting total_retail
         if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                     I_item,
                                                     I_to_loc,
                                                     I_to_loc_type,
                                                     L_f_std_av_cost,
                                                     L_f_std_cost_to_loc,
                                                     L_f_store_retail,
                                                     L_f_selling_unit_retail,
                                                     L_f_selling_uom) = FALSE then
            return FALSE;
         end if;
         L_f_store_total_retail := L_f_store_retail * L_qty;

         if I_weight_cuom is NULL then
            L_f_store_total_retail := L_qty * L_f_store_retail;
         else
            -- catch weight simple pack
            if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                                    L_f_store_total_retail,
                                                    I_pack_no,
                                                    L_f_store_retail,
                                                    I_weight_cuom,
                                                    I_qty) then
               return FALSE;
            end if;
         end if;

         -- Convert the customer cost to the franchise store currency.
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_tot_customer_cost,
                                 L_from_loc_currency,
                                 L_f_store_currency,
                                 L_tot_to_loc_cost_l_curr,      --Output Total Cost
                                 'C',
                                 L_tran_date,
                                 L_exchange_type) = FALSE then
            return FALSE;
         end if;

         L_tran_code := 20;
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                I_item,
                                                I_dept,
                                                I_class,
                                                I_subclass,
                                                I_to_loc,
                                                I_to_loc_type,
                                                L_tran_date,
                                                L_tran_code,
                                                NULL,
                                                L_qty,
                                                L_tot_to_loc_cost_l_curr,  --Total Cost
                                                L_f_store_total_retail,    --Total Retail
                                                NULL,
                                                I_distro_no,                --I_ref_no_1
                                                L_wf_order_no,              --I_ref_no_2
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_function,
                                                L_tax_code_cost,            --gl_ref_no, use cost tax_code for inbound tran_code 20
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                I_pack_no,
                                                NULL) = FALSE then
             return FALSE;
         end if;
      end if;  -- Stockholding Franchise store
   elsif L_franchise_type in ('FR') then
      if L_destroy_onsite then
         if STKLEDGR_SQL.INIT_TRAN_DATA_INSERT(O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;

      -- Get the WAC at the company location for total cost posting against 83.
      if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                    L_company_loc_wac,
                                    I_item,
                                    I_dept,
                                    I_class,
                                    I_subclass,
                                    I_to_loc,
                                    I_to_loc_type,
                                    L_tran_date) = FALSE then
         return FALSE;
      end if;

      if I_weight_cuom is NULL then
         -- for a non-simple pack catch weight item, L_qty is the same as I_qty
         L_qty := I_qty;
         L_extended_cost    := L_qty * L_company_loc_wac;
      else
         -- for a simple pack catch weight item, total cost should be based on the actual weight
         -- I_weight_cuom is expressed in the pack's cost_uom. Get the CUOM corresponding to I_weight_cuom.
         if NOT ITEM_SUPP_COUNTRY_SQL.GET_COST_UOM(O_error_message,
                                                   L_cuom,
                                                   I_pack_no) then
            return FALSE;
         end if;
         -- For a simple pack catch weight item, there are two scenarioes:
         -- 1) the component item's standard UOM is weight, then TRAN_DATA.UNITS should be updated with
         -- the actual weight converted to component item's SUOM.
         -- 2) the component item's standard UOM is Eaches, then TRAN_DATA.UNITS should be updated with
         -- I_qty, which is based on v_packsku_qty.
         ---
         if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                                  L_qty,   -- output qty
                                                  I_item,
                                                  I_qty,   -- input qty
                                                  I_weight_cuom,
                                                  L_cuom) = FALSE then
            return FALSE;
         end if;
         -- calculate total cost based on weight
         if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                                 L_extended_cost,
                                                 I_pack_no,
                                                 L_company_loc_wac,  -- component wh wac
                                                 I_weight_cuom,
                                                 I_qty) then
            return FALSE;
         end if;
      end if;

      -- If the return is a regular return, then get return cost using distro number
      -- Else for destroy at site use RMA number.
      if NOT L_destroy_onsite then
         If I_pack_no is NOT NULL Then
            if WF_RETURN_SQL.GET_RETURN_COST(O_error_message,
                                             L_return_unit_cost,
                                             L_return_currency,
                                             L_restock_cost,
                                             NULL,
                                             I_distro_no,
                                             I_pack_no) = false then
               return FALSE;
            end if;
            L_return_unit_cost := L_return_unit_cost * I_pct_in_pack;
         else
            if WF_RETURN_SQL.GET_RETURN_COST(O_error_message,
                                             L_return_unit_cost,
                                             L_return_currency,
                                             L_restock_cost,
                                             NULL,
                                             I_distro_no,
                                             I_item) = false then
               return FALSE;
            end if;
         end if;

      else -- Destroy on site. Get cost by passing RMA number.
         if I_pack_no is NOT NULL then
            if WF_RETURN_SQL.GET_RETURN_COST(O_error_message,
                                             L_return_unit_cost,
                                             L_return_currency,
                                             L_restock_cost,
                                             I_rma_no,
                                             NULL,
                                             I_pack_no) = FALSE then
               return FALSE;
            end if;
            L_return_unit_cost := L_return_unit_cost * I_pct_in_pack;
         else
            if WF_RETURN_SQL.GET_RETURN_COST(O_error_message,
                                             L_return_unit_cost,
                                             L_return_currency,
                                             L_restock_cost,
                                             I_rma_no,
                                             NULL,
                                             I_item) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;

      -- Get company location currency.
      open  C_GET_CURRENCY(I_to_loc);
      fetch C_GET_CURRENCY into L_to_loc_currency;
      close C_GET_CURRENCY;

      open  C_GET_CURRENCY(I_from_loc);
      fetch C_GET_CURRENCY into L_from_loc_currency;
      close C_GET_CURRENCY;

      if L_return_currency = L_to_loc_currency then
         L_return_unit_cost_l_curr := L_return_unit_cost;
      else
         -- Convert return unit cost to company location currency for markup/markdown and total retail calculation.
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_return_unit_cost,
                                 L_return_currency,
                                 L_to_loc_currency,
                                 L_return_unit_cost_l_curr,---  Output Retail in company location currency
                                 'C',
                                 L_tran_date,
                                 L_exchange_type) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_weight_cuom is NULL then
         L_wf_return_cost := L_qty * L_return_unit_cost_l_curr;
      else
         -- catch weight simple pack
         if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                                 L_wf_return_cost,
                                                 I_pack_no,
                                                 L_return_unit_cost_l_curr,
                                                 I_weight_cuom,
                                                 I_qty) then
            return FALSE;
         end if;
      end if;

      -- If the franchise location is stockholding, post 24.
      open C_STORE_TYPE(I_from_loc);
      fetch C_STORE_TYPE into L_store_type,
                              L_stockholding_ind,
                              L_f_store_currency;
      close C_STORE_TYPE;

      --For returns from non-stockholding franchise stores:
      --1) Use return_unit_cost (converted to sending-loc currency) as O_tsf_alloc_unit_cost for shipsku.unit_cost update.
      --2) Use return_unit_cost (converted to sending_loc currency) as O_unit_retail for shipsku.unit_retail update.
      if L_stockholding_ind = 'N' then
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_return_unit_cost,
                                 L_return_currency,
                                 L_f_store_currency,
                                 O_unit_retail,
                                 'C',
                                 L_tran_date,
                                 L_exchange_type) = FALSE then
            return FALSE;
         end if;
         O_tsf_alloc_unit_cost := O_unit_retail;
      end if;

      if WF_RETURN_SQL.GET_TAX_INFO (O_error_message,
                                    L_tax_rate_cost,
                                    L_tax_code_cost,
                                    I_item,
                                    I_to_loc,
                                    I_to_loc_type,
                                    'C',
                                    I_from_loc,
                                    I_from_loc_type,
                                    L_rma_no) = FALSE then
         return FALSE;
      end if;

      if NVL(L_tax_rate_cost,0) = 0 then
         LP_exempt_ind := 'Y';
      end if;
      -- Get the cost VAT in the transaction. This will be used to post 87 (negative) at franchise location
      LP_vat_amount_cost := L_wf_return_cost * nvl(L_tax_rate_cost,0)/100;

      if NVL(L_from_loc_currency,'X') <> NVL(L_to_loc_currency,'X') then
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 LP_vat_amount_cost,
                                 L_to_loc_currency,
                                 L_from_loc_currency,
                                 LP_vat_amount_cost_f_curr,     -- Output Vat Amount in franchise store currency
                                 'C',
                                 L_tran_date,
                                 L_exchange_type) = FALSE then
            return FALSE;
         end if;
      else
         LP_vat_amount_cost_f_curr :=  LP_vat_amount_cost;
      end if;

      if WF_RETURN_SQL.GET_TAX_INFO (O_error_message,
                                    L_tax_rate_retail,
                                    L_tax_code_retail,
                                    I_item,
                                    I_to_loc,
                                    I_to_loc_type,
                                    'R',
                                    I_from_loc,
                                    I_from_loc_type,
                                    L_rma_no) = FALSE then
         return FALSE;
      end if;

      if NVL(L_tax_rate_retail,0) = 0 then
         LP_exempt_ind := 'Y';
      end if;
      -- Get the retail VAT in the transaction. This will be used to post 88 (negative) at company location
      LP_vat_amount_rtl := L_wf_return_cost * nvl(L_tax_rate_retail,0)/100;

      -- Call moved before tran_data_insert
      if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                  I_item,
                                                  I_to_loc,
                                                  I_to_loc_type,
                                                  L_std_av_cost,
                                                  L_std_cost_to_loc,
                                                  L_to_loc_retail_l_curr,
                                                  L_selling_unit_retail,
                                                  L_selling_uom) = FALSE then
         return FALSE;
      end if;


      -- VAT postings for Franchise to be routed through Custom Tax calls based on vat_calc_type in vat_region.
      -- The value for Retail is modified to add/remove VAT based on STKLDGR_VAT_INCL_RETL_IND,CLASS_LEVEL_VAT_IND
      -- and CLASS.CLASS_VAT_IND before the value is passed to BUILD_TRAN_DATA_INSERT where it will not be changed.

      L_return_unit_cost_vat := L_return_unit_cost_l_curr;
      if L_system_options.stkldgr_vat_incl_retl_ind = 'Y' then
         L_tot_comp_loc_retail_vat_adj := L_wf_return_cost * (1 + nvl(L_tax_rate_retail,0)/100);
         L_return_unit_cost_vat := L_return_unit_cost_vat * (1 + nvl(L_tax_rate_retail,0)/100);
         if (L_system_options.class_level_vat_ind = 'Y' and L_class_vat_ind ='N') then
            --  VAT to be added
            L_to_loc_retail_l_curr := L_to_loc_retail_l_curr * (1 + nvl(L_tax_rate_retail,0)/100);
         end if;
      else -- L_system_options.stkldgr_vat_incl_retl_ind = 'N'
         L_tot_comp_loc_retail_vat_adj := L_wf_return_cost ;
         if L_system_options.class_level_vat_ind = 'N' or (L_system_options.class_level_vat_ind = 'Y' and L_class_vat_ind ='Y') then
            -- VAT to be removed
            L_to_loc_retail_l_curr := L_to_loc_retail_l_curr /(1 + nvl(L_tax_rate_retail,0)/100);
         end if;
      end if;
      --

      L_tran_code := 83;
      -- write standard transfer records
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             I_dept,
                                             I_class,
                                             I_subclass,
                                             I_to_loc,
                                             I_to_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,
                                             L_qty,
                                             L_extended_cost,                -- Total Cost
                                             L_tot_comp_loc_retail_vat_adj,  -- Total Retail
                                             NULL,
                                             I_distro_no,                   --I_ref_no_1
                                             L_rma_no,                      --I_ref_no_2
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function,
                                             L_tax_code_retail,             --gl_ref_no, use retail tax_code for tran_code 83
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NVL(L_tax_rate_retail,0),
                                             NULL,
                                             I_pack_no,
                                             NULL) = FALSE then
         return FALSE;
      end if;

      if L_to_loc_retail_l_curr != L_return_unit_cost_vat then

         if L_to_loc_retail_l_curr < L_return_unit_cost_vat then
            L_tran_code := 85;
         else
            L_tran_code := 84;
         end if;

         L_total_retail := L_qty * ABS(L_to_loc_retail_l_curr - L_return_unit_cost_vat);
         -- write standard transfer records
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                I_item,
                                                I_dept,
                                                I_class,
                                                I_subclass,
                                                I_to_loc,
                                                I_to_loc_type,
                                                L_tran_date,
                                                L_tran_code,
                                                NULL,
                                                L_qty,
                                                L_null_cost,       -- Total cost
                                                L_total_retail,    -- Total Retail
                                                NULL,
                                                I_distro_no,                   --I_ref_no_1
                                                L_rma_no,                      --I_ref_no_2
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_function,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NVL(L_tax_rate_retail,0),
                                                NULL,
                                                I_pack_no,
                                                NULL) = FALSE then
            return FALSE;
         end if;
      end if;

      if L_destroy_onsite then
         L_reason := 190;                 -- Destroy on Site for WF returns
         if INVADJ_SQL.GET_REASON_INFO(O_error_message,
                                       L_reason_desc,
                                       L_cogs_ind,
                                       L_reason) = false then
            return FALSE;
         end if;
         if (L_cogs_ind = 'Y') then
            L_tran_code := 23;
         else
            L_tran_code := 22;
         end if;
         L_extended_cost := -1 * L_extended_cost;
         -- inventory adj for destroy onsite transaction
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                I_item,
                                                I_dept,
                                                I_class,
                                                I_subclass,
                                                I_to_loc,
                                                I_to_loc_type,
                                                L_tran_date,
                                                L_tran_code,
                                                NULL,
                                                -1 * L_qty,
                                                L_extended_cost,                               -- Total Cost
                                                -1 * L_to_loc_retail_l_curr * L_qty,           -- Total Retail
                                                L_return_unit_cost,
                                                L_rma_no,                                --I_ref_no_1
                                                NULL,                                    --I_ref_no_2
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_function,
                                                L_reason,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                I_pack_no,
                                                NULL) = FALSE then          -- gl_ref_no
            return FALSE;
         end if;
      end if; --   L_destroy_onsite

      if L_restock_cost is NOT NULL and L_restock_cost <> 0 then
         -- Convert to company location currency.
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_restock_cost,
                                 L_return_currency,
                                 L_to_loc_currency,
                                 L_restock_cost_l_curr,
                                 'C',
                                 L_tran_date,
                                 L_exchange_type) = FALSE then
            return FALSE;
         end if;

         if I_pack_no is NOT NULL then
            L_restock_cost_l_curr := (L_restock_cost_l_curr * I_pct_in_pack) * L_qty;
         else
            L_restock_cost_l_curr := L_restock_cost_l_curr * L_qty;
         end if;


         -- VAT postings for Franchise to be routed through Custom Tax calls based on vat_calc_type in vat_region.
         -- The value for Retail is modified to add/remove VAT based on STKLDGR_VAT_INCL_RETL_IND,CLASS_LEVEL_VAT_IND
         -- and CLASS.CLASS_VAT_IND before the value is passed to BUILD_TRAN_DATA_INSERT where it will not be changed.
         if L_system_options.stkldgr_vat_incl_retl_ind = 'Y' then
            if (L_system_options.class_level_vat_ind = 'Y' and L_class_vat_ind ='N') then
               --  VAT to be added
               L_restock_cost_l_curr := L_restock_cost_l_curr * (1 + nvl(L_tax_rate_retail,0)/100);
            end if;
         else -- L_system_options.stkldgr_vat_incl_retl_ind = 'N'
            if L_system_options.class_level_vat_ind = 'N' or (L_system_options.class_level_vat_ind = 'Y' and L_class_vat_ind ='Y') then
               -- VAT to be removed
               L_restock_cost_l_curr := L_restock_cost_l_curr /(1 + nvl(L_tax_rate_retail,0)/100);
            end if;
         end if;
         --
         L_tran_code := 86;
         -- write whosale franchise restocking fee transactions
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                I_item,
                                                I_dept,
                                                I_class,
                                                I_subclass,
                                                I_to_loc,
                                                I_to_loc_type,
                                                L_tran_date,
                                                L_tran_code,
                                                NULL,
                                                L_qty,
                                                L_dummy_cost,           -- Total Cost
                                                L_restock_cost_l_curr,  -- Total Retail
                                                NULL,
                                                I_distro_no,         --I_ref_no_1
                                                L_rma_no,            --I_ref_no_2
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_function,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                I_pack_no,
                                                NULL) = FALSE then
            return FALSE;
         end if;
      end if;---restock fee

     ---writing RTV ,cost variance and restocking fee tran data records for a stockholding franchise store
      if(L_store_type = 'F' and L_stockholding_ind = 'Y') then
       -- Get the total return cost in franchise location currency.
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_wf_return_cost,    -- This is at Company location currency.
                                 L_to_loc_currency,
                                 L_f_store_currency,
                                 L_tot_from_loc_cost_l_curr, -- Total Cost for franchise location
                                 'C',
                                 L_tran_date,
                                 L_exchange_type) = FALSE then
            return FALSE;
         end if;

         if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                     I_item,
                                                     I_from_loc,
                                                     I_from_loc_type,
                                                     L_f_std_av_cost,
                                                     L_f_std_cost_to_loc,
                                                     L_f_store_retail,
                                                     L_f_selling_unit_retail,
                                                     L_f_selling_uom) = FALSE then
            return FALSE;
         end if;
         L_f_store_total_retail := L_f_store_retail * L_qty;

         L_tran_code := 24;
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                I_item,
                                                I_dept,
                                                I_class,
                                                I_subclass,
                                                I_from_loc,
                                                I_from_loc_type,
                                                L_tran_date,
                                                L_tran_code,
                                                NULL,
                                                L_qty,
                                                L_tot_from_loc_cost_l_curr, -- Total cost
                                                L_f_store_total_retail,     -- Total Retail
                                                NULL,
                                                I_distro_no,         --I_ref_no_1
                                                L_rma_no,            --I_ref_no_2
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_function,
                                                L_tax_code_cost,     --gl_ref_no, use cost tax_code for tran_code 24
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                0,
                                                NULL,
                                                I_pack_no,
                                                NULL) = FALSE then
            return FALSE;
         end if;
         if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                       L_f_store_wac,
                                       I_item,
                                       I_dept,
                                       I_class,
                                       I_subclass,
                                       I_from_loc,
                                       I_from_loc_type,
                                       L_tran_date) = FALSE then
            return FALSE;
         end if;
         -- Get the return unit cost in franchise location currency.
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_return_unit_cost_l_curr,
                                 L_to_loc_currency,
                                 L_f_store_currency,
                                 L_from_loc_cost_l_curr,-- Output cost in franchise location currency
                                 'C',
                                 L_tran_date,
                                 L_exchange_type) = FALSE then
            return FALSE;
         end if;

         --For returns from stockholding franchise stores:
         --1) Use return_unit_cost (converted to sending-loc currency) as O_tsf_alloc_unit_cost for shipsku.unit_cost update.
         --2) Use franchise loc's unit_retail(in sending_loc currency) as O_unit_retail for shipsku.unit_retail update.

         O_tsf_alloc_unit_cost := L_from_loc_cost_l_curr;
         O_unit_retail := L_f_store_retail;


         -- Post Cost variance at franchise location if the return cost is different from wac.
         if L_from_loc_cost_l_curr is NOT NULL and L_f_store_wac is not NULL then
            if NOT POST_COST_VARIANCE(O_error_message,
                                      I_item,
                                      I_pack_no,
                                      I_dept,
                                      I_class,
                                      I_subclass,
                                      I_from_loc,
                                      I_from_loc_type,
                                      L_from_loc_cost_l_curr,
                                      L_f_store_wac,
                                      L_qty,
                                      I_weight_cuom,
                                      I_distro_no,         --I_ref_no_1
                                      L_rma_no,            --I_ref_no_2,
                                      L_tran_date) then
               return FALSE;
            end if;
         end if;

         if L_restock_cost is NOT NULL and L_restock_cost <> 0 then
            ---Converting total restock cost in franchise store currency
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_restock_cost,
                                    L_return_currency,
                                    L_f_store_currency,
                                    L_restock_cost_f_curr,
                                    'C',
                                    L_tran_date,
                                    L_exchange_type) = FALSE then
               return FALSE;
            end if;

            if I_pack_no is NOT NULL then
               L_restock_cost_f_curr := (L_restock_cost_f_curr * I_pct_in_pack) * L_qty;
            else
               L_restock_cost_f_curr := L_restock_cost_f_curr * L_qty;
            end if;

            LP_vat_amount_cost_f_curr := NULL;
            LP_vat_amount_cost_f_curr := L_restock_cost_f_curr * nvl(L_tax_rate_cost,0)/100;

            L_tran_code := 65;
            if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                   I_item,
                                                   I_dept,
                                                   I_class,
                                                   I_subclass,
                                                   I_from_loc,
                                                   I_from_loc_type,
                                                   L_tran_date,
                                                   L_tran_code,
                                                   NULL,
                                                   L_qty,
                                                   L_restock_cost_f_curr,   -- Total Cost
                                                   NULL,                    -- Total Retail.
                                                   NULL,
                                                   I_distro_no,         --I_ref_no_1
                                                   L_rma_no,            --I_ref_no_2
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_function,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   0,
                                                   NULL,
                                                   I_pack_no,
                                                   NULL) = FALSE then
               return FALSE;
            end if;
         end if;
      end if; --stockholding franchise store

      if L_destroy_onsite then
         if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT(O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;

   end if;     -- end if for distro type as FO or FR

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
   return FALSE;
END PROCESS_WF_WRITE_FINANCIALS;
----------------------------------------------------------------------------------------------
FUNCTION WF_WRITE_FINANCIALS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tsf_alloc_unit_cost OUT    TRAN_DATA.TOTAL_COST%TYPE,
                             O_unit_retail         OUT    TRAN_DATA.TOTAL_RETAIL%TYPE,
                             I_distro_no           IN     SHIPSKU.DISTRO_NO%TYPE,
                             I_distro_type         IN     SHIPSKU.DISTRO_TYPE%TYPE,
                             I_tran_date           IN     PERIOD.VDATE%TYPE,
                             I_item                IN     ITEM_MASTER.ITEM%TYPE,
                             I_pack_no             IN     ITEM_MASTER.ITEM%TYPE,
                             I_pct_in_pack         IN     NUMBER,
                             I_dept                IN     ITEM_MASTER.DEPT%TYPE,
                             I_class               IN     ITEM_MASTER.CLASS%TYPE,
                             I_subclass            IN     ITEM_MASTER.SUBCLASS%TYPE,
                             I_qty                 IN     TSFDETAIL.TSF_QTY%TYPE,
                             I_from_loc            IN     ITEM_LOC.LOC%TYPE,
                             I_from_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_to_loc              IN     ITEM_LOC.LOC%TYPE,
                             I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_weight_cuom         IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                             I_rma_no              IN     WF_RETURN_HEAD.RMA_NO%TYPE,
                             I_shipment            IN     SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                             I_carton              IN     SHIPSKU.CARTON%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program                      VARCHAR2(64) := 'STKLEDGR_SQL.WF_WRITE_FINANCIALS';
   L_deposit_item_type            ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE;
   L_container_item               ITEM_MASTER.CONTAINER_ITEM%TYPE;
   L_container_rec                ITEM_MASTER%ROWTYPE;
   L_dummy_container_unit_cost    TRAN_DATA.TOTAL_COST%TYPE;
   L_dummy_container_unit_retail  TRAN_DATA.TOTAL_RETAIL%TYPE;

   cursor C_ITEM_TYPE is
      select deposit_item_type,
             container_item
        from item_master
       where item = I_item;


BEGIN

   -- Check if the input item is a deposit content. Include deposit container also.
   open  C_ITEM_TYPE;
   fetch C_ITEM_TYPE into L_deposit_item_type,
                          L_container_item;
   close C_ITEM_TYPE;

   LP_franchise_transaction := 'Y';

   -- When this function is called with a deposit container item, the container item
   -- is either part of a buyer pack (order as pack) or part of a vendor pack. The
   -- container item's cost and retail must be returned to build the pack's cost/retail.
   if NVL(L_deposit_item_type,'X') = 'A' and I_pack_no is NOT NULL then
      if PROCESS_WF_WRITE_FINANCIALS(O_error_message,
                                     O_tsf_alloc_unit_cost,   --return container item's from-loc wac as the cost
                                     O_unit_retail,           --return container item's prorated pricing cost as the retail
                                     I_distro_no,
                                     I_distro_type,
                                     I_tran_date,
                                     I_item,
                                     I_pack_no,
                                     I_pct_in_pack,
                                     I_dept,
                                     I_class,
                                     I_subclass,
                                     I_qty,
                                     I_from_loc,
                                     I_from_loc_type,
                                     I_to_loc,
                                     I_to_loc_type,
                                     I_weight_cuom,
                                     I_rma_no,
                                     I_shipment,             --pass in I_shipment/I_carton to use shipsku.unit_cost in case of stock reconciliation.
                                     I_carton) = FALSE then
         return FALSE;
      end if;
   else

      -- When this function is called for a deposit content item and the content item is NOT part of a pack,
      -- Call PROCESS_WF_WRITE_FINANCIALS to write tran-data for the container item first and then for the content item.
      -- Do NOT include container item's cost/retail in the output of O_tsf_alloc_unit_cost and O_unit_retail
      -- since shipsku will be written for the content item and its unit_cost/retail should NOT include
      -- container item's cost/retail.

      -- When this function is called for a deposit content item and the content item IS part of a pack,
      -- do NOT call PROCESS_WF_WRITE_FINANCIALS for the container item since WF_WRITE_FINANCIALS will be
      -- called independently for the container item as a pack component (handled in the above code branch).

      if NVL(L_deposit_item_type,'X') = 'E' and I_pack_no is NULL then
         -- Get the container_item details
         if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                            L_container_rec,
                                            L_container_item) = FALSE then
            return FALSE;
         end if;
         ---
         if PROCESS_WF_WRITE_FINANCIALS(O_error_message,
                                        L_dummy_container_unit_cost,
                                        L_dummy_container_unit_retail,
                                        I_distro_no,
                                        I_distro_type,
                                        I_tran_date,
                                        L_container_item,
                                        NULL,      -- pack_no
                                        NULL,      -- pct_in_pack
                                        L_container_rec.dept,
                                        L_container_rec.class,
                                        L_container_rec.subclass,
                                        I_qty,
                                        I_from_loc,
                                        I_from_loc_type,
                                        I_to_loc,
                                        I_to_loc_type,
                                        I_weight_cuom,
                                        I_rma_no) = FALSE then
            return FALSE;
         end if;
      end if;

      if PROCESS_WF_WRITE_FINANCIALS(O_error_message,
                                     O_tsf_alloc_unit_cost,
                                     O_unit_retail,
                                     I_distro_no,
                                     I_distro_type,
                                     I_tran_date,
                                     I_item,
                                     I_pack_no,
                                     I_pct_in_pack,
                                     I_dept,
                                     I_class,
                                     I_subclass,
                                     I_qty,
                                     I_from_loc,
                                     I_from_loc_type,
                                     I_to_loc,
                                     I_to_loc_type,
                                     I_weight_cuom,
                                     I_rma_no,
                                     I_shipment,
                                     I_carton) = FALSE then
         return FALSE;
      end if;
   end if;

   --Reset the package variable to null.
   LP_franchise_transaction := NULL;
   return TRUE;

EXCEPTION
   when OTHERS then
      LP_franchise_transaction := NULL;
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END WF_WRITE_FINANCIALS;
--------------------------------------------------------------------------------
FUNCTION POST_COST_VARIANCE(O_error_message       IN OUT VARCHAR2,
                            I_item                IN     item_master.item%TYPE,
                            I_pack_no             IN     packitem.pack_no%TYPE,
                            I_dept                IN     item_master.dept%TYPE,
                            I_class               IN     item_master.class%TYPE,
                            I_subclass            IN     item_master.subclass%TYPE,
                            I_from_loc            IN     item_loc.loc%TYPE,
                            I_from_loc_type       IN     item_loc.loc_type%TYPE,
                            I_tsf_rtv_cost        IN     item_loc_soh.unit_cost%TYPE,
                            I_from_wac            IN     item_loc_soh.av_cost%TYPE,
                            I_qty                 IN     tsfdetail.tsf_qty%TYPE,
                            I_weight_cuom         IN     item_loc_soh.average_weight%TYPE,
                            I_ref_no_1            IN     tran_data.ref_no_1%TYPE,
                            I_ref_no_2            IN     tran_data.ref_no_2%TYPE,
                            I_tran_date           IN     period.vdate%TYPE)
RETURN BOOLEAN IS

   L_function              VARCHAR2(60) := 'STKLEDGR_SQL.POST_COST_VARIANCE';

   L_profit_type      DEPS.PROFIT_CALC_TYPE%TYPE;
   L_markup_type      DEPS.MARKUP_CALC_TYPE%TYPE;
   L_tran_code        TRAN_DATA.TRAN_CODE%TYPE;
   L_total_variance   ITEM_LOC_SOH.AV_COST%TYPE;
   L_store_row        STORE%ROWTYPE;
   L_exist_store      BOOLEAN;

BEGIN
   -- Both transfer cost and from loc's WAC are in from loc's currency.
   -- The variance between from loc's wac and tsf_cost is written against the from loc.

   if I_from_wac is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_from_wac', L_function, NULL);
      return FALSE;
   end if;

   if I_tsf_rtv_cost is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_tsf_rtv_cost', L_function, NULL);
      return FALSE;
   end if;

   if I_from_loc_type = 'S' then
      if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                  L_exist_store,
                                  L_store_row,
                                  I_from_loc) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exist_store = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      -- Check if the input store is a valid WF store.
      elsif L_store_row.store_type in ('W', 'F') and L_store_row.stockholding_ind = 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('NONSTOCK_WF_STORE_INV',
                                               L_function,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   -- I_tsf_rtv_cost is in from loc's currency.
   if round(I_from_wac, 4) = round(I_tsf_rtv_cost, 4) then
      -- no cost variance, done
      return TRUE;
   end if;

   if NOT DEPT_ATTRIB_SQL.GET_ACCTNG_METHODS(O_error_message,
                                             I_dept,
                                             L_profit_type,
                                             L_markup_type) then
      return FALSE;
   end if;

   if L_profit_type = 1 then
      -- cost accounting
      L_tran_code := 72;
   else
      -- retail accounting
      L_tran_code := 71;
   end if;

   if I_weight_cuom is NOT NULL then
      if I_pack_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_pack_no', L_function, NULL);
         return FALSE;
      end if;
      -- catch weight simple pack item, total cost variance is based on weight
      if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                              L_total_variance,
                                              I_pack_no,
                                              I_from_wac-I_tsf_rtv_cost,  -- component unit cost variance
                                              I_weight_cuom,
                                              I_qty) then
         return FALSE;
      end if;
   else
      if I_qty is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_qty', L_function, NULL);
         return FALSE;
      end if;
      ---
      L_total_variance := (I_from_wac-I_tsf_rtv_cost)*I_qty;
   end if;

   if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                          I_item,
                                          I_dept,
                                          I_class,
                                          I_subclass,
                                          I_from_loc,
                                          I_from_loc_type,
                                          I_tran_date,
                                          L_tran_code,
                                          NULL,
                                          I_qty,
                                          L_total_variance,
                                          NULL,   -- total retail
                                          I_ref_no_1,
                                          I_ref_no_2,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          L_function,
                                          NULL) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
   return FALSE;
END POST_COST_VARIANCE;
--------------------------------------------------------------------------
FUNCTION GET_TSF_MARKDOWN_LOC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_tsf_markdown_loc    IN OUT ITEM_LOC.LOC%TYPE,
                              I_tsf_no              IN     TSFHEAD.TSF_NO%TYPE,
                              I_from_loc            IN     ITEM_LOC.LOC%TYPE,
                              I_to_loc              IN     ITEM_LOC.LOC%TYPE,
                              I_from_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                              I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE)
   return BOOLEAN IS

   L_program              VARCHAR2(64)     := 'STKLEDGR_SQL.GET_TSF_MARKDOWN_LOC';

   L_system_options       SYSTEM_OPTIONS%ROWTYPE;
   L_from_loc_type        TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_to_loc_type          TSFHEAD.TO_LOC_TYPE%TYPE;

   CURSOR C_tsf_parent_info is
      select from_loc_type
        from tsfhead th1
       where exists (select 'X'
                       from tsfhead th2
                      where th2.tsf_no = I_tsf_no
                        and th1.tsf_no = th2.tsf_parent_no);

   CURSOR C_tsf_child_info is
      select to_loc_type
        from tsfhead
       where tsf_parent_no = I_tsf_no;

BEGIN
   -- For a multi-legged transfer the initial sending loc's and the final receiving loc's
   -- loc types will be used to determine the tsf markdown location. Finisher does not have
   -- unit retail of its own. Finisher takes either the sending loc's or the receiving loc's
   -- unit retail.

   if I_from_loc_type = 'E' then
      --2nd leg of 2-legs transfer
      -- get the parent transfer's from loc type
      open C_tsf_parent_info;
      fetch C_tsf_parent_info into L_from_loc_type;
      close C_tsf_parent_info;

      L_to_loc_type := I_to_loc_type;

   elsif I_to_loc_type = 'E' then
      --1st leg of 2-legs transfer
      -- get the child transfer's to loc type
      open C_tsf_child_info;
      fetch C_tsf_child_info into L_to_loc_type;
      close C_tsf_child_info;

      L_from_loc_type := I_from_loc_type;

   else
      -- no finisher involved, single-legged transfer
      L_from_loc_type := I_from_loc_type;
      L_to_loc_type := I_to_loc_type;
   end if;

   if L_from_loc_type NOT in ('S', 'W') then
      O_error_message := sql_lib.create_msg('INV_FROM_LOC',
                                            L_program,
                                            NULL,
                                            NULL);
      RETURN FALSE;
   end if;

   if L_to_loc_type NOT in ('S', 'W') then
      O_error_message := sql_lib.create_msg('INV_TO_LOC',
                                            L_program,
                                            NULL,
                                            NULL);
      RETURN FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      return FALSE;
   end if;

   if L_from_loc_type = 'S' and L_to_loc_type = 'S' then
      if L_system_options.tsf_md_store_to_store_snd_rcv = 'S' then
         O_tsf_markdown_loc := I_from_loc;
      else
         O_tsf_markdown_loc := I_to_loc;
      end if;
   elsif  L_from_loc_type = 'W' and L_to_loc_type = 'S' then
      if L_system_options.tsf_md_wh_to_store_snd_rcv = 'S' then
         O_tsf_markdown_loc := I_from_loc;
      else
         O_tsf_markdown_loc := I_to_loc;
      end if;
   elsif  L_from_loc_type = 'S' and L_to_loc_type = 'W' then
      if L_system_options.tsf_md_store_to_wh_snd_rcv  = 'S' then
         O_tsf_markdown_loc := I_from_loc;
      else
         O_tsf_markdown_loc := I_to_loc;
      end if;
   elsif  L_from_loc_type = 'W' and L_to_loc_type = 'W' then
      if L_system_options.tsf_md_wh_to_wh_snd_rcv  = 'S' then
         O_tsf_markdown_loc := I_from_loc;
      else
         O_tsf_markdown_loc := I_to_loc;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TSF_MARKDOWN_LOC;
---------------------------------------------------------------------------------
FUNCTION POST_VAT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  IO_l10n_fin_rec   IN OUT   L10N_OBJ)
   return BOOLEAN IS

   L_program        VARCHAR2(64)   := 'STKLEDGR_SQL.POST_VAT';
   L_l10n_fin_rec   L10N_FIN_REC;

BEGIN

   L_l10n_fin_rec   := TREAT(IO_l10n_fin_rec as L10N_FIN_REC);

   P_tran_data_size                                  := P_tran_data_size + 1;
   P_tran_data_item(P_tran_data_size)                := L_l10n_fin_rec.item;
   P_tran_data_dept(P_tran_data_size)                := L_l10n_fin_rec.dept;
   P_tran_data_class(P_tran_data_size)               := L_l10n_fin_rec.class;
   P_tran_data_subclass(P_tran_data_size)            := L_l10n_fin_rec.subclass;
   P_tran_data_pack_ind(P_tran_data_size)            := L_l10n_fin_rec.tran_data.pack_ind;
   P_tran_data_location(P_tran_data_size)            := L_l10n_fin_rec.tran_data.location;
   P_tran_data_loc_type(P_tran_data_size)            := L_l10n_fin_rec.tran_data.loc_type;
   P_tran_data_tran_date(P_tran_data_size)           := TO_DATE(TO_CHAR(L_l10n_fin_rec.tran_data.tran_date,'YYYYMMDD'),'YYYYMMDD');
   P_tran_data_tran_code(P_tran_data_size)           := L_l10n_fin_rec.tran_data.tran_code;
   P_tran_data_adj_code(P_tran_data_size)            := L_l10n_fin_rec.tran_data.adj_code;
   P_tran_data_units(P_tran_data_size)               := L_l10n_fin_rec.tran_data.units;
   P_tran_data_total_cost(P_tran_data_size)          := L_l10n_fin_rec.tran_data.total_cost;
   P_tran_data_total_retail(P_tran_data_size)        := L_l10n_fin_rec.tran_data.total_retail;
   P_tran_data_ref_no_1(P_tran_data_size)            := L_l10n_fin_rec.tran_data.ref_no_1;
   P_tran_data_ref_no_2(P_tran_data_size)            := L_l10n_fin_rec.tran_data.ref_no_2;
   P_tran_data_gl_ref_no(P_tran_data_size)           := L_l10n_fin_rec.tran_data.gl_ref_no;
   P_tran_data_old_unit_retail(P_tran_data_size)     := L_l10n_fin_rec.tran_data.old_unit_retail;
   P_tran_data_new_unit_retail(P_tran_data_size)     := L_l10n_fin_rec.tran_data.new_unit_retail;
   P_tran_data_pgm_name(P_tran_data_size)            := L_l10n_fin_rec.tran_data.pgm_name;
   P_tran_data_sales_type(P_tran_data_size)          := L_l10n_fin_rec.tran_data.sales_type;
   P_tran_data_vat_rate(P_tran_data_size)            := L_l10n_fin_rec.tran_data.vat_rate;
   P_tran_data_av_cost(P_tran_data_size)             := L_l10n_fin_rec.tran_data.av_cost;
   P_tran_data_timestamp(P_tran_data_size)           := L_l10n_fin_rec.tran_data.timestamp;
   P_tran_data_ref_pack_no(P_tran_data_size)         := L_l10n_fin_rec.tran_data.ref_pack_no;
   P_tran_data_tot_cost_excl_elc(P_tran_data_size)   := L_l10n_fin_rec.tran_data.total_cost_excl_elc;
   P_tran_data_act_location(P_tran_data_size)        := NULL;
   P_tran_data_act_loc_type(P_tran_data_size)        := NULL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,L_program,NULL);
      return FALSE;
END POST_VAT;
----------------------------------------------------------------------------------
FUNCTION PROCESS_WF_WRITE_FINANCIALS_PO(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                                        I_tran_date           IN     PERIOD.VDATE%TYPE,
                                        I_item                IN     ITEM_MASTER.ITEM%TYPE,
                                        I_pack_no             IN     ITEM_MASTER.ITEM%TYPE,
                                        I_pct_in_pack         IN     NUMBER,
                                        I_dept                IN     ITEM_MASTER.DEPT%TYPE,
                                        I_class               IN     ITEM_MASTER.CLASS%TYPE,
                                        I_subclass            IN     ITEM_MASTER.SUBCLASS%TYPE,
                                        I_qty                 IN     ORDLOC.QTY_ORDERED%TYPE,
                                        I_costing_loc         IN     ITEM_LOC.LOC%TYPE,
                                        I_costing_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                                        I_loc                 IN     ITEM_LOC.LOC%TYPE,
                                        I_loc_type            IN     ITEM_LOC.LOC_TYPE%TYPE,
                                        I_weight_cuom         IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE)
RETURN BOOLEAN IS

   L_function                      VARCHAR2(60)                         := 'STKLEDGR_SQL.PROCESS_WF_WRITE_FINANCIALS_PO';
   L_system_options                SYSTEM_OPTIONS%ROWTYPE;
   L_tran_code                     TRAN_DATA.TRAN_CODE%TYPE             := NULL;
   L_tran_date                     PERIOD.VDATE%TYPE                    := TO_DATE(TO_CHAR(I_tran_date,'YYYYMMDD'),'YYYYMMDD');
   L_total_cost                    ORDLOC.UNIT_COST%TYPE                := 0;
   L_sale_price                    ITEM_LOC.UNIT_RETAIL%TYPE            := 0;
   L_sale_price_l_curr             ITEM_LOC.UNIT_RETAIL%TYPE            := 0;
   L_cost_loc_retail_l_curr        ITEM_LOC.UNIT_RETAIL%TYPE            := 0;
   L_tot_customer_cost             WF_ORDER_DETAIL.CUSTOMER_COST%TYPE   := 0;
   L_tot_comp_loc_retail_vat_adj   ITEM_LOC.UNIT_RETAIL%TYPE            := 0;
   L_tot_to_loc_cost_l_curr        ITEM_LOC_SOH.UNIT_COST%TYPE          := 0;
   L_total_retail                  ITEM_LOC.UNIT_RETAIL%TYPE            := 0;
   L_company_loc_wac               ITEM_LOC_SOH.UNIT_COST%TYPE          := 0;
   L_markup_markdown               ITEM_LOC_SOH.UNIT_COST%TYPE          := 0;
   ---- dummy variables
   L_null_cost                     TRAN_DATA.TOTAL_COST%TYPE            := NULL;
   L_std_av_cost                   ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_selling_unit_retail           ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_uom                   ITEM_LOC.SELLING_UOM%TYPE;
   L_std_cost_to_loc               ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_f_std_av_cost                 ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_f_selling_unit_retail         ITEM_LOC.UNIT_RETAIL%TYPE;
   L_f_selling_uom                 ITEM_LOC.SELLING_UOM%TYPE;
   L_f_unit_cost                   ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_f_std_cost_to_loc             ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_templ_id                      WF_COST_RELATIONSHIP.TEMPL_ID%TYPE;
   L_templ_desc                    WF_COST_BUILDUP_TMPL_HEAD.TEMPL_DESC%TYPE;
   L_margin_pct                    WF_COST_BUILDUP_TMPL_HEAD.MARGIN_PCT%TYPE;
   L_calc_type                     WF_COST_BUILDUP_TMPL_HEAD.FIRST_APPLIED%TYPE;
   L_acqui_cost                    FUTURE_COST.ACQUISITION_COST%TYPE;
   L_currency_code                 FUTURE_COST.CURRENCY_CODE%TYPE;
   L_tax_rate_retail               VAT_CODE_RATES.VAT_RATE%TYPE        := NULL;
   L_tax_rate_cost                 VAT_CODE_RATES.VAT_RATE%TYPE        := NULL;
   L_tax_code_retail               VAT_CODE_RATES.VAT_CODE%TYPE        := NULL;
   L_tax_code_cost                 VAT_CODE_RATES.VAT_CODE%TYPE        := NULL;
   ---- for updating TRAN_DATA.UNITS
   L_qty                           ORDLOC.QTY_ORDERED%TYPE             := 0;
   L_cuom                          ITEM_SUPP_COUNTRY.COST_UOM%TYPE     := NULL;
   --
   L_store_type                    STORE.STORE_TYPE%TYPE;
   L_stockholding_ind              STORE.STOCKHOLDING_IND%TYPE;
   L_f_store_total_retail          ITEM_LOC.UNIT_RETAIL%TYPE;
   L_f_store_retail                ITEM_LOC.UNIT_RETAIL%TYPE;
   L_wf_order_no                   ORDHEAD.WF_ORDER_NO%TYPE            := NULL;
   L_class_vat_ind                 CLASS.CLASS_VAT_IND%TYPE            := NULL;
   --
   L_exchange_type                 CURRENCY_RATES.EXCHANGE_TYPE%TYPE;
   L_costing_loc_currency          CURRENCIES.CURRENCY_CODE%TYPE;
   L_to_loc_currency               CURRENCIES.CURRENCY_CODE%TYPE;
   L_f_store_currency              CURRENCIES.CURRENCY_CODE%TYPE;
   L_order_currency                CURRENCIES.CURRENCY_CODE%TYPE;
   L_inv_parm                      VARCHAR2(30)                      := NULL;
   L_loc_type                      WF_ORDER_DETAIL.SOURCE_LOC_TYPE%TYPE;
   --
   L_ref_no_1                      TRAN_DATA.REF_NO_1%TYPE;

   cursor C_GET_WF_ORDER_DETAILS(IC_item IN item_master.item%TYPE) is
      select NVL(wod.fixed_cost,wod.customer_cost),
             woh.currency_code
        from wf_order_detail wod,
             wf_order_head woh,
             ordhead oh,
             item_loc il
       where woh.wf_order_no  = wod.wf_order_no
         and woh.wf_order_no  = oh.wf_order_no
         and oh.order_no      = I_order_no
         and wod.item         = IC_item
         and wod.customer_loc = I_loc
         and il.loc           = wod.customer_loc
         and il.costing_loc   = I_costing_loc;

   cursor C_GET_CURRENCY(I_loc ITEM_LOC.LOC%TYPE) is
      select currency_code
        from wh
       where wh = I_loc
      UNION
      select currency_code
        from store
       where store = I_loc;

   cursor C_GET_WF_ORDER_NO is
      select wf_order_no
        from ordhead
       where order_no = I_order_no;

BEGIN

   if I_dept is NULL then
      L_inv_parm := 'I_dept';
   elsif I_class is NULL then
      L_inv_parm := 'I_class';
   elsif I_subclass is NULL then
      L_inv_parm := 'I_subclass';
   end if;

   -- assigning package level variables to NULL to cleanup previous run values.
   LP_from_loc               := NULL;
   LP_to_loc                 := NULL;
   LP_vat_amount_rtl         := NULL;
   LP_vat_amount_cost        := NULL;
   LP_vat_amount_cost_f_curr := NULL;

   -- Setup package level variables to be used in RECORD_INSERTS
   LP_from_loc := I_costing_loc;
   LP_to_loc   := I_loc;

   if L_inv_parm is NOT NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_function,
                                             NULL);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      return FALSE;
   end if;

   select class_vat_ind
     into L_class_vat_ind
     from class
    where class = I_class
      and dept = I_dept;

   if I_tran_date is NULL then
      L_tran_date := GET_VDATE;
   else
      L_tran_date := I_tran_date;
   end if;

   open C_GET_WF_ORDER_NO;
   fetch C_GET_WF_ORDER_NO into L_wf_order_no;
   close C_GET_WF_ORDER_NO;

   if L_wf_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_FPO',
                                            I_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                 L_company_loc_wac,
                                 I_item,
                                 I_dept,
                                 I_class,
                                 I_subclass,
                                 I_costing_loc,
                                 I_costing_loc_type,
                                 L_tran_date) = FALSE then
      return FALSE;
   end if;

   if I_weight_cuom is NULL then
      L_qty := I_qty;
      L_total_cost := L_qty * L_company_loc_wac;
   else
      if NOT ITEM_SUPP_COUNTRY_SQL.GET_COST_UOM(O_error_message,
                                                L_cuom,
                                                I_pack_no) then
         return FALSE;
      end if;

      if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                               L_qty,   -- output qty
                                               I_item,
                                               I_qty,   -- input qty
                                               I_weight_cuom,
                                               L_cuom) = FALSE then
         return FALSE;
      end if;

      if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                              L_total_cost,
                                              I_pack_no,
                                              L_company_loc_wac,  -- component source loc wac
                                              I_weight_cuom,
                                              I_qty) then
         return FALSE;
      end if;
   end if;

   if I_pack_no is NOT NULL then
      open  C_GET_WF_ORDER_DETAILS(I_pack_no);
      fetch C_GET_WF_ORDER_DETAILS into L_sale_price,
                                        L_order_currency;
      close C_GET_WF_ORDER_DETAILS;

      if L_sale_price = 0 then
         if WF_ORDER_SQL.GET_COST_INFORMATION(O_error_message,
                                              L_templ_id,
                                              L_templ_desc,
                                              L_margin_pct,
                                              L_calc_type,
                                              L_sale_price,
                                              L_acqui_cost,
                                              L_currency_code,
                                              I_pack_no,
                                              I_loc) = FALSE then
            return FALSE;
         end if;
      end if;

      L_sale_price := L_sale_price * I_pct_in_pack;
   else
      open C_GET_WF_ORDER_DETAILS(I_item);
      fetch C_GET_WF_ORDER_DETAILS into L_sale_price,
                                        L_order_currency;
      close C_GET_WF_ORDER_DETAILS;

      if L_sale_price = 0 then
         if WF_ORDER_SQL.GET_COST_INFORMATION(O_error_message,
                                              L_templ_id,
                                              L_templ_desc,
                                              L_margin_pct,
                                              L_calc_type,
                                              L_sale_price,
                                              L_acqui_cost,
                                              L_currency_code,
                                              I_item,
                                              I_loc) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   open  C_GET_CURRENCY(I_costing_loc);
   fetch C_GET_CURRENCY into L_costing_loc_currency;
   close C_GET_CURRENCY;

   open  C_GET_CURRENCY(I_loc);
   fetch C_GET_CURRENCY into L_to_loc_currency;
   close C_GET_CURRENCY;

   -- Convert the unit customer cost to source location currency. This will be used for markup/markdown calculation.
   if CURRENCY_SQL.CONVERT(O_error_message,
                           L_sale_price,
                           L_order_currency,
                           L_costing_loc_currency,
                           L_sale_price_l_curr,     -- Output Retail in from-location currency.
                           'C',                          -- Using 'C' as this is Customer Cost which is posted as Retail for source location
                           L_tran_date,
                           L_exchange_type) = FALSE then
      return FALSE;
   end if;

   if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                               I_item,
                                               I_costing_loc,
                                               I_costing_loc_type,
                                               L_std_av_cost,
                                               L_std_cost_to_loc,
                                               L_cost_loc_retail_l_curr,
                                               L_selling_unit_retail,
                                               L_selling_uom) = FALSE then
      return FALSE;
   end if;

   if I_weight_cuom is NULL then
      L_tot_customer_cost := L_qty * L_sale_price_l_curr;
   else
      if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                              L_tot_customer_cost,
                                              I_pack_no,
                                              L_sale_price_l_curr,
                                              I_weight_cuom,
                                              I_qty) then
         return FALSE;
      end if;
   end if;

   -- Get the Retail tax rate at the source location.
   if I_costing_loc_type = 'S' then
      L_loc_type := 'ST';
   elsif I_costing_loc_type = 'W' then
      L_loc_type := 'WH';
   end if;

   if WF_ORDER_SQL.GET_TAX_INFO (O_error_message,
                                 L_tax_rate_cost,
                                 L_tax_code_cost,
                                 I_item,
                                 L_loc_type,
                                 I_costing_loc,
                                 I_loc,
                                 'C',
                                 L_wf_order_no) = FALSE then
      return FALSE;
   end if;
   if NVL(L_tax_rate_cost,0) = 0 then
      LP_exempt_ind := 'Y';
   end if;
   -- Set the cost tax code to LP_vat_code_cost_rtl so that it will be written to gl_ref_no on tran_data for tran_code 87.
   LP_vat_code_cost_rtl := L_tax_code_cost;

   -- Get the cost vat amount to post 87 at franchise location
   LP_vat_amount_cost := L_tot_customer_cost * nvl(L_tax_rate_cost,0)/100;

   if NVL(L_costing_loc_currency,'X') <> NVL(L_to_loc_currency,'X') then
      if CURRENCY_SQL.CONVERT(O_error_message,
                              LP_vat_amount_cost,
                              L_costing_loc_currency,
                              L_to_loc_currency,
                              LP_vat_amount_cost_f_curr,     -- Output Vat Amount in franchise store currency
                              'C',
                              L_tran_date,
                              L_exchange_type) = FALSE then
         return FALSE;
      end if;
   else
      LP_vat_amount_cost_f_curr := LP_vat_amount_cost;
   end if;

   if WF_ORDER_SQL.GET_TAX_INFO (O_error_message,
                                 L_tax_rate_retail,
                                 L_tax_code_retail,
                                 I_item,
                                 L_loc_type,
                                 I_costing_loc,
                                 I_loc,
                                 'R',
                                 L_wf_order_no) = FALSE then
      return FALSE;
   end if;
   if NVL(L_tax_rate_retail,0) = 0 then
      LP_exempt_ind := 'Y';
   end if;
   -- Get the retail vat amount to post 88 at company location
   LP_vat_amount_rtl := L_tot_customer_cost * nvl(L_tax_rate_retail,0)/100;


   -- VAT postings for Franchise to be routed through Custom Tax calls based on vat_calc_type in vat_region.
   -- The value for Retail is modified to add/remove VAT based on STKLDGR_VAT_INCL_RETL_IND,CLASS_LEVEL_VAT_IND
   -- and CLASS.CLASS_VAT_IND before the value is passed to BUILD_TRAN_DATA_INSERT where it will not be changed.
   if L_system_options.stkldgr_vat_incl_retl_ind = 'Y' then
      -- Add VAT to customer price
      L_tot_comp_loc_retail_vat_adj := L_tot_customer_cost * (1 + nvl(L_tax_rate_retail,0)/100);
      L_sale_price_l_curr := L_sale_price_l_curr * (1 + nvl(L_tax_rate_retail,0)/100);
      if (L_system_options.class_level_vat_ind = 'Y' and L_class_vat_ind ='N') then
         --  VAT to be added
         L_cost_loc_retail_l_curr := L_cost_loc_retail_l_curr * (1 + nvl(L_tax_rate_retail,0)/100);
      end if;
   else -- L_system_options.stkldgr_vat_incl_retl_ind = 'N'
      L_tot_comp_loc_retail_vat_adj := L_tot_customer_cost ;
      if L_system_options.class_level_vat_ind = 'N' or (L_system_options.class_level_vat_ind = 'Y' and L_class_vat_ind ='Y') then
         -- VAT to be removed
         L_cost_loc_retail_l_curr := L_cost_loc_retail_l_curr /(1 + nvl(L_tax_rate_retail,0)/100);
      end if;
   end if;
   --
   L_tran_code := 82;

   if STORE_ATTRIB_SQL.GET_STOCKHOLDING_IND(O_error_message,
                                            L_stockholding_ind,
                                            I_loc) = FALSE then
      return FALSE;
   end if;

   select max(tsf_no)
     into L_ref_no_1
    from tsfhead
   where order_no = I_order_no
     and from_loc = I_costing_loc
     and from_loc_type = I_costing_loc_type
     and to_loc = I_loc
     and to_loc_type = 'S';

   -- write Franchise Sales record
   if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                          I_item,
                                          I_dept,
                                          I_class,
                                          I_subclass,
                                          I_costing_loc,
                                          I_costing_loc_type,
                                          L_tran_date,
                                          L_tran_code,
                                          NULL,
                                          L_qty,
                                          L_total_cost,                    --Total Cost
                                          L_tot_comp_loc_retail_vat_adj,   --Total Retail
                                          L_cost_loc_retail_l_curr,
                                          L_ref_no_1,                      --I_ref_no_1
                                          L_wf_order_no,                   --I_ref_no_2
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          L_function,
                                          L_tax_code_retail,               --gl_ref_no, use retail tax_code for tran_code 82
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NVL(L_tax_rate_retail,0),
                                          NULL,
                                          I_pack_no,
                                          NULL) = FALSE then
      return FALSE;
   end if;


   if L_sale_price_l_curr != L_cost_loc_retail_l_curr then

      if L_cost_loc_retail_l_curr > L_sale_price_l_curr then
         L_tran_code := 85;
      else
         L_tran_code := 84;
      end if;

      L_total_retail := L_qty * ABS(L_cost_loc_retail_l_curr - L_sale_price_l_curr);

      -- Include vat to the markdown retail amount for markdown/markup calculation.
      L_markup_markdown := NULL;
      -- write Franchise Markup/Markdown record
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             I_dept,
                                             I_class,
                                             I_subclass,
                                             I_costing_loc,
                                             I_costing_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,
                                             L_qty,
                                             L_null_cost,      -- Total Cost
                                             L_total_retail,   -- Total Retail
                                             NULL,
                                             I_order_no,       --I_ref_no_1
                                             L_wf_order_no,    --I_ref_no_2
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_function,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NVL(L_tax_rate_retail,0),
                                             NULL,
                                             I_pack_no,
                                             NULL) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
   return FALSE;
END PROCESS_WF_WRITE_FINANCIALS_PO;
--------------------------------------------------------------------------
FUNCTION WF_WRITE_FINANCIALS_PO(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                                I_tran_date           IN     PERIOD.VDATE%TYPE,
                                I_item                IN     ITEM_MASTER.ITEM%TYPE,
                                I_pack_no             IN     ITEM_MASTER.ITEM%TYPE,
                                I_pct_in_pack         IN     NUMBER,
                                I_dept                IN     ITEM_MASTER.DEPT%TYPE,
                                I_class               IN     ITEM_MASTER.CLASS%TYPE,
                                I_subclass            IN     ITEM_MASTER.SUBCLASS%TYPE,
                                I_qty                 IN     ORDLOC.QTY_ORDERED%TYPE,
                                I_costing_loc         IN     ITEM_LOC.LOC%TYPE,
                                I_costing_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                                I_loc                 IN     ITEM_LOC.LOC%TYPE,
                                I_loc_type            IN     ITEM_LOC.LOC_TYPE%TYPE,
                                I_weight_cuom         IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE)
RETURN BOOLEAN IS

   L_program                      VARCHAR2(64) := 'STKLEDGR_SQL.WF_WRITE_FINANCIALS';
   L_deposit_item_type            ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE;
   L_container_item               ITEM_MASTER.CONTAINER_ITEM%TYPE;
   L_container_rec                ITEM_MASTER%ROWTYPE;

   cursor C_ITEM_TYPE is
      select deposit_item_type,
             container_item
        from item_master
       where item = I_item;
BEGIN
    -- Check if the input item is a deposit content. Include deposit container also.
   open  C_ITEM_TYPE;
   fetch C_ITEM_TYPE into L_deposit_item_type,
                          L_container_item;
   close C_ITEM_TYPE;

   -- Ignore deposit container item to avoid duplicate posting.
   -- Deposit container item records will be written based on content item.
   if NVL(L_deposit_item_type,'X') = 'A' then
      return TRUE;
   end if;

   LP_franchise_transaction := 'Y';
   -- Write the Deposit container record if the input item is a deposit content
   if NVL(L_deposit_item_type,'X') = 'E' then
      -- Get the container_item details
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_container_rec,
                                         L_container_item) = FALSE then
         return FALSE;
      end if;
      ---
      if PROCESS_WF_WRITE_FINANCIALS_PO(O_error_message,
                                        I_order_no,
                                        I_tran_date,
                                        L_container_item,
                                        NULL,      -- pack_no
                                        NULL,      -- pct_in_pack
                                        L_container_rec.dept,
                                        L_container_rec.class,
                                        L_container_rec.subclass,
                                        I_qty,
                                        I_costing_loc,
                                        I_costing_loc_type,
                                        I_loc,
                                        I_loc_type,
                                        I_weight_cuom) = FALSE then
         return FALSE;
      end if;
   end if;

   if PROCESS_WF_WRITE_FINANCIALS_PO(O_error_message,
                                     I_order_no,
                                     I_tran_date,
                                     I_item,
                                     I_pack_no,
                                     I_pct_in_pack,
                                     I_dept,
                                     I_class,
                                     I_subclass,
                                     I_qty,
                                     I_costing_loc,
                                     I_costing_loc_type,
                                     I_loc,
                                     I_loc_type,
                                     I_weight_cuom) = FALSE then
      return FALSE;
   end if;
   LP_franchise_transaction := NULL;
   return TRUE;

EXCEPTION
   when OTHERS then
      LP_franchise_transaction := NULL;
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END WF_WRITE_FINANCIALS_PO;
--------------------------------------------------------------------------------------------
FUNCTION LIBRARY_TRAN_DATA_INSERT(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_item                 IN      TRAN_DATA.ITEM%TYPE,
                                  I_dept                 IN      TRAN_DATA.DEPT%TYPE,
                                  I_class                IN      TRAN_DATA.CLASS%TYPE,
                                  I_subclass             IN      TRAN_DATA.SUBCLASS%TYPE,
                                  I_location             IN      TRAN_DATA.LOCATION%TYPE,
                                  I_loc_type             IN      TRAN_DATA.LOC_TYPE%TYPE,
                                  I_tran_date            IN      TRAN_DATA.TRAN_DATE%TYPE,
                                  IO_tran_code           IN OUT  TRAN_DATA.TRAN_CODE%TYPE,
                                  I_adj_code             IN      TRAN_DATA.ADJ_CODE%TYPE,
                                  I_units                IN      TRAN_DATA.UNITS%TYPE,
                                  IO_total_cost          IN OUT  TRAN_DATA.TOTAL_COST%TYPE,
                                  I_total_retail         IN      TRAN_DATA.TOTAL_RETAIL%TYPE,
                                  I_ref_no_1             IN      TRAN_DATA.REF_NO_1%TYPE,
                                  I_ref_no_2             IN      TRAN_DATA.REF_NO_2%TYPE,
                                  I_tsf_source_location  IN      TRAN_DATA.LOCATION%TYPE,
                                  I_tsf_source_loc_type  IN      TRAN_DATA.LOC_TYPE%TYPE,
                                  I_old_unit_retail      IN      TRAN_DATA.OLD_UNIT_RETAIL%TYPE,
                                  I_new_unit_retail      IN      TRAN_DATA.NEW_UNIT_RETAIL%TYPE,
                                  I_source_dept          IN      TRAN_DATA.DEPT%TYPE,
                                  I_source_class         IN      TRAN_DATA.CLASS%TYPE,
                                  I_source_subclass      IN      TRAN_DATA.SUBCLASS%TYPE,
                                  I_pgm_name             IN      TRAN_DATA.PGM_NAME%TYPE,
                                  I_gl_ref_no            IN      TRAN_DATA.GL_REF_NO%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)     := 'STKLEDGR_SQL.LIBRARY_TRAN_DATA_INSERT';

BEGIN
   --call build
   if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                          I_item,
                                          I_dept,
                                          I_class,
                                          I_subclass,
                                          I_location,
                                          I_loc_type,
                                          I_tran_date,
                                          IO_tran_code,
                                          I_adj_code,
                                          I_units,
                                          IO_total_cost,
                                          I_total_retail,
                                          I_ref_no_1,
                                          I_ref_no_2,
                                          I_tsf_source_location,
                                          I_tsf_source_loc_type,
                                          I_old_unit_retail,
                                          I_new_unit_retail,
                                          I_source_dept,
                                          I_source_class,
                                          I_source_subclass,
                                          I_pgm_name,
                                          I_gl_ref_no) = FALSE then

      return FALSE;
   end if;

   if P_tran_data_size > 0 then
      SQL_LIB.SET_MARK('INSERT',NULL,'tran_data',NULL);
      insert into tran_data (item,
                             dept,
                             class,
                             subclass,
                             pack_ind,
                             location,
                             loc_type,
                             tran_date,
                             tran_code,
                             adj_code,
                             units,
                             total_cost,
                             total_retail,
                             ref_no_1,
                             ref_no_2,
                             gl_ref_no,
                             old_unit_retail,
                             new_unit_retail,
                             pgm_name,
                             sales_type,
                             vat_rate,
                             av_cost,
                             timestamp)
                      values(P_tran_data_item(P_tran_data_size),
                             P_tran_data_dept(P_tran_data_size),
                             P_tran_data_class(P_tran_data_size),
                             P_tran_data_subclass(P_tran_data_size),
                             P_tran_data_pack_ind(P_tran_data_size),
                             P_tran_data_location(P_tran_data_size),
                             P_tran_data_loc_type(P_tran_data_size),
                             P_tran_data_tran_date(P_tran_data_size),
                             P_tran_data_tran_code(P_tran_data_size),
                             P_tran_data_adj_code(P_tran_data_size),
                             P_tran_data_units(P_tran_data_size),
                             P_tran_data_total_cost(P_tran_data_size),
                             P_tran_data_total_retail(P_tran_data_size),
                             P_tran_data_ref_no_1(P_tran_data_size),
                             P_tran_data_ref_no_2(P_tran_data_size),
                             P_tran_data_gl_ref_no(P_tran_data_size),
                             P_tran_data_old_unit_retail(P_tran_data_size),
                             P_tran_data_new_unit_retail(P_tran_data_size),
                             P_tran_data_pgm_name(P_tran_data_size),
                             P_tran_data_sales_type(P_tran_data_size),
                             P_tran_data_vat_rate(P_tran_data_size),
                             P_tran_data_av_cost(P_tran_data_size),
                             P_tran_data_timestamp(P_tran_data_size));
      P_tran_data_size := P_tran_data_size - 1;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END LIBRARY_TRAN_DATA_INSERT;
--------------------------------------------------------------------------------------------
END STKLEDGR_SQL;
/