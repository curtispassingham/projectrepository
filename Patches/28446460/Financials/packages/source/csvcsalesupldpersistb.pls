create or replace
PACKAGE BODY CORESVC_SALES_UPLOAD_PRST_SQL AS
 
   LP_system_options_rec    SYSTEM_OPTIONS%ROWTYPE;
   LP_user                  SVCPROV_CONTEXT.USER_NAME%TYPE   := GET_USER;
----------------------------------------------------------------------------------------------
$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then
   ---
   -------------------------------------------------------------------------------------------
   -- Function Name: LOCK_TABLES
   -- Purpose      : This function will check if sales transaction tables are locked.
   --                These tables are to be updated by succeeding processing of this
   --                package and record locking will be checked prior to processing.
   -------------------------------------------------------------------------------------------
   FUNCTION LOCK_TABLES(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_sales_process_id   IN              NUMBER,
                        I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_ITEM_LOC_SOH
   -- Purpose      : This function will modify associated inventory columns at ITEM_LOC_SOH table
   --                based on the uploaded sales quantity and other item specifications.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_ITEM_LOC_SOH(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_sales_process_id   IN              NUMBER,
                                 I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_DAILY_SALES_DISCOUNT
   -- Purpose      : This function will modify associated columns at DAILY_SALES_DISCOUNT
   --                based on the uploaded discounted sales quantity and amount.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_DAILY_SALES_DISCOUNT(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_sales_process_id   IN              NUMBER,
                                         I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_WASTAGE
   -- Purpose      : This function will post transaction data and information (TRAN_DATA)
   --                where tran_code is 13. This function is being invoked by PERSIST_TRAN_DATA
   --                function.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_WASTAGE(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id   IN              NUMBER,
                                      I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_SNAPSHOT
   -- Purpose      : This function will update the records found at EDI_DAILY_SALES when applicable.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_SNAPSHOT(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sales_process_id   IN              NUMBER,
                             I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_DEAL_INCOME
   -- Purpose      : This function will post adjustments to deal income (Billback Sales) posted
   --                for backposted transactions. Tran_code = 6. This is being invoked by
   --                PERSIST_TRAN_DATA function.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_DEAL_INCOME(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                          I_sales_process_id   IN              NUMBER,
                                          I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_DEAL_ACTUALS_IL
   -- Purpose      : This function will update deal_actuals_item_loc table and update turnover
   --                units with sales quantity and revenue with associated sales amount.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_DEAL_ACTUALS_IL(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id   IN              NUMBER,
                                    I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_VAT_HISTORY
   -- Purpose      : This function will update VAT_HISTORY table with associated VAT amount on sales.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_VAT_HISTORY(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sales_process_id   IN              NUMBER,
                                I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_EDI_DAILY_SALES
   -- Purpose      : This function will update the EDI_DAILY_SALES table based on values associated
   --                with sales posted.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_EDI_DAILY_SALES(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id   IN              NUMBER,
                                    I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_ITEM_LOC_HIST
   -- Purpose      : This function will update the ITEM_LOC_HIST table on the retail change
   --                associated with the POSU sales file uploaded.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_ITEM_LOC_HIST(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_sales_process_id   IN              NUMBER,
                                  I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   ------------------------------------------------------------------------
   -- Function Name: PERSIST_ITEM_LOC_HIST_MTH
   -- Purpose      : This function will update the ITEM_LOC_HIST_MTH table on the retail change
   --                associated with the POSU sales file uploaded if reporting period is Monthly.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_ITEM_LOC_HIST_MTH(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id   IN              NUMBER,
                                      I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------------------
   -- Function Name: PERSIST_CONCESSION_DATA
   -- Purpose      : This function will insert new records on the CONCESSION_DATA table on
   --                the event of sales posted for concession items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_CONCESSION_DATA(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id   IN              NUMBER,
                                    I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_CONSIGNMENT_DATA
   -- Purpose      : This function will post the transactions related to sales posted for items
   --                belonging to consignment departments.
   --                Post transaction data for purchase, update RTV tables, post records related to
   --                invoice tables.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_CONSIGNMENT_DATA(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_sales_process_id   IN              NUMBER,
                                     I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: LOCK_CONSIGNMENT_TABLES
   -- Purpose      : This function will lock the associated tables related to consignment transactions
   --                such as RTV_HEAD and INVC_MERCH_VAT.
   -------------------------------------------------------------------------------------------
   FUNCTION LOCK_CONSIGNMENT_TABLES(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id   IN              NUMBER,
                                    I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_RTV_HEAD
   -- Purpose      : This function will update the RTV tables related to the transaction posted
   --                for consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_RTV_HEAD(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sales_process_id   IN              NUMBER,
                             I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_ORDHEAD
   -- Purpose      : This function will update the order tables related to the transaction posted
   --                for consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_ORDHEAD(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_sales_process_id   IN              NUMBER,
                            I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_INVC_HEAD
   -- Purpose      : This function will update the inventory table (header) related to the
   --                transactions posted for consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_INVC_HEAD(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_sales_process_id   IN              NUMBER,
                              I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_INVC_DETAIL
   -- Purpose      : This function will update the inventory tables (detail) related to the
   --                transactions posted for consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_INVC_DETAIL(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sales_process_id   IN              NUMBER,
                                I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
      -- Function Name: PERSIST_INVC_MERCH_VAT
      -- Purpose      : This function will update the inventory tables (vat amount related to
      --                inventory) related to the transactions posted for consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_INVC_MERCH_VAT(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id   IN              NUMBER,
                                   I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_CONSIGN_TRAN_DATA
   -- Purpose      : This function will post regular transaction data to stock ledger table
   --                TRAN_DATA related to consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_CONSIGN_TRAN_DATA(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id   IN              NUMBER,
                                      I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_CONSIGN_VAT_OUT
   -- Purpose      : This function will post VAT related transaction data to stock ledger table
   --                TRAN_DATA (tran code 88) related to consignment items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_CONSIGN_VAT_OUT(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id   IN              NUMBER,
                                    I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA
   -- Purpose      : This function will post transaction data to stock ledger tables
   --                TRAN_DATA related to sales transactions. This will call several functions
   --                that will post the different tran_data with different transaction codes
   --                relevant to the item and parameter configurations.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_sales_process_id   IN              NUMBER,
                              I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_SALE
   -- Purpose      : This function will post transaction data to stock ledger tables.
   --                Tran_code 1 and 3=sales. This function is being invoked by PERSIST_TRAN_DATA.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_SALE(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id   IN              NUMBER,
                                   I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_RETURN
   -- Purpose      : This function will post transaction data to stock ledger tables.
   --                Tran_code 4 =returns. This function is being invoked by PERSIST_TRAN_DATA.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_RETURN(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_sales_process_id   IN              NUMBER,
                                     I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_REVERSAL
   -- Purpose      : This function will post transaction data to stock ledger tables.
   --                Tran_code 13 and 14=markdown (different retail values).
   --                This function is being invoked by PERSIST_TRAN_DATA.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_REVERSAL(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_sales_process_id   IN              NUMBER,
                                       I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_VAT_OUT
   -- Purpose      : This function will post VAT related transaction data to stock ledger table
   --                TRAN_DATA (tran code 88) related to regular items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_VAT_OUT(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id   IN              NUMBER,
                                      I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_EMP_DISC
   -- Purpose      : This function will post transaction data related to employee discount promotion.
   --                TRAN_DATA (tran code 60) related to sales.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_EMP_DISC(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_sales_process_id   IN              NUMBER,
                                       I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_OFF_RETAIL
   -- Purpose      : This function will post transaction data related to off-retail promotions.
   --                TRAN_DATA (tran code 15) related to sales.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_OFF_RETAIL(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_sales_process_id   IN              NUMBER,
                                         I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_PACK_UP_DOWN
   -- Purpose      : This function will post transaction data related to pack items.
   --                TRAN_DATA (tran code 11 and 13) related to sales.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_PACK_UP_DOWN(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                           I_sales_process_id   IN              NUMBER,
                                           I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_PROMO_MKDN
   -- Purpose      : This function will post transaction data related to promotional markdowns.
   --                TRAN_DATA (tran code 15) related to sales.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_PROMO_MKDN(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_sales_process_id   IN              NUMBER,
                                         I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_TRAN_DATA_WEIGHT_VAR
   -- Purpose      : This function will post transaction data related to weight variances.
   --                TRAN_DATA (tran code 10) related to catchweight items.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_TRAN_DATA_WEIGHT_VAR(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_sales_process_id   IN              NUMBER,
                                         I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_CO_RTN_BOOK_TSF
   -- Purpose      : This function will create a book transfer for customer order returns
   --                originating from OMS.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_CO_RTN_BOOK_TSF(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id   IN              NUMBER,
                                    I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------
   -- Function Name: PERSIST_CUST_ORDER_RESV
   -- Purpose      : This function will knock off the customer reserve bucket for external
   --                customer order sales.
   -------------------------------------------------------------------------------------------
   FUNCTION PERSIST_CUST_ORDER_RESV(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id   IN              NUMBER,
                                    I_chunk_id           IN              NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------

$end
----------------------------------------------------------------------------------------
FUNCTION LOCK_TABLES(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_sales_process_id   IN              NUMBER,
                     I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_table                 VARCHAR2(30) := null;
   L_vdate                 DATE := GET_VDATE();
   ---
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_retry_lock_attempts   RMS_PLSQL_BATCH_CONFIG.RETRY_LOCK_ATTEMPTS%TYPE;
   L_retry_wait_time       RMS_PLSQL_BATCH_CONFIG.RETRY_WAIT_TIME%TYPE;
   L_locked_acquired       VARCHAR2(1) := 'Y';

   cursor C_GET_LOCK_PARAM is
      select NVL(retry_lock_attempts,2),
             NVL(retry_wait_time,0)
        from rms_plsql_batch_config
       where program_name = 'CORESVC_SALES_UPLOAD_SQL';

   cursor C_LOCK_ITEM_LOC_SOH is
      select 'x'
        from svc_posupld_line_item_gtt pli,
             item_loc_soh soh
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id         = I_chunk_id
         and pli.error_msg        is null
         and soh.item             = pli.im_item
         and soh.loc              = pli.loc_value
         and ((pli.im_item_xform_ind = 'N')
              or
              (pli.im_item_xform_ind = 'Y' and pli.xform_sellable_item is not null))
         for update of soh.stock_on_hand nowait;

   cursor C_LOCK_ITEM_LOC_HIST is
      select 'x'
        from svc_posupld_line_item_gtt pli,
             item_loc_hist hist
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id         = I_chunk_id
         and pli.error_msg        is null
         and hist.item            = pli.im_item
         and hist.sales_type      = pli.sales_type
         and hist.loc             = pli.loc_value
         and hist.eow_date        = pli.eow_date
         and pli.im_pack_ind      = 'N'
         for update of hist.sales_issues nowait;

   cursor C_LOCK_ITEM_LOC_HIST_MTH is
      select 'x'
        from svc_posupld_line_item_gtt pli,
             item_loc_hist_mth hist
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id         = I_chunk_id
         and pli.error_msg        is null
         and hist.item            = pli.im_item
         and hist.loc             = pli.loc_value
         and hist.eom_date        = pli.eom_date
         and pli.im_pack_ind      = 'N'
         for update of hist.sales_issues nowait;

   cursor C_LOCK_VAT_HISTORY is
      select 'x'
        from svc_posupld_line_item_gtt pli,
             vat_history vh
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id         = I_chunk_id
         and pli.error_msg        is null
         and vh.item              = pli.im_item
         and vh.store             = pli.loc_value
         and vh.tran_date         = pli.tran_date
         and pli.im_pack_ind      = 'N'
         for update of vh.vat_amt nowait;

   cursor C_LOCK_EDI_DAILY_SALES is
      select 'x'
        from svc_posupld_line_item_gtt pli,
             edi_daily_sales edi
       where pli.sales_process_id         = I_sales_process_id
         and pli.chunk_id                 = I_chunk_id
         and pli.error_msg                is null
         and edi.item                     = pli.im_item
         and edi.loc                      = pli.loc_value
         and edi.loc_type                 = 'S'
         and edi.tran_date                = pli.tran_date
         and pli.sups_edi_sales_rpt_freq  = 'D'
         and pli.tran_date       >= L_vdate
         for update of edi.sales_qty nowait;

   cursor C_LOCK_DEAL_ACTUALS_ITEM_LOC is
      select 'x'
        from svc_posupld_item_disc_gtt plid,
             svc_posupld_line_item_gtt pli,
             deal_head dh,
             deal_comp_prom dcp,
             deal_actuals_forecast daf,
             deal_actuals_item_loc dail
       where pli.sales_process_id  = I_sales_process_id
         and pli.chunk_id          = I_chunk_id
         and pli.error_msg         is null
         and pli.is_consignment_rate is not null
         and pli.sales_process_id = plid.sales_process_id
         and pli.chunk_id = plid.chunk_id
         and pli.posupld_line_item_id  = plid.posupld_line_item_id
         and dcp.promotion_id      = plid.prom_no
         and dcp.promo_comp_id     = plid.prom_component
         and dcp.deal_id           = dh.deal_id
         and daf.deal_id           = dcp.deal_id
         and daf.deal_detail_id    = dcp.deal_detail_id
         and dh.status            != 'C'
         and daf.reporting_date    in (select min(reporting_date)
                                         from deal_actuals_forecast daf2
                                        where daf2.deal_id          = dcp.deal_id
                                          and daf2.deal_detail_id   = dcp.deal_detail_id
                                          and daf2.reporting_date  >= get_vdate)
         and dail.deal_id          = dcp.deal_id
         and dail.deal_detail_id   = dcp.deal_detail_id
         and dail.reporting_date   = daf.reporting_date
         and dail.item             = pli.im_item
         and dail.location         = pli.loc_value
         and dail.loc_type         = 'S'
         and dail.order_no         is NULL
         for update of dail.actual_turnover_units nowait;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_LOCK_PARAM;
   fetch C_GET_LOCK_PARAM into L_retry_lock_attempts,
                               L_retry_wait_time;
   close C_GET_LOCK_PARAM;
   --
   L_table := 'ITEM_LOC_SOH';
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   FOR i in 1..(l_retry_lock_attempts - 1) LOOP
      ---
      BEGIN
         open C_LOCK_ITEM_LOC_SOH;
         close C_LOCK_ITEM_LOC_SOH;
         L_locked_acquired := 'Y';
         exit;
         --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      --
      dbms_lock.sleep(L_retry_wait_time);
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      open C_LOCK_ITEM_LOC_SOH;
      close C_LOCK_ITEM_LOC_SOH;
   end if;

   L_locked_acquired := 'N';
   -----------------------------------------------------
   L_table := 'ITEM_LOC_HIST';
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   FOR i in 1..(l_retry_lock_attempts - 1) LOOP
      ---
      BEGIN
         open C_LOCK_ITEM_LOC_HIST;
         close C_LOCK_ITEM_LOC_HIST;
         L_locked_acquired := 'Y';
         exit;
         --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      dbms_lock.sleep(L_retry_wait_time);
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      open C_LOCK_ITEM_LOC_HIST;
      close C_LOCK_ITEM_LOC_HIST;
   end if;

   L_locked_acquired := 'N';
   ----------------------------------------------------
   L_table := 'VAT_HISTORY';
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   FOR i in 1..(l_retry_lock_attempts - 1) LOOP
      ---
      BEGIN
         open C_LOCK_VAT_HISTORY;
         close C_LOCK_VAT_HISTORY;
         L_locked_acquired := 'Y';
         exit;
      --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      dbms_lock.sleep(L_retry_wait_time);
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      open C_LOCK_VAT_HISTORY;
      close C_LOCK_VAT_HISTORY;
   end if;

   L_locked_acquired := 'N';
   ----------------------------------------------------
   L_table := 'EDI_DAILY_SALES';
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   FOR i in 1..(l_retry_lock_attempts - 1) LOOP
      ---
      BEGIN
         open C_LOCK_EDI_DAILY_SALES;
         close C_LOCK_EDI_DAILY_SALES;
         L_locked_acquired := 'Y';
         exit;
      --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      dbms_lock.sleep(L_retry_wait_time);
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      open C_LOCK_EDI_DAILY_SALES;
      close C_LOCK_EDI_DAILY_SALES;
   end if;

   L_locked_acquired := 'N';
   ----------------------------------------------------
   L_table := 'DEAL_ACTUALS_ITEM_LOC';
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   FOR i in 1..(l_retry_lock_attempts - 1) LOOP
      ---
      BEGIN
         open C_LOCK_DEAL_ACTUALS_ITEM_LOC;
         close C_LOCK_DEAL_ACTUALS_ITEM_LOC;
         L_locked_acquired := 'Y';
         exit;
         --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      dbms_lock.sleep(L_retry_wait_time);
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      open C_LOCK_DEAL_ACTUALS_ITEM_LOC;
      close C_LOCK_DEAL_ACTUALS_ITEM_LOC;
   end if;
   ----------------------------------------------------
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            null,
                                            null);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.LOCK_TABLES',
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_TABLES;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_ITEM_LOC_SOH(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_sales_process_id   IN              NUMBER,
                              I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_vdate                   PERIOD.VDATE%TYPE := GET_VDATE();
   L_stock_count_processed   BOOLEAN := FALSE;
   L_cycle_count             STAKE_HEAD.CYCLE_COUNT%TYPE := NULL;
   L_sales_qty               ITEM_LOC_SOH.QTY_SOLD%TYPE;

   cursor C_GET_ITEM_LOC is
      select pli.im_item,
             pli.loc_value,
             (pli.xform_yield_pct *
              (pli.tran_type_num * pli.total_wastage_qty * pli.pack_qty ) /
              (1-pli.xform_prod_loss_pct)) qty,
             pli.tran_date,
             pli.eow_date,
             pli.im_pack_ind,
             pli.pack_comp_ind,
             pli.im_inventory_ind
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id         = I_chunk_id         
         and pli.is_concession_rate  is NULL
         and pli.is_consignment_rate is NULL
         and pli.error_msg           is NULL
         and ((pli.im_item_xform_ind = 'N')
               or (pli.im_item_xform_ind = 'Y' and pli.xform_sellable_item is NOT NULL))
         and NOT (pli.im_item_xform_ind = 'N' and pli.tran_type_char = 'R' and NVL(pli.subtrans_type_ind,'X') = 'D')
    order by pli.im_item,
             pli.loc_value,
             pli.tran_date;

   TYPE ilsoh_type is TABLE of C_GET_ITEM_LOC%ROWTYPE INDEX BY BINARY_INTEGER;

   L_ilsoh_tab   ILSOH_TYPE;

BEGIN

   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_ITEM_LOC;
   fetch C_GET_ITEM_LOC bulk collect into L_ilsoh_tab;
   close C_GET_ITEM_LOC;

   ---
   if L_ilsoh_tab.FIRST is NOT NULL then
      FOR soh_rec in L_ilsoh_tab.FIRST .. L_ilsoh_tab.LAST LOOP
         L_sales_qty := L_ilsoh_tab(soh_rec).qty;
         if L_ilsoh_tab(soh_rec).tran_date < L_vdate then
            if STKCNT_ATTRIB_SQL.STOCK_COUNT_PROCESSED(O_error_message,
                                                       L_stock_count_processed,
                                                       L_cycle_count,
                                                       L_ilsoh_tab(soh_rec).tran_date,
                                                       L_ilsoh_tab(soh_rec).im_item,
                                                       'S',
                                                       L_ilsoh_tab(soh_rec).loc_value) = FALSE then
               return FALSE;
            end if;

            if L_stock_count_processed then
               L_ilsoh_tab(soh_rec).qty := 0;
            end if;
         end if;

         update item_loc_soh ils
            set ils.stock_on_hand         = decode(L_ilsoh_tab(soh_rec).im_inventory_ind,
                                                   'Y', decode(L_ilsoh_tab(soh_rec).im_pack_ind,
                                                               'Y', decode(L_ilsoh_tab(soh_rec).pack_comp_ind,
                                                                           'N', ils.stock_on_hand,
                                                                           ils.stock_on_hand - L_ilsoh_tab(soh_rec).qty),
                                                                    ils.stock_on_hand - L_ilsoh_tab(soh_rec).qty),
                                                        -- non-inventory item does not keep SOH
                                                        ils.stock_on_hand),
                ils.last_hist_export_date = decode(L_ilsoh_tab(soh_rec).im_inventory_ind,
                                                   'Y', (case
                                                        when L_ilsoh_tab(soh_rec).im_pack_ind = 'Y' and L_ilsoh_tab(soh_rec).pack_comp_ind = 'N' then
                                                           decode(ils.last_hist_export_date,
                                                                  NULL, L_vdate,
                                                                  decode(sign(ils.last_hist_export_date - L_vdate),
                                                                         -1, L_vdate,
                                                                             ils.last_hist_export_date))
                                                        else
                                                           decode(SIGN(L_ilsoh_tab(soh_rec).eow_date - NVL(ils.last_hist_export_date, L_ilsoh_tab(soh_rec).eow_date - 1)),
                                                                  1, ils.last_hist_export_date,
                                                                     L_ilsoh_tab(soh_rec).eow_date - 7)
                                                        end),
                                                        ils.last_hist_export_date),
                ils.last_update_datetime  = SYSDATE,
                ils.soh_update_datetime   = decode(L_ilsoh_tab(soh_rec).im_inventory_ind,
                                                   'Y', decode(L_ilsoh_tab(soh_rec).im_pack_ind,
                                                               'Y', decode(L_ilsoh_tab(soh_rec).pack_comp_ind,
                                                                           'Y', SYSDATE,
                                                                                NULL),
                                                                    SYSDATE),
                                                        ils.soh_update_datetime),
                ils.last_update_id        = LP_user,
                -- non-inventory item is allowed to have values in the following columns for informative purposes
                ils.first_sold            = NVL(ils.first_sold, L_ilsoh_tab(soh_rec).tran_date),
                ils.last_sold             = L_ilsoh_tab(soh_rec).tran_date,
                ils.qty_sold              = L_sales_qty
         where ils.item = L_ilsoh_tab(soh_rec).im_item
           and ils.loc = L_ilsoh_tab(soh_rec).loc_value;
      END LOOP;
   end if;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_ITEM_LOC_SOH',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_ITEM_LOC_SOH;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_DAILY_SALES_DISCOUNT(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id   IN              NUMBER,
                                      I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_table                 VARCHAR2(30) := null;
   L_vdate                 DATE := GET_VDATE();
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_retry_lock_attempts   RMS_PLSQL_BATCH_CONFIG.RETRY_LOCK_ATTEMPTS%TYPE;
   L_retry_wait_time       RMS_PLSQL_BATCH_CONFIG.RETRY_WAIT_TIME%TYPE;
   L_locked_acquired       VARCHAR2(1) := 'Y';

   cursor C_GET_LOCK_PARAM is
      select NVL(retry_lock_attempts,2),
             NVL(retry_wait_time,0)
        from rms_plsql_batch_config
       where program_name = 'CORESVC_SALES_UPLOAD_SQL';

   cursor C_LOCK_DSD is
      select 'x'
        from svc_posupld_dsd_helper_gtt h,
             daily_sales_discount dsd
       where h.sales_process_id    = I_sales_process_id
         and h.chunk_id            = I_chunk_id
         and dsd.item              = h.item
         and dsd.store             = h.store
         and dsd.prom_type         = h.prom_type
         and dsd.data_date         = h.data_date
         and dsd.tran_type         = h.tran_type
         and NVL(dsd.promotion,-999) = NVL(h.promotion,-999)
         and NVL(dsd.prom_component,-999) = NVL(h.prom_component,-999)
         for update of dsd.sales_qty nowait;

   cursor C_DSD_DETAILS is
      select h.item,
             h.store,
             h.prom_type,
             h.data_date,
             h.tran_type,
             sum(h.sales_qty) sales_qty,
             sum(h.sales_retail) sales_retail,
             sum(h.discount_amt) discount_amt,
             sum(h.expected_retail) expected_retail,
             sum(h.actual_retail) actual_retail,
             NVL(h.promotion,-999) promotion,
             sum(h.gross_profit_amt) gross_profit_amt,
             NVL(h.prom_component,-999) prom_component
        from svc_posupld_dsd_helper_gtt h,
             daily_sales_discount d
       where h.sales_process_id = I_sales_process_id
         and h.chunk_id          = I_chunk_id
         and d.item              = h.item
         and d.store             = h.store
         and d.prom_type         = h.prom_type
         and d.tran_type         = h.tran_type
         and d.data_date         = h.data_date
         and NVL(d.promotion,-999) = NVL(h.promotion,-999)
         and NVL(d.prom_component,-999) = NVL(h.prom_component,-999)
    group by h.item,
             h.store,
             h.prom_type,
             h.data_date,
             h.tran_type,
             NVL(h.promotion,-999),
             NVL(h.prom_component,-999);

   TYPE dsd_detl_type is TABLE OF c_dsd_details%ROWTYPE index by binary_integer;

   L_dsd_detl_tab   DSD_DETL_TYPE;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   --write records for each TDETL line (process_daily_sales_discount)
   insert into svc_posupld_dsd_helper_gtt (sales_process_id,
                                           chunk_id,
                                           item,
                                           store,
                                           prom_type,
                                           data_date,
                                           tran_type,
                                           sales_qty,
                                           sales_retail,
                                           discount_amt,
                                           expected_retail,
                                           actual_retail,
                                           promotion,
                                           gross_profit_amt,
                                           prom_component)
                                    select I_sales_process_id,
                                           I_chunk_id,
                                           nvl(inner.xform_sellable_item ,inner.im_item),
                                           inner.loc_value,
                                           inner.prom_tran_type,
                                           inner.tran_date,
                                           inner.tran_type_char,
                                           inner.sales_qty * inner.tran_type_num,
                                           inner.sales_val_loc * inner.tran_type_num,
                                           inner.discount_val_loc * inner.tran_type_num,
                                           NULL,
                                           NULL,
                                           inner.prom_no,
                                           inner.gross_profit_amt * inner.tran_type_num,
                                           inner.prom_component
                                      from (select pli.im_item,
                                                   pli.xform_sellable_item,
                                                   pli.loc_value,
                                                   plid.prom_tran_type,
                                                   pli.tran_date,
                                                   pli.tran_type_char,
                                                   pli.tran_type_num,
                                                   sum(plid.sales_qty) sales_qty,
                                                   sum(DECODE(pli.class_class_vat_ind,
                                                              'N',plid.sales_value_without_vat,
                                                              plid.sales_value_with_vat)) sales_val_loc,
                                                   sum(DECODE(pli.class_class_vat_ind,
                                                              'N',DECODE(plid.prom_tran_type,
                                                                         1005,plid.emp_disc_amt_without_vat,
                                                                         plid.disc_amt_without_vat),
                                                              DECODE(plid.prom_tran_type,
                                                                     1005,plid.emp_disc_amt_with_vat,
                                                                     plid.disc_amt_with_vat))) discount_val_loc,                                                   
                                                   plid.prom_no,
                                                   sum(DECODE(pli.class_class_vat_ind,
                                                              'N',plid.sales_value_without_vat,
                                                              plid.sales_value_with_vat) - DECODE(pli.im_pack_ind,
                                                                  'Y', plic.total_comp_cost, 
                                                                  DECODE(LP_system_options_rec.std_av_ind,'S',pli.ils_unit_cost,pli.ils_av_cost) 
                                                                     * (plid.sales_qty/(1-pli.im_waste_pct)))) gross_profit_amt,
                                                   plid.prom_component
                                              from svc_posupld_line_item_gtt pli,
                                                   svc_posupld_item_disc_gtt plid,
                                                   (select pack_no,
                                                           sales_process_id,
                                                           chunk_id,
                                                           sales_thead_id,
                                                           sum((sales_qty * (1-im_waste_pct)) * (decode(LP_system_options_rec.std_av_ind,
                                                                                                        'S',(pack_qty * ils_unit_cost)
                                                                                                           ,(pack_qty * ils_av_cost)))) total_comp_cost
                                                      from svc_posupld_line_item_gtt
                                                     where sales_process_id = I_sales_process_id
                                                       and chunk_id         = I_chunk_id
                                                       and pack_comp_ind = 'Y'
                                                  group by pack_no,
                                                           sales_process_id,
                                                           chunk_id,
                                                           sales_thead_id) plic
                                             where pli.sales_process_id = I_sales_process_id
                                               and pli.chunk_id  = I_chunk_id
                                               and pli.error_msg is NULL
                                               and pli.xform_sellable_item is NULL
                                               and pli.sales_process_id = plid.sales_process_id
                                               and pli.chunk_id = plid.chunk_id
                                               and pli.sales_thead_id   = plid.sales_thead_id
                                               and pli.im_item          = plid.im_item
                                               and plid.prom_tran_type is NOT NULL
                                               and pli.pack_comp_ind = 'N'
                                               and pli.sales_process_id = plic.sales_process_id(+)
                                               and pli.chunk_id         = plic.chunk_id(+)
                                               and pli.sales_thead_id   = plic.sales_thead_id(+)
                                             group by pli.im_item,
                                                      pli.xform_sellable_item,
                                                      pli.loc_value,
                                                      plid.prom_tran_type,
                                                      pli.tran_date,
                                                      pli.tran_type_char,
                                                      pli.tran_type_num,
                                                      plid.prom_no,
                                                      plid.prom_component) inner;

   --write records for off retail sales (1006 records) (write_off_retail)
   --This records the additional difference between regular retail and
   --sales retail after accounting for all the promotions in TDETL lines.
   insert into svc_posupld_dsd_helper_gtt (sales_process_id,
                                           chunk_id,
                                           item,
                                           store,
                                           prom_type,
                                           data_date,
                                           tran_type,
                                           sales_qty,
                                           sales_retail,
                                           discount_amt,
                                           expected_retail,
                                           actual_retail,
                                           promotion,
                                           gross_profit_amt,
                                           prom_component)
                                    select I_sales_process_id,
                                           I_chunk_id,
                                           NVL(inner.xform_sellable_item, inner.im_item),
                                           inner.loc_value,
                                           inner.prom_tran_type,
                                           inner.tran_date,
                                           inner.tran_type_char,
                                           inner.sales_qty * inner.tran_type_num,
                                           inner.retail * inner.tran_type_num,
                                           inner.off_retail_amount * inner.tran_type_num,
                                           inner.expected_retail * inner.tran_type_num,
                                           inner.actual_retail * inner.tran_type_num,
                                           NULL,
                                           inner.gross_profit_amt * inner.tran_type_num,
                                           NULL
                                      from (select v.im_item,
                                                   v.xform_sellable_item,
                                                   v.loc_value,
                                                   1006 prom_tran_type,
                                                   v.tran_date,
                                                   v.tran_type_char,
                                                   v.tran_type_num,
                                                   sum(v.xform_yield_pct * v.sales_qty_std_uom * v.pack_qty) sales_qty,
                                                   --
                                                   sum(v.dsd_sales_value) retail,
                                                   case when v.catchweight_ind = 'Y' and
                                                             v.im_standard_uom = 'EA' then
                                                      sum((v.actualweight_qty * v.dsd_old_retail 
                                                              / DECODE(v.ils_average_weight,0,1,v.ils_average_weight)) 
                                                          - (v.dsd_sales_value + NVL(v.dsd_total_disc_value,0)))
                                                   else
                                                      sum((v.dsd_old_retail * (v.xform_yield_pct * v.sales_qty_std_uom * v.pack_qty ))
                                                          - (((v.dsd_sales_value + NVL(v.dsd_total_disc_value,0))/ v.sales_qty_std_uom) 
                                                             * DECODE(v.sales_qty_std_uom,
                                                                      1, 1,
                                                                      v.xform_yield_pct * v.sales_qty_std_uom * v.pack_qty )))
                                                   end as off_retail_amount,
                                                   sum(v.dsd_old_retail * (v.xform_yield_pct * v.sales_qty_std_uom * v.pack_qty )) 
                                                       as expected_retail,
                                                   sum(v.dsd_sales_value) + sum(NVL(v.dsd_total_disc_value,0)) actual_retail,
                                                   v.st_currency_rtl_dec,
                                                   sum(v.dsd_sales_value - DECODE(v.im_pack_ind,
                                                                                 'Y', v.total_comp_cost,
                                                                                  DECODE(LP_system_options_rec.std_av_ind,'S',v.ils_unit_cost,v.ils_av_cost)
                                                                                     * ((v.xform_yield_pct * v.sales_qty_std_uom * v.pack_qty)/(1-v.im_waste_pct)))) 
                                                       as gross_profit_amt
                                              from (select pli.*,
                                                           DECODE(pli.class_class_vat_ind,
                                                                  'N',pli.sales_value_without_vat,
                                                                  pli.sales_value_with_vat) dsd_sales_value,
                                                           DECODE(pli.class_class_vat_ind,
                                                                  'N',pli.old_retail_without_vat,
                                                                  pli.old_retail_with_vat) dsd_old_retail,
                                                           DECODE(pli.class_class_vat_ind,
                                                                  'N',total.total_disc_without_vat,
                                                                  total.total_disc_with_vat) dsd_total_disc_value,
                                                                  plic.total_comp_cost	  
                                                      from svc_posupld_line_item_gtt pli,
                                                           (select distinct e1.posupld_line_item_id,
                                                                   e1.sales_process_id,
                                                                   e1.chunk_id,
                                                                   e1.sales_thead_id,
                                                                   e1.im_item,
                                                                   sum(e1.sales_qty) sales_qty,
                                                                   sum(DECODE(e1.prom_tran_type,
                                                                              1005,e1.emp_disc_amt_with_vat,
                                                                              e1.disc_amt_with_vat)) total_disc_with_vat,
                                                                   sum(DECODE(e1.prom_tran_type,
                                                                              1005,e1.emp_disc_amt_without_vat,
                                                                              e1.disc_amt_without_vat)) total_disc_without_vat
                                                              from svc_posupld_item_disc_gtt e1
                                                          group by e1.posupld_line_item_id,
                                                                   e1.sales_process_id,
                                                                   e1.chunk_id,
                                                                   e1.sales_thead_id,
                                                                   e1.im_item) total,
                                                           (select pack_no,
                                                                   sales_process_id,
                                                                   chunk_id,
                                                                   sales_thead_id,
                                                                   sum((sales_qty * (1-im_waste_pct)) * (DECODE(LP_system_options_rec.std_av_ind,
                                                                                                                'S',(pack_qty * ils_unit_cost)
                                                                                                                   ,(pack_qty * ils_av_cost)))) total_comp_cost
                                                              from svc_posupld_line_item_gtt
                                                             where sales_process_id = I_sales_process_id
                                                               and chunk_id         = I_chunk_id
                                                               and pack_comp_ind = 'Y'
                                                          group by pack_no,
                                                                   sales_process_id,
                                                                   chunk_id,
                                                                   sales_thead_id) plic																   
                                                     where pli.sales_process_id   = I_sales_process_id
                                                       and pli.chunk_id           = I_chunk_id
                                                       and pli.posupld_line_item_id  = total.posupld_line_item_id(+)
                                                       and pli.sales_process_id   = total.sales_process_id(+)
                                                       and pli.chunk_id           = total.chunk_id(+)
                                                       and pli.sales_thead_id     = total.sales_thead_id(+)
                                                       and pli.im_item            = total.im_item(+)
                                                       and pli.pack_comp_ind      = 'N'
                                                       and pli.error_msg          is NULL
                                                       and pli.xform_sellable_item is NULL
                                                       and pli.sales_process_id = plic.sales_process_id(+)
                                                       and pli.chunk_id         = plic.chunk_id(+)
                                                       and pli.sales_thead_id   = plic.sales_thead_id(+)) v													   
                                          group by v.im_item,
                                                   v.xform_sellable_item,
                                                   v.loc_value,
                                                   v.tran_date,
                                                   v.tran_type_char,
                                                   v.tran_type_num,
                                                   v.st_currency_rtl_dec,
                                                   v.catchweight_ind,
                                                   v.im_standard_uom) inner
                                     where abs(inner.off_retail_amount) > power(10, -1 * inner.st_currency_rtl_dec);

   -- Get batch locking parameters
   open C_GET_LOCK_PARAM;
   fetch C_GET_LOCK_PARAM into L_retry_lock_attempts,
                               L_retry_wait_time;
   close C_GET_LOCK_PARAM;
   --
   L_table := 'DAILY_SALES_DISCOUNT';
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   FOR i in 1..(l_retry_lock_attempts - 1) LOOP
      ---
      BEGIN
         --lock daily_sales_discount
         open C_LOCK_DSD;
         close C_LOCK_DSD;
         ---
         L_locked_acquired := 'Y';
         exit;
         --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      --
      dbms_lock.sleep(L_retry_wait_time);
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      --lock daily_sales_discount
      open C_LOCK_DSD;
      close C_LOCK_DSD;
      ---
   end if;
   ---

   open C_DSD_DETAILS;
   fetch C_DSD_DETAILS bulk collect into L_dsd_detl_tab;
   close C_DSD_DETAILS;

   FORALL dsd_detl_ind in L_dsd_detl_tab.FIRST..L_dsd_detl_tab.LAST
            update daily_sales_discount d
               set d.sales_qty        = d.sales_qty + L_dsd_detl_tab(dsd_detl_ind).sales_qty,
                   d.sales_retail     = d.sales_retail + L_dsd_detl_tab(dsd_detl_ind).sales_retail,
                   d.discount_amt     = d.discount_amt + L_dsd_detl_tab(dsd_detl_ind).discount_amt,
                   d.expected_retail  = nvl(d.expected_retail, 0) + nvl(L_dsd_detl_tab(dsd_detl_ind).expected_retail, 0),
                   d.actual_retail    = nvl(d.actual_retail, 0) + nvl(L_dsd_detl_tab(dsd_detl_ind).actual_retail, 0),
                   d.gross_profit_amt = nvl(d.gross_profit_amt, 0) + nvl(L_dsd_detl_tab(dsd_detl_ind).gross_profit_amt, 0)
             where d.item      = L_dsd_detl_tab(dsd_detl_ind).item
               and d.store     = L_dsd_detl_tab(dsd_detl_ind).store
               and d.prom_type = L_dsd_detl_tab(dsd_detl_ind).prom_type
               and d.tran_type = L_dsd_detl_tab(dsd_detl_ind).tran_type
               and d.data_date = L_dsd_detl_tab(dsd_detl_ind).data_date
         and NVL(d.promotion,-999) = L_dsd_detl_tab(dsd_detl_ind).promotion
         and NVL(d.prom_component,-999) = L_dsd_detl_tab(dsd_detl_ind).prom_component;
		 
            insert into daily_sales_discount(item,
                                             store,
                                             prom_type,
                                             data_date,
                                             tran_type,
                                             sales_qty,
                                             sales_retail,
                                             discount_amt,
                                             expected_retail,
                                             actual_retail,
                                             promotion,
                                             gross_profit_amt,
                                             prom_component)
                                                                  select h.item,
                                    h.store,
                                    h.prom_type,
                                    h.data_date,
                                    h.tran_type,
                                    h.sales_qty,
                                    h.sales_retail,
                                    h.discount_amt,
                                    h.expected_retail,
                                    h.actual_retail,
                                    h.promotion,
                                    h.gross_profit_amt,
                                    h.prom_component
                               from svc_posupld_dsd_helper_gtt h
                              where not exists (select 'x'
                                                  from daily_sales_discount d
                                                 where d.item      = h.item
                                                   and d.store     = h.store
                                                   and d.prom_type = h.prom_type
                                                   and d.tran_type = h.tran_type
                                                   and d.data_date = h.data_date
                                                   and NVL(d.promotion,-999) = NVL(h.promotion,-999)
                                                   and NVL(d.prom_component,-999) = NVL(h.prom_component,-999));
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            null,
                                            null);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_DAILY_SALES_DISCOUNT',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_DAILY_SALES_DISCOUNT;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_TRAN_DATA_WASTAGE(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id   IN              NUMBER,
                                   I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   cursor C_GET_TRAN_WASTAGE is
      select pli.im_item,
             --
             case when pli.im_pack_ind = 'Y' and pli.pack_comp_ind = 'Y' then
                'N'
             else
                pli.im_pack_ind
             end as im_pack_ind,
             --
             pli.im_dept,
             pli.im_class,
             pli.im_subclass,
             pli.loc_value,
             pli.tran_date,
             13 tran_code,
             (pli.xform_yield_pct * (pli.total_wastage_qty * pli.pack_qty)) /
             (1 - pli.xform_prod_loss_pct) * pli.tran_type_num  units,
             null total_cost,
             ROUND(decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                          'Y', (pli.xform_yield_pct * pli.tran_type_num *
                                (pli.old_retail_with_vat - (pli.old_retail_with_vat * (1 - pli.im_waste_pct))) *
                                (pli.total_wastage_qty * pli.pack_qty)),
                               (pli.xform_yield_pct * pli.tran_type_num *
                                (pli.old_retail_without_vat - (pli.old_retail_without_vat * (1 - pli.im_waste_pct))) *
                                (pli.total_wastage_qty * pli.pack_qty))), pli.st_currency_rtl_dec) total_retail,
             decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                    'Y', pli.xform_yield_pct * pli.old_retail_with_vat,
                         pli.xform_yield_pct * pli.old_retail_without_vat) old_retail,
             decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                    'Y', pli.xform_yield_pct * (pli.old_retail_with_vat * (1 - pli.im_waste_pct)),
                         pli.xform_yield_pct * (pli.old_retail_without_vat * (1 - pli.im_waste_pct))) new_retail,
             null vat_rate,
             null av_cost,
             pli.pack_no
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id    = I_sales_process_id
         and pli.chunk_id            = I_chunk_id
         and pli.error_msg           is null
         and pli.im_waste_type       = 'SL'
         and ((pli.im_item_xform_ind = 'N')
              or
              (pli.im_item_xform_ind = 'Y' and pli.xform_sellable_item is not null))
         and pli.is_concession_rate  is null;

   TYPE tran_wastage is TABLE of C_GET_TRAN_WASTAGE%ROWTYPE INDEX BY BINARY_INTEGER;

   L_tran_wastage_tab   TRAN_WASTAGE;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_TRAN_WASTAGE;
   fetch C_GET_TRAN_WASTAGE bulk collect into L_tran_wastage_tab;
   close C_GET_TRAN_WASTAGE;

   if L_tran_wastage_tab.FIRST is not NULL then
      FOR int_tran in L_tran_wastage_tab.FIRST..L_tran_wastage_tab.LAST LOOP
         insert into tran_data (item,
                                dept,
                                class,
                                subclass,
                                pack_ind,
                                loc_type,
                                location,
                                tran_date,
                                tran_code,
                                units,
                                total_cost,
                                total_retail,
                                ref_no_1,
                                ref_no_2,
                                gl_ref_no,
                                old_unit_retail,
                                new_unit_retail,
                                pgm_name,
                                sales_type,
                                vat_rate,
                                av_cost,
                                timestamp,
                                ref_pack_no,
                                total_cost_excl_elc)
                        values (L_tran_wastage_tab(int_tran).im_item,
                                L_tran_wastage_tab(int_tran).im_dept,
                                L_tran_wastage_tab(int_tran).im_class,
                                L_tran_wastage_tab(int_tran).im_subclass,
                                L_tran_wastage_tab(int_tran).im_pack_ind,
                                'S',
                                L_tran_wastage_tab(int_tran).loc_value,
                                L_tran_wastage_tab(int_tran).tran_date,
                                L_tran_wastage_tab(int_tran).tran_code,
                                L_tran_wastage_tab(int_tran).units,
                                L_tran_wastage_tab(int_tran).total_cost,
                                L_tran_wastage_tab(int_tran).total_retail,
                                null,
                                null,
                                null,
                                L_tran_wastage_tab(int_tran).old_retail,
                                L_tran_wastage_tab(int_tran).new_retail,
                                'salesprocess',
                                null,
                                L_tran_wastage_tab(int_tran).vat_rate,
                                L_tran_wastage_tab(int_tran).av_cost,
                                sysdate,
                                L_tran_wastage_tab(int_tran).pack_no,
                                null);
      END LOOP;
   end if;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_TRAN_DATA_WASTAGE',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_TRAN_DATA_WASTAGE;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_SNAPSHOT(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_sales_process_id   IN              NUMBER,
                          I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_vdate   DATE := GET_VDATE();

   cursor C_SNAP is
       select pli.im_item,
              pli.sups_edi_sales_rpt_freq,
              pli.loc_value,
              pli.tran_date,
              (tran_type_num * (pli.total_wastage_qty * pli.pack_qty)) sales_qty
         from svc_posupld_line_item_gtt pli
        where pli.sales_process_id    = I_sales_process_id
          and pli.chunk_id            = I_chunk_id
          and pli.error_msg           is null
          and pli.xform_sellable_item is null
          and pli.im_pack_ind         = 'N'
          and (pli.im_inventory_ind = 'Y'
             or (pli.im_inventory_ind = 'N' and pli.im_sellable_ind = 'Y' and pli.im_item_xform_ind = 'Y'))
          and pli.tran_date         < L_vdate;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   FOR rec in c_snap LOOP
      if UPDATE_SNAPSHOT_SQL.EXECUTE(O_error_message,
                                     'SALADJ',
                                     rec.im_item,
                                     rec.sups_edi_sales_rpt_freq,
                                     rec.loc_value,
                                     rec.tran_date,
                                     L_vdate,
                                     rec.sales_qty) = FALSE then
         return FALSE;
      end if;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_SNAPSHOT',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_SNAPSHOT;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_TRAN_DATA_DEAL_INCOME(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_sales_process_id   IN              NUMBER,
                                       I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_vdate              DATE := GET_VDATE();
   L_eom_date           DATE;
   L_month_closed_ind   VARCHAR2(1);

   cursor C_GET_EOM_DATE is
      select sv.next_eom_date,
             CASE WHEN L_vdate > sv.next_eom_date THEN 'N' ELSE 'Y' END
        from system_variables sv;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_EOM_DATE;
   fetch C_GET_EOM_DATE into L_eom_date,
                             L_month_closed_ind;
   close C_GET_EOM_DATE;
   ---
   -- transaction code 6 - Sales Income Reversal
   insert into temp_tran_data (item,
                               dept,
                               class,
                               subclass,
                               pack_ind,
                               loc_type,
                               location,
                               tran_date,
                               tran_code,
                               adj_code,
                               units,
                               total_cost,
                               total_retail,
                               ref_no_1,
                               ref_no_2,
                               gl_ref_no,
                               old_unit_retail,
                               new_unit_retail,
                               pgm_name,
                               sales_type,
                               vat_rate,
                               av_cost,
                               timestamp)
     WITH vrd_min AS (SELECT /*+ parallel(daf1,4) */ 
                             deal_id, 
                             deal_detail_id, 
                             MIN(reporting_date) reporting_date
                        FROM deal_actuals_forecast daf1
                       WHERE actual_forecast_ind = 'F'
                         AND reporting_date >= L_vdate
                    GROUP BY deal_id, 
                             deal_detail_id),
          vrd_max AS (SELECT /*+ parallel(daf2,4) */ 
                             deal_id, 
                             deal_detail_id, 
                             MAX(reporting_date) reporting_date
                        FROM deal_actuals_forecast daf2
                       WHERE reporting_date <= L_eom_date
                    GROUP BY deal_id, 
                             deal_detail_id)
      select pli.im_item,
             pli.im_dept,
             pli.im_class,
             pli.im_subclass,
             null,
             'S',
             pli.loc_value,
             decode(L_month_closed_ind,'N',(CASE WHEN pli.tran_date <= L_eom_date 
                                            THEN vrd2.reporting_date
                                            ELSE vrd1.reporting_date END), 
                                   'Y', vrd1.reporting_date) tran_date,
             6,
             null,
             plid.sales_qty * pli.tran_type_num sales_qty,
             0,
             ROUND(plid.discount_value * (dcp.contribution_pct/100), pli.st_currency_rtl_dec) * pli.tran_type_num total_retail,
             dcp.deal_id,
             decode(dh.stock_ledger_ind,'Y',1,0),
             null,
             null,
             null,
             'salesprocess',
             null,
             null,
             null,
             sysdate
        from svc_posupld_item_disc_gtt plid,
             svc_posupld_line_item_gtt pli,
             deal_head dh,
             deal_comp_prom dcp,
             vrd_min vrd1,
             vrd_max vrd2
       where pli.sales_process_id  = I_sales_process_id
         and pli.chunk_id          = I_chunk_id
         and pli.sales_process_id  = plid.sales_process_id
         and pli.chunk_id          = plid.chunk_id
         and pli.sales_thead_id    = plid.sales_thead_id
         and pli.error_msg         is null
         and dcp.promotion_id      = plid.prom_no
         and dcp.promo_comp_id     = plid.prom_component
         and dcp.deal_id           = dh.deal_id
         and dcp.deal_id           = vrd1.deal_id(+)
         and dcp.deal_detail_id    = vrd1.deal_detail_id (+)
         and dcp.deal_id           = vrd2.deal_id(+)
         and dcp.deal_detail_id    = vrd2.deal_detail_id (+)
         and (vrd1.reporting_date IS NOT NULL OR vrd2.reporting_date IS NOT NULL)
         and dh.status            != 'C'
         and exists (select 'x'
                       from deal_item_loc_explode dile
                      where dh.rpm_deal_ind = 'N'   --- RMS deal
                        and dile.item = pli.im_item --- item
                        and dile.location = pli.loc_value   --- location
                        and dile.deal_id = dcp.deal_id  --- deal_id
            and dile.deal_detail_id = dcp.deal_detail_id    --- deal_detail_id
                    union all
                     select 'x'
                       from item_supp_country_loc iscl
                      where dh.rpm_deal_ind = 'Y'   --- RPM deal
                        and iscl.item = pli.im_item  --- item
                        and iscl.loc = pli.loc_value    --- location
                        and ((dh.partner_type = 'S' and iscl.supplier in (select supplier
                                                                            from sups
                                                                           where supplier_parent = dh.supplier
                                                                              or supplier = dh.supplier))
                             or (dh.partner_type = 'S1' and iscl.supp_hier_lvl_1 = dh.partner_id)
                             or (dh.partner_type = 'S2' and iscl.supp_hier_lvl_2 = dh.partner_id)
                             or (dh.partner_type = 'S3' and iscl.supp_hier_lvl_3 = dh.partner_id)));
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_TRAN_DATA_DEAL_INCOME',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_TRAN_DATA_DEAL_INCOME;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_DEAL_ACTUALS_IL(O_error_message         IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_sales_process_id      IN              NUMBER,
                                 I_chunk_id              IN              NUMBER)
RETURN BOOLEAN IS

   L_vdate              DATE := GET_VDATE();
   L_eom_date           DATE;
   L_month_closed_ind   VARCHAR2(1);

   cursor C_GET_EOM_DATE is
      select sv.next_eom_date,
             CASE WHEN L_vdate > sv.next_eom_date THEN 'N' ELSE 'Y' END
        from system_variables sv;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_EOM_DATE;
   fetch C_GET_EOM_DATE into L_eom_date,
                             L_month_closed_ind;
   close C_GET_EOM_DATE;
   ---
   merge into deal_actuals_item_loc dail
   using (
   WITH vrd_min AS (SELECT /*+ parallel(daf1,4) */ 
                           deal_id, 
                           deal_detail_id, 
                           MIN(reporting_date) reporting_date
                      FROM deal_actuals_forecast daf1
                     WHERE actual_forecast_ind = 'F'
                       AND reporting_date >= L_vdate
                  GROUP BY deal_id, 
                           deal_detail_id),
        vrd_max AS (SELECT /*+ parallel(daf2,4) */ 
                           deal_id, 
                           deal_detail_id, 
                           MAX(reporting_date) reporting_date
                      FROM deal_actuals_forecast daf2
                     WHERE reporting_date <= L_eom_date
                  GROUP BY deal_id, 
                           deal_detail_id)
      select dcp.deal_id,
             dcp.deal_detail_id,
             decode(L_month_closed_ind, 'N', (CASE WHEN pli.tran_date <= L_eom_date 
                              THEN vrd2.reporting_date
                              ELSE vrd1.reporting_date END), 
                     'Y', vrd1.reporting_date) reporting_date,
             pli.im_item,
             pli.im_dept,
             pli.im_class,
             pli.im_subclass,
             pli.loc_value,
             'S' loc_type,
             decode(L_month_closed_ind, 'N', (CASE WHEN pli.tran_date <= L_eom_date 
                                            THEN vrd2.reporting_date
                                            ELSE vrd1.reporting_date END), 
                                   'Y', vrd1.reporting_date) tran_date,
             plid.sales_qty * pli.tran_type_num sales_qty,
             plid.discount_value * pli.tran_type_num discount_value,
             plid.discount_value * pli.tran_type_num * (dcp.contribution_pct/100) income
        from svc_posupld_item_disc_gtt plid,
             svc_posupld_line_item_gtt pli,
             deal_head dh,
             deal_comp_prom dcp,
             vrd_min vrd1,
             vrd_max vrd2
       where pli.sales_process_id  = I_sales_process_id
         and pli.chunk_id          = I_chunk_id
         and pli.sales_process_id  = plid.sales_process_id
         and pli.chunk_id          = plid.chunk_id
         and pli.sales_thead_id    = plid.sales_thead_id
         and pli.error_msg         is null
         and dcp.promotion_id      = plid.prom_no
         and dcp.promo_comp_id     = plid.prom_component
         and dcp.deal_id           = dh.deal_id
         and dcp.deal_id           = vrd1.deal_id(+)
         and dcp.deal_detail_id    = vrd1.deal_detail_id (+)
         and dcp.deal_id           = vrd2.deal_id(+)
         and dcp.deal_detail_id    = vrd2.deal_detail_id (+)
         and (vrd1.reporting_date IS NOT NULL OR vrd2.reporting_date IS NOT NULL)
         and dh.status            != 'C'
         and exists (select 'x'
                       from deal_item_loc_explode dile
                      where dh.rpm_deal_ind = 'N'   --- RMS deal
                        and dile.item = pli.im_item --- item
                        and dile.location = pli.loc_value   --- location
                        and dile.deal_id = dcp.deal_id  --- deal_id
            and dile.deal_detail_id = dcp.deal_detail_id    --- deal_detail_id
                    union all
                     select 'x'
                       from item_supp_country_loc iscl
                      where dh.rpm_deal_ind = 'Y'   --- RPM deal
                        and iscl.item = pli.im_item  --- item
                        and iscl.loc = pli.loc_value    --- location
                        and ((dh.partner_type = 'S'  and iscl.supplier in (select supplier
                                                                             from sups
                                                                            where supplier_parent = dh.supplier
                                                                               or supplier = dh.supplier))
                             or (dh.partner_type = 'S1' and iscl.supp_hier_lvl_1 = dh.partner_id)
                             or (dh.partner_type = 'S2' and iscl.supp_hier_lvl_2 = dh.partner_id)
                             or (dh.partner_type = 'S3' and iscl.supp_hier_lvl_3 = dh.partner_id)))) use_this
        on (dail.deal_id        = use_this.deal_id
       and dail.deal_detail_id = use_this.deal_detail_id
       and dail.reporting_date = use_this.reporting_date
       and dail.item           = use_this.im_item
       and dail.location       = use_this.loc_value
       and dail.loc_type       = use_this.loc_type
       and dail.order_no       is null)
   when matched then
      update set dail.actual_turnover_units   = nvl(dail.actual_turnover_units,0) + use_this.sales_qty,
                 dail.actual_turnover_revenue = nvl(dail.actual_turnover_revenue,0) + use_this.discount_value,
                 dail.actual_income           = nvl(dail.actual_income,0) + use_this.income
   when not matched then
      insert (dai_id,
              deal_id,
              deal_detail_id,
              reporting_date,
              item,
              location,
              loc_type,
              actual_turnover_units,
              actual_turnover_revenue,
              order_no,
              actual_income)
      values (deal_actuals_itemloc_seq.nextval,
              use_this.deal_id,
              use_this.deal_detail_id,
              use_this.reporting_date,
              use_this.im_item,
              use_this.loc_value,
              use_this.loc_type,
              use_this.sales_qty,
              use_this.discount_value,
              null,
              use_this.income);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_DEAL_ACTUALS_IL',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_DEAL_ACTUALS_IL;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_VAT_HISTORY(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sales_process_id   IN              NUMBER,
                             I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   merge into vat_history v
   using (
      select pli.im_item,
             pli.xform_sellable_item,
             pli.loc_value,
             pli.tran_date,
             sum(ROUND((pli.tran_type_num * NVL(pli.vi_vat_amt,0) * pli.pack_price_ratio), pli.st_currency_rtl_dec)) vat_amt,
             min(pli.st_currency_rtl_dec) st_currency_rtl_dec,
             min(nvl(pli.vi_vat_rate,0)) vi_vat_rate,
             min(pli.vi_vat_code) vi_vat_code
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id    = I_sales_process_id
         and pli.chunk_id            = I_chunk_id
         and pli.vi_vat_code is NOT NULL
         and pli.error_msg           is null
         and pli.xform_sellable_item is null
         and (pli.im_pack_ind         = 'N'
          or (pli.im_pack_ind         = 'Y'
         and  pli.pack_no             is not null))
    group by pli.im_item,
             pli.xform_sellable_item,
             pli.loc_value,
             pli.tran_date) use_this
    on (v.item = nvl(use_this.xform_sellable_item, use_this.im_item)
       and v.store = use_this.loc_value
       and v.tran_date = use_this.tran_date)
   when matched then
      update set v.vat_amt = v.vat_amt + round(use_this.vat_amt, use_this.st_currency_rtl_dec),
                 v.last_update_datetime = sysdate,
                 v.last_update_id       = LP_user
   when not matched then
      insert (item,
              store,
              tran_date,
              vat_amt,
              vat_rate,
              vat_code,
              create_datetime,
              last_update_datetime,
              last_update_id)
      values (nvl(use_this.xform_sellable_item, use_this.im_item),
              use_this.loc_value,
              use_this.tran_date,
              round(use_this.vat_amt, use_this.st_currency_rtl_dec),
              use_this.vi_vat_rate,
              use_this.vi_vat_code,
              sysdate,
              sysdate,
              LP_user);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_VAT_HISTORY',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_VAT_HISTORY;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_EDI_DAILY_SALES(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_sales_process_id   IN              NUMBER,
                                 I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_vdate   DATE := GET_VDATE();

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   merge into edi_daily_sales edi
   using (
      select pli.im_item,
             pli.loc_value,
             'S' s_loc_type,
             pli.tran_date,
             sum(pli.tran_type_num * pli.total_wastage_qty * pli.pack_qty) sales_qty
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id   = I_sales_process_id
         and pli.chunk_id           = I_chunk_id
         and pli.error_msg          is NULL
         and (pli.im_item_xform_ind = 'N' 
              or (pli.im_item_xform_ind = 'Y' 
                  and pli.xform_sellable_item is NOT NULL))
         and (pli.im_pack_ind = 'N'
              or (pli.im_pack_ind = 'Y'
                  and pli.pack_no is NOT NULL))
         and pli.sups_edi_sales_rpt_freq  = 'D'
         and pli.tran_date           <= L_vdate
    group by pli.im_item,
             pli.loc_value,
             pli.tran_date) use_this
   on (edi.item          = use_this.im_item
       and edi.loc       = use_this.loc_value
       and edi.loc_type  = use_this.s_loc_type
       and edi.tran_date = use_this.tran_date)
   when matched then
      update set edi.sales_qty = edi.sales_qty + use_this.sales_qty
   when not matched then
      insert (item,
              loc,
              loc_type,
              tran_date,
              sales_qty,
              stock_on_hand,
              in_transit_qty)
      values (use_this.im_item,
              use_this.loc_value,
              'S',
              use_this.tran_date,
              use_this.sales_qty,
              0,
              0);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_EDI_DAILY_SALES',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_EDI_DAILY_SALES;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_ITEM_LOC_HIST(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_sales_process_id   IN              NUMBER,
                               I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   merge into item_loc_hist ilh
   using (
      select temp.item,
             temp.loc,
             temp.eow_date,
             temp.eow_date_454_week,
             temp.eow_date_454_month,
             temp.eow_date_454_year,
             temp.sales_type,
             sum(temp.sales_issues) sales_issues,
             sum(temp.value) value,
             sum(temp.retail) retail,
             sum(temp.gp) gp,
             temp.dept,
             temp.class,
             temp.subclass
        from (select nvl(sim.item,pli.im_item) item,
                     pli.loc_value loc,
                     pli.eow_date,
                     pli.eow_date_454_week,
                     pli.eow_date_454_month,
                     pli.eow_date_454_year,
                     pli.sales_type,
                     sum(pli.total_wastage_qty * pli.pack_qty * pli.xform_yield_pct * pli.tran_type_num) sales_issues,
                     sum(decode(pli.class_class_vat_ind,
                                       'Y', pli.sales_value_with_vat * pli.pack_price_ratio + pli.wastage_amt_with_vat,
                                       pli.sales_value_without_vat * pli.pack_price_ratio + pli.wastage_amt_without_vat) * pli.xform_yield_pct * pli.tran_type_num) value,
                     sum(decode(pli.im_pack_ind,
                           'Y', decode(pli.pack_comp_ind,
                                                             'N', decode(pli.class_class_vat_ind,
                                                                  'N', pli.sales_value_without_vat,
                                                                   pli.sales_value_with_vat) / pli.total_wastage_qty,
                                                            null) * pli.tran_type_num)) retail,                                       
                     nvl(sum(decode(pli.im_pack_ind,
                          'Y', decode(pli.pack_comp_ind, 'Y' ,
                                           ((pli.sales_value_without_vat * pli.pack_price_ratio) -
                                               (decode(LP_system_options_rec.std_av_ind,
                                                    'A',decode(pli.xform_sellable_item, null, nvl(pli.total_av_cost_loc, 0), null),
                                       decode(pli.xform_sellable_item,  null, nvl(pli.total_unit_cost_loc, 0), null)))) * pli.xform_yield_pct,null),
                                           ((pli.sales_value_without_vat * pli.pack_price_ratio) -
                                                (case when pli.im_item_xform_ind = 'Y' and pli.total_unit_cost_loc is not NULL then
                                                    pli.total_unit_cost_loc
                                                 else
                                                   decode(LP_system_options_rec.std_av_ind,
                                                          'A',decode(pli.xform_sellable_item, null, nvl(pli.total_av_cost_loc, 0), null),
                                                           decode(pli.xform_sellable_item,  null, nvl(pli.total_unit_cost_loc, 0), null))
                                                   end)) * pli.xform_yield_pct) * pli.tran_type_num * pli.sales_sign_num), 0) gp,
                     pli.im_dept dept,
                     pli.im_class class,
                     pli.im_subclass subclass
                from svc_posupld_line_item_gtt pli, 
                     sub_items_detail sim
               where pli.sales_process_id    = I_sales_process_id
                 and pli.chunk_id            = I_chunk_id
                 and pli.error_msg           is null
                 and pli.tran_type_char      = 'S'
                 and pli.im_item             = sim.sub_item(+)
                 and pli.tran_date  between sim.start_date(+) and sim.end_date(+)
                 and sim.substitute_reason (+) = 'P' 
            group by nvl(sim.item,pli.im_item),
                     pli.im_pack_ind,
                     pli.loc_value,
                     pli.eow_date,
                     pli.eow_date_454_week,
                     pli.eow_date_454_month,
                     pli.eow_date_454_year,
                     pli.tran_type_char,
                     pli.sales_sign_char,
                     pli.sales_type,
                     pli.im_dept,
                     pli.im_class,
                     pli.im_subclass) temp
    group by temp.item,
             temp.loc,
             temp.eow_date,
             temp.eow_date_454_week,
             temp.eow_date_454_month,
             temp.eow_date_454_year,
             temp.sales_type,
             temp.dept,
             temp.class,
             temp.subclass) use_this
   on (ilh.item       = use_this.item
       and ilh.loc        = use_this.loc
       and ilh.sales_type = use_this.sales_type
       and ilh.eow_date   = use_this.eow_date)
   when matched then
      update set ilh.sales_issues         = ilh.sales_issues + use_this.sales_issues,
                 ilh.value                = ilh.value        + use_this.value,
                 ilh.retail               = ilh.retail       + nvl(use_this.retail,0),
                 ilh.gp                   = ilh.gp           + use_this.gp,
                 ilh.last_update_datetime = sysdate,
                 ilh.last_update_id       = LP_user
   when not matched then
      insert (item,
              loc,
              loc_type,
              eow_date,
              week_454,
              month_454,
              year_454,
              sales_type,
              sales_issues,
              value,
              gp,
              retail,
              create_datetime,
              last_update_datetime,
              last_update_id,
              dept,
              class,
              subclass)
      values (use_this.item,
              use_this.loc,
              'S',
              use_this.eow_date,
              use_this.eow_date_454_week,
              use_this.eow_date_454_month,
              use_this.eow_date_454_year,
              use_this.sales_type,
              use_this.sales_issues,
              use_this.value,
              use_this.gp,
              use_this.retail,
              sysdate,
              sysdate,
              LP_user,
              use_this.dept,
              use_this.class,
              use_this.subclass);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_ITEM_LOC_HIST',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_ITEM_LOC_HIST;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_ITEM_LOC_HIST_MTH(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id   IN              NUMBER,
                                   I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if LP_system_options_rec.calendar_454_ind != 'C' then
      return TRUE;
   end if;

   merge into item_loc_hist_mth ilhm
   using (
      select temp.item,
             temp.loc,
             temp.eom_date,
             temp.eom_date_454_month,
             temp.eom_date_454_year,
             temp.sales_type,
             sum(temp.sales_issues) sales_issues,
             sum(temp.value) value,
             sum(temp.retail) retail,
             sum(temp.gp) gp
        from (select nvl(sim.item,pli.im_item) item,
             pli.loc_value loc,
             pli.eom_date,
             pli.eom_date_454_month,
             pli.eom_date_454_year,
             pli.sales_type,
             sum(pli.total_wastage_qty * pli.pack_qty * pli.xform_yield_pct * pli.tran_type_num) sales_issues,
             sum(decode(pli.class_class_vat_ind,
                                    'Y', pli.sales_value_with_vat * pli.pack_price_ratio + pli.wastage_amt_with_vat,
                                    pli.sales_value_without_vat * pli.pack_price_ratio + pli.wastage_amt_without_vat) * pli.xform_yield_pct * pli.tran_type_num) value,
             sum(decode(pli.im_pack_ind,
                        'Y', decode(pli.pack_comp_ind,
                                    'N', decode(pli.class_class_vat_ind,
                                                               'N', pli.sales_value_without_vat,
                                                                pli.sales_value_with_vat) / pli.total_wastage_qty,
                                    null) * pli.tran_type_num)) retail,                                                
             sum(decode(pli.im_pack_ind, 'Y', decode(pli.pack_comp_ind, 'Y', ((pli.sales_value_without_vat * pli.pack_price_ratio) -
                                               (pli.total_unit_cost_loc)) * pli.xform_yield_pct,
                                               null),
                                              ((pli.sales_value_without_vat * pli.pack_price_ratio) -
                                               (pli.total_unit_cost_loc)) * pli.xform_yield_pct) * pli.tran_type_num * pli.sales_sign_num) gp
        from svc_posupld_line_item_gtt pli, 
             sub_items_detail sim
       where pli.sales_process_id    = I_sales_process_id
         and pli.chunk_id            = I_chunk_id
         and pli.error_msg           is null
         and pli.tran_type_char      = 'S'
         and pli.im_item             = sim.sub_item(+)
         and pli.tran_date  between sim.start_date(+) and sim.end_date(+)
         and sim.substitute_reason (+) = 'P' 
    group by nvl(sim.item,pli.im_item),
             pli.im_pack_ind,
             pli.loc_value,
             pli.eom_date,
             pli.eom_date_454_month,
             pli.eom_date_454_year,
             pli.tran_type_char,
             pli.sales_sign_char,
             pli.sales_type) temp
    group by temp.item,
             temp.loc,
             temp.eom_date,
             temp.eom_date_454_month,
             temp.eom_date_454_year,
             temp.sales_type) use_this
   on (ilhm.item           = use_this.item
       and ilhm.loc        = use_this.loc
       and ilhm.sales_type = use_this.sales_type
       and ilhm.eom_date   = use_this.eom_date)
   when matched then
      update set ilhm.sales_issues         = ilhm.sales_issues + use_this.sales_issues,
                 ilhm.value                = ilhm.value        + use_this.value,
                 ilhm.retail               = ilhm.retail       + nvl(use_this.retail,0),
                 ilhm.gp                   = ilhm.gp           + use_this.gp,
                 ilhm.last_update_datetime = sysdate,
                 ilhm.last_update_id       = LP_user
   when not matched then
      insert (item,
              loc,
              loc_type,
              eom_date,
              month_454,
              year_454,
              sales_type,
              sales_issues,
              value,
              retail,
              gp,
              create_datetime,
              last_update_datetime,
              last_update_id)
      values (use_this.item,
              use_this.loc,
              'S',
              use_this.eom_date,
              use_this.eom_date_454_month,
              use_this.eom_date_454_year,
              use_this.sales_type,
              use_this.sales_issues,
              use_this.value,
              use_this.retail,
              use_this.gp,
              sysdate,
              sysdate,
              LP_user);

   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_ITEM_LOC_HIST_MTH',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_ITEM_LOC_HIST_MTH;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_CONCESSION_DATA(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_sales_process_id   IN              NUMBER,
                                 I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   insert into concession_data (item,
                                supplier,
                                store,
                                tran_date,
                                concession_amount,
                                processed_ind,
                                concession_amount_incvat)
      select pli.im_item,
             pli.is_supplier,
             pli.loc_value,
             pli.tran_date,
             pli.sales_value_without_vat * (pli.is_concession_rate/100) * pli.tran_type_num,
             'N',
             pli.sales_value_with_vat * (pli.is_concession_rate/100) * pli.tran_type_num
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id    = I_sales_process_id
         and pli.chunk_id            = I_chunk_id
         and pli.error_msg           is null
         and pli.xform_sellable_item is null
         and pli.deps_purchase_type  = 2
         and pli.im_pack_ind         = 'N';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_CONCESSION_DATA',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_CONCESSION_DATA;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_CONSIGNMENT_DATA(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_sales_process_id   IN              NUMBER,
                                  I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_vdate          DATE := GET_VDATE();
   L_invc_id        INVC_HEAD.INVC_ID%TYPE;
   L_rtv_order_no   RTV_HEAD.RTV_ORDER_NO%TYPE;
   L_order_no       ORDHEAD.ORDER_NO%TYPE;
   L_return_code    VARCHAR2(5) := 'FALSE';

   cursor C_GET_CONSIGN_RTV is
      select distinct
             pch.po_invc_date,
             pch.im_item,
             pch.loc_value,
             pch.is_supplier,
             r.rtv_order_no,
             decode(r.rtv_order_no, null, 'N', 'Y') existing_rtv_ind
        from svc_posupld_cons_helper_gtt pch,
             rtv_head r
       where pch.tran_type           = 'R'
         and pch.sales_process_id    = I_sales_process_id
         and pch.chunk_id            = I_chunk_id
         and r.created_date(+)       = pch.po_invc_date
         and r.item(+)               = pch.im_item
         and r.store(+)              = pch.loc_value
         and r.supplier(+)           = pch.is_supplier;

   cursor C_GET_CONSIGN_ORDER is
      select distinct inner.order_no,
                      inner.im_dept,
                      inner.po_invc_date,
                      inner.im_item,
                      inner.is_supplier,
                      inner.existing_order_ind
        from (select oh.order_no,
                     pch.im_dept,
                     pch.po_invc_date,
                     pch.im_item,
                     pch.is_supplier,
                     decode(oh.order_no, null, 'N', 'Y') existing_order_ind
                from svc_posupld_cons_helper_gtt pch,
                     ordhead oh
               where pch.tran_type           = 'S'
                 and pch.sales_process_id    = I_sales_process_id
                 and pch.chunk_id            = I_chunk_id
                 and pch.is_consignment_rate is not null
                 and oh.dept(+)              = pch.im_dept
                 and oh.written_date(+)      = pch.po_invc_date
                 and oh.supplier(+)          = pch.is_supplier
                 and oh.orig_ind(+)          = 4
                 and 'S'                     = LP_system_options_rec.gen_con_invc_itm_sup_loc_ind
              union all
              select oh.order_no,
                     pch.im_dept,
                     pch.po_invc_date,
                     pch.im_item,
                     pch.is_supplier,
                     decode(oh.order_no, null, 'N', 'Y') existing_order_ind
                from svc_posupld_cons_helper_gtt pch,
                     ordhead oh
               where pch.tran_type           = 'S'
                 and pch.sales_process_id    = I_sales_process_id
                 and pch.chunk_id            = I_chunk_id
                 and pch.is_consignment_rate is not null
                 and oh.dept(+)              = pch.im_dept
                 and oh.written_date(+)      = pch.po_invc_date
                 and oh.supplier(+)          = pch.is_supplier
                 and oh.orig_ind(+)          = 4
                 and oh.location(+)          = pch.loc_value
                 and 'L'                     = LP_system_options_rec.gen_con_invc_itm_sup_loc_ind
              union all
              select oh.order_no,
                     pch.im_dept,
                     pch.po_invc_date,
                     pch.im_item,
                     pch.is_supplier,
                     decode(oh.order_no, null, 'N', 'Y') existing_order_ind
                from svc_posupld_cons_helper_gtt pch,
                     ordhead oh
               where pch.tran_type           = 'S'
                 and pch.sales_process_id    = I_sales_process_id
                 and pch.chunk_id            = I_chunk_id
                 and pch.is_consignment_rate is not null
                 and oh.dept(+)              = pch.im_dept
                 and oh.written_date(+)      = pch.po_invc_date
                 and oh.supplier(+)          = pch.is_supplier
                 and oh.orig_ind(+)          = 4
                 and oh.location(+)          = pch.loc_value
                 and oh.item(+)              = pch.im_item
                 and 'I'                     = LP_system_options_rec.gen_con_invc_itm_sup_loc_ind) inner;

   cursor C_GET_CONSIGN_INVC is
      select pch.posupld_consignment_helper_id,
             ih.invc_id,
             decode(ih.invc_id, null, 'N', 'Y') existing_invc_ind
        from svc_posupld_cons_helper_gtt pch,
             invc_head ih
       where pch.sales_process_id  = I_sales_process_id
         and pch.chunk_id          = I_chunk_id
         and ih.ext_ref_no(+)      = to_char (nvl(pch.order_no, pch.rtv_order_no))
         and ih.status(+)         != 'P'
         and ih.invc_date(+)       = pch.po_invc_date
         and ih.edi_sent_ind(+)   != 'Y';

   TYPE consign_data_rtv_type is TABLE of C_GET_CONSIGN_RTV%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE consign_data_order_type is TABLE of C_GET_CONSIGN_ORDER%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE consign_data_invc_type is TABLE OF C_GET_CONSIGN_INVC%ROWTYPE INDEX BY BINARY_INTEGER;

   L_consign_rtv_tab     CONSIGN_DATA_RTV_TYPE;
   L_consign_order_tab   CONSIGN_DATA_ORDER_TYPE;
   L_consign_invc_tab    CONSIGN_DATA_INVC_TYPE;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   insert into svc_posupld_cons_helper_gtt (posupld_consignment_helper_id,
                                            posupld_line_item_id,
                                            sales_process_id,
                                            chunk_id,
                                            sales_thead_id,
                                            tran_type,
                                            sales_sign,
                                            im_item,
                                            im_pack_ind,
                                            im_dept,
                                            im_class,
                                            im_subclass,
                                            loc_value,
                                            is_supplier,
                                            is_consignment_rate,
                                            sups_currency_code,
                                            sups_terms,
                                            sups_edi_sales_rpt_freq,
                                            sups_ret_courier,
                                            sups_handling_pct,
                                            sups_ret_allow_ind,
                                            sups_auto_appr_invc_ind,
                                            terms_percent,
                                            terms_duedays,
                                            vi_vat_code,
                                            vi_vat_rate,
                                            eow_date,
                                            eom_date,
                                            po_invc_date,
                                            sales_value_without_vat,
                                            total_wastage_qty,
                                            order_no,
                                            existing_order_ind,
                                            rtv_order_no,
                                            existing_rtv_ind,
                                            total_unit_cost_loc,
                                            total_av_cost_loc)
      select posupld_consignment_helper_seq.nextval,
             pli.posupld_line_item_id,
             pli.sales_process_id,
             pli.chunk_id,
             pli.sales_thead_id,
             pli.tran_type_char,
             pli.sales_sign_num,
             pli.im_item,
             case when pli.im_pack_ind = 'Y' and pli.pack_comp_ind = 'Y' then
                'N'
             else
                pli.im_pack_ind
             end as im_pack_ind,
             pli.im_dept,
             pli.im_class,
             pli.im_subclass,
             pli.loc_value,
             pli.is_supplier,
             pli.is_consignment_rate,
             pli.sups_currency_code,
             pli.sups_terms,
             pli.sups_edi_sales_rpt_freq,
             pli.sups_ret_courier,
             pli.sups_handling_pct,
             pli.sups_ret_allow_ind,
             pli.sups_auto_appr_invc_ind,
             pli.terms_percent,
             pli.terms_duedays,
             pli.vi_vat_code,
             nvl(pli.vi_vat_rate,0),
             --
             pli.eow_date,
             pli.eom_date,
             decode(LP_system_options_rec.gen_consignment_invc_freq,
                    'P', L_vdate,
                    'D', L_vdate,
                    'W', (case when L_vdate > pli.eow_date then
                             L_vdate
                          else
                             pli.eow_date
                          end),
                    'M', pli.eom_date,
                    pli.eow_date) po_invc_date,
             --
             pli.sales_value_without_vat,
             pli.total_wastage_qty,
             --
             null, --pli.order_no,
             null, --existing_order_ind,
             null, --rtv_order_no,
             null,  --existing_rtv_ind
             pli.total_unit_cost_loc,
             pli.total_av_cost_loc
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id    = I_sales_process_id
         and pli.chunk_id            = I_chunk_id
         and pli.error_msg           is null
         and pli.xform_sellable_item is null
         and pli.is_consignment_rate is not null
         and pli.im_pack_ind         = 'N'; --pack_comp_ind

   open  C_GET_CONSIGN_RTV;
   fetch C_GET_CONSIGN_RTV bulk collect into L_consign_rtv_tab;
   close C_GET_CONSIGN_RTV;
   ---
   if L_consign_rtv_tab.FIRST is not NULL then
      FOR consign_rec in L_consign_rtv_tab.FIRST..L_consign_rtv_tab.LAST LOOP
         if L_consign_rtv_tab(consign_rec).rtv_order_no is NULL or LP_system_options_rec.gen_consignment_invc_freq = 'P' then
            L_consign_rtv_tab(consign_rec).rtv_order_no := NULL;
            NEXT_RTV_ORDER_NO(L_rtv_order_no,
                              L_return_code,
                              O_error_message);
            if (L_return_code = 'FALSE') THEN
               return FALSE;
            end if;
         end if;
         update svc_posupld_cons_helper_gtt pch
            set pch.rtv_order_no     = nvl(L_consign_rtv_tab(consign_rec).rtv_order_no, L_rtv_order_no),
                pch.existing_rtv_ind = L_consign_rtv_tab(consign_rec).existing_rtv_ind
          where pch.sales_process_id = I_sales_process_id
            and pch.chunk_id         = I_chunk_id
            and pch.po_invc_date     = L_consign_rtv_tab(consign_rec).po_invc_date
            and pch.im_item          = L_consign_rtv_tab(consign_rec).im_item
            and pch.loc_value        = L_consign_rtv_tab(consign_rec).loc_value
            and pch.is_supplier      = L_consign_rtv_tab(consign_rec).is_supplier
            and pch.tran_type        = 'R';
      END LOOP;
   end if;
   ---
   open  C_GET_CONSIGN_ORDER;
   fetch C_GET_CONSIGN_ORDER bulk collect into L_consign_order_tab;
   close C_GET_CONSIGN_ORDER;
   ---
   if L_consign_order_tab.FIRST is not NULL then
      FOR consign_rec in L_consign_order_tab.FIRST..L_consign_order_tab.LAST LOOP
         if LP_system_options_rec.gen_consignment_invc_freq = 'P' or L_consign_order_tab(consign_rec).order_no is null then
            L_consign_order_tab(consign_rec).existing_order_ind := 'N';
            NEXT_ORDER_NUMBER(L_order_no,
                              L_return_code,
                              O_error_message);
            if L_return_code = 'FALSE' then
               return FALSE;
            end if;
         end if;
         update svc_posupld_cons_helper_gtt pch
            set pch.order_no           = nvl(L_consign_order_tab(consign_rec).order_no, L_order_no),
                pch.existing_order_ind = L_consign_order_tab(consign_rec).existing_order_ind
          where pch.tran_type          = 'S'
            and pch.sales_process_id   = I_sales_process_id
            and pch.chunk_id           = I_chunk_id
            and pch.is_consignment_rate is not null
            and pch.im_dept            = L_consign_order_tab(consign_rec).im_dept
            and pch.po_invc_date       = L_consign_order_tab(consign_rec).po_invc_date
            and pch.is_supplier        = L_consign_order_tab(consign_rec).is_supplier
            and pch.im_item            = L_consign_order_tab(consign_rec).im_item;
      END LOOP;
   end if;

   open  C_GET_CONSIGN_INVC;
   fetch C_GET_CONSIGN_INVC bulk collect into L_consign_invc_tab;
   close C_GET_CONSIGN_INVC;

   if L_consign_invc_tab.FIRST is not NULL then
      FOR consign_rec in L_consign_invc_tab.FIRST..L_consign_invc_tab.LAST LOOP
         if L_consign_invc_tab(consign_rec).invc_id = -9999 or L_consign_invc_tab(consign_rec).invc_id is NULL then
            if INVC_SQL.NEXT_INVC_ID(O_error_message,
                                     L_invc_id) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         update svc_posupld_cons_helper_gtt pch
            set pch.invc_id  = decode(nvl(L_consign_invc_tab(consign_rec).invc_id, 0), 0, L_invc_id,
                                                                                      -9999, L_invc_id,
                                                                                      L_consign_invc_tab(consign_rec).invc_id),--is:invc_sequence.nextval),
                pch.existing_invc_ind = decode(nvl(L_consign_invc_tab(consign_rec).invc_id, 0), 0, 'N',
                                                                                      -9999, 'N',
                                                                                      L_consign_invc_tab(consign_rec).existing_invc_ind)
          where pch.posupld_consignment_helper_id = L_consign_invc_tab(consign_rec).posupld_consignment_helper_id
            and pch.sales_process_id = I_sales_process_id
            and pch.chunk_id = I_chunk_id;
      END LOOP;
   end if;

   if LOCK_CONSIGNMENT_TABLES(O_error_message,
                              I_sales_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_RTV_HEAD(O_error_message,
                       I_sales_process_id,
                       I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_ORDHEAD(O_error_message,
                      I_sales_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_INVC_HEAD(O_error_message,
                        I_sales_process_id,
                        I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_INVC_DETAIL(O_error_message,
                          I_sales_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   -- only attempt to insert/update records in INVC_MERCH_VAT if the default tax type is SVAT
   if LP_system_options_rec.default_tax_type = 'SVAT' then
      if PERSIST_INVC_MERCH_VAT(O_error_message,
                                I_sales_process_id,
                                I_chunk_id) = FALSE then
         return FALSE;
      end if;
   end if;

   if PERSIST_CONSIGN_TRAN_DATA(O_error_message,
                                I_sales_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_CONSIGN_VAT_OUT(O_error_message,
                              I_sales_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_CONSIGNMENT_DATA',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_CONSIGNMENT_DATA;
----------------------------------------------------------------------------------------
FUNCTION LOCK_CONSIGNMENT_TABLES(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_sales_process_id   IN              NUMBER,
                                 I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_table                 VARCHAR2(30) := null;
   L_vdate                 DATE := GET_VDATE();
   --
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_retry_lock_attempts   RMS_PLSQL_BATCH_CONFIG.RETRY_LOCK_ATTEMPTS%TYPE;
   L_retry_wait_time       RMS_PLSQL_BATCH_CONFIG.RETRY_WAIT_TIME%TYPE;
   L_locked_acquired       VARCHAR2(1) := 'Y';

   cursor C_GET_LOCK_PARAM is
      select NVL(retry_lock_attempts,2),
             NVL(retry_wait_time,0)
        from rms_plsql_batch_config
       where program_name = 'CORESVC_SALES_UPLOAD_SQL';

   cursor C_LOCK_RTV is
      select 'x'
        from svc_posupld_cons_helper_gtt pch,
             rtv_head rtv
       where pch.sales_process_id    = I_sales_process_id
         and pch.chunk_id            = I_chunk_id
         and rtv.supplier            = pch.is_supplier
         and rtv.item                = pch.im_item
         and rtv.store               = pch.loc_value
         and rtv.created_date        = pch.po_invc_date
         and pch.im_pack_ind         = 'N'
         for update of rtv.total_order_amt nowait;

   cursor C_LOCK_INVC_MERCH_VAT is
      select 'x'
        from svc_posupld_cons_helper_gtt pch,
             invc_merch_vat vat
       where pch.sales_process_id    = I_sales_process_id
         and pch.chunk_id            = I_chunk_id
         and vat.invc_id             = pch.invc_id
         and vat.vat_code            = pch.vi_vat_code
         for update of vat.total_cost_excl_vat nowait;

    cursor C_LOCK_INVC_HEAD is
      select 'x'
        from invc_head ih
       where exists (select 1
                       from invc_head ih,
                            svc_posupld_cons_helper_gtt pch
                      where pch.sales_process_id  = I_sales_process_id
                        and pch.chunk_id          = I_chunk_id
                        and pch.existing_invc_ind = 'Y'
                        and pch.invc_id           = ih.invc_id
                        and rownum = 1)
        for update of ih.total_merch_cost, ih.total_qty nowait;

    cursor C_LOCK_INVC_DETAIL is
      select 'x'
        from invc_detail id
       where exists (select 1
                       from svc_posupld_cons_helper_gtt pch
                      where pch.sales_process_id    = I_sales_process_id
                        and pch.chunk_id            = I_chunk_id
                        and pch.existing_invc_ind   = 'Y'
                        and pch.invc_id             = id.invc_id
                        and pch.im_item             = id.item
                        and pch.total_unit_cost_loc / pch.total_wastage_qty = id.invc_unit_cost
                        and rownum = 1)
        for update of id.invc_qty nowait;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_LOCK_PARAM;
   fetch C_GET_LOCK_PARAM into L_retry_lock_attempts,
                               L_retry_wait_time;
   close C_GET_LOCK_PARAM;
   --
   L_table := 'RTV_HEAD';
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   FOR i in 1..(l_retry_lock_attempts - 1) LOOP
      ---
      BEGIN
         open C_LOCK_RTV;
         close C_LOCK_RTV;
         L_locked_acquired := 'Y';
         exit;
         --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      --
      dbms_lock.sleep(L_retry_wait_time);
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      open C_LOCK_RTV;
      close C_LOCK_RTV;
   end if;

   L_locked_acquired := 'N';
   -----------------------------------------------------
   L_table := 'INVC_MERCH_VAT';
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   FOR i in 1..(l_retry_lock_attempts - 1) LOOP
      ---
      BEGIN
         open C_LOCK_INVC_MERCH_VAT;
         close C_LOCK_INVC_MERCH_VAT;
         L_locked_acquired := 'Y';
         exit;
         --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      --
      dbms_lock.sleep(L_retry_wait_time);
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      open C_LOCK_INVC_MERCH_VAT;
      close C_LOCK_INVC_MERCH_VAT;
   end if;

   --
   -----------------------------------------------------
   L_table := 'INVC_HEAD';
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   FOR i in 1..(l_retry_lock_attempts - 1) LOOP
      ---
      BEGIN
         open C_LOCK_INVC_HEAD;
         close C_LOCK_INVC_HEAD;
         L_locked_acquired := 'Y';
         exit;
         --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      --
      dbms_lock.sleep(L_retry_wait_time);
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      open C_LOCK_INVC_HEAD;
      close C_LOCK_INVC_HEAD;
   end if;

   --
   -----------------------------------------------------
   L_table := 'INVC_DETAIL';
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   FOR i in 1..(l_retry_lock_attempts - 1) LOOP
      ---
      BEGIN
         open C_LOCK_INVC_DETAIL;
         close C_LOCK_INVC_DETAIL;
         L_locked_acquired := 'Y';
         exit;
         --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      --
      dbms_lock.sleep(L_retry_wait_time);
   END LOOP;

   -- Retry the lock for the last time if previous attempts failed
   if L_locked_acquired != 'Y' then
      open C_LOCK_INVC_DETAIL;
      close C_LOCK_INVC_DETAIL;
   end if;
   --
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            null,
                                            null);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.LOCK_CONSIGNMENT_TABLES',
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_CONSIGNMENT_TABLES;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_RTV_HEAD(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_sales_process_id   IN              NUMBER,
                          I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   merge into rtv_head rh
   using (
      select distinct
             pch.is_supplier,
             pch.im_item,
             pch.loc_value,
             pch.sups_ret_courier,
             pch.sups_handling_pct,
             a.add_1,
             a.add_2,
             a.add_3,
             a.city,
             a.state,
             a.country_id,
             a.post,
             pch.po_invc_date,
             --convert to supplier curency before last * by wast qty
             sum(((pch.sales_value_without_vat / pch.is_consignment_rate) / pch.total_wastage_qty) * total_wastage_qty)
             over (partition by pch.rtv_order_no) total_order_amt,
             pch.rtv_order_no,
             pch.existing_rtv_ind
        from svc_posupld_cons_helper_gtt pch,
             addr a
       where pch.sales_process_id    = I_sales_process_id
         and pch.chunk_id            = I_chunk_id
         and pch.tran_type           = 'R'
         and pch.sups_ret_allow_ind  = 'Y'
         and a.key_value_1           = pch.is_supplier
         and a.module                = 'SUPP'
         and a.addr_type             = 3) use_this
   on (rh.rtv_order_no           = use_this.rtv_order_no
       and use_this.existing_rtv_ind = 'Y')
   when matched then
      update set rh.total_order_amt = nvl(rh.total_order_amt,0) + use_this.total_order_amt
   when not matched then
      insert (rtv_order_no,
              supplier,
              status_ind,
              store,
              wh,
              total_order_amt,
              ship_to_add_1,
              ship_to_add_2,
              ship_to_add_3,
              ship_to_city,
              state,
              ship_to_country_id,
              ship_to_pcode,
              ret_auth_num,
              courier,
              freight,
              created_date,
              completed_date,
              ext_ref_no,
              comment_desc,
              mrt_no,
              not_after_date,
              restock_pct,
              restock_cost,
             item)
      values (use_this.rtv_order_no,
              use_this.is_supplier,
              15,
              use_this.loc_value,
               -1,
              use_this.total_order_amt,
              use_this.add_1,
              use_this.add_2,
              use_this.add_3,
              use_this.city,
              use_this.state,
              use_this.country_id,
              use_this.post,
              null,
              use_this.sups_ret_courier,
              null,
              use_this.po_invc_date,
              use_this.po_invc_date,
              null,
              null,
              null,
              null,
              null,
              null,
              use_this.im_item);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_RTV_HEAD',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_RTV_HEAD;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_ORDHEAD(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_sales_process_id   IN              NUMBER,
                         I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_vdate   DATE := GET_VDATE();

   cursor C_GET_ORDER_DETL is
      select distinct pch.im_dept,
             pch.is_supplier,
             pch.po_invc_date,
             pch.eom_date,
             pch.eow_date,
             pch.sups_terms,
             pch.sups_currency_code,
             pch.loc_value,
             'S' as s_loc_type,
             pch.im_item,
             pch.order_no,
             pch.existing_order_ind
        from svc_posupld_cons_helper_gtt pch
       where pch.sales_process_id = I_sales_process_id
         and pch.chunk_id         = I_chunk_id
         and pch.tran_type        = 'S'
         and not exists (select 'x'
                            from ordhead oh,
                                 svc_posupld_cons_helper_gtt pch1
                           where pch1.order_no = pch.order_no
                             and oh.order_no = pch1.order_no
                             and pch1.existing_order_ind = 'Y'
                             and rownum = 1);

   TYPE order_detl_typ is TABLE of C_GET_ORDER_DETL%ROWTYPE INDEX BY BINARY_INTEGER;

   L_order_detl_tab   ORDER_DETL_TYP;

   L_order_no         ORDHEAD.ORDER_NO%TYPE := NULL;
   L_return_val       VARCHAR2(5);

BEGIN

  ---
  if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_ORDER_DETL;
   fetch C_GET_ORDER_DETL bulk collect into L_order_detl_tab;
   close C_GET_ORDER_DETL;
   ---
   if L_order_detl_tab.FIRST is not null then
      FOR order_dtl_ind in L_order_detl_tab.FIRST..L_order_detl_tab.LAST LOOP
         insert into ordhead (order_no,
                              order_type,
                              dept,
                              supplier,
                              supp_add_seq_no,
                              qc_ind,
                              written_date,
                              not_before_date,
                              not_after_date,
                              otb_eow_date,
                              terms,
                              status,
                              edi_po_ind,
                              po_ack_recvd_ind,
                              pre_mark_ind,
                              currency_code,
                              include_on_order_ind,
                              import_order_ind,
                              import_country_id,
                              loc_type,
                              location,
                              item,
                              orig_ind)
                      values (L_order_detl_tab(order_dtl_ind).order_no,
                              'N/B',
                              L_order_detl_tab(order_dtl_ind).im_dept,
                              L_order_detl_tab(order_dtl_ind).is_supplier,
                              1,
                              'N',
                              L_order_detl_tab(order_dtl_ind).po_invc_date,
                              L_order_detl_tab(order_dtl_ind).po_invc_date,
                              L_order_detl_tab(order_dtl_ind).po_invc_date,
                              decode(LP_system_options_rec.gen_consignment_invc_freq,
                                     'M', L_order_detl_tab(order_dtl_ind).eom_date,
                                          L_order_detl_tab(order_dtl_ind).eow_date),
                              L_order_detl_tab(order_dtl_ind).sups_terms,
                              'C',
                              'N',
                              'N',
                              'N',
                              L_order_detl_tab(order_dtl_ind).sups_currency_code,
                              'Y',
                              'N',
                              LP_system_options_rec.base_country_id,
                              decode(LP_system_options_rec.gen_con_invc_itm_sup_loc_ind,
                                     'L', L_order_detl_tab(order_dtl_ind).s_loc_type,
                                     'I', L_order_detl_tab(order_dtl_ind).s_loc_type,
                                          null),
                              decode(LP_system_options_rec.gen_con_invc_itm_sup_loc_ind,
                                     'L', L_order_detl_tab(order_dtl_ind).loc_value,
                                     'I', L_order_detl_tab(order_dtl_ind).loc_value,
                                          null),
                              decode(LP_system_options_rec.gen_con_invc_itm_sup_loc_ind,
                                     'I', L_order_detl_tab(order_dtl_ind).im_item,
                                          null),
                              4);
         ---
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_ORDHEAD',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_ORDHEAD;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_INVC_HEAD(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_sales_process_id   IN              NUMBER,
                           I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_vdate   DATE := GET_VDATE();

   cursor C_GET_INVC_DETL is
      select pch.invc_id,
             pch.existing_invc_ind,
             decode(TRAN_TYPE,'R','P','O') invc_type,
             pch.is_supplier,
             nvl(pch.order_no, pch.rtv_order_no) ext_ref_no,
             pch.sups_auto_appr_invc_ind,
             decode(pch.sups_auto_appr_invc_ind, 'Y', 'A', 'M') status,
             pch.rtv_order_no,
             pch.sups_terms,
             pch.po_invc_date,
             pch.terms_percent,
             NVL(pch.terms_duedays, 0) terms_duedays,
             pch.sups_currency_code,
             pch.total_unit_cost_loc * pch.sales_sign as total_merch_cost,
             pch.total_wastage_qty * pch.sales_sign as total_qty,
             addr.addr_key
        from svc_posupld_cons_helper_gtt pch,
             addr
       where pch.sales_process_id    = I_sales_process_id
         and pch.chunk_id            = I_chunk_id
         and addr.key_value_1        = to_char(pch.is_supplier)
         and addr.module             = 'SUPP'
         and addr.primary_addr_ind   = 'Y'
         and addr.addr_type          = '05'
         and not exists (select 'x'
                           from invc_head ih,
                                svc_posupld_cons_helper_gtt pch1
                          where pch1.invc_id = pch.invc_id
                            and ih.invc_id = pch.invc_id
                            and pch1.existing_invc_ind = 'Y'
                            and rownum = 1);

   cursor C_GET_INVC_HEAD is
      select ih.invc_id,
             sum(pch.total_unit_cost_loc * pch.sales_sign) total_merch_cost ,
             sum(pch.total_wastage_qty * pch.sales_sign) total_qty
        from invc_head ih,
             svc_posupld_cons_helper_gtt pch
       where pch.sales_process_id  = I_sales_process_id
         and pch.chunk_id          = I_chunk_id
         and pch.existing_invc_ind = 'Y'
         and pch.invc_id           = ih.invc_id
    group by ih.invc_id;

   TYPE invc_detl_typ is TABLE of C_GET_INVC_DETL%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE invc_head_typ is TABLE of C_GET_INVC_HEAD%ROWTYPE INDEX BY BINARY_INTEGER;

   L_invc_detl_tab   INVC_DETL_TYP;
   L_invc_head_tab   INVC_HEAD_TYP;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_INVC_HEAD;
   fetch C_GET_INVC_HEAD bulk collect into L_invc_head_tab;
   close C_GET_INVC_HEAD;

   if L_invc_head_tab.FIRST is not null then
      FORALL invc_head_ind in L_invc_head_tab.FIRST..L_invc_head_tab.LAST
         update invc_head
            set total_merch_cost = total_merch_cost + L_invc_head_tab(invc_head_ind).total_merch_cost,
                total_qty        = total_qty +  L_invc_head_tab(invc_head_ind).total_qty
          where invc_id = L_invc_head_tab(invc_head_ind).invc_id;
   end if;

   open C_GET_INVC_DETL;
   fetch C_GET_INVC_DETL bulk collect into L_invc_detl_tab;
   close C_GET_INVC_DETL;

   if L_invc_detl_tab.FIRST is not null then
      FOR invc_detl_ind in L_invc_detl_tab.FIRST..L_invc_detl_tab.LAST LOOP
         insert into invc_head (invc_id,
                                invc_type,
                                supplier,
                                ext_ref_no,
                                status,
                                edi_invc_ind,
                                edi_sent_ind,
                                match_fail_ind,
                                ref_invc_id,
                                ref_rtv_order_no,
                                ref_rsn_code,
                                terms,
                                due_date,
                                payment_method,
                                terms_dscnt_pct,
                                terms_dscnt_appl_ind,
                                terms_dscnt_appl_non_mrch_ind,
                                freight_terms,
                                create_id,
                                create_date,
                                invc_date,
                                match_id,
                                match_date,
                                approval_id,
                                approval_date,
                                force_pay_ind,
                                force_pay_id,
                                post_date,
                                currency_code,
                                exchange_rate,
                                total_merch_cost,
                                total_qty,
                                direct_ind,
                                addr_key,
                                paid_ind,
                                comments)
                        values (L_invc_detl_tab(invc_detl_ind).invc_id,
                                L_invc_detl_tab(invc_detl_ind).invc_type,
                                L_invc_detl_tab(invc_detl_ind).is_supplier,
                                L_invc_detl_tab(invc_detl_ind).ext_ref_no,
                                L_invc_detl_tab(invc_detl_ind).status,
                                'N',
                                'N',
                                'N',
                                null,
                                L_invc_detl_tab(invc_detl_ind).rtv_order_no,
                                null,
                                L_invc_detl_tab(invc_detl_ind).sups_terms,
                                L_invc_detl_tab(invc_detl_ind).po_invc_date + L_invc_detl_tab(invc_detl_ind).terms_duedays,
                                null,
                                L_invc_detl_tab(invc_detl_ind).terms_percent,
                                'N',
                                'N',
                                null,
                                'salesprocess',
                                sysdate,
                                L_invc_detl_tab(invc_detl_ind).po_invc_date,
                                'salesprocess',
                                L_invc_detl_tab(invc_detl_ind).po_invc_date,
                                decode(L_invc_detl_tab(invc_detl_ind).sups_auto_appr_invc_ind, 'Y', 'salesprocess', null),
                                decode(L_invc_detl_tab(invc_detl_ind).sups_auto_appr_invc_ind, 'Y', L_invc_detl_tab(invc_detl_ind).po_invc_date, null),
                                'N',
                                null,
                                null,
                                L_invc_detl_tab(invc_detl_ind).sups_currency_code,
                                null,
                                L_invc_detl_tab(invc_detl_ind).total_merch_cost,
                                L_invc_detl_tab(invc_detl_ind).total_qty,
                                'N',
                                L_invc_detl_tab(invc_detl_ind).addr_key,
                                'N',
                                null);
      END LOOP;
   end if;

   insert into invc_xref (invc_id,
                          order_no,
                          shipment,
                          asn,
                          location,
                          loc_type,
                          apply_to_future_ind)
   select distinct pch.invc_id,
          pch.order_no,
          null,
          null,
          pch.loc_value,
          'S',
          'N'
     from svc_posupld_cons_helper_gtt pch
    where pch.sales_process_id    = I_sales_process_id
      and pch.chunk_id            = I_chunk_id
      and pch.existing_invc_ind   = 'N';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_INVC_HEAD',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_INVC_HEAD;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_INVC_DETAIL(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sales_process_id   IN              NUMBER,
                             I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   cursor C_GET_INVC_DETAIL_QTY is
      select id.invc_id,
             sum(pch.total_wastage_qty) invc_qty
        from invc_detail id,
             svc_posupld_cons_helper_gtt pch
       where pch.sales_process_id    = I_sales_process_id
         and pch.chunk_id            = I_chunk_id
         and pch.existing_invc_ind   = 'Y'
         and pch.invc_id             = id.invc_id
         and pch.im_item             = id.item
         and ROUND(pch.total_unit_cost_loc / pch.total_wastage_qty, 4) = ROUND(id.invc_unit_cost, 4)
    group by id.invc_id;

   TYPE invc_detail_qty_typ is TABLE of C_GET_INVC_DETAIL_QTY%ROWTYPE INDEX BY BINARY_INTEGER;

   L_invc_detl_qty_tab   INVC_DETAIL_QTY_TYP;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   
   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   --invc_detail record exists
   open C_GET_INVC_DETAIL_QTY;
   fetch C_GET_INVC_DETAIL_QTY bulk collect into L_invc_detl_qty_tab;
   close C_GET_INVC_DETAIL_QTY;

   if L_invc_detl_qty_tab.FIRST is not null then
      FORALL detail_recs IN L_invc_detl_qty_tab.FIRST..L_invc_detl_qty_tab.LAST
         update invc_detail
            set invc_qty = invc_qty + L_invc_detl_qty_tab(detail_recs).invc_qty
          where invc_id = L_invc_detl_qty_tab(detail_recs).invc_id;
   end if;
   --invc_detail record does not exists
   insert into invc_detail(invc_id,
                           item,
                           ref_item,
                           invc_unit_cost,
                           invc_qty,
                           invc_vat_rate,
                           status,
                           orig_unit_cost,
                           orig_qty,
                           orig_vat_rate,
                           cost_dscrpncy_ind,
                           qty_dscrpncy_ind,
                           vat_dscrpncy_ind,
                           processed_ind,
                           comments,
                           vat_code)
   select pch.invc_id,
          pch.im_item,
          null,
          pch.total_unit_cost_loc / pch.total_wastage_qty,  --convert to sup currency
          sum(pch.total_wastage_qty * pch.sales_sign)
          over (partition by pch.invc_id, pch.im_item, pch.sales_value_without_vat) invc_qty,
          pch.vi_vat_rate,
          'M',
          null,
          null,
          null,
          'N',
          'N',
          'N',
          'N',
          null,
          pch.vi_vat_code
     from svc_posupld_cons_helper_gtt pch
    where pch.sales_process_id      = I_sales_process_id
      and pch.chunk_id              = I_chunk_id
      and not exists(select 'x'
                       from invc_detail id
                      where pch.invc_id                 = id.invc_id
                        and pch.im_item                 = id.item
                        and ROUND(pch.total_unit_cost_loc / pch.total_wastage_qty, 4) = ROUND(id.invc_unit_cost, 4)
                        and rownum = 1);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_INVC_DETAIL',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_INVC_DETAIL;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_INVC_MERCH_VAT(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sales_process_id   IN              NUMBER,
                                I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---

   merge into invc_merch_vat v
   using (
      select distinct
             pch.invc_id,
             pch.vi_vat_code,
             sum(((pch.total_unit_cost_loc) / pch.total_wastage_qty) * pch.total_wastage_qty * pch.sales_sign)
             over (partition by pch.invc_id, pch.vi_vat_code) total_cost_excl_vat
        from svc_posupld_cons_helper_gtt pch
       where pch.sales_process_id = I_sales_process_id
         and pch.chunk_id = I_chunk_id
         and pch.vi_vat_code is NOT NULL) use_this
   on (v.invc_id  = use_this.invc_id
       and v.vat_code = use_this.vi_vat_code)
   when matched then
      update set v.total_cost_excl_vat = v.total_cost_excl_vat + use_this.total_cost_excl_vat
   when not matched then
      insert (invc_id,
              vat_code,
              total_cost_excl_vat)
      values (use_this.invc_id,
              use_this.vi_vat_code,
              use_this.total_cost_excl_vat);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_INVC_MERCH_VAT',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_INVC_MERCH_VAT;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_CONSIGN_TRAN_DATA(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id   IN              NUMBER,
                                   I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   cursor C_GET_CONSIGN_ITEMS is
      select distinct pli.im_item,
             pli.im_dept,
             pli.im_class,
             pli.im_subclass,
             pli.loc_value,
             pli.tran_date,
             decode(pli.tran_type_char, 'R', 24, 20) tran_code, --Purchases or Return to Vendor
             --For tran codes 20/24, do NOT multiply units/total_cost/total_retail by pli.tran_type_num since these
             --tran codes already account for it being a sales/return transaction.
             pli.total_wastage_qty,
             pli.sales_value_without_vat * (pli.is_consignment_rate/100) as total_cost, --is:prom_consignment_rate
             case when pli.class_class_vat_ind = 'N' then
                pli.sales_value_without_vat
             else
                pli.sales_value_with_vat
             end as total_retail,
             nvl(ch.order_no, ch.rtv_order_no) order_no,
             pli.pack_no
        from svc_posupld_line_item_gtt pli,
             svc_posupld_cons_helper_gtt ch
       where pli.sales_process_id        = I_sales_process_id
         and pli.chunk_id                = I_chunk_id
         and pli.sales_process_id        = ch.sales_process_id
         and pli.chunk_id                = ch.chunk_id
         and pli.posupld_line_item_id    = ch.posupld_line_item_id         
         and pli.pack_no                 is null
         and pli.im_item_xform_ind       = 'N'
         and pli.is_consignment_rate     is not null;

   TYPE consign_type is TABLE of C_GET_CONSIGN_ITEMS%ROWTYPE INDEX BY BINARY_INTEGER;

   L_consign_tab   CONSIGN_TYPE;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_CONSIGN_ITEMS;
   fetch C_GET_CONSIGN_ITEMS bulk collect into L_consign_tab;
   close C_GET_CONSIGN_ITEMS;
   --
   if L_consign_tab.FIRST is not null then
      FOR cons_ind in L_consign_tab.FIRST..L_consign_tab.LAST LOOP
         if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                          L_consign_tab(cons_ind).im_item,
                                          L_consign_tab(cons_ind).im_dept,
                                          L_consign_tab(cons_ind).im_class,
                                          L_consign_tab(cons_ind).im_subclass,
                                          L_consign_tab(cons_ind).loc_value,
                                          'S',
                                          L_consign_tab(cons_ind).tran_date,
                                          L_consign_tab(cons_ind).tran_code,
                                          NULL,
                                          L_consign_tab(cons_ind).total_wastage_qty,
                                          L_consign_tab(cons_ind).total_cost,
                                          L_consign_tab(cons_ind).total_retail,
                                          L_consign_tab(cons_ind).order_no,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'salesprocess') = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_CONSIGN_TRAN_DATA',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_CONSIGN_TRAN_DATA;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_CONSIGN_VAT_OUT(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_sales_process_id   IN              NUMBER,
                                 I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if LP_system_options_rec.default_tax_type = 'SVAT' then
      insert into tran_data (item,
                             dept,
                             class,
                             subclass,
                             pack_ind,
                             loc_type,
                             location,
                             tran_date,
                             tran_code,
                             adj_code,
                             units,
                             total_cost,
                             total_retail,
                             ref_no_1,
                             ref_no_2,
                             gl_ref_no,
                             old_unit_retail,
                             new_unit_retail,
                             pgm_name,
                             sales_type,
                             vat_rate,
                             av_cost,
                             timestamp,
                             ref_pack_no,
                             total_cost_excl_elc)
      select distinct pli.im_item,
             pli.im_dept,
             pli.im_class,
             pli.im_subclass,
             case when pli.im_pack_ind = 'Y' and pli.pack_comp_ind = 'Y' then
                'N'
             else
                pli.im_pack_ind
             end as im_pack_ind,
             'S',
             pli.loc_value,
             pli.tran_date,
             88 as tran_code, -- Vat Out Retail
             null,--is:
             pli.sales_qty_std_uom * pli.tran_type_num units,
             null as  total_cost,
             ROUND(NVL(pli.vi_vat_amt,0) * pli.tran_type_num, pli.st_currency_rtl_dec),
             null,
             null,
             pli.vi_vat_code,
             null,
             null,
             'salesprocess',
             null,
             null,
             null,
             sysdate,
             pli.pack_no,
             null
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id    = I_sales_process_id
         and pli.chunk_id            = I_chunk_id
         and pli.error_msg           is null
         and pli.im_pack_ind         = 'N'
         and pli.is_consignment_rate is not null
         and NVL(pli.vi_vat_amt, 0) != 0;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_CONSIGN_VAT_OUT',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_CONSIGN_VAT_OUT;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_TRAN_DATA(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_sales_process_id   IN              NUMBER,
                           I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if PERSIST_TRAN_DATA_WASTAGE(O_error_message,
                                I_sales_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_TRAN_DATA_DEAL_INCOME(O_error_message,
                                    I_sales_process_id,
                                    I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_TRAN_DATA_SALE(O_error_message,
                             I_sales_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_TRAN_DATA_RETURN(O_error_message,
                               I_sales_process_id,
                               I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_TRAN_DATA_REVERSAL(O_error_message,
                                 I_sales_process_id,
                                 I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_TRAN_DATA_VAT_OUT(O_error_message,
                                I_sales_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_TRAN_DATA_EMP_DISC(O_error_message,
                                 I_sales_process_id,
                                 I_chunk_id) = FALSE then
      return FALSE;
   end if;
   


   if PERSIST_TRAN_DATA_OFF_RETAIL(O_error_message,
                                   I_sales_process_id,
                                   I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_TRAN_DATA_PACK_UP_DOWN(O_error_message,
                                     I_sales_process_id,
                                     I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_TRAN_DATA_PROMO_MKDN(O_error_message,
                                   I_sales_process_id,
                                   I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_TRAN_DATA_WEIGHT_VAR(O_error_message,
                                   I_sales_process_id,
                                   I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_TRAN_DATA',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_TRAN_DATA;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_TRAN_DATA_SALE(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sales_process_id   IN              NUMBER,
                                I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   insert into tran_data (item,
                          dept,
                          class,
                          subclass,
                          pack_ind,
                          loc_type,
                          location,
                          tran_date,
                          tran_code,
                          adj_code,
                          units,
                          total_cost,
                          total_retail,
                          ref_no_1,
                          ref_no_2,
                          gl_ref_no,
                          old_unit_retail,
                          new_unit_retail,
                          pgm_name,
                          sales_type,
                          vat_rate,
                          av_cost,
                          timestamp,
                          ref_pack_no,
                          total_cost_excl_elc)
   select pli.im_item,
          pli.im_dept,
          pli.im_class,
          pli.im_subclass,
          case when pli.im_pack_ind = 'Y' and pli.pack_comp_ind = 'Y' then
             'N'
          else
             pli.im_pack_ind
          end as im_pack_ind,
          'S',
          pli.loc_value,
          pli.tran_date,
          case when pli.im_inventory_ind = 'N' and
                    pli.im_item_xform_ind = 'N' and
                    nvl(pli.im_deposit_item_type, 'X') != 'A' and
                    pli.is_consignment_rate is NULL then
             3
          else
             1
          end as tran_code,
          NULL,
          --
          case when pli.im_inventory_ind = 'N' and
                    pli.im_item_xform_ind = 'N' and
                    nvl(pli.im_deposit_item_type, 'X') != 'A' and
                    pli.is_consignment_rate is NOT NULL then
                   (pli.xform_yield_pct * pli.sales_qty_std_uom * pli.pack_qty) / (1-pli.xform_prod_loss_pct) * pli.tran_type_num
              else
                 decode(pli.subtrans_type_ind,
                    'A', 0,
                    (pli.xform_yield_pct * pli.sales_qty_std_uom * pli.pack_qty) / (1-pli.xform_prod_loss_pct) * pli.tran_type_num)
          end as units,
          --
          case when pli.im_inventory_ind = 'N' and
                    pli.im_item_xform_ind = 'N' and
                    nvl(pli.im_deposit_item_type, 'X') != 'A' and
                    pli.is_consignment_rate is NOT NULL then
                    ROUND(pli.xform_yield_pct *
                             decode(LP_system_options_rec.std_av_ind,
                               'S', pli.total_unit_cost_loc * pli.tran_type_num,
                                pli.total_av_cost_loc   * pli.tran_type_num), pli.st_currency_cost_dec)
               else
                  case when pli.subtrans_type_ind = 'A' or (pli.im_item_xform_ind = 'Y' and pli.total_unit_cost_loc is NULL) then
                     0
                       when pli.im_item_xform_ind = 'Y' and pli.total_unit_cost_loc is not NULL then
                          ROUND(pli.xform_yield_pct * pli.total_unit_cost_loc * pli.tran_type_num, pli.st_currency_cost_dec)
               else
                  ROUND(pli.xform_yield_pct *
                          decode(LP_system_options_rec.std_av_ind,
                                 'S', pli.total_unit_cost_loc * pli.tran_type_num * pli.xform_yield_pct,
                                      pli.total_av_cost_loc   * pli.tran_type_num * pli.xform_yield_pct), pli.st_currency_cost_dec)
               end
          end as total_cost,
          ROUND((pli.xform_yield_pct *
                  decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                         'Y', pli.sales_value_with_vat    * pli.pack_price_ratio * pli.tran_type_num,
                              pli.sales_value_without_vat * pli.pack_price_ratio * pli.tran_type_num)), pli.st_currency_rtl_dec) as total_retail,
          null,
          null,
          null,
          null,
          null,
          'salesprocess',
          pli.sales_type,
          nvl(pli.vi_vat_rate,0),
          case when pli.im_inventory_ind = 'N' and
                    pli.im_item_xform_ind = 'N' and
                    nvl(pli.im_deposit_item_type, 'X') != 'A' and
                    pli.is_consignment_rate is NOT NULL then
             pli.ils_av_cost
          else
             decode(pli.subtrans_type_ind,
                    'A', 0,
                    pli.ils_av_cost)
          end as av_cost,
          SYSDATE,
          pli.pack_no,
          NULL
     from svc_posupld_line_item_gtt pli
    where pli.sales_process_id    = I_sales_process_id
      and pli.chunk_id            = I_chunk_id
      and pli.error_msg           is NULL
      and (pli.im_pack_ind        = 'N'
       or (pli.im_pack_ind        = 'Y'
      and  pli.pack_no            is NOT NULL))
      and pli.is_concession_rate  is NULL
      and ((pli.im_item_xform_ind = 'N')
           or
           (pli.im_item_xform_ind = 'Y' and pli.xform_sellable_item is NOT NULL));

   if LP_system_options_rec.default_tax_type != 'SALES' and LP_system_options_rec.stkldgr_vat_incl_retl_ind = 'Y' then
      insert into tran_data (item,
                             dept,
                             class,
                             subclass,
                             pack_ind,
                             loc_type,
                             location,
                             tran_date,
                             tran_code,
                             adj_code,
                             units,
                             total_cost,
                             total_retail,
                             ref_no_1,
                             ref_no_2,
                             gl_ref_no,
                             old_unit_retail,
                             new_unit_retail,
                             pgm_name,
                             sales_type,
                             vat_rate,
                             av_cost,
                             timestamp,
                             ref_pack_no,
                             total_cost_excl_elc)
      select pli.im_item,
             pli.im_dept,
             pli.im_class,
             pli.im_subclass,
             case when pli.im_pack_ind = 'Y' and pli.pack_comp_ind = 'Y' then
                'N'
             else
                pli.im_pack_ind
             end as im_pack_ind,
             'S',
             pli.loc_value,
             pli.tran_date,
             case when pli.im_inventory_ind = 'N' and
                       pli.im_item_xform_ind = 'N' and
                       nvl(pli.im_deposit_item_type, 'X') != 'A' and
                       pli.is_consignment_rate is NULL then
                5
             else
                2
             end as tran_code,
             null,
             --
             case when pli.im_inventory_ind = 'N' and
                       pli.im_item_xform_ind = 'N' and
                       nvl(pli.im_deposit_item_type, 'X') != 'A' and
                       pli.is_consignment_rate is NOT NULL then
                (pli.xform_yield_pct * pli.sales_qty_std_uom * pli.pack_qty) / (1-pli.xform_prod_loss_pct) * pli.tran_type_num
             else
                case when pli.subtrans_type_ind = 'A' then
                   0
                else
                   (pli.xform_yield_pct * pli.sales_qty_std_uom * pli.pack_qty) / (1-pli.xform_prod_loss_pct) * pli.tran_type_num
                end
             end as units,
             --
             case when pli.im_inventory_ind = 'N' and
                       pli.im_item_xform_ind = 'N' and
                       nvl(pli.im_deposit_item_type, 'X') != 'A' and
                       pli.is_consignment_rate is NOT NULL then
                ROUND(pli.xform_yield_pct *
                      decode(LP_system_options_rec.std_av_ind,
                           'S', pli.total_unit_cost_loc * pli.tran_type_num,
                                pli.total_av_cost_loc   * pli.tran_type_num), pli.st_currency_cost_dec)
             else
                case when pli.subtrans_type_ind = 'A' or (pli.im_item_xform_ind = 'Y' and pli.total_unit_cost_loc is NULL) then
                   0
                when pli.im_item_xform_ind = 'Y' and pli.total_unit_cost_loc is NOT NULL then
                   ROUND(pli.xform_yield_pct * pli.total_unit_cost_loc * pli.tran_type_num, pli.st_currency_cost_dec)
                else
                   ROUND(pli.xform_yield_pct *
                         decode(LP_system_options_rec.std_av_ind,
                          'S', pli.total_unit_cost_loc * pli.tran_type_num,
                               pli.total_av_cost_loc   * pli.tran_type_num), pli.st_currency_cost_dec)
                end
             end as total_cost,
             --
             ROUND(pli.xform_yield_pct * (pli.sales_value_without_vat * pli.pack_price_ratio * pli.tran_type_num), pli.st_currency_rtl_dec) as total_retail,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             'salesprocess',
             NULL,
             NULL, --pli.vi_vat_rate,
             NULL, --pli.ils_av_cost,
             SYSDATE,
             pli.pack_no,
             NULL
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id    = I_sales_process_id
         and pli.chunk_id            = I_chunk_id
         and pli.error_msg           is NULL
         and (pli.im_pack_ind        = 'N'
          or (pli.im_pack_ind        = 'Y'
         and  pli.pack_no             is NOT NULL))
         and pli.is_concession_rate is NULL
         and ((pli.im_item_xform_ind = 'N')
              or
              (pli.im_item_xform_ind = 'Y' and pli.xform_sellable_item is not null));

   end if;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_TRAN_DATA_SALE',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_TRAN_DATA_SALE;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_TRAN_DATA_RETURN(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_sales_process_id   IN              NUMBER,
                                  I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   insert into tran_data (item,
                          dept,
                          class,
                          subclass,
                          pack_ind,
                          loc_type,
                          location,
                          tran_date,
                          tran_code,
                          adj_code,
                          units,
                          total_cost,
                          total_retail,
                          ref_no_1,
                          ref_no_2,
                          gl_ref_no,
                          old_unit_retail,
                          new_unit_retail,
                          pgm_name,
                          sales_type,
                          vat_rate,
                          av_cost,
                          timestamp,
                          ref_pack_no,
                          total_cost_excl_elc)
   select pli.im_item,
          pli.im_dept,
          pli.im_class,
          pli.im_subclass,
          case when pli.im_pack_ind = 'Y' and pli.pack_comp_ind = 'Y' then
             'N'
          else
             pli.im_pack_ind
          end as im_pack_ind,
          'S',
          pli.loc_value,
          pli.tran_date,
          4 tran_code, --Returns
          NULL,
          --For tran code 4, do NOT multiply units/total_cost/total_retail by pli.tran_type_num since this tran code
          --already accounts for it being a return transaction.
          (pli.xform_yield_pct * pli.sales_qty_std_uom * pli.pack_qty) / (1-pli.xform_prod_loss_pct) units,
          ROUND(NVL(decode(pli.subtrans_type_ind,
                           'D', 0,
                           pli.xform_yield_pct *
                           decode(LP_system_options_rec.std_av_ind,
                                  'S', pli.total_unit_cost_loc,
                                       pli.total_av_cost_loc)), 0), pli.st_currency_cost_dec) as total_cost,
          ROUND((pli.xform_yield_pct *
                  decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                         'Y', pli.sales_value_with_vat    * pli.pack_price_ratio,
                              pli.sales_value_without_vat * pli.pack_price_ratio)), pli.st_currency_rtl_dec) as total_retail,
          NULL,
          NULL,
          NULL,
          NULL,
          NULL,
          'salesprocess',
          NULL, -- sales_type
          NULL, -- pli.vi_vat_rate,
          NULL, -- pli.ils_av_cost,
          sysdate,
          pli.pack_no,
          NULL
     from svc_posupld_line_item_gtt pli
    where pli.sales_process_id   = I_sales_process_id
      and pli.chunk_id           = I_chunk_id
      and pli.error_msg          is NULL
      and (pli.im_pack_ind        = 'N' or
           (pli.im_pack_ind        = 'Y'and
            pli.pack_no            is NOT NULL))
      and pli.is_concession_rate  is NULL
      and pli.is_consignment_rate is NULL
      and pli.tran_type_char      = 'R'
      and (pli.im_inventory_ind   = 'Y' or
           NVL(pli.im_deposit_item_type, 'X') = 'A') -- write the reversal only for returns of inventory items
      and ((pli.im_item_xform_ind = 'N')
           or
           (pli.im_item_xform_ind = 'Y' and pli.xform_sellable_item is not null));

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_TRAN_DATA_RETURN',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_TRAN_DATA_RETURN;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_TRAN_DATA_REVERSAL(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id   IN              NUMBER,
                                    I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   insert into tran_data (item,
                          dept,
                          class,
                          subclass,
                          pack_ind,
                          loc_type,
                          location,
                          tran_date,
                          tran_code,
                          adj_code,
                          units,
                          total_cost,
                          total_retail,
                          ref_no_1,
                          ref_no_2,
                          gl_ref_no,
                          old_unit_retail,
                          new_unit_retail,
                          pgm_name,
                          sales_type,
                          vat_rate,
                          av_cost,
                          timestamp,
                          ref_pack_no,
                          total_cost_excl_elc)
   select pli.im_item,
          pli.im_dept,
          pli.im_class,
          pli.im_subclass,
          case when pli.im_pack_ind = 'Y' and pli.pack_comp_ind = 'Y' then
             'N'
          else
             pli.im_pack_ind
          end as im_pack_ind,
          'S',
          pli.loc_value,
          pli.tran_date,
          decode(sign(pli.ph_reg_retail - pli.ph_curr_retail),
                     -1, 14,               --Markdown Cancel
                         13) tran_code,    --Permanent Markdown
          null,
          -1 * (pli.xform_yield_pct * pli.total_wastage_qty * pli.pack_qty * pli.tran_type_num) / (1-pli.xform_prod_loss_pct) units,
          null total_cost,
          -1 * ROUND(((pli.xform_yield_pct * abs(pli.ph_curr_retail - pli.ph_reg_retail) * pli.total_wastage_qty * pli.pack_qty)) * pli.tran_type_num, pli.st_currency_rtl_dec) as total_retail,
          null,
          null,
          null,
          null,
          null,
          'salesprocess',
          pli.sales_type,
          nvl(pli.vi_vat_rate,0),
          pli.ils_av_cost,
          sysdate,
          pli.pack_no,
          null
     from svc_posupld_line_item_gtt pli
    where pli.sales_process_id   = I_sales_process_id
      and pli.chunk_id           = I_chunk_id
      and pli.error_msg          is null
      and pli.im_pack_ind        = 'N'
      and pli.is_concession_rate is null
      and pli.ph_curr_retail    != pli.ph_reg_retail
      and ((pli.im_item_xform_ind = 'N')
            or
            (pli.im_item_xform_ind = 'Y' and pli.xform_sellable_item is not null));

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_TRAN_DATA_REVERSAL',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_TRAN_DATA_REVERSAL;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_TRAN_DATA_VAT_OUT(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_sales_process_id   IN              NUMBER,
                                   I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if LP_system_options_rec.default_tax_type = 'SVAT' then
      insert into tran_data (item,
                             dept,
                             class,
                             subclass,
                             pack_ind,
                             loc_type,
                             location,
                             tran_date,
                             tran_code,
                             adj_code,
                             units,
                             total_cost,
                             total_retail,
                             ref_no_1,
                             ref_no_2,
                             gl_ref_no,
                             old_unit_retail,
                             new_unit_retail,
                             pgm_name,
                             sales_type,
                             vat_rate,
                             av_cost,
                             timestamp,
                             ref_pack_no,
                             total_cost_excl_elc)
      select pli.im_item,
             pli.im_dept,
             pli.im_class,
             pli.im_subclass,
             case when pli.im_pack_ind = 'Y' and pli.pack_comp_ind = 'Y' 
             then
                'N'
             else
                pli.im_pack_ind
             end as im_pack_ind,
             'S',
             pli.loc_value,
             pli.tran_date,
             88 tran_code,    --Vat Out Retail
             null,--is:
             pli.xform_yield_pct * pli.sales_qty_std_uom * pli.pack_qty * pli.tran_type_num  / (1-pli.xform_prod_loss_pct) units,
             null total_cost,
             ROUND(pli.xform_yield_pct * NVL(pli.vi_vat_amt,0) * pli.tran_type_num * pli.pack_price_ratio, pli.st_currency_rtl_dec) as total_retail,
             null,
             null,
             pli.vi_vat_code,
             null,
             null,
             'salesprocess',
             null,
             null,
             null,
             sysdate,
             pli.pack_no,
             null
        from svc_posupld_line_item_gtt pli
       where pli.sales_process_id   = I_sales_process_id
         and pli.chunk_id           = I_chunk_id
         and pli.error_msg          is null
         and pli.deps_purchase_type = 0
         and (pli.im_pack_ind         = 'N'
          or (pli.im_pack_ind         = 'Y'
         and  pli.pack_no             is not null))
         and pli.is_concession_rate is null
         and (pli.is_consignment_rate is null or pli.tran_type_char = 'R')
         and ((pli.im_item_xform_ind = 'N')
              or
              (pli.im_item_xform_ind = 'Y' and pli.xform_sellable_item is not null))
         and NVL(pli.vi_vat_amt, 0) != 0;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_TRAN_DATA_VAT_OUT',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_TRAN_DATA_VAT_OUT;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_TRAN_DATA_EMP_DISC(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_sales_process_id   IN              NUMBER,
                                    I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   insert into tran_data (item,
                          dept,
                          class,
                          subclass,
                          pack_ind,
                          loc_type,
                          location,
                          tran_date,
                          tran_code,
                          adj_code,
                          units,
                          total_cost,
                          total_retail,
                          ref_no_1,
                          ref_no_2,
                          gl_ref_no,
                          old_unit_retail,
                          new_unit_retail,
                          pgm_name,
                          sales_type,
                          vat_rate,
                          av_cost,
                          timestamp,
                          ref_pack_no,
                          total_cost_excl_elc)
   select pli.im_item,
          pli.im_dept,
          pli.im_class,
          pli.im_subclass,
          'N' pack_ind,
          'S',
          pli.loc_value,
          pli.tran_date,
          60 tran_code,    --Employee Discount
          null adj_code,                 
          pli.xform_yield_pct * plid.sales_qty * pli.pack_qty * pli.tran_type_num / (1-pli.xform_prod_loss_pct) units,
          null total_cost,                 
          ROUND(pli.xform_yield_pct * pli.tran_type_num * pli.pack_price_ratio 
                  * decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                           'Y', plid.disc_amt_with_vat, 
                           plid.disc_amt_without_vat)
                  * decode(pli.im_item_xform_ind, 'Y', 1, 
                           decode(pli.subtrans_type_ind, 'A', -1,1)), 
                pli.st_currency_rtl_dec) as total_retail,                           
          null ref_no_1,                 
          null ref_no_2,                 
          null gl_ref_no,                 
          null old_unit_retail,                 
          null new_unit_retail,                 
          'salesprocess' pgm_name,                 
          null sales_type,                 
          null vat_rate,                 
          null av_cost,                 
          sysdate as timestamp,                 
          pack_no ref_pack_no,                 
          null total_cost_excl_elc
     from svc_posupld_line_item_gtt pli,
          svc_posupld_item_disc_gtt plid
    where pli.sales_process_id       = I_sales_process_id
      and pli.chunk_id               = I_chunk_id
      and pli.sales_process_id       = plid.sales_process_id
      and pli.chunk_id               = plid.chunk_id
      and pli.sales_thead_id         = plid.sales_thead_id
      and pli.posupld_line_item_id   = plid.posupld_line_item_id
      and pli.error_msg              is null
      and pli.is_concession_rate     is null
      and pli.im_pack_ind         = 'N'
      and plid.prom_tran_type      = 1005
      and ((pli.im_item_xform_ind = 'N' and pli.posupld_line_item_id = plid.posupld_line_item_id)
           or
           (pli.im_item_xform_ind = 'Y' and pli.xform_sellable_item is not null and
            pli.xform_sellable_item = plid.im_item));
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_TRAN_DATA_EMP_DISC',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_TRAN_DATA_EMP_DISC;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_TRAN_DATA_OFF_RETAIL(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id   IN              NUMBER,
                                      I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   -- insert regular transactional-level items (non-packs) on the POSU file
   insert into tran_data (item,
                          dept,
                          class,
                          subclass,
                          pack_ind,
                          loc_type,
                          location,
                          tran_date,
                          tran_code,
                          adj_code,
                          units,
                          total_cost,
                          total_retail,
                          ref_no_1,
                          ref_no_2,
                          gl_ref_no,
                          old_unit_retail,
                          new_unit_retail,
                          pgm_name,
                          sales_type,
                          vat_rate,
                          av_cost,
                          timestamp,
                          ref_pack_no,
                          total_cost_excl_elc)
   select inner.item,
          inner.dept,
          inner.class,
          inner.subclass,
          inner.pack_ind,
          inner.loc_type,
          inner.location,
          inner.tran_date,
          inner.tran_code,
          inner.adj_code,
          inner.units * inner.tran_sign_mult units,
          inner.total_cost,
          ROUND(inner.old_retail - inner.new_retail, inner.st_currency_rtl_dec)
             * inner.tran_sign_mult as total_retail,
          inner.ref_no_1,
          inner.ref_no_2,
          inner.gl_ref_no,
          ROUND(inner.old_retail / inner.units, inner.st_currency_rtl_dec) old_unit_retail,
          ROUND(inner.new_retail / inner.units, inner.st_currency_rtl_dec) new_unit_retail,
          inner.pgm_name,
          inner.sales_type,
          null as vat_rate,
          inner.av_cost,
          inner.timestamp,
          inner.ref_pack_no,
          inner.total_cost_excl_elc
     from (select pli.im_item as item,
                  pli.im_dept as dept,
                  pli.posupld_line_item_id,
                  pli.im_class as class,
                  pli.im_subclass as subclass,
                  'N' pack_ind,
                  'S' as loc_type,
                  pli.loc_value as location,
                  pli.tran_date as tran_date,
                  15 as tran_code, --Promotional Markdown
                  null as adj_code,
                  pli.xform_yield_pct * pli.sales_qty_std_uom * pli.pack_qty / (1-pli.xform_prod_loss_pct) as units,
                  null total_cost,
                  ----
                  pli.tran_type_num 
                    * decode(pli.im_item_xform_ind, 'Y', 1, 
                             decode(pli.subtrans_type_ind, 'A', -1,1)) as tran_sign_mult,
                  case when pli.catchweight_ind = 'Y' and
                             pli.im_standard_uom = 'EA' then
                     pli.xform_yield_pct * pli.actualweight_qty / pli.ils_average_weight   
                       * decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                                'Y', pli.old_retail_with_vat,
                                pli.old_retail_without_vat) 
                  else
                     pli.xform_yield_pct * pli.pack_price_ratio * pli.sales_qty_std_uom 
                       * decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                                'Y', nvl(pack.old_retail_with_vat, pli.old_retail_with_vat),
                                nvl(pack.old_retail_without_vat, pli.old_retail_without_vat)) 
                  end old_retail,
                  pli.xform_yield_pct * pli.pack_price_ratio
                    * decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                             'Y', pli.sales_value_with_vat + nvl(ed.total_disc_amt_with_vat,0),
                             pli.sales_value_without_vat + nvl(ed.total_disc_amt_without_vat,0)) new_retail,
                  ----
                  null as ref_no_1,
                  null as ref_no_2,
                  null as gl_ref_no,
                  ----
                  'salesprocess' as pgm_name,
                  null as sales_type,
                  nvl(pli.vi_vat_rate,0) as vat_rate,
                  null as av_cost,
                  sysdate as timestamp,
                  pli.pack_no as ref_pack_no,
                  null as total_cost_excl_elc,
                  ----
                  pli.st_currency_rtl_dec
             from svc_posupld_line_item_gtt pli,
                  svc_posupld_line_item_gtt pack,
                  (select e.sales_process_id,
                          e.chunk_id,
                          e.sales_thead_id,
                          e.posupld_line_item_id,
                          e.im_item,
                          sum(e.disc_amt_with_vat) total_disc_amt_with_vat,
                          sum(e.disc_amt_without_vat) total_disc_amt_without_vat                          
                     from svc_posupld_item_disc_gtt e
                    where e.sales_process_id = I_sales_process_id
                      and e.chunk_id = I_chunk_id
                 group by e.posupld_line_item_id,
                          e.sales_process_id,
                          e.chunk_id,
                          e.sales_thead_id,
                          e.im_item) ed
            where pli.sales_process_id    = I_sales_process_id
              and pli.chunk_id            = I_chunk_id
              and pli.error_msg           is null
              and pli.im_pack_ind        = 'N'
              and pli.is_consignment_rate is null
              and pli.is_concession_rate  is null
              and pli.sales_process_id    = ed.sales_process_id(+)
              and pli.chunk_id            = ed.chunk_id(+)
              and pli.sales_thead_id      = ed.sales_thead_id(+)
              and nvl(pli.xform_sellable_item, pli.im_item)   = ed.im_item(+)
              and (pli.im_item_xform_ind = 'N'
                   or (pli.im_item_xform_ind = 'Y' and pli.xform_sellable_item is not null))
              and pli.sales_process_id    = pack.sales_process_id(+)
              and pli.chunk_id            = pack.chunk_id(+)
              and pli.sales_thead_id      = pack.sales_thead_id(+)
              and pli.pack_no             = pack.im_item(+)) inner
   where abs(inner.old_retail - inner.new_retail) > power(10, -1 * inner.st_currency_rtl_dec);

   ---
   -- insert component items of packs on the POSU file
/*  -- prior insert is handling both pack and non-pack so this shouldn't be needed
   insert into tran_data (item,
                          dept,
                          class,
                          subclass,
                          pack_ind,
                          loc_type,
                          location,
                          tran_date,
                          tran_code,
                          adj_code,
                          units,
                          total_cost,
                          total_retail,
                          ref_no_1,
                          ref_no_2,
                          gl_ref_no,
                          old_unit_retail,
                          new_unit_retail,
                          pgm_name,
                          sales_type,
                          vat_rate,
                          av_cost,
                          timestamp,
                          ref_pack_no,
                          total_cost_excl_elc)
   select inner.item,
          inner.dept,
          inner.class,
          inner.subclass,
          inner.pack_ind,
          inner.loc_type,
          inner.location,
          inner.tran_date,
          inner.tran_code,
          inner.adj_code,
          inner.units,
          inner.total_cost,
          ROUND(inner.total_retail, inner.st_currency_rtl_dec) as total_retail,
          inner.ref_no_1,
          inner.ref_no_2,
          inner.gl_ref_no,
          decode(nvl(LP_system_options_rec.stkldgr_vat_incl_retl_ind,'N'),
                  'Y', inner.old_unit_retail,
                       inner.old_unit_retail - inner.vat_amt) as old_unit_retail,
          decode(nvl(LP_system_options_rec.stkldgr_vat_incl_retl_ind,'N'),
                  'Y', inner.new_unit_retail,
                       inner.new_unit_retail - inner.vat_amt) as new_unit_retail,
          inner.pgm_name,
          inner.sales_type,
          null as vat_rate,
          inner.av_cost,
          inner.timestamp,
          inner.ref_pack_no,
          inner.total_cost_excl_elc
     from (select distinct pli.im_item as item,
                  pli.im_dept as dept,
                  pli.im_class as class,
                  pli.im_subclass as subclass,
                  case when pli.im_pack_ind = 'Y' and pli.pack_comp_ind = 'Y' then
                     'N'
                  else
                     pli.im_pack_ind
                  end as pack_ind,
                  'S' as loc_type,
                  pli.loc_value as location,
                  pli.gtax_value,
                  pli.tran_date as tran_date,
                  15 as tran_code, --Promotional Markdown
                  null as adj_code,
                  pli.xform_yield_pct * pli.sales_qty_std_uom * pli.pack_qty * pli.tran_type_num  / (1-pli.xform_prod_loss_pct) as units,
                  null total_cost,
                  ----                  
                  pack.xform_yield_pct * ((pack.old_retail_without_vat * pack.sales_qty_std_uom) -
                  (pack.sales_value_without_vat + nvl(ed.total_disc_value,0))) *
                   pack.pack_qty * pack.tran_type_num   * pli.pack_price_ratio * decode(pli.im_item_xform_ind, 'Y',
                                                                              1, decode(pli.subtrans_type_ind, 'A',
                                                                                         -1,1)) total_retail,
                  ----
                  null as ref_no_1,
                  null as ref_no_2,
                  null as gl_ref_no,
                  ----
                  case when pli.im_item_xform_ind = 'Y' then
                     0
                  else
                     pli.xform_yield_pct * pack.old_retail_without_vat *
                        (pli.pack_price_ratio / pli.pack_qty)
                  end old_unit_retail,
                  ----
                  pli.xform_yield_pct * ((pli.sales_value_without_vat + nvl(ed.total_disc_value,0)) /
                     pli.sales_qty_std_uom) * (pli.pack_price_ratio / pli.pack_qty) as new_unit_retail,
                  ----
                  'salesprocess' as pgm_name,
                  null as sales_type,
                  nvl(pli.vi_vat_rate,0) as vat_rate,
                  null as av_cost,
                  sysdate as timestamp,
                  pli.pack_no as ref_pack_no,
                  null as total_cost_excl_elc,
                  ----
                  pli.st_currency_rtl_dec,
                  (nvl(pli.vi_vat_amt,0) * pli.pack_price_ratio) / (pli.pack_qty * pli.sales_qty) as vat_amt
             from svc_posupld_line_item_gtt pli,
                  svc_posupld_line_item_gtt pack,
                  (select e.sales_process_id,
                          e.chunk_id,
                          e.sales_thead_id,
                          e.posupld_line_item_id,
                          e.im_item,
                          sum(e.discount_value) total_disc_value
                     from svc_posupld_item_disc_gtt e
                    where e.sales_process_id = I_sales_process_id
                      and e.chunk_id = I_chunk_id
                 group by e.posupld_line_item_id,
                          e.sales_process_id,
                          e.chunk_id,
                          e.sales_thead_id,
                          e.im_item) ed
            where pli.sales_process_id    = I_sales_process_id
              and pli.chunk_id            = I_chunk_id
              and pli.error_msg           is null
              and pli.im_pack_ind         = 'N'
              and pli.pack_comp_ind       = 'Y'
              and (pli.im_item_xform_ind = 'N' or
                  (pli.im_item_xform_ind = 'Y' and pli.xform_sellable_item is not null))
              and pli.posupld_line_item_id  = ed.posupld_line_item_id(+)
              and nvl(pli.xform_sellable_item, pli.im_item)   = ed.im_item(+)
              and pli.is_consignment_rate is null
              and pli.sales_process_id    = pack.sales_process_id
              and pli.chunk_id            = pack.chunk_id
              and pli.pack_no             = pack.im_item
              and pli.sales_process_id    = ed.sales_process_id(+)
              and pli.chunk_id            = ed.chunk_id(+)
              and pli.sales_thead_id      = ed.sales_thead_id(+)
              ) inner
   where abs(inner.old_unit_retail - inner.new_unit_retail) > power(10, -1 * inner.st_currency_rtl_dec);
*/
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_TRAN_DATA_OFF_RETAIL',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_TRAN_DATA_OFF_RETAIL;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_TRAN_DATA_PACK_UP_DOWN(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_sales_process_id   IN              NUMBER,
                                        I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   insert into tran_data (item,
                          dept,
                          class,
                          subclass,
                          pack_ind,
                          loc_type,
                          location,
                          tran_date,
                          tran_code,
                          adj_code,
                          units,
                          total_cost,
                          total_retail,
                          ref_no_1,
                          ref_no_2,
                          gl_ref_no,
                          old_unit_retail,
                          new_unit_retail,
                          pgm_name,
                          sales_type,
                          vat_rate,
                          av_cost,
                          timestamp,
                          ref_pack_no,
                          total_cost_excl_elc)
   select im_item,
          im_dept,
          im_class,
          im_subclass,
          'N' pack_ind,
          'S',
          loc_value,
          tran_date,
          case when comp_unit_retail * pack_qty - pack_unit_retail * pack_price_ratio > 0 then
             13
          else
             11
          end tran_code,
          --
          NULL,
          sales_qty_std_uom * pack_qty * tran_type_num  as units,
          NULL as total_cost,
          ROUND(abs(( comp_unit_retail * pack_qty - pack_unit_retail * pack_price_ratio) * sales_qty_std_uom) * tran_type_num, 
                st_currency_rtl_dec) as total_retail,
          NULL,
          NULL,
          NULL,
          ROUND(comp_unit_retail, st_currency_rtl_dec) old_unit_retail,
          ROUND(pack_unit_retail * pack_price_ratio / pack_qty, st_currency_rtl_dec) new_unit_retail,
          'salesprocess',
          NULL,
          NULL,
          NULL,
          SYSDATE,
          pack_no,
          NULL
     from (select it.*,
                  decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                         'Y', it.ph_reg_retail,
                         it.ph_reg_retail/(1+NVL(it.vi_vat_rate,0)/100)) comp_unit_retail,
                  decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                         'Y', p.ph_reg_retail,
                         p.ph_reg_retail/(1+NVL(p.vi_vat_rate,0)/100)) pack_unit_retail
             from svc_posupld_line_item_gtt it,
                  svc_posupld_line_item_gtt p
            where it.sales_process_id = I_sales_process_id
              and it.chunk_id         = I_chunk_id
              and p.sales_process_id  = it.sales_process_id
              and p.chunk_id          = it.chunk_id
              and p.sales_thead_id    = it.sales_thead_id
              and it.error_msg        is NULL
              and p.error_msg         is NULL
              and it.pack_no = p.im_item) inner
    where abs((comp_unit_retail * pack_qty - pack_unit_retail * pack_price_ratio) * sales_qty_std_uom) > power(10, -1 * st_currency_rtl_dec);
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_TRAN_DATA_PACK_UP_DOWN',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_TRAN_DATA_PACK_UP_DOWN;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_TRAN_DATA_PROMO_MKDN(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id   IN              NUMBER,
                                      I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_complex_promo_allowed_ind   RPM_SYSTEM_OPTIONS.COMPLEX_PROMO_ALLOWED_IND%TYPE;
   L_non_emp_disc_ind            VARCHAR2(1) := NULL;

  cursor C_RPM_SO is
      select nvl(complex_promo_allowed_ind,0)
        from rpm_system_options; 

   cursor C_CHK_MULT_PROM is
      select 'x'
        from svc_posupld_item_disc_gtt plid,
             svc_posupld_line_item_gtt pli
       where plid.total_disc_amt_with_vat * pli.pack_price_ratio != 0
         and plid.posupld_line_item_id = pli.posupld_line_item_id
         and plid.sales_process_id = I_sales_process_id
         and plid.chunk_id = I_chunk_id
         and plid.sales_process_id = pli.sales_process_id
         and plid.chunk_id = pli.chunk_id
         and rownum = 1;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_RPM_SO;
   fetch C_RPM_SO into L_complex_promo_allowed_ind;
   close C_RPM_SO;
   ---
   open C_CHK_MULT_PROM;
   fetch C_CHK_MULT_PROM into L_non_emp_disc_ind;
   close C_CHK_MULT_PROM;
   ---
   if L_non_emp_disc_ind is not NULL then
      insert into tran_data (item,
                             dept,
                             class,
                             subclass,
                             pack_ind,
                             loc_type,
                             location,
                             tran_date,
                             tran_code,
                             adj_code,
                             units,
                             total_cost,
                             total_retail,
                             ref_no_1,
                             ref_no_2,
                             gl_ref_no,
                             old_unit_retail,
                             new_unit_retail,
                             pgm_name,
                             sales_type,
                             vat_rate,
                             av_cost,
                             timestamp,
                             ref_pack_no,
                             total_cost_excl_elc)
                      select pli.im_item,
                             pli.im_dept,
                             pli.im_class,
                             pli.im_subclass,
                             'N' pack_ind,
                             'S' loc_type,
                             pli.loc_value,
                             pli.tran_date,
                             15 tran_code, -- promotional markdown
                             NULL, -- adj_code
                             pli.xform_yield_pct * plid.sales_qty * pli.pack_qty * pli.tran_type_num / (1-pli.xform_prod_loss_pct) units,
                             NULL, -- total cost
                             ROUND(pli.xform_yield_pct * pli.tran_type_num * pli.pack_price_ratio 
                                         * decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                                                  'Y', plid.disc_amt_with_vat, 
                                                  plid.disc_amt_without_vat)
                                         * decode(pli.im_item_xform_ind, 'Y', 1, 
                                                  decode(pli.subtrans_type_ind, 'A', -1,1)), 
                                       pli.st_currency_rtl_dec) as total_retail,                           
                             null,
                             plid.prom_no,
                             NULL, -- gl_ref_no
                             --
                             NULL as old_unit_retail, -- Old / New unit_retail not populated for TDETL discounts
                             --
                             NULL as new_unit_retail, -- Old / New unit_retail not populated for TDETL discounts
                             'salesprocess',
                             --
                             decode(plid.prom_tran_type,
                                    '9999', 'P',
                                    '2000', 'P',
                                    'R') prom_tran_type,
                             NULL, -- vat_rate
                             NULL, --  av_cost
                             sysdate,
                             pli.pack_no, -- ref_pack_no
                             NULL -- total_cost_excl_elc
                        from svc_posupld_line_item_gtt pli,
                             svc_posupld_item_disc_gtt plid
                       where pli.sales_process_id           = I_sales_process_id
                         and pli.chunk_id                   = I_chunk_id
                         and pli.sales_process_id           = plid.sales_process_id
                         and pli.chunk_id                   = plid.chunk_id
                         and pli.sales_thead_id             = plid.sales_thead_id
                         and pli.error_msg                  IS NULL
                         and pli.is_concession_rate         IS NULL
                         and plid.prom_tran_type     != '1005'
                         and pli.im_pack_ind         = 'N'
                         and pli.is_consignment_rate  is null
                         and ((pli.im_item_xform_ind = 'N' and pli.posupld_line_item_id = plid.posupld_line_item_id)
                              or
                              (pli.im_item_xform_ind = 'Y' and pli.xform_sellable_item is not null and
                               pli.xform_sellable_item = plid.im_item));
   end if;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_TRAN_DATA_PROMO_MKDN',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_TRAN_DATA_PROMO_MKDN;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_TRAN_DATA_WEIGHT_VAR(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_sales_process_id   IN              NUMBER,
                                      I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   insert into tran_data (item,
                          dept,
                          class,
                          subclass,
                          pack_ind,
                          loc_type,
                          location,
                          tran_date,
                          tran_code,
                          adj_code,
                          units,
                          total_cost,
                          total_retail,
                          ref_no_1,
                          ref_no_2,
                          gl_ref_no,
                          old_unit_retail,
                          new_unit_retail,
                          pgm_name,
                          sales_type,
                          vat_rate,
                          av_cost,
                          timestamp,
                          ref_pack_no,
                          total_cost_excl_elc)
   select pli.im_item,
          pli.im_dept,
          pli.im_class,
          pli.im_subclass,
          case when pli.im_pack_ind = 'Y' and pli.pack_comp_ind = 'Y' then
             'N'
          else
             pli.im_pack_ind
          end as im_pack_ind,
          'S',
          pli.loc_value,
          pli.tran_date,
          10 tran_code, --Weight Variance
          null,
          pli.tran_type_num * pli.sales_qty_std_uom * pli.pack_qty  as units,
          null as total_cost,
          ROUND(decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                       'Y', ((pli.old_retail_with_vat * pli.sales_qty_std_uom) -
                             (pli.actualweight_qty * pli.old_retail_with_vat / pli.ils_average_weight)) *
                            pli.pack_price_ratio,
                       ((pli.old_retail_without_vat * pli.sales_qty_std_uom) -
                        (pli.actualweight_qty * pli.old_retail_without_vat / pli.ils_average_weight)) *
                       pli.pack_price_ratio) * pli.tran_type_num, pli.st_currency_rtl_dec) as total_retail,
          null,
          null,
          null,
          decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                 'Y', pli.old_retail_with_vat * (pli.pack_price_ratio / pli.pack_qty),
                      pli.old_retail_without_vat * (pli.pack_price_ratio / pli.pack_qty)) as old_unit_retail,

          decode(LP_system_options_rec.stkldgr_vat_incl_retl_ind,
                 'Y', (pli.actualweight_qty * (pli.old_retail_with_vat) / (pli.ils_average_weight * pli.sales_qty_std_uom)) *
                        (pli.pack_price_ratio / pli.pack_qty),
                      (pli.actualweight_qty * (pli.old_retail_without_vat) / (pli.ils_average_weight * pli.sales_qty_std_uom)) *
                        (pli.pack_price_ratio / pli.pack_qty)) as new_unit_retail,
          'salesprocess',
          null,
          null,
          null,
          sysdate,
          pli.pack_no,
          null
     from svc_posupld_line_item_gtt pli
    where pli.sales_process_id     = I_sales_process_id
      and pli.chunk_id             = I_chunk_id
      and pli.error_msg            is null
      and pli.catchweight_ind    = 'Y'
      and pli.im_standard_uom      = 'EA'
      and pli.is_consignment_rate  is null
      and abs((pli.old_retail_with_vat * pli.sales_qty_std_uom)-(pli.actualweight_qty * pli.old_retail_with_vat / pli.ils_average_weight)) >
          power(10, -1 * pli.st_currency_rtl_dec)
      and (pli.old_retail_with_vat * pli.sales_qty_std_uom)-(pli.actualweight_qty * pli.old_retail_with_vat / pli.ils_average_weight) *
          pli.pack_price_ratio != 0;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_TRAN_DATA_WEIGHT_VAR',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_TRAN_DATA_WEIGHT_VAR;
-------------------------------------------------------------------------------------------
FUNCTION PERSIST_CO_RTN_BOOK_TSF(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_sales_process_id   IN              NUMBER,
                                 I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_obj_book_tsf_co_rtn_tbl   "OBJ_BOOK_TSF_CO_RTN_TBL" := NULL;
   L_obj_book_tsf_co_rtn_rec   "OBJ_BOOK_TSF_CO_RTN_REC" := NULL;

   cursor C_LOCK_ITEM_LOC_SOH is
      select 'x'
        from item_loc_soh ils
       where exists (select 'x'
                       from TABLE(CAST(L_obj_book_tsf_co_rtn_tbl AS "OBJ_BOOK_TSF_CO_RTN_TBL")) qry_tbl,
                            inv_status_codes i
                      where qry_tbl.I_item = ils.item
                        and qry_tbl.I_from_loc = ils.loc
                        and qry_tbl.I_return_without_inventory_ind = 'N'
                        and qry_tbl.I_return_disposition = i.inv_status_code)
         for update nowait;

   cursor C_GET_RET_INFO is
      select pli.loc_value,
             'S' from_loc_type,
             pli.return_wh,
             'W' to_loc_type,
             pli.im_item,
             sum(pli.xform_yield_pct *
                (pli.total_wastage_qty * pli.pack_qty ) /
                (1-pli.xform_prod_loss_pct)) return_qty,
             pli.return_disposition,
             pli.no_inv_ret_ind,
             pli.tran_date
        from svc_posupld_line_item_gtt pli,
             store s
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id         = I_chunk_id
         and pli.loc_value        = s.store
         and s.stockholding_ind   = 'N'
         and s.store_type         = 'C'
         and pli.tran_type_char   = 'R'
         and pli.resa_sales_type  = 'E'
         and pli.sales_sign_char  = 'P'
         and pli.return_wh is NOT NULL
         and ((pli.im_pack_ind = 'N' and pli.pack_comp_ind = 'Y')
            or (pli.im_pack_ind = 'N' and pli.pack_comp_ind = 'N'))
         and pli.error_msg is NULL
       group by pli.loc_value,
                pli.return_wh,
                pli.im_item,
                pli.return_disposition,
                pli.no_inv_ret_ind,
                pli.tran_date;

   TYPE RET_INFO_TBL is TABLE OF C_GET_RET_INFO%ROWTYPE
      INDEX BY BINARY_INTEGER;

   L_ret_info_tbl   RET_INFO_TBL;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   --Fetch records from the SVC_POSUPLD_LINE_ITEM_GTT global temp table.
   open C_GET_RET_INFO;
   fetch C_GET_RET_INFO bulk collect INTO L_ret_info_tbl;
   close C_GET_RET_INFO;

   if L_ret_info_tbl.FIRST is NULL then
      return TRUE;
   end if;

   --Initialize the L_obj_book_tsf_co_rtn_rec composite.
   L_obj_book_tsf_co_rtn_rec := "OBJ_BOOK_TSF_CO_RTN_REC"(NULL,  --I_from_loc,
                                                          NULL,  --I_from_loc_type,
                                                          NULL,  --I_to_loc,
                                                          NULL,  --I_to_loc_type,
                                                          NULL,  --I_item,
                                                          NULL,  --I_return_qty,
                                                          NULL,  --I_return_disposition,
                                                          NULL,  --I_return_without_inventory_ind,
                                                          NULL,  --I_tran_date
                                                          NULL,  --tsf_no,
                                                          NULL,  --dept,
                                                          NULL,  --class,
                                                          NULL,  --subclass,
                                                          NULL,  --pack_ind,
                                                          NULL,  --supp_pack_size,
                                                          NULL,  --inv_status,
                                                          NULL); --virtual_wh

   L_obj_book_tsf_co_rtn_tbl := "OBJ_BOOK_TSF_CO_RTN_TBL"();

   FOR i IN 1..L_ret_info_tbl.COUNT LOOP
      L_obj_book_tsf_co_rtn_rec.I_from_loc                     := L_ret_info_tbl(i).loc_value;
      L_obj_book_tsf_co_rtn_rec.I_from_loc_type                := L_ret_info_tbl(i).from_loc_type;
      L_obj_book_tsf_co_rtn_rec.I_to_loc                       := L_ret_info_tbl(i).return_wh;
      L_obj_book_tsf_co_rtn_rec.I_to_loc_type                  := L_ret_info_tbl(i).to_loc_type;
      L_obj_book_tsf_co_rtn_rec.I_item                         := L_ret_info_tbl(i).im_item;
      L_obj_book_tsf_co_rtn_rec.I_return_qty                   := L_ret_info_tbl(i).return_qty;
      L_obj_book_tsf_co_rtn_rec.I_return_disposition           := L_ret_info_tbl(i).return_disposition;
      L_obj_book_tsf_co_rtn_rec.I_return_without_inventory_ind := L_ret_info_tbl(i).no_inv_ret_ind;
      L_obj_book_tsf_co_rtn_rec.I_tran_date                    := L_ret_info_tbl(i).tran_date;
      L_obj_book_tsf_co_rtn_rec.tsf_no                         := NULL;
      L_obj_book_tsf_co_rtn_rec.dept                           := NULL;
      L_obj_book_tsf_co_rtn_rec.class                          := NULL;
      L_obj_book_tsf_co_rtn_rec.subclass                       := NULL;
      L_obj_book_tsf_co_rtn_rec.pack_ind                       := NULL;
      L_obj_book_tsf_co_rtn_rec.supp_pack_size                 := NULL;
      L_obj_book_tsf_co_rtn_rec.inv_status                     := NULL;
      L_obj_book_tsf_co_rtn_rec.virtual_wh                     := NULL;

      L_obj_book_tsf_co_rtn_tbl.EXTEND();
      L_obj_book_tsf_co_rtn_tbl(L_obj_book_tsf_co_rtn_tbl.COUNT) := L_obj_book_tsf_co_rtn_rec;
   END LOOP;

   open C_LOCK_ITEM_LOC_SOH;
   close C_LOCK_ITEM_LOC_SOH;

   -- Increment non_sellable_qty for virtual store if return_disposition exists in inv_status_types.
   merge into item_loc_soh target
   using (select I_item,
                 I_from_loc,
                 I_return_qty
            from TABLE(CAST(L_obj_book_tsf_co_rtn_tbl AS "OBJ_BOOK_TSF_CO_RTN_TBL")) qry_tbl,
                 inv_status_codes i
           where qry_tbl.I_return_without_inventory_ind = 'N'
             and qry_tbl.I_return_disposition = i.inv_status_code
             and i.inv_status is NOT NULL) use_this
      on (    target.item = use_this.I_item
          and target.loc = use_this.I_from_loc)
    when matched then
       update set non_sellable_qty = use_this.I_return_qty,
                  last_update_datetime = SYSDATE,
                  last_update_id = LP_user;

   -- Insert into inv_status_qty if return_disposition exists in inv_status_types.
   insert into inv_status_qty(item,
                              inv_status,
                              loc_type,
                              location,
                              qty,
                              create_datetime,
                              last_update_datetime,
                              last_update_id)
                       select I_item,
                              i.inv_status,
                              I_from_loc_type,
                              I_from_loc,
                              I_return_qty,
                              SYSDATE,
                              SYSDATE,
                              LP_user
                         from TABLE(CAST(L_obj_book_tsf_co_rtn_tbl AS "OBJ_BOOK_TSF_CO_RTN_TBL")) qry_tbl,
                              inv_status_codes i
                        where qry_tbl.I_return_without_inventory_ind = 'N'
                          and qry_tbl.I_return_disposition = i.inv_status_code
                          and i.inv_status is NOT NULL;

   -- Call the TSF_BT_SQL.CREATE_BOOK_TSF_CO_RTN function to generate the Book Transfer for this return transaction.
   if TSF_BT_SQL.CREATE_BOOK_TSF_CO_RTN(O_error_message,
                                        L_obj_book_tsf_co_rtn_tbl) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_CO_RTN_BOOK_TSF',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_CO_RTN_BOOK_TSF;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_CUST_ORDER_RESV(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_sales_process_id   IN              NUMBER,
                                 I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

   L_cust_ord_sale_tbl   "CUST_ORD_SALE_TBL" := NULL;
   L_cust_ord_sale_rec   "CUST_ORD_SALE_REC" := NULL;

   cursor C_GET_CO_RESV_INFO is
      select pli.im_item,
             pli.loc_value,
             sum(pli.xform_yield_pct *
                (pli.tran_type_num * pli.total_wastage_qty * pli.pack_qty ) /
                (1-pli.xform_prod_loss_pct)) qty,
             pli.selling_uom
        from svc_posupld_line_item_gtt pli,
             store s
       where pli.sales_process_id = I_sales_process_id
         and pli.chunk_id         = I_chunk_id
         and pli.resa_sales_type  = 'E'
         and pli.tran_type_char   = 'S'
         and pli.loc_value        = s.store
         and s.stockholding_ind   = 'Y'
         and ((pli.im_pack_ind = 'N' and pli.pack_comp_ind = 'Y')
            or (pli.im_pack_ind = 'N' and pli.pack_comp_ind = 'N'))
         and pli.error_msg is NULL
        group by pli.im_item,
                 pli.loc_value,
                 pli.selling_uom;

   TYPE CO_RESV_INFO_TBL is TABLE OF C_GET_CO_RESV_INFO%ROWTYPE
      INDEX BY BINARY_INTEGER;

   L_co_resv_info_tbl   CO_RESV_INFO_TBL;

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_CO_RESV_INFO;
   fetch C_GET_CO_RESV_INFO bulk collect INTO L_co_resv_info_tbl;
   close C_GET_CO_RESV_INFO;

   if L_co_resv_info_tbl.FIRST is NULL then
      return TRUE;
   end if;

   --Initialize the L_obj_book_tsf_co_rtn_rec composite.
   L_cust_ord_sale_rec := "CUST_ORD_SALE_REC"(NULL,  --item,
                                              NULL,  --location,
                                              NULL,  --qty,
                                              NULL); --qty_uom

   L_cust_ord_sale_tbl := "CUST_ORD_SALE_TBL"();
   FOR i in 1..L_co_resv_info_tbl.COUNT LOOP
      L_cust_ord_sale_rec.item           := L_co_resv_info_tbl(i).im_item;
      L_cust_ord_sale_rec.location       := L_co_resv_info_tbl(i).loc_value;
      L_cust_ord_sale_rec.qty            := L_co_resv_info_tbl(i).qty;
      L_cust_ord_sale_rec.qty_uom        := L_co_resv_info_tbl(i).selling_uom;

      L_cust_ord_sale_tbl.EXTEND();
      L_cust_ord_sale_tbl(L_cust_ord_sale_tbl.COUNT) := L_cust_ord_sale_rec;

   END LOOP;

   -- Call the CUSTOMER_RESERVE_SQL.PROCESS_CO_SALES function to release the customer reserve inventory for this sale transaction.
   if CUSTOMER_RESERVE_SQL.PROCESS_CO_SALES(O_error_message,
                                            L_cust_ord_sale_tbl) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST_CUST_ORDER_RESV',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_CUST_ORDER_RESV;
----------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message      IN OUT NOCOPY   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_sales_process_id   IN              NUMBER,
                 I_chunk_id           IN              NUMBER)
RETURN BOOLEAN IS

BEGIN
   ---
   if I_sales_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_sales_process_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_chunk_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if LOCK_TABLES(O_error_message,
                  I_sales_process_id,
                  I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_rec) = FALSE then
         return FALSE;
   end if;

   if PERSIST_ITEM_LOC_SOH(O_error_message,
                           I_sales_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_DAILY_SALES_DISCOUNT(O_error_message,
                                   I_sales_process_id,
                                   I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_SNAPSHOT(O_error_message,
                       I_sales_process_id,
                       I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_DEAL_ACTUALS_IL(O_error_message,
                              I_sales_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if LP_system_options_rec.default_tax_type = 'SVAT' then
      if PERSIST_VAT_HISTORY(O_error_message,
                             I_sales_process_id,
                             I_chunk_id) = FALSE then
         return FALSE;
      end if;
   end if;

   if PERSIST_EDI_DAILY_SALES(O_error_message,
                              I_sales_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_ITEM_LOC_HIST(O_error_message,
                            I_sales_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_ITEM_LOC_HIST_MTH(O_error_message,
                                I_sales_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_CONCESSION_DATA(O_error_message,
                              I_sales_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_CONSIGNMENT_DATA(O_error_message,
                               I_sales_process_id,
                               I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_TRAN_DATA(O_error_message,
                        I_sales_process_id,
                        I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_CO_RTN_BOOK_TSF(O_error_message,
                              I_sales_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_CUST_ORDER_RESV(O_error_message,
                              I_sales_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CORESVC_SALES_UPLOAD_PRST_SQL.PERSIST',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST;
----------------------------------------------------------------------------------------
END CORESVC_SALES_UPLOAD_PRST_SQL;
/
