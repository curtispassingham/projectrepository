
#include "saimptlog.h"

/*-------------------------------------------------------------------------
Final() closes all files used by the application and performs any other
special cleanup that may be needed.
-------------------------------------------------------------------------*/
int Final( void )
{
   char *function = "Final";
   int rv, i;

   rv = TRUE;

   for (i = LEN_LOC - 1; (i > 0) && (Location[i] == ' '); i--)
      Location[i] = '\0';

    if (gi_rej_record == TRUE)
      WrBadTranError( RECORDS_REJECTED, "", -1, -1, SART_THEAD, "", 0 );

   if (gi_error_flag != 2)
   {
      if ( CreateTermRecords())
      {
         if ( ! WrOutputData())
         {
            ExitCode = EXIT_FAILURE;
            rv = FALSE;
         }
      }
      else
      {
         ExitCode = EXIT_FAILURE;
         rv = FALSE;
      }
   }

   free_rounding_rule();

   /* call Final function for input format */
   FinalInputData();
   if (ExitCode == EXIT_SUCCESS && !gi_error_flag)
   {
      if (strcmp( SysOpt.s_check_dup_miss_tran, YSNO_Y) == 0)
      {
         /* output missing tran file... */
         VERBOSE( "INFO: Outputing missing tran data\n" );
         if(strcmp(Rtlog_Orig_Sys, "OMS") != 0)
            tdup_misstran( Location, BusinessDate );

         /* save duplicate transaction data */
         VERBOSE( "INFO: Outputing tdup data\n" );
         tdup_savedata( Location , Rtlog_Orig_Sys);
      }

      if (!FinalOutputData())
         rv = FALSE;

      if ( closeSAVoucher() != 0 )
         rv = FALSE;
   }
   else
   {
      InitOutputClean();
      DeleteSAVoucher();
   }

   if(rv == FALSE && gi_error_flag == 0)
      gi_error_flag = 3;

   if (retek_close() != 0)
      rv = FALSE;

   if (retek_refresh_thread() != 0) 
      rv = FALSE;
   
   VERBOSE( "INFO: Final finished\n" );
   return( rv );
}
