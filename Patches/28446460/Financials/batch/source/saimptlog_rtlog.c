
/*=========================================================================

   The files rtlog.h and rtlog.pc are custom input files written
   specifically to handle Retek's RTLOG input file format. Customers
   have two choices: They can reformat their POS data to RTLOG format,
   or they can rewrite this module to handle their standard POS format
   as is. Note that the second choice (custom rewrite) is higher
   performance because one I/O cycle is avoided.

=========================================================================*/

#include "saimptlog.h"
#include "saimptlog_tdup.h"
#include "saoranum.h"
#include "saimptlog_ccval.h"


/* behavior defines */
#define BAD_TRAN_SEPARATOR_IN_FILE /* blank line between bad transactions */
#define CLEAR_BUFFER_BEFORE_READ   /* clear input buffer each record */
#define RAW_RECORD_CHECK           /* check raw data for embedded \n, \0, etc */

/* grow transaction arrays by these sizes */
#define ALLOCTRANRECS   1000  /* this cannot be less than 2 */
#define MAX_LOCK_TRIES  300  /* max number of attempts to obtain a lock */
#define DUP_TCUST_TRAN   1  /* used to check if there are two TCUST on a THEAD-TTAIL */
int numCustPerTran = 0;  /*Intitializing the variale to 0 */
static long UsedTranRecs  = 0;

/*-------------------------------------------------------------------------
local data structures
-------------------------------------------------------------------------*/
typedef struct {        /* RECINFO structure */
   char *frecdesc;      /* frecdesc identifier, such as "FHEAD" */
   char rectype;        /* record type token */
} RECINFO;

/*-------------------------------------------------------------------------
local function prototypes
-------------------------------------------------------------------------*/
static int getRTLRec( char *p, FILE *f );
static void resetTran( int copyrec );
static RECINFO *rtFind( char *recid );
static int procRTLFHead( void *rp );
static int procRTLFTail( void *rp );
static int procRTLTHead( void *rp );
static int procRTLTTail( void *rp );
static int procRTLTCust( void *rp );
static int procRTLCAtt( void *rp );
static int procRTLTItem( void *rp,
                         double *od_uom_conv_ratio,
                         pt_item_uom *uom );
static int procRTLIDisc( void *rp,
                         double *id_uom_conv_ratio,
                         pt_disc_uom *uom );
static int procRTLIGtax( void *rp,
                         pt_igtax_tia *tia );
static int procRTLTTax( void *rp );
static int procRTLTPymt( void *rp );
static int procRTLTTend( void *rp );
static int chkTranFormat( void );
static int chkTranTailCount( RTL_TTAIL *p );
#ifdef RAW_RECORD_CHECK
static int rrchk( void *rp, size_t reclen );
#endif
static int reformatTran( pt_item_uom *uom, pt_disc_uom *disc_uom, pt_igtax_tia *igtax_tia, void *rp );
static void WrBadTran( int stop );
static int decdate( char *datestr, int decdays );
static void num2str( char *str, int lstr, char *num, int lnum, const char *sign);


/*-------------------------------------------------------------------------
local global variables
-------------------------------------------------------------------------*/
static FILE *RTLinFile = NULL;   /* RTLOG input file */
static FILE *BadTranFile = NULL; /* bad transaction output file */

static int Recidx;               /* index into Rectype[] and Recbuf[] arrays */
static char LastRectype;         /* type of last record read */

static RTL_FHEAD RTLFHead;       /* place to keep RTL_FHEAD */
static RTL_THEAD *LastTHead;     /* pointer to last RTL_THEAD */
static RTL_TITEM *CurItem;       /* pointer to current RTL_TITEM */

static long int FileRecInCount;  /* file record in count */
static int TranRecCount;         /* transaction record count */

static int inFileBlock;          /* indicates FHEAD-FTAIL block */
static int inTranBlock;          /* indicates THEAD-TTAIL block */
static int inTranCustBlock;      /* indicates TCUST block */
static int inTranItemBlock;      /* indicates TITEM block */

static int isTranDuplicate = 1;  /* indicates the transaction is duplicate */

static int CurTrat;              /* current transaction type */
static int CurTras;              /* current sub transaction type */

static char *InFile;             /* current input filename */

static char SaleTotal[NULL_SIGNED_AMT];
static char TenderTotal[NULL_SIGNED_AMT];
static char ps_rtl_catchweight_dec[NULL_CURRENCY_RTL_DEC] ="4";
static char ps_currency_rtl_dec[NULL_CURRENCY_RTL_DEC];
static char ps_currency_code[NULL_CURRENCY_CODE];
static char ps_country_id[NULL_COUNTRY_ID];
static char ps_vat_include_ind[NULL_IND]; /* Vat  Indicator at Store level */
static char ps_tax_level[NULL_CODE];

int pi_tender_ref_no9 = 0;

/*-------------------------------------------------------------------------
InitInputData() does any special initialization the incoming file format
needs. Returns TRUE if ok or FALSE if failure.
-------------------------------------------------------------------------*/
int InitInputData( char *infile, char *badfile )
{
   char *function = "InitInputData";

   /* open RTLOG input file */
   VERBOSE2( "INFO: Opening RTLOG input file <%s>\n", infile );
   if ( ( RTLinFile = fopen( infile, SA_READMODE ) ) == NULL )
   {
      sprintf( logmsg, "%s: Cannot open transaction input file - %s",
               infile, strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( FALSE );
   }
   InFile = strdup( infile);

   /* open bad transaction output file */
   VERBOSE2( "INFO: Opening bad tran file <%s>\n", badfile );
   if ( ( BadTranFile = fopen( badfile, SA_WRITEMODE ) ) == NULL )
   {
      sprintf( logmsg, "%s: Cannot open bad transaction output file - %s",
               badfile, strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      return( FALSE );
   }

   Rectype = NULL;
   Recbuf = NULL;

   FileRecInCount = 0L;
   TranRecCount = 0;

   LastRectype = RTLRT_END;
   inFileBlock = FALSE;
   inTranBlock = FALSE;
   inTranCustBlock = FALSE;
   inTranItemBlock = FALSE;

   return( TRUE );
}


/*-------------------------------------------------------------------------
getNextTran() reads individual records, compiles them into a transaction,
and reformats them into the standard output format.
It returns a 0 if success, a 1 if non-fatal error and a -1 if fatal error.
-------------------------------------------------------------------------*/
int getNextTran( void )
{
   char *function = "getNextTran";
   static pt_item_uom *la_uom_info = NULL;
   static pt_disc_uom *la_disc_uom_info = NULL;
   static pt_igtax_tia *la_igtax_tia_info = NULL;
   RECINFO *r;
   char lrt;
   void *t;
   long i;
   int rv = 0;
   double ld_uom_conv_ratio;
   int itemseq = 1;
   int discseq = 1;
   int igtaxseq = 1;
   int LastTrat;

   /* loop as long as there are tran records to get */
   /* (note that Recidx should get reset with each new transaction) */
   for ( Recidx = 0; ; ++Recidx )
   {
      /* Allocate more space as needed for the transactions */
      if ( (Recidx + 1) >= UsedTranRecs ) /* make sure that there is room to mark the end */
      {
         t = realloc( Rectype, (UsedTranRecs + ALLOCTRANRECS) * sizeof( char));
         if (t == NULL)
         {
            WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                         "Cannot allocate space for Rectype" );
            ExitCode = EXIT_FAILURE;
            return( -1 );
         }
         Rectype = t;
         t = realloc( Recbuf, (UsedTranRecs + ALLOCTRANRECS) * sizeof( void *));
         if (t == NULL)
         {
            WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                         "Cannot allocate space for Recbuf" );
            ExitCode = EXIT_FAILURE;
            return( -1 );
         }
         Recbuf = t;
         for (i = UsedTranRecs; i < (UsedTranRecs + ALLOCTRANRECS); i++)
            Recbuf[i] = NULL;
         t = realloc( la_uom_info, (UsedTranRecs + ALLOCTRANRECS) * sizeof( pt_item_uom));
         if (t == NULL)
         {
            WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                         "Cannot allocate space for la_uom_info" );
            ExitCode = EXIT_FAILURE;
            return( -1 );
         }
         la_uom_info = t;
         t = realloc( la_disc_uom_info, (UsedTranRecs + ALLOCTRANRECS) * sizeof( pt_disc_uom));
         if (t == NULL)
         {
            WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                         "Cannot allocate space for la_disc_uom_info" );
            ExitCode = EXIT_FAILURE;
            return( -1 );
         }
         la_disc_uom_info = t;
         t = realloc( la_igtax_tia_info, (UsedTranRecs + ALLOCTRANRECS) * sizeof( pt_igtax_tia));
         if (t == NULL)
         {
            WRITE_ERROR( RET_FUNCTION_ERR, function, "",
                         "Cannot allocate space for la_igtax_tia_info" );
            ExitCode = EXIT_FAILURE;
            return( -1 );
         }
         la_igtax_tia_info = t;

         UsedTranRecs += ALLOCTRANRECS;
      }

      /* make sure we have memory here */
      if ( Recbuf[ Recidx ] == NULL )
      {
         if ( ( Recbuf[ Recidx ] = malloc( RTL_MAXRECSIZE ) ) == NULL )
         {
            WRITE_ERROR( RET_FUNCTION_ERR, function, "", "Out of memory" );
            ExitCode = EXIT_FAILURE;
            return( -1 );
         }
      }

#ifdef CLEAR_BUFFER_BEFORE_READ
      /* clear memory buffer to spaces */
      memset( Recbuf[ Recidx ], ' ', RTL_MAXRECSIZE );
#endif
      /* read next record */
      if ( getRTLRec( Recbuf[ Recidx ], RTLinFile ) )
      {
         /* count it */
         ++FileRecInCount;

         /* search for record type in table */
         if ( ( r = rtFind( Recbuf[ Recidx ] ) ) != NULL )
         {
            VERBOSE2( "RECORD: <%-5.5s>\n", Recbuf[ Recidx ] );
            /* set record type */
            Rectype[ Recidx ] = lrt = r->rectype;

            /* mark next as end in case it is */
            Rectype[ Recidx + 1 ] = RTLRT_END;

            /* find record type and call handler function. New record types have been added - IGTAX, TPYMT */
            switch( r->rectype )
            {
               case RTLRT_END:
                  /* obviously some kinda problem */
                  return( -1 );

               case RTLRT_FHEAD:
                  if ( procRTLFHead( Recbuf[ Recidx ] ) )
                  {
                     if (writeSAVoucherFHEAD( RTLFHead.business_date) != 0)
                        return( -1 );

                     /* copy location and business_date to globals */
                     strncpy( Location, RTLFHead.location, LEN_LOC );
                     Location[ LEN_LOC ] = '\0';
                     strncpy( BusinessDate, RTLFHead.business_date, LEN_DATEONLY );
                     BusinessDate[ LEN_DATEONLY ] = '\0';

                     /* restart loop for next tran */
                     resetTran( DONT_COPY_RECORD );
                  }
                  else
                     return( -1 );
                  break;

               case RTLRT_FTAIL:
                  if ( procRTLFTail( Recbuf[ Recidx ] ) )
                  {
                     if (writeSAVoucherFTAIL() != 0)
                        return( -1 );

                     /* we are now done with the file */
                     return( EOF_REACHED );
                  }
                  else
                     return( -1 );
                  break;

               case RTLRT_THEAD:
                  if ( ! procRTLTHead( Recbuf[ Recidx ] ) )
                     return( -1 );
                  else
                  {
                     /* Reject the transaction if the transaction processing system is invalid */

                     gi_tsys = code_get_seq( TSYS, LastTHead->tran_process_sys, sizeof(LastTHead->tran_process_sys) );

                     if (gi_tsys == 0)
                     {
                        isTranDuplicate = 0;
                        WrBadTranErrorFile( INVLD_TSYS, function, SART_THEAD, LastTHead->flineid );
                        WrBadTran( Recidx + 1 );
                        resetTran( DONT_COPY_RECORD );
                        LastRectype = lrt;
                        continue;
                     }

                     /* For transactions that originate from OMS and processed in POS, only SALE and RETURN will be
                        imported.  Other transactions will be rejected and will be imported to ReSA through
                        their POS originating transactions. */

                     LastTrat = code_get_seq( TRAT, LastTHead->tran_type, sizeof( LastTHead->tran_type));

                     if ((gi_rsys == RSYSTT_OMS) &&
                         (gi_tsys == TSYSTT_POS) &&
                         (LastTrat != TRATTT_SALE) &&
                         (LastTrat != TRATTT_RETURN))
                     {
                        isTranDuplicate = 0;
                        WrBadTranErrorFile( DUP_REC, function, SART_THEAD, LastTHead->flineid );
                        WrBadTran( Recidx + 1 );
                        resetTran( DONT_COPY_RECORD );
                        LastRectype = lrt;
                        continue;
                     }

                     /*  Trying to find out is there any duplicate transaction at the header level so that
                         we write reject records for other tran types too. The duplicates will be detected
                         from the tdupxxxx.dat file */

                     if ( (strcmp( SysOpt.s_check_dup_miss_tran, YSNO_Y) == 0) &&
                          ( ! EMPTY_FIELD( LastTHead->tran_no ) ) &&
                          (SA_VALID_ALL_NUMERIC( LastTHead->tran_no ) == 0) &&
                          (tdup_addtran( Location, LastTHead->pos_register, LastTHead->tran_no, BusinessDate
                                                                 ) == TDUP_TRANDUP_ATFILE))
                          {
                             isTranDuplicate = 0;
                             WrBadTranErrorFile( DUP_TRAN, function, SART_THEAD, LastTHead->flineid );
                             WrBadTran( Recidx + 1 );
                             resetTran( DONT_COPY_RECORD );
                             LastRectype = lrt; continue;
                          }
                  }
                  break;

               case RTLRT_TTAIL:
                  if ( procRTLTTail( Recbuf[ Recidx ] ) )
                  {
                     if ( isTranDuplicate == 0 )
                     {
                        WrBadTran( Recidx + 1 );
                        resetTran( DONT_COPY_RECORD );
                        isTranDuplicate = 1;
                        LastRectype = lrt;
                        return(1);
                     }
                     else
                     {
                        /* we should now have a fully loaded transaction */
                        if ( ! chkTranFormat() )
                        {
                           WrBadTran( Recidx + 1 );
                           resetTran( DONT_COPY_RECORD );
                           return(1);
                        }
                        else
                        {
                           if ( ! chkTranTailCount( (RTL_TTAIL *)Recbuf[ Recidx ] ) )
                           {
                              WrBadTran( Recidx + 1 );
                              resetTran( DONT_COPY_RECORD );
                              return(1);
                           }
                           else
                           {
                              /* if non-fatal error, write to reject file */
                              if ((rv = reformatTran( la_uom_info, la_disc_uom_info, la_igtax_tia_info, Recbuf[ Recidx ] ) )== 1)
                              {
                                 WrBadTran( Recidx + 1 );
                                 resetTran( DONT_COPY_RECORD );
                              }
                              /* remember last record type for next call */
                              LastRectype = lrt;
                              return( rv );
                           }
                        }
                     }
                  }
                  else
                     return( -1 );
                  break;

               case RTLRT_TCUST:
                  if ( ! procRTLTCust( Recbuf[ Recidx ] ) )
                     return( -1 );
                  else
                  {
                     if ( isTranDuplicate == 0  )
                     {
                        WrBadTran( Recidx + 1 );
                        resetTran( DONT_COPY_RECORD );
                        LastRectype = lrt;continue;
                     }
                  }
                  break;

               case RTLRT_CATT:
                  if ( ! procRTLCAtt( Recbuf[ Recidx ] ) )
                     return( -1 );
                  else
                  {
                     if ( isTranDuplicate == 0 )
                     {
                        WrBadTran( Recidx + 1 );
                        resetTran( DONT_COPY_RECORD );
                        LastRectype = lrt;continue;
                     }
                  }
                  break;

               case RTLRT_TITEM:
                  if ( ! procRTLTItem( Recbuf[ Recidx ],
                                       &ld_uom_conv_ratio,
                                       &la_uom_info[ itemseq ] ) )
                     return( -1 );
                  else
                  {
                     if ( isTranDuplicate == 0 )
                     {
                        WrBadTran( Recidx + 1 );
                        resetTran( DONT_COPY_RECORD );
                        LastRectype = lrt;continue;
                     }
                     else
                        itemseq++;
                  }
                  break;

               case RTLRT_IDISC:
                  if ( ! procRTLIDisc( Recbuf[ Recidx ],
                                       &ld_uom_conv_ratio,
                                       &la_disc_uom_info[ discseq ] ) )
                     return( -1 );
                  else
                  {
                     discseq++;
                     if ( isTranDuplicate == 0 )
                     {
                        WrBadTran( Recidx + 1 );
                        resetTran( DONT_COPY_RECORD );
                        LastRectype = lrt;continue;
                     }
                  }
                  break;

               case RTLRT_IGTAX:
                  if ( ! procRTLIGtax( Recbuf[ Recidx ],
                                       &la_igtax_tia_info[ igtaxseq ] ) )
                     return( -1 );
                  else
                  {
                     igtaxseq++;
                     if ( isTranDuplicate == 0 )
                     {
                        WrBadTran( Recidx + 1 );
                        resetTran( DONT_COPY_RECORD );
                        LastRectype = lrt;continue;
                     }
                  }
                  break;

               case RTLRT_TTAX:
                  if ( ! procRTLTTax( Recbuf[ Recidx ] ) )
                     return( -1 );
                  else
                  {
                     if ( isTranDuplicate == 0 )
                     {
                        WrBadTran( Recidx + 1 );
                        resetTran( DONT_COPY_RECORD );
                        LastRectype = lrt;continue;
                     }
                  }
                  break;

               case RTLRT_TPYMT:
                  if ( ! procRTLTPymt( Recbuf[ Recidx ] ) )
                     return( -1 );
                  else
                  {
                     if ( isTranDuplicate == 0 )
                     {
                        WrBadTran( Recidx + 1 );
                        resetTran( DONT_COPY_RECORD );
                        LastRectype = lrt;continue;
                     }
                  }
                  break;

               case RTLRT_TTEND:
                  if ( ! procRTLTTend( Recbuf[ Recidx ] ) )
                     return( -1 );
                  else
                  {
                     if ( isTranDuplicate == 0 )
                     {
                        WrBadTran( Recidx + 1 );
                        resetTran( DONT_COPY_RECORD );
                        LastRectype = lrt;continue;
                     }
                  }
                  break;

               default:
                  sprintf( logmsg, "Bad record type: %-*.*s at line %-*.*s",
                           LEN_FREC_DESC, LEN_FREC_DESC, (char *)Recbuf[ Recidx ],
                           LEN_FILE_LINE_NO, LEN_FILE_LINE_NO, (char *)Recbuf[ Recidx ] + LEN_FREC_DESC );
                  WRITE_ERROR( RET_FUNCTION_ERR, function, "", logmsg );
                  ExitCode = EXIT_FAILURE;
                  return( -1 );
                  break;
            }

            /* remember last record type for next loop */
            LastRectype = lrt;
         }
         else
         {
            /* just add as unknown type, we'll get it later */
            VERBOSE2( "RECORD: <%-5.5s> (unknown)\n", Recbuf[ Recidx ] );
            Rectype[ Recidx ] = RTLRT_UNKNOWN;
            Rectype[ Recidx + 1 ] = RTLRT_END;
            LastRectype = RTLRT_UNKNOWN;
         }
      }
      else
      {
         if (ferror(RTLinFile))
         {
            WrBadTranErrorFile(FILE_ERROR, function, Recbuf[ Recidx ], NULL);
            ExitCode = EXIT_FAILURE;
            return(-1);
         }
         if (feof(RTLinFile))
         {
            WrBadTranErrorFile(FTAIL_NOT_LAST, function, Recbuf[ Recidx ], NULL);
            ExitCode = EXIT_FAILURE;
            return(-1);
         }
         break;
      }
   }

   return( rv );
}


/*-------------------------------------------------------------------------
FinalInputData() does any special cleanup the incoming file format needs.
Returns TRUE if ok or FALSE if failure.
-------------------------------------------------------------------------*/
int FinalInputData( void )
{
   char *function = "FinalInputData";
   int rv;

   rv = TRUE;

   /* close RTLOG input file */
   if (RTLinFile != NULL && fclose( RTLinFile ) != 0 )
   {
      sprintf( logmsg, "Failure closing input file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = FALSE;
   }

   /* close bad tran output file */
   if (BadTranFile != NULL && fclose( BadTranFile ) != 0 )
   {
      sprintf( logmsg, "Failure closing badtran file - %s", strerror( errno) );
      WRITE_ERROR( RET_FILE_ERR, function, "", logmsg );
      ExitCode = EXIT_FAILURE;
      rv = FALSE;
   }

   return( rv );
}


/*-------------------------------------------------------------------------
getRTLRec( p, f ) gets the next record from the file and puts it at p.
Returns TRUE if ok or FALSE if EOF.
-------------------------------------------------------------------------*/
static int getRTLRec( char *p, FILE *f )
{
   static long lineno = 0;
   char *function = "getRTLRec";
   int c, n;

   n = 0;
   while ( ( c = getc( f ) ) != EOF )
   {
      *p = c;
      p++;

      if ( c == '\n' )
      {
         lineno++;
         return( TRUE );
      }
      if ( ++n >= RTL_MAXRECSIZE - 1 )
      {
         sprintf( logmsg, "Record at line %ld in %s is too long. "
                          "Increase size of RTL_MAXRECSIZE.", lineno, InFile);
         WRITE_ERROR( RET_FUNCTION_ERR, function, "", logmsg );
         /* terminate unknown record */
         *p = '\n';
         /* eat rest of it */
         while ( ( c = getc( f ) ) != EOF )
            if ( c == '\n' )
            {
               lineno++;
               break;
            }
         return( TRUE );
      }
   }
   return( FALSE );
}


/*-------------------------------------------------------------------------
resetTran() resets Recidx to prepare for a new transaction. The parameter
copyrec indicates whether or not the last read record should be copied
down to Recidx[ 0 ] to act as the start of a new transaction or not. Of
course, Recidx then must be set to 1 (if copy) or 0 (if no copy).
-------------------------------------------------------------------------*/
static void resetTran( int copyrec )
{
   void *tmp;

   if ( copyrec )
   {
      /* init list with copy of last record */
      /* Ooooo... do it fast by swapping pointers */
      tmp = Recbuf[ Recidx ];
      Recbuf[ Recidx ] = Recbuf[ 0 ];
      Recbuf[ 0 ] = tmp;
      Rectype[ 0 ] = Rectype[ Recidx ];
      Rectype[ 1 ] = RTLRT_END;
      /* note: Recidx will immediately hereafter be incremented by main loop */
      Recidx = 0;
   }
   else
   {
      /* init list to empty */
      Rectype[ 0 ] = RTLRT_END;
      /* note: Recidx will immediately hereafter be incremented by main loop */
      Recidx = -1;
   }
}


/*-------------------------------------------------------------------------
rtFind() searches the RECINFO table for a match to the record identifier.
Returns a pointer to the RECINFO entry, or NULL if match not found.
IGTAX, Due file changes  - Corresponding new record types have been
defined.
-------------------------------------------------------------------------*/
static RECINFO *rtFind( char *recid )
{
   /* RECINFO table which contains info on various record types */
   /* These are ordered by likely frequency, for performance */
   static RECINFO rit[] = {
      { RTL_TITEM_FRECDESC, RTLRT_TITEM },
      { RTL_IGTAX_FRECDESC, RTLRT_IGTAX },
      { RTL_TTEND_FRECDESC, RTLRT_TTEND },
      { RTL_TPYMT_FRECDESC, RTLRT_TPYMT },
      { RTL_TTAX_FRECDESC,  RTLRT_TTAX  },
      { RTL_IDISC_FRECDESC, RTLRT_IDISC },
      { RTL_THEAD_FRECDESC, RTLRT_THEAD },
      { RTL_TTAIL_FRECDESC, RTLRT_TTAIL },
      { RTL_TCUST_FRECDESC, RTLRT_TCUST },
      { RTL_CATT_FRECDESC,  RTLRT_CATT  },
      { RTL_FHEAD_FRECDESC, RTLRT_FHEAD },
      { RTL_FTAIL_FRECDESC, RTLRT_FTAIL },
      { NULL, 0 }
   };
   RECINFO *r;

   for ( r = rit; r->frecdesc; ++r )
      if ( strncmp( recid, r->frecdesc, strlen( r->frecdesc) ) == 0 )
         return( r );

   return( NULL );
}


/*-------------------------------------------------------------------------
procRTLFHead() processes an RTLFHead record from the input file.
-------------------------------------------------------------------------*/
static int procRTLFHead( void *rp )
{
   char *function = "procRTLFHead";
   RTL_FHEAD *p = (RTL_FHEAD *)rp;
   char file_cutoff_date[ NULL_DATEONLY ];
   char dup_cutoff_date[ NULL_DATEONLY ];
   STOREDAYDAT *sd;
   STOREPOSDAT *storepos;
   char vdate[ NULL_DATEONLY ];
   long tranno_start, tranno_end;
   char store_day_seq_no[NULL_BIG_SEQ_NO];
   char emsg[NULL_ERROR_TEXT];
   long ll_lock_retry = 0;
   char ls_rtlog_orig_sys[LEN_RTLOG_ORIG_SYS];

   if ((EMPTY_FIELD( p->flineid)) ||
       (SA_VALID_ALL_NUMERIC( p->flineid) != 0))
      WrBadTranErrorFile( FHEAD_FIL_STIN, function, SART_FHEAD, p->flineid );

   if ( inFileBlock )
   {
      WrBadTranErrorFile( TOO_MANY_FHEADS, function,
                          SART_FHEAD, p->flineid );
      ExitCode = EXIT_FAILURE;
      return( FALSE );
   }

   if ( LastRectype != RTLRT_END )
   {
      WrBadTranErrorFile( FHEAD_NOT_FIRST, function,
                          SART_FHEAD, p->flineid );
      ExitCode = EXIT_FAILURE;
      return( FALSE );
   }

   inFileBlock = TRUE;
   RTLFHead = *p;

   if (( EMPTY_FIELD( p->business_date) ) ||
       ( SA_VALID_ALL_NUMERIC( p->business_date) != 0 ) ||
       ( SA_VALID_DATE( p->business_date) != 0 ))
   {
      WrBadTranErrorFile( INVLD_BUSINESS_DATE, function,
                          SART_FHEAD, p->flineid );
      ExitCode = EXIT_FAILURE;
      return( FALSE );
   }

   if ( (sd = storeday_lookup( p->location, p->business_date )) == NULL )
   {
      WrBadTranErrorFile( DATAUNEXPECTEDSTOREDAY, function,
                          SART_FHEAD, p->flineid );
      ExitCode = EXIT_FAILURE;
      return( FALSE );
   }
   strncpy( StoreDaySeqNo, sd->store_day_seq_no, LEN_BIG_SEQ_NO);
   strncpy( Day, sd->day, LEN_DAY);
   strncpy( Currency_rtl_dec, sd->currency_rtl_dec, LEN_CURRENCY_RTL_DEC);
   strncpy( ps_currency_code, sd->currency_code, LEN_CURRENCY_CODE);
   nullpad( ps_currency_code, NULL_CURRENCY_CODE);
   strncpy( ps_country_id, sd->country_id, LEN_COUNTRY_ID);
   nullpad( ps_country_id, NULL_COUNTRY_ID);
   strncpy( ps_vat_include_ind, sd->vat_include_ind, LEN_IND);
   nullpad( ps_vat_include_ind, NULL_IND);
   strncpy( ps_tax_level, sd->tax_level, LEN_CODE);
   nullpad( ps_tax_level, NULL_CODE);

   if ( CMPFXLSTR( sd->pos_data_expected, YSNO_N) )
   {
      WrBadTranErrorFile( POSUNEXPECTEDSTOREDAY, function,
                          SART_FHEAD, p->flineid );
      ExitCode = EXIT_FAILURE;
      return( FALSE );
   }

   /* initialize file cutoff date */
   if (fetchVdate( vdate) != 0)
   {
      WRITE_ERROR( RET_FUNCTION_ERR, function, "period", "Cannot get vdate." );
      ExitCode = EXIT_FAILURE;
      return( FALSE );
   }
   strncpy( file_cutoff_date, vdate, LEN_DATEONLY );
   file_cutoff_date[ LEN_DATEONLY ] = '\0';
   decdate( file_cutoff_date, SysOpt.l_day_post_sale );

   /* make sure file isn't too old */
   if ( strncmp( p->business_date, file_cutoff_date, LEN_DATEONLY ) < 0  )
   {
      WrBadTranErrorFile( POSDATATOOOLD, function,
                          SART_FHEAD, p->flineid );
      ExitCode = EXIT_FAILURE;
      return( FALSE );
   }


   memcpy( store_day_seq_no, sd->store_day_seq_no, LEN_BIG_SEQ_NO);
   store_day_seq_no[LEN_BIG_SEQ_NO] = '\0';
   while ((get_lock( store_day_seq_no, SYSI_POS, SALT_W, emsg) != 0) &&
          (ll_lock_retry < MAX_LOCK_TRIES))
   {
         ll_lock_retry++;
         sleep(1);
   }
   if (ll_lock_retry >= MAX_LOCK_TRIES)
   {
      WrBadTranErrorFile( STOREDAY_LOCK_FAILED, function,
                          SART_FHEAD, p->flineid );
      WRITE_ERROR( RET_FUNCTION_ERR, function, "", emsg);
      ExitCode = EXIT_FAILURE;
      return( FALSE );
   }

   if (updateDataStatus( store_day_seq_no) != 0)
   {
      WrBadTranErrorFile( STOREDAYNOTREADYTOBELOAD, function,
                          SART_FHEAD, p->flineid );
      ExitCode = EXIT_FAILURE;
      return( FALSE );
   }

   strncpy(ls_rtlog_orig_sys, p->rtlog_orig_sys,LEN_RTLOG_ORIG_SYS);

   gi_rsys = code_get_seq( RSYS, ls_rtlog_orig_sys, sizeof(ls_rtlog_orig_sys) );

   if (gi_rsys == 0)
   {
      WrBadTranErrorFile( INVLD_RTLOG_ORIG_SYS, function,
                          SART_FHEAD, p->flineid );
      ExitCode = EXIT_FAILURE;
      return( FALSE );
   }

   strncpy(Rtlog_Orig_Sys, p->rtlog_orig_sys,LEN_RTLOG_ORIG_SYS);
   nullpad(Rtlog_Orig_Sys,NULL_RTLOG_ORIG_SYS);

   if (strcmp( SysOpt.s_check_dup_miss_tran, YSNO_Y) == 0)
   {
      /* initialize dup cutoff date */
      strncpy( dup_cutoff_date, vdate, LEN_DATEONLY );
      dup_cutoff_date[ LEN_DATEONLY ] = '\0';
      /* Since transactions on the day with Date = (VDATE - SysOpt.l_max_days_compare_dups) must also be checked,
      dup_cutoff_date is set to (SysOpt.l_max_days_compare_dups+1) */
      decdate( dup_cutoff_date, (SysOpt.l_max_days_compare_dups + 1) );

      storepos = storepos_lookup( p->location);
      if (storepos != NULL)
      {
         tranno_start = ATOLN( storepos->start_tran_no);
         tranno_end = ATOLN( storepos->end_tran_no);
      }
      else
      {
         tranno_start = 1L;
         tranno_end = LONG_MAX;
      }

      /* Save the generated Tran Number flag for later use. */
      strncpy(TranNoGen, sd->tran_no_generated, STRG_SIZE);

      /* load tdup database now that we know loc/date */
      tdup_loaddata( p->location, dup_cutoff_date,
                     tranno_start, tranno_end, Rtlog_Orig_Sys);
   }

   return( TRUE );
}


/*-------------------------------------------------------------------------
procRTLFTail() processes an RTLFTail record from the input file.
-------------------------------------------------------------------------*/
static int procRTLFTail( void *rp )
{
   char *function = "procRTLFTail";
   RTL_FTAIL *p = (RTL_FTAIL *)rp;
   long cnt;

   if ((EMPTY_FIELD( p->flineid)) ||
       (SA_VALID_ALL_NUMERIC( p->flineid) != 0))
      WrBadTranErrorFile( FTAIL_FIL_STIN, function, SART_FTAIL, p->flineid );

   if ( inFileBlock )
   {
      if ( LastRectype != RTLRT_TTAIL )
      {
         VERBOSE( "FTAIL in illegal position\n" );
         WrBadTranErrorFile( FTAIL_IN_TRAN, function, SART_FTAIL, p->flineid );
         WrBadTran( Recidx );
         resetTran( COPY_RECORD );
      }

      inFileBlock = FALSE;

      /* validate file transaction count */
      /* have to init own pointer because the record may have moved */
      if ((! EMPTY_FIELD( p->file_rec_counter)) &&
          (SA_VALID_ALL_NUMERIC( p->file_rec_counter) == 0))
      {
         cnt = ATOLN( p->file_rec_counter );
         FileRecInCount -= 2; /* (-2 because we counted FHEAD and FTAIL) */
         if ( cnt == FileRecInCount )
            return( TRUE );
         else
            WrBadTranErrorFile( INVLD_FTAIL_TRAN_CNT, function, SART_FTAIL, p->flineid );
      }
      else
         WrBadTranErrorFile( FTAIL_FRC_STIN, function, SART_FTAIL, p->flineid );
   }
   else
      WrBadTranErrorFile( INVLD_FTAIL_POS, function, SART_FTAIL, p->flineid );

   ExitCode = EXIT_FAILURE;
   return( FALSE );
}


/*-------------------------------------------------------------------------
procRTLTHead() processes an RTLTHead record from the input file.
-------------------------------------------------------------------------*/
static int procRTLTHead( void *rp )
{
   char *function = "procRTLTHead";
   numCustPerTran = 0 ;
   RTL_THEAD *p = (RTL_THEAD *)rp;

   if ((EMPTY_FIELD( p->flineid)) ||
       (SA_VALID_ALL_NUMERIC( p->flineid) != 0))
      WrBadTranErrorFile( THEAD_FIL_STIN, function, SART_THEAD, p->flineid );

   memset( SaleTotal, '0', sizeof( SaleTotal) - 1);
   SaleTotal[ sizeof( SaleTotal) - 1] = '\0';
   memset( TenderTotal, '0', sizeof( TenderTotal) - 1);
   TenderTotal[ sizeof( TenderTotal) - 1] = '\0';

   SaleTotal[0] = ' ';
   TenderTotal[0] = ' ';

   if ( inFileBlock )
   {
      if ( inTranBlock )
      {
         VERBOSE( "THEAD encountered while in transaction\n" );
         WrBadTranErrorFile( THEAD_IN_TRAN, function, SART_THEAD, p->flineid );
         WrBadTran( Recidx );
         resetTran( COPY_RECORD );
      }
      else
         if ( LastRectype != RTLRT_FHEAD && LastRectype != RTLRT_TTAIL )
         {
            VERBOSE( "THEAD in illegal position\n" );
            WrBadTranErrorFile( THEAD_IN_ILLEGAL_POS, function, SART_THEAD, p->flineid );
            WrBadTran( Recidx );
            resetTran( COPY_RECORD );
         }

      LastTHead = (RTL_THEAD *)Recbuf[ Recidx ];
      inTranBlock = TRUE;
      inTranCustBlock = FALSE;
      inTranItemBlock = FALSE;
      TranRecCount = 0;

      return( TRUE );
   }
   else
      WrBadTranErrorFile( THEAD_IN_ILLEGAL_POS, function, SART_THEAD, p->flineid );

   LastTHead = NULL;
   ExitCode = EXIT_FAILURE;
   return( FALSE );
}


/*-------------------------------------------------------------------------
procRTLTTail() processes an RTLTTail record from the input file.
-------------------------------------------------------------------------*/
static int procRTLTTail( void *rp )
{
   char *function = "procRTLTTail";
   RTL_TTAIL *p = (RTL_TTAIL *)rp;

   if ((EMPTY_FIELD( p->flineid)) ||
       (SA_VALID_ALL_NUMERIC( p->flineid) != 0))
      WrBadTranErrorFile( TTAIL_FIL_STIN, function, SART_TTAIL, p->flineid );

   if ( inFileBlock )
   {
      if ( ! inTranBlock )
      {
         VERBOSE( "TTAIL encountered outside transaction\n" );
         WrBadTranErrorFile( TTAIL_WITHOUT_THEAD, function, SART_TTAIL, p->flineid );
         WrBadTran( Recidx + 1 );
         resetTran( DONT_COPY_RECORD );
      }
      else
         if ( LastRectype != RTLRT_THEAD
           && LastRectype != RTLRT_TITEM
           && LastRectype != RTLRT_IDISC
           && LastRectype != RTLRT_IGTAX
           && LastRectype != RTLRT_TTAX
           && LastRectype != RTLRT_TPYMT
           && LastRectype != RTLRT_TTEND
           && LastRectype != RTLRT_TCUST
           && LastRectype != RTLRT_CATT )
         {
            VERBOSE( "TTAIL in illegal position\n" );
            WrBadTranErrorFile( TTAIL_IN_ILLEGAL_POS, function, SART_TTAIL, p->flineid );
            WrBadTran( Recidx + 1 );
            resetTran( DONT_COPY_RECORD );
         }

      inTranBlock = FALSE;
      inTranCustBlock = FALSE;
      inTranItemBlock = FALSE;

      return( TRUE );
   }
   else
      WrBadTranErrorFile( TTAIL_IN_ILLEGAL_POS, function, SART_TTAIL, p->flineid );

   ExitCode = EXIT_FAILURE;
   return( FALSE );
}


/*-------------------------------------------------------------------------
procRTLTCust() processes an RTLTCust record from the input file.
-------------------------------------------------------------------------*/
static int procRTLTCust( void *rp )
{
   char *function = "procRTLTCust";
   RTL_TCUST *p = (RTL_TCUST *)rp;
   if (numCustPerTran == DUP_TCUST_TRAN)
      {
          WrBadTranErrorFile( TCUST_IN_ILLEGAL_POS, function, SART_TCUST, p->flineid );
          WrBadTran( Recidx + 1 );
          resetTran( DONT_COPY_RECORD );
      }
      numCustPerTran++;

   if ((EMPTY_FIELD( p->flineid)) ||
       (SA_VALID_ALL_NUMERIC( p->flineid) != 0))
      WrBadTranErrorFile( TCUST_FIL_STIN, function, SART_TCUST, p->flineid );

   if ( inFileBlock )
   {
      if ( inTranBlock )
         ++TranRecCount;
      else
      {
         WrBadTranErrorFile( TCUST_IN_ILLEGAL_POS, function, SART_TCUST, p->flineid );
         WrBadTran( Recidx + 1 );
         resetTran( DONT_COPY_RECORD );
      }
      inTranCustBlock = TRUE;
      inTranItemBlock = FALSE;
      return( TRUE );
   }
   else
      WrBadTranErrorFile( TCUST_IN_ILLEGAL_POS, function, SART_TCUST, p->flineid );

   ExitCode = EXIT_FAILURE;
   return( FALSE );
}


/*-------------------------------------------------------------------------
procRTLCAtt() processes an RTLCAtt record from the input file.
-------------------------------------------------------------------------*/
static int procRTLCAtt( void *rp )
{
   char *function = "procRTLCAtt";
   RTL_CATT *p = (RTL_CATT *)rp;

   if ((EMPTY_FIELD( p->flineid)) ||
       (SA_VALID_ALL_NUMERIC( p->flineid) != 0))
      WrBadTranErrorFile( CATT_FIL_STIN, function, SART_CATT, p->flineid );

   if ( inFileBlock )
   {
      if ( inTranBlock && inTranCustBlock )
         ++TranRecCount;
      else
      {
         WrBadTranErrorFile( CATT_IN_ILLEGAL_POS, function, SART_CATT, p->flineid );
         WrBadTran( Recidx + 1 );
         resetTran( DONT_COPY_RECORD );
      }
      return( TRUE );
   }
   else
      WrBadTranErrorFile( CATT_IN_ILLEGAL_POS, function, SART_CATT, p->flineid );

   ExitCode = EXIT_FAILURE;
   return( FALSE );
}


/*-------------------------------------------------------------------------
procRTLTItem() processes an RTLTItem record from the input file.
-------------------------------------------------------------------------*/
static int procRTLTItem( void   *rp,
                         double *od_uom_conv_ratio,
                         pt_item_uom *uom )
{
   char       *function = "procRTLTItem";
   RTL_TITEM  *p = (RTL_TITEM *)rp;
   ITEMDAT    *item;
   REFITEMDAT *ref_item;
   PRIMVARDAT *prim_variant;
   VUPCDAT    *vupc;
   char       qty[NULL_SIGNED_DEC_QTY];
   char       uom_qty[NULL_SIGNED_DEC_QTY];
   char       ld_uom_conv_factor[NULL_SIGNED_DEC_QTY];
   char       unit_retail[NULL_DEC_AMT];
   char       orig_unit_retail[NULL_DEC_AMT];
   char       subtotal[NULL_SIGNED_AMT+4]; /* +4 because this gets multipled by 10000 twice: qty & amt */
   char       ls_temp_standard_qty[NULL_QTY];
   char       ls_temp_standard_unit_retail[NULL_AMT];
   char       ls_temp_standard_orig_unit_retail[NULL_AMT];
   char       ls_item[NULL_ITEM];
   char       ls_standard_uom[NULL_UOM];
   char       ls_selling_uom[NULL_UOM];
   int        li_return_value;
   int        li_valid_item = 0;
   int        li_item_status;
   double     ld_conv_factor = 1.0;
   char       ls_conv_factor[NULL_AMT];
   char       ls_catchweight_ind[NULL_IND];
   char       ls_total_igtax_amt[NULL_IGTAX_AMT];
   char       ls_totaligtaxamt[NULL_AMT]; /* Total IGTAX Amount - Holding 20 characters after rounding */

   memset( subtotal, '0', sizeof( subtotal) - 1);
   subtotal[ sizeof( subtotal) - 1] = '\0';
   subtotal[0] = ' ';

   if ((EMPTY_FIELD( p->flineid)) ||
       (SA_VALID_ALL_NUMERIC( p->flineid) != 0))
      WrBadTranErrorFile( TITEM_FIL_STIN, function, SART_TITEM, p->flineid );

   if ( inFileBlock )
   {
      if ( inTranBlock )
         ++TranRecCount;
      else
      {
         WrBadTranErrorFile( TITEM_IN_ILLEGAL_POS, function, SART_TITEM, p->flineid );
         WrBadTran( Recidx + 1 );
         resetTran( DONT_COPY_RECORD );
      }
      CurItem = (RTL_TITEM *)Recbuf[ Recidx ];
      inTranCustBlock = FALSE;
      inTranItemBlock = TRUE;

      if ( CMPFXLSTR( p->item_type, SAIT_REF ) )
      {
         if ( ! EMPTY_FIELD( p->ref_item ) )
         {
            /* Decode item if it is a variable PLU */
            if ( CMPFXLSTR( p->item_number_type, UPCT_VPLU ) )
            {
               decodeVPLU( p );
               /* else we'll catch the error over in manval.c */
            }
            /* translate to item */
            if ( ( ref_item = ref_item_lookup( p->ref_item ) ) != NULL )
            {
               CPYFXLFXL( p->item, ref_item->item, FT_NUMBER );
            }
            if ( ( item = item_lookup( p->item ) ) != NULL )
            CPYSTRFXL(uom->class_vat_ind, item->class_vat_ind, FT_VARCHAR );
            /* else we'll catch the error over in manval.c */
         }
      }

      if ( ! EMPTY_FIELD( p->item ) )
      {
         /* Decode item if it is a variable PLU and it hasn't already been done */
         if ( (CMPFXLSTR( p->item_number_type, UPCT_VPLU )) && (EMPTY_FIELD( p->ref_item )) )
         {
            decodeVPLU( p );
            /* else we'll catch the error over in manval.c */
         }

         /* lookup item and fill fields */
         if ( ( item = item_lookup( p->item ) ) == NULL )
         {
            if ( ( prim_variant = prim_variant_lookup( Location, p->item ) ) != NULL )
            {
               CPYFXLFXL( p->item, prim_variant->primary_variant, FT_VARCHAR );
               if ( ( item = item_lookup( p->item ) ) != NULL )
               {
                  CPYFXLFXL( p->dept, item->dept, FT_NUMBER );
                  CPYFXLFXL( p->class, item->class, FT_NUMBER );
                  CPYFXLFXL( p->subclass, item->subclass, FT_NUMBER );
                  CPYFXLFXL( uom->standard_uom, item->standard_uom, FT_VARCHAR );
                  CPYSTRFXL(uom->class_vat_ind, item->class_vat_ind, FT_VARCHAR );
                  li_valid_item = 1;
               }
            }
         }
         else
         {
            CPYFXLFXL( p->dept, item->dept, FT_NUMBER );
            CPYFXLFXL( p->class, item->class, FT_NUMBER );
            CPYFXLFXL( p->subclass, item->subclass, FT_NUMBER );
            CPYFXLFXL( uom->standard_uom, item->standard_uom, FT_VARCHAR );
            CPYSTRFXL(uom->class_vat_ind, item->class_vat_ind, FT_VARCHAR );
            li_valid_item = 1;
         }
         /* else we'll catch the error over in manval.c */
      }

      if ( CMPFXLSTR( p->item_type, SAIT_GCN ) ||
          (li_valid_item == 0) ||
          ( (!CMPFXLSTR(p->item_type, SAIT_GCN)) &&
            (!CMPFXLSTR(p->item_type, SAIT_ITEM)) &&
            (!CMPFXLSTR(p->item_type, SAIT_REF)) ))
      {
         CPYFXLFXL(uom->standard_uom,p->selling_uom,FT_VARCHAR);
         CPYFXLFXL(uom->standard_qty,p->qty,FT_NUMBER);
         CPYFXLFXL(uom->standard_unit_retail,p->unit_retail,FT_NUMBER);
         if ( !EMPTY_FIELD( p->orig_unit_retail ) )
            CPYFXLFXL(uom->standard_orig_unit_retail,p->orig_unit_retail,FT_NUMBER);
         else
            memset(uom->standard_orig_unit_retail,' ',sizeof(uom->standard_orig_unit_retail));

         CPYSTRFXL(uom->conversion_error,YSNO_N,FT_VARCHAR);

         if ( CMPFXLSTR( p->item_type, SAIT_GCN ) )
            CPYSTRFXL(uom->class_vat_ind, YSNO_N, FT_VARCHAR );
         else if ( ( item = item_lookup( p->non_merch_item ) ) != NULL )
               CPYSTRFXL(uom->class_vat_ind, item->class_vat_ind, FT_VARCHAR );

            *od_uom_conv_ratio = ld_conv_factor;
      }
      else if ( !EMPTY_FIELD( p->item ) && (li_valid_item) )
      {
         if ( strncmp( p->selling_uom, item->standard_uom, LEN_UOM ) == 0 )
         {
            CPYFXLFXL(uom->standard_uom,item->standard_uom,FT_VARCHAR);
            CPYFXLFXL(uom->standard_qty,p->qty,FT_NUMBER);
            CPYFXLFXL(uom->standard_unit_retail,p->unit_retail,FT_NUMBER);
            if ( !EMPTY_FIELD( p->orig_unit_retail ) )
               CPYFXLFXL(uom->standard_orig_unit_retail,p->orig_unit_retail,FT_NUMBER);
            else
               memset(uom->standard_orig_unit_retail,' ',sizeof(uom->standard_orig_unit_retail));

            CPYSTRFXL(uom->conversion_error,YSNO_N,FT_VARCHAR);
            *od_uom_conv_ratio = ld_conv_factor;
         }
         else /* Convert from selling to standard UOM */
         {
            strncpy(ls_item, p->item, LEN_ITEM);
            strncpy(ls_standard_uom, item->standard_uom, LEN_UOM);
            strncpy(ls_selling_uom, p->selling_uom, LEN_UOM);
            nullpad(ls_selling_uom,NULL_UOM);
            nullpad(ls_standard_uom,NULL_UOM);
            nullpad(ls_item,NULL_ITEM);

            strncpy(ls_catchweight_ind, p->catchweight_ind, LEN_IND);
            ls_catchweight_ind[LEN_IND] = '\0';

            /* For catchweight/variable weight eaches, skip the nominal UOM conversion */
            if ((!strcmp (ls_standard_uom,"EA")) && (!strcmp(ls_catchweight_ind,"Y")))
            {
               li_return_value = 0;
            }
            else
            {
               li_return_value = uom_convert(ls_item,
                                             ls_selling_uom,
                                             ls_standard_uom,
                                             &ld_conv_factor);
            }

            if (li_return_value < 0)
            {
               ExitCode = EXIT_FAILURE;
               return( FALSE );
            }
            else if (li_return_value > 0)
            {
               CPYSTRFXL(uom->conversion_error,YSNO_Y,FT_VARCHAR);
               CPYFXLFXL(uom->standard_uom,item->standard_uom,FT_VARCHAR);
               memset(uom->standard_qty,' ',sizeof(uom->standard_qty));
               memset(uom->standard_unit_retail,' ',sizeof(uom->standard_unit_retail));
               memset(uom->standard_orig_unit_retail,' ',sizeof(uom->standard_orig_unit_retail));
               *od_uom_conv_ratio = ld_conv_factor;
            }
            else if ((! EMPTY_FIELD( p->qty)) &&
                     (SA_VALID_ALL_NUMERIC( p->qty) == 0) &&
                     (! EMPTY_FIELD( p->unit_retail)) &&
                     (SA_VALID_ALL_NUMERIC( p->unit_retail) == 0))
            {
               num2str( qty, sizeof( qty), p->qty, sizeof( p->qty), p->qty_sign);
               num2str( unit_retail, sizeof(unit_retail),
                        p->unit_retail, sizeof( p->unit_retail), SIGN_P);
               *od_uom_conv_ratio = ld_conv_factor;
               /* For variable weight eaches, conv factor is the actual weight divided by # of eaches */
               if ((!strcmp (ls_standard_uom,"EA")) && (!strcmp(ls_catchweight_ind,"Y")))
               {
                  CPYFXLFXL(ls_temp_standard_qty,qty,FT_NUMBER);
                  if ((! EMPTY_FIELD( p->uom_qty)) &&
                      (SA_VALID_ALL_NUMERIC( p->uom_qty) == 0))
                  {
                     num2str( uom_qty, sizeof( uom_qty), p->uom_qty, sizeof( p->uom_qty), p->qty_sign);
                     OraNumMul( uom_qty, "10000", ls_conv_factor, sizeof(ls_conv_factor));
                     OraNumDiv( ls_conv_factor, qty, ls_conv_factor, sizeof(ls_conv_factor));
                     OraNumQtyRtl( unit_retail, ls_conv_factor,ls_temp_standard_unit_retail, sizeof(ls_temp_standard_unit_retail),
                                   ps_rtl_catchweight_dec, "10000" );
                  }
                  else
                  {
                     strcpy(ls_conv_factor, "1");
                     strcpy(ls_temp_standard_unit_retail, unit_retail);
                  }
               }
               /* otherwise use the UOM conversion factor */
               else
               {
                  OraNumMuld( qty, ld_conv_factor,
                              ls_temp_standard_qty, sizeof(ls_temp_standard_qty));
                  /* Multiply by inverse of the conversion factor for unit_retail */
                  ld_conv_factor = 1/ld_conv_factor;
                  OraNumMuld( unit_retail, ld_conv_factor,
                              ls_temp_standard_unit_retail, sizeof(ls_temp_standard_unit_retail));
               }
               sprintf(ls_temp_standard_qty,"%0*.0f",
                       LEN_QTY, atof(ls_temp_standard_qty));
               sprintf(ls_temp_standard_unit_retail,"%0*.0f",
                       LEN_AMT, atof(ls_temp_standard_unit_retail));

               CPYFXL(uom->standard_qty,ls_temp_standard_qty,FT_NUMBER);
               CPYFXL(uom->standard_unit_retail,ls_temp_standard_unit_retail,FT_NUMBER);
               CPYFXLFXL(uom->standard_uom,item->standard_uom,FT_VARCHAR);
               CPYSTRFXL(uom->conversion_error,YSNO_N,FT_VARCHAR);

               if (!EMPTY_FIELD( p->orig_unit_retail ))
               {
                  num2str( orig_unit_retail, sizeof(orig_unit_retail),
                           p->orig_unit_retail, sizeof(p->orig_unit_retail), SIGN_P);
                  /* For variable weight eaches, conv factor is */
                  /* the actual weight divided by # of eaches (calculated above) */
                  if ((!strcmp (ls_standard_uom,"EA")) && (!strcmp(ls_catchweight_ind,"Y")))
                  {
                     OraNumQtyRtl( orig_unit_retail,
                                   ls_conv_factor,ls_temp_standard_orig_unit_retail, sizeof(ls_temp_standard_orig_unit_retail),
                                   ps_rtl_catchweight_dec, "10000" );
                  }
                  else
                  {
                     OraNumMuld( orig_unit_retail, ld_conv_factor,
                                 ls_temp_standard_orig_unit_retail, sizeof(ls_temp_standard_orig_unit_retail));
                  }
                  sprintf(ls_temp_standard_orig_unit_retail,"%0*.0f",
                          LEN_AMT, atof(ls_temp_standard_orig_unit_retail));
                  CPYFXL(uom->standard_orig_unit_retail,ls_temp_standard_orig_unit_retail,FT_NUMBER);
               }
               else
                  memset(uom->standard_orig_unit_retail,' ',sizeof(uom->standard_orig_unit_retail));
            }
            else
            {
               CPYFXLFXL(uom->standard_uom,item->standard_uom,FT_VARCHAR);
               CPYSTRFXL(uom->conversion_error,YSNO_N,FT_VARCHAR);
               memset(uom->standard_qty,' ',sizeof(uom->standard_qty));
               memset(uom->standard_unit_retail,' ',sizeof(uom->standard_unit_retail));
               memset(uom->standard_orig_unit_retail,' ',sizeof(uom->standard_orig_unit_retail));
               *od_uom_conv_ratio = ld_conv_factor;
            }
         }
      }

      if ( EMPTY_FIELD( p->drop_ship_ind ) )
         CPYSTRFXL( p->drop_ship_ind, YSNO_N, FT_VARCHAR );

      if ((code_lookup( SIGN, p->qty_sign, sizeof( p->qty_sign)) != NULL) &&
          (! EMPTY_FIELD( p->qty)) &&
          (SA_VALID_ALL_NUMERIC( p->qty) == 0) &&
          (! EMPTY_FIELD( p->unit_retail)) &&
          (SA_VALID_ALL_NUMERIC( p->unit_retail) == 0))
      {
         {
         strncpy(ls_catchweight_ind, p->catchweight_ind, LEN_IND);
         ls_catchweight_ind[LEN_IND] = '\0';
         if (!strcmp(ls_catchweight_ind, "Y"))
            {
            num2str( qty, sizeof( qty), p->uom_qty, sizeof( p->uom_qty), p->qty_sign);
            }
         else
            {
            num2str( qty, sizeof( qty), p->qty, sizeof( p->qty), p->qty_sign);
            }
         }
            num2str( unit_retail, sizeof(unit_retail),
                     p->unit_retail, sizeof( p->unit_retail), SIGN_P);
         /* 'Unit-Retial * Qty' will not be considered in subtotal for following */
         /* Item Statuses, rather we will be considering 'Payment Amount'  total */
         /* in balancing logic of SaleTotal with TenderTotal */
         li_item_status = code_get_seq( SASI, p->item_status, sizeof( p->item_status) );
         if (!((li_item_status == SASITT_ORI) || (li_item_status == SASITT_ORC) || (li_item_status == SASITT_ORD) ||
             (li_item_status == SASITT_LIN) || (li_item_status == SASITT_LCA) || (li_item_status == SASITT_LCO)))
         {
            if (!strcmp(ls_catchweight_ind, "Y"))
            {
               OraNumQtyRtl( unit_retail, qty, subtotal, sizeof(subtotal),
                             ps_rtl_catchweight_dec, "10000" );
            }
            else
            {
               OraNumQtyRtl( unit_retail, qty, subtotal, sizeof(subtotal),
                             Currency_rtl_dec, "10000" );
            }
         }
            OraNumAdd( subtotal, SaleTotal, SaleTotal, sizeof( SaleTotal));
      }

      if ((! EMPTY_FIELD( p->total_igtax_amt)) &&
          (SA_VALID_ALL_NUMERIC( p->total_igtax_amt) == 0))
      {
         /* Convert p->total_igtax_amt field from 21 to 20 characters - Basically rounding it */
         strncpy( ls_total_igtax_amt, p->total_igtax_amt, LEN_IGTAX_AMT );
         nullpad(ls_total_igtax_amt, NULL_IGTAX_AMT );

         /* Working around a limitation of OraNumRound which assumes the original value has 4 decimal places */
         /* So with an input of "3", rounding digit will be -1 which is the last 21st digit*/
         OraNumRound( ls_total_igtax_amt, "3", ls_total_igtax_amt, sizeof( ls_total_igtax_amt));
         /* OraNumRound puts a space in the first byte so we will overwrite it with '0' */
         ls_total_igtax_amt[0] = '0';

         strncpy(ls_totaligtaxamt, ls_total_igtax_amt, LEN_AMT);
         nullpad(ls_totaligtaxamt, NULL_AMT);
         strcpy( uom->total_igtax_amt, ls_totaligtaxamt );
      }
      return( TRUE );
   }
   else
      WrBadTranErrorFile( TITEM_IN_ILLEGAL_POS, function, SART_TITEM, p->flineid );

   CurItem = NULL;
   ExitCode = EXIT_FAILURE;
   return( FALSE );
}


/*-------------------------------------------------------------------------
procRTLIDisc() processes an RTLIDisc record from the input file.
-------------------------------------------------------------------------*/
static int procRTLIDisc( void *rp,
                         double *id_uom_conv_ratio,
                         pt_disc_uom *uom )
{
   char *function = "procRTLIDisc";
   RTL_IDISC  *p = (RTL_IDISC *)rp;
   char qty[NULL_SIGNED_QTY];
   char uom_qty[NULL_SIGNED_DEC_QTY];
   char unit_disc_amt[NULL_AMT];
   char subtotal[NULL_SIGNED_AMT+4]; /* +4 because this gets multipled by 10000 twice: qty & amt */
   char ls_temp_standard_qty[NULL_QTY];
   char ls_temp_standard_disc_amt[NULL_AMT];
   double ld_uom_conv_ratio = *id_uom_conv_ratio;
   char ls_catchweight_ind[NULL_IND];
   char ls_conv_factor[NULL_AMT];
   int  li_disc_item_status;
   memset( subtotal, '0', sizeof( subtotal) - 1);
   subtotal[ sizeof( subtotal) - 1] = '\0';
   subtotal[0] = ' ';

   if ((EMPTY_FIELD( p->flineid)) ||
       (SA_VALID_ALL_NUMERIC( p->flineid) != 0))
      WrBadTranErrorFile( IDISC_FIL_STIN, function, SART_IDISC, p->flineid );

   if ( inFileBlock )
   {
      if ( inTranBlock && inTranItemBlock )
         ++TranRecCount;
      else
      {
         WrBadTranErrorFile( IDISC_IN_ILLEGAL_POS, function, SART_IDISC, p->flineid );
         WrBadTran( Recidx + 1 );
         resetTran( DONT_COPY_RECORD );
      }
      inTranCustBlock = FALSE;

      if ((! EMPTY_FIELD( p->qty)) &&
          (SA_VALID_ALL_NUMERIC( p->qty) == 0) &&
          (! EMPTY_FIELD( p->unit_disc_amt)) &&
          (SA_VALID_ALL_NUMERIC( p->unit_disc_amt) == 0))
      {
         if (ld_uom_conv_ratio != 0)
         {
            strncpy(ls_catchweight_ind, p->catchweight_ind, LEN_IND);
            ls_catchweight_ind[LEN_IND] = '\0';
            num2str( qty, sizeof( qty), p->qty, sizeof( p->qty), p->qty_sign);
            num2str( unit_disc_amt, sizeof(unit_disc_amt),
                     p->unit_disc_amt, sizeof( p->unit_disc_amt), SIGN_P);
                  /* For variable weight eaches, conv factor is */
                  /* the actual weight divided by # of eaches (calculated above) */
            if (!strcmp(ls_catchweight_ind,"Y") && ld_uom_conv_ratio == 1.0)
            {
               CPYFXLFXL(ls_temp_standard_qty,qty,FT_NUMBER);
               if ((! EMPTY_FIELD( p->uom_qty)) &&
                   (SA_VALID_ALL_NUMERIC( p->uom_qty) == 0))
               {
                  num2str( uom_qty, sizeof( uom_qty), p->uom_qty, sizeof( p->uom_qty), p->qty_sign);
                  OraNumMul(unit_disc_amt, uom_qty,
                            ls_temp_standard_disc_amt, sizeof(ls_temp_standard_disc_amt));
                  OraNumDiv(ls_temp_standard_disc_amt, qty,
                            ls_temp_standard_disc_amt, sizeof(ls_temp_standard_disc_amt));
               }
               else
               {
                  strcpy(ls_conv_factor, "1");
                  strcpy(ls_temp_standard_disc_amt, unit_disc_amt);
               }
            }
            /* otherwise, use the UOM conversion factor calculated in procRTLTItem */
            else
            {
               OraNumMuld( qty, ld_uom_conv_ratio,
                           ls_temp_standard_qty, sizeof(ls_temp_standard_qty) );
               /* Invert ratio to determine unit disc amt */
               ld_uom_conv_ratio = 1/ld_uom_conv_ratio;
               OraNumMuld( unit_disc_amt, ld_uom_conv_ratio,
                           ls_temp_standard_disc_amt, sizeof(ls_temp_standard_disc_amt) );
            }
            sprintf(ls_temp_standard_qty,"%0*.0f",
                    LEN_QTY, atof(ls_temp_standard_qty));
            sprintf(ls_temp_standard_disc_amt,"%0*.0f",
                    LEN_AMT, atof(ls_temp_standard_disc_amt));

            CPYFXL( uom->standard_qty, ls_temp_standard_qty, FT_NUMBER );
            CPYFXL( uom->standard_unit_disc_amt, ls_temp_standard_disc_amt, FT_NUMBER );
            CPYSTRFXL( uom->conversion_error, YSNO_N, FT_VARCHAR );
         }
         else
         {
            CPYSTRFXL( uom->conversion_error, YSNO_Y, FT_VARCHAR );
         }
      }

      if ((code_lookup( SIGN, p->qty_sign, sizeof( p->qty_sign)) != NULL) &&
          (! EMPTY_FIELD( p->qty)) &&
          (SA_VALID_ALL_NUMERIC( p->qty) == 0) &&
          (! EMPTY_FIELD( p->unit_disc_amt)) &&
          (SA_VALID_ALL_NUMERIC( p->unit_disc_amt) == 0))
      {
         num2str( qty, sizeof( qty), p->qty, sizeof( p->qty), p->qty_sign);
         num2str( unit_disc_amt, sizeof( unit_disc_amt),
                  p->unit_disc_amt, sizeof( p->unit_disc_amt), SIGN_P);

         /* Unit Discount Amount * Qty will not be considered in subtotal for following    */
         /* Item Statuses, rather we will be considering Payment Amount total in balancing */
         /* logic of SaleTotal with TenderTotal */
         li_disc_item_status = code_get_seq( SASI, CurItem->item_status, sizeof( CurItem->item_status) );
         if (!((li_disc_item_status == SASITT_ORI) || (li_disc_item_status == SASITT_ORC) || (li_disc_item_status == SASITT_ORD) ||
               (li_disc_item_status == SASITT_LIN) || (li_disc_item_status == SASITT_LCA) || (li_disc_item_status == SASITT_LCO)))
         {
            /* For variable weight eaches, conv factor is the actual weight divided by # of eaches */
            if (!strcmp(ls_catchweight_ind,"Y") && ld_uom_conv_ratio == 1.0)
            {
                OraNumQtyRtl( ls_temp_standard_disc_amt, ls_temp_standard_qty, subtotal, sizeof(subtotal),
                              ps_rtl_catchweight_dec, "10000" );
            }
            else
            {
                if (!strcmp(ls_catchweight_ind,"Y"))
                {
                    OraNumQtyRtl( unit_disc_amt, qty, subtotal, sizeof(subtotal),
                                  ps_rtl_catchweight_dec, "10000" );
                }
                else
                {
                   OraNumQtyRtl( unit_disc_amt, qty, subtotal, sizeof(subtotal),
                              Currency_rtl_dec, "10000" );
                }
           }
         }
         OraNumSub( SaleTotal, subtotal, SaleTotal, sizeof( SaleTotal));
      }
      return( TRUE );
   }
   else
      WrBadTranErrorFile( IDISC_IN_ILLEGAL_POS, function, SART_IDISC, p->flineid );

   ExitCode = EXIT_FAILURE;
   return( FALSE );
}


/*------------------------------------------------------------------------------
procRTLIGtax() processes an RTLIGtax record from the input file.
------------------------------------------------------------------------------*/
static int procRTLIGtax( void *rp,
                         pt_igtax_tia *tia )
{
   char *function = "procRTLIGtax";
   RTL_IGTAX  *p = (RTL_IGTAX *)rp;
   ITEMDAT *itemvat;
   char ls_total_igtax_amt[NULL_IGTAX_AMT]; /* Total IGTAX Amount - Holding 21 characters from input RTLOG file */
   char ls_totaligtaxamt[NULL_AMT]; /* Total IGTAX Amount - Holding 20 characters after rounding */
   char TotalIgtaxAmt[NULL_SIGNED_AMT]; /* IGTAX Changes - 20 characters with sign */
   char ls_class_vat_ind[NULL_IND]; /* Vat Indicator at class-item level */
   int  li_igtax_item_status; /* Referring to Last item's Status */
   char ls_vat_calc_type [2]; /* For storing vat calc type of the vat_region */
   
   if ((EMPTY_FIELD( p->flineid)) ||
       (SA_VALID_ALL_NUMERIC( p->flineid) != 0))
      WrBadTranErrorFile( IGTAX_FIL_STIN, function, SART_IGTAX, p->flineid );

   if ( strcmp( ps_tax_level, "TTAX") == 0)
   {
      WrBadTranErrorFile( TRANLVLTAX_NO_IGTAX, function, SART_IGTAX, p->flineid );
   }

      memset( TotalIgtaxAmt, '0', sizeof( TotalIgtaxAmt) - 1);
      TotalIgtaxAmt[ sizeof( TotalIgtaxAmt) - 1] = '\0';
      TotalIgtaxAmt[0] = ' ';

   if ( inFileBlock )
   {
      if ( inTranBlock && inTranItemBlock )
         ++TranRecCount;
      else
      {
         WrBadTranErrorFile( IGTAX_IN_ILLEGAL_POS, function, SART_IGTAX, p->flineid );
         WrBadTran( Recidx + 1 );
         resetTran( DONT_COPY_RECORD );
      }
      inTranCustBlock = FALSE;

      if ((code_lookup( SIGN,  p->igtax_amt_sign, sizeof(  p->igtax_amt_sign )) != NULL) &&
          (! EMPTY_FIELD( p->total_igtax_amt)) &&
          (SA_VALID_ALL_NUMERIC( p->total_igtax_amt) == 0))
      {
          /* Convert p->total_igtax_amt field from 21 to 20 characters - Basically rounding it */
          strncpy( ls_total_igtax_amt, p->total_igtax_amt, LEN_IGTAX_AMT );
          nullpad(ls_total_igtax_amt, NULL_IGTAX_AMT );


          /* Working around a limitation of OraNumRound which assumes the original value has 4 decimal places */
          /* So with an input of "3", rounding digit will be -1 which is the last 21st digit*/
          OraNumRound( ls_total_igtax_amt, "3", ls_total_igtax_amt, sizeof( ls_total_igtax_amt));
          /* OraNumRound puts a space in the first byte so we will overwrite it with '0' */
          ls_total_igtax_amt[0] = '0';


          strncpy( ls_totaligtaxamt, ls_total_igtax_amt, LEN_AMT );
          nullpad(ls_totaligtaxamt, NULL_AMT );

          num2str( TotalIgtaxAmt, sizeof( TotalIgtaxAmt),
                  ls_totaligtaxamt, sizeof( ls_totaligtaxamt), p->igtax_amt_sign );
                  strcpy(tia->TotalIgtaxAmt, TotalIgtaxAmt);

          /* if system options vat_ind is 'Y', check out at 'class-item' level and store level for the item */
          /* If it is 'Y' at one of the tables, which indicates unit retail is inclusive of vat, so total  */
          /* igtax amt should be added to sale total, else igtax is playing a role of Sales Tax hence      */
          /* included in sale total for balancing with tender total */

          if ( ( itemvat = item_lookup( CurItem->item ) ) != NULL )
          {
             strncpy(ls_class_vat_ind,  itemvat->class_vat_ind, LEN_IND);
             nullpad(ls_class_vat_ind ,NULL_IND);
          }

          if (! EMPTY_FIELD(CurItem->non_merch_item))
          {
             if ( ( itemvat = item_lookup( CurItem->non_merch_item ) ) != NULL )
             {
                strncpy(ls_class_vat_ind,  itemvat->class_vat_ind, LEN_IND);
                nullpad(ls_class_vat_ind, NULL_IND);
             }
             else
             {
                strcpy(ls_class_vat_ind, YSNO_N);
             }
          }
          
          li_igtax_item_status = code_get_seq( SASI, CurItem->item_status, sizeof( CurItem->item_status) );

          if ( strcmp( SysOpt.s_default_tax_type, "SALES") == 0 )
          {
             if (!((li_igtax_item_status == SASITT_ORI) || (li_igtax_item_status == SASITT_ORC) || (li_igtax_item_status == SASITT_ORD) ||
                   (li_igtax_item_status == SASITT_LIN) || (li_igtax_item_status == SASITT_LCA) || (li_igtax_item_status == SASITT_LCO)))
             {
                OraNumAdd( TotalIgtaxAmt, SaleTotal, SaleTotal, sizeof( SaleTotal));
             }
          }
          else if ( strcmp( SysOpt.s_default_tax_type, "SVAT") == 0  )
          {  
             /* get vat calc type for the store  */
             if (fetchvatcalctype(Location,ls_vat_calc_type) != 0)
             {
                WRITE_ERROR( RET_FUNCTION_ERR, function, "vat_region", "Cannot get vat calculation type." );
                ExitCode = EXIT_FAILURE;
                return( FALSE );
             }
             
             if ( strcmp(ls_vat_calc_type, "E") == 0 )
             {
                if (!((li_igtax_item_status == SASITT_ORI) || (li_igtax_item_status == SASITT_ORC) || (li_igtax_item_status == SASITT_ORD) ||
                      (li_igtax_item_status == SASITT_LIN) || (li_igtax_item_status == SASITT_LCA) || (li_igtax_item_status == SASITT_LCO)))
                {
                   OraNumAdd( TotalIgtaxAmt, SaleTotal, SaleTotal, sizeof( SaleTotal));
                }
             }
             else
             {
                if ( strcmp( SysOpt.s_class_level_vat_ind, YSNO_Y) == 0 )
                {
                   if ( strcmp(ls_class_vat_ind, YSNO_N) == 0 )
                   {
                      if (!((li_igtax_item_status == SASITT_ORI) || (li_igtax_item_status == SASITT_ORC) || (li_igtax_item_status == SASITT_ORD) ||
                            (li_igtax_item_status == SASITT_LIN) || (li_igtax_item_status == SASITT_LCA) || (li_igtax_item_status == SASITT_LCO)))
                      {
                         OraNumAdd( TotalIgtaxAmt, SaleTotal, SaleTotal, sizeof( SaleTotal));
                      }
                   }
                }
                else if ( strcmp( ps_vat_include_ind, YSNO_N) == 0 )
                {
                   if (!((li_igtax_item_status == SASITT_ORI) || (li_igtax_item_status == SASITT_ORC) || (li_igtax_item_status == SASITT_ORD) ||
                         (li_igtax_item_status == SASITT_LIN) || (li_igtax_item_status == SASITT_LCA) || (li_igtax_item_status == SASITT_LCO)))
                   {
                      OraNumAdd( TotalIgtaxAmt, SaleTotal, SaleTotal, sizeof( SaleTotal));
                   }
                }
             }
          }
      }
      else
      {
         /* For invalid tax amount, set the value as 0. Also, replacing the first character of ' ' with 0 */
         TotalIgtaxAmt[0] = '0';
         strcpy(tia->TotalIgtaxAmt, TotalIgtaxAmt);
      }
      return( TRUE );

   }
   else
      WrBadTranErrorFile( IGTAX_IN_ILLEGAL_POS, function, SART_IGTAX, p->flineid );

   ExitCode = EXIT_FAILURE;
   return( FALSE );
}


/*-------------------------------------------------------------------------
procRTLTTax() processes an RTLTTax record from the input file.
-------------------------------------------------------------------------*/
static int procRTLTTax( void *rp )
{
   char *function = "procRTLTTax";
   ITEMDAT *itemtax;
   RTL_TTAX *p = (RTL_TTAX *)rp;
   char tax_amt[NULL_SIGNED_AMT];
   char ls_class_vat_ind[NULL_IND];
   int  li_tax_item_status;;
   char ls_vat_calc_type [2];
   
   if ((EMPTY_FIELD( p->flineid)) ||
       (SA_VALID_ALL_NUMERIC( p->flineid) != 0))
      WrBadTranErrorFile( TTAX_FIL_STIN, function, SART_TTAX, p->flineid );

   if ( strcmp( ps_tax_level, "IGTAX") == 0)
   {
      WrBadTranErrorFile( ITEMLVLTAX_NO_TTAX, function, SART_TTAX, p->flineid );
   }

   if ( inFileBlock )
   {
      if ( inTranBlock )
         ++TranRecCount;
      else
      {
         WrBadTranErrorFile( TTAX_IN_ILLEGAL_POS, function, SART_TTAX, p->flineid );
         WrBadTran( Recidx + 1 );
         resetTran( DONT_COPY_RECORD );
      }
      inTranCustBlock = FALSE;
      inTranItemBlock = FALSE;

      if ((code_lookup( SIGN, p->tax_amt_sign, sizeof( p->tax_amt_sign)) != NULL) &&
          (! EMPTY_FIELD( p->tax_amt)) &&
          (SA_VALID_ALL_NUMERIC( p->tax_amt) == 0))
      {

         if ( ( itemtax = item_lookup( CurItem->item ) ) != NULL )
         {
            strncpy(ls_class_vat_ind,  itemtax->class_vat_ind, LEN_IND);
            nullpad(ls_class_vat_ind ,NULL_IND);
         }

         if (! EMPTY_FIELD(CurItem->non_merch_item))
         {
            if ( ( itemtax = item_lookup( CurItem->non_merch_item ) ) != NULL )
            {
               strncpy(ls_class_vat_ind, itemtax->class_vat_ind, LEN_IND);
               nullpad(ls_class_vat_ind, NULL_IND);
            }
            else
            {
               strcpy(ls_class_vat_ind, YSNO_N);
            }
         }

         num2str( tax_amt, sizeof( tax_amt),
                  p->tax_amt, sizeof( p->tax_amt), p->tax_amt_sign);
         OraNumRound( SaleTotal, Currency_rtl_dec, SaleTotal, sizeof( SaleTotal));

         li_tax_item_status = code_get_seq( SASI, CurItem->item_status, sizeof( CurItem->item_status) );

         if ( strcmp( SysOpt.s_default_tax_type, "SALES") == 0 )
         {
            OraNumAdd( tax_amt, SaleTotal, SaleTotal, sizeof( SaleTotal));
         }
         else if ( strcmp( SysOpt.s_default_tax_type, "SVAT") == 0  )
         {
            /* get vat calc type for the store  */
            if (fetchvatcalctype(Location,ls_vat_calc_type) != 0)
            {
               WRITE_ERROR( RET_FUNCTION_ERR, function, "vat_region", "Cannot get vat calculation type." );
               ExitCode = EXIT_FAILURE;
               return( FALSE );
            }
            if ( strcmp(ls_vat_calc_type, "E") == 0 )
            {
               if (!((li_tax_item_status == SASITT_ORI) || (li_tax_item_status == SASITT_ORC) || (li_tax_item_status == SASITT_ORD) ||
                     (li_tax_item_status == SASITT_LIN) || (li_tax_item_status == SASITT_LCA) || (li_tax_item_status == SASITT_LCO)))
               {
                  OraNumAdd( tax_amt, SaleTotal, SaleTotal, sizeof( SaleTotal));
               }
            }
            else
            {
               if ( strcmp( SysOpt.s_class_level_vat_ind, YSNO_Y) == 0 )
               {
                  if ( strcmp(ls_class_vat_ind, YSNO_N) == 0 )
                  {
                     if (!((li_tax_item_status == SASITT_ORI) || (li_tax_item_status == SASITT_ORC) || (li_tax_item_status == SASITT_ORD) ||
                           (li_tax_item_status == SASITT_LIN) || (li_tax_item_status == SASITT_LCA) || (li_tax_item_status == SASITT_LCO)))
                     {
                        OraNumAdd( tax_amt, SaleTotal, SaleTotal, sizeof( SaleTotal));
                     }
                  }
               }
               else if ( strcmp( ps_vat_include_ind, YSNO_N) == 0 )
               {
                  if (!((li_tax_item_status == SASITT_ORI) || (li_tax_item_status == SASITT_ORC) || (li_tax_item_status == SASITT_ORD) ||
                        (li_tax_item_status == SASITT_LIN) || (li_tax_item_status == SASITT_LCA) || (li_tax_item_status == SASITT_LCO)))
                  {
                     OraNumAdd( tax_amt, SaleTotal, SaleTotal, sizeof( SaleTotal));
                  }
               }
            }
         }
      }
      return( TRUE );
   }
   else
      WrBadTranErrorFile( TTAX_IN_ILLEGAL_POS, function, SART_TTAX, p->flineid );

   ExitCode = EXIT_FAILURE;
   return( FALSE );
}


/*---------------------------------------------------------------------------------
procRTLTPymt() processes an RTLTPymt record from the input file. - Due File Changes
---------------------------------------------------------------------------------*/
static int procRTLTPymt( void *rp )
{
   char *function = "procRTLTPymt";
   RTL_TPYMT *p = (RTL_TPYMT *)rp;
   char payment_amt[NULL_SIGNED_AMT];

   if ((EMPTY_FIELD( p->flineid)) ||
       (SA_VALID_ALL_NUMERIC( p->flineid) != 0))
      WrBadTranErrorFile( TPYMT_FIL_STIN, function, SART_TPYMT, p->flineid );

   if ( inFileBlock )
   {
      if ( inTranBlock )
         ++TranRecCount;
      else
      {
         WrBadTranErrorFile( TPYMT_IN_ILLEGAL_POS, function, SART_TPYMT, p->flineid );
         WrBadTran( Recidx + 1 );
         resetTran( DONT_COPY_RECORD );
      }
      inTranCustBlock = FALSE;
      inTranItemBlock = FALSE;

      if ((code_lookup( SIGN, p->payment_amt_sign, sizeof( p->payment_amt_sign)) != NULL) &&
          (! EMPTY_FIELD( p->payment_amt)) &&
          (SA_VALID_ALL_NUMERIC( p->payment_amt) == 0))
      {
         num2str( payment_amt, sizeof( payment_amt),
                  p->payment_amt, sizeof( p->payment_amt), p->payment_amt_sign);

         OraNumAdd( payment_amt, SaleTotal, SaleTotal, sizeof( SaleTotal));
      }
      return( TRUE );
   }
   else
      WrBadTranErrorFile( TPYMT_IN_ILLEGAL_POS, function, SART_TPYMT, p->flineid );

   ExitCode = EXIT_FAILURE;
   return( FALSE );
}


/*-------------------------------------------------------------------------
procRTLTTend() processes an RTLTTend record from the input file.
-------------------------------------------------------------------------*/
static int procRTLTTend( void *rp )
{
   char *function = "procRTLTTend";
   RTL_TTEND *p = (RTL_TTEND *)rp;
   char tender_amt[NULL_SIGNED_AMT];
   char ref_no9[NULL_REF_NO]="                              ";
   int ccrv;
   int canrv;

   if ((EMPTY_FIELD( p->flineid)) ||
       (SA_VALID_ALL_NUMERIC( p->flineid) != 0))
      WrBadTranErrorFile( TTEND_FIL_STIN, function, SART_TTEND, p->flineid );

   if ( inFileBlock )
   {
      if ( inTranBlock )
         ++TranRecCount;
      else
      {
         WrBadTranErrorFile( TTEND_IN_ILLEGAL_POS, function, SART_TTEND, p->flineid );
         WrBadTran( Recidx + 1 );
         resetTran( DONT_COPY_RECORD );
      }
      inTranCustBlock = FALSE;
      inTranItemBlock = FALSE;

      if ((code_lookup( SIGN, p->tender_amt_sign, sizeof( p->tender_amt_sign)) != NULL) &&
          (! EMPTY_FIELD( p->tender_amt)) &&
          (SA_VALID_ALL_NUMERIC( p->tender_amt) == 0))
      {
         num2str( tender_amt, sizeof( tender_amt),
                  p->tender_amt, sizeof( p->tender_amt), p->tender_amt_sign);
         OraNumAdd( tender_amt, TenderTotal, TenderTotal, sizeof( TenderTotal));
      }

      if ( !EMPTY_FIELD(p->cc_no) )
      {
         ccrv = ccval( p->cc_no, SysOpt.s_cc_no_mask_char, p->cc_exp_date,
                       BusinessDate );
         /* make sure the Credit Card Masked is valid */
         if ( (ccrv & CCVAL_NOTOK_MASKDIGIT) != CCVAL_OK)
         {
            WrBadTranErrorFile( INVLD_CC_MASK, function, SART_TTEND, p->flineid );
            ExitCode = EXIT_FAILURE;
            return( FALSE );
         }
      }

      if ( !EMPTY_FIELD(p->check_acct_no) )
      {
         canrv = can_len_mask_val(p->check_acct_no, SysOpt.s_cc_no_mask_char);
         /* make sure the Checking Account Number Masked is valid */
         if(canrv != 0)
         {
            WrBadTranErrorFile( INVLD_CAN_MASK, function, SART_TTEND, p->flineid );
            ExitCode = EXIT_FAILURE;
            return( FALSE );
         }
      }

      if(! CMPFXLSTR( p->ref_no9, ref_no9) ) 
          pi_tender_ref_no9 = 1;
      return( TRUE );
   }
   else
      WrBadTranErrorFile( TTEND_IN_ILLEGAL_POS, function, SART_TTEND, p->flineid );

   ExitCode = EXIT_FAILURE;
   return( FALSE );
}


/*-------------------------------------------------------------------------
chkTranFormat() checks the current transaction for RTLOGish problems and
indicates with return value if good or bad.
-------------------------------------------------------------------------*/
static int chkTranFormat( void )
{
   char *function = "chkTranFormat";
   int nthead, nttail, ntcust, ncatt, ntitem, nidisc, nigtax, nttax, ntpymt, nttend;
   int goodflag, i;
   RTL_THEAD *thead;
   RTL_TITEM *titem;
   int li_titem_size;

   /* init flag */
   goodflag = TRUE;

   /* count record types, watch for errors */
   nthead = nttail = ntcust = ncatt = ntitem = nidisc = nigtax = nttax = ntpymt = nttend = 0;
   for ( i = 0; Rectype[ i ] != RTLRT_END; ++i )
      switch( Rectype[ i ] )
      {
         case RTLRT_THEAD:
            ++nthead;
            thead = (RTL_THEAD *)Recbuf[ i ];
#ifdef RAW_RECORD_CHECK
            if ( ! rrchk( Recbuf[ i ], sizeof( RTL_THEAD ) ) )
            {
               WrBadTranErrorFile( GARBAGE_IN_RECORD, function, SART_THEAD,
                                   thead->flineid );
               goodflag = FALSE;
               ExitCode = EXIT_FAILURE;
            }
#endif
            break;

         case RTLRT_TTAIL:
            ++nttail;
#ifdef RAW_RECORD_CHECK
            if ( ! rrchk( Recbuf[ i ], sizeof( RTL_TTAIL ) ) )
            {
               WrBadTranErrorFile( GARBAGE_IN_RECORD, function, SART_TTAIL,
                                   ((RTL_TTAIL *)Recbuf[ i ])->flineid );
               goodflag = FALSE;
               ExitCode = EXIT_FAILURE;
            }
#endif
            break;

         case RTLRT_TCUST:
            ++ntcust;
#ifdef RAW_RECORD_CHECK
            if ( ! rrchk( Recbuf[ i ], sizeof( RTL_TCUST ) ) )
            {
               WrBadTranErrorFile( GARBAGE_IN_RECORD, function, SART_TCUST,
                                   ((RTL_TCUST *)Recbuf[ i ])->flineid );
               goodflag = FALSE;
               ExitCode = EXIT_FAILURE;
            }
#endif
            break;

         case RTLRT_CATT:
            ++ncatt;
#ifdef RAW_RECORD_CHECK
            if ( ! rrchk( Recbuf[ i ], sizeof( RTL_CATT ) ) )
            {
               WrBadTranErrorFile( GARBAGE_IN_RECORD, function, SART_CATT,
                                   ((RTL_CATT *)Recbuf[ i ])->flineid );
               goodflag = FALSE;
               ExitCode = EXIT_FAILURE;
            }
#endif
            break;

         case RTLRT_TITEM:
            ++ntitem;
            titem = (RTL_TITEM *)Recbuf[ i ];
#ifdef RAW_RECORD_CHECK
            li_titem_size = sizeof( RTL_TITEM );
            if ( ! rrchk( Recbuf[ i ], li_titem_size ) )
            {
               WrBadTranErrorFile( GARBAGE_IN_RECORD, function, SART_TITEM,
                                   ((RTL_TITEM *)Recbuf[ i ])->flineid );
               goodflag = FALSE;
               ExitCode = EXIT_FAILURE;
            }
#endif
            break;

         case RTLRT_IDISC:
            ++nidisc;
#ifdef RAW_RECORD_CHECK
            if ( ! rrchk( Recbuf[ i ], sizeof( RTL_IDISC ) ) )
            {
               WrBadTranErrorFile( GARBAGE_IN_RECORD, function, SART_IDISC,
                                   ((RTL_IDISC *)Recbuf[ i ])->flineid );
               goodflag = FALSE;
               ExitCode = EXIT_FAILURE;
            }
#endif
            if ( ntitem == 0 )
            {
               VERBOSE( "IDISC before any TITEM\n" );
               WrBadTranErrorFile( IDISC_IN_ILLEGAL_POS, function, SART_IDISC,
                                   ((RTL_IDISC *)Recbuf[ i ])->flineid );
               goodflag = FALSE;
               ExitCode = EXIT_FAILURE;
            }
            break;

         case RTLRT_IGTAX:
            ++nigtax;
#ifdef RAW_RECORD_CHECK
            if ( ! rrchk( Recbuf[ i ], sizeof( RTL_IGTAX ) ) )
            {
               WrBadTranErrorFile( GARBAGE_IN_RECORD, function, SART_IGTAX,
                                   ((RTL_IGTAX *)Recbuf[ i ])->flineid );
               goodflag = FALSE;
               ExitCode = EXIT_FAILURE;
            }
#endif
            if ( ntitem == 0 )
            {
               VERBOSE( "IGTAX before any TITEM\n" );
               WrBadTranErrorFile( IGTAX_IN_ILLEGAL_POS, function, SART_IGTAX,
                                 ((RTL_IGTAX *)Recbuf[ i ])->flineid );
               goodflag = FALSE;
           }
           break;

         case RTLRT_TTAX:
            ++nttax;
#ifdef RAW_RECORD_CHECK
            if ( ! rrchk( Recbuf[ i ], sizeof( RTL_TTAX ) ) )
            {
               WrBadTranErrorFile( GARBAGE_IN_RECORD, function, SART_TTAX,
                                   ((RTL_TTAX *)Recbuf[ i ])->flineid );
               goodflag = FALSE;
               ExitCode = EXIT_FAILURE;
            }
#endif
            break;

         case RTLRT_TPYMT:
            ++ntpymt;
#ifdef RAW_RECORD_CHECK
            if ( ! rrchk( Recbuf[ i ], sizeof( RTL_TPYMT ) ) )
            {
               WrBadTranErrorFile( GARBAGE_IN_RECORD, function, SART_TPYMT,
                                   ((RTL_TPYMT *)Recbuf[ i ])->flineid );
               goodflag = FALSE;
               ExitCode = EXIT_FAILURE;
            }
#endif
            break;

         case RTLRT_TTEND:
            ++nttend;
#ifdef RAW_RECORD_CHECK
            if ( ! rrchk( Recbuf[ i ], sizeof( RTL_TTEND ) ) )
            {
               WrBadTranErrorFile( GARBAGE_IN_RECORD, function, SART_TTEND,
                                   ((RTL_TTEND *)Recbuf[ i ])->flineid );
               goodflag = FALSE;
               ExitCode = EXIT_FAILURE;
            }
#endif
            break;

         case RTLRT_UNKNOWN:
            VERBOSE2( "Unknown record type <%-5.5s>\n", Recbuf[ i ] );
            WrBadTranErrorFile( TRAN_XTRA_REC, function, (char *)Recbuf[ i ],
                                ((char *)(Recbuf[ i ]) + LEN_FREC_DESC) );
            goodflag = FALSE;
            break;

         default:
            break;
      }

   /* check from record type counts */
   /* thead required */
   if ( nthead == 0 )
   { VERBOSE( "Missing THEAD\n" );
      WrBadTranErrorFile( MISSING_THEAD, function, SART_THEAD, NULL );
      goodflag = FALSE;
   }

   /* ttail required */
   if ( nttail == 0 )
   {
      VERBOSE( "Missing TTAIL\n" );
      WrBadTranErrorFile( MISSING_TTAIL, function, SART_TTAIL, LastTHead->flineid );
      goodflag = FALSE;
   }

   return( goodflag );
}


/*-------------------------------------------------------------------------
chkTranTailCount( p ) checks the current transactions tail count, and
indicates with return value if good or bad.
-------------------------------------------------------------------------*/
static int chkTranTailCount( RTL_TTAIL *p )
{
   char *function = "chkTranTailCount";
   int cnt;

   VERBOSE( "INFO: Checking tran tail count\n" );
   /* validate transaction record count */
   if ((! EMPTY_FIELD( p->tran_rec_counter)) &&
       (SA_VALID_ALL_NUMERIC( p->tran_rec_counter) == 0))
   {
      cnt = ATOIN( p->tran_rec_counter );
      if ( cnt == TranRecCount )
         return( TRUE );
      else
      {
         /* write error record */
         VERBOSE( "TTAIL Transaction record count incorrect\n" );
         if ( cnt < TranRecCount )
            WrBadTranErrorFile( TRAN_XTRA_REC, function, SART_TTAIL, p->flineid );
         else
            WrBadTranErrorFile( TRAN_MISS_REC, function, SART_TTAIL, p->flineid );
      }
   }
   else
      WrBadTranErrorFile( TTAIL_TRC_STIN, function, SART_TTAIL, p->flineid );

   return( FALSE );
}


#ifdef RAW_RECORD_CHECK
/*-------------------------------------------------------------------------
rrchk() is an optional function that carefully examines the incoming
record for any embedded stuff that might screw it up, such as nulls, early
newlines, etc. Returns TRUE if ok, FALSE if bad.
-------------------------------------------------------------------------*/
static int rrchk( void *rp, size_t reclen )
{
   int i, enlp, rv;

   rv = TRUE;
   enlp = reclen - 1; /* expected newline position */
   for ( i = 0; i < reclen; ++i )
      switch( ((char *)rp)[ i ] )
      {
         case '\0':
            RRC_VERBOSE5( "<%*.*s> Embedded null at position %d\n",
                          (LEN_FREC_DESC + LEN_FILE_LINE_NO),
                          (LEN_FREC_DESC + LEN_FILE_LINE_NO),
                          (char *)rp, i );
            rv = FALSE;
            break;

         case '\t':
            RRC_VERBOSE5( "<%*.*s> Embedded tab at position %d\n",
                          (LEN_FREC_DESC + LEN_FILE_LINE_NO),
                          (LEN_FREC_DESC + LEN_FILE_LINE_NO),
                          (char *)rp, i );
            rv = FALSE;
            break;

         case '\r':
            RRC_VERBOSE5( "<%*.*s> Embedded CR at position %d\n",
                          (LEN_FREC_DESC + LEN_FILE_LINE_NO),
                          (LEN_FREC_DESC + LEN_FILE_LINE_NO),
                          (char *)rp, i );
            rv = FALSE;
            break;

         case '\n':
            if ( i != enlp ) /* only if not correct one at end */
            {
               RRC_VERBOSE5( "<%*.*s> Embedded newline at position %d\n",
                             (LEN_FREC_DESC + LEN_FILE_LINE_NO),
                             (LEN_FREC_DESC + LEN_FILE_LINE_NO),
                             (char *)rp, i );
               rv = FALSE;
            }
            break;

         default:
            break;
      }

   return( rv );
}
#endif


/*-------------------------------------------------------------------------
reformatTran() takes the current RTLOG tran and reformats it into the
SQL load flat file format.
It returns a 0 if success, a 1 if non-fatal error and a -1 if fatal error.
-------------------------------------------------------------------------*/
static int reformatTran( pt_item_uom *uom, pt_disc_uom *disc_uom, pt_igtax_tia *igtax_tia, void *rp )
{
   char *function = "reformatTran";
   int ntcust, ncatt, ntitem, nidisc, nigtax, nttax, ntpymt, nttend;
   int attribseq, itemseq, discseq, igtaxseq, taxseq, pymtseq, tendseq, actual_discseq, actual_igtaxseq, igtax_seq, item_seq;
   int i,rv;
   int errorflag, igtaxerrorflag;
   RTL_THEAD *p = (RTL_THEAD *)rp;
   char igtax_ind[LEN_IND] = "N";

   ntcust = ncatt = ntitem = nidisc = nigtax = nttax = ntpymt = nttend = item_seq = 0;

   /* check transaction type */
   CurTrat = code_get_seq( TRAT, LastTHead->tran_type, sizeof( LastTHead->tran_type));
   if (CurTrat == 0)
      CurTrat = TRATTT_ERR;
   else if (CurTrat == TRATTT_TERM)
   {
      WrBadTranErrorFile( TRAN_XTRA_REC, function, (char *)Recbuf[ 0 ],
                          ((char *)(Recbuf[ 0 ]) + LEN_FREC_DESC) );
      /* exit prematurely, with code 1 -- non-fatal error */
      abortFmt();
      return( 1 );
   }
   CurTras = code_get_seq( TRAS, LastTHead->sub_tran_type, sizeof( LastTHead->sub_tran_type));

   rv = 0;
   resetFmt();
   attribseq = itemseq = discseq = igtaxseq = taxseq = pymtseq = tendseq = actual_discseq = actual_igtaxseq = 0;
   for ( i = 0; Rectype[ i ] != RTLRT_END; ++i )
   {
      errorflag = 0;
      igtaxerrorflag = 0;

      switch( Rectype[ i ] )
      {
         case RTLRT_FHEAD:
            break;

         case RTLRT_FTAIL:
            break;

         case RTLRT_THEAD:
            itemseq = discseq = igtaxseq = taxseq = pymtseq = tendseq = actual_discseq = actual_igtaxseq = 0;

            TranSeqNo = nextTranSeqNo(SysOpt.s_table_owner);

            if (!mvSATHead( (RTL_THEAD *)Recbuf[ i ]))
            {
               errorflag = 1;
            }
            if (fmtSATHead( (RTL_THEAD *)Recbuf[ i ], SaleTotal ) == FALSE)
               rv = -1;
            if ((CurTrat == TRATTT_SALE) || (CurTrat == TRATTT_RETURN) || (CurTrat == TRATTT_EEXCH) || (CurTrat == TRATTT_SPLORD))
            {
               if(get_rounding_rule(ps_currency_code, ps_country_id,
                                    Currency_rtl_dec,
                                    &SaleTotal, &TenderTotal, pi_tender_ref_no9) < 0)
                  return(-1);
               if (strcmp( SaleTotal, TenderTotal) != 0)
               {
                  char msg[ NULL_ORIG_VALUE ];
                  sprintf( msg, "S:%s T:%s", SaleTotal, TenderTotal);
                  WrBadTranError( TRAN_OUT_BAL, TranSeqNo, -1, -1,
                                  SART_THBALC, msg, strlen( msg));
                  errorflag = 1;
               }
            }
            if (errorflag)
            {
               setErrorSATHead();
            }
            break;

         case RTLRT_TTAIL:
            if (rv == 0)
               saveFmt();
            break;

         case RTLRT_TCUST:
            ++ntcust;
            if ( ntcust > 1 )
            {
               VERBOSE( "Extra TCUST record\n" );
               WrBadTranError( TRAN_XTRA_CUST, TranSeqNo, -1, -1,
                               SART_TCUST, "", 0 );
               errorflag = 1;
            }
            switch( CurTrat )
            {
               case TRATTT_ERR:
                  break;
               case TRATTT_OPEN:
                  WrBadTranError( OPEN_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_NOSALE:
                  WrBadTranError( NOSALE_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_VOID:
                  break;
               case TRATTT_PVOID:
                  WrBadTranError( PVOID_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_SALE:
                  break;
               case TRATTT_RETURN:
                  break;
               case TRATTT_EEXCH:
                  break;
               case TRATTT_SPLORD:
                  break;
               case TRATTT_PAIDIN:
                  WrBadTranError( PAIDIN_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PAIDOU:
                  if (CurTras != TRASTT_CACCOM)
                  {
                     WrBadTranError( PAIDOU_NO_CUST, TranSeqNo, -1, -1,
                                     SART_TCUST, "", 0 );
                     errorflag = 1;
                  }
                  break;
               case TRATTT_PULL:
                  WrBadTranError( PULL_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_LOAN:
                  WrBadTranError( LOAN_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_COND:
                  WrBadTranError( COND_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_CLOSE:
                  WrBadTranError( CLOSE_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TOTAL:
                  WrBadTranError( TOTAL_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_REFUND:
                  break;
               case TRATTT_METER:
                  WrBadTranError( METER_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PUMPT:
                  WrBadTranError( PUMPT_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TANKDP:
                  WrBadTranError( TANKDP_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_DCLOSE:
                  WrBadTranError( DCLOSE_NO_CUST, TranSeqNo, -1, -1,
                                  SART_TCUST, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_OTHER:
                  break;
               default:
                  WrBadTranErrorFile( TRAN_NOT_RECOGNIZED, function,
                                      (char *)Recbuf[ i ],
                                      ((char *)(Recbuf[ i ]) + LEN_FREC_DESC) );
                  rv = -1;
                  break;
            }
            if (rv != 0)
            {
               abortFmt();
               return( rv );
            }
            if (!mvSATCust( (RTL_TCUST *)Recbuf[ i ] ))
            {
               errorflag = 1;
            }
            if (fmtSACustomer( (RTL_TCUST *)Recbuf[ i ] ) == FALSE)
               rv = -1;

            if (errorflag)
            {
               setErrorSATHead();
            }
            break;

         case RTLRT_CATT:
            ++ncatt;
            ++attribseq;
            if ( ntcust == 0 )
            {
               VERBOSE( "CATT without TCUST\n" );
               WrBadTranError( CATT_IN_ILLEGAL_POS, TranSeqNo, -1, -1,
                               SART_CATT, "", 0 );
            }
            switch( CurTrat )
            {
               case TRATTT_ERR:
                  break;
               case TRATTT_OPEN:
                  WrBadTranError( OPEN_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_NOSALE:
                  WrBadTranError( NOSALE_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_VOID:
                  break;
               case TRATTT_PVOID:
                  WrBadTranError( PVOID_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_SALE:
                  break;
               case TRATTT_RETURN:
                  break;
               case TRATTT_EEXCH:
                  break;
               case TRATTT_SPLORD:
                  break;
               case TRATTT_PAIDIN:
                  WrBadTranError( PAIDIN_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PAIDOU:
                  if (CurTras != TRASTT_CACCOM)
                  {
                     WrBadTranError( PAIDOU_NO_CATT, TranSeqNo, attribseq, -1,
                                     SART_CATT, "", 0 );
                     errorflag = 1;
                  }
                  break;
               case TRATTT_PULL:
                  WrBadTranError( PULL_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_LOAN:
                  WrBadTranError( LOAN_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_COND:
                  WrBadTranError( COND_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_CLOSE:
                  WrBadTranError( CLOSE_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TOTAL:
                  WrBadTranError( TOTAL_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_REFUND:
                  break;
               case TRATTT_METER:
                  WrBadTranError( METER_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PUMPT:
                  WrBadTranError( PUMPT_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TANKDP:
                  WrBadTranError( TANKDP_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_DCLOSE:
                  WrBadTranError( DCLOSE_NO_CATT, TranSeqNo, attribseq, -1,
                                  SART_CATT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_OTHER:
                  break;
               default:
                  WrBadTranErrorFile( TRAN_NOT_RECOGNIZED, function,
                                      (char *)Recbuf[ i ],
                                      ((char *)(Recbuf[ i ]) + LEN_FREC_DESC) );
                  rv = -1;
                  break;
            }
            if (rv != 0)
            {
               abortFmt();
               return( rv );
            }
            if (!mvSACAtt( (RTL_CATT *)Recbuf[ i ], attribseq ))
            {
               errorflag = 1;
            }
            if (fmtSACAtt( (RTL_CATT *)Recbuf[ i ], attribseq ) == FALSE)
               rv = -1;

            if (errorflag)
            {
               setErrorSATHead();
            }
            break;

         case RTLRT_TITEM:
            ++ntitem;
            ++itemseq;
            actual_discseq = 0;
            actual_igtaxseq = 0;
            CurItem = (RTL_TITEM *)Recbuf[ i ];
            switch( CurTrat )
            {
               case TRATTT_ERR:
                  break;
               case TRATTT_OPEN:
                  WrBadTranError( OPEN_NO_ITEM, TranSeqNo, itemseq, -1,
                                  SART_TITEM, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_NOSALE:
                  WrBadTranError( NOSALE_NO_ITEM, TranSeqNo, itemseq, -1,
                                  SART_TITEM, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_VOID:
                  break;
               case TRATTT_PVOID:
                  WrBadTranError( PVOID_NO_ITEM, TranSeqNo, itemseq, -1,
                                  SART_TITEM, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_SALE:
                  break;
               case TRATTT_RETURN:
                  break;
               case TRATTT_EEXCH:
                  break;
               case TRATTT_SPLORD:
                  break;
               case TRATTT_PAIDIN:
                  WrBadTranError( PAIDIN_NO_ITEM, TranSeqNo, itemseq, -1,
                                  SART_TITEM, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PAIDOU:
                  if (CurTras != TRASTT_CACCOM)
                  {
                     WrBadTranError( PAIDOU_NO_ITEM, TranSeqNo, itemseq, -1,
                                     SART_TITEM, "", 0 );
                     errorflag = 1;
                  }
                  break;
               case TRATTT_PULL:
                  WrBadTranError( PULL_NO_ITEM, TranSeqNo, itemseq, -1,
                                  SART_TITEM, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_LOAN:
                  WrBadTranError( LOAN_NO_ITEM, TranSeqNo, itemseq, -1,
                                  SART_TITEM, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_COND:
                  WrBadTranError( COND_NO_ITEM, TranSeqNo, itemseq, -1,
                                  SART_TITEM, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_CLOSE:
                  WrBadTranError( CLOSE_NO_ITEM, TranSeqNo, itemseq, -1,
                                  SART_TITEM, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TOTAL:
                  WrBadTranError( TOTAL_NO_ITEM, TranSeqNo, itemseq, -1,
                                  SART_TITEM, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_REFUND:
                  WrBadTranError( REFUND_NO_ITEM, TranSeqNo, itemseq, -1,
                                  SART_TITEM, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_METER:
                  break;
               case TRATTT_PUMPT:
                  break;
               case TRATTT_TANKDP:
                  break;
               case TRATTT_DCLOSE:
                  WrBadTranError( DCLOSE_NO_ITEM, TranSeqNo, itemseq, -1,
                                  SART_TITEM, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_OTHER:
                  break;
               default:
                  WrBadTranErrorFile( TRAN_NOT_RECOGNIZED, function,
                                      (char *)Recbuf[ i ],
                                      ((char *)(Recbuf[ i ]) + LEN_FREC_DESC) );
                  rv = -1;
                  break;
            }
            if (rv != 0)
            {
               abortFmt();
               return( rv );
            }

            /* Before processing next item, validate for duplicate igtax codes in igtax */
            /* records if any. For pack items igtax code and component item combination */
            /* should be unique. */
            if ( strcmp( igtax_ind, "Y") == 0 )
            {
               if (!mvSAIGTaxVal( item_seq, igtax_seq ) )
                  errorflag = 1;

               /* reset the igtax_ind to "N" for next item-igtax validation */
               strncpy(igtax_ind, "N", LEN_IND);
            }

            if (!mvSATItem( (RTL_TITEM *)Recbuf[ i ], itemseq, &uom[ itemseq ] ))
            {
               errorflag = 1;
            }

            if (fmtSATItem( (RTL_TITEM *)Recbuf[ i ],
                            itemseq,
                            &uom[ itemseq ] ) == FALSE )
               rv = -1;

            if (errorflag)
               setErrorSATItem();
            igtax_seq = 0;
            ++item_seq;
            break;

         case RTLRT_IDISC:
            ++nidisc;
            ++discseq;
            ++actual_discseq;
            switch( CurTrat )
            {
               case TRATTT_ERR:
                  break;
               case TRATTT_OPEN:
                  WrBadTranError( OPEN_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_NOSALE:
                  WrBadTranError( NOSALE_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_VOID:
                  break;
               case TRATTT_PVOID:
                  WrBadTranError( PVOID_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_SALE:
                  break;
               case TRATTT_RETURN:
                  break;
               case TRATTT_EEXCH:
                  break;
               case TRATTT_SPLORD:
                  break;
               case TRATTT_PAIDIN:
                  WrBadTranError( PAIDIN_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PAIDOU:
                  if (CurTras != TRASTT_CACCOM)
                  {
                     WrBadTranError( PAIDOU_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                     SART_IDISC, "", 0 );
                     errorflag = 1;
                  }
                  break;
               case TRATTT_PULL:
                  WrBadTranError( PULL_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_LOAN:
                  WrBadTranError( LOAN_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_COND:
                  WrBadTranError( COND_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_CLOSE:
                  WrBadTranError( CLOSE_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TOTAL:
                  WrBadTranError( TOTAL_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_REFUND:
                  WrBadTranError( REFUND_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_METER:
                  break;
               case TRATTT_PUMPT:
                  WrBadTranError( PUMPT_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TANKDP:
                  WrBadTranError( TANKDP_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_DCLOSE:
                  WrBadTranError( DCLOSE_NO_DISC, TranSeqNo, itemseq, actual_discseq,
                                  SART_IDISC, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_OTHER:
                  break;
               default:
                  WrBadTranErrorFile( TRAN_NOT_RECOGNIZED, function,
                                      (char *)Recbuf[ i ],
                                      ((char *)(Recbuf[ i ]) + LEN_FREC_DESC) );
                  rv = -1;
                  break;
            }
            if (rv != 0)
            {
               abortFmt();
               return( rv );
            }
            if (!mvSATDisc( (RTL_IDISC *)Recbuf[ i ], itemseq, actual_discseq, &disc_uom[ discseq ] ))
            {
               errorflag = 1;
            }
            if (fmtSATDisc( (RTL_IDISC *)Recbuf[ i ], itemseq, actual_discseq, &disc_uom[ discseq ] ) == FALSE)
               rv = -1;

            if (errorflag)
                setErrorSATDisc();
            break;

         case RTLRT_IGTAX:
            ++nigtax;
            ++igtaxseq;
            ++actual_igtaxseq;
            switch( CurTrat )
            {
               case TRATTT_ERR:
                  break;
               case TRATTT_OPEN:
                  WrBadTranError( OPEN_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_NOSALE:
                  WrBadTranError( NOSALE_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_VOID:
                  break;
               case TRATTT_PVOID:
                  WrBadTranError( PVOID_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_SALE:
                  break;
               case TRATTT_RETURN:
                  break;
               case TRATTT_EEXCH:
                  break;
               case TRATTT_SPLORD:
                  break;
               case TRATTT_PAIDIN:
                  WrBadTranError( PAIDIN_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PAIDOU:
                  if (CurTras != TRASTT_CACCOM)
                  {
                     WrBadTranError( PAIDOU_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                     SART_IGTAX, "", 0 );
                     errorflag = 1;
                  }
                  break;
               case TRATTT_PULL:
                  WrBadTranError( PULL_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_LOAN:
                  WrBadTranError( LOAN_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_COND:
                  WrBadTranError( COND_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_CLOSE:
                  WrBadTranError( CLOSE_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TOTAL:
                  WrBadTranError( TOTAL_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_REFUND:
                  WrBadTranError( REFUND_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_METER:
                  WrBadTranError( METER_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PUMPT:
                  WrBadTranError( PUMPT_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TANKDP:
                  WrBadTranError( TANKDP_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_DCLOSE:
                  WrBadTranError( DCLOSE_NO_IGTAX, TranSeqNo, itemseq, actual_igtaxseq,
                                  SART_IGTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_OTHER:
                  break;
               default:
                  WrBadTranErrorFile( TRAN_NOT_RECOGNIZED, function,
                                      (char *)Recbuf[ i ],
                                      ((char *)(Recbuf[ i ]) + LEN_FREC_DESC) );
                  rv = -1;
                  break;
            }
            if (rv != 0)
            {
               abortFmt();
               return( rv );
            }
             
            if (!mvSAIGtax( (RTL_IGTAX *)Recbuf[ i ], itemseq, actual_igtaxseq, igtax_seq, &igtax_tia[ igtaxseq ] , Location))
            {
               errorflag = 1;
            }

            if (fmtSAIGtax( (RTL_IGTAX *)Recbuf[ i ], itemseq, actual_igtaxseq, &igtax_tia[ igtaxseq ] ) == FALSE)
               rv = -1;

            if (errorflag)
                setErrorSAIGtax();

            strcpy(igtax_ind, "Y");
            ++igtax_seq;
            break;

         case RTLRT_TTAX:
            ++nttax;
            ++taxseq;
            switch( CurTrat )
            {
               case TRATTT_ERR:
                  break;
               case TRATTT_OPEN:
                  WrBadTranError( OPEN_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_NOSALE:
                  WrBadTranError( NOSALE_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_VOID:
                  break;
               case TRATTT_PVOID:
                  WrBadTranError( PVOID_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_SALE:
                  break;
               case TRATTT_RETURN:
                  break;
               case TRATTT_EEXCH:
                  break;
               case TRATTT_SPLORD:
                  break;
               case TRATTT_PAIDIN:
                  WrBadTranError( PAIDIN_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PAIDOU:
                  if (CurTras != TRASTT_CACCOM)
                  {
                     WrBadTranError( PAIDOU_NO_TAX, TranSeqNo, taxseq, -1,
                                     SART_TTAX, "", 0 );
                     errorflag = 1;
                  }
                  break;
               case TRATTT_PULL:
                  WrBadTranError( PULL_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_LOAN:
                  WrBadTranError( LOAN_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_COND:
                  WrBadTranError( COND_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_CLOSE:
                  WrBadTranError( CLOSE_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TOTAL:
                  WrBadTranError( TOTAL_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_REFUND:
                  WrBadTranError( REFUND_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_METER:
                  WrBadTranError( METER_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PUMPT:
                  WrBadTranError( PUMPT_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TANKDP:
                  WrBadTranError( TANKDP_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_DCLOSE:
                  WrBadTranError( DCLOSE_NO_TAX, TranSeqNo, taxseq, -1,
                                  SART_TTAX, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_OTHER:
                  break;
               default:
                  WrBadTranErrorFile( TRAN_NOT_RECOGNIZED, function,
                                      (char *)Recbuf[ i ],
                                      ((char *)(Recbuf[ i ]) + LEN_FREC_DESC) );
                  rv = -1;
                  break;
            }
            if (rv != 0)
            {
               abortFmt();
               return( rv );
            }

            if (!mvSATTax( (RTL_TTAX  *)Recbuf[ i ], taxseq, Location ))
            {
               errorflag = 1;
            }
            if (fmtSATTax( (RTL_TTAX  *)Recbuf[ i ], taxseq ) == FALSE)
               rv = -1;

            if (errorflag)
               setErrorSATTax();
            break;

         case RTLRT_TPYMT:
            ++ntpymt;
            ++pymtseq;
            switch( CurTrat )
            {
               case TRATTT_ERR:
                  break;
               case TRATTT_OPEN:
                  WrBadTranError( OPEN_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_NOSALE:
                  WrBadTranError( NOSALE_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_VOID:
                  WrBadTranError( VOID_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PVOID:
                  WrBadTranError( PVOID_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_SALE:
                  break;
               case TRATTT_RETURN:
                  WrBadTranError( RETURN_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_EEXCH:
                  WrBadTranError( EEXCH_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_SPLORD:
                  break;
               case TRATTT_PAIDIN:
                  WrBadTranError( PAIDIN_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PAIDOU:
                  WrBadTranError( PAIDOU_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PULL:
                  WrBadTranError( PULL_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_LOAN:
                  WrBadTranError( LOAN_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_COND:
                  WrBadTranError( COND_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_CLOSE:
                  WrBadTranError( CLOSE_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TOTAL:
                  WrBadTranError( TOTAL_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_REFUND:
                  WrBadTranError( REFUND_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_METER:
                  WrBadTranError( METER_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PUMPT:
                  WrBadTranError( PUMPT_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TANKDP:
                  WrBadTranError( TANKDP_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_DCLOSE:
                  WrBadTranError( DCLOSE_NO_PYMT, TranSeqNo, pymtseq, -1,
                                  SART_TPYMT, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_OTHER:
                  break;
               default:
                  WrBadTranErrorFile( TRAN_NOT_RECOGNIZED, function,
                                      (char *)Recbuf[ i ],
                                      ((char *)(Recbuf[ i ]) + LEN_FREC_DESC) );
                  rv = -1;
                  break;
            }
            if (rv != 0)
            {
               abortFmt();
               return( rv );
            }

            /* Before processing payment record, validate for duplicate igtax codes in igtax  */
            /* records if any for previous item. For pack items igtax code and component item */
            /* combination should be unique. */
            if ( strcmp( igtax_ind, "Y") == 0 )
            {
               if (!mvSAIGTaxVal( item_seq, igtax_seq ) )
                  igtaxerrorflag = 1;

               /* rest the igtax_ind to "N" for next transaction-item validation */
               strncpy(igtax_ind, "N", LEN_IND);
            }
            if (!mvSATPymt( (RTL_TPYMT  *)Recbuf[ i ], pymtseq ))
            {
               errorflag = 1;
            }
            if (fmtSATPymt( (RTL_TPYMT  *)Recbuf[ i ], pymtseq ) == FALSE)
               rv = -1;

            if (igtaxerrorflag)
               setErrorSATItem();

            if (errorflag)
               setErrorSATPymt();
            break;

         case RTLRT_TTEND:
            ++nttend;
            ++tendseq;
            switch( CurTrat )
            {
               case TRATTT_ERR:
                  break;
               case TRATTT_OPEN:
                  WrBadTranError( OPEN_NO_TEND, TranSeqNo, tendseq, -1,
                                  SART_TTEND, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_NOSALE:
                  break;
               case TRATTT_VOID:
                  break;
               case TRATTT_PVOID:
                  WrBadTranError( PVOID_NO_TEND, TranSeqNo, tendseq, -1,
                                  SART_TTEND, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_SALE:
                  break;
               case TRATTT_RETURN:
                  break;
               case TRATTT_SPLORD:
                  break;
               case TRATTT_PAIDIN:
                  break;
               case TRATTT_PAIDOU:
                  break;
               case TRATTT_PULL:
                  break;
               case TRATTT_LOAN:
                  break;
               case TRATTT_COND:
                  WrBadTranError( COND_NO_TEND, TranSeqNo, tendseq, -1,
                                  SART_TTEND, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_CLOSE:
                  WrBadTranError( CLOSE_NO_TEND, TranSeqNo, tendseq, -1,
                                  SART_TTEND, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TOTAL:
                  WrBadTranError( TOTAL_NO_TEND, TranSeqNo, tendseq, -1,
                                  SART_TTEND, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_REFUND:
                  break;
               case TRATTT_METER:
                  WrBadTranError( METER_NO_TEND, TranSeqNo, tendseq, -1,
                                  SART_TTEND, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_PUMPT:
                  WrBadTranError( PUMPT_NO_TEND, TranSeqNo, tendseq, -1,
                                  SART_TTEND, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_TANKDP:
                  WrBadTranError( TANKDP_NO_TEND, TranSeqNo, tendseq, -1,
                                  SART_TTEND, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_DCLOSE:
                  WrBadTranError( DCLOSE_NO_TEND, TranSeqNo, tendseq, -1,
                                  SART_TTEND, "", 0 );
                  errorflag = 1;
                  break;
               case TRATTT_OTHER:
                  break;
               default:
                  WrBadTranErrorFile( TRAN_NOT_RECOGNIZED, function,
                                      (char *)Recbuf[ i ],
                                      ((char *)(Recbuf[ i ]) + LEN_FREC_DESC) );
                  rv = -1;
                  break;
            }
            if (rv != 0)
            {
               abortFmt();
               return( rv );
            }

            /* Before processing tender record, validate for duplicate igtax codes in igtax  */
            /* records if any for previous item. For pack items igtax code and component item */
            /* combination should be unique. */
            if ( strcmp( igtax_ind, "Y") == 0 )
            {
               if (!mvSAIGTaxVal( item_seq, igtax_seq ) )
                  igtaxerrorflag = 1;

               /* reset the igtax_ind to "N" for next transaction-item validation */
               strncpy(igtax_ind, "N", LEN_IND);
            }
            if (!mvSATTend( (RTL_TTEND *)Recbuf[ i ], tendseq ))
            {
               errorflag = 1;
            }
            if (fmtSATTend( (RTL_TTEND *)Recbuf[ i ], tendseq ) == FALSE)
               rv = -1;

             if (igtaxerrorflag)
               setErrorSATItem();

            if (errorflag)
               setErrorSATTend();
            break;

         default:
            WrBadTranErrorFile( TRAN_XTRA_REC, function, (char *)Recbuf[ i ],
                                ((char *)(Recbuf[ i ]) + LEN_FREC_DESC) );
            rv = -1;
            break;
      }
      if (rv == 0 && gi_non_fatal_err_flag != 0)
      {
         /** To cancel out the current transaction **/
         rv = 1;

         /** Resetting the flag for further use **/
         gi_non_fatal_err_flag = 0;
      }

      if (rv != 0)
      {
         abortFmt();
         return( rv );
      }
   }

   errorflag = 0;

   /* check for trantype specific format things */
   switch( CurTrat )
   {
      case TRATTT_ERR:
         break;

      case TRATTT_OPEN:
         break;

      case TRATTT_NOSALE:
         break;

      case TRATTT_VOID:
         break;

      case TRATTT_PVOID:
         break;

      case TRATTT_SALE:
         if (nttend == 0)
         {
            WrBadTranError( SALE_TENDER_REQ, TranSeqNo, -1, -1, SART_THEAD, "", 0 );
            errorflag = 1;
         }
         break;

      case TRATTT_RETURN:
         if (ntitem == 0)
         {
            WrBadTranError( RETURN_ITEM_REQ, TranSeqNo, -1, -1, SART_THEAD, "", 0 );
            errorflag = 1;
         }
         if (nttend == 0)
         {
            WrBadTranError( RETURN_TENDER_REQ, TranSeqNo, -1, -1, SART_THEAD, "", 0 );
            errorflag = 1;
         }
         break;

      case TRATTT_EEXCH:
         if (ntitem < 2)
         {
            WrBadTranError( EEXCH_ITEM_REQ, TranSeqNo, -1, -1, SART_THEAD, "", 0 );
            errorflag = 1;
         }
         break;

      case TRATTT_SPLORD:
         break;

      case TRATTT_PAIDIN:
         if ( nttend < 1 )
         {
            WrBadTranError( PAIDIN_TENDER_REQ, TranSeqNo, -1, -1, SART_THEAD, "", 0 );
            errorflag = 1;
         }
         break;

      case TRATTT_PAIDOU:
         if ( nttend < 1 )
         {
            WrBadTranError( PAIDOU_TENDER_REQ, TranSeqNo, -1, -1, SART_THEAD, "", 0 );
            errorflag = 1;
         }
         break;

      case TRATTT_PULL:
         if ( nttend < 1 )
         {
            WrBadTranError( PULL_TENDER_REQ, TranSeqNo, -1, -1, SART_THEAD, "", 0 );
            errorflag = 1;
         }
         break;

      case TRATTT_LOAN:
         if ( nttend < 1 )
         {
            WrBadTranError( LOAN_TENDER_REQ, TranSeqNo, -1, -1, SART_THEAD, "", 0 );
            errorflag = 1;
         }
         break;

      case TRATTT_COND:
         break;

      case TRATTT_CLOSE:
         break;

      case TRATTT_TOTAL:
         break;

      case TRATTT_REFUND:
         if ( nttend < 1 )
         {
            WrBadTranError( REFUND_TENDER_REQ, TranSeqNo, -1, -1, SART_THEAD, "", 0 );
            errorflag = 1;
         }
         break;

      case TRATTT_METER:
         if ( ntitem < 1 )
         {
            WrBadTranError( METER_ITEM_REQ, TranSeqNo, -1, -1, SART_THEAD, "", 0 );
            errorflag = 1;
         }
         break;

      case TRATTT_PUMPT:
         if ( ntitem < 1 )
         {
            WrBadTranError( PUMPT_ITEM_REQ, TranSeqNo, -1, -1, SART_THEAD, "", 0 );
            errorflag = 1;
         }
         break;

      case TRATTT_TANKDP:
         if ( ntitem < 1 )
         {
            WrBadTranError( TANKDP_ITEM_REQ, TranSeqNo, -1, -1, SART_THEAD, "", 0 );
            errorflag = 1;
         }
         break;

      case TRATTT_DCLOSE:
         break;

      case TRATTT_OTHER:
         break;

      case TRATTT_TERM:
         /* An error was written earlier about this -- no need to write two error messages */
         rv = -1;
         break;

      default:
         VERBOSE( "Unknown Transaction Type (TRAT)\n" );
         WrBadTranErrorFile( TRAN_NOT_RECOGNIZED, function,
                             (char *)Recbuf[ 0 ],
                             ((char *)(Recbuf[ 0 ]) + LEN_FREC_DESC) );
         rv = -1;
         break;
   }

   if (errorflag)
   {
      setErrorSATHead();
   }
   if (rv == 0 && gi_non_fatal_err_flag != 0)
   {
      WRITE_ERROR( RET_FUNCTION_ERR, function, "inside reformat cancel", " ");

      /** To cancel out the current transaction **/
      rv = 1;

      /** Resetting the flag for further use **/
      gi_non_fatal_err_flag = 0;
   }


   if (rv != 0)
      abortFmt();
   pi_tender_ref_no9 = 0;
   return( rv );
}


/*-------------------------------------------------------------------------
WrBadTran( stop ) is called when a transaction is bad enough to be sent
to its room. Stop is the index to stop dumping at (non-inclusive, so
record at stop is *not* dumped).
-------------------------------------------------------------------------*/
static void WrBadTran( int stop )
{
   int i;
   char *cp;

   VERBOSE( "INFO: Writing to bad transaction file\n" );
   /* just dump all records until stop or end */
   for ( i = 0; i < stop && Rectype[ i ] != RTLRT_END; ++i )
   {
      for ( cp = Recbuf[ i ]; *cp; ++cp )
      {
         putc( *cp, BadTranFile );
         if ( *cp == '\n' )
            break;
      }
   }

#ifdef BAD_TRAN_SEPARATOR_IN_FILE
   /* add blank line for viewing convenience */
   putc( '\n', BadTranFile );
#endif
}


/*-------------------------------------------------------------------------
Write an error to the error log file after looking up the error description
and recommended solution.
We do not expect any "special" SART codes here, i.e. those that are 6 chars long
-------------------------------------------------------------------------*/
void WrBadTranErrorFile(
   const char *err,
   char       *func,
   const char *rec_type,
   char       *line_no)
{
   ERRORDAT *e;
   char efmt[] = "%s on %-*.*s record at line %-*.*s in %s - %s - %s";
   char fmt[] = "%s on %-*.*s record at line %-*.*s in %s";
   char emsg[ sizeof( efmt) + sizeof( fmt) +
              LEN_ERROR_CODE +
              LEN_FREC_DESC +
              LEN_FILE_LINE_NO +
              FILENAME_MAX +
              2 * LEN_ERROR_TEXT + 1 ];

   e = error_lookup( err);
   if (e != NULL)
      sprintf( emsg, efmt, err,
               LEN_FREC_DESC, LEN_FREC_DESC, rec_type,
               LEN_FILE_LINE_NO, LEN_FILE_LINE_NO,
               ((line_no != NULL) ? line_no : "UNKNOWN"),
               InFile, e->error_desc, e->rec_solution);
   else
      sprintf( emsg, fmt, err,
               LEN_FREC_DESC, LEN_FREC_DESC, rec_type,
               LEN_FILE_LINE_NO, LEN_FILE_LINE_NO,
               ((line_no != NULL) ? line_no : "UNKNOWN"),
               InFile);

   WRITE_ERROR( RET_FUNCTION_ERR, func, "", emsg);
   gi_rej_record = TRUE;
}


/*-------------------------------------------------------------------------
decdate() decrements the date string in datestr the specified number of
days, and puts the result in datestr. Returns 0 if ok or 1 if problem.
-------------------------------------------------------------------------*/
static int decdate( char *datestr, int decdays )
{
     static int monlen[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
     int year, mon, day;

     /* Get starting date, validate, and set February length */
     year = atoin( &datestr[ 0 ], 4 );
     if ( year < 1900 || year > 2100 )
          return( 1 );
     if ( isleapyear( year ) )
          monlen[ 2 ] = 29;
     else monlen[ 2 ] = 28;
     mon = atoin( &datestr[ 4 ], 2 );
     if ( mon < 1 || mon > 12 )
          return( 1 );
     day = atoin( &datestr[ 6 ], 2 );
     if ( day < 1 || day > monlen[ mon ] )
          return( 1 );

     /* Decrement date the specified number of days */
     /* I realize this is brute force and inelegant, but this */
     /* is really a pain in the rear what with leap years and */
     /* not knowing the max possible decdays. */
     /* It is only done once per data file so no big deal. */
     while ( decdays-- )
          if ( --day == 0 )
          {
               if ( --mon == 0 )
               {
                    --year;
                    mon = 12;
                    if ( isleapyear( year ) )
                         monlen[ 2 ] = 29;
                    else monlen[ 2 ] = 28;
               }
               day = monlen[ mon ];
          }

     /* format into datestr */
     sprintf( datestr, "%04d%02d%02d", year, mon, day );

     /* must be good */
     return( 0 );
}


/*-------------------------------------------------------------------------
Compare a fixed length string with a \0 terminated string.
Trailing blanks are ignored.
-------------------------------------------------------------------------*/
int cmpfxlstr( const char *s1, unsigned int l1, const char *s2, unsigned int l2)
{
   int i;

   if (l1 == l2)
   {
      if (memcmp( s1, s2, l1) != 0)
         return( FALSE );
   }
   else if (l1 > l2)
   {
      if (memcmp( s1, s2, l2) != 0)
         return( FALSE );
      for (i = l2; i < l1; i++)
         if ((s1[i] != ' ') && (s1[i] != '\0'))
            return( FALSE );
   }
   else
      return( FALSE );

   return( TRUE );
}

/*-------------------------------------------------------------------------
Convert a number (stored in a string) with a SIGN indicator to a string
that has a leading - if appropriate and with 0 padding on the left.
A + is never added - it is always assumed.
-------------------------------------------------------------------------*/
static void num2str( char *str, int lstr, char *num, int lnum, const char *sign)
{
   fldcpy( str, lstr - 1, num, lnum, FT_NUMBER);
   str[ lstr - 1 ] = '\0';
   if (strncmp( sign, SIGN_N, LEN_SALES_SIGN) == 0)
      str[0] = '-';
}

/* This function, can_len_mask_val(), is used to find true length of Checking Account Number
   and check the masked digit in the Checking Account Number */
/*--------------------------------------------------------------------------*/
int can_len_mask_val( char *is_can_no, char *is_can_msk_char )
{
   int  li_len, li_num, mask_len=0;
   char ls_can_mask[LEN_CHECK_ACCT_NO];
   char ls_can_mask_tmp[LEN_CHECK_ACCT_NO];

   for(li_len = LEN_CHECK_ACCT_NO - 1; li_len >= 0; --li_len )

      if ( is_can_no[ li_len ] != ' ' )
         break;

   li_len = li_len + 1;

   /* check for a masked digit in the CC number*/
   strncpy(ls_can_mask, is_can_no, li_len );
   nullpad(ls_can_mask, (li_len + LEN_IND) );

   for(li_num=0; li_num < li_len; li_num++)
   {
      strncpy(ls_can_mask_tmp,&ls_can_mask[li_num],LEN_IND);
      nullpad(ls_can_mask_tmp,NULL_IND);
      if(strcmp( ls_can_mask_tmp, is_can_msk_char ) == 0 )
         mask_len = mask_len + 1;
   }

   if (mask_len < 2)
      return(1);
   else
      return(0);

} /* end of can_len_mask_val */
