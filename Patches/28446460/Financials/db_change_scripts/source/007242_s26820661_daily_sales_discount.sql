--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'DAILY_SALES_DISCOUNT'
ALTER TABLE DAILY_SALES_DISCOUNT MODIFY PROM_COMPONENT NULL
/

ALTER TABLE DAILY_SALES_DISCOUNT MODIFY PROM_COMPONENT DEFAULT NULL
/


PROMPT DROPPING Primary Key on 'DAILY_SALES_DISCOUNT'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'PK_DAILY_SALES_DISCOUNT'
     AND CONSTRAINT_TYPE = 'P';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE DAILY_SALES_DISCOUNT DROP CONSTRAINT PK_DAILY_SALES_DISCOUNT DROP INDEX';
  end if;
end;
/


PROMPT Creating UNIQUE KEY CONSTRAINT on 'DAILY_SALES_DISCOUNT'
DECLARE
L_part_table_exists number := 0;
L_part_index_exists number := 0;
L_table_exists number :=0;
L_index_exists number :=0;
BEGIN
SELECT count(*) INTO L_part_table_exists
FROM user_part_tables
WHERE table_name = 'DAILY_SALES_DISCOUNT';

SELECT count(*) INTO L_index_exists FROM user_indexes
WHERE INDEX_NAME = 'UK_DAILY_SALES_DISCOUNT';


if (L_part_table_exists = 0 and L_index_exists !=0) then      
execute immediate 'ALTER TABLE DAILY_SALES_DISCOUNT DROP CONSTRAINT UK_DAILY_SALES_DISCOUNT DROP INDEX';
execute immediate 'ALTER TABLE DAILY_SALES_DISCOUNT ADD (CONSTRAINT UK_DAILY_SALES_DISCOUNT UNIQUE( ITEM,STORE, PROM_TYPE, PROMOTION,PROM_COMPONENT, TRAN_TYPE, DATA_DATE) USING INDEX INITRANS 12 TABLESPACE RETAIL_INDEX)';

elsif (L_part_table_exists = 0 and L_index_exists = 0) then   
execute immediate 'ALTER TABLE DAILY_SALES_DISCOUNT ADD (CONSTRAINT UK_DAILY_SALES_DISCOUNT UNIQUE( ITEM,STORE, PROM_TYPE, PROMOTION,PROM_COMPONENT, TRAN_TYPE, DATA_DATE) USING INDEX INITRANS 12 TABLESPACE RETAIL_INDEX)';

elsif (L_part_table_exists != 0 and L_index_exists !=0) then  
execute immediate 'ALTER TABLE DAILY_SALES_DISCOUNT DROP CONSTRAINT UK_DAILY_SALES_DISCOUNT DROP INDEX';
execute immediate 'ALTER TABLE DAILY_SALES_DISCOUNT ADD (CONSTRAINT UK_DAILY_SALES_DISCOUNT UNIQUE( ITEM,STORE, PROM_TYPE, PROMOTION,PROM_COMPONENT, TRAN_TYPE, DATA_DATE) USING INDEX LOCAL INITRANS 12 TABLESPACE RETAIL_INDEX)';

elsif (L_part_table_exists != 0 and L_index_exists = 0) then  
execute immediate 'ALTER TABLE DAILY_SALES_DISCOUNT ADD (CONSTRAINT UK_DAILY_SALES_DISCOUNT UNIQUE( ITEM,STORE, PROM_TYPE, PROMOTION,PROM_COMPONENT, TRAN_TYPE, DATA_DATE) USING INDEX LOCAL INITRANS 12 TABLESPACE RETAIL_INDEX)';


end if;
end;
/

PROMPT Modifying Table column descriptions 'DAILY_SALES_DISCOUNT'
COMMENT ON COLUMN DAILY_SALES_DISCOUNT.EXPECTED_RETAIL is 'For off retail records (type 1006), this field contains the retail at which RMS expected the item to be sold, inclusive of discounts sent with the transaction. The value will include TAX unless the default tax type is set to SALES or the default tax type is set to SVAT and the class_level_vat_ind set at the system level is ''Y'' and class_vat_ind for the item''s class is ''N''.'
/

COMMENT ON COLUMN DAILY_SALES_DISCOUNT.ACTUAL_RETAIL is 'For off retail records (type 1006), this field contains the retail at which the item was actually sold, inclusive of discounts sent with the transaction. The value will include TAX unless the default tax type is set to SALES or the default tax type is set to SVAT and the class_level_vat_ind set at the system level is ''Y'' and class_vat_ind for the item''s class is ''N''.'
/

COMMENT ON COLUMN DAILY_SALES_DISCOUNT.SALES_RETAIL is 'This field contains the retail amount of the items sold in the transaction with the discount, using the sales value sent in the detail line of the sales transaction (TDETL). The value will include TAX unless the default tax type is set to SALES or the default tax type is set to SVAT and the class_level_vat_ind set at the system level is ''Y'' and class_vat_ind for the item''s class is ''N''.'
/

COMMENT ON COLUMN DAILY_SALES_DISCOUNT.GROSS_PROFIT_AMT is 'This field is captured for each transaction detail sent in the sales transaction, as well as for off-retail (type 1006), when required. It is calculated as sales retail - cost of sales, where sales retail is the SALES_RETAIL value in this record and cost of sales is either the item''s current average cost or unit cost depending on system option std_av_ind. The value will include TAX unless the default tax type is set to SALES or the default tax type is set to SVAT and the class_level_vat_ind set at the system level is ''Y'' and class_vat_ind for the item''s class is ''N''.'
/

COMMENT ON COLUMN DAILY_SALES_DISCOUNT.PROM_TYPE is 'This field indicates the type of promotion that was taken for the transaction.  Valid values are stored in the codes table with a code type of PRMT. Additionally, it can be 1006 to indicate the difference between expected retail and actual retail (i.e. the sales retail inclusive of discounts).'
/
