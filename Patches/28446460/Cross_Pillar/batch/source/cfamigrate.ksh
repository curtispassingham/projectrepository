#! /bin/ksh

#-------------------------------------------------------------------------------
#  File: cfamigrate.ksh
#
#  Desc: This shell script will create a file with CFA metadata to copy it from
#         env to another.
#-------------------------------------------------------------------------------

pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date
stmpDate=`date`           # Today's date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate
FATAL=255
NON_FATAL=1
OK=0
TRUE=1
FALSE=0


USAGE="Usage: $pgmName.$pgmExt <connect> <Level L for all, E for entity, S for group set, G for group, A for attribute, C for codes or R for rec group> <Entity/Group Set/Group/Attribute/Code/Rec Group ID>"

function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE

   return $retCode
}

function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi

   return $retCode
}

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set termout off;
      set verify off;
      set echo off
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL}
      return ${FATAL}
   fi

   return ${OK}
}


function COPY_CODES
{
  sqlplus -s $connectStr  <<EOF>> $fileName
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 150
SET VERIFY OFF
SET FEEDBACK OFF
SET SERVEROUTPUT ON

DECLARE

   cursor C_GET_CODE_HEAD is
      select CODE_TYPE,
             CODE_TYPE_DESC             
        from code_head
       where CODE_TYPE IN (SELECT DISTINCT CODE_TYPE FROM CFA_ATTRIB WHERE CODE_TYPE= DECODE('${levelId}', 'ALL', CODE_TYPE, '${levelId}'));

   cursor C_GET_CODE_DETAIL is
      select CODE_TYPE,
             CODE,             
             CODE_DESC,
             REQUIRED_IND,
             CODE_SEQ             
        from CODE_DETAIL
       where CODE_TYPE IN (SELECT DISTINCT CODE_TYPE FROM CFA_ATTRIB WHERE CODE_TYPE= DECODE('${levelId}', 'ALL', CODE_TYPE, '${levelId}'));


BEGIN
   ---
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('---------------------Insert/Update CODE_HEAD------------------------------');

   For i IN C_GET_CODE_HEAD LOOP
      DBMS_OUTPUT.PUT_LINE('merge into code_head ch using ( select');
      DBMS_OUTPUT.PUT_LINE(''''||i.CODE_TYPE||''' CODE_TYPE, '''||i.CODE_TYPE_DESC||''' CODE_TYPE_DESC');
      DBMS_OUTPUT.PUT_LINE('from dual) use_this');
      DBMS_OUTPUT.PUT_LINE('on (ch.CODE_TYPE = use_this.CODE_TYPE)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set ch.CODE_TYPE_DESC = use_this.CODE_TYPE_DESC');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert ( CODE_TYPE, CODE_TYPE_DESC)');
      DBMS_OUTPUT.PUT_LINE('values (use_this.CODE_TYPE, use_this.CODE_TYPE_DESC);');
      DBMS_OUTPUT.PUT_LINE('---------------------');
   END LOOP;

   DBMS_OUTPUT.PUT_LINE('------------------Insert/Update CODE_DETAIL-------------------------');

   FOR x IN C_GET_CODE_DETAIL LOOP
      DBMS_OUTPUT.PUT_LINE('merge into code_detail cd using ( select');
      DBMS_OUTPUT.PUT_LINE(''''||x.CODE_TYPE||''' CODE_TYPE, '''||x.CODE||''' CODE, '''||
        x.CODE_DESC||''' CODE_DESC, '''||x.REQUIRED_IND||''' REQUIRED_IND, '''||x.CODE_SEQ||''' CODE_SEQ');
      DBMS_OUTPUT.PUT_LINE('from dual) use_this');
      DBMS_OUTPUT.PUT_LINE('on (cd.CODE_TYPE = use_this.CODE_TYPE and cd.CODE = use_this.CODE)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set 
        cd.CODE_DESC = use_this.CODE_DESC, cd.REQUIRED_IND = use_this.REQUIRED_IND, cd.CODE_SEQ = use_this.CODE_SEQ');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert ( CODE_TYPE, CODE,  CODE_DESC, REQUIRED_IND, CODE_SEQ)');
      DBMS_OUTPUT.PUT_LINE('values (use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, use_this.REQUIRED_IND,
        use_this.CODE_SEQ);');
      DBMS_OUTPUT.PUT_LINE('---------------------');
   END LOOP;

END;
/
EOF

  if [[ $? -ne ${OK} ]]; then
      echo "Failed in COPY_CODES" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi

}


function COPY_REC_GRP
{
  sqlplus -s $connectStr  <<EOF>> $fileName
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 150
SET VERIFY OFF
SET FEEDBACK OFF
SET SERVEROUTPUT ON

DECLARE

   cursor C_GET_REC_GRP is
      select REC_GROUP_ID,
             REC_GROUP_NAME,
             replace(QUERY,chr(39),chr(39)||chr(39)) QUERY,
             BASE_IND,
             QUERY_TYPE,
             TABLE_NAME,
             COLUMN_1,
             COLUMN_2,
             WHERE_COL_1,
             WHERE_OPERATOR_1,
             WHERE_COND_1,
             WHERE_COL_2,
             WHERE_OPERATOR_2,
             WHERE_COND_2,
             COL_1_DATA_TYPE,
             COL_1_DATA_LENGTH 
        from CFA_REC_GROUP
       where REC_GROUP_ID = DECODE('${levelId}', 'ALL', REC_GROUP_ID, '${levelId}');

   cursor C_GET_REC_GRP_LB is
      select rl.REC_GROUP_ID, 
             LANG, 
             DEFAULT_LANG_IND, 
             LOV_TITLE, 
             LOV_COL1_HEADER, 
             LOV_COL2_HEADER,
             rg.REC_GROUP_NAME
        from CFA_REC_GROUP_LABELS rl,
             CFA_REC_GROUP rg
       where rg.REC_GROUP_ID = DECODE('${levelId}', 'ALL', rg.REC_GROUP_ID, '${levelId}')
         and rg.REC_GROUP_ID = rl.REC_GROUP_ID;


BEGIN
   ---
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('---------------------Insert/Update CFA_REC_GROUP------------------------------');


   For i IN C_GET_REC_GRP LOOP
      DBMS_OUTPUT.PUT_LINE('merge into cfa_rec_group rg using ( select');
      DBMS_OUTPUT.PUT_LINE(''''||i.REC_GROUP_NAME||''' REC_GROUP_NAME, '''|| i.QUERY||''' QUERY, '''||i.BASE_IND||''' BASE_IND, '''||
        i.QUERY_TYPE||''' QUERY_TYPE, '''||i.TABLE_NAME||''' TABLE_NAME, '''||i.COLUMN_1||''' COLUMN_1, '''||i.COLUMN_2||''' COLUMN_2, '''||
        i.WHERE_COL_1||''' WHERE_COL_1, '''||i.WHERE_OPERATOR_1||''' WHERE_OPERATOR_1, '''||i.WHERE_COND_1||''' WHERE_COND_1, '''||
        i.WHERE_COL_2||''' WHERE_COL_2, '''||i.WHERE_OPERATOR_2||''' WHERE_OPERATOR_2, '''||i.WHERE_COND_2||''' WHERE_COND_2, '''||
        i.COL_1_DATA_TYPE||''' COL_1_DATA_TYPE, '''||i.COL_1_DATA_LENGTH||''' COL_1_DATA_LENGTH');
      DBMS_OUTPUT.PUT_LINE('from dual) use_this');
      DBMS_OUTPUT.PUT_LINE('on (rg.REC_GROUP_NAME = use_this.REC_GROUP_NAME)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set rg.QUERY = use_this.QUERY, rg.BASE_IND = use_this.BASE_IND, rg.QUERY_TYPE = use_this.QUERY_TYPE,
        rg.TABLE_NAME = use_this.TABLE_NAME, rg.COLUMN_1 = use_this.COLUMN_1, rg.COLUMN_2 = use_this.COLUMN_2,
        rg.WHERE_COL_1 = use_this.WHERE_COL_1, rg.WHERE_OPERATOR_1 = use_this.WHERE_OPERATOR_1, rg.WHERE_COND_1 = use_this.WHERE_COND_1,
        rg.WHERE_COL_2 = use_this.WHERE_COL_2, rg.WHERE_OPERATOR_2 = use_this.WHERE_OPERATOR_2, rg.WHERE_COND_2 = use_this.WHERE_COND_2,
        rg.COL_1_DATA_TYPE = use_this.COL_1_DATA_TYPE, rg.COL_1_DATA_LENGTH = use_this.COL_1_DATA_LENGTH');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert ( REC_GROUP_ID, REC_GROUP_NAME, QUERY, BASE_IND, QUERY_TYPE, TABLE_NAME, COLUMN_1,
        COLUMN_2, WHERE_COL_1, WHERE_OPERATOR_1, WHERE_COND_1, WHERE_COL_2, WHERE_OPERATOR_2, WHERE_COND_2, COL_1_DATA_TYPE, COL_1_DATA_LENGTH)');
      DBMS_OUTPUT.PUT_LINE('values (CFA_REC_GROUP_SEQ.NEXTVAL, use_this.REC_GROUP_NAME, use_this.QUERY, use_this.BASE_IND, use_this.QUERY_TYPE,
        use_this.TABLE_NAME, use_this.COLUMN_1, use_this.COLUMN_2, use_this.WHERE_COL_1, use_this.WHERE_OPERATOR_1, use_this.WHERE_COND_1,
        use_this.WHERE_COL_2, use_this.WHERE_OPERATOR_2, use_this.WHERE_COND_2, use_this.COL_1_DATA_TYPE, use_this.COL_1_DATA_LENGTH);');
      DBMS_OUTPUT.PUT_LINE('---------------------');
   END LOOP;

   DBMS_OUTPUT.PUT_LINE('------------------Insert/Update CFA_REC_GROUP_LABELS-------------------------');

   FOR x IN C_GET_REC_GRP_LB LOOP
      DBMS_OUTPUT.PUT_LINE('merge into cfa_rec_group_labels rl using ( select REC_GROUP_ID,');
      DBMS_OUTPUT.PUT_LINE(''''||x.DEFAULT_LANG_IND||''' DEFAULT_LANG_IND, '''||x.LANG||''' LANG, '''||
        x.LOV_TITLE||''' LOV_TITLE, '''||x.LOV_COL1_HEADER||''' LOV_COL1_HEADER, '''||x.LOV_COL2_HEADER||''' LOV_COL2_HEADER');
      DBMS_OUTPUT.PUT_LINE('from cfa_rec_group where REC_GROUP_NAME = '''||x.REC_GROUP_NAME||''') use_this');
      DBMS_OUTPUT.PUT_LINE('on (rl.REC_GROUP_ID = use_this.REC_GROUP_ID)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set rl.LANG = use_this.LANG, rl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND,
        rl.LOV_TITLE = use_this.LOV_TITLE, rl.LOV_COL1_HEADER = use_this.LOV_COL1_HEADER, rl.LOV_COL2_HEADER = use_this.LOV_COL2_HEADER');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert ( REC_GROUP_ID, LANG, DEFAULT_LANG_IND, LOV_TITLE, LOV_COL1_HEADER, LOV_COL2_HEADER)');
      DBMS_OUTPUT.PUT_LINE('values (use_this.REC_GROUP_ID, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.LOV_TITLE, use_this.LOV_COL1_HEADER, use_this.LOV_COL2_HEADER);');
      DBMS_OUTPUT.PUT_LINE('---------------------');
   END LOOP;

END;
/
EOF

  if [[ $? -ne ${OK} ]]; then
      echo "Failed in COPY_REC_GRP" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi

}


function COPY_ENTITY
{
  sqlplus -s $connectStr  <<EOF>> $fileName
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 150
SET VERIFY OFF
SET FEEDBACK OFF
SET SERVEROUTPUT ON

DECLARE

   cursor C_GET_EXT is
      select EXT_ENTITY_ID, 
             BASE_RMS_TABLE, 
             CUSTOM_EXT_TABLE, 
             ACTIVE_IND, 
             BASE_IND, 
             VALIDATION_FUNC
        from CFA_EXT_ENTITY
       where EXT_ENTITY_ID = DECODE('${levelId}', 'ALL', EXT_ENTITY_ID, '${levelId}');

   cursor C_GET_EXT_KEY is
      select k.BASE_RMS_TABLE, 
             KEY_COL, 
             KEY_NUMBER, 
             DATA_TYPE, 
             DESCRIPTION_CODE
        from CFA_EXT_ENTITY_KEY k,
             CFA_EXT_ENTITY e
       where e.EXT_ENTITY_ID = DECODE('${levelId}', 'ALL', e.EXT_ENTITY_ID, '${levelId}')
         and e.BASE_RMS_TABLE = k.BASE_RMS_TABLE;

   cursor C_GET_EXT_KEY_LABELS is
      select l.BASE_RMS_TABLE,
             KEY_COL,
             LANG,
             DEFAULT_LANG_IND,
             LABEL
        from CFA_EXT_ENTITY_KEY_LABELS l,
             CFA_EXT_ENTITY e
       where e.EXT_ENTITY_ID = DECODE('${levelId}', 'ALL', e.EXT_ENTITY_ID, '${levelId}')
         and e.BASE_RMS_TABLE = l.BASE_RMS_TABLE;

BEGIN
   ---
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('---------------------Insert/Update CFA_EXT_ENTITY-----------------------------');

   For i IN C_GET_EXT LOOP
      DBMS_OUTPUT.PUT_LINE('merge into cfa_ext_entity ext using ( select');
      DBMS_OUTPUT.PUT_LINE(''''||i.BASE_RMS_TABLE||''' BASE_RMS_TABLE, '''||i.CUSTOM_EXT_TABLE||''' CUSTOM_EXT_TABLE, '''||
        i.ACTIVE_IND||''' ACTIVE_IND, '''||i.BASE_IND||''' BASE_IND, '''||i.VALIDATION_FUNC||''' VALIDATION_FUNC');
      DBMS_OUTPUT.PUT_LINE('from dual) use_this');
      DBMS_OUTPUT.PUT_LINE('on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set ext.VALIDATION_FUNC = use_this.VALIDATION_FUNC');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, CUSTOM_EXT_TABLE, ACTIVE_IND, BASE_IND, VALIDATION_FUNC)');
      DBMS_OUTPUT.PUT_LINE('values (EXT_ENTITY_SEQ.NEXTVAL, use_this.BASE_RMS_TABLE, use_this.CUSTOM_EXT_TABLE, use_this.ACTIVE_IND,
        use_this.BASE_IND, use_this.VALIDATION_FUNC);');
      DBMS_OUTPUT.PUT_LINE('---------------------');
   END LOOP;

   DBMS_OUTPUT.PUT_LINE('-------------------Insert/Update CFA_EXT_ENTITY_KEY---------------------------');

   For x IN C_GET_EXT_KEY LOOP
      DBMS_OUTPUT.PUT_LINE('merge into cfa_ext_entity_key key using ( select');
      DBMS_OUTPUT.PUT_LINE(''''||x.BASE_RMS_TABLE||''' BASE_RMS_TABLE, '''||x.KEY_COL||''' KEY_COL, '||x.KEY_NUMBER||' KEY_NUMBER, '''||x.DATA_TYPE||
        '''  DATA_TYPE, '''||x.DESCRIPTION_CODE||''' DESCRIPTION_CODE');
      DBMS_OUTPUT.PUT_LINE('from dual) use_this');
      DBMS_OUTPUT.PUT_LINE('on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE)');
      DBMS_OUTPUT.PUT_LINE('values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE);');
      DBMS_OUTPUT.PUT_LINE('---------------------');
   END LOOP;


   DBMS_OUTPUT.PUT_LINE('---------------Insert/Update CFA_EXT_ENTITY_KEY_LABELS------------------------');

   For y IN C_GET_EXT_KEY_LABELS LOOP
      DBMS_OUTPUT.PUT_LINE('merge into cfa_ext_entity_key_labels kl using ( select');
      DBMS_OUTPUT.PUT_LINE(''''||y.BASE_RMS_TABLE||''' BASE_RMS_TABLE, '''||y.KEY_COL||''' KEY_COL, '||y.LANG||' LANG, '''||y.DEFAULT_LANG_IND||
        ''' DEFAULT_LANG_IND, '''||y.LABEL||''' LABEL');
      DBMS_OUTPUT.PUT_LINE('from dual ) use_this');
      DBMS_OUTPUT.PUT_LINE('on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.LABEL = use_this.LABEL');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, LABEL)');
      DBMS_OUTPUT.PUT_LINE('values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.LABEL);');
      DBMS_OUTPUT.PUT_LINE('---------------------');
   END LOOP;


END;
/
EOF

  if [[ $? -ne ${OK} ]]; then
      echo "Failed in COPY_ENTITY" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi

}


function COPY_GRP_SET
{
  sqlplus -s $connectStr  <<EOF>> $fileName
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 150
SET VERIFY OFF
SET FEEDBACK OFF
SET SERVEROUTPUT ON

DECLARE

   L_label CFA_ATTRIB_GROUP_SET_LABELS.LABEL%TYPE := NULL;

   cursor C_GET_GRP_SET is
      select gs.GROUP_SET_ID,
             e.EXT_ENTITY_ID,
             e.BASE_RMS_TABLE,
             gs.GROUP_SET_VIEW_NAME,
             gs.DISPLAY_SEQ,
             gs.QUALIFIER_FUNC,
             gs.DEFAULT_FUNC,
             gs.VALIDATION_FUNC,
             gs.STAGING_TABLE_NAME,
             gs.ACTIVE_IND,
             gs.BASE_IND
        from CFA_ATTRIB_GROUP_SET gs,
             CFA_EXT_ENTITY e
       where gs.EXT_ENTITY_ID = e.EXT_ENTITY_ID
         and gs.GROUP_SET_ID  = DECODE('${level}', 'S', '${levelId}', gs.GROUP_SET_ID)
         and gs.EXT_ENTITY_ID = DECODE('${level}', 'E', DECODE('${levelId}', 'ALL', gs.EXT_ENTITY_ID, '${levelId}'), gs.EXT_ENTITY_ID);

   cursor C_GET_GRP_SET_LABELS is
      select gsl.GROUP_SET_ID,
             e.BASE_RMS_TABLE,
             gs.GROUP_SET_VIEW_NAME,
             gsl.LANG,
             gsl.DEFAULT_LANG_IND,
             gsl.LABEL
        from CFA_ATTRIB_GROUP_SET_LABELS gsl,
             CFA_ATTRIB_GROUP_SET gs,
             CFA_EXT_ENTITY e
       where gsl.GROUP_SET_ID = gs.GROUP_SET_ID
         and gs.EXT_ENTITY_ID = e.EXT_ENTITY_ID
         and gs.GROUP_SET_ID  = DECODE('${level}', 'S', '${levelId}', gs.GROUP_SET_ID)
         and gs.EXT_ENTITY_ID = DECODE('${level}', 'E', DECODE('${levelId}', 'ALL', gs.EXT_ENTITY_ID, '${levelId}'), gs.EXT_ENTITY_ID);

BEGIN
   ---
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------Insert/Update CFA_ATTRIB_GROUP_SET--------------------------');

   For x IN C_GET_GRP_SET LOOP
      DBMS_OUTPUT.PUT_LINE('merge into cfa_attrib_group_set gs using ( select e.EXT_ENTITY_ID, ');
      DBMS_OUTPUT.PUT_LINE(''''||x.BASE_RMS_TABLE||''' BASE_RMS_TABLE, '''||x.GROUP_SET_VIEW_NAME||''' GROUP_SET_VIEW_NAME, '||x.DISPLAY_SEQ||
        ' DISPLAY_SEQ, '''||x.QUALIFIER_FUNC||''' QUALIFIER_FUNC, '''||x.DEFAULT_FUNC||''' DEFAULT_FUNC, '''||x.VALIDATION_FUNC||
        ''' VALIDATION_FUNC, '''||x.STAGING_TABLE_NAME||''' STAGING_TABLE_NAME, '''||'N'||''' ACTIVE_IND, '''||x.BASE_IND||''' BASE_IND');
      DBMS_OUTPUT.PUT_LINE('from cfa_ext_entity e where e.BASE_RMS_TABLE = '''||x.BASE_RMS_TABLE||''' ) use_this');
      DBMS_OUTPUT.PUT_LINE('on (gs.EXT_ENTITY_ID = use_this.EXT_ENTITY_ID and gs.GROUP_SET_VIEW_NAME = use_this.GROUP_SET_VIEW_NAME)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set gs.DISPLAY_SEQ = use_this.DISPLAY_SEQ, gs.QUALIFIER_FUNC = use_this.QUALIFIER_FUNC, gs.DEFAULT_FUNC = '||
        'use_this.DEFAULT_FUNC, gs.VALIDATION_FUNC = use_this.VALIDATION_FUNC');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert (GROUP_SET_ID, EXT_ENTITY_ID, GROUP_SET_VIEW_NAME, DISPLAY_SEQ, QUALIFIER_FUNC, DEFAULT_FUNC, '||
        'VALIDATION_FUNC, STAGING_TABLE_NAME, ACTIVE_IND, BASE_IND)');
      DBMS_OUTPUT.PUT_LINE('values (CFA_ATTRIB_GROUP_SET_SEQ.NEXTVAL, use_this.EXT_ENTITY_ID, use_this.GROUP_SET_VIEW_NAME, use_this.DISPLAY_SEQ, '||
        'use_this.QUALIFIER_FUNC, use_this.DEFAULT_FUNC, use_this.VALIDATION_FUNC, use_this.STAGING_TABLE_NAME, use_this.ACTIVE_IND, use_this.BASE_IND);');
      DBMS_OUTPUT.PUT_LINE('---------------------');
   END LOOP;

   DBMS_OUTPUT.PUT_LINE('----------------Insert/Update CFA_ATTRIB_GROUP_SET_LABELS---------------------');

   For y IN C_GET_GRP_SET_LABELS LOOP
      DBMS_OUTPUT.PUT_LINE('merge into CFA_ATTRIB_GROUP_SET_LABELS gsl using ( select gs.GROUP_SET_ID, ');
      DBMS_OUTPUT.PUT_LINE(''''||y.GROUP_SET_VIEW_NAME||''' GROUP_SET_VIEW_NAME, '||y.LANG||' LANG, '''||y.DEFAULT_LANG_IND||''' DEFAULT_LANG_IND, '''||
        y.LABEL||''' LABEL');
      DBMS_OUTPUT.PUT_LINE('from CFA_ATTRIB_GROUP_SET gs where gs.GROUP_SET_VIEW_NAME = '''||y.GROUP_SET_VIEW_NAME||''') use_this');
      DBMS_OUTPUT.PUT_LINE('on (gsl.GROUP_SET_ID = use_this.GROUP_SET_ID)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set gsl.LANG = use_this.LANG, gsl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, gsl.LABEL = use_this.LABEL');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert (GROUP_SET_ID, LANG, DEFAULT_LANG_IND, LABEL)');
      DBMS_OUTPUT.PUT_LINE('values (use_this.GROUP_SET_ID, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.LABEL);');
      DBMS_OUTPUT.PUT_LINE('---------------------');
   END LOOP;

END;
/
EOF

  if [[ $? -ne ${OK} ]]; then
      echo "Failed in COPY_GRP_SET" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi
}


function COPY_GRP
{
  sqlplus -s $connectStr  <<EOF>> $fileName
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 150
SET VERIFY OFF
SET FEEDBACK OFF
SET SERVEROUTPUT ON

DECLARE

   cursor C_GET_GRP is
      select g.GROUP_ID,
             gs.GROUP_SET_ID,
             g.GROUP_VIEW_NAME,
             gs.GROUP_SET_VIEW_NAME,
             g.DISPLAY_SEQ,
             g.VALIDATION_FUNC,
             g.ACTIVE_IND,
             g.BASE_IND
        from CFA_ATTRIB_GROUP g,
             CFA_ATTRIB_GROUP_SET gs
       where g.GROUP_SET_ID   = gs.GROUP_SET_ID
         and g.GROUP_ID       = DECODE('${level}', 'G', '${levelId}', g.GROUP_ID)
         and gs.GROUP_SET_ID  = DECODE('${level}', 'S', '${levelId}', gs.GROUP_SET_ID)
         and gs.EXT_ENTITY_ID = DECODE('${level}', 'E', DECODE('${levelId}', 'ALL', gs.EXT_ENTITY_ID, '${levelId}'), gs.EXT_ENTITY_ID);

   cursor C_GET_GRP_LABELS is
      select gl.GROUP_ID,
             g.GROUP_VIEW_NAME,
             gl.LANG,
             gl.DEFAULT_LANG_IND,
             gl.LABEL
        from CFA_ATTRIB_GROUP_LABELS gl,
             CFA_ATTRIB_GROUP g,
             CFA_ATTRIB_GROUP_SET gs
       where gl.GROUP_ID      = g.GROUP_ID
         and g.GROUP_SET_ID   = gs.GROUP_SET_ID
         and g.GROUP_ID       = DECODE('${level}', 'G', '${levelId}', g.GROUP_ID)
         and gs.GROUP_SET_ID  = DECODE('${level}', 'S', '${levelId}', gs.GROUP_SET_ID)
         and gs.EXT_ENTITY_ID = DECODE('${level}', 'E', DECODE('${levelId}', 'ALL', gs.EXT_ENTITY_ID, '${levelId}'), gs.EXT_ENTITY_ID);

BEGIN
   ---

   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('--------------------Insert/Update CFA_ATTRIB_GROUP----------------------------');

   For x IN C_GET_GRP LOOP
      DBMS_OUTPUT.PUT_LINE('merge into CFA_ATTRIB_GROUP g using ( select gs.GROUP_SET_ID, ');
      DBMS_OUTPUT.PUT_LINE(''''||x.GROUP_VIEW_NAME||''' GROUP_VIEW_NAME, '''||x.VALIDATION_FUNC||''' VALIDATION_FUNC, '||
        x.DISPLAY_SEQ||' DISPLAY_SEQ, '''||'N'||''' ACTIVE_IND, '''||x.BASE_IND||''' BASE_IND');
      DBMS_OUTPUT.PUT_LINE('from CFA_ATTRIB_GROUP_SET gs where gs.GROUP_SET_VIEW_NAME = '''||x.GROUP_SET_VIEW_NAME||''') use_this');
      DBMS_OUTPUT.PUT_LINE('on (g.GROUP_VIEW_NAME = use_this.GROUP_VIEW_NAME)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set g.DISPLAY_SEQ = use_this.DISPLAY_SEQ, g.VALIDATION_FUNC = use_this.VALIDATION_FUNC');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert (GROUP_ID, GROUP_SET_ID, GROUP_VIEW_NAME, DISPLAY_SEQ, VALIDATION_FUNC, ACTIVE_IND, BASE_IND)');
      DBMS_OUTPUT.PUT_LINE('values (CFA_ATTRIB_GROUP_SEQ.NEXTVAL, use_this.GROUP_SET_ID, use_this.GROUP_VIEW_NAME, use_this.DISPLAY_SEQ, '||
        'use_this.VALIDATION_FUNC, use_this.ACTIVE_IND, use_this.BASE_IND);');
      DBMS_OUTPUT.PUT_LINE('---------------------');
   END LOOP;


   DBMS_OUTPUT.PUT_LINE('------------------Insert/Update CFA_ATTRIB_GROUP_LABELS-----------------------');

   For y IN C_GET_GRP_LABELS LOOP
      DBMS_OUTPUT.PUT_LINE('merge into CFA_ATTRIB_GROUP_LABELS gl using ( select g.GROUP_ID, ');
      DBMS_OUTPUT.PUT_LINE(''''||y.GROUP_VIEW_NAME||''' GROUP_VIEW_NAME, '||y.LANG||' LANG, '''||y.DEFAULT_LANG_IND||''' DEFAULT_LANG_IND, '''||
        y.LABEL||''' LABEL');
      DBMS_OUTPUT.PUT_LINE('from CFA_ATTRIB_GROUP g where g.GROUP_VIEW_NAME = '''||y.GROUP_VIEW_NAME||''') use_this');
      DBMS_OUTPUT.PUT_LINE('on (gl.GROUP_ID = use_this.GROUP_ID)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set gl.LANG = use_this.LANG, gl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, gl.LABEL = use_this.LABEL');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert (GROUP_ID, LANG, DEFAULT_LANG_IND, LABEL)');
      DBMS_OUTPUT.PUT_LINE('values (use_this.GROUP_ID, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.LABEL);');
      DBMS_OUTPUT.PUT_LINE('---------------------');
   END LOOP;


END;
/
EOF

  if [[ $? -ne ${OK} ]]; then
      echo "Failed in COPY_GRP" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi
}


function COPY_ATTRIB
{
  sqlplus -s $connectStr  <<EOF>> $fileName
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 150
SET VERIFY OFF
SET FEEDBACK OFF
SET SERVEROUTPUT ON

DECLARE

   cursor C_GET_ATTRIB is
      select a.ATTRIB_ID,
             a.GROUP_ID,
             g.GROUP_VIEW_NAME,
             a.VIEW_COL_NAME, 
             a.STORAGE_COL_NAME,
             a.DISPLAY_SEQ,
             a.DATA_TYPE,
             a.UI_WIDGET,
             a.REC_GROUP_ID,
             rg.REC_GROUP_NAME,
             a.CODE_TYPE,
             a.ENABLE_IND,
             a.VALUE_REQ,
             a.MAXIMUM_LENGTH,
             a.LOWEST_ALLOWED_VALUE,
             a.HIGHEST_ALLOWED_VALUE,
             a.VALIDATION_FUNC,
             a.ACTIVE_IND,
             a.BASE_IND
        from CFA_ATTRIB a,
             CFA_ATTRIB_GROUP g,
             CFA_ATTRIB_GROUP_SET gs,
             CFA_REC_GROUP rg
       where a.GROUP_ID       = g.GROUP_ID
         and g.GROUP_SET_ID   = gs.GROUP_SET_ID
         and a.REC_GROUP_ID   = rg.REC_GROUP_ID(+)
         and a.ATTRIB_ID      = DECODE('${level}', 'A', '${levelId}', a.ATTRIB_ID)
         and g.GROUP_ID       = DECODE('${level}', 'G', '${levelId}', g.GROUP_ID)
         and gs.GROUP_SET_ID  = DECODE('${level}', 'S', '${levelId}', gs.GROUP_SET_ID)
         and gs.EXT_ENTITY_ID = DECODE('${level}', 'E', DECODE('${levelId}', 'ALL', gs.EXT_ENTITY_ID, '${levelId}'), gs.EXT_ENTITY_ID);

   cursor C_GET_ATTRIB_LABELS is
      select al.ATTRIB_ID,
             a.VIEW_COL_NAME,
             g.GROUP_VIEW_NAME,
             al.LANG,
             al.DEFAULT_LANG_IND,
             al.LABEL
        from CFA_ATTRIB a,
             CFA_ATTRIB_LABELS al,
             CFA_ATTRIB_GROUP g,
             CFA_ATTRIB_GROUP_SET gs
       where al.ATTRIB_ID     = a.ATTRIB_ID
         and a.GROUP_ID       = g.GROUP_ID
         and g.GROUP_SET_ID   = gs.GROUP_SET_ID
         and a.ATTRIB_ID      = DECODE('${level}', 'A', '${levelId}', a.ATTRIB_ID)
         and g.GROUP_ID       = DECODE('${level}', 'G', '${levelId}', g.GROUP_ID)
         and gs.GROUP_SET_ID  = DECODE('${level}', 'S', '${levelId}', gs.GROUP_SET_ID)
         and gs.EXT_ENTITY_ID = DECODE('${level}', 'E', DECODE('${levelId}', 'ALL', gs.EXT_ENTITY_ID, '${levelId}'), gs.EXT_ENTITY_ID);

BEGIN
   ---
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------------Insert/Update CFA_ATTRIB------------------------------');

   For x IN C_GET_ATTRIB LOOP
      if x.REC_GROUP_NAME is not NULL then
          DBMS_OUTPUT.PUT_LINE('merge into CFA_ATTRIB a using ( select g.GROUP_ID, g.GROUP_VIEW_NAME, rg.REC_GROUP_ID, ');
      else
          DBMS_OUTPUT.PUT_LINE('merge into CFA_ATTRIB a using ( select g.GROUP_ID, g.GROUP_VIEW_NAME, NULL REC_GROUP_ID, ');
      end if;
      DBMS_OUTPUT.PUT_LINE(''''||x.VIEW_COL_NAME||''' VIEW_COL_NAME, '''||x.STORAGE_COL_NAME||''' STORAGE_COL_NAME, '||x.DISPLAY_SEQ||' DISPLAY_SEQ, '''||
        x.DATA_TYPE||''' DATA_TYPE, '''||x.UI_WIDGET||''' UI_WIDGET, '''||x.CODE_TYPE||''' CODE_TYPE, '''||x.ENABLE_IND||''' ENABLE_IND, '''||
        x.VALUE_REQ||''' VALUE_REQ, '''||x.MAXIMUM_LENGTH||''' MAXIMUM_LENGTH, '''||x.LOWEST_ALLOWED_VALUE||''' LOWEST_ALLOWED_VALUE, '''||
        x.HIGHEST_ALLOWED_VALUE||''' HIGHEST_ALLOWED_VALUE, '''||x.VALIDATION_FUNC||''' VALIDATION_FUNC, '''||'N'||''' ACTIVE_IND, '''||
        x.BASE_IND||''' BASE_IND');
      if x.REC_GROUP_NAME is not NULL then
         DBMS_OUTPUT.PUT_LINE('from CFA_ATTRIB_GROUP g, CFA_REC_GROUP rg where g.GROUP_VIEW_NAME = '''||x.GROUP_VIEW_NAME||''' and rg.REC_GROUP_NAME = '''||
           x.REC_GROUP_NAME||''' ) use_this');
      else
         DBMS_OUTPUT.PUT_LINE('from CFA_ATTRIB_GROUP g where g.GROUP_VIEW_NAME = '''||x.GROUP_VIEW_NAME||''' ) use_this');
      end if;
      DBMS_OUTPUT.PUT_LINE('on (a.VIEW_COL_NAME = use_this.VIEW_COL_NAME and a.GROUP_ID = use_this.GROUP_ID)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set a.DISPLAY_SEQ = use_this.DISPLAY_SEQ, a.REC_GROUP_ID = use_this.REC_GROUP_ID, a.CODE_TYPE = use_this.CODE_TYPE, '||
        'a.ENABLE_IND = use_this.ENABLE_IND, a.VALIDATION_FUNC = use_this.VALIDATION_FUNC ');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert (ATTRIB_ID, GROUP_ID, VIEW_COL_NAME, STORAGE_COL_NAME, DISPLAY_SEQ, DATA_TYPE, UI_WIDGET, '||
        'REC_GROUP_ID, CODE_TYPE, ENABLE_IND, VALUE_REQ, MAXIMUM_LENGTH, LOWEST_ALLOWED_VALUE, HIGHEST_ALLOWED_VALUE, VALIDATION_FUNC, ACTIVE_IND, '||
        'BASE_IND)');
      DBMS_OUTPUT.PUT_LINE('values (CFA_ATTRIB_SEQ.NEXTVAL, use_this.GROUP_ID, use_this.VIEW_COL_NAME, use_this.STORAGE_COL_NAME, use_this.DISPLAY_SEQ, '||
        'use_this.DATA_TYPE, use_this.UI_WIDGET, use_this.REC_GROUP_ID, use_this.CODE_TYPE, use_this.ENABLE_IND, use_this.VALUE_REQ, '||
        'use_this.MAXIMUM_LENGTH, use_this.LOWEST_ALLOWED_VALUE, use_this.HIGHEST_ALLOWED_VALUE, use_this.VALIDATION_FUNC, use_this.ACTIVE_IND, '||
        'use_this.BASE_IND);');
      DBMS_OUTPUT.PUT_LINE('---------------------');
   END LOOP;


   DBMS_OUTPUT.PUT_LINE('----------------------Insert/Update CFA_ATTRIB_LABELS-------------------------');

   For y IN C_GET_ATTRIB_LABELS LOOP
      DBMS_OUTPUT.PUT_LINE('merge into CFA_ATTRIB_LABELS al using ( select a.ATTRIB_ID, ');
      DBMS_OUTPUT.PUT_LINE(''''||y.VIEW_COL_NAME||''' VIEW_COL_NAME, '''||y.GROUP_VIEW_NAME||''' GROUP_VIEW_NAME, '||y.LANG||' LANG, '''||
        y.DEFAULT_LANG_IND||''' DEFAULT_LANG_IND, '''||y.LABEL||''' LABEL');
      DBMS_OUTPUT.PUT_LINE('from CFA_ATTRIB a, CFA_ATTRIB_GROUP g where a.GROUP_ID = g.GROUP_ID '||
        'and a.VIEW_COL_NAME = '''||y.VIEW_COL_NAME||''' and g.GROUP_VIEW_NAME = '''||y.GROUP_VIEW_NAME||''') use_this');
      DBMS_OUTPUT.PUT_LINE('on (al.ATTRIB_ID = use_this.ATTRIB_ID)');
      DBMS_OUTPUT.PUT_LINE('when matched then update');
      DBMS_OUTPUT.PUT_LINE('set al.LANG = use_this.LANG, al.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, al.LABEL = use_this.LABEL');
      DBMS_OUTPUT.PUT_LINE('when not matched then insert (ATTRIB_ID, LANG, DEFAULT_LANG_IND, LABEL)');
      DBMS_OUTPUT.PUT_LINE('values (use_this.ATTRIB_ID, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.LABEL);');
      DBMS_OUTPUT.PUT_LINE('---------------------');   
   END LOOP;


END;
/
EOF

  if [[ $? -ne ${OK} ]]; then
      echo "Failed in COPY_ATTRIB" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi
}



#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [ $# -lt 2 ]
then
   echo $USAGE
   exit 1
fi

if [ $# -lt 3 ]
then
   connectStr=$1
   level=$2
   levelId="ALL"

   if [[ $level != 'E' && $level != 'C' && $level != 'R' && $level != 'L' ]]; then
      echo "ID is required for Group Set, Group, and Attribute level."
      exit ${FATAL}
   fi
else
   connectStr=$1
   level=$2
   levelId=$3
fi 

USER=${connectStr%/*}

if [[ $level != 'E' && $level != 'G' && $level != 'S' && $level != 'A' && $level != 'C' && $level != 'R' && $level != 'L' ]]; then
   echo "Invalid level, please enter L for complate copy, E for Entity, S for Group Set, G for Group, A for Attribute level, C for Codes or R for Rec Group."
   exit ${FATAL}
fi

LOG_MESSAGE "Started by ${USER}"

if [[ $level = 'L' ]]; then
   fileName="cfa_all_metadata.sql"
   echo "--  Genereated by cfamigrate.sql on ${stmpDate}  --">$fileName
   echo "SET DEFINE OFF" >>$fileName
fi

if [[ $level = 'C' || $level = 'L' ]]; then

   if [[ $level = 'C' ]]; then
      fileName="cfa_codes_${levelId}.sql"
      echo "--  Genereated by cfamigrate.sql on ${stmpDate}  --">$fileName
      echo "SET DEFINE OFF" >>$fileName
   fi

   COPY_CODES

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi
fi

if [[ $level = 'R' || $level = 'L' ]]; then

   if [[ $level = 'R' ]]; then
      fileName="cfa_rec_group_${levelId}.sql"
      echo "--  Genereated by cfamigrate.sql on ${stmpDate}  --">$fileName
      echo "SET DEFINE OFF" >>$fileName
   fi

   COPY_REC_GRP

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi
fi

if [[ $level = 'E' || $level = 'L' ]]; then

   if [[ $level = 'E' ]]; then
      fileName="cfa_entity_${levelId}.sql"
      echo "--  Genereated by cfamigrate.sql on ${stmpDate}  --">$fileName
      echo "SET DEFINE OFF" >>$fileName
   fi

   COPY_ENTITY

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi
fi

if [[ $level = 'S' || $level = 'E' || $level = 'L' ]]; then

   if [[ $level = 'S' ]]; then
      fileName="cfa_group_set_${levelId}.sql"
      echo "--  Genereated by cfamigrate.sql on ${stmpDate}  --">$fileName
      echo "SET DEFINE OFF" >>$fileName
   fi

   COPY_GRP_SET

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi
fi

if [[ $level = 'G' || $level = 'S' || $level = 'E' || $level = 'L' ]]; then

   if [[ $level = 'G' ]]; then
      fileName="cfa_group_${levelId}.sql"
      echo "--  Genereated by cfamigrate.sql on ${stmpDate}  --">$fileName
      echo "SET DEFINE OFF" >>$fileName
   fi;

   COPY_GRP

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi
fi

if [[ $level = 'A' || $level = 'G' || $level = 'S' || $level = 'E' || $level = 'L' ]]; then

   if [[ $level = 'A' ]]; then
      fileName="cfa_attribute_${levelId}.sql"
      echo "--  Genereated by cfamigrate.sql on ${stmpDate}  --">$fileName
   fi

   COPY_ATTRIB

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi
fi


echo "--  End of cfamigrate.sql output on ${stmpDate}  --">>$fileName

if [[ $? -ne ${OK} ]]; then
   exit ${FATAL}
else
   LOG_MESSAGE "Terminated Successfully"
   exit ${OK}
fi

