#! /bin/ksh

#---------------------------------------------------------------------------------
#  File:  refreshmview.ksh
#
#  Desc:  This korn shell script refreshes the materialized view passed as input. 
#         The second optional parameter is whether the underlying materialized views 
#         that the input mview is made of should also be refreshed or not.
#         This script does a complete refresh of the materialized view.
#---------------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
traceLog="${MMHOME}/log/${pgmName}_${pgmPID}_tracefiles.log"
OK=0
FATAL=255

USAGE="Usage: `basename $0` <connect> <name of the mview> <[ nested refresh indicator Y/N ]>"
#-------------------------------------------------------------------------
# Function Name: LOG_ERROR
# Purpose      : Log the error messages to the error file.
#-------------------------------------------------------------------------
function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: LOG_MESSAGE
# Purpose      : Log the  messages to the log file.
#-------------------------------------------------------------------------
function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code   := 0;
      EXEC :GV_script_error  := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL}
      return ${FATAL}
   fi

   return ${OK}
}


#-------------------------------------------------------------------------
# Function Name: REFRESH_MVIEW
#-------------------------------------------------------------------------

function REFRESH_MVIEW
{
   sqlTxt="
      DECLARE
         L_owner SYSTEM_OPTIONS.TABLE_OWNER%TYPE := NULL;

      BEGIN
         select table_owner
           into L_owner
           from system_options;
           
         DBMS_MVIEW.REFRESH(L_owner||'.$MVIEW_NAME','C',NULL,FALSE,FALSE,0,0,0,FALSE,$NESTED,FALSE);

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "REFRESH_MVIEW Failed" >>${ERRORFILE}
      return ${FATAL}
   else
      LOG_MESSAGE "Successfully Completed"
      return ${OK}
   fi
}

#-----------------------------------------------
# Main program starts 
# Parse the command line
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 2 ] 
then
   echo $USAGE
   exit 1
elif [ $# -eq 2 ] 
then
   NESTED="TRUE"
elif [ $# -eq 3 ]  
then 
    case "$3" in 
    "Y")NESTED="TRUE"
    ;;
    "N")NESTED="FALSE"
    ;;
    *) echo $USAGE
    exit 1
    ;;
    esac 
else
   echo $USAGE
   exit 1
fi

CONNECT=$1
MVIEW_NAME=$2

$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >>$ERRORFILE
EOF

if [ `cat $ERRORFILE | wc -l` -gt 1 ]
then
   LOG_MESSAGE "Exiting due to ORA/LOGIN Error. Check error file."
   exit 1;
fi

LOG_MESSAGE "Started by ${USER}"

REFRESH_MVIEW

if [[ ! ( -s $ERRORFILE ) ]]
then
   rm -f $ERRORFILE
fi

exit 0
