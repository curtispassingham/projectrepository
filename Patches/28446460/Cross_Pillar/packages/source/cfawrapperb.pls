CREATE OR REPLACE PACKAGE BODY CFA_WRAPPER_SQL AS
--------------------------------------------------------------------------------

FUNCTION INITIALIZE_CFAS(O_error_message        IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                    I_base_table           IN               CFA_SQL.GP_BASE_TABLE%TYPE,
                    I_group_set_id            IN               CFA_SQL.GP_GROUP_SET%TYPE,
                   I_key_name_1       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_1        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_2       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_2        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_3       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_3        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_4       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_4        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_5       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_5        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_6       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_6        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_7       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_7        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_8       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_8        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_9       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_9        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_10      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_10       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_11      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_11       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_12      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_12       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_13      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_13       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_14      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_14       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_15      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_15       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_16      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_16       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_17      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_17       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_18      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_18       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_19      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_19       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_20      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_20       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_lang                 IN               LANG.LANG%TYPE DEFAULT NULL,
                   I_simulate_ind         IN               VARCHAR2 DEFAULT 'N'
)
   RETURN BOOLEAN AS
   
   L_Default_Func CFA_VALIDATE_SQL.GP_FUNC%TYPE;
BEGIN

   if NOT CFA_SQL.BUILD_KEY_TBL(O_error_message,
                                I_base_table,
                                I_key_name_1,
                                I_key_val_1,
                                I_key_name_2,
                                I_key_val_2,
                                I_key_name_3,
                                I_key_val_3,
                                I_key_name_4,
                                I_key_val_4,
                                I_key_name_5,
                                I_key_val_5,
                                I_key_name_6,
                                I_key_val_6,
                                I_key_name_7,
                                I_key_val_7,
                                I_key_name_8,
                                I_key_val_8,
                                I_key_name_9,
                                I_key_val_9,
                                I_key_name_10,
                                I_key_val_10,
                                I_key_name_11,
                                I_key_val_11,
                                I_key_name_12,
                                I_key_val_12,
                                I_key_name_13,
                                I_key_val_13,
                                I_key_name_14,
                                I_key_val_14,
                                I_key_name_15,
                                I_key_val_15,
                                I_key_name_16,
                                I_key_val_16,
                                I_key_name_17,
                                I_key_val_17,
                                I_key_name_18,
                                I_key_val_18,
                                I_key_name_19,
                                I_key_val_19,
                                I_key_name_20,
                                I_key_val_20) then
         return FALSE;
   end if;
   
   


  if NOT CFA_SQL.INITIALIZE(O_error_message,
                           I_base_table,
                           I_group_set_id,
                           I_lang,
                           I_simulate_ind) then
               
         return FALSE;
   end if;
   
   if NOT CFA_SQL.BUILD_ATTRIB_TBL(O_error_message,
                           I_base_table,
                           I_group_set_id,
                           I_simulate_ind) then
               
   
         return FALSE;
   end if;
    if NOT CFA_VALIDATE_SQL.GET_GROUP_SET_DEFAULT_FUNC(O_error_message,
                                     L_Default_Func,
                                     I_group_set_id) then
      return FALSE;
   end if;
   
   IF L_Default_Func IS NULL THEN
        return FALSE;
   END IF;
   GP_DEFAULT_ONLY_INSERT:='Y';
   IF NOT CFA_VALIDATE_SQL.DEFAULT_GROUP_SET(--
        O_error_message,                        --
        I_base_table,                     --
        I_group_set_id                        --
        ) THEN
        return false;
      END IF;
   
   
   
   IF NOT CFA_SQL.PERSIST_DATA(O_error_message) THEN
        return false;
    END IF;
    
   GP_DEFAULT_ONLY_INSERT:='';      
   return TRUE;
   
   
END INITIALIZE_CFAS;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_ATTRIB(O_error_message        IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                         I_attrib_id            IN               cfa_attrib.attrib_id%TYPE,
               I_base_table           IN               CFA_SQL.GP_BASE_TABLE%TYPE,
               I_group_set_id            IN               CFA_SQL.GP_GROUP_SET%TYPE) 
    RETURN BOOLEAN AS
    
   cursor C_GET_ATTRIB_VAL_FUNC is
      select validation_func
        from cfa_attrib
       where attrib_id = I_attrib_id;
       
       L_program      VARCHAR2(50) := 'CFA_WRAPPER_SQL.VALIDATE_ATTRIB';
        L_function         cfa_attrib.validation_func%TYPE;
        L_return           BOOLEAN;
        L_stmt         VARCHAR2(1000);
        L_output_field   VARCHAR2(60);
        L_output_value   VARCHAR2(250);
    L_index  number;
    
    L_data_type CFA_SQL.GP_DATA_TYPE%TYPE;
    L_desc CFA_SQL.GP_FIELD_DESC_VALUE%TYPE;
    
BEGIN
  if NOT CFA_SQL.BUILD_ATTRIB_TBL(O_error_message,
                           I_base_table,
                           I_group_set_id,
                           'N') then
               
   
         return FALSE;
   end if;
   
  open  C_GET_ATTRIB_VAL_FUNC;
      fetch C_GET_ATTRIB_VAL_FUNC into L_function;
      close C_GET_ATTRIB_VAL_FUNC;
      

   --CFA_SQL.GP_return_tbl.delete;
          
   
    if L_function is NOT NULL then
       

        L_stmt := 'BEGIN '||
                  'if NOT '||L_function||'(:O_error_message) then '||
                     ':L_return := FALSE; '||
                  'else '||
                     ':L_return := TRUE; '||
                  'end if; '||
               'END;';


    EXECUTE IMMEDIATE L_stmt
        USING IN OUT O_error_message,
              IN OUT L_return;      
     
    
      if CFA_SQL.GP_return_tbl.count > 0 then
      
           
          if NOT CFA_SQL.PERSIST_DATA( O_error_message ) then
          
            return false;
          end if;
          
        
       end if;
  else 
   L_return := TRUE;
   end if;
    
      return L_return;
      
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
      
END VALIDATE_ATTRIB;



--------------------------------------------------------------------------------

FUNCTION VALIDATE_GROUP_SET(O_error_message        IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                         I_group_set_id            IN               cfa_attrib_group_set.group_set_id%TYPE) 
    RETURN BOOLEAN AS
    
   cursor C_GET_GRP_SET_VAL_FUNC is
      select ags.validation_func,
             ent.base_rms_table
        from cfa_ext_entity ent, 
             cfa_attrib_group_set ags
       where group_set_id = I_group_set_id
         and Ent.Ext_Entity_Id      = Ags.Ext_Entity_Id  ;    
       
        L_program          VARCHAR2(50) := 'CFA_WRAPPER_SQL.VALIDATE_GROUP_SET';
        L_function         cfa_attrib_group_set.validation_func%TYPE;
        L_return           BOOLEAN;
        L_stmt             VARCHAR2(1000);
        L_base_table       CFA_SQL.GP_BASE_TABLE%TYPE ;
BEGIN

  
   open  C_GET_GRP_SET_VAL_FUNC;
   fetch C_GET_GRP_SET_VAL_FUNC into L_function,L_base_table;
   close C_GET_GRP_SET_VAL_FUNC;
      
   if L_function is NOT NULL then
      if NOT CFA_SQL.BUILD_ATTRIB_TBL(O_error_message,
                                  L_base_table,
                                  I_group_set_id,
                                 'N') then
               
   
         return FALSE;
      end if;
      L_stmt := 'BEGIN '||
                  'if NOT '||L_function||'(:O_error_message) then '||
                     ':L_return := FALSE; '||
                  'else '||
                     ':L_return := TRUE; '||
                  'end if; '||
               'END;';
               
       EXECUTE IMMEDIATE L_stmt
         USING IN OUT O_error_message,
              IN OUT L_return;      
   end if;      
   return L_return;
      
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END VALIDATE_GROUP_SET;
---------------------------------------------------------------------------------------
FUNCTION CFAS_QUALIFIER_GROUPSET(O_error_message        IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                    I_base_table           IN               CFA_SQL.GP_BASE_TABLE%TYPE,
                    I_group_set_id            IN               CFA_SQL.GP_GROUP_SET%TYPE,
                   I_key_name_1       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_1        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_2       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_2        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_3       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_3        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_4       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_4        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_5       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_5        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_6       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_6        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_7       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_7        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_8       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_8        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_9       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_9        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_10      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_10       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_11      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_11       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_12      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_12       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_13      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_13       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_14      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_14       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_15      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_15       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_16      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_16       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_17      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_17       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_18      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_18       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_19      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_19       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_20      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_20       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_lang                 IN               LANG.LANG%TYPE DEFAULT NULL,
           I_simulate_ind         IN               VARCHAR2 DEFAULT 'N'
)
   RETURN NUMBER AS
BEGIN
   if NOT CFA_SQL.BUILD_KEY_TBL(O_error_message,
                                I_base_table,
                                I_key_name_1,
                                I_key_val_1,
                                I_key_name_2,
                                I_key_val_2,
                                I_key_name_3,
                                I_key_val_3,
                                I_key_name_4,
                                I_key_val_4,
                                I_key_name_5,
                                I_key_val_5,
                                I_key_name_6,
                                I_key_val_6,
                                I_key_name_7,
                                I_key_val_7,
                                I_key_name_8,
                                I_key_val_8,
                                I_key_name_9,
                                I_key_val_9,
                                I_key_name_10,
                                I_key_val_10,
                                I_key_name_11,
                                I_key_val_11,
                                I_key_name_12,
                                I_key_val_12,
                                I_key_name_13,
                                I_key_val_13,
                                I_key_name_14,
                                I_key_val_14,
                                I_key_name_15,
                                I_key_val_15,
                                I_key_name_16,
                                I_key_val_16,
                                I_key_name_17,
                                I_key_val_17,
                                I_key_name_18,
                                I_key_val_18,
                                I_key_name_19,
                                I_key_val_19,
                                I_key_name_20,
                                I_key_val_20) then
                                
                                    return 0;
                            end if;
            IF NOT CFA_VALIDATE_SQL.QUALIFY_GROUP_SET(--
                        O_error_message,                        --                        
                        I_group_set_id                        --
                        ) THEN
                        return 0;
            END IF;
        return 1;

END CFAS_QUALIFIER_GROUPSET;
------------------------------------------------------------------------------------
FUNCTION CFAS_ENTITY_VALIDATE(O_error_message        IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                    I_base_table           IN               CFA_SQL.GP_BASE_TABLE%TYPE,
                    I_group_set_id            IN               CFA_SQL.GP_GROUP_SET%TYPE,
                   I_key_name_1       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_1        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_2       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_2        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_3       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_3        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_4       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_4        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_5       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_5        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_6       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_6        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_7       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_7        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_8       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_8        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_9       IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_9        IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_10      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_10       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_11      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_11       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_12      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_12       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_13      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_13       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_14      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_14       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_15      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_15       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_16      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_16       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_17      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_17       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_18      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_18       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_19      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_19       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_key_name_20      IN      CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL,
                   I_key_val_20       IN      CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL,
                   I_lang                 IN               LANG.LANG%TYPE DEFAULT NULL,
           I_simulate_ind         IN               VARCHAR2 DEFAULT 'N'
)
   RETURN NUMBER AS
BEGIN
   if NOT CFA_SQL.BUILD_KEY_TBL(O_error_message,
                                I_base_table,
                                I_key_name_1,
                                I_key_val_1,
                                I_key_name_2,
                                I_key_val_2,
                                I_key_name_3,
                                I_key_val_3,
                                I_key_name_4,
                                I_key_val_4,
                                I_key_name_5,
                                I_key_val_5,
                                I_key_name_6,
                                I_key_val_6,
                                I_key_name_7,
                                I_key_val_7,
                                I_key_name_8,
                                I_key_val_8,
                                I_key_name_9,
                                I_key_val_9,
                                I_key_name_10,
                                I_key_val_10,
                                I_key_name_11,
                                I_key_val_11,
                                I_key_name_12,
                                I_key_val_12,
                                I_key_name_13,
                                I_key_val_13,
                                I_key_name_14,
                                I_key_val_14,
                                I_key_name_15,
                                I_key_val_15,
                                I_key_name_16,
                                I_key_val_16,
                                I_key_name_17,
                                I_key_val_17,
                                I_key_name_18,
                                I_key_val_18,
                                I_key_name_19,
                                I_key_val_19,
                                I_key_name_20,
                                I_key_val_20) then
                                
                                    return 0;
                            end if;
            IF NOT CFA_VALIDATE_SQL.VALIDATE_EXT_ENTITY(--
                        O_error_message,                        --                        
                        I_base_table                        --
                        ) THEN
                        return 0;
            END IF;
        return 1;

END CFAS_ENTITY_VALIDATE;
------------------------------------------------------------------------------------

END CFA_WRAPPER_SQL;
/