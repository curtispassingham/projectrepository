CREATE OR REPLACE PACKAGE BODY CFA_SQL AS

ERRNUM_PACKAGE_CALL           NUMBER(6) := -20020;  -- Define error for raise_application_error

LP_header_field_wd            CONSTANT   GP_DIM%TYPE := 68;  -- header field width (updated by UI at runtime)
LP_header_field_ht            CONSTANT   GP_DIM%TYPE := 15;  -- header field height (standard)
LP_header_field_desc_wd       CONSTANT   GP_DIM%TYPE := 130; -- header description field width (updated by UI at runtime)

LP_attrib_field_wd            CONSTANT   GP_DIM%TYPE := 90;  -- attrib field width (updated by UI at runtime)
LP_attrib_field_ht            CONSTANT   GP_DIM%TYPE := 15;  -- attrib field height (standard)
LP_attrib_field_desc_wd       CONSTANT   GP_DIM%TYPE := 180; -- attrib description field width (updated by UI at runtime)
LP_attrib_field_pb_cb_wd      CONSTANT   GP_DIM%TYPE := 14;  -- lov pushbutton/checkbox width (standard)

LP_field_to_field_ysp         CONSTANT   GP_DIM%TYPE := 18;  -- field to field vertical spacing
LP_field_to_field_xsp         CONSTANT   GP_DIM%TYPE := 10;  -- field to field horizontal spacing
LP_canvas_edge_to_field       CONSTANT   GP_DIM%TYPE := 8;   -- space between the canvas edge and the field (or prompt)
LP_main_to_stack              CONSTANT   GP_DIM%TYPE := 8;   -- main canvas edge to stacked canvas edge spacing
LP_stack_to_stack             CONSTANT   GP_DIM%TYPE := 5;   -- edge to edge space between stacked canvas
LP_prompt_offset              CONSTANT   GP_DIM%TYPE := 3;   -- Prompt to field spacing

LP_char_pt_size_large         CONSTANT   GP_DIM%TYPE := 5;   -- Prompt character size
LP_char_pt_size_mid           CONSTANT   GP_DIM%TYPE := 4.25;-- Prompt character size
LP_char_pt_size_small         CONSTANT   GP_DIM%TYPE := 3;   -- Prompt character size

LP_attrib_init_ysp            CONSTANT   GP_DIM%TYPE := 25;  -- Field initial Y position on attrib canvas
LP_header_init_xsp            CONSTANT   GP_DIM%TYPE := 150;
LP_header_min_field_xsp       CONSTANT   GP_DIM%TYPE := 67;  -- Minimum spacing between header fields

LP_group_canvas_wd            CONSTANT   GP_DIM%TYPE := 166;
LP_editor_field_wd            CONSTANT   GP_DIM%TYPE := 15;

LP_min_attrib_canvas_wd       GP_DIM%TYPE := 115; -- minimum attrib canvas width
LP_min_attrib_canvas_ht       GP_DIM%TYPE := 0;   -- minimum attrib canvas height

LP_max_attrib_row_wd          GP_DIM%TYPE;
LP_max_header_row_wd          GP_DIM%TYPE;
LP_max_header_label           GP_DIM%TYPE;

LP_header_row_cnt             NUMBER;
LP_attrib_set_cnt             NUMBER;

LP_lang                       LANG.LANG%TYPE;
LP_simulate_ind               VARCHAR2(1);

--------------------------------------------------------------------------------------
PROCEDURE QUERY_GROUP(IO_group_tbl     IN OUT  NOCOPY  TYP_GROUP_TBL,
                      I_base_table     IN              GP_BASE_TABLE%TYPE,
                      I_group_set      IN              GP_GROUP_SET%TYPE) AS

   L_program         VARCHAR2(62) := 'CFA_SQL.QUERY_GROUP';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;

   L_cnt             NUMBER := 0;

   cursor C_GET_ATTRIB_GROUP is
      select grp.group_id,
             NVL(gd.label, gdf.label) label
        from cfa_attrib_group grp,
             cfa_attrib_group_labels gd,
             cfa_attrib_group_labels gdf,
             cfa_attrib_group_set gs,
             cfa_ext_entity ext
       where ext.ext_entity_id = gs.ext_entity_id
         and grp.group_set_id = gs.group_set_id
         and grp.group_id = gdf.group_id
         and gdf.default_lang_ind = 'Y'
         and (grp.active_ind = 'Y' or
              LP_simulate_ind = 'Y')
         and gd.group_id (+) = grp.group_id
         and gd.lang (+) = LP_lang
         and ext.base_rms_table = I_base_table
         and gs.group_set_id = NVL(I_group_set, gs.group_set_id)
       order by grp.display_seq;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program||chr(10)||
               'I_base_table  : '||I_base_table||chr(10)||
               'I_group_set: '||I_group_set);

   FOR rec in C_GET_ATTRIB_GROUP LOOP
       L_cnt := L_cnt + 1;
       IO_group_tbl(L_cnt).group_id := rec.group_id;
       IO_group_tbl(L_cnt).group_desc := rec.label;
   END LOOP;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return;
   ---
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      DBG_SQL.MSG(L_program,
               'Error in '||L_program||' '||L_error_message);
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
END QUERY_GROUP;
--------------------------------------------------------------------------------------
PROCEDURE QUERY_ATTRIB(IO_attrib_tbl   IN OUT  NOCOPY  TYP_ATTRIB_TBL,
                       I_group_id      IN              CFA_ATTRIB_GROUP.GROUP_ID%TYPE) AS

   L_program             VARCHAR2(62) := 'CFA_SQL.QUERY_ATTRIB';
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;

   INV_OBJ_NAME          EXCEPTION;
   PRAGMA                EXCEPTION_INIT(INV_OBJ_NAME, -44002);

   TYPE CUR_attrib       IS REF CURSOR;
   C_GET_EXT_ATTRIB      CUR_attrib;

   L_attrib_rec          TYP_attrib_rec;
   L_index               GP_INDEX%TYPE;

   L_query               VARCHAR2(2000);
   L_select              VARCHAR2(400);
   L_from                VARCHAR2(100);
   L_where               VARCHAR2(1500);

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program||chr(10)||
               'I_group_id  : '||I_group_id);
   ---

   if GP_attrib_value_tbl.count > 0 then
      L_index := GP_attrib_value_tbl.first;

      loop
         exit when L_index is NULL;

         DBG_SQL.MSG(L_program,
                  'L_index: '||L_index);

         if GP_attrib_cfg_tbl(L_index).group_id = I_group_id then
            IO_attrib_tbl(I_group_id).group_id := I_group_id;
            case GP_attrib_cfg_tbl(L_index).storage_col
               when 'VARCHAR2_1' then
                  IO_attrib_tbl(I_group_id).varchar2_1 := GP_attrib_value_tbl(L_index).field_value;
               when 'VARCHAR2_2' then
                  IO_attrib_tbl(I_group_id).varchar2_2 := GP_attrib_value_tbl(L_index).field_value;
               when 'VARCHAR2_3' then
                  IO_attrib_tbl(I_group_id).varchar2_3 := GP_attrib_value_tbl(L_index).field_value;
               when 'VARCHAR2_4' then
                  IO_attrib_tbl(I_group_id).varchar2_4 := GP_attrib_value_tbl(L_index).field_value;
               when 'VARCHAR2_5' then
                  IO_attrib_tbl(I_group_id).varchar2_5 := GP_attrib_value_tbl(L_index).field_value;
               when 'VARCHAR2_6' then
                  IO_attrib_tbl(I_group_id).varchar2_6 := GP_attrib_value_tbl(L_index).field_value;
               when 'VARCHAR2_7' then
                  IO_attrib_tbl(I_group_id).varchar2_7 := GP_attrib_value_tbl(L_index).field_value;
               when 'VARCHAR2_8' then
                  IO_attrib_tbl(I_group_id).varchar2_8 := GP_attrib_value_tbl(L_index).field_value;
               when 'VARCHAR2_9' then
                  IO_attrib_tbl(I_group_id).varchar2_9 := GP_attrib_value_tbl(L_index).field_value;
               when 'VARCHAR2_10' then
                  IO_attrib_tbl(I_group_id).varchar2_10 := GP_attrib_value_tbl(L_index).field_value;
               when 'NUMBER_11' then
                  IO_attrib_tbl(I_group_id).number_11  := to_number(GP_attrib_value_tbl(L_index).field_value);
               when 'NUMBER_12' then
                  IO_attrib_tbl(I_group_id).number_12  := to_number(GP_attrib_value_tbl(L_index).field_value);
               when 'NUMBER_13' then
                  IO_attrib_tbl(I_group_id).number_13  := to_number(GP_attrib_value_tbl(L_index).field_value);
               when 'NUMBER_14' then
                  IO_attrib_tbl(I_group_id).number_14  := to_number(GP_attrib_value_tbl(L_index).field_value);
               when 'NUMBER_15' then
                  IO_attrib_tbl(I_group_id).number_15  := to_number(GP_attrib_value_tbl(L_index).field_value);
               when 'NUMBER_16' then
                  IO_attrib_tbl(I_group_id).number_16  := to_number(GP_attrib_value_tbl(L_index).field_value);
               when 'NUMBER_17' then
                  IO_attrib_tbl(I_group_id).number_17  := to_number(GP_attrib_value_tbl(L_index).field_value);
               when 'NUMBER_18' then
                  IO_attrib_tbl(I_group_id).number_18  := to_number(GP_attrib_value_tbl(L_index).field_value);
               when 'NUMBER_19' then
                  IO_attrib_tbl(I_group_id).number_19  := to_number(GP_attrib_value_tbl(L_index).field_value);
               when 'NUMBER_20' then
                  IO_attrib_tbl(I_group_id).number_20  := to_number(GP_attrib_value_tbl(L_index).field_value);
               when 'DATE_21' then
                  IO_attrib_tbl(I_group_id).date_21    := to_date(GP_attrib_value_tbl(L_index).field_value,GP_date_format);
               when 'DATE_22' then
                  IO_attrib_tbl(I_group_id).date_22    := to_date(GP_attrib_value_tbl(L_index).field_value,GP_date_format);   
                 when 'DATE_23' then
                  IO_attrib_tbl(I_group_id).date_23    := to_date(GP_attrib_value_tbl(L_index).field_value,GP_date_format);
                  when 'DATE_24' then
                  IO_attrib_tbl(I_group_id).date_24    := to_date(GP_attrib_value_tbl(L_index).field_value,GP_date_format);
               when 'DATE_25' then
                  IO_attrib_tbl(I_group_id).date_25    := to_date(GP_attrib_value_tbl(L_index).field_value,GP_date_format);
            end case;
         end if;
         L_index := GP_attrib_value_tbl.next(L_index);
      end loop;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return;
   ---
EXCEPTION

   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      DBG_SQL.MSG(L_program,
               'Error in '||L_program||' '||L_error_message);
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
END QUERY_ATTRIB;
--------------------------------------------------------------------------------------
PROCEDURE MERGE_ATTRIB(IO_attrib_tbl   IN OUT  NOCOPY  TYP_ATTRIB_TBL,
                       I_group_id      IN              CFA_ATTRIB_GROUP.GROUP_ID%TYPE,
                       I_group_set     IN              GP_GROUP_SET%TYPE) AS

   L_program            VARCHAR2(62) := 'CFA_SQL.MERGE_ATTRIB';
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;

   L_index              GP_INDEX%TYPE;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program||chr(10)||
               'I_group_id : '||I_group_id||chr(10)||
               'I_group_set: '||I_group_set);
   ---

   if GP_attrib_value_tbl.count > 0 then
      L_index := GP_attrib_value_tbl.first;

      loop
         exit when L_index is NULL;

         DBG_SQL.MSG(L_program,
                  'L_index: '||L_index);

         if GP_attrib_cfg_tbl(L_index).group_id = I_group_id then
            case GP_attrib_cfg_tbl(L_index).storage_col
               when 'VARCHAR2_1' then
                  GP_attrib_value_tbl(L_index).field_value := IO_attrib_tbl(1).varchar2_1;
               when 'VARCHAR2_2' then
                  GP_attrib_value_tbl(L_index).field_value := IO_attrib_tbl(1).varchar2_2;
               when 'VARCHAR2_3' then
                  GP_attrib_value_tbl(L_index).field_value := IO_attrib_tbl(1).varchar2_3;
               when 'VARCHAR2_4' then
                  GP_attrib_value_tbl(L_index).field_value := IO_attrib_tbl(1).varchar2_4;
               when 'VARCHAR2_5' then
                  GP_attrib_value_tbl(L_index).field_value := IO_attrib_tbl(1).varchar2_5;
               when 'VARCHAR2_6' then
                  GP_attrib_value_tbl(L_index).field_value := IO_attrib_tbl(1).varchar2_6;
               when 'VARCHAR2_7' then
                  GP_attrib_value_tbl(L_index).field_value := IO_attrib_tbl(1).varchar2_7;
               when 'VARCHAR2_8' then
                  GP_attrib_value_tbl(L_index).field_value := IO_attrib_tbl(1).varchar2_8;
               when 'VARCHAR2_9' then
                  GP_attrib_value_tbl(L_index).field_value := IO_attrib_tbl(1).varchar2_9;
               when 'VARCHAR2_10' then
                  GP_attrib_value_tbl(L_index).field_value := IO_attrib_tbl(1).varchar2_10;
               when 'NUMBER_11' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).number_11);
               when 'NUMBER_12' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).number_12);
               when 'NUMBER_13' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).number_13);
               when 'NUMBER_14' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).number_14);
               when 'NUMBER_15' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).number_15);
               when 'NUMBER_16' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).number_16);
               when 'NUMBER_17' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).number_17);
               when 'NUMBER_18' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).number_18);
               when 'NUMBER_19' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).number_19);
               when 'NUMBER_20' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).number_20);
               when 'DATE_21' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).date_21);
                  when 'DATE_22' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).date_22);
                  when 'DATE_23' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).date_23);
                  when 'DATE_24' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).date_24);
               when 'DATE_25' then
                  GP_attrib_value_tbl(L_index).field_value := to_char(IO_attrib_tbl(1).date_25);
            end case;
         end if;
         L_index := GP_attrib_value_tbl.next(L_index);
      end loop;
   end if;

   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return;
   ---
EXCEPTION

   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      DBG_SQL.MSG(L_program,
               'Error in '||L_program||' '||L_error_message);
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
END MERGE_ATTRIB;
--------------------------------------------------------------------------------------
PROCEDURE LOCK_ATTRIB(IO_attrib_tbl    IN OUT  NOCOPY  TYP_ATTRIB_TBL,
                      I_group_id       IN              CFA_ATTRIB_GROUP.GROUP_ID%TYPE) AS

   L_program         VARCHAR2(62) := 'CFA_SQL.LOCK_ATTRIB';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;

   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   INV_OBJ_NAME       EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INV_OBJ_NAME, -44002);


   TYPE CUR_attrib    IS REF CURSOR;
   C_LOCK_ATTRIB      CUR_attrib;

   L_index            GP_INDEX%TYPE;

   L_query            VARCHAR2(2000);
   L_select           VARCHAR2(400);
   L_from             VARCHAR2(100);
   L_where            VARCHAR2(1500);

BEGIN

   return;
   ---
EXCEPTION
   when RECORD_LOCKED then
      L_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            GP_ext_table,
                                            I_group_id);
      DBG_SQL.MSG(L_program,
               'Error in '||L_program||' '||L_error_message);
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);

   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      DBG_SQL.MSG(L_program,
               'Error in '||L_program||' '||L_error_message);
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
END LOCK_ATTRIB;
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
FUNCTION INITIALIZE(O_error_message        IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                    I_base_table           IN               GP_BASE_TABLE%TYPE,
                    I_group_set            IN               GP_GROUP_SET%TYPE,
                    I_lang                 IN               LANG.LANG%TYPE,
                    I_simulate_ind         IN               VARCHAR2)
   RETURN BOOLEAN AS

   L_program             VARCHAR2(62)   := 'CFA_SQL.INITIALIZE';

   L_index               GP_INDEX%TYPE;

   L_sum_field_wd        GP_DIM%TYPE := 0;

   L_init_xpos           GP_POS%TYPE := LP_header_init_xsp;
   L_current_xpos        GP_POS%TYPE := L_init_xpos;
   L_current_ypos        GP_POS%TYPE := LP_canvas_edge_to_field;

   L_init_header_row_wd  GP_DIM%TYPE := 0;
   L_max_attrib_row_wd   GP_DIM%TYPE := 0;
   L_max_header_row_wd   GP_DIM%TYPE := 0;
   L_max_header_label    GP_DIM%TYPE := 0;  -- max leftmost fields header prompt length
   L_label_length        GP_DIM%TYPE;

   L_cnt                 NUMBER := 0;
   L_attrib_cnt          NUMBER := 0;
   L_header_row_cnt      NUMBER := 1;
   L_upper_char_cnt      NUMBER;

   L_length                 NUMBER(3);
   L_half                   NUMBER(3);
   L_xpos_cap               NUMBER(3) := 185;-- An incredibly long prompt will put all the fields off the screen if the program
                          -- is allowed to dynamically spread everything out to accommodate every character.  Instead of a hard
                          -- cap limiting the number of characters entered as a prompt, a soft cap is being utilized.  The UI
                          -- will expand out to allow for about 60 characters in a prompt(label) value.  More than that and the
                          -- user may experience some overlapping.
   L_prompt_wraparound_lgth NUMBER(2) := 30;  -- number of characters in a prompt which causes
                                              -- it to be broken onto 2 lines.

   L_tst_cnt             NUMBER;

   cursor C_DATE_FORMAT is
     select value
       from v$nls_parameters
      where parameter = 'NLS_DATE_FORMAT';

   cursor C_GET_HEADER_CONFIG is
      select KEY_WDGT field_type,
             ent.custom_ext_table ext_table, -- change to cfa ext table
             ent.base_rms_table base_table,
             ek.key_col,
             ek.key_number,
             ek.data_type,
             NVL(kd.label,kdp.label) label,
             ek.description_code,
             NVL(length(NVL(kd.label,kdp.label)),0) char_cnt,
             NVL(regexp_count(NVL(kd.label,kdp.label), '[a-hjkm-z02-9 ]'), 0) lower_char_cnt,
             NVL(regexp_count(NVL(kd.label,kdp.label), '[ilI1]'), 0) narrow_char_cnt,
             LP_header_field_wd +
                decode(ek.description_code, NULL, 0, LP_header_field_desc_wd + 1) field_length -- 1 is the field to desc spacing
        from cfa_ext_entity ent,
             cfa_ext_entity_key ek,
             cfa_ext_entity_key_labels kd,
             cfa_ext_entity_key_labels kdp
       where ent.base_rms_table = ek.base_rms_table
         and ent.base_rms_table = I_base_table
         and ek.base_rms_table = kdp.base_rms_table
         and ek.key_col = kdp.key_col
         and kdp.default_lang_ind = 'Y'
         and ek.base_rms_table = kd.base_rms_table (+)
         and ek.key_col = kd.key_col (+)
         and kd.lang (+)= LP_lang;

   cursor C_GET_ATTRIB_CONFIG is
      select cfg.*,
             max(cfg.attrib_cnt)
                over(partition by cfg.ext_entity_id) max_attrib_cnt,
             max((cfg.char_cnt - (cfg.lower_char_cnt + cfg.narrow_char_cnt)) * LP_char_pt_size_large +
                  cfg.lower_char_cnt * LP_char_pt_size_mid +
                  cfg.narrow_char_cnt * LP_char_pt_size_small +
                  LP_canvas_edge_to_field)
                over(partition by cfg.ext_entity_id) max_label_length,
             max(field_length)
                over(partition by cfg.ext_entity_id) max_field_length
        from (select ext.ext_entity_id,
                     grp.group_set_id,
                     att.attrib_id,
                     att.group_id,
                     att.view_col_name,
                     att.data_type,
                     att.storage_col_name,
                     NVL(ad.label, adf.label) label,
                     att.display_seq,
                     att.ui_widget,
                     att.enable_ind,
                     att.value_req,
                     att.editor_req,
                     att.maximum_length,
                     att.lowest_allowed_value,
                     att.highest_allowed_value,
                     att.validation_func,
                     att.rec_group_id,
                     att.code_type,
                     to_number(substr(att.storage_col_name, INSTR(att.storage_col_name, '_')+1)) col_no,
                     NVL(length(NVL(ad.label, adf.label)), 0) char_cnt,
                     NVL(regexp_count(NVL(ad.label, adf.label), '[a-hjkm-z02-9 ]'), 0) lower_char_cnt,
                     NVL(regexp_count(NVL(ad.label, adf.label), '[ilI1]'), 0) narrow_char_cnt,
                     (row_number()
                       over (partition by ext.ext_entity_id,
                                          grp.group_set_id,
                                          grp.group_id
                                 order by att.display_seq) - 1) * LP_field_to_field_ysp + LP_attrib_init_ysp field_ypos,
                     case
                        when ui_widget = CB_WDGT then
                           to_number(LP_attrib_field_pb_cb_wd)
                        when ui_widget in (TI_WDGT,LI_WDGT) then
                           to_number(LP_attrib_field_wd) + decode(att.editor_req,'Y',to_number(LP_editor_field_wd),0)
                        when ui_widget = DT_WDGT then
                           LP_attrib_field_wd + 1 + LP_attrib_field_pb_cb_wd -- 1 is the spacing between fields
                        when ui_widget = RG_WDGT then
                           LP_attrib_field_wd + 1 + LP_attrib_field_pb_cb_wd -- + 1 + LP_attrib_field_desc_wd Corresponding Desc field will be added as an enhancement 6718
                     end field_length,
                     count(att.attrib_id)
                        over(partition by ext.ext_entity_id,
                                          grp.group_set_id,
                                          grp.group_id) attrib_cnt,
                     lead(case
                            when att.ui_widget in (CB_WDGT,LI_WDGT) then
                               att.ui_widget||'_'||att.storage_col_name
                            else
                               att.storage_col_name
                          end)
                        over(partition by att.group_id
                                 order by att.display_seq) next_nav_item,
                     lag(case
                            when att.ui_widget in (CB_WDGT,LI_WDGT) then
                               att.ui_widget||'_'||att.storage_col_name
                            else
                               att.storage_col_name
                         end)
                        over(partition by att.group_id
                                  order by att.display_seq) prev_nav_item
                from cfa_ext_entity ext,
                     cfa_attrib_group_set gs,
                     cfa_attrib_group grp,
                     cfa_attrib att,
                     cfa_attrib_labels ad,
                     cfa_attrib_labels adf
               where ext.base_rms_table = I_base_table
                 and ext.ext_entity_id = gs.ext_entity_id
                 and gs.group_set_id = NVL(I_group_set, gs.group_set_id)
                 and grp.group_set_id = gs.group_set_id
                 and att.group_id = grp.group_id
                 and adf.attrib_id = att.attrib_id
                 and adf.default_lang_ind = 'Y'
                 and ((gs.active_ind = 'Y' and
                      grp.active_ind = 'Y' and
                      att.active_ind = 'Y') or
                      LP_simulate_ind = 'Y')
                 and ad.attrib_id (+) = att.attrib_id
                 and ad.lang (+)= LP_lang) cfg
       order by cfg.group_id, cfg.display_seq;


   cursor C_ATTRIB_CNT is
      select count(*)
        from cfa_ext_entity ext,
                     cfa_attrib_group_set gs,
                     cfa_attrib_group grp,
                     cfa_attrib att,
                     cfa_attrib_labels ad,
                     cfa_attrib_labels adf
               where ext.base_rms_table = I_base_table
                 and ext.ext_entity_id = gs.ext_entity_id
                 and gs.display_seq = NVL(I_group_set, gs.display_seq)
                 and grp.group_set_id = gs.group_set_id
                 and att.group_id = grp.group_id
                 and adf.attrib_id = att.attrib_id
                 and adf.default_lang_ind = 'Y'
                 and ((gs.active_ind = 'Y' and
                      grp.active_ind = 'Y' and
                      att.active_ind = 'Y') or
                      LP_simulate_ind = 'Y')
                 and ad.attrib_id (+) = att.attrib_id
                 and ad.lang (+)= LP_lang;

BEGIN

   DBG_SQL.MSG(L_program,
               'Begin: '||L_program||chr(10)||
               'I_base_table  : '||I_base_table||chr(10)||
               'I_group_set   : '||I_group_set||chr(10)||
               'I_lang        : '||I_lang||chr(10)||
               'I_simulate_ind: '||I_simulate_ind);

   GP_base_table    := I_base_table;

   LP_lang          := NVL(I_lang, GET_USER_LANG);
   LP_simulate_ind  := NVL(I_simulate_ind, 'N');

   GP_header_cfg_tbl.delete;
   GP_attrib_cfg_tbl.delete;
   open C_DATE_FORMAT;
   fetch C_DATE_FORMAT into GP_date_format;
   close C_DATE_FORMAT;

   for rec in C_GET_HEADER_CONFIG loop

      L_index := rec.key_col;
      L_cnt   := L_cnt + 1;

      DBG_SQL.MSG(L_program,
                  'L_index: '||L_index||chr(10)||
                  'L_cnt : '||L_cnt);
      ---
      if L_cnt = 1 then
         GP_ext_table := rec.ext_table;

         DBG_SQL.MSG(L_program,
                  'GP_ext_table: '||GP_ext_table);
      end if;
      --
      DBG_SQL.MSG(L_program,
                  'rec.field_type: '||rec.field_type);
      GP_header_cfg_tbl(L_index).header_type   := rec.field_type;
      GP_header_cfg_tbl(L_index).key_col       := rec.key_col;
      GP_header_cfg_tbl(L_index).key_no        := rec.key_number;
      if length(rec.label) > L_prompt_wraparound_lgth then
         -- if the header label length is greater than the wraparound value
         -- add break to put the prompt on 2 lines.
         L_length := length(rec.label);
         L_half := round(L_length/2);
         GP_header_cfg_tbl(L_index).label := substr(rec.label,1,L_half)||chr(10)||substr(rec.label,L_half+1,L_length);
      else
            GP_header_cfg_tbl(L_index).label   := rec.label;
      end if;

      GP_header_cfg_tbl(L_index).data_type     := rec.data_type;
      GP_header_cfg_tbl(L_index).desc_code     := rec.description_code;

      DBG_SQL.MSG(L_program,
                  'after GP_header_cfg_tbl pop');


      if rec.data_type = CHAR_TYPE then
         GP_key_value_tbl(L_index).field_value_qry := ''''||GP_key_value_tbl(L_index).field_value||'''';
      elsif rec.data_type = NUM_TYPE then
         GP_key_value_tbl(L_index).field_value_qry := GP_key_value_tbl(L_index).field_value;
      elsif rec.data_type = DATE_TYPE then
         GP_key_value_tbl(L_index).field_value_qry := 'to_date('''||GP_key_value_tbl(L_index).field_value||''')';
      end if;

      GP_key_value_tbl(L_index).data_type := rec.data_type;

      DBG_SQL.MSG(L_program,
                  'after GP_key_value_tbl pop');

      L_upper_char_cnt := rec.char_cnt - (rec.lower_char_cnt + rec.narrow_char_cnt);

      L_label_length := L_upper_char_cnt * LP_char_pt_size_large +
                        rec.lower_char_cnt * LP_char_pt_size_mid +
                        rec.narrow_char_cnt * LP_char_pt_size_small +
                        LP_prompt_offset;
      if L_label_length > L_max_header_label then
         L_max_header_label := L_label_length;
      end if;

      DBG_SQL.MSG(L_program,
                  'L_upper_char_cnt: '||L_upper_char_cnt||chr(10)||
                  'L_label_length  : '||L_label_length);
      ---
      if L_cnt = 1 then
         GP_header_cfg_tbl(L_index).ui_xpos := LP_header_init_xsp;
         L_current_xpos := LP_header_init_xsp + rec.field_length + LP_field_to_field_xsp;
         ---
         L_sum_field_wd := rec.field_length; -- leftmost field label not included

         DBG_SQL.MSG(L_program,
                     '<hcfg>.ui_xpos    : '||GP_header_cfg_tbl(L_index).ui_xpos||chr(10)||
                     'L_current_xpos    : '||L_current_xpos||chr(10)||
                     'L_max_header_label: '||L_max_header_label||chr(10)||
                     'L_sum_field_wd    : '||L_sum_field_wd);
      else
         DBG_SQL.MSG(L_program,
                     'L_max_header_label('||L_max_header_label||') + '||
                     'L_sum_field_wd('||L_sum_field_wd||') + '||
                     'LP_field_to_field_xsp('||LP_field_to_field_xsp||') + '||
                     'L_label_length('||L_label_length||') + '||
                     'rec.field_length('||rec.field_length||') <= '||
                     'L_init_header_row_wd('||L_init_header_row_wd||')');
         if L_max_header_label +                  -- longest leftmost label
            L_sum_field_wd +                      -- total horizontal field lengths and labels w/o then left most label
            LP_field_to_field_xsp +               -- added horizontal field to field spacing
            L_label_length + rec.field_length <=  -- new field and label to be fitted on the current row
            L_init_header_row_wd then             -- horizontal row length limit calc from group and attrib horizontal field sizes


            GP_header_cfg_tbl(L_index).ui_xpos := L_current_xpos + L_label_length;

            DBG_SQL.MSG(L_program,
                     'New L_current_xpos: '||chr(10)||
                     'L_current_xpos('||L_current_xpos||') + '||
                     'L_label_length('||L_label_length||') + '||
                     'rec.field_length('||rec.field_length||') + '||
                     'LP_field_to_field_xsp('||LP_field_to_field_xsp||')');

            L_current_xpos := L_current_xpos + L_label_length + rec.field_length + LP_field_to_field_xsp;
            ---
            DBG_SQL.MSG(L_program,
                     'New L_sum_field_wd: '||chr(10)||
                     'L_sum_field_wd('||L_sum_field_wd||') + '||
                     'LP_field_to_field_xsp('||LP_field_to_field_xsp||') + '||
                     'L_label_length('||L_label_length||') + '||
                     'rec.field_length('||rec.field_length||')');

            L_sum_field_wd := L_sum_field_wd + LP_field_to_field_xsp + L_label_length + rec.field_length;


         else
            GP_header_cfg_tbl(L_index).ui_xpos := LP_header_init_xsp;

            L_current_xpos := LP_header_init_xsp + rec.field_length + LP_field_to_field_xsp;
            L_current_ypos := L_current_ypos + LP_field_to_field_ysp;

            DBG_SQL.MSG(L_program,
                     '<hcfg>.ui_xpos    : '||LP_header_init_xsp||chr(10)||
                     'L_current_xpos    : '||L_current_xpos||chr(10)||
                     'L_current_ypos    : '||L_current_ypos);
            ---
            L_sum_field_wd := rec.field_length; -- leftmost field label not included;

            DBG_SQL.MSG(L_program,
                     'L_max_header_label: '||L_max_header_label||chr(10)||
                     'L_sum_field_wd    : '||L_sum_field_wd);
            ---
            L_sum_field_wd := rec.field_length;

            DBG_SQL.MSG(L_program,
                        'L_sum_field_wd: '||L_sum_field_wd);
            ---
            L_header_row_cnt := L_header_row_cnt + 1;

         end if;
      end if;

      GP_header_cfg_tbl(L_index).ui_ypos := L_current_ypos;
      ---
      if L_sum_field_wd > L_max_header_row_wd then
         L_max_header_row_wd := L_sum_field_wd;
      end if;

      DBG_SQL.MSG(L_program,
                  'call CFA_VALIDATE_SQL.GET_KEY_DESC');
      -- Get the header field value descriptions
      if NOT CFA_VALIDATE_SQL.GET_KEY_DESC(O_error_message,
                                           GP_header_cfg_tbl(L_index).key_desc_value,
                                           I_base_table,
                                           GP_header_cfg_tbl(L_index).key_col) then
         return FALSE;
      end if;

      DBG_SQL.MSG(L_program,
                  '<hcfg>.header_type: '||GP_header_cfg_tbl(L_index).header_type||chr(10)||
                  '<hcfg>.key_col  : '||GP_header_cfg_tbl(L_index).key_col||chr(10)||
                  '<hcfg>.key_no   : '||GP_header_cfg_tbl(L_index).key_no||chr(10)||
                  '<hcfg>.key_label: '||GP_header_cfg_tbl(L_index).label||chr(10)||
                  '<hcfg>.data_type: '||GP_header_cfg_tbl(L_index).data_type||chr(10)||
                  '<hcfg>.desc_code: '||GP_header_cfg_tbl(L_index).desc_code||chr(10)||
                  '<hcfg>.ui_xpos  : '||GP_header_cfg_tbl(L_index).ui_xpos||chr(10)||
                  '<hcfg>.ui_ypos  : '||GP_header_cfg_tbl(L_index).ui_ypos);

      DBG_SQL.MSG(L_program,
                  '<hval>.field_type     : '||GP_key_value_tbl(L_index).field_type||chr(10)||
                  '<hval>.field_name     : '||GP_key_value_tbl(L_index).field_name||chr(10)||
                  '<hval>.field_value    : '||GP_key_value_tbl(L_index).field_value||chr(10)||
                  '<hval>.field_value_qry: '||GP_key_value_tbl(L_index).field_value_qry||chr(10)||
                  '<hval>.data_type      : '||GP_key_value_tbl(L_index).data_type);

   end loop;

   L_cnt := 0;

   FOR rec in C_GET_ATTRIB_CONFIG LOOP
      L_index := rec.view_col_name;
      L_cnt := L_cnt + 1;
      ---
      GP_attrib_cfg_tbl(L_index).attrib_id               := rec.attrib_id;
      GP_attrib_cfg_tbl(L_index).group_id                := rec.group_id;
      GP_attrib_cfg_tbl(L_index).view_col                := rec.view_col_name;
      GP_attrib_cfg_tbl(L_index).data_type               := rec.data_type;
      GP_attrib_cfg_tbl(L_index).storage_col             := rec.storage_col_name;
      ---
      if length(rec.label) > L_prompt_wraparound_lgth then
         -- if the label length is greater than the wraparound value
         -- add break to put the prompt on 2 lines.
         L_length := length(rec.label);
         L_half := round(L_length/2);
         GP_attrib_cfg_tbl(L_index).label := substr(rec.label,1,L_half)||chr(10)||substr(rec.label,L_half+1,L_length);
      else
            GP_attrib_cfg_tbl(L_index).label             := rec.label;
      end if;
      ---
      GP_attrib_cfg_tbl(L_index).display_seq             := rec.display_seq;
      GP_attrib_cfg_tbl(L_index).ui_widget               := rec.ui_widget;
      GP_attrib_cfg_tbl(L_index).enable_ind              := rec.enable_ind;
      GP_attrib_cfg_tbl(L_index).value_req               := rec.value_req;
      GP_attrib_cfg_tbl(L_index).editor_req              := rec.editor_req;
      GP_attrib_cfg_tbl(L_index).maximum_length          := rec.maximum_length;
      GP_attrib_cfg_tbl(L_index).lowest_allowed_value    := rec.lowest_allowed_value;
      GP_attrib_cfg_tbl(L_index).highest_allowed_value   := rec.highest_allowed_value;
      GP_attrib_cfg_tbl(L_index).validation_func         := rec.validation_func;
      GP_attrib_cfg_tbl(L_index).rec_group_id            := rec.rec_group_id;
      GP_attrib_cfg_tbl(L_index).code_type               := rec.code_type;

      GP_attrib_cfg_tbl(L_index).attrib_no               := rec.col_no;
      ---
      GP_attrib_cfg_tbl(L_index).next_nav_item           := rec.next_nav_item;
      GP_attrib_cfg_tbl(L_index).prev_nav_item           := rec.prev_nav_item;
      ---
      if rec.max_label_length > L_xpos_cap then
         -- if the label is beyond the cap, set the xpos to the cap
         GP_attrib_cfg_tbl(L_index).ui_xpos              := L_xpos_cap;
      else
         GP_attrib_cfg_tbl(L_index).ui_xpos              := rec.max_label_length;
      end if;
      GP_attrib_cfg_tbl(L_index).ui_ypos                 := rec.field_ypos;
      ---
      L_max_attrib_row_wd                                := rec.max_label_length + rec.max_field_length;
      L_attrib_cnt                                       := rec.max_attrib_cnt;

      DBG_SQL.MSG(L_program,
                  '<acfg>.attrib_id    : '||GP_attrib_cfg_tbl(L_index).attrib_id||chr(10)||
                  '<acfg>.group_id     : '||GP_attrib_cfg_tbl(L_index).group_id||chr(10)||
                  '<acfg>.view_col     : '||GP_attrib_cfg_tbl(L_index).view_col||chr(10)||
                  '<acfg>.data_type    : '||GP_attrib_cfg_tbl(L_index).data_type||chr(10)||
                  '<acfg>.storage_col  : '||GP_attrib_cfg_tbl(L_index).storage_col||chr(10)||
                  '<acfg>.attrib_label : '||GP_attrib_cfg_tbl(L_index).label||chr(10)||
                  '<acfg>.display_seq  : '||GP_attrib_cfg_tbl(L_index).display_seq||chr(10)||
                  '<acfg>.ui_widget    : '||GP_attrib_cfg_tbl(L_index).ui_widget||chr(10)||
                  '<acfg>.rec_group_id : '||GP_attrib_cfg_tbl(L_index).rec_group_id||chr(10)||
                  '<acfg>.li_code_type : '||GP_attrib_cfg_tbl(L_index).code_type||chr(10)||
                  '<acfg>.attrib_no    : '||GP_attrib_cfg_tbl(L_index).attrib_no||chr(10)||
                  '<acfg>.next_nav_item: '||GP_attrib_cfg_tbl(L_index).next_nav_item||chr(10)||
                  '<acfg>.prev_nav_item: '||GP_attrib_cfg_tbl(L_index).prev_nav_item||chr(10)||
                  '<acfg>.ui_xpos      : '||GP_attrib_cfg_tbl(L_index).ui_xpos||chr(10)||
                  '<acfg>.ui_ypos      : '||GP_attrib_cfg_tbl(L_index).ui_ypos||chr(10)||
                  'L_max_attrib_row_wd : '||L_max_attrib_row_wd||chr(10)||
                  'L_attrib_cnt        : '||L_attrib_cnt);
   END LOOP;
   ---
   open C_ATTRIB_CNT;
   fetch C_ATTRIB_CNT into L_tst_cnt;
   close C_ATTRIB_CNT;

   DBG_SQL.MSG(L_program,
               'GP_attrib_cfg_tbl count: '||GP_attrib_cfg_tbl.count||chr(10)||
               'L_attrib_cnt: '||L_attrib_cnt||chr(10)||
               'L_tst_cnt: '||L_tst_cnt);

   if L_max_attrib_row_wd < LP_min_attrib_canvas_wd then
      L_max_attrib_row_wd := LP_min_attrib_canvas_wd;
   end if;

   LP_min_attrib_canvas_ht  := 18;
   LP_max_attrib_row_wd     := L_max_attrib_row_wd;
   LP_attrib_set_cnt        := L_attrib_cnt;
   LP_max_header_row_wd     := L_max_header_row_wd;
   LP_header_row_cnt        := L_header_row_cnt;
   LP_max_header_label      := L_max_header_label;

   DBG_SQL.MSG(L_program,
               'LP_max_attrib_row_wd : '||LP_max_attrib_row_wd||chr(10)||
               'LP_attrib_set_cnt    : '||LP_attrib_set_cnt||chr(10)||
               'LP_max_header_row_wd : '||LP_max_header_row_wd||chr(10)||
               'LP_header_row_cnt    : '||LP_header_row_cnt||chr(10)||
               'LP_max_header_label  : '||LP_max_header_label);

   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END INITIALIZE;
--------------------------------------------------------------------------------
FUNCTION BUILD_KEY_TBL(
   O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
   I_base_table       IN      GP_BASE_TABLE%TYPE,
   I_key_name_1       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_1        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_2       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_2        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_3       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_3        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_4       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_4        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_5       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_5        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_6       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_6        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_7       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_7        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_8       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_8        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_9       IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_9        IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_10      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_10       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_11      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_11       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_12      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_12       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_13      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_13       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_14      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_14       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_15      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_15       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_16      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_16       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_17      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_17       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_18      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_18       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_19      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_19       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL,
   I_key_name_20      IN      GP_FIELD_ITEM%TYPE DEFAULT NULL,
   I_key_val_20       IN      GP_FIELD_VALUE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(62)  := 'CFA_SQL.BUILD_KEY_TBL';
   L_index    GP_INDEX%TYPE := NULL;

   cursor C_GET_KEYS is
      select ek.key_col,
             DECODE(kd.label, NULL, kd2.label, kd.label) key_label,
             ek.data_type,
             key_number,
             description_code
        from cfa_ext_entity ent,
             cfa_ext_entity_key ek,
             cfa_ext_entity_key_labels kd,
             cfa_ext_entity_key_labels kd2
       where ent.base_rms_table   = ek.base_rms_table
         and ek.base_rms_table    = kd.base_rms_table(+)
         and ek.key_col           = kd.key_col(+)
         and ent.base_rms_table   = I_base_table
         and kd.lang(+)           = GET_USER_LANG
         and ek.base_rms_table    = kd2.base_rms_table
         and ek.key_col           = kd2.key_col
         and kd2.default_lang_ind = 'Y';

BEGIN

   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);

   if I_base_table is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','I_base_table');
     return FALSE;
   end if;

   GP_key_value_tbl.delete;

   FOR rec in C_GET_KEYS LOOP
      L_index:= rec.key_col;
      --
      GP_key_value_tbl(L_index).field_type         := CFA_KEY;
      GP_key_value_tbl(L_index).field_name         := rec.key_col;
      GP_key_value_tbl(L_index).data_type          := rec.data_type;
      GP_key_value_tbl(L_index).field_no           := rec.key_number;
      --GP_key_value_tbl(L_index).label              := rec.key_label;
      --GP_key_value_tbl(L_index).description_code := rec.description_code;
      --
      --The order of the input key_name and key_val does not matter in the following.
      --Only missing input key_name would raise an error, extra key_name are ignored.

      case upper(rec.key_col)
         when upper(I_key_name_1)  then
            GP_key_value_tbl(L_index).field_value  := I_key_val_1;
         when upper(I_key_name_2)  then
            GP_key_value_tbl(L_index).field_value  := I_key_val_2;
         when upper(I_key_name_3)  then
            GP_key_value_tbl(L_index).field_value  := I_key_val_3;
         when upper(I_key_name_4)  then
            GP_key_value_tbl(L_index).field_value  := I_key_val_4;
         when upper(I_key_name_5)  then
            GP_key_value_tbl(L_index).field_value  := I_key_val_5;
         when upper(I_key_name_6)  then
            GP_key_value_tbl(L_index).field_value  := I_key_val_6;
         when upper(I_key_name_7)  then
            GP_key_value_tbl(L_index).field_value  := I_key_val_7;
         when upper(I_key_name_8)  then
            GP_key_value_tbl(L_index).field_value  := I_key_val_8;
         when upper(I_key_name_9)  then
            GP_key_value_tbl(L_index).field_value  := I_key_val_9;
         when upper(I_key_name_10) then
            GP_key_value_tbl(L_index).field_value  := I_key_val_10;
         when upper(I_key_name_11) then
            GP_key_value_tbl(L_index).field_value  := I_key_val_11;
         when upper(I_key_name_12) then
            GP_key_value_tbl(L_index).field_value  := I_key_val_12;
         when upper(I_key_name_13) then
            GP_key_value_tbl(L_index).field_value  := I_key_val_13;
         when upper(I_key_name_14) then
            GP_key_value_tbl(L_index).field_value  := I_key_val_14;
         when upper(I_key_name_15) then
            GP_key_value_tbl(L_index).field_value  := I_key_val_15;
         when upper(I_key_name_16) then
            GP_key_value_tbl(L_index).field_value  := I_key_val_16;
         when upper(I_key_name_17) then
            GP_key_value_tbl(L_index).field_value  := I_key_val_17;
         when upper(I_key_name_18) then
            GP_key_value_tbl(L_index).field_value  := I_key_val_18;
         when upper(I_key_name_19) then
            GP_key_value_tbl(L_index).field_value  := I_key_val_19;
         when upper(I_key_name_20) then
            GP_key_value_tbl(L_index).field_value  := I_key_val_20;
         else
            O_error_message := SQL_LIB.CREATE_MSG('CFA_MISSING_KEY',NULL); return FALSE;
      end case;

      if upper(rec.data_type) = DATE_TYPE then
         GP_key_value_tbl(L_index).field_value_qry := 'to_date(' || GP_key_value_tbl(L_index).field_value || ')';
      elsif upper(rec.data_type) = NUM_TYPE then
         GP_key_value_tbl(L_index).field_value_qry := 'to_number(' || GP_key_value_tbl(L_index).field_value || ')';
      elsif upper(rec.data_type) = CHAR_TYPE then
         GP_key_value_tbl(L_index).field_value_qry := '''' || GP_key_value_tbl(L_index).field_value || '''';
      end if;
      --
   END LOOP;

   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BUILD_KEY_TBL;
--------------------------------------------------------------------------------
FUNCTION BUILD_ATTRIB_TBL(
   O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
   I_base_table         IN      GP_BASE_TABLE%TYPE,
   I_group_set          IN      GP_GROUP_SET%TYPE,
   I_simulate_ind       IN      VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_SQL.BUILD_ATTRIB_TBL';

   INV_OBJ_NAME   EXCEPTION;
   PRAGMA         EXCEPTION_INIT(INV_OBJ_NAME, -44002);

   L_index          GP_INDEX%TYPE;

   L_from           GP_EXT_TABLE%TYPE:= GP_ext_table;
   L_where          VARCHAR2(500);
   L_query          VARCHAR2(3000);

   L_first          BOOLEAN      := TRUE;

   L_col_name       GP_FIELD_ITEM%TYPE;
   L_data_type      GP_DATA_TYPE%TYPE;
   L_attrib_value   GP_FIELD_VALUE%TYPE;

   cursor C_EXT_TBL is
      select custom_ext_table
        from cfa_ext_entity
       where base_rms_table = I_base_table;

   TYPE CUR_attr_value  IS REF CURSOR;
   C_ATTR_VALUE         CUR_attr_value;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program||chr(10)||
               'I_base_table: '||I_base_table||chr(10)||
               'I_group_set: '||I_group_set||chr(10)||
               'I_simulate_ind: '||I_simulate_ind);
   ---
   GP_attrib_value_tbl.delete;   
   -- For simulation, pre-set the attrib value collection
   if LP_simulate_ind = 'Y' then
      L_index := GP_attrib_cfg_tbl.first;
      loop
         exit when L_index is NULL;

         DBG_SQL.MSG(L_program,
               'L_index: '||L_index);

         GP_attrib_value_tbl(L_index).field_type  := CFA_EXT;
         GP_attrib_value_tbl(L_index).field_name  := GP_attrib_cfg_tbl(L_index).view_col;
         GP_attrib_value_tbl(L_index).data_type   := GP_attrib_cfg_tbl(L_index).data_type;
         GP_attrib_value_tbl(L_index).field_value := NULL;

         L_index := GP_attrib_cfg_tbl.next(L_index);
      end loop;
   end if;

   if L_from is NULL then
      if I_base_table is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','I_base_table');
         return FALSE;
      end if;
      ---
      open C_EXT_TBL;
      fetch C_EXT_TBL into L_from;
      close C_EXT_TBL;
   end if;
   ---
   DBG_SQL.MSG(L_program,
               'L_from: '||L_from);

   L_from := dbms_assert.sql_object_name(L_from);

   DBG_SQL.MSG(L_program,
               'L_from (after assert): '||L_from);

   L_index := GP_key_value_tbl.first;
   loop
      exit when L_index is NULL;
      if GP_key_value_tbl(L_index).field_type = CFA_KEY then
         if L_first then
            L_where := 'where ' || GP_key_value_tbl(L_index).field_name || ' = '|| GP_key_value_tbl(L_index).field_value_qry || ' ';
            L_first := FALSE;
         else
            L_where := L_where || ' and ' || GP_key_value_tbl(L_index).field_name || ' = '|| GP_key_value_tbl(L_index).field_value_qry || ' ';
         end if;
      end if;
      L_index := GP_key_value_tbl.next (L_index);
   end loop;
   ---
   L_query := 'select att.view_col_name, '||
                     'att.data_type, '||
                     'piv.attrib_value '||
                'from cfa_ext_entity ext, '||
                     'cfa_attrib_group_set gst, '||
                     'cfa_attrib_group grp, '||
                     'cfa_attrib att, '||
                     '(select group_id, '||
                             'attrib_col, '||
                             'attrib_value '||
                        'from (select group_id, '||
                                     'varchar2_1, '||
                                     'varchar2_2, '||
                                     'varchar2_3, '||
                                     'varchar2_4, '||
                                     'varchar2_5, '||
                                     'varchar2_6, '||
                                     'varchar2_7, '||
                                     'varchar2_8, '||
                                     'varchar2_9, '||
                                     'varchar2_10, '||
                                     'to_char(number_11) number_11, '||
                                     'to_char(number_12) number_12, '||
                                     'to_char(number_13) number_13, '||
                                     'to_char(number_14) number_14, '||
                                     'to_char(number_15) number_15, '||
                                     'to_char(number_16) number_16, '||
                                     'to_char(number_17) number_17, '||
                                     'to_char(number_18) number_18, '||
                                     'to_char(number_19) number_19, '||
                                     'to_char(number_20) number_20, '||
                                     'to_char(date_21, '''||GP_date_format||''') date_21, '||
                                     'to_char(date_22, '''||GP_date_format||''') date_22, '||
                                     'to_char(date_23, '''||GP_date_format||''') date_23, '||
                                     'to_char(date_24, '''||GP_date_format||''') date_24, '||
                                     'to_char(date_25, '''||GP_date_format||''') date_25 '||
                                'from ' || L_from ||' '||
                                L_where ||') ext '||
                              'unpivot  '||
                                '(attrib_value for attrib_col in (varchar2_1  as ''VARCHAR2_1'', '||
                                                                 'varchar2_2  as ''VARCHAR2_2'', '||
                                                                 'varchar2_3  as ''VARCHAR2_3'', '||
                                                                 'varchar2_4  as ''VARCHAR2_4'', '||
                                                                 'varchar2_5  as ''VARCHAR2_5'', '||
                                                                 'varchar2_6  as ''VARCHAR2_6'', '||
                                                                 'varchar2_7  as ''VARCHAR2_7'', '||
                                                                 'varchar2_8  as ''VARCHAR2_8'', '||
                                                                 'varchar2_9  as ''VARCHAR2_9'', '||
                                                                 'varchar2_10 as ''VARCHAR2_10'', '||
                                                                 'number_11   as ''NUMBER_11'', '||
                                                                 'number_12   as ''NUMBER_12'', '||
                                                                 'number_13   as ''NUMBER_13'', '||
                                                                 'number_14   as ''NUMBER_14'', '||
                                                                 'number_15   as ''NUMBER_15'', '||
                                                                 'number_16   as ''NUMBER_16'', '||
                                                                 'number_17   as ''NUMBER_17'', '||
                                                                 'number_18   as ''NUMBER_18'', '||
                                                                 'number_19   as ''NUMBER_19'', '||
                                                                 'number_20   as ''NUMBER_20'', '||
                                                                 'date_21     as ''DATE_21'', '||
                                                                 'date_22     as ''DATE_22'', '||
                                                                 'date_23     as ''DATE_23'', '||
                                                                 'date_24     as ''DATE_24'', '||
                                                                 'date_25     as ''DATE_25''))) piv '||
               'where ext.base_rms_table   = '''|| I_base_table || '''' ||
                 'and ext.ext_entity_id    = gst.ext_entity_id '||
                 'and gst.group_set_id     = NVL(' || I_group_set || ',gst.group_set_id) ' ||
                 'and gst.group_set_id     = grp.group_set_id '||
                 'and ((gst.active_ind      = ''Y'' and '||
                       'grp.active_ind      = ''Y'' and '||
                       'att.active_ind      = ''Y'') or '||
                       ''''||I_simulate_ind ||'''= ''Y'') '||
                 'and att.group_id         = grp.group_id '||
                 'and piv.attrib_col(+)    = att.storage_col_name '||
                 'and piv.group_id(+)      = att.group_id '||
               'order by att.group_id ';
   ---
   DBG_SQL.MSG(L_program,
               L_query);

   open  C_ATTR_VALUE for L_query;
   loop
     fetch C_ATTR_VALUE INTO L_col_name,
                             L_data_type,
                             L_attrib_value;
     exit when C_ATTR_VALUE%NOTFOUND;
     --
     L_index := L_col_name;
     GP_attrib_value_tbl(L_index).field_type  := CFA_EXT;
     GP_attrib_value_tbl(L_index).field_name  := L_col_name;
     GP_attrib_value_tbl(L_index).data_type   := L_data_type;
     GP_attrib_value_tbl(L_index).field_value := L_attrib_value;

   end loop;
   close C_ATTR_VALUE;

   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when INV_OBJ_NAME then
      if LP_simulate_ind = 'Y' then
         return TRUE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('EXT_NOT_SETUP',
                                               L_from);
         return FALSE;
      end if;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END BUILD_ATTRIB_TBL;
--------------------------------------------------------------------------------
FUNCTION GET_HEADER_CFG(O_error_message       IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                        IO_header_cfg_tbl     IN OUT  NOCOPY   TYP_HEADER_CFG_TBL)
   RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_SQL.GET_HEADER_CFG';
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   IO_header_cfg_tbl := GP_header_cfg_tbl;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_HEADER_CFG;
--------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_CFG(O_error_message    IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                        IO_attrib_cfg_tbl  IN OUT  NOCOPY   TYP_ATTRIB_CFG_TBL)
   RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_SQL.GET_ATTRIB_CFG';
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   IO_attrib_cfg_tbl := GP_attrib_cfg_tbl;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_ATTRIB_CFG;
--------------------------------------------------------------------------------------
FUNCTION GET_GROUP_SET(
   O_error_message      IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
   IO_group_set_tbl     IN OUT  NOCOPY   TBL_group_set_tbl,
   I_base_table         IN               GP_BASE_TABLE%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_SQL.GET_GROUP_SET';

   cursor C_GET_GRP_SET is
      SELECT ags.group_set_id,
             ags.ext_entity_id,
             ags.display_seq,
             ags.active_ind,
             NVL(agsl.label, agsl2.label) label
        FROM cfa_ext_entity ent,
             cfa_attrib_group_set ags,
             cfa_attrib_group_set_labels agsl,
             cfa_attrib_group_set_labels agsl2
       WHERE ent.base_rms_table     = I_base_table
         AND ent.ext_entity_id      = ags.ext_entity_id
         AND ags.group_set_id       = agsl.group_set_id(+)
         AND agsl.lang(+)           = GET_USER_LANG
         AND ags.group_set_id       = agsl2.group_set_id
         AND agsl2.default_lang_ind = 'Y'
       order by ags.display_seq;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   open C_GET_GRP_SET;
   fetch C_GET_GRP_SET BULK COLLECT into IO_group_set_tbl;
   close C_GET_GRP_SET;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_GROUP_SET;
--------------------------------------------------------------------------------------
FUNCTION GET_UI_VARIABLES(O_error_message  IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_ui_var_rec    IN OUT  NOCOPY   TYP_UI_VAR_REC)
   RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_SQL.GET_UI_VARIABLES';
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   IO_ui_var_rec.header_field_wd       := LP_header_field_wd;
   IO_ui_var_rec.header_field_ht       := LP_header_field_ht;
   IO_ui_var_rec.header_field_desc_wd  := LP_header_field_desc_wd;
      --
   IO_ui_var_rec.attrib_field_wd       := LP_attrib_field_wd;
   IO_ui_var_rec.attrib_field_ht       := LP_attrib_field_ht;
   IO_ui_var_rec.attrib_field_desc_wd  := LP_attrib_field_desc_wd;
   IO_ui_var_rec.attrib_field_pb_wd    := LP_attrib_field_pb_cb_wd;

   -- Spacing
   IO_ui_var_rec.field_to_field_ysp    := LP_field_to_field_ysp;
   IO_ui_var_rec.field_to_field_xsp    := LP_field_to_field_xsp;
   IO_ui_var_rec.canvas_edge_to_field  := LP_canvas_edge_to_field;
   IO_ui_var_rec.header_offset_xsp     := LP_header_init_xsp;
   IO_ui_var_rec.attrib_offset_ysp     := LP_attrib_init_ysp;
   IO_ui_var_rec.main_to_stack         := LP_main_to_stack;
   IO_ui_var_rec.stack_to_stack        := LP_stack_to_stack;
   IO_ui_var_rec.min_attrib_canvas_wd  := LP_min_attrib_canvas_wd;
   IO_ui_var_rec.min_attrib_canvas_ht  := LP_min_attrib_canvas_ht;
   ---
   IO_ui_var_rec.char_data_type        := CHAR_TYPE;
   IO_ui_var_rec.num_data_type         := NUM_TYPE;
   IO_ui_var_rec.date_data_type        := DATE_TYPE;
   IO_ui_var_rec.text_item_widget      := TI_WDGT;
   IO_ui_var_rec.rec_group_widget      := RG_WDGT;
   IO_ui_var_rec.list_item_widget      := LI_WDGT;
   IO_ui_var_rec.check_box_widget      := CB_WDGT;
   IO_ui_var_rec.date_widget           := DT_WDGT;
   IO_ui_var_rec.pb_widget             := PB_WDGT;
   IO_ui_var_rec.desc_widget           := DESC_WDGT;
   IO_ui_var_rec.key_widget            := KEY_WDGT;
   ---
   IO_ui_var_rec.max_attrib_row_wd     := LP_max_attrib_row_wd;
   IO_ui_var_rec.attrib_set_cnt        := LP_attrib_set_cnt;
   IO_ui_var_rec.max_header_row_wd     := LP_max_header_row_wd;
   IO_ui_var_rec.header_row_cnt        := LP_header_row_cnt;
   IO_ui_var_rec.max_header_label      := LP_max_header_label;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_UI_VARIABLES;
--------------------------------------------------------------------------------------
FUNCTION PERSIST_DATA(
   O_error_message    IN OUT           RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN AS

   L_program            VARCHAR2(62) := 'CFA_SQL.PERSIST_DATA';
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;

   L_data_tbl           TYP_attrib_tbl;

   INV_OBJ_NAME         EXCEPTION;
   PRAGMA               EXCEPTION_INIT(INV_OBJ_NAME, -44002);

   L_index              GP_INDEX%TYPE;
   L_group_id           GP_GROUP_ID%TYPE;

   L_merge_init         VARCHAR2(200);
   L_insert_init        VARCHAR2(500);
   L_values_init        VARCHAR2(500);
   L_on_init            VARCHAR2(1000) := 'on (';

   L_merge              VARCHAR2(32767);
   L_insert             VARCHAR2(1000);
   L_values             VARCHAR2(1000);
   L_using              VARCHAR2(3000);
   L_on                 VARCHAR2(1000);
   L_when_matched       VARCHAR2(3000);
   L_when_not_matched   VARCHAR2(3000);

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   if GP_attrib_value_tbl.count > 0 then
      L_index := GP_attrib_value_tbl.first;

      loop
         exit when L_index is NULL;

         DBG_SQL.MSG(L_program,
                  'L_index: '||L_index);

         L_group_id := GP_attrib_cfg_tbl(L_index).group_id;
         L_data_tbl(L_group_id).group_id := L_group_id;

         case GP_attrib_cfg_tbl(L_index).storage_col
            when 'VARCHAR2_1' then
               L_data_tbl(L_group_id).varchar2_1 := replace(GP_attrib_value_tbl(L_index).field_value,'''','''''');
            when 'VARCHAR2_2' then
               L_data_tbl(L_group_id).varchar2_2 := replace(GP_attrib_value_tbl(L_index).field_value,'''','''''');
            when 'VARCHAR2_3' then
               L_data_tbl(L_group_id).varchar2_3 := replace(GP_attrib_value_tbl(L_index).field_value,'''','''''');
            when 'VARCHAR2_4' then
               L_data_tbl(L_group_id).varchar2_4 := replace(GP_attrib_value_tbl(L_index).field_value,'''','''''');
            when 'VARCHAR2_5' then
               L_data_tbl(L_group_id).varchar2_5 := replace(GP_attrib_value_tbl(L_index).field_value,'''','''''');
            when 'VARCHAR2_6' then
               L_data_tbl(L_group_id).varchar2_6 := replace(GP_attrib_value_tbl(L_index).field_value,'''','''''');
            when 'VARCHAR2_7' then
               L_data_tbl(L_group_id).varchar2_7 := replace(GP_attrib_value_tbl(L_index).field_value,'''','''''');
            when 'VARCHAR2_8' then
               L_data_tbl(L_group_id).varchar2_8 := replace(GP_attrib_value_tbl(L_index).field_value,'''','''''');
            when 'VARCHAR2_9' then
               L_data_tbl(L_group_id).varchar2_9 := replace(GP_attrib_value_tbl(L_index).field_value,'''','''''');
            when 'VARCHAR2_10' then
               L_data_tbl(L_group_id).varchar2_10 := replace(GP_attrib_value_tbl(L_index).field_value,'''','''''');
            when 'NUMBER_11' then
               L_data_tbl(L_group_id).number_11  := to_number(GP_attrib_value_tbl(L_index).field_value);
            when 'NUMBER_12' then
               L_data_tbl(L_group_id).number_12  := to_number(GP_attrib_value_tbl(L_index).field_value);
            when 'NUMBER_13' then
               L_data_tbl(L_group_id).number_13  := to_number(GP_attrib_value_tbl(L_index).field_value);
            when 'NUMBER_14' then
               L_data_tbl(L_group_id).number_14  := to_number(GP_attrib_value_tbl(L_index).field_value);
            when 'NUMBER_15' then
               L_data_tbl(L_group_id).number_15  := to_number(GP_attrib_value_tbl(L_index).field_value);
            when 'NUMBER_16' then
               L_data_tbl(L_group_id).number_16  := to_number(GP_attrib_value_tbl(L_index).field_value);
            when 'NUMBER_17' then
               L_data_tbl(L_group_id).number_17  := to_number(GP_attrib_value_tbl(L_index).field_value);
            when 'NUMBER_18' then
               L_data_tbl(L_group_id).number_18  := to_number(GP_attrib_value_tbl(L_index).field_value);
            when 'NUMBER_19' then
               L_data_tbl(L_group_id).number_19  := to_number(GP_attrib_value_tbl(L_index).field_value);
            when 'NUMBER_20' then
               L_data_tbl(L_group_id).number_20  := to_number(GP_attrib_value_tbl(L_index).field_value);
            when 'DATE_21' then
               L_data_tbl(L_group_id).date_21    := to_date(GP_attrib_value_tbl(L_index).field_value);
            when 'DATE_22' then
               L_data_tbl(L_group_id).date_22    := to_date(GP_attrib_value_tbl(L_index).field_value);
            when 'DATE_23' then
               L_data_tbl(L_group_id).date_23    := to_date(GP_attrib_value_tbl(L_index).field_value);
            when 'DATE_24' then
               L_data_tbl(L_group_id).date_24    := to_date(GP_attrib_value_tbl(L_index).field_value);
            when 'DATE_25' then
               L_data_tbl(L_group_id).date_25    := to_date(GP_attrib_value_tbl(L_index).field_value);
         end case;

         L_index := GP_attrib_value_tbl.next(L_index);
      end loop;
   end if;


   --- build merge statement
   L_merge_init := 'merge into '||dbms_assert.sql_object_name(GP_ext_table) || ' ext ';

   if GP_header_cfg_tbl.count > 0 then
      L_index := GP_header_cfg_tbl.first;

      loop
         exit when L_index is NULL;

         if GP_header_cfg_tbl(L_index).header_type = KEY_WDGT then
            L_on_init := L_on_init ||GP_header_cfg_tbl(L_index).key_col||' = '||GP_key_value_tbl(L_index).field_value_qry || ' and ';
            ---
            L_insert_init := L_insert_init || GP_header_cfg_tbl(L_index).key_col || ', ';
            L_values_init := L_values_init || GP_key_value_tbl(L_index).field_value_qry || ', ';
         end if;
         ---
         L_index := GP_header_cfg_tbl.next(L_index);
      end loop;
   end if;


   L_group_id := L_data_tbl.first;

   loop

     exit when L_group_id is NULL;

     L_merge  := L_merge_init;
     L_on     := L_on_init;
     L_insert := L_insert_init;
     L_values := L_values_init;

     L_using := 'using (select '||L_group_id ||' group_id, '||
                               case when L_data_tbl(L_group_id).varchar2_1  is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).varchar2_1  ||'''' end||' varchar2_1, ' ||
                               case when L_data_tbl(L_group_id).varchar2_2  is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).varchar2_2  ||'''' end||' varchar2_2, ' ||
                               case when L_data_tbl(L_group_id).varchar2_3  is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).varchar2_3  ||'''' end||' varchar2_3, ' ||
                               case when L_data_tbl(L_group_id).varchar2_4  is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).varchar2_4  ||'''' end||' varchar2_4, ' ||
                               case when L_data_tbl(L_group_id).varchar2_5  is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).varchar2_5  ||'''' end||' varchar2_5, ' ||
                               case when L_data_tbl(L_group_id).varchar2_6  is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).varchar2_6  ||'''' end||' varchar2_6, ' ||
                               case when L_data_tbl(L_group_id).varchar2_7  is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).varchar2_7  ||'''' end||' varchar2_7, ' ||
                               case when L_data_tbl(L_group_id).varchar2_8  is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).varchar2_8  ||'''' end||' varchar2_8, ' ||
                               case when L_data_tbl(L_group_id).varchar2_9  is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).varchar2_9  ||'''' end||' varchar2_9, ' ||
                               case when L_data_tbl(L_group_id).varchar2_10 is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).varchar2_10 ||'''' end||' varchar2_10, '||
                               case when L_data_tbl(L_group_id).number_11   is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).number_11   ||'''' end||' number_11, '  ||
                               case when L_data_tbl(L_group_id).number_12   is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).number_12   ||'''' end||' number_12, '  ||
                               case when L_data_tbl(L_group_id).number_13   is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).number_13   ||'''' end||' number_13, '  ||
                               case when L_data_tbl(L_group_id).number_14   is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).number_14   ||'''' end||' number_14, '  ||
                               case when L_data_tbl(L_group_id).number_15   is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).number_15   ||'''' end||' number_15, '  ||
                               case when L_data_tbl(L_group_id).number_16   is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).number_16   ||'''' end||' number_16, '  ||
                               case when L_data_tbl(L_group_id).number_17   is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).number_17   ||'''' end||' number_17, '  ||
                               case when L_data_tbl(L_group_id).number_18   is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).number_18   ||'''' end||' number_18, '  ||
                               case when L_data_tbl(L_group_id).number_19   is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).number_19   ||'''' end||' number_19, '  ||
                               case when L_data_tbl(L_group_id).number_20   is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).number_20   ||'''' end||' number_20, '  ||
                               case when L_data_tbl(L_group_id).date_21     is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).date_21     ||'''' end||' date_21, '    ||
                               case when L_data_tbl(L_group_id).date_22     is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).date_22     ||'''' end||' date_22, '    ||
                               case when L_data_tbl(L_group_id).date_23     is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).date_23     ||'''' end||' date_23, '    ||
                               case when L_data_tbl(L_group_id).date_24     is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).date_24     ||'''' end||' date_24, '    ||
                               case when L_data_tbl(L_group_id).date_25     is NULL then 'NULL' else ''''||L_data_tbl(L_group_id).date_25     ||'''' end||' date_25 '     ||
                         'from dual) tmp ';

     L_on := L_on ||'ext.group_id = tmp.group_id) ';

     L_when_matched := 'when matched then '||
                           'update '||
                               'set '||'ext.varchar2_1 = tmp.varchar2_1, ' ||
                                       'ext.varchar2_2 = tmp.varchar2_2, ' ||
                                       'ext.varchar2_3 = tmp.varchar2_3, ' ||
                                       'ext.varchar2_4 = tmp.varchar2_4, ' ||
                                       'ext.varchar2_5 = tmp.varchar2_5, ' ||
                                       'ext.varchar2_6 = tmp.varchar2_6, ' ||
                                       'ext.varchar2_7 = tmp.varchar2_7, ' ||
                                       'ext.varchar2_8 = tmp.varchar2_8, ' ||
                                       'ext.varchar2_9 = tmp.varchar2_9, ' ||
                                       'ext.varchar2_10= tmp.varchar2_10, '||
                                       'ext.number_11  = tmp.number_11, '  ||
                                       'ext.number_12  = tmp.number_12, '  ||
                                       'ext.number_13  = tmp.number_13, '  ||
                                       'ext.number_14  = tmp.number_14, '  ||
                                       'ext.number_15  = tmp.number_15, '  ||
                                       'ext.number_16  = tmp.number_16, '  ||
                                       'ext.number_17  = tmp.number_17, '  ||
                                       'ext.number_18  = tmp.number_18, '  ||
                                       'ext.number_19  = tmp.number_19, '  ||
                                       'ext.number_20  = tmp.number_20, '  ||
                                       'ext.date_21    = tmp.date_21, '    ||
                                       'ext.date_22    = tmp.date_22, '    ||
                                       'ext.date_23    = tmp.date_23, '    ||
                                       'ext.date_24    = tmp.date_24, '    ||
                                       'ext.date_25    = tmp.date_25 ';

     L_when_not_matched := 'when not matched then '||
                              'insert ('||L_insert||
                                       'group_id, '||
                                       'varchar2_1, '||
                                       'varchar2_2, '||
                                       'varchar2_3, '||
                                       'varchar2_4, '||
                                       'varchar2_5, '||
                                       'varchar2_6, '||
                                       'varchar2_7, '||
                                       'varchar2_8, '||
                                       'varchar2_9, '||
                                       'varchar2_10,'||
                                       'number_11, ' ||
                                       'number_12, ' ||
                                       'number_13, ' ||
                                       'number_14, ' ||
                                       'number_15, ' ||
                                       'number_16, ' ||
                                       'number_17, ' ||
                                       'number_18, ' ||
                                       'number_19, ' ||
                                       'number_20, ' ||
                                       'date_21, '   ||
                                       'date_22, '   ||
                                       'date_23, '   ||
                                       'date_24, '   ||
                                       'date_25)'    ||
                               'values('||L_values||
                                       L_group_id||', '||
                                       'tmp.varchar2_1, '||
                                       'tmp.varchar2_2, '||
                                       'tmp.varchar2_3, '||
                                       'tmp.varchar2_4, '||
                                       'tmp.varchar2_5, '||
                                       'tmp.varchar2_6, '||
                                       'tmp.varchar2_7, '||
                                       'tmp.varchar2_8, '||
                                       'tmp.varchar2_9, '||
                                       'tmp.varchar2_10,'||
                                       'tmp.number_11, ' ||
                                       'tmp.number_12, ' ||
                                       'tmp.number_13, ' ||
                                       'tmp.number_14, ' ||
                                       'tmp.number_15, ' ||
                                       'tmp.number_16, ' ||
                                       'tmp.number_17, ' ||
                                       'tmp.number_18, ' ||
                                       'tmp.number_19, ' ||
                                       'tmp.number_20, ' ||
                                       'tmp.date_21, '   ||
                                       'tmp.date_22, '   ||
                                       'tmp.date_23, '   ||
                                       'tmp.date_24, '   ||
                                       'tmp.date_25)';

      if CFA_WRAPPER_SQL.GP_DEFAULT_ONLY_INSERT ='Y' then
            L_merge := L_merge || L_using || L_on || L_when_not_matched;
      else
            L_merge := L_merge || L_using || L_on || L_when_matched || L_when_not_matched;
      end if;
---
      DBG_SQL.MSG(L_program,
                  L_merge);

      EXECUTE IMMEDIATE L_merge;

      L_group_id := L_data_tbl.next(L_group_id);

   end loop;
   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END PERSIST_DATA;
--------------------------------------------------------------------------------
FUNCTION GET_VALUE_TBL(
   O_error_message    IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
   IO_value_tbl       IN OUT  NOCOPY   TYP_VALUE_TBL,
   I_value_type       IN               VARCHAR2)
RETURN BOOLEAN AS

   L_program            VARCHAR2(62) := 'CFA_SQL.GET_VALUE_TBL';

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   if I_value_type = CFA_KEY then
      IO_value_tbl := GP_key_value_tbl;
   elsif I_value_type = CFA_EXT then
      IO_value_tbl := GP_attrib_value_tbl;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_VALUE_TYPE');
      return FALSE;
   end if;

   ---
   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_VALUE_TBL;
--------------------------------------------------------------------------------------
FUNCTION GET_VALUE(
   O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   O_field_value       OUT      GP_FIELD_VALUE%TYPE,
   O_field_desc_value  OUT      GP_FIELD_DESC_VALUE%TYPE,
   O_data_type         OUT      GP_DATA_TYPE%TYPE,
   I_field_name        IN       CFA_SQL.GP_INDEX%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(62) := 'CFA_SQL.GET_VALUE';

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);

   if GP_key_value_tbl.exists(I_field_name) and GP_key_value_tbl(I_field_name).field_type = CFA_KEY then
      O_field_value      := GP_key_value_tbl(I_field_name).field_value;
      O_field_desc_value := GP_key_value_tbl(I_field_name).field_desc_value;
      O_data_type        := GP_key_value_tbl(I_field_name).data_type;
   elsif GP_attrib_value_tbl.exists(I_field_name)  then
      O_field_value      := GP_attrib_value_tbl(I_field_name).field_value;
      O_field_desc_value := GP_attrib_value_tbl(I_field_name).field_desc_value;
      O_data_type        := GP_attrib_value_tbl(I_field_name).data_type;
   else
      O_error_message := SQL_LIB.CREATE_MSG('FIELD_NOT_DEFINED');
      return FALSE;
   end if;

   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END GET_VALUE;
--------------------------------------------------------------------------------
FUNCTION SET_VALUE(
   O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_field_type         IN       GP_FIELD_TYPE%TYPE,
   I_field              IN       GP_FIELD_ITEM%TYPE,
   I_field_value        IN       GP_FIELD_VALUE%TYPE       DEFAULT NULL,
   I_field_desc_value   IN       GP_FIELD_DESC_VALUE%TYPE  DEFAULT NULL)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_SQL.SET_VALUE';
   L_index        GP_INDEX%TYPE;

   L_attrib_rec   TYP_attrib_rec;
   L_attrib_tbl   TYP_attrib_tbl;

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   L_index := I_field;

   if I_field_type = CFA_KEY then
      if NOT GP_key_value_tbl.exists(L_index) then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_NOT_DEFINED');
         return FALSE;
      end if;

      GP_key_value_tbl(L_index).field_name  := I_field;
      GP_key_value_tbl(L_index).field_value := I_field_value;
      GP_key_value_tbl(L_index).field_desc_value := I_field_desc_value;
   elsif I_field_type = CFA_EXT then
      if NOT GP_attrib_value_tbl.exists(L_index) then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_NOT_DEFINED');
         return FALSE;
      end if;

      GP_attrib_value_tbl(L_index).field_name  := I_field;
      GP_attrib_value_tbl(L_index).field_value := I_field_value;
      GP_attrib_value_tbl(L_index).field_desc_value := I_field_desc_value;

   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_TYPE');
   end if;

   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END SET_VALUE;
--------------------------------------------------------------------------------
FUNCTION SET_OUTPUT(
   O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_field_type         IN       GP_FIELD_TYPE%TYPE,
   I_field              IN       GP_FIELD_ITEM%TYPE,
   I_field_value        IN       GP_FIELD_VALUE%TYPE       DEFAULT NULL,
   I_field_desc_value   IN       GP_FIELD_DESC_VALUE%TYPE  DEFAULT NULL)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_SQL.SET_OUTPUT';
   L_index        GP_INDEX%TYPE;
BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   if I_field_type NOT in (CFA_SQL.CFA_KEY, CFA_SQL.CFA_EXT) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_TYPE');
   end if;
   -- Set the collection values
   if NOT SET_VALUE(O_error_message,
                    I_field_type,
                    I_field,
                    I_field_value,
                    I_field_desc_value) then
      return FALSE;
   end if;
   ---

   -- Set the output collection
   if NOT SET_RETURN_FIELD(O_error_message,
                           I_field) then
      return FALSE;
   end if;

   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END SET_OUTPUT;
--------------------------------------------------------------------------------
FUNCTION CLEAR_OUTPUT_TBL(
   O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(62) := 'CFA_SQL.CLEAR_OUTPUT_TBL';

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   GP_return_tbl.delete;

   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END CLEAR_OUTPUT_TBL;
--------------------------------------------------------------------------------
FUNCTION SET_RETURN_FIELD(
   O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
   I_field              IN       GP_FIELD_ITEM%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(62) := 'CFA_SQL.SET_RETURN_FIELD';

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   GP_return_tbl(GP_return_tbl.count).field_name := I_field;

   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END SET_RETURN_FIELD;
--------------------------------------------------------------------------------
FUNCTION CLEAR_TBLS(
   O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN AS

   L_program      VARCHAR2(62) := 'CFA_SQL.CLEAR_TBLS';

BEGIN
   DBG_SQL.MSG(L_program,
               'Begin: '||L_program);
   ---
   GP_return_tbl.delete;
   GP_header_cfg_tbl.delete;
   GP_attrib_cfg_tbl.delete;
   GP_key_value_tbl.delete;
   GP_attrib_value_tbl.delete;
   GP_return_tbl.delete;

   --LP_defaults_tbl.delete; -- von

   DBG_SQL.MSG(L_program,
               'End: '||L_program);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END CLEAR_TBLS;
--------------------------------------------------------------------------------
FUNCTION ADD_STORE_ATTRIB(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_store         IN     STORE.STORE%TYPE)
RETURN BOOLEAN IS
 
   L_program             VARCHAR2(62)   := 'CFA_SQL.ADD_STORE_ATTRIB';

BEGIN

   INSERT INTO store_cfa_ext(store,
                             group_id,
                             varchar2_1,
                             varchar2_2,
                             varchar2_3,
                             varchar2_4,
                             varchar2_5,
                             varchar2_6,
                             varchar2_7,
                             varchar2_8,
                             varchar2_9,
                             varchar2_10,
                             number_11,
                             number_12,
                             number_13,
                             number_14,
                             number_15,
                             number_16,
                             number_17,
                             number_18,
                             number_19,
                             number_20,
                             date_21,
                             date_22,
                             date_23,
                             date_24,
                             date_25)
                     (SELECT store,
                             group_id,
                             varchar2_1,
                             varchar2_2,
                             varchar2_3,
                             varchar2_4,
                             varchar2_5,
                             varchar2_6,
                             varchar2_7,
                             varchar2_8,
                             varchar2_9,
                             varchar2_10,
                             number_11,
                             number_12,
                             number_13,
                             number_14,
                             number_15,
                             number_16,
                             number_17,
                             number_18,
                             number_19,
                             number_20,
                             date_21,
                             date_22,
                             date_23,
                             date_24,
                             date_25
                        FROM store_add_cfa_ext 
                       WHERE store = I_store); 

   
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_STORE_ATTRIB;
--------------------------------------------------------------------------------
END;
/