create or replace
PACKAGE BODY CORESVC_ADMIN_SQL AS
----------------------------------------------------------------------------
FUNCTION DELETE_PROCESSES(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%type,
                          I_process_id_tab IN num_tab)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75) := 'CORESVC_ADMIN_SQL.DELETE_PROCESSES';
  RECORD_LOCKED   EXCEPTION;
  PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
  L_table         VARCHAR2(50);  
  L_file_id       SVC_PROCESS_TRACKER.file_id%type;
BEGIN
  
  L_table := 'SVC_PROCESS_TRACKER';
  for i IN 1..I_process_id_tab.count LOOP
     logger.log('Locking the row on SVC_PROCESS_TRACKER for delete'||I_process_id_tab(i));
     select file_id 
       into L_file_id 
       from SVC_PROCESS_TRACKER 
      where process_id = I_process_id_tab(i)
     FOR UPDATE NOWAIT;

     DELETE FROM S9T_ERRORS WHERE file_id = L_file_id;
     DELETE FROM S9T_FOLDER WHERE file_id = L_file_id;
  
  END LOOP;  
  
  forall i IN 1..I_process_id_tab.count
    DELETE FROM SVC_ADMIN_UPLD_ER WHERE process_id = I_process_id_tab(i);

  forall i IN 1..I_process_id_tab.count
    DELETE FROM SVC_PROCESS_TRACKER WHERE process_id = I_process_id_tab(i);


   RETURN TRUE;
EXCEPTION
WHEN RECORD_LOCKED THEN
  ROLLBACK;
  O_error_message := SQL_LIB.CREATE_MSG('RECORDS_LOCKED', L_table);
  RETURN FALSE;

WHEN OTHERS THEN
  ROLLBACK;
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END DELETE_PROCESSES;
----------------------------------------------------------------------------------------
FUNCTION INIT_PROCESS(O_error_message IN OUT rtk_errors.rtk_text%type,
                      I_process_id    IN svc_process_tracker.process_id%type,
    		      I_process_desc  IN svc_process_tracker.process_desc%type,
                      I_template_key  IN svc_process_tracker.template_key%type,
                      I_action_type   IN svc_process_tracker.action_type%type,
                      I_source        IN svc_process_tracker.process_source%type,
                      I_destination   IN svc_process_tracker.process_destination%type,
                      I_rms_async_id  IN svc_process_tracker.rms_async_id%type,
                      I_file_id       IN svc_process_tracker.file_id%type,
                      I_file_path     IN svc_process_tracker.file_path%type,
                      I_user_id       IN svc_process_tracker.user_id%type)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75):= 'CORESVC_ADMIN_SQL.INIT_PROCESS';
BEGIN
  INSERT
  INTO svc_process_tracker
    (
      process_id,
      process_desc,
      template_key,
      action_type,
      process_source,
      process_destination,
      status,
      user_id,
      rms_async_id,
      file_id,
      file_path,
      module_type
    )
    VALUES
    (
      I_process_id,
      I_process_desc,
      I_template_key,
      I_action_type,
      I_source,
      I_destination,
      'N',
      I_user_id,
      I_rms_async_id,
      I_file_id,
      I_file_path,
      'ADF'
    );
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END INIT_PROCESS;
-------------------------------------------------------------------------------------
FUNCTION PURGE_ADMIN_SVC_TABLES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(75)   := 'CORESVC_ADMIN_SQL.PURGE_ADMIN_SVC_TABLES';
   L_proc_ret_days   NUMBER;

   cursor C_GET_RETN_DAYS is
      select proc_data_retention_days
        from system_options;

   cursor C_GET_PROC_ID_PURGE is
      select process_id
        from svc_process_tracker
       where  process_destination in('RESA','RMS')
        and (status ='PS'
          or (status = 'PE' and
              action_date < (SYSDATE - L_proc_ret_days)))
         and template_key in (select template_key
                                   from s9t_template
                                  where template_type in (select code
                                                            from code_detail
                                                           where code_type in('RMST','RSAT')))
         and rownum <= 100;

   L_proc_id_purg_tbl   NUM_TAB;

BEGIN

   L_proc_ret_days := 0;

   open C_GET_RETN_DAYS;
   fetch C_GET_RETN_DAYS into L_proc_ret_days;
   close C_GET_RETN_DAYS;

   if L_proc_ret_days <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PROC_RETENTION_DAYS',
                                             NULL,
                                             NULL,
                                             NULL);
   end if;

   LOOP
      if C_GET_PROC_ID_PURGE%ISOPEN then
         close C_GET_PROC_ID_PURGE;
      end if;

      open C_GET_PROC_ID_PURGE;
      fetch C_GET_PROC_ID_PURGE BULK COLLECT INTO L_proc_id_purg_tbl;
      close C_GET_PROC_ID_PURGE;

      EXIT WHEN L_proc_id_purg_tbl is NULL or L_proc_id_purg_tbl.COUNT = 0;

      if L_proc_id_purg_tbl is NOT NULL and L_proc_id_purg_tbl.COUNT > 0 then
         if DELETE_PROCESSES(O_error_message,
                                       L_proc_id_purg_tbl) = FALSE then
            return FALSE;
         end if;
      end if;

      COMMIT;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END PURGE_ADMIN_SVC_TABLES;
-------------------------------------------------------------------------------------
FUNCTION GET_ADMIN_API(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_dnld_api        IN OUT   SVC_TMPL_API_MAP.DNLD_API%TYPE,
                       O_upld_api        IN OUT   SVC_TMPL_API_MAP.UPLD_API%TYPE,
                       O_process_api     IN OUT   SVC_TMPL_API_MAP.PROCESS_API%TYPE,
                       I_template_key    IN       SVC_TMPL_API_MAP.TEMPLATE_KEY%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(75)   := 'CORESVC_ADMIN_SQL.GET_ADMIN_API';

   cursor C_GET_ADMIN_API is
      select DNLD_API,
             UPLD_API,
             PROCESS_API
        from SVC_TMPL_API_MAP
       where TEMPLATE_KEY = I_template_key;

BEGIN

   open C_GET_ADMIN_API;
   fetch C_GET_ADMIN_API into O_dnld_api,O_upld_api,O_process_api;
   if C_GET_ADMIN_API%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('API_MAP_NOT_FOUND',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   close C_GET_ADMIN_API;

   return TRUE;

EXCEPTION
   when OTHERS then

      if C_GET_ADMIN_API%ISOPEN then
         close C_GET_ADMIN_API;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END GET_ADMIN_API;
-------------------------------------------------------------------------------------
FUNCTION CALL_DNLD_API(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id              IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind    IN       CHAR DEFAULT 'N',
                       I_dnld_api             IN       SVC_TMPL_API_MAP.DNLD_API%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(75)   := 'CORESVC_ADMIN_SQL.CALL_DNLD_API';
   L_plsql_block   VARCHAR2(500);
   L_bool          BOOLEAN;
BEGIN

   L_plsql_block := 'BEGIN :ret_bool := ' ||I_dnld_api|| '(:v1,:v2); END;';

   EXECUTE IMMEDIATE L_plsql_block USING OUT L_bool, IN OUT O_error_message, IN OUT O_file_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END CALL_DNLD_API;
-------------------------------------------------------------------------------------
FUNCTION CALL_UPLD_API(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_error_count          IN OUT   NUMBER,
                       I_file_id              IN       S9T_FOLDER.FILE_ID%TYPE,
                       I_process_id           IN       NUMBER,
                       I_upld_api             IN       SVC_TMPL_API_MAP.UPLD_API%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(75)   := 'CORESVC_ADMIN_SQL.CALL_UPLD_API';
   L_plsql_block     VARCHAR2(500);
   L_bool            BOOLEAN;

BEGIN

   L_plsql_block := 'BEGIN :ret_bool := ' ||I_upld_api|| '(:v1,:v2,:v3,:v4); END;';

   EXECUTE IMMEDIATE L_plsql_block USING OUT L_bool, IN OUT O_error_message,IN OUT O_error_count, IN I_file_id,IN I_process_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END CALL_UPLD_API;
-------------------------------------------------------------------------------------
FUNCTION CALL_PROCESS_API(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_error_count     IN OUT   NUMBER,
                          I_process_id      IN       NUMBER,
                          I_chunk_id        IN       NUMBER,
                          I_process_api     IN       SVC_TMPL_API_MAP.PROCESS_API%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(75)   := 'CORESVC_ADMIN_SQL.CALL_PROCESS_API';
   L_plsql_block   VARCHAR2(500);
   L_bool          BOOLEAN;

BEGIN
   L_plsql_block := 'BEGIN :ret_bool := ' ||I_process_api|| '(:v1,:v2,:v3,:v4); END;';

   EXECUTE IMMEDIATE L_plsql_block USING OUT L_bool, IN OUT O_error_message,IN OUT O_error_count, IN I_process_id,IN I_chunk_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END CALL_PROCESS_API;
-------------------------------------------------------------------------------------
FUNCTION DELETE_PROCESSES_WRP(O_error_message  IN OUT rtk_errors.rtk_text%type,
                              I_process_id_tab IN     num_tab)
RETURN NUMBER IS
  L_program VARCHAR2(255) := 'ITEM_INDUCT_SQL.DELETE_PROCESSES_WRP';
BEGIN
    if delete_processes(O_error_message,
                        I_process_id_tab) then
  RETURN 1;
  else RETURN 0;
  end if;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN 0;
END DELETE_PROCESSES_WRP;
----------------------------------------------------
END CORESVC_ADMIN_SQL;
/