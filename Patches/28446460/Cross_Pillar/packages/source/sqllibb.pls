CREATE OR REPLACE PACKAGE BODY SQL_LIB IS
---------------------------------------------------------------------
ERROR_LOOKUP   rtk_errors.rtk_key%TYPE; --lookup key for retek error table
ERROR_PROGRAM  VARCHAR2(65);            --program name
ERROR_ACTION   VARCHAR2(6);             --UPDATE,DELETE,SELECT,INSERT,FETCH,OPEN,CLOSE
ERROR_TABLE    VARCHAR2(255);           --table name(s), up to 255 character
ERROR_DATA     VARCHAR2(500);           --action, cursor, keys, program
ERROR_CODE     NUMBER(10);              --SQLCODE
ERROR_CURSOR   VARCHAR2(32);            --cursor name
ERROR_KEYS     VARCHAR2(150);           --key field(s) and value(s)
---------------------------------------------------------------------
FUNCTION PARSE_MSG (IO_msg IN OUT VARCHAR2,
                    O_key  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_key            VARCHAR2(255);
   L_txt_1          VARCHAR2(4000);
   L_txt_2          VARCHAR2(4000);
   L_txt_3          VARCHAR2(4000);
   L_old_key        VARCHAR2(4000) := IO_msg;
   L_begin_location NUMBER := 0;
   L_pos_1          NUMBER := 0;
   L_pos_2          NUMBER := 0;
   L_pos_3          NUMBER := 0;
   L_pos_4          NUMBER := 0;

BEGIN

   L_old_key := rtrim(substrb(IO_msg,1,255));

   if length (IO_msg) > 3 and substr (IO_msg, 1, 2) = '@0' then
      L_pos_1 := INSTR(L_old_key, '@0');
      L_pos_2 := INSTR(L_old_key, '@1');

      if L_pos_2 = 0 then -- there are no parameters
         L_key := substr (L_old_key, 3);
         L_txt_1 := NULL;
         L_txt_2 := NULL;
         L_txt_3 := NULL;
         goto build;
        else
         L_key := substr (L_old_key, 3, (L_pos_2-L_pos_1-2));
        end if;

      L_pos_3 := INSTR(L_old_key, '@2');

      if L_pos_3 = 0 then -- there is only one parameter
         L_txt_1 := substr (L_old_key, L_pos_2+2);
         L_txt_2 := NULL;
         L_txt_3 := NULL;
         goto build;
      else
         L_txt_1 := substr (L_old_key, L_pos_2+2, (L_pos_3-L_pos_2-2));
      end if;

      L_pos_4 := INSTR(L_old_key, '@3');

      if L_pos_4 = 0 then -- there are two parameters
         L_txt_2 := substr (L_old_key, L_pos_3+2);
         L_txt_3 := NULL;
         goto build;
      else
         L_txt_2 := substr (L_old_key, L_pos_3+2, (L_pos_4-L_pos_3-2));
         L_txt_3 := substr (L_old_key, L_pos_4+2);
      end if;

      <<build>>

      O_key := L_key;
      IO_msg := SQL_LIB.GET_MESSAGE_TEXT (L_key, L_txt_1, L_txt_2, L_txt_3);    
   end if;

   logger.log_permanent('sql_lib.parse_msg IO_msg:'||IO_msg||' L_key:'||O_key);

   return TRUE;

EXCEPTION

   when OTHERS then
      return FALSE;

END PARSE_MSG;
---------------------------------------------------------------------
FUNCTION CREATE_MSG(I_key   IN varchar2,
                    I_txt_1 IN varchar2 := NULL,
                    I_txt_2 IN varchar2 := NULL,
                    I_txt_3 IN varchar2 := NULL) RETURN VARCHAR2 IS

   L_merged_text  VARCHAR2 (4000);
   L_total_length NUMBER := 0;

BEGIN

   ERROR_LOOKUP := I_key;

   --- make sure the msg will fit
   if I_key is NULL then
      return NULL;
   else
      L_total_length := L_total_length + lengthb (I_key) + 2;
   end if;

   if I_txt_1 is not NULL then
      L_total_length := L_total_length + lengthb (I_txt_1) + 2;
   end if;

   if I_txt_2 is not NULL then
      L_total_length := L_total_length + lengthb (I_txt_2) + 2;
   end if;

   if I_txt_3 is not NULL then
      L_total_length := L_total_length + lengthb (I_txt_3) + 2;
   end if;

   if L_total_length > 255 then
      return (RTRIM(substrb(I_key||':'||I_txt_1||':'||I_txt_2||':'||I_txt_3, 1, 255)));
   end if;

   --  sets the forms error message and batch parameters

   if I_key = 'PACKAGE_ERROR' then
      ERROR_CODE := to_number(I_txt_3);
      ERROR_PROGRAM := I_txt_2;
      -- check again for 'PACKAGE_ERROR' when concatenating parameter three
   end if;

   --- create message
   L_merged_text := '@0' || I_key;

   if I_txt_1 is not NULL then
      L_merged_text := L_merged_text || '@1' || I_txt_1;
   end if;

   if I_txt_2 is not NULL then
      L_merged_text := L_merged_text || '@2' || I_txt_2;
   end if;

   if I_key != 'PACKAGE_ERROR' and I_txt_3 is not NULL then
      L_merged_text := L_merged_text || '@3' || I_txt_3;
   end if;
   
   logger.log_permanent('sql_lib.create_msg message:'||L_merged_text);
 
   return (L_merged_text);
EXCEPTION
   when OTHERS then

      return ('Unhandled error in SQL_LIB.CREATE_MSG: '||SQLERRM);


END CREATE_MSG;
---------------------------------------------------------------------
PROCEDURE SET_MARK(I_action IN VARCHAR2,
                   I_cursor IN VARCHAR2,
                   I_table  IN VARCHAR2,
                   I_keys   IN VARCHAR2) IS

BEGIN
   ERROR_ACTION := I_action;
   ERROR_CURSOR := I_cursor;
   ERROR_TABLE := I_table;
   ERROR_KEYS := I_keys;
   ERROR_CODE := NULL; 
END SET_MARK;
---------------------------------------------------------------------
PROCEDURE HANDLED_ERR(I_package_name IN VARCHAR2,
                      I_error_code   IN NUMBER) IS

BEGIN
   ERROR_PROGRAM := I_package_name;
   ERROR_CODE := I_error_code;
END HANDLED_ERR;
---------------------------------------------------------------------
PROCEDURE BATCH_MSG(O_err_code IN OUT NUMBER,
                    O_table    IN OUT VARCHAR2,
                    O_err_data IN OUT VARCHAR2) IS

L_key      rtk_errors.rtk_key%TYPE := NULL;

BEGIN

   if ERROR_CODE is NULL then --decode string and concatenate to pckname
      
      if PARSE_MSG(O_err_data,
                   L_key) = FALSE then
         O_err_data := 'Error parsing error message from SQL_LIB.BATCH_MSG ';
      end if;
      
      ERROR_CODE := 105;
      ---
      if ERROR_PROGRAM is not NULL and ERROR_LOOKUP != 'PACKAGE_ERROR' then
         O_err_data := ERROR_PROGRAM||'returns error: '||O_err_data;
      end if;
   else --(unhandled exception, populate with SET_MARK info)
      if ERROR_ACTION is NULL then
         if PARSE_MSG(O_err_data,
                      L_key) = FALSE then
            O_err_data := 'Error parsing error message from SQL_LIB.BATCH_MSG ';
         end if;
      elsif ERROR_CURSOR is NULL then
         ERROR_DATA := (ERROR_ACTION || ' in package ' ||
                        ERROR_PROGRAM || '     ' ||
                        ERROR_KEYS);

         O_err_data := ERROR_DATA;
      else
         ERROR_DATA := (ERROR_ACTION || ' cursor ' ||
                        ERROR_CURSOR || ' in package ' ||
                        ERROR_PROGRAM || '     ' ||
                        ERROR_KEYS);
         O_err_data := ERROR_DATA;
      end if;
   end if;

   if ERROR_CODE is NULL then
      O_err_code := 0;
   else
      O_err_code := ERROR_CODE;
   end if;

   if ERROR_TABLE is NULL then
      O_table := ' ';
   else
      O_table := ERROR_TABLE;
   end if;

   --reset values
   ERROR_CURSOR  := NULL;
   ERROR_PROGRAM := NULL;
   ERROR_KEYS    := NULL;
   ERROR_DATA    := NULL;
   ERROR_TABLE   := NULL;
   ERROR_ACTION  := NULL;
   ERROR_CODE    := NULL;

   logger.log_permanent('sql_lib.batch_msg O_err_code:'||O_err_code||' O_table:'||O_table||' O_err_data:'||O_err_data);

EXCEPTION
   when OTHERS then
      O_err_data := 'Unhandled error raised in SQL_LIB.BATCH_MSG'||
                ' procedure: '||SQLERRM;
END BATCH_MSG;
--------------------------------------------------------------------
FUNCTION GET_MESSAGE_TEXT(I_key   varchar2,
                          I_txt_1 varchar2 := NULL,
                          I_txt_2 varchar2 := NULL,
                          I_txt_3 varchar2 := NULL) return varchar2 IS
   ---
   cursor C_GET_MSG(S_KEY rtk_errors.rtk_key%TYPE) is
      select rtk_text
        from v_rtk_errors_tl
       where rtk_key = S_KEY;
   ---
   L_key_upper  varchar2(255) := null;
   L_key_raw    varchar2(255) := null;
   L_key        varchar2(4000) := null;
   L_txt_1      varchar2(4000) := null;
   L_txt_2      varchar2(4000) := null;
   L_txt_3      varchar2(4000) := null;
   ---
   L_disp_msg        varchar2(4000) := NULL; -- made larger to handle %s1,%s2, %s3 emessages.
   L_ret_val         number        := 0;   -- dummy return!
   L_sub_str         varchar2(3)   := '%s?';
   L_table           boolean       := FALSE; -- from table
   L_error_dummy     varchar2(4000);

BEGIN

   L_key_upper  := upper(rtrim(substrb(I_key,1,255))); 
   L_key_raw    := rtrim(substrb(I_key,1,255));       
   L_key        := rtrim(substrb(I_key,1,255)); 
   L_txt_1      := rtrim(substrb(I_txt_1,1,255));   
   L_txt_2      := rtrim(substrb(I_txt_2,1,255));  
   L_txt_3      := rtrim(substrb(I_txt_3,1,255)); 
   --
   -- figure out whether TO or NOT TO look in the table
   -- for the message. if the length of the key is > 25
   -- or if the key is mixed case then I will  NOT do a
   -- table lookup. OK!                   PhD 10-Nov-94
   --
   if L_key_raw != L_key_upper or    -- mixed case NOT a key
      length(L_key_raw) > 25         -- too long to be key
   then                              -- so key is the msg
      L_disp_msg := L_key_raw;       -- so start building it
   else                              -- otherwise DO a LOOKUP
      ---
      open C_GET_MSG(L_key_raw);
      fetch C_GET_MSG into L_disp_msg;
      if C_GET_MSG%NOTFOUND or L_disp_msg = 'x' then
         --
         -- Not Found so send back the incoming
         -- key surrounded by square brackets.
         -- 
         L_disp_msg := '[' || L_key_raw || ']';
         close C_GET_MSG;
         return (L_disp_msg);
      else
         close C_GET_MSG;
         L_table := TRUE;
      end if;
   end if;
   --
   -- did the user send in a second parameter
   -- to be included/appended in the message.
   --
   if L_txt_1 is not NULL then
   --
   -- he did, so if msg IS on the table and that
   -- message has a substitute string in it then put
   -- the string in place of that substitute string,
   -- otherwise append it.
   --
      L_sub_str := '%s1';

      if L_table AND instr(L_disp_msg, L_sub_str) > 0 then
         L_disp_msg := replace (L_disp_msg, L_sub_str, L_txt_1);      
      else     
         L_disp_msg := L_disp_msg || ' ' || L_txt_1;
      end if;
   end if;

   if L_txt_2 is not NULL then
   --
   -- check second parameter
   --
      L_sub_str := '%s2';

      if L_table AND instr(L_disp_msg, L_sub_str) > 0 then
         L_disp_msg := replace (L_disp_msg, L_sub_str, L_txt_2);
      else     
         L_disp_msg := L_disp_msg || ' ' || L_txt_2;
      end if;
   end if;

   if L_txt_3 is not NULL then
   --
   --
   -- check third parameter
   --
      L_sub_str := '%s3';

      if L_table AND instr(L_disp_msg, L_sub_str) > 0 then
         L_disp_msg := replace (L_disp_msg, L_sub_str, L_txt_3);
      else     
         L_disp_msg := L_disp_msg || ' ' || L_txt_3;
      end if;
   end if;

   --- Check totals
   if (NVL(LENGTHB(L_disp_msg),0) > 1000) then
      L_disp_msg := RTRIM(substrb(I_key||':'||I_txt_1||':'||I_txt_2||':'||I_txt_3, 1, 255));
      return (L_disp_msg);   
   end if;

   logger.log_permanent('sql_lib.get_message_text message:'||L_disp_msg);

   Return ( L_disp_msg );

EXCEPTION
   when OTHERS then
      return ('Unhandled error in SQL_LIB.GET_MESSAGE_TEXT: '||SQLERRM);
END GET_MESSAGE_TEXT;
----------------------------------------------------------------------------------------
FUNCTION CHECK_NUMERIC(O_error_message IN OUT VARCHAR2,
                       I_value         IN     VARCHAR2)
RETURN BOOLEAN IS   

   L_number        NUMBER;

BEGIN

   L_number := to_number(I_value);
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('NON_NUMERIC', 
                                            NULL, NULL, NULL); 
      RETURN FALSE;
END CHECK_NUMERIC;
---------------------------------------------------------------------
FUNCTION GET_USER_NAME(O_error_message IN OUT VARCHAR2,
                       O_user_name     IN OUT VARCHAR2)
                       RETURN BOOLEAN IS

   cursor C_GET_USER_NAME is
      select user
        from dual;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_USER_NAME', 
                    'DUAL',
                    NULL);

   open  C_GET_USER_NAME;
  
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_USER_NAME', 
                    'DUAL',
                    NULL);
 
   fetch C_GET_USER_NAME into O_user_name;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_USER_NAME', 
                    'DUAL',
                    NULL);

   close C_GET_USER_NAME;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM, 
                                             'SQL_LIB.GET_USER_NAME', 
                                             to_char(SQLCODE));
      RETURN FALSE;

END GET_USER_NAME;
---------------------------------------------------------------------------------------------
FUNCTION GET_RF_MSG (O_line1            OUT       VARCHAR2,
                     O_line2            OUT       VARCHAR2,
                     O_line3            OUT       VARCHAR2,
                     O_line4            OUT       VARCHAR2,
                     O_line5            OUT       VARCHAR2,
                     I_key              IN        rtk_errors.rtk_key%TYPE,
                     I_substr1          IN        varchar2 DEFAULT NULL,
                     I_substr2          IN        varchar2 DEFAULT NULL,
                     I_substr3          IN        varchar2 DEFAULT NULL)
   RETURN BOOLEAN
   IS
   
   L_err_text                   varchar2(4000);
   L_key                        rtk_errors.rtk_key%TYPE;
   L_text_length                NUMBER(3);
   L_begin_search_from          NUMBER(3);
   L_space_at                   NUMBER(2);
   L_SPACE             CONSTANT VARCHAR2(1) := ' ';
   L_MAX_LINE_LENGTH   CONSTANT NUMBER(2) := 18;   -- default value based on Wavelink error msg API
                                                   -- used in RSS1.0
                                                   -- future version of this function might take a
                                                   -- max line length input argument to generalize its use
                                                   -- across different text-terminal or gui-field widths.

BEGIN

   if ( SUBSTR(I_key, 1, 2) = '@0' ) then
      L_err_text := I_key;
      if PARSE_MSG(L_err_text,
                   L_key) = FALSE then
         NULL; -- if parse fails, then just display the key as passed in.
      end if;
   else
      L_err_text := GET_MESSAGE_TEXT(I_key, I_substr1, I_substr2, I_substr3);
   end if;

   L_text_length := NVL(LENGTH(L_err_text), 0);

   --------------------------
   -- set line 1
   --------------------------
   if (L_text_length <= L_MAX_LINE_LENGTH) then
      O_line1 := L_err_text;
      RETURN TRUE;
   else
      -----------------------
      -- L_begin_search_from will be used in calling 'INSTR'. It will have a negative value.  
      -- INSTR interprets a negative begin-from argument(-n, e.g.) by searching backwards,
      -- starting n characters from the end of the string.
      -- To determine the correct value for n in this case, subtract the length of the 
      -- entire text from the max line length.  This gives the position one to the right of the
      -- longest possible substring that can be returned.
      -----------------------
      L_begin_search_from := L_MAX_LINE_LENGTH - L_text_length;  

      L_space_at := INSTR(L_err_text, L_SPACE, L_begin_search_from);

      -- there are no spaces in the first 19 (max_line_len + 1) characters
      if (L_space_at = 0) then
         O_line1 := RTRIM(SUBSTRB(L_err_text, 1, L_MAX_LINE_LENGTH));
         L_err_text := RTRIM(SUBSTRB(L_err_text, L_MAX_LINE_LENGTH + 1));
         L_text_length := L_text_length - L_MAX_LINE_LENGTH;

      -- the 19th character is a space
      elsif (L_space_at = L_MAX_LINE_LENGTH + 1) then
         O_line1 := RTRIM(SUBSTRB(L_err_text, 1, L_MAX_LINE_LENGTH));
         L_err_text := RTRIM(SUBSTRB(L_err_text, L_MAX_LINE_LENGTH + 2));
         L_text_length := L_text_length - (L_MAX_LINE_LENGTH + 1);

      -- there is a space somewhere in the middle
      else
         O_line1 := RTRIM(SUBSTRB(L_err_text, 1, (L_space_at - 1)));
         L_err_text := RTRIM(SUBSTRB(L_err_text, (L_space_at + 1)));
         L_text_length := L_text_length - L_space_at;
      end if;
   end if;

   --------------------------
   -- set line 2
   --------------------------
   if (L_text_length <= L_MAX_LINE_LENGTH) then
      O_line2 := L_err_text;
      RETURN TRUE;
   else
      L_begin_search_from := L_MAX_LINE_LENGTH - L_text_length;  

      L_space_at := INSTR(L_err_text, L_SPACE, L_begin_search_from);

      if (L_space_at = 0) then
         O_line2 := RTRIM(SUBSTRB(L_err_text, 1, L_MAX_LINE_LENGTH));
         L_err_text := RTRIM(SUBSTRB(L_err_text, L_MAX_LINE_LENGTH + 1));
         L_text_length := L_text_length - L_MAX_LINE_LENGTH;
      elsif (L_space_at = L_MAX_LINE_LENGTH + 1) then
         O_line2 := RTRIM(SUBSTRB(L_err_text, 1, L_MAX_LINE_LENGTH));
         L_err_text := RTRIM(SUBSTRB(L_err_text, L_MAX_LINE_LENGTH + 2));
         L_text_length := L_text_length - (L_MAX_LINE_LENGTH + 1);
      else
         O_line2 := RTRIM(SUBSTRB(L_err_text, 1, (L_space_at - 1)));
         L_err_text := RTRIM(SUBSTRB(L_err_text, (L_space_at + 1)));
         L_text_length := L_text_length - L_space_at;
      end if;
   end if;

   --------------------------
   -- set line 3
   --------------------------
   if (L_text_length <= L_MAX_LINE_LENGTH) then
      O_line3 := L_err_text;
      RETURN TRUE;
   else
      L_begin_search_from := L_MAX_LINE_LENGTH - L_text_length;  

      L_space_at := INSTR(L_err_text, L_SPACE, L_begin_search_from);

      if (L_space_at = 0) then
         O_line3 := RTRIM(SUBSTRB(L_err_text, 1, L_MAX_LINE_LENGTH));
         L_err_text := RTRIM(SUBSTRB(L_err_text, L_MAX_LINE_LENGTH + 1));
         L_text_length := L_text_length - L_MAX_LINE_LENGTH;
      elsif (L_space_at = L_MAX_LINE_LENGTH + 1) then
         O_line3 := RTRIM(SUBSTRB(L_err_text, 1, L_MAX_LINE_LENGTH));
         L_err_text := RTRIM(SUBSTRB(L_err_text, L_MAX_LINE_LENGTH + 2));
         L_text_length := L_text_length - (L_MAX_LINE_LENGTH + 1);
      else
         O_line3 := RTRIM(SUBSTRB(L_err_text, 1, (L_space_at - 1)));
         L_err_text := RTRIM(SUBSTRB(L_err_text, (L_space_at + 1)));
         L_text_length := L_text_length - L_space_at;
      end if;
   end if;

   --------------------------
   -- set line 4
   --------------------------
   if (L_text_length <= L_MAX_LINE_LENGTH) then
      O_line4 := L_err_text;
      RETURN TRUE;
   else
      L_begin_search_from := L_MAX_LINE_LENGTH - L_text_length;  

      L_space_at := INSTR(L_err_text, L_SPACE, L_begin_search_from);

      if (L_space_at = 0) then
         O_line4 := RTRIM(SUBSTRB(L_err_text, 1, L_MAX_LINE_LENGTH));
         L_err_text := RTRIM(SUBSTRB(L_err_text, L_MAX_LINE_LENGTH + 1));
         L_text_length := L_text_length - L_MAX_LINE_LENGTH;
      elsif (L_space_at = L_MAX_LINE_LENGTH + 1) then
         O_line4 := RTRIM(SUBSTRB(L_err_text, 1, L_MAX_LINE_LENGTH));
         L_err_text := RTRIM(SUBSTRB(L_err_text, L_MAX_LINE_LENGTH + 2));
         L_text_length := L_text_length - (L_MAX_LINE_LENGTH + 1);
      else
         O_line4 := RTRIM(SUBSTRB(L_err_text, 1, (L_space_at - 1)));
         L_err_text := RTRIM(SUBSTRB(L_err_text, (L_space_at + 1)));
         L_text_length := L_text_length - L_space_at;
      end if;
   end if;

  --------------------------
   -- set line 5
   --------------------------
   if (L_text_length <= L_MAX_LINE_LENGTH) then
      O_line5 := L_err_text;
      RETURN TRUE;
   else
      L_begin_search_from := L_MAX_LINE_LENGTH - L_text_length;

      L_space_at := INSTR(L_err_text, L_SPACE, L_begin_search_from);

      if ((L_space_at = 0) OR (L_space_at = L_MAX_LINE_LENGTH + 1)) then
         O_line5 := RTRIM(SUBSTRB(L_err_text, 1, L_MAX_LINE_LENGTH));
      else
         O_line5 := RTRIM(SUBSTRB(L_err_text, 1, (L_space_at - 1)));
      end if;
   end if;

   RETURN TRUE;   

EXCEPTION
   when OTHERS then
      RETURN FALSE;
END GET_RF_MSG;
---------------------------------------------------------------------
FUNCTION GET_ERROR_TYPE(O_error_type    IN OUT VARCHAR2,
                        I_key           IN     VARCHAR2) 
RETURN BOOLEAN IS


 cursor C_GET_ERROR_TYPE is
      select rtk_type
        from rtk_errors
       where rtk_key = I_key;

BEGIN

   open C_GET_ERROR_TYPE;
   fetch C_GET_ERROR_TYPE into O_error_type;
   close C_GET_ERROR_TYPE;

   return TRUE;

EXCEPTION
   when OTHERS then
      return FALSE;
END GET_ERROR_TYPE;
--------------------------------------------------------------------
PROCEDURE API_MSG(O_error_type    IN OUT VARCHAR2,
                  O_error_message IN OUT VARCHAR2) IS

L_key    rtk_errors.rtk_key%TYPE := NULL;

BEGIN

   -- decode string -- 
   if PARSE_MSG(O_error_message,
                L_key) = FALSE then
        O_error_message := 'Error parsing error message from SQL_LIB.API_MSG ';
   end if;

   -- retrive error type using key --     
   if GET_ERROR_TYPE(O_error_type,
                     L_key) = FALSE then
       O_error_message := 'Error parsing error message from SQL_LIB.BATCH_MSG ';
   end if;
   
   logger.log_permanent('sql_lib.api_msg O_error_type:'||O_error_type||' O_error_message:'||O_error_message);

EXCEPTION
   when OTHERS then
      O_error_message := 'Unhandled error raised in SQL_LIB.API_MSG'||
                ' procedure: '||SQLERRM;
END API_MSG;
--------------------------------------------------------------------
FUNCTION PARSE_MSG (I_key IN  VARCHAR2)
RETURN VARCHAR2  IS

   L_key            VARCHAR2(255);
   L_txt_1          VARCHAR2(4000);
   L_txt_2          VARCHAR2(4000);
   L_txt_3          VARCHAR2(4000);
   L_old_key        VARCHAR2(4000) := I_key;
   L_begin_location NUMBER := 0;
   L_pos_1          NUMBER := 0;
   L_pos_2          NUMBER := 0;
   L_pos_3          NUMBER := 0;
   L_pos_4          NUMBER := 0;
   L_msg            RTK_ERRORS.RTK_TEXT%TYPE; 

BEGIN

   L_old_key := rtrim(substrb(I_key,1,255));
   L_pos_1   := INSTR(L_old_key, '@0');
   IF L_pos_1 = 0 THEN
      L_key :=  L_old_key;
      L_txt_1 := NULL;
      L_txt_2 := NULL;
      L_txt_3 := NULL;
      goto build;
   END IF;
   L_pos_1 := INSTR(L_old_key, '@0');
   L_pos_2 := INSTR(L_old_key, '@1');

   if L_pos_2 = 0 then -- there are no parameters
      L_key := substr (L_old_key, 3);
      L_txt_1 := NULL;
      L_txt_2 := NULL;
      L_txt_3 := NULL;
      goto build;
   else
      L_key := substr (L_old_key, 3, (L_pos_2-L_pos_1-2));
   end if;

   L_pos_3 := INSTR(L_old_key, '@2');

   if L_pos_3 = 0 then -- there is only one parameter
      L_txt_1 := substr (L_old_key, L_pos_2+2);
      L_txt_2 := NULL;
      L_txt_3 := NULL;
      goto build;
   else
      L_txt_1 := substr (L_old_key, L_pos_2+2, (L_pos_3-L_pos_2-2));
   end if;

   L_pos_4 := INSTR(L_old_key, '@3');

   if L_pos_4 = 0 then -- there are two parameters
      L_txt_2 := substr (L_old_key, L_pos_3+2);
      L_txt_3 := NULL;
      goto build;
   else
      L_txt_2 := substr (L_old_key, L_pos_3+2, (L_pos_4-L_pos_3-2));
      L_txt_3 := substr (L_old_key, L_pos_4+2);
   end if;
   <<build>>
       
   L_msg := SQL_LIB.GET_MESSAGE_TEXT (L_key, L_txt_1, L_txt_2, L_txt_3);

   logger.log_permanent('sql_lib.parse_msg message:'||L_msg);

   return L_msg;

END PARSE_MSG;
----------------------------------------------------------------------------------
END SQL_LIB;
/


